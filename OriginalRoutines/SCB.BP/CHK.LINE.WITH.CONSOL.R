*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CHK.LINE.WITH.CONSOL.R
$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.RE.STAT.LINE.CONT
$INCLUDE T24.BP I_F.CONSOLIDATE.ASST.LIAB
$INCLUDE T24.BP I_F.CATEGORY
*---------------------------------------------------
    FN.STAT = "FBNK.RE.STAT.LINE.CONT"
    F.STAT  = ""
*
    FN.CONSL = "FBNK.CONSOLIDATE.ASST.LIAB"
    F.CONSL  = ""
*
    FN.CATEG = "F.CATEGORY"
    F.CATEG = ""
*----------------------------------------------------
    WS.LIN = 0
    WS.NET.ALL = 0
*----------------------------------------------------
    CALL OPF (FN.STAT,F.STAT)
*
    CALL OPF (FN.CONSL,F.CONSL)
*
    CALL OPF (FN.CATEG,F.CATEG)

*----------------------------GET DATE FOR COMPAIRING --------------------
    WS.NOT.FOUND = 0
*----------------------------GET DATE FOR COMPAIRING --------------------
*    YTEXT = "ENTER BRANCH CODE EG00100XX"
    YTEXT = "ENTER BRANCH CODE XX"
    CALL TXTINP(YTEXT, 8, 22, "2.2", "A")
    WS.ACPT = COMI
    IF WS.ACPT EQ "XX" THEN
        RETURN
    END
    IF WS.ACPT NE "XX" THEN
        WS.CO = "EG00100":WS.ACPT
        GOSUB A.100.PROC
    END
    IF WS.NOT.FOUND EQ 1 THEN
        TEXT = "�� ���� ������ �����" ; CALL REM
    END
    IF WS.NOT.FOUND EQ 0 THEN
        TEXT = "�� ���� ������ ��� �����" ; CALL REM
    END
    RETURN
*------------------------------------------------
A.100.PROC:
    WS.STAT.ID = "GENLED.9990.":WS.CO
    CALL F.READ(FN.STAT,WS.STAT.ID,R.STAT,F.STAT,MSG.STAT)
    WS.COUNT = DCOUNT(R.STAT<RE.SLC.ASST.CONSOL.KEY>,@VM)
    GOSUB A.200.READ.CONSOL
A.100.EXIT:
    RETURN
*-------------------------------------------------
A.200.READ.CONSOL:
    FOR A = 1 TO WS.COUNT
        WS.CRT =  R.STAT<RE.SLC.ASST.CONSOL.KEY,A>
        WS.RS.ASST.TYPE = R.STAT<RE.SLC.ASSET.TYPE,A>
        GOSUB A.210.READ
    NEXT A
    RETURN
*--------------------------------
A.210.READ:
    WS.CRT.1 =  WS.CRT[1,2]
    WS.RS.ASST.TYPE.1 = WS.RS.ASST.TYPE[1,7]
    IF WS.CRT.1 EQ "LD" AND WS.RS.ASST.TYPE.1 EQ "FORWARD" THEN
        RETURN
    END
    WS.CONSL.ID =  R.STAT<RE.SLC.ASST.CONSOL.KEY,A>
    CALL F.READ(FN.CONSL,WS.CONSL.ID,R.CONSL,F.CONSL,MSG.CONSL)
    WS.CY = R.CONSL<RE.ASL.CURRENCY>
    WS.GL = R.CONSL<RE.ASL.VARIABLE.1>

    CALL F.READ(FN.CATEG,WS.GL,R.CATEG,F.CATEG,MSG.CATEG)
    WS.CAT.NAME = R.CATEG<EB.CAT.DESCRIPTION,2>

*    WS.CNT.DR = DCOUNT(R.CONSL<RE.ASL.DEBIT.MOVEMENT>,@VM)
    WS.CNT.DR = DCOUNT(R.CONSL<RE.ASL.TYPE>,@VM)
    GOSUB A.220.CHK
A.210.EXIT:
    RETURN
*---------------------------------------------------
A.220.CHK:
    FOR B = 1 TO WS.CNT.DR
        GOSUB A.230.READ
    NEXT A
    RETURN
*------------------------------------------------------
A.230.READ:
    WS.NET = 0
    WS.NET.L = 0
    WS.NET.DSP = 0
    WS.DR.MV = R.CONSL<RE.ASL.DEBIT.MOVEMENT,B>
    WS.CR.MV = R.CONSL<RE.ASL.CREDIT.MOVEMENT,B>
    WS.BAL   = R.CONSL<RE.ASL.BALANCE,B>
    WS.NET = WS.DR.MV + WS.CR.MV + WS.BAL

    WS.DR.L  = R.CONSL<RE.ASL.LOCAL.DEBIT.MVE,B>
    WS.CR.L  = R.CONSL<RE.ASL.LOCAL.CREDT.MVE,B>
    WS.BAL.L = R.CONSL<RE.ASL.LOCAL.BALANCE,B>
    WS.NET.L = WS.DR.L + WS.CR.L + WS.BAL.L

    IF  WS.NET EQ 0 AND WS.NET.L EQ 0 THEN
        RETURN
    END
    IF  WS.NET EQ "" AND WS.NET.L EQ "" THEN
        RETURN
    END

    IF  WS.CY EQ "EGP" THEN
        WS.NET.DSP = WS.NET
    END

    IF  WS.CY NE "EGP" THEN
        WS.NET.DSP = WS.NET.L
    END
TEXT = WS.CAT.NAME:"         ":"CATEG = ":WS.GL:" ":WS.CY:"  ":WS.NET.DSP ; CALL REM
WS.NOT.FOUND = 2

    RETURN
END
