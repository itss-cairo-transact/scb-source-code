
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.NO

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_ENQUIRY.COMMON
$INCLUDE T24.BP I_F.ACCR.ACCT.CR
$INCLUDE TEMENOS.BP I_F.SCB.BT.BATCH

    IF O.DATA THEN

        CONT = DCOUNT(O.DATA,@VM)
        TEXT = "CONT" : CONT ; CALL REM
        O.DATA = CONT
    END
    RETURN
END
