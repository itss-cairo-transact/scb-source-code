    SUBROUTINE SBW.STMT.IN.OUT1000.R3

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_ENQUIRY.COMMON
$INCLUDE T24.BP I_F.STMT.ENTRY

    REC.ID = O.DATA

    FN.STMT = "FBNK.STMT.ENTRY"
    F.STMT = ''
    R.STMT=''
    Y.STMT=''
    Y.STMT.ERR=''
*------------------------
    CALL OPF(FN.STMT,F.STMT)
*------------------------
*****************************************************************
    CALL F.READ(FN.STMT,REC.ID,R.STMT,F.STMT,ERR.STMT)

    DR.AMT  = 0
    CR.AMT  = 0

    YY = DCOUNT(R.STMT<AC.STE.AMOUNT.LCY>,VM)
    FOR X = 1 TO YY

        AMT = R.STMT<AC.STE.AMOUNT.LCY,1,X>
***        IF    AMT LT 0    THEN
***            DR.AMT  = DR.AMT + AMT
***        END
        IF    AMT GT 0    THEN
            CR.AMT  = CR.AMT + AMT
        END
    NEXT X

    O.DATA = CR.AMT

    RETURN
END
