    SUBROUTINE SBD.CASH.RES

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.USER
$INCLUDE T24.BP I_F.RE.STAT.LINE.BAL
$INCLUDE T24.BP I_F.DEPT.ACCT.OFFICER
$INCLUDE T24.BP I_F.RE.STAT.REP.LINE
$INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM
$INCLUDE T24.BP I_F.DATES
$INCLUDE T24.BP I_F.COMPANY
$INCLUDE T24.BP I_USER.ENV.COMMON
    COMP = ID.COMPANY


    DAT.ID = COMP
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.CASH.RES'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.OPEN.BAL = 0
    Y.DEP.BAL = 0
    Y.TELL.BAL = 0
    Y.TOT.TELL.BAL = 0
    Y.TOT.DEP.BAL = 0

    RETURN
*===============================================================
CALLDB:
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''

    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*************************************************************************
*========================================================================
    T.SEL  = "SELECT ":FN.LN:" WITH @ID IN ( GENLED.0110 GENLED.0510 GENLED.0520 GENLED.0530 GENLED.0540 GENLED.0550 )"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,2,2>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,ECC)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
            POS = DCOUNT(CCURR,VM)
*************************************************************************
            FOR X = 1 TO POS
                BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":COMP
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                RATE = R.CCY<RE.BCP.RATE,X>
                IF DESC.ID EQ "0110" THEN
                    Y.TELL.BAL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                END ELSE
                    Y.DEP.BAL +=  R.BAL<RE.SLB.CLOSING.BAL.LCL>
                END
            NEXT X
********************************************************************
        NEXT I
        XX  = SPACE(120)
        XX<1,1>[1,20]   = 0
        XX<1,1>[25,20]  = Y.DEP.BAL
        XX<1,1>[50,20]  = Y.TELL.BAL
        PRINT XX<1,1>
*-------------------------------------------------------------------
    END ELSE
        ZZ = 0
    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TDD = TD1
    T.DAY1 = FMT(TDD,"####/##/##")
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":REPORT.ID
    PR.HD :="'L'":SPACE(50):"���� ��������� ������ �� :":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� �������" :SPACE(11):"�������":SPACE(13):"�������"
    PR.HD :="'L'":STR('_',120)
    HEADING PR.HD
    PRINT
    RETURN
*==============================================================
END
