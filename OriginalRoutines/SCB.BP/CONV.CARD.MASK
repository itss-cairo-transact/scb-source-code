****NESSREEN AHMED 19/4/2010********
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CARD.MASK

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_ENQUIRY.COMMON
    $INCLUDE T24.BP I_F.CARD.ISSUE
    $INCLUDE TEMENOS.BP I_F.SCB.VISA.TRANS.TOT
    $INCLUDE TEMENOS.BP I_F.SCB.MAST.TRANS
    IF O.DATA THEN
        CARD.ID = O.DATA
        N1 = CARD.ID[1,6]
        N2 = CARD.ID[13,4]
        NNN = N1:"XXXXXX":N2
        O.DATA = NNN
    END
    RETURN
END
