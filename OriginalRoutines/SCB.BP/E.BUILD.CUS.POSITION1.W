* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
    SUBROUTINE E.BUILD.CUS.POSITION1.W(ENQUIRY.DATA)
    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_ENQUIRY.COMMON
    $INCLUDE T24.BP I_CUSTOMER.POSITION.COMMON
    $INCLUDE T24.BP I_F.CUSTOMER.POSITION
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE T24.BP I_F.USER
    $INCLUDE TEMENOS.BP I_CU.LOCAL.REFS

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''

    YTEXT = "Pleas put the customer number here : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    WAGDY.XX = COMI


** 19/12/97 - GB9701470
**            Build the data dependent on the rebuild flag being set
*
*  08/06/05 - EN_10002549
*             Set C$CUS.POS.UPDATE.XREF to 0, so that cache is used
*             instead on CUSTOMER.POSITION.XREF.

*   LOCATE 'REBUILD.DATA' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
*       REBUILD = ENQUIRY.DATA<4,RB.POS>
*   END ELSE
*       REBUILD = 'Y'
*    END
*   IF REBUILD[1,1] NE 'N' THEN         ;* Do not execute on level dwon
*LOCATE "CUSTOMER.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
    CUST.ID = WAGDY.XX
    CONVERT " " TO VM IN CUST.ID
    IF DCOUNT(CUST.ID,VM) GT 1 OR CUST.ID = "ALL" THEN
        ENQ.ERROR = "ONLY ONE CUSTOMER ALLOWED"
    END ELSE
*********************************************
        FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""
        CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,READ.ERR1)
        LOOP
            REMOVE AC.ID FROM R.CUS SETTING POS.CUST
        WHILE AC.ID:POS.CUST

* IF ( AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 ) OR ( AC.ID[11,4] EQ 1710 OR AC.ID[11,4] EQ 1711 ) THEN
*    CUST.ID = "DUMMY"
* END
            CALL OPF( FN.ACCOUNT,F.ACC)
            CALL F.READ(FN.ACCOUNT,AC.ID,R.ACC,F.ACC, ETEXT)
            SUSP = R.ACC<AC.INT.NO.BOOKING>

* IF  SUSP EQ "SUSPENSE"  THEN
*    CUST.ID = "DUMMY"
* END
        REPEAT
********************************************************
        C$CUST.POS.UPDATE.XREF = 0      ;* EN_10002549
        CALL CUS.BUILD.POSITION.DATA(WAGDY.XX)
        C$CUST.POS.UPDATE.XREF = 1      ;* EN_10002549 Reset to 1
    END
*END
*END
*******UPDATED BY NESSREEN AHMED ON 30/12/2008**********************
    FN.CU = 'FBNK.CUSTOMER' ;F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    CALL F.READ(FN.CU,WAGDY.XX,R.CU,F.CU,READ.ERR1)

    EMP.NO = R.CU<EB.CUS.LOCAL.REF,CULR.EMPLOEE.NO>

    USR.N =  R.USER<EB.USE.SIGN.ON.NAME>
    USR.ID = TRIM(USR.N, "0", "L")
*TEXT = "USR.ID" : USR.ID  ; CALL REM
*TEXT =  "EMP.NO" : EMP.NO ; CALL REM
    IF  EMP.NO NE USR.ID THEN
*TEXT = "HIII" ; CALL REM
        FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
        CALL OPF(FN.CUS.POS,F.CUS.POS)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""

        T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":WAGDY.XX :"... AND (@ID LIKE ...*1002)"

        CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)
***Updated by Nessreen Ahmed 25/9/2016 *************************
            DELETE F.CUS.POS , KEY.LIST<I>
***         CALL F.DELETE (FN.CUS.POS , KEY.LIST<I>)
***End of Update 25/9/2016 9 ****************************
        NEXT I
    END
*************************************************************
*
    RETURN
*
END
