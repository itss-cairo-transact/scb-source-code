    SUBROUTINE CONV.INIT.APP

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_ENQUIRY.COMMON
$INCLUDE T24.BP I_USER.ENV.COMMON
$INCLUDE T24.BP I_F.HELPTEXT.MAINMENU
$INCLUDE T24.BP I_F.USER
*----------------------------------------------------------------------**
    COMP = ID.COMPANY
    USE.ID   = ''
    USE.ID   = O.DATA
    FN.USE   = 'F.USER'            ; F.USE    = ''
    CALL OPF(FN.USE,F.USE)
    FN.APP = 'F.HELPTEXT.MAINMENU'  ; F.APP    = ''
    CALL OPF(FN.APP,F.APP)
    NUM.C    = 0
    XX       = ""
    HMM.ID   = ''
***--------------------------------------------------------------****
    CALL F.READ(FN.USE,USE.ID,R.USE,F.USE,E11)
    INIT.APP.NN  = R.USE<EB.USE.INIT.APPLICATION>
    HMM.ID.NN    = FIELD(INIT.APP.NN,'?',2)

    CALL F.READ(FN.APP,HMM.ID.NN,R.APP,F.APP,E12)
    APP.NAM  = R.APP<EB.MME.DESCRIPT>
    NUM.C    = DCOUNT(APP.NAM,VM)

    FOR I = 1 TO NUM.C
        XX = XX :" / ":APP.NAM<1,I>
    NEXT I

    O.DATA    = XX
    RETURN
*==============================================================********
END
