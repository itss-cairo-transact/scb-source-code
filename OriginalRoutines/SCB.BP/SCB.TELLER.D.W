*-----------------------------------------------------------------------------
* <Rating>1046</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

    SUBROUTINE SCB.TELLER.D.W


******************************************************************

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE T24.BP I_F.CURRENCY
    $INCLUDE T24.BP I_F.CARD.TYPE
    $INCLUDE TEMENOS.BP I_CU.LOCAL.REFS
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

    IF E THEN V$ERROR = 1

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

*R.NEW(HOLD.REG.VAL.PAY) = "NO"
*R.NEW(HOLD.DATE.RIGHT.TO) = TODAY
*R.NEW(HOLD.VALID.CCY) = LCCY


    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

*  IF AF = HOLD.RETURN.RIGHT.TO THEN

*   IF COMI < R.NEW(HOLD.DATE.RIGHT.TO) THEN

*  E = "UNEXPECTED"
*  CALL REBUILD.SCREEN ; P = 0

*  END

* END
*..................................
*   IF AF = HOLD.DATE.RIGHT.TO THEN

*   IF COMI > R.NEW(HOLD.RETURN.RIGHT.TO) THEN

*  E = "UNEXPECTED"
*  CALL REBUILD.SCREEN ; P = 0

*  END

* END
*..................................

* IF AF = HOLD.RETURN.RIGHT.FROM THEN

*    IF COMI < R.NEW(HOLD.DATE.RIGHT.FROM) THEN

*   E = "UNEXPECTED"
*   CALL REBUILD.SCREEN ; P = 0

*    END

*  END
    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************
DELIVERY.PREVIEW:

    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
    CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
    CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = "" ; MAT N = "" ; MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = ""

    ID.F  = "TT.ID"; ID.N  = "20"; ID.T  = "A"
*
    Z=0
*
***********************************************************************
    Z+=1 ; F(Z)  = "TR.FLAG"          ;  N(Z) = "1"  ; T(Z) = "A" ; T(Z)<2> = "D_W"
    Z+=1 ; F(Z)  = "ID.REF"           ;  N(Z) = "16" ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CARD.CURRENCY"    ;  N(Z) = "3"  ; T(Z) = "" ; T(Z) = "A"
    CHECKFILE(Z) = "CURRENCY":FM:3:FM:"L"
    Z+=1 ; F(Z)  = "AMT.LCY"          ;  N(Z) = "19" ; T(Z) = "AMT"
    Z+=1 ; F(Z)  = "AMT.FCY"          ;  N(Z) = "19" ; T(Z) = "AMT"
    Z+=1 ; F(Z)  = "RATE"             ;  N(Z) = "19" ; T(Z) = "AMT"
    Z+=1 ; F(Z)  = "NEW.SECTOR"       ;  N(Z) = "10" ; T(Z) = ""
    Z+=1 ; F(Z)  = "SEC.FLAG"         ;  N(Z) = "1"  ; T(Z) = ""   ; T(Z)<2> = "1_2"
    Z+=1 ; F(Z)  = "ACCOUNT.1"        ;  N(Z) = "16" ; T(Z) = "A"
    CHECKFILE(Z) = "ACCOUNT":FM:AC.SHORT.TITLE
    Z+=1 ; F(Z)  = "ACCOUNT.2"        ;  N(Z) = "16" ; T(Z) = "A"
    CHECKFILE(Z) = "ACCOUNT":FM:AC.SHORT.TITLE
    Z+=1 ; F(Z)  = "VAULT.AC.NO"      ;  N(Z) = "16" ; T(Z) = "A"
    CHECKFILE(Z) = "ACCOUNT":FM:AC.SHORT.TITLE
    Z+=1 ; F(Z)  = "AUTH.DATE"        ;  N(Z) = "8"  ; T(Z) = "D"
    Z+=1 ; F(Z)  = "COMP.CO"          ;  N(Z) = "9"     ;  T(Z)= "ANY"
    CHECKFILE(Z) = "COMPANY":FM:1:FM:"L"
*****UPDATED BY NESSREEN AHMED 6/2/2018********************
    Z+=1 ; F(Z)  = "TRANS.TYPE"       ;  N(Z) = "8" ; T(Z) = "A"
    Z+=1 ; F(Z)  = "TR.TYPE.CL"       ;  N(Z) = "8" ; T(Z) = "A"
*****END OF UPDATE 6/2/2018********************************

    V = Z + 9
    RETURN
*************************************************************************
END
