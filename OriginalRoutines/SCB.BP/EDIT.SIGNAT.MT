*-----------------------------------------------------------------------------
* <Rating>81</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EDIT.SIGNAT.MT(ENQ)

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_ENQUIRY.COMMON
$INCLUDE T24.BP I_F.CUSTOMER
$INCLUDE T24.BP I_F.COMPANY
$INCLUDE T24.BP I_F.IM.DOCUMENT.IMAGE
$INCLUDE T24.BP I_F.USER
$INCLUDE T24.BP I_USER.ENV.COMMON
    COMP = C$ID.COMPANY
    DDD = TODAY[3,6]
    DD = "...":DDD:"..."
* DD = "...050712..."
*    TEXT = DD ; CALL REM
    CUS.USR = R.USER<EB.USE.DEPARTMENT.CODE>
    T.SEL = "SELECT F.IM.DOCUMENT.IMAGE WITH DATE.TIME LIKE ":DD:" AND CURR.NO GT 1  AND DEPT.CODE EQ ": CUS.USR :" AND CO.CODE EQ ":COMP


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*TEXT = "SELECTED":SELECTED ; CALL REM
    ENQ.LP  = '' ; OPER.VAL = ''
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
