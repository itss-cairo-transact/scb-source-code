*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VVR.LC.OPERATION.T
* A ROUTINE TO CHECK IF TF.LC.OPERATION IS NOT 'O' THEN DISPLAY ERROR

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.LETTER.OF.CREDIT
$INCLUDE TEMENOS.BP I_F.SCB.LC.DOCTERMS

IF COMI # 'T' THEN ETEXT = 'The.Operation.Must.Be.T'

RETURN
END
