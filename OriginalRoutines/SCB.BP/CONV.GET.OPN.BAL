*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.GET.OPN.BAL

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_ENQUIRY.COMMON
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE T24.BP I_F.STMT.ENTRY
*--------------------------------------
*    ACCOUNT.NO = O.DATA
* TEXT = O.DATA ; CALL REM
    ACCOUNT.NO = FIELD(O.DATA,"-",1)
    START.DATE = FIELD(O.DATA,"-",2)
    END.DATE   = FIELD(O.DATA,"-",2)
    CALL EB.ACCT.ENTRY.LIST(ACCOUNT.NO<1>,START.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)

    O.DATA = OPENING.BAL
*TEXT = O.DATA ; CALL REM
*--------------------------------------
    RETURN
END
