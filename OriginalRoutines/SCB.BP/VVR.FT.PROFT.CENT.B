*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*****CREATED BY MAI SAAD 11/02/2018*******
    SUBROUTINE  VVR.FT.PROFT.CENT.B

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.FUNDS.TRANSFER
    $INCLUDE T24.BP I_USER.ENV.COMMON
    COMP = C$ID.COMPANY
    COM.CODE      = COMP[8,2]
    IF COM.CODE[1,1]  EQ '0' THEN
        COM.CODE  = COMP[9,1]
    END ELSE
        COM.CODE  = COMP[8,2]
    END
    CREDIT.ACC = COMI
    DEBIT.ACC =  R.NEW(FT.DEBIT.ACCT.NO)
    IF V$FUNCTION = 'I' THEN
        IF (LEN(DEBIT.ACC)) < 16 OR (LEN(CREDIT.ACC)) < 16 THEN
            IF (LEN(DEBIT.ACC)) < 16 THEN
                PL.CAT = DEBIT.ACC[3,5]
                IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                    IF (COMP EQ 'EG0010099') THEN
                        IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                            TEXT ='��� ����� ��� �������' C;CALL REM
                        END
                    END ELSE
** IF R.NEW(FT.PROFIT.CENTRE.DEPT) # 7070 THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
** END
                    END
                END ELSE
                    IF (COMP EQ 'EG0010099') THEN
                        IF R.NEW(FT.PROFIT.CENTRE.DEPT) = '' THEN
                            R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                        END
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                    END
                END
            END
            IF (LEN(CREDIT.ACC)) < 16 THEN
                PL.CAT.C = CREDIT.ACC[3,5]
                IF (PL.CAT.C GE 52880 AND PL.CAT.C LE 52900) OR (PL.CAT.C GE 56001 AND PL.CAT.C LE 56019) OR (PL.CAT.C GE 60000 AND PL.CAT.C LE 69998 ) OR PL.CAT.C EQ 54036 THEN
                    IF (COMP EQ 'EG0010099') THEN
                        IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                            TEXT ='��� ����� ��� �������' C;CALL REM
                        END
                    END ELSE
**IF R.NEW(FT.PROFIT.CENTRE.DEPT) # 7070 THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
** END
                    END
                END ELSE
                    IF (COMP EQ 'EG0010099') THEN
                        IF R.NEW(FT.PROFIT.CENTRE.DEPT) = '' THEN
                            R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                        END
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                    END
                END
            END
** END
        END ELSE
            IF NOT ( NUM(CREDIT.ACC[1,3])) AND ( DEBIT.ACC[1,2] EQ '99') THEN
                R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
            END ELSE
                IF NOT (NUM(DEBIT.ACC[1,3])) AND (CREDIT.ACC[1,2] EQ '99') THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END
            END
        END
    END
    RETURN
END
