    SUBROUTINE BLD.RISK.GROUP.F(ENQ.DATA)
$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE TEMENOS.BP I_F.SCB.RISK.MAST
$INCLUDE T24.BP I_USER.ENV.COMMON
    COMP = ID.COMPANY
    CUS.ID.FLAG = 0

    FN.RS = "F.SCB.RISK.MAST"
    F.RS = ''
    R.RS=''
    Y.RS.ID=''
    Y.RS.ERR=''
    K = 0
    YTEXT = "Enter the Group No. : "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.GROUP.NO = COMI
    YTEXT = "Enter the Year  : "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.YY = COMI
    *WS.YY = 2010

    CALL OPF(FN.RS,F.RS)
    SEL.CMD ="SELECT ":FN.RS:" WITH GROUP.CODE EQ ":WS.GROUP.NO:" AND SYSTEM.DATE LIKE ":WS.YY:"... BY CUSTOMER.ID BY SYSTEM.DATE"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN
        FOR I = 1 TO NOREC
            WS.CUS.ID = FIELD(SELLIST<I>,"*",1)
            WS.DAT.ID = FIELD(SELLIST<I>,"*",2)

            IF CUS.ID.FLAG EQ 0 THEN
                CUS.ID.FLAG =  WS.CUS.ID
            END
            IF CUS.ID.FLAG NE WS.CUS.ID THEN
                K ++
                Y = (I - 1)
                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = SELLIST<Y>
            END
            CUS.ID.FLAG =  WS.CUS.ID
*K ++
*ENQ.DATA<2,K> = "@ID"
*ENQ.DATA<3,K> = "EQ"
*ENQ.DATA<4,K> = SELLIST<I>
*IF K = 20 THEN I = 1
        NEXT I
        K ++
        ENQ.DATA<2,K> = "@ID"
        ENQ.DATA<3,K> = "EQ"
        ENQ.DATA<4,K> = SELLIST<I>

    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
