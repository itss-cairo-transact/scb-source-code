*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 07/04/2003***********

SUBROUTINE VVR.LC.OPERATION.D
* A ROUTINE TO CHECK IF TF.LC.OPERATION IS NOT 'D' THEN DISPLAY ERROR

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.LETTER.OF.CREDIT


IF COMI # 'D' THEN ETEXT = 'The.Operation.Must.Be.D'




RETURN
END
