*-----------------------------------------------------------------------------
* <Rating>248</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CUS.MM
*a routine to empty fields credit.customer & debit.amount & credit.amount with every change of the
*debit.customer
*routine defaults field ordering.cust with customer.name
    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.FUNDS.TRANSFER
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE T24.BP I_F.USER
    $INCLUDE T24.BP I_F.DEPT.ACCT.OFFICER
    $INCLUDE TEMENOS.BP I_CU.LOCAL.REFS
    $INCLUDE TEMENOS.BP I_FT.LOCAL.REFS
    $INCLUDE T24.BP I_F.POSTING.RESTRICT

    IF V$FUNCTION = "I" THEN
        IF COMI THEN
            CUU = COMI
            CALL DBR('CUSTOMER':@FM: EB.CUS.POSTING.RESTRICT,CUU,POST)
            CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
            IF POST # "" THEN
         **       ETEXT = DESC ;CALL STORE.END.ERROR
            END ELSE


                IF LEN(COMI) = 7 THEN
                    IF COMI[2,1] = 6 THEN

                        ETEXT = '���� �� ���� ���� �����' ; CALL STORE.END.ERROR

                    END
                END ELSE
                    IF COMI[3,1] = 6 THEN
                        ETEXT = '���� �� ���� ���� �����' ; CALL STORE.END.ERROR
                    END
                END

                IF COMI # R.NEW(FT.DEBIT.CUSTOMER) THEN
                    R.NEW(FT.DEBIT.ACCT.NO) = ""
                    R.NEW(FT.DEBIT.AMOUNT) = ""
                    R.NEW(FT.CREDIT.AMOUNT) = ""
                END
                CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,COMI,CU.NAME)
                R.NEW(FT.ORDERING.CUST) = CU.NAME<1>
                R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> = R.USER< EB.USE.DEPARTMENT.CODE>
* TEXT = "BRN = ":R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> ; CALL REM

                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
