*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CHK.ATM.APP

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_ENQUIRY.COMMON
    $INCLUDE T24.BP I_F.LD.LOANS.AND.DEPOSITS
    $INCLUDE TEMENOS.BP I_F.SCB.ATM.APP
*----------------------------------------------
    CUS.NO = O.DATA

    FN.ATM = "F.SCB.ATM.APP"  ; F.ATM = ""
    CALL OPF(FN.ATM,F.ATM)

    ER.MSG = ""
    T.SEL  = "SELECT F.SCB.ATM.APP WITH CUSTOMER EQ " : CUS.NO
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        O.DATA = "YES"
    END ELSE
        O.DATA = "NO"
    END
*----------------------------------------------
    RETURN
END
