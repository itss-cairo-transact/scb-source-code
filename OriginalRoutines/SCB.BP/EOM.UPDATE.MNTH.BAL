*-----------------------------------------------------------------------------
* <Rating>538</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOM.UPDATE.MNTH.BAL

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.COMPANY
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE TEMENOS.BP I_F.SCB.ACCOUNT.OPEN.BAL

*TEXT = TODAY ; CALL REM
*   T.SEL = "SELECT FBNK.ACCOUNT WITH @ID EQ 9020002510650105"
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
    T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER NE '' AND OPEN.ACTUAL.BAL NE ''"
    T.SEL := ' EVAL"@ID': ":'|':" :'OPEN.ACTUAL.BAL"'

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FVAR.AC.BAL = '' ; FN.AC.BAL = "F.SCB.ACCOUNT.OPEN.BAL"
        READ.FOUND = ''
        **OPEN FN.AC.BAL TO FVAR.AC.BAL THEN
            CALL OPF(FN.AC.BAL,FVAR.AC.BAL)
            FOR AC.LP = 1 TO SELECTED
                R.REC = ''
                ACCT.ID = FIELD(KEY.LIST<AC.LP>,"|",1)
                BAL = FIELD(KEY.LIST<AC.LP>,"|",2)
                CALL DBR("ACCOUNT":@FM:AC.OPEN.ACTUAL.BAL, ACCT.ID, BAL)
                IF BAL THEN
                    IF TODAY[5,2] # 12 THEN
                        AC.BAL.ID = ACCT.ID:".":TODAY[1,4]
**                        READ R.REC FROM FVAR.AC.BAL, AC.BAL.ID ELSE READ.FOUND = "NO"
                        CALL F.READ(FN.AC.BAL,AC.BAL.ID,R.REC,FVAR.AC.BAL,ERR1)
                        IF ERR1 THEN
                           READ.FOUND = "NO"
                        END
                        MN = TODAY[5,2]*1
                        MONTH = MN + 1
                    END ELSE
                        YYY = TODAY[1,4] + 1
                        AC.BAL.ID = ACCT.ID:".":YYY
**                        READ R.REC FROM FVAR.AC.BAL, AC.BAL.ID ELSE READ.FOUND = "NO" 
**                        MONTH = 1
                        CALL F.READ(FN.AC.BAL,AC.BAL.ID,R.REC,FVAR.AC.BAL,ERR1)
                        IF ERR1 THEN
                            READ.FOUND = "NO"
                        END
                            MONTH = 1
                    END
                    IF READ.FOUND = "NO" THEN
                        R.REC<MONTH> = BAL
**                        WRITE R.REC ON FVAR.AC.BAL , AC.BAL.ID ON ERROR
**                            TEXT = '1 CAN NOT WRITE RECORD ':AC.BAL.ID:' TO APPLICATION ':FN.AC.BAL
**                        END
                        CALL F.WRITE(FVAR.AC.BAL , AC.BAL.ID , R.REC)
                        CALL JOURNAL.UPDATE('')
                    END ELSE
                        R.REC<MONTH> = BAL
**                        WRITE R.REC ON FVAR.AC.BAL , AC.BAL.ID ON ERROR
**                            TEXT = '2 CAN NOT WRITE RECORD ':AC.BAL.ID:' TO APPLICATION ':FN.AC.BAL
**                        END
                        CALL F.WRITE(FVAR.AC.BAL , AC.BAL.ID , R.REC)
                        CALL JOURNAL.UPDATE('')
                    END
                END
            NEXT AC.LP
        *END  ELSE
        *    OPEN.FOUND = "NO"
        *END
    END

    RETURN
END
