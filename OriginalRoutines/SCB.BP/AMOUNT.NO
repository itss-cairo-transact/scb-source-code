*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE AMOUNT.NO(ARG)

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.LD.LOANS.AND.DEPOSITS
$INCLUDE T24.BP I_F.CURRENCY

    CUR = R.NEW(LD.CURRENCY)
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
*AMOUNT = (ARG)
    PR = ARG
   INTT = R.NEW(LD.TOT.INTEREST.AMT)
 *   TOT = PR + INTT
     TOT = R.NEW(LD.REIMBURSE.AMOUNT)+INTT
    IN.AMOUNT = TOT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
   * ARG = OUT.AMOUNT:" ":CURR
     ARG = TOT
    RETURN
END
