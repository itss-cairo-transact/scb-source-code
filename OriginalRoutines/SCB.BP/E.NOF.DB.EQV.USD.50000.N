****NESSREEN AHMED 25/03/2013******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.DB.EQV.USD.50000.N(Y.RET.DATA)


    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.STMT.ENTRY
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE T24.BP I_F.BATCH
    $INCLUDE T24.BP I_F.STMT.ENTRY
    $INCLUDE T24.BP I_F.STMT.ACCT.DR
    $INCLUDE T24.BP I_F.DATES
    $INCLUDE T24.BP I_F.TELLER
    $INCLUDE TEMENOS.BP I_TT.LOCAL.REFS
    $INCLUDE TEMENOS.BP I_F.SCB.CURRENCY.DAILY
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    TOT.TT = 0 ; TOT.AMT = 0
    TOT.AMT.SUN = 0 ; TOT.AMT.MON = 0 ; TOT.AMT.TUE = 0 ; TOT.AMT.WED = 0 ; TOT.AMT.THU = 0
    TOT.ALL.DAY = 0

    YTEXT = "����� ����� ����� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
    YTEXT = "���� ����� ����� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND CONTRACT.GRP EQ '' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CUSTOMER.1 BY AUTH.DATE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    CUST<1>         = R.TT<TT.TE.CUSTOMER.1>
    CURR<1>         = R.TT<TT.TE.CURRENCY.1>
    AMT.FCY<1>      = R.TT<TT.TE.AMOUNT.FCY.1>
    AMT.LCY<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1>     = R.TT<TT.TE.AUTH.DATE>
    TT.DATE = AUTH.DAT<1>
*DAY.NUM = ICONV(TT.DATE,"DW")
*DAY.NUM2= OCONV(DAY.NUM,"DW")
    KEY.ID<1> = "USD":"-" :"EGP":"-":TT.DATE
    CALL F.READ(FN.CURR.DALY,KEY.ID<1>,R.CURR.DALY,F.CURR.DALY,ERR2)
    DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
    CURR.MARK = R.CURR.DALY<SCCU.CURR.MARKET>
    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
        SELL.RATE<1> = R.CURR.DALY<SCCU.SELL.RATE ,NN>
    END
    IF CURR<1> = "USD" THEN
        BEGIN CASE
        CASE DAY.NUM2 = 7
            TOT.AMT.SUN = TOT.AMT.SUN + AMT.FCY<1>
        CASE DAY.NUM2 = 1
            TOT.AMT.MON = TOT.AMT.MON + AMT.FCY<1>
        CASE DAY.NUM2 = 2
            TOT.AMT.TUE = TOT.AMT.TUE + AMT.FCY<1>
        CASE DAY.NUM2 = 3
            TOT.AMT.WED = TOT.AMT.WED + AMT.FCY<1>
        CASE DAY.NUM2 = 4
            TOT.AMT.THU = TOT.AMT.THU + AMT.FCY<1>
        END CASE
    END ELSE
****OTHER FCY CURR*******************************
        BEGIN CASE
        CASE DAY.NUM2 = 7
            AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
            TOT.AMT.SUN = TOT.AMT.SUN + AMT.EQV.USD
        CASE DAY.NUM2 = 1
            AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
            TOT.AMT.MON = TOT.AMT.MON + AMT.EQV.USD
        CASE DAY.NUM2 = 2
            AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
            TOT.AMT.TUE = TOT.AMT.TUE + AMT.EQV.USD
        CASE DAY.NUM2 = 3
            AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
            TOT.AMT.WED = TOT.AMT.WED + AMT.EQV.USD
        CASE DAY.NUM2 = 4
            AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
            TOT.AMT.THU = TOT.AMT.THU + AMT.EQV.USD
        END CASE
    END
**********END OF FIRST RECORD**********************************
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        TT.DATE = AUTH.DAT<I>
**       DAY.NUM = ICONV(TT.DATE,"DW")
**       DAY.NUM2= OCONV(DAY.NUM,"DW")
        KEY.ID<I> = "USD":"-" :"EGP":"-":TT.DATE
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
        LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
            SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
        END
        IF CUST<I> # CUST<I-1> THEN
            TOT.ALL.DAYS = TOT.AMT.SUN + TOT.AMT.MON + TOT.AMT.TUE + TOT.AMT.WED + TOT.AMT.THU
            IF TOT.ALL.DAYS > 50000 THEN
                Y.RET.DATA<-1> = CUST<I-1> :"*": CURR<I-1>:"*":TOT.AMT.SUN:"*":TOT.AMT.MON:"*":TOT.AMT.TUE:"*":TOT.AMT.WED:"*":TOT.AMT.THU:"*":TOT.ALL.DAYS:"*":EN.DATE
                TOT.AMT.SUN = 0 ; TOT.AMT.MON = 0 ; TOT.AMT.TUE = 0 ; TOT.AMT.WED = 0 ; TOT.AMT.THU = 0 ; TOT.ALL.DAYS = 0
            END ELSE
                TOT.AMT.SUN = 0 ; TOT.AMT.MON = 0 ; TOT.AMT.TUE = 0 ; TOT.AMT.WED = 0 ; TOT.AMT.THU = 0 ; TOT.ALL.DAYS = 0
            END
            IF CURR<I> = "USD" THEN
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.FCY<I>
                CASE DAY.NUM2 = 1
                    TOT.AMT.MON = TOT.AMT.MON + AMT.FCY<I>
                CASE DAY.NUM2 = 2
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.FCY<I>
                CASE DAY.NUM2 = 3
                    TOT.AMT.WED = TOT.AMT.WED + AMT.FCY<I>
                CASE DAY.NUM2 = 4
                    TOT.AMT.THU = TOT.AMT.THU + AMT.FCY<I>
                END CASE
            END ELSE
                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                END
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.EQV.USD
                CASE DAY.NUM2 = 1
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.MON = TOT.AMT.MON + AMT.EQV.USD
                CASE DAY.NUM2 = 2
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.EQV.USD
                CASE DAY.NUM2 = 3
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.WED = TOT.AMT.WED + AMT.EQV.USD
                CASE DAY.NUM2 = 4
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.THU = TOT.AMT.THU + AMT.EQV.USD
                END CASE
            END     ;** END OF OTHER FCY**
*****SAME CUSTOMER*************
        END ELSE
            IF CURR<I> = "USD" THEN
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.FCY<I>
                CASE DAY.NUM2 = 1
                    TOT.AMT.MON = TOT.AMT.MON + AMT.FCY<I>
                CASE DAY.NUM2 = 2
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.FCY<I>
                CASE DAY.NUM2 = 3
                    TOT.AMT.WED = TOT.AMT.WED + AMT.FCY<I>
                CASE DAY.NUM2 = 4
                    TOT.AMT.THU = TOT.AMT.THU + AMT.FCY<I>
                END CASE
            END ELSE
****OTHER FCY**********
                    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                        SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                    END
                BEGIN CASE
                CASE DAY.NUM2 = 7
                     AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                     TOT.AMT.SUN = TOT.AMT.SUN + AMT.EQV.USD
                CASE DAY.NUM2 = 1
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.MON = TOT.AMT.MON + AMT.EQV.USD
                CASE DAY.NUM2 = 2
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.EQV.USD
                CASE DAY.NUM2 = 3
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.WED = TOT.AMT.WED + AMT.EQV.USD
                CASE DAY.NUM2 = 4
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.THU = TOT.AMT.THU + AMT.EQV.USD
                END CASE
            END     ;**END OF OTHER FCY**
*****END OF SAME CUSTOMER******
        END         ;**END OF SAME CUSTOMER**
        IF I = SELECTED.N THEN
            TEXT = 'LAST RECORD' ; CALL REM
            TOT.ALL.DAYS = TOT.AMT.SUN + TOT.AMT.MON + TOT.AMT.TUE + TOT.AMT.WED + TOT.AMT.THU
            IF TOT.ALL.DAYS > 50000 THEN
                Y.RET.DATA<-1> = CUST<I> :"*": CURR<I>:"*":TOT.AMT.SUN:"*":TOT.AMT.MON:"*":TOT.AMT.TUE:"*":TOT.AMT.WED:"*":TOT.AMT.THU:"*":TOT.ALL.DAYS:"*":EN.DATE
           END
        END
    NEXT I
    RETURN
END
