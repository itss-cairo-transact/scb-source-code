*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BR.CHECK.MOD

* PREVENT USER FROM INPUTING A NEW RECORD
* PREVENT USER FROM MODIFYING A RECORD

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.BILL.REGISTER

    ETEXT = ''
    E     = ''

    IF V$FUNCTION = 'I' THEN

        FN.BR = "FBNK.BILL.REGISTER"
        F.BR  = ""
        ETEXT = ""

        CALL OPF(FN.BR,F.BR)
        CALL F.READ(FN.BR,ID.NEW,R.BR,F.BR,ETEXT)
        IF (ETEXT) THEN
            E = "�� ���� �������" ;CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
    RETURN
END
