*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CD.TODAY.MM(ENQ)

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_ENQUIRY.COMMON
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE T24.BP I_F.COMPANY
    $INCLUDE T24.BP I_F.LD.LOANS.AND.DEPOSITS
    $INCLUDE T24.BP I_F.USER
    $INCLUDE T24.BP I_USER.ENV.COMMON
    $INCLUDE T24.BP  I_F.DATES
*-------------------------------------------------
    COMP     = ID.COMPANY
    KEY.LIST =""
    SELECTED =""
    ER.MSG   =""
    B        =""
    CUST     =""
    DAT.ID = "EG0010001"

    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.LWD.DATE)

    DATE.TO = '...':WS.LWD.DATE[3,6]:'...'

    DATE.FROM = TODAY[3,6]:'0001'
    CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>

    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ( CATEGORY GE 21019 AND CATEGORY LE 21042 )  AND ( VERSION.NAME EQ ',SCB.CD.AMEND1' OR VERSION.NAME EQ ',SCB.CD.NAME1' ) AND DATE.TIME LIKE ": DATE.TO :" AND CO.CODE EQ " : COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN
        FOR KK = 1 TO SELECTED
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
*---------------------------------------------------------------
    RETURN
END
