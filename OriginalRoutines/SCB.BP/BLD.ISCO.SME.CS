*-----------------------------------------------------------------------------
* <Rating>-45</Rating>
*-----------------------------------------------------------------------------
*PROGRAM BLD.ISCO.SME
    SUBROUTINE BLD.ISCO.SME.CS(ENQ)

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.USER
$INCLUDE T24.BP I_F.CUSTOMER
$INCLUDE TEMENOS.BP I_CU.LOCAL.REFS
$INCLUDE TEMENOS.BP I_F.SCB.ISCO.SME.CS
$INCLUDE TEMENOS.BP I_F.SCB.ISCO.SME.CF


    EOF  = ''
    R.CF = ''
    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
*============================ OPEN TABLES =========================

    FN.CF  = 'F.SCB.ISCO.SME.CF' ; F.CF = '' ; R.CF = ''
    CALL OPF(FN.CF,F.CF)
    FN.CS  = 'F.SCB.ISCO.SME.CS' ; F.CS = '' ; R.CS = ''
    CALL OPF(FN.CS,F.CS)


    T.SEL  = "SELECT F.SCB.ISCO.SME.CS WITH @ID LIKE ...":YEAR:NEW.MONTH:"... AND BAC3 EQ 'Y' BY CF.ACC.NO"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
 TEXT = SELECTED; CALL REM
*======================== BB.DATA ===========================
    CUST.NO = ''
    PRE.CUST.NO = ''
    TOT.APP.AMT = 0
    TOT.USE.AMT = 0
    K = 1

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"

    END
RETURN

END
