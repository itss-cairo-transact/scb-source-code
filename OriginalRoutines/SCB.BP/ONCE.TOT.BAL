*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ONCE.TOT.BAL

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_ENQUIRY.COMMON
    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM
    $INCLUDE T24.BP I_F.DATES
    $INCLUDE T24.BP I_USER.ENV.COMMON
    $INCLUDE TEMENOS.BP I_F.SCB.CUS.POS.LW


    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    COMP = ID.COMPANY
    DAT.ID = COMP
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,TD2)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)


    AMT.POS1 = 0 ; KK1 = 0 ; KK2 = 0
    FN.POS = 'F.SCB.CUS.POS.LW'
    F.POS  = ''
    CALL OPF(FN.POS,F.POS)
*=========== CHECK USD AMOUNT =========================
    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*======================================================
    T.SEL = "SELECT F.SCB.CUS.POS.LW WITH DEAL.CCY NE 'EGP'"
    T.SEL := " AND SYS.DATE EQ ":TD2:" AND LCY.AMOUNT NE 0"
    T.SEL := " AND ((CATEGORY IN (5001 5020) AND LCY.AMOUNT LT 0 )"
    T.SEL := " OR (CATEGORY EQ 21076 AND MATURITY.DATE GT ":TD1:" AND VALUE.DATE LE ":TD1:"))"
    T.SEL := " BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*=======================================================
    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
            AMT.POS = 0 ; SCB.ORG.CUR.RATE = 0
            SCB.ORG.CUR = '' ;
            CALL F.READ(FN.POS,KEY.LIST<KK>,R.POS,F.POS,ERR)
            CUS.ID = R.POS<CUPOS.CUSTOMER>
            IF KK = 1 THEN CCC.NO = CUS.ID ; ST.NO = KK
            AMT.POS    = R.POS<CUPOS.DEAL.AMOUNT>
            SCB.ORG.CUR = R.POS<CUPOS.DEAL.CCY>
            IF AMT.POS LT 0 THEN AMT.POS = AMT.POS * -1
            LOCATE SCB.ORG.CUR IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            SCB.ORG.CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
            LOCAL.FORIGN.AMT = AMT.POS * SCB.ORG.CUR.RATE
            AMT.POS1 = AMT.POS1 + LOCAL.FORIGN.AMT
        NEXT KK

    END


    O.DATA = AMT.POS1 / 1000 * 10 / 100
    O.DATA = DROUND(O.DATA,"0")
    RETURN
