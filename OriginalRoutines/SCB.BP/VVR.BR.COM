*-----------------------------------------------------------------------------
* <Rating>317</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.COM

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.BILL.REGISTER
    $INCLUDE T24.BP I_F.USER
    $INCLUDE T24.BP I_F.DEPT.ACCT.OFFICER
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE TEMENOS.BP I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
    $INCLUDE T24.BP I_USER.ENV.COMMON
    $INCLUDE TEMENOS.BP I_F.SCB.BANK.BRANCH
    $INCLUDE TEMENOS.BP I_F.SCB.BR.CUS
    $INCLUDE T24.BP I_F.FT.COMMISSION.TYPE

*  IF MESSAGE EQ '' THEN
    CURR      = R.NEW(EB.BILL.REG.CURRENCY)
    AMTT      = R.NEW(EB.BILL.REG.AMOUNT)
    COM.ID    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>

    IF COMI NE '' THEN

        FN.FT.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
        CALL OPF(FN.FT.COM,F.FT.COM)

        CALL F.READ(FN.FT.COM,COMI,R.FT.COM,F.FT.COM,E1)

        LOCATE CURR IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
        PL.COM  = R.FT.COM<FT4.PERCENTAGE,J>
    END
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 7 THEN
        COMM.AMT  = (PL.COM * AMTT)/100
        CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
**---------------------------------
        MIN = "10"
        MAX = "100"

        IF COMM.AMT LT MIN THEN

            COMM.AMT = MIN
        END ELSE
            IF COMM.AMT GT MAX THEN

                COMM.AMT = MAX
            END
        END
**-------------------------------------------
**R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR:COMM.AMT
***-------------20100805-----------------------------------------------------**

        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>
        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN

            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = "BILLCOLL"
*           R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = "EGP":COMM.AMT
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> =  CURR:COMM.AMT

        END ELSE
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""

        END

        FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
        CALL OPF(FN.BR.COM,F.BR.COM)
        BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
        CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)
        IF NOT(E11) THEN
            COM.AMT   = R.BR.COM<BR.CUS.COMM.AMT>
            COM.PR    = R.BR.COM<BR.CUS.COMM.PREC>
            CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>

            IF CHRG.AMT EQ 'YES' OR COM.AMT EQ 0 THEN

                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
            END

            IF CHRG.AMT EQ 'YES' THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
                CALL REBUILD.SCREEN
            END

        END

***-------------20100805-----------------------------------------------------**
    END ELSE
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
    END
    RETURN
    CALL REBUILD.SCREEN
* END
END
