*-----------------------------------------------------------------------------
* <Rating>255</Rating>
*-----------------------------------------------------------------------------
*   PROGRAM ACH.SCCD.TXT
    SUBROUTINE ACH.SCCD.TXT

*NEW CREATED CREATED 17-11-2014
* NOHA HAMED

*-----------------------------------
    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE T24.BP I_F.CATEGORY
    $INCLUDE T24.BP I_F.DEPT.ACCT.OFFICER
    $INCLUDE TEMENOS.BP I_F.SCB.SCCD.INT
    $INCLUDE TEMENOS.BP I_F.SCB.BANK

    $INCLUDE JBC.h

    HH.DATA    = ''
    BB.DATA    = ''
    TT.DATE    = ''

    FN.CD = "F.SCB.SCCD.INT"; F.CD = ''; R.CD = ''
    CALL OPF(FN.CD,F.CD)

*************************ENTER DATE******************
    CO.CODE = ID.COMPANY
    IF CO.CODE EQ 'EG0010099' THEN
        YTEXT = "Enter Date.(YYYYMMDD) :"
        CALL TXTINP(YTEXT, 2, 22, "8", "D")
        T.DATE  = COMI
*****************************************************


        FILE.NAME = "ACH.CSV"
        OPENSEQ "ACH" , FILE.NAME TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"ACH":' ':FILE.NAME
            HUSH OFF
        END
        OPENSEQ "ACH" , FILE.NAME TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE CREATED IN ACH/'
            END
            ELSE
                STOP 'Cannot create File IN ACH/'
            END
        END
*******************
        T.SEL  = "SELECT F.SCB.SCCD.INT WITH INT.DATE EQ ":T.DATE:" AND BANK.CODE NE '' AND BANK.ACCT.NO NE '' AND NET.AMOUNT NE '' BY BANK.CODE BY @ID"
*       T.SEL  = "SELECT F.SCB.SCCD.INT WITH BANK.CODE NE '' AND BANK.ACCT.NO NE '' AND NET.AMOUNT NE '' BY BANK.CODE BY @ID"


        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        IF SELECTED THEN
            BB.DATA =  "Instruction ID":",Creditor Name":",Creditor Account Number":",Creditor Account Type":",Creditor Bank":",Creditor Bank Branch":",Debtor Name":",Debtor Account Number":",Debtor Account Type":",Transaction Amount":",Transaction Purpose":",Remittance Information"
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            FOR I = 1 TO SELECTED


                CALL F.READ( FN.CD,KEY.LIST<I>, R.CD, F.CD, CD.E1)
                BB.DATA               = KEY.LIST<I>:','
                CUS.NO                = R.CD<SCCD.CUS.NO>
*               CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUS.NO,CUS.NAME)
                CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,CUS.NO,CUS.NAME)
                BB.DATA              := CUS.NAME:','
                CRD.ACCT.NO           = R.CD<SCCD.BANK.ACCT.NO>
                BB.DATA              := CRD.ACCT.NO:','
                BB.DATA              := 'CACC':','
                BNK.CODE              = R.CD<SCCD.BANK.CODE>

                BEGIN CASE
                CASE BNK.CODE = '0005'
                    BNK.ABBR  = 'BOA'
                CASE BNK.CODE = '0029'
                    BNK.ABBR  = 'EGB'
                CASE BNK.CODE = '0031'
                    BNK.ABBR  = 'UB'
                CASE BNK.CODE = '0036'
                    BNK.ABBR  = 'CAE'
                CASE BNK.CODE = '0057'
                    BNK.ABBR  = 'AAIB'

                END CASE

                BB.DATA              := BNK.ABBR:','

*****************SCB CUSTOMER BRANCH CODE ******************************
                BRN.CODE             =  R.CD<SCCD.BRANCH.CODE>
* CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRN.CODE,BRN.NAME)
* YYBRN  = FIELD(BRN.NAME,'.',2)
************************************************************************

                BB.DATA              := ','
                BB.DATA              := CUS.NAME:','
                ACCT.NO               = R.CD<SCCD.INT.LIQ.ACCT>
                BB.DATA              := ACCT.NO:','
                BB.DATA              := 'CACC':','
                AMT                   = R.CD<SCCD.NET.AMOUNT>
                BB.DATA              := AMT:','
                BB.DATA              := 'SCCD,'
                BB.DATA              := BRN.CODE


                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

            NEXT I
        END
        CALL EOD.SCCD.INT.ACH.CBE
    END ELSE
        TEXT = "��� ����� �����" ; CALL REM
    END
END
