*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/05/12  ***
********************************************
*    SUBROUTINE SBM.HNDOF.MOD
    PROGRAM SBM.HNDOF.MOD

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.OFS.SOURCE
    $INCLUDE T24.BP I_F.USER
    $INCLUDE TEMENOS.BP I_OFS.SOURCE.LOCAL.REFS


** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

    WS.XWAIT = ''
    WS.XCON  = ''
****

*---------------------------------------------------------------------------------------------------------
    EXECUTE "cp ../bnk.data/ac/FBNK.AC.S001 ../bnk.data/ac/FBNK.AC.S001.BFR.UPD"

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"DEL.NO.ACTIVE.CUST")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.HNDOF.CHK")

    EXECUTE "cp ../bnk.data/ac/FBNK.AC.S001 ../bnk.data/ac/FBNK.AC.S001.AFT.UPD"

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SCB.POST.LIST.FILE")

    EXECUTE "CLEAR-FILE REP"
    EXECUTE "CLEAR-FILE STMT.DIFF"

*    EXECUTE "COPY FROM REP TO &SAVEDLISTS& ALL OVERWRITING"

    RETURN
*-----------------------------------------------
