*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.TELLER.SHARM.N(ENQ)

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.COMPANY
$INCLUDE T24.BP I_ENQUIRY.COMMON
$INCLUDE T24.BP I_F.TELLER
$INCLUDE T24.BP I_USER.ENV.COMMON

    FN.TT ='FBNK.TELLER$HIS' ;R.TT = '';F.TT =''
    CALL OPF(FN.TT,F.TT)

    COMP = C$ID.COMPANY
    LOCATE "TELLER.ID.1" IN ENQ<2,1> SETTING TT.POS THEN
        TT.ID = ENQ<4,TT.POS>
    END
    LOCATE "AUTH.DATE" IN ENQ<2,1> SETTING DD.POS THEN
        DD.ID = ENQ<4,DD.POS>
    END

    T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN ( 23 67 58 24 68 ) AND CO.CODE EQ ":COMP :" AND TELLER.ID.1 EQ ":TT.ID :" AND AUTH.DATE GE ":DD.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        CALL F.READ( FN.TT,KEY.LIST<I>, R.TT, F.TT,ETEXT)
        D.T    = R.TT<TT.TE.DATE.TIME>[1,6]
        AUTH.D = R.TT<TT.TE.AUTH.DATE>[3,6]

        IF D.T NE AUTH.D THEN
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        END ELSE
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = "DUM"
        END

    NEXT I
    RETURN
END
