***** 18/9/2002 RANIA SCB *****
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LD.LOANS.PEP
* A Subroutine to make sure that the Loan ID is an existing & from Loans Contract
* Also to default VALUE.DATE by Today's date

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.LD.LOANS.AND.DEPOSITS

IF V$FUNCTION = 'I' THEN

  MYID = '' ; CATEGORY = ''
  CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYID)
  IF ETEXT THEN E = 'Not An Existing Loan Contract' ;CALL ERR ;MESSAGE = 'REPEAT'
   ELSE IF CATEGORY # '21050' CATEGORY # '21051' OR CATEGORY # '21052' THEN E = 'This Is A Deposit Contract' ;CALL ERR ;MESSAGE = 'REPEAT'

END

********************************************************************************

R.NEW (LD.AMT.V.DATE) = TODAY


RETURN
END
