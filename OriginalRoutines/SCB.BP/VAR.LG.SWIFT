*-----------------------------------------------------------------------------
* <Rating>212</Rating>
*-----------------------------------------------------------------------------
**** RANIA *********
*TO CREATE SWIFT MESSAGE
    SUBROUTINE VAR.LG.SWIFT

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.LD.LOANS.AND.DEPOSITS
$INCLUDE TEMENOS.BP I_LD.LOCAL.REFS
$INCLUDE TEMENOS.BP I_F.SCB.LG.SWIFT
$INCLUDE TEMENOS.BP I_F.SCB.LG.AMEND.NUMBER
$INCLUDE TEMENOS.BP I_F.SCB.LG.769


    IF R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> NE '' THEN
*************** Updating Amend.No If Msg is 767 ************************
        IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> = '767' THEN
            FN.SCB.LG.AMEND.NUMBER='F.SCB.LG.AMEND.NUMBER';ID=ID.NEW;R.AMEND='';F.AMEND=''
            CALL F.READ( FN.SCB.LG.AMEND.NUMBER,ID, R.AMEND, F.AMEND, ETEXT)
            IF ETEXT THEN
                R.AMEND<SCB.AMEND.NO> = '1'
            END ELSE
                MYAMEND =  R.AMEND<SCB.AMEND.NO>
                MYAMEND = MYAMEND + 1
                R.AMEND<SCB.AMEND.NO>=MYAMEND
            END
            AMEND.NO= R.AMEND<SCB.AMEND.NO>
            CALL F.WRITE(FN.SCB.LG.AMEND.NUMBER,ID,R.AMEND)
*CALL JOURNAL.UPDATE(ID)
        END
********** Getting Amount.Increase & Amt.V.Date If Msg is 769 ********
*IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> = '769' THEN
* REDUCE.AMT=LINK.DATA<ABS(R.NEW(LD.AMOUNT.INCREASE))>
*TEXT=REDUCE.AMT ;CALL REM
*REDUCE.DATE=LINK.DATA<ABS(R.NEW(LD.AMT.V.DATE))>
*TEXT=REDUCE.AMT ;CALL REM
*END
        IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> = '769' THEN
            CALL DBR ('SCB.LG.769':@FM:SCB.AMOUNT.INCREASE,ID.NEW,REDUCE.AMT)
            *TEXT = REDUCE.AMT ;CALL REM
            CALL DBR ('SCB.LG.769':@FM:SCB.AMT.V.DATE,ID.NEW,REDUCE.DATE)
            *TEXT = REDUCE.DATE ;CALL REM
        END
*************** Calling SCB.LG.PROD.SWIFT To draw Message *************
        STORE.DEL.REFS = ""
        ER.MSG = ""
        CALL SCB.LG.PROD.SWIFT(MAT R.NEW,ID.NEW,AMEND.NO,REDUCE.AMT,REDUCE.DATE,STORE.DEL.REFS,ER.MSG)
        *TEXT = STORE.DEL.REFS ;CALL REM

**************** Updating SCB.LG.Swift With Delivery Ref **************
        DAT= R.NEW(LD.AGREEMENT.DATE)
        FN.SCB.LG.SWIFT='F.SCB.LG.SWIFT';ID=ID.NEW:".":DAT;R.SWIFT='';F.SWIFT=''
        CALL F.READ( FN.SCB.LG.SWIFT,ID, R.SWIFT, F.SWIFT, ETEXT)
        IF ETEXT THEN
            R.SWIFT<SCB.DELIVERY.REF> = STORE.DEL.REFS
        END ELSE
            MYDEV = R.SWIFT<SCB.DELIVERY.REF>
            MYCOUNT = DCOUNT(MYDEV,@VM)
            MYCOUNT = MYCOUNT +1
            R.SWIFT<SCB.DELIVERY.REF,MYCOUNT> = STORE.DEL.REFS
        END

************************************************************************
        CALL F.WRITE(FN.SCB.LG.SWIFT,ID,R.SWIFT)
*CALL JOURNAL.UPDATE(ID)

************************************************************************
    END
    RETURN
END
