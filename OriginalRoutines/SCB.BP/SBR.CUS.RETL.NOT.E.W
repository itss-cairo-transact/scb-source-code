*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.CUS.RETL.NOT.E.W

    $INCLUDE T24.BP I_COMMON
    $INCLUDE T24.BP I_EQUATE
    $INCLUDE T24.BP I_F.USER
    $INCLUDE T24.BP I_F.DATES
    $INCLUDE T24.BP I_F.CUSTOMER
    $INCLUDE T24.BP I_F.ACCOUNT
    $INCLUDE T24.BP I_F.COMPANY
    $INCLUDE T24.BP I_F.CATEGORY
    $INCLUDE T24.BP I_F.SECTOR
    $INCLUDE TEMENOS.BP I_AC.LOCAL.REFS
    $INCLUDE TEMENOS.BP I_CU.LOCAL.REFS
*----------------------------------------------
    FILE.NAME = "SBR.CUS.RETL.NOT.E.W.TXT"
    OPENSEQ "/home/signat/" , FILE.NAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat/":' ':FILE.NAME
        HUSH OFF
    END
    OPENSEQ "/home/signat/" , FILE.NAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CREATED IN /home/signat/'
        END ELSE
            STOP 'Cannot create File IN /home/signat/'
        END
    END

    EOF     = ''
    BB.DATA = "" ; FLG = 0
    FN.AC   = 'FBNK.ACCOUNT'  ; F.AC  = '' ; R.AC  = ''
    CALL OPF(FN.AC,F.AC)
    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.CU.AC = "FBNK.CUSTOMER.ACCOUNT" ; F.CU.AC = '' ; R.CU.AC = ''
    CALL OPF(FN.CU.AC, F.CU.AC)
    FN.CAT = "F.CATEGORY" ; F.CAT = "" ; R.CAT = ""
    CALL OPF(FN.CAT, F.CAT)

    BB.DATA  = "Customer.ID":","
    BB.DATA := "Customer En. Name":","
    BB.DATA := "Customer Ar. Name":","
    BB.DATA := "Customer Type":","
    BB.DATA := "Branch":","
*    BB.DATA := "Account.Number":","
*    BB.DATA := "Category.Name":","
*    BB.DATA := "Citizen.ID":","
*    BB.DATA := "Client.Type(New.Sector)":","
*    BB.DATA := "ENGLISH.Name":","
*    BB.DATA := "CIF.Key(Customer.ID)":","
*    BB.DATA := "ENGLISH.Address":","
    BB.DATA := "Telephone":","
*    BB.DATA := "GOVERNORATE":","
*    BB.DATA := "REGION":","
*    BB.DATA := "Postal Code":","
    BB.DATA := "EMAIL":","
    BB.DATA := "BIRTH DATE":","
    BB.DATA := "Have E-banking":","
    BB.DATA := "Have E-wallet":","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    T.SEL  = "SSELECT FBNK.CUSTOMER WITH NEW.SECTOR EQ 4650 AND SECTOR NE 5010 AND SECTOR NE 5020 "
    T.SEL := "AND POSTING.RESTRICT LT 89 BY @ID"
*    T.SEL := "AND (INTER.BNK.DATE EQ '' OR IS.EWALLET EQ '') AND POSTING.RESTRICT EQ '' BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU.AC,KEY.LIST<I>, R.CU.AC, F.CU.AC, E.CU.AC)
            LOOP
                REMOVE ACC.NO FROM R.CU.AC SETTING POS
            WHILE ACC.NO:POS
                BREAK
            REPEAT

            IF ACC.NO THEN
                CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,E.AC)
                CURR = R.AC<AC.CURRENCY>
                CATG = R.AC<AC.CATEGORY>
                CALL F.READ(FN.CAT,CATG, R.CAT, F.CAT, E.CAT)


                CUS.IDD  = KEY.LIST<I>
*   CUS.IDD  = FMT(CUS.IDD, 'R%8')

                BB.DATA  = "" ; FLG = 0
                BB.DATA  = CUS.IDD:","
                CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,E.CUS)
*  BB.DATA := ACC.NO :","

                NAME.1   = R.CUS<EB.CUS.NAME.1><1,1>
                NAME.1   = TRIM(NAME.1 , ',' , 'A')
                BB.DATA := NAME.1:","
                AR.NAME.1   = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:","
                AR.NAME.1   = TRIM(AR.NAME.1 , ',' , 'A')
                BB.DATA := AR.NAME.1:","
                CUS.TYP = ''
                CALL DBR ('SECTOR':@FM:EB.SEC.DESCRIPTION,R.CUS<EB.CUS.SECTOR>,CUS.TYP)
                BB.DATA := CUS.TYP:","
                BRANCH.NM = ''
                CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,R.CUS<EB.CUS.COMPANY.BOOK>,BRANCH.NM)
                BB.DATA := BRANCH.NM:","
*                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>:","
*                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>:","
*                BB.DATA := CUS.IDD:","
*                EN.ADD   = R.CUS<EB.CUS.ADDRESS><1,1>
*                BB.DATA := TRIM(EN.ADD,',','A'):","
                PHON     = ""
                PHON     = R.CUS<EB.CUS.SMS.1>
                IF NOT(PHON) THEN
                    TEL = R.CUS<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
                    FFF = DCOUNT(TEL,SM)
                    FOR HH = 1 TO FFF
                        XXX = LEN(TEL<1,1,HH>)
                        IF TEL<1,1,HH>[1,2] EQ "01" AND XXX EQ 11 THEN
                            PHON = TEL<1,1,HH>
                        END
                    NEXT HH
                END

                BB.DATA := PHON:","
*                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>:","
*                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.REGION>:",,"
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>:","
                DAT = R.CUS<EB.CUS.BIRTH.INCORP.DATE>
                DAT = FMT(DAT,"####/##/##")
                BB.DATA := DAT:","
                FLG.EB = '' ; FLG.WAL = ''
                IF R.CUS<EB.CUS.LOCAL.REF><1,CULR.INTER.BNK.DATE> NE '' THEN FLG.EB = "YES"
                IF R.CUS<EB.CUS.LOCAL.REF><1,CULR.IS.EWALLET> NE '' THEN FLG.WAL = "YES"
                BB.DATA := FLG.EB:","
                BB.DATA := FLG.WAL
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I
    END
    CLOSESEQ BB
*    STOP
END
