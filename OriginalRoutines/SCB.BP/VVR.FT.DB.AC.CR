*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.DB.AC.CR

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.FUNDS.TRANSFER
$INCLUDE T24.BP I_F.ACCOUNT
$INCLUDE T24.BP I_F.POSTING.RESTRICT

*CHECK IF CREDIT.ACCT # INTERNAL ACCOUNT

    IF MESSAGE NE 'VAL' THEN
        IF V$FUNCTION = 'I' THEN
            IF COMI THEN
                R.NEW(FT.CREDIT.ACCT.NO) = COMI
            END

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
