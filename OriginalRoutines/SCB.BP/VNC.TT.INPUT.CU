
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.TT.INPUT.CU
*1-TO MAKE FIELDS CUSTOMER.1 & CUSTOMER.2 IN INPUT MODE

$INCLUDE T24.BP I_COMMON
$INCLUDE T24.BP I_EQUATE
$INCLUDE T24.BP I_F.TELLER

 IF V$FUNCTION = "I" THEN
  T(TT.TE.CUSTOMER.1)<3> = ''
  T(TT.TE.CUSTOMER.2)<3> = ''
 END

RETURN
END
