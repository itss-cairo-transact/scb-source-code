*-----------------------------------------------------------------------------
* <Rating>-60</Rating>
*-----------------------------------------------------------------------------
      $PACKAGE AC.ModelBank

    SUBROUTINE E.MB.TXN.ENTRY.BUILD
*-----------------------------------------------------------------------------
* Subroutine type : Subroutine
* Attached to     : Enquiry MB.TXN.ENTRIES
* Attached as     : Conversion routine
* Purposes        : Returns the STMT, SPEC and CATEG Entry records for the Lead
*                   Company in which the enquiry is executed.
* @author         : prabha@temenos.com/madhusudananp@temenos.com

*-----------------------------------------------------------------------------
* @author         : rgayathri@temenos.com
**---------------------------------------------------------------------------------------
*Modification History
* 26/09/08 - BG_100020158
*            Problem in displaying categ entries.
*
* 27/07/11 - Defect 247831/ Task 251312
*            Cannot drilldown the enquiry TXN.ENTRY.MB if the Entry's account belongs
*            to another company. O.DATA value is suffixed with company mnemonic.
*
* 10/04/13 - DEFECT  631995 / TASK 646155
*			 Company mnemonic displayed is of Lead Company, respective company's mnemonics
*            have to be displayed. O.DATA value is now suffixed with only respective company
*            mnemonic instead of lead company.
*
*----------------------------------------------------------------------------------------

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.STMT.ENTRY
    $INSERT I_F.CATEG.ENTRY
    $INSERT I_F.CONSOL.ENT.TODAY
    $INSERT I_F.RE.CONSOL.SPEC.ENTRY
    $INSERT I_F.CATEGORY
    $INSERT I_F.RE.TXN.CODE
*
*--------------------------------------
*
    STMT.ID = O.DATA['*',1,1]
    CO.MNE =  O.DATA['*',2,1]
    IF CO.MNE THEN
        LEAD.MNE = ''
        CALL GET.COMPANY(CO.MNE,"","",LEAD.MNE)
    END
* BG_100020158 S
    TXN.ID = STMT.ID[2,99]

    BEGIN CASE
        CASE STMT.ID[1,1] = 'S'
            GOSUB READ.STMT.ENTRY

        CASE STMT.ID[1,1] = 'R'  OR STMT.ID[1,1] = 'E'
            GOSUB READ.SPEC.ENTRY

        CASE STMT.ID[1,1] = 'C'
            GOSUB READ.CATEG.ENTRY


    END CASE
* BG_100020158 E

    RETURN
*
*-------------------------------------------
*
READ.STMT.ENTRY:

    FN.STMT.ENTRY = 'F':LEAD.MNE:'.STMT.ENTRY'
    F.STMT.ENTRY = ''
    CALL OPF(FN.STMT.ENTRY,F.STMT.ENTRY)

    FN.STMT.ENTRY.DTL = 'F':LEAD.MNE:'.STMT.ENTRY.DETAIL'
    F.STMT.ENTRY.DTL  = ''
    CALL OPF(FN.STMT.ENTRY.DTL,F.STMT.ENTRY.DTL)

    CALL F.READ(FN.STMT.ENTRY,TXN.ID,R.STMT.ENTRY,F.STMT.ENTRY,R.STMT.ERR)
    IF R.STMT.ERR THEN
        CALL F.READ(FN.STMT.ENTRY.DTL,TXN.ID,R.STMT.ENTRY.DTL,F.STMT.ENTRY.DTL,R.STMT.ERR.DTL)
        R.RECORD = R.STMT.ENTRY.DTL
        O.DATA = 'T':TXN.ID:'*':CO.MNE
    END ELSE
        R.RECORD = R.STMT.ENTRY
    END

    RETURN
*
*----------------------------------------------
*
READ.CATEG.ENTRY:

    FN.CAT.ENT = 'F':LEAD.MNE:'.CATEG.ENTRY'
    F.CAT.ENT = ''
    CALL OPF(FN.CAT.ENT,F.CAT.ENT)

    FN.CAT.ENT.DTL = 'F':LEAD.MNE:'.CATEG.ENTRY.DETAIL'
    F.CAT.ENT.DTL = ''
    CALL OPF(FN.CAT.ENT.DTL,F.CAT.ENT.DTL)

    CALL F.READ(FN.CAT.ENT,TXN.ID,R.CAT.ENT,F.CAT.ENT,R.CAT.ENT.ERR)
    IF R.CAT.ENT.ERR THEN
        CALL F.READ(FN.CAT.ENT.DTL,TXN.ID,R.CAT.ENT,F.CAT.ENT.DTL,R.CAT.DTL.ERR)
        O.DATA = 'N':TXN.ID:'*':CO.MNE
    END
    R.RECORD = R.CAT.ENT
    RETURN

*
*-----------------------------------------------
*
READ.SPEC.ENTRY:
    FN.RE.SPEC.ENT = 'F':LEAD.MNE:'.RE.CONSOL.SPEC.ENTRY'
    F.RE.SPEC.ENT = ''
    CALL OPF(FN.RE.SPEC.ENT,F.RE.SPEC.ENT)

    FN.RE.ENT.DTL = 'F':LEAD.MNE:'.RE.SPEC.ENTRY.DETAIL'
    F.RE.ENT.DTL = ''
    CALL OPF(FN.RE.ENT.DTL,F.RE.ENT.DTL)

    FN.CON.ENT = 'F':LEAD.MNE:'.CONSOL.ENT.TODAY'
    F.CON.ENT = ''
    CALL OPF(FN.CON.ENT,F.CON.ENT)
    CALL F.READ(FN.RE.SPEC.ENT,TXN.ID,R.SPEC.REC,F.RE.SPEC.ENT,R.RE.ERR)
    IF R.RE.ERR THEN
        CALL F.READ(FN.RE.ENT.DTL,TXN.ID,R.SPEC.REC,F.RE.ENT.DTL,R.RE.DTL.ERR)
        IF R.RE.DTL.ERR THEN
            CALL F.READ (FN.CON.ENT,TXN.ID,R.SPEC.REC,F.CON.ENT,R.CON.ERR)
            O.DATA = 'E':TXN.ID:'*':CO.MNE
            GOSUB FILL.CET
        END ELSE
            O.DATA = 'R':TXN.ID:'*':CO.MNE
            R.RECORD = R.SPEC.REC
            TXN.CODE = R.SPEC.REC<RE.CSE.TRANSACTION.CODE>
            GOSUB GET.TXN.CODE.DESC
        END
    END ELSE
        O.DATA = 'D':TXN.ID:'*':CO.MNE
        R.RECORD = R.SPEC.REC
        TXN.CODE = R.SPEC.REC<RE.CSE.TRANSACTION.CODE>
        GOSUB GET.TXN.CODE.DESC
    END

    RETURN
*BG_100020158
*-----------------------------------------------
FILL.CET:

* the fields of the enquiry are designed for STMT,CATEG and SPEC
* it cannot display for CET so send the values in the position of
* other entries
* VDATE -----> 11th position : customer -----> 8th position
* ACCOUNT -----> 1ST position : Booking date -----> 25th position
* Fcy  amt  -----> 13th position : Lcy amount -----> 3rd position
* Get these position from STMT than hard coding them
    R.RECORD = R.SPEC.REC
    R.RECORD<AC.STE.VALUE.DATE> = R.SPEC.REC<RE.CET.VALUE.DATE>
    R.RECORD<AC.STE.CUSTOMER.ID> = R.SPEC.REC<RE.CET.CUSTOMER>
* For account display the transaction code
    TXN.CODE = R.SPEC.REC<RE.CET.TXN.CODE>
    GOSUB GET.TXN.CODE.DESC
    R.RECORD<AC.STE.BOOKING.DATE> = R.SPEC.REC<RE.CET.BOOKING.DATE>
    R.RECORD<AC.STE.CURRENCY> = R.SPEC.REC<RE.CET.CURRENCY>
    THIS.AMT = 0
    BEGIN CASE
        CASE R.SPEC.REC<RE.CET.LOCAL.CR> NE ""
            THIS.AMT = R.SPEC.REC<RE.CET.LOCAL.CR>
            R.RECORD<AC.STE.AMOUNT.LCY> = THIS.AMT
            R.RECORD<AC.STE.AMOUNT.FCY> = 0
        CASE R.SPEC.REC<RE.CET.LOCAL.DR> NE ""
            THIS.AMT = R.SPEC.REC<RE.CET.LOCAL.DR>
            R.RECORD<AC.STE.AMOUNT.LCY> = THIS.AMT
            R.RECORD<AC.STE.AMOUNT.FCY> = 0
        CASE R.SPEC.REC<RE.CET.FOREIGN.CR> NE ""
            THIS.AMT = R.SPEC.REC<RE.CET.FOREIGN.CR>
            R.RECORD<AC.STE.AMOUNT.LCY> = 0
            R.RECORD<AC.STE.AMOUNT.FCY> = THIS.AMT
        CASE R.SPEC.REC<RE.CET.FOREIGN.DR> NE ""
            THIS.AMT = R.SPEC.REC<RE.CET.FOREIGN.DR>
            R.RECORD<AC.STE.AMOUNT.LCY> = 0
            R.RECORD<AC.STE.AMOUNT.FCY> = THIS.AMT
    END CASE
    R.RECORD<AC.STE.COMPANY.CODE> = R.SPEC.REC<RE.CET.CO.CODE>
    RETURN
*-------------------------------------------------------------------
GET.TXN.CODE.DESC:

    F.TXN = ""; FN.TXN = "F.RE.TXN.CODE"; ENT.TXN.REC = ""
    CALL OPF(FN.TXN,F.TXN)
    CALL F.READ(FN.TXN,TXN.CODE,ENT.TXN.REC,F.TXN,TXN.ER)
    R.RECORD<1> = ENT.TXN.REC<RE.TXN.SHORT.DESC>
    SPEC.FLAG = ""

    RETURN
*--------------------------------------------------------------------
*BG_100020158
    END

