*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
	$PACKAGE AC.ModelBank
	
    SUBROUTINE E.AC.CURR.AVAIL.BAL(BALANCE,IN.DATA)
*************************************************************************
* 15/05/06 - EN_10002924
*            Use core routine and set the flag ENT.TODAY.UPDATE in
*            in ACCOUNT.PARAMETER to get OPEN.ACTUAL.BAL and OPEN.CLEARED.BAL
*            to avoid the usage of OPEN balances fields in ACCOUNT application
*            and hence allow removal of ACCT.ENT.TODAY .
*
* 08/11/06 - CI_10045362
*            Move this code to AC.CURR.AVAIL.BAL routine.
*
********************************************************************************
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
*
    CALL AC.CURR.AVAIL.BAL(BALANCE,IN.DATA)
*
    RETURN
*
END

