*-----------------------------------------------------------------------------
* ToolBox Common area to equate tag variables to tags.
*-----------------------------------------------------------------------------

*** <region name = Generic XML Tag Definitions>
      EQU C$ACTION.TAG.O TO '<action>'
      EQU C$ACTION.TAG.C TO '</action>'
      EQU C$RESP.ENCRYPT.TAG.O TO '<responseEncryption>'
      EQU C$RESP.ENCRYPT.TAG.C TO '</responseEncryption>'
      EQU C$ERROR.TAG.O TO '<responseerror>'
      EQU C$ERROR.TAG.C TO '</responseerror>'
      EQU C$FLD.TAG.O TO '<fld>'           ;* Field Node
      EQU C$FLD.TAG.C TO '</fld>'
      EQU C$FLDS.TAG.O TO '<flds>'         ;* Fields Node
      EQU C$FLDS.TAG.C TO '</flds>'
      EQU C$XML.RECORD.TAG.O TO '<xmlRecord>'
      EQU C$XML.RECORD.TAG.C TO '</xmlRecord>'
*** </region>

*** <region name = XML Tag Definitions Used in EB.COMMON.REQUESTS>

      EQU C$CRREQU.TAG.O TO '<crequest>'
      EQU C$CRREQU.TAG.C TO '</crequest>'
      EQU C$CRRESP.TAG.O TO '<cresponse>'
      EQU C$CRRESP.TAG.C TO '</cresponse>'
      
      * Field List
      EQU C$FN.TAG.O TO '<fn>'
      EQU C$FN.TAG.C TO '</fn>'
      EQU C$FSVP.TAG.O TO '<fsvp>'
      EQU C$FSVP.TAG.C TO '</fsvp>'
      EQU C$FLNG.TAG.O TO '<flng>'
      EQU C$FLNG.TAG.C TO '</flng>'
      EQU C$FNO.TAG.O TO '<fno>'
      EQU C$FNO.TAG.C TO '</fno>'
      EQU C$FSM.TAG.O TO '<fsm>'
      EQU C$FSM.TAG.C TO '</fsm>'
      EQU C$FVL.TAG.O TO '<fvl>'
      EQU C$FVL.TAG.C TO '</fvl>'
      
      * Record List
      EQU C$R.IDS.TAG.O TO '<rIds>'
      EQU C$R.IDS.TAG.C TO '</rIds>'
      EQU C$R.ID.TAG.O TO '<rId>'
      EQU C$R.ID.TAG.C TO '</rId>'
      EQU C$APP.ID.O TO '<appId>'
      EQU C$APP.ID.C TO '</appId>'
      
      * System Languages
      EQU C$LC.TAG.O TO '<lc>'
      EQU C$LC.TAG.C TO '</lc>'
      EQU C$LNME.TAG.O TO '<lmne>'
      EQU C$LNME.TAG.C TO '</lmne>'
      EQU C$LDESC.TAG.O TO '<ldesc>'
      EQU C$LDESC.TAG.C TO '</ldesc>'
      EQU C$LANG.TAG.O TO '<lng>'
      EQU C$LANG.TAG.C TO '</lng>'
      EQU C$LANGS.TAG.O TO '<lngs>'
      EQU C$LANGS.TAG.C TO '</lngs>'
*** </region>

*** <region name = XML Tag Definitions Used in EB.SCREEN.DESIGNER>
      
      EQU C$SDREQU.TAG.O TO '<sdrequest>'
      EQU C$SDREQU.TAG.C TO '</sdrequest>'
      EQU C$SDRESP.TAG.O TO '<sdresponse>'
      EQU C$SDRESP.TAG.C TO '</sdresponse>'
      
      EQU C$BAS.TAG.O TO '<baseApp>'       ;* Base Application Definition (Object Map) e.g. CUSTOMER
      EQU C$BAS.TAG.C TO '</baseApp>'
      EQU C$VER.TAG.O TO '<verDef>'        ;* Version Definition (Object Map) e.g. CUSTOMER,CLIENT
      EQU C$VER.TAG.C TO '</verDef>'
      EQU C$VRE.TAG.O TO '<verDet>'        ;* Version Record Details
      EQU C$VRE.TAG.C TO '</verDet>'
      EQU C$VSC.TAG.O TO '<verFile>'       ;* File Version Record was read from
      EQU C$VSC.TAG.C TO '</verFile>'
      EQU C$ICP.TAG.O TO '<icp>'           ;* Initial Cursor Position
      EQU C$ICP.TAG.C TO '</icp>'
      EQU C$APP.TAG.O TO '<applicationId>' ;* The Application ID
      EQU C$APP.TAG.C TO '</applicationId>'
      EQU C$FRF.TAG.O TO '<frf>'           ;* Field reference 3, 13.2, null
      EQU C$FRF.TAG.C TO '</frf>'
      EQU C$MAS.TAG.O TO '<mas>'           ;* Associated multivalue field number
      EQU C$MAS.TAG.C TO '</mas>'
      EQU C$SAS.TAG.O TO '<sas>'           ;* Associated subvalue field number
      EQU C$SAS.TAG.C TO '</sas>'
      EQU C$TYP.TAG.O TO '<typ>'           ;* Field type, CUS, CCY
      EQU C$TYP.TAG.C TO '</typ>'
      EQU C$COL.TAG.O TO '<col>'           ;* Relative column number for display
      EQU C$COL.TAG.C TO '</col>'
      EQU C$LIN.TAG.O TO '<lin>'           ;* Relative line number for display
      EQU C$LIN.TAG.C TO '</lin>'
      EQU C$SIZ.TAG.O TO '<siz>'           ;* Size of field
      EQU C$SIZ.TAG.C TO '</siz>'
      EQU C$MXM.TAG.O TO '<mxm>'           ;* Max number of characters
      EQU C$MXM.TAG.C TO '</mxm>'
      EQU C$MNM.TAG.O TO '<mnm>'           ;* Min number of characters
      EQU C$MNM.TAG.C TO '</mnm>'
      EQU C$IPT.TAG.O TO '<ipt>'           ;* Inputtable
      EQU C$IPT.TAG.C TO '</ipt>'
      EQU C$VIS.TAG.O TO '<vis>'           ;* Visible
      EQU C$VIS.TAG.C TO '</vis>'
      EQU C$PWD.TAG.O TO '<pwd>'           ;* Width of prompt
      EQU C$PWD.TAG.C TO '</pwd>'
      EQU C$PRM.TAG.O TO '<prm>'           ;* Prompt or field name
      EQU C$PRM.TAG.C TO '</prm>'
      EQU C$EWD.TAG.O TO '<ewd>'           ;* Width of enrichments & errors
      EQU C$EWD.TAG.C TO '</ewd>'
      EQU C$HED.TAG.O TO '<hed>'           ;* Flag to indicate object is in header
      EQU C$HED.TAG.C TO '</hed>'
      EQU C$LNG.TAG.O TO '<lng>'           ;* 1TOMv languaged 2TOSv languaged
      EQU C$LNG.TAG.C TO '</lng>'
      EQU C$NAM.TAG.O TO '<nam>'           ;* Field name for help
      EQU C$NAM.TAG.C TO '</nam>'
      EQU C$REL.TAG.O TO '<rel>'           ;* Name of related file
      EQU C$REL.TAG.C TO '</rel>'
      EQU C$LST.TAG.O TO '<lst>'           ;* List of options Y_NO
      EQU C$LST.TAG.C TO '</lst>'
      EQU C$MVE.TAG.O TO '<mve>'           ;* Multi value expansion
      EQU C$MVE.TAG.C TO '</mve>'
      EQU C$SVE.TAG.O TO '<sve>'           ;* Sub value expansion
      EQU C$SVE.TAG.C TO '</sve>'
      EQU C$IMG.TAG.O TO '<img>'           ;* Associated image
      EQU C$IMG.TAG.C TO '</img>'
      EQU C$CTX.TAG.O TO '<cix>'           ;* Associated context enquiry
      EQU C$CTX.TAG.C TO '</cix>'
      EQU C$PMT.TAG.O TO '<pmt>'           ;* Prompt Text for Versions Fields
      EQU C$PMT.TAG.C TO '</pmt>'
      EQU C$CAS.TAG.O TO '<cas>'           ;* Case Conversion for Version Fields
      EQU C$CAS.TAG.C TO '</cas>'
      EQU C$POP.TAG.O TO '<pop>'           ;* Pop Up Controls for Version Fields
      EQU C$POP.TAG.C TO '</pop>'
      EQU C$DDL.TAG.O TO '<ddl>'           ;* Drop Down List Associated with Version Fields
      EQU C$DDL.TAG.C TO '</ddl>'
      EQU C$DDS.TAG.O TO '<dds>'           ;* Drop Down Selection List Associated with Version Fields
      EQU C$DDS.TAG.C TO '</dds>'
      EQU C$HYP.TAG.O TO '<hyp>'           ;* Allows any Standard Command to be launched when the field prompt is clicked
      EQU C$HYP.TAG.C TO '</hyp>'
      EQU C$HTX.TAG.O TO '<htx>'           ;* Flag to indicate Hyperlink is a "TEXT"
      EQU C$HTX.TAG.C TO '</htx>'
      EQU C$FTP.TAG.O TO '<ftp>'           ;* Display Type
      EQU C$FTP.TAG.C TO '</ftp>'
      EQU C$ASS.TAG.O TO '<ass>'           ;* Association Field
      EQU C$ASS.TAG.C TO '</ass>'
      EQU C$ILK.TAG.O TO '<ilk>'           ;* IDescriptor
      EQU C$ILK.TAG.C TO '</ilk>'
      EQU C$GRD.TAG.O TO '<grd>'           ;* Enable Grid
      EQU C$GRD.TAG.C TO '</grd>'
      EQU C$MDL.TAG.O TO '<mdl>'           ;* Max Display Lines
      EQU C$MDL.TAG.C TO '</mdl>'
      EQU C$IDP.TAG.O TO '<idp>'           ;* IDescriptor Flag
      EQU C$IDP.TAG.C TO '</idp>'
      EQU C$HOT.TAG.O TO '<hot>'           ;* Hot Field associated with Version Fields
      EQU C$HOT.TAG.C TO '</hot>'
      EQU C$ECOL.TAG.O TO '<ecol>'         ;* Field Enrichment Position
      EQU C$ECOL.TAG.C TO '</ecol>'
      EQU C$PCOL.TAG.O TO '<pcol>'         ;* Field Prompt Position.
      EQU C$PCOL.TAG.C TO '</pcol>'
      EQU C$TTP.TAG.O TO '<ttp>'           ;* Tool Tip
      EQU C$TTP.TAG.C TO '</ttp>'
      EQU C$LRF.TAG.O TO '<lrf>'           ;* Local Ref Field
      EQU C$LRF.TAG.C TO '</lrf>'
      EQU C$HVA.TAG.O TO '<hva>'           ;* Hot Validate
      EQU C$HVA.TAG.C TO '</hva>'
      EQU C$WEB.TAG.O TO '<web>'           ;* Web Validation Field associated with Version Fields
      EQU C$WEB.TAG.C TO '</web>'
      EQU C$DTO.TAG.O TO '<dto>'           ;* DISPLAY.TYPE Options (field on VERSION)
      EQU C$DTO.TAG.C TO '</dto>'
      EQU C$PUO.TAG.O TO '<puo>'           ;* POPUP.CONTROL Options (field on VERSION)
      EQU C$PUO.TAG.C TO '</puo>'
      EQU C$ATO.TAG.O TO '<ato>'           ;* ATTRIBUTES Options (field on VERSION)
      EQU C$ATO.TAG.C TO '</ato>'
      EQU C$AO.TAG.O TO '<ao>'             ;* ATTRIBS Options (field on VERSION)
      EQU C$AO.TAG.C TO '</ao>'
      EQU C$ENR.TAG.O TO '<enr>'           ;* Enrichment Only
      EQU C$ENR.TAG.C TO '</enr>'
      EQU C$PDF.TAG.O TO '<pdf>'           ;* Popup Dropdown
      EQU C$PDF.TAG.C TO '</pdf>'      
 	  EQU C$NBP.TAG.O TO '<nbp>'           ;* No browser Text
      EQU C$NBP.TAG.C TO '</nbp>'       

*** </region>

*** <region name = XML Tag Definitions Used in EB.SYSTEM.INFO>
      EQU C$SIREQU.TAG.O TO '<sirequest>'
      EQU C$SIREQU.TAG.C TO '</sirequest>'
      EQU C$SIRESP.TAG.O TO '<siresponse>'
      EQU C$SIRESP.TAG.C TO '</siresponse>'
      
      EQU C$SIPRODS.TAG.O TO '<products>'
      EQU C$SIPRODS.TAG.C TO '</products>'
      EQU C$SIPROD.TAG.O TO '<product>'
      EQU C$SIPROD.TAG.C TO '</product>'
      EQU C$SICOMPS.TAG.O TO '<components>'
      EQU C$SICOMPS.TAG.C TO '</components>'
      
      EQU C$SIJDIAG.TAG.O TO '<jdiag>'
      EQU C$SIJDIAG.TAG.C TO '</jdiag>'
      EQU C$SIOSSYSTEM.TAG.O TO '<ossystem>'
      EQU C$SIOSSYSTEM.TAG.C TO '</ossystem>'
      EQU C$SIJALL.TAG.O TO '<jdall>'
      EQU C$SIJALL.TAG.C TO '</jdall>'
      EQU C$SISYSDEF.TAG.O TO '<sysdef>'
      EQU C$SISYSDEF.TAG.C TO '</sysdef>'
      EQU C$SITAF.TAG.O TO '<taf>'
      EQU C$SITAF.TAG.C TO '</taf>'

*** </region>
