*-----------------------------------------------------------------------------
* <Rating>-93</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.AA.GET.DELIVERY.MESSAGE.REF(RETURN.ARRAY)
*
*** <region name= Description>
*** <desc>Task of the sub-routine</desc>
* Program Description
*
** This routine will be used get the delivery message reference from AA.ARRANGEMENT.ACTIIFVTY record.
**
*-----------------------------------------------------------------------------
* @package retaillending.AA
* @class AA.ModelBank
* @stereotype subroutine
* @link STANDARD.SELECTION>NOFILE.AA.DETAILS.MESSAGES
* @author psabari@temenos.com
*-----------------------------------------------------------------------------
*
*** </region>

*** <region name= Arguments>
*** <desc>Input and out arguments required for the sub-routine</desc>
* Arguments
*
* Input
*
* @param ENQ.SELECTION - <2,1>   Arrangement id
*
* Output
*
* @param RETURN.ARRAY  - <1>     Activity date
*                      - <2>     Activity
*                      - <3>     Activity reference
*                      - <4>     Delivery message reference
*-----------------------------------------------------------------------------

*** <region name= Modification History>
*** <desc>Modifications done in the sub-routine</desc>
* Modification History
*
* 28/03/13 - New routine.
*            Defect: 607823
*
* 26/11/13  - Defect : 846936
*             Task   : 847019
*             Return value should be separated by "#" other than "*".Since some activity will have  '*' in their name.
*
* 10/02/15  - Defect : 1247848
*             Task   : 1250600
*             Restricted Backdating: HELOC Statement generated based on the Defer Days disappeared
*** </region>
*-----------------------------------------------------------------------------

*** <region name= Inserts>
*** <desc>File inserts and common variables used in the sub-routine</desc>
* Inserts

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.AA.ACTIVITY.HISTORY
    $INSERT I_F.AA.ARRANGEMENT.ACTIVITY

*** </region>
*-----------------------------------------------------------------------------

*** <region name= Main control>
*** <desc>main control logic in the sub-routine</desc>

    GOSUB INITIALISE
    LOCATE 'ARRANGEMENT.ID' IN ENQ.SELECTION<2,1> SETTING ARR.POS THEN
        ARR.ID = ENQ.SELECTION<4,ARR.POS>
    END
    IF ARR.ID THEN  ;* Process only when arrangement id is present.
        GOSUB OPEN.FILES
        GOSUB PROCESS
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------

*** <region name= Initialise>
*** <desc>File variables and local variables</desc>
INITIALISE:
*----------

    EFFECTIVE.DATE = ''
    ACTIVITY = ''
    ACTIVITY.REF = ''
    DELIVERY.REF = ''
    ARR.ID = ''
    RETURN.ARRAY = ''
    DELIVERY.REFERENCES = ""
    ADJUSTMENT.REFERENCES = ""

    RETURN

*** </region>
*-----------------------------------------------------------------------------

*** <region name= Open Files>
*** <desc>Open the required files to be used</desc>
OPEN.FILES:
*----------

    FN.AA.ACTIVITY.HISTORY = "F.AA.ACTIVITY.HISTORY"
    F.AA.ACTIVITY.BALANCES = ""
    CALL OPF(FN.AA.ACTIVITY.HISTORY, F.AA.ACTIVITY.HISTORY)

    FN.AA.ARRANGEMENT.ACTIVITY = 'F.AA.ARRANGEMENT.ACTIVITY'
    F.AA.ARRANGEMENT.ACTIVITY = ''
    CALL OPF(FN.AA.ARRANGEMENT.ACTIVITY, F.AA.ARRANGEMENT.ACTIVITY)

    FN.AA.ARRANGEMENT.ACTIVITY$HIS = 'F.AA.ARRANGEMENT.ACTIVITY$HIS'
    F.AA.ARRANGEMENT.ACTIVITY$HIS = ''
    CALL OPF(FN.AA.ARRANGEMENT.ACTIVITY$HIS, F.AA.ARRANGEMENT.ACTIVITY$HIS)

    FN.AA.ARRANGEMENT.ACTIVITY$NAU = 'F.AA.ARRANGEMENT.ACTIVITY$NAU'
    F.AA.ARRANGEMENT.ACTIVITY$NAU = ''
    CALL OPF(FN.AA.ARRANGEMENT.ACTIVITY$NAU, F.AA.ARRANGEMENT.ACTIVITY$NAU)

    RETURN
*** </region>
*-----------------------------------------------------------------------------

*** <region name= Process>
*** <desc>Set-up action processing</desc>
PROCESS:
*-------

    R.AA.ACTIVITY.HISTORY = ''
    CALL F.READ(FN.AA.ACTIVITY.HISTORY, ARR.ID, R.AA.ACTIVITY.HISTORY, F.AA.ACTIVITY.HISTORY, ERR.MSG)
    EFF.DATE.CNT = DCOUNT(R.AA.ACTIVITY.HISTORY<AA.AH.EFFECTIVE.DATE>,VM)
    INT.DATE.CNT = 1

    LOOP
    WHILE (INT.DATE.CNT LE EFF.DATE.CNT)
        INT.ACCT.CNT = 1
        ACCT.CNT = DCOUNT(R.AA.ACTIVITY.HISTORY<AA.AH.ACTIVITY,INT.DATE.CNT>,SM)
        LOOP
        WHILE (INT.ACCT.CNT LE ACCT.CNT)

            ACTIVITY.STATUS = R.AA.ACTIVITY.HISTORY<AA.AH.ACT.STATUS,INT.DATE.CNT,INT.ACCT.CNT>
            EFFECTIVE.DATE  = R.AA.ACTIVITY.HISTORY<AA.AH.EFFECTIVE.DATE,INT.DATE.CNT>
            ACTIVITY        = R.AA.ACTIVITY.HISTORY<AA.AH.ACTIVITY,INT.DATE.CNT,INT.ACCT.CNT>
            ACTIVITY.REF    = R.AA.ACTIVITY.HISTORY<AA.AH.ACTIVITY.REF,INT.DATE.CNT,INT.ACCT.CNT>
            ACTIVITY.TYPE   = R.AA.ACTIVITY.HISTORY<AA.AH.INITIATION,INT.DATE.CNT,INT.ACCT.CNT>

            BEGIN CASE
            CASE ACTIVITY.STATUS EQ 'AUTH'
                FILE.TO.CHECK = ""
                GOSUB GET.AAA.FROM.LIVE
                GOSUB GET.DELIVERY.MESSAGE.REF
            CASE ACTIVITY.STATUS EQ 'DELETE-REV'
                FILE.TO.CHECK = ""
                GOSUB GET.AAA.FROM.LIVE
                GOSUB GET.DELIVERY.MESSAGE.REF
            CASE ACTIVITY.STATUS EQ 'AUTH-REV'
                FILE.TO.CHECK = "$HIS"
                GOSUB GET.AAA.FROM.HISTORY.OR.NAU
                GOSUB GET.DELIVERY.MESSAGE.REF
            CASE ACTIVITY.STATUS EQ 'REVERSE'
                FILE.TO.CHECK = "$NAU"
                GOSUB GET.AAA.FROM.HISTORY.OR.NAU
                GOSUB GET.DELIVERY.MESSAGE.REF
            END CASE

            INT.ACCT.CNT += 1
        REPEAT
        INT.DATE.CNT += 1
    REPEAT

    RETURN
*** </region>
*-----------------------------------------------------------------------------

*** <region name= Get AAA From Live>
*** <desc>Get AA Arrangement Activity Record from Live file</desc>
GET.AAA.FROM.LIVE:
*-----------------

    AAA.TXN.REFERENCE = ACTIVITY.REF
    GOSUB READ.AA.ARRANGEMENT.ACTIVITY
    DELIVERY.REFERENCES = R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.DELIVERY.REF>

    RETURN

*** </region>
*-----------------------------------------------------------------------------

*** <region name= Get AAA From History or INAU>
*** <desc>Get AA Arrangement Activity Record from History or Unauthorised file</desc>
READ.AA.ARRANGEMENT.ACTIVITY:
*----------------------------

    R.AA.ARRANGEMENT.ACTIVITY = ''
    ERR.R.AA.ARRANGEMENT.ACTIVITY = ''

    BEGIN CASE
    CASE FILE.TO.CHECK EQ "$NAU"
        CALL F.READ(FN.AA.ARRANGEMENT.ACTIVITY$NAU, AAA.TXN.REFERENCE, R.AA.ARRANGEMENT.ACTIVITY, F.AA.ARRANGEMENT.ACTIVITY$NAU, ERR.R.AA.ARRANGEMENT.ACTIVITY)
    CASE FILE.TO.CHECK EQ "$HIS"
        AAA.TXN.REFERENCE := ";2"       ;* Incase History add the Cur No.
        CALL F.READ(FN.AA.ARRANGEMENT.ACTIVITY$HIS, AAA.TXN.REFERENCE, R.AA.ARRANGEMENT.ACTIVITY, F.AA.ARRANGEMENT.ACTIVITY$HIS, ERR.R.AA.ARRANGEMENT.ACTIVITY)
        AAA.TXN.REFERENCE = AAA.TXN.REFERENCE[";", 1, 1]    ;* Restore it back, we may be using it.
    CASE 1
        CALL F.READ(FN.AA.ARRANGEMENT.ACTIVITY, AAA.TXN.REFERENCE, R.AA.ARRANGEMENT.ACTIVITY, F.AA.ARRANGEMENT.ACTIVITY, ERR.R.AA.ARRANGEMENT.ACTIVITY)
    END CASE

    IF R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.ADJUSTMENT> THEN          ;* Storing adjustement references is important because a user activity Id may already changed by RR and User also reversed the new one manually. 
        
        ADJUSTMENT.REF = R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.ADJUSTMENT>
        LOCATE ADJUSTMENT.REF IN ADJUSTMENT.REFERENCES<1,1> SETTING ADJ.POS ELSE
            ADJUSTMENT.REFERENCES<1,ADJ.POS> = ADJUSTMENT.REF
            ADJUSTMENT.REFERENCES<2,ADJ.POS> = ACTIVITY.STATUS
            NULL
        END

    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------

*** <region name= Get AAA From History or NAU>
*** <desc>Get AA Arrangement Activity Record from History or NAU</desc>
GET.AAA.FROM.HISTORY.OR.NAU:
*---------------------------

    AAA.TXN.REFERENCE = ACTIVITY.REF
    GOSUB READ.AA.ARRANGEMENT.ACTIVITY
    DELIVERY.REFERENCES = R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.DELIVERY.REF>
    REV.MASTER.AAA      = R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.REV.MASTER.AAA>
    LINKED.ACTIVITY     = R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.LINKED.ACTIVITY>

    IF NOT(DELIVERY.REFERENCES) THEN    ;* We don't Need to check the master because it doesn't have a delivery referance generated.
        RETURN
    END

    LOOP
    WHILE ACTIVITY.TYPE EQ "SECONDARY"

        LINKED.ACTIVITY = R.AA.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.LINKED.ACTIVITY>
        LOCATE LINKED.ACTIVITY IN R.AA.ACTIVITY.HISTORY<AA.AH.ACTIVITY.REF,INT.DATE.CNT,INT.ACCT.CNT> SETTING ACT.POS THEN
            AAA.TXN.REFERENCE = R.AA.ACTIVITY.HISTORY<AA.AH.ACTIVITY.REF, INT.DATE.CNT, ACT.POS> : ";2"
            ACTIVITY.TYPE    = R.AA.ACTIVITY.HISTORY<AA.AH.INITIATION,INT.DATE.CNT,ACT.POS>
            GOSUB READ.AA.ARRANGEMENT.ACTIVITY
        END ELSE
            ACTIVITY.TYPE = ""
        END

    REPEAT

    LOCATE AAA.TXN.REFERENCE IN ADJUSTMENT.REFERENCES<1,1> SETTING ADJ.REF.POS THEN       ;* Locate the AAA.TXN.REFERENCE in Adjustment Activity list. if it exist then, it means we need to ignore the Delelivery messages generated by him.
        IF ADJUSTMENT.REFERENCES<2,ADJ.REF.POS> EQ "AUTH-REV" THEN
            DELIVERY.REFERENCES = ""
        END
    END ELSE
        IF AAA.TXN.REFERENCE EQ REV.MASTER.AAA THEN         ;* Incase the Original Master AAA is different from Activity Reference.
            DELIVERY.REFERENCES = ""
        END
    END

    RETURN
*** </region>
*-----------------------------------------------------------------------------

*** <region name= Get DELIVERY.MESSAGE.REF>
*** <desc>Get the delivery message reference from AAA record</desc>
GET.DELIVERY.MESSAGE.REF:
*------------------------

    LOOP
        REMOVE DELIVERY.REF FROM DELIVERY.REFERENCES SETTING DEL.POS
    WHILE DELIVERY.REF : DEL.POS

        IF NOT(RETURN.ARRAY) THEN
            RETURN.ARRAY = EFFECTIVE.DATE:'#':ACTIVITY:'#':ACTIVITY.REF:'#':DELIVERY.REF  ;* Display multi delivery reference under single activity.
        END ELSE
            RETURN.ARRAY:=FM:EFFECTIVE.DATE:'#':ACTIVITY:'#':ACTIVITY.REF:'#':DELIVERY.REF
        END

        EFFECTIVE.DATE = ""
        ACTIVITY = ""
        ACTIVITY.REF = ""

    REPEAT

    RETURN
*** </region>
*-----------------------------------------------------------------------------
END

