*-----------------------------------------------------------------------------------
* <Rating>-164</Rating>
*-----------------------------------------------------------------------------
   $PACKAGE AC.ModelBank

    SUBROUTINE E.AI.LAST.N.TXNS.BUILD(ENQ.DATA)
*-----------------------------------------------------------------------------------
* MODIFICATION HISTORY:
************************
* 25/10/11 - En- 99120 / Task - 156274
*            Improvement odf stmt.enquiries
*
*--------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_GTS.COMMON
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.COMPANY
    $INSERT I_F.ACCOUNT
*-----------------------------------------------------------------------------------
*
    GOSUB INITIALISE
    GOSUB CHECK.PRELIM.CONDITIONS
    IF PROCESS.GOAHEAD THEN
        GOSUB PROCESS
    END

    RETURN
*-----------------------------------------------------------------------------------
INITIALISE:
*---------
    PROCESS.GOAHEAD = 1
    STORED.ENTRIES.LIST = ""
    LOCATE.FIELD.MANDATORY = ""
    LOCATE.DEFAULT.VALUE = ""
    LOCATE.FIELD.NUMERIC = ""
    TXN.DATE = ""
    ORIG.ACCOUNT.NUMBER = ""

    FN.AC = "F.ACCOUNT" ; F.AC = ""

    RETURN
*-----------------------------------------------------------------------------------
CHECK.PRELIM.CONDITIONS:
*
    LOOP.CNT = 1 ; MAX.LOOPS = 8
    LOOP
    WHILE LOOP.CNT LE MAX.LOOPS AND PROCESS.GOAHEAD DO

        BEGIN CASE
            CASE LOOP.CNT EQ 1
                GOSUB GET.ACCOUNT

            CASE LOOP.CNT EQ 2
                GOSUB GET.REQUIRED.ENTRY.COUNT

            CASE LOOP.CNT EQ 3
                GOSUB GET.MAX.HISTORY.MONTHS

            CASE LOOP.CNT EQ 4
                GOSUB CHECK.PROCESSING.DATE.FLAG

            CASE LOOP.CNT EQ 5
                GOSUB GET.IN.START.DATE
                GOSUB VALIDATE.IN.START.DATE          ;! Against the Max threshold we can go back in history

            CASE LOOP.CNT EQ 6
                GOSUB GET.IN.END.DATE

            CASE LOOP.CNT EQ 7
                GOSUB LOAD.ACCOUNT.RECORD

            CASE LOOP.CNT EQ 8
                GOSUB GET.ACTIVITY.MONTHS

        END CASE

        IF ENQ.ERROR THEN
            PROCESS.GOAHEAD = 0
        END

        LOOP.CNT += 1

    REPEAT

    RETURN          ;* From CHECK.PRELIM.CONDITIONS
*-----------------------------------------------------------------------------------
GET.ACCOUNT:

    LOCATE.FIELD = "ACCT.ID"
    LOCATE.FIELD.MANDATORY = 1
    GOSUB GET.VALUE

    IF NOT(ENQ.ERROR) THEN
        ACCOUNT.NUMBER = LOCATE.VALUE
    END

    RETURN
*-----------------------------------------------------------------------------------
GET.REQUIRED.ENTRY.COUNT:

    LOCATE.FIELD = "NO.OF.ENTRIES"
    LOCATE.FIELD.NUMERIC = 1
    LOCATE.DEFAULT.VALUE = 10
    GOSUB GET.VALUE

    IF NOT(ENQ.ERROR) THEN
        REQUIRED.ENTRY.COUNT = LOCATE.VALUE
    END

    RETURN
*------------------------------------------------------------------------------------
GET.MAX.HISTORY.MONTHS:

    LOCATE.FIELD = "MAX.HISTORY.MONTHS"
    LOCATE.FIELD.NUMERIC = 1
    LOCATE.DEFAULT.VALUE = 12
    GOSUB GET.VALUE

    IF NOT(ENQ.ERROR) THEN
        MAX.HISTORY.MONTHS = LOCATE.VALUE
    END

    RETURN
*-----------------------------------------------------------------------------------
CHECK.PROCESSING.DATE.FLAG:

    ! In case the list needs to be returned based on PROCESSING.DATE
    LOCATE.FIELD = "TXN.DATE"
    LOCATE.DEFAULT.VALUE = "BOOK"
    GOSUB GET.VALUE
    IF NOT(ENQ.ERROR) THEN
        TXN.DATE = LOCATE.VALUE
    END

    RETURN
*-----------------------------------------------------------------------------------
GET.IN.START.DATE:

    IN.START.DATE = ""
    LOCATE.FIELD = "IN.START.DATE"
    GOSUB GET.VALUE
    IF NOT(ENQ.ERROR) THEN
        IN.START.DATE = LOCATE.VALUE
    END

    RETURN
*-----------------------------------------------------------------------------------
VALIDATE.IN.START.DATE:

    IF IN.START.DATE THEN
        ! Assuming TODAY is 2000 05 11 and IN.START.DATE is 1999 10 31

        ! 2000
        MAX.HISTORY.YYYY = TODAY[1,4]
        ! 05
        MAX.HISTORY.MM = TODAY[5,2]
        ! -1
        MAX.HISTORY.MM = MAX.HISTORY.MM - 6
        IF MAX.HISTORY.MM LE 0 THEN
            ! 1999
            MAX.HISTORY.YYYY = MAX.HISTORY.YYYY - 1
            ! 12 + (-1) = 11
            MAX.HISTORY.MM = 12 + MAX.HISTORY.MM
            ! (19)99 GT (20)10
            IF MAX.HISTORY.YYYY[3,2] GT TODAY[3,2] THEN
                ! 20 - 1 = 19
                MAX.HISTORY.YYYY[1,2] = MAX.HISTORY.YYYY[1,2] - 1
            END
        END
        ! 1999 11 11
        MAX.HISTORY.DATE = MAX.HISTORY.YYYY : STR("0",2-LEN(MAX.HISTORY.MM)) : MAX.HISTORY.MM : TODAY[7,2]
        ! 1999 10 31 LT 1999 11 11 and will result in an error
        IF IN.START.DATE LT MAX.HISTORY.DATE THEN
            ENQ.ERROR = "EB-RMB1.START.DATE.OUT.OF.RANGE"
            ENQ.ERROR<2,1> = MAX.HISTORY.MONTHS
        END
    END

    RETURN
*-----------------------------------------------------------------------------------
GET.IN.END.DATE:

    IN.END.DATE = ""
    LOCATE.FIELD = "IN.END.DATE"
    GOSUB GET.VALUE
    IF NOT(ENQ.ERROR) THEN
        IN.END.DATE = LOCATE.VALUE
        IF NOT(IN.END.DATE) AND IN.START.DATE THEN
            IN.END.DATE = TODAY
        END
    END

    RETURN
*-----------------------------------------------------------------------------------
GET.VALUE:

    LOCATE.VALUE = ""
    LOCATE LOCATE.FIELD IN ENQ.DATA<2,1> SETTING FLD.FOUND.POS THEN
        IF ENQ.DATA<3,FLD.FOUND.POS> EQ "EQ" THEN
            LOCATE.VALUE = ENQ.DATA<4,FLD.FOUND.POS>
        END ELSE
            ENQ.ERROR = "EB-RMB1.OPERAND.MUST.BE.EQ.FOR.":LOCATE.FIELD
        END
    END ELSE
        IF LOCATE.FIELD.MANDATORY THEN
            ENQ.ERROR = "EB-RMB1.":LOCATE.FIELD:".MANDATORY"
        END
    END
*
    BEGIN CASE
        CASE LOCATE.FIELD.NUMERIC
            IF LOCATE.VALUE AND NOT(NUM(LOCATE.VALUE)) THEN
                ENQ.ERROR = "EB-RMB1.":LOCATE.FIELD:".NOT.NUMERIC"
            END

        CASE NOT(LOCATE.VALUE)
            LOCATE.VALUE = LOCATE.DEFAULT.VALUE
    END CASE

    LOCATE.FIELD.MANDATORY = ""
    LOCATE.FIELD.NUMERIC = ""
    LOCATE.DEFAULT.VALUE = ""

    RETURN
*-----------------------------------------------------------------------------------
LOAD.ACCOUNT.RECORD:

    ACCOUNT.RECORD = "" ; ERR.AC = ""
    CALL F.READ(FN.AC,ACCOUNT.NUMBER,ACCOUNT.RECORD,F.AC,ERR.AC)
    IF ERR.AC THEN
        ENQ.ERROR = "EB-RMB1.REC.MISS.FILE"
        ENQ.ERROR<2,1> = ACCOUNT.NUMBER
        ENQ.ERROR<2,2> = FN.AC
    END

    RETURN
*-----------------------------------------------------------------------------------
GET.ACTIVITY.MONTHS:

    IF NOT(IN.START.DATE) THEN
        ACCT.ACTIVITY.MONTHS = ""
        *
        * This will return the list of YYYYMMs when there has been any activity on the account
        * in an FM delimited array (as stored in EB.CONTRACT.BALANCES)
        *

        IF ACCOUNT.RECORD<AC.ARRANGEMENT.ID> THEN ;*For arrangement account
            ARRANGEMENT.ID = ACCOUNT.RECORD<AC.ARRANGEMENT.ID>
            CALL AA.GET.BALANCE.TYPE('ACCOUNT', ARRANGEMENT.ID, BALANCE.TYPE, RET.ERROR)  ;* Get the CUR Balance type for Account property class
            IF BALANCE.TYPE THEN
                ORIG.ACCOUNT.NUMBER = ACCOUNT.NUMBER
                ACCOUNT.NUMBER = ACCOUNT.NUMBER:'.':BALANCE.TYPE
            END
        END

        CALL GET.ACTIVITY.DATES(ACCOUNT.NUMBER,ACCT.ACTIVITY.MONTHS)
        IF NOT(ACCT.ACTIVITY.MONTHS) THEN
            PROCESS.GOAHEAD = 0
        END

        IF ORIG.ACCOUNT.NUMBER THEN
            ACCOUNT.NUMBER = ORIG.ACCOUNT.NUMBER  ;*Resume the original account number
        END
    END

    RETURN
*-----------------------------------------------------------------------------------
PROCESS:
*------
    BEGIN CASE

        CASE IN.START.DATE AND IN.END.DATE
            LOCATE 'IN.START.DATE' IN ENQ.DATA<2,1> SETTING START.POS THEN
                DEL ENQ.DATA<2,START.POS>
                DEL ENQ.DATA<3,START.POS>
                DEL ENQ.DATA<4,START.POS>
            END
            LOCATE 'IN.END.DATE' IN ENQ.DATA<2,1> SETTING END.POS THEN
                DEL ENQ.DATA<2,END.POS>
                DEL ENQ.DATA<3,END.POS>
                DEL ENQ.DATA<4,END.POS>
            END
            ENQ.DATA<2,-1> = "BOOKING.DATE"
            ENQ.DATA<3,-1> = "RG"
            ENQ.DATA<4,-1> = IN.START.DATE:@SM:IN.END.DATE

        CASE OTHERWISE
            NO.OF.ACTIVITY.MONTHS = DCOUNT(ACCT.ACTIVITY.MONTHS,FM)
            ENQ.DATA<2,-1> = "ACTIVITY.MONTHS"
            ENQ.DATA<3,-1> = "EQ"
            ENQ.DATA<4,-1> = LOWER(LOWER(ACCT.ACTIVITY.MONTHS))
    END CASE

    RETURN
*-----------------------------------------------------------------------------------

    END

