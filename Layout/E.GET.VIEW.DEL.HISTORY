*-----------------------------------------------------------------------------
* <Rating>482</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.GET.VIEW.DEL.HISTORY(ENQ.DETAILS)
*---------------------------------------------------------------------------------------------------------------
* Program description:
*------------------------
* This is the part of core enquiry and it is called from Standard selection record NOFILE.VIEW.DELETE.HISTORY
* enquiry created to view a deleted record details from the $DEL file and the drill down enquiry will open the
* exact records.
*
*
* @author - kbalakrishnan@temenos.com
* @stereotype - SUBROUTINE
* @param - ENQ.DETAILS (mandatory) and (output)
* @package - EB-CORE
* @uses - ''
* @returns - $DEL record details for the respective applications
* @links - OPF(1)
*
*---------------------------------------------------------------------------------------
* MODIFICATION HISTORY:
*-----------------------
*
* 28/08/12 - Enhancement - 371776, Task - 452007
*            Deleted item history (SAMBA)
*            New generic enquiry to view the DEL records
*
*---------------------------------------------------------------------------------------
*** <region name= INSERT FILES>
*** <desc>Insert the files required </desc>
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON

*
*** </region>
*---------------------------------------------------------------------------
*** <region name= MAIN.PROCESS>
*** <desc>The actual flow </desc>
*

    GOSUB Initialise          ;* Initialise the variables
    GOSUB formSelection ; *Form selection creteria based on the SELECTION criteria
    IF dontRunEnquiry THEN ;* Go out from enquiry
        RETURN ;* go out
    END

    GOSUB Process   ;* Create Enquiry

    RETURN
*
*** </region>
*----------------------------------------------------------------------------
*** <region name= Initialise>
*** <desc>Initialise the variables </desc>
Initialise:
*----------
* Initialise the variables

    enqValue =''
    actualDate = ''
    dontRunEnquiry='' ;* DONT run enquiry further

    RETURN

*** </region>

*------------------------------------------------------------------------------
*** <region name= Process>
*** <desc>Create the txn on a particluar day enquiry </desc>
Process:
*-----------

    GOSUB openFile  ;* Opens the file
    selList = theList

    LOOP
        REMOVE delRecId FROM selList SETTING recPos
    WHILE delRecId:recPos

        recStatus =''
        currNo = ''
        inputter = ''
        authoriser = ''
        dateTime=''
        companyId= ''
        includeRec = 0

        CALL F.READ(delApplicationName,delRecId,deletedRecord,fDelApplicationName,delErr)

        IF actualDate THEN    ;* if DATE value provided in selection criteria then go  with operand based selection
            BEGIN CASE

                CASE dateOperant EQ 'EQ'  ;* operand is equals
                    IF deletedRecord<V-5>[1,6] EQ actualDate THEN ;* fetch only the record same date record
                        includeRec = 1
                    END
                CASE dateOperant EQ 'NE' ;* operand is not equals
                    IF deletedRecord<V-5>[1,6] NE actualDate THEN ;* fetch all the record with other than the date give in selection
                        includeRec = 1
                    END

                CASE dateOperant EQ 'LT' ;* operand is less than
                    IF deletedRecord<V-5>[1,6] LT actualDate[1,6] THEN  ;* fetch all the record with lesser than the date give in selection
                        includeRec = 1
                    END

                CASE dateOperant EQ 'GT' ;* operand is greater than
                    IF deletedRecord<V-5>[1,6] GT actualDate[1,6] THEN ;* fetch all the record with greater than the date give in selection
                        includeRec = 1
                    END

                CASE dateOperant EQ 'LE' ;* operand is less and equalsthan
                    IF deletedRecord<V-5>[1,6] LE actualDate[1,6] THEN ;* fetch all the record with lesser than and equals the date give in selection
                        includeRec = 1
                    END
                CASE dateOperant EQ 'GE' ;* operand is greater than and equals
                    IF deletedRecord<V-5>[1,6] GE actualDate[1,6] THEN ;* fetch all the record with greater than and equals the date give in selection
                        includeRec = 1
                    END

                CASE OTHERWISE ;* for other operands add all the records
                    includeRec = 1
            END CASE
        END ELSE
            includeRec = 1 ;* DATE not in selection criteria than include all the records
        END

        IF includeRec THEN ;* include records to display
            recStatus  = deletedRecord<V-8>       ;*STATUS
            currNo     = deletedRecord<V-7>       ;*CURR.NO
            inputter   = deletedRecord<V-6>       ;*INPUTTER
            inputter   = inputter<1,1>  ;* Latest user details need to be displayed
            dateTime   = FIELD(delRecId,';',2)    ;* DATE.TIME from record id
            authoriser = deletedRecord<V-4>       ;*AUTHORISER
            companyId  = deletedRecord<V-3>       ;* COMPANY.CODE
            ENQ.DETAILS<-1>=delRecId:'*':recStatus:'*':currNo:'*':inputter:'*':authoriser:'*':dateTime:'*':companyId    ;* OUT VALUES

        END
    REPEAT

    RETURN


*
*** </region>
*-------------------------------------------------------------------------------
*** <region name= openFile>
*** <desc>OPens the TV.TRANS.INDEX file </desc>
openFile:
*-----------
* Opens the file

    CALL GET.STANDARD.SELECTION.DETS( applicationName, recApplss ) ;* read SS record
    CALL FIELD.NAMES.TO.NUMBERS('AUDIT.DATE.TIME',recApplss,fieldPos,'','','','',notaField) ;* get the last field name

    V=fieldPos

    RETURN
*
*** </region>
*----------------------------------------------------------------------------------
*** <region name= selectOperand>
selectOperand:
*** <desc>support all the operands </desc>

    BEGIN CASE

        CASE operantval = "CT" OR operantval = "BW" OR operantval = "EW" OR operantval = "LK"  ;* append ... and say actual operand is LIKE
            GOSUB appendWildCard
            operantval = "LIKE" ;* select stmt should run with LIKE

        CASE operantval = "NC" OR operantval = "DNBW" OR operantval = "DNEW"  OR operantval = "UL"    ;* append ... and say actual operand is UNLIKE
            GOSUB appendWildCard  ;* BG_100015460 S E
            operantval = "UNLIKE" ;* select stmt should run with UNLIKE

        CASE operantval = "EQ"
            enqValue = enqValue:'...' ;* APPEND ..., Since  audit DATE.TIME will have time also with it so we must separate date and append ... during selection
            operantval = 'LIKE' ;* select stmt should run with LIKE

        CASE operantval = "NE"
            enqValue = enqValue:'...' ;* APPEND ..., Since  audit DATE.TIME will have time also with it so we must separate date and append ... during selection
            operantval = 'UNLIKE' ;* select stmt should run with UNLIKE

    END CASE

    RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= appendWildCard>
*** <desc>Change to data to append or prepend a wildcard</desc>

appendWildCard:

    BEGIN CASE

        CASE operantval = "CT" OR operantval = "NC" ;* contains and not contains
            * don't do anything if user has already put a wildcard in
            IF NOT(INDEX(enqValue,'...',1)) THEN ;* already ... added then dont add during run time
                enqValue =  '...':enqValue:'...'
            END

        CASE operantval = "BW" OR operantval = "DNBW"
            IF NOT(INDEX(enqValue,'...',1)) THEN ;* already ... added then dont add during run time
                enqValue =  enqValue:'...' ;* add ... after the actual value
            END

        CASE operantval = "EW" OR operantval = "DNEW"
            IF NOT(INDEX(enqValue,'...',1)) THEN ;* already ... added then dont add during run time
                enqValue =  '...':enqValue ;* add ... before the actual value
            END

    END CASE

    RETURN
*** </region>
*-----------------------------------------------------------------------------

*** <region name= formSelection>
formSelection:
*** <desc>Form selection creteria based on the SELECTION criteria </desc>

    LOCATE "APPLICATION.NAME" IN ENQ.SELECTION<2,1> SETTING appPos THEN ;* check APPLICATION.NAME available in SELECTION CRITERIA
      
        applicationName = ENQ.SELECTION<4,appPos> ;* Get the input from the enq selection common variable
        
        delApplicationName= 'F.':applicationName:'$DEL' ;*$DEL file
        
        delApplicationName = delApplicationName:FM:'NO.FATAL.ERROR'

        CALL OPF(delApplicationName,fDelApplicationName) ;* open a $del file 

        IF ETEXT THEN  ;* IF ETEXT finds then dont run the enquiry
            dontRunEnquiry = 1 ;* FILE not available, go out
            RETURN ;* dont continue
        END

        selCmd= 'SELECT ':delApplicationName  ;* SELECT the records from $del file
    END

    LOCATE "DATE" IN ENQ.SELECTION<2,1> SETTING DATE.POS THEN ;* check DATE available in SELECTION CRITERIA
        actualDate = ENQ.SELECTION<4,DATE.POS>    ;* Get the Date
        operantval= ENQ.SELECTION<3,DATE.POS> ;* Get operand
        dateOperant=operantval ;* store tha actual selection date it
        actualDate=actualDate[3,6] ;* get date as like audit fields format

        BEGIN CASE
            CASE dateOperant MATCHES 'LT':VM:'GE'    ;* Append '0000' with date for the operand 'Greater than and equals' and 'Lesser than'
                actualDate=actualDate:'0000'
            CASE dateOperant MATCHES 'LE':VM:'GT' ;* Append '9999' with date for the operand 'Greater than' or 'Lesser than and equals' date
                actualDate=actualDate:'9999'
        END CASE

        enqValue=actualDate ;* assign to get with wildcard char
        GOSUB selectOperand   ;*support all the operands
        selCmd = selCmd: ' WITH DATE.TIME ':operantval:' ':enqValue
    END

    LOCATE "TRANSACTION.REF" IN ENQ.SELECTION<2,1> SETTING TRANS.POS THEN

        enqValue = ''
        operantval = ''
        transRef = ENQ.SELECTION<4,TRANS.POS>
        operantval= ENQ.SELECTION<3,TRANS.POS>
        enqValue=transRef
        GOSUB selectOperand   ;*support all the operands
        transRef = enqValue
        selCmd = selCmd:' AND WITH @ID ':operantval:' ': transRef
    END

    CALL EB.READLIST(selCmd,theList,'',NO.OF.ITEMS,'')

    RETURN
*** </region>

    END



