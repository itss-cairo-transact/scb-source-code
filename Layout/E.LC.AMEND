* Version 3 06/06/01  GLOBUS Release No. G12.0.00 29/06/01
*-----------------------------------------------------------------------------
* <Rating>-40</Rating>
	$PACKAGE LC.ModelBank
	
    SUBROUTINE E.LC.AMEND
*
* Subroutine to return changes in amount due to amendments.
*
********************************************************************
*
*
* 27/02/07 - BG_100013043
*            CODE.REVIEW changes.
* 07/02/13 - Defect 498863 / Task 583569
*            Since new fields are introduced in LC,positions are modified
*            to display the LC details in enquiry.
*
* 03/09/14 - Task - 1104132
*           New line to be added in the enquiry if LC is fully utilised.
*           Defect - 1092770
*
* 09/12/14 - Task : 1116645 / Enhancement : 990544
* 			 LC Componentization and Incorporation
*
********************************************************************
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    
    $USING LC.ModelBank
    $USING LC.Contract
    $USING EB.DataAccess 
    
*******************************************************************
*
    LC.NUM = FIELD(O.DATA, "*", 1)
    CURR.NO = FIELD(O.DATA, "*", 2)
    O.DATE = ""
    OLD.AMEND.DATE = ""
    AMEND.DATE = ""
    O.DIFF = ""
    O.AMOUNT = ""
    NEW.AMOUNT = ''
    LC.AMOUNT = ''
    LC.UTILISED = ''
    TEMP.LC.AMOUNT = ''
    LC.FULLY.UTIL = ''
    NEW.LC.FULLY.UTIL = ''
    R.LC.HIS = ''
    LC.HIS.ERR = ''
    O.DESC = R.RECORD<400>
    O.CCY = R.RECORD<408>
    O.DR.TYPE = R.RECORD<407>
    O.DATE = R.RECORD<401>
    O.AMOUNT = R.RECORD<402>
    N.DIFF = R.RECORD<402>
    O.DIFF = R.RECORD<403>
    O.TXNREF = R.RECORD<410>
    IF CURR.NO > 1 THEN
        FOR I = 1 TO (CURR.NO - 1)
            HIST.ID = LC.NUM:";":I
            OLD.LC.AMOUNT = LC.AMOUNT
            LC.Contract.LetterOfCreditHis(HIST.ID,R.LC.HIS, LC.HIS.ERR)
            LC.AMOUNT = R.LC.HIS<LC.Contract.LetterOfCredit.TfLcLcAmount>
            LC.FULLY.UTIL = R.LC.HIS<LC.Contract.LetterOfCredit.TfLcFullyUtilised>
            IF LC.AMOUNT EQ '_' THEN
                LC.AMOUNT = OLD.LC.AMOUNT         ;* BG_100013043 - S
            END     ;*BG_100013043  - E
            LC.CURRENCY = R.RECORD<LC.Contract.LetterOfCredit.TfLcLcCurrency>
            IF I = 1 THEN
                INIT.AMOUNT = LC.AMOUNT
            END

            GOSUB CHECK.ON.LC.AMOUNT    ;* BG_100013043 - S / E

        NEXT I

        GOSUB CHECK.ON.NEW.LC.AMOUNT    ;* BG_100013043 - S / E

        IF O.AMOUNT NE "" THEN
*  Now put result in R.RECORD starting position 180
*
            R.RECORD<400> = O.DESC
            R.RECORD<407> = O.DR.TYPE
            R.RECORD<408> = O.CCY
            R.RECORD<401> = O.DATE
            R.RECORD<402> = N.DIFF
            R.RECORD<403> = O.DIFF
            R.RECORD<410> = O.TXNREF
            VM.COUNT = DCOUNT(O.AMOUNT, VM)
        END
    END
    RETURN
**********************************************************************************************************
* BG_100013043 - S
*==================
CHECK.ON.LC.AMOUNT:
*==================
    IF LC.AMOUNT <> INIT.AMOUNT AND LC.AMOUNT NE "_" OR LC.FULLY.UTIL EQ 'Y' THEN
        OLD.AMEND.DATE = AMEND.DATE
        AMEND.DATE = R.LC.HIS<LC.Contract.LetterOfCredit.TfLcAmendmentDte>
        IF AMEND.DATE = "_" THEN
            AMEND.DATE = OLD.AMEND.DATE
        END
        TEMP.LC.AMOUNT = LC.AMOUNT      ;*Before making LC Amount as zero if LC is fully utilised,storing the amount in Temp variable for further calculation.
        IF NOT(LC.UTILISED) AND LC.FULLY.UTIL EQ 'Y' THEN   ;*If LC is fully utilised the outstanding amount should be zero
            LC.AMOUNT = 0
            LC.UTILISED = 1
        END
        AMEND.NO = R.LC.HIS<LC.Contract.LetterOfCredit.TfLcAmendmentNo>
        IF O.DESC EQ "" THEN
            O.DESC = "AMEND"
            O.CCY = LC.CURRENCY
            O.DR.TYPE = "-"
            O.DATE = AMEND.DATE
            O.AMOUNT = LC.AMOUNT
            DIFF = LC.AMOUNT - INIT.AMOUNT
            N.DIFF = DIFF
            O.DIFF = AMEND.NO
            INIT.AMOUNT = TEMP.LC.AMOUNT
        END ELSE
            O.CCY = O.CCY:VM:LC.CURRENCY
            O.DESC = O.DESC:VM:"AMEND"
            O.DR.TYPE = O.DR.TYPE:VM:"-"
            O.DATE = O.DATE:VM:AMEND.DATE
            O.AMOUNT = O.AMOUNT:VM:LC.AMOUNT
            DIFF = LC.AMOUNT - INIT.AMOUNT
            N.DIFF = N.DIFF:VM:DIFF
            O.DIFF = O.DIFF:VM:AMEND.NO
            INIT.AMOUNT = TEMP.LC.AMOUNT
            O.TXNREF = O.TXNREF:VM:HIST.ID
        END
    END
    RETURN
**********************************************************************************************************
*======================
CHECK.ON.NEW.LC.AMOUNT:
*======================
*** Check to see if current record has changed too.
    NEW.AMOUNT = R.RECORD<LC.Contract.LetterOfCredit.TfLcLcAmount>
    NEW.CCY = R.RECORD<LC.Contract.LetterOfCredit.TfLcLcCurrency>
    NEW.AMEND.NO = R.RECORD<LC.Contract.LetterOfCredit.TfLcAmendmentNo>
    NEW.LC.FULLY.UTIL = R.RECORD<LC.Contract.LetterOfCredit.TfLcFullyUtilised>
    IF NEW.AMOUNT <> TEMP.LC.AMOUNT OR NEW.LC.FULLY.UTIL EQ 'Y' THEN
        IF NOT(LC.UTILISED) AND NEW.LC.FULLY.UTIL EQ 'Y' THEN
            NEW.AMOUNT = "-" : NEW.AMOUNT
        END
        IF O.AMOUNT EQ "" THEN
            O.DESC = "AMEND"
            O.DR.TYPE = "-"
            O.AMOUNT = NEW.AMOUNT
            O.CCY = NEW.CCY
            O.DATE = R.RECORD<LC.Contract.LetterOfCredit.TfLcAmendmentDte>
            DIFF = NEW.AMOUNT - TEMP.LC.AMOUNT
            N.DIFF = DIFF
            O.DIFF = NEW.AMEND.NO
        END ELSE
            O.DESC = O.DESC:VM:"AMEND"
            O.DR.TYPE = O.DR.TYPE:VM:"-"
            O.CCY = O.CCY:VM:NEW.CCY
            O.DATE = O.DATE:VM:R.RECORD<LC.Contract.LetterOfCredit.TfLcAmendmentDte>
            O.AMOUNT = O.AMOUNT:VM:NEW.AMOUNT
            IF NEW.LC.FULLY.UTIL EQ 'Y' THEN      ;*If fully utilised is set as 'Y' then calculate the difference between drawings and lc amount
                DIFF = NEW.AMOUNT + R.RECORD<LC.Contract.LetterOfCredit.TfLcDrawings>
            END ELSE
                DIFF = NEW.AMOUNT - LC.AMOUNT
            END
            N.DIFF = N.DIFF:VM:DIFF
            O.DIFF = O.DIFF:VM:NEW.AMEND.NO
            O.TXNREF = O.TXNREF:VM:ID
        END
    END

    RETURN          ;* BG_100013043 - E
**********************************************************************************************************
END

