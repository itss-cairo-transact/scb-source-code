*-----------------------------------------------------------------------------
* <Rating>-55</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.FX.SF.SWAP.BUILD(ENQUIRY.DATA)

* Build routine attached to the enquiry FX.SF.SWAP to update FX.SF.SWAP file
* The routine will do the following:
*     Clear the  contents of file FX.SF.SWAP
*     Select FOREX Swap contracts with REVALUATION.TYPE = 'SF' and STATUS NE 'MAT'
*     calls DAS.FOREX routine to achieve the above selection.
*     The contents from FOREX are written in to FX.SF.SWAP in the related fields for all selected contracts
*
* Modifications:
* -------------
* 07/06/07 - EN_10003396
*            I-Descriptor Cleanup
*

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.FOREX
    $INSERT I_DAS.COMMON
    $INSERT I_DAS.FOREX
    $INSERT I_F.ENQUIRY
    $INSERT I_F.FX.SF.SWAP

    GOSUB INITIALISATION
    GOSUB CLEAR.FX.SF.SWAP


MAIN.PROCESS:

    GOSUB EXECUTE.DAS

* Contents are written to FX.SF.SWAP
    ID.FX = ''
    LOOP
        REMOVE ID.FX FROM FX.LIST SETTING FX.POS.NEW
    WHILE ID.FX:FX.POS.NEW
        GOSUB GET.FOREX.DETAILS
        R.FX.SF.SWAP<FX.SF.SWAP.OTHER.CCY> = SW.NON.BASE.CCY
        R.FX.SF.SWAP<FX.SF.SWAP.PL.VALUE.DATE> = SWAP.PL.VALUE.DATE
        R.FX.SF.SWAP<FX.SF.SPOT.DATE> = R.FOREX<FX.SPOT.DATE>
        R.FX.SF.SWAP<FX.SF.TOT.INT.BOUGHT> = R.FOREX<FX.TOTAL.INT.BOUGHT>
        R.FX.SF.SWAP<FX.SF.BUY.DAILY.ACC.F> = R.FOREX<FX.BUY.DAILY.ACC.F>
        R.FX.SF.SWAP<FX.SF.BUY.ACC.TDATE.F> = R.FOREX<FX.BUY.ACC.TDATE.F>
        R.FX.SF.SWAP<FX.SF.TOT.INT.SOLD> = R.FOREX<FX.TOTAL.INT.SOLD>
        R.FX.SF.SWAP<FX.SF.SEL.DAILY.ACC.F> = R.FOREX<FX.SEL.DAILY.ACC.F>
        R.FX.SF.SWAP<FX.SF.SEL.ACC.TDATE.F> = R.FOREX<FX.SEL.ACC.TDATE.F>
        R.FX.SF.SWAP<FX.SF.ACCOUNT.OFFICER> = R.FOREX<FX.ACCOUNT.OFFICER>
        SWAP.REF.NO = R.FOREX<FX.SWAP.REF.NO>
        CONVERT VM TO '*' IN SWAP.REF.NO
        R.FX.SF.SWAP<FX.SF.SWAP.REF.NO> = SWAP.REF.NO

        CALL F.WRITE(FN.FX.SF.SWAP,ID.FX,R.FX.SF.SWAP)
        CALL JOURNAL.UPDATE('FX.SF.SWAP:':ID.FX)

    REPEAT
    RETURN
*-----------------------------------------------------------------------------
GET.FOREX.DETAILS:

    READ R.FOREX FROM F.FOREX,ID.FX ELSE
        R.FOREX = ''
    END
    SWAP.BASE.CCY = R.FOREX<FX.SWAP.BASE.CCY>
    CURRENCY.BOUGHT = R.FOREX<FX.CURRENCY.BOUGHT>
    CURRENCY.SOLD = R.FOREX<FX.CURRENCY.SOLD>
    VALUE.DATE.BUY = R.FOREX<FX.VALUE.DATE.BUY>
    VALUE.DATE.SELL = R.FOREX<FX.VALUE.DATE.SELL>
* extracts the SWAP non base currency and its value date
    IF SWAP.BASE.CCY EQ '' THEN
        SW.NON.BASE.CCY = ''
        SWAP.PL.VALUE.DATE = ''
    END ELSE
        IF SWAP.BASE.CCY EQ CURRENCY.BOUGHT THEN
            SW.NON.BASE.CCY = CURRENCY.SOLD
            SWAP.PL.VALUE.DATE = VALUE.DATE.SELL
        END ELSE
            SW.NON.BASE.CCY = CURRENCY.BOUGHT
            SWAP.PL.VALUE.DATE = VALUE.DATE.BUY
        END
    END
    RETURN
*--------------------------------------------------------------------------------
INITIALISATION:

    FN.FOREX = 'F.FOREX'
    F.FOREX = ""
    CALL OPF(FN.FOREX,F.FOREX)

    FN.FX.SF.SWAP = "F.FX.SF.SWAP"
    F.FX.SF.SWAP = ''
    CALL OPF(FN.FX.SF.SWAP,F.FX.SF.SWAP)

    THE.ARGS = ''
    TABLE.SUFFIX = ''
    ID.FX = ''
    ACCOUNT.OFFICER = ''
    SWAP.OTHER.CCY = ''
    R.FX.SF.SWAP = ''
    R.FOREX = ''
    FX.LIST.NEW = ''
    FX.POS.NEW = ''
    FX.SF.POS = ''
    FX.SF.SWAP.LIST = ''

    RETURN
*------------------------------------------------------------------------------
CLEAR.FX.SF.SWAP:

    FX.SF.SWAP.LIST = dasAllIds
    CALL DAS('FX.SF.SWAP',FX.SF.SWAP.LIST,THE.ARGS,TABLE.SUFFIX)

    FX.SF.SWAP.ID = ''
    LOOP
        REMOVE FX.SF.SWAP.ID FROM FX.SF.SWAP.LIST SETTING FX.SF.POS
    WHILE FX.SF.SWAP.ID:FX.SF.POS
        CALL F.DELETE(FN.FX.SF.SWAP,FX.SF.SWAP.ID)
    REPEAT
    RETURN
*----------------------------------------------------------------------------
EXECUTE.DAS:

* Selects Forex With Revaluation type eq 'SF' and STATUS ne 'MAT'
    THE.ARGS = ''
    FX.LIST = dasForexWithRevalTypeAndStatusCondition
    CALL DAS('FOREX',FX.LIST,THE.ARGS,TABLE.SUFFIX)
    RETURN
*---------------------------------------------------------------------------
END

