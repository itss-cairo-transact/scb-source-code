*-----------------------------------------------------------------------------
* <Rating>3785</Rating>
*-----------------------------------------------------------------------------
* Version 20 06/06/01  GLOBUS Release No. G12.0.00 29/06/01
* Version 9.1.0A released on 29/09/89
    SUBROUTINE E.LIM.ENQ.REC.BUILD
*
**** =============================================================== ****
**** Formats R.RECORD for Limit enquiry. Record is either read from  ****
**** the LIMIT file or ACCOUNT file.                                 ****
**** Key to be used for reading the files is passed in O.DATA by the ****
**** Enquiry system. The key to account records has 'AC\Customer No  ****
**** Concatenated in front of the account number. More than one a/c  ****
**** nos may be sent in O.DATA. A/c Nos are separated by '.'.        ****
**** Consolidated balances of all a/c nos passed to be set up in     ****
**** R.RECORD.                                                       ****
**** The key to limit records are passed one at a time.              ****
**** =============================================================== ****
*
*************************************************************************
* Modification Log:
* =================
*
* 12/03/93 - GB9300367
*            Allow for deposit and fid limits to be any reference no
*
* 09/05/96 - GB9600597 & GB9600900
*            Show value of securities holdings for the customer
*
* 23/01/98 - GB9800042
*            Check field ALLOW.NETTING when calculating working balance.
*
*
* 01/09/98 - GB9801068
*            SC.CALC.CONSID has two more parameters added to it.
*
* 09/01/03 - EN_10001552
*          - Removed the changes done with regard to LIMIT.REVAL.OS,
*          - cause this is handled by the new routine LIMIT.RATE.REVAL
*          - Also included the LOWEST.LIM.REF as the last parameter
*          - of LIMIT.CURR.CONV.
*
* 28/02/03 - BG_100003410
*          - When UPD.RATE in LIMIT.PARAMETER is set to YES, then
*          - TOTAL.OS needs to display the converted amt. This change
*          - is pertaining to the enchancement done in - EN_10001552.
*
*18/11/03  - CI_10014930
*            For Nostro Account YLIM.REF was getting Non Nummerid Value
*            Assigned because of which LIMIT.CURR.CONV was called with
*            wrong value And resulting in giving the error Non Numeric Data
*
* 03/03/04 - GLOBUS_CI_10017872
*            Fix to avoid LIMIT Enquiry Fataling out due to SMS Restrictions
*
* 21/12/04 - EN_10002382
*            Securities Phase I non stop processing.
*
* 24/10/05 - CI_10036017
*            The selection routine E.LIM.LIAB.SELECTION returns the list of
*            accounts with its lead company mnemonic. So while storing the accounts
*            in the corresponding company list for drill down, check the key account
*            mnemonic with the current company's financial company mnemonic.
*
* 20/03/06 - EN_10002868
*            Bond Pricing Calculation - Fixed
*
* 22/06/07 - BG_100014293
*            While opneing limit account with compnay code, in case of branch
*            company do OPF with financial company mnemonic.
*
* 07/11/08 - CI_10058746
*            In the para PROCESS.LIMIT.ACCOUNTS,
*            When there are no accounts attached to the limit for a customer, then LOCAL7
*            variable will hold null value. While populating R.RECORD<9>, R.RECORD<10> a check
*            is done for the value in LOCAL7. Hence no value is stored in R.RECORD<9> and
*            R.RECORD<10> in this case. So while drilling down, displays the error message.
*
* 27/07/09 - GLOBUS_CI_10064862
*            To calculate the Available Amount based upon the Account Parameter setup
*
* 09/09/09 - CI_10065967
*            Updated display of enquiry output.
*
* 07/10/09 - CI_10066608
*            Updation of TIME.CODE field number in R.RECORD
*            Replaced the old field number 17 to 30 which is the field number of TIME.CODE in LIMIT
*
* 14/05/10 - DEFECT 12313 / TASK 49127
*            Decrement OFS$ENQ.KEYS whenever an account limit is removed or
*            deleted from ENQ.KEYS to display a correct record count in browser display.
*
* 06/09/10 - ENHANCEMENT - 34396 - SAR-2009-12-17-0001
*            Introducing New Price type - 'COL.YIELD�
*
*
* 23/11/10 - T 110525 // D 91269
*            Changes done to call LIMIT.REVAL.OS when accounts are attached to limit
*            even though UPDATE.RATE field in the LIMIT.PARAMETER is set to 'YES'.
*
* 15/02/2011 - DEFECT 36446 / TASK 74363
*              Changes done to show WORKING.BALANCE for ACCOUNT instead of AVAILABLE.BAL
*
* 30/05/2011 - Task 218798
*			   Changes done for showing the correct outstanding amount when ALLOW.NETTING is
*			   set in ACCOUNT and LIMIT record
*
* 14/07/11 -   Task 238249 / Defect 237799
*              Reverting the changes done through DEFECT 36446 / TASK 74363.
*              Enquiry should display balances according to the CREDIT.CHECK field.
*
* 23/05/11 - Enhancement - 182581 / Task- 191536
*            Moving Balances to ECB from Account Balance Fields.
*
* 01/01/13 - Enhancement - 450817 / Task 486148
*            Changes done to get the account utilisation details from new work file
*			 LI.LIMIT.GROUP.ALLOCATION for the account whose customer has group limit setup.
*
* 13/02/13 - Defect 585905 / Task 450894
*            Group Limit Changes, for Liab enquiry
*
* 02/04/13 - Defect 638332 / Task 638389
*            For group limits set the drill down enq as LIM.CUST and pass the LINE.ID as the
*            first three parts of group limit id.
*
* 06/02/2014 - Defect 902096 / Task 907708
* 			   The working balance of account is set in variable workingBal before checking the balance.
*
* 11/06/14 - Defect 986559 / Task 1024288
*            Changes has been done such that even for the sub group limits, the description is
*            displayed from the LIMIT.SHARING.GROUP
*
* 30/06/14 - Enhancement 930605 / Task 930608
*            Consider TDGL movements while fetching balance for all accounts based on
*            the parameter setup, that is fetched from API GET.CREDIT.CHECK.
*
* 16/07/14 - Defect 986559 / Task 1059394
*	         Changes has been done such that the utilisation is shown correctly
* 			 even for the sub group limits
*
* 06/11/14 - Enhancement 608555 / Task 608562
*            AC.BALANCE.TYPE set to exclude credit check process will not utilise the limits
*            so in order to get the account balance attached to limit call the core API
*            GET.WORKING.AVAIL.BALANCE instead of directly taking working balance from account.
*            Also the LIMIT CR ACCOUNT balance is not displayed correctly.
*            Since the variable YACC.AMT is not added with Y.LIMIT.CREDIT enquiry
*            failed to display the credit amount correctly.
*
* 11/11/14 - Task 1165382
*            Removed the addition of TDGL balance. Since this is done in GET.WORKING.AVAIL.BAL
*            routine.
***********************************************************************************
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
*
    $INSERT I_ENQUIRY.COMMON
*
    $INSERT I_F.LIMIT
    $INSERT I_F.ACCOUNT
    $INSERT I_F.LIMIT.REFERENCE
    $INSERT I_F.COMPANY
    $INSERT I_F.SECURITY.MASTER
    $INSERT I_F.SECURITY.POSITION
    $INSERT I_F.PRICE.TYPE
    $INSERT I_F.COLLATERAL
    $INSERT I_F.LETTER.OF.CREDIT
    $INSERT I_F.DRAWINGS
    $INSERT I_F.LIMIT.PARAMETER         ;* BG_100003410
    $INSERT I_AccountService_WorkingBalance
    $INSERT I_F.LIMIT.SHARING.GROUP
    $INSERT I_AccountService_TradeDatedGLBal
*
**** =============================================================== ****
MAIN.PARA:
*--------
*
**** Open all files..
*
    GOSUB OPEN.REQD.FILES:
*
*** Main process.
*
    DIM MLIM.REC(LI.AUDIT.DATE.TIME)
    DIM MACC.REC(AC.AUDIT.DATE.TIME)
    R.RECORD = ''
    VM.COUNT = 1
    GOSUB PROCESS.PARA:
*
*** Return to enquiry
*
    RETURN
*
**** =============================================================== ****
OPEN.REQD.FILES:
*--------------
*
    F.LIMIT = ''
    CALL OPF('F.LIMIT',F.LIMIT)
*
    F.LIMIT.LINES = ''
    CALL OPF('F.LIMIT.LINES',F.LIMIT.LINES)
*
    F.ACCOUNT = ""
    CALL OPF("F.ACCOUNT",F.ACCOUNT)
*
    LOCATE "SC" IN R.COMPANY(EB.COM.APPLICATIONS)<1,1> SETTING SC.POS THEN
        F.SECURITY.MASTER = ""
        CALL OPF("F.SECURITY.MASTER", F.SECURITY.MASTER)
    END
*
    LOCATE "CO" IN R.COMPANY(EB.COM.APPLICATIONS)<1,1> SETTING CO.POS THEN
        F.COLLATERAL = ""
        CALL OPF("F.COLLATERAL", F.COLLATERAL)
    END
*
    LOCATE "LC" IN R.COMPANY(EB.COM.APPLICATIONS)<1,1> SETTING LC.POS THEN
        F.LETTER.OF.CREDIT = ""
        CALL OPF("F.LETTER.OF.CREDIT",F.LETTER.OF.CREDIT)
        F.LETTER.OF.CREDIT$NAU = ""
        CALL OPF("F.LETTER.OF.CREDIT$NAU",F.LETTER.OF.CREDIT)
        F.DRAWINGS = ""
        CALL OPF("F.DRAWINGS",F.DRAWINGS)
        F.DRAWINGS$NAU = ""
        CALL OPF("F.DRAWINGS$NAU",F.DRAWINGS)
    END
*
    R.LIMIT.PARAMETER = ""
    YERR = ""
    CALL CACHE.READ('F.LIMIT.PARAMETER','SYSTEM',R.LIMIT.PARAMETER,YERR)         ;* BG_100003410s/e
*
    RETURN
*
**** =============================================================== ****
PROCESS.PARA:
*-----------
*
    YKEY = O.DATA
    BEGIN CASE
        CASE FIELD(YKEY,'\',1) = 'AC'
            *
**** String of account numbers passed.
            *
            GOSUB PROCESS.ACCOUNTS:
            *
        CASE FIELD(YKEY,"\",1) = "SC"
            GOSUB PROCESS.SECURITIES
            *
        CASE FIELD(YKEY,"\",1) = "CO"
            GOSUB PROCESS.COLLATERAL
            *
        CASE FIELD(YKEY,'\',1) = 'LI'
            GOSUB PROCESS.LIMIT.ACCOUNTS:
            *
        CASE FIELD(YKEY,'\',1) = 'LC'
            GOSUB PROCESS.LC
            *
        CASE 1
            *
**** Limit key identified.
            *
            GOSUB PROCESS.LIMITS:
    END CASE
*
    RETURN
*
**** =============================================================== ***
PROCESS.LC:
*----------
*
    R.RECORD = ''
    Y.LC.LIMIT = 0
    Y.LC.CUST = FIELD(YKEY,'\',2)
    Y.LC.LIST = FIELD(YKEY,'\',3)
    CONVERT "*" TO FM IN Y.LC.LIST
    LOOP
        REMOVE LC.NO FROM Y.LC.LIST SETTING MORE
    WHILE MORE:LC.NO
        IF LEN(LC.NO) = 12 THEN         ;! It is a LC record
        LC.READ.ERR = ''
        LC.REC = ''
        CALL F.READ('F.LETTER.OF.CREDIT$NAU',LC.NO,LC.REC,F.LETTER.OF.CREDIT$NAU,LC.READ.ERR)
        IF LC.READ.ERR # '' THEN
            CALL F.READ('F.LETTER.OF.CREDIT',LC.NO,LC.REC,F.LETTER.OF.CREDIT,LC.READ.ERR)
        END
        IF LC.REC # '' THEN
            YCCY.AMNT = LC.REC<TF.LC.LIABILITY.AMT>
            YCONV.CURRENCY = LC.REC<TF.LC.LC.CURRENCY>
            YLIM.REF = LC.REC<TF.LC.LIMIT.REFERENCE>
            GOSUB CONV.AMOUNTS
        END
    END ELSE    ;! It is a DR record
        DR.READ.ERR = ''
        DR.REC = '' ; DR.NO = LC.NO
        CALL F.READ('F.DRAWINGS$NAU',DR.NO,DR.REC,F.DRAWINGS$NAU,DR.READ.ERR)
        IF DR.READ.ERR # '' THEN
            CALL F.READ('F.DRAWINGS',DR.NO,DR.REC,F.DRAWINGS,DR.READ.ERR)
        END
        IF DR.REC # '' THEN
            YCCY.AMNT = DR.REC<TF.DR.DOCUMENT.AMOUNT>
            YCONV.CURRENCY = DR.REC<TF.DR.DRAW.CURRENCY>
            YLIM.REF = DR.REC<TF.DR.LIMIT.REFERENCE>
            GOSUB CONV.AMOUNTS
        END
    END
    Y.LC.LIMIT = Y.LC.LIMIT + YCCY.AMNT
    REPEAT
    YTEXT = 'LC BENEFICIARY'
    CALL TXT(YTEXT)
    R.RECORD<1> = Y.LC.CUST
    R.RECORD<2> = YTEXT
    R.RECORD<4> = LCCY
    R.RECORD<6> = Y.LC.LIMIT/1000
    R.RECORD<9> = 'LC.EXPORT'
    R.RECORD<10> = 'LC.BENEFICIARY EQ ':Y.LC.CUST
    RETURN

**** =============================================================== ****
PROCESS.ACCOUNTS:
*---------------
*
**** --------------------------------------------------------------- ****
**** Multi valued field. The first contains "AC" followed by a "\"   ****
**** Each subsequent multi value hold the company mnemonic followed  ****
**** by a "\" then the account numbers under that company separated  ****
**** by a ".".
**** eq. AC\1234vmECO\19.27vmCO2\35                                  ****
**** --------------------------------------------------------------- ****
*
    R.RECORD = ''
    Y.LIMIT.CREDIT = 0
    Y.NON.LIMIT.CREDIT = 0
    Y.NON.LIMIT.DEBIT = 0
    CONVERT "|" TO VM IN YKEY
    REMOVE YKEY.VALUE FROM YKEY SETTING YKEY.DELIM          ;* Get rid of the liab no
    Y.LIAB.NO = FIELD(YKEY.VALUE,'\',2)
    Y.LIM.AC = '' ; Y.LIM.AC.THIS.CO = ""
    Y.NON.LIM.AC = '' ; Y.NON.LIM.AC.THIS.CO = ""
*
    LOOP
        REMOVE YKEY.VALUE FROM YKEY SETTING YKEY.DELIM
    UNTIL YKEY.VALUE = ""
        Y.ACCT.LIST = FIELD(YKEY.VALUE,"\",2)
        *
        CONVERT '.' TO FM IN Y.ACCT.LIST
        YACCT.MNE = FIELD(YKEY.VALUE,"\",1)
        *
        LOOP
            REMOVE Y.ACC.NO FROM Y.ACCT.LIST SETTING YINDEX.1
        WHILE Y.ACC.NO
            ACC.READ.ERR = "" ; YR.ACCOUNT = ""
            CALL F.READ('F.ACCOUNT',Y.ACC.NO, YR.ACCOUNT, F.ACCOUNT, ACC.READ.ERR)
            *
            * Only add the balances if the LIMIT.REF is not numeric as these have
            * already been taken into account
            *
            * CI_10014930 Commented this line instead YLIM.REF is assigned a value inside
            * the IF/ELSE statment
            *            YLIM.REF = YR.ACCOUNT<AC.LIMIT.REF>     ;* CI_10014930 S/E
            accountKey = Y.ACC.NO
            GOSUB GET.WORKING.BALANCE
            IF NOT(YR.ACCOUNT<AC.LIMIT.REF> MATCHES "1N0X") THEN
                *
                YCCY.AMNT =  workingBal<Balance.workingBal>
                YCONV.CURRENCY = YR.ACCOUNT<AC.CURRENCY>
                YLIM.REF = '' ;* CI_10014930 S/E     Assigned '' b'cos AC.LIMIT.REF holds NOSTRO
                GOSUB CONV.AMOUNTS:

                IF workingBal<Balance.workingBal> GT 0 THEN
                    Y.NON.LIMIT.CREDIT = Y.NON.LIMIT.CREDIT + YCCY.AMNT
                END ELSE
                    Y.NON.LIMIT.DEBIT = Y.NON.LIMIT.DEBIT + YCCY.AMNT
                END
                *
                * Store accounts in this company separately so that they can be processed
                * first by the next enquiry down, otherwise the user will never be able
                * to see accounts in another company. This way he will be able tio see the
                * accounts of the company he is signed on to.
                *
                IF YACCT.MNE = R.COMPANY(EB.COM.FINANCIAL.MNE) THEN   ;* CI_10036017 - S/E
                    YSEARCH.ACC = Y.ACC.NO:"\":YACCT.MNE
                    LOCATE YSEARCH.ACC IN Y.NON.LIM.AC.THIS.CO<1,1,1> BY 'AL' SETTING Y.AC.POS ELSE
                        INS YSEARCH.ACC BEFORE Y.NON.LIM.AC.THIS.CO<1,1,Y.AC.POS>
                    END
                END ELSE
                    YSEARCH.ACC = Y.ACC.NO:"\":YACCT.MNE
                    LOCATE YSEARCH.ACC IN Y.NON.LIM.AC<1,1,1> BY 'AL' SETTING Y.AC.POS ELSE
                        INS YSEARCH.ACC BEFORE Y.NON.LIM.AC<1,1,Y.AC.POS>
                    END
                END
                *
            END ELSE
                YLIM.REF = YR.ACCOUNT<AC.LIMIT.REF>         ;* CI_10014930 S/E  Assign AC.LIMIT.REF to YLIM.REF as it holds a valid limit reference id
                IF workingBal<Balance.workingBal> GT 0 THEN
                    YCCY.AMNT = workingBal<Balance.workingBal>
                    YCONV.CURRENCY = YR.ACCOUNT<AC.CURRENCY>
                    GOSUB CONV.AMOUNTS
                    Y.LIMIT.CREDIT = Y.LIMIT.CREDIT + YCCY.AMNT
                    *
                    * Store accounts in this company separately so that they can be processed
                    * first by the next enquiry down, otherwise the user will never be able
                    * to see accounts in another company. This way he will be able tio see the
                    * accounts of the company he is signed on to.
                    *
                    IF YACCT.MNE = R.COMPANY(EB.COM.FINANCIAL.MNE) THEN         ;* CI_10036017 - S/E
                        YSEARCH.ACC = Y.ACC.NO:"\":YACCT.MNE
                        LOCATE YSEARCH.ACC IN Y.LIM.AC.THIS.CO<1,1,1> BY 'AL' SETTING Y.AC.POS ELSE
                            INS YSEARCH.ACC BEFORE Y.LIM.AC.THIS.CO<1,1,Y.AC.POS>
                        END
                    END ELSE
                        YSEARCH.ACC = Y.ACC.NO:"\":YACCT.MNE
                        LOCATE YSEARCH.ACC IN Y.LIM.AC<1,1,1> BY 'AL' SETTING Y.AC.POS ELSE
                            INS YSEARCH.ACC BEFORE Y.LIM.AC<1,1,Y.AC.POS>
                        END
                    END
                END
            END

        REPEAT
        *
    REPEAT
*
* If there are accounts for this company insert them before the other
* account list.
*
    IF Y.LIM.AC OR Y.LIM.AC.THIS.CO THEN
        Y.LIM.AC = Y.LIM.AC.THIS.CO:SM:Y.LIM.AC
    END
    IF Y.NON.LIM.AC OR Y.NON.LIM.AC.THIS.CO THEN
        Y.NON.LIM.AC = Y.NON.LIM.AC.THIS.CO:SM:Y.NON.LIM.AC
    END
*
    LOCAL7 = ''
    LOCAL7<1> = Y.LIMIT.CREDIT
    LOCAL7<2> = Y.LIM.AC

    CUST.ID = ''
    CUST.ID = FIELD(Y.LIAB.NO,VM,1)

* Print accounts without limit  if they exist else print limit accounts and remove trigger to
* print accounts with limit  from ENQ.KEYS as they are printed currently.

    IF Y.NON.LIM.AC NE '' THEN
        R.RECORD<1> = Y.LIAB.NO
        YTEXT = 'CURRENT ACCOUNTS'
        CALL TXT(YTEXT)
        R.RECORD<2> = YTEXT
        R.RECORD<3> = ''
        R.RECORD<4> = LCCY
        R.RECORD<5> = ''
        R.RECORD<6> = Y.NON.LIMIT.DEBIT / 1000
        R.RECORD<7> = Y.NON.LIMIT.CREDIT / 1000
        R.RECORD<9> = 'ACCT.BAL.TODAY'
        CONVERT SM TO ' ' IN Y.NON.LIM.AC
        R.RECORD<10> = 'ACCOUNT.NUMBER EQ ':TRIM(Y.NON.LIM.AC)
    END ELSE
        IF Y.LIM.AC <> '' THEN
            YKEY = "LI\":CUST.ID
            GOSUB PROCESS.LIMIT.ACCOUNTS
            GOSUB UPDATE.ENQ.KEYS
        END
    END
* Remove trigger to display accounts with limit if accounts with limit do not exist.
    IF Y.LIM.AC EQ '' THEN
        GOSUB UPDATE.ENQ.KEYS
    END

    RETURN
*
*-------------------------------------------------------------------------------
GET.WORKING.BALANCE:
*-------------------
* Get the account balance as per the credit check setup

    WORKING.BALANCE = 0
    MAT MACC.REC = ''
    MATPARSE MACC.REC FROM YR.ACCOUNT

* AC.BALANCE.TYPE can be setup to exclude from credit checking process
* such balances will not utilise limits, So call core api to return the
* account balance which utilises limit

    AF.DATE = '' ;* Exclude credit check available
    CALL GET.WORKING.AVAIL.BAL(AF.DATE, accountKey, MAT MACC.REC, '', WORKING.BALANCE,'')
    workingBal = WORKING.BALANCE

    RETURN
*--------------------------------------------------------------------------------
UPDATE.ENQ.KEYS:
*------------------------------------------
*Display of credit accounts is avoided if
*credit accounts do not exist or are already printed.
*
    KEY.ID = ''
    KEY.ID = "LI\":CUST.ID
    LOCATE KEY.ID IN ENQ.KEYS<1> SETTING KEY.POS THEN
        DEL ENQ.KEYS<KEY.POS>
        OFS$ENQ.KEYS -= 1     ;* Decrement a value for browser display of record count
    END

    RETURN

*
**** =============================================================== ****
PROCESS.LIMIT.ACCOUNTS:
*---------------------
*
**** =============================================================== ****
**** Exctracts the cumulative balance for the accounts in credit and ****
**** having a link to limits. This amount is calculated in the prev. ****
**** call when the keys are passed with 'AC\' appended in front.     ****
**** The amounts calculated are stored in common variable LOCAL7.    ****
**** LOCAL7 field 1 contains the balance and field 2 contains the    ****
**** a/c nos matching this condition. The a/c nos are stored as sub  ****
**** values.                                                         ****
**** =============================================================== ****
*
    Y.LI.LIAB.NO = FIELD(YKEY,'.',1)
    Y.LIAB.NO = FIELD(Y.LI.LIAB.NO,'\',2)
    R.RECORD = ''
    Y.LIM.AC = LOCAL7<2>
    IF Y.LIM.AC NE '' THEN
        R.RECORD<1> = Y.LIAB.NO
        YTEXT = 'LIMIT CR ACCOUNTS'
        CALL TXT(YTEXT)
        R.RECORD<2> = YTEXT
        R.RECORD<3> = ''
        R.RECORD<4> = LCCY
        R.RECORD<5> = ""
        R.RECORD<6> = ""
        Y.LIMIT.CREDIT = LOCAL7<1>
        R.RECORD<7> = Y.LIMIT.CREDIT / 1000
        R.RECORD<9> = 'ACCT.BAL.TODAY'
        CONVERT SM TO ' ' IN Y.LIM.AC
        R.RECORD<10> = 'ACCOUNT.NUMBER EQ ':TRIM(Y.LIM.AC)
    END
    RETURN
*
*-------------------------------------------------------------------------
PROCESS.SECURITIES:
*==================
** Multi valued filed The first contains SC followed by "\"
** Each subsequent value holds the company menmonic separated by
** a "\" then the security position ids for that company separated by a "*"
** Amounts will be shown as the local equivalent
*
    CREDIT.SC.AMT = "" ; DEBIT.SC.AMT = ""
    CONVERT "|" TO VM IN YKEY
    REMOVE YKEY.VALUE FROM YKEY SETTING YKEY.DELIM          ;* Get rid of liab no
    LIAB.NO = YKEY.VALUE["\",2,1]       ;* Extract liability
    SC.POSITION.IDS = ""      ;* Store list of security positions
*
    LOOP
        REMOVE YLIST FROM YKEY SETTING YKEY.DELIM
    UNTIL YLIST = ""          ;* List per company
        SC.POS.LIST = YLIST["\",2,1]    ;* Poisition ids
        *
        CONVERT "*" TO FM IN SC.POS.LIST
        YPORT.MNE = YLIST["\",1,1]      ;* Company code
        F.SECURITY.POSITION = ""
        CALL OPF("F.SECURITY.POSITION", F.SECURITY.POSITION)
        *
        LOOP
            REMOVE SC.POS.ID FROM SC.POS.LIST SETTING SCD
        WHILE SC.POS.ID:SCD
            *
            LOCATE SC.POS.ID IN SC.POSITION.IDS<1,1,1> BY "AL" SETTING YSC.ID.POS ELSE
                INS SC.POS.ID BEFORE SC.POSITION.IDS<1,1,YSC.ID.POS>
            END
***            READ YR.SC.POSITION FROM F.SECURITY.POSITION, SC.POS.ID THEN
            SP.ID = SC.POS.ID
            LOCK.RECORD = 0
            PROCESS.MAIN.SP = 0
            GOSUB READ.SEC.POSITION
            YR.SC.POSITION = SP.RECORD
            IF READ.ERROR EQ '' AND YR.SC.POSITION NE '' THEN
                IF YR.SC.POSITION<SC.SCP.CLOSING.BAL.NO.NOM> THEN     ;* Don't bother for zero position
                    SM.ID = YR.SC.POSITION<SC.SCP.SECURITY.NUMBER>    ;* Key to security master
                    *
                    ** Work out the pricing technique from SECURITY.MASTER, and the latest
                    ** price
                    *
                    PRICE = "" ; VALUE.DATE = TODAY ; SC.VALUE = ""
                    READ SM.REC FROM F.SECURITY.MASTER, SM.ID THEN
                        SC.CCY = SM.REC<SC.SCM.SECURITY.CURRENCY>     ;* Price returned in this
                        CALC.METHOD = ""
                        CALL DBR("PRICE.TYPE":FM:SC.PRT.CALCULATION.METHOD, SM.REC<SC.SCM.PRICE.TYPE>, CALC.METHOD)
                        IF CALC.METHOD MATCHES "DISCOUNT":VM:"YIELD":VM:"TYIELD":VM:"COL.YIELD" THEN
                            PRICE = SM.REC<SC.SCM.DISC.YLD.PERC>      ;* Discounted percentage
                        END ELSE
                            PRICE = SM.REC<SC.SCM.LAST.PRICE>
                        END
                        * GB9801068 (Starts)
                        CAP.RATE = "" ; CAP.AMT = ""
                        CALL SC.CALC.CONSID(SM.ID, YR.SC.POSITION<SC.SCP.CLOSING.BAL.NO.NOM>, PRICE, VALUE.DATE, SC.VALUE,CAP.RATE,CAP.AMT,FACTOR)
                        * GB9801068 (Ends)
                        YCCY.AMNT = SC.VALUE ; YCONV.CURRENCY = SC.CCY
                        YLIM.REF = SM.REC<SC.SCM.LIMIT.REF>
                        GOSUB CONV.AMOUNTS
                        IF SC.VALUE GT 0 THEN
                            CREDIT.SC.AMT += YCCY.AMNT
                        END ELSE
                            DEBIT.SC.AMT += YCCY.AMNT
                        END
                    END
                END
            END
            *
        REPEAT
        *
    REPEAT
*
** Return R.RECORD
*
    R.RECORD = ""
    R.RECORD<1> = LIAB.NO
    YTEXT = "SECURITIES HELD" ; CALL TXT(YTEXT)
    R.RECORD<2> = YTEXT
    R.RECORD<3> = ""
    R.RECORD<4> = LCCY
    R.RECORD<5> = ""
    R.RECORD<6> = (CREDIT.SC.AMT - DEBIT.SC.AMT) / 1000     ;* Net position
    R.RECORD<9> = "SC.PORT.HOLD.SUM"    ;* Next level enquiry
    CONVERT SM TO "" IN SC.POSITION.IDS
    R.RECORD<10> = "CUSTOMER.NUMBER EQ ":LIAB.NO  ;* Selection for next enquiry
*
    LOCAL7 = ""     ;* No securities limits at present
*
    RETURN
*
**** =============================================================== ****
PROCESS.COLLATERAL:
*==================
** Process collateral records and extract the nominal value, convert
** to local currency
*
    COLL.AMT = ""
    CONVERT "|" TO VM IN YKEY
    REMOVE YKEY.VALUE FROM YKEY SETTING YKEY.DELIM          ;* Get rid of liab no
    LIAB.NO = YKEY.VALUE["\",2,1]       ;* Extract liability
    COLLATERAL.IDS = ""
*
    LOOP
        REMOVE YLIST FROM YKEY SETTING YKEY.DELIM
    UNTIL YLIST = ""          ;* List per company
        CO.POS.LIST = YLIST["\",2,1]    ;* Poisition ids
        *
        CONVERT "*" TO FM IN CO.POS.LIST
        YCOLL.MNE = YLIST["\",1,1]      ;* Company code
        *
        LOOP
            REMOVE CO.ID FROM CO.POS.LIST SETTING COD
        WHILE CO.ID:COD
            *
            LOCATE CO.ID[".",1,2] IN COLLATERAL.IDS<1,1,1> BY "AL" SETTING COL.POS ELSE
                INS CO.ID[".",1,2] BEFORE COLLATERAL.IDS<1,1,COL.POS>
            END
            READ YR.COLLAT FROM F.COLLATERAL, CO.ID THEN
                YCCY.AMNT = YR.COLLAT<COLL.NOMINAL.VALUE> ; YCONV.CURRENCY = YR.COLLAT<COLL.CURRENCY>
                GOSUB CONV.AMOUNTS
                COLL.AMT += YCCY.AMNT
            END
            *
        REPEAT
        *
    REPEAT
*
** Return R.RECORD
*
    R.RECORD = ""
    R.RECORD<1> = LIAB.NO
    YTEXT = "EXTERNAL COLLATERAL" ; CALL TXT(YTEXT)
    R.RECORD<2> = YTEXT
    R.RECORD<3> = ""
    R.RECORD<4> = LCCY
    R.RECORD<5> = ""
    R.RECORD<6> = COLL.AMT / 1000       ;* Long position
    R.RECORD<9> = "CO.100"    ;* Next level enquiry
    CONVERT SM TO " " IN COLLATERAL.IDS
    R.RECORD<10> = "COLLATERAL.RIGHT EQ ":COLLATERAL.IDS    ;* Selection for next enquiry
*
    LOCAL7 = ""     ;* No securities limits at present
*
    RETURN
*
**************************************************************************
PROCESS.LIMITS:
*-------------
*
**** Build R.RECORD from Limit files.
*
    FN.LIMIT.SHARING.GROUP = "F.LIMIT.SHARING.GROUP"
    F.LIMIT.SHARING.GROUP = ''

    R.RECORD = ''
    Y.LIMIT.ID = YKEY
    READ Y.LIMIT FROM F.LIMIT, Y.LIMIT.ID ELSE
        Y.LIMIT = ''
    END
*
    R.RECORD<1> = FIELD(YKEY,'.',1)
*
**** LIMIT.FIND.REF
*
    Y.LIMIT.REF = FIELD(YKEY,'.',2)
    Y.LIMIT.REF = Y.LIMIT.REF * 1
    IF LEN(Y.LIMIT.REF) > 4 THEN
        YLEN = LEN(Y.LIMIT.REF)
        IF Y.LIMIT.REF[YLEN-3,4] NE "0000" THEN
            Y.LIMIT.REF = Y.LIMIT.REF[YLEN-3,4]
            Y.LIMIT.REF = Y.LIMIT.REF * 1
        END
    END
* For group limits get the description from LIMIT.SHARNG.GROUP

    IF FIELD(YKEY,'.',1)[1,1] = 'M' OR FIELD(YKEY,'.',1)[1,1] = 'S'THEN ;* For the group limits get the description from Limit sharing group
        GROUP.ID = FIELD(YKEY,'.',1)
        R.LIMIT.SHARING.GROUP = ''
        CALL F.READ(FN.LIMIT.SHARING.GROUP, GROUP.ID, R.LIMIT.SHARING.GROUP, F.LIMIT.SHARING.GROUP, ERR)
        Y.REF.DESC = R.LIMIT.SHARING.GROUP<LI.SG.SHORT.DESC>
    END ELSE
        YCHK.FILE = ''
        YCHK.FILE<1> = 'LIMIT.REFERENCE'
        YCHK.FILE<2> = LI.REF.SHORT.NAME
        YCHK.FILE<3> = 'L'
        Y.REF.DESC = ''
        CALL DBR(YCHK.FILE,Y.LIMIT.REF,Y.REF.DESC)
    END

    R.RECORD<2> = Y.REF.DESC
    R.RECORD<3> = FIELD(YKEY,'.',3,1)
    R.RECORD<4> = Y.LIMIT<LI.LIMIT.CURRENCY>
    YCOUNT.AMTS = COUNT(Y.LIMIT<LI.ONLINE.LIMIT>,VM) + 1
*
    FOR Y.VC = 1 TO YCOUNT.AMTS
        GOSUB LIMIT.CALC.COMM:
        GOSUB LIMIT.CALC.OS:
        GOSUB LIMIT.CALC.AVAIL:
    NEXT Y.VC
*
**** Reformat the date.
*
    Y.EXP.DT = Y.LIMIT<LI.EXPIRY.DATE>
    R.RECORD<8> = Y.EXP.DT
    R.RECORD<30> = Y.LIMIT<LI.TIME.CODE>
****
**** Linking to Next enquiry.
****
    VM.COUNT = YCOUNT.AMTS
    GOSUB SET.NEXT.ENQ:
    RETURN
*
**** =============================================================== ****
SET.NEXT.ENQ:
*-----------
*
****  Decide which is to be the next enquiry depending on the limit
****  record id.
*

* For group limits Next drill down enquiry will be LIM.CUST
* Pass the line id as the first three parts of limit id for group limits
* LINE.ID is set as C type in SS and hence it needs to be the if of limit lines file

    CUSTOMER.ID = ''
    IF FIELD(Y.LIMIT.ID,'.',1,1)[1,1] = 'M' THEN
        R.RECORD<9> = 'LIM.CUST'
        R.RECORD<10> = 'LINE.ID EQ ':FIELD(Y.LIMIT.ID,'.',1,3) ;* Pass Group limit id as in LIMIT.LINES
        * When the enquiry is run for LIMIT SHARING GROUP ID Customer part will be null
        * on that case we have to display all the limits of the customer hence set CUST.NO as ALL
        IF FIELD(Y.LIMIT.ID,'.',4,1) THEN ;*
            CUSTOMER.ID = FIELD(Y.LIMIT.ID,'.',4,1)
        END ELSE
            CUSTOMER.ID = 'ALL'
        END

        R.RECORD<11> = "CUST.NO EQ ":CUSTOMER.ID

        RETURN
    END

    YLIM.REF = FIELD(Y.LIMIT.ID,'.',2)
    READ Y.LIMIT.LINES FROM F.LIMIT.LINES, Y.LIMIT.ID ELSE
        NULL
    END
    IF Y.LIMIT.LINES THEN
        Y.FLAG.OVER = ''
        Y.CUST.PRESENT = ''
        LOOP
            REMOVE Y.LIM.FIELD FROM Y.LIMIT.LINES SETTING Y.LIM.LINE.INDEX
        UNTIL Y.LIM.FIELD = '' OR Y.FLAG.OVER = 1
            IF FIELD(Y.LIM.FIELD,'.',2) <> YLIM.REF THEN
                Y.FLAG.OVER = 1
                Y.NEXT.ENQ = 'LIM.TRADE'
            END ELSE
                IF INDEX(Y.LIM.FIELD,'.',3) THEN
                    Y.CUST.PRESENT = 1
                END
            END
        REPEAT
        IF Y.FLAG.OVER THEN
            NULL
        END ELSE
            IF Y.CUST.PRESENT THEN
                Y.NEXT.ENQ = 'LIM.CUST'
            END ELSE
                IF Y.LIMIT<LI.FX.OR.TIME.BAND> = 'FX' THEN
                    Y.NEXT.ENQ = 'LIM.FX1'
                END ELSE
                    Y.NEXT.ENQ = 'LIM.TXN'
                END
            END
        END
        R.RECORD<9> = Y.NEXT.ENQ
        BEGIN CASE
            CASE Y.NEXT.ENQ = 'LIM.TRADE'
                R.RECORD<10> = 'LINE.ID EQ ':Y.LIMIT.ID
                R.RECORD<11> = "CUST.NO EQ ''"
            CASE Y.NEXT.ENQ = 'LIM.CUST'
                R.RECORD<10> = 'LINE.ID EQ ':Y.LIMIT.ID
                R.RECORD<11> = "CUST.NO EQ ALL"
            CASE OTHERWISE
                R.RECORD<10> = 'LIAB.NO EQ ':FIELD(Y.LIMIT.ID,'.',1)
                R.RECORD<11> = 'REF.NO EQ ':FIELD(Y.LIMIT.ID,'.',2)
                R.RECORD<12> = 'SER.NO EQ ':FIELD(Y.LIMIT.ID,'.',3)
        END CASE
    END



    RETURN
LIMIT.CALC.COMM:
*--------------
*
**** Cauculates the net committed amount, ie. including sub allocations
**** Copy of LIMIT.CALC.COMM routine.
*
    IF Y.LIMIT<LI.ONLINE.LIMIT,Y.VC> <> "" OR Y.LIMIT<LI.SUB.ALLOC.TAKEN,Y.VC> <> "" OR Y.LIMIT<LI.SUB.ALLOC.GIVEN,Y.VC> <> "" THEN
        Y.CALC.LIM.AMT = Y.LIMIT<LI.ONLINE.LIMIT,Y.VC> + Y.LIMIT<LI.SUB.ALLOC.TAKEN,Y.VC> + Y.LIMIT<LI.SUB.ALLOC.GIVEN,Y.VC>
        IF NUM(Y.CALC.LIM.AMT) = 1 AND Y.CALC.LIM.AMT <> "" THEN
            IF Y.CALC.LIM.AMT >= 500 THEN
                Y.CALC.LIM.AMT = Y.CALC.LIM.AMT /1000
            END ELSE
                Y.CALC.LIM.AMT = 0
            END
        END
        R.RECORD<5,Y.VC> = Y.CALC.LIM.AMT
    END
*
    RETURN
*
**** =============================================================== ****
*LIMIT.CALC.OS:
**------------
**
***** Used to calculate the outstanding amount in effect as at a certain
***** date.  Makes a call to routine LIMIT.CALC.OS.
**
*      YSAVE.R.RECORD = R.RECORD
*      R.RECORD = Y.LIMIT
*      YSAVE.VC = VC
*      YSAVE.ID = ID
*      VM.COUNT = Y.VC
*      ID = YKEY
*      O.DATA = ''
**
*      CALL LIMIT.CALC.OS
**
*      R.RECORD = YSAVE.R.RECORD
*      VM.COUNT = YSAVE.VC
*      ID = YSAVE.ID
*      R.RECORD<6,Y.VC> = O.DATA
*      RETURN
*
**** =============================================================== ****
LIMIT.CALC.AVAIL:
*---------------
*
**** Used to calculate the net available amount, ie. including sub
**** allocations. ( Copy of routine LIMIT.CALC.AVAIL )
*
    IF Y.LIMIT<LI.AVAIL.AMT,Y.VC> <> "" THEN
        Y.CALC.AVAIL.AMT = Y.LIMIT<LI.AVAIL.AMT,Y.VC>
        Y.CALC.AVAIL.AMT = Y.CALC.AVAIL.AMT /1000
        R.RECORD<7,Y.VC> = Y.CALC.AVAIL.AMT
    END
    RETURN
*
**** =============================================================== ****
CONV.AMOUNTS:
*===========
*
    IF YCONV.CURRENCY NE LCCY THEN
        YLOCAL.CONV.AMT = ""
        * EN_10001552s
        * Forming the LOWEST.LIM.REF to pass as the last parameter for
        * LIMIT.CURR.CONV.
        YLIM.REF = FIELD(YLIM.REF,'.',1)
        IF LEN(YLIM.REF) EQ 5 THEN
            LIM.REF = YLIM.REF[4]
        END ELSE
            LIM.REF = YLIM.REF
        END
        *
        * Use LIMIT.CURR.CONV� to calculate the local equivalent
        *
        CALL LIMIT.CURR.CONV(YCONV.CURRENCY,YCCY.AMNT,LCCY,YLOCAL.CONV.AMT,LIM.REF)       ;* EN_10001552e
        YCCY.AMNT = YLOCAL.CONV.AMT
        ETEXT = ""
    END
    RETURN
*
**** =============================================================== ****
LIMIT.CALC.OS:
*------------
*
***** Copy of LIMIT.CALC.OS
*
* GB9800042s
*
    TOTAL.CREDIT.AMT = ''
    TOTAL.CREDIT.CCY = ''
    TOTAL.DEBIT.AMT = ''
    TOTAL.DEBIT.CCY = ''
*
* GB9800042e
*
    MAT MLIM.REC = ""
    MAT MACC.REC = ""
    MATPARSE MLIM.REC FROM Y.LIMIT, FM
    YWORK.OS.CCY = MLIM.REC(LI.OS.CCY)<1,Y.VC>
    YWORK.OS.AMT = MLIM.REC(LI.OS.AMT)<1,Y.VC>
    YWORK.COMMITM.CCY = MLIM.REC(LI.COMMITM.CCY)<1,Y.VC>
    YWORK.COMMITM.AMT = MLIM.REC(LI.COMMITM.AMT)<1,Y.VC>
    IF YWORK.COMMITM.CCY<1,1,1> NE "" THEN
        YNO.OF.COMMITM = COUNT(YWORK.COMMITM.CCY<1,1>, SM) + 1
        FOR YX = 1 TO YNO.OF.COMMITM
            YCOMM.CCY = YWORK.COMMITM.CCY<1,1,YX>
            YCOMM.AMT = YWORK.COMMITM.AMT<1,1,YX>
            IF YCOMM.AMT <> "" THEN
                LOCATE YCOMM.CCY IN YWORK.OS.CCY<1,1,1> SETTING YCOMM.POINTER ELSE
                    YCOMM.POINTER = 0
                END
                IF YCOMM.POINTER <> 0 THEN
                    YWORK.OS.AMT<1,1,YCOMM.POINTER> = YWORK.OS.AMT<1,1,YCOMM.POINTER> + YCOMM.AMT
                END ELSE
                    YWORK.OS.CCY = INSERT(YWORK.OS.CCY,1,1,-1,YCOMM.CCY)
                    YWORK.OS.AMT = INSERT(YWORK.OS.AMT,1,1,-1,YCOMM.AMT)
                END
            END
        NEXT YX
    END
    YREVERSE.SIGN = 0 - 1
    PACC.TOTAL = 0
    PREVAL.TOTAL = 0
    PMOVE.CCY = ''
    PMOVE.AMT = ''
    YWORK.ACC.CCY  = ''
    YWORK.ACC.AMT = ''
*-------------------------------------------------Get account balances
    IF MLIM.REC(LI.ACCOUNT) <> "" THEN
        YNO.OF.ACCOUNTS = COUNT(MLIM.REC(LI.ACCOUNT), VM) + 1
        PREV.MNE = ""
        FOR X = 1 TO YNO.OF.ACCOUNTS
            ACC.MNE = MLIM.REC(LI.ACC.COMPANY)<1,X>
            IF PREV.MNE NE ACC.MNE THEN
                YAC.COM.CODE = "" ; YAC.LEAD.COM = "" ; YAC.LEAD.MNE = ''
                CALL GET.COMPANY(ACC.MNE, YAC.COM.CODE, YAC.LEAD.COM, YAC.LEAD.MNE)
                Y.ACCOUNT.FILE = "F":YAC.LEAD.MNE:".ACCOUNT"
                F.ACCOUNT = ""
                CALL OPF(Y.ACCOUNT.FILE, F.ACCOUNT)
                PREV.MNE = ACC.MNE
            END
            YKEY = MLIM.REC(LI.ACCOUNT)<1,X>
            YERR = ""
            CALL F.MATREAD(Y.ACCOUNT.FILE, YKEY, MAT MACC.REC,
            AC.AUDIT.DATE.TIME, F.ACCOUNT, YERR)
            IF YERR <> "" THEN
                GOTO NEXT.ACCT          ;* CI_1017872 s/e
            END
            YACC.CCY = MACC.REC(AC.CURRENCY)
            YACC.AMT = 0
            CALL GET.WORKING.AVAIL.BAL('', YKEY, MAT MACC.REC, '', YACC.AMT,'') ;*API to return the balance to be considered for credit check

            IF YACC.AMT < 0 THEN
                * For the customers with group setup account balance is shared between many group limits
                * so get the exact amount allocated to the passed limit instead of using the whole account balance
                R.ACCOUNT.REC = ''
                GROUP.CUST = '' ;* Set this flag to return the utilisation of the passed account
                LIMIT.UTILISED = 0
                MATBUILD R.ACCOUNT.REC FROM MACC.REC
                ACCOUNT.ID = YKEY
                LIMIT.ID = Y.LIMIT.ID

                * The limit Id has been changed to master group limit to show the utilisation incase of sub group limits

                SAVE.LIMIT.ID = LIMIT.ID
                IF FIELD(LIMIT.ID,'.',1)[1,1] = 'S' THEN
                    CUSTOMER.NUMBER = MACC.REC(AC.CUSTOMER)<1,1>
                    PARENT.GROUP = R.LIMIT.SHARING.GROUP<LI.SG.PARENT.GROUP>
                    LIM.REF = FIELD(LIMIT.ID,'.',2)
                    SERIAL.NO = FIELD(LIMIT.ID,'.',3)
                    LIMIT.ID = R.LIMIT.SHARING.GROUP<LI.SG.PARENT.GROUP> :'.' :LIM.REF :'.' :SERIAL.NO :'.' :CUSTOMER.NUMBER
                END
                * Get the limit allocated amount for the account
                CALL LI.GET.ACCOUNT.UTILISATION(ACCOUNT.ID, R.ACCOUNT.REC, LIMIT.ID, Y.LIMIT, GROUP.CUST, LIMIT.UTILISED, '', ERR)
                LIMIT.ID = SAVE.LIMIT.ID
                * In case the account does not use the passed limit and the balance is allocated by some other limits
                * balance returned value will be 0 so checking the return flag insted of checking return balance
                IF GROUP.CUST = 'YES' THEN ;* This flag indicates the passed account customer is in sharing group so use the returned value
                    YACC.AMT = LIMIT.UTILISED ;* Take the returned balance
                END
            END

            IF YACC.AMT <> 0 THEN
                * GB9800042s
                IF YACC.AMT < 0 OR (MLIM.REC(LI.ALLOW.NETTING)[1,1] = "Y" AND MACC.REC(AC.ALLOW.NETTING)[1,1] = "Y") THEN
                    *
                    IF YACC.AMT < 0 THEN
                        LOCATE YACC.CCY IN TOTAL.DEBIT.CCY<1,1,1> SETTING POS ELSE
                            POS = 0
                        END
                        IF POS THEN
                            TOTAL.DEBIT.AMT<1,1,POS> += YACC.AMT
                        END ELSE
                            TOTAL.DEBIT.CCY = INSERT(TOTAL.DEBIT.CCY,1,1,-1,YACC.CCY)
                            TOTAL.DEBIT.AMT = INSERT(TOTAL.DEBIT.AMT,1,1,-1,YACC.AMT)
                        END
                    END ELSE
                        LOCATE YACC.CCY IN TOTAL.CREDIT.CCY<1,1,1> SETTING POS ELSE
                            POS = 0
                        END
                        IF POS THEN
                            TOTAL.CREDIT.AMT<1,1,POS> += YACC.AMT
                        END ELSE
                            TOTAL.CREDIT.CCY = INSERT(TOTAL.CREDIT.CCY,1,1,-1,YACC.CCY)
                            TOTAL.CREDIT.AMT = INSERT(TOTAL.CREDIT.AMT,1,1,-1,YACC.AMT)
                        END
                    END
                    * GB9800042e
                    LOCATE YACC.CCY IN YWORK.ACC.CCY<1,1,1> SETTING YACC.POINTER ELSE
                        YACC.POINTER = 0
                    END
                    IF YACC.POINTER <> 0 THEN
                        YWORK.ACC.AMT<1,1,YACC.POINTER> = YWORK.ACC.AMT<1,1,YACC.POINTER> + YACC.AMT
                    END ELSE
                        YWORK.ACC.CCY = INSERT(YWORK.ACC.CCY,1,1,-1,YACC.CCY)
                        YWORK.ACC.AMT = INSERT(YWORK.ACC.AMT,1,1,-1,YACC.AMT)
                    END
                END
            END
NEXT.ACCT:
        NEXT X
        *
        * GB9800042s
        *
        PLIM.CCY = MLIM.REC(LI.LIMIT.CURRENCY)

        IF R.LIMIT.PARAMETER<LI.PAR.UPDATE.RATE> = 'YES' THEN
            CALL LIMIT.RATE.REVAL(ID, MAT MLIM.REC, YWORK.ACC.CCY, YWORK.ACC.AMT, PREVAL.TOTAL, PACC.TOTAL, ER)
        END ELSE
            CALL LIMIT.REVAL.OS(PLIM.CCY, YWORK.ACC.CCY, YWORK.ACC.AMT, PMOVE.CCY, PMOVE.AMT, PREVAL.TOTAL, PACC.TOTAL)
        END

        IF PACC.TOTAL > 0 THEN ;* Consider only if the netted amount is debit balance.
            PACC.TOTAL = 0
        END

        *EN_10001552 - removed the changes done to the stated pif.
        TOTAL.DEBIT = 0
        TOTAL.CREDIT = 0
        * BG_100003410s
        IF R.LIMIT.PARAMETER<LI.PAR.UPDATE.RATE> = 'YES' AND NOT(MLIM.REC(LI.ACCOUNT)) THEN
            CALL LIMIT.RATE.REVAL(Y.LIMIT.ID,MAT MLIM.REC,TOTAL.DEBIT.CCY,TOTAL.DEBIT.AMT,"",TOTAL.DEBIT,ERR)
            CALL LIMIT.RATE.REVAL(Y.LIMIT.ID,MAT MLIM.REC,TOTAL.CREDIT.CCY,TOTAL.CREDIT.AMT,"",TOTAL.CREDIT,ERR)
        END ELSE    ;* BG_100003410e
            CALL LIMIT.REVAL.OS(PLIM.CCY, TOTAL.DEBIT.CCY, TOTAL.DEBIT.AMT, "", "", "", TOTAL.DEBIT)
            CALL LIMIT.REVAL.OS(PLIM.CCY, TOTAL.CREDIT.CCY, TOTAL.CREDIT.AMT, "", "", "", TOTAL.CREDIT)
        END
        R.RECORD<18,Y.VC> = TOTAL.DEBIT/1000
        R.RECORD<19,Y.VC> = TOTAL.CREDIT/1000
        *
        * GB9800042e
        *
    END

    PCCY.LIST = YWORK.OS.CCY
    PAMT.LIST = YWORK.OS.AMT

    PLIM.CCY = MLIM.REC(LI.LIMIT.CURRENCY)
*EN_10001552 - removed the changes done to the stated pif.

    PTXN.TOTAL = 0

    IF R.LIMIT.PARAMETER<LI.PAR.UPDATE.RATE> = 'YES' AND NOT(MLIM.REC(LI.ACCOUNT)) THEN
        CALL LIMIT.RATE.REVAL(Y.LIMIT.ID,MAT MLIM.REC,PCCY.LIST,PAMT.LIST,PREVAL.TOTAL,PTXN.TOTAL,ERR)
    END ELSE
        CALL LIMIT.REVAL.OS(PLIM.CCY, PCCY.LIST, PAMT.LIST, PMOVE.CCY,
        PMOVE.AMT, PREVAL.TOTAL, PTXN.TOTAL)
    END

    PTXN.TOTAL += PACC.TOTAL

    O.DATA = PTXN.TOTAL
    YREF = FIELD(Y.LIMIT.ID,".",2,1)
    YCUST = FIELD(Y.LIMIT.ID,".",4)
    BEGIN CASE
        CASE Y.LIMIT<LI.FX.OR.TIME.BAND> = "DP"
            Y.LIMIT<LI.AVAIL.AMT,Y.VC> = ""
            GOTO SET.ODATA
        CASE Y.LIMIT<LI.FX.OR.TIME.BAND> = "IN" AND Y.LIMIT<LI.CHECK.LIMIT> = "NO"
            Y.LIMIT<LI.AVAIL.AMT,Y.VC> = ""
            GOTO SET.ODATA
        CASE Y.LIMIT<LI.ONLINE.LIMIT,Y.VC> NE ""
            NULL
        CASE Y.LIMIT<LI.SUB.ALLOC.TAKEN,Y.VC> NE "" AND Y.LIMIT<LI.SUB.ALLOC.TAKEN,Y.VC> NE "0"
            NULL
        CASE YCUST NE ""
            Y.LIMIT<LI.AVAIL.AMT,Y.VC> = ""
            GOTO SET.ODATA
        CASE YREF[1,3] NE "000" AND YREF[4,4] EQ "0000"
            NULL
        CASE YREF[1,3] EQ "000" AND YREF[4,2] NE "00" AND YREF[6,2] EQ "00"
            NULL
        CASE OTHERWISE
            Y.LIMIT<LI.AVAIL.AMT,Y.VC> = ""
            GOTO SET.ODATA
    END CASE
    Y.LIMIT<LI.AVAIL.AMT,Y.VC> = Y.LIMIT<LI.ONLINE.LIMIT,Y.VC> + Y.LIMIT<LI.SUB.ALLOC.TAKEN,Y.VC> + Y.LIMIT<LI.SUB.ALLOC.GIVEN,Y.VC> + PTXN.TOTAL
SET.ODATA:
*---------------------------------------------------------Divide by 1000
    O.DATA = O.DATA /1000
    R.RECORD<6,Y.VC> = O.DATA
    RETURN
*
**** =============================================================== ****
********************
READ.SEC.POSITION:
********************

    REV1 = ''
    REV2 = ''
    REV3 = ''
    REV4 = ''
    READ.ERROR = ''
    SP.RECORD = ''
    SP.RECORD.ORG = ''

    CALL SC.READ.POSITION(SP.ID,LOCK.RECORD,PROCESS.MAIN.SP,REV1,REV2,SP.RECORD,SP.RECORD.ORG,READ.ERROR,REV3,REV4)

    RETURN

**** =============================================================== ****

FATAL.ERROR:
    YSOURCE.ROUTINE = "E.LIM.ENQ.REC.BUILD"
    CALL FATAL.ERROR(YSOURCE.ROUTINE)
    RETURN
    END

