*-----------------------------------------------------------------------------
* <Rating>-124</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.GET.SIM.COMPARE.DETAILS(OUT.DATA)

*** <region name= PROGRAM DESCRIPTION>
***
*
** Nofile routine returning combined arrangement details
** Term Amount, Interest, Schedule details are the output
*
*** </region>
*-----------------------------------------------------------------------------
*
*** <region name= MODIFICATION HISTORY>
***
* Modification History :
*
*  12-09-13   Task 737278
*             Enhancement 715620
*             Nofile routine returning combined arrangement details -
*             Term Amount, Interest, Schedule details are the output
*
*  24-12-13   Task 868089
*             Defect 868078
*             Payment Type, Property description should be taken based on
*             If description not available then get it using default.
*
*** </region>
*-----------------------------------------------------------------------------
*** <region name= INSERTS>
***
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.AA.INTEREST
    $INSERT I_F.AA.PAYMENT.SCHEDULE
    $INSERT I_F.AA.ACCOUNT
    $INSERT I_F.AA.TERM.AMOUNT
    $INSERT I_F.AA.ARRANGEMENT
    $INSERT I_F.AA.PROPERTY
    $INSERT I_F.AA.PAYMENT.TYPE
    $INSERT I_F.PERIODIC.INTEREST
    $INSERT I_F.BASIC.RATE.TEXT
    $INSERT I_F.AA.SIMULATION.RUNNER

*** </region>
*-----------------------------------------------------------------------------
*** <region name= PROCESS LOGIC>
***
    GOSUB INITIALISE
    GOSUB OPENFILES
    GOSUB BUILD.PROCESS

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= INITIALISE>
***
INITIALISE:

* Initialise local variables

    OUT.DATA = ''
    PRIN.INT.RATE = ""
    PEN.INT.RATE = ""
    TOT.OUTS.AMT = ""
    TOT.INT.PAYABLE = ""
    TOT.OUTS.INT.AMT = ""
    EMI.AMT = ""
    TOT.MAT.AMT = ""
    ARR.ID = ""
    SIM.REF = ""

    INT.VAR.TEXT = ""
    TERM.VAR.TEXT = ""
    PS.VAR.TEXT = ""
    PROP.DESC.TEXT = ""
    RATE.TYPE.TEXT = ""
    FIX.TEXT = ""
    OTHER.TEXT = ""
    PAY.TYPE.TEXT = ""
    PROP.TEXT = ""
    AMOUNT.TEXT = ""
    PAY.METHOD.TEXT = ""
    FREQ.TEXT = ""

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= OPENFILES>
***
OPENFILES:

* Open needed files

    FN.AA.PROPERTY = 'F.AA.PROPERTY'
    F.AA.PROPERTY = ''
    CALL OPF(FN.AA.PROPERTY,F.AA.PROPERTY)

    FN.BASIC.RATE.TEXT = "F.BASIC.RATE.TEXT"
    F.BASIC.RATE.TEXT = ""
    CALL OPF(FN.BASIC.RATE.TEXT,F.BASIC.RATE.TEXT)

    FN.PERIODIC.INTEREST = "F.PERIODIC.INTEREST"
    F.PERIODIC.INTEREST = ""
    CALL OPF(FN.PERIODIC.INTEREST,F.PERIODIC.INTEREST)

    FN.AA.PAYMENT.TYPE = "F.AA.PAYMENT.TYPE"
    F.AA.PAYMENT.TYPE = ""
    CALL OPF(FN.AA.PAYMENT.TYPE,F.AA.PAYMENT.TYPE)

    FN.AA.SIMULATION.RUNNER = "F.AA.SIMULATION.RUNNER"
    F.AA.SIMULATION.RUNNER = ""
    CALL OPF(FN.AA.SIMULATION.RUNNER,F.AA.SIMULATION.RUNNER)

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= BUILD.PROCESS>
***
BUILD.PROCESS:

    GOSUB FIND.ARRANGEMENT

    IF SIM.REF NE "NONE" THEN
        ENQ$SIM.REF = SIM.REF
        GOSUB GET.PROD.PROP.RECORD
        GOSUB GET.TERM.DETAILS
        GOSUB GET.INTEREST.DETAILS
        GOSUB GET.SCHEDULE.DETAILS

        CNT.PROP.DESC.TEXT = DCOUNT(PROP.DESC.TEXT,FM)
        FOR CNT.VAL = 1 TO CNT.PROP.DESC.TEXT
            IF CNT.VAL EQ '1' THEN
                HEADER = "Interest"
            END ELSE
                IF CNT.VAL EQ CNT.PROP.DESC.TEXT THEN
                    HEADER = HEADER:FM:" ":FM:"Schedule"
                END ELSE
                    HEADER = HEADER:FM:" "
                END
            END
        NEXT CNT.VAL

        LOCATE 'Fixed Principal Payment' IN PAY.TYPE.TEXT SETTING PAY.TYPE1.POS ELSE
            PAY.TYPE.TEXT<-1> = 'Fixed Principal Payment'
            PROP.TEXT<-1> = 'Account'
            AMOUNT.TEXT<-1> = ' '
            PAY.METHOD.TEXT<-1> = ' '
            FREQ.TEXT<-1> = ' '
        END

        ALL.PROP.DESC = PROP.DESC.TEXT:FM:PAY.TYPE.TEXT
        ALL.RATE.TYPE = RATE.TYPE.TEXT:FM:PROP.TEXT
        ALL.FIX.OR.VAR = FIX.TEXT:FM:AMOUNT.TEXT
        ALL.OTHER.VALUES = OTHER.TEXT:FM:PAY.METHOD.TEXT
        ALL.TEMP.VALUES = TEMP.TEXT:FM:FREQ.TEXT

        ALL.DETAILS = ALL.PROP.DESC:"*":ALL.RATE.TYPE:"*":ALL.FIX.OR.VAR:"*":ALL.OTHER.VALUES:"*":ALL.TEMP.VALUES
        OUT.DATA = TERM.VAR.TEXT:"*":HEADER:"*":ALL.DETAILS
        CHANGE FM TO VM IN OUT.DATA
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= FIND.ARRANGEMENT>
***
FIND.ARRANGEMENT:

    LOCATE 'SIM.REF' IN ENQ.SELECTION<2,1> SETTING SIMPOS THEN
        SIM.REF = ENQ.SELECTION<4,SIMPOS>         ;* Pick the Simulation Reference
    END

    LOCATE 'ARRANGEMENT.ID' IN ENQ.SELECTION<2,1> SETTING ARRPOS THEN
        ARR.ID = ENQ.SELECTION<4,ARRPOS>          ;* Pick the Arrangement ID
    END

    IF ARR.ID EQ '' THEN
        CALL F.READ(FN.AA.SIMULATION.RUNNER,SIM.REF,R.AA.SIMULATION.RUNNER,F.AA.SIMULATION.RUNNER,SIM.ERR)
        ARR.ID = R.AA.SIMULATION.RUNNER<AA.SIM.ARRANGEMENT.REF>
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= GET.PROD.PROP.RECORD>
***
GET.PROD.PROP.RECORD:

    CALL AA.GET.ARRANGEMENT(ARR.ID, R.ARRANGEMENT, RET.ERROR)
    PRODUCT.OR.PROPERTY = "PRODUCT"
    PRODUCT.ID = R.ARRANGEMENT<AA.ARR.PRODUCT>
    EFF.DATE = TODAY
    PRODUCT.RECORD = ""
    RET.ERR = ""
    CALL AA.GET.PRODUCT.PROPERTY.RECORD(PRODUCT.OR.PROPERTY, "", PRODUCT.ID, "", "", "", "", EFF.DATE, PRODUCT.RECORD, RET.ERR)
    PRODUCT.LINE = R.ARRANGEMENT<AA.ARR.PRODUCT.LINE>
    ACCOUNT.ID = R.ARRANGEMENT<AA.ARR.LINKED.APPL.ID>:"-":R.ARRANGEMENT<AA.ARR.CURRENCY>

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= GET.PROPERTY.DESCRIPTION>
***
GET.PROPERTY.DESCRIPTION:

    CALL F.READ(FN.AA.PROPERTY,PROPERTY.ID,R.PROPERTY,F.PROPERTY,PROP.ERR)
    
    IF R.PROPERTY<AA.PROP.DESCRIPTION,LNGG> THEN
    	PROPERTY.DESCRIPTION = R.PROPERTY<AA.PROP.DESCRIPTION,LNGG>
	END ELSE
    	PROPERTY.DESCRIPTION = R.PROPERTY<AA.PROP.DESCRIPTION,1>
	END
   
    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= TERM.DETAILS>
***
GET.TERM.DETAILS:

    ENQ.DATA.NEW<1> = ENQ.SELECTION<1>
    ENQ.DATA.NEW<2> = 'ID.COMP.1'
    ENQ.DATA.NEW<3> = 'EQ'
    ENQ.DATA.NEW<4> = ARR.ID
    ENQ.DATA.NEW<17> = 'SIM LIV'
    TEMP.R.RNQ = R.ENQ
    R.ENQ<2> = "AA.ARR.TERM.AMOUNT"
    CALL E.AA.BUILD.ARR.COND(ENQ.DATA.NEW)
    R.ENQ = TEMP.R.RNQ

    TERM.PROPERTY.CLASS = "TERM.AMOUNT"
    CALL AA.GET.PROPERTY.NAME(PRODUCT.RECORD,TERM.PROPERTY.CLASS,TERM.PROPERTY)

    ID.TO.ADD = ENQ.DATA.NEW<4>:"%":SIM.REF

    CALL F.READ('F.AA.ARR.TERM.AMOUNT$SIM',ID.TO.ADD,TERM.PROPERTY.RECORD,F.AA.SIM.TERM.AMOUNT,SIM.ERR)
    IF TERM.PROPERTY.RECORD ELSE
        ARR.NO = ARR.ID
        TERM.PROPERTY.CLASS = "TERM.AMOUNT"
        EFF.DATE = TODAY
        CALL AA.GET.PROPERTY.RECORD('', ARR.NO, TERM.PROPERTY, EFF.DATE, TERM.PROPERTY.CLASS, '', TERM.PROPERTY.RECORD, RET.ERROR)
    END

    TERM.AMOUNT = TERM.PROPERTY.RECORD<AA.AMT.AMOUNT>
    TERM = TERM.PROPERTY.RECORD<AA.AMT.TERM>
    REVOLVING = TERM.PROPERTY.RECORD<AA.AMT.REVOLVING>
    IF REVOLVING EQ "PAYMENT" THEN
        REVOLVING.TXT = "Revolving on Payment"
    END ELSE
        REVOLVING.TXT = "Revolving on Prepayment"
    END
    TERM.VAR.TEXT = TERM.AMOUNT:"*":REVOLVING.TXT:"*":TERM

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= GET.INTEREST.DETAILS>
***

GET.INTEREST.DETAILS:

    ENQ.DATA.NEW<1> = ENQ.SELECTION<1>
    ENQ.DATA.NEW<2> = 'ID.COMP.1'
    ENQ.DATA.NEW<3> = 'EQ'
    ENQ.DATA.NEW<4> = ARR.ID
    ENQ.DATA.NEW<17> = 'SIM LIV'
    TEMP.R.RNQ = R.ENQ
    R.ENQ<2> = "AA.ARR.INTEREST"
    CALL E.AA.BUILD.ARR.COND(ENQ.DATA.NEW)
    R.ENQ = TEMP.R.RNQ
    ENQ.PROPERTY = ENQ.DATA.NEW<4>
    CONVERT ' ' TO FM IN ENQ.PROPERTY
    CNT.PROPERTY = DCOUNT(ENQ.PROPERTY,FM)

    FOR CNT.I = 1 TO CNT.PROPERTY
        INT.DATA = ENQ.PROPERTY<CNT.I>
        INT.PROPERTY = FIELDS(INT.DATA,'-',2,1)
        PROPERTY.ID = INT.PROPERTY
        GOSUB GET.PROPERTY.DESCRIPTION

        ID.TO.ADD = INT.DATA:"%":SIM.REF
        CALL F.READ('F.AA.ARR.INTEREST$SIM',ID.TO.ADD,INT.PROPERTY.RECORD,F.AA.SIM.INTEREST,SIM.ERR)
        IF  INT.PROPERTY.RECORD ELSE
            ARR.NO = ARR.ID
            INT.PROPERTY.CLASS = 'INTEREST'
            CALL AA.GET.PROPERTY.RECORD('', ARR.NO, INT.PROPERTY, EFF.DATE, INT.PROPERTY.CLASS, '', INT.PROPERTY.RECORD, RET.ERROR)
        END
        GOSUB GET.REQUIRED.INT.VALUES
        BEGIN CASE
        CASE RATE.TIER.TYPE EQ 'SINGLE'
            GOSUB SINGLE.TYPE.PROCESS
        CASE RATE.TIER.TYPE EQ 'BAND'
            GOSUB BAND.TYPE.PROCESS
        CASE RATE.TIER.TYPE EQ 'LEVEL'
            GOSUB LEVEL.TYPE.PROCESS
        END CASE
    NEXT CNT.I

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= SINGLE.TYPE.PROCESS>
***
SINGLE.TYPE.PROCESS:

    RATE.TIER.TYPE.TEXT = "Single Rate"
    PERIODIC.INDEX.VAL = PERIODIC.INDEX
    FLOATING.INDEX.VAL = FLOATING.INDEX
    FIXED.RATE.VAL = FIXED.RATE
    MARGIN.OPER.VAL = MARGIN.OPER
    EFF.RATE.VAL = EFF.RATE
    MARGIN.RATE.VAL = MARGIN.RATE
    GOSUB COMMON.FORM.TEXT.MESSAGES
    GOSUB CLEAR.VARIABLES
    PROP.DESC.TEXT<-1> = PROPERTY.DESCRIPTION
    RATE.TYPE.TEXT<-1> = RATE.TIER.TYPE.TEXT
    FIX.TEXT<-1> = FIX.VAR.VAL
    OTHER.TEXT<-1> = EFF.RATE.VAL:"% (":DESCRIPTION.VAL.TEXT:" ":MARGIN.OPER.VAL:" ":MARGIN.RATE<1,1,1>:"%)":TIER.VAL.TEXT
    TEMP.TEXT<-1> = " "

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= BAND.TYPE.PROCESS>
***
BAND.TYPE.PROCESS:

    RATE.TIER.TYPE.TEXT = "Tier Bands"
    EFF.RATE.CNT = DCOUNT(EFF.RATE,VM)
    FOR CNT.EF.RATE = 1 TO EFF.RATE.CNT
        PERIODIC.INDEX.VAL = PERIODIC.INDEX<1,CNT.EF.RATE>
        FIXED.RATE.VAL = FIXED.RATE<1,CNT.EF.RATE>
        FLOATING.INDEX.VAL = FLOATING.INDEX<1,CNT.EF.RATE>
        EFF.RATE.VAL = EFF.RATE<1,CNT.EF.RATE>
        MARGIN.RATE.VAL = MARGIN.RATE<1,CNT.EF.RATE>
        TIER.AMOUNT.VAL = TIER.AMOUNT<1,CNT.EF.RATE>
        NEXT.TIER.AMOUNT.VAL = TIER.AMOUNT<1,CNT.EF.RATE+1>
        GOSUB CLEAR.VARIABLES
        GOSUB COMMON.FORM.TEXT.MESSAGES
        IF CNT.EF.RATE EQ '1' THEN
            PROP.DESC.TEXT<-1> = PROPERTY.DESCRIPTION
        END ELSE
            PROP.DESC.TEXT<-1> = " "
        END
        RATE.TYPE.TEXT<-1> = RATE.TIER.TYPE.TEXT
        FIX.TEXT<-1> = FIX.VAR.VAL
        OTHER.TEXT<-1> = EFF.RATE.VAL:"% (":DESCRIPTION.VAL.TEXT:" ":MARGIN.OPER.VAL:" ":MARGIN.RATE<1,1,1>:"%)":TIER.VAL.TEXT
    NEXT CNT.EF.RATE

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= LEVEL.TYPE.PROCESS>
***
LEVEL.TYPE.PROCESS:

    RATE.TIER.TYPE.TEXT = "Tier Levels"
    EFF.RATE.CNT = DCOUNT(EFF.RATE,VM)
    FOR CNT.EF.RATE = 1 TO EFF.RATE.CNT
        PERIODIC.INDEX.VAL = PERIODIC.INDEX<1,CNT.EF.RATE>
        FIXED.RATE.VAL = FIXED.RATE<1,CNT.EF.RATE>
        FLOATING.INDEX.VAL = FLOATING.INDEX<1,CNT.EF.RATE>
        EFF.RATE.VAL = EFF.RATE<1,CNT.EF.RATE>
        MARGIN.RATE.VAL = MARGIN.RATE<1,CNT.EF.RATE>
        TIER.AMOUNT.VAL = TIER.AMOUNT<1,CNT.EF.RATE>
        NEXT.TIER.AMOUNT.VAL = TIER.AMOUNT<1,CNT.EF.RATE+1>
        GOSUB CLEAR.VARIABLES
        GOSUB COMMON.FORM.TEXT.MESSAGES
        IF CNT.EF.RATE EQ '1' THEN
            PROP.DESC.TEXT<-1> = PROPERTY.DESCRIPTION
        END ELSE
            PROP.DESC.TEXT<-1> = " "
        END
        RATE.TYPE.TEXT<-1> = RATE.TIER.TYPE.TEXT
        FIX.TEXT<-1> = FIX.VAR.VAL
        OTHER.TEXT<-1> = EFF.RATE.VAL:"% (":DESCRIPTION.VAL.TEXT:" ":MARGIN.OPER.VAL:" ":MARGIN.RATE<1,1,1>:"%)":TIER.VAL.TEXT
    NEXT CNT.EF.RATE

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= CLEAR.VARIABLES>
***
CLEAR.VARIABLES:

    TIER.VAL.TEXT = ""

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= GET.REQUIRED.INT.VALUES>
***
GET.REQUIRED.INT.VALUES:

    RATE.TIER.TYPE = INT.PROPERTY.RECORD<AA.INT.RATE.TIER.TYPE>
    FIXED.RATE = INT.PROPERTY.RECORD<AA.INT.FIXED.RATE>
    EFF.RATE = INT.PROPERTY.RECORD<AA.INT.EFFECTIVE.RATE>
    FLOATING.INDEX = INT.PROPERTY.RECORD<AA.INT.FLOATING.INDEX>
    PERIODIC.INDEX = INT.PROPERTY.RECORD<AA.INT.PERIODIC.INDEX>
    PERIODIC.RATE = INT.PROPERTY.RECORD<AA.INT.PERIODIC.RATE>
    MARGIN.OPER = INT.PROPERTY.RECORD<AA.INT.MARGIN.OPER>
    MARGIN.RATE = INT.PROPERTY.RECORD<AA.INT.MARGIN.RATE>
    TIER.AMOUNT = INT.PROPERTY.RECORD<AA.INT.TIER.AMOUNT>

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= COMMON.FORM.TEXT.MESSAGES>
***
COMMON.FORM.TEXT.MESSAGES:

    IF PERIODIC.INDEX.VAL OR FIXED.RATE.VAL THEN
        FIX.VAR.VAL = "Fixed"
    END ELSE
        FIX.VAR.VAL = "Variable"
    END

    GOSUB FIND.FLOAT.PERIODIC.DESCRIPTION
    GOSUB FIND.MARGIN.TEXT
    GOSUB FIND.TIER.TEXT

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= FIND.FLOAT.PERIODIC.DESCRIPTION>
***
FIND.FLOAT.PERIODIC.DESCRIPTION:

    IF FLOATING.INDEX.VAL THEN
        CALL F.READ(FN.BASIC.RATE.TEXT,FLOATING.INDEX.VAL,R.BASIC.RATE.TEXT,F.BASIC.RATE.TEXT,BASIC.RATE.TEXT.ERR)
        IF R.BASIC.RATE.TEXT THEN
    	    IF R.BASIC.RATE.TEXT<EB.BRT.DESCRIPTION,LNGG> THEN
    			DESCRIPTION.VAL.TEXT = R.BASIC.RATE.TEXT<EB.BRT.DESCRIPTION,LNGG>
			END ELSE
    			DESCRIPTION.VAL.TEXT = R.BASIC.RATE.TEXT<EB.BRT.DESCRIPTION,1>
			END
           END
    END ELSE
        PI.ID = PERIODIC.INDEX.VAL:R.ARRANGEMENT<AA.ARR.CURRENCY>:TODAY
        CALL F.READ(FN.PERIODIC.INTEREST,PI.ID,R.PERIODIC.INTEREST,F.PERIODIC.INTEREST,PI.ERR)
        IF R.PERIODIC.INTEREST THEN
   			IF R.PERIODIC.INTEREST<PI.DESCRIPTION,LNGG> THEN
    			DESCRIPTION.VAL.TEXT = R.PERIODIC.INTEREST<PI.DESCRIPTION,LNGG>
			END ELSE
    			DESCRIPTION.VAL.TEXT = R.PERIODIC.INTEREST<PI.DESCRIPTION,1>
    		END
        END
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= FIND.MARGIN.TEXT>
***
FIND.MARGIN.TEXT:

    MARGIN.OPER.VAL = MARGIN.OPER<1,CNT.EF.RATE>
    IF MARGIN.OPER.VAL THEN
        BEGIN CASE
        CASE MARGIN.OPER.VAL EQ 'ADD'
            MARGIN.OPER.VAL = "+"
        CASE MARGIN.OPER.VAL EQ 'SUB'
            MARGIN.OPER.VAL = "-"
        CASE MARGIN.OPER.VAL EQ 'MULTIPLY'
            MARGIN.OPER.VAL = "*"
        END CASE
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= FIND.TIER.TEXT>
***
FIND.TIER.TEXT:

    TEMP.TIER.AMOUNT = TIER.AMOUNT
    CHANGE VM TO '' IN TEMP.TIER.AMOUNT

    IF TEMP.TIER.AMOUNT NE '' THEN
        IF TIER.AMOUNT.VAL NE '' THEN
            TIER.VAL.TEXT = "up to ":TIER.AMOUNT.VAL
        END ELSE
            TIER.VAL.TEXT = "remainder"
        END
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= GET.SCHEDULE.DETAILS>
***
GET.SCHEDULE.DETAILS:

    ENQ.DATA.NEW<1> = ENQ.SELECTION<1>
    ENQ.DATA.NEW<2> = 'ID.COMP.1'
    ENQ.DATA.NEW<3> = 'EQ'
    ENQ.DATA.NEW<4> = ARR.ID
    ENQ.DATA.NEW<17> = 'SIM LIV'
    TEMP.R.RNQ = R.ENQ
    R.ENQ<2> = "AA.ARR.PAYMENT.SCHEDULE"
    CALL E.AA.BUILD.ARR.COND(ENQ.DATA.NEW)
    R.ENQ = TEMP.R.RNQ

    PS.PROPERTY.CLASS = "PAYMENT.SCHEDULE"
    CALL AA.GET.PROPERTY.NAME(PRODUCT.RECORD,PS.PROPERTY.CLASS,PS.PROPERTY)

    ID.TO.ADD = ENQ.DATA.NEW<4>:"%":SIM.REF

    CALL F.READ('F.AA.ARR.PAYMENT.SCHEDULE$SIM',ID.TO.ADD,PS.PROPERTY.RECORD,F.AA.SIM.PAYMENT.SCHEDULE,SIM.ERR)
    IF PS.PROPERTY.RECORD ELSE
        ARR.NO = ARR.ID
        CALL AA.GET.PROPERTY.RECORD('', ARR.NO, PS.PROPERTY, EFF.DATE, PS.PROPERTY.CLASS, '', PS.PROPERTY.RECORD, RET.ERROR)
    END

    PAYMENT.TYPE = PS.PROPERTY.RECORD<AA.PS.PAYMENT.TYPE>
    TOT.PAY.TYPE = DCOUNT(PAYMENT.TYPE,VM)
    FOR CNT.J = 1 TO TOT.PAY.TYPE
        PS.PROPERTIES = PS.PROPERTY.RECORD<AA.PS.PROPERTY,CNT.J>
        GOSUB PROCESS.BY.PROPERTY
    NEXT CNT.J

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= PROCESS.BY.PROPERTY>
***
PROCESS.BY.PROPERTY:

    FOR CNT.Y = 1 TO DCOUNT(PS.PROPERTIES,VM)
        PS.PROPERTY = PS.PROPERTIES<1,CNT.Y>
        PROPERTY.ID = PS.PROPERTY
        GOSUB GET.PROPERTY.DESCRIPTION
        CALL AA.GET.PROPERTY.CLASS(PS.PROPERTY,PROP.CLASS)
        GOSUB FIND.AMOUNT
        FREQ = PS.PROPERTY.RECORD<AA.PS.DUE.FREQ,CNT.J>
        IN.DATA = ""
        OUT.MASK = ""
        CALL EB.BUILD.RECURRENCE.MASK(FREQ,IN.DATA,OUT.MASK)
        FREQ = OUT.MASK
        PAY.METHOD = PS.PROPERTY.RECORD<AA.PS.PAYMENT.METHOD,CNT.J>
        CALL F.READ(FN.AA.PAYMENT.TYPE,PAYMENT.TYPE<1,CNT.J>,R.AA.PAYMENT.TYPE,F.AA.PAYMENT.TYPE,TYPE.ERR)
   
        IF R.AA.PAYMENT.TYPE<AA.PT.DESCRIPTION,LNGG> THEN
    		PAYMENT.TYPE.DESCRIPTION = R.AA.PAYMENT.TYPE<AA.PT.DESCRIPTION,LNGG>
		END ELSE
    		PAYMENT.TYPE.DESCRIPTION = R.AA.PAYMENT.TYPE<AA.PT.DESCRIPTION,1>
		END
 
        IF AMOUNT EQ '' THEN
            AMOUNT = " "
        END
        IF AMOUNT THEN
            AMOUNT = FMT(AMOUNT, "R2#10")
        END
        IF PAY.METHOD EQ '' THEN
            PAY.METHOD = " "
        END
        IF FREQ EQ '' THEN
            FREQ = " "
        END
        PAY.TYPE.TEXT<-1> = PAYMENT.TYPE.DESCRIPTION
        PROP.TEXT<-1> = PROPERTY.DESCRIPTION
        AMOUNT.TEXT<-1> = AMOUNT
        PAY.METHOD.TEXT<-1> = PAY.METHOD
        FREQ.TEXT<-1> = FREQ
    NEXT CNT.Y

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= FIND.AMOUNT>
***
FIND.AMOUNT:

    AMOUNT = ' '
    IF PROP.CLASS EQ 'ACCOUNT' THEN
        ACTUAL.AMT = PS.PROPERTY.RECORD<AA.PS.ACTUAL.AMT,CNT.J>
        CALC.AMT = PS.PROPERTY.RECORD<AA.PS.CALC.AMOUNT,CNT.J>
        IF ACTUAL.AMT THEN
            AMOUNT = ACTUAL.AMT
        END ELSE
            AMOUNT = CALC.AMT
        END
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------

END

