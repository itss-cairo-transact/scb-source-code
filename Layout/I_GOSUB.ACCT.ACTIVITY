* Version 22 01/03/01  GLOBUS Release No. 200512 09/12/05
* Version 9.1.0A released on 29/09/89
REM "I_GOSUB.ACCT.ACTIVITY",850723-001
* Version 1 released on 16/08/85
*
*************************************************************************
*
* CHANGE CONTROL
* --------------
*
* 13/07/98 - GB9800851
*            Insert file needs changes in the way the exposure date
*            ladder will be processed.
*
* 06/01/00 - GB0000386
*            Move the processing in EOD.UPDATE.ACCT.ACT to here
*
* 07/11/00 - GB0002716
*            The changes were effected to nullify the Debit/Credit movements in relevant
*            CRF entries for reversal transactions where the CRF.REVERSAL.FLAG has been
*            set in ACCOUNT.PARAMETER
*
* 22/12/00 - GB0003231
*            Do not update STMT.VAL.ENTRY for internal accounts (these can get massive)
* 28/02/02 - EN_10000421
*            Update AC.OPEN.AVAILABLE.BAL with AC.OPEN.CLEARED.BAL
*
* 29/04/02 - EF_13
*            Initialised the variable YR.VAL.ENT
* 07/05/02 - EN_10000650
*            Remove changes from EN_10000421
*
* 06/10/04 - CI_10023745
*            Jbase memory error due to Readu of STMT.VAL.ENTRY done for
*            each entry.
*
* 26/10/04 - EN_10002358
*            Remove updating of different currency markets. Only use the
*            ACCOUNT CURRENCY.MARKET.
*
* 24/10/05 - CI_10035929 / REF: HD0515045
*            If Initiation field of transaction is specified as "BANK", the
*            account record is updated with incorrect value.
*************************************************************************
*
    IF YR.ENTRY(AC.STE.VALUE.DATE) = '' THEN
        IF YR.ENTRY(AC.STE.BOOKING.DATE) THEN
            YR.ENTRY(AC.STE.VALUE.DATE) = YR.ENTRY(AC.STE.BOOKING.DATE)
        END
    END
*
    IF YR.ENTRY(AC.STE.VALUE.DATE) = "" THEN YR.ENTRY(AC.STE.VALUE.DATE) = TODAY
    IF YR.ENTRY(AC.STE.BOOKING.DATE) = "" THEN YR.ENTRY(AC.STE.BOOKING.DATE) = TODAY
* Default value = Today
*
* read account activity record
*
    YYM = YR.ENTRY(AC.STE.VALUE.DATE)[1,6] ; YID.ACTIV = YID.ACCT:"-":YYM
    IF YID.ACTIV.LAST <> YID.ACTIV THEN
        IF YID.ACTIV.LAST <> "" THEN
            MATWRITE YR.ACT TO F.ACCT.ACTIVITY, YID.ACTIV.LAST
*     write activity record if new one has another year/month
        END
        YLOC = "OK"
        LOCATE YYM IN YT.YEARM<1> BY "AR" SETTING YLOCYM ELSE
            YT.YEARM = INSERT(YT.YEARM,YLOCYM,0,0,YYM)
            YLOC = "" ; MAT YR.ACT = "" ; YT.YEARM.CHANGE = "Y"
*     'Y' means: table has to be written back
        END
        IF YLOC = "OK" THEN
            MATREAD YR.ACT FROM F.ACCT.ACTIVITY, YID.ACTIV ELSE
                MAT YR.ACT = ""
*     'ELSE' normally not possible
            END
        END
    END
    IF Y1ST.DATE = "" OR YR.ENTRY(AC.STE.VALUE.DATE) < Y1ST.DATE THEN
        Y1ST.DATE = YR.ENTRY(AC.STE.VALUE.DATE)
*     Y1ST.DATE is used to control correction of last capitalisations
    END
    YDAYNO = YR.ENTRY(AC.STE.VALUE.DATE)[7,2]
    LOCATE YDAYNO IN YR.ACT(IC.ACT.DAY.NO)<1,1> BY "AR" SETTING YLOCDAY ELSE
        YR.ACT(IC.ACT.DAY.NO) = INSERT(YR.ACT(IC.ACT.DAY.NO),1,YLOCDAY,0,YDAYNO)
        YR.ACT(IC.ACT.TURNOVER.CREDIT) = INSERT(YR.ACT(IC.ACT.TURNOVER.CREDIT),1,YLOCDAY,0,"")
        YR.ACT(IC.ACT.TURNOVER.DEBIT) = INSERT(YR.ACT(IC.ACT.TURNOVER.DEBIT),1,YLOCDAY,0,"")
        YR.ACT(IC.ACT.BALANCE) = INSERT(YR.ACT(IC.ACT.BALANCE),1,YLOCDAY,0,"")
*     locate DAY.NO in activity record
*
* try to find previous balance, when new day
*
        IF YLOCDAY > 1 THEN
            YR.ACT(IC.ACT.BALANCE)<1,YLOCDAY> = YR.ACT(IC.ACT.BALANCE)<1,YLOCDAY-1>
*     old balance found in record of same year/month
        END ELSE
            YLOCYM.OLD = YLOCYM
            LOOP
                YLOCYM.OLD = YLOCYM.OLD-1
            UNTIL YLOCYM.OLD = 0 DO
                Y = YID.ACCT:"-":YT.YEARM<YLOCYM.OLD>
                MATREAD YR.ACT.OLD FROM F.ACCT.ACTIVITY, Y
                ELSE MAT YR.ACT.OLD = ""
                IF YR.ACT.OLD(IC.ACT.DAY.NO) <> "" THEN
                    X = COUNT(YR.ACT.OLD(IC.ACT.DAY.NO),VM)+1
                    YR.ACT(IC.ACT.BALANCE)<1,YLOCDAY> = YR.ACT.OLD(IC.ACT.BALANCE)<1,X>
                    YLOCYM.OLD = 1
*     old balance found in last previous year/month
                END
            REPEAT
        END
    END
*
* store transaction numbers + amount, turnover
*
    YID.TRS = YR.ENTRY(AC.STE.TRANSACTION.CODE)
    IF YR.ACCOUNT(AC.CURRENCY) = LCCY THEN YAMT = YR.ENTRY(AC.STE.AMOUNT.LCY)
    ELSE YAMT = YR.ENTRY(AC.STE.AMOUNT.FCY)
*     get local or foreign amount
    IF YAMT # "" THEN
        IF YAMT > 0 THEN
            YR.ACT(IC.ACT.TURNOVER.CREDIT)<1,YLOCDAY> = YR.ACT(IC.ACT.TURNOVER.CREDIT)<1,YLOCDAY> + YAMT
****  Add transaction amount to credit turnover
        END ELSE
            YR.ACT(IC.ACT.TURNOVER.DEBIT)<1,YLOCDAY> = YR.ACT(IC.ACT.TURNOVER.DEBIT)<1,YLOCDAY> + YAMT
****  Add transaction amount to debit turnover
        END
    END
    IF YR.ENTRY(AC.STE.VALUE.DATE)[1,6] = YR.ENTRY(AC.STE.BOOKING.DATE)[1,6] THEN
        LOCATE YID.TRS IN YR.ACT(IC.ACT.TRANSACT.CODE)<1,1> BY "AR" SETTING YLOCTRS ELSE
            YR.ACT(IC.ACT.TRANSACT.CODE) = INSERT(YR.ACT(IC.ACT.TRANSACT.CODE),1,YLOCTRS,0,YID.TRS)
            YR.ACT(IC.ACT.NO.OF.TRANSACT) = INSERT(YR.ACT(IC.ACT.NO.OF.TRANSACT),1,YLOCTRS,0,"")
            YR.ACT(IC.ACT.TRANSACT.AMT) = INSERT(YR.ACT(IC.ACT.TRANSACT.AMT),1,YLOCTRS,0,"")
        END
        IF YR.ENTRY(AC.STE.REVERSAL.MARKER) <> "R" THEN
            YR.ACT(IC.ACT.NO.OF.TRANSACT)<1,YLOCTRS> = YR.ACT(IC.ACT.NO.OF.TRANSACT)<1,YLOCTRS> + 1
        END ELSE
            YR.ACT(IC.ACT.NO.OF.TRANSACT)<1,YLOCTRS> = YR.ACT(IC.ACT.NO.OF.TRANSACT)<1,YLOCTRS> - 1
        END
        YR.ACT(IC.ACT.TRANSACT.AMT)<1,YLOCTRS> = YR.ACT(IC.ACT.TRANSACT.AMT)<1,YLOCTRS> + YAMT
****  Add transaction amount
    END ELSE
        YYM.TEMP = YR.ENTRY(AC.STE.BOOKING.DATE)[1,6]
        YID.ACTIV.TEMP = YID.ACCT:"-":YYM.TEMP
        READ YR.ACT.TEMP FROM F.ACCT.ACTIVITY,YID.ACTIV.TEMP ELSE YR.ACT.TEMP = ""
        LOCATE YYM.TEMP IN YT.YEARM<1> BY "AR" SETTING YLOCYM.TEMP ELSE
            YT.YEARM = INSERT(YT.YEARM,YLOCYM.TEMP,0,0,YYM.TEMP)
            YR.ACT.TEMP = "" ; YT.YEARM.CHANGE = "Y"
*     'Y' means: table has to be written back
        END
        YDAYNO.TEMP = YR.ENTRY(AC.STE.BOOKING.DATE)[7,2]
        LOCATE YID.TRS IN YR.ACT.TEMP<IC.ACT.TRANSACT.CODE,1> BY "AR" SETTING YLOC.TEMP ELSE
            YR.ACT.TEMP = INSERT(YR.ACT.TEMP,IC.ACT.TRANSACT.CODE,YLOC.TEMP,0,YID.TRS)
            YR.ACT.TEMP = INSERT(YR.ACT.TEMP,IC.ACT.NO.OF.TRANSACT,YLOC.TEMP,0,"")
            YR.ACT.TEMP = INSERT(YR.ACT.TEMP,IC.ACT.TRANSACT.AMT,YLOC.TEMP,0,"")
        END
        IF YR.ENTRY(AC.STE.REVERSAL.MARKER) <> "R" THEN
            YR.ACT.TEMP<IC.ACT.NO.OF.TRANSACT,YLOC.TEMP> = YR.ACT.TEMP<IC.ACT.NO.OF.TRANSACT,YLOC.TEMP> + 1
        END ELSE
            YR.ACT.TEMP<IC.ACT.NO.OF.TRANSACT,YLOC.TEMP> = YR.ACT.TEMP<IC.ACT.NO.OF.TRANSACT,YLOC.TEMP> - 1
        END
        YR.ACT.TEMP<IC.ACT.TRANSACT.AMT,YLOC.TEMP> = YR.ACT.TEMP<IC.ACT.TRANSACT.AMT,YLOC.TEMP> + YAMT
****  Add transaction amount
        WRITE YR.ACT.TEMP ON F.ACCT.ACTIVITY,YID.ACTIV.TEMP
        LOCATE YYM IN YT.YEARM<1> BY "AR" SETTING YLOCYM ELSE NULL
    END
*     add 1 to number of transactions and add amount
    YID.ACTIV.LAST = YID.ACTIV
*

* GB0000386* Starts
*** Add in the booking date fields if not already present
    IF YR.ENTRY(AC.STE.VALUE.DATE) # YR.ENTRY(AC.STE.BOOKING.DATE) THEN
        LOCATE YR.ENTRY(AC.STE.VALUE.DATE)[7,2] IN YR.ACT(IC.ACT.VALUE.DAY)<1,1> SETTING VALUE.POS ELSE
            INS YR.ENTRY(AC.STE.VALUE.DATE)[7,2] BEFORE YR.ACT(IC.ACT.VALUE.DAY)<1,VALUE.POS>
        END
        LOCATE YR.ENTRY(AC.STE.BOOKING.DATE) IN YR.ACT(IC.ACT.BOOKING.DATE)<1,VALUE.POS,1> SETTING BOOK.POS ELSE
            INS YR.ENTRY(AC.STE.BOOKING.DATE) BEFORE YR.ACT(IC.ACT.BOOKING.DATE)<1,VALUE.POS,BOOK.POS>
            INS '' BEFORE YR.ACT(IC.ACT.BOOK.TOVR.CR)<1,VALUE.POS,BOOK.POS>
            INS '' BEFORE YR.ACT(IC.ACT.BOOK.TOVR.DB)<1,VALUE.POS,BOOK.POS>
        END

        IF YR.ENTRY(AC.STE.CURRENCY) = LCCY OR YR.ENTRY(AC.STE.CURRENCY) = '' THEN
            TOVR.AMT = YR.ENTRY(AC.STE.AMOUNT.LCY)
        END ELSE
            TOVR.AMT = YR.ENTRY(AC.STE.AMOUNT.FCY)
        END

        IF YR.ENTRY(AC.STE.REVERSAL.MARKER) <> 'Y' THEN
            BEGIN CASE
            CASE TOVR.AMT >= 0
                YR.ACT(IC.ACT.BOOK.TOVR.CR)<1,VALUE.POS,BOOK.POS> +=TOVR.AMT
            CASE TOVR.AMT < 0
                YR.ACT(IC.ACT.BOOK.TOVR.DB)<1,VALUE.POS,BOOK.POS> +=TOVR.AMT
            END CASE
        END ELSE
            BEGIN CASE
            CASE TOVR.AMT < 0
                YR.ACT(IC.ACT.BOOK.TOVR.CR)<1,VALUE.POS,BOOK.POS> += TOVR.AMT
            CASE TOVR.AMT >= 0
                YR.ACT(IC.ACT.BOOK.TOVR.DB)<1,VALUE.POS,BOOK.POS> += TOVR.AMT
            END CASE
        END
    END
* GB0000386* Ends
***
    IF YAMT <> "" THEN IF YAMT > 0 THEN

*   < GB0002716

        IF YR.ENTRY(AC.STE.REVERSAL.MARKER) = 'R' AND R.ACCOUNT.PARAMETER<AC.PAR.CRF.REVERSAL.FLAG> = 'YES' THEN
            IF YR.ACCOUNT(AC.CURRENCY) = LCCY THEN
                YAMT.CON<1> = YAMT.CON<1> + YAMT  ;* EN_10002358/S
            END ELSE
                YAMT.CON<3> = YAMT.CON<3> + YR.ENTRY(AC.STE.AMOUNT.LCY)
                YAMT.CON<1> = YAMT.CON<1> + YAMT
            END

*   > GB0002716

        END ELSE
            IF YR.ACCOUNT(AC.CURRENCY) = LCCY THEN
                YAMT.CON<2> = YAMT.CON<2> + YAMT
            END ELSE
                YAMT.CON<4> = YAMT.CON<4> + YR.ENTRY(AC.STE.AMOUNT.LCY)
                YAMT.CON<2> = YAMT.CON<2> + YAMT  ;* EN_10002358/E
            END
        END
****  Add transaction amount to consolidation credit total
    END
    IF YAMT <> "" THEN IF YAMT < 0 THEN

*   < GB0002716

        IF YR.ENTRY(AC.STE.REVERSAL.MARKER) = 'R' AND R.ACCOUNT.PARAMETER<AC.PAR.CRF.REVERSAL.FLAG> = 'YES' THEN
            IF YR.ACCOUNT(AC.CURRENCY) = LCCY THEN
                YAMT.CON<2> = YAMT.CON<2> + YAMT  ;* EN_10002358/S
            END ELSE
                YAMT.CON<4> = YAMT.CON<4> + YR.ENTRY(AC.STE.AMOUNT.LCY)
                YAMT.CON<2> = YAMT.CON<2> + YAMT
            END

*   > GB0002716

        END ELSE
            IF YR.ACCOUNT(AC.CURRENCY) = LCCY THEN
                YAMT.CON<1> = YAMT.CON<1> + YAMT
            END ELSE
                YAMT.CON<3> = YAMT.CON<3> + YR.ENTRY(AC.STE.AMOUNT.LCY)
                YAMT.CON<1> = YAMT.CON<1> + YAMT  ;* EN_10002358/E
            END
        END
****  Add transaction amount to consolidation debit total
    END
*
* add amount to all balances beginning with value date
*
    YLOC.END = COUNT(YR.ACT(IC.ACT.DAY.NO),VM)+1
    LOOP UNTIL YLOCDAY > YLOC.END
        YR.ACT(IC.ACT.BALANCE)<1,YLOCDAY> = YR.ACT(IC.ACT.BALANCE)<1,YLOCDAY> + YAMT
        YLOCDAY = YLOCDAY+1
*     add in record with same year/month
    REPEAT
    YLOCYM.END = COUNT(YT.YEARM,FM)+1
    IF YLOCYM.END > YLOCYM THEN
        MATWRITE YR.ACT TO F.ACCT.ACTIVITY, YID.ACTIV
*     write activity record when next one has to be updated
        YLOCYM.OLD = YLOCYM
        LOOP
            YLOCYM.OLD = YLOCYM.OLD+1
        UNTIL YLOCYM.OLD > YLOCYM.END DO
            YID.ACTIV = YID.ACCT:"-":YT.YEARM<YLOCYM.OLD>
            MATREAD YR.ACT FROM F.ACCT.ACTIVITY, YID.ACTIV
            ELSE MAT YR.ACT = ""
            IF YR.ACT(IC.ACT.DAY.NO) <> "" THEN
                YCOUNT = COUNT(YR.ACT(IC.ACT.DAY.NO),VM)+1
                FOR X = 1 TO YCOUNT
                    YR.ACT(IC.ACT.BALANCE)<1,X> = YR.ACT(IC.ACT.BALANCE)<1,X> + YAMT
*     update balances > year.month of value date
                NEXT X
            END
            IF YLOCYM.OLD = YLOCYM.END THEN
                YID.ACTIV.LAST = YID.ACTIV
            END ELSE
                MATWRITE YR.ACT TO F.ACCT.ACTIVITY, YID.ACTIV
            END
            YLOCYM = YLOCYM+1
        REPEAT
    END
*
* update account record
*
*
*** Multi-Market
*** Find the ccy market balances and update
*
*
*
    YR.ACCOUNT(AC.OPEN.ACTUAL.BAL) = YR.ACCOUNT(AC.OPEN.ACTUAL.BAL) + YAMT
*    add OPEN.ACTUAL.BALANCE
* GB9800851 (Starts)
    IF YR.ENTRY(AC.STE.EXP.SPLIT.DATE) THEN
        NO.OF.EXP.DATES = DCOUNT(YR.ENTRY(AC.STE.EXP.SPLIT.DATE),VM)
        FOR EXP.CNT = 1 TO NO.OF.EXP.DATES
            IF YR.ENTRY(AC.STE.EXP.SPLIT.DATE)<1,EXP.CNT> <= TODAY THEN
                YR.ACCOUNT(AC.OPEN.CLEARED.BAL) = YR.ACCOUNT(AC.OPEN.CLEARED.BAL) + YR.ENTRY(AC.STE.EXP.SPLIT.AMT)<1,EXP.CNT>
* EN_10000421 S
*Update AC.OPEN.AVAILABLE.BAL with AC.OPEN.CLEARED.BAL
*               YR.ACCOUNT(AC.OPEN.AVAILABLE.BAL)= YR.ACCOUNT(AC.OPEN.CLEARED.BAL) ; EN_10000650
*EN_10000421 E

*    add OPEN.CLEARED.BALANCE in accordance to EXPOSURE.DATES
            END
        NEXT EXP.CNT
    END ELSE
        IF YR.ENTRY(AC.STE.EXPOSURE.DATE) <= TODAY THEN
            YR.ACCOUNT(AC.OPEN.CLEARED.BAL) = YR.ACCOUNT(AC.OPEN.CLEARED.BAL) + YAMT
* EN_10000421
*Update AC.OPEN.AVAILABLE.BAL with AC.OPEN.CLEARED.BAL
*            YR.ACCOUNT(AC.OPEN.AVAILABLE.BAL)= YR.ACCOUNT(AC.OPEN.CLEARED.BAL) ; EN_10000650
* EN_10000421 E

*    add OPEN.CLEARED.BALANCE in accordance to EXPOSURE.DATES
        END
    END
* GB9800851 (Ends)
    YFIELD = "INITIATION" ; CALL UPD.TRS(YID.TRS,YFIELD)
    IF ETEXT <> "" THEN E = ETEXT ; GOTO FATAL.ERROR
*     ask for INITIATION (AUTO, BANK, CUSTOMER) or transaction code
    BEGIN CASE
    CASE YFIELD = "AUTO"
        YACCT.FIELD = AC.DATE.LAST.DR.AUTO
    CASE YFIELD = "BANK"
        YACCT.FIELD = AC.DATE.LAST.DR.BANK
    CASE OTHERWISE ; YACCT.FIELD = AC.DATE.LAST.DR.CUST
    END CASE
    IF YAMT > 0 THEN YACCT.FIELD = YACCT.FIELD-9
*************************************************************************
****  Store greatest transaction value only for today                ****
*************************************************************************
*
******** DEREK HOULTON 13 MAY 87
*
*** YR.ENTRY(AC.STE.RECORD.STATUS) is the record status of the STMT ENTRY record
*** If the record status is REVE then don't update fields
*** in the ACCOUNT record  RFM 87 172
********
    IF YR.ENTRY(AC.STE.RECORD.STATUS) NE "REVE" THEN
        IF YR.ACCOUNT(YACCT.FIELD) = TODAY THEN
            IF ABS(YR.ACCOUNT(YACCT.FIELD+1)) < ABS(YAMT) THEN
                YR.ACCOUNT(YACCT.FIELD+1) = YAMT
                YR.ACCOUNT(YACCT.FIELD+2) = YID.TRS
            END
        END ELSE
            YR.ACCOUNT(YACCT.FIELD) = TODAY
            YR.ACCOUNT(YACCT.FIELD+1) = YAMT
            YR.ACCOUNT(YACCT.FIELD+2) = YID.TRS
        END
    END
*
    IF R.ACCOUNT.PARAMETER<AC.PAR.LAST.VAL.PURGE> LE YR.ENTRY(AC.STE.VALUE.DATE) AND NUM(YID.ACCT) THEN
        YID.VAL.ENT = YID.ACCT:'-':YR.ENTRY(AC.STE.VALUE.DATE)
* Write only when the ID changes.
        IF YID.VAL.ENT NE YID.VAL.ENT.PREV THEN
            IF YID.VAL.ENT.PREV THEN WRITE YR.VAL.ENT ON F.STMT.VAL.ENTRY,YID.VAL.ENT.PREV
* Read the next ID.
            READ YR.VAL.ENT FROM F.STMT.VAL.ENTRY, YID.VAL.ENT ELSE
                YR.VAL.ENT = ""
            END
        END
        IF YR.VAL.ENT THEN
            YR.VAL.ENT := FM: YID.STMT
        END ELSE
            YR.VAL.ENT = YID.STMT
        END
    END
*
*************************************************************************
