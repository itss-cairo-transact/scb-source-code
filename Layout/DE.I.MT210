* Version 1 25/06/02  GLOBUS Release No. G13.2.00 25/06/02
*-----------------------------------------------------------------------------
* <Rating>-184</Rating>
    $PACKAGE FT.Delivery
    SUBROUTINE DE.I.MT210
*************************************************************************
*                                                                       *
* Inward delivery template                                              *
*                                                                       *
*************************************************************************
*                                                                       *
*   MODIFICATIONS                                                       *
*                                                                       *
* 10/07/02 -  EN_10000786                                              *
*            New Program                                                *
*                                                                       *
* 08/10/02 - BG_100002295
*            Call SUBROUTINE only if the tag routine is specified in
*            DE.I.SUBROUTINE.TABLE.

*
* 16/07/03 - CI_10010874
*            The dimension array R.MESSAGE was increased to
*            last field in the record, namely AUDIT.DATE.TIME
*            to aviod the system being fatalling out with
*            "Array subscript out of range" message.
*
* 05/01/04 - CI_10016284
*            Messages goes to repair if incoming 210 contains multiple sequence
*
* 27/02/07 - BG_100013037
*            CODE.REVIEW changes.
*
* 17/05/10 - Task 27812 / Defect 25860
*            Added additional fields to DE.I.HEADER to store the header, trailer, inward transaction ref
*            and ofs request deatils id information.
*
* 25/02/15 - Enhancement 1265068/ Task 1265069
*          - Including $PACKAGE
*************************************************************************

    $INSERT I_COMMON
    $INSERT I_DEICOM
    $INSERT I_EQUATE
    $INSERT I_F.DE.FORMAT.SWIFT
    $INSERT I_F.DE.HEADER
    $INSERT I_F.DE.I.FT.TXN.TYPES
    $INSERT I_F.DE.MESSAGE
    $INSERT I_F.AC.EXPECTED.RECS
    $INSERT I_F.ER.PARAMETER
    $INSERT I_F.DE.I.SUBROUTINE.TABLE

    GOSUB INITIALISE

* Message Header Processing

    GOSUB VALIDATE.MESSAGE.TYPE

    GOSUB IDENTIFY.THE.SENDER

* Generic Body Processing

    CALL DE.GET.MSG.STRUCTURE(MESSAGE.TYPE,R.DE.I.MSG,FIELD.TAGS,MULTIPLE.TAG,SEQUENCED.TAGS,SEQUENCED.MESSAGE,MAXIMUM.REPEATS)


* Method 2 - To generate multiple application records with the same core data and changing sequence data (use this OR method 1)

    TAG.FIELD.COUNT = DCOUNT(SEQUENCED.MESSAGE,@FM)
    TAG.VAL.COUNT = MAXIMUM.REPEATS<1>

    FOR TAG.VAL.NO = 1 TO TAG.VAL.COUNT

        TAG.SUB.COUNT = 0

        FOR TAG.FIELD.NO = 1 TO TAG.FIELD.COUNT
            FIELD.SUB.VALS = DCOUNT(SEQUENCED.MESSAGE<TAG.FIELD.NO,TAG.VAL.NO>,@SM)
            IF FIELD.SUB.VALS > TAG.SUB.COUNT THEN
                TAG.SUB.COUNT = FIELD.SUB.VALS
            END
        NEXT TAG.FIELD.NO

        FOR TAG.SUB.NO = 1 TO TAG.SUB.COUNT

            GOSUB PROCESS.EACH.TAG      ;* BG_100013037 - S / E

            GOSUB ADD.NON.TAG.FIELDS    ;* Specific Application Record Processing

            CALL OFS.GLOBUS.MANAGER(K.OFS.SOURCE,R.OFS.DATA)
            GOSUB GET.INFO    ;*Get the trans ref, ofs req details id to update DE.I.HEADER

        NEXT TAG.SUB.NO

        R.OFS.DATA = ''
        R.OFS.DATA = K.VERSION:"/I,,,"

    NEXT TAG.VAL.NO

* End of Method 2

* Further methods may be added here if a specific message-transaction scenarios require them
    R.HEAD(DE.HDR.OFS.REQ.DET.KEY) = OFS.REQ.DET.ID         ;* Store the ofs request details id
    R.HEAD(DE.HDR.T24.INW.TRANS.REF) = T24.TRANS.REF        ;* Inward T24 trans ref
    R.HEAD(DE.HDR.DISPOSITION) = 'OFS FORMATTED'
    MATWRITE R.HEAD TO F.DE.I.HEADER, R.KEY

    APPLICATION = TEMP.APPLICATION

    RETURN          ;* From main program

************************************************************************
GET.INFO:
* Get the transaction ref and ofs.req detail id

    T24.TRANS.REF<1,-1> = FIELD(R.OFS.DATA,'/',1) ;* Get the inward trans ref
    OFS.REQ.DET.ID<1,-1> = FIELD(R.OFS.DATA,'/',2)          ;* Get the ofs request detail id

    RETURN

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

VALIDATE.MESSAGE.TYPE:

* Check if the message is valid type and retrieve message format information

    MESSAGE.TYPE = R.HEAD(DE.HDR.MESSAGE.TYPE)
    IF MESSAGE.TYPE NE '210' THEN       ;* Input the type for this template
        MESSAGE.ERROR = 'Trying to process message ':MESSAGE.TYPE:' in message template MT210'
        GOSUB HOLD.ON.ERROR
    END

    CALL F.READ(FN.DE.FORMAT.SWIFT,'210.1.1',R.DE.FORMAT.SWIFT,F.DE.FORMAT.SWIFT,READ.ERROR)

    IF READ.ERROR THEN
        MESSAGE.ERROR = 'Message not found in DE.FORMAT.SWIFT FILE'
        GOSUB HOLD.ON.ERROR
    END

    RETURN

*************************************************************************

IDENTIFY.THE.SENDER:

* Check if the sender is a customer

    SENDERS.BIC.CODE = SUBSTRINGS(R.HEAD(DE.HDR.FROM.ADDRESS),1,11)

    CALL DE.SWIFT.BIC(SENDERS.BIC.CODE,ID.COMPANY,SENDING.CUSTOMER)

    RETURN

*************************************************************************

HOLD.ON.ERROR:

* Processing when an error is found in the message

*   R.OFS.DATA := 'INBOUND.ERROR,':ERROR.COUNT:'=':MESSAGE.ERROR:','

*   ERROR.COUNT = ERROR.COUNT + 1

* MESSAGE.ERROR = ''

    RETURN

*************************************************************************

INITIALISE:

* Initialise variables

    FIELD.TO.FIND = ''
    FIELD.TO.DEFAULT = ''
    SWIFT.TAG.NO = ''
    SWIFT.TAG.DATA = ''
    MESSAGE.ERROR = ''
    ERROR.COUNT = 1
    DIM R.MESSAGE(DE.MSG.AUDIT.DATE.TIME)         ;*      CI_10010874 -  S/E
* CI_10016284 S
    DE.I.ALL.FIELD.DATA = ''
    TXN.REFERENCE = ''
    VAL.DATE = ''
    ACCT.ID = ''
    OFS.REQ.DET.ID = ''
    T24.TRANS.REF = ''
* CI_10016284 E
* Open Files

    FN.ACCOUNT = "F.ACCOUNT"
    F.ACCOUNT = ""
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    FN.DE.I.MSG = "F.DE.I.MSG"
    F.DE.I.MSG = ""
    CALL OPF(FN.DE.I.MSG,F.DE.I.MSG)

    FN.DE.MESSAGE = "F.DE.MESSAGE"
    F.DE.MESSAGE=""
    CALL OPF(FN.DE.MESSAGE,F.DE.MESSAGE)

    FN.DE.FORMAT.SWIFT = "F.DE.FORMAT.SWIFT"
    F.DE.FORMAT.SWIFT=""
    CALL OPF(FN.DE.FORMAT.SWIFT,F.DE.FORMAT.SWIFT)

    FN.DE.I.FT.TXN.TYPES = "F.DE.I.FT.TXN.TYPES"
    F.DE.I.FT.TXN.TYPES=""
    CALL OPF(FN.DE.I.FT.TXN.TYPES,F.DE.I.FT.TXN.TYPES)

    FN.DE.I.SUBROUTINE.TABLE = "F.DE.I.SUBROUTINE.TABLE"
    F.DE.I.SUBROUTINE.TABLE=""
    CALL OPF(FN.DE.I.SUBROUTINE.TABLE,F.DE.I.SUBROUTINE.TABLE)

    R.DE.I.MSG = ''
    CALL F.READ(FN.DE.I.MSG,R.KEY,R.DE.I.MSG,F.DE.I.MSG,E)

    R.DE.MESSAGE = ''
    CALL F.READ(FN.DE.MESSAGE,R.HEAD(DE.HDR.MESSAGE.TYPE),R.DE.MESSAGE,F.DE.MESSAGE,E)

    FN.ER.PARAMETER = "F.ER.PARAMETER"
    F.ER.PARAMETER = ""
    CALL OPF(FN.ER.PARAMETER,F.ER.PARAMETER)

    F.OFS.SOURCE = ""
    FN.OFS.SOURCE = "F.OFS.SOURCE"
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)


    R.ER.PARAMETER = ""
    YERR = ''
    ER.PARAMETER.ID = 'SYSTEM'
    CALL F.READ(FN.ER.PARAMETER, ER.PARAMETER.ID,R.ER.PARAMETER,F.ER.PARAMETER,YERR)
    IF YERR NE '' THEN
        TEXT = 'READ FAILED FILE ER.PARAMETER REC SYSTEM'
        RETURN
    END

* Checks for Integrity

    CALL F.MATREAD(FN.DE.MESSAGE,
    R.HEAD(DE.HDR.MESSAGE.TYPE),
    MAT R.MESSAGE,
    DE.MSG.AUDIT.DATE.TIME,
    F.DE.MESSAGE,
    ER)

    IF ER THEN
        R.HEAD(DE.HDR.ERROR.CODE)="Message type does not exist"
        GOSUB WRITE.REPAIR
        RETURN
    END

    OFS.ID = R.MESSAGE(DE.MSG.OFS.SOURCE)         ;*Ofs Source File ID...
    R.OFS.SOURCE=""


    IF OFS.ID = '' THEN
        R.HEAD(DE.HDR.ERROR.CODE)= "OFS.SOURCE field on DE.MESSAGE blank"
        GOSUB WRITE.REPAIR
        RETURN
    END

    OFS$SOURCE.REC=R.OFS.SOURCE         ;*Set up Common OFS.SOURCE Record...
    OFS$SOURCE.ID=OFS.ID      ;*Set up Common OFS.ID...

    CALL F.READ(FN.OFS.SOURCE,OFS.ID,R.OFS.SOURCE,F.OFS.SOURCE,ER)

    IF ER THEN
        R.HEAD(DE.HDR.ERROR.CODE)="OFS ID ":OFS.ID:" DOES NOT EXIST IN OFS.SOURCE"
        GOSUB WRITE.REPAIR
        RETURN
    END



    K.VERSION = R.DE.MESSAGE<DE.MSG.IN.OFS.VERSION>
    K.OFS.SOURCE = R.DE.MESSAGE<DE.MSG.OFS.SOURCE>

    TEMP.APPLICATION = APPLICATION
    APPLICATION = FIELD(K.VERSION,",",1)

    R.OFS.DATA = K.VERSION:"/I,,,"

    RETURN


*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

ADD.NON.TAG.FIELDS:

* Complete any fields not directly populated from input Tags
* CI_10016284 S
    IF TAG.VAL.NO = 1 THEN
        GOSUB STORE.MAIN.SEQ.INFO
    END ELSE
        IF TXN.REFERENCE THEN
            R.OFS.DATA := 'REFERENCE:1:1':'=':QUOTE(TXN.REFERENCE):','
        END
        IF ACCT.ID THEN
            R.OFS.DATA := 'ACCOUNT.ID':'=':ACCT.ID:','
        END
        IF VAL.DATE THEN
            R.OFS.DATA := 'VALUE.DATE':'=':VAL.DATE:','
        END
    END
* CI_10016284 E

    R.OFS.DATA := 'DATE.ENTERED':'=':TODAY:','
    R.OFS.DATA := 'DESCRIPTION':'=':'AUTO':','
    R.OFS.DATA := 'FUNDS.TYPE':'=':'ER':','
    R.OFS.DATA := 'DELIVERY.IN.REF=':R.KEY:','

    RETURN

**************************************************************************

WRITE.REPAIR:


    R.HEAD(DE.HDR.DISPOSITION)="REPAIR"
*

    R.REPAIR=R.KEY
    CALL DE.UPDATE.I.REPAIR(R.REPAIR,'')
*
    RETURN

*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
CALL.SUBROUTINE:
**************************************************************************
*
* Process each tag routine
*
* NOTE:  Store all the erros in the variable 'MESSAGE.ERROR' separated by FM
* so that all errors will be written in R.OFS.DATA at one shot before calling
* OFS.GLOBUS.MANAGER.

    IF DE.TAG.ID EQ '' THEN
        RETURN      ;* BG_100013037 - S
    END   ;* BG_100013037 - E

    R.DE.I.SUBROUTINE.TABLE = ''
    SET.ERROR = ''

    CALL F.READ(FN.DE.I.SUBROUTINE.TABLE,DE.TAG.ID,R.DE.I.SUBROUTINE.TABLE,F.DE.I.SUBROUTINE.TABLE, TAG.ERR)

    IF TAG.ERR THEN
        SET.ERROR = "TAG ROUTINE FOR ":DE.TAG.ID:" - MISSING"
    END ELSE

        SUBROUTINE.ID = R.DE.I.SUBROUTINE.TABLE<SR.TB.SUBROUTINE>
        OFS.DATA = ''
        COMPILED.OR.NOT = ''
        DE.I.FIELD.DATA = ''  ;* CI_10016284 S/E
        CALL CHECK.ROUTINE.EXIST(SUBROUTINE.ID, COMPILED.OR.NOT, R.ERR)

        IF NOT(COMPILED.OR.NOT) THEN
            SET.ERROR = "SUBROUTINE FOR TAG ":DE.TAG.ID:" NOT COMPILED"
        END ELSE
            CALL @SUBROUTINE.ID (SEQ.TAG.ID,DE.TAG.SEQ.MSG, OFS.DATA,SENDING.CUSTOMER,'','','', DE.I.FIELD.DATA, SET.ERROR)       ;* CI_10016284 S/E
            IF OFS.DATA NE '' THEN
                R.OFS.DATA := OFS.DATA:","
                DE.I.ALL.FIELD.DATA<-1> = DE.I.FIELD.DATA   ;* CI_10016284 S/E
            END
        END
    END

    IF SET.ERROR THEN
        MESSAGE.ERROR<-1> = SET.ERROR   ;* BG_100013037 - S
    END   ;* BG_100013037 - E

    RETURN
* CI_10016284 S
STORE.MAIN.SEQ.INFO:
    FIELD.TO.FIND = 'REFERENCE'
    GOSUB PROCESS.SEARCH.FIELD
    TXN.REFERENCE = FIELD.TO.SEARCH.DATA

    FIELD.TO.FIND = 'ACCOUNT.ID'
    GOSUB PROCESS.SEARCH.FIELD
    ACCT.ID = FIELD.TO.SEARCH.DATA

    FIELD.TO.FIND = 'VALUE.DATE'
    GOSUB PROCESS.SEARCH.FIELD
    VAL.DATE = FIELD.TO.SEARCH.DATA
    RETURN

PROCESS.SEARCH.FIELD:
    FIELD.TO.SEARCH.DATA = ''
    FINDSTR FIELD.TO.FIND IN DE.I.ALL.FIELD.DATA SETTING FMS,VMS THEN
        FIELD.TO.SEARCH.DATA = FIELD( DE.I.ALL.FIELD.DATA<FMS>,CHARX(251),2)
        CONVERT VM TO FM IN FIELD.TO.SEARCH.DATA
    END
    RETURN
* CI_10016284 E
*************************************************************************************************************
* BG_100013037 - S
*================
PROCESS.EACH.TAG:
*================
    FOR TAG.FIELD.NO = 1 TO TAG.FIELD.COUNT

        FIELD.VALS = DCOUNT(SEQUENCED.MESSAGE<TAG.FIELD.NO>,@VM)
        IF FIELD.VALS < TAG.VAL.NO THEN
            TAG.VAL.IDX = FIELD.VALS
        END ELSE
            TAG.VAL.IDX = TAG.VAL.NO
        END
        FIELD.SUBS = DCOUNT(SEQUENCED.MESSAGE<TAG.FIELD.NO,TAG.VAL.IDX>,@SM)
        IF FIELD.SUBS < TAG.SUB.NO THEN
            TAG.SUB.IDX = FIELD.SUBS
        END ELSE
            TAG.SUB.IDX = TAG.SUB.NO
        END
* CI_10016284 S
        BLANK.REPEAT.FIELD = 0
        IF MULTIPLE.TAG<TAG.FIELD.NO>[1,2] GT 0 THEN
            IF FIELD.VALS<TAG.VAL.NO OR FIELD.SUBS<TAG.SUB.NO THEN
                BLANK.REPEAT.FIELD = 1
            END
        END
* The values of single repetitive sequcene field should be handled within
* the tag routine and it should be separated by VM s.

        MULTIPLE.FIELD.NO = MULTIPLE.TAG<TAG.FIELD.NO>
        IF MULTIPLE.FIELD.NO[1,1] = 'R' THEN
            DE.TAG.ID = SEQUENCED.TAGS<TAG.FIELD.NO>[1,2]
            SEQ.TAG.ID = SEQUENCED.TAGS<TAG.FIELD.NO>
            DE.TAG.SEQ.MSG = SEQUENCED.MESSAGE<TAG.FIELD.NO,TAG.VAL.IDX>
            CONVERT SM TO VM IN DE.TAG.SEQ.MSG

            GOSUB CALL.SUBROUTINE
        END ELSE

*                IF SEQUENCED.MESSAGE<TAG.FIELD.NO,TAG.VAL.IDX,TAG.SUB.IDX> NE '' THEN
            IF SEQUENCED.MESSAGE<TAG.FIELD.NO,TAG.VAL.IDX,TAG.SUB.IDX> NE '' AND BLANK.REPEAT.FIELD = 0 THEN
*                   DE.TAG.ID = SEQUENCED.TAGS<TAG.FIELD.NO>[1,2]
                SEQ.TAG.ID = SEQUENCED.TAGS<TAG.FIELD.NO,TAG.VAL.IDX,TAG.SUB.IDX>
                DE.TAG.ID = SEQ.TAG.ID[1,2]
* CI_10016284 E
                DE.TAG.SEQ.MSG = SEQUENCED.MESSAGE<TAG.FIELD.NO,TAG.VAL.IDX,TAG.SUB.IDX>
                GOSUB CALL.SUBROUTINE   ;* BG_100002295 S/E
            END     ;* CI_10016284 S/E
        END

    NEXT TAG.FIELD.NO
    RETURN          ;*BG_100013037 - E
*************************************************************************************************************
END

