*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE getIBANfromBIC(BIC.ID, AC.NO, INST.NAME, CTY.HD, IBAN.NAT.ID, RET.IBAN, RET.CD)
*---------------------------------------------------------------------------
*<doc>
* Enquiry routine that used for fetching the IBAN from the input BIC/INST.NAME and CITY.HEADING.
* @author tejomaddi@temenos.com
* @stereotype Application
* @IN_Config
* </doc>
*
****************************************************************************
*                  M O D I F I C A T I O N  H I S T O R Y                  *
****************************************************************************
*
* 01/06/12 - Enhancement 379826/SI 167927
*            Payments - Development for IBAN.
*
* 16/07/12 - Enahancement - 381897 / Task - 439575
*            This enquiry should work for thrid party accounts also.
*            So, check against account file is removed.
*
* 11/10/13 - Enhancement 785613 / Task 809936
*            Supporting IBAN Plus Directory (SWIFT 2013 changes), IBAN related information
*            is no more available in DE.BIC, changes done to get IBAN PLUS id in the enquiry
*
*---------------------------------------------------------------------------

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.IN.IBAN.STRUCTURE
    $INSERT I_F.DE.BIC
    $INSERT I_F.IN.IBAN.PLUS
    $INSERT I_F.ACCOUNT
    $INSERT I_DAS.DE.BIC
    $INSERT I_DAS.IN.IBAN.PLUS

    GOSUB DEFINE.PARAMETERS
    GOSUB PROCESS

    RETURN
*---------------------------------------------------------------------------
DEFINE.PARAMETERS:
******************

    F.BIC.IBAN.PLUS = ""
    R.BIC.IBAN.PLUS = ""
    BIC.READ.ERR = ""
    MOD.VALUE = ''
*
    IBAN.PLUS.ID = ''

    FN.IBAN.STR = "F.IN.IBAN.STRUCTURE"
    F.IBAN.STR = ""
    R.IBAN.STR = ""
    IBAN.STR.READ.ERR = ""
*
    IN.CNTRY.CD = ""
    IBAN.NO = ""
    ERR = ""
    COMP.CODE = ID.COMPANY

    FN.IN.IBAN.PLUS = 'F.IN.IBAN.PLUS'
    F.IN.IBAN.PLSU = ''
    CALL OPF(FN.IN.IBAN.PLUS, F.IN.IBAN.PLUS)

*
    RETURN



*---------------------------------------------------------------------------
PROCESS:
********
* Finding the Country code from the given bic.


    BEGIN CASE
        CASE IBAN.NAT.ID
            GOSUB GET.IBAN.PLUS.FROM.NID ; *

        CASE BIC.ID
            GOSUB GET.IBAN.PLUS.FROM.BIC ; *Read the concat file IN.BIC.IBAN.PLUS get the first field as the IBAN.PLUS record id

        CASE INST.NAME AND CTY.HD
            GOSUB GET.BIC
            GOSUB GET.IBAN.PLUS.FROM.BIC ; *Read the concat file IN.BIC.IBAN.PLUS get the first field as the IBAN.PLUS record id

        CASE OTHERWISE ;* Either of the above is required to find the country and national id so return
            RETURN
    END CASE

    GOSUB READ.RECORDS

    IF NOT(ETEXT) THEN
        GOSUB FORM.POSSIBLE.IBAN
    END

    RETURN
*---------------------------------------------------------------------------
READ.RECORDS:
*--*********
* Reading BIC record with the given BIC
*
    IBAN.PLUS.READ.ERR = ''
    R.IBAN.PLUS = ''
    F.IBAN.PLUS = ''
    CALL F.READ('F.IN.IBAN.PLUS', IBAN.PLUS.ID, R.IBAN.PLUS, F.IBAN.PLUS, IBAN.PLUS.READ.ERR)
    IF IBAN.PLUS.READ.ERR THEN
        ETEXT = "IN-INVALID.IBAN.PLUS.ENTERED"
        RETURN
    END

    IN.CNTRY.CD = R.IBAN.PLUS<IN.PL.IBAN.COUNTRY.CODE>

* Finding out the IBAN Structure file for this.
*

    CALL F.READ(FN.IBAN.STR, IN.CNTRY.CD, R.IBAN.STR, F.IBAN.STR, IBAN.STR.READ.ERR)
    IF IBAN.STR.READ.ERR THEN
        ETEXT = "IN-IBAN.STR.FILE.NOT.FOUND"
    END


    RETURN
*---------------------------------------------------------------------------
GET.BIC:
********
    THE.LIST = DAS.DE.BIC$FINDBICCODE
    THE.ARGS<1> = INST.NAME
    THE.ARGS<2> = CTY.HD
    TABLE.SUFFIX = ""
    CALL DAS("DE.BIC", THE.LIST, THE.ARGS, TABLE.SUFFIX)
    BIC.ID = THE.LIST<1>

    RETURN
*---------------------------------------------------------------------------
FORM.POSSIBLE.IBAN:
*******************

    ACCT.LEN = R.IBAN.STR<IN.IBAN.STR.AC.NUMBER.LEN>
    ACCT.FMT = "R%":ACCT.LEN
    ACCOUNT.NO = FMT(AC.NO, ACCT.FMT)

    POSSIBLE.BBAN = R.IBAN.PLUS<IN.PL.IBAN.NATIONAL.ID> : ACCOUNT.NO
    REARRANGED.IBAN = POSSIBLE.BBAN : IN.CNTRY.CD : '00'
    CALL CONV.ALPHA.TO.NUMERIC(REARRANGED.IBAN , MOD.VALUE)
    CHECK.DIGIT = 98 - MOD.VALUE

* IBAN is formed from COUNTRY.CODE, REMAINDER and BBAN

    RET.IBAN = IN.CNTRY.CD : FMT(CHECK.DIGIT,"R%2") : POSSIBLE.BBAN
    RET.IBAN := '*':IBAN.PLUS.ID

    RETURN
*-----------------------------------------------------------------------------

*** <region name= GET.IBAN.PLUS.FROM.BIC>
GET.IBAN.PLUS.FROM.BIC:
*** <desc>Read the concat file IN.BIC.IBAN.PLUS get the first field as the IBAN.PLUS record id </desc>

    CALL F.READ('F.IN.BIC.IBAN.PLUS', BIC.ID, R.BIC.IBAN.PLUS, F.BIC.IBAN.PLUS, BIC.READ.ERR)
    IF NOT(R.BIC.IBAN.PLUS) THEN
        ETEXT = "IN-INVALID.BIC.ENTERED"
        RETURN
    END ELSE
        IBAN.PLUS.ID = R.BIC.IBAN.PLUS<1>
    END

    RETURN
*** </region>
*-----------------------------------------------------------------------------

*** <region name= GET.IBAN.PLUS.FROM.NID>
GET.IBAN.PLUS.FROM.NID:
*** <desc> </desc>
* Get the IBAN Plus record id passing the given national id

    THE.LIST = dasInIbanPlus$FIND.IBAN.PLUS.FROM.NID
    THE.ARGS = ''
    THE.ARGS<1> = IBAN.NAT.ID
    TABLE.SUFFIX = ""
    CALL DAS("IN.IBAN.PLUS", THE.LIST, THE.ARGS, TABLE.SUFFIX)

    IBAN.PLUS.ID = THE.LIST<1>

    RETURN
*** </region>
*-----------------------------------------------------------------------------

    END



