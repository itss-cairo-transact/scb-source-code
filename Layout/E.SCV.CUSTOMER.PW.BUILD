*-----------------------------------------------------------------------------
* <Rating>130</Rating>
*-----------------------------------------------------------------------------
    $PACKAGE ST.ModelBank

    SUBROUTINE E.SCV.CUSTOMER.PW.BUILD(ENQ.DATA)
*
* Subroutine Type : BUILD Routine
* Attached to     : SCV.CUSTOMER.PW
* Attached as     : Build Routine
* Primary Purpose : We need a way of searching a customer based on any products held
*                   by the customer (like current account or a deposit or a loan) or
*                   even a Card number.
*                   Current account, deposit and loan are all in ACCOUNT table. Card number
*                   is in CARD.ISSUE
*                   Optionally we should also be able to link the photo of the customer (from
*                   IM.DOCUMENT.IMAGE.
*                   There are 4 new S type I-descs added to CUSTOMER table namely ACCOUNT.NO,
*                   CARD.NO and INCLUDE.IMAGE and IM.DOCUMENT.IMAGE
*                   It is important that the Operand for all these three I-Descs should always be
*                   EQ.
*                   Based on the account number or card number, try to get the customer number
*                   and add it as a selection criteria to ENQ.DATA which core will use to
*                   select CUSTOMER table with.
*                   'S' type i-desc are not used by core for selection. But provide a way
*                   of supplying data to routines like this one without intruding core
*                    processing.
*
* Incoming:
* ---------
*
*
* Outgoing:
* ---------
*
*
* Error Variables:
* ----------------
*
*
*-----------------------------------------------------------------------------------
* Modification History:
*
* 28 OCT 2010 - Sathish PS
*               New Development for RMB1 SI
*
* 22 NOV 2010 - Included additional requirement to fetch details when Portfolio No
*               is given as selection
*
*-----------------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE

    $INSERT I_GTS.COMMON
    $INSERT I_ENQUIRY.COMMON

    $INSERT I_F.ACCOUNT
    $INSERT I_F.CARD.ISSUE

    GOSUB INITIALISE
    GOSUB OPEN.FILES
    GOSUB CHECK.PRELIM.CONDITIONS
    IF PROCESS.GOAHEAD THEN
        GOSUB PROCESS
    END

    RETURN          ;* Program RETURN
*-----------------------------------------------------------------------------------
PROCESS:

    IF CUSTOMER.NO THEN
        GOSUB ADD.CUSTOMER.NO.TO.SELECTION
    END

    RETURN          ;* from PROCESS
*-----------------------------------------------------------------------------------
ADD.CUSTOMER.NO.TO.SELECTION:

    LOCATE 'CUSTOMER.CODE' IN ENQ.DATA<2,1> SETTING CUS.CODE.POS THEN
        IF ENQ.DATA<4,CUS.CODE.POS> NE CUSTOMER.NO THEN
            ENQ.ERROR = 'EB-RMB1.CUSTOMER.CODE.MISMATCH'
            ENQ.ERROR<2,1> = ENQ.DATA<4,CUS.CODE.POS>
            ENQ.ERROR<2,2> = CUSTOMER.NO
        END
    END

    IF NOT(ENQ.ERROR) THEN
        ENQ.DATA<2,-1> = 'CUSTOMER.CODE'
        ENQ.DATA<3,-1> = 'EQ'
        ENQ.DATA<4,-1> = CUSTOMER.NO
    END

    RETURN
*-----------------------------------------------------------------------------------
* <New Subroutines>

* </New Subroutines>
*-----------------------------------------------------------------------------------*
*//////////////////////////////////////////////////////////////////////////////////*
*////////////////P R E  P R O C E S S  S U B R O U T I N E S //////////////////////*
*//////////////////////////////////////////////////////////////////////////////////*
INITIALISE:

    PROCESS.GOAHEAD = 1
    SEL.FIELDS := VM: 'ACCOUNT.NO' :VM: 'LOAN.NO' :VM: 'DEPOSIT.NO'
    SEL.FIELDS := VM: 'CARD.NO'
    CUSTOMER.NO = ''

    RETURN          ;* From INITIALISE
*-----------------------------------------------------------------------------------
OPEN.FILES:

    FN.AC = 'F.ACCOUNT' ; F.AC = ''
    FN.CUS = 'F.CUSTOMER' ; F.CUS = ''
    FN.CI = 'F.CARD.ISSUE' ; F.CI = ''
    CALL OPF(FN.CI,F.CI)

    RETURN          ;* From OPEN.FILES
*-----------------------------------------------------------------------------------
CHECK.PRELIM.CONDITIONS:
*
    LOOP.CNT = 1 ; MAX.LOOPS = 3
    LOOP
    WHILE LOOP.CNT LE MAX.LOOPS AND PROCESS.GOAHEAD DO

        BEGIN CASE
        CASE LOOP.CNT EQ 1
            LOCATE 'ACCOUNT.NO' IN ENQ.DATA<2,1> SETTING AC.NO.POS THEN
                IF ENQ.DATA<3,AC.NO.POS> NE 'EQ' THEN
                    ENQ.ERROR = 'EB-RMB1.ALLOWED.OPERAND.FOR.ACCOUNT.NO'
                END ELSE
                     ACCOUNT.NO = ENQ.DATA<4,AC.NO.POS>
                    GOSUB GET.CUSTOMER.NO.FROM.ACCOUNT.NO
                END
            END

        CASE LOOP.CNT EQ 2
            LOCATE 'CARD.NO' IN ENQ.DATA<2,1> SETTING CARD.NO.POS THEN
                IF ENQ.DATA<3,CARD.NO.POS> NE 'EQ' THEN
                    ENQ.ERROR = 'EB-RM1.ALLOWED.OPERAND.FOR.CARD.NO'
                END ELSE
                    CARD.NO = ENQ.DATA<4,CARD.NO.POS>
                    GOSUB GET.CUSTOMER.NO.FROM.CARD.NO
                    IF NOT(ENQ.ERROR) THEN
                        ENQ.DATA<4,CARD.NO.POS> = CARD.NO   ;! The full Card Number including the Card Type as a Prefix
                    END
                END
            END

            CASE LOOP.CNT EQ 3
                LOCATE 'PORTFOLIO.NO' IN ENQ.DATA<2,1> SETTING PORTFOLIO.NO.POS THEN
                         PORTFOLIO.NO = ENQ.DATA<4,PORTFOLIO.NO.POS>
                         CUSTOMER.NO=FIELD(PORTFOLIO.NO,'-',1)
                  END

        END CASE
        LOOP.CNT += 1

        BEGIN CASE
        CASE ENQ.ERROR
            PROCESS.GOAHEAD = 0

        CASE CUSTOMER.NO
            BREAK

        END CASE

    REPEAT

    RETURN          ;* From CHECK.PRELIM.CONDITIONS
*-----------------------------------------------------------------------------------
GET.CUSTOMER.NO.FROM.ACCOUNT.NO:
!
! VAL.PROG will anyway be set to IN2.ALLACCVAL for this 'S' type field in SS... Check anyway
!
    IF NOT(NUM(ACCOUNT.NO)) THEN
        GOSUB GET.ACCOUNT.NO
    END
    IF NOT(ENQ.ERROR) THEN
        R.AC = '' ; ERR.AC = ''
        CALL F.READ(FN.AC,ACCOUNT.NO,R.AC,F.AC,ERR.AC)
        IF R.AC THEN
            CUSTOMER.NO = R.AC<AC.CUSTOMER>
        END ELSE
            ENQ.ERROR = 'EB-RMB1.INVALID.PRODUCT.REFERENCE'
        END
    END

    RETURN
*-----------------------------------------------------------------------------------
GET.ACCOUNT.NO:

    SAVE.COMI = COMI ; SAVE.ETEXT = ETEXT ; SAVE.DISPLAY = V$DISPLAY
    N1 = '35' ; T1 = '.ALLACCVAL'
    CALL IN2.ALLACCVAL(N1,T1)
    IF ETEXT THEN
        ENQ.ERROR = ETEXT
    END ELSE
        ACCOUNT.NO = COMI
    END
    COMI = SAVE.COMI ; ETEXT = SAVE.ETEXT ; V$DISPLAY = SAVE.DISPLAY

    RETURN
*-----------------------------------------------------------------------------------
GET.CUSTOMER.NO.FROM.CARD.NO:

    GOSUB SELECT.FROM.CARD.ISSUE
    IF NOT(ENQ.ERROR) THEN
        R.CI = '' ; ERR.CI = ''
        CALL F.READ(FN.CI,CARD.NO,R.CI,F.CI,ERR.CI)
        IF R.CI THEN
            ACCOUNT.NO = R.CI<CARD.IS.ACCOUNT,1>
            GOSUB GET.CUSTOMER.NO.FROM.ACCOUNT.NO
        END ELSE
            ENQ.ERROR = 'EB-RMB1.INVALID.CARD.NO'
        END
    END

    RETURN
*-----------------------------------------------------------------------------------
SELECT.FROM.CARD.ISSUE:

    SEL.CMD = 'SELECT ':FN.CI
    SEL.CMD := ' LIKE ...':CARD.NO
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.SEL,RETURN.CODE)
    IF SEL.LIST THEN
        CARD.NO = SEL.LIST<1>
    END

    RETURN
*-----------------------------------------------------------------------------------
END

