*-----------------------------------------------------------------------------
* <Rating>607</Rating>
*-----------------------------------------------------------------------------
* Version 5 25/10/00  GLOBUS Release No. G11.0.00 29/06/00

    SUBROUTINE PC.PERIOD

* 05/10/01 - GLOBUS_BG_100000114
*            jBASE changes.
*            OPEN.SEQ with a ELSE clause in jbase is not
*            able to handle the condition of opening the PATH.
*            Soln is to add the ON ERROR clause along with the
*            ELSE clause.
*
* 21/09/02 - EN_10001196
*            Conversion of error messages to error codes.
*
* 31/10/03 - CI_10014225
*          - When using function "C", the PC.PERIOD is accepting
*          - Invalid Date
*
* 29/07/05 - BG_100009139
*            Added MB code.
*            In a MB env, only the lead companies are allowed to input
*            in the field COMPANY. When the lead company is input, PC.PERIOD.XREF
*            should be populated with book companies as well.
*            Ref: SAR-2005-05-06-0014
*
* 07/10/05 - CI_10035389 / REF:HD0514007
*          - When using version with Autom.fields, PC.PERIOD allows backdated record.
*
* 03/11/06 - BG_100012366
*            OPENSEQ is failing in jbase higher releases(4.1).Hence commenting out.
*
* 05/03/07 - EN_10003242
*            Modified to call DAS to select data.
*
* 14/02/08 - CI_10053712 (CSS REF:HD0802955)
*            In CHECK.FIELDS para PC.PCP.PERIOD.CLOSED.CANNOT.CHANGE.FURTHER is changed to
*            PC.PCP.PERIOD.CLOSED.CANT.CHANGE.FURTHER
*            Since both the error codes contain similar error message.
*
* 23/07/13 - Defect 730171 / Task 737686
*            Validation for the field DBASE.PATH has be modifed to accept any valid file
*            or a path (No \ or / are expected).
*
* 22/03/14 - Defect 907468 / Task 948779
*           When PC.PERIOD is created for a lead company with books from a lead company
*           without books the PC.PERIOD.XREF file gets updated only for the lead company
*           not for the book companies
*
* 12/01/15 - Enhancement - 1163835/Task  1215789
*            To Add Local Reference field.
*
*************************************************************************
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.PC.PERIOD
    $INSERT I_F.PC.PERIOD.XREF
    $INSERT I_F.PC.STMT.ADJUSTMENT
    $INSERT I_F.PC.CATEG.ADJUSTMENT
    $INSERT I_F.CATEG.ENTRY
    $INSERT I_F.STMT.ENTRY
    $INSERT I_F.DATES
    $INSERT I_F.COMPANY
    $INSERT I_F.COMPANY.CHECK
    $INSERT I_DAS.COMMON
    $INSERT I_DAS.PC.PERIOD
    $INSERT I_DAS.PC.STMT.ADJUSTMENT
    $INSERT I_DAS.PC.CATEG.ADJUSTMENT
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        RETURN
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            CALL RECORD.READ

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN
                CALL ERR
                CONTINUE ;* Continue to get the new id
            END

            IF MESSAGE EQ 'REPEAT' THEN
                CONTINUE ;* Continue to get the new id
            END

            CALL MATRIX.ALTER

            REM >       GOSUB CHECK.RECORD              ;* Special Editing of Record
            REM >       IF ERROR THEN GOTO MAIN.REPEAT

            REM >       GOSUB PROCESS.DISPLAY           ;* For Display applications

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN
            T.SEQU<-1> = A + 1
        END

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            REM >       GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            REM >       GOSUB CROSS.VALIDATION
            REM >       IF NOT(ERROR) THEN
            REM >          GOSUB DELIVERY.PREVIEW
            REM >       END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                REM >          GOSUB CHECK.DELETE              ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                REM >          GOSUB CHECK.REVERSAL            ;* Special Reversal checks
            CASE OTHERWISE
                REM >          GOSUB CROSS.VALIDATION          ;* Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        REM >    IF NOT(ERROR) THEN
        REM >       GOSUB BEFORE.UNAU.WRITE         ;* Special Processing before write
        REM >    END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            REM >       IF MESSAGE NE "ERROR" THEN
            REM >          GOSUB AFTER.UNAU.WRITE          ;* Special Processing after write
            REM >       END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        REM >    GOSUB AUTH.CROSS.VALIDATION          ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            REM >            IF MESSAGE NE "ERROR" THEN
            REM >               GOSUB AFTER.AUTH.WRITE
            REM >            END
        END

    END

    RETURN

*************************************************************************

PROCESS.DISPLAY:

* Display the record fields.

    IF SCREEN.MODE EQ 'MULTI' THEN
        CALL FIELD.MULTI.DISPLAY
    END ELSE
        CALL FIELD.DISPLAY
    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************
*
CHECK.ID:

    IF NOT(ID.OLD) AND V$FUNCTION <> 'S' THEN
        IF ID.NEW NE R.DATES(EB.DAT.PERIOD.END) THEN
            E ='PC.PCP.NEW.PERIOD':FM:R.DATES(EB.DAT.PERIOD.END)
        END
    END

    IF E THEN
        V$ERROR = 1
    END
*
    RETURN
*
*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


    RETURN

*************************************************************************

CHECK.FIELDS:

    BEGIN CASE
        CASE AF = PC.PER.PERIOD.STATUS
            COMP.FLG = '' ; COMP.ID = ''
            IF NOT(R.OLD(PC.PER.PERIOD.STATUS)) AND  COMI = 'CLOSED' THEN
                E ='PC.PCP.STATUS.DISALLOWED.WHEN.DEFINING.NEW.PERIOD'
            END

            IF R.OLD(PC.PER.PERIOD.STATUS) = 'OPEN' THEN

                P.LIST       = dasPcPeriodOpenIdLtById
                THE.ARGS     = ID.NEW
                TABLE.SUFFIX = ''
                CALL DAS('PC.PERIOD',P.LIST,THE.ARGS,TABLE.SUFFIX)
                PCNT = DCOUNT(P.LIST, FM)

                IF P.LIST NE '' THEN
                    E = 'PC.PCP.PERIOD/S.PRIOR.STILL.OPEN':FM:ID.NEW
                END ELSE

                    * check for outstanding entries that have not been posted
                    * must check both stmt and categ adjustment files.

                    GOSUB SETUP.POST.CHECK
                END
            END

            * GB0002245 S
            IF R.OLD(PC.PER.PERIOD.STATUS) = 'CLOSED' THEN
                IF COMI AND (COMI NE R.OLD(AF)) THEN
                    * GB0002245 E
                    E ='PC.PCP.PERIOD.CLOSED.CANT.CHANGE.FURTHER'
                END
            END

        CASE AF = PC.PER.COMPANY
            E = ''

            IF NOT(COMI) THEN ;* If no value nothing to validate
                RETURN
            END

            ERR = ''
            CALL CACHE.READ('F.COMPANY',COMI,R.COMP,ERR)
            IF ERR THEN
                ERR = ''
                E = 'PC.PCP.INVALID.COMP'
                RETURN
            END

            LEAD.CO.CODE = R.COMP<EB.COM.FINANCIAL.COM>
            IF C$MULTI.BOOK AND LEAD.CO.CODE <> COMI THEN
                E = 'PC.PCP.MUST.BE.LEAD.COMP'
            END

            LOCATE  COMI IN R.NEW(PC.PER.COMPANY)<1,1> SETTING YUP THEN
            E ='PC.PCP.YOU.HAVE.ENT.COMP.ALRDY'
        END

    CASE AF = PC.PER.COMP.STATUS
        COMP.FLG = '' ; COMP.ID = ''
        * GB0002245 S
        IF R.OLD(PC.PER.PERIOD.STATUS) = 'CLOSED' THEN
            IF COMI AND (COMI NE R.OLD(AF)) THEN
                E ='PC.PCP.PERIOD.CLOSED.CANT.CHANGE.FURTHER'
                RETURN
            END
            * GB0002245 E
        END

        IF NOT(R.OLD(PC.PER.COMP.STATUS)) THEN
            IF COMI = 'CLOSED' THEN
                E ='PC.PCP.STATUS.DISALLOWED.WHEN.DEFINING.NEW.PERIOD'
            END
        END ELSE
            IF R.OLD(PC.PER.COMP.STATUS)<1,AV> = 'OPEN' THEN
                COMP.FLG = 1
                COMP.ID = R.OLD(PC.PER.COMPANY)<1,AV>
                GOSUB SETUP.POST.CHECK
            END
        END

    CASE AF = PC.PER.DBASE.PATHNAME
        * GB0002245 S
        GOSUB VALIDATE.DB.PATH ; ** Validate weather the entered path is valid
    END CASE

    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

    RETURN

*************************************************************************

VALIDATE.DB.PATH:
* Validate weather the entered path is valid

    IF R.OLD(PC.PER.PERIOD.STATUS) = 'CLOSED' THEN
        IF COMI AND (COMI NE R.OLD(AF)) THEN
            E ='PC.PCP.PERIOD.CLOSED.CANT.CHANGE.FURTHER'
        END
    END

** GB0002245 E

    VALID.PATH = 1
    FLD.DATA = COMI
    CONVERT '\' TO '/' IN FLD.DATA

* In Relational data base, PC Data base path will not contain any '/' or '\', so condition to check NO.SEP is removed.
* OPENPATH will open any given absolute or relative path
* e.g. 1. 'BP'
*      2. '../../bnk.data'
*      3. 'C:/Temenos/bnk.data'
* Condition modified to support all the data base, just to check whether the entered path or file is valid.

    OPENPATH FLD.DATA TO TEMP.OPEN ELSE
    E ='PC.PCP.INVALID.PATHNAME.SPECIFIED'
    END
*OPENSEQ FLD.DATA TO TEMP.OPEN ON ERROR FAILS = 1 ELSE FAILS = 1     ;* GLOBUS_BG_100000114 S/E

    RETURN

*************************************************************************

SETUP.POST.CHECK:

* Elaborate check to see whether there are postings outstanding
* that have not been applied to the PC database yet
*  .eg Given that ....
* a single posting made to period 20000131 , will bear impact on all
* open periods after and including this period , one cannot close
* a company , or a period which this posting affects , until that
* posting has been applied to the PC database else we land up with
* a situation where a single posting may have been applied to one
* period or company , and not to another hence chaos

    FNAME = '' ; FNAME.1 = '' ; FNAME.2 = '' ; FNAME.3 = ''
    FOR POST.CHK = 1 TO 2

        THE.ARGS = ID.NEW
        IF POST.CHK = 1 THEN
            FNAME = FN.PC.STMT.ADJUSTMENT
            FNAME.1 = F.PC.STMT.ADJUSTMENT
            FNAME.2 = FN.STMT.ENTRY
            FNAME.3 = F.STMT.ENTRY
            P.LIST = dasPcStmtAdjustmentStartPeriodLeById
            CALL DAS('PC.STMT.ADJUSTMENT',P.LIST,THE.ARGS,'')
        END ELSE
            FNAME = FN.PC.CATEG.ADJUSTMENT
            FNAME.1 = F.PC.CATEG.ADJUSTMENT
            FNAME.2 = FN.CATEG.ENTRY
            FNAME.3 = F.CATEG.ENTRY
            P.LIST = dasPcCategAdjustmentStartPeriodLeById
            CALL DAS('PC.CATEG.ADJUSTMENT',P.LIST,THE.ARGS,'')
        END

        * Select all entries in PC.STMT/CATEG.ADJUSTMENT with a period LE
        * the period in question , as these files are keyed with the period
        * that is FIRST impacted by the posting . Obviously , there may be
        * other periods as well , but we'll find this out once we get to
        * the actual entry in STMT/CATEG entry from the PC.PERIOD.END fld
        * As soon as we find a relevant unapplied posting , we display the
        * error and get out .

        PCNT = DCOUNT(P.LIST,FM)

        IF PCNT > 0 THEN
            GOSUB CHECK.POST.FLAG
            IF E THEN
                EXIT
            END
        END
    NEXT POST.CHK

    RETURN
*************************************************************************

CHECK.POST.FLAG:

    FOR CHK.LP = 1 TO PCNT
        CHK.ID = TRIM(FIELD(P.LIST<CHK.LP>,'-',3))
        CHK.REC = '' ; ERRTXT = ''
        * read entry from either stmt/categ entry
        CALL F.READ(FNAME.2,CHK.ID,CHK.REC,FNAME.3,ERRTXT)
        IF ERRTXT NE '' THEN
            TEXT = 'ERROR READING ':FNAME.2:' ID = ':CHK.ID
            CALL FATAL.ERROR('PC.PERIOD')
        END

        * Field positions for STMT.ENTRY and CATEG.ENTRY are same hence removed the file based conditions
        * condition CHK.REC<AC.STE.PC.APPLIED,CHK.ARR> NE 'Y' is common for period end and company specific check and taken as the prime condition
        FOR CHK.ARR = 1 TO DCOUNT(CHK.REC<AC.STE.PC.PERIOD.END>,@VM)
            IF CHK.REC<AC.STE.PC.PERIOD.END,CHK.ARR> <= ID.NEW THEN
                IF CHK.REC<AC.STE.PC.APPLIED,CHK.ARR> NE 'Y' THEN
                    IF NOT(COMP.FLG) THEN     ;* period close check
                        E = 'PC.PCP.UNAPPLIED.ADJUSTMENTS.PERIOD.EXIST':FM:CHK.REC<AC.STE.PC.PERIOD.END,CHK.ARR>
                        RETURN
                    END
                    * Once COMP.PLAG is not set the above condition will be satisfied and returned no need
                    * to but the company check as else case, thus avoiding one level of nesting

                    IF CHK.REC<AC.STE.COMPANY.CODE> = COMP.ID THEN  ;* company close check
                        E = 'PC.PCP.ADJUSTMENTS.TO.COMPANY':FM:COMP.ID:VM:CHK.REC<AC.STE.PC.PERIOD.END,CHK.ARR>
                        RETURN
                    END
                END
            END ELSE
                EXIT
            END
        NEXT CHK.ARR
    NEXT CHK.LP

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = 'D'
        T.SEQU<-1> = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END

    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
* Overrides go here

REM > ERROR = '' ; ETEXT = '' ; TEXT = ''
REM > CALL XX.OVERRIDE(CURR.NO)

REM >      IF TEXT = 'NO' THEN
REM >         ERROR = 1
REM >         MESSAGE = 'ERROR'
REM >      END

    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:

    RETURN

*************************************************************************

AFTER.AUTH.WRITE:

    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

* Auto populate or delete PC.PERIOD.XREF application , depending on
* the changes that have been applied to PC.PERIOD (current applic)

    PERIOD.ID = ID.NEW

    R.COMPANY.CHECK = ''

* Removed the multi book check because when a PC.PERIOD is created for a
* lead company with books from lead company without books the PC.PERIOD.XREF
* does not get updated for the book companies as the read was done only if
* the variable C$MULTI.BOOK is set. It will be set only when the lead company
* in which the user has logged in has book companies

    CALL F.READ('F.COMPANY.CHECK','FINANCIAL',R.COMPANY.CHECK,'',ERR)

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"
            * Either a new period , or company has been amended

            FOR CO.CNT = 1 TO DCOUNT(R.NEW(PC.PER.COMPANY),@VM)
                XREF.ID = R.NEW(PC.PER.COMPANY)<1,CO.CNT>
                BOOK.COMP.IDS = ''
                LOCATE XREF.ID IN R.COMPANY.CHECK<EB.COC.COMPANY.CODE,1> SETTING COMP.POS THEN
                BOOK.COMP.IDS = R.COMPANY.CHECK<EB.COC.USING.COM,COMP.POS>
                CHANGE SM TO FM IN BOOK.COMP.IDS
            END
            I = 0
            LOOP
            WHILE XREF.ID

                ERTXT = '' ; XREF.REC = ''
                CALL F.READ('F.PC.PERIOD.XREF',XREF.ID,XREF.REC,F.PC.PERIOD.XREF,ERTXT)
                IF NOT(ERTXT) THEN      ;* must be new period
                    LOCATE PERIOD.ID IN XREF.REC<PC.XREF.PERIOD.END,1> BY 'AL' SETTING FND ELSE
                    XREF.REC<PC.XREF.PERIOD.END> = INSERT(XREF.REC<PC.XREF.PERIOD.END>,1,FND,0,PERIOD.ID)
                END
            END ELSE      ;* entirely new company
                XREF.REC<PC.XREF.PERIOD.END,1> = PERIOD.ID
            END
            CALL F.WRITE('F.PC.PERIOD.XREF',XREF.ID,XREF.REC)
            I+=1
            XREF.ID = BOOK.COMP.IDS<I>
        REPEAT
    NEXT CO.CNT

    CASE R.NEW(V-8)[1,3] = "RNA"
* Period record reversed , thus amend PC.PERIOD.XREF accordingly

    FOR CO.CNT = 1 TO DCOUNT(R.NEW(PC.PER.COMPANY),@VM)
        XREF.ID = R.NEW(PC.PER.COMPANY)<1,CO.CNT>
        BOOK.COMP.IDS = ''
        LOCATE XREF.ID IN R.COMPANY.CHECK<EB.COC.COMPANY.CODE,1> SETTING COMP.POS THEN
        BOOK.COMP.IDS = R.COMPANY.CHECK<EB.COC.USING.COM,COMP.POS>
        CHANGE SM TO FM IN BOOK.COMP.IDS
    END
    I = 0
    LOOP
    WHILE XREF.ID
        ERTXT = '' ; XREF.REC = ''
        CALL F.READ('F.PC.PERIOD.XREF',XREF.ID,XREF.REC,F.PC.PERIOD.XREF,ERTXT)
        IF NOT(ERTXT) THEN
            LOCATE PERIOD.ID IN XREF.REC<PC.XREF.PERIOD.END,1> BY 'AL' SETTING FND THEN
            XREF.REC<PC.XREF.PERIOD.END> = DELETE(XREF.REC<PC.XREF.PERIOD.END>,1,FND,0)
        END
        CALL F.WRITE('F.PC.PERIOD.XREF',XREF.ID,XREF.REC)
    END
    I+=1
    XREF.ID = BOOK.COMP.IDS<I>
    REPEAT
    NEXT CO.CNT

    END CASE
*
* If there are any OVERRIDES a call to EXCEPTION.LOG should be made
*
* IF R.NEW(V-9) THEN
*    EXCEP.CODE = "110" ; EXCEP.MESSAGE = "OVERRIDE CONDITION"
*    GOSUB EXCEPTION.MESSAGE
* END
*

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E ='PC.PCP.FUNT.NOT.ALLOW.APP'
        CALL ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************
*
EXCEPTION.MESSAGE:
*
    CALL EXCEPTION.LOG("U",
    APP.CODE,
    APPLICATION,
    APPLICATION,
    EXCEP.CODE,
    "",
    FULL.FNAME,
    ID.NEW,
    R.NEW(V-7),
    EXCEP.MESSAGE,
    ACCT.OFFICER)
*
    RETURN

*************************************************************************

INITIALISE:

    GOSUB OPEN.XREF.PERIOD
    APP.CODE = ""   ;* Set to product code ; e.g FT, FX
    ACCT.OFFICER = ""         ;* Used in call to EXCEPTION. Should be relevant A/O
    EXCEP.CODE = ""

    RETURN

*************************************************************************

DEFINE.PARAMETERS:

    MAT F = "" ; MAT N = "" ; MAT T = "" ; ID.T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = ""
    Z = 0

    ID.F = 'PERIOD.END' ; ID.N = '11..C' ; ID.T<1> = 'D'

    Z += 1 ; F(Z) = "PERIOD.STATUS" ; N(Z) = "8.1.C" ; T(Z)<2> = 'OPEN_CLOSED'
    Z += 1 ; F(Z) = "XX<COMPANY" ; N(Z) = "10.1.C" ; T(Z)<1> = "COM"
    Z += 1 ; F(Z) = "XX>COMP.STATUS" ; N(Z) = '8.1.C' ; T(Z)<2> = 'OPEN_CLOSED'
    Z += 1 ; F(Z) = "DBASE.PATHNAME" ; N(Z) = '35.1.C' ; T(Z) = 'A'
    Z += 1 ; F(Z) = 'RESERVED.1' ; N(Z) = '' ; T(Z)<3> = 'NOINPUT'
    Z += 1 ; F(Z) = "XX.LOCAL.REF" ; N(Z) = "35" ; T(Z) = "A" ; T(Z)<3> = "NOINPUT"
    Z += 1 ; F(Z) = 'RESERVED.3' ; N(Z) = '' ; T(Z)<3> = 'NOINPUT'

    V = Z + 9 ; PREFIX = 'PC.PER'

    RETURN

OPEN.XREF.PERIOD:

    F.PC.PERIOD.XREF = ''
    CALL OPF('F.PC.PERIOD.XREF',F.PC.PERIOD.XREF)

    F.PC.PERIOD = ''
    FN.PC.PERIOD = 'F.PC.PERIOD'
    CALL OPF(FN.PC.PERIOD,F.PC.PERIOD)

    F.PC.STMT.ADJUSTMENT = ''
    FN.PC.STMT.ADJUSTMENT = 'F.PC.STMT.ADJUSTMENT'
    CALL OPF(FN.PC.STMT.ADJUSTMENT,F.PC.STMT.ADJUSTMENT)

    F.PC.CATEG.ADJUSTMENT = ''
    FN.PC.CATEG.ADJUSTMENT = 'F.PC.CATEG.ADJUSTMENT'
    CALL OPF(FN.PC.CATEG.ADJUSTMENT,F.PC.CATEG.ADJUSTMENT)

    F.STMT.ENTRY = ''
    FN.STMT.ENTRY = 'F.STMT.ENTRY'
    CALL OPF(FN.STMT.ENTRY,F.STMT.ENTRY)

    F.CATEG.ENTRY = ''
    FN.CATEG.ENTRY = 'F.CATEG.ENTRY'
    CALL OPF(FN.CATEG.ENTRY,F.CATEG.ENTRY)

    RETURN

*************************************************************************

    END

