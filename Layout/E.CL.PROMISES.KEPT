*-----------------------------------------------------------------------------
* <Rating>34</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.CL.PROMISES.KEPT(ENQ.LIST)
*** <region name= Description>
*** <desc>Purpose of the sub-routine</desc>
* Program Description
* This Enquiry used to display the collection item's which avialble in "KPTP".
*** <doc>
*
* @author johnson@temenos.com
* @stereotype template
* @uses NOFILE.CL.PROMISES.KEPT
* @package retaillending.CL
*
*** </doc>
*** <region name= Arguments>
*** <desc>Input and output arguments required for the sub-routine</desc>
* Arguments
*
* Input :
*
*
*
*
* Output
*
*  ENQ.LIST - Return the collection item's Details.
*
*** </region>
*** </region>
*** <region name= Modification History>
*** <desc>Changes done in the sub-routine</desc>
* Modification History :
*-----------------------------------------------------------------------------
* 11/04/14 -  ENHANCEMENT - 908020 /Task - 988392
*          -  Loan Collection Process
* ----------------------------------------------------------------------------
*** </region>
*-----------------------------------------------------------------------------
*** <region name= INSERTS>
*** <desc>File inserts and common variables used in the sub-routine</desc>
* Inserts

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.AA.ARRANGEMENT
    $INSERT I_F.USER
    $INSERT I_F.CL.COLLECTOR
    $INSERT I_F.CL.COLLECTOR.USER
    $INSERT I_F.CL.QUEUE
    $INSERT I_F.CL.COLLECTION.ITEM

  
    
*** </region>
*** <region name= Main section>
*** <desc>main control logic in the sub-routine</desc>    

    GOSUB INITIALISE
    GOSUB PROCESS

    RETURN
    
*** </region>

*** <region name= INITIALISE>
*** <desc>File variables and local variables</desc>    
*----------*
INITIALISE:
*----------*

    FN.CL.COLLECTOR = "F.CL.COLLECTOR"
    F.CL.COLLECTOR = ""
    CALL OPF(FN.CL.COLLECTOR,F.CL.COLLECTOR)

    FN.AA.ARRANGEMENT = 'F.AA.ARRANGEMENT'
    F.AA.ARRANGEMENT = ''
    CALL OPF(FN.AA.ARRANGEMENT,F.AA.ARRANGEMENT)

    FN.USER = 'F.USER'
    F.USER = ''
    CALL OPF(FN.USER,F.USER)

    FN.CL.QUEUE = "F.CL.QUEUE"
    F.CL.QUEUE = ""
    CALL OPF(FN.CL.QUEUE,F.CL.QUEUE)

    FN.CL.COLLECTION.ITEM = "F.CL.COLLECTION.ITEM"
    F.CL.COLLECTOR.ITEM = ""
    CALL OPF(FN.CL.COLLECTION.ITEM,F.CL.COLLECTOR.ITEM)

    FN.CL.COLLECTOR.USER = "F.CL.COLLECTOR.USER"
    F.CL.COLLECTOR.USER = ''
    CALL OPF(FN.CL.COLLECTOR.USER,F.CL.COLLECTOR.USER)

    ENQ.LIST = ""
    PREV.SELECTED = ""
    NOW.PROCESSED = ""
    DUP = ""

    RETURN
*** </region>    

*** <region name= PROCESS>
*** <desc>Main Process</desc>
    
*------*
PROCESS:
*------*
* Get the collector ID from the Enquiry input.

    GOSUB USERID
    IF NOT(NO.OF.RECS) THEN
        ENQ.ERROR = 'This user does not belong to collector'
        RETURN
    END

    LOCATE "COLLECTOR.ID" IN D.FIELDS<1> SETTING COLL.POS THEN
        COLL.ID= D.RANGE.AND.VALUE<COLL.POS>
    END ELSE
        COLL.ID = ""
    END

    R.CL.COLLECTOR = ""
    COLL.READ.ERR = ""
    CALL F.READ(FN.CL.COLLECTOR,COLL.ID,R.CL.COLLECTOR,F.CL.COLLECTOR,COLL.READ.ERR)
    IF COLL.READ.ERR NE '' AND COLL.ID NE "" THEN
        ENQ.ERROR = 'Invalid Colletcor ID'
        RETURN
    END
* Get the all Queues assigned to this collector.

    ENQ.LIST = ""
    QUEUES.LIST = R.CL.COLLECTOR<CL.COLL.ASSIGNED.QUEUES>
    NO.OF.QUEUES = DCOUNT(QUEUES.LIST,VM)

* This Para tries to get all the COLLECTION.ITEMs with QUEUE
* that are equal to the ASSIGNED to this collector.
    GOSUB GET.COLL.ITEM.PROCESS.ONE

    RETURN
    
*** </region>
 
 *** <region name= Selection Process>
*** <desc>Take Collection Item one by one to process</desc>
   
*------------------------*
GET.COLL.ITEM.PROCESS.ONE:
*------------------------*

    ALL.CMD = ""
    FOR QUEUE.CNT = 1 TO NO.OF.QUEUES
        QUEUE.ID = QUEUES.LIST<1,QUEUE.CNT>
        IF QUEUE.CNT NE NO.OF.QUEUES THEN
            ALL.CMD := " QUEUE EQ ":"'":QUEUE.ID:"'":" OR"
        END ELSE
            ALL.CMD := " QUEUE EQ ":"'":QUEUE.ID:"'"
        END
    NEXT QUEUE.CNT

* Sort using WEIGHT.

    IF ALL.CMD NE "" THEN
        SEL.CMD = "SELECT ":FN.CL.COLLECTION.ITEM:" WITH":ALL.CMD:" BY.DSND WEIGHT"
    END ELSE

        SEL.CMD = "SELECT ":FN.CL.COLLECTION.ITEM:" BY.DSND WEIGHT"

    END
    SEL.LIST = ""
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.RECS,SEL.ERR)
    LOOP
        REMOVE COLL.ITEM.ID FROM SEL.LIST SETTING MORE
    WHILE COLL.ITEM.ID:MORE
        GOSUB FORM.ARRAY
    REPEAT
    RETURN
*** </region>

 *** <region name= Array Form>
*** <desc>Form the Datas to Display</desc>

*---------*
FORM.ARRAY:
*---------*
    R.CL.COLLECTOR.ITEM = ""
    COLL.ITEM.READ.ERR = ""
    CALL F.READ(FN.CL.COLLECTION.ITEM,COLL.ITEM.ID,R.CL.COLLECTOR.ITEM,F.CL.COLLECTOR.ITEM,COLL.ITEM.READ.ERR)

    CURRENT.QUEUE = R.CL.COLLECTOR.ITEM<CL.CIT.QUEUE>
    PREV.QUEUE = R.CL.COLLECTOR.ITEM<CL.CIT.PREVIOUS.QUEUE>
    Y.CODE = R.CL.COLLECTOR.ITEM<CL.CIT.OUTCOME.CODE>
    Y.ACTION = R.CL.COLLECTOR.ITEM<CL.CIT.ACTION.CODE>
    IF CURRENT.QUEUE NE PREV.QUEUE AND PREV.QUEUE NE "" THEN
        RA.FLAG = "YES"
    END ELSE
        RA.FLAG = "NO"
    END
    Y.OUTCOME.DUE.DATE = R.CL.COLLECTOR.ITEM<CL.CIT.OUTCOME.DUE.DATE>
    Y.TODAY = TODAY
    Y.NO.OF.DAYS = "W"
    IF LEN(Y.OUTCOME.DUE.DATE) EQ "8" THEN
        CALL CDD("",Y.OUTCOME.DUE.DATE,Y.TODAY,Y.NO.OF.DAYS)
    END

    IF Y.NO.OF.DAYS LE 2 THEN
        IF (Y.CODE EQ 'KPTP') AND (Y.ACTION NE '') THEN
            NO.OF.PDS = DCOUNT(R.CL.COLLECTOR.ITEM<CL.CIT.UL.CONTRACT.REF>,VM)
            FOR PD.CNT = 1 TO NO.OF.PDS
                IF PD.CNT EQ 1 THEN
                    GOSUB ARRNG
                    CI.ARRAY = COLL.ITEM.ID:"*":R.CL.COLLECTOR.ITEM<CL.CIT.TOT.OVERDUE.AMT>:"*":R.CL.COLLECTOR.ITEM<CL.CIT.TOT.OUTSTDING.AMT>:"*"
                    CI.ARRAY := R.CL.COLLECTOR.ITEM<CL.CIT.NO.OF.DAYS.PD>:"*":Y.ACTION:"*"
                    CI.ARRAY := R.CL.COLLECTOR.ITEM<CL.CIT.ACTION.DATE>:"*":Y.CODE:"*"
                    CI.ARRAY := R.CL.COLLECTOR.ITEM<CL.CIT.COLLECTOR>:"*":CURRENT.QUEUE:"*":RA.FLAG:"*":R.CL.COLLECTOR.ITEM<CL.CIT.UL.CONTRACT.REF,PD.CNT>:"*":COLL.ID:"*"
                    CI.ARRAY := CONTRACT.ID:"*":ACCOUNT.NO:"*":PRODUCT:"*":R.CL.COLLECTOR.ITEM<CL.CIT.PTP.DATE,PD.CNT>:"*":R.CL.COLLECTOR.ITEM<CL.CIT.PTP.PAID.AMT,PD.CNT>
                END ELSE
                    GOSUB ARRNG
                    CI.ARRAY = "**********":R.CL.COLLECTOR.ITEM<CL.CIT.UL.CONTRACT.REF,PD.CNT>:"*":COLL.ID:"*"
                    CI.ARRAY := CONTRACT.ID:"*":ACCOUNT.NO:"*":PRODUCT:"*":R.CL.COLLECTOR.ITEM<CL.CIT.PTP.DATE,PD.CNT>:"*":R.CL.COLLECTOR.ITEM<CL.CIT.PTP.PAID.AMT,PD.CNT>
                END
                ENQ.LIST<-1> = CI.ARRAY
            NEXT PD.CNT
        END
    END
    RETURN

*** </region>

 *** <region name= Arrangement Record>
*** <desc>Take Arrangement Details</desc>

ARRNG:

    CONTRACT.ID = R.CL.COLLECTOR.ITEM<CL.CIT.UL.CONTRACT.REF,PD.CNT>

    CALL F.READ(FN.AA.ARRANGEMENT,CONTRACT.ID,R.AA.ARRANGEMENT,F.AA.ARRANGEMENT,AA.ERR)
    IF R.AA.ARRANGEMENT THEN
        ACCOUNT.NO = R.AA.ARRANGEMENT<AA.ARR.LINKED.APPL.ID>
        PRODUCT = R.AA.ARRANGEMENT<AA.ARR.PRODUCT>
    END

    RETURN
*** </region>

 *** <region name= Check Current Operator>
*** <desc>Check current operation is relevant to collector user</desc>

USERID:

    SEL.CMD = "SELECT ":FN.CL.COLLECTOR:" WITH COLLECTOR.USER EQ ":'"':OPERATOR:'"'
    SEL.LIST = ""
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.RECS,SEL.ERR)
    RETURN
    
*** </region>
END






















