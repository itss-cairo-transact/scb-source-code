*-----------------------------------------------------------------------------
* <Rating>-74</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.AA.GET.ACCT.BALANCE.MVMT(ACTIVITY.MVMT)

* For the given arrangement id and balance type, returns the balance movements by reading ACCT.BALANCE.ACTIVITY

* Parameters
* Out - ACTIVITY.MVMT - Returns the Movements in arrangement
*
*-----------------------------------------------------------------------------
*** <region name= Modification History>
*** <desc>Changes done in the sub-routine</desc>
* Modification History
*
* 12/01/11 - Task - 129174
*			 New routine to fetch movements of specific balance type of a arrangement.
*
*** </region>
*-----------------------------------------------------------------------------
*** <region name= Inserts>
*** <desc>File inserts and common variables used in the sub-routine</desc>
* Inserts
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.AA.ARRANGEMENT
    $INSERT I_F.AA.PROPERTY.CLASS
    $INSERT I_F.ACCT.ACTIVITY
    $INSERT I_F.EB.CONTRACT.BALANCES

*** </region>
*---------------------------------------------------------------------------
*** <region name= Main control>
*** <desc>Main control logic in the sub-routine</desc>

    GOSUB INITIALISE
    GOSUB PROCESS

    RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= Inilialise>
*** <desc>Initialise variables</desc>

INITIALISE:

    ACTIVITY.IDS = ''

    FN.EB.CONTRACT.BALANCES = 'F.EB.CONTRACT.BALANCES'
    F.EB.CONTRACT.BALANCES = ''
    CALL OPF(FN.EB.CONTRACT.BALANCES, F.EB.CONTRACT.BALANCES)

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= Main control>
*** <desc>Process validation</desc>
PROCESS:

    LOCATE "ARRANGEMENT.ID" IN D.FIELDS<1> SETTING ARR.POS ELSE
        RETURN
    END

    LOCATE "BALANCE.TYPE" IN D.FIELDS<1> SETTING BAL.POS ELSE
        RETURN
    END

    ACCT.YR = ''
    LOCATE "START.YEAR.MONTH" IN D.FIELDS<1> SETTING START.POS THEN
        START.YRMTH = D.RANGE.AND.VALUE<START.POS>
    END

    ACCT.MTH = ''
    LOCATE "END.YEAR.MONTH" IN D.FIELDS<1> SETTING END.POS THEN
        END.YRMTH = D.RANGE.AND.VALUE<END.POS>
    END
    
    BEGIN CASE
    CASE START.YRMTH AND NOT(END.YRMTH)
		END.YRMTH = START.YRMTH
		
	CASE NOT(START.YRMTH) AND END.YRMTH
		START.YRMTH = END.YRMTH

    CASE NOT(START.YRMTH) AND NOT(END.YRMTH) 
        START.YRMTH = TODAY[1,6]
        END.YRMTH = TODAY[1,6]
    END CASE

    ARR.ID = D.RANGE.AND.VALUE<ARR.POS>
    BAL.TYPE = D.RANGE.AND.VALUE<BAL.POS>

    CALL AA.GET.ARRANGEMENT(ARR.ID, R.ARR, ARR.ERR)

* Get the account number
    LOCATE "ACCOUNT" IN R.ARR<AA.ARR.LINKED.APPL,1> SETTING POS THEN
        ACCOUNT.NO = R.ARR<AA.ARR.LINKED.APPL.ID,POS>
    END

    GOSUB READ.BALANCES

    RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= Read Balances>
*** <desc> Read Balance of given balance type</desc>
READ.BALANCES:

    ACCT.ACTIVITY.REC = ''
    ACCT.ID = ACCOUNT.NO:'.':BAL.TYPE

    VALUE.OR.TRADE = "VALUE"

    GOSUB READ.ECB

    ACCT.IDX = 1
    LOOP
        ACTIVITY.ID = ACTIVITY.IDS<ACCT.IDX>
    WHILE ACTIVITY.ID
        CALL EB.GET.ACCT.ACTIVITY(ACCT.ID, "", ACTIVITY.ID, VALUE.OR.TRADE, "", ACCT.ACTIVITY.REC)

* Loop the each day in acct.activity to form dates
        PARSE.DATES = DCOUNT(ACCT.ACTIVITY.REC<1>,VM)
        FOR ADD.DATE = 1 TO PARSE.DATES
            ACCT.DATE = ACTIVITY.ID:ACCT.ACTIVITY.REC<1,ADD.DATE>
            GOSUB GET.BALANCES
        NEXT ADD.DATE

        ACCT.IDX += 1
    REPEAT

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= Get the activity record>
*** <desc>Get the activity movements</desc>
GET.BALANCES:

    REQUEST.TYPE = ''
    REQUEST.TYPE<2> = 'ALL'
    CALL AA.GET.PERIOD.BALANCES(ACCOUNT.NO, BAL.TYPE, REQUEST.TYPE, ACCT.DATE, "", "", BAL.DETAILS, "")

    IF BAL.DETAILS THEN
        ACTIVITY.MVMT<-1> = BAL.DETAILS<IC.ACT.DAY.NO>:"*":BAL.DETAILS<IC.ACT.TURNOVER.CREDIT>:"*":BAL.DETAILS<IC.ACT.TURNOVER.DEBIT>:"*":BAL.DETAILS<IC.ACT.BALANCE>
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= Read EB.CONTRACT.BALANCES>
*** <desc>Read ECB to fetch the activity ids for given period</desc>
READ.ECB:

    CALL F.READ(FN.EB.CONTRACT.BALANCES, ACCOUNT.NO, R.ECB, F.EB.CONTRACT.BALANCES, ECB.ERR)
    LOCATE BAL.TYPE IN R.ECB<ECB.BAL.TYPE,1> SETTING BT.POS THEN
        LOCATE START.YRMTH IN R.ECB<ECB.BT.ACT.MONTHS, BT.POS, 1> BY "AR" SETTING START.ACCT.POS ELSE
            IF START.ACCT.POS NE 1 THEN
			    START.ACCT.POS -= 1
			END
        END
        LOCATE END.YRMTH IN R.ECB<ECB.BT.ACT.MONTHS, BT.POS, 1> BY "AR" SETTING END.ACCT.POS ELSE
            END.ACCT.POS -= 1
        END
        IF START.ACCT.POS AND END.ACCT.POS THEN
            GOSUB FORM.ACTIVITY.DATES
        END
    END

    RETURN

*** </region>
*-----------------------------------------------------------------------------
*** <region name= Activity Ids>
*** <desc>Get the Activity ids for given period</desc>
FORM.ACTIVITY.DATES:

    FOR IDX = START.ACCT.POS TO END.ACCT.POS
        ACTIVITY.IDS<-1> = R.ECB<ECB.BT.ACT.MONTHS, BT.POS, IDX>
    NEXT IDX

    RETURN

*** </region>
*-----------------------------------------------------------------------------
END

