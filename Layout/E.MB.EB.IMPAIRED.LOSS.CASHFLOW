*---------------------------------------------------------------------------------------
* <Rating>601</Rating>
*---------------------------------------------------------------------------------------
* Subroutine to get the EB.CASHFLOW details and IFRS.ACCT.BALANCES details of Contracts
* attached in the STANDARD.SELECTION for the NOFILE enquiries
    SUBROUTINE E.MB.EB.IMPAIRED.LOSS.CASHFLOW(Y.DATA)

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.EB.CASHFLOW
    $INSERT I_F.IFRS.ACCT.BALANCES
    $INSERT I_DAS.EB.CASHFLOW
    $INSERT I_DAS.EB.CASHFLOW.NOTES

* MAIN PROCESSING
    GOSUB INITIALISE
    GOSUB SELECTFILES
    GOSUB PROCESS
    RETURN

*---------------------------------------------------------------------------------------
INITIALISE:
*---------------------------------------------------------------------------------------
* Variables for opening EB.CASHFLOW and IFRS.ACCT.BALANCES file

    FN.EBCASHFLOW = 'F.EB.CASHFLOW'
    F.EBCASHFLOW = ''

    FN.IFRSACCTBALANCES = 'F.IFRS.ACCT.BALANCES'
    F.IFRSACCTBALANCES = ''

*---------------------------------------------------------------------------------------

    SEL.CMD = ''
    SEL.LIST = ''
    NO.OF.SEL = ''
    SEL.ERR = ''
    EBCASHFLOW.ID = ''
    IFRSACCTBALANCES.ID = ''
    R.EBCASHFLOW = ''
    R.IFRSACCTBALANCES = ''
    POS = ''

    TRANS.POS = ''

    IMP.AMC.ADJ.POS = ''
    UND.POS = ''
    AMC.POS = ''
    FVEQ.POS = ''
    FVPL.POS = ''


*---------------------------------------------------------------------------------------
* Variables to hold the EB.CASHFLOW Details

    TRANS.REF = ''
    CUSTOMER.NO = ''
    CURRENCY = ''
    VALUE.DATE = ''
    MATURITY.DATE = ''
    IAS.CLASSIFICATION = ''
    IMPAIR.STATUS = ''

    ACCOUNTING.METHOD = ''

    ASSET.LIAB.IND = ''

    IMPAIR.EFFECTIVE.DATE = ''

    BID.OR.OFFER = ''

    MARKET.KEY = ''
    MARKET.MARGIN = ''
    MARGIN.OPERAND = ''

    EIR = ''
    MARKET.RATE = ''

    DATE.IMPAIRED = ''

*---------------------------------------------------------------------------------------
* Variables to hold the Contractual Cahflow Details from EB.CASHFLOW

    CONTRACTUAL.CASHFLOW.DATE = ''
    CONTRACTUAL.CASHFLOW.AMOUNT = ''
    CONTRACTUAL.CASHFLOW.CURRENCY = ''
    CONTRACTUAL.CASHFLOW.TYPE = ''

*---------------------------------------------------------------------------------------
* Variables to hold the Expected Cashflow Details from EB.CASHFLOW

    EXPECTED.CASHFLOW.DATE = ''
    EXPECTED.CASHFLOW.AMOUNT = ''
    EXPECTED.CASHFLOW.CURRENCY = ''
    EXPECTED.CASHFLOW.TYPE = ''

*---------------------------------------------------------------------------------------
* Variables to hold the Collateral Details from EB.CASHFLOW

    EXPECTED.COLLATERAL.DATE = ''
    EXPECTED.COLLATERAL.AMOUNT = ''

*---------------------------------------------------------------------------------------
* Variables to hold the IFRS.ACCT.BALANCES Details

    CONTRACT.BALANCE = ''

    NPV.CON.CF.AMORT = ''
    NPV.CON.CF.FV = ''

    NPV.EXP.CF.AMORT = ''
    NPV.EXP.CF.FV = ''
    VAL.EXP.COLL.AMORT = ''
    VAL.EXP.COLL.FV = ''

    IMPAIR.AMC.ADJUST.AMOUNT = ''
    IMPAIR.AMC.ADJUST.LCY.AMOUNT = ''
    UNWIND.AMOUNT = ''
    UNWIND.LCY.AMOUNT = ''
    IMPAIR.AMORTISED.AMOUNT = ''
    IMPAIR.AMORTISED.LCY.AMOUNT = ''
    IMPAIR.FAIRVALUE.AMOUNT = ''
    IMPAIR.FAIRVALUE.LCY.AMOUNT = ''
    IMPAIRMENT.LOSS = ''
    LCY.IMPAIRMENT.LOSS = ''
    IMPAIR.LAST.CALC.DATE = ''
    ACCT.CALC.AMT = ''
    LAST.CALC.DATE = ''
    BALANCES = ''
*---------------------------------------------------------------------------------------

    ERR.TXT = ''

    ARRAY = ''
    Y.DATA = ''


*---------------------------------------------------------------------------------------
* Getting the Transaction Reference from the Selection Criteria

    LOCATE '@ID' IN ENQ.SELECTION<2,1> SETTING TRANS.POS THEN
        TRANS.REFERENCE = ENQ.SELECTION<4,TRANS.POS>
    END

    RETURN

*---------------------------------------------------------------------------------------

SELECTFILES:
*---------------------------------------------------------------------------------------
*Selecting the EB.CASHFLOW record based on the selection criteria

    IF TRANS.REFERENCE NE "" THEN
        THE.ARGS<1> = "@ID"
        THE.ARGS<2> = "LK"
        THE.ARGS<3> = TRANS.REFERENCE:"..."
    END

    TABLE.NAME = "EB.CASHFLOW"
    TABLE.SUFFIX = ""
    THE.LIST = dasSelectImpairedRecords

    CALL DAS(TABLE.NAME,THE.LIST,THE.ARGS,TABLE.SUFFIX)

    RETURN
*---------------------------------------------------------------------------------------
*Loop begins here

PROCESS:

    LOOP
        REMOVE EBCASHFLOW.ID FROM THE.LIST SETTING POS
    WHILE EBCASHFLOW.ID:POS

* Reading the EB.CASHFLOW record for the particular Contract Id

        CALL F.READ(FN.EBCASHFLOW,EBCASHFLOW.ID,R.EBCASHFLOW,F.EBCASHFLOW,EBCASHFLOW.ERR)
        IFRSACCTBALANCES.ID = EBCASHFLOW.ID

* Reading the IFRS.ACCT.BALANCES record for the particular Contract Id

        CALL F.READ(FN.IFRSACCTBALANCES,IFRSACCTBALANCES.ID,R.IFRSACCTBALANCES,F.IFRSACCTBALANCES,IFRSACCTBALANCES.ERR)

*----------------------------------------------------------------------------------------
* EB.CASHFLOW Details

        TRANS.REF = EBCASHFLOW.ID
        CUSTOMER.NO = R.EBCASHFLOW<IA.CSHF.CUSTOMER.ID>
        CURRENCY = R.EBCASHFLOW<IA.CSHF.CURRENCY>
        VALUE.DATE = R.EBCASHFLOW<IA.CSHF.VALUE.DATE>
        MATURITY.DATE = R.EBCASHFLOW<IA.CSHF.MATURITY.DATE>
        IAS.CLASSIFICATION = R.EBCASHFLOW<IA.CSHF.IAS.CLASSIFICATION>
        IMPAIR.STATUS = R.EBCASHFLOW<IA.CSHF.IMPAIRMENT.STATUS>
        DATE.IMPAIRED = R.EBCASHFLOW<IA.CSHF.DATE.IMPAIRED>
        ACCOUNTING.METHOD = R.EBCASHFLOW<IA.CSHF.ACCOUNTING.METHOD>

* Reading Account Head Type from IFRS.ACCT.BALANCES

        ACCT.HEAD.TYPE = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.ACCT.HEAD.TYPE>

*----------------------------------------------------------------------------------------
*IFRS.ACCT.BALANCES Details

* Counting the number of values in Account Head Type Using DCOUNT

        FIELD.COUNT = DCOUNT(ACCT.HEAD.TYPE,VM)

* Checking the Local Curremcy

        IF CURRENCY EQ LCCY THEN

* loop for calculating Loss for each Account head type present in set.
* ACCT.BAL.BALANCES and ACCT.BAL.BALANCES fields are added to calculate Loss for Local Currency

            FOR I = 1 TO FIELD.COUNT
                ACCT.CALC.AMT = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.LAST.CALC.AMT>,VM,I)
                LAST.CALC.DATE = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.LAST.CALC.DATE>,VM,I)
                BALANCES = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.BALANCE>,VM,I)
                IMPAIRMENT.LOSS = -(IMPAIRMENT.LOSS + ACCT.CALC.AMT + BALANCES)
                IMPAIR.LAST.CALC.DATE = LAST.CALC.DATE
            NEXT I
* continue to the next iteration for processing next account type head.

        END
* Else part used to calculate Loss for foreign currency
* ACCT.BAL.LCY.BALANCE and ACCT.BAL.LST.CAL.LCY.AMT fields are used in calculating Loss for foreign currency

        ELSE
            FOR I = 1 TO FIELD.COUNT
                ACCT.CALC.AMT = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.LAST.CALC.AMT>,VM,I)
                LAST.CALC.DATE = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.LAST.CALC.DATE>,VM,I)
                ACCT.CALC.AMT.LCY = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.LST.CAL.LCY.AMT>,VM,I)
                LCY.BALANCES = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.LCY.BALANCE>,VM,I)
                BALANCES = FIELD(R.IFRSACCTBALANCES<IFRS.ACCT.BAL.BALANCE>,VM,I)
                IMPAIRMENT.LOSS = -(IMPAIRMENT.LOSS + ACCT.CALC.AMT + BALANCES)
                LCY.IMPAIRMENT.LOSS = -(LCY.IMPAIRMENT.LOSS + ACCT.CALC.AMT.LCY + LCY.BALANCES)
                IMPAIR.LAST.CALC.DATE = LAST.CALC.DATE
            NEXT I
* continue to the next iteration for processing next account type head.
        END

*----------------------------------------------------------------------------------------

        ASSET.LIAB.IND = R.EBCASHFLOW<IA.CSHF.ASSET.LIAB.IND>

        IMPAIR.EFFECTIVE.DATE = R.EBCASHFLOW<IA.CSHF.IMPAIR.EFF.DATE>

        MARKET.KEY = R.EBCASHFLOW<IA.CSHF.MARKET.KEY>
        MARKET.MARGIN = R.EBCASHFLOW<IA.CSHF.MARKET.MARGIN>
        MARGIN.OPERAND = R.EBCASHFLOW<IA.CSHF.MARGIN.OPERAND>

        IF ASSET.LIAB.IND EQ "A" THEN
            BID.OR.OFFER = "O"
        END
        ELSE
            BID.OR.OFFER = "B"
        END

        EIR = R.EBCASHFLOW<IA.CSHF.EIR>

*----------------------------------------------------------------------------------------
* Calling the Core routine to calculate the Market Rate by giving the Market Key,Market Margin,Market Operand,
* Currency,Maturity Date of the Contract and the Bid/Offer Rate Marker for the Contract

        CALL EB.GET.MARKET.RATE(MARKET.KEY,MARKET.RATE,MARKET.MARGIN,MARGIN.OPERAND,CURRENCY,BID.OR.OFFER,MATURITY.DATE,ERR.TXT)

*----------------------------------------------------------------------------------------
* Contractual Cashflow Details

        CONTRACTUAL.CASHFLOW.DATE = R.EBCASHFLOW<IA.CSHF.CASH.FLOW.DATE>
        CONTRACTUAL.CASHFLOW.AMOUNT = R.EBCASHFLOW<IA.CSHF.CASH.FLOW.AMT>
        CONTRACTUAL.CASHFLOW.CURRENCY = R.EBCASHFLOW<IA.CSHF.CASHFLOW.CCY>
        CONTRACTUAL.CASHFLOW.TYPE = R.EBCASHFLOW<IA.CSHF.CASH.FLOW.TYPE>

*----------------------------------------------------------------------------------------
* Expected Cashflow Details

        EXPECTED.CASHFLOW.DATE = R.EBCASHFLOW<IA.CSHF.EXP.CFLOW.DATE>
        EXPECTED.CASHFLOW.AMOUNT = R.EBCASHFLOW<IA.CSHF.EXP.CFLOW.AMT>
        EXPECTED.CASHFLOW.CURRENCY = R.EBCASHFLOW<IA.CSHF.EXP.CFLOW.CCY>
        EXPECTED.CASHFLOW.TYPE = R.EBCASHFLOW<IA.CSHF.EXP.CFLOW.TYPE>

*----------------------------------------------------------------------------------------
* Collateral Details

        EXPECTED.COLLATERAL.DATE = R.EBCASHFLOW<IA.CSHF.EXP.COLL.DATE>
        EXPECTED.COLLATERAL.AMOUNT = R.EBCASHFLOW<IA.CSHF.EXP.COLL.AMT>

*----------------------------------------------------------------------------------------
* IFRS.ACCT.BALANCES Details

        CONTRACT.BALANCE = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.CONTRACT.BALANCE>

        NPV.CON.CF.AMORT = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.NPV.CON.CF.AMORT>
        NPV.CON.CF.FV = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.NPV.CON.CF.FV>

        NPV.EXP.CF.AMORT = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.NPV.EXP.CF.AMORT>
        NPV.EXP.CF.FV = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.NPV.EXP.CF.FV>
        VAL.EXP.COLL.AMORT = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.VAL.EXP.COLL.AMORT>
        VAL.EXP.COLL.FV = R.IFRSACCTBALANCES<IFRS.ACCT.BAL.VAL.EXP.COLL.FV>

*----------------------------------------------------------------------------------------
* Forming the Final array which contains all the necessary details from EB.CASHFLOW and IFRS.ACCT.BALANCES
* each data seperated by '*' to be returned from the routine

* For Reference, the positions in which each data will be hold in the array seperated by '*' are
*-----------------------------------------------------------------------------------------
* EB.CASHFLOW Data
*-----------------------------------------------------------------------------------------
* POS<1> = Transaction Reference , POS<2> = Customer Number , POS<3> = Currency , POS<4> = Value date
* POS<5> = Maturity Date , POS<6> = IAS Classification , POS<7> = Impair Status
* POS<8> = EIR , POS<9> Market Rate
* POS<10> = Contractual Cashflow Date , POS<11> = Contractual Cashflow Amount
* POS<12> = Contractual Cashflow Curreny , POS<13> = Contractual Cashflow Type
* POS<14> = Expected Cashflow Date , POS<15> = Expected Cashflow Amount
* POS<16> = Expected Cashflow Currency , POS<17> = Expected Cashflow Type
* POS<18> = Expected Collateral Date , POS<19> = Expected Collateral Amount , POS<31> = Impairment Date
*-----------------------------------------------------------------------------------------
* IFRS.ACCT.BALANCES Data
*-----------------------------------------------------------------------------------------
* POS<20> = Contract Balance , POS<21> = NPV Contractual Cash flow at EIR
* POS<22> = NPV Contractual Cash flow at Market Rate , POS<23> = NPV Expected Cash flow at EIR
* POS<24> = NPV Expected Cash flow at Market Rate , POS<25> = NPV Collateral at EIR
* POS<26> = NPV  Collateral at FV , POS<27> = Impair Effective Date , POS<28> = Impairment Loss
* POS<29> = Lcy Impairment Loss , POS<30> = Impair Last Calc Date
*-----------------------------------------------------------------------------------------

        ARRAY = TRANS.REF:'*':CUSTOMER.NO:'*':CURRENCY:'*':VALUE.DATE:'*':MATURITY.DATE:'*':IAS.CLASSIFICATION:'*':IMPAIR.STATUS:'*':EIR:'*':MARKET.RATE
        ARRAY = ARRAY:'*':CONTRACTUAL.CASHFLOW.DATE:'*':CONTRACTUAL.CASHFLOW.AMOUNT:'*':CONTRACTUAL.CASHFLOW.CURRENCY:'*':CONTRACTUAL.CASHFLOW.TYPE
        ARRAY = ARRAY:'*':EXPECTED.CASHFLOW.DATE:'*':EXPECTED.CASHFLOW.AMOUNT:'*':EXPECTED.CASHFLOW.CURRENCY:'*':EXPECTED.CASHFLOW.TYPE
        ARRAY = ARRAY:'*':EXPECTED.COLLATERAL.DATE:'*':EXPECTED.COLLATERAL.AMOUNT
        ARRAY = ARRAY:'*':CONTRACT.BALANCE:'*':NPV.CON.CF.AMORT:'*':NPV.CON.CF.FV:'*':NPV.EXP.CF.AMORT:'*':NPV.EXP.CF.FV:'*':VAL.EXP.COLL.AMORT:'*':VAL.EXP.COLL.FV
        ARRAY = ARRAY:'*':IMPAIR.EFFECTIVE.DATE:'*':IMPAIRMENT.LOSS:'*':LCY.IMPAIRMENT.LOSS:'*':IMPAIR.LAST.CALC.DATE:'*':DATE.IMPAIRED

* checking the impairment loss details has amount.
        IF  IMPAIRMENT.LOSS NE "" THEN
            IF IMPAIRMENT.LOSS NE "0"  THEN
                Y.DATA<-1> = ARRAY
            END
        END
*-----------------------------------------------------------------------------------------
* All the Variables holding the data are made null to hold new data in the next Loop

        TRANS.REF = ''
        CUSTOMER.NO = ''
        CURRENCY = ''
        VALUE.DATE = ''
        MATURITY.DATE = ''
        IAS.CLASSIFICATION = ''
        IMPAIR.STATUS = ''
        DATE.IMPAIRMENT = ''

        ACCOUNTING.METHOD = ''

        ASSET.LIAB.IND = ''

        IMPAIR.EFFECTIVE.DATE = ''

        BID.OR.OFFER = ''

        MARKET.KEY = ''
        MARKET.MARGIN = ''
        MARGIN.OPERAND = ''

        EIR = ''
        MARKET.RATE = ''

        CONTRACTUAL.CASHFLOW.DATE = ''
        CONTRACTUAL.CASHFLOW.TYPE = ''
        CONTRACTUAL.CASHFLOW.AMOUNT = ''
        CONTRACTUAL.CASHFLOW.CURRENCY = ''

        EXPECTED.CASHFLOW.DATE = ''
        EXPECTED.CASHFLOW.TYPE = ''
        EXPECTED.CASHFLOW.AMOUNT = ''
        EXPECTED.CASHFLOW.CURRENCY = ''

        EXPECTED.COLLATERAL.DATE = ''
        EXPECTED.COLLATERAL.AMOUNT = ''

        CONTRACT.BALANCE = ''

        NPV.CON.CF.AMORT = ''
        NPV.CON.CF.FV = ''

        NPV.EXP.CF.AMORT = ''
        NPV.EXP.CF.FV = ''
        VAL.EXP.COLL.AMORT = ''
        VAL.EXP.COLL.FV = ''

        IMPAIR.AMC.ADJUST.AMOUNT = ''
        IMPAIR.AMC.ADJUST.LCY.AMOUNT = ''
        UNWIND.AMOUNT = ''
        UNWIND.LCY.AMOUNT = ''
        IMPAIR.AMORTISED.AMOUNT = ''
        IMPAIR.AMORTISED.LCY.AMOUNT = ''
        IMPAIR.FAIRVALUE.AMOUNT = ''
        IMPAIR.FAIRVALUE.LCY.AMOUNT = ''
        IMPAIRMENT.LOSS = ''
        LCY.IMPAIRMENT.LOSS = ''
        IMPAIR.LAST.CALC.DATE = ''
        ACCT.CALC.AMT  = ''
        LAST.CALC.DATE = ''
        BALANCES = ''
        ERR.TXT = ''

        ARRAY = ''

        IMP.AMC.ADJ.POS = ''
        UND.POS = ''
        AMC.POS = ''
        FVEQ.POS = ''
        FVPL.POS = ''

        EBCASHFLOW.ID = ''
        IFRSACCTBALANCES.ID = ''
        R.EBCASHFLOW = ''
        R.IFRSACCTBALANCES = ''
        POS = ''

    REPEAT

    RETURN

*Loop ends here
*---------------------------------------------------------------------------------------
END

