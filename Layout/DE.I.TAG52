    SUBROUTINE DE.I.TAG52(TAG,ORD.INST,OFS.DATA,SPARE1,SPARE2,SPARE3,SPARE4,DE.I.FIELD.DATA,TAG.ERR)
*-----------------------------------------------------------------------------
* <Rating>-63</Rating>
*****************************************************************
*
* This routine assigns SWIFT tag52 - Ordering Bank to the ofs message being
* build up via inward delivery
* translate the raw data into OFS format and written away to the ofs directory specified
*
* Inward
*  Tag            -  The swift tag either 52A or 52D
*  Ord inst       -  The swift data
*
* Outward
* OFS.DATA        - The corresponding application field in OFS format
* DE.I.FIELD.DATA - Field name : TM: field values separated by VM
* TAG.ERR         - Tag error.

*********************************************************************
*
*       MODIFICATIONS
*      ---------------
*
* 24/07/02 - EN_10000786
*            New Program
*
* 06/05/04 - EN_10002261
*            SWIFT related changes for bulk credit/debit processing
*
* 17/06/04 - BG_100006684
*            If account no unable to find in the message MT111, then an
*            EB.MESSAGE.111 will be created and the tag52 info is mapped to
*            DRAWER.CUS and DRAWER.ACC  in EB.MESSAGE.111.
*
* 30/06/04 - BG_100006876
*            For message type 102 tag 52 may have option A or C
*
* 07/05/05 - CI_10030002
*            Map tag52 of MT111 to IN.DRAWER.BANK Field in EB.MESSAGE.111
*
* 21/02/07 - BG_100013037
*            CODE.REVIEW changes.
*
* 28/04/09 - EN_10004043
*            SAR Ref: SAR-2008-12-19-0003
*            For 202C this tag will be called twice. One for A and another one
*            is for B sequence. To differentiate it TAG<2> will contain 'B', i.e.,
*            it is being called for B seq tags
*
* 26/03/11 - Task 631815
*            Tag or value before sending to OFS is quoted so that the message will not
*            be trauncated
*
*********************************************************************
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
*

    GOSUB INITIALISE
    IF TAG.ERR THEN
        RETURN
    END   ;* BG_100013037 - E
    GOSUB GET.ORD.ACCT.DATA
*
    BEGIN CASE
    CASE TAG = '52A'
        CALL DE.SWIFT.BIC(ORD.INST,ID.COMPANY,CUSTOMER.NO)
        IF CUSTOMER.NO EQ '' THEN
            CUSTOMER.NO = PREFIX:ORD.INST
        END
        OFS.DATA := FIELD.NAME:':1=':QUOTE(CUSTOMER.NO)
        DE.I.FIELD.DATA<1> = '"':FIELD.NAME:'"': CHARX(251):CUSTOMER.NO
    CASE TAG = '52D' OR TAG = '52C'
        CONVERT CRLF TO VM IN ORD.INST
        NO.CRLF = DCOUNT(ORD.INST,VM)
        FOR C.CRLF = 1 TO NO.CRLF
            FIELD.DATA = ORD.INST<1,C.CRLF>
            FIELD.DATA = QUOTE(FIELD.DATA)
            IF C.CRLF = NO.CRLF THEN
                COMMA.SEP = ''
            END ELSE
                COMMA.SEP = ','
            END
            OFS.DATA :=FIELD.NAME:':':C.CRLF:'=':FIELD.DATA:COMMA.SEP

        NEXT C.CRLF

        DE.I.FIELD.DATA<1> = '"':FIELD.NAME:'"':CHARX(251):ORD.INST
    CASE 1
        TAG.ERR = 'FIELD NOT MAPPED FOR TAG -':TAG


    END CASE
*
    RETURN
*
**********************************************************************
INITIALISE:
**********************************************************************
*
    CUSTOMER.NO = ''
    FIELD.NAME = ''
    CRLF = CHARX(013):CHARX(010)
    OFS.DATA = ''
    LEN.CRLF = LEN(CRLF)
    LEN.ORD.INST = LEN(ORD.INST)
    TAG.ERR = ''
    DE.I.FIELD.DATA = ''
    ACCNT.ID.FLD = ''
    B.TAG = ''
    IF TAG<2> = 'B' THEN
        B.TAG = 1
        TAG = TAG<1>
    END

    BEGIN CASE
    CASE APPLICATION = 'AC.EXPECTED.RECS'
        FIELD.NAME = 'ORD.INSTITUTION'
        PREFIX = 'SW-'

    CASE APPLICATION = 'FUNDS.TRANSFER'
        IF B.TAG THEN
            FIELD.NAME = 'IN.C.ORD.BK'
        END ELSE
            FIELD.NAME = 'IN.ORDERING.BK'
        END
        PREFIX = 'SW-'
    CASE APPLICATION MATCHES 'PAYMENT.STOP':VM:'EB.MESSAGE.111'
        FIELD.NAME = 'IN.DRAWER.BANK'
        PREFIX = 'SW-'
        ACCNT.ID.FLD = "IN.DRAWER.BK.ACCT"
    CASE 1
        TAG.ERR = 'APPLICATION MISSING FOR TAG - ':TAG

    END CASE
*
    RETURN

**********************************************************************
GET.ORD.ACCT.DATA:
**********************************************************************
    IF INDEX(ORD.INST,'/',1) THEN
        CRLF.POS = INDEX(ORD.INST,CRLF,1)

* If the first 2 characters are // , then it may be the clearing code
* bit of the party identifier. Hence ignore the clearing code part and
* the rest of the content should be mapped to bank details
*
* If the frist 3 characters are /C/ or /D/, then it indicates the debit
* or credit identifier and it can be followed by account number.
* In this case get the account number between the 2nd / and first crlf.
* The rest of the content is bank details
*
* If the first character is / and CRLF is found, then the account number
* may be present between the / and CRLF.
* The rest of the content is bank details.
*
* If none of the above is met, then the entire tag is assumed to be for
* bank details only.

        BEGIN CASE
        CASE ORD.INST[1,2] = '//'
            ORD.INST= ORD.INST[CRLF.POS+LEN.CRLF,LEN.ORD.INST]
        CASE ORD.INST[1,3] MATCHES '/C/':VM:'/D/'
            SLASH = INDEX(ORD.INST,'/',2)
            INST.ACCT.DATA = ORD.INST[SLASH+1,CRLF.POS-(SLASH+1)]
            ORD.INST= ORD.INST[CRLF.POS+LEN.CRLF,LEN.ORD.INST]
        CASE ORD.INST[1,1] = '/' AND CRLF.POS
            SLASH = INDEX(ORD.INST,'/',1)
            INST.ACCT.DATA = ORD.INST[SLASH+1,CRLF.POS-(SLASH+1)]
            ORD.INST= ORD.INST[CRLF.POS+LEN.CRLF,LEN.ORD.INST]
        END CASE
        IF ACCNT.ID.FLD THEN
            OFS.DATA := ACCNT.ID.FLD :':1=': QUOTE(INST.ACCT.DATA):','
            DE.I.FIELD.DATA<2> ='"':ACCNT.ID.FLD:'"':CHARX(251):INST.ACCT.DATA

        END
    END

    RETURN


END

