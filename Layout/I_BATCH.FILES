* Version 9 07/04/00  GLOBUS Release No. G13.1.01 25/11/02

* Batch common area

*************************************************************************
* note. process.name variable indicates the process currently
* executed as a phantom task.
* 29/11/90 - GB9000326
*            Add BATCH.INFO variable to hold F.BATCH record id & the name
*            of the job currently being run. ID<fm>JOB.
*
* 25/06/96 - GB9600561
*            Change F.SPARE.12 to C$BATCH.START.DATE which will hold
*            the bank date on which the batch was started.
*
* 08/03/00 - GB0000361
*            Add SESSION.NO & KEYS.PROCESSED to store the session number
*            of this thread and the number of keys processed by the
*            standard multi-thread job BATCH.JOB.CONTROL
*
* 11/09/02 - EN_10001339
*            Add CONTROL.LIST - updated by .SELECT programs if they need to
*            be called more than once (ie for LD schedules)
*  Remove pre and post routine support.
*  Changes to core batch routines for commitment control
*
* 01/07/04 -  BG_100006900
*             Added variable to support last message and como name - to be
*             stored on the tsa status record
*
* 30/08/08 -  EN_10003814
*             Added variables to store the select statement,selection type
*             contracts bulked by a cob job
* 24/03/09 - CI_10061532
*            The variable JOB.USER is introduced to fetch the USER defined for the job
*
* 05/10/09 - EN_10004366( SAR-2009-03-05-0010)
*            Include the tags and the common variables to indicate whether the como,job,
*            process and service tags has been opened .
*
* 19/10/09 - BG_100025532
*                   Change in the logic to determine the HIGHEST.AGENT to be updated in TSA.PARAMETER.
*	              NUMBER.OF.SESSIONS is populated in SJR and used in BJC
*
* 12/02/10 - Defect - 21457 / Task - 21916
*            Include reserved fields. 
*     
*************************************************************************

    COMMON /B.FILES/ SESSION.NO,        ;* Agent number
    KEYS.PROCESSED, ;* Total number of contracts to process for this job
    BATCH.THREAD.KEY,         ;* Key to list file <space> contract id
    BATCH.LIST.FILE,          ;* Name of list file F.JOB.LIST.2
    NUMBER.OF.SESSIONS,     ;* used in BJC in extract portion
    CONTROL.LIST,   ;* Controls if select process must be called more than once
    COMO.NAME,      ;* Name of como used for this agent - stored in TSA.STATUS
    LAST.MESSAGE,   ;* Last output from OCOMO - stored in TSA.STATUS
    JOB.PROGRESS,   ;* Where are we in BatchJobControl 1=processing contracts, 2=selecting etc
    LAST.JOB,
    JOB.USER,       ;* Holds the USER defined for the job
    C$BATCH.START.DATE,       ;* GB9600561 this variable to hold the date on which the batch was started
    BATCH.INFO,     ;* <1>Process Name <2>Routine <3>Job Name
    F.BATCH,        ;* File variable for F.BATCH
    PROCESS.NAME,   ;* Current process
    R.USER.SAVE,    ;* No longer used - I think
    SELECT.STATEMENT,         ;* Select statement used for selecting the ids to be processed
    SELECTION.MODE, ;* Type of selection (PREDEFINED,ALL,CRITERIA,FILTER)
    BULK.NUMBER,     ;* No.of contracts to be bulked
    COMO.TAG.OPEN , ;* indicates whether the como tag has been opened
    SERVICE.TAG.OPEN,         ;* indicates whether the service tag has been opened
    PROCESS.TAG.OPEN ,        ;* indicates whether the process tag has been opened
    JOB.TAG.OPEN,    ;* indicates whether the job name tag has been opened
    BATCH.RESERVED(20) ;*  added reserved fields 

* Tags
    EQU C$COMO.TAG TO '<como>'
    EQU C$COMO.TAG.C TO '</como>'
    EQU C$AGENT.TAG TO '<agent>'
    EQU C$AGENT.TAG.C TO '</agent>'
    EQU C$PROCESSID.TAG TO '<processid>'
    EQU C$PROCESSID.TAG.C TO '</processid>'
    EQU C$PORT.NO.TAG TO '<portno>'
    EQU C$PORT.NO.TAG.C TO '</portno>'
    EQU C$SERVER.NAME.TAG TO '<servername>'
    EQU C$SERVER.NAME.TAG.C TO '</servername>'
    EQU C$SERVICE.TAG TO '<service'
    EQU C$SERVICE.TAG.C TO '</service>'
    EQU C$PROCESS.TAG TO '<process'
    EQU C$PROCESS.TAG.C TO '</process>'
    EQU C$JOB.TAG TO '<job'
    EQU C$JOB.TAG.C TO '</job>'
