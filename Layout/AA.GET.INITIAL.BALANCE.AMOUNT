*-----------------------------------------------------------------------------
* <Rating>-29</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE AA.GET.INITIAL.BALANCE.AMOUNT(PROPERTY.ID, START.DATE, END.DATE, CURRENT.DATE, BALANCE.TYPE, ACTIVITY.IDS, CURRENT.VALUE, START.VALUE, END.VALUE)

*** <region name= Description>
*** <desc>Task of the sub-routine</desc>
* Program Description
* This is a RULE.VAL.RTN designed and released to evaluate the newly created
* Periodic Attribute Classes
*
*-----------------------------------------------------------------------------
* @uses I_AA.APP.COMMON
* @package retaillending.AA
* @stereotype subroutine
* @author carolbabu@temenos.com
*-----------------------------------------------------------------------------
*** </region>
**
*** <region name= Arguments>
*** <desc>Input and out arguments required for the sub-routine</desc>
* Arguments
*
* Input
*
* @param Property ID   - Property ID
* @param Start Date    - Rule Start Date
* @param End Date      - Rule End Date
* @param Current Date  - The current date at which the arrangement is running and for which the balance amount is sought
* @param Balance Type  - Balance Type for which the Balance Amount is required
* @param Activity ID   - Activity ID for which the Balance Amount is required
*
* Ouptut
* @return Current Value - The value for the attribute on the actual date.
* @return Start Value   - Balance Amounts of the Authorised Movements
* @return End Value     - Balance Amounts of all the Movements
*
*** </region>

*-----------------------------------------------------------------------------

*** <region name= Modification History>
*** <desc>Modifications done in the sub-routine</desc>
* Modification History
*
* 24/11/10 - EN_72965
*            New routine to get the balance amounts
*            used in rule evaluation
*
* 11/07/14 - Task : 1033488
*            Defect : 1021864
*            Check if the activity has been already processed once, else get the balance amount of the particular period balance
*            instead of ECB.
*
*** </region>

*-----------------------------------------------------------------------------

*** <region name= Inserts>
*** <desc>File inserts and common variables used in the sub-routine</desc>
* Inserts
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_AA.APP.COMMON
    $INSERT I_AA.ACTION.CONTEXT
    $INSERT I_F.AA.BILL.DETAILS
    $INSERT I_F.AA.ARRANGEMENT.ACTIVITY
    $INSERT I_GTS.COMMON
    $INSERT I_F.ACCT.ACTIVITY
    $INSERT I_F.AA.ACTIVITY.HISTORY
*** </region>

*-----------------------------------------------------------------------------

*** <region name= Main control>
*** <desc>main control logic in the sub-routine</desc>

    GOSUB INITIALISE
    GOSUB PROCESS

    RETURN
*** </region>
*-----------------------------------------------------------------------------

*** <region name= Initialise>
*** <desc>Initialise para in the sub routine</desc>
INITIALISE:

    START.VALUE = ''
    END.VALUE = ''
    ARRANGEMENT.ID = AA$ARR.ID
    ACT.POS = ''
    R.ACTIVITY.HISTORY = ''
    R.ARRANGEMENT.ACTIVITY = AA$R.ARRANGEMENT.ACTIVITY

    RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= Process>
*** <desc>To check whether the activity is called for the first time sub routine</desc>
PROCESS:

    CALL AA.READ.ACTIVITY.HISTORY(ARRANGEMENT.ID, '', '', R.ACTIVITY.HISTORY)

    LOCATE ACTIVITY.IDS IN R.ACTIVITY.HISTORY<AA.AH.ACTIVITY.ID, 1> SETTING ACT.POS THEN  ;* Check if the activity is already present in the activity history
    END

** Get the count of the activity present under the activity history. If that activity is already triggered but its status is in
** either DELETE or AUTH-REV then COUNT would be reduced.

    IF R.ACTIVITY.HISTORY<AA.AH.TOT.ACT.CNT, ACT.POS, 1> THEN
** Only during the first time processing of the particular activity, system must validate the amount against the rule value.
        END.VALUE = "NOCOMPARE"
    END ELSE
** Get the amount from arrangement activity, since during the transaction the txn amount wont be available under the activity history.
        END.VALUE = R.ARRANGEMENT.ACTIVITY<AA.ARR.ACT.TXN.AMOUNT>
    END

    RETURN
*** </region>
*-----------------------------------------------------------------------------

END

