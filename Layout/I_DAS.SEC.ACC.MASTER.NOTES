*-----------------------------------------------------------------------------
* Insert for  Data Access Service.
* ----------------------------------------
* Defines the named queries that can be issued against the SEC.ACC.MASTER table.
* For the implementation of these queries, refer to DAS.SECC.ACC.MASTER
*-----------------------------------------------------------------------------
* You MUST have a dimensioned array for DAS$CACHE and DAS$NOTES
*-----------------------------------------------------------------------------
* Modification History:
*
* 15/10/06 - EN_10003115
*            Creation.
*            Ref: SAR-2006-06-14-0003
*
* 12/11/07 - EN_10003521
*            Portfolios with MANAGED.ACCOUNT eq.
*
* 25/03/08 - GLOBUS_EN_10003603 cgraf@temenos.com
*            Select with PORT.COMP.ID and with ACCOUNT.OFFICER
*
* 06/05/08 - GLOBUS_BG_100018365 cgraf@temenos.com
*            Compilation error due to missing ')'
*
* 04-02-2011 Defect 19774 coding task 146630 
*            EB.ReadList change to DAS
*
* 11/07/11 - Enhancement 147214 Task 236678
*			 Advanced Alerts
*
* 21/08/12 - Enhancement_390096 Task_400871
*            Locking client Reporting.
*
* 07/01/14 - Enhancement_709853 Task_861661
*             Select SAM with Sec Margin Ratio
*
*-----------------------------------------------------------------------------
      COMMON/DAS.SEC.ACC.MASTER/DAS$CACHE(100),DAS$NOTES(100)
*-----------------------------------------------------------------------------
      DAS$NOTES(dasSecAccMasterNonNullAccountNos)  = 'Portfolios with an account number'
      DAS$NOTES(dasSecAccMasterCustomerPortfolios) = 'Portfolios for a specific customer'
      DAS$NOTES(dasSecAccMasterHasNewReferenceCcyEffective) = 'Has new reference currency and new reference currency effective today or before'
      DAS$NOTES(dasSecAccMasterGivenAccountOfficer) = 'Portfolios for a specific account officer'
      DAS$NOTES(dasSecAccMasterThisCompany) = 'Portfolios for this company'
      DAS$NOTES(dasSecAccMasterAcctOfficerInRangeAnd) = 'Portfolios with Account Officer in both of two ranges'
      DAS$NOTES(dasSecAccMasterAcctOfficerInRangeOr) = 'Portfolios with Account Officer in either of two ranges'
      DAS$NOTES(dasSecAccMasterAcctOfficerGreaterThan) = 'Portfolios with Account Officer greater than'
      DAS$NOTES(dasSecAccMasterAcctOfficerLessThan) = 'Portfolios with Account Officer less than'
      DAS$NOTES(dasSecAccMasterAcctOfficerGreaterOrEqual) = 'Portfolios with Account Officer greater than or equal'
      DAS$NOTES(dasSecAccMasterAcctOfficerLessOrEqual) = 'Portfolios with Account Officer less than or equal'
      DAS$NOTES(dasSecAccMasterAcctOfficerNotEqual) = 'Portfolios with Account Officer not equal'
      DAS$NOTES(dasSecAccMasterAcctOfficerLike) = 'Portfolios with Account Officer like'
      DAS$NOTES(dasSecAccMasterSafeGrpPortAdvGrpPort) = 'Portfolios with SAFE.GROUP.PORT and ADV.GROUP.PORT equal to'
      DAS$NOTES(dasSecAccMasterDynSelect)             = 'Dynamic portfolio selection'
      DAS$NOTES(dasSecAccMasterNotNullCompFreq)          = 'Select portfolios where compare frequency is not null'
      DAS$NOTES(dasSecAccMasterManagedAccounts)       = 'Portfolios with MANAGED.ACCOUNT equal to'
      DAS$NOTES(dasSecAccMasterThisCompanyGivenAccOfficer) = 'Portfolios for a specific account officer for this company'
      DAS$NOTES(dasSecAccMasterEvent) = 'Portfolios with sepecific events'
      DAS$NOTES(dasSecAccMasterPortClassType) = 'Portfolios with specific PORT.CLASS.TYPE'
      DAS$NOTES(dasSecAccMasterwithSmr) = 'Portfolios with SEC.MARGIN.RATIO'
      DAS$NOTES(dasSecAccMasterCustomerGivenAccOfficer) = 'Portfolios for Given customer for Given Account officer'
