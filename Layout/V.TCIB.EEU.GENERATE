*--------------------------------------------------------------------------------------------
* <Rating>-44</Rating>
*--------------------------------------------------------------------------------------------
    SUBROUTINE V.TCIB.EEU.GENERATE
*--------------------------------------------------------------------------------------------
* Attached to     : AA.ARRANGEMENT.ACTIVITY,TCIB.NEW Version as a Before Auth Routine
* Incoming        : N/A
* Outgoing        : External User Id
*---------------------------------------------------------------------------------------------
* Description:
* Will upon creation of an internet Service Arrangement create an EB.EXTERNAL.USER
*---------------------------------------------------------------------------------------------
* Modification History :
* 01/07/14 - Enhancement 1001222/Task 1001223
*            TCIB : User management enhancements and externalisation
*----------------------------------------------------------------------------------------------

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_GTS.COMMON
    $INSERT I_System
    $INSERT I_F.RELATION
    $INSERT I_F.AA.ARRANGEMENT.ACTIVITY
    $INSERT I_F.EB.EXTERNAL.USER
    $INSERT I_F.CHANNEL.PARAMETER
    $INSERT I_F.CUSTOMER
    $INSERT I_F.AUTO.ID.START
*
*-------------------------------------------------------------------------------------------------

    IF (OFS$OPERATION NE "PROCESS") THEN RETURN   ;*This check is used to filter during validation and to proceed only at process time

    GOSUB CHECK.AUTO.ID
*
    IF APP.AVAIALBILITY NE '' THEN      ;*Checking whether auto id start is available
        GOSUB INITIALISE
        GOSUB OPEN.FILES
        GOSUB MAIN.PROCESS
    END

    RETURN

*--------------------------------------------------------------------------------------------------
CHECK.AUTO.ID:
*--------------

    FN.AUTO.ID.START = 'F.AUTO.ID.START'          ;*Open file the auto id start appliation
    F.AUTO.ID.START = ''
    CALL OPF(FN.AUTO.ID.START,F.AUTO.ID.START)

    APP.AVAIALBILITY = ''     ;*Initialising variable for application availability
    R.AUTO.ID = ''  ;*initialising array
    ERR.ID = ''     ;*initialising variable
    AUTO.ID = 'EB.EXTERNAL.USER';       ;*Application
    CALL F.READ(FN.AUTO.ID.START,AUTO.ID,R.AUTO.ID,F.AUTO.ID,ERR.ID)
    IF R.AUTO.ID THEN
        APP.AVAIALBILITY = 'YES'        ;*Setting value as auto id application available
    END
    RETURN

*---------------------------------------------------------------------------------------------------
INITIALISE:
*-----------

    Y.END.DATE = '';*Initialising variable
    Y.TODAY = OCONV(DATE(),"D4*")       ;* Data variable to bring the today date
    Y.TODAY = FIELD(Y.TODAY,"*",3):FIELD(Y.TODAY,"*",1):FIELD(Y.TODAY,"*",2)    ;*formatting as per t24 date format
    Y.END.DATE = Y.TODAY

    ID_ARR     = R.NEW(AA.ARR.ACT.ARRANGEMENT)    ;*Arrangement id is assigned to the variable
    ID_CUSTOMER  = R.NEW(AA.ARR.ACT.CUSTOMER)     ;*Arrangement customer id is assigned to the variable

    CALL CDT('',Y.END.DATE,'+180C')

    RETURN
*
*--------------------------------------------------------------------------------------------------
OPEN.FILES:
*------------

    FN.EB.EXTERNAL.USER  = "F.EB.EXTERNAL.USER"   ;*open external user application
    F.EB.EXTERNAL.USER   = ""
    R.EXTERNAL.USER   = ""
    ERR.EXTN = ""
    CALL OPF(FN.EB.EXTERNAL.USER,F.EB.EXTERNAL.USER)

    FN.CUSTOMER  = "F.CUSTOMER"         ;*open customer application
    F.CUSTOMER   = ""
    R.CUSTOMER   = ""
    ERR.CUSTOMER = ""
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CHANNEL.PARAMETER = 'F.CHANNEL.PARAMETER'  ;*open channel parameter application
    F.CHANNEL.PARAMETER = ''
    CALL OPF(FN.CHANNEL.PARAMETER,F.CHANNEL.PARAMETER)

    RETURN          ;*Opf return
*-------------------------------------------------------------------------------------------------
MAIN.PROCESS:
*-----------

*----------Initialise OFS Message--------------

    GOSUB SUB.INIT.OFS.MESSAGE
    var_ofsApplication = "EB.EXTERNAL.USER"       ;*application defaulted with external user

    R.CHANNEL.PARAMETER = ''
    ERR.PARAM = ''
    CALL CACHE.READ(FN.CHANNEL.PARAMETER, "SYSTEM" ,R.CHANNEL.PARAMETER, ERR.PARAM)       ;*read channel parameter
    IF R.CHANNEL.PARAMETER THEN
        Y.VERSION = R.CHANNEL.PARAMETER<CPR.VERSION.NAME,1> ;*pick the version name from the channel parameter table
        var_ofsVersion     = Y.VERSION  ;*assign it to the ofs variable
        var_ofsSourceId = R.CHANNEL.PARAMETER<CPR.OFS.SOURCE>         ;*ofs type taken from the channel parameter table
    END

    var_inApp = "EB.EXTERNAL.USER"      ;*Retrieve next User ID
    GOSUB SUB.BUILD.NEXT.ID
    var_ofsTxnId = var_outID  ;* Returns the unique External user id

    GOSUB SUB.READ.CUSTOMER   ;*Read Customer Name

*-------Create the External User record via OFS
    GOSUB SUB.BUILD.OFS.EXTERNAL.USER   ;*builds up var_ofsMessage
    GOSUB SUB.SEND.OFS.LOCAL.REQUEST    ;*Adds in the ofs request queue

*-------Display Error and exit in case of Error

    IF status NE "OKAY" THEN  ;*check for success
        ETEXT="EB-EXT.USER.FAILURE"     ;*failure message
        GOSUB DISPLAY.ERROR
        RETURN
    END

    RETURN
*--------------------------------------------------------------------------------------------------------------------------------
SUB.INIT.OFS.MESSAGE:
*--------------------------

*    Initailising default variables for Ofs request

    var_ofsHeader        = ""
    var_ofsBody          = ""
    var_ofsSourceId      = ""
    var_ofsTxnId         = ""
    var_ofsApplication   = ""
    var_ofsVersion       = ""
    var_ofsFunction      = "I"          ;*ofs function to be used
    var_ofsProcess       = "PROCESS"    ;*ofs request formed with process
    var_ofsGtsmode       = 1  ;*ERROR = $NAU file with status set to HLD. Override accepted automatically transaction committed
    var_ofsNoOfAuth      = "1"          ;*Setting the no of auth always to 1 as it needs to be amended or authorised by another user.
    var_ofsError         = ""
    var_ofsCompany       = ""
    var_record           = ""
    var_ofsOptions      = OPERATOR      ;*operator passes the login values

    RETURN
*-------------------------------------------------------------------------------------------------
SUB.BUILD.NEXT.ID:
*--------------------------
    var_outID = ""
    CALL TCIB.AI.GET.NEXT.ID(var_inApp, var_outID)          ;*call routine to fetch the next id of the external user application
    var_inApp = ""  ;*nullfying the incoming variable
*
    RETURN
*
*------------------------------------------------------------------------------------------------
SUB.READ.CUSTOMER:
*-------------------

    CALL F.READ(FN.CUSTOMER,ID_CUSTOMER,R.CUSTOMER,F.CUSTOMER, ERR.CUSTOMER)
    VAR_NAME1 = R.CUSTOMER<EB.CUS.NAME.1,1>       ;*read the customer name to default in the external user short name by default

    RETURN
*----------------------------------------------------------------------------------------------
SUB.BUILD.OFS.EXTERNAL.USER:
*--------------------------

    var_record<EB.XU.NAME>             = VAR_NAME1          ;*external user short name
    var_record<EB.XU.CUSTOMER>    = ID_CUSTOMER   ;*external user customer name
    var_record<EB.XU.ARRANGEMENT>       = ID_ARR  ;*external user arrangement name
    var_record<EB.XU.START.DATE> = Y.TODAY        ;*today value in start date
    var_record<EB.XU.END.DATE> = Y.END.DATE       ;*Setting end date
    var_record<EB.XU.ALLOWED.CUSTOMER> = ID_CUSTOMER        ;*allowed customer is external user

    CALL OFS.BUILD.RECORD(var_ofsApplication,var_ofsFunction,var_ofsProcess,var_ofsVersion,var_ofsGtsmode,var_ofsNoOfAuth,var_ofsTxnId,var_record,var_ofsMessage)

    RETURN
*---------------------------------------------------------------------------------------------------
SUB.SEND.OFS.LOCAL.REQUEST:
*------------------------------

    CALL ofs.addLocalRequest(var_ofsMessage,"txn",var_ofsError)       ;*Add ofs local request for external user creation
    status = " OKAY"          ;*variable for success
    IF var_ofsError NE "" THEN
        status = " ":var_ofsError;      ;*if error add the error in the variable
    END
    RETURN
*---------------------------------------------------------------------------------------------------
DISPLAY.ERROR:
*------------------------------
    V$ERROR=1

END

