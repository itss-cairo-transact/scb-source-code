* Copyright(c) 2001 INFORMER SA Greece
* File layout for SCB.CUS.EXP.P created at 05/07/2009
*               PREFIX[EXP.P]
 EQU EXP.P.CUSTOMER TO 1,
     EXP.P.TYPE TO 2,
     EXP.P.CURRENCY TO 3,
     EXP.P.P1 TO 4,
     EXP.P.PERC1 TO 5,
     EXP.P.P2 TO 6,
     EXP.P.PERC2 TO 7,
     EXP.P.P3 TO 8,
     EXP.P.PERC3 TO 9,
     EXP.P.P4 TO 10,
     EXP.P.PERC4 TO 11,
     EXP.P.EXP.DATE TO 12,
     EXP.P.RESERVED7 TO 13,
     EXP.P.RESERVED6 TO 14,
     EXP.P.RESERVED5 TO 15,
     EXP.P.RESERVED4 TO 16,
     EXP.P.RESERVED3 TO 17,
     EXP.P.RESERVED2 TO 18,
     EXP.P.RESERVED1 TO 19,
     EXP.P.RECORD.STATUS TO 20,
     EXP.P.CURR.NO TO 21,
     EXP.P.INPUTTER TO 22,
     EXP.P.DATE.TIME TO 23,
     EXP.P.AUTHORISER TO 24,
     EXP.P.CO.CODE TO 25,
     EXP.P.DEPT.CODE TO 26,
     EXP.P.AUDITOR.CODE TO 27,
     EXP.P.AUDIT.DATE.TIME TO 28
