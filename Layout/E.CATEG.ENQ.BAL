* Version 2 25/10/00  GLOBUS Release No. G11.0.00 29/06/00
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
   $PACKAGE AC.ModelBank

      SUBROUTINE E.CATEG.ENQ.BAL
*
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_ENQUIRY.COMMON
$INSERT I_ENQ.CE.COMMON
$INSERT I_F.CATEG.ENTRY
*
      YRMTH = O.DATA
      LOCATE YRMTH IN YR.MTH.BAL<1,1> BY "AR" SETTING YPOS THEN
         O.DATA = YR.MTH.BAL<2,YPOS>
      END ELSE                           ; * For the last balance use RUNNING.BALANCE
         O.DATA = YRUNNING.BAL
      END
*
      RETURN
   END

