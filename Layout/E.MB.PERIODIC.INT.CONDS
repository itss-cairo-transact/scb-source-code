*------------------------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-93</Rating>
*-----------------------------------------------------------------------------
      $PACKAGE AC.ModelBank

    SUBROUTINE E.MB.PERIODIC.INT.CONDS(RET.ARR)
*------------------------------------------------------------------------------------------
*------------------------------------------------------------------------------------------
* DESCRIPTION :
* ------------
* This routine is attached to the NOFILE enquiry PERIODIC.INTEREST.CONDS.
* The enquiry displays all the conditions set in the application PERIODIC.INTEREST based in the KEY
* or CURRENCY given in the selection criteria of the enquiry.
*
*------------------------------------------------------------------------------------------
*------------------------------------------------------------------------------------------
*
* MODIFICATION HISTORY :
* --------------------
*
*  VERSION : 1.0               DATE : 28 JUL 2009          CD  : EN_10004268
*                                                          SAR : SAR-2009-01-14-0003
*
*  VERSION : 1.1               DATE : 10 AUG 2009          CD  : BG_100024451
*                                                          TTS : TTS0908934
*
*  VERSION : 1.2               DATE : 03 NOV 2009          CD  : BG_100025714
*                                                          TTS : TTS0909226
*
*           DESCRIPTION :  Fixed Selction for the field KEY was not available. The changes include
*                          following. The list of values available for the field KEY are stored in a variable
*                          KEY.VAL.ID from FIXED SELCTION and Enquiry Selection.
*                          For each value of KEY given either in Enquiry Selection or
*                          FIXED SELECTION the corresponding DAS is called on the application BASIC.INTEREST
*------------------------------------------------------------------------------------------
*------------------------------------------------------------------------------------------

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.PERIODIC.INTEREST
    $INSERT I_DAS.COMMON
    $INSERT I_DAS.PERIODIC.INTEREST

    GOSUB INITIALISE
    GOSUB LOCATE.FIELDS

    RETURN


*---------
INITIALISE:
*---------

    FN.PERIODIC.INT = "F.PERIODIC.INTEREST"
    F.PERIODIC.INT  = ""

    CALL OPF(FN.PERIODIC.INT, F.PERIODIC.INT)

    DAS.LIST = ''
    KEY.VAL  = ''
    CURR.VAL = ''
    ID.VAL   = ''

    Y.FIELDS.CNT = ''
    Y.OPERAND = ''
    Y.RANGE.AND.VALUE = ''

    RETURN

*------------
LOCATE.FIELDS:
*------------


    LOCATE 'CURRENCY' IN D.FIELDS SETTING CURR.POS THEN     ;* Locate the CURRENCY in enquiry selection
                                                            ;*S/BG_100025714
        CURR.VAL = D.RANGE.AND.VALUE<CURR.POS>

    END

    LOCATE 'DATE' IN D.FIELDS SETTING DATE.POS THEN         ;*Locate the DATE in enquiry selection

        DATE.VAL = D.RANGE.AND.VALUE<DATE.POS>

    END


    Y.FIELDS.CNT = DCOUNT(D.FIELDS,FM)

    FOR Y.COUNT = 1 TO Y.FIELDS.CNT

        IF D.FIELDS<Y.COUNT> EQ 'KEY' THEN        ;* Locate the values of KEY in enquiry selction as well as

            Y.OPERAND = D.LOGICAL.OPERANDS<Y.COUNT>         ;* the fixed selection of the enquiry.

            Y.RANGE.AND.VALUE = D.RANGE.AND.VALUE<Y.COUNT>

            Y.SEL.OPERAND = OPERAND.LIST<Y.OPERAND>

            IF Y.SEL.OPERAND EQ 'LK' THEN
                Y.SEL.OPERAND = 'LIKE'
            END

            Y.STR1 = SM
            Y.STR2 = FM

            CHANGE Y.STR1 TO Y.STR2 IN Y.RANGE.AND.VALUE    ;* Convert the VM to FM in the variable Y.RANGE.AND.VALUE

            KEY.VAL.ID<-1> = Y.RANGE.AND.VALUE    ;* Appends the list of values for the field KEY either in

        END         ;* Enquiry Selection or Fixed Selection of the Enquiry

    NEXT Y.COUNT

    IF KEY.VAL.ID NE '' THEN

        LOOP

            REMOVE KEY.VAL FROM KEY.VAL.ID SETTING KEY.VAL.POS

        WHILE KEY.VAL:KEY.VAL.POS       ;* For each value of KEY the sub block LOCATE.SUB.PROCESS is called and the

            KEY.VAL.LEN = LEN(KEY.VAL)  ;* values are populated in the return variable RET.ARR

            IF KEY.VAL.LEN EQ '1' THEN

                KEY.VAL = "0":KEY.VAL

            END

            GOSUB LOCATE.SUB.PROCESS

        REPEAT

    END ELSE

        GOSUB LOCATE.SUB.PROCESS                            ;*E/BG_100025714

    END

    RETURN

*-----------------
LOCATE.SUB.PROCESS:
*-----------------


    BEGIN CASE      ;* Here based on the values given in enquiry selection and fixed selection the
                    ;* ID.VAL variable is formed to CALL DAS on BASIC.INTEREST application.

    CASE  KEY.VAL EQ '' AND CURR.VAL EQ '' AND DATE.VAL EQ ''    ;*S/BG_100025714

        ID.VAL = ''

    CASE KEY.VAL EQ '' AND CURR.VAL NE '' AND DATE.VAL NE ''

        ID.VAL = "...":CURR.VAL:DATE.VAL

    CASE KEY.VAL NE '' AND CURR.VAL NE '' AND DATE.VAL NE ''

        ID.VAL = KEY.VAL:CURR.VAL:DATE.VAL

    CASE KEY.VAL NE '' AND CURR.VAL EQ '' AND DATE.VAL EQ ''

        ID.VAL = KEY.VAL:"..."

    CASE KEY.VAL NE '' AND CURR.VAL EQ '' AND DATE.VAL NE ''

        ID.VAL = KEY.VAL:"...":DATE.VAL

    CASE KEY.VAL NE '' AND CURR.VAL NE '' AND DATE.VAL EQ ''

        ID.VAL = KEY.VAL:CURR.VAL:"..."

    CASE KEY.VAL EQ '' AND CURR.VAL EQ '' AND DATE.VAL NE ''

        ID.VAL = "...":DATE.VAL

    CASE KEY.VAL EQ '' AND CURR.VAL NE '' AND DATE.VAL EQ ''         ;*S/BG_100025714

        ID.VAL = "...":CURR.VAL:"..."

    END CASE

    GOSUB CALL.DAS.PERIODIC.INT

    RETURN

*--------------------
CALL.DAS.PERIODIC.INT:
*--------------------


    IF ID.VAL NE '' THEN

        DAS.LIST     = dasPeriodicInterestIDLikeKeyCurrByDsndId
        TABLE.NAME    = "PERIODIC.INTEREST"
        ARGUMENTS    = ID.VAL
        TABLE.SUFFIX = ''

        CALL DAS(TABLE.NAME, DAS.LIST, ARGUMENTS, TABLE.SUFFIX)

    END ELSE

        DAS.LIST     = dasPeriodicInterestIDLikeKeyCurrByDsndId1
        TABLE.NAME    = "PERIODIC.INTEREST"
        ARGUMENTS    = ''
        TABLE.SUFFIX = ''

        CALL DAS(TABLE.NAME, DAS.LIST, ARGUMENTS, TABLE.SUFFIX)

    END

    GOSUB RETURN.ARRAY


    RETURN


*-----------
RETURN.ARRAY:
*-----------

    IF DAS.LIST NE '' THEN

        LOOP

            REMOVE P.INT.ID FROM DAS.LIST SETTING P.INT.ID.POS

        WHILE P.INT.ID:P.INT.ID.POS

            CALL F.READ(FN.PERIODIC.INT, P.INT.ID, R.PERIODIC.INT, F.PERIODIC.INT, ERR.PERIODIC.INT)

            P.KEY             = P.INT.ID[1, LEN(P.INT.ID)-11]

            P.DATE            = P.INT.ID[LEN(P.INT.ID)-7, LEN(P.INT.ID)-5]

            P.DESC            = R.PERIODIC.INT<PI.DESCRIPTION>

            CURR.TEMP         = RIGHT(P.INT.ID, 11)

            P.CURR            = LEFT(CURR.TEMP, 3)

            P.REST.PERIOD     = R.PERIODIC.INT<PI.REST.PERIOD>

            P.REST.DATE       = R.PERIODIC.INT<PI.REST.DATE>

            P.DAYS.SINCE.SPOT = R.PERIODIC.INT<PI.DAYS.SINCE.SPOT>

            P.BID.RATE        = R.PERIODIC.INT<PI.BID.RATE>

            P.OFFER.RATE      = R.PERIODIC.INT<PI.OFFER.RATE>

            P.AMOUNT          = R.PERIODIC.INT<PI.AMT>

            P.DRILL           = P.INT.ID

            RET.ARR<-1>  = P.KEY:"*":P.DATE:"*":P.DESC:"*":P.CURR:"*":P.REST.PERIOD:"*":P.REST.DATE:"*":P.DAYS.SINCE.SPOT:"*":P.BID.RATE:"*":P.OFFER.RATE:"*":P.AMOUNT:"*":P.DRILL


        REPEAT

    END

    RETURN

