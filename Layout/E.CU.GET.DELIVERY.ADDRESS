*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
    $PACKAGE ST.ModelBank

    SUBROUTINE E.CU.GET.DELIVERY.ADDRESS(ID.LIST)
*
******************************************************************************
*
*   Incoming
*
*   Outgoing
*   ID.LIST - Contains records to be displayed in the following format
*             CUSTOMER.ID*ADDRESS*CARRIER.
*             The ADDRESS component will be Street address if CARRIER is PRINT,
*             Phone number if carrier is SMS, Email address if it is EMAIL and
*             so on.,
*
******************************************************************************
*
*   Modification History
*
*   09/08/10 - Enhancement 43265, Task 43268
*              Customer Services
*
******************************************************************************
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.CUSTOMER
    $INSERT I_F.DE.ADDRESS
    $INSERT I_F.DE.PRODUCT
    $INSERT I_F.DE.CARRIER
    $INSERT I_ENQUIRY.COMMON
*
    GOSUB INITIALISE
*
    IF NOT(ENQ.ERROR) THEN
        GOSUB PROCESS
    END
*
    RETURN
*
**** <region name= INITIALISE>
*** <desc>Initialise the necassary variables </desc>
*
***********
INITIALISE:
***********
*
    ER = ''
    CARRIER = ''
    R.ADDRESS = ''
    R.DE.MESSAGE = ''
    CUS.ID = ''
    DE.MSG.ID= ''

    LOCATE 'CUSTOMER' IN D.FIELDS SETTING CUS.POS THEN
        CUS.ID = D.RANGE.AND.VALUE<CUS.POS>       ;* get the customer id
    END

    LOCATE 'MESSAGE.ID' IN D.FIELDS SETTING MSG.POS THEN
        DE.MSG.ID = D.RANGE.AND.VALUE<MSG.POS>    ;* get the message id
    END

*
    IF NOT(CUS.ID) THEN
        ENQ.ERROR = 'CUSTOMER ID NOT SUPPLIED'
        RETURN
    END
*
    IF NOT(DE.MSG.ID) THEN
        ENQ.ERROR = 'MESSAGE ID NOT SUPPLIED'
        RETURN
    END
*
    F.CUSTOMER = ''
    FN.CUSTOMER = 'F.CUSTOMER'
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
*
    CALL F.READ(FN.CUSTOMER,CUS.ID,YR.CUST,F.CUSTOMER,ER)
    IF ER THEN
        ENQ.ERROR = 'CUSTOMER RECORD NOT FOUND'
        RETURN
    END
*
    CALL CACHE.READ('F.DE.MESSAGE',DE.MSG.ID,R.DE.MESSAGE,ER)
    IF ER THEN
        ENQ.ERROR = 'DE.MESSAGE RECORD NOT FOUND'
        RETURN
    END
*
    F.DE.ADDRESS = ''
    FN.DE.ADDRESS = 'F.DE.ADDRESS'
    CALL OPF(FN.DE.ADDRESS,F.DE.ADDRESS)
*
    RETURN
*
*** </region>
*
*** <region name= PROCESS>
*** <desc>Main Process </desc>
*
********
PROCESS:
********
*
    PRODUCT.MSG.ID = ID.COMPANY:'.C-':CUS.ID
    GOSUB CHECK.ROUTING
*
    IF RECORD.NOT.FOUND THEN
        ENQ.ERROR = 'NO PRODUCT RECORD FOUND'
        RETURN
    END
*
    CALL CACHE.READ('F.DE.PRODUCT', PROD.KEY, R.DE.PRODUCT, ER)
    IF ER THEN
        ENQ.ERROR = 'PRODUCT RECORD ':PROD.KEY:' NOT FOUND'
        RETURN
    END
*
    CARRIER.IDS = R.DE.PRODUCT<DE.PRD.CARR.ADD.NO>
    NO.OF.CAR = DCOUNT(CARRIER.IDS,@VM)

    FOR CAR.CNT = 1 TO NO.OF.CAR
        CARRIER.ADDR.NO = R.DE.PRODUCT<DE.PRD.CARR.ADD.NO,CAR.CNT>
        CARRIER = FIELD(CARRIER.ADDR.NO,'.',1)
        ADDR.NO = FIELD(CARRIER.ADDR.NO,'.',2)
        GOSUB GET.CARRIER
        GOSUB GET.DE.ADDRESS
    NEXT
*
    RETURN
*
*** </region>
*
**** <region name= CHECK.ROUTING>
*** <desc>Check DE.ROUTING & DE.PRODUCT for the given customer and message ID. </desc>
*
**************
CHECK.ROUTING:
**************
*
    RECORD.NOT.FOUND = 1
*
    ER = ''
    R.ROUTE = ''

    CALL CACHE.READ('F.DE.ROUTING', PRODUCT.MSG.ID, R.ROUTE, ER)
*
    IF ER THEN
        RETURN
    END
*
    RECORD.NOT.FOUND = 0

    PROD.KEY = PRODUCT.MSG.ID:'.':DE.MSG.ID:'.ALL'
    LOCATE PROD.KEY IN R.ROUTE<1,1> SETTING FOUND ELSE
        RECORD.NOT.FOUND = 1
    END

    RETURN
*** </region>
*
**** <region name= GET.DE.ADDRESS>
*** <desc>Get the corressponding address </desc>
*
***************
GET.DE.ADDRESS:
***************
*
    FLD.CNT =''
    ADDRESS =''
    R.ADDRESS = ''
    ADDRESS.ID = ID.COMPANY : '.C-' : CUS.ID : '.' : CARRIER.ADDR.NO
    CALL F.READ(FN.DE.ADDRESS, ADDRESS.ID, R.ADDRESS, F.DE.ADDRESS, ER)

    BEGIN CASE

    CASE CARRIER  EQ 'PRINT'
        FOR I = DE.ADD.BRANCHNAME.TITLE TO DE.ADD.COUNTRY
            IF R.ADDRESS<I> THEN
                FLD.CNT +=1
                ADDRESS<1,FLD.CNT> = R.ADDRESS<I>
            END
        NEXT I

    CASE CARRIER EQ 'SWIFT' OR CARRIER EQ 'TELEX'
        ADDRESS = R.ADDRESS<DE.ADD.DELIVERY.ADDRESS>

    CASE CARRIER EQ 'EMAIL'
        ADDRESS = R.ADDRESS<DE.ADD.EMAIL.1>

    CASE CARRIER EQ 'SMS'
        ADDRESS = R.ADDRESS<DE.ADD.SMS.1>

    CASE 1
        ADDRESS = R.ADDRESS<DE.ADD.BRANCHNAME.TITLE>

    END CASE

    ID.LIST<-1> = CUS.ID:'*':CARRIER:'*':ADDRESS
*
    RETURN
*
*** </region>
*
*** <region name= GET.CARRIER>
*** <desc>Read Carrier record </desc>
*
***************
GET.CARRIER:
***************
*
    R.CARRIER = ''
    CALL CACHE.READ('F.DE.CARRIER',CARRIER,R.CARRIER,ER)
    CARRIER.ADDR.NO = R.CARRIER<DE.CARR.ADDRESS>:'.':ADDR.NO
*
    RETURN
*
*** </region>
END

