* Version 1 13/04/00  GLOBUS Release No. G14.0.00 03/07/03
*
* Common area for Advisory Fee charge basis calculation
*
*-----------------------------------------------------------------------------
* Modification History:
* 24/10/07 - Creation  dadkinson@temenos.com
*
* 01/11/07 - CHARGE.AMT variable prefixed
*             K.SAFE.PARAMS added
*
* 13/11/07 - EN_10003565 - dadkinson@temenos.com
*            New variables to support PREV.MONTH.CLOSE method
*-----------------------------------------------------------------------------
*
* Passed:     SAM.ID                Portfolio key
*             SECURITY.NO           Security number
*             DEPOT                 Depository
*             NO.MONTHS             Number of months to be included in charge calculation
*             NOM.QTY.LCY           Used for AVERAGE NOMINAL calculation
*             R.GROUP.REC           SCSK.GROUP.CONDITION record
*             R.EXTRACT             SAFECUSTODY.EXTRACT record
*             CALL.TYPE             ACCRUAL or REAL
*             SUB.PROCESS           'OPEN' if new SAM
*             PERIOD.START.DATE     Start of charge period
*             DAILY.ACCRUAL         True or False
*             PERCENT               From PRICE.TYPE
*             BOND.OR.SHARE         From SECURITY.MASTER
*             CHG.PARAM$SINGLE.CHARGE.METHOD 'PREV.MONTH.CLOSE'  or Null
*             SAM.START.DATE        Portfolio starts
*             SAM.CLOSURE.DATE      Portfolio closure date
*             SAME.MONTH.CLOSURE    True if opened/closed in same month
*
* Returned:   R.CHG.PARAMS         Charge parameter record
*             CHG.PARAM$CHARGE.AMT Charge basis amount.
*                                  (prefix avoids clash with I_BV.SAFE.CHG.RECALC.COMMON)
*             CHG.TYPE             Charge type
*             CHG.RATE             Charge rate
*             MARKET.VALUE         Market values
*             AVG.BAL.MNTHS.CTOT   Average totalled for all holdings
*             NOM.QTY.VAL          Nominal Quantity Value
*             NOM.TO.DISPLAY       Nominal for display
*             MRKT.AVG.VAL         Market average value
*             NOM.QTY              Nominal quantity
*             K.SAFE.PARAMS        Charge parameter key
*             MONTH.END.DATES      Multivalued end dates for  relevant charge months
*             MONTH.CHARGE.AMTS    Corresponding chargeable values

      COMMON /SAFE.CHG.PARAM.COMMON/R.EXTRACT,
          PERCENT,
         BOND.OR.SHARE,
         MARKET.VALUE,
         NOM.QTY.VAL,
         NOM.TO.DISPLAY,
         MRKT.AVG.VAL,
         NOM.QTY,
         MONTH.END.DATES,
         MONTH.CHARGE.AMTS

