*-----------------------------------------------------------------------------
* <Rating>-44</Rating>
*-----------------------------------------------------------------------------
    $PACKAGE ST.ChqIssue

    SUBROUTINE CHEQUE.ISSUE.FIELD.DEFINITIONS
*-----------------------------------------------------------------------------
*
*----------------------------------------------------------------------------------------
* Modification History :
*
* 19/02/99 - GB9900129
*            Add new reserved fields.
*
*            This application must additionally check to see that if the
*            field CHEQUE.NOS.USED has been set to 'YES' the cheque nos.
*            being issued here do not exist in the field CHEQUE.NOS.USED
*
*            If the CHEQUE.REGISTER field on the ACCOUNT.PARAMETER record
*            has been set to 'YES' and the CHEQUE.TYPE of the cheques being
*            issued have not been linked to a TRANSACTION record then an
*            error must be thrown at the user. The concat file TRN.CHQ.TRNS
*            could be used to check this.
*
* 06/09/01 - GLOBUS_EN_10000101
*            Enhanced Cheque.Issue to collect charges at each Status
*            and link to Soft Delivery
*            Changed Cheque.Issue to standard template
*            Changed all values captured in ER to capture in E
*            - All the variables are set in I_CI.COMMON
*
*            New fields added to the template are
*            - Cheque.Status
*            - Chrg.Code
*            - Chrg.Amount
*            - Tax.Code
*            - Tax.Amt
*            - Waive.Charges
*            - Class.Type       : -   Link to Soft Delivery
*            - Message.Class    : -      -  do  -
*            - Activity         : -      -  do  -
*            - Delivery.Ref     : -      -  do  -
*
* 17/10/01 - GLOBUS_BG_100000146
*            Modification of definition of field NOTES corrected
*
* 20/11/01 - GLOBUS_CI_10000527
*            Modifying N array of field CHQ.NO.START with correct format
*
* 14/02/02 - GLOBUS_EN_10000353
*            Introduce the fields STOCK.REGISTER,STOCK.NUMBER, SERIES.ID for stock
*            application.
*
*18/03/02 -  GLOBUS_BG_100000738
*            STOCK.REGISTER,SERIES.ID & AUTO.CHEQUE.NO made noinputtable field.
*
* 26/03/02 - GLOBUS_BG_100000778
*            Include check field validation for STOCK.REG. & SERIES.ID
*
* 12/07/05 - EN_10002578
*            Browser issues in CC.
*            SERIES.ID and STOCK.REG is made as inputtable fields.
*            This is because, when SERIES.ID is made as HOT.FIELD to populate CHQ.NO.START, check.fields will
*            will not get trigerred for SERIES.ID, as this is defined as NOINPUT field in template level.
*
* 14/12/06 - BG_100012531
*            Problem with CHQ.NO.START field.
*
* 07/02/07 - EN_10003189
*            When LAST.EVENT.SEQ of CHEQUE.REGISTER reaches 99999 no new cheque issue record
*            is being allowed to input.
*
* 28/06/07 - CI_10051630
*            T array fields are made as inputtable for easy handling of Browser.
*
* 04/03/09 - CI_10061060
*            The length of cheque number is increased
*
* 02/03/15 - Enhancement 1265068 / Task 1269515
*           - Rename the component CHQ_Issue as ST_ChqIssue and include $PACKAGE
*
* -----------------------------------------------------------------------------------------
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.CHEQUE.ISSUE
    $INSERT I_F.CHEQUE.STATUS ;* EN_10000101
    $INSERT I_F.EB.MESSAGE.CLASS        ;* EN_10000101
    $INSERT I_CI.COMMON       ;* EN_10000101

    $INSERT I_F.STOCK.REGISTER          ;* GLOBUS_BG_100000738
*
*-----------------------------------------------------------------------------
    GOSUB INITIALISE

    GOSUB DEFINE.FIELDS

    RETURN
*
*-----------------------------------------------------------------------------
*
DEFINE.FIELDS:

    OBJECT.ID="ACCOUNT"
    CQ$MAX.LEN=""
    CALL EB.GET.OBJECT.LENGTH(OBJECT.ID,CQ$MAX.LEN)
    ID.LEN=13+CQ$MAX.LEN      ;* EN_10003189 - S/E
*
    ID.F = "CHEQUE.TYPE.NO" ; ID.N = ID.LEN:'.1' ; ID.T = "A"
    Z = 0

*EN_10000101 -s
    Z +=1 ; F(Z)='CHEQUE.STATUS' ; N(Z)='2..C' ; T(Z)=''
    CHECKFILE(Z)='CHEQUE.STATUS':FM:CHEQUE.STS.DESCRIPTION
*EN_10000101 -e
    Z +=1 ; F(Z)='ISSUE.DATE' ; N(Z)='11..C' ; T(Z)='D'
    Z +=1 ; F(Z)='NUMBER.ISSUED' ; N(Z)='5..C' ; T(Z)=''
    Z +=1 ; F(Z)='CURRENCY' ; N(Z)='3..C' ; T(Z)='CCY' ; T(Z)<3>='NOINPUT'
    Z +=1 ; F(Z)='CHARGES' ; N(Z)='019..C' ; T(Z)='AMT' ; T(Z)<2,2>=CHEQUE.IS.CURRENCY
    Z +=1 ; F(Z)='CHARGE.DATE' ; N(Z)='11..C' ; T(Z)='D'

* GLOBUS_EN_10000353 -S

** GLOBUS_BG_100000738 -S

    Z+=1 ; F(Z) = "STOCK.REG" ; N(Z)="35..C" ; T(Z) = "A"   ;* GLOBUS_BG_100000778  ;*EN_10002578 S
    CHECKFILE(Z) = "STOCK.REGISTER":FM:FM:"L"
    Z+=1 ; F(Z) = "SERIES.ID" ; N(Z)="35..C" ; T(Z)<1>="A"  ;* GLOBUS_BG_100000778 ;*EN_10002578 E

** GLOBUS_BG_100000738 -E

* GLOBUS _EN_10000353 -E

*     Chq.No.Start is accepted only when status is like 'ISSUE',
*     otherwise it is not accepted

*      Z +=1 ; F(Z)='CHQ.NO.START' ; N(Z)='14..C' ; T(Z)='' ; T(Z)<3>='NOCHANGE'  ; *EN_10000101
    Z +=1 ; F(Z)='CHQ.NO.START' ; N(Z)='0035..C' ; T(Z)=''

*      Z +=1 ; F(Z)='XX.NOTES' ; N(Z)='35' ; T(Z)='A'      ; * EN_10000101 - commented
*      Z +=1 ; F(Z)='NOTES' ; N(Z)='35' ; T(Z)='TEXT'         ; * EN_10000101
    Z +=1 ; F(Z)='XX.NOTES' ; N(Z)='35' ; T(Z)='S' ; T(Z)<7>='TEXT'   ;* BG_100000146

*EN_10000101 -s
    Z +=1 ; F(Z)='XX<CHG.CODE' ; N(Z)='20..C' ; T(Z)='CHG' ; T(Z)<2> = 'CHG':VM:'COM'
    Z +=1 ; F(Z)='XX>CHG.AMOUNT' ; N(Z)='019..C' ; T(Z)='AMT' ; T(Z)<2,2> = CHEQUE.IS.CURRENCY
    Z +=1 ; F(Z)='XX<TAX.CODE' ; N(Z)='3..C' ; T(Z)='CHG' ; T(Z)<3>='NOINPUT'
    Z +=1 ; F(Z)='XX>TAX.AMT' ; N(Z)='019..C' ; T(Z)='AMT' ; T(Z)<2,2> = CHEQUE.IS.CURRENCY ; T(Z)<3>='NOINPUT'

    Z +=1 ; F(Z)='WAIVE.CHARGES' ; N(Z)='3.1.C' ; T(Z)<2>='YES_NO'
    Z += 1 ; F(Z) = 'XX<CLASS.TYPE' ; N(Z) = '20..C' ;
    T(Z)<2> = "USERDEFINE.1_USERDEFINE.2_USERDEFINE.3_USERDEFINE.4_USERDEFINE.5_USERDEFINE.6_USERDEFINE.7_USERDEFINE.8_USERDEFINE.9"
    Z += 1 ; F(Z) = 'XX>MESSAGE.CLASS' ; N(Z) = '15..C' ; T(Z) = 'SSS'
    CHECKFILE(Z) = "EB.MESSAGE.CLASS":FM:EB.MC.DESCRIPTION
    Z +=1 ; F(Z)='ACTIVITY' ; N(Z)='5' ; T(Z)='A' ; T(Z)<3>='NOINPUT'
    Z +=1 ; F(Z)='XX.DELIVERY.REF' ; N(Z)='16' ; T(Z)='A' ; T(Z)<3>='NOINPUT'
* EN_10000101 -e

* GB9900129 (Starts)

** GLOBUS_EN_10000353 - S

    Z+=1 ; F(Z) = "AUTO.CHEQUE.NUMBER" ; N(Z)="20" ; T(Z)<1>="A" ; T(Z)<3> = "NOINPUT"

** GLOBUS_EN_10000353 - E

    Z+=1 ; F(Z) = "RESERVED.9" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.8" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.7" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.6" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.5" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.4" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.3" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.2" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.1" ; N(Z)="35" ; T(Z)<1>="A" ; T(Z)<3>="NOINPUT"
* GB9900129 (Ends)
    Z+=1 ; F(Z)='XX.LOCAL.REF' ; N(Z)='35' ; T(Z)='A'
* EN_10000101 -s
    Z+=1 ; F(Z) = 'XX.STMT.NO' ; N(Z) = "35" ; T(Z)<1> = 'A' ; T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z) = 'XX.OVERRIDE' ; N(Z) = "35" ; T(Z)<1> = 'A' ; T(Z)<3> = 'NOINPUT'
* EN_10000101 -e
******
    V = Z + 9
    RETURN
*
*-----------------------------------------------------------------------------
*
INITIALISE:
*----------
    MAT F = "" ; MAT N = "" ; MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = "AL"

    RETURN
*-----------(Initialise)

*-----------------------------------------------------------------------------

END
*-----(End of Cheque.Issue.Field.Definitions)

