*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
*

* Subroutine Type: Subroutine

* Incoming : ENQ.DATA Common Variable contains all the Enquiry Selection Criteria Details

* Outgoing : ENQ.DATA Common Variable

* Attached to : ENQUIRY STMT.ENT.BOOK

* Purpose  : To get the Selection Field values and passed to main routine
*-----------------------------------------------------------------------------
* MODIFICATION HISTORY:
*************************
* 07/12/11 - Defect 319668 / Task 320322
*            Validation added to check for value,book or processing.date.
*
* 06/02/13 - Defect - 539102 / Task - 584381
*            System doesn't display the static info. when there are no entries to display it.
*
* 16/04/13 - Defect - 640740 / Task - 651632
*            When enquiry STMT.ENT.BOOK is launched without without any date then
*            enquiry throws server disconnected error.
*
** 30/10/13 - Defect 801588 / Task 824032
*            In STMT.ENT.BOOK enquiry, FIXED.SORT field as VALUE.DATE. Hence entry�s
*            sorted based on VALUE.DATE, but client requirement is to sort the entries based on BOOKING.DATE or as per the sort selection.
*            FIXED.SORT field has been removed from enquiry and code is introduced to sort as per the user requirement.
************************************************************************************************

      $PACKAGE AC.ModelBank

    SUBROUTINE E.MB.STMT.BOOK.BUILD(ENQ.DATA)

*************************************************************************************************

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_F.ENQUIRY

***************************************************************************************************

    GOSUB INITIALISE
    GOSUB PROCESS
    IF DATE.TO.PROCESS THEN
        GOSUB CHECK.IF.MVMT.EXISTS
    END

    RETURN
****************************************************************************************************

INITIALISE:

    ACCT.POS = ""
    ACCT.NO = ""
    BOOK.POS = ""
    BOOK.VAL = ""
    PROCESS.POS = ""
    PROCESS.VAL = ""
    VALUE.POS = ""
    VALUE.VAL = ""
    OPER.VAL = ""
    SET.FLAG = ""
    DATE.TO.PROCESS = ""
    Y.DATES = ""
    OLD.ENQ.DATA = ENQ.DATA
    RETURN

PROCESS:


    LOCATE "ACCT.ID" IN ENQ.DATA<2,1> SETTING ACCT.POS THEN
        ACCT.NO = ENQ.DATA<4,ACCT.POS>
    END

    LOCATE "BOOKING.DATE" IN ENQ.DATA<2,1> SETTING BOOK.POS THEN
        BOOK.VAL = ENQ.DATA<4,BOOK.POS>
        OPER.VAL = ENQ.DATA<3,BOOK.POS>
        IF BOOK.VAL  THEN
            DATE.TO.PROCESS = 'BOOK'
        END
    END

    LOCATE "VALUE.DATE" IN ENQ.DATA<2,1> SETTING VALUE.POS THEN
        VALUE.VAL = ENQ.DATA<4,VALUE.POS>
        OPER.VAL = ENQ.DATA<3,VALUE.POS>
        IF VALUE.VAL THEN
            DATE.TO.PROCESS = 'VALUE'
        END
    END

    LOCATE "PROCESSING.DATE" IN ENQ.DATA<2,1> SETTING PROCESS.POS THEN
        PROCESS.VAL = ENQ.DATA<4,PROCESS.POS>
        OPER.VAL = ENQ.DATA<3,PROCESS.POS>
        IF PROCESS.VAL THEN
            DATE.TO.PROCESS = 'PROCESS'
        END
    END
* Do not modify ENQ.DATA when selection has any two dates.
    BEGIN CASE
        CASE BOOK.VAL
            IF PROCESS.VAL NE '' OR  VALUE.VAL NE '' THEN
                ENQ.DATA = OLD.ENQ.DATA
                RETURN
            END
        CASE PROCESS.VAL
            IF BOOK.VAL NE '' OR  VALUE.VAL NE '' THEN
                ENQ.DATA = OLD.ENQ.DATA
                RETURN
            END

        CASE VALUE.VAL
            IF BOOK.VAL NE '' OR  PROCESS.VAL NE '' THEN
                ENQ.DATA = OLD.ENQ.DATA
                RETURN
            END

    END CASE

    ENQ.DATA<2,1> = "ACCT.ID"
    ENQ.DATA<3,1> = "EQ"
    ENQ.DATA<4,1> = ACCT.NO
    GOSUB FILTER.SORT
    GOSUB USER.DEFINED.SORT   ;*sorting data based on sort.
    RETURN

*----------------------------------------------------------------
FILTER.SORT:
*************
*If enquiry does'nt have sorting condition(ENQ.DATE<9> Equail to null)  , than system should consider the selection filed
    BEGIN CASE

        CASE DATE.TO.PROCESS = 'BOOK'

            ENQ.DATA<2,2> = "BOOKING.DATE"
            ENQ.DATA<3,2> =  OPER.VAL
            ENQ.DATA<4,2> = BOOK.VAL
            IF ENQ.DATA<9> EQ '' THEN       ;*Enquiry doesn't have sorting condition
                R.ENQ<4,1> = "BOOKING.DATE" ;*pass the booking. date
                R.ENQ<4,2> ="DATE.TIME"
            END
        CASE DATE.TO.PROCESS = 'VALUE'

            ENQ.DATA<2,2> = "VALUE.DATE"
            ENQ.DATA<3,2> =  OPER.VAL
            ENQ.DATA<4,2> = VALUE.VAL
            IF ENQ.DATA<9> EQ '' THEN
                R.ENQ<4,1> = "VALUE.DATE"
                R.ENQ<4,2> ="DATE.TIME"
            END
        CASE DATE.TO.PROCESS = 'PROCESS'

            ENQ.DATA<2,2> = "PROCESSING.DATE"
            ENQ.DATA<3,2> =  OPER.VAL
            ENQ.DATA<4,2> = PROCESS.VAL
            IF ENQ.DATA<9> EQ '' THEN
                R.ENQ<4,1> = "PROCESSING.DATE"
                R.ENQ<4,2> ="DATE.TIME"
            END
    END CASE

    RETURN
*------------------------------------------------------
USER.DEFINED.SORT:
******************
*if enquiry has sorting condition, than pass the enquiry (R.ENQ<4>) as per the sorting order
    IF ENQ.DATA<9> THEN
        FIXED.CNT = DCOUNT(ENQ.DATA<9>,VM)        ;* Dcount the user sort
        IF R.ENQ<4> THEN      ;*if FIXED.SORT is set in Enquiry
            SAVE.ENQ.SORT=R.ENQ<4>
        END
        SORT.CNT = 1
        LOOP
        WHILE SORT.CNT LE FIXED.CNT
            R.ENQ<4,SORT.CNT> = ENQ.DATA<9,SORT.CNT>
            SORT.CNT += 1
        REPEAT
        IF SAVE.ENQ.SORT THEN
            Y.DATE.CNT=FIXED.CNT + 1
            R.ENQ<4,Y.DATE.CNT>=SAVE.ENQ.SORT     ;*adding DATE&TIME to the last position
        END
    END

    RETURN
*-------------------------------------------------------------------------------------
CHECK.IF.MVMT.EXISTS:
*********************
* Check whether any transaction exists within the statement period if so then modify the selection so that
* core routine will form dummy entry.
*
    IF ENQ.DATA<4,2> EQ '!TODAY' THEN
        ENQ.DATA<4,2> = TODAY                           ;* If period is today, assign today's date.
    END

    GOSUB CONV.OPER.VAL.TO.NUM.OPER.VAL           ;* Core enquriy can understand the operands only in numeric.

    ENTRY.LIST = ''                               ;* System will return the entries if exist for the given account.

    IF OPER.VAL NE 'RG' THEN
        Y.DATES = ENQ.DATA<4,2>                       ;* Get the selection period.
    END

    CALL AC.GET.ACCT.ENTRIES(ACCT.NO, NUM.OPER.VAL, Y.DATES, DATE.TO.PROCESS, ENTRY.LIST)   ;* Check for entries for the given account.

    IF NOT(ENTRY.LIST) THEN                       ;* No entries exist. Append another condition to pick atleast dummy entry.
        ENQ.DATA<15,2> = 'OR'                     ;* Join the conditions using 'OR' conjunction.
        ENQ.DATA<2,3> = ENQ.DATA<2,2>
        ENQ.DATA<3,3> = 'EQ'
        ENQ.DATA<4,3> = ''
    END

    RETURN
*------------------------------------------------------------------------
CONV.OPER.VAL.TO.NUM.OPER.VAL:
******************************

    NUM.OPER.VAL = ''          ;* Possible operands are EQ, RG, LT, LE, GT, GE.

    BEGIN CASE
        CASE OPER.VAL EQ 'EQ'
            NUM.OPER.VAL = 1
        CASE OPER.VAL EQ 'RG'
            NUM.OPER.VAL = 2
            IF NOT(INDEX(ENQ.DATA<4,2>, ' ', 1)) THEN
                ENQ.DATA<4,2> = ENQ.DATA<4,2>:' ':ENQ.DATA<4,2>
                Y.DATES = ENQ.DATA<4,2>:@VM:ENQ.DATA<4,2>
            END ELSE
                START.DATE = FIELD(ENQ.DATA<4,2>, ' ', 1)
                END.DATE = FIELD(ENQ.DATA<4,2>, ' ', 2)
                Y.DATES = START.DATE:@VM:END.DATE
            END
        CASE OPER.VAL EQ 'LT'
            NUM.OPER.VAL = 3
            CALL CDT('', ENQ.DATA<4,2>, '-1C')
        CASE OPER.VAL EQ 'LE'
            NUM.OPER.VAL = 8
        CASE OPER.VAL EQ 'GT'
            NUM.OPER.VAL = 4
            CALL CDT('', ENQ.DATA<4,2>, '+1C')
        CASE OPER.VAL EQ 'GE'
            NUM.OPER.VAL = 9
    END CASE

    RETURN
*------------------------------------------------------------------------
    END

