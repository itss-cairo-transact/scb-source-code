* Version 2 05/02/97  GLOBUS Release No. G7.2.00 14/03/97
*************************************************************************
* I_MI.PL.COMMON
*
* Common area used during the P & L allocation in MI. Main purpose is to
* hold various records and file pointers so that these to do have to
* be initialised every time the EOD.MI.PL.ALLOCATE.2 routine is called.
*
* Also holds all of the netting information.
*
* This common block is initialised in the routine LOAD.MI.PL.COMMON
* which is called from EOD.MI.PL.ALLOCATE.
*
*************************************************************************
* 03/07/96 - GB9600905
*            Initial version
*
* 29/01/97 - GB9700101
*            Modification of layout due to netting processing being
*            run in MI.PROCESS.NETTING, and as defined on the
*            MI.NETTING.PARAMS records.
*
* 08/10/2001 - GLOBUS_CI_2459
*              Stores debit category from R.ENTRY.FROM record in
*              MI.PROCESS.NETTING  Subroutine
*
* 19/05/2009 - GLOBUS_BG_100023601
*              Introducing a new Common variable NO.OF.RECORDS.SAVE to store the number
*              of Ids in the array MI.AUTO.CONCAT.RECS
*************************************************************************
*
* CATEGORY.LIST<1> = LIst of category ids
* CATEGORY.LIST<2> = List of MIPL2 rules to check for this category. Includes rules from MI.CATEG.GROUP
*
    COM/MIPL/CATEGORY.LIST,
*
* MI.PL2.LIST<1> = List of MI.PL.ALLOCATION.2 ids
* MI.PL2.LIST<2> = Lowered MI.PL.ALLOCATION.2 records.
*
    MI.PL2.LIST,

* File pointers

    F.MI.AUTO.ENTRY.MONTH,
    F.MI.AUTO.ENTRY,
    F.MI.AUTO.ENTRY.DICT,
    F.CATEGORY,
    F.CATEG.ENTRY.DICT,
    F.MI.AUTO.MAPPING,

* File names

    FN.MI.AUTO.ENTRY.MONTH,
    FN.MI.AUTO.ENTRY,
    FN.CATEGORY,
    FN.MI.AUTO.MAPPING,

* Field defintion saves

    CATEG.FIELD.LIST,
    CATEG.FIELD.TYPE.LIST,
    CATEG.FIELD.NO.LIST,

    AUTO.FIELD.LIST,
    AUTO.FIELD.TYPE.LIST,
    AUTO.FIELD.NO.LIST,

    CATEG.DICT.LIST,
    CATEG.DICT.REC.LIST,

    AUTO.DICT.LIST,
    AUTO.DICT.REC.LIST,

    CREATE.RECORD.2,

* The netting information must be kept here for use in MI.PROCESS.NETTING

    CHANGE.2,       ;* No of records netted to each netted record
    ID.TO.2,        ;*  id of the TO record to be written
    ID.FROM.2,      ;*  id of the FROM record to be written
    NETTED.FIELDS,  ;* The evaluated netted fields as defined on MI.NETTING.PARAMS
    NETTED.FIELDS.2,          ;*2459 S/E
    NETTING.PARAMS.IDS,       ;* List of MI.NETTING.PARAMS ids
    NETTING.PARAMS.FIELDS,    ;* List of the netting fields for each MI.NETTING.PARAMS record
    NETTING.PARAMS.DATES,     ;* List of either VALUE or MONTHLY for each MI.NETTING.PARAMS record.
    TYPES.OF.NETTING,
    MI.AUTO.CONCAT.KEYS,      ;* Used to process the MI.AUTO.ENTRY.MONTH file.
    MI.AUTO.CONCAT.RECS,      ;* Used to process the MI.AUTO.ENTRY.MONTH file.
    NET.LCY.2,      ;*  LCY amount
    NET.FCY.2,      ;*  FCY amount
    NO.OF.NETS.2,   ;* Current number of netting records.
    COUNT.2,

    TDATE,

* Used to allocate the MI.ENTRY.TYPE to records.

    CATEGORY.IDS,   ;* List of category ids
    CATEGORY.TYPES, ;* List of corresponding MI.ENTRY.TYPE
    REC.MI.AUTO.ENT.MNTH,     ;* Save MI.AUTO.ENTRY.MONTH record if it already exists
    CONCAT.KEY.SAVE,          ;* Save the previous concat key
    NEW.RECORD      ;* variable to check if the record is a new record or not
