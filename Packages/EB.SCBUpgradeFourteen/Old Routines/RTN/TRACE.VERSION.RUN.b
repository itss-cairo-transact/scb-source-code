* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>395</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE TRACE.VERSION.RUN( NEST.LEVEL, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, R.ITEM)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION

** ----- ----- -----
MAIN:
** ----- ----- -----

      ** ----- associated versions -----

      ASSOC.VERSION = R.ITEM< EB.VER.ASSOC.VERSION>
      ASSOC.VERSION.NO = DCOUNT( ASSOC.VERSION, @VM)
      IF ASSOC.VERSION.NO THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Associated versions ::')
         FOR ASSOC.VERSION.IDX = 1 TO ASSOC.VERSION.NO

*Line [ 43 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'VERSION', NEST.LEVEL + 1, ASSOC.VERSION< 1, ASSOC.VERSION.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'ASSOCIATED VERSION')

         NEXT ASSOC.VERSION.IDX

      END

      ** ----- next version -----

      NEXT.VERSION = R.ITEM< EB.VER.NEXT.VERSION>
      IF NEXT.VERSION THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Next version ::')
*Line [ 56 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
         CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'VERSION', NEST.LEVEL + 1, NEXT.VERSION, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'NEXT VERSION')

      END

      ** ----- deal slip formats -----

      D.SLIP.FORMAT = R.ITEM< EB.VER.D.SLIP.FORMAT>
      D.SLIP.FORMAT.NO = DCOUNT( D.SLIP.FORMAT, @VM)
      IF D.SLIP.FORMAT.NO THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Deal slip formats ::')
         FOR D.SLIP.FORMAT.IDX = 1 TO D.SLIP.FORMAT.NO

*Line [ 70 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'DEAL.SLIP.FORMAT', NEST.LEVEL + 1, D.SLIP.FORMAT< 1, D.SLIP.FORMAT.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'DEAL SLIP FORMAT')

         NEXT D.SLIP.FORMAT.IDX

      END

      ** ----- local fields -----

      LOCAL.FIELDS.HEADER = 1

      VER.FIELD = R.ITEM< EB.VER.FIELD.NO>
      VER.FIELD.NO = DCOUNT( VER.FIELD, @VM)
      FOR VER.FIELD.IDX = 1 TO VER.FIELD.NO

         IF FIELD( VER.FIELD< 1, VER.FIELD.IDX>, '-', 1) = 'LOCAL.REF' THEN

            IF LOCAL.FIELDS.HEADER THEN LOCAL.FIELDS.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Local fields ::')

            LOCAL.FIELD = FIELD( FIELD( VER.FIELD< 1, VER.FIELD.IDX>, '-', 2), '.', 1)
*Line [ 90 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'LOCAL.REF.TABLE', NEST.LEVEL + 1, FIELD( ITEM.NAME, ',', 1):'!':LOCAL.FIELD, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'LOCAL FIELD')

         END

      NEXT VER.FIELD.IDX

      ** ----- list enquiries -----

      NO.ENQUIRY.HEADER = 1

      LIST.ENQUIRY = ITEM.NAME : '-LIST'
      GOSUB PROCESS.LIST.ENQUIRY

      LIST.ENQUIRY = '%' : ITEM.NAME
      GOSUB PROCESS.LIST.ENQUIRY

      ENQ.APPLICATION = FIELD( ITEM.NAME, ',', 1)
      ENQ.VERSION.NAME = FIELD( ITEM.NAME, ',', 2)

      LIST.ENQUIRY = '%' : ENQ.APPLICATION : '$NAU,' : ENQ.VERSION.NAME
      GOSUB PROCESS.LIST.ENQUIRY

      LIST.ENQUIRY = '%' : ENQ.APPLICATION : '$HIS,' : ENQ.VERSION.NAME
      GOSUB PROCESS.LIST.ENQUIRY

      LIST.ENQUIRY = '%' : ITEM.NAME : '-DEFAULT'
      GOSUB PROCESS.LIST.ENQUIRY

      ** ----- auto new content routines -----

      NEW.CONT.ROUTINE = R.ITEM< EB.VER.AUT.NEW.CONTENT>
      NEW.CONT.ROUTINE.NO = DCOUNT( NEW.CONT.ROUTINE, @VM)
      IF NEW.CONT.ROUTINE.NO THEN

         NO.NEW.CONT.HEADER = 1
         FOR NEW.CONT.ROUTINE.IDX = 1 TO NEW.CONT.ROUTINE.NO

            IF NEW.CONT.ROUTINE< 1, NEW.CONT.ROUTINE.IDX>[ 1, 1] = '@' THEN

               IF NO.NEW.CONT.HEADER THEN NO.NEW.CONT.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Auto new content routines ::')
*Line [ 131 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
               CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, FIELD( NEW.CONT.ROUTINE< 1, NEW.CONT.ROUTINE.IDX>, '@', 2), REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'AUTO NEW CONTENT ROUTINE')

            END

         NEXT NEW.CONT.ROUTINE.IDX

      END

      ** ----- validation routines -----

      VAL.ROUTINE = R.ITEM< EB.VER.VALIDATION.RTN>
      VAL.ROUTINE.NO = DCOUNT( VAL.ROUTINE, @VM)
      IF VAL.ROUTINE.NO THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Validation routines ::')
         FOR VAL.ROUTINE.IDX = 1 TO VAL.ROUTINE.NO

*Line [ 149 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, VAL.ROUTINE< 1, VAL.ROUTINE.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'VALIDATION ROUTINE')

         NEXT VAL.ROUTINE.IDX

      END

      ** ----- input routines -----

      INPUT.ROUTINE = R.ITEM< EB.VER.INPUT.ROUTINE>
      INPUT.ROUTINE.NO = DCOUNT( INPUT.ROUTINE, @VM)
      IF INPUT.ROUTINE.NO THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Input routines ::')
         FOR INPUT.ROUTINE.IDX = 1 TO INPUT.ROUTINE.NO

*Line [ 165 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, INPUT.ROUTINE< 1, INPUT.ROUTINE.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'INPUT ROUTINE')

         NEXT INPUT.ROUTINE.IDX

      END

      ** ----- authorisation routines -----

      AUTH.ROUTINE = R.ITEM< EB.VER.AUTH.ROUTINE>
      AUTH.ROUTINE.NO = DCOUNT( AUTH.ROUTINE, @VM)
      IF AUTH.ROUTINE.NO THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Authorisation routines ::')
         FOR AUTH.ROUTINE.IDX = 1 TO AUTH.ROUTINE.NO

*Line [ 181 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, AUTH.ROUTINE< 1, AUTH.ROUTINE.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'AUTHORISATION ROUTINE')

         NEXT AUTH.ROUTINE.IDX

      END

      ** ----- ----- -----

      RETURN

** ----- ----- -----
PROCESS.LIST.ENQUIRY:
** ----- ----- -----

      ETEXT = ''
      CALL DBR( 'ENQUIRY':@FM:1, LIST.ENQUIRY, MY.DUMMY)
      IF ETEXT THEN ETEXT = ''
      ELSE

         IF NO.ENQUIRY.HEADER THEN NO.ENQUIRY.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: List enquiries ::')
*Line [ 202 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
         CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ENQUIRY', NEST.LEVEL + 1, LIST.ENQUIRY, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'LIST ENQUIRY')

      END

      RETURN

** ----- ----- -----

END
