* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>251</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.AC.LOCK.LD.OFS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*-----------------------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(COLL.RECORD.STATUS)='INAU' THEN
        LOCK.FLG  = R.NEW(COLL.LOCAL.REF)<1,COLR.LOCK.FLG>
        IF LOCK.FLG EQ 'YES' THEN
            GOSUB INITIALISE
            GOSUB PROCESS
        END
    END
*--- Reverse -----------------------------------------------------
    IF V$FUNCTION = 'A' AND R.NEW(COLL.RECORD.STATUS)='RNAU' THEN
        LOCK.FLG  = R.NEW(COLL.LOCAL.REF)<1,COLR.LOCK.FLG>

        IF LOCK.FLG EQ 'YES' THEN
            GOSUB INITIALISE
            GOSUB PROCESS
        END
    END
*----------------------------------------------------------------
    RETURN
*----------------------------------------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E
    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS   = "AC.LOCK.OFS"
    COMP          = C$ID.COMPANY
    COMP.CODE     = COMP[8,2]

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "AC.LOCKS.OFS.":ID.NEW
    RETURN
*----------------------------------------------------
PROCESS:
*-------
    FLG = 0

    FN.AC.LOC = "F.COLLATERAL"    ; F.AC.LOC = ""
    CALL OPF(FN.AC.LOC, F.AC.LOC)

    ACCT.NUM = R.NEW(COLL.APPLICATION.ID)
    DEPT.NO  = R.NEW(COLL.LOCAL.REF)<1,COLR.DEPT.ISSUE>
*------------- IF APP.ID EQ LD... ---------------------------
    IF ACCT.NUM[1,2] EQ 'LD' THEN
        FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"   ; F.LD = ""
        CALL OPF(FN.LD,F.LD)

        LD.NUM   = ACCT.NUM
        CALL F.READ(FN.LD,LD.NUM, R.LD, F.LD ,E.LD)
        ACCT.NUM = R.LD<LD.PRIN.LIQ.ACCT>
    END
*------------------------------------------------------------
    IF DEPT.NO EQ '2400' THEN
        TRNS.TYPE = 'AC'
        DR.ACC    = ACCT.NUM

        FN.ACC = "FBNK.ACCOUNT"   ; F.ACC = ""
        CALL OPF(FN.ACC,F.ACC)

        CALL F.READ(FN.ACC,DR.ACC,R.ACC, F.ACC, E.ACC1)
        DR.CUR = R.ACC<AC.CURRENCY>
        CR.ACC = 'PL54025'

        W.BAL   = R.ACC<AC.WORKING.BALANCE>
        CUS.NUM = R.ACC<AC.CUSTOMER>

        FN.CUS  = "FBNK.CUSTOMER"   ; F.CUS = ""
        CALL OPF(FN.CUS,F.CUS)
        CALL F.READ(FN.CUS,CUS.NUM, R.CUS, F.CUS, E.CUS)
        CUST.COMP      = R.CUS<EB.CUS.COMPANY.BOOK>
        COMP.CODE      = CUST.COMP[8,2]
        OFS.USER.INFO  = "INPUTT" : COMP.CODE : "//" :CUST.COMP

*-------- CALCULATE AMOUNT -------------------------------------
        LIM.REF = R.ACC<AC.LIMIT.REF>
        ACT.CUS = R.ACC<AC.CUSTOMER>

        LIM.ID = ACT.CUS:".":"0000":LIM.REF
        CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM.ID,AVIL.BAL)
        CALC.AMT = W.BAL + AVIL.BAL

        LOC.BAL  = R.ACC<AC.LOCKED.AMOUNT>

*Line [ 151 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        BALC     = DCOUNT(LOC.BAL,@VM)
        BALCC    = LOC.BAL<1,BALC>
        CALC.AMT = CALC.AMT - BALCC
        CALC.AMT = W.BAL
        AMOUNT   = 20
        CUR.CODE = DR.ACC[9,2]
        CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.CODE,CUR.NAME)
        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR.NAME,RATE)

        CR.CUR   = CUR.NAME

        IF CUR.NAME NE "EGP" THEN
            AMOUNT = ( AMOUNT / RATE<1,1> )
            AMOUNT = DROUND(AMOUNT,2)
        END

        IF CALC.AMT LT AMOUNT[1,4] THEN
            GOSUB GET.DR.ACCT
            GOSUB FLG.RET
        END

        GOSUB BUILD.RECORD
***** SCB R15 UPG 20160628 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
    END
    RETURN
*------------------------------------------------------------
GET.DR.ACCT:
*-----------
    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"   ; F.CUS.ACC = ""
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    CALL F.READ(FN.CUS.ACC,CUS.NUM, R.CUS.ACC, F.CUS.ACC ,ETEXT)
    LOOP
        REMOVE ACCOUNT.NUMBER FROM R.CUS.ACC SETTING POS.ACC
    WHILE ACCOUNT.NUMBER:POS.ACC
        CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACCT, F.ACC, E.ACC1)

        W.BAL    = R.ACCT<AC.WORKING.BALANCE>
        CALC.AMT = W.BAL
        AMOUNT   = 20
        CUR.CODE = ACCOUNT.NUMBER[9,2]
        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR.CODE,RATE)

        CR.CUR   = CUR.CODE
        CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.CODE,CUR.NAME)

        IF CUR.NAME EQ "EGP" THEN
            RATE   = 1
            AMOUNT = (AMOUNT / RATE<1,1>)
            AMOUNT = DROUND(AMOUNT,2)
        END
        IF CUR.NAME NE "EGP" THEN
            AMOUNT = (AMOUNT / RATE<1,1>)
            AMOUNT = DROUND(AMOUNT,2)
        END

        IF CALC.AMT GE AMOUNT[1,4] THEN
            CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACCT, F.ACC, E.ACC1)
            CATEG = R.ACCT<AC.CATEGORY>
            IF FLG EQ 0 THEN
                IF (CATEG EQ 3050) OR (CATEG EQ 3005) OR (CATEG EQ 3010) OR (CATEG EQ 3011) OR (CATEG EQ 3012) OR (CATEG EQ 3013) OR (CATEG EQ 3060) THEN
****************** DO NOTHING
                END ELSE
                    DR.ACC = ACCOUNT.NUMBER
                    DR.CUR = R.ACCT<AC.CURRENCY>
                    FLG    = 1
                    BREAK
                END
            END
        END
    REPEAT
    RETURN
*--------------------------------------------------------------
FLG.RET:
*-------
    IF FLG EQ 0 THEN
        DR.ACC = ACCT.NUM
        CALL F.READ(FN.ACC,ACCT.NUM,R.ACCT, F.ACC, E.ACC1)
        DR.CUR = R.ACCT<AC.CURRENCY>
    END
    RETURN
*----------------------------------------------------
BUILD.RECORD:
*------------
    COMMA  = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE="    :TODAY:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :AMOUNT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE="   :TODAY:COMMA
    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA
    OFS.MESSAGE.DATA :=  "NOTE.DEBITED="        :ID.NEW:COMMA
    OFS.MESSAGE.DATA :=  "CO.CODE="             :CUST.COMP:COMMA
    OFS.MESSAGE.DATA :=  "VERSION.NAME="        :",FT.LOCKS.MANUAL"

    OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,NEW.FILE ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160628 - S
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E

    RETURN
*-------------------------------------------------------
END
