* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.STATEMENT.REQUESTS.RUN
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STATEMENT.REQUESTS

    GOSUB INITIALISE
    GOSUB PROCESS

    RETURN
*
**
*
INITIALISE:
*---------

    FN.SCB.STMT.REQUEST.LIST = "F.SCB.STMT.REQUEST.LIST"
    FV.SCB.STMT.REQUEST.LIST = ""
    CALL OPF(FN.SCB.STMT.REQUEST.LIST,FV.SCB.STMT.REQUEST.LIST)

    FN.ACCOUNT = "F.ACCOUNT"
    FV.ACCOUNT = ""
    CALL OPF(FN.ACCOUNT,FV.ACCOUNT)

    RETURN
*
**
*
PROCESS:
*------

    REQUEST.IDS = ""
*Line [ 55 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.OF.ACCS = DCOUNT(R.NEW(SCB.STMT.REQ.ACCOUNT.ID),@VM)
    FOR ACC.IND = 1 TO NO.OF.ACCS
        ACC.ID = R.NEW(SCB.STMT.REQ.ACCOUNT.ID)<1,ACC.IND>
        CALL F.READ(FN.ACCOUNT,ACC.ID,R.ACCOUNT,FV.ACCOUNT,"")
*Line [ 60 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        NO.OF.STMTS = DCOUNT(R.NEW(SCB.STMT.REQ.STMT.NO)<1,ACC.IND>,@SM)
        FOR STMT.IND = 1 TO NO.OF.STMTS
            IF R.NEW(SCB.STMT.REQ.PRINT.STMT)<1,ACC.IND,STMT.IND> = "YES" THEN
                IF STMT.IND = 1 THEN
                    START.DATE = R.ACCOUNT<AC.OPENING.DATE>
                END ELSE
                    START.DATE = R.NEW(SCB.STMT.REQ.STMT.DATE)<1,ACC.IND,STMT.IND-1>
                    CALL CDT('',START.DATE,'+01C')
                    IF START.DATE < R.ACCOUNT<AC.OPENING.DATE> THEN
                        START.DATE = R.ACCOUNT<AC.OPENING.DATE>
                    END
                END
                END.DATE = R.NEW(SCB.STMT.REQ.STMT.DATE)<1,ACC.IND,STMT.IND>
                REQUEST.IDS<-1> = ID.NEW:"|":ACC.ID:"|":START.DATE:"|":END.DATE
            END
        NEXT STMT.IND
        IF R.NEW(SCB.STMT.REQ.FROM.DATE)<1,ACC.IND> THEN
            REQUEST.IDS<-1> = ID.NEW:"|":ACC.ID:"|":R.NEW(SCB.STMT.REQ.FROM.DATE)<1,ACC.IND>:"|":R.NEW(SCB.STMT.REQ.TO.DATE)<1,ACC.IND>
        END
    NEXT ACC.IND

    IF R.NEW(SCB.STMT.REQ.PRINT.TIME) = "OFFLINE" THEN
        IF REQUEST.IDS THEN
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
***            WRITE REQUEST.IDS TO FV.SCB.STMT.REQUEST.LIST,ID.NEW ON ERROR NULL
            CALL F.WRITE(FN.SCB.STMT.REQUEST.LIST,ID.NEW,REQUEST.IDS)
            CALL JOURNAL.UPDATE(ID.NEW)
**********
        END
    END ELSE
*Line [ 91 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.SCB.PRINT.STATEMENTS.LOAD
*Line [ 93 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        NO.OF.REQS = DCOUNT(REQUEST.IDS,@FM)
        FOR REQ.IND = 1 TO NO.OF.REQS
            REQ.TEXT = REQUEST.IDS<REQ.IND>
            CALL SCB.PRINT.STATEMENTS(REQ.TEXT)
        NEXT REQ.IND
    END

    RETURN
END
