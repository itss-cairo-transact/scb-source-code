* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>345</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* Create By Nahrawy
* Edit By Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE SCR.HISTORY.CHARGE.DATE

*PROGRAM SCR.HISTORY.CHARGE.DATE
    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.USER
    $INSERT T24.BP  I_F.USER.SIGN.ON.NAME
    $INSERT T24.BP  I_F.DRAWINGS
    $INSERT T24.BP  I_F.ACCOUNT
    $INSERT T24.BP  I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP  I_F.CUSTOMER
    $INSERT T24.BP  I_F.CURRENCY
    $INSERT T24.BP  I_F.CATEGORY
    $INSERT T24.BP  I_F.FT.CHARGE.TYPE
    $INSERT T24.BP  I_F.FT.COMMISSION.TYPE
    $INSERT T24.BP  I_F.LC.ACCOUNT.BALANCES
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "��� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID = 'SCR.HISTORY.CHARGE.DATE'
    CALL PRINTER.ON(REPORT.ID,'')

    OUT.AMT       = 0
    CHARGE.CURR   = ''
    CHARGE.AMOUNT = 0
    RETURN
*========================================================================
PROCESS:
*-------
    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    QQ = COMI
    CALL F.READ(FN.ACC,QQ,R.ACC,F.ACC,E1)

    YTEXT = "Enter the DATE No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    YY = COMI

    TOT = 0
    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '����� �������� ���������':' - ':'SIGHT PAYMENT OF INWORD COLLECTIONS(CHARGE)'
*------------------------------------------------------------------------
    DB.ACC = ''; ACC.NO = ''
*************************MODIFIED ON 18-12-2014 ABEER
    LOCATE YY IN R.ACC<LCAC.CHRG.DATE.DUE,1> SETTING III THEN
        DB.ACC    = R.ACC<LCAC.SETTLE.AC.FROM,III>
        ACC.NO = DB.ACC
    END
********************
    ACC.NO = DB.ACC
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,DB.ACC,CUS.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,DB.ACC,CUR.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,DB.ACC,CAT.ID)
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
    ACC.BR2 = ACC.BR1[8,2]
    ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
    CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME2    = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
    CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>

*Line [ 104 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.CHARGE = DCOUNT(R.ACC<LCAC.CHRG.CODE>,@VM)
    DAT            = R.ACC<LCAC.CHRG.DATE.DUE>
    MAT.DATE       = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    INPUTT    = R.ACC<LCAC.INPUTTER>
    INP       = FIELD(INPUTT,'_',2)
    AUTH.SON  = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH.SON,AUTH)
    AUTHI     =  AUTH
*------------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:
    PR.HD :="'L'":NN.TITLE
    PR.HD :="'L'":"SCR.HISTORY.CHARGE.DATE"
    PRINT
    HEADING PR.HD
    PRINT " "
*------------------------------------------------------------------
    XX = SPACE(132)

    XX<1,1>[3,35]  = "��� ������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME2
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = "�������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS2
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS3
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = ACC.NO
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = CATEG
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '����� ������� : '
    YY.NEW = YY[7,2]:'/':YY[5,2]:'/':YY[1,4]
    XX<1,1>[50,35] = YY.NEW
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,15]  = '������         : '
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "
*-------------------------------------------------------------------
    WS.INDX = 0
    T.SEL   = "SELECT ":FN.ACC: " WITH @ID EQ ": QQ
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CUR = ""
    TOT = 0
    IF SELECTED THEN
        CALL F.READ(FN.ACC,KEY.LIST,R.ACC,F.ACC,E1)
*Line [ 191 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAT22.DECOUNT = DCOUNT(R.ACC<LCAC.CHRG.DATE.DUE>,@VM)
        FOR W = 1 TO DAT22.DECOUNT
            DAT22 = R.ACC<LCAC.CHRG.DATE.DUE><1,W>
            IF DAT22 EQ YY THEN
                CHARGCODE      = R.ACC<LCAC.CHRG.CODE>
                CHARGE.CODE    = R.ACC<LCAC.CHRG.CODE><1,W>
                CHARGE.AMOUNT  = R.ACC<LCAC.AMT.REC><1,W>
                CHARGE.CURR    = R.ACC<LCAC.CHRG.CCY><1,W>
                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)

                IF ETEXT THEN
** TEXT = "ETEXT = " : ETEXT ; CALL REM
                    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
                END

                IN.AMOUNT = R.ACC<LCAC.AMT.REC><1,W>
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
                OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                TOT     += CHARGE.AMOUNT

                XX<1,1>[3,35]   = '�������  :'
                XX<1,1>[50,30]  = CHARGE.NAME
                PRINT XX<1,1>
                XX<1,1> = ""

                XX<1,1>[3,35]   = '������     : '
                XX<1,1>[50,30]  = CHARGE.AMOUNT
                PRINT XX<1,1>
                XX<1,1> = ""

                CALL WORDS.ARABIC.DEAL(CHARGE.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                XX<1,1>[3,35]    = '������ ������� : '
                XX<1,1>[50,30]   = OUT.AMT
                PRINT XX<1,1>
                XX<1,1> = ""
                PRINT " "

                WS.INDX ++
            END
        NEXT W

        FOR II = WS.INDX TO 4
            XX<1,1>[3,35]   = '�������  :'
            XX<1,1>[50,30]  = " "
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]   = '������     : '
            XX<1,1>[50,30]  = " "
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]    = '������ ������� : '
            XX<1,1>[50,30]   = " "
            PRINT XX<1,1>
            XX<1,1> = ""
            PRINT " "
        NEXT II

        XX<1,1>[3,35]    = '������     : '
        XX<1,1>[50,35]   = CUR
        PRINT XX<1,1>
        XX<1,1> = ""
        PRINT " "

        XX<1,1>[3,35]    = '������ ��������'
        XX<1,1>[50,30]   = TOT
        PRINT XX<1,1>
        XX<1,1> = ""

        CALL WORDS.ARABIC.DEAL(TOT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        XX<1,1>[3,35]    = '������ ������� : '
        XX<1,1>[50,30]   = OUT.AMT
        PRINT XX<1,1>
        XX<1,1> = ""
        PRINT " "

        XX<1,1>[3,35]   = '������'
        XX<1,1>[50,35]  = INP
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]  = '��� �������'
        XX<1,1>[50,35] = QQ
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]  = '������'
        XX<1,1>[50,35] = AUTHI
        PRINT XX<1,1>
        XX<1,1> = ""
    END
*===============================================================
    RETURN
END
