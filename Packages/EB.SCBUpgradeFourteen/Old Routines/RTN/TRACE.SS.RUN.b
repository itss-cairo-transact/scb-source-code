* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>857</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE TRACE.SS.RUN( NEST.LEVEL, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, R.ITEM)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDARD.SELECTION

** ----- ----- -----
MAIN:
** ----- ----- -----

      NO.HEADER = 1
      IF NOT( COUNT( ITEM.NAME, '!')) THEN

         USR.FIELD.NAME = R.ITEM< SSL.USR.FIELD.NAME>
         USR.FIELD.NAME.NO = DCOUNT( USR.FIELD.NAME, @VM)
         FOR USR.FIELD.NAME.IDX = 1 TO USR.FIELD.NAME.NO

            ITEM.NAME = FIELD( ITEM.NAME, '!', 1) : '!' : USR.FIELD.NAME< 1, USR.FIELD.NAME.IDX> : '!' : USR.FIELD.NAME.IDX
            GOSUB TRACE.SS.FIELD

         NEXT USR.FIELD.NAME.IDX

      END ELSE GOSUB TRACE.SS.FIELD

      RETURN

** ----- ----- -----
TRACE.SS.FIELD:
** ----- ----- -----

      RECORD.NAME = FIELD( ITEM.NAME, '!', 1)
      FIELD.NAME = FIELD( ITEM.NAME, '!', 2)
      USR.FIELD.IDX = FIELD( ITEM.NAME, '!', 3)

      IF FIELD.NAME THEN

         IF NOT( USR.FIELD.IDX) THEN

            USR.FIELD.NAME = ''
            CALL DBR( 'STANDARD.SELECTION':@FM:SSL.USR.FIELD.NAME, RECORD.NAME, USR.FIELD.NAME)
            IF ETEXT THEN

               ETEXT = '' ; E = 'ERROR READING STANDARD.SELECTION (&)' : @FM : RECORD.NAME ; CALL TXT( E)
               CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

            END
               ELSE LOCATE FIELD.NAME IN USR.FIELD.NAME< 1, 1> SETTING USR.FIELD.IDX ELSE USR.FIELD.IDX = ''

         END

         IF USR.FIELD.IDX THEN

            USR.TYPE = R.ITEM< SSL.USR.TYPE, USR.FIELD.IDX>
            USR.FIELD.NO = R.ITEM< SSL.USR.FIELD.NO, USR.FIELD.IDX>
            CONVERT @SM TO '' IN USR.FIELD.NO      ; * CONVERT SUBVALUES TO ONE STRING

            BEGIN CASE

                  ** ----- R-type routine -----

               CASE USR.TYPE = 'R'

                  ROUTINE.NAME = USR.FIELD.NO
                  IF ROUTINE.NAME THEN

                     GOSUB WRITE.HEADER
*Line [ 90 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                     CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, ROUTINE.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'R-TYPE FIELD ROUTINE')

                  END

                  ** ----- ----- -----

               CASE USR.TYPE = 'I'

                  BEGIN CASE

                        ** ----- I-type routine -----

                     CASE FIELD( USR.FIELD.NO, '(', 1) = 'SUBR'

                        IF COUNT( USR.FIELD.NO, '"') THEN ROUTINE.NAME = FIELD( USR.FIELD.NO, '"', 2) ELSE ROUTINE.NAME = FIELD( USR.FIELD.NO, "'", 2)
                        IF ROUTINE.NAME THEN

                           GOSUB WRITE.HEADER
*Line [ 109 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                           CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, ROUTINE.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'I-TYPE FIELD ROUTINE')

                        END

                        ** ----- local field -----

                     CASE FIELD( USR.FIELD.NO, '<', 1) = 'LOCAL.REF'

                        LRT.FIELD.NAME = FIELD( FIELD( USR.FIELD.NO, ",", 2), '>', 1)
                        IF LRT.FIELD.NAME THEN

                           GOSUB WRITE.HEADER
*Line [ 122 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                           CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'LOCAL.REF.TABLE', NEST.LEVEL + 1, RECORD.NAME:'!':LRT.FIELD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'LOCAL FIELD')

                        END

                        ** ----- ----- -----

                  END CASE

            END CASE

         END

      END

      RETURN

** ----- ----- -----
WRITE.HEADER:
** ----- ----- -----

      IF NO.HEADER THEN NO.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Fields extensions ::')

      RETURN

** ----- ----- -----

END
