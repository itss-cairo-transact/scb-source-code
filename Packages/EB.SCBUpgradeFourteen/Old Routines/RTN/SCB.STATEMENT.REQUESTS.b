* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1216</Rating>
*-----------------------------------------------------------------------------
* Version 8 02/06/00  GLOBUS Release No. G15.0.01 31/08/04

    SUBROUTINE SCB.STATEMENT.REQUESTS
******************************************************************
* Enter Program Description Here
*-----------------------------------------------------------------------------
* Modification History:
*
* 27/08/06 - New Program
*
*-----------------------------------------------------------------------------
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.STATUS.FLAG
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STATEMENT.REQUESTS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_SCB.PRINT.STATEMENTS.COMMON

*************************************************************************

* Call $RUN Routine and Exit if running as Phantom *

    IF PHNO THEN
REM > CALL TEMPLATE.W$RUN
        GOTO V$EXIT
    END

*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL MESSAGE = 'RET' DO

        V$ERROR = ''

        IF MESSAGE = 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END
            IF V$FUNCTION EQ 'V' THEN
                FILE.TYPE = "I"
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE = 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

REM > GOSUB CHECK.RECORD                ;* Special Editing of Record
REM > IF V$ERROR THEN GOTO MAIN.REPEAT

REM > GOSUB PROCESS.DISPLAY             ;* For Display applications

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE MESSAGE = 'ERROR' DO REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP

        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    UNTIL MESSAGE <> "" DO

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'VAL' OR MESSAGE EQ 'VER' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'R'
            END.ERROR = ""
REM > GOSUB CHECK.REVERSAL              ;* Special Reversal checks
        CASE V$FUNCTION EQ 'V'
REM > GOSUB CHECK.VERIFY                ;* Special Verify checks
            GOSUB CHECK.CHANGES         ;* look for changed field
            IF YWRITE EQ 0 AND NOT(END.ERROR) THEN
                GOSUB CALL.$RUN         ;* update using $RUN
                GOTO PROCESS.MESSAGE.EXIT         ;* and return to Id input
            END
        CASE OTHERWISE
REM > GOSUB AUTH.CROSS.VALIDATION       ;* Special Cross Validation
        END CASE

        IF END.ERROR THEN
            GOSUB POINT.TO.ERROR        ;* position on error field
            GOTO PROCESS.MESSAGE.EXIT   ;* return to field input
        END

REM > IF NOT(V$ERROR) THEN
REM > GOSUB BEFORE.AUTH.WRITE           ;* Special Processing before write
REM > END

        IF NOT(V$ERROR) THEN
            CALL AUTH.RECORD.WRITE

            IF MESSAGE <> "ERROR" THEN
REM > GOSUB AFTER.AUTH.WRITE            ;* Special Processing after write

                IF V$FUNCTION EQ 'V' THEN
                    GOSUB CALL.$RUN
                END

            END

        END

    END

PROCESS.MESSAGE.EXIT:

    RETURN

*************************************************************************

PROCESS.DISPLAY:

* Display the record fields.

    IF SCREEN.MODE EQ 'MULTI' THEN
        CALL FIELD.MULTI.DISPLAY
    END ELSE
        CALL FIELD.DISPLAY
    END

    RETURN

*************************************************************************

CHECK.CHANGES:

    YWRITE = 0
    IF END.ERROR EQ '' THEN
        A = 1
        LOOP UNTIL A GT V OR YWRITE DO
            IF R.NEW(A) NE R.OLD(A) THEN YWRITE = 1 ELSE A += 1
        REPEAT
    END

    RETURN

*************************************************************************

POINT.TO.ERROR:

    IF END.ERROR EQ 'Y' THEN
        P = 0 ; A = 1
        LOOP UNTIL T.ETEXT<A> NE '' DO
            A += 1
        REPEAT
        T.SEQU = A
        IF SCREEN.MODE EQ 'SINGLE' THEN
            IF INPUT.BUFFER[1,LEN(C.F)] EQ C.F THEN
                INPUT.BUFFER = INPUT.BUFFER[LEN(C.F) + 2,99]
* cancel C.F function after C.W usage
* (+2 for space separator)
            END
        END ELSE
            E = T.ETEXT<A>
*Line [ 253 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.ERR
        END
    END ELSE
        T.SEQU = 'ACTION' ; E = END.ERROR ; L = 22
*Line [ 258 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END

    CALL DISPLAY.MESSAGE("", "1")       ;* Clear the VALIDATED message
    VAL.TEXT = ""
    MESSAGE = 'ERROR'

    RETURN

*************************************************************************

    CALL.$RUN:

* Process the 'Work' file using the $Run Routine *

    V$FUNCTION = FUNCTION.SAVE

*Line [ 276 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeFourteen.SCB.STATEMENT.REQUESTS.RUN

    IF V$FUNCTION EQ 'B' THEN
        V$FUNCTION = 'V'
    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.


    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


    RETURN

*************************************************************************

CHECK.FIELDS:

    BEGIN CASE

    CASE AF EQ SCB.STMT.REQ.ACCOUNT.ID
        CALL DUP
        IF NOT(E) THEN
            GOSUB CLEAR.OTHER.FIELDS
            CALL F.READ(FN.ACCT.STMT.PRINT,COMI,R.ACCT.STMT.PRINT,FV.ACCT.STMT.PRINT,READ.ERR)
            IF NOT(READ.ERR) THEN
*Line [ 317 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                NO.OF.STMTS = DCOUNT(R.ACCT.STMT.PRINT,@FM)
                FOR STMT.IND = 1 TO NO.OF.STMTS
                    STMT.DATE = FIELD(R.ACCT.STMT.PRINT<STMT.IND>,"/",1,1)
                    OPEN.BAL = FIELD(R.ACCT.STMT.PRINT<STMT.IND>,"/",2,1)
                    IF STMT.IND = 1 THEN
                        R.NEW(SCB.STMT.REQ.STMT.NO)<1,AV> = STMT.IND
                        R.NEW(SCB.STMT.REQ.STMT.DATE)<1,AV> = STMT.DATE
                        R.NEW(SCB.STMT.REQ.OPEN.BAL)<1,AV> = OPEN.BAL
                        R.NEW(SCB.STMT.REQ.PRINT.STMT)<1,AV> = "NO"
                    END ELSE
                        R.NEW(SCB.STMT.REQ.STMT.NO)<1,AV,-1> = STMT.IND
                        R.NEW(SCB.STMT.REQ.STMT.DATE)<1,AV,-1> = STMT.DATE
                        R.NEW(SCB.STMT.REQ.OPEN.BAL)<1,AV,-1> = OPEN.BAL
                        R.NEW(SCB.STMT.REQ.PRINT.STMT)<1,AV,-1> = "NO"
                    END
                NEXT STMT.IND
            END
        END
        CALL REBUILD.SCREEN

    CASE AF EQ SCB.STMT.REQ.FROM.DATE
        IF R.NEW(SCB.STMT.REQ.TO.DATE)<1,AV> AND COMI THEN
            IF COMI > R.NEW(SCB.STMT.REQ.TO.DATE)<1,AV> THEN
                E = "FROM DATE MUST BE LESS THAN TO DATE"
            END
        END

    CASE AF EQ SCB.STMT.REQ.TO.DATE
        IF R.NEW(SCB.STMT.REQ.FROM.DATE)<1,AV> THEN
            IF COMI THEN
                IF COMI < R.NEW(SCB.STMT.REQ.TO.DATE)<1,AV> THEN
                    E = "FROM DATE MUST BE LESS THAN TO DATE"
                END
            END
        END ELSE
            IF COMI THEN
                E = "ENTER FROM DATE BEFORE TO DATE"
            END
        END

    END CASE

    IF E THEN
        T.SEQU = "IFLD"
*Line [ 362 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END

    RETURN

*************************************************************************

CLEAR.OTHER.FIELDS:

    R.NEW(SCB.STMT.REQ.STMT.NO)<1,AV> = ""
    R.NEW(SCB.STMT.REQ.STMT.DATE)<1,AV> = ""
    R.NEW(SCB.STMT.REQ.OPEN.BAL)<1,AV> = ""
    R.NEW(SCB.STMT.REQ.PRINT.STMT)<1,AV> = ""

    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************

CHECK.VERIFY:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:


    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    FUNCTION.SAVE = V$FUNCTION
    IF V$FUNCTION EQ "B" THEN
        V$FUNCTION = "V"
        ID.ALL = ""
    END
    IF INDEX('ADEFQ',V$FUNCTION,1) THEN
        E ='EB.RTN.FUNT.NOT.ALLOWED.APP.17'
*Line [ 427 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    FN.ACCT.STMT.PRINT = "F.ACCT.STMT.PRINT"
    FV.ACCT.STMT.PRINT = ""
    CALL OPF(FN.ACCT.STMT.PRINT,FV.ACCT.STMT.PRINT)

    FN.SCB.STMT.REQUEST.LIST = "F.SCB.STMT.REQUEST.LIST"
    FV.SCB.STMT.REQUEST.LIST = ""
    CALL OPF(FN.SCB.STMT.REQUEST.LIST,FV.SCB.STMT.REQUEST.LIST)

    FN.ACCOUNT = "F.ACCOUNT"
    FV.ACCOUNT = ""
    CALL OPF(FN.ACCOUNT,FV.ACCOUNT)


    RETURN

*************************************************************************

DEFINE.PARAMETERS:

* SEE 'I_RULES' FOR DESCRIPTIONS *

    MAT F = "" ; MAT N = "" ; MAT T = "" ; ID.T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = ""

    ID.F = "REQUEST.ID" ; ID.N = "35" ; ID.T = "A"

    Z = 0
    Z += 1; F(Z) = "XX<ACCOUNT.ID" ; N(Z) = "16..C" ; T(Z) = "ACC"
    Z += 1; F(Z) = "XX-XX<STMT.NO" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "XX-XX-STMT.DATE" ; N(Z) = "11" ; T(Z) = "D" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "XX-XX-OPEN.BAL" ; N(Z) = "18" ; T(Z) = "AMT" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "XX-XX>PRINT.STMT" ; N(Z) = "3" ; T(Z) = "" ; ; T(Z)<2> = "YES_NO"
    Z += 1; F(Z) = "XX-FROM.DATE" ; N(Z) = "8..C" ; T(Z) = ""
    Z += 1; F(Z) = "XX>TO.DATE" ; N(Z) = "8..C" ; T(Z) = ""
    Z += 1; F(Z) = "PRINT.TIME" ; N(Z) = "7.1" ; T(Z) = "" ; T(Z)<2> = "ONLINE_OFFLINE"
    Z += 1; F(Z) = "PRINT.TO.FILE" ; N(Z) = "3.1" ; T(Z) = "" ; ; T(Z)<2> = "YES_NO"
    Z += 1; F(Z) = "RESERVED9" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED8" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED7" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED6" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED5" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED4" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED3" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED2" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"
    Z += 1; F(Z) = "RESERVED1" ; N(Z) = "9" ; T(Z) = "" ; T(Z)<3> = "NOINPUT"

    V = Z + 9 ; PREFIX = "SCB.STMT.REQ."

    RETURN

*************************************************************************

END
