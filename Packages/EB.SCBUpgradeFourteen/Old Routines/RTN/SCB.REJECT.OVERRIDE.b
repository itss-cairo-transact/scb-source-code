* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCB.REJECT.OVERRIDE

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER

    OVE.ARRAY = R.NEW(FT.OVERRIDE)
*Line [ 23 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    Y.CNT = DCOUNT(OVE.ARRAY,@VM)
    Y.ETEXT.ARR = ''
    IF OVE.ARRAY THEN
        FOR I = 1 TO Y.CNT
*TEXT = "OVER.1 = ":FIELD(OVE.ARRAY<1,I>,'}',1) ; CALL REM
            Y.ETEXT.ARR<1,-1> = FIELD(OVE.ARRAY<1,I>,'}',1)
        NEXT I

    END
*Line [ 33 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    NOT.ERROR='ACCT.BAL.LT.LOCKED':@FM: 'ACCT.UNAUTH.OD' :@FM: 'LT.MINIMUM.BAL' :@FM: 'WITHDRAWL.LT.MIN.BAL' :@FM: 'AVAILABLE.FUNDS.EXCESS' :@FM: 'AVAILABLE.BALANCE.OVERDRAFT' :@FM: 'LIMIT.EXPS.BEF.TXN'
*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    Y.OVER.CNT = DCOUNT(Y.ETEXT.ARR,@VM)
    FOR J=1 TO Y.OVER.CNT
        LOCATE Y.ETEXT.ARR<1,J> IN NOT.ERROR<1> SETTING POS THEN

            ETEXT = Y.ETEXT.ARR<1,J>
            CALL STORE.END.ERROR
        END

    NEXT J
    RETURN
