* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1449</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE TRACE.ENQUIRY.RUN( NEST.LEVEL, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, R.ITEM)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ENQUIRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDARD.SELECTION

** ----- ----- -----
MAIN:
** ----- ----- -----

      ** ----- NOFILE enquiry -----

      FILE.NAME = R.ITEM< ENQ.FILE.NAME>
      SS.FILE.NAME = FIELD( FILE.NAME, '$', 1)

      NOFILE.ENQUIRY = ( FIELD( FILE.NAME, '.', 1) = 'NOFILE' )
      IF NOFILE.ENQUIRY THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: NOFILE enquiry ::')
*Line [ 45 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
         CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'STANDARD.SELECTION', NEST.LEVEL + 1, SS.FILE.NAME:'!', REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'NOFILE ENQUIRY')

      END

      ** ----- user fields -----

      NO.USER.FIELD.NAMES = 1
      NO.USER.FIELD.HEADER = 1

      FIELD.COMMENT = 'FIXED SELECTION'
      FIXED.SELECTION = R.ITEM< ENQ.FIXED.SELECTION>
      FIXED.SELECTION.NO = DCOUNT( FIXED.SELECTION, @VM)
      FOR FIXED.SELECTION.IDX = 1 TO FIXED.SELECTION.NO

         FIELD.NAME = FIELD( FIXED.SELECTION< 1, FIXED.SELECTION.IDX>, ' ', 1) ; GOSUB PROCESS.FIELD

      NEXT FIXED.SELECTION.IDX

      FIELD.COMMENT = 'FIXED SORT'
      FIXED.SORT = R.ITEM< ENQ.FIXED.SORT>
      FIXED.SORT.NO = DCOUNT( FIXED.SORT, @VM)
      FOR FIXED.SORT.IDX = 1 TO FIXED.SORT.NO

         FIELD.NAME = FIELD( FIXED.SORT< 1, FIXED.SORT.IDX>, ' ', 1) ; GOSUB PROCESS.FIELD

      NEXT FIXED.SORT.IDX

      FIELD.COMMENT = 'SELECTION FIELD'
      SELECTION.FLDS = R.ITEM< ENQ.SELECTION.FLDS>
      SELECTION.FLDS.NO = DCOUNT( SELECTION.FLDS, @VM)
      FOR SELECTION.FLDS.IDX = 1 TO SELECTION.FLDS.NO

         FIELD.NAME = SELECTION.FLDS< 1, SELECTION.FLDS.IDX> ; GOSUB PROCESS.FIELD

      NEXT SELECTION.FLDS.IDX

      FIELD.COMMENT = 'OPERATION FIELD'
      OPERATION = R.ITEM< ENQ.OPERATION>
      OPERATION.NO = DCOUNT( OPERATION, @VM)
      FOR OPERATION.IDX = 1 TO OPERATION.NO

         CURRENT.OPERATION = TRIM( OPERATION< 1, OPERATION.IDX>)
         IF CURRENT.OPERATION[ 1, 1] # '!' AND CURRENT.OPERATION[ 1, 1] # '"' THEN

            IF COUNT( CURRENT.OPERATION, ' ') THEN

               IF FIELD( CURRENT.OPERATION, ' ', 1) = 'IDESC' THEN FIELD.NAME = FIELD( CURRENT.OPERATION, ' ', 2) ; GOSUB PROCESS.FIELD

            END ELSE

               IF NOT( NUM( CURRENT.OPERATION)) THEN FIELD.NAME = CURRENT.OPERATION ; GOSUB PROCESS.FIELD

            END

         END

      NEXT OPERATION.IDX

      ** ----- build routines -----

      BUILD.ROUTINE = R.ITEM< ENQ.BUILD.ROUTINE>
      BUILD.ROUTINE.NO = DCOUNT( BUILD.ROUTINE, @VM)
      IF BUILD.ROUTINE.NO THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Build routines ::')
         FOR BUILD.ROUTINE.IDX = 1 TO BUILD.ROUTINE.NO

*Line [ 113 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, BUILD.ROUTINE< 1, BUILD.ROUTINE.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'BUILD ROUTINE')

         NEXT BUILD.ROUTINE.IDX

      END

      ** ----- context version/enquiry -----

      CONTEXT.ITEM = R.ITEM< ENQ.ENQUIRY.NAME>
      CONTEXT.ITEM.NO = DCOUNT( CONTEXT.ITEM, @VM)
      IF CONTEXT.ITEM.NO THEN

         NO.CONTEXT.ITEM.HEADER = 1
         FOR CONTEXT.ITEM.IDX = 1 TO CONTEXT.ITEM.NO

            CONTEXT.BASE = FIELD( CONTEXT.ITEM< 1, CONTEXT.ITEM.IDX>, ' ', 1)
            IF CONTEXT.BASE # 'QUIT' AND CONTEXT.BASE # 'VIEW' THEN

               CONTEXT.OPERAND = FIELD( CONTEXT.ITEM< 1, CONTEXT.ITEM.IDX>, ' ', 2)
               IF CONTEXT.OPERAND = 'SEE' OR CONTEXT.OPERAND = 'S' THEN

                  IF COUNT( CONTEXT.BASE, ',') THEN

                     IF NO.CONTEXT.ITEM.HEADER THEN NO.CONTEXT.ITEM.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Context version/enquiry ::')

                     ETEXT = ''
                     CALL DBR( 'VERSION':@FM:1, CONTEXT.BASE, MY.DUMMY)
                     IF ETEXT THEN

                        ETEXT = ''

                        E = 'UNKNOWN CONTEXT VERSION' ; CALL TXT( E)
                        CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

*Line [ 148 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                     END ELSE CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'VERSION', NEST.LEVEL + 1, CONTEXT.BASE, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'CONTEXT VERSION')

                  END

               END ELSE

                  IF CONTEXT.BASE # ITEM.NAME THEN

                     IF NO.CONTEXT.ITEM.HEADER THEN NO.CONTEXT.ITEM.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Context version/enquiry ::')

                     ETEXT = ''
                     CALL DBR( 'ENQUIRY':@FM:1, CONTEXT.BASE, MY.DUMMY)
                     IF ETEXT THEN

                        ETEXT = ''

                        E = 'UNKNOWN CONTEXT ENQUIRY' ; CALL TXT( E)
                        CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

*Line [ 168 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                     END ELSE CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ENQUIRY', NEST.LEVEL + 1, CONTEXT.BASE, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'CONTEXT ENQUIRY')

                  END

               END

            END

         NEXT CONTEXT.ITEM.IDX

      END

      ** ----- conversion routines -----

      CONV.ROUTINE = R.ITEM< ENQ.CONVERSION>
      CONVERT @SM TO @VM IN CONV.ROUTINE
      CONV.ROUTINE.NO = DCOUNT( CONV.ROUTINE, @VM)
      IF CONV.ROUTINE.NO THEN

         NO.CONV.HEADER = 1
         FOR CONV.ROUTINE.IDX = 1 TO CONV.ROUTINE.NO

            CONV.FIRST.CHARACTER = CONV.ROUTINE< 1, CONV.ROUTINE.IDX>[ 1, 1]
            IF CONV.FIRST.CHARACTER = '@' OR CONV.FIRST.CHARACTER = '$' THEN

               IF NO.CONV.HEADER THEN NO.CONV.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Conversion routines ::')
*Line [ 195 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
               CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, FIELD( CONV.ROUTINE< 1, CONV.ROUTINE.IDX>, ' ', 2), REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'CONVERSION ROUTINE')

            END

         NEXT CONV.ROUTINE.IDX

      END

      ** ----- ----- -----

      RETURN

** ----- ----- -----
PROCESS.FIELD:
** ----- ----- -----

      IF NO.USER.FIELD.NAMES THEN

         NO.USER.FIELD.NAMES = 0

         USR.FIELD.NAME = ''
         CALL DBR( 'STANDARD.SELECTION':@FM:SSL.USR.FIELD.NAME, SS.FILE.NAME, USR.FIELD.NAME)
         IF ETEXT THEN

            ETEXT = '' ; E = 'ERROR READING STANDARD.SELECTION (&)' : @FM : SS.FILE.NAME ; CALL TXT( E)
            CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

         END

      END

      IF USR.FIELD.NAME THEN

         LOCATE FIELD.NAME IN USR.FIELD.NAME< 1, 1> SETTING USR.FIELD.IDX THEN

            IF NO.USER.FIELD.HEADER THEN NO.USER.FIELD.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: User fields ::')
*Line [ 232 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'STANDARD.SELECTION', NEST.LEVEL + 1, SS.FILE.NAME:'!':FIELD.NAME:'!':USR.FIELD.IDX, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, FIELD.COMMENT)

         END

      END

      RETURN

** ----- ----- -----

END
