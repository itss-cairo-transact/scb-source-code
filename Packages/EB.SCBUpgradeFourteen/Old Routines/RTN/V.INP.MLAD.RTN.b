* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>550</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE V.INP.MLAD.RTN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM

    FN.ML  = 'F.SCB.MLAD.FILE.PARAM' ; F.ML = '' ; R.ML = ''
    CALL OPF( FN.ML,F.ML)
    IF V$FUNCTION EQ 'I' THEN
        MLAD.TYPE = R.NEW(MLPAR.MLAD.TYPE)
        LINE.NO   = R.NEW(MLPAR.MLAD.LINE.NO)
        GENLED.NO = R.NEW(MLPAR.GENLED.LINE.NO)
        CAT.FROM  = R.NEW(MLPAR.CATEGORY.FROM)
        CAT.TO    = R.NEW(MLPAR.CATEGORY.TO)
        MAT.DATE  = R.NEW(MLPAR.MATURITY.DATE)
        N.DAY     = R.NEW(MLPAR.NEXT.DAY)
        N.WEEK    = R.NEW(MLPAR.NEXT.WEEK)
        N.MONTH   = R.NEW(MLPAR.NEXT.MONTH)
        N.3MONTH  = R.NEW(MLPAR.NEXT.3MONTH)
        N.6MONTH  = R.NEW(MLPAR.NEXT.6MONTH)
        N.YEAR    = R.NEW(MLPAR.NEXT.YEAR)
        N.3YEAR   = R.NEW(MLPAR.NEXT.3YEAR)
        N.MORE    = R.NEW(MLPAR.NEXT.MORE)

*------------------
        MLAD.TYPE = FIELD(ID.NEW,'-',1,1)
        LINE.NO   = FIELD(ID.NEW,'-',2,1)

        R.NEW(MLPAR.MLAD.TYPE)      = MLAD.TYPE
        R.NEW(MLPAR.MLAD.LINE.NO)   = LINE.NO

        MLAD.TYPE = OCONV(MLAD.TYPE,"MCT")
        X = OCONV('X',"MCT")
        Y = OCONV('Y',"MCT")


        IF MLAD.TYPE EQ X THEN
            GOTO MLAD.TYPE.OK
        END

        IF MLAD.TYPE EQ Y THEN
            GOTO MLAD.TYPE.OK
        END


        AF=1
        ETEXT = "MUST BE ":X:" OR ":Y:" ONLY"
        CALL STORE.END.ERROR
*------------------
MLAD.TYPE.OK:


        IF GENLED.NO EQ '' AND CAT.FROM  EQ '' THEN
            AF = 5
            ETEXT = "YOU HAVE ENTER GENLED LINE NO "
            CALL STORE.END.ERROR

        END

*------------------

        T.SEL   = "SELECT F.SCB.MLAD.FILE.PARAM WITH MLAD.TYPE EQ ":MLAD.TYPE:" AND MLAD.LINE.NO EQ ":LINE.NO
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        IF SELECTED THEN
            SEL = SELECTED + 1
        END
        ELSE
            SEL = 1
        END

        SEL = FMT(SEL,"R%4")
        R.NEW(MLPAR.MLAD.SERIAL) = SEL

        RECORD.NO = MLAD.TYPE:"-":LINE.NO:"-":R.NEW(MLPAR.MLAD.SERIAL)

        R.NEW(MLPAR.MLAD.RECORD.NO) = RECORD.NO


        IF GENLED.NO NE '' THEN
            IF CAT.FROM NE '' THEN
                AF=6
                ETEXT = "MUST NOT ENTER CATEGORY FROM"
                CALL STORE.END.ERROR
            END
            IF CAT.TO NE '' THEN
                AF=7
                ETEXT = "MUST NOT ENTER CATEGORY TO"
                CALL STORE.END.ERROR
            END

        END
        ELSE
            IF CAT.FROM GT CAT.TO THEN
                AF = 7
                ETEXT = "CATEGORY TO MUST BE GREATER THAN CATEGORY FROM"
                CALL STORE.END.ERROR

            END
        END

        TOTAL.PERC = N.DAY + N.WEEK + N.MONTH + N.3MONTH + N.6MONTH + N.YEAR + N.3YEAR + N.MORE
        IF MAT.DATE EQ 'N' THEN
            IF TOTAL.PERC GT 100 OR  TOTAL.PERC LT 100 THEN
                AF=9
                ETEXT = "����� ��������� ��� ����"
                CALL STORE.END.ERROR

            END
        END


    END

END
