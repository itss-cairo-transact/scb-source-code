* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>71</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.REPRINT.SLIP.CLEARING

*REPRINT Cheques & Bills Portfolio

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

*=============================================================================*
*Line [ 41 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        CALL TXTINP('Enter Transaction Reference', 8, 23, '12', 'ANY')
        IF COMI THEN
            TXN.ID = '' ; TXN.ID = COMI ; ER.MSG = '' ; ETEXT = ''
            BEGIN CASE
            CASE TXN.ID[1,2] = 'BS'
                GOSUB SAVE.PARAMETERS
                GOSUB PROCESS.TT.RECORD
                IF NOT(ER.MSG) THEN
                    GOSUB REPRINT.SLIP
                END
                GOSUB RESTORE.PARAMETERS
*                  CALL JOURNAL.UPDATE(TXN.ID)
            CASE OTHERWISE
                ER.MSG = 'Invalid Transaction Reference'
            END CASE

            IF ER.MSG THEN
                TEXT = ER.MSG
                CALL REM
*CALL TXTINP(ER.MSG, 8, 23, '1.1', '')
            END
        END
    END

    GOTO PROGRAM.END

*=============================================================================*
SAVE.PARAMETERS:
    SAVE.FUNCTION = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS = '' ; SIZE = ''
    TXN.NO = '' ; PROCESS = ''
    NBG.CODE = '' ; RECEIPT = ''
    RETURN
*=============================================================================*
RESTORE.PARAMETERS:
    MAT R.NEW = MAT R.SAVE
    V$FUNCTION = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW = SAVE.ID.NEW
    RETURN
*===================== PROCESS SCB.BR.SLIPS RECORD =================================*
PROCESS.TT.RECORD:
*      IF TXN.ID[3,5] = R.DATES(EB.DAT.JULIAN.DATE)[5] THEN
    IF TXN.ID # '' THEN
        VF.SCB.BR.SLIPS = '' ; CALL OPF('FBNK.SCB.BR.SLIPS', VF.SCB.BR.SLIPS)
        ETEXT = ''
        DIM APP.REC(SCB.BS.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(SCB.BS.AUDIT.DATE.TIME)  ; MAT R.SAVE = ''
*         MAT R.NEW = MAT R.SAVE
        MAT R.SAVE = MAT R.NEW
        SIZE = SCB.BS.AUDIT.DATE.TIME

        CALL F.MATREAD('FBNK.SCB.BR.SLIPS',TXN.ID,MAT APP.REC,SIZE,VF.SCB.BR.SLIPS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            APPLICATION = 'SCB.BR.SLIPS' ; ID.NEW = COMI ; V$FUNCTION = 'I'
            IF R.NEW(SCB.BS.VERSION.NAME)[1,1] # ',' THEN
******                VER.NAME = "SCB.BR.SLIPS,":R.NEW(SCB.BS.VERSION.NAME)
                VER.NAME = "SCB.BR.SLIPS,REGISTER.1"
            END
            ELSE
      *****          VER.NAME = "SCB.BR.SLIPS":R.NEW(SCB.BS.VERSION.NAME)
                     VER.NAME ="SCB.BR.SLIPS,REGISTER.1"
            END
            GOSUB GET.DSF.NAMES.ARRAY
        END
    END
    ELSE
        VF.SCB.BR.SLIPS$HIS = '' ; CALL OPF('FBNK.SCB.BR.SLIPS$HIS', VF.SCB.BR.SLIPS$HIS)

*         TXN.ID := ';1'
        ETEXT = '' ; DIM APP.REC(SCB.BS.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(SCB.BS.AUDIT.DATE.TIME) ; MAT R.SAVE = ''
        SIZE = SCB.BS.AUDIT.DATE.TIME
        CALL F.MATREAD('FBNK.SCB.BR.SLIPS$HIS',TXN.ID,MAT APP.REC,SIZE,VF.SCB.BR.SLIPS$HIS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            IF R.NEW(SCB.BS.RECORD.STATUS) # 'REVE' THEN
                APPLICATION = 'SCB.BR.SLIPS' ; ID.NEW = COMI ; V$FUNCTION = 'I'
                PARM.ID = 'SCB.BR.SLIPS,'
                PARM.ID := R.NEW(SCB.BS.VERSION.NAME)
                UPDATE.REC = 'FBNK':R.COMPANY(EB.COM.MNEMONIC):'.SCB.BR.SLIPS$HIS'
                UPDATE.TS = 'YES'
            END
            ELSE
                ER.MSG = 'INVALID RECORD STATUS FOR REPRINT'
            END
        END
    END
    RETURN
*=============================================================================*
REPRINT.SLIP:
    IF DSF.CNT >= 1 THEN
        FOR DSF.LP = 1 TO DSF.CNT
            CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP>))
        NEXT DSF.LP
    END
    ELSE
        ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
    END
    RETURN
*=============================================================================*
GET.DSF.NAMES.ARRAY:
    IF VER.NAME THEN
        R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 161 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
       CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
       IF NOT(ETEXT) THEN
*         TEXT = "ARRAY = ":R.DSF ; CALL REM
            DSF.CNT = DCOUNT(R.DSF,@VM)
*         TEXT = "DSF COUNT = ":DSF.CNT  ; CALL REM
        END
        ELSE
            ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
        END
    END

*=================== SUBROUTINE END ... YES !!! ==============================*
PROGRAM.END:
    RETURN
END
