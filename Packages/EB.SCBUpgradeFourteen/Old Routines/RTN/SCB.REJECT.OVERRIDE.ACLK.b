* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCB.REJECT.OVERRIDE.ACLK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS

    OVE.ARRAY = R.NEW(AC.LCK.OVERRIDE)
*Line [ 26 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    Y.CNT = DCOUNT(OVE.ARRAY,@VM)
    Y.ETEXT.ARR = ''
    IF OVE.ARRAY THEN
        FOR I = 1 TO Y.CNT
            Y.ETEXT.ARR<1,-1> = 'OVERRIDE :':FIELD(OVE.ARRAY<1,I>,'}',1)
        NEXT I

    END
    IF Y.ETEXT.ARR THEN
        ETEXT = CHANGE(Y.ETEXT.ARR,VM,'*')
        CALL STORE.END.ERROR
    END

    RETURN
