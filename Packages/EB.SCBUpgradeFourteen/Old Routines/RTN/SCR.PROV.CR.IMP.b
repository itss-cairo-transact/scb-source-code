* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCR.PROV.CR.IMP
**    PROGRAM SCR.PROV.CR.IMP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*------------------------------------------------------------------------

    GOSUB INITIATE
    GOSUB GET.DATA
    IF MARG.FLG EQ 'Y' THEN
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� �������" ; CALL REM
    END
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCR.PROV.CR.IMP'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
GET.DATA:
*---------------------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG="";OUT.AMOUNT=''
    IF ID.NEW EQ '' THEN
        FN.LC = 'FBNK.LETTER.OF.CREDIT'
    END ELSE
        FN.LC = 'FBNK.LETTER.OF.CREDIT$NAU'
    END
    F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    FN.LC.NAU = 'FBNK.LETTER.OF.CREDIT$NAU'

    IF ID.NEW EQ '' THEN
        YTEXT = "Enter the TF No. : "
        CALL TXTINP(YTEXT, 8, 22, "14", "A")
        LC.ID =COMI[1,12]
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E1)
    END ELSE
*****************************************************************
        IF ID.NEW NE '' THEN
            LC.ID =ID.NEW
            CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E11)
            MARG.FLG         = R.LC<TF.LC.PROVISION>
        END
    END
*********************************************************************
    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = 'SCR.PROV.CR.IMP'
*------------------------------------------------------------------------

    REFERENCE = R.LC<TF.LC.OLD.LC.NUMBER>
    ACC.NO    = R.LC<TF.LC.CREDIT.PROVIS.ACC>
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)

    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
***CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
    BRN.ID  = AC.COMP[2]
    ACC.BR = TRIM(BRN.ID,"0","L")

    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
*** CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHARGE.CODE,COMM.AC.NO)
*** CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION<1,2>,CHARGE.CODE,COMM.AC.NAME)
    MARGIN.AMOUNT    = R.LC<TF.LC.PROVIS.AMOUNT>
    MARG.FLG         = R.LC<TF.LC.PROVISION>

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CUR1)

    IF  MARGIN.AMOUNT = '' THEN
        OUT.AMT = 0
    END
    IF  MARGIN.AMOUNT NE '' THEN
        IN.AMOUNT = MARGIN.AMOUNT
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR1 : ' ' : '�����'
    END
    IF ID.NEW NE '' THEN
        INPUTT = R.NEW(TF.LC.INPUTTER)
        INP   = FIELD(INPUTT,'_',2)
        AUTHI=OPERATOR
    END ELSE
        INPUTT = R.LC<TF.LC.INPUTTER>
        INP   = FIELD(INPUTT,'_',2)
        AUTH = R.LC<TF.LC.AUTHORISER >
        AUTHI   = FIELD(AUTH,'_',2)

    END

    RETURN
*------------------------------------------------------------------------
PROCESS:
    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

***    XX1<1,1>[3,35]   = COMM.AC.NAME
    XX1<1,1>[3,35]   = CUST.NAME
    XX2<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = MARGIN.AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[50,15] = '��� ������ : '
    XX2<1,1>[63,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR1

* XX4<1,1>[45,15] = '���� ������� : '
* XX4<1,1>[59,15] = MARGIN.PERC
    XX4<1,1>[45,15] = '������ :'
    XX4<1,1>[59,15] = REFERENCE

    XX6<1,1>[1,15]  = '��� �������'
    XX6<1,1>[30,15]   = '������'

    XX7<1,1>[1,15]  = LC.ID
    XX7<1,1>[30,15] = INP


    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������'
    XX9<1,1>[20,15] = AUTHI

*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(45) : "����� ������� ����� "
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX<1,1>
* PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN

END
