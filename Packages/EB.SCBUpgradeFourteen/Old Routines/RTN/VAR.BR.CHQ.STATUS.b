* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE VAR.BR.CHQ.STATUS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.STATUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS

    DCOUNTBR.ID = ""
    NUMBERBR.ID = ""
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
    FOR NUMBERBR.ID = 1 TO DCOUNTBR.ID

        FN.BR  = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
        BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
        KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
        T.SEL  = "SELECT FBNK.BILL.REGISTER WITH @ID EQ ": BR.ID
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        CALL OPF( FN.BR,F.BR)
        CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)


        IF SELECTED THEN
            IF R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID> THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> = 7
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.RETURN.REASON> = R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE> = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE> = TODAY
                CALL F.WRITE(FN.BR,KEY.LIST,R.BR)
            END ELSE
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> = 8
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>  = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE> = TODAY
                CALL F.WRITE(FN.BR,KEY.LIST,R.BR)
            END
        END
    NEXT NUMBERBR.ID




    RETURN
END
