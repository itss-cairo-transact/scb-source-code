* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE VAR.BILL.COLL.CLER.NEW.1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    FN.BR ='FBNK.BILL.REGISTER' ;R.BR ='';F.BR =''
    CALL OPF(FN.BR,F.BR)
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='INAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        ZFLAG = 0
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                GOSUB COLLECT
                GOSUB DR.CHRG
            END

            IF (R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                ZFLAG = ZFLAG + 1
            END

        NEXT NUMBERBR.ID

        IF ZFLAG EQ 0 THEN
            GOSUB KHASM.3ALA.MARKAZE
        END

    END
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='RNAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        KFLAG = 0
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN

                GOSUB COLLECT.REV
                GOSUB DR.CHRG.REV
            END
            IF (R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                KFLAG = KFLAG + 1
            END

        NEXT NUMBERBR.ID

        IF KFLAG EQ 0 THEN
            GOSUB KHASM.3ALA.MARKAZE.REV
            TEXT = "MARKAZE REVERCE IS OK " ; CALL REM
        END
    END
    RETURN
*---------
COLLECT:
*---------
    CURR =   R.BR<EB.BILL.REG.CURRENCY>
*********************************
* EDAFA LE EL 3AMIL KEMET EL BR *
*********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    Y.AMT  = R.BR<EB.BILL.REG.AMOUNT>
    W.OUR.REF = BR.ID
    T.CODE = '993'
    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    GOSUB AC.STMT.ENTRY
    TEXT = "BR VALUE WAS ADDED TO CUSTOMER ACCOUNT" ; CALL REM
    RETURN
**********************************
*----
KHASM.3ALA.MARKAZE:
*----
****************************************************
* KHASM MEN EL MARKAZE
****************************************************
    CHRG.AMT  = R.NEW(SCB.BT.TOT.CHRG.AMT)
    Y.ACCT    = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>
    Y.AMT     = ( R.NEW(SCB.BT.TOT.DEBIT.AMT) - CHRG.AMT ) * -1
    W.OUR.REF = ID.NEW
    T.CODE    = '993'
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    GOSUB AC.STMT.ENTRY
    TEXT = "BR VALUE - CHARGE WAS TAKEN FORM CENTRAL BANK" ; CALL REM
    RETURN
*****************************************************
*----
DR.CHRG:
*----
********************************
* KHASM MEN EL 3AMEL EL CHARGE *
********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    X.CHRG.AMT = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID>
    W.OUR.REF  = BR.ID
    T.CODE     = '995'
    IF X.CHRG.AMT NE '' AND X.CHRG.AMT NE 0 THEN
        Y.AMT  = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID> * -1
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
        GOSUB AC.STMT.ENTRY
        TEXT = "CHARGE WAS TAKEN FROM CUSTOMER" ; CALL REM
    END

    RETURN
*******************************  REVERCE  ********************************
*---------
COLLECT.REV:
*---------
    CURR =   R.BR<EB.BILL.REG.CURRENCY>
*----
* CR
*********************************
* EDAFA LE EL 3AMIL KEMET EL BR *
*********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    Y.AMT  = R.BR<EB.BILL.REG.AMOUNT> * -1
    W.OUR.REF = BR.ID
    T.CODE = '993'
    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    GOSUB AC.STMT.ENTRY
    RETURN
**********************************
*----
KHASM.3ALA.MARKAZE.REV:
*----
****************************************************
* KHASM MEN EL MARKAZE
****************************************************
    CHRG.AMT = R.NEW(SCB.BT.TOT.CHRG.AMT)
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>
    W.OUR.REF = ID.NEW
    T.CODE = '993'
    TEXT = "CHARGE IS " : CHRG.AMT ; CALL REM
    Y.AMT  = ( R.NEW(SCB.BT.TOT.DEBIT.AMT) - CHRG.AMT )
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    GOSUB AC.STMT.ENTRY
    RETURN
*****************************************************
*----
DR.CHRG.REV:
*----
********************************
* KHASM MEN EL 3AMEL EL CHARGE *
********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    X.CHRG.AMT = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID>
    W.OUR.REF = BR.ID
    T.CODE = '995'
    IF X.CHRG.AMT NE '' AND X.CHRG.AMT NE 0 THEN
        Y.AMT  = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID>
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
        GOSUB AC.STMT.ENTRY
    END

    RETURN
**************************************************************************
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = T.CODE
    ENTRY<AC.STE.THEIR.REFERENCE>  = R.NEW(SCB.BT.CBE.REF)
    ENTRY<AC.STE.TRANS.REFERENCE>  = W.OUR.REF
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = W.OUR.REF
    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
