* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.START.DAY(REQ.DATE)
************************************************************************
***********by Mahmoud Elhawary ** 5/10/2009 ****************************
************************************************************************
    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE
************************************************************************
* convert the date required to the start date for statement
************************************************************************

    TDR   = REQ.DATE
    TDRF  = TDR[1,6]:'01'
    CALL CDT("",TDRF,'-1W')
    TDR = TDRF
    TDRDD = TDR[7,2]
    TDRMN = TDR[5,2]
    TDRYY = TDR[1,4]
    M.DAT = TDRMN
    IF TDRMN EQ 3 THEN
        M.DAT = 1
    END
    IF TDRMN EQ 6 THEN
        M.DAT = 4
    END
    IF TDRMN EQ 9 THEN
        M.DAT = 7
    END
    IF TDRMN EQ 12 THEN
        M.DAT = 10
    END
    IF LEN(M.DAT) EQ 1 THEN
        M.DAT = '0':M.DAT
    END
    MMDAT = TDRYY:M.DAT:'01'

    TDD = MMDAT
    REQ.DATE = TDD
    RETURN 
