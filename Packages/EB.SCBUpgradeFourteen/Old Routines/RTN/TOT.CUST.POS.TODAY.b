* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*********NESSREEN AHMED 03/09/2008
*-----------------------------------------------------------------------------
* <Rating>134</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TOT.CUST.POS.TODAY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.POS.TODAY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    F.CUSTOMER = '' ; FN.CUSTOMER = 'F.CUSTOMER' ; R.CUSTOMER = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    F.ACCOUNT = '' ; FN.ACCOUNT = 'F.ACCOUNT' ; R.ACCOUNT = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    F.LD = '' ; FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; R.LD = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.LD,F.LD)

    F.LC = '' ; FN.LC = 'F.LETTER.OF.CREDIT' ; R.LC = '' ; E4 = '' ; RETRY4 = ''
    CALL OPF(FN.LC,F.LC)


    F.CUS.POS = '' ; FN.CUS.POS = 'F.SCB.CUST.POS.TODAY'  ; R.CUS.POS = '' ; E5 = '' ; RETRY5 = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)

    F.LIMIT = '' ; FN.LIMIT = 'F.LIMIT'  ; R.LIMIT = '' ; E6 = '' ; RETRY6 = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)

    F.COLL = '' ; FN.COLL = 'F.COLLATERAL'  ; R.COLL = '' ; E7 = '' ; RETRY7 = ''
    CALL OPF(FN.COLL,F.COLL)

    CUSTT.AC = '' ;CUST.BR.AC = '' ; KEY.AC = '' ; CATEG.AC = '' ; CURR.AC = '' ; COMP.CO.AC = ''
    LD.CUST = '' ; CUST.BR.LD = '' ; LD.CURR = '' ; LD.CATEG = '' ; KEY.LD = '' ; LD.COMP.CO = ''
    CD.CUST = '' ; CUST.BR.CD = '' ; CD.CURR = '' ; CD.CATEG = '' ; KEY.CD = '' ; CD.COMP.CO = ''
    LG.CUST = '' ; CUST.BR.LG = '' ; LG.CURR = '' ; LG.CATEG = '' ; KEY.LG = '' ; LG.COMP.CO = ''
    CUSTT.LC.IMP = '' ; CUST.BR.LC.IMP = '' ; CURR.LC.IMP = '' ; LC.CAT.IMP = '' ; KEY.LC.IMP = '' ; LC.COMP.CO.I = ''
    CUSTT.LC.EXP = '' ; CUST.BR.LC.EXP = '' ; CURR.LC.EXP = '' ; LC.CAT.EXP = '' ; KEY.LC.EXP = '' ; LC.CURR.EXP = '' ; LC.COMP.CO.E = ''
    CUSTT.LC.COLL = ''; CUST.BR.LC.COLL = '' ; CURR.LC.COLL = '' ; LC.CAT.COLL = '' ; KEY.LC.COLL = '' ; LC.CURR.COLL = '' ; LC.COMP.CO.C = ''
    CUSTT.LIMIT = ''  ; CUST.BR.LIMIT  = '' ; LIMIT.CURR = '' ; LIMIT.PRODUCT = '' ; KEY.LIMIT = '' ; LIMIT.COMP.CO = ''
    CUSTT.COLL = '' ; COLL.CURR = '' ; COLL.CODE = '' ; CUST.BR.COLL = '' ; KEY.COLL = '' ; COLL.COMP.CO = ''
    CUST.OLD.ID.AC = '' ; CUST.OLD.ID.LD = '' ;  CUST.OLD.ID.CD = '' ; CUST.OLD.ID.LG = '' ; CUST.OLD.ID.IM = ''
    CUST.OLD.ID.EX = '' ; CUST.OLD.ID.CO = '' ;  CUST.OLD.ID.LI = ''
    KEY.HIS.ID = ''
*****ADDED BY MAHMOUD 25/10/2009******
    CUST.CO.BOOK = ''
**************************************
*=========MOVE OLD DATA TO HISTORY FILE===================================
    F.CUS.POS.H = '' ; FN.CUS.POS.H = 'F.SCB.CUST.POS.TODAY$HIS'  ; R.CUS.POS.H = ''
    CALL OPF(FN.CUS.POS.H,F.CUS.POS.H)
    PDAT = TODAY
*    TEXT = 'PDAT=':PDAT ; CALL REM
    H.SEL = "SSELECT F.SCB.CUST.POS.TODAY WITH @ID UNLIKE ...":PDAT
    KEY.LIST.H ="" ; SELECTED.H="" ;  ER.MSG.H=""
    CALL EB.READLIST(H.SEL,KEY.LIST.H,"",SELECTED.H,ER.MSG.H)
*    TEXT = 'SELECTED=':SELECTED.H ; CALL REM
    IF SELECTED.H THEN
        FOR GG = 1 TO SELECTED.H
            CALL F.READ(FN.CUS.POS, KEY.LIST.H<GG>, R.CUS.POS, F.CUS.POS, E8)
            KEY.HIS.ID<GG> = KEY.LIST.H<GG>:";1"
            CALL F.READ(FN.CUS.POS.H,KEY.HIS.ID<GG>, R.CUS.POS.H, F.CUS.POS.H, E9)
            CALL F.WRITE(FN.CUS.POS.H,KEY.HIS.ID<GG>, R.CUS.POS.H)
            CALL JOURNAL.UPDATE(KEY.HIS.ID<GG>)
            *UPDATED BY NOHA HAMED 21/3/2016
            *DELETE F.CUS.POS , KEY.LIST.H<GG>
            CALL F.DELETE (FN.CUS.POS, KEY.LIST.H<GG>)

        NEXT GG
    END
    M.SEL = "SSELECT F.SCB.CUST.POS.TODAY WITH @ID LIKE ...":PDAT
    KEY.LIST.D ="" ; SELECTED.D="" ;  ER.MSG.D=""
    CALL EB.READLIST(M.SEL,KEY.LIST.D,"",SELECTED.D,ER.MSG.D)
*    TEXT = 'SELECTED.D=':SELECTED.D ; CALL REM
    IF SELECTED.D THEN
        FOR SS =1 TO SELECTED.D
            CALL F.READ(FN.CUS.POS, KEY.LIST.D<SS>, R.CUS.POS, F.CUS.POS, E9)
            *DELETE F.CUS.POS , KEY.LIST.D<SS>
            CALL F.DELETE (FN.CUS.POS , KEY.LIST.D<SS>)
        NEXT SS
    END
*==================ACCOUNT SELECTION=============================================================
    ACC.SEL = "SELECT FBNK.ACCOUNT WITH ONLINE.ACTUAL.BAL NE '' AND ONLINE.ACTUAL.BAL NE 0 AND CUSTOMER NE '' AND CO.CODE NE EG0010099 BY CUSTOMER"
    KEY.LIST.ACC ="" ; SELECTED.ACC="" ;  ER.MSG.ACC=""
    CALL EB.READLIST(ACC.SEL,KEY.LIST.ACC,"",SELECTED.ACC,ER.MSG.ACC)
    IF SELECTED.ACC THEN
*        TEXT = 'SELECTED.AC=':SELECTED.ACC ; CALL REM
        FOR ACC = 1 TO SELECTED.ACC
            CALL F.READ(FN.ACCOUNT, KEY.LIST.ACC<ACC>, R.ACCOUNT, F.ACCOUNT, E2)
            CUSTT.AC<ACC>= R.ACCOUNT<AC.CUSTOMER>
            CALL F.READ(FN.CUSTOMER,CUSTT.AC<ACC>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.AC<ACC> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.AC<ACC> = TRIM(CUST.BR.AC<ACC>,"0","L")


*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.AC<ACC> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            KEY.AC<ACC> = CUSTT.AC<ACC>:'-':TODAY
            CATEG.AC<ACC> = R.ACCOUNT<AC.CATEGORY>
            CURR.AC<ACC> = R.ACCOUNT<AC.CURRENCY>
*****MAHMOUD 19/7/2011********************************
*            COMP.CO.AC<ACC> = R.ACCOUNT<AC.CO.CODE>
            COMP.CO.AC<ACC> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
******************************************************
            CALL F.READ(FN.CUS.POS, KEY.AC<ACC>, R.CUS.POS, F.CUS.POS, E5)
            IF NOT(E5) THEN
                CUST.AC.ID = R.CUS.POS<CUST.AC>
*Line [ 148 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(CUST.AC.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.AC,XX>= KEY.LIST.ACC<ACC>
                R.CUS.POS<CUST.AC.CURR,XX>= CURR.AC<ACC>
                R.CUS.POS<CUST.AC.CATEG,XX>= CATEG.AC<ACC>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.AC<ACC>
                R.CUS.POS<CUST.CO.CODE> = COMP.CO.AC<ACC>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.AC<ACC>
                CALL F.WRITE(FN.CUS.POS,KEY.AC<ACC>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.AC<ACC>)
            END ELSE
                R.CUS.POS<CUST.AC,1>= KEY.LIST.ACC<ACC>
                R.CUS.POS<CUST.AC.CURR,1>= CURR.AC<ACC>
                R.CUS.POS<CUST.AC.CATEG,1>= CATEG.AC<ACC>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.AC<ACC>

                R.CUS.POS< CUST.CO.CODE> = COMP.CO.AC<ACC>

                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.AC<ACC>
                CALL F.WRITE(FN.CUS.POS,KEY.AC<ACC>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.AC<ACC>)
            END
        NEXT ACC
    END   ;**SELECTED.ACC**

*==================LD SELECTION ====================================================================
    LD.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT NE 0 AND CUSTOMER.ID NE '' AND STATUS NE LIQ AND (CATEGORY GE 21001 AND CATEGORY LE 21010) BY CUSTOMER.ID BY CATEGORY BY CURRENCY"
    KEY.LIST.LD ="" ; SELECTED.LD="" ;  ER.MSG.LD=""
    CALL EB.READLIST(LD.SEL,KEY.LIST.LD,"",SELECTED.LD,ER.MSG.LD)
    IF SELECTED.LD THEN
*        TEXT = 'SELECTED.LD=':SELECTED.LD ; CALL REM
        FOR LD = 1 TO SELECTED.LD
            DD = 0 ; XX = 0
            CALL F.READ(FN.LD,KEY.LIST.LD<LD>,R.LD,F.LD,E3)
            LD.CUST<LD> = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CUSTOMER,LD.CUST<LD>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.LD<LD> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.LD<LD> = TRIM(CUST.BR.LD<LD>,"0","L")

*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.LD<LD> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            LD.CURR<LD> = R.LD<LD.CURRENCY>
            LD.CATEG<LD> = R.LD<LD.CATEGORY>
*****MAHMOUD 19/7/2011*******************************
*            LD.COMP.CO<LD> = R.LD<LD.CO.CODE>
            LD.COMP.CO<LD> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
*****************************************************
            KEY.LD<LD> = LD.CUST<LD>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.LD<LD>, R.CUS.POS, F.CUS.POS, E5)
            IF NOT(E5) THEN
                CUST.LD.ID = R.CUS.POS<CUST.LD>
*Line [ 201 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(CUST.LD.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.LD,XX>= KEY.LIST.LD<LD>
                R.CUS.POS<CUST.LD.CURR,XX>= LD.CURR<LD>
                R.CUS.POS<CUST.LD.CATEG,XX>= LD.CATEG<LD>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LD<LD>
                R.CUS.POS<CUST.CO.CODE> = LD.COMP.CO<LD>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.LD<LD>
                CALL F.WRITE(FN.CUS.POS,KEY.LD<LD>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LD<LD>)
            END ELSE
                R.CUS.POS<CUST.LD,1>= KEY.LIST.LD<LD>
                R.CUS.POS<CUST.LD.CURR,1>= LD.CURR<LD>
                R.CUS.POS<CUST.LD.CATEG,1>= LD.CATEG<LD>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LD<LD>
                R.CUS.POS<CUST.CO.CODE> = LD.COMP.CO<LD>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.LD<LD>
                CALL F.WRITE(FN.CUS.POS,KEY.LD<LD>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LD<LD>)
            END
        NEXT LD
    END   ;**END OF SELECTED.LD**
*==================CD SELECTION ====================================================================
    CD.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT NE 0 AND CUSTOMER.ID NE '' AND STATUS NE LIQ AND (CATEGORY GE 21020 AND CATEGORY LE 21025) BY CUSTOMER.ID BY CATEGORY BY CURRENCY"
    KEY.LIST.CD ="" ; SELECTED.CD="" ;  ER.MSG.CD=""
    CALL EB.READLIST(CD.SEL,KEY.LIST.CD,"",SELECTED.CD,ER.MSG.CD)
    IF SELECTED.CD THEN
*        TEXT = 'SELECTED.CD=':SELECTED.CD ; CALL REM
        FOR CD = 1 TO SELECTED.CD
            DD = 0 ; XX = 0
            CALL F.READ(FN.LD,KEY.LIST.CD<CD>,R.LD,F.LD,E3)
            CD.CUST<CD> = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CUSTOMER,CD.CUST<CD>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.CD<CD> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.CD<CD> = TRIM(CUST.BR.CD<CD>,"0","L")

*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.CD<CD> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            CD.CURR<CD> = R.LD<LD.CURRENCY>
            CD.CATEG<CD> = R.LD<LD.CATEGORY>
*****MAHMOUD 19/7/2011***************************
*            CD.COMP.CO<CD> = R.LD<LD.CO.CODE>
            CD.COMP.CO<CD> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
*************************************************
            KEY.CD<CD> = CD.CUST<CD>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.CD<CD>, R.CUS.POS, F.CUS.POS, E5)
            IF NOT(E5) THEN
                KEY.CD.ID = R.CUS.POS<CUST.CD>
*Line [ 251 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.CD.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.CD,XX>= KEY.LIST.CD<CD>
                R.CUS.POS<CUST.CD.CURR,XX>= CD.CURR<CD>
                R.CUS.POS<CUST.CD.CATEG,XX>= CD.CATEG<CD>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.CD<CD>
                R.CUS.POS<CUST.CO.CODE> = CD.COMP.CO<CD>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.CD<CD>
                CALL F.WRITE(FN.CUS.POS,KEY.CD<CD>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.CD<CD>)
            END ELSE
                R.CUS.POS<CUST.CD,1>= KEY.LIST.CD<CD>
                R.CUS.POS<CUST.CD.CURR,1>= CD.CURR<CD>
                R.CUS.POS<CUST.CD.CATEG,1>= CD.CATEG<CD>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.CD<CD>
                R.CUS.POS<CUST.CO.CODE> = CD.COMP.CO<CD>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.CD<CD>
                CALL F.WRITE(FN.CUS.POS,KEY.CD<CD>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.CD<CD>)
            END
        NEXT CD
    END   ;**END OF SELECTED.CD**
*==================LG SELECTION ====================================================================
    LG.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT NE 0 AND CUSTOMER.ID NE '' AND STATUS NE LIQ AND CATEGORY EQ 21096 BY CUSTOMER.ID BY CATEGORY BY CURRENCY"
    KEY.LIST.LG ="" ; SELECTED.LG="" ;  ER.MSG.LG=""
    CALL EB.READLIST(LG.SEL,KEY.LIST.LG,"",SELECTED.LG,ER.MSG.LG)
    IF SELECTED.LG THEN
*        TEXT = 'SELECTED.LG=':SELECTED.LG ; CALL REM
        FOR LG = 1 TO SELECTED.LG
            DD = 0 ; XX = 0
            CALL F.READ(FN.LD,KEY.LIST.LG<LG>,R.LD,F.LD,E3)
            LG.CUST<LG> = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CUSTOMER,LG.CUST<LG>, R.CUSTOMER, F.CUSTOMER, E1)
*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.LG<LG> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            CUST.BR.LG<LG> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.LG<LG> = TRIM(CUST.BR.LG<LG>,"0","L")

            LG.CURR<LG> = R.LD<LD.CURRENCY>
            LG.CATEG<LG> = R.LD<LD.CATEGORY>
*****MAHMOUD 19/7/2011 *******************************
*            LG.COMP.CO<LG> = R.LD<LD.CO.CODE>
            LG.COMP.CO<LG> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
******************************************************
            KEY.LG<LG> = LG.CUST<LG>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.LG<LG>, R.CUS.POS, F.CUS.POS, E5)
            IF NOT(E5) THEN
                KEY.LG.ID = R.CUS.POS<CUST.LG>
*Line [ 301 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.LG.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.LG,XX>= KEY.LIST.LG<LG>
                R.CUS.POS<CUST.LG.CURR,XX>= LG.CURR<LG>
                R.CUS.POS<CUST.LG.CATEG,XX>= LG.CATEG<LG>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LG<LG>
                R.CUS.POS<CUST.CO.CODE> = LG.COMP.CO<LG>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.LG<LG>
                CALL F.WRITE(FN.CUS.POS,KEY.LG<LG>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LG<LG>)
            END ELSE
                R.CUS.POS<CUST.LG,1>= KEY.LIST.LG<LG>
                R.CUS.POS<CUST.LG.CURR,1>= LG.CURR<LG>
                R.CUS.POS<CUST.LG.CATEG,1>= LG.CATEG<LG>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LG<LG>
                R.CUS.POS<CUST.CO.CODE> = LG.COMP.CO<LG>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.LG<LG>
                CALL F.WRITE(FN.CUS.POS,KEY.LG<LG>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LG<LG>)
            END
        NEXT LG
    END   ;**END OF SELECTED.LG**
*=================LC IMP SELECTION =================================================================
    LC.SEL.IMP = "SELECT FBNK.LETTER.OF.CREDIT WITH APPLICANT.CUSTNO NE '' AND LIABILITY.AMT NE 0 AND LC.TYPE LIKE LI... BY APPLICANT.CUSTNO BY LC.TYPE BY LC.CURRENCY"
    KEY.LIST.LC.IMP ="" ; SELECTED.LC.IMP="" ;  ER.MSG.LC.IMP=""
    CALL EB.READLIST(LC.SEL.IMP,KEY.LIST.LC.IMP,"",SELECTED.LC.IMP,ER.MSG.LC.IMP)
    IF SELECTED.LC.IMP THEN
*        TEXT = 'SEL.LC.IMP=':SELECTED.LC.IMP ; CALL REM
        FOR LCC = 1 TO SELECTED.LC.IMP
            DD = 0 ; XX = 0
            CALL F.READ(FN.LC,KEY.LIST.LC.IMP<LCC>,R.LC,F.LC,E4)
            CUSTT.LC.IMP<LCC> = R.LC<TF.LC.APPLICANT.CUSTNO>
            CALL F.READ(FN.CUSTOMER,CUSTT.LC.IMP<LCC>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.LC.IMP<LCC> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.LC.IMP<LCC> = TRIM(CUST.BR.LC.IMP<LCC>,"0","L")


*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.IM<LCC> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            CURR.LC.IMP<LCC> = R.LC<TF.LC.LC.CURRENCY>
            LC.CAT.IMP<LCC> = R.LC<TF.LC.CATEGORY.CODE>
*****MAHMOUD 19/7/2011 ***********************************
*            LC.COMP.CO.I<LCC> = R.LC<TF.LC.CO.CODE>
            LC.COMP.CO.I<LCC> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
**********************************************************
            KEY.LC.IMP<LCC> = CUSTT.LC.IMP<LCC>:'-':TODAY
            CALL F.READ(FN.CUS.POS, KEY.LC.IMP<LCC> , R.CUS.POS, F.CUS.POS, E5)
            IF NOT(E5) THEN
                KEY.LC.IMP.ID = R.CUS.POS<CUST.LC.IMP>
*Line [ 352 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.LC.IMP.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.LC.IMP,XX>= KEY.LIST.LC.IMP<LCC>
                R.CUS.POS<CUST.LC.CURR.IMP,XX>= CURR.LC.IMP<LCC>
                R.CUS.POS<CUST.LC.CAT.IMP,XX>= LC.CAT.IMP<LCC>
                R.CUS.POS<CUST.CUS.BRANCH> = CUSTT.LC.IMP<LCC>
                R.CUS.POS<CUST.CO.CODE> = LC.COMP.CO.I<LCC>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.IM<LCC>
                CALL F.WRITE(FN.CUS.POS,KEY.LC.IMP<LCC>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LC.IMP<LCC>)
            END ELSE
                R.CUS.POS<CUST.LC.IMP,1>= KEY.LIST.LC.IMP<LCC>
                R.CUS.POS<CUST.LC.CURR.IMP,1>= CURR.LC.IMP<LCC>
                R.CUS.POS<CUST.LC.CAT.IMP,1>= LC.CAT.IMP<LCC>
                R.CUS.POS<CUST.CUS.BRANCH> = CUSTT.LC.IMP<LCC>
                R.CUS.POS<CUST.CO.CODE> = LC.COMP.CO.I<LCC>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.IM<LCC>
                CALL F.WRITE(FN.CUS.POS,KEY.LC.IMP<LCC>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LC.IMP<LCC>)
            END
        NEXT LCC
    END
*=================LC EXP SELECTION =================================================================
    LC.SEL.EXP = "SELECT FBNK.LETTER.OF.CREDIT WITH BENEFICIARY.CUSTNO NE '' AND LIABILITY.AMT NE 0 AND LC.TYPE LIKE LE... BY BENEFICIARY.CUSTNO BY LC.TYPE BY LC.CURRENCY"
    KEY.LIST.LC.EXP ="" ; SELECTED.LC.EXP="" ;  ER.MSG.LC.EXP=""
    CALL EB.READLIST(LC.SEL.EXP,KEY.LIST.LC.EXP,"",SELECTED.LC.EXP,ER.MSG.LC.EXP)
    IF SELECTED.LC.EXP THEN
*        TEXT = 'SEL.LC.EXP=':SELECTED.LC.EXP ; CALL REM
        FOR LCEXP = 1 TO SELECTED.LC.EXP
            DD = 0 ; XX = 0
            CALL F.READ(FN.LC,KEY.LIST.LC.EXP<LCEXP>,R.LC,F.LC,E4)
            CUSTT.LC.EXP<LCEXP> = R.LC<TF.LC.BENEFICIARY.CUSTNO>
            CALL F.READ(FN.CUSTOMER,CUSTT.LC.EXP<LCEXP>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.LC.EXP<LCEXP> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.LC.EXP<LCEXP> = TRIM(CUST.BR.LC.EXP<LCEXP>,"0","L")

*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.EX<LCEXP> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            LC.CURR.EXP<LCEXP> = R.LC<TF.LC.LC.CURRENCY>
            LC.CAT.EXP<LCEXP> = R.LC<TF.LC.CATEGORY.CODE>
*****MAHMOUD 19/7/2011 ************************************
*            LC.COMP.CO.E<LCEXP> = R.LC<TF.LC.CO.CODE>
            LC.COMP.CO.E<LCEXP> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
***********************************************************
            KEY.LC.EXP<LCEXP> =  CUSTT.LC.EXP<LCEXP>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.LC.EXP<LCEXP> , R.CUS.POS, F.CUS.POS , E5)
            IF NOT(E5) THEN
                KEY.LC.EXP.ID = R.CUS.POS<CUST.LC.EXP>
*Line [ 402 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.LC.EXP.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.LC.EXP,XX>= KEY.LIST.LC.EXP<LCEXP>
                R.CUS.POS<CUST.LC.CURR.EXP,XX>= LC.CURR.EXP<LCEXP>
                R.CUS.POS<CUST.LC.CAT.EXP,XX>= LC.CAT.EXP<LCEXP>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LC.EXP<LCEXP>
                R.CUS.POS<CUST.CO.CODE> = LC.COMP.CO.E<LCEXP>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.EX<LCEXP>
                CALL F.WRITE(FN.CUS.POS, KEY.LC.EXP<LCEXP>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LC.EXP<LCEXP>)
            END ELSE
                R.CUS.POS<CUST.LC.EXP,1>= KEY.LIST.LC.EXP<LCEXP>
                R.CUS.POS<CUST.LC.CURR.EXP,1>= LC.CURR.EXP<LCEXP>
                R.CUS.POS<CUST.LC.CAT.EXP,1>= LC.CAT.EXP<LCEXP>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LC.EXP<LCEXP>
                R.CUS.POS<CUST.CO.CODE> = LC.COMP.CO.E<LCEXP>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.EX<LCEXP>
                CALL F.WRITE(FN.CUS.POS, KEY.LC.EXP<LCEXP>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LC.EXP<LCEXP>)
            END
        NEXT LCEXP
    END
*==============================================================
*=================LC COLL SELECTION =================================================================
    LC.SEL.COLL = "SELECT FBNK.LETTER.OF.CREDIT WITH LIABILITY.AMT NE 0 AND LC.TYPE LIKE DI... BY BENEFICIARY.CUSTNO BY LC.TYPE BY LC.CURRENCY"
    KEY.LIST.LC.COLL ="" ; SELECTED.LC.COLL="" ;  ER.MSG.LC.COLL=""
    CALL EB.READLIST(LC.SEL.COLL,KEY.LIST.LC.COLL,"",SELECTED.LC.COLL,ER.MSG.LC.COLL)
    IF SELECTED.LC.COLL THEN
*        TEXT = 'SEL.LC.COLL=':SELECTED.LC.COLL ; CALL REM
        FOR LCCOLL = 1 TO SELECTED.LC.COLL
            DD = 0 ; XX = 0
*            TEXT = 'LC.COLL.ID=' :KEY.LIST.LC.COLL<LCCOLL> ; CALL REM
            CALL F.READ(FN.LC,KEY.LIST.LC.COLL<LCCOLL>,R.LC,F.LC,E4)
            CUSTT.LC.COLL<LCCOLL> = R.LC<TF.LC.BENEFICIARY.CUSTNO>
            CALL F.READ(FN.CUSTOMER,CUSTT.LC.COLL<LCCOLL>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.LC.COLL<LCCOLL> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.LC.COLL<LCCOLL> = TRIM(CUST.BR.LC.COLL<LCCOLL>,"0","L")

*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.CO<LCCOLL> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            LC.CURR.COLL<LCCOLL> = R.LC<TF.LC.LC.CURRENCY>
            LC.CAT.COLL<LCCOLL> = R.LC<TF.LC.CATEGORY.CODE>
*****MAHMOUD 19/7/2011 **************************************
*            LC.COMP.CODE<LCCOLL> = R.LC<TF.LC.CO.CODE>
            LC.COMP.CODE<LCCOLL> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
*************************************************************
            KEY.LC.COLL<LCCOLL> =  CUSTT.LC.COLL<LCCOLL>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.LC.COLL<LCCOLL> , R.CUS.POS, F.CUS.POS , E5)
            IF NOT(E5) THEN
                KEY.LC.COLL.ID = R.CUS.POS<CUST.LC.COLL>
*Line [ 454 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.LC.COLL.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.LC.COLL,XX>= KEY.LIST.LC.COLL<LCCOLL>
*                TEXT = 'COLL.ID=':KEY.LIST.LC.COLL<LCCOLL> ; CALL REM
                R.CUS.POS<CUST.LC.CURR.COLL,XX>= LC.CURR.EXP<LCCOLL>
                R.CUS.POS<CUST.LC.CAT.COLL,XX>= LC.CAT.EXP<LCCOLL>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LC.COLL<LCCOLL>
                R.CUS.POS<CUST.CO.CODE> = LC.COMP.CO.C<LCCOLL>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.CO<LCCOLL>
                CALL F.WRITE(FN.CUS.POS, KEY.LC.COLL<LCCOLL>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LC.COLL<LCCOLL>)
            END ELSE
                R.CUS.POS<CUST.LC.COLL,1>= KEY.LIST.LC.COLL<LCCOLL>
                R.CUS.POS<CUST.LC.CURR.COLL,1>= LC.CURR.EXP<LCCOLL>
                R.CUS.POS<CUST.LC.CAT.COLL,1>= LC.CAT.EXP<LCCOLL>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LC.COLL<LCCOLL>
                R.CUS.POS<CUST.CO.CODE> = LC.COMP.CO.C<LCCOLL>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.CO<LCCOLL>
                CALL F.WRITE(FN.CUS.POS, KEY.LC.COLL<LCCOLL>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LC.COLL<LCCOLL>)
            END
        NEXT LCCOLL
    END
*====================================================================================
*=================LIMIT SELECTION =================================================================
    LIMIT.SEL = "SELECT FBNK.LIMIT WITH PRODUCT.ALLOWED EQ '' AND MAXIMUM.TOTAL NE '' "
    KEY.LIST.LIMIT ="" ; SELECTED.LIMIT="" ;  ER.MSG.LIMIT=""
    CALL EB.READLIST(LIMIT.SEL,KEY.LIST.LIMIT,"",SELECTED.LIMIT,ER.MSG.LIMIT)
    IF SELECTED.LIMIT THEN
*        TEXT = 'SEL.LIMIT=':SELECTED.LIMIT ; CALL REM
        FOR LM = 1 TO SELECTED.LIMIT
            DD = 0 ; XX = 0
            CALL F.READ(FN.LIMIT,KEY.LIST.LIMIT<LM>,R.LIMIT,F.LIMIT,E6)
            CUSTT.LIMIT<LM> = R.LIMIT<LI.LIABILITY.NUMBER>
            CALL F.READ(FN.CUSTOMER,CUSTT.LIMIT<LM>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.LIMIT<LM> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.LIMIT<LM> = TRIM(CUST.BR.LIMIT<LM>,"0","L")


*****ADDED BY MAHMOUD 25/10/2009******
            CUST.CO.BOOK<LM>  = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
**************************************
*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.LI<LM> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            LIMIT.CURR<LM> = R.LIMIT<LI.LIMIT.CURRENCY>
            LIMIT.PRODUCT<LM> = R.LIMIT<LI.LIMIT.PRODUCT>
*****UPDATED BY MAHMOUD 25/10/2009******
***          LIMIT.COMP.CO<LM> = R.LIMIT<LI.LOCAL.REF,LILR.COMPANY.BOOK>
            LIMIT.COMP.CO<LM> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
****************************************
            KEY.LIMIT<LM> =  CUSTT.LIMIT<LM>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.LIMIT<LM> , R.CUS.POS, F.CUS.POS , E5)
            IF NOT(E5) THEN
                KEY.LIM.ID = R.CUS.POS<CUST.LIMIT>
*Line [ 510 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.LIM.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.LIMIT,XX>= KEY.LIST.LIMIT<LM>
                R.CUS.POS<CUST.LIMIT.CURR,XX>= LIMIT.CURR<LM>
                R.CUS.POS<CUST.LIMIT.PRODUCT,XX>= LIMIT.PRODUCT<LM>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LIMIT<LM>
                R.CUS.POS<CUST.CO.CODE> = LIMIT.COMP.CO<LM>
                R.CUS.POS<CUST.OLD.CUST.ID>= CUST.OLD.ID.LI<LM>
                CALL F.WRITE(FN.CUS.POS, KEY.LIMIT<LM>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LIMIT<LM>)
            END ELSE
                R.CUS.POS<CUST.LIMIT,1>= KEY.LIST.LIMIT<LM>
                R.CUS.POS<CUST.LIMIT.CURR,1>= LIMIT.CURR<LM>
                R.CUS.POS<CUST.LIMIT.PRODUCT,1>= LIMIT.PRODUCT<LM>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.LIMIT<LM>
                R.CUS.POS<CUST.CO.CODE> = LIMIT.COMP.CO<LM>
                R.CUS.POS<CUST.OLD.CUST.ID>= CUST.OLD.ID.LI<LM>
                CALL F.WRITE(FN.CUS.POS, KEY.LIMIT<LM>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.LIMIT<LM>)
            END
        NEXT LM
    END
*====================================================================================
*=================COLLATERAL SELECTION =================================================================
    COLL.SEL = "SELECT FBNK.COLLATERAL WITH NOMINAL.VALUE NE 0 "
    KEY.LIST.COLL ="" ; SELECTED.COLL="" ;  ER.MSG.COLL=""
    CALL EB.READLIST(COLL.SEL,KEY.LIST.COLL,"",SELECTED.COLL,ER.MSG.COLL)
    IF SELECTED.COLL THEN
*        TEXT = 'SEL.COLL=':SELECTED.COLL ; CALL REM
        FOR COLL = 1 TO SELECTED.COLL
            DD = 0 ; XX = 0
            CALL F.READ(FN.COLL,KEY.LIST.COLL<COLL>,R.COLL,F.COLL,E7)
            CUSTT.COLL<COLL> = R.COLL<COLL.LOCAL.REF,COLR.CUS.LIAB>
            CALL F.READ(FN.CUSTOMER,CUSTT.COLL<COLL>, R.CUSTOMER, F.CUSTOMER, E1)
            CUST.BR.COLL<COLL> = R.CUSTOMER<EB.CUS.CO.CODE>[2]
            CUST.BR.COLL<COLL> = TRIM(CUST.BR.COLL<COLL>,"0","L")


*****ADDED ON 14/10/2008*****
            CUST.OLD.ID.CL<COLL> = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************************
            COLL.CURR<COLL> = R.COLL<COLL.CURRENCY>
            COLL.CODE<COLL> = R.COLL<COLL.COLLATERAL.CODE>
*****UPDATED BY MAHMOUD 25/10/2009******
***          COLL.COMP.CO<COLL> = R.COLL<COLL.LOCAL.REF,COLR.COMPANY.BOOK>
            COLL.COMP.CO<COLL> = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
****************************************
            KEY.COLL<COLL> =  CUSTT.COLL<COLL>:'-':TODAY
            CALL F.READ(FN.CUS.POS,KEY.COLL<COLL> , R.CUS.POS, F.CUS.POS , E5)
            IF NOT(E5) THEN
                KEY.COLL.ID = R.CUS.POS<CUST.COLLATERAL>
*Line [ 562 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(KEY.COLL.ID,@VM)
                XX = DD+1
                R.CUS.POS<CUST.COLLATERAL,XX>= KEY.LIST.COLL<COLL>
                R.CUS.POS<CUST.COLL.CURR,XX>= COLL.CURR<COLL>
                R.CUS.POS<CUST.COLL.CODE,XX>= COLL.CODE<COLL>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.COLL<COLL>
                R.CUS.POS<CUST.CO.CODE>  = COLL.COMP.CO<COLL>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.CL<COLL>
                CALL F.WRITE(FN.CUS.POS, KEY.COLL<COLL>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.COLL<COLL>)
            END ELSE
                R.CUS.POS<CUST.COLLATERAL,1>= KEY.LIST.COLL<COLL>
                R.CUS.POS<CUST.COLL.CURR,1>= COLL.CURR<COLL>
                R.CUS.POS<CUST.COLL.CODE,1>= COLL.CODE<COLL>
                R.CUS.POS<CUST.CUS.BRANCH> = CUST.BR.COLL<COLL>
                R.CUS.POS<CUST.CO.CODE>  = COLL.COMP.CO<COLL>
                R.CUS.POS<CUST.OLD.CUST.ID> = CUST.OLD.ID.CL<COLL>
                CALL F.WRITE(FN.CUS.POS, KEY.COLL<COLL>, R.CUS.POS)
                CALL JOURNAL.UPDATE(KEY.COLL<COLL>)
            END
        NEXT COLL
    END
*====================================================================================
    RETURN
END
