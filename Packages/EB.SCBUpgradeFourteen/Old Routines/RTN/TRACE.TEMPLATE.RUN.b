* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>129</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE TRACE.TEMPLATE.RUN( NEST.LEVEL, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

** ----- ----- -----
MAIN:
** ----- ----- -----

      ** ----- routine file -----

      CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Routine file ::')
*Line [ 35 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
      CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'ROUTINE FILE FOR TEMPLATE')

      ** ----- include file -----

      INCLUDE.FILE.NAME = 'I_F.' : ITEM.NAME
      SELECT.STATEMENT = 'SELECT ' : ROUTINE.DIR : ' WITH EQ "' : INCLUDE.FILE.NAME : '"'
      CALL EB.READLIST( SELECT.STATEMENT, KEY.LIST, '', SELECTED, SYSTEM.RETURN.CODE)

      IF SYSTEM.RETURN.CODE >= 0 AND SELECTED THEN

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Include file ::')
*Line [ 47 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
         CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, INCLUDE.FILE.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'INCLUDE FILE FOR TEMPLATE')

      END

      ** ----- FILE.CONTROL entry -----

      CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: FILE.CONTROL entry ::')
*Line [ 55 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
      CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'FILE.CONTROL', NEST.LEVEL + 1, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'FILE.CONTROL FOR TEMPLATE')

      ** ----- STANDARD.SELECTION entry -----

      CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: STANDARD.SELECTION entry ::')
*Line [ 61 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
      CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'STANDARD.SELECTION', NEST.LEVEL + 1, ITEM.NAME:'!', REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'STANDARD.SELECTION FOR TEMPLATE')

      ** ----- list enquiries -----

      NO.ENQUIRY.HEADER = 1

      LIST.ENQUIRY = ITEM.NAME : '-LIST'
      GOSUB PROCESS.LIST.ENQUIRY

      LIST.ENQUIRY = '%' : ITEM.NAME
      GOSUB PROCESS.LIST.ENQUIRY

      LIST.ENQUIRY = '%' : ITEM.NAME : '$NAU'
      GOSUB PROCESS.LIST.ENQUIRY

      LIST.ENQUIRY = '%' : ITEM.NAME : '$HIS'
      GOSUB PROCESS.LIST.ENQUIRY

      ** ----- ----- -----

      RETURN

** ----- ----- -----
PROCESS.LIST.ENQUIRY:
** ----- ----- -----

      ETEXT = ''
      CALL DBR( 'ENQUIRY':@FM:1, LIST.ENQUIRY, MY.DUMMY)
      IF ETEXT THEN ETEXT = ''
      ELSE

         IF NO.ENQUIRY.HEADER THEN NO.ENQUIRY.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: List enquiries ::')
*Line [ 94 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
         CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ENQUIRY', NEST.LEVEL + 1, LIST.ENQUIRY, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'LIST ENQUIRY FOR TEMPLATE')

      END

      RETURN

** ----- ----- -----

END
