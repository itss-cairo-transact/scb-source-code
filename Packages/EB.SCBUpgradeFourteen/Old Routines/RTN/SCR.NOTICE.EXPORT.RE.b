* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
***************************NI7OOOOOOOOOOO**************************
*-----------------------------------------------------------------------------
* <Rating>-121</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCR.NOTICE.EXPORT.RE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCR.NOTICE.EXPORT.RE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID = COMI
    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    FN.DR.HIS = 'FBNK.DRAWINGS$HIS' ; F.DR.HIS = ''
    CALL OPF(FN.DR.HIS,F.DR.HIS)

    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    TEXT = "COMI = " : COMI ; CALL REM
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    OUT.AMOUNT = ''

    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '����� ���' : "SCR.NOTICE.NEW"
*------------------------------------------------------------------------
    DB.ACC = R.DR<TF.DR.DRAWDOWN.ACCOUNT>
    CR.ACC = R.DR<TF.DR.PAYMENT.ACCOUNT>
    ACC.NO = DB.ACC
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
     CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
     ACC.BR2 = ACC.BR1[8,2]
     ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
    CUST.NAME   = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME.2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
    DR.AMOUNT = R.DR<TF.DR.REIMBURSE.AMOUNT>
    CR.AMOUNT = R.DR<TF.DR.PAYMENT.AMOUNT>
    DOC.AMT   = R.DR<TF.DR.DOCUMENT.AMOUNT>
    DR.RATE   = R.DR<TF.DR.RATE.BOOKED>
    REF.LOC   = R.DR<TF.DR.LOCAL.REF><1,DRLR.OUR.REFERENCE>

    AMOUNT    = DR.AMOUNT
** AMOUNT    = CR.AMOUNT
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    DAT       = R.DR<TF.DR.DEBIT.VALUE>
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    BR.DATA   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES>
* REFER     = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE>
    BR.DATA1   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES.DEBIT,1>
    BR.DATA2   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES.DEBIT,2>
    BR.DATA3   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES.DEBIT,3>
    BR.DATA4   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES.DEBIT,4>
    REFER      = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE.DEBIT>

    ID.HIS = COMI:';1'


    CALL F.READ(FN.DR.HIS,ID.HIS,R.DR.HIS,F.DR.HIS,ERR.HIS)
    INPUTT    = R.DR.HIS<TF.DR.INPUTTER>
**    AUTH.SON  = R.USER<EB.USE.SIGN.ON.NAME>
    AUTH.SON  = R.DR.HIS<TF.DR.AUTHORISER>
    TEXT = AUTH.SON ; CALL REM
* CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH.SON,AUTH)
    INP       = FIELD(INPUTT,'_',2)
    AUTHI     = FIELD(AUTH.SON,'_',2)
*------------------------------------------------------------------------

    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX12   = SPACE(132)  ; XX13  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX14   = SPACE(132)  ; XX15  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX16   = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX20   = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11  = SPACE(132)

*  IF CAT.ID EQ 1512 THEN
*      LOAN.AMT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.MARG.AMOUNT>
*      DAT1= R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NO.DAY>
*      LOAN.DATE = DAT1[7,2]:'/':DAT1[5,2]:"/":DAT1[1,4]
*      XX11<1,1>[3,15]  = '���� �����     : ':' ':LOAN.AMT
*      XX11<1,1>[45,35] = '������ ����� �� : ':' ':LOAN.DATE
*  END


    XX1<1,1>[3,35]   = CUST.NAME
    XX1<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ ���� '
    XX1<1,1>[45,15] = '���� ����� ���� '
    XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ��������� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX6<1,1>[1,15]  = '������'
    XX6<1,1>[30,15] = '��� �������'
    XX6<1,1>[60,15] = '������'

    XX7<1,1>[1,15]  = INP:' �'
    XX7<1,1>[30,15] = COMI:' �'
    XX7<1,1>[60,15] = AUTHI:' �'

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
    XX9<1,1>[3,15]  = '������         : '
    XX20<1,1>[3,15] = '����  :  ' :DOC.AMT : '���� :' : DR.RATE
    XX12<1,1>[3,15] = BR.DATA1
    XX13<1,1>[3,15] = BR.DATA2
    XX14<1,1>[3,15] = BR.DATA3
    XX15<1,1>[3,15] = BR.DATA4

    XX10<1,1>[3,15]  = '������    : '
    IF REFER NE '' THEN
        XX10<1,1>[20,15] = REFER
    END
    IF REFER EQ '' THEN
        XX10<1,1>[20,15] = REF.LOC
    END

*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    TEXT = "BRANCH = " :BRANCH ; CALL REM
    YYBRN  = FIELD(BRANCH,'.',2)

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : " : YYBRN
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":CUST.NAME:' ' : CUST.NAME.2

    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX20<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX10<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
