* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*******ABEER
    SUBROUTINE SCCD.CUS.21102
*    PROGRAM SCCD.CUS.21102
***************************
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH

***********
        NEW.FIL = 'SCCD.CU.21102.':TODAY

        OPENSEQ "SCCD" , NEW.FIL  TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"SCCD":' ':NEW.FIL
            HUSH OFF
        END
        OPENSEQ "SCCD" , NEW.FIL TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE SCCD.CUS.21102 CREATED IN SCCD'
            END ELSE
                STOP 'Cannot create SCCD.CUS.21102 File IN SCCD'
            END
        END

        KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        MAT.DATE = TODAY
        CALL CDT('EG00',MAT.DATE, '1W')
        PRINT MAT.DATE:'AFTER CDT'
        PRINT MAT.DATE:'-MAT.DATE'
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21102 AND STATUS NE LIQ AND FIN.MAT.DATE EQ " : MAT.DATE : " BY CO.CODE "
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        PRINT SELECTED:'-SELE'
        IF SELECTED THEN
            FOR I = 1 TO SELECTED

                CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
                LOCAL.REF = R.LD<LD.LOCAL.REF>

                GRN.WILL=LOCAL.REF<1,LDLR.GRANT.WILL>

                AMT         =R.LD<LD.AMOUNT>
*****
                LIQ.AMT           = LOCAL.REF<1,LDLR.CD.LIQ.AMT>

                IF LIQ.AMT EQ  '' THEN
                    CD.AMT  =  AMT * 1.8
                END ELSE
                    CD.AMT  =  LIQ.AMT
                END

*****
                LD.DEPT.1   = R.LD<LD.DEPT.CODE>
                LD.CODE.1   = R.LD<LD.CO.CODE>
                LD.BRN.1    = LD.CODE.1[8,2]
                LD.TYPE     = LOCAL.REF<1,LDLR.CD.TYPE>
                IF LD.TYPE EQ 'EGP-10-60M-SUEZ.BNK' THEN
                    DB.ACCT     = 'EGP16104000300':LD.BRN.1
                END ELSE
                    IF LD.TYPE EQ 'EGP-100-60M-SUEZ.BNK' THEN
                        DB.ACCT     = 'EGP16104000200':LD.BRN.1
                    END
                END
                CR.ACCT     = LOCAL.REF<1,LDLR.ACCT.STMP>
                LD.CUST.ID  = R.LD<LD.CUSTOMER.ID>
                FIN.DAT     = R.LD<LD.FIN.MAT.DATE>

                IDD = 'FUNDS.TRANSFER,SCCD.MAT,AUTO.SZ//':LD.CODE.1

                OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"ACSZ":','
                OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":','
                OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":"EGP":','
                OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:','
                OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:','
                OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":CD.AMT:','
                OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":FIN.DAT:','
                OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":FIN.DAT:','
                OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":KEY.LIST<I>:','
                OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":KEY.LIST<I>:','
                OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
                IF GRN.WILL NE '' THEN
                    OFS.MESSAGE.DATA := "LOCAL.REF:42:1=":GRN.WILL:','
                END
                OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":','
                OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB.21102':','
                MSG.DATA = IDD:",":",":OFS.MESSAGE.DATA
                WRITESEQ MSG.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            NEXT I
            PRINT SELECTED
        END
*** COPY TO OFS ***
**    END
    EXECUTE 'COPY FROM SCCD TO OFS.IN ':NEW.FIL
   EXECUTE 'DELETE ':"SCCD":' ':NEW.FIL

    RETURN
END
