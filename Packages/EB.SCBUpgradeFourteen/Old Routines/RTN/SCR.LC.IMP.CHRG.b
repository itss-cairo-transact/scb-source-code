* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCR.LC.IMP.CHRG
**PROGRAM  SCR.LC.IMP.CHRG

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.USER
    $INSERT T24.BP  I_F.USER.SIGN.ON.NAME
    $INSERT T24.BP  I_F.DRAWINGS
    $INSERT T24.BP  I_F.ACCOUNT
    $INSERT T24.BP  I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP  I_F.CUSTOMER
    $INSERT T24.BP  I_F.CURRENCY
    $INSERT T24.BP  I_F.CATEGORY
    $INSERT T24.BP  I_F.FT.CHARGE.TYPE
    $INSERT T24.BP  I_F.LETTER.OF.CREDIT
    $INSERT T24.BP  I_F.FT.COMMISSION.TYPE
    $INSERT T24.BP  I_F.LC.ACCOUNT.BALANCES
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_DR.LOCAL.REFS

    GOSUB INITIATE; GOSUB BUILD.DATA
    CALL PRINTER.OFF;CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT CREATED" ; CALL REM
    RETURN
**********************************************************
INITIATE:
    REPORT.ID = 'SCR.LC.IMP.CHRG' ; CALL PRINTER.ON(REPORT.ID,'')
    OUT.AMT  = 0; CHARGE.CURR   = '' ; CHARGE.AMOUNT = 0
    RETURN
**********************************************************
BUILD.DATA:
*-------
    R.LC = ''  ;R.ACC = '' ;R.LCL = ''    ;R.DRL = ''     ;AUTHI = '';INP = ''
    OLD.NO = '';DB.ACC = '';TXT.DESC = '' ; KEY.LIST = '' ; SELECTED = ''
    ER.MSG = '';YY = ''
    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.LC = 'FBNK.LETTER.OF.CREDIT'   ;F.LC='' ;R.LC =''
    CALL OPF(FN.LC,F.LC)

    FN.LC.NAU = 'FBNK.LETTER.OF.CREDIT$NAU'   ;F.LC.NAU='' ;R.LC.NAU =''
    CALL OPF(FN.LC.NAU,F.LC.NAU)

    IF ID.NEW EQ '' THEN
        YTEXT = "Enter the TF No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        LC.ID = COMI
        CALL F.READ(FN.ACC,LC.ID,R.ACC,F.ACC,E1)

        YTEXT = "Enter the DATE No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        YY = COMI
        DB.ACC = ''; ACC.NO = '';TEXT.DESC=''

        LOCATE YY IN R.ACC<LCAC.CHRG.DATE.DUE,1> SETTING III THEN
            DB.ACC    = R.ACC<LCAC.SETTLE.AC.FROM,III>
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DAT.NO = DCOUNT(R.ACC<LCAC.CHRG.DATE.DUE>,@VM)
            ACC.NO = DB.ACC
            CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E11)
            OLD.NO    =  R.LC<TF.LC.OLD.LC.NUMBER>
            INPUTT    =  R.LC<TF.LC.INPUTTER>
            INP       =  FIELD(INPUTT,'_',2)
            AUTHI     =  R.LC<TF.LC.AUTHORISER>
            AUTHI       =  FIELD(AUTHI,'_',2)
            TXT.DESC = '���'
        END
    END ELSE
        IF ID.NEW NE '' THEN
            LC.ID = ID.NEW
            CALL F.READ(FN.LC.NAU,LC.ID,R.LC.NAU,F.LC.NAU,E111)

            DB.ACC = R.LC.NAU<TF.LC.CHARGE.ACCT>

            OLD.NO = R.LC.NAU<TF.LC.OLD.LC.NUMBER>
            INPUTT    = R.LC.NAU<TF.LC.INPUTTER>
            INP       = FIELD(INPUTT,'_',2)
            AUTHI =  OPERATOR
            TXT.DESC = '���'
            ACC.NO =DB.ACC
        END
        ACC.NO =DB.ACC
        YY =TODAY
    END

    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
     CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
     ACC.BR2 = ACC.BR1[8,2]
     ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
    CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME2    = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
    CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>

    DAT            = R.ACC<LCAC.CHRG.DATE.DUE>
    MAT.DATE       = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD   ="'L'":'SCR.LC.IMP.CHRG'
    PR.HD  :="'L'":SPACE(1):"��� ���� ������"
    PR.HD  :="'L'":"������� : ":T.DAY
    PR.HD  :="'L'":"����� : ":YYBRN:

    PRINT

    HEADING PR.HD
    PRINT " "
*------------------------------------------------------------------
    XX = SPACE(132)

    XX<1,1>[3,35]  = "��� ������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME2
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = "�������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS2
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS3
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = ACC.NO
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = CATEG
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '����� ������� : '
    YY.NEW = YY[7,2]:'/':YY[5,2]:'/':YY[1,4]
    XX<1,1>[50,35] = YY.NEW
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,15]  = '������         : '
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    WS.INDX = '0'; CUR = ''; TOT = 0
    IF ID.NEW EQ '' THEN
*Line [ 195 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAT.NO = DCOUNT(R.ACC<LCAC.CHRG.DATE.DUE>,@VM)
    END ELSE
*Line [ 198 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAT.NO= DCOUNT(R.LC.NAU<TF.LC.CHARGE.CODE>,@VM)
    END
    FOR W = 1 TO DAT.NO
        IF ID.NEW EQ '' THEN
            DAT22          = R.ACC<LCAC.CHRG.DATE.DUE><1,W>
            CHARGCODE      = R.ACC<LCAC.CHRG.CODE>
            CHARGE.CODE    = R.ACC<LCAC.CHRG.CODE><1,W>
            CHARGE.AMOUNT  = R.ACC<LCAC.AMT.REC><1,W>
            CHARGE.CURR    = R.ACC<LCAC.CHRG.CCY><1,W>
            IN.AMOUNT      = R.ACC<LCAC.AMT.REC><1,W>
        END ELSE
            DAT22 = YY
            CHARGE.CODE    = R.LC.NAU<TF.LC.CHARGE.CODE><1,W>
            CHARGE.AMOUNT  = R.LC.NAU<TF.LC.CHARGE.AMOUNT><1,W>
            CHARGE.CURR    = R.LC.NAU<TF.LC.CHARGE.CURRENCY><1,W>
            IN.AMOUNT      = R.LC.NAU<TF.LC.CHARGE.AMOUNT><1,W>
        END
        IF DAT22 EQ YY THEN
            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)

            IF ETEXT THEN
                CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
            END
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
            OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
            TOT     += CHARGE.AMOUNT

            XX<1,1>[3,35]   = '�������  :'
            XX<1,1>[50,30]  = CHARGE.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]   = '������     : '
            XX<1,1>[50,30]  = CHARGE.AMOUNT
            PRINT XX<1,1>
            XX<1,1> = ""

            CALL WORDS.ARABIC.DEAL(CHARGE.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
            XX<1,1>[3,35]    = '������ ������� : '
            XX<1,1>[50,30]   = OUT.AMT
            PRINT XX<1,1>
            XX<1,1> = ""
            PRINT " "

            WS.INDX ++
        END
    NEXT W

    FOR II = WS.INDX TO 4
        XX<1,1>[3,35]   = '�������  :'
        XX<1,1>[50,30]  = " "
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]   = '������     : '
        XX<1,1>[50,30]  = " "
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]    = '������ ������� : '
        XX<1,1>[50,30]   = " "
        PRINT XX<1,1>
        XX<1,1> = ""
        PRINT " "
    NEXT II
**********************************************
    XX<1,1>[3,35]    = '������     : '
    XX<1,1>[50,35]   = CUR
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]    = '������ ��������'
    XX<1,1>[50,30]   = TOT
    PRINT XX<1,1>
    XX<1,1> = ""

    CALL WORDS.ARABIC.DEAL(TOT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    XX<1,1>[3,35]    = '������ ������� : '
    XX<1,1>[50,30]   = OUT.AMT
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]   = '������'
    XX<1,1>[50,35]  = INP
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = '��� �������'
    XX<1,1>[50,35] = LC.ID
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = '������'
    XX<1,1>[50,35] = AUTHI
    PRINT XX<1,1>
    XX<1,1> = ""


    XX<1,1>[3,35]  = '������'
    XX<1,1>[50,35] =  OLD.NO
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = '��� �������'
    XX<1,1>[50,35] = TXT.DESC
    PRINT XX<1,1>
    XX<1,1> = ""

    RETURN
END
