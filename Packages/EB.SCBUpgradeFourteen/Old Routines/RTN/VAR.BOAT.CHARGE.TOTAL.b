* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE VAR.BOAT.CHARGE.TOTAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*----------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*----------------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
*------------------------------------------
    COMP = ID.COMPANY
    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    AMT = 0 ; CUS.NO = 0
    BF.NO = 0
    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
    BOT.CUR = R.NEW(BOT.CURRENCY)
*----------------------------------------------
*Line [ 71 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCUS = DCOUNT(R.NEW(BOT.CUST.NO),@VM)
    FOR X = 1 TO DCUS
        BF.NO += R.NEW(BOT.CUST.NO)<1,X>
    NEXT X
    CUS.NO = BF.NO - R.NEW(BOT.CUS.BNK.NO)
*----------------------------------------------
    IF BOT.CUR EQ 'EGP' THEN
        AMT = CUS.NO * 10
    END

    IF BOT.CUR EQ 'USD' THEN
        AMT = CUS.NO * 2
    END
*----------------------------------------------
    V.DATE  = TODAY
*------------------------------------------
    IF R.NEW(BOT.BOAT.CODE) NE '50' THEN
        IF COMP EQ 'EG0010002' THEN
            IF BOT.CUR EQ 'EGP' THEN
                ACCT.NO.DR = 'EGP1200100010002'
            END
            IF BOT.CUR EQ 'USD' THEN
                ACCT.NO.DR = 'USD1200100010002'
            END
        END
        IF COMP EQ 'EG0010020' THEN
            IF BOT.CUR EQ 'EGP' THEN
                ACCT.NO.DR = 'EGP1200100010020'
            END
            IF BOT.CUR EQ 'USD' THEN
                ACCT.NO.DR = 'USD1200100010020'
            END
        END
    END ELSE
        ACCT.NO.DR = '2020097510100101'
    END
*------------------------------------------
    ACCT.NO.CR = 'PL52251'
*------------------------------------------
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC15":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":BOT.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":BOT.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":ACCT.NO.DR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":ACCT.NO.CR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ID.NEW:COMMA

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY
    OFS.ID = "T":TNO:".":ID.NEW:"-":DAT:"-COM"

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E

    RETURN
************************************************************
END
