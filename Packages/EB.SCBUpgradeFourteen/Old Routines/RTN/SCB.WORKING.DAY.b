* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.WORKING.DAY(REQ.DATE,CURR)
************************************************************************
***********by Mahmoud Elhawary ** 3/8/2009 *****************************
************************************************************************
    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE
    INCLUDE T24.BP I_F.CURRENCY
    INCLUDE T24.BP I_F.HOLIDAY
************************************************************************
* convert the date required to the working day according to currency
************************************************************************

    D.TYP = ''
    D.TYP1= ''
    SSS   = 13
    TTT   = ''
    TDR   = ''
    TDM   = ''
    TDMM  = ''
    TTD   = ''
    TDY   = ''
    TDYY  = ''
    FDX   = ''
    CUR.HOL = ''
    FN.HOL = 'F.HOLIDAY' ; F.HOL = '' ; R.HOL = ''
    CALL OPF(FN.HOL,F.HOL)
    FN.HOL2 = 'F.HOLIDAY' ; F.HOL2 = '' ; R.HOL2 = ''
    CALL OPF(FN.HOL2,F.HOL2)
    FN.CURR = "FBNK.CURRENCY" ; F.CURR = '' ; R.CURR = ''
    CALL OPF(FN.CURR,F.CURR)

    TDR   = REQ.DATE
    TDY   = TDR[1,4]
    TDM   = TDR[5,2]
    TTD   = TDR[7,2]
    IF TTD[1,1] EQ '0' THEN
        TTT  = TTD[2,1]
    END ELSE
        TTT = TTD
    END
    TDYY = TDY
    TDMM = TDM
    IF TDMM[1,1] EQ '0' THEN
        TDMM  = TDM[2,1]
    END ELSE
        TDMM = TDM
    END

    CALL DBR('CURRENCY':@FM:EB.CUR.COUNTRY.CODE,CURR,CN.CODE)
    CUR.HOL = CN.CODE:"00":TDY

    IF CURR EQ 'EGP' THEN
        HOL.ID  = "EG00":TDY
        HOL.ID2 = "EG00":TDY
    END ELSE
        HOL.ID  = "EG00":TDY
        HOL.ID2 = CUR.HOL
    END

    GOSUB WW.DAY

    IF REQ.DATE NE '' AND D.TYP NE '' AND D.TYP2 NE '' THEN
        LOOP
        WHILE D.TYP NE 'W' OR D.TYP2 NE 'W'
            D.TYP  = ''
            D.TYP2 = ''
            TTT = TTT + 1
            IF TTT GT 31 THEN
                TDMM = TDMM + 1
                TTT = 1
            END
            IF TDMM GT 12 THEN
                TTT = 1
                TDMM = 1
                TDYY = TDYY +1
                HOL.ID  = HOL.ID[1,4]:TDYY
                HOL.ID2 = HOL.ID2[1,4]:TDYY
            END
            GOSUB WW.DAY
        REPEAT
    END
    IF LEN(TDMM) EQ 1 THEN
        TDMM = "0":TDMM
    END
    IF LEN(TTT) EQ 1 THEN
        TTT = "0":TTT
    END
    REQ.DATE = TDYY:TDMM:TTT
    RETURN
*--------------------------------------------------------
WW.DAY:
    FDX = TDMM + SSS
    CALL F.READ(FN.HOL,HOL.ID,R.HOL,F.HOL,ER.HOL)
    CALL F.READ(FN.HOL2,HOL.ID2,R.HOL2,F.HOL2,ER.HOL2)
    D.TYP  = R.HOL<FDX>[TTT,1]
    D.TYP2 = R.HOL2<FDX>[TTT,1]
    RETURN
*--------------------------------------------------------
END
