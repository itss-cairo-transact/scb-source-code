* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.PRINT.STATEMENTS.LOAD
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_SCB.PRINT.STATEMENTS.COMMON

    FN.STATEMENTS.FOLDER = "F.STATEMENTS.FOLDER"
    FV.STATEMENTS.FOLDER = ""
*OPEN FN.STATEMENTS.FOLDER TO FV.STATEMENTS.FOLDER ELSE NULL
    CALL OPF(FN.STATEMENTS.FOLDER , FV.STATEMENTS.FOLDER)

    FN.SCB.STATEMENT.REQUESTS = "F.SCB.STATEMENT.REQUESTS"
    FV.SCB.STATEMENT.REQUESTS = ""
    CALL OPF(FN.SCB.STATEMENT.REQUESTS,FV.SCB.STATEMENT.REQUESTS)

    FN.SCB.STMT.REQUEST.LIST = "F.SCB.STMT.REQUEST.LIST"
    FV.SCB.STMT.REQUEST.LIST = ""
    CALL OPF(FN.SCB.STMT.REQUEST.LIST,FV.SCB.STMT.REQUEST.LIST)

    FN.ACCOUNT = "F.ACCOUNT"
    FV.ACCOUNT = ""
    CALL OPF(FN.ACCOUNT,FV.ACCOUNT)

    FN.CUSTOMER = "F.CUSTOMER"
    FV.CUSTOMER = ""
    CALL OPF(FN.CUSTOMER,FV.CUSTOMER)

    FN.STMT.ENTRY = "F.STMT.ENTRY"
    FV.STMT.ENTRY = ""
    CALL OPF(FN.STMT.ENTRY,FV.STMT.ENTRY)

    FN.ACCOUNT.STATEMENT = "F.ACCOUNT.STATEMENT"
    FV.ACCOUNT.STATEMENT = ""
    CALL OPF(FN.ACCOUNT.STATEMENT,FV.ACCOUNT.STATEMENT)

    RETURN
END
