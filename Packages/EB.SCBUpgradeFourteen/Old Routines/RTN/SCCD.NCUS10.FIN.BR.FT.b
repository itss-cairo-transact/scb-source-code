* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
***************MAI SAAD*************
*  PROGRAM  SCCD.NCUS10.FIN.BR.FT
    SUBROUTINE  SCCD.NCUS10.FIN.BR.FT
************************************
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
************ 
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
        AMT = 0 ; LCY.AMT = 0 ; LD.COMP = ''
        DB.ACCT = '' ; CR.ACCT = ''

        LIQ.DATE = TODAY
        CALL CDT('EG00', LIQ.DATE, '1W')

        T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21101 AND CD.TYPE EQ 'EGP-10-60M-SUEZ' AND ACTUAL.EXP.DATE EQ ":LIQ.DATE:" BY CO.CODE"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        PRINT 'SELECTED= ': SELECTED
**DIM ZZZ(SELECTED)
        FOR I = 1 TO SELECTED

            LD.ID = KEY.LIST<I>
*********
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E2)
            CALL F.READ(FN.LD,KEY.LIST<I+1>,R.LD.1,F.LD,E3)
            LD.COMP              = R.LD<LD.CO.CODE>
            LD.COMP.1            =R.LD.1<LD.CO.CODE>
**********

            COM.CODE             = LD.COMP[8,2]
            LD.AMT               = R.LD<LD.AMOUNT>
            LD.LOCAL             = R.LD<LD.LOCAL.REF>

            IF LD.LOCAL<1,LDLR.CD.LIQ.AMT> NE '' THEN
                LD.LIQ.AMT  = LD.LOCAL<1,LDLR.CD.LIQ.AMT>
            END ELSE
                LD.LIQ.AMT = LD.AMT * 1.8
            END

            CURR                 = R.LD<LD.CURRENCY>
            ACT.EXP.DT           = LD.LOCAL<LDLR.ACTUAL.EXP.DATE>
            IF LD.COMP EQ LD.COMP.1 THEN
                LCY.AMT += LD.LIQ.AMT
            END ELSE
                LCY.AMT += LD.LIQ.AMT
                CR.ACCT = "EGP16104000500":LD.COMP[8,2]
                DB.ACCT = "EGP1610400050099"

                GOSUB OFS.ENTRY
                LCY.AMT = ''
            END
        NEXT I
***********
        RETURN
**********************************************************
OFS.ENTRY:
***********************************************************
*  Build base OFS entry fields.                    *
***********************************************************
        NEW.FIL          = 'SCCD.NCUS.10.BR':".":CR.ACCT

        OPENSEQ "SCCD" , NEW.FIL TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"SCCD":' ':NEW.FIL
            HUSH OFF
        END
        OPENSEQ "SCCD" , NEW.FIL TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE':NEW.FIL: 'CREATED IN SCCD'
            END ELSE
                STOP 'Cannot create':NEW.FIL: ' File IN SCCD'
            END
        END


        COMMA        =  ","
        IDD = 'FUNDS.TRANSFER,SCCD.LIQ.BR.TRANS,AUTO.SZ//':LD.COMP


        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"ACSZ":','
        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":','
        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":"EGP":','
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:','
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:','
        OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":LCY.AMT:','
        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":ACT.EXP.DT:','
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":ACT.EXP.DT:','
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CR.ACCT:','
        OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":CR.ACCT:','
        OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
        OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":','
        OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB.21101.':COM.CODE:','
*************
        MSG.DATA = IDD:",":",":OFS.MESSAGE.DATA
        WRITESEQ MSG.DATA TO BB  ELSE
            PRINT " ERROR WRITE FILE "
        END

*** COPY TO OFS ***

        EXECUTE 'COPY FROM SCCD TO OFS.IN ':NEW.FIL
        EXECUTE 'DELETE ':"SCCD":' ':NEW.FIL

    RETURN
END
