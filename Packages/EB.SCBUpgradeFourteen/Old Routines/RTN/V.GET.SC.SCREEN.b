* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE V.GET.SC.SCREEN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    FN.FT   ='FBNK.FUNDS.TRANSFER'; F.FT =''
    CALL OPF(FN.FT,F.FT)


    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)


    TRANS.ID = ID.NEW
    AMT          = R.NEW(FT.DEBIT.AMOUNT)
    DR.ACCT.ALL  = R.NEW(FT.DEBIT.ACCT.NO)

    DR.ACCT   = DR.ACCT.ALL[1,8]:'10':DR.ACCT.ALL[11,6]
    DR.CUR   = R.NEW(FT.DEBIT.CURRENCY)
    DB.THEIR = R.NEW(FT.DEBIT.THEIR.REF)
    DB.THEIR = TRANS.ID

    CALL F.READ(FN.CUR,DR.CUR,R.CUR,F.CUR,ERR1)
    RATE        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    Y.AMT       = AMT * RATE
    AMT.PER     = Y.AMT * 0.00114
    CALL EB.ROUND.AMOUNT ('',AMT.PER,'',"2")


    IF AMT.PER LE '11.4' THEN
        AMT.FIN = 11.4
    END

    IF AMT.PER GE  '114' THEN
        AMT.FIN = 114
    END

    IF AMT.PER GT '11.4' AND AMT.PER LT '114' THEN
        AMT.FIN = AMT.PER
    END
    CR.ACCT = 'EGP1611400070099'
    DR.CUR = 'EGP'
**********************************************************
    LINK.DATA<1> = DR.ACCT
    LINK.DATA<2> = CR.ACCT
    LINK.DATA<3> = AMT.FIN
    LINK.DATA<4> = DR.CUR
    LINK.DATA<5> = DB.THEIR


    IF V$FUNCTION = 'I' THEN
        INPUT.BUFFER = C.U:' FUNDS.TRANSFER,SCB.SUEZCANAL.USD2 I ':C.F
        CALL EB.SET.NEXT.TASK(INPUT.BUFFER)
    END
    RETURN
END
