* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.AC.HOLD.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS



    FN.AC.HLD='FBNK.ACCOUNT';ID.HLD=ID.NEW;R.AC.HLD='';F.AC.HLD=''
    CALL OPF(FN.AC.HLD,F.AC.HLD)
    CALL DBR('ACCOUNT':@FM:AC.LOCAL.REF,ID.NEW, MYLOCAL)
    CALL F.READ(FN.AC.HLD,ID.HLD,R.AC.HLD,F.AC.HLD,ETEXT)

*Line [ 41 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    HLD.NO=DCOUNT(R.AC.HLD<AC.LOCAL.REF><1,ACLR.HOLD.DATE>,@SM )

    FOR  I = 1  TO HLD.NO

        R.AC.HLD<AC.LOCAL.REF,ACLR.HOLD.DETAILS,I>=I

        CALL F.WRITE(FN.AC.HLD,ID.HLD,R.AC.HLD)

    NEXT I
    RETURN
END
