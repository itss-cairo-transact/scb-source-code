* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1150</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

    SUBROUTINE SCB.VISA.CBE


******************************************************************

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.TYPE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

    IF E THEN V$ERROR = 1

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')

    EMP.ID = FIELD(ID.NEW,".",1)
    EMP.ID.CHK = FIELD(ID.NEW,".",2)
    IF EMP.ID.CHK EQ '' THEN
        ID.NEW = ID.NEW:'.':DAT[1,6]
    END ELSE
        CHK.LN = LEN(EMP.ID.CHK)
        IF CHK.LN NE '6' THEN
            E = "����� ����� ������� ���� ����"
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END

    CALL F.READ(FN.CU,EMP.ID,R.CU,F.CU,E1)
    IF E1 THEN
        E = '��� ������ ��� ����'
        CALL ERR ; MESSAGE = 'REPEAT'
    END

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

*R.NEW(HOLD.REG.VAL.PAY) = "NO"
*R.NEW(HOLD.DATE.RIGHT.TO) = TODAY
*R.NEW(HOLD.VALID.CCY) = LCCY


    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

*  IF AF = HOLD.RETURN.RIGHT.TO THEN

*   IF COMI < R.NEW(HOLD.DATE.RIGHT.TO) THEN

*  E = "UNEXPECTED"
*  CALL REBUILD.SCREEN ; P = 0

*  END

* END
*..................................
*   IF AF = HOLD.DATE.RIGHT.TO THEN

*   IF COMI > R.NEW(HOLD.RETURN.RIGHT.TO) THEN

*  E = "UNEXPECTED"
*  CALL REBUILD.SCREEN ; P = 0

*  END

* END
*..................................

* IF AF = HOLD.RETURN.RIGHT.FROM THEN

*    IF COMI < R.NEW(HOLD.DATE.RIGHT.FROM) THEN

*   E = "UNEXPECTED"
*   CALL REBUILD.SCREEN ; P = 0

*    END

*  END
    IF E THEN
        T.SEQU = "IFLD"
*Line [ 295 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************
DELIVERY.PREVIEW:

    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
    CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
    CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
*Line [ 429 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = "" ; MAT N = "" ; MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = ""

**    ID.F = "CUST.ID"; ID.N = "8"; ID.T  = ""
**    ID.F = "CUST_ID.DATEE"; ID.N = "14"; ID.T  = "A"
    ID.F  = "CUST.SER"; ID.N  = "25"; ID.T  = "A"
    Z=0
***********************************************************************
    Z+=1 ; F(Z)  = "CUSTOMER"       ;  N(Z) = "8"  ; T(Z) = "" ; T(Z) = ""
*Line [ 458 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:2:@FM:"L"
    Z+=1 ; F(Z)  = "REP.DATE"       ;  N(Z) = "8"  ; T(Z) = "D"
    Z+=1 ; F(Z)  = "BRANCH"         ;  N(Z) = "9"  ; T(Z) = "A" ; T(Z)<2> = ""
*Line [ 462 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "COMPANY":@FM:1:@FM:"L"
    Z+=1 ; F(Z)  = "CUST.ACCT"      ;  N(Z) = "16" ; T(Z) = "A"
*Line [ 465 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:5:@FM:"L"
    Z+=1 ; F(Z)  = "LATE.CODE"      ;  N(Z) = "4"  ; T(Z) = ""
*Line [ 468 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.VISA.CBE.LCODES":@FM:1
    Z+=1 ; F(Z)  = "CUS.BIRTH.DATE" ;  N(Z) = "8"  ; T(Z) = "D"
    Z+=1 ; F(Z)  = "CARD.TYPE"      ;  N(Z) = "2"   ; T(Z) = ""
*Line [ 472 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.CUS.ID.TYPE.CBE":@FM:1
    Z+=1 ; F(Z)  = "ID.NO"          ;  N(Z) = "35"  ; T(Z) = ""
    Z+=1 ; F(Z)  = "ID.ISS.DATE"    ;  N(Z) = "8"   ; T(Z) = "D"
    Z+=1 ; F(Z)  = "ID.EXP.DATE"    ;  N(Z) = "8"   ; T(Z) = "D"
    Z+=1 ; F(Z)  = "WORK.TEL"       ;  N(Z) = "16"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "HOME.TEL"       ;  N(Z) = "16"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "MOB.TEL"        ;  N(Z) = "16"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "CARD.ISS.DATE"  ;  N(Z) = "8"   ; T(Z) = "D"
    Z+=1 ; F(Z)  = "CARD.CURRENCY"  ;  N(Z) = "3"   ; T(Z) = "" ; T(Z) = "A"
    Z+=1 ; F(Z)  = "LIMIT"          ;  N(Z) = "19"  ; T(Z) = "AMT"
    Z+=1 ; F(Z)  = "STOP.DATE"      ;  N(Z) = "8"   ; T(Z) = "D"
    Z+=1 ; F(Z)  = "USED.LIMIT"     ;  N(Z) = "19"  ; T(Z) = "AMT"
    Z+=1 ; F(Z)  = "CARD.NO"        ;  N(Z) = "16"  ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CU.SEX"         ;  N(Z) = "10"  ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CU.NAME"        ;  N(Z) = "35"  ; T(Z) = "A"
    Z+=1 ; F(Z)  = "BIRTH.PLACE"    ;  N(Z) = "35"  ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CU.ADDRESS"     ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "JOB.DESC"       ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "ID.ISS.PLACE"   ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "EMAIL"          ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "CUSTOMER.TYPE"  ;  N(Z) = "10"  ; T(Z) = ""   ; T(Z)<2> = "����_����"
    Z+=1 ; F(Z)  = "CUSTOMER.STATE" ;  N(Z) = "20"  ; T(Z) = ""   ; T(Z)<3> = "������_�����_������� �������"
    Z+=1 ; F(Z)  = "LOAN.KIND"      ;  N(Z) = "2"  ; T(Z) = ""
*Line [ 496 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.LOAN.TYPE":@FM:1

    Z+=1 ; F(Z)  = "CUS.LEGAL.ACTION.M"  ;  N(Z) = "2"   ; T(Z) = ""
    Z+=1 ; F(Z)  = "CUS.LEGAL.ACTION.S"  ;  N(Z) = "2"   ; T(Z) = ""
    Z+=1 ; F(Z)  = "RESERVED1"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED2"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED3"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED4"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED5"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED6"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED7"           ;  N(Z) = "35"  ; T(Z) = "ANY"
    Z+=1 ; F(Z)  = "RESERVED8"           ;  N(Z) = "35"  ; T(Z) = "ANY"

***********************************************************************
    V = Z + 9

    RETURN
*************************************************************************
END
