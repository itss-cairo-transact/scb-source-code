* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
****************************NI7OOOOOOOOOOOOO***********************

    SUBROUTINE SCR.NOTICE.PR.DP.SIGHT.COLL


*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCR.NOTICE.PR.DP.SIGHT.COLL'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '����� �������� ���������':' - ':'SIGHT PAYMENT OF INWORD COLLECTIONS(CHARGE)'
*------------------------------------------------------------------------
*   DB.ACC = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
    DB.ACC = R.NEW(TF.DR.CHARGE.ACCOUNT)
    CR.ACC = R.NEW(TF.DR.PAYMENT.ACCOUNT)
    ACC.NO = DB.ACC
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
     CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
     ACC.BR2 = ACC.BR1[8,2]
     ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.ADDRESS=LOCAL.REF<1,CULR.ARABIC.ADDRESS>
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
    DR.AMOUNT    = R.NEW(TF.DR.DOCUMENT.AMOUNT)

*Line [ 93 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.CHARGE = DCOUNT(R.NEW(TF.DR.CHARGE.CODE),@VM)


* AMOUNT    = DR.AMOUNT+CHARGE.AMOUNT
*  IN.AMOUNT = CHARGE.AMOUNT
*  CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*  CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
*  OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
*    DAT  = R.NEW(TF.DR.VALUE.DATE)
    DAT  = R.NEW(TF.DR.CHARGE.DATE)
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    INPUTT = R.NEW(TF.DR.INPUTTER)
    AUTH.SON  = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH.SON,AUTH)
    INP   = FIELD(INPUTT,'_',2)
    AUTHI =  AUTH
*------------------------------------------------------------------------

    XX   = SPACE(132)  ; XX3  = SPACE(132)   ; XX10 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)   ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)   ; XX12 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX1<1,1>[3,35]  = CUST.NAME
    XX<1,1>[3,35]   = CUST.ADDRESS

* XX<1,1>[45,15]  = '������     : '
* XX<1,1>[59,15]  = CHARGE.AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

*   XX3<1,1>[45,15] = '������     : '
*   XX3<1,1>[59,15] = CHARGE.CURR

    XX4<1,1>[45,15] = '����� ������� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX6<1,1>[1,15]  = '������'
    XX6<1,1>[30,15] = '��� �������'
    XX6<1,1>[60,15] = '������'

    XX7<1,1>[1,15]  = INP:' �'
    XX7<1,1>[30,15] = ID.NEW:' �'
    XX7<1,1>[60,15] = AUTHI:' �'

*  XX8<1,1>[3,35]  = '������ ������� : '
*  XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������         : '
*            XX9<1,1>[20,15] = BR.DATA

*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT STR(' ',82)
*   PRINT XX3<1,1>
*   PRINT STR(' ',82)
*  PRINT XX8<1,1>
*  PRINT XX9<1,1>
*  PRINT XX5<1,1>
*  PRINT STR(' ',82)
*    PRINT XX<1,1>
    FOR I = 1 TO DECOUNT.CHARGE
        CHARGE.CODE      = R.NEW(TF.DR.CHARGE.CODE)<1,I>
        CHARGE.AMOUNT    = R.NEW(TF.DR.CHARGE.AMOUNT)<1,I>
        CHARGE.CURR      = R.NEW(TF.DR.CHARGE.CURRENCY)<1,I>
        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)

        IF ETEXT THEN
            TEXT = "ETEXT = " : ETEXT ; CALL REM
            CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
        END
        TOT = ''
* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
        IN.AMOUNT = R.NEW(TF.DR.CHARGE.AMOUNT)<1,I>

        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        TOT += CHARGE.AMOUNT
        IF IN.AMOUNT = '' THEN
            OUT.AMOUNT = ''
            OUT.AMT = ''
        END

*  TOT = 0 +  CHARGE.AMOUNT<1,I>

        XX10<1,1>[3,35]    = '�������  :'
        XX11<1,I>[20,35]   = CHARGE.CODE
        XX11<1,I>[40,35]   = CHARGE.NAME
        XX11<1,I>[55,35]   = CHARGE.AMOUNT
        XX11<1,I>[65,15]   = CUR

* XX8<1,1>[3,35]     = '������ ������� : '
        XX8<1,I>[20,15]    = OUT.AMT
       * XX12<1,1>[3,15]    ='������ ��������'
       * XX12<1,1>[28,15]   = TOT
        XX12<1,I>[60,15]   = CUR
        PRINT XX10<1,I>
        PRINT XX11<1,I>
        PRINT XX8<1,I>

    NEXT I
    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>

*===============================================================
    RETURN
END
