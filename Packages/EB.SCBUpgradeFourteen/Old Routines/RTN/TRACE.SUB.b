* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>3614</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
    SUBROUTINE TRACE.SUB( DL.FILE.NAME, NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, ITEM.RELATION)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DL.DEFINE

** ----- ----- -----
MAIN:
** ----- ----- -----

    NEW.ITEM = ( DL.FILE.NAME : '|' : FIELD( DL.RECORD.NAME, '!', 1, 2) )

    MUTUAL = 1
    LOCATE NEW.ITEM IN ITEM.OWNER< 1> SETTING MY.DUMMY ELSE MUTUAL = 0

** ----- ----- -----

    IF MUTUAL THEN IF.MUTUAL = ' ( MUTUAL RELATION )' ELSE IF.MUTUAL = ''
    REPORT.DESC = DL.FILE.NAME : IF.MUTUAL : ' ---> ' : FIELD( DL.RECORD.NAME, '!', 1)

    FIELD.LEVEL.ITEM = COUNT( DL.RECORD.NAME, '!')
    IF FIELD.LEVEL.ITEM THEN

        REPORT.DESC = '!!! Manually : ' : REPORT.DESC

        DL.FIELD = FIELD( DL.RECORD.NAME, '!', 2)
        IF DL.FIELD THEN IF.CORE = DL.FIELD ELSE IF.CORE = 'CORE FIELDS'
        IF DL.FIELD # '.' THEN IF.DL.FIELD = ' / ' : IF.CORE ELSE IF.DL.FIELD = ''
        IF ITEM.RELATION THEN IF.ITEM.RELATION = ' ( ':ITEM.RELATION:' )' ELSE IF.ITEM.RELATION = ''
        REPORT.DESC := IF.DL.FIELD : IF.ITEM.RELATION

    END

    GLOBUS.ROUTINE = 0
    IF DL.FILE.NAME = 'ROUTINE' THEN

        OPENSEQ 'GLOBUS.BP',DL.RECORD.NAME TO FILE THEN GLOBUS.ROUTINE = 1 ; CLOSESEQ FILE ; REPORT.DESC := ' ( CORE ROUTINE )'

    END

** ----- ----- -----

    IF NEST.LEVEL = 1 THEN CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '')
    CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, REPORT.DESC)

** ----- ----- -----

    IF NOT( MUTUAL) AND NOT( GLOBUS.ROUTINE) THEN

        IF R.DL.DEFINE AND NOT( FIELD.LEVEL.ITEM) THEN

            IF DL.FILE.NAME # 'ROUTINE' AND DL.FILE.NAME # 'TEMPLATE' THEN DL.FILE.NAME.NEW = DL.FILE.NAME ELSE DL.FILE.NAME.NEW = ROUTINE.DIR

            IDX.LE = -1 ; IDX.EQ = 0
            DL.ITEM.NO = DCOUNT( R.DL.DEFINE< DL.DEF.FILE.NAME>, @VM)
            FOR IDX = 1 TO DL.ITEM.NO

                IF R.DL.DEFINE< DL.DEF.FILE.NAME, IDX> = DL.FILE.NAME.NEW AND R.DL.DEFINE< DL.DEF.RECORD.NAME, IDX> = DL.RECORD.NAME THEN IDX.EQ = IDX ; EXIT
                IF IDX.LE < 0 AND FIELD( R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX, 1>, '|', 1) <= NEST.LEVEL THEN IDX.LE = IDX

            NEXT IDX

            IF ITEM.RELATION THEN IF.ITEM.RELATION = '/' : ITEM.RELATION ELSE IF.ITEM.RELATION = ''
            DL.RECORD.DESC = FMT( NEST.LEVEL, 'R%3') : ' | ' : ( FIELD( ITEM.OWNER< 1>, '|', 2) : IF.ITEM.RELATION )

            IF IDX.EQ THEN

                IF IDX.LE < 0 THEN

*Line [ 93 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    IDX.@SM.LE = -1 ; IDX.@SM.EQ = 0
                    DL.DESC.NO = DCOUNT( R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ>, @SM)
*Line [ 96 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    FOR IDX.@SM = 1 TO DL.DESC.NO

*Line [ 99 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        IF FIELD( R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ, IDX.@SM>, '|', 2) = FIELD( DL.RECORD.DESC, '|', 2) THEN IDX.@SM.EQ = IDX.@SM ; EXIT
*Line [ 101 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        IF IDX.@SM.LE < 0 AND FIELD( R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ, IDX.@SM>, '|', 1) <= NEST.LEVEL THEN IDX.@SM.LE = IDX.@SM

*Line [ 104 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    NEXT IDX.@SM

*Line [ 107 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    IF IDX.@SM.EQ AND IDX.@SM.LE > 0 THEN DEL R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ, IDX.@SM.EQ>
*Line [ 109 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    IF NOT( IDX.@SM.EQ) OR IDX.@SM.LE > 0 THEN INS DL.RECORD.DESC BEFORE R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ, IDX.@SM.LE>

                END ELSE

                    DL.DESC.NO = DCOUNT( R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ>, @SM)
*Line [ 115 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    FOR IDX.@SM = 1 TO DL.DESC.NO

*Line [ 118 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        IF FIELD( R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ, IDX.@SM>, '|', 2) = FIELD( DL.RECORD.DESC, '|', 2) THEN DEL R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ, IDX.@SM> ; EXIT

*Line [ 121 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    NEXT IDX.@SM

                    IF R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ> THEN DL.RECORD.DESC := ( @SM : R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ> )

                    DEL R.DL.DEFINE< DL.DEF.FILE.NAME, IDX.EQ>
                    DEL R.DL.DEFINE< DL.DEF.RECORD.NAME, IDX.EQ>
                    DEL R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.EQ>

                END

            END

            IF NOT( IDX.EQ) OR IDX.LE > 0 THEN

                INS DL.FILE.NAME.NEW BEFORE R.DL.DEFINE< DL.DEF.FILE.NAME, IDX.LE>
                INS DL.RECORD.NAME BEFORE R.DL.DEFINE< DL.DEF.RECORD.NAME, IDX.LE>
                INS DL.RECORD.DESC BEFORE R.DL.DEFINE< DL.DEF.RECORD.DESC, IDX.LE>

            END

            IF NOT( IDX.EQ) THEN

                IDX.PRECEDING.EQ = 0
                DL.PRECEDING.ITEM.NO = DCOUNT( R.DL.PRECEDING< DL.DEF.FILE.NAME>, @VM)
                FOR IDX = 1 TO DL.PRECEDING.ITEM.NO

                    IF R.DL.PRECEDING< DL.DEF.FILE.NAME, IDX> = DL.FILE.NAME.NEW AND R.DL.PRECEDING< DL.DEF.RECORD.NAME, IDX> = DL.RECORD.NAME THEN IDX.PRECEDING.EQ = IDX ; EXIT

                NEXT IDX

                IF NOT( IDX.PRECEDING.EQ) THEN

                    INS DL.FILE.NAME.NEW BEFORE R.DL.UPDATE< DL.DEF.FILE.NAME, -1>
                    INS DL.RECORD.NAME BEFORE R.DL.UPDATE< DL.DEF.RECORD.NAME, -1>

                END

            END

        END

        IF NESTED.TRACE = 'ALL' OR ( NESTED.TRACE = 'ONE' AND NEST.LEVEL <= 1 ) THEN

            ETEXT = ''
            IF DL.FILE.NAME # 'ROUTINE' AND DL.FILE.NAME # 'TEMPLATE' THEN

                FN.ITEM.TABLE = 'F.' : DL.FILE.NAME ; F.ITEM.TABLE = ''
                CALL OPF( FN.ITEM.TABLE, F.ITEM.TABLE)
                CALL F.READ( FN.ITEM.TABLE, FIELD( DL.RECORD.NAME, '!', 1), R.ITEM, F.ITEM.TABLE, ETEXT)

            END

            IF ETEXT THEN

                E = ETEXT ; ETEXT = '' ; CALL TXT( E)
                CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

            END ELSE

                NEW.ITEM.OWNER = NEW.ITEM : @FM : ITEM.OWNER

                BEGIN CASE

                CASE DL.FILE.NAME = 'VERSION'

*Line [ 187 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.TRACE.VERSION.RUN( NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, NEW.ITEM.OWNER, R.ITEM)

                CASE DL.FILE.NAME = 'ENQUIRY'

*Line [ 192 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.TRACE.ENQUIRY.RUN( NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, NEW.ITEM.OWNER, R.ITEM)

                CASE DL.FILE.NAME = 'STANDARD.SELECTION'

*Line [ 197 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.TRACE.SS.RUN( NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, NEW.ITEM.OWNER, R.ITEM)

                CASE DL.FILE.NAME = 'LOCAL.REF.TABLE'

*Line [ 202 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.TRACE.LRT.RUN( NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, NEW.ITEM.OWNER, R.ITEM)

                CASE DL.FILE.NAME = 'ROUTINE'

*Line [ 207 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.TRACE.ROUTINE.RUN( NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, NEW.ITEM.OWNER)

                CASE DL.FILE.NAME = 'TEMPLATE'

*Line [ 212 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.TRACE.TEMPLATE.RUN( NEST.LEVEL, DL.RECORD.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, NEW.ITEM.OWNER)

                END CASE

            END

        END

    END

    RETURN

** ----- ----- -----

END
