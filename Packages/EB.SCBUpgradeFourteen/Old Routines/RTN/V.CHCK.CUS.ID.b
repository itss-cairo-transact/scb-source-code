* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE V.CHCK.CUS.ID

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*    $INSERT I_F.ENQUIRY

*--------------------------------------
    FN.AC = 'FBNK.CUSTOMER' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)
*--------------------------------------
    IF COMI THEN    ;* ID.NEW would not have been set now
        ACCT.ID = COMI
        CUS.ID  = ACCT.ID[1,8]
        IF ACCT.ID[1,1] EQ 0 THEN CUS.ID = ACCT.ID[2,7]
*    TEXT = CUS.ID ; CALL REM
        CALL F.READ(FN.AC,CUS.ID,R.AC,F.AC,E2)
        SECTOR = R.AC<EB.CUS.SECTOR>
        IF SECTOR NE 1100 AND SECTOR NE 1300 AND SECTOR NE 2000 THEN
            E = "SECTOR NOT ALLOWED"
*Line [ 39 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.ERR
            MESSAGE = 'REPEAT'
            V$ERROR = 1
        END
    END
    RETURN
END
