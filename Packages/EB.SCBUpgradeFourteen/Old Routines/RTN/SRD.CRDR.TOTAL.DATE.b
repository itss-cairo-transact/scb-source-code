* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*** CREATED BY KHALED ***
***  ��� ������ ������� ������� ��������   ***
***=================================

    SUBROUTINE SRD.CRDR.TOTAL.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 46 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SRD.CRDR.TOTAL'
    CALL PRINTER.ON(REPORT.ID,'')

    XX  = SPACE(120) ; XX1 = SPACE(120)
    XX2 = SPACE(120) ; XX3 = SPACE(120)
    XX4 = SPACE(120) ; XX5 = SPACE(120)
    XX6 = SPACE(120) ; XX7 = SPACE(120)

    YTEXT = "Enter Date. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    TD1 = COMI

    COMP = ID.COMPANY
    RETURN
*===============================================================
CALLDB:
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ;  F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    CR.MOV   = 0  ; CR.MOV1  = 0 ; CR.MOV2  = 0 ; CR.MOV3  = 0 ; CR.MOV4 = 0
    CR.MOV11 = 0  ; CR.MOV21 = 0 ; CR.MOV31 = 0 ; CR.MOV41 = 0
    DB.MOV   = 0  ; DB.MOV1  = 0 ; DB.MOV2  = 0 ; DB.MOV3  = 0 ; DB.MOV4 = 0
    DB.MOV11 = 0  ; DB.MOV21 = 0 ; DB.MOV31 = 0 ; DB.MOV41 = 0

** YTEXT = "Enter Date. : "
** CALL TXTINP(YTEXT, 8, 22, "16", "A")
** TD1 = COMI

    T.SEL  = "SELECT ":FN.LN:" WITH @ID LIKE GENLED.... AND @ID UNLIKE GENLED.9990 AND @ID UNLIKE GENLED.9993 AND @ID UNLIKE GENLED.9994 AND @ID UNLIKE GENLED.9995 BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,2,2>
            IF Y.DESC EQ '' THEN
                Y.DESC = R.LN<RE.SRL.DESC,2,1>
            END
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
*************************************************************************
            FOR X = 1 TO POS
                BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":COMP
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                RATE = R.CCY<RE.BCP.RATE,X>
                ID2 = DESC.ID
                IF ID2 LT 1010 THEN
                    CUR.ID = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                    DB.MOV = R.BAL<RE.SLB.DB.MOVEMENT>
                    CR.MOV = R.BAL<RE.SLB.CR.MOVEMENT>
                    IF DB.MOV NE '' OR CR.MOV NE '' THEN
                        XX<1,I>[1,35]   = Y.DESC
                        XX<1,I>[45,15]  = DB.MOV
                        XX<1,I>[75,15]  = CR.MOV
                        XX<1,I>[105,15] = CUR.ID
                        PRINT XX<1,I>
                        PRINT STR('-',120)
                        XX = ''
                        IF ID2 GE 0020 AND ID2 LE 0120 THEN
                            DB.MOV1 += R.BAL<RE.SLB.DB.MOVEMENT>
                            CR.MOV1 += R.BAL<RE.SLB.CR.MOVEMENT>
                        END
                        IF ID2 GE 0510 AND ID2 LE 0630 THEN
                            DB.MOV2 += R.BAL<RE.SLB.DB.MOVEMENT>
                            CR.MOV2 += R.BAL<RE.SLB.CR.MOVEMENT>
                        END
                        IF ID2 GE 0220 AND ID2 LE 0320 THEN
                            DB.MOV31 = R.BAL<RE.SLB.DB.MOVEMENT>
                            CR.MOV31 = R.BAL<RE.SLB.CR.MOVEMENT>

                            DB.MOV3 += DB.MOV31 * RATE
                            DB.MOV3 = DROUND(DB.MOV3,'2')

                            CR.MOV3 += CR.MOV31 * RATE
                            CR.MOV3 = DROUND(CR.MOV3,'2')
                        END
                        IF ID2 GE 0710 AND ID2 LE 0840 THEN
                            DB.MOV41 = R.BAL<RE.SLB.DB.MOVEMENT>
                            CR.MOV41 = R.BAL<RE.SLB.CR.MOVEMENT>

                            DB.MOV4 += DB.MOV41 * RATE
                            DB.MOV4 = DROUND(DB.MOV4,'2')

                            CR.MOV4 += CR.MOV41 * RATE
                            CR.MOV4 = DROUND(CR.MOV4,'2')
                        END
                    END
                END
            NEXT X
        NEXT I
        TOT.DB1 = DB.MOV1 + DB.MOV2
        TOT.CR1 = CR.MOV1 + CR.MOV2
        TOT.DB2 = DB.MOV3 + DB.MOV4
        TOT.CR2 = CR.MOV3 + CR.MOV4
        TOT1 = TOT.DB1 + TOT.DB2
        TOT2 = TOT.CR1 + TOT.CR2

        XX1<1,1>[1,35]  = "����� ������ ����"
        XX1<1,1>[45,15] = DB.MOV1
        XX1<1,1>[75,15] = CR.MOV1
        XX2<1,1>[1,35]  = "����� ������ ����"
        XX2<1,1>[45,15] = DB.MOV2
        XX2<1,1>[75,15] = CR.MOV2
        XX3<1,1>[1,35]  = "����� ������ �����"
        XX3<1,1>[45,15] = DB.MOV3
        XX3<1,1>[75,15] = CR.MOV3
        XX4<1,1>[1,35]  = "����� ������ �����"
        XX4<1,1>[45,15] = DB.MOV4
        XX4<1,1>[75,15] = CR.MOV4

        XX5<1,1>[1,35]  = "����� ������"
        XX5<1,1>[45,15] = TOT.DB1
        XX5<1,1>[75,15] = TOT.CR1

        XX6<1,1>[1,35]  = "����� �������"
        XX6<1,1>[45,15] = TOT.DB2
        XX6<1,1>[75,15] = TOT.CR2

        XX7<1,1>[1,35]  = "������� �����"
        XX7<1,1>[45,15] = TOT1
        XX7<1,1>[75,15] = TOT2


        PRINT STR(' ',120)
        PRINT STR(' ',120)
        PRINT XX1<1,1>
        PRINT XX2<1,1>
        PRINT STR('=',120)
        PRINT XX5<1,1>
        PRINT STR(' ',120)
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT STR('=',120)
        PRINT XX6<1,1>
        PRINT STR('=',120)
        PRINT STR(' ',120)
        PRINT XX7<1,1>

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TD = TD1
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]
    YYBRN  = BRANCH
    DATY   = TODAY

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"��� ������ ������� ������� �������� : ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������" :SPACE(35):"������ �������":SPACE(15):"������ �������":SPACE(15):"������"
    PR.HD :="'L'":STR('_',120)
**    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
