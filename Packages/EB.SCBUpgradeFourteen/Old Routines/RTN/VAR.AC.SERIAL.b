* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
** ----- 13.06.2002 BAKRY SCB -----

    SUBROUTINE VAR.AC.SERIAL
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.SERIAL

* IF V$FUNCTION ='I' THEN
    KEY.LIST = '' ; SELECTED.NO = '' ; ER.MSG = '' ; ETEXT = '';NEW.SER = '' ;SER.ID=''
    FN.SCB.ACCT.SERIAL = 'F.SCB.ACCT.SERIAL' ; F.SCB.ACCT.SERIAL = '' ; R.SCB.ACCT.SERIAL = ''
    LEN.ID = LEN(ID.NEW)
    BASE.ID = ID.NEW[1,LEN.ID-2]


    CALL DBR('SCB.ACCT.SERIAL':@FM:SCB.AC.SER.SERIAL.NO,BASE.ID,SER.ID)
**    TEXT = 'SER.ID=':SER.ID ; CALL REM
    IF ETEXT THEN
        NEW.SER = 1
    END ELSE
        NEW.SER = SER.ID + 1
    END

    R.SCB.ACCT.SERIAL<SCB.AC.SER.SERIAL.NO> = NEW.SER
    R.SCB.ACCT.SERIAL<SCB.AC.SER.INPUTTER> = OPERATOR
    CALL OPF( FN.SCB.ACCT.SERIAL,F.SCB.ACCT.SERIAL)
  **  TEXT = 'BASE.ID=':BASE.ID ; CALL REM
    CALL F.WRITE(FN.SCB.ACCT.SERIAL,BASE.ID,R.SCB.ACCT.SERIAL)
*CALL JOURNAL.UPDATE(BASE.ID)
* END
    CLOSE F.SCB.ACCT.SERIAL
    RETURN
END
