* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCR.LC.DR.ALL
** PROGRAM SCR.LC.DR.ALL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB CREATE.HEAD
*Line [ 46 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*==============================================================
INITIATE:
    REPORT.ID    = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')


    CUST.NAME   = ''; LC.NU = '' ; OLD.LC.NU = '' ;  LC.CUR  = '';CUR.DESC=''
    LC.AMT = '' ; LIAB.AMT         = ''; TD1 = TODAY

    RETURN

*===============================================================
CALLDB:
    FN.LC  = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = '';R.LC1=''
    FN.DRW = 'FBNK.DRAWINGS' ; F.DRW = '' ; R.DRW = ''
    FN.CU  = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    FN.CUR = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''

    CALL OPF(FN.CUR,F.CUR)
    CALL OPF (FN.CU,F.CU)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DRW,F.DRW)

    KEY.LIST       = ""
    SELECTED       = ""
    ER.MSG         = ""
    LIB.AMT.CON    = '0'
    LIB.AMT.UNCON  = '0'
    DRW.AMT.ALL    = '0'
    LC.CON         = '0'
    LC.UNCON       = '0'
    LC.CON.ALL     = '0'
    LC.UNCON.ALL   = '0'
    LC.CON.END     = '0'
    LC.UNCON.END   = '0'
    LC.CON.I       = '0'
    LC.UNCON.I     = '0'
    LC.CON.SHOW    = '0'
    LC.UNCON.SHOW  = '0'
    LIB.AMT.CON.I  = '0'
    LIB.AMT.UNCON.I= '0'

    AMT.CON.ALL    = '0'
    AMT.UNCON.ALL  = '0'

    AMT.CON.EQU    = '0'
    AMT.UNCON.EQU  = '0'

    LIB.AMT.CON.I.EQU = '0'
    LIB.AMT.UNCON.I.EQU = '0'

    ALL.SHOW = '0'
    AMT.ALL.SHOW ='0'

    LIB.AMT.CON.I.EQU.SH = '0'
    LIB.AMT.UNCON.I.EQU.SH = '0'
    AMT.CON.EQU.SH = '0'
    AMT.UNCON.EQU.SH = '0'

    LIB.AMT.CON.SH = '0'
    LIB.AMT.UNCON.SH = '0'
    LIB.AMT.CON.SHO = '0'
    LIB.AMT.UNCON.SHO = '0'

    XXX='';  XXX1 = '';XXX2 ='';XXX3='';XXX4 ='';XXX5='';XXX6=''
*========================================================================
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... BY LC.CURRENCY BY LC.TYPE "
    CALL EB.READLIST(T.SEL, KEY.LIST,"", SELECTED, ASD)
*************
    XXX<1,1>[1,5]     = '������'
    XXX<1,1>[20,5]    = '��� ����'
    XXX<1,1>[30,5]    = '������ ���� ����'
    XXX<1,1>[50,5]    = '������ ���� ����'
    XXX<1,1>[68,5]    = '��� ��� ����'
    XXX<1,1>[81,5]    = '������ ���� ��� ����'
    XXX<1,1>[103,5]   ='������ ���� ���� ��� ����'
    PRINT XXX
    PRINT ''
    PRINT ''
**************
    FOR I = 1 TO SELECTED
        XXX1<1,1>=''
        LC.ID =KEY.LIST<I>
        LC.ID1=KEY.LIST<I+1>
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)
        CALL F.READ(FN.LC,LC.ID1,R.LC1,F.LC,ERR1)
        LC.TYP             =  R.LC<TF.LC.LC.TYPE>
        LC.CUR             =  R.LC<TF.LC.LC.CURRENCY>
        LC.CUR1            =  R.LC1<TF.LC.LC.CURRENCY>
************
        IF LC.CUR EQ LC.CUR1 THEN
            IF LC.TYP EQ 'LEDC' THEN
                GOSUB GET.DR
                IF DR.FLG EQ '1' OR R.LC<TF.LC.LIABILITY.AMT> GT '0' THEN
                    LIB.AMT.CON.ALL = DRW.AMT.ALL + R.LC<TF.LC.LIABILITY.AMT>
                    LIB.AMT.CON     = LIB.AMT.CON + LIB.AMT.CON.ALL
                    LC.CON          = LC.CON + 1
                END
            END ELSE
                IF LC.TYP EQ 'LEDU' THEN
                    GOSUB GET.DR
                    IF DR.FLG EQ '1' OR R.LC<TF.LC.LIABILITY.AMT> GT '0' THEN
                        LIB.AMT.UNCON.ALL  = DRW.AMT.ALL + R.LC<TF.LC.LIABILITY.AMT>
                        LIB.AMT.UNCON      = LIB.AMT.UNCON+ LIB.AMT.UNCON.ALL
                        LC.UNCON           = LC.UNCON + 1
                    END
                END
            END
            IF I EQ SELECTED THEN
                GOSUB GET.RATE
                XXX1<1,1>[1,5]     = CUR.DESC
                XXX1<1,1>[20,5]    = LC.CON
                LIB.AMT.CON.SH=LIB.AMT.CON
                LIB.AMT.CON.SH=FMT(LIB.AMT.CON.SH,"L2,")
                XXX1<1,1>[30,5]    = LIB.AMT.CON.SH

                LC.CON.I           = LC.CON
                LIB.AMT.CON.I      = LIB.AMT.CON
***************
                LIB.AMT.CON.I.EQU  =  LIB.AMT.CON.I * EQV.RAT
                LIB.AMT.CON.I.EQU.SH = LIB.AMT.CON.I.EQU
                LIB.AMT.CON.I.EQU.SH=FMT(LIB.AMT.CON.I.EQU.SH,"L2,")
***************

                XXX1<1,1>[50,5]    = LIB.AMT.CON.I.EQU.SH
                XXX1<1,1>[70,5]    = LC.UNCON
                LIB.AMT.UNCON.SH   = LIB.AMT.UNCON
                LIB.AMT.UNCON.SH   = FMT(LIB.AMT.UNCON.SH,"L2,")
                XXX1<1,1>[82,5]    = LIB.AMT.UNCON.SH

                LC.UNCON.I=LC.UNCON
                LIB.AMT.UNCON.I=LIB.AMT.UNCON
******************
                LIB.AMT.UNCON.I.EQU = LIB.AMT.UNCON.I * EQV.RAT
                LIB.AMT.UNCON.I.EQU.SH=LIB.AMT.UNCON.I.EQU
                LIB.AMT.UNCON.I.EQU.SH=FMT(LIB.AMT.UNCON.I.EQU.SH,"L2,")
******************
                XXX1<1,1>[103,5]   = LIB.AMT.UNCON.I.EQU.SH

                PRINT XXX1
                XXX1<1,1> = ''
            END
        END  ELSE
            IF LC.TYP EQ 'LEDC' THEN
                GOSUB GET.DR
                IF DR.FLG EQ '1' OR R.LC<TF.LC.LIABILITY.AMT> GT '0' THEN
                    LIB.AMT.CON.ALL     = DRW.AMT.ALL + R.LC<TF.LC.LIABILITY.AMT>
                    LIB.AMT.CON         = LIB.AMT.CON + LIB.AMT.CON.ALL
                    LC.CON              = LC.CON + 1
                END
            END ELSE
                IF LC.TYP EQ 'LEDU' THEN
                    GOSUB GET.DR
                    IF DR.FLG EQ '1' OR R.LC<TF.LC.LIABILITY.AMT> GT '0' THEN
                        LIB.AMT.UNCON.ALL  = DRW.AMT.ALL + R.LC<TF.LC.LIABILITY.AMT>
                        LIB.AMT.UNCON      = LIB.AMT.UNCON+ LIB.AMT.UNCON.ALL
                        LC.UNCON           = LC.UNCON + 1
                    END
                END
            END
            IF LIB.AMT.CON EQ '0' AND LIB.AMT.UNCON EQ '0' AND LC.CON EQ '0' AND LC.UNCON EQ '0' THEN
                XXX1 = ''
            END ELSE
                GOSUB GET.RATE
                XXX1<1,1>[1,5]     = CUR.DESC
                XXX1<1,1>[20,5]    = LC.CON
                LIB.AMT.CON.SHO     = LIB.AMT.CON
                LIB.AMT.CON.SHO     = FMT(LIB.AMT.CON.SHO,"L2,")
                XXX1<1,1>[30,5]     = LIB.AMT.CON.SHO
                AMT.CON.EQU         = LIB.AMT.CON * EQV.RAT
                AMT.CON.EQU.SH=AMT.CON.EQU
                AMT.CON.EQU.SH=FMT(AMT.CON.EQU.SH,"L2,")
                XXX1<1,1>[50,5]    = AMT.CON.EQU.SH
                LC.CON.ALL         = LC.CON.ALL + LC.CON
                AMT.CON.ALL        = AMT.CON.ALL + AMT.CON.EQU
*************
                XXX1<1,1>[70,5]    = LC.UNCON
                LIB.AMT.UNCON.SHO  = LIB.AMT.UNCON
                LIB.AMT.UNCON.SHO  = FMT(LIB.AMT.UNCON.SHO,"L2,")
                XXX1<1,1>[82,5]    = LIB.AMT.UNCON.SHO
                AMT.UNCON.EQU      = LIB.AMT.UNCON * EQV.RAT
                AMT.UNCON.EQU.SH   = AMT.UNCON.EQU
                AMT.UNCON.EQU.SH   = FMT(AMT.UNCON.EQU.SH,"L2,")
                XXX1<1,1>[103,5]    = AMT.UNCON.EQU.SH
                LC.UNCON.ALL       = LC.UNCON.ALL + LC.UNCON
                AMT.UNCON.ALL      = AMT.UNCON.ALL + AMT.UNCON.EQU
*************
                PRINT XXX1
                XXX1<1,1> = ''

                LC.CON   = '0'
                LC.UNCON = '0'

                LIB.AMT.CON   =  '0'
                LIB.AMT.UNCON =   '0'

                LC.UNCON = '0'
                LC.CON   = '0'

                DRW.AMT = ''
                DRW.AMT.ALL = ''

                AMT.CON.EQU = ''
                AMT.UNCON.EQU=''



            END
        END
    NEXT I

    LC.CON.SHOW = LC.CON.ALL + LC.CON.I
    LC.AMT.CON.SHOW= AMT.CON.ALL + LIB.AMT.CON.I.EQU
    LC.UNCON.SHOW     = LC.UNCON.ALL +LC.UNCON.I
    LC.AMT.UNCON.SHOW = AMT.UNCON.ALL + LIB.AMT.UNCON.I.EQU

    ALL.SHOW = LC.CON.SHOW + LC.UNCON.SHOW
    AMT.ALL.SHOW = LC.AMT.CON.SHOW + LC.AMT.UNCON.SHOW


    XXX2<1,1>[20,5]  = '���'
    XXX2<1,1>[40,5]  =  '������ ���� �����'

    XXX3<1,1>[1,5]  ="����"
    XXX3<1,1>[20,5] = LC.CON.SHOW
    LC.AMT.CON.SHOW=FMT(LC.AMT.CON.SHOW,"L2,")
    XXX3<1,1>[40,5] = LC.AMT.CON.SHOW


    XXX4<1,1>[1,5]  ='��� ����'
    XXX4<1,1>[20,5] = LC.UNCON.SHOW
    LC.AMT.UNCON.SHOW=FMT(LC.AMT.UNCON.SHOW,"L2,")
    XXX4<1,1>[40,5] = LC.AMT.UNCON.SHOW



    XXX5<1,1>[1,5] ='___________'
    XXX5<1,1>[20,5]= '________'
    XXX5<1,1>[40,5] ='____________'


    XXX6<1,1>[1,5] = '������ ����'
    XXX6<1,1>[20,5] =  ALL.SHOW
    AMT.ALL.SHOW=FMT(AMT.ALL.SHOW,"L2,")
    XXX6<1,1>[40,5] =  AMT.ALL.SHOW

    PRINT ''
    PRINT SPACE(25):'_______________________________'
    PRINT ''
    PRINT XXX2
    PRINT ''
    PRINT XXX3
    PRINT ''
    PRINT XXX4
    PRINT ''
    PRINT XXX5
    PRINT ''
    PRINT XXX6

    RETURN
************
GET.DR:
    FN.DRW = "FBNK.DRAWINGS"; F.DRW  = ""; R.DRW = ""
    CALL OPF( FN.DRW,F.DRW)
    DRW.AMT  = 0
    LC.NO    =  LC.ID

    DRW.NOO  = 30
    FOR JJ = 1 TO DRW.NOO -1
        IF JJ LT 10 THEN
            DRW.ID = LC.NO:"0":JJ
        END ELSE
            DRW.ID = LC.NO:JJ
        END
        CALL F.READ(FN.DRW,DRW.ID,R.DRW,F.DRW,ERR.DRW)
        IF R.DRW<TF.DR.DRAWING.TYPE> = "DP" THEN
            DRW.AMT = DRW.AMT + R.DRW<TF.DR.DOCUMENT.AMOUNT>
        END
    NEXT JJ
    DRW.AMT.ALL = DRW.AMT
    IF DRW.AMT.ALL GT '0' THEN
        DR.FLG = '1'
    END ELSE
        DR.FLG = '0'
    END
    RETURN
*************************************
GET.RATE:

    CALL F.READ(FN.CUR,LC.CUR,R.CUR,F.CUR,EEE)
    CUR.DESC = R.CUR<EB.CUR.CCY.NAME,2>
    CCY.MAR = '1'
    LOCATE CCY.MAR IN R.CUR<EB.CUR.CURRENCY.MARKET,1>  SETTING KK THEN
        EQV.RAT = R.CUR< EB.CUR.MID.REVAL.RATE,KK>
    END

    IF LC.CUR EQ 'EGP' THEN
        EQV.RAT = 1
    END

    IF LC.CUR EQ 'JPY' THEN
        EQV.RAT = (EQV.RAT / 100)
    END
    RETURN
***************************************
CREATE.HEAD:

    PR.HD  ="'L'":SPACE(25):"������ ���� ������� - ������� ����-���"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(25):STR('_',40)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

    RETURN
***************************************
END
