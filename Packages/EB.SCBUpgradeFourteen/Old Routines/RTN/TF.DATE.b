* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*----------------------------NI7OOOOOO-------------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TF.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS


    FN.CR='FBNK.STMT.ENTRY';F.CR=''
    CALL OPF(FN.CR,F.CR)
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS';F.LD=''
    CALL OPF(FN.LD,F.LD)
    FN.LDH='FBNK.LD.LOANS.AND.DEPOSITS$HIS';F.LDH=''
    CALL OPF(FN.LDH,F.LDH)
    FN.TT='FBNK.TELLER';F.TT=''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H='FBNK.TELLER$HIS';F.TT.H=''
    CALL OPF(FN.TT.H,F.TT.H)

    FN.LC='FBNK.LETTER.OF.CREDIT';F.LC=''
    CALL OPF(FN.LC,F.LC)
    FN.LC.H='FBNK.LETTER.OF.CREDIT$HIS';F.LC.H=''
    CALL OPF(FN.LC.H,F.LC.H)

    FN.DR='FBNK.DRAWINGS';F.DR=''
    CALL OPF(FN.DR,F.DR)
    FN.DR.H='FBNK.DRAWINGS$HIS';F.DR.H=''
    CALL OPF(FN.DR.H,F.DR.H)

    XX  = O.DATA
    IF XX[1,2] EQ 'TF' THEN
        T.SEL = "SELECT FBNK.DRAWINGS WITH @ID EQ ":XX
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CALL F.READ(FN.DR,XX,R.DR,F.DR,E2)
            DATE.DR   = R.DR<TF.DR.MATURITY.REVIEW>
            DATE.DB   = R.DR<TF.DR.DEBIT.VALUE>
            DATE.ISS  = R.DR<TF.DR.VALUE.DATE>
            IF DATE.DR NE '' THEN
                O.DATA = DATE.DR
            END
            IF DATE.DR EQ '' THEN
                O.DATA = DATE.DB
            END
            IF DATE.DR EQ '' AND  DATE.DB EQ '' THEN
                O.DATA = DATE.ISS
            END
        END ELSE
            YY = O.DATA:";1"
            CALL F.READ(FN.DR.H,YY,R.DR.H,F.DR.H,E3)
            DATE.DR.H   = R.DR.H<TF.DR.MATURITY.REVIEW>
            DATE.DB.H   = R.DR.H<TF.DR.DEBIT.VALUE>
            DATE.ISS.H  = R.DR.H<TF.DR.VALUE.DATE>
            IF DATE.DR.H NE '' THEN
                O.DATA = DATE.DR.H
            END
            IF DATE.DR.H EQ '' THEN
                O.DATA = DATE.DB.H
            END
            IF DATE.DR.H EQ '' AND  DATE.DB.H EQ '' THEN
                O.DATA = DATE.ISS.H
            END

        END
    END
    IF XX[1,2] EQ 'FT' THEN
        ZZ = O.DATA:";1"
*TEXT = ZZ ; CALL REM
        CALL F.READ(FN.FT.HIS,ZZ,R.FT.H,F.FT.HIS,E3)
        V.DAT = R.FT.H<FT.PROCESSING.DATE>
        O.DATA = V.DAT
    END

    RETURN
END
