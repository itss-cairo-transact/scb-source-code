* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNjIyNTg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>47</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TEL.CUST.SERVICE.ADD

*NEW SUBROUTINE CREATED 10-03-2009
* NOHA HAMED

*-----------------------------------
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.SERVICE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    PROMPT ''

    GOSUB OPEN.FILES
    EOF = ''

    LOOP WHILE NOT(EOF)
        GOSUB READ.TXT.FILE
    REPEAT
    CLOSESEQ VAR.FILE
    GOTO PROGRAM.END
**********************************************
OPEN.FILES:
    F.CUST = "F.SCB.CUST.TEL.SERVICE"; FVAR.CUST=''
*CALL OPF(F.CUST,FVAR.CUST)

*OPEN F.CUST TO FVAR.CUST ELSE
*   TEXT = "ERROR OPEN FILE" ; CALL REM
*  RETURN
*END
*UPDATED BY NOHA HAMED 21/3/2016
    CALL OPF(F.CUST,FVAR.CUST)
    DIR.NAME = "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&"
    TEXT.FILE = "SUZ_Cairo_Alex.txt"
    VAR.FILE = ''

    OPENSEQ DIR.NAME,TEXT.FILE TO VAR.FILE ELSE
        TEXT = "ERROR OPEN FILE ":TEXT.FILE ; CALL REM
        RETURN
    END

    RETURN
**********************************************
READ.TXT.FILE:
    R.LINE = '' ; R.CUST = ''; T.SEL='';KEY.LIST=''; SELECTED=0
    UPDATE.FLAG = ''; ERR=''; ER=''; L.LIST=''; SELECT.NO=0; Y.SEL=''
    ZEROS=''; NO.OF.RECORDS=0; NO.OF.REC=0; Z=''

    READSEQ R.LINE  FROM VAR.FILE THEN
        R.CUST<TEL.IRB.CUST.REF>      = TRIM(FIELD(R.LINE,";",1))
        R.CUST<TEL.IRB.ACCT.NO>       = TRIM(FIELD(R.LINE,";",2))
        R.CUST<TEL.CUST.PHONES,1>     = TRIM(FIELD(R.LINE,";",3))
        R.CUST<TEL.CUST.NO>           = TRIM(FIELD(R.LINE,";",4))
        R.CUST<TEL.TRN.CODE>          = TRIM(FIELD(R.LINE,";",5))
        R.CUST<TEL.MANDATE.CODE>      = 'Y'
        R.CUST<TEL.ACTIVE.FROM>       = TODAY
        R.CUST<TEL.CUST.BANK.CODE>    = '995'
        R.CUST<TEL.CURR.NO>           = '1'
        R.CUST<TEL.RECORD.STATUS>     = 'IHLD'
        R.CUST<TEL.INPUTTER>          = 'RTN.CUST.CORR'

        CUS.NO                        = R.CUST<TEL.CUST.NO>


*********************TO GET BRANCH CODE*****************
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.DEPT.CODE,CUS.NO,DEPTCODE)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
DEPTCODE=R.ITSS.CUSTOMER<EB.CUS.DEPT.CODE>
        R.CUST<TEL.BRAN.NO>           = DEPTCODE

*********************TO GET MANDATE REF*****************
        COMP.CODE=ID.COMPANY
        COMP= COMP.CODE[7,3]
        T.SEL="SELECT F.SCB.CUST.TEL.SERVICE"
        CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)
        NO.OF.RECORDS=SELECTED+1
        MANDATE.REF='SUZ':COMP
        LEN.NO=LEN(NO.OF.RECORDS)
        FOR I=LEN.NO TO 13
            ZEROS=ZEROS:'0'
        NEXT I
        MANDATE.REF=MANDATE.REF:ZEROS:NO.OF.RECORDS
        R.CUST<TEL.MANDATE.REF>           = MANDATE.REF
        R.CUST<TEL.CO.CODE>               = COMP.CODE

**********************TO GET @ID*********************************
        Y.SEL="SELECT F.SCB.CUST.TEL.SERVICE WITH @ID LIKE ":CUS.NO:"..."
        CALL EB.READLIST(Y.SEL,L.LIST,'',SELECT.NO,ER)
        NO.OF.REC=SELECT.NO + 1
        LEN.NO=LEN(NO.OF.REC)
        FOR I=LEN.NO TO 2
            Z=Z:'0'
        NEXT I
        SERIAL.NO=Z:NO.OF.REC
        CUST.SERIAL = CUS.NO:'.':SERIAL.NO

*****************************************************************
        UPDATE.FLAG = 'YES'
        GOSUB WRITE.CUST.TO.HOLD
    END ELSE
        EOF = 'END OF FILE'
    END


    RETURN
**************************************************************
WRITE.CUST.TO.HOLD:
    IF UPDATE.FLAG = 'YES' THEN
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**        WRITE R.CUST TO FVAR.CUST , CUST.SERIAL  ON ERROR
**          STOP 'CAN NOT WRITE RECORD ':CUST.SERIAL:' TO FILE ':F.CUST
**    END
        CALL F.WRITE(F.CUST,CUST.SERIAL,R.CUST)
        CALL JOURNAL.UPDATE(CUST.SERIAL)
**********
    END


    RETURN
**************************************************************
PROGRAM.END:
END
