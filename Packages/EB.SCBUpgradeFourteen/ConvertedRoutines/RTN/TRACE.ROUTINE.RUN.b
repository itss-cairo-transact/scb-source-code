* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzQxNzc6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2650</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE TRACE.ROUTINE.RUN( NEST.LEVEL, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

** ----- ----- -----
MAIN:
** ----- ----- -----

      ** ----- routine code -----

      SELECT.STATEMENT = 'SELECT ' : ROUTINE.DIR
      CALL EB.READLIST( SELECT.STATEMENT, ROUTINE.LIST, '', SELECTED, SYSTEM.RETURN.CODE)

      IF SYSTEM.RETURN.CODE < 0 THEN

         E = 'ERROR READING ROUTINE LIST (&)' : @FM : SELECT.STATEMENT ; CALL TXT( E) ; CALL ERR
         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

      END ELSE

         CONNECTED.ROUTINES = ''

         OPENSEQ ROUTINE.DIR,ITEM.NAME TO FILE
         THEN

            LINE.NO = 0

            FILE.EOF = 0
            LOOP WHILE NOT( FILE.EOF)

               INPUT.LINE = ''
               READSEQ INPUT.LINE FROM FILE
               THEN

                  LINE.NO += 1

                  IN.SQUOTES = 0
                  IN.DQUOTES = 0

                  STATEMENT.NO = 0
                  STATEMENT.START = 1

                  LINE.LEN = LEN( INPUT.LINE)
                  FOR LINE.IDX = 1 TO LINE.LEN

                     CURRENT.CHAR = INPUT.LINE[ LINE.IDX, 1]

                     IF LINE.IDX = STATEMENT.START AND TRIMF( INPUT.LINE[ LINE.IDX, LINE.LEN - LINE.IDX + 1])[ 1, 1] = '*' THEN EXIT
                     ELSE

                        BEGIN CASE

                           CASE CURRENT.CHAR = "'" ; IF NOT( IN.DQUOTES) THEN IN.SQUOTES = NOT( IN.SQUOTES)
                           CASE CURRENT.CHAR = '"' ; IF NOT( IN.SQUOTES) THEN IN.DQUOTES = NOT( IN.DQUOTES)

                        END CASE

                     END

                     IF LINE.IDX = LINE.LEN OR ( CURRENT.CHAR = ';' AND NOT( IN.SQUOTES OR IN.DQUOTES)) THEN

                        STATEMENT.NO += 1
                        IF CURRENT.CHAR = ';' THEN IF.EOL = 0 ELSE IF.EOL = 1
                        STATEMENT = TRIM( CONVERT( CHARX( 9), CHARX( 30), INPUT.LINE[ STATEMENT.START, LINE.IDX - STATEMENT.START + IF.EOL]))

                        IF STATEMENT # '' THEN

*                            CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, 'CODE ' : FMT( LINE.NO, 'R%4') : ':' : FMT( STATEMENT.NO, 'R%2') : ' ' : STATEMENT) ;* statement preview diagnostic message

                           ROUTINE = ''

                           * --- --- ---

                           FIRST.PART = FIELD( STATEMENT, ' ', 1)
                           BEGIN CASE

                              CASE FIRST.PART = 'SUBROUTINE' OR FIRST.PART = 'FUNCTION' OR FIRST.PART = 'PROGRAM'

                                 CODE.NAME = TRIMB( FIELD( FIELD( STATEMENT, ' ', 2, 99), '(', 1))
                                 IF CODE.NAME # ITEM.NAME THEN

                                    WARNING.TXT = 'CODE NAME DIFFERENT THAN ROUTINE FILE NAME (&)' : @FM : ITEM.NAME ; CALL TXT( WARNING.TXT)
                                    CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Warning : ' : WARNING.TXT)

                                 END

                              CASE FIRST.PART = 'CALL' OR FIRST.PART = 'DEFFUN'

                                 ROUTINE = TRIMB( FIELD( FIELD( STATEMENT, ' ', 2, 99), '(', 1))
                                 LOCATE ROUTINE IN ROUTINE.LIST< 1> SETTING MY.DUMMY ELSE ROUTINE = ''

*Line [ 115 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23

                                 IF NOT( FIELD( STATEMENT, ' ', 3)) THEN

                                    ROUTINE = FIELD( STATEMENT, ' ', 2)
                                    LOCATE ROUTINE IN ROUTINE.LIST< 1> SETTING MY.DUMMY ELSE ROUTINE = ''

                                 END ELSE

                                    IF FIELD( STATEMENT, ' ', 2) = ROUTINE.DIR THEN ROUTINE = FIELD( STATEMENT, ' ', 3)

                                 END

                           END CASE

                           * --- --- ---

                           IF ROUTINE THEN

                              LOCATE ROUTINE IN CONNECTED.ROUTINES< 1> SETTING MY.DUMMY ELSE CONNECTED.ROUTINES< -1> = ROUTINE

                           END

                        END

                        STATEMENT.START = LINE.IDX + 1

                     END

                  NEXT LINE.IDX

               END
               ELSE

                  IF STATUS() = 1 THEN FILE.EOF = 1
                  ELSE

                     E = 'CANNOT READ ROUTINE FILE (&) FROM (&) STATUS (&)' : @FM : ITEM.NAME : @VM : ROUTINE.DIR : @VM : STATUS() ; CALL TXT( E)
                     CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

                  END

               END

            REPEAT

            CLOSESEQ FILE

         END
         ELSE

            E = 'CANNOT OPEN ROUTINE FILE (&) FROM (&)' : @FM : ITEM.NAME : @VM : ROUTINE.DIR ; CALL TXT( E)
            CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, '!!! Error : ' : E)

         END

         IF CONNECTED.ROUTINES THEN

            CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: Connected routines ::')

            ROUTINE.NO = DCOUNT( CONNECTED.ROUTINES, @FM)
            FOR ROUTINE.IDX = 1 TO ROUTINE.NO

*Line [ 178 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
               CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'ROUTINE', NEST.LEVEL + 1, CONNECTED.ROUTINES< ROUTINE.IDX>, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'CONNECTED ROUTINE')

            NEXT ROUTINE.IDX

         END

      END

      ** ----- PGM.FILE entry -----

      ETEXT = ''
*Line [ 190 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*      CALL DBR( 'PGM.FILE':@FM:1, ITEM.NAME, MY.DUMMY)
F.ITSS.PGM.FILE = 'F.PGM.FILE'
FN.F.ITSS.PGM.FILE = ''
CALL OPF(F.ITSS.PGM.FILE,FN.F.ITSS.PGM.FILE)
CALL F.READ(F.ITSS.PGM.FILE,ITEM.NAME,R.ITSS.PGM.FILE,FN.F.ITSS.PGM.FILE,ERROR.PGM.FILE)
MY.DUMMY=R.ITSS.PGM.FILE<1>
      IF ETEXT THEN ETEXT = ''
      ELSE

         CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: PGM.FILE entry ::')
*Line [ 195 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
         CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'PGM.FILE', NEST.LEVEL + 1, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'PGM.FILE FOR ROUTINE')

      END

      ** ----- ----- -----

      RETURN

** ----- ----- -----

END
END
