* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MzU2MTg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VAR.BILL.COLL.CLER.TT.1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.TT

    FN.BR = 'FBNK.BILL.REGISTER' ;R.BR = ''; F.BR  = ''
    CALL OPF(FN.BR,F.BR)

    IF V$FUNCTION   = 'I' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT(R.NEW(BT.TT.OUR.REFERENCE),@VM)
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(BT.TT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

* GOSUB COLLECT.CR

        NEXT NUMBERBR.ID

        GOSUB COLLECT.DR

    END
    IF V$FUNCTION   = 'R' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT(R.NEW(BT.TT.OUR.REFERENCE),@VM)
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(BT.TT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

* GOSUB COLLECT.REV.DR

        NEXT NUMBERBR.ID
**GOSUB COLLECT.REV.CR
    END

    RETURN
*---------
COLLECT.CR:
*---------
    CURR =   R.BR<EB.BILL.REG.CURRENCY>
*----
* CR
*----
    Y.ACCT = R.NEW(BT.TT.CREDIT.ACCOUNT)<1,NUMBERBR.ID>
    Y.AMT  = R.NEW(BT.TT.BR.AMT)<1,NUMBERBR.ID>

*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>

*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*GOSUB AC.STMT.ENTRY

    RETURN

*---------
COLLECT.DR:
*---------
*----
* DR
*----
*Y.ACCT = R.NEW(BT.TT.DEBIT.ACCOUNT)

    Y.ACCT = "EGP1000101140001"
    Y.AMT  = '-1'

*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY

    RETURN
*------------
COLLECT.REV.CR:
*------------
    CURR =   R.BR<EB.BILL.REG.CURRENCY>
*----
* CR
*----


    Y.ACCT = R.NEW(BT.TT.DEBIT.ACCOUNT)
    Y.AMT  = R.NEW(BT.TT.TOTAL.DEBIT.AMT)
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*GOSUB AC.STMT.ENTRY

    RETURN
*------------
COLLECT.REV.DR:
*------------
*----
* DR
*----
    Y.ACCT = R.NEW(BT.TT.CREDIT.ACCOUNT)<1,NUMBERBR.ID>
    Y.AMT  = R.NEW(BT.TT.BR.AMT)<1,NUMBERBR.ID> * -1
*Line [ 157 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*GOSUB AC.STMT.ENTRY

    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '856'
    ENTRY<AC.STE.THEIR.REFERENCE>  = BR.ID
    ENTRY<AC.STE.TRANS.REFERENCE>  = BR.ID
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = BR.ID

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
