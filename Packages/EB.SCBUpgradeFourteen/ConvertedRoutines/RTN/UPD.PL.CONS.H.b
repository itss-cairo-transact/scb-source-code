* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODU0NDg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>383</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM UPD.PL.CONS.H
    SUBROUTINE UPD.PL.CONS.H

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

*------------------------------------------------------
*****
*----------------------------------------------------------------
    GOSUB GETPL
    RETURN
*****************************************************************************
GETPL:
*--------
***DEBUG
    FN.CONS = 'F.CONSOLIDATE.PRFT.LOSS' ; F.CONS = ''
    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''

    CALL OPF(FN.CONS,F.CONS)
    CALL OPF(FN.BASE,F.BASE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*    DAT.ID = R.USER<EB.USE.COMPANY.CODE,1>
*    BNK.DATE1 = TODAY
*    CALL CDT("",BNK.DATE1,'-1C')
*    WORK.DATE = BNK.DATE1

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)

    T.SEL = "SELECT ":FN.CONS:" WITH @ID UNLIKE PL.53000... AND @ID UNLIKE ...EG0010099 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT "SELECTED": SELECTED
    IF SELECTED THEN
        FOR X = 1 TO SELECTED
            CALL OPF(FN.CONS,F.CONS)

            R.CONS = ''

            CALL F.READ(FN.CONS,KEY.LIST<X>,R.CONS,F.CONS,E3)

*Line [ 66 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            P = DCOUNT(R.CONS<RE.PTL.CURRENCY>,@VM)

            PRINT "X= " : X

            FOR L = 1 TO P
                RATE = ''
                CURR = R.CONS<RE.PTL.CURRENCY,L>
***PRINT CURR
                IF CURR # 'EGP' THEN
                    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

                    LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

                        RATE = R.BASE<RE.BCP.RATE,POS>

*                        R.CONS<RE.PTL.BALANCE,L> = R.CONS<RE.PTL.CCY.BALANCE,L> * RATE
*                        R.CONS<RE.PTL.BALANCE,L> = DROUND(R.CONS<RE.PTL.BALANCE,L>,2)
*                        IF R.CONS<RE.PTL.BALANCE,L> = 0 THEN R.CONS<RE.PTL.BALANCE,L> = ''
                        R.CONS<RE.PTL.CREDIT.MOVEMENT,L> = R.CONS<RE.PTL.CCY.CREDT.MVE,L> * RATE
                        R.CONS<RE.PTL.CREDIT.MOVEMENT,L> = DROUND(R.CONS<RE.PTL.CREDIT.MOVEMENT,L>,2)
                        IF R.CONS<RE.PTL.CREDIT.MOVEMENT,L> = 0 THEN R.CONS<RE.PTL.CREDIT.MOVEMENT,L> = ''
                        R.CONS<RE.PTL.DEBIT.MOVEMENT,L> = R.CONS<RE.PTL.CCY.DEBIT.MVE,L> * RATE
                        R.CONS<RE.PTL.DEBIT.MOVEMENT,L> = DROUND(R.CONS<RE.PTL.DEBIT.MOVEMENT,L>,2)
                        IF R.CONS<RE.PTL.DEBIT.MOVEMENT,L> = 0 THEN R.CONS<RE.PTL.DEBIT.MOVEMENT,L> = ''
*                        R.CONS<RE.PTL.BALANCE.YTD,L> = R.CONS<RE.PTL.CCY.BALANCE.YTD,L> * RATE
*                        R.CONS<RE.PTL.BALANCE.YTD,L> = DROUND(R.CONS<RE.PTL.BALANCE.YTD,L>,2)
*                        IF R.CONS<RE.PTL.BALANCE.YTD,L> = 0 THEN R.CONS<RE.PTL.BALANCE.YTD,L> = ''
*===============================================================

                    END
                END
            NEXT L
*WRITE  R.CONS TO F.CONS , KEY.LIST<X> ON ERROR
*    PRINT "CAN NOT WRITE RECORD":KEY.LIST<X>:"TO" :FN.CONS
*END
            CALL F.WRITE(FN.CONS,KEY.LIST<X>, R.CONS)
            CALL JOURNAL.UPDATE(KEY.LIST<X>)
        NEXT X
    END
    RETURN
*****************************************************************************
END
