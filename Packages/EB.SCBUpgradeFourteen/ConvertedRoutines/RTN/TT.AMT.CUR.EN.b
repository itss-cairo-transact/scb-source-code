* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzY5NDM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.AMT.CUR.EN(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    CUR.ID = R.NEW(TT.TE.CURRENCY.1)
    FN.CUR = "FBNK.CURRENCY" ; F.CUR = ""
    CALL OPF(FN.CUR, F.CUR)
    CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ER.CUR)
    CURR = R.CUR<EB.CUR.CCY.NAME><1,1>

*   CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)

    NET.AMOUNT = (ARG)
    IN.AMOUNT  = ARG
    CALL WORDS.ENGLISH(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    ARG =OUT.AMOUNT:" ":CURR

    RETURN
END
