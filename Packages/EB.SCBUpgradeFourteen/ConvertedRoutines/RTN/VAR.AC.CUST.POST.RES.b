* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MTUxODA6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
** ----- 13.10.2004 NESSREEN SCB -----
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.AC.CUST.POST.RES

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*TO INPUT RESTERCTION IN THE CUSTOMER RECORD IF OPEN THE ACCOUNT WITHOUT PAY THE EXPENSES

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
    CUST = R.NEW(AC.CUSTOMER)
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR( 'CUSTOMER.ACCOUNT':@FM:EB.CAC.ACCOUNT.NUMBER,CUST,ACCTNO)
F.ITSS.CUSTOMER.ACCOUNT = 'FBNK.CUSTOMER.ACCOUNT'
FN.F.ITSS.CUSTOMER.ACCOUNT = ''
CALL OPF(F.ITSS.CUSTOMER.ACCOUNT,FN.F.ITSS.CUSTOMER.ACCOUNT)
CALL F.READ(F.ITSS.CUSTOMER.ACCOUNT,CUST,R.ITSS.CUSTOMER.ACCOUNT,FN.F.ITSS.CUSTOMER.ACCOUNT,ERROR.CUSTOMER.ACCOUNT)
ACCTNO=R.ITSS.CUSTOMER.ACCOUNT<EB.CAC.ACCOUNT.NUMBER>
    IF NOT(ACCTNO) THEN
        CALL OPF( FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ( FN.CUSTOMER,CUST, R.CUSTOMER, F.CUSTOMER, ETEXT)
        LC = R.CUSTOMER<EB.CUS.LOCAL.REF>
        VERNAME = LC<1,CULR.VERSION.NAME>
        IF VERNAME = ',SCB.PRIVATE' OR VERNAME = ',SCB.CORPORATE' THEN
            R.CUSTOMER<EB.CUS.POSTING.RESTRICT> = '11'
            CALL F.WRITE(FN.CUSTOMER,CUST,R.CUSTOMER)
        END
    END
    RETURN
END
