* @ValidationCode : MjoxOTA3NTc2NTM0OkNwMTI1MjoxNjQ1MjkxNDQwMzY0OnVzZXI6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:24:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.START.DAY(REQ.DATE)
************************************************************************
***********by Mahmoud Elhawary ** 5/10/2009 ****************************
************************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
************************************************************************
* convert the date required to the start date for statement
************************************************************************

    TDR   = REQ.DATE
    TDRF  = TDR[1,6]:'01'
    CALL CDT("",TDRF,'-1W')
    TDR = TDRF
    TDRDD = TDR[7,2]
    TDRMN = TDR[5,2]
    TDRYY = TDR[1,4]
    M.DAT = TDRMN
    IF TDRMN EQ 3 THEN
        M.DAT = 1
    END
    IF TDRMN EQ 6 THEN
        M.DAT = 4
    END
    IF TDRMN EQ 9 THEN
        M.DAT = 7
    END
    IF TDRMN EQ 12 THEN
        M.DAT = 10
    END
    IF LEN(M.DAT) EQ 1 THEN
        M.DAT = '0':M.DAT
    END
    MMDAT = TDRYY:M.DAT:'01'

    TDD = MMDAT
    REQ.DATE = TDD
RETURN
