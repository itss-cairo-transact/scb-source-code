* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODAwNzM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*****NESSREEN AHMED 29/10/2018********
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.FLT.SUEZ.CHRG.MIX(CUSTOMER,DEAL.AMOUNT,DEAL.CURRENCY,CURRENCY.MARKET,CROSS.RATE,CROSS.CURRENCY,DRAWDOWN.CCY,T.DATA,CUST.COND,CHARGE.AMOUNT)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------
    FN.CURR ='FBNK.CURRENCY' ; F.CURR = ''
    CALL OPF(FN.CURR,F.CURR)

    LOCAL.REF          = R.NEW(TT.TE.LOCAL.REF)
    FCY.AMT.P          = LOCAL.REF<1,TTLR.PAID.AMT>
    AMT.LCY             = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    AMT.FCY             = FCY.AMT.P
*  AMT.DEB             = R.NEW(FT.DEBIT.AMOUNT)
    CALL F.READ(FN.CURR,'USD',R.CURR,F.CURR,ERR1)
    RATE.USD            = R.CURR<EB.CUR.MID.REVAL.RATE><1,1>
    EQV.AMT             = AMT.FCY * RATE.USD
    TOT.AMT             =  EQV.AMT + AMT.LCY
    TOT.AMT.PER         = DROUND((TOT.AMT * 0.00114),2)

    IF TOT.AMT.PER LE '11.4' THEN
        AMT.FIN = 11.4
    END

    IF TOT.AMT.PER GE  '114' THEN
        AMT.FIN = 114
    END
    IF TOT.AMT.PER GT '11.4' AND TOT.AMT.PER LT '114' THEN
        AMT.FIN = TOT.AMT.PER
    END

**    R.NEW(TT.TE.CHRG.AMT.LOCAL)<1,2>  = AMT.FIN
    CHARGE.AMOUNT = AMT.FIN
    CURRENCY = 'EGP'
*    TEXT = 'CHRG.AMT=': AMT.FIN ; CALL REM
    CALL REBUILD.SCREEN

    RETURN
END
