* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzQwODg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*********NESSREEN AHMED 13/01/2009
*-----------------------------------------------------------------------------
* <Rating>-93</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TOT.CUST.SUS.MRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    F.ACCOUNT = '' ; FN.ACCOUNT = 'FBNK.ACCOUNT' ; R.ACCOUNT = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    F.CUSTACC = '' ; FN.CUSTACC = 'FBNK.CUSTOMER.ACCOUNT' ; R.CUSTACC = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.CUSTACC,F.CUSTACC)

    CUST.AC = '' ; CUST.LD = '' ; CUST.LC.IMP = '' ; CUST.LC.EXP = ''
*==================ACCOUNT SELECTION=============================================================
**********SUSPENSE SELECTION*****************************************************************
    ACC.SEL = "SELECT FBNK.ACCOUNT WITH (CATEGORY EQ '1710' OR CATEGORY EQ '1711') OR INT.NO.BOOKING EQ 'SUSPENSE' BY CUSTOMER"
    KEY.LIST.ACC ="" ; SELECTED.ACC="" ;  ER.MSG.ACC=""
    CALL EB.READLIST(ACC.SEL,KEY.LIST.ACC,"",SELECTED.ACC,ER.MSG.ACC)
    IF SELECTED.ACC THEN
        ***TEXT = 'SELECTED.SUS=':SELECTED.ACC ; CALL REM
        X = ''
        CUST.COMP = ''
        FOR ACC = 1 TO SELECTED.ACC
            CALL F.READ(FN.ACCOUNT,KEY.LIST.ACC<ACC>,R.ACCOUNT,F.ACCOUNT,E2)
            CUST.AC<ACC> = R.ACCOUNT<AC.CUSTOMER>
            IF CUST.AC<ACC> # CUST.AC<ACC-1> THEN
                X = X+1
                CUST.COMP<X> = CUST.AC<ACC>
                CALL F.READ(FN.CUSTOMER, CUST.AC<ACC>, R.CUSTOMER, F.CUSTOMER, E1)
                R.CUSTOMER<EB.CUS.LOCAL.REF, CULR.CREDIT.STAT>= 'SUS'
                CALL F.WRITE(FN.CUSTOMER,CUST.AC<ACC>, R.CUSTOMER)
                CALL JOURNAL.UPDATE(CUST.AC<ACC>)
            END
        NEXT ACC
    END   ;**SELECTED.ACC**
** TEXT = 'X=':X ; CALL REM
*==================MARGIN SELECTION ====================================================================
    R.CUSTOMER = ''
    ACC.MRG = "SELECT FBNK.ACCOUNT WITH (CATEGORY GE 1220 AND CATEGORY LE 1229) OR CATEGORY EQ '9090' BY CUSTOMER"
    KEY.LIST.MRG ="" ; SELECTED.MRG="" ;  ER.MSG.MRG=""
    CALL EB.READLIST(ACC.MRG,KEY.LIST.MRG,"",SELECTED.MRG,ER.MSG.MRG)
    IF SELECTED.MRG THEN
        *****TEXT = 'SELECTED.MRG=':SELECTED.MRG ; CALL REM
        XX = ''
        CUST.COMP.1 = ''
        FOR MGG = 1 TO SELECTED.MRG
            CALL F.READ(FN.ACCOUNT,KEY.LIST.MRG<MGG>,R.ACCOUNT,F.ACCOUNT,E2)
            CUST.MRG<MGG> = R.ACCOUNT<AC.CUSTOMER>
            IF CUST.MRG<MGG> # CUST.MRG<MGG-1> THEN
                CALL F.READ(FN.CUSTOMER,CUST.MRG<MGG>, R.CUSTOMER, F.CUSTOMER, E1)
                R.CUSTOMER<EB.CUS.LOCAL.REF, CULR.CREDIT.STAT>= 'MRG'
                CALL F.WRITE(FN.CUSTOMER,CUST.MRG<MGG>, R.CUSTOMER)
                CALL JOURNAL.UPDATE(CUST.MRG<MGG>)
            END
        NEXT MGG
    END   ;**END OF SELECTED.MRG**
*==============================================================
    RETURN
END
