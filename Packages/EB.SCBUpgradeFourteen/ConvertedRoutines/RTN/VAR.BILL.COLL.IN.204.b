* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0NDQ4OTU6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-73</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BILL.COLL.IN.204

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.BR.V.DATE
*-----------------------------------------------------
    FN.BR ='FBNK.BILL.REGISTER'     ; F.BR    = ''
    CALL OPF(FN.BR,F.BR)

    FN.CUS.V ='F.SCB.CUS.BR.V.DATE' ; F.CUS.V = ''
    CALL OPF(FN.CUS.V,F.CUS.V)
*-----------------------------------------------------
    TRNS.CODE = ""
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='INAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 58 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)

        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID> THEN

                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>      = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>   = 7
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.RETURN.REASON>  = R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>    = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE> = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BR.STATUS>      = "IN"

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
* END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
            END

            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>      = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>   = 8
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>    = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE> = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BR.STATUS>      = "IN"

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
                GOSUB COLLECT
            END
        NEXT NUMBERBR.ID
    END
*------------------------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='RNAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)

        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID> THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>   =  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.RETURN.REASON>  =  ""
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BR.STATUS>      =  ""

                MAT.EX = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                PLACE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
                HH     = "+":PLACE:"W"
                CALL CDT('',MAT.EX,HH)
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE> = MAT.EX
*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
            END
            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN

                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>   =  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BR.STATUS>      = ""

                MAT.EX = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                PLACE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
                HH     = "+":PLACE:"W"
                CALL CDT('',MAT.EX,HH)
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE> = MAT.EX
*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
                GOSUB COLLECT.REV
            END
        NEXT NUMBERBR.ID
    END
    RETURN
*--------------------------------------------------------------------
COLLECT:
*-------
    CURR = R.BR<EB.BILL.REG.CURRENCY>
    IF CURR NE 'EGP' THEN
        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    END ELSE
        RATE = ''
    END
*----
* CR
*----
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,Y.ACCT,CUS.BR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.BR=R.ITSS.ACCOUNT<AC.CUSTOMER>

    CALL F.READ(FN.CUS.V,CUS.BR, R.CUS.V, F.CUS.V, ETEXT.V)
    CUS.V.DATE = R.CUS.V<BR.V.NO.VALUE.DAY>

    AMT = R.BR<EB.BILL.REG.AMOUNT>

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END

*Line [ 178 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
***--------------HYTHAM 20100810-----------------------------***
    NEW.VALUE.DATE = TODAY

    IF CUS.V.DATE EQ '' THEN
*    CALL CDT('', NEW.VALUE.DATE , '+1W')
    END ELSE
        HH = '+':CUS.V.DATE:'W'
        CALL CDT('', NEW.VALUE.DATE , HH)
    END
***--------------HYTHAM 20100810-----------------------------***
    TRNS.CODE = '791'
    GOSUB AC.STMT.ENTRY
*----
* DR
*----
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>

    AMT  = R.BR<EB.BILL.REG.AMOUNT> * -1
    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END

*Line [ 219 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    NEW.VALUE.DATE = TODAY
    TRNS.CODE      = '204'
    GOSUB AC.STMT.ENTRY
    RETURN
********************
COLLECT.REV:
    CURR =   R.BR<EB.BILL.REG.CURRENCY>

    IF CURR NE 'EGP' THEN
        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    END ELSE
        RATE = ''
    END
*----
* DR
*----
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
*Line [ 246 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,Y.ACCT,CUS.BR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.BR=R.ITSS.ACCOUNT<AC.CUSTOMER>

    CALL F.READ( FN.CUS.V,CUS.BR, R.CUS.V, F.CUS.V, ETEXT.V)
    CUS.V.DATE = R.CUS.V<BR.V.NO.VALUE.DAY>

    AMT  = R.BR<EB.BILL.REG.AMOUNT> * -1

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END
*Line [ 267 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 274 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
***--------------HYTHAM 20100810-----------------------------***
    NEW.VALUE.DATE = TODAY
    IF CUS.V.DATE EQ '' THEN
*    CALL CDT('', NEW.VALUE.DATE , '+1W')
    END ELSE
        HH = '+':CUS.V.DATE:'W'
        CALL CDT('', NEW.VALUE.DATE , HH)
    END
***--------------HYTHAM 20100810-----------------------------***
    TRNS.CODE  = '204'
    GOSUB AC.STMT.ENTRY
*----
* CR
*----
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>
    AMT    = R.BR<EB.BILL.REG.AMOUNT>

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END
*Line [ 306 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    NEW.VALUE.DATE = TODAY
    TRNS.CODE      = '791'
    GOSUB AC.STMT.ENTRY

    RETURN
*----------------------------------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
    ENTRY         = ""
    MULTI.ENTRIES = ""

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRNS.CODE
*--- 2019/03/04
    ENTRY<AC.STE.THEIR.REFERENCE>  = BR.ID
    ENTRY<AC.STE.TRANS.REFERENCE>  = BR.ID

    CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)
    ENTRY<AC.STE.NARRATIVE>        = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = NEW.VALUE.DATE
    ENTRY<AC.STE.EXPOSURE.DATE>    = NEW.VALUE.DATE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = BR.ID

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
*-------------------------------------------------------
    RETURN
END
