* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNTMxNzM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SR.HOLD.MAIL(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HOLD.MAIL

*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*     *CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
     DEPT = R.NEW(SR.SCRIPT.HOLD.DEPT)
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*     CALL DBR("SCB.HOLD.MAIL":@FM:HDM.Description,DEPT,DEPT.NAME)
F.ITSS.SCB.HOLD.MAIL = 'F.SCB.HOLD.MAIL'
FN.F.ITSS.SCB.HOLD.MAIL = ''
CALL OPF(F.ITSS.SCB.HOLD.MAIL,FN.F.ITSS.SCB.HOLD.MAIL)
CALL F.READ(F.ITSS.SCB.HOLD.MAIL,DEPT,R.ITSS.SCB.HOLD.MAIL,FN.F.ITSS.SCB.HOLD.MAIL,ERROR.SCB.HOLD.MAIL)
DEPT.NAME=R.ITSS.SCB.HOLD.MAIL<HDM.Description>
     ARG = DEPT.NAME

 RETURN
 END
