* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MTIyODA6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*---------------------------------NI7OOOOOOOOOOOOOOOO-----------------------------------
    SUBROUTINE VALUEDATE(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    DATE1   = R.NEW(LD.VALUE.DATE)
    DATE2   = R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>
    TEXT    = "1:" : DATE1 ; CALL REM
    TEXT    = "2:" : DATE2 ; CALL REM
    IF DATE2 LT DATE1 THEN
        ARG =  DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]
    END
    IF DATE2 GT DATE1 THEN
        ARG =  DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
    END
    IF DATE2 = '' THEN
        ARG = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
TEXT = "ARG : ": ARG ; CALL REM
    END
    RETURN
END
