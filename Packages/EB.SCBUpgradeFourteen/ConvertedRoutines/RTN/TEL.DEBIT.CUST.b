* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNjIzNzM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>365</Rating>
*-----------------------------------------------------------------------------
    PROGRAM TEL.DEBIT.CUST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.SERVICE
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.PAYMENT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* WRITTEN BY NOHA HAMED-SCB


    GOSUB INITIALISE
*Line [ 58 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 59 ] Adding EB_SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-19
*Line [ 60 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
    RETURN
*==============================================================
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
*    OFS.OPTIONS = "SCB.AC.CONV"
    OFS.OPTIONS = "SCB.AC.TEL"
***    OFS.USER.INFO = "/"


*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********
    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*==============================================================
CALLDB:
    F.TEL.PAYMENT = '' ; FN.TEL.PAYMENT = 'F.SCB.CUST.TEL.PAYMENT'
    R.TEL.PAYMENT = '' ; E1 = ''
    CALL OPF(FN.TEL.PAYMENT,F.TEL.PAYMENT)
    F.TEL = '' ; FN.TEL = 'F.SCB.CUST.TEL.SERVICE'
    R.TEL = '' ; E2 = ''
    CALL OPF(FN.TEL,F.TEL)


    FN.CU  = 'FBNK.CUSTOMER'        ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)

*    T.SEL = "SELECT F.SCB.CUST.TEL.PAYMENT WITH TRN.DATE EQ '' AND CUST.BANK.ACCT.NO NE '0000'"
    T.SEL = "SELECT F.SCB.CUST.TEL.PAYMENT WITH CUST.BANK.ACCT.NO NE '0000'"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG="" ; E3=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
*Line [ 116 ] Comment DEBUG - ITSS - R21 Upgrade - 2021-12-26
*Line [ 118 ] Comment DEBUG - ITSS - R21 Upgrade - 2022-01-19
*Line [ 120 ] Comment DEBUG - ITSS - R21 Upgrade - 2022-02-09
    ***DEBUG
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            ACCT.NO = '' ; CUST.NO = ''; ZEROS=''; REF=''
            TOT.ALL = ''; NET.BAL='';R.TEL.PAYMENT='';TEL.TEL=''
            NEW.KEY = ''
            NEW.KEY = KEY.LIST<I>
            CALL F.READ(FN.TEL.PAYMENT,NEW.KEY,R.TEL.PAYMENT,F.TEL.PAYMENT,E2)

            ACC.NO  = R.TEL.PAYMENT<PAY.CUST.BANK.ACCT.NO>
            PL.ACCT = ACC.NO[1,2]
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
            CALL F.READ( FN.CU,CUST.NO, R.CU, F.CU, ETEXT)
            CUST.COMP   = R.CU<EB.CUS.COMPANY.BOOK>
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR('ACCOUNT':@FM: AC.WORKING.BALANCE,ACC.NO,WORK.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR('ACCOUNT':@FM: AC.LOCKED.AMOUNT,ACC.NO,LOCK.AMNT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMNT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
            IF LOCK.AMNT NE '' THEN
*Line [ 135 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                YYY = DCOUNT(LOCK.AMNT,@VM)
                TT  = LOCK.AMNT<YYY>
            END
            ELSE
                TT  = 0
            END
            NET.BAL = WORK.BAL - TT
            TOT.ALL = R.TEL.PAYMENT<PAY.PAYMENT.AMOUNT>
            PAYMENT.ID = NEW.KEY
            BR.ACC = 'EGP1107700010001'
            IF NET.BAL LT TOT.ALL THEN
*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR('ACCOUNT':@FM: AC.LIMIT.REF,ACC.NO,LIMIT.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIMIT.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
                REF=FIELD(LIMIT.REF,'.',1)
                LEN.REF=LEN(REF)
                FOR J = LEN.REF TO 6
                    ZEROS=ZEROS:'0'
                NEXT J
************************************
                LIMIT.ID=CUST.NO:'.':ZEROS:LIMIT.REF
*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR('LIMIT':@FM: LI.MAXIMUM.TOTAL ,LIMIT.ID,LIMIT.AMOUNT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIMIT.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
LIMIT.AMOUNT=R.ITSS.LIMIT<LI.MAXIMUM.TOTAL>
                NET.BAL = NET.BAL + LIMIT.AMOUNT
            END

            IF NET.BAL GE TOT.ALL OR PL.ACCT EQ 'PL' THEN
                COMMA = ","

*******************************************
                OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":'AC28':COMMA
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": ACC.NO:COMMA
                OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.ALL:COMMA
                OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":BR.ACC:COMMA
                OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB':COMMA
                OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":'1':COMMA
                OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":'PHONE':TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":'PHONE':TODAY


                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                OFS.ID = "PHONE":ACCT.NO:RND(10000):".":TODAY
                WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160628 - S
*                SCB.OFS.SOURCE = "SCBOFFLINE"
*                SCB.OFS.ID = '' ; SCB.OPT = ''
*                CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*                END
*                CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E

*****************update scb.cust.tel.payment table***********
                R.TEL.PAYMENT<PAY.TRN.DATE> = TODAY
                R.TEL.PAYMENT<PAY.PAYMENT.STATUS>='1'


                CALL F.WRITE(FN.TEL.PAYMENT,PAYMENT.ID,R.TEL.PAYMENT)
                CALL JOURNAL.UPDATE(PAYMENT.ID)

            END
            ELSE
                R.TEL.PAYMENT<PAY.TRN.DATE> = TODAY
                R.TEL.PAYMENT<PAY.PAYMENT.STATUS>='2'


                CALL F.WRITE(FN.TEL.PAYMENT,PAYMENT.ID,R.TEL.PAYMENT)
                CALL JOURNAL.UPDATE(PAYMENT.ID)

            END

**************************************************************************
        NEXT I
    END
    RETURN
END
