* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0NzYyNDk6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:01:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>159</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BOAT.AMOUNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*----------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*----------------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
*------------------------------------------
    COMP  = ID.COMPANY
    AC.BR = COMP[2]
    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.BOT = 'F.SCB.BOAT.TOTAL' ; F.BOT = ''
    CALL OPF(FN.BOT,F.BOT)

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
    BOT.CUR = R.NEW(BO.CURRENCY)
    AMT     = R.NEW(BO.AMOUNT)
    V.DATE  = TODAY
*************************************************
    BOT.ID  = R.NEW(BO.BOT.ID)
    BOAT.ID = R.NEW(BO.BOAT.CODE)

**    T.SEL = "SELECT ":FN.BOT:" WITH @ID EQ ":BOT.ID:" AND BOAT.CODE EQ ":BOAT.ID
**    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**    IF SELECTED THEN

**        CALL F.READ(FN.BOT,KEY.LIST,R.BOT,F.BOT,E1)
**        BTT = R.BOT<BOT.BOAT.CODE>

**        LOCATE BOAT.ID IN BTT<1,1> SETTING POS ELSE NULL

**        R.BOT<BOT.REG.CUS,POS> -= 1
**        R.BOT<BOT.REG.AMT,POS> -= R.NEW(BO.AMOUNT)

**        CALL F.WRITE(FN.BOT,KEY.LIST,R.BOT)

**    END

**    CALL F.READ(FN.BOT,BOT.ID,R.BOT,F.BOT,E1)
**    DAMT = DCOUNT(R.BOT<BOT.REG.AMT>,VM)
**    FOR I = 1 TO DAMT
**        AMT.REG += R.BOT<BOT.REG.AMT><1,I>
**    NEXT I
*----------------------------------------------
**    DCUS = DCOUNT(R.BOT<BOT.REG.CUS>,VM)
**    FOR X = 1 TO DCUS
**        CUS.REG += R.BOT<BOT.REG.CUS><1,X>
**    NEXT X
**-----------------
**    IF AMT.REG EQ 0 AND CUS.REG EQ 0 THEN
**        R.BOT<BOT.STATUS> = '2'
**        CALL F.WRITE(FN.BOT,KEY.LIST,R.BOT)
**    END
*************************************************
*------------------------------------------
    IF BOT.CUR EQ 'EGP' THEN
        ACCT.NO.DR = 'EGP12001000100':AC.BR
    END
    IF BOT.CUR EQ 'USD' THEN
        ACCT.NO.DR = 'USD12001000100':AC.BR
    END
*------------------------------------------
    IF BOAT.ID NE '50' THEN
        IF R.NEW(BO.ACCOUNT.NO) EQ '' THEN
            IF BOT.CUR EQ 'EGP' THEN
                ACCT.NO.CR = 'EGP16188000300':AC.BR
            END
            IF BOT.CUR EQ 'USD' THEN
                ACCT.NO.CR = 'USD16188000300':AC.BR
            END
        END ELSE
            ACCT.NO.CR = R.NEW(BO.ACCOUNT.NO)
            R.NEW(BO.STATUS) = '2'
        END
    END

    IF BOAT.ID EQ '50' THEN
        IF R.NEW(BO.ACCOUNT.NO) EQ '' THEN
            ACCT.NO.CR = 'EGP1618800040020'
        END ELSE
            ACCT.NO.CR = R.NEW(BO.ACCOUNT.NO)
            R.NEW(BO.STATUS) = '2'
        END
    END
*------------------------------------------
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC16":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":BOT.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":BOT.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":ACCT.NO.DR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":ACCT.NO.CR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ID.NEW:COMMA

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY
    OFS.ID = "T":TNO:".":ID.NEW:"-":DAT

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E

    RETURN
************************************************************
END
