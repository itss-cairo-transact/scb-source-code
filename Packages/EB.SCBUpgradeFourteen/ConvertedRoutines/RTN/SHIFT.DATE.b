* @ValidationCode : MjoxOTUzMTIxOTIwOkNwMTI1MjoxNjQ1MjkxNDQ3Mzg3OnVzZXI6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:24:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>756</Rating>
*-----------------------------------------------------------------------------
** ----- 04.12.2002 Pawel TEMENOS EE -----
SUBROUTINE SHIFT.DATE( START.DATE, SHIFT, ROUND)

* function to shift date forword by specified period
*
* accepted shift formats are:
* xxx or xxxD - shift xxx days
* xxxW - shift xxx weeks
* xxxM - shift xxx months
* xxxY - shift xxx years
*
* supported date rounding:
* ROUND = 'UP' ---> 20000131 + 1M = 20000301
* ROUND = 'DOWN' ---> 20000131 + 1M = 20000229
* ROUND = '' ---> 20000131 + 1M = 20000303

*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE

    TEXT = ''
    END.DATE = ''

** ----- ----- -----

    IF NOT( TEXT) THEN

        SHIFT = TRIM( SHIFT)
        YEAR.SHIFT = 0 ; MONTH.SHIFT = 0 ; DAY.SHIFT = 0
        IF SHIFT MATCH '1N0N' THEN DAY.SHIFT = SHIFT
        ELSE

            SHIFT.TYPE = UPCASE( SHIFT[ 1])
            SHIFT.NUMBER = SHIFT[ 1, LEN( SHIFT) - 1]
            IF NOT( SHIFT.NUMBER) THEN SHIFT.NUMBER = 1

            IF SHIFT.NUMBER MATCH '1N0N' THEN

                BEGIN CASE

                    CASE SHIFT.TYPE = 'Y' ; YEAR.SHIFT = SHIFT.NUMBER
                    CASE SHIFT.TYPE = 'M' ; MONTH.SHIFT = SHIFT.NUMBER
                    CASE SHIFT.TYPE = 'W' ; DAY.SHIFT = 7 * SHIFT.NUMBER
                    CASE SHIFT.TYPE = 'D' ; DAY.SHIFT = SHIFT.NUMBER
                    CASE OTHERWISE ; TEXT = 'WRONG SHIFT TYPE (&)':@FM:SHIFT.TYPE

                END CASE

            END ELSE TEXT = 'WRONG SHIFT NUMBER (&)':@FM:SHIFT.NUMBER

        END

    END

** ----- ----- -----

    IF NOT( TEXT) THEN

        END.DAY = START.DATE[ 7, 2] + DAY.SHIFT
        END.MONTH = START.DATE[ 5, 2] + MONTH.SHIFT
        END.YEAR = START.DATE[ 1, 4] + YEAR.SHIFT

        IF END.MONTH > 12 THEN END.YEAR += DIV( ( END.MONTH - 1), 12) ; END.MONTH = 1 + MOD( ( END.MONTH - 1), 12); END.YEAR = END.YEAR[1,4]

        IF END.DAY < 29 THEN
            END.DATE = FMT( END.YEAR, 'R%4') : FMT( END.MONTH, 'R%2') : FMT( END.DAY, 'R%2')
        END ELSE

            END.DATE = FMT( END.YEAR, 'R%4') : FMT( END.MONTH, 'R%2') : '01'

            ETEXT = ''
            CALL CDT( '', END.DATE, '+':( END.DAY - 1 ):'C')
            IF ETEXT THEN TEXT = ETEXT ; ETEXT = ''

        END

    END

** ----- ----- -----

    IF NOT( TEXT) THEN

        IF ROUND AND NOT( DAY.SHIFT) AND END.DATE[ 7, 2] # START.DATE[ 7, 2] THEN

            ROUND = UPCASE( ROUND)
            BEGIN CASE

                CASE ROUND = 'UP' ; END.DATE = END.DATE[ 1, 6] : '01'
                CASE ROUND = 'DOWN'

                    END.DATE = END.DATE[ 1, 6] : '01'

                    ETEXT = ''
                    CALL CDT( '', END.DATE, '-1C')
                    IF ETEXT THEN TEXT = ETEXT ; ETEXT = ''

                CASE OTHERWISE ; TEXT = 'WRONG ROUND (&)':@FM:ROUND

            END CASE

        END

    END

** ----- ----- -----

    IF TEXT THEN CALL REM ; TEXT = '' ; END.DATE = ''

RETURN( END.DATE)
*RETURN
END
