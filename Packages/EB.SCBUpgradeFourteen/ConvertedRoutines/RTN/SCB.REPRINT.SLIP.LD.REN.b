* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMDk5ODE6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:56:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.REPRINT.SLIP.LD.REN(TXN.ID)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*=============================================================================*
*    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', FM:'Y_N')
*   IF COMI[1,1] = 'Y' THEN
*      CALL TXTINP('Enter Transaction Reference', 8, 23, '13', 'ANY')
*     IF COMI THEN
*            DEBUG
*   TXN.ID = '' ; TXN.ID = COMI ; ER.MSG = '' ; ETEXT = ''
    ER.MSG = '' ; ETEXT = ''
    BEGIN CASE
    CASE TXN.ID[1,2] = 'LD'
        GOSUB SAVE.PARAMETERS
        GOSUB PROCESS.TT.RECORD
        IF NOT(ER.MSG) THEN
            GOSUB REPRINT.SLIP
        END
        GOSUB RESTORE.PARAMETERS
*                  CALL JOURNAL.UPDATE(TXN.ID)
    CASE OTHERWISE
        ER.MSG = 'Invalid Transaction Reference'
    END CASE

    IF ER.MSG THEN
        TEXT = ER.MSG
        CALL REM
*CALL TXTINP(ER.MSG, 8, 23, '1.1', '')
*        END
*   END
    END

    GOTO PROGRAM.END

*=============================================================================*
SAVE.PARAMETERS:
    SAVE.FUNCTION = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS = '' ; SIZE = ''
    TXN.NO = '' ; PROCESS = ''
    NBG.CODE = '' ; RECEIPT = ''
    RETURN
*=============================================================================*
RESTORE.PARAMETERS:
    MAT R.NEW = MAT R.SAVE
    V$FUNCTION = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW = SAVE.ID.NEW
    RETURN
*===================== PROCESS LD.LOANS.AND.DEPOSITS RECORD =================================*
PROCESS.TT.RECORD:
*    IF TXN.ID[3,5] = R.DATES(EB.DAT.JULIAN.DATE)[5] THEN
    IF TXN.ID # '' THEN
        VF.LD.LOANS.AND.DEPOSITS = '' ; CALL OPF('F.LD.LOANS.AND.DEPOSITS', VF.LD.LOANS.AND.DEPOSITS)
        ETEXT = ''
        DIM APP.REC(LD.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(LD.AUDIT.DATE.TIME)  ; MAT R.SAVE = ''
*         MAT R.NEW = MAT R.SAVE
        MAT R.SAVE = MAT R.NEW
        SIZE = LD.AUDIT.DATE.TIME

        CALL F.MATREAD('F.LD.LOANS.AND.DEPOSITS',TXN.ID,MAT APP.REC,SIZE,VF.LD.LOANS.AND.DEPOSITS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            APPLICATION = 'LD.LOANS.AND.DEPOSITS' ; ID.NEW = TXN.ID ; V$FUNCTION = 'I'
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>[1,1] # ',' THEN
                VER.NAME = "LD.LOANS.AND.DEPOSITS,":R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
            END
            ELSE
                VER.NAME = "LD.LOANS.AND.DEPOSITS":R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
            END
            GOSUB GET.DSF.NAMES.ARRAY
        END
    END
    ELSE
        VF.LD.LOANS.AND.DEPOSITS$HIS = '' ; CALL OPF('F.LD.LOANS.AND.DEPOSITS$HIS', VF.LD.LOANS.AND.DEPOSITS$HIS)

*    TXN.ID := ';1'
        ETEXT = '' ; DIM APP.REC(LD.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(LD.AUDIT.DATE.TIME) ; MAT R.SAVE = ''
        SIZE = LD.AUDIT.DATE.TIME
        CALL F.MATREAD('F.LD.LOANS.AND.DEPOSITS$HIS',TXN.ID,MAT APP.REC,SIZE,VF.LD.LOANS.AND.DEPOSITS$HIS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            IF R.NEW(LD.RECORD.STATUS) # 'REVE' THEN
                APPLICATION = 'LD.LOANS.AND.DEPOSITS' ; ID.NEW = TXN.ID ; V$FUNCTION = 'I'
                PARM.ID = 'LD.LOANS.AND.DEPOSITS,'
                PARM.ID := R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
                UPDATE.REC = 'F':R.COMPANY(EB.COM.MNEMONIC):'.LD.LOANS.AND.DEPOSITS$HIS'
                UPDATE.TS = 'YES'
            END
            ELSE
                ER.MSG = 'INVALID RECORD STATUS FOR REPRINT'
            END
        END
    END
    RETURN
*=============================================================================*
REPRINT.SLIP:
    IF VER.NAME THEN
        R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 144 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
R.DSF=R.ITSS.VERSION<EB.VER.D.SLIP.FORMAT>
        IF NOT(ETEXT) THEN
*         TEXT = "ARRAY = ":R.DSF ; CALL REM
            DSF.CNT = DCOUNT(R.DSF,@VM)
*         TEXT = "DSF COUNT = ":DSF.CNT  ; CALL REM
        END



        IF DSF.CNT >= 1 THEN
            FOR DSF.LP = 1 TO DSF.CNT
                FOR J=1 TO 20
                    CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP,J>))
                NEXT J
            NEXT DSF.LP
        END
        ELSE
            ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
        END
        RETURN
*=============================================================================*
GET.DSF.NAMES.ARRAY:
        IF VER.NAME THEN
            R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 169 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 176 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
R.DSF=R.ITSS.VERSION<EB.VER.D.SLIP.FORMAT>
            IF NOT(ETEXT) THEN
*         TEXT = "ARRAY = ":R.DSF ; CALL REM
                DSF.CNT = DCOUNT(R.DSF,@VM)
*         TEXT = "DSF COUNT = ":DSF.CNT  ; CALL REM
            END
            ELSE
                ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
            END
        END

*=================== SUBROUTINE END ... YES !!! ==============================*
PROGRAM.END:
        RETURN
    END
