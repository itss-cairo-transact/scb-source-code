* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzQxNTI6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>173</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE TRACE.LRT.RUN( NEST.LEVEL, ITEM.NAME, REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, R.ITEM)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LOCAL.REF.TABLE

** ----- ----- -----
MAIN:
** ----- ----- -----

      NO.HEADER = 1
      IF NOT( COUNT( ITEM.NAME, '!')) THEN

         LRT.LOCAL.TABLE = R.ITEM< EB.LRT.LOCAL.TABLE.NO>
         LRT.LOCAL.TABLE.NO = DCOUNT( LRT.LOCAL.TABLE, @VM)
         FOR LRT.LOCAL.TABLE.IDX = 1 TO LRT.LOCAL.TABLE.NO

            ITEM.NAME = FIELD( ITEM.NAME, '!', 1) : '!' : LRT.LOCAL.TABLE.IDX
            GOSUB TRACE.LRT.FIELD

         NEXT LRT.LOCAL.TABLE.IDX

      END ELSE GOSUB TRACE.LRT.FIELD

      RETURN

** ----- ----- -----
TRACE.LRT.FIELD:
** ----- ----- -----

      RECORD.NAME = FIELD( ITEM.NAME, '!', 1)
      FIELD.IDX = FIELD( ITEM.NAME, '!', 2)

      IF FIELD.IDX THEN

         ** ----- LOCAL.TABLE entry -----

         LT.ID = R.ITEM< EB.LRT.LOCAL.TABLE.NO, FIELD.IDX>
         IF LT.ID THEN

            IF NO.HEADER THEN NO.HEADER = 0 ; CALL WRITE.REPORT.LINE( NEST.LEVEL, REPORT, ':: LOCAL.TABLE entry ::')
*Line [ 65 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.SUB( 'LOCAL.TABLE', NEST.LEVEL + 1, LT.ID:'!.', REPORT, NESTED.TRACE, ROUTINE.DIR, R.DL.DEFINE, R.DL.UPDATE, R.DL.PRECEDING, ITEM.OWNER, 'LOCAL.TABLE RECORD')

         END

         ** ----- ----- -----

      END

      RETURN

** ----- ----- -----

END
