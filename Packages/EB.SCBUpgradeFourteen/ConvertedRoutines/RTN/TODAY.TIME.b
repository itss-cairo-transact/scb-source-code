* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNjgzMTI6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE TODAY.TIME
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

    DATY    = TODAY

    IF O.DATA EQ TODAY THEN

        TIMEE   = TIMEDATE()
        HHH     = FIELD(TIMEE,":", 1)
        MIN     = FIELD(TIMEE,":", 2)
        PART3   = FIELD(TIMEE,":", 3)
        SEC     = PART3[1,2]
        TIMEFMT = HHH:":":MIN

        T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

       * O.DATA = T.DAY
        O.DATA = T.DAY :'      ': TIMEFMT

    END
    RETURN
END
