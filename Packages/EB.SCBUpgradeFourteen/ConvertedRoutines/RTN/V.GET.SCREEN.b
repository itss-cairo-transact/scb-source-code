* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MDA0Nzk6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE V.GET.SCREEN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    FN.FT   ='FBNK.FUNDS.TRANSFER'; F.FT =''
    CALL OPF(FN.FT,F.FT)


    FN.CUR ='FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)


    TRANS.ID = ID.NEW
    AMT      = R.NEW(FT.DEBIT.AMOUNT)
    DR.ACCT  = R.NEW(FT.DEBIT.ACCT.NO)
    CR.ACCT  = R.NEW(FT.CREDIT.ACCT.NO)
    DR.CUR   = R.NEW(FT.DEBIT.CURRENCY)
    DB.THEIR = R.NEW(FT.DEBIT.THEIR.REF)
    DB.THEIR = TRANS.ID
    BEN.CUST1 = R.NEW(FT.BEN.CUSTOMER)<1,1>
    BEN.CUST2 = R.NEW(FT.BEN.CUSTOMER)<1,2>
    BEN.CUST3 = R.NEW(FT.BEN.CUSTOMER)<1,3>
****************TO CALAC COMMISION FOR CENTRAL BANK **************************
*****************FOR FIVE DOLLAR COMMISSION ******************************

    IF DR.CUR EQ 'USD' THEN
        AMT5 = '5'
    END ELSE
        CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,ERR1)
        RATE.USD        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        AMT5.EGP        = 5 * RATE.USD
        CALL F.READ(FN.CUR,'EUR',R.CUR,F.CUR,ERR1)
        RATE.EUR        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        AMT5.EUR         = AMT5.EGP / RATE.EUR
        CALL EB.ROUND.AMOUNT ('',AMT5.EUR,'',"2")
        AMT5 = AMT5.EUR
    END
*****************FOR 30 TO 300 EGP COMMISSION ******************************
    IF DR.CUR NE 'EGP' THEN
        CALL F.READ(FN.CUR,DR.CUR,R.CUR,F.CUR,ERR1)
        RATE.FOR        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        AMT30.RATE       = AMT * '0.002'
        AMT30.EGP       = AMT30.RATE * RATE.FOR
        IF AMT30.EGP LE '30' THEN
            AMT30.EGP = '30'
        END
        IF AMT30.EGP GE '300' THEN
            AMT30.EGP = '300'
        END
        IF AMT30.EGP GT '30' AND AMT30.EGP LT '300' THEN
            AMT30.EGP = AMT30.EGP
        END
        AMT30 = AMT30.EGP / RATE.FOR
        AMT.TOT = AMT5 + AMT30
        CALL EB.ROUND.AMOUNT ('',AMT.TOT,'',"2")
**********************************************************
        LINK.DATA<1> = DR.ACCT
        LINK.DATA<2> = CR.ACCT
        LINK.DATA<3> = AMT.TOT
        LINK.DATA<4> = DR.CUR
        LINK.DATA<5> = DB.THEIR
        LINK.DATA<6> = BEN.CUST1
        LINK.DATA<7> = BEN.CUST2
        LINK.DATA<8> = BEN.CUST3

        FN.FT   = 'FBNK.FUNDS.TRANSFER'; F.FT =''
        CALL OPF(FN.FT,F.FT)

        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)

        IF V$FUNCTION = 'A' THEN
            INPUT.BUFFER = C.U:' FUNDS.TRANSFER,SCB.COMESA.22 I ': C.F
            CALL EB.SET.NEXT.TASK(INPUT.BUFFER)
        END
        RETURN
    END
