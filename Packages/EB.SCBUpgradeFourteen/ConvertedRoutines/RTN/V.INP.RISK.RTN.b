* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MDk0ODc6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE V.INP.RISK.RTN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PAY.CBE.CODES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*----------------------------------------------
    FUNTP    = ""
    FUN.CODE = ""

    FUNTP    = R.NEW(CBE.PAY.FUNTP.CODE)
    DOCTP    = R.NEW(CBE.PAY.DOCTP.CODE)
    CURR     = R.NEW(CBE.PAY.CUR.CODE)
    NOTES.1  = R.NEW(CBE.PAY.NOTES1)
    NOTES.2  = R.NEW(CBE.PAY.NOTES2) 

    IF FUNTP EQ 1 AND DOCTP EQ 4 THEN
        IF NOTES.1 EQ '' THEN
            AF=21
            ETEXT = "VALUE MISSING"
            CALL STORE.END.ERROR
        END
    END
    IF FUNTP EQ 2 AND DOCTP EQ 5 THEN
        IF NOTES.1 EQ '' THEN
            AF=21
            ETEXT = "VALUE MISSING"
            CALL STORE.END.ERROR
        END
    END
    IF CURR EQ 'SAR' THEN
        IF NOTES.1 EQ '' THEN
            AF=21
            ETEXT = "VALUE MISSING"
            CALL STORE.END.ERROR
        END
    END

    IF NOTES.1 NE '' AND NOTES.2 NE '' AND (DOCTP EQ 4 OR DOCTP EQ 5 OR CURR EQ 'SAR') THEN
        AF=23
        ETEXT = "YOU MUST NOT ENTER NOTES HERE"
        CALL STORE.END.ERROR

    END
    IF NOTES.1 EQ '' AND (DOCTP EQ 4 OR DOCTP EQ 5 OR CURR EQ 'SAR') THEN
        AF=21
        ETEXT = "YOU MUST ENTER NOTES HERE"
        CALL STORE.END.ERROR

    END

END
