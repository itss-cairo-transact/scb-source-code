* @ValidationCode : MjotMTkzMTU2MjIzMjpDcDEyNTI6MTY0ODU0NDQ4MjgyOTpNb3VuaXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Mar 2022 11:01:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>106</Rating>
*-----------------------------------------------------------------------------

** ----- 28.08.2002 Pawel TEMENOS -----
SUBROUTINE TRACE.VERSION.SUB( NEST.LEVEL, ITEM.NAME, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.OWNER, ITEM.RELATION)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.VERSION

    DL.NAME = 'VERSION'
    ITEM.NAME.TXT = 'Version'
    FN.ITEM.TABLE = 'F.VERSION'

*Line [ 34 ] Removed I_F.TRACE.ITEM.SUB cause it is not used - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_F.TRACE.ITEM.SUB

** ----- ----- -----
PROCESS.DATA:
** ----- ----- -----
** ----- associated versions -----
*Line [ 41 ] intialise R.ITEM - ITSS - R21 Upgrade - 2021-12-23
    R.ITEM =''
    ASSOC.VERSION = R.ITEM<EB.VER.ASSOC.VERSION>
    ASSOC.VERSION.NO = DCOUNT( ASSOC.VERSION, @VM)
    IF ASSOC.VERSION.NO THEN

        REPORT.LINE = 'Associated versions :'
*Line [ 49 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
* GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

        FOR ASSOC.VERSION.IDX = 1 TO ASSOC.VERSION.NO

*Line [ 52 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeFourteen.TRACE.VERSION.SUB( NEST.LEVEL + 1, ASSOC.VERSION< 1, ASSOC.VERSION.IDX>, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'ASSOCIATED VERSION')

        NEXT ASSOC.VERSION.IDX

    END

** ----- next version -----

    NEXT.VERSION = R.ITEM<EB.VER.NEXT.VERSION>
    IF NEXT.VERSION THEN

        REPORT.LINE = 'Next version :'
*Line [ 67 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
*GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

*Line [ 70 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.TRACE.VERSION.SUB( NEST.LEVEL + 1, NEXT.VERSION, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'NEXT VERSION')

    END

** ----- deal slip formats -----

    D.SLIP.FORMAT = R.ITEM<EB.VER.D.SLIP.FORMAT>
    D.SLIP.FORMAT.NO = DCOUNT(D.SLIP.FORMAT, @VM)
    IF D.SLIP.FORMAT.NO THEN

        REPORT.LINE = 'Deal slip formats :'
*Line [ 82 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
*GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

        FOR D.SLIP.FORMAT.IDX = 1 TO D.SLIP.FORMAT.NO

            CALL TRACE.DEAL.SLIP.FORMAT.SUB( NEST.LEVEL + 1, D.SLIP.FORMAT< 1, D.SLIP.FORMAT.IDX>, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'DEAL SLIP FORMAT')

        NEXT D.SLIP.FORMAT.IDX

    END

** ----- list enquiries -----

    NO.ENQUIRY.HEADER = 1

    LIST.ENQUIRY = ITEM.NAME : '-LIST'
    GOSUB PROCESS.LIST.ENQUIRY

    LIST.ENQUIRY = '%' : ITEM.NAME
    GOSUB PROCESS.LIST.ENQUIRY

    ENQ.APPLICATION = FIELD( ITEM.NAME, ',', 1)
    ENQ.VERSION.NAME = FIELD( ITEM.NAME, ',', 2)

    LIST.ENQUIRY = '%' : ENQ.APPLICATION : '$NAU,' : ENQ.VERSION.NAME
    GOSUB PROCESS.LIST.ENQUIRY

    LIST.ENQUIRY = '%' : ENQ.APPLICATION : '$HIS,' : ENQ.VERSION.NAME
    GOSUB PROCESS.LIST.ENQUIRY

** ----- auto new content routines -----

    NEW.CONT.ROUTINE = R.ITEM<EB.VER.AUT.NEW.CONTENT>
    NEW.CONT.ROUTINE.NO = DCOUNT(NEW.CONT.ROUTINE, @VM)
    IF NEW.CONT.ROUTINE.NO THEN

        NO.NEW.CONT.HEADER = 1
        FOR NEW.CONT.ROUTINE.IDX = 1 TO NEW.CONT.ROUTINE.NO

            IF NEW.CONT.ROUTINE< 1, NEW.CONT.ROUTINE.IDX>[ 1, 1] = '@' THEN

                IF NO.NEW.CONT.HEADER THEN

                    REPORT.LINE = 'Auto new content routines :'
*Line [ 126 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
*GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

                    NO.NEW.CONT.HEADER = 0

                END

                CALL TRACE.ROUTINE.SUB( NEST.LEVEL + 1, FIELD( NEW.CONT.ROUTINE< 1, NEW.CONT.ROUTINE.IDX>, '@', 2), REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'AUTO NEW CONTENT ROUTINE')

            END

        NEXT NEW.CONT.ROUTINE.IDX

    END

** ----- validation routines -----

    VAL.ROUTINE = R.ITEM<EB.VER.VALIDATION.RTN>
    VAL.ROUTINE.NO = DCOUNT(VAL.ROUTINE, @VM)
    IF VAL.ROUTINE.NO THEN

        REPORT.LINE = 'Validation routines :'
*Line [ 148 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
* GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

        FOR VAL.ROUTINE.IDX = 1 TO VAL.ROUTINE.NO

            CALL TRACE.ROUTINE.SUB( NEST.LEVEL + 1, VAL.ROUTINE< 1, VAL.ROUTINE.IDX>, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'VALIDATION ROUTINE')

        NEXT VAL.ROUTINE.IDX

    END

** ----- input routines -----

    INPUT.ROUTINE = R.ITEM<EB.VER.INPUT.ROUTINE>
    INPUT.ROUTINE.NO = DCOUNT( INPUT.ROUTINE, @VM)
    IF INPUT.ROUTINE.NO THEN

        REPORT.LINE = 'Input routines :'
*Line [ 166 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
*GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

        FOR INPUT.ROUTINE.IDX = 1 TO INPUT.ROUTINE.NO

            CALL TRACE.ROUTINE.SUB( NEST.LEVEL + 1, INPUT.ROUTINE< 1, INPUT.ROUTINE.IDX>, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'INPUT ROUTINE')

        NEXT INPUT.ROUTINE.IDX

    END

** ----- authorisation routines -----

    AUTH.ROUTINE = R.ITEM<EB.VER.AUTH.ROUTINE>
    AUTH.ROUTINE.NO = DCOUNT(AUTH.ROUTINE, @VM)
    IF AUTH.ROUTINE.NO THEN

        REPORT.LINE = 'Authorisation routines :'
*Line [ 184 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
*GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

        FOR AUTH.ROUTINE.IDX = 1 TO AUTH.ROUTINE.NO

            CALL TRACE.ROUTINE.SUB( NEST.LEVEL + 1, AUTH.ROUTINE< 1, AUTH.ROUTINE.IDX>, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'AUTHORISATION ROUTINE')

        NEXT AUTH.ROUTINE.IDX

    END

** ----- ----- -----

RETURN

** ----- ----- -----
PROCESS.LIST.ENQUIRY:
** ----- ----- -----

    ETEXT = ''
*Line [ 204 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*   CALL DBR( 'ENQUIRY':@FM:1, LIST.ENQUIRY, MY.DUMMY)
    F.ITSS.ENQUIRY = 'F.ENQUIRY'
    FN.F.ITSS.ENQUIRY = ''
    CALL OPF(F.ITSS.ENQUIRY,FN.F.ITSS.ENQUIRY)
    CALL F.READ(F.ITSS.ENQUIRY,LIST.ENQUIRY,R.ITSS.ENQUIRY,FN.F.ITSS.ENQUIRY,ERROR.ENQUIRY)
    MY.DUMMY=R.ITSS.ENQUIRY<1>
    IF ETEXT THEN ETEXT = ''
    ELSE

        IF NO.ENQUIRY.HEADER THEN

            REPORT.LINE = 'List enquiries :'
*Line [ 217 ] Removed GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED due missing gosub - ITSS - R21 Upgrade - 2021-12-23
* GOSUB WRITE.INDENT.REPORT.LINE.SEPARATED

            NO.ENQUIRY.HEADER = 0

        END

        CALL TRACE.ENQUIRY.SUB( NEST.LEVEL + 1, LIST.ENQUIRY, REPORT.FILE, NESTED.TRACE, R.DL.DEFINE, ITEM.NAME, 'LIST ENQUIRY')

    END

RETURN

** ----- ----- -----

END
