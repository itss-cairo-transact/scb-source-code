* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODU1MTU6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
****NESSREEN AHMED 25/11/2010***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE UPDATE.VISA.DATA.MSCC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "SUEZ_CREDIT_":"Edit_Private_Accounts_":"0001_":TODAY:".xml"
    NEW.FILE.2 = "cards_not_found_MSCC"

*****1ST FILE OPEN*********************************************
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
************************************************************
*****2ND FILE OPEN******************************************
    OPENSEQ DIR.NAME,NEW.FILE.2 TO V.FILE.IN.2 THEN
        CLOSESEQ V.FILE.IN.2
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE.2
        HUSH OFF
        PRINT 'FILE ':NEW.FILE.2:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE.2 TO V.FILE.IN.2 ELSE
        CREATE V.FILE.IN.2 THEN
            PRINT 'FILE ' :NEW.FILE.2:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE.2:' to ':DIR.NAME
        END
    END
************************************************************
    F.VISA.DB.AD = '' ; FN.VISA.DB.AD = 'F.SCB.READ.DB.ADVICE' ; R.VISA.DB.AD= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.DB.AD,F.VISA.DB.AD)

    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    YTEXT = "Enter the End Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    ID.KEY = '' ; MBR = '' ; CR.CARD.NO = '' ; X = 0  ; LST.ST.D = '' ; XX = 0  ; MM = 0
    DIM YY(2)

    YY(1) = "<?xml version=":'"':"1.0":'"':" ":"encoding=":'"':"UTF-8":'"':" ?>"
    YY(2) = "<ROOT>"

    FOR SS=1 TO 2
        WRITESEQ YY(SS) TO V.FILE.IN ELSE
            PRINT  'CAN NOT WRITE LINE ':YY(SS)
        END
    NEXT SS
    S.SEL = "SELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO LIKE 4042..."
    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)
    TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM
    IF SELECTED.SS THEN
        FOR I = 1 TO SELECTED.SS
            CR.CARD.NO = '' ; EXT.ACC.NEW = '' ; BR.CODE = '' ; INT.ACC = ''
            CALL F.READ(FN.CARD.ISSUE,KEY.LIST.SS<I>, R.CARD.ISSUE, F.CARD.ISSUE ,E3)
            LOCAL.REF      = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            CR.CARD.NO<I>  = LOCAL.REF<1,LRCI.VISA.NO>
            EXT.ACC.NEW<I> = R.CARD.ISSUE<CARD.IS.ACCOUNT>
            BR.CODE<I>     = R.CARD.ISSUE<CARD.IS.CO.CODE>

            T.SEL = "SELECT F.SCB.READ.DB.ADVICE WITH LST.STAT.DATE EQ " :COMI : " AND CARD.NO EQ " : CR.CARD.NO<I>
            KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
            CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)

            IF SELECTED THEN
                CALL F.READ(FN.VISA.DB.AD,KEY.LIST<1>, R.VISA.DB.AD, F.VISA.DB.AD ,E1)
                INT.ACC     = R.VISA.DB.AD<SCB.DB.INT.ACCT.NO>
                XX = XX+1
                DIM VISA.DATA(5)
                VISA.DATA(1) = '<Record>'
                VISA.DATA(2) = '<PAN>':CR.CARD.NO<I>:'</PAN>'
                VISA.DATA(3) = '<ACCOUNT>':INT.ACC:'</ACCOUNT>'
                VISA.DATA(4) = '<EXTACCOUNT>':EXT.ACC.NEW<I>:'</EXTACCOUNT>'
                VISA.DATA(5) = '</Record>'
                FOR A = 1 TO 5
                    WRITESEQ VISA.DATA(A) TO V.FILE.IN ELSE
                        PRINT  'CAN NOT WRITE LINE ':VISA.DATA(A)
                    END
                NEXT A
            END ELSE
                DIM VISA.DATA.ERR(1)
                VISA.DATA.ERR(1) = CR.CARD.NO<I>

                WRITESEQ VISA.DATA.ERR(1) TO V.FILE.IN.2 ELSE
                    PRINT  'CAN NOT WRITE LINE ':VISA.DATA.ERR(1)
                END
                MM = MM +1
            END
        NEXT I
    END
    DIM NN(1)
    NN(1) = '</ROOT>'
    WRITESEQ NN(1) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':NN(1)
    END

    TEXT = 'NO.REC.NOTFOUND=':MM ; CALL REM
    RETURN
END
