* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyNTY1MjY6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:57:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>570</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

    SUBROUTINE SCB.WH.REGISTER


******************************************************************

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.
*CALL EB.FORMAT.ID("WH")
* IF ID.NEW[1,2] # "WH" THEN E = "You Must Press F3"
    CONT.ACCT = ID.NEW[3,16]
*Line [ 214 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:1,CONT.ACCT,ACCT.EXIST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CONT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACCT.EXIST=R.ITSS.ACCOUNT<1>
    IF NOT(ACCT.EXIST) THEN
        E= 'THERE IS NO CONTINGENT ACCOUNT'
    END

    IF E THEN  CALL ERR ; MESSAGE = 'REPEAT'
    IF E THEN V$ERROR = 1

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.
    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS


    IF E THEN
        T.SEQU = "IFLD"
*Line [ 239 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*

*    FOR I = 1 TO DCOUNT(R.NEW(SCB.BS.BL.CHQ.TYPE),VM)
*        IF R.NEW(SCB.BS.BL.CHQ.TYPE)<1,I> THEN
*            IF NOT (R.NEW(SCB.BS.TOTAL.AMT)<1,I>) THEN          ;* OR NOT(R.NEW(SCB.BS.TOTAL.NO)<1,I>)OR NOT(R.NEW(SCB.BS.NOTES)) THEN
*                ETEXT = "Must.Enter.Value"
*                AF = SCB.BS.TOTAL.AMT ; AV = I
*                CALL STORE.END.ERROR
*            END
*            IF NOT(R.NEW(SCB.BS.TOTAL.NO)<1,I>) THEN
*                ETEXT = "Must.Enter.Value"
*                AF = SCB.BS.TOTAL.NO ; AV = I
*                CALL STORE.END.ERROR
*            END
*            IF NOT(R.NEW(SCB.BS.NOTES)<1,I>) THEN
*                ETEXT = "Must.Enter.Value"
*                AF = SCB.BS.NOTES ; AV = I
*                CALL STORE.END.ERROR
*            END
*
*        END
*    NEXT I
*
*    FOR I = 1 TO DCOUNT(R.NEW(SCB.BS.BL.CHQ.TYPE),VM)
*        IF R.NEW(SCB.BS.BL.CHQ.TYPE)<1,I> THEN
*            IF NOT (R.NEW(SCB.BS.TOTAL.AMT)<1,I>) THEN  ;* OR NOT(R.NEW(SCB.BS.
*                ETEXT = "Must.Enter.Value"
*                AF = SCB.BS.TOTAL.AMT ; AV = I
*                CALL STORE.END.ERROR
*            END
*            IF NOT(R.NEW(SCB.BS.TOTAL.NO)<1,I>) THEN
*                ETEXT = "Must.Enter.Value"
*                AF = SCB.BS.TOTAL.NO ; AV = I
*                CALL STORE.END.ERROR
*            END
*            IF  NOT(R.NEW(SCB.BS.NOTES)<1,I>) THEN
*                ETEXT = "Must.Enter.Value"
*                AF = SCB.BS.NOTES ; AV = I
*                CALL STORE.END.ERROR
*            END
*        END
*    NEXT I
*REM > CALL XX.CROSSVAL
*
** If END.ERROR has been set then a cross validation error has occurred
*
*    IF END.ERROR THEN
*        A = 1
*        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
*        T.SEQU = A
*        V$ERROR = 1
*        MESSAGE = 'ERROR'
*    END
*    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE
*
*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************
DELIVERY.PREVIEW:

    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
    CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
    CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
*Line [ 414 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = "" ; MAT N = "" ; MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = ""

    ID.F  = "WH:Cont.Account.NO"; ID.N  = "20"; ID.T  = "A"
*
    Z=0
*
    Z+=1 ; F(Z) = "CUSTOMER"         ;  N(Z) = "10.1"  ; T(Z) = "CUS"
*Line [ 442 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"

    Z+=1 ; F(Z) = "CURRENCY" ;  N(Z) = "10.1" ; T(Z) = "CCY"
*Line [ 446 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:EB.CUR.CCY.NAME:@FM:"L"

    Z+=1 ; F(Z) = "CATEGORY" ;  N(Z) = "10.1" ; T(Z) = ""
*Line [ 450 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CATEGORY":@FM:EB.CAT.SHORT.NAME:@FM:"L"

    Z+=1 ; F(Z) = "WHAREHOUSE.NO"    ;  N(Z) = "2.1"   ; T(Z) = ""

    Z+=1 ; F(Z) = "BRANCH"           ;  N(Z) = "2.1"   ; T(Z) = ""
*Line [ 456 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"

    Z+=1 ; F(Z) = "VALUE.BALANCE"    ;  N(Z) = "19"  ; T(Z) = "AMT"
    Z+=1 ; F(Z) = "ITEM.STORAGE.DATE"  ;  N(Z) = "10"  ; T(Z) = "D"


    Z+=1 ; F(Z) = "XX<INSURANCE.TYPE" ; N(Z) = "15" ; T(Z) = ""
*    T(Z)<2> = '���_����_����������'
*    T(Z)<2> = '���_����_����������_��� �������_����� ���������'
    T(Z)<2> = '���_����_����� �����_��� �������_����� ���������_�� ���� �������'
    Z+=1 ; F(Z) = "XX-INSURANCE.NO"    ; N(Z) = "15" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "XX-INSURANCE.VALUE" ; N(Z) = "19" ; T(Z) = "AMT"
    Z+=1 ; F(Z) = "XX>EXPIRY.DATE"     ; N(Z) = "10"  ; T(Z) = "D"


    Z+=1 ; F(Z) = "XX.WAREHOUSE.ADDRESS" ; N(Z) = "35" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "WAREHOUSE.TYPE" ; N(Z) = "15" ; T(Z) = ""
    T(Z)<2> = '����_�����_�������'

    Z+=1 ; F(Z) = "COLLACTRAL.ID"        ;  N(Z) = "30"  ; T(Z) = "A"
*Line [ 477 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "COLLATERAL":@FM:0:@FM:"L"

    Z+=1 ; F(Z) = "RESERVED.FIELD9" ; N(Z) = "35" ; T(Z) = "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z) = "RESERVED.FIELD10" ; N(Z) = "35" ; T(Z) = "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z) = "VERSION.NAME" ; N(Z) = "35" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "XX.OVERRIDE" ; N(Z) = "35" ; T(Z) = "ANY"
    T(Z)<3> = 'NOINPUT'
*
    V = Z + 9
*

    RETURN

*************************************************************************

END
