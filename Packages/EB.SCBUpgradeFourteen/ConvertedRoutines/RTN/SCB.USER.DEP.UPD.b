* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMjgwODg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:57:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*****************MAHMOUD 9/5/2012******************************
* Create ofs to update user SCB dept. for some users on GLOBUS*
***************************************************************
    PROGRAM SCB.USER.DEP.UPD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    COMP = ID.COMPANY
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN

*------------------------------
INITIALISE:
    FN.USER = "F.USER" ; F.USER  = '' ; R.USER = '' ; ERR.USER = ''
    CALL OPF(FN.USER,F.USER)
    FN.OFS.SOURCE    ="F.OFS.SOURCE"
    F.OFS.SOURCE     = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "USER"
    OFS.OPTIONS      = "SCB2"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    OFS.USER.INFO    = ""

    COMP = 'EG0010001'
    COM.CODE = COMP[2]
    OFS.USER.INFO = "INPUTT01":"/":"/" :COMP

    KK1 = 0
    FILENAME = "SCB.USER.DEP.CSV"
    PATHNAME = "&SAVEDLISTS&"

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA    = ","
    KEY.LIST = "" ; SELECTED        = "" ;  ER.MSG = ""
    FN.COMP  = 'F.COMPANY' ; F.COMP = ''
    F.PATH  = FN.OFS.IN

***********************************
***********************************

    OPENSEQ PATHNAME , FILENAME TO MY.FILE THEN
        EOF = ''
        I = 0
        LOOP WHILE NOT(EOF)
            READSEQ Line FROM MY.FILE THEN
                USER.ID  = "SCB.":FIELD(Line,",",1)
                SCB.DEPT = FIELD(Line,",",2)

                OFS.MESSAGE.DATA  = "SCB.DEPT.CODE=":SCB.DEPT

                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:USER.ID:COMMA:OFS.MESSAGE.DATA
                OFS.ID = "T":TNO:".USER.":USER.ID
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                CRT USER.ID:"--":SCB.DEPT
            END ELSE
                EOF = 1
            END
        REPEAT
        CLOSESEQ MY.FILE
    END
*********************************************************
END
