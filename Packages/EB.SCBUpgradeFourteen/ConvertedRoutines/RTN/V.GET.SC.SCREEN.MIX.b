* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzOTczMTk6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE V.GET.SC.SCREEN.MIX

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    FN.FT   ='FBNK.FUNDS.TRANSFER'; F.FT =''
    CALL OPF(FN.FT,F.FT)


    FN.CUR ='FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)


    TRANS.ID = ID.NEW
    AMT.DEB          = R.NEW(FT.DEBIT.AMOUNT)
    AMT.FOR          = R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO>
    DR.ACCT.ALL      = R.NEW(FT.DEBIT.ACCT.NO)
    DR.NOTE        =  R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT>
    DR.ACCT   = DR.ACCT.ALL[1,8]:'20':DR.ACCT.ALL[11,6]
    DR.CUR   = R.NEW(FT.DEBIT.CURRENCY)
    DB.THEIR = R.NEW(FT.DEBIT.THEIR.REF)
    DB.THEIR = TRANS.ID


    CR.ACCT = '9949990020321701'
    DR.CUR = 'USD'
**********************************************************
    LINK.DATA<1> = DR.ACCT
    LINK.DATA<2> = CR.ACCT
    LINK.DATA<3> = AMT.FOR
    LINK.DATA<4> = DR.CUR
    LINK.DATA<5> = DB.THEIR
    LINK.DATE<6> = DR.NOTE

    IF V$FUNCTION = 'I' THEN
        INPUT.BUFFER = C.U:' FUNDS.TRANSFER,SCB.SUEZCANAL.MIX2 I ':C.F
        CALL EB.SET.NEXT.TASK(INPUT.BUFFER)
    END
    RETURN
END
