* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyNzkxOTQ6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:57:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
***************************************NI7OOOOOOOOOO*******************
*-----------------------------------------------------------------------------
* <Rating>-145</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCR.HISTORY.CHARGE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.ACCOUNT.BALANCES
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCR.HISTORY.CHARGE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LC = 'FBNK.LETTER.OF.CREDIT$HIS' ; F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    CALL F.READ(FN.ACC,COMI,R.ACC,F.ACC,E1)
    TEXT = "COMI = :" :COMI ; CALL REM

**** FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
**** CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    TOT=''
    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '����� �������� ���������':' - ':'SIGHT PAYMENT OF INWORD COLLECTIONS(CHARGE)'
*------------------------------------------------------------------------
    DB.ACC = R.ACC<LCAC.SETTLE.AC.FROM>
    ACC.NO = DB.ACC
*Line [ 93 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,DB.ACC,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,DB.ACC,CUR.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,DB.ACC,CAT.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*Line [ 123 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*     CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BR1=R.ITSS.ACCOUNT<AC.CO.CODE>
     ACC.BR2 = ACC.BR1[8,2]
     ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
*Line [ 107 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.CHARGE = DCOUNT(R.ACC<LCAC.CHRG.CODE>,@VM)


* AMOUNT    = DR.AMOUNT+CHARGE.AMOUNT
*  IN.AMOUNT = CHARGE.AMOUNT
*  CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*  CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
*  OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
*    DAT   = R.NEW(TF.DR.VALUE.DATE)
    DAT       = R.ACC<LCAC.CHRG.DATE.DUE>
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    ID.HIS = COMI:';1'
    TEXT = "COMI:" : ID.HIS ; CALL REM
    CALL F.READ(FN.LC,ID.HIS,R.LC,F.LC,ERR)
    TEXT = R.LC ; CALL REM
    INPUTT = R.LC<TF.LC.INPUTTER>
    INP    = FIELD(INPUTT,'_',2)
    AUTH   =  R.LC<TF.LC.AUTHORISER>

    AUTHI  =  FIELD(AUTH,'_',2)
*------------------------------------------------------------------------

    XX   = SPACE(132)  ; XX3  = SPACE(132)   ; XX10 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)   ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)   ; XX12 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX1<1,1>[3,35]  = CUST.NAME
    XX2<1,1>[3,35]   = CUST.ADDRESS

* XX<1,1>[45,15]  = '������     : '
* XX<1,1>[59,15]  = CHARGE.AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[3,15] = CUST.NAME.2

    XX4<1,1>[45,15] = '����� ������� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX6<1,1>[1,15]  = '������'
    XX6<1,1>[30,15] = '��� �������'
    XX6<1,1>[60,15] = '������'

    XX7<1,1>[1,15]  = INP:' �'
    XX7<1,1>[30,15] = COMI:' �'
    XX7<1,1>[60,15] = AUTHI:' �'

*  XX8<1,1>[3,35]  = '������ ������� : '
*  XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������         : '
*            XX9<1,1>[20,15] = BR.DATA

*-------------------------------------------------------------------
*Line [ 204 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACC.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT STR(' ',82)
*   PRINT XX3<1,1>
*   PRINT STR(' ',82)
*  PRINT XX8<1,1>
*  PRINT XX9<1,1>
*  PRINT XX5<1,1>
*  PRINT STR(' ',82)
*    PRINT XX<1,1>
    FOR I = 1 TO DECOUNT.CHARGE
        CHARGE.CODE      = R.ACC<LCAC.CHRG.CODE><1,I>
        CHARGE.AMOUNT    = R.ACC<LCAC.AMT.REC><1,I>
        CHARGE.CURR      = R.ACC<LCAC.CHRG.CCY><1,I>
*Line [ 242 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHARGE.CODE,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CHARGE.NAME=R.ITSS.FT.CHARGE.TYPE<FT5.DESCRIPTION>

        IF ETEXT THEN
            TEXT = "ETEXT = " : ETEXT ; CALL REM
*Line [ 252 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
FN.F.ITSS.FT.COMMISSION.TYPE = ''
CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,CHARGE.CODE,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
CHARGE.NAME=R.ITSS.FT.COMMISSION.TYPE<FT4.SHORT.DESCR>
        END

* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
        IN.AMOUNT = R.ACC<LCAC.AMT.REC><1,I>
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
        IF IN.AMOUNT = '' THEN
            OUT.AMOUNT = 0
            OUT.AMT = 0
        END

*Line [ 270 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CHARGE.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        IF IN.AMOUNT = '' THEN
            OUT.AMOUNT = ''
            OUT.AMT = ''
        END
*       TOT = CHARGE.AMOUNT<1,1> + CHARGE.AMOUNT<1,I>
        TEXT = "CHARGE.AMOUNT : ": CHARGE.AMOUNT ; CALL REM
        TOT +=   CHARGE.AMOUNT

        XX10<1,1>[3,35]    = '�������  :'
        XX11<1,I>[20,35]   = CHARGE.CODE
        XX11<1,I>[40,35]   = CHARGE.NAME
        XX11<1,I>[55,35]   = CHARGE.AMOUNT
        XX11<1,I>[65,15]   = CUR

* XX8<1,1>[3,35]     = '������ ������� : '
        XX8<1,I>[20,15]    = OUT.AMT
        XX12<1,1>[3,15]    ='������ ��������'
        XX12<1,1>[28,15]   = TOT
        XX12<1,I>[60,15]   = CUR
        PRINT XX10<1,I>
        PRINT XX11<1,I>
        PRINT XX8<1,I>

    NEXT I

    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
