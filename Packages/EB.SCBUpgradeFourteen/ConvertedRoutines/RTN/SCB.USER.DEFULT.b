* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMjgwMDc6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:57:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.USER.DEFULT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*Line [ 34 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(EB.USE.COMPANY.RESTR),@VM)
    H = NO.M + 1
    R.NEW(EB.USE.COMPANY.RESTR)<1,H> = "ALL"
    R.NEW(EB.USE.APPLICATION)<1,H>  = "ACCOUNT"
    R.NEW(EB.USE.FUNCTION)<1,H>  = "A 2 B C D F H I P R S V"
    R.NEW(EB.USE.FIELD.NO)<1,H>  = "2"
    R.NEW(EB.USE.DATA.COMPARISON)<1,H>  = "NE"
    R.NEW(EB.USE.DATA.FROM)<1,H>  = "1002"


    CALL REBUILD.SCREEN
    RETURN
END
