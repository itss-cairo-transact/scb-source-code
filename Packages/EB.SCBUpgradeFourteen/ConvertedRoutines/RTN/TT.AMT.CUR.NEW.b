* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzY5ODI6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.AMT.CUR.NEW(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    CUR.T = '' IN.AMOUNT = ''
    CUR = R.NEW(TT.TE.CURRENCY.1)
*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*****UPDATED BY NESSREEN AHMED 31/5/2010************************
***      NET.AMOUNT = (ARG)
***      IN.AMOUNT = ARG
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('TELLER':@FM:TT.TE.CURRENCY.1,ARG,CUR.T)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
CUR.T=R.ITSS.TELLER<TT.TE.CURRENCY.1>
    IF CUR.T = 'EGP' THEN
**  IN.AMOUNT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('TELLER':@FM:TT.TE.AMOUNT.LOCAL.1,ARG,IN.AMOUNT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
IN.AMOUNT=R.ITSS.TELLER<TT.TE.AMOUNT.LOCAL.1>
    END ELSE
**  IN.AMOUNT = R.NEW(TT.TE.AMOUNT.FCY.1)
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('TELLER':@FM:TT.TE.AMOUNT.FCY.1,ARG,IN.AMOUNT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
IN.AMOUNT=R.ITSS.TELLER<TT.TE.AMOUNT.FCY.1>
    END

    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    ARG = OUT.AMOUNT:" ":CURR:" ":"�����"
*********************************************************************
    RETURN
END
