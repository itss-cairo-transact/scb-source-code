* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyNjc2Njc6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:57:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*******ABEER
    SUBROUTINE SCCD.CUS.21103.ACH


*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

****SELECT F.BATCH
    NEW.FIL = 'SCCD.CU.21103.ACH.':TODAY

    OPENSEQ "SCCD" , NEW.FIL  TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"SCCD":' ':NEW.FIL
        HUSH OFF
    END
    OPENSEQ "SCCD" , NEW.FIL TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCCD.CUS.21103.ACH CREATED IN SCCD'
        END ELSE
            STOP 'Cannot create SCCD.CUS.21103.ACH File IN SCCD'
        END
    END

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    MAT.DATE = TODAY
    CALL CDT('EG00',MAT.DATE, '1W')
    PRINT MAT.DATE:'AFTER CDT'
    PRINT MAT.DATE:'-MAT.DATE'
*    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21103 AND STATUS NE LIQ AND FIN.MAT.DATE EQ " : MAT.DATE : " BY CO.CODE "
    T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21103 AND ACCT.ANOTHER.BN NE '' " : " AND FIN.MAT.DATE EQ ": MAT.DATE :" BY CO.CODE "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED:'-SELE'
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LOCAL.REF = R.LD<LD.LOCAL.REF>

            GRN.WILL=LOCAL.REF<1,LDLR.GRANT.WILL>

            AMT         =R.LD<LD.AMOUNT>
            LD.DEPT.1   = R.LD<LD.DEPT.CODE>
            LD.CODE.1   = R.LD<LD.CO.CODE>
            LD.BRN.1    = LD.CODE.1[8,2]
            CR.ACCT     = 'EGP16118000100':LD.BRN.1
            DB.ACCT     = LOCAL.REF<1,LDLR.ACCT.STMP>
            LD.CUST.ID  = R.LD<LD.CUSTOMER.ID>
            FIN.DAT     = R.LD<LD.FIN.MAT.DATE>
            ACT.BNK      = LOCAL.REF<1,LDLR.ACCT.ANOTHER.BN>

            IDD = 'FUNDS.TRANSFER,SCCD.LIQ.BR.TRANS,AUTO.SZ//':LD.CODE.1

            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"ACSZ":','
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":','
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":"EGP":','
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:','
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:','
            OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:','
            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":FIN.DAT:','
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":FIN.DAT:','
            OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":KEY.LIST<I>:'.ACH':','
            OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":KEY.LIST<I>:','
            OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
            OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":','
            OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB.21103.ACH':','
            MSG.DATA = IDD:",":",":OFS.MESSAGE.DATA
            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
        PRINT SELECTED
    END
*** COPY TO OFS ***

    EXECUTE 'COPY FROM SCCD TO OFS.IN ':NEW.FIL
    EXECUTE 'DELETE ':"SCCD":' ':NEW.FIL

    RETURN
END
