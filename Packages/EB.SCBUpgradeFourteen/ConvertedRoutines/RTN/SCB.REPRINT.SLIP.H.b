* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMDk5MjQ6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:56:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>72</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.REPRINT.SLIP.H

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*=============================================================================*
*Line [ 36 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        CALL TXTINP('Enter Transaction Reference', 8, 23, '15', 'ANY')
        IF COMI THEN
            TXN.ID = '' ; TXN.ID = COMI ; ER.MSG = '' ; ETEXT = ''
            BEGIN CASE
            CASE TXN.ID[1,2] = 'TT'
                GOSUB SAVE.PARAMETERS
                GOSUB PROCESS.TT.RECORD
                IF NOT(ER.MSG) THEN
                    GOSUB REPRINT.SLIP
                END
                GOSUB RESTORE.PARAMETERS
*                  CALL JOURNAL.UPDATE(TXN.ID)
            CASE OTHERWISE
                ER.MSG = 'Invalid Transaction Reference'
            END CASE

            IF ER.MSG THEN
                TEXT = ER.MSG
                CALL REM
*CALL TXTINP(ER.MSG, 8, 23, '1.1', '')
            END
        END
    END

    GOTO PROGRAM.END

*=============================================================================*
SAVE.PARAMETERS:
    SAVE.FUNCTION = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS = '' ; SIZE = ''
    TXN.NO = '' ; PROCESS = ''
    NBG.CODE = '' ; RECEIPT = ''
    RETURN
*=============================================================================*
RESTORE.PARAMETERS:
    MAT R.NEW = MAT R.SAVE
    V$FUNCTION = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW = SAVE.ID.NEW
    RETURN
*===================== PROCESS TELLER RECORD =================================*
PROCESS.TT.RECORD:
    IF TXN.ID[3,5] = R.DATES(EB.DAT.JULIAN.DATE)[5] THEN
        VF.TELLER = '' ; CALL OPF('F.TELLER', VF.TELLER)
        ETEXT = ''
        DIM APP.REC(TT.TE.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(TT.TE.AUDIT.DATE.TIME)  ; MAT R.SAVE = ''
*         MAT R.NEW = MAT R.SAVE
        MAT R.SAVE = MAT R.NEW
        SIZE = TT.TE.AUDIT.DATE.TIME

        CALL F.MATREAD('F.TELLER',TXN.ID,MAT APP.REC,SIZE,VF.TELLER,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            APPLICATION = 'TELLER' ; ID.NEW = COMI ; V$FUNCTION = 'I'
            IF R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VERSION.NAME>[1,1] # ',' THEN
                VER.NAME = "TELLER,":R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VERSION.NAME>
            END
            ELSE
                VER.NAME = "TELLER":R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VERSION.NAME>
            END
*TEXT = 'VER=':VER.NAME ; CALL REM
            GOSUB GET.DSF.NAMES.ARRAY
        END
    END
    ELSE
        VF.TELLER$HIS = '' ; CALL OPF('F.TELLER$HIS', VF.TELLER$HIS)

*         TXN.ID := ';1'
        ETEXT = '' ; DIM APP.REC(TT.TE.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(TT.TE.AUDIT.DATE.TIME) ; MAT R.SAVE = ''
        SIZE = TT.TE.AUDIT.DATE.TIME
*** TEXT = 'SIZE=':SIZE ; CALL REM
*** TEXT = 'TXN.ID=':TXN.ID ; CALL REM
        CALL F.MATREAD('F.TELLER$HIS',TXN.ID,MAT APP.REC,SIZE,VF.TELLER$HIS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            IF R.NEW(TT.TE.RECORD.STATUS) # 'REVE' THEN
                APPLICATION = 'TELLER' ; ID.NEW = COMI ; V$FUNCTION = 'I'
***TEXT = 'VN=':R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VERSION.NAME> ; CALL REM
                VER.NAME = APPLICATION:R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VERSION.NAME>
***TEXT = 'VN2=':VER.NAME ; CALL REM
                PARM.ID = 'TELLER,'
                PARM.ID := R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VERSION.NAME>
*TEXT = "PAR.ID=":PARM.ID ; CALL REM
*TEXT = "VER.NH=":VER.NAME ; CALL REM
                UPDATE.REC = 'F':R.COMPANY(EB.COM.MNEMONIC):'.TELLER$HIS'
                UPDATE.TS = 'YES'
                GOSUB GET.DSF.NAMES.ARRAY
            END
            ELSE
                ER.MSG = 'INVALID RECORD STATUS FOR REPRINT'
            END
        END
    END
    RETURN
*TEXT = VER.NAME ; CALL REM
*=============================================================================*
REPRINT.SLIP:
    IF DSF.CNT >= 1 THEN
        FOR DSF.LP = 1 TO DSF.CNT
***TEXT = 'TR=':TRIM(R.DSF<DSF.LP>) ; CALL REM
            CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP>))
        NEXT DSF.LP
    END
    ELSE
        ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
    END
    RETURN
*=============================================================================*
GET.DSF.NAMES.ARRAY:
    IF VER.NAME THEN
        R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 164 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 165 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
R.DSF=R.ITSS.VERSION<EB.VER.D.SLIP.FORMAT>
*TEXT = "R.DSF=":R.DSF ; CALL REM
***TEXT = 'R.DSF=' :R.DSF ; CALL REM
        IF NOT(ETEXT) THEN
*         TEXT = "ARRAY = ":R.DSF ; CALL REM
            DSF.CNT = DCOUNT(R.DSF,@VM)
***TEXT = "DSF COUNT = ":DSF.CNT  ; CALL REM
        END
        ELSE
            ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
        END
    END

*=================== SUBROUTINE END ... YES !!! ==============================*
PROGRAM.END:
    RETURN
END
