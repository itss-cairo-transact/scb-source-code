* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNjUxNDg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>800</Rating>
* This routine is used to perform the below cases
* If the LIMIT records ( Self liability, without liability and different liability)
* contains the incorrect value in the fields COMMT.AVAIL.AMT,
* CMMT.CCY and CMTT.AMT, AVAIL.AMT then system will remove the values from the fields
* COMMT.AMT.AVAIL.COMMITM.CCY.COMMITM.AMT and rebuild the LIMIT records with correct
* values
* Also, if the commitment type LD matures and LIMIT record contains the value
* in the field CMMT.AVAIL.AMT then system will remove the values in LIMIT record.
* Correction routine prepared for the issue HD1047654-GBI-LIMIT
* Modification has been done in the LOCATE statements, as it is not working fine.
* Instead of LOCATE, FINDSTR statements have been introduced.
*-----------------------------------------------------------------------------
    SUBROUTINE TEST.COMMIT.MOD
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT.REFERENCE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    EXECUTE "COMO ON TEST.COMMIT.LOG"
    GOSUB INIT
    GOSUB OPENFILES

    GOSUB COMMIT.PROCESS
    GOSUB PROCESS
    GOSUB MAT.LD
    EXECUTE "COMO OFF TEST.COMMIT.LOG"
    RETURN

INIT:

    FN.LD="F.LD.LOANS.AND.DEPOSITS"
    F.LD=""
    FN.LIMIT="F.LIMIT"
    F.LIMIT=""
    FN.LD.CMT="F.LD.COMMITMENT"
    F.LD.CMT=""
    FN.LMM.CUS="F.LMM.CUSTOMER"
    F.LMM.CUS=""
    FN.LIMIT.REFERENCE="F.LIMIT.REFERENCE"
    F.LIMIT.REFERENCE=""
    P.ID= ''
    LIM.ID=''
    PROCESSED.IDS=''
    LIMIT.PRODUCT=''
    YAMT.TO = ''
    LIM.CCY = ''
    A = ''
    B = ''
    P.ID = ''
    R.LIMIT = ''
    X = 1
    LD.REC.CCY = ''
    LD.REC.AMT = ''
    CMT.LD.AMT = ''
    COMMT.AVAIL.AMT = ''
    LD.COM.ARR = ''
    LD.COM.VAR = ''
    RESULT = ''
    LD.AMT.ARR = ''
    LD.CCY.ARR = ''
    LIM.IDS.ARR = ''
    PROC.ID = ''
    LIM.ID.1 = ''
    CMT.AVL.AMT = ''
    ONLI.LIM = ''
    CMT.LD.IDS = ''
    LMM.POS.2=''
    ARR.POS = ''
    LD.COM = 1
    PROCESS.ID = ''
    LIM.ERR = ''
    LD.COM.POS2 = ''
    LD.COM.POS3 = ''
    LIM.REC = ''
    LIM.ID.ARR = ''
    LI.ID = ''
    REC.LIMIT = ''
    LIM.REC.LIST = ''
    LIMIT.POS1=''
    LP.PER = ''
    LP =''
    LP.PERC = ''
    LR.PER=''
    LIM.REF1=''
    LIM.REF2=''
    RETURN


OPENFILES:
    CALL OPF(FN.LIMIT,F.LIMIT)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.CMT,F.LD.CMT)
    CALL OPF(FN.LMM.CUS,F.LMM.CUS)
    CALL OPF(FN.LIMIT.REFERENCE,F.LIMIT.REFERENCE)
    RETURN

*******************************************************
* In the PROCESS para, system read the LIMIT records and clear all the
* commitment related fields and re update the correct commitment amount
* in the LIMIT record.
*
**********************************************************************

PROCESS:

    LOOP
        REMOVE LIM.REC.ID FROM LD.COM.ARR SETTING LI.POS
    WHILE LIM.REC.ID NE '' DO
        A1 = LIM.REC.ID
        LIM.REC=FIELD(A1,'*',1)
        LIM.ID.ARR <-1> = LIM.REC
        LD.REC.CCY = FIELD(A1,'*',3)
        LD.REC.AMT = FIELD(A1, '*',2)
        CALL F.READ(FN.LIMIT,LIM.REC,R.LIMIT,F.LIMIT,LIM.ERR)
        IF LIM.ERR EQ '' THEN
            LOCATE LIM.REC IN PROC.ID SETTING POS1 ELSE
                PROC.ID<-1> = LIM.REC
                R.LIMIT<LI.COMMITM.CCY> =''
                R.LIMIT<LI.COMMITM.AMT> =''
                R.LIMIT<LI.COMMT.AMT.AVAIL> =''
                R.LIMIT<LI.AVAIL.AMT>=''
            END
        END
        LOCATE LD.REC.CCY IN R.LIMIT<LI.COMMITM.CCY,1,1> SETTING CMMT.LD.LD.POS THEN
            R.LIMIT<LI.COMMITM.AMT,1,CMMT.LD.LD.POS> = R.LIMIT<LI.COMMITM.AMT,1,CMMT.LD.LD.POS> + LD.REC.AMT
            IF LD.REC.CCY NE R.LIMIT<LI.LIMIT.CURRENCY> THEN
                CALL LIMIT.CURR.CONV(LD.REC.CCY,LD.REC.AMT,R.LIMIT<LI.LIMIT.CURRENCY>, CMT.AVL.AMT,'')
                R.LIMIT<LI.COMMT.AMT.AVAIL> = R.LIMIT<LI.COMMT.AMT.AVAIL> + CMT.AVL.AMT
            END ELSE
                R.LIMIT<LI.COMMT.AMT.AVAIL> = R.LIMIT<LI.COMMT.AMT.AVAIL> + LD.REC.AMT
            END
        END ELSE
            INS LD.REC.CCY BEFORE  R.LIMIT<LI.COMMITM.CCY,1,-1>
            INS LD.REC.AMT BEFORE  R.LIMIT<LI.COMMITM.AMT,1,-1>
            IF LD.REC.CCY NE R.LIMIT<LI.LIMIT.CURRENCY> THEN
                CALL LIMIT.CURR.CONV(LD.REC.CCY,LD.REC.AMT,R.LIMIT<LI.LIMIT.CURRENCY>, CMT.AVL.AMT,'')
                R.LIMIT<LI.COMMT.AMT.AVAIL> = R.LIMIT<LI.COMMT.AMT.AVAIL> + CMT.AVL.AMT
            END ELSE
                R.LIMIT<LI.COMMT.AMT.AVAIL> = R.LIMIT<LI.COMMT.AMT.AVAIL> + LD.REC.AMT
            END
        END
        ONLI.LIM = R.LIMIT<LI.ONLINE.LIMIT,1>
        IF ONLI.LIM THEN
            R.LIMIT<LI.AVAIL.AMT,1> = R.LIMIT<LI.ONLINE.LIMIT,1> + R.LIMIT<LI.SUB.ALLOC.TAKEN,1> + R.LIMIT<LI.SUB.ALLOC.GIVEN,1> + R.LIMIT<LI.TOTAL.OS,1> + R.LIMIT<LI.COMMT.AMT.AVAIL>
        END
        CALL F.WRITE(FN.LIMIT,LIM.REC,R.LIMIT)
        CALL JOURNAL.UPDATE(LIM.REC)
        LD.REC.CCY = ''
        LD.REC.AMT = ''
        COMMT.AVAIL.AMT = ''
    REPEAT
    RETURN

**********************************************************************
*
* In the COMMIT.PROCESS para, system will select the LMM.CUSTOMER file and read
* LD contracts. If the LD contract is commitment type ( 21095 -21099) then system
* will read the COMMITMENT type LD contracts
*
**********************************************************************

COMMIT.PROCESS:


    SEL.LD = " SELECT " : FN.LMM.CUS
    CALL EB.READLIST(SEL.LD,LMM.LIST,'','','')
    LOOP
        REMOVE CUS.ID FROM LMM.LIST SETTING LMM.POS.1
    WHILE CUS.ID:LMM.POS.2
        CALL F.READ(FN.LMM.CUS,CUS.ID,R.LMM.CUS,F.LMM.CUS,LMM.ERR)
        IF R.LMM.CUS THEN
*Line [ 197 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            NO.OF.LD.IDS = DCOUNT(R.LMM.CUS, @FM)
            FOR I = 1 TO NO.OF.LD.IDS
                LD.ID = R.LMM.CUS<I>
                IF LD.ID[1,2] NE 'LD' THEN CONTINUE
                CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,LD.ERR)
                IF LD.ERR EQ '' THEN
                    CATG.NO = R.LD<LD.CATEGORY>
                    CMT.NO = R.LD<LD.COMMITMENT.NO>
                    IF CATG.NO GE 21095 AND CATG.NO LE 21099 THEN
                        GOSUB COMMT.LD
                    END
                END ELSE
                    CRT "MISSING LD : " : LD.ID : "   " : LD.ERR
                END
            NEXT I
        END
    REPEAT
    RETURN

**********************************************************************
* In the COMMT.LD para, system collects all the commitment type LD contracts
* in the array and process the LD records one by one
* System read the LIMIT.REF field, customer from LD contract to make the LIMIT record
* In order to identify the revolving and non-revolving type of LD contract we are taking
* the value of the field LIMIT.REF from the LD contract
* It collects the details of LIMIT.ID, LD.CCY and COMMIT.AVAIL.AMT
*
* Included the additional check when the LIMIT.REFERENCE record contains the LIMIT.PERCENTAGE
* other than 100
**********************************************************************

COMMT.LD:

    P.ID = ''
    PROCESS.ID = ''
    LR.PER = ''
    LR.PERC = ''
    LD.COM = 1
    CMT.LD.IDS<-1> = LD.ID
    IF R.LD<LD.COMMT.AVAIL.AMT> > 0 THEN
        LD.CCY = R.LD<LD.CURRENCY>
        LD.AMT = R.LD<LD.COMMT.AVAIL.AMT>*-1
        CUS.ID = R.LD<LD.CUSTOMER.ID>
        LIM.REF1 = R.LD<LD.LIMIT.REFERENCE>
        LIM.REF2 = FIELD(LIM.REF1,'.',1)
        LIM.REF = LIM.REF2[4]
        LIM.REF = TRIM(LIM.REF,'0',"L")
* TEST
*Line [ 246 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 247 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR ("LIMIT.REFERENCE":@FM:LI.REF.LIMIT.PERCENTAGE,LIM.REF,LR.PERC)
F.ITSS.LIMIT.REFERENCE = 'F.LIMIT.REFERENCE'
FN.F.ITSS.LIMIT.REFERENCE = ''
CALL OPF(F.ITSS.LIMIT.REFERENCE,FN.F.ITSS.LIMIT.REFERENCE)
CALL F.READ(F.ITSS.LIMIT.REFERENCE,LIM.REF,R.ITSS.LIMIT.REFERENCE,FN.F.ITSS.LIMIT.REFERENCE,ERROR.LIMIT.REFERENCE)
LR.PERC=R.ITSS.LIMIT.REFERENCE<LI.REF.LIMIT.PERCENTAGE>
        LR.PER = LR.PERC
        LD.AMT = LD.AMT * (LR.PER/100)
*Line [ 250 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 257 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR("CUSTOMER":@FM:EB.CUS.CUSTOMER.LIABILITY,CUS.ID,LIAB.NO)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LIAB.NO=R.ITSS.CUSTOMER<EB.CUS.CUSTOMER.LIABILITY>
        LIAB.ID = LIAB.NO
        LIM.REF = FMT(LIM.REF1,"10'0'R")

******** Update the COMMITMENT amount for self-liability LIMIT records
        IF ( LIAB.ID NE '' ) AND ( LIAB.ID EQ CUS.ID ) THEN
            LIM.ID = LIAB.ID : "." : LIM.REF :".":CUS.ID
            A = LD.AMT:"*":LD.CCY:"*":LD.ID
            LD.COM.VAR = LIM.ID:"*":A
            P.ID<-1> = LIM.ID
            LD.COM.ARR<-1> = LD.COM.VAR
            GOSUB LI.FRAME.PROCESS
            LIM.ID = CUS.ID :".": LIM.REF
*LD.AMT = LD.AMT * (LR.PER/100)
            A = LD.AMT:"*":LD.CCY:"*":LD.ID
            LD.COM.VAR = LIM.ID:"*":A
* TEST P.ID
            P.ID<-1> = LIM.ID

* To avoid the update of COMMIT.AMT for 2 times in the LIMIT record we need to check the
* array variable LD.COM.ARR for finding the already processed LIMIT record id

*LOCATE LD.COM.VAR IN LD.COM.ARR SETTING LD.COM.POS THEN
*   LD.COM = 0
*END
            FINDSTR LD.COM.VAR IN LD.COM.ARR SETTING LD.COM.POS THEN
                LD.COM = 0
            END
            IF LD.COM THEN
                LD.COM.ARR<-1> = LD.COM.VAR
                P.ID<-1> = LIM.ID
                GOSUB LI.FRAME.PROCESS
            END ELSE
                LD.COM = 0
            END
        END
* END

********* Update the COMMITMENT amount for different -liability LIMIT records
        LD.COM = 1
        IF ( LIAB.ID NE '' ) AND ( LIAB.ID NE CUS.ID ) THEN
            LIM.ID = LIAB.ID : "." : LIM.REF :".":CUS.ID
*LD.AMT = LD.AMT * (LR.PER/100)
            A = LD.AMT:"*":LD.CCY:"*":LD.ID
            LD.COM.VAR = LIM.ID:"*":A
            P.ID<-1> = LIM.ID
            LD.COM.ARR<-1> = LD.COM.VAR
            GOSUB LI.FRAME.PROCESS
            LIM.ID = LIAB.ID : "." : LIM.REF
*LD.AMT = LD.AMT * (LR.PER/100)
            A = LD.AMT:"*":LD.CCY:"*":LD.ID
            LD.COM.VAR = LIM.ID:"*":A
            P.ID<-1> = LIM.ID
*LOCATE LD.COM.VAR IN LD.COM.ARR SETTING LD.COM.POS2 THEN
*   LD.COM = 0
*END

            FINDSTR LD.COM.VAR IN LD.COM.ARR SETTING LD.COM.POS2 THEN
                LD.COM = 0
            END
            IF LD.COM THEN
                LD.COM.ARR<-1> = LD.COM.VAR
                P.ID<-1> = LIM.ID
                GOSUB LI.FRAME.PROCESS
            END ELSE
                LD.COM = 0
            END
        END
*END

***************** Update the COMMITMENT amount without liability LIMIT records
        LD.COM = 1
        IF ( CUS.ID NE '' ) AND ( LIAB.ID EQ '' ) THEN
            LIM.ID = CUS.ID :".": LIM.REF
*LD.AMT = LD.AMT * (LR.PER/100)
            A = LD.AMT:"*":LD.CCY:"*":LD.ID
            LD.COM.VAR = LIM.ID:"*":A
            LD.COM.ARR<-1> = LD.COM.VAR
            P.ID<-1>= LIM.ID
            GOSUB LI.FRAME.PROCESS
            LIM.ID = CUS.ID : "." : LIM.REF
            LD.AMT = LD.AMT * (LR.PER/100)
*A = LD.AMT:"*":LD.CCY:"*":LD.ID
            LD.COM.VAR = LIM.ID:"*":A
            P.ID<-1> = LIM.ID
*LOCATE LD.COM.VAR IN LD.COM.ARR SETTING LD.COM.POS3 THEN
*   LD.COM = 0
*END
            FINDSTR LD.COM.VAR IN LD.COM.ARR SETTING LD.COM.POS3 THEN
                LD.COM = 0
            END
            IF LD.COM THEN
                LD.COM.ARR<-1> = LD.COM.VAR
                P.ID<-1> = LIM.ID
                GOSUB LI.FRAME.PROCESS
            END ELSE
                LD.COM = 0
            END
        END
    END
    RETURN



***********************************************************
* In the LI.FRAME.PROCESS para, system reads the RECORD.PARENT of
* LIMIT record to update the LD commitment amount for the parent record also
*
**********************************************************************

LI.FRAME.PROCESS:


    LOOP
        REMOVE LIM.ID1 FROM P.ID SETTING LIM.POS
*Line [ 366 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 379 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR ("LIMIT":@FM:LI.RECORD.PARENT,LIM.ID1,REC.PAR)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID1,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
REC.PAR=R.ITSS.LIMIT<LI.RECORD.PARENT>
    WHILE REC.PAR NE '' DO
        LOCATE REC.PAR IN PROCESS.ID SETTING REC.POS ELSE
            LD.COM.VAR = REC.PAR : "*" :A
            LD.COM.ARR<-1> = LD.COM.VAR
            PROCESS.ID<-1> = REC.PAR
            LIM.ID1 = ''
            LIM.ID1 = REC.PAR
            IF LIM.ID1 NE '' THEN
                GOSUB REC.PAR
            END
        END
        P.ID = ''
    REPEAT
    RETURN

******  Read the LIMIT record to find out the parent LIMIT

REC.PAR:

*Line [ 387 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 406 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ("LIMIT":@FM:LI.RECORD.PARENT,LIM.ID1,REC.PAR)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID1,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
REC.PAR=R.ITSS.LIMIT<LI.RECORD.PARENT>
    IF REC.PAR NE '' THEN
        LOCATE REC.PAR IN PROCESS.ID SETTING REC.POS ELSE
            LD.COM.VAR = REC.PAR : "*" :A
            LD.COM.ARR<-1> = LD.COM.VAR
            PROCESS.ID<-1> = REC.PAR
            LIM.ID1 = ''
            LIM.ID1 = REC.PAR
            IF LIM.ID1 NE '' THEN
                GOSUB REC.PAR
            END
        END
    END

    RETURN


****************************************************************************************************
* This para is used to find out whether the LIMIT record contains the COMMIT.AMT, CMT.CCY
* if the commitment LD becomes matured
**********************************************************************

MAT.LD:

    SMD.LIST="SELECT ":FN.LIMIT:" WITH COMMT.AMT.AVAIL NE '' OR COMMITM.CCY NE '' OR COMMITM.AMT NE ''"
    CALL EB.READLIST(SMD.LIST,LIM.REC.LIST,'','','')
    LOOP
        REMOVE LI.ID FROM LIM.REC.LIST SETTING LIM.POS1
    WHILE LI.ID:LIMIT.POS1
        CALL F.READ(FN.LIMIT,LI.ID,REC.LIMIT,F.LIMIT,LI.ERR)
        IF LI.ERR EQ '' THEN
            LOCATE LI.ID IN LIM.ID.ARR SETTING MAT.LD.POS ELSE
                REC.LIMIT<LI.COMMITM.CCY>=''
                REC.LIMIT<LI.COMMITM.AMT>=''
                REC.LIMIT<LI.COMMT.AMT.AVAIL>=''
                CALL F.WRITE(FN.LIMIT,LI.ID,REC.LIMIT)
                CALL JOURNAL.UPDATE(LI.ID)
            END
        END
    REPEAT
    RETURN 
