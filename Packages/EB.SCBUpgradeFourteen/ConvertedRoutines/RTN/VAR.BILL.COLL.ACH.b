* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0Mjk3OTI6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*WAGDY-----------------------------------------------------------------------------
* <Rating>-78</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BILL.COLL.ACH

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    FN.BR ='FBNK.BILL.REGISTER' ;R.BR ='';F.BR =''
    CALL OPF(FN.BR,F.BR)
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='INAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        ZFLAG = 0
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                GOSUB COLLECT
                GOSUB DR.CHRG
            END

            IF (R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                ZFLAG = ZFLAG + 1
            END

        NEXT NUMBERBR.ID

        IF ZFLAG EQ 0 THEN
            GOSUB KHASM.3ALA.MARKAZE
        END

    END
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='RNAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 77 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        KFLAG = 0
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN

                GOSUB COLLECT.REV
                GOSUB DR.CHRG.REV
            END
            IF (R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                KFLAG = KFLAG + 1
            END

        NEXT NUMBERBR.ID

        IF KFLAG EQ 0 THEN
            GOSUB KHASM.3ALA.MARKAZE.REV
            TEXT = "MARKAZE REVERCE IS OK " ; CALL REM
        END
    END
    RETURN
*---------
COLLECT:
*---------
    CURR =   R.BR<EB.BILL.REG.CURRENCY>
*********************************
* EDAFA LE EL 3AMIL KEMET EL BR *
*********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    Y.AMT  = R.BR<EB.BILL.REG.AMOUNT>
    W.OUR.REF = BR.ID
    T.CODE = '993'
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>

*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY
    TEXT = "BR VALUE WAS ADDED TO CUSTOMER ACCOUNT" ; CALL REM
    RETURN
**********************************
*----
KHASM.3ALA.MARKAZE:
*----
****************************************************
* KHASM MEN EL MARKAZE
****************************************************
    CHRG.AMT = R.NEW(SCB.BT.TOT.CHRG.AMT)
*    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>
    Y.ACCT = "9943330010509301"
    Y.AMT  = ( R.NEW(SCB.BT.TOT.DEBIT.AMT) - CHRG.AMT ) * -1
    W.OUR.REF = ID.NEW
    T.CODE = '993'
*Line [ 143 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY
    TEXT = "BR VALUE - CHARGE WAS TAKEN FORM CENTRAL BANK" ; CALL REM
    RETURN
*****************************************************
*----
DR.CHRG:
*----
********************************
* KHASM MEN EL 3AMEL EL CHARGE *
********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    X.CHRG.AMT = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID>
    W.OUR.REF = BR.ID
    T.CODE = '995'
    IF X.CHRG.AMT NE '' AND X.CHRG.AMT NE 0 THEN
        Y.AMT  = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID> * -1
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        GOSUB AC.STMT.ENTRY
        TEXT = "CHARGE WAS TAKEN FROM CUSTOMER" ; CALL REM
    END

    RETURN
*******************************  REVERCE  ********************************
*---------
COLLECT.REV:
*---------
    CURR =   R.BR<EB.BILL.REG.CURRENCY>
*----
* CR
*********************************
* EDAFA LE EL 3AMIL KEMET EL BR *
*********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    Y.AMT  = R.BR<EB.BILL.REG.AMOUNT> * -1
    W.OUR.REF = BR.ID
    T.CODE = '993'
*Line [ 192 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>

*Line [ 200 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY
    RETURN
**********************************
*----
KHASM.3ALA.MARKAZE.REV:
*----
****************************************************
* KHASM MEN EL MARKAZE
****************************************************
    CHRG.AMT = R.NEW(SCB.BT.TOT.CHRG.AMT)
*    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>
    Y.ACCT = "9943330010509301"
    W.OUR.REF = ID.NEW
    T.CODE = '993'
    TEXT = "CHARGE IS " : CHRG.AMT ; CALL REM
    Y.AMT  = ( R.NEW(SCB.BT.TOT.DEBIT.AMT) - CHRG.AMT )
*Line [ 223 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY
    RETURN
*****************************************************
*----
DR.CHRG.REV:
*----
********************************
* KHASM MEN EL 3AMEL EL CHARGE *
********************************
    Y.ACCT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    X.CHRG.AMT = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID>
    W.OUR.REF = BR.ID
    T.CODE = '995'
    IF X.CHRG.AMT NE '' AND X.CHRG.AMT NE 0 THEN
        Y.AMT  = R.NEW(SCB.BT.CHRG.AMT)<1,NUMBERBR.ID>
*Line [ 245 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        GOSUB AC.STMT.ENTRY
    END

    RETURN
**************************************************************************
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = T.CODE
    ENTRY<AC.STE.THEIR.REFERENCE>  = R.NEW(SCB.BT.CBE.REF)
    ENTRY<AC.STE.TRANS.REFERENCE>  = W.OUR.REF
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = W.OUR.REF
    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
