* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNjUwNDc6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
***NESSREEN AHMED************
*-----------------------------------------------------------------------------
* <Rating>901</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TELLER.TOT.CD.DR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 45 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 46 ] Adding EB_SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-19
*Line [ 47 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='TELLER.TOT.CD.DR'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    F.TELLER = '' ; FN.TELLER = 'F.TELLER' ; R.TELLER = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.TELLER,F.TELLER)

    XX = ''
    ZZ = ''

    VOPEN.BAL = ''
    COMP = C$ID.COMPANY
**   TEXT = 'COMP=': COMP ; CALL REM
****UPDATED ON 18/11/2008*******************************
**  VLT.ACCT = "EGP100000199"
    VLT.ACCT = "EGP1000001990001"
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, VLT.ACCT , VOPEN.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,VLT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
    XX<1,ZZ>[15,16] = VOPEN.BAL
    PRINT XX<1,ZZ>
    XX = ''
    ZZ = ZZ+1
*****UPDATED BY NESSREEN AHMED ON 04/02/2009*************************
** T.SEL = "SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 9 OR TRANSACTION.CODE EQ 10 OR TRANSACTION.CODE EQ 62 OR TRANSACTION.CODE EQ 42 OR TRANSACTION.CODE EQ 89 OR TRANSACTION.CODE EQ 37 OR TRANSACTION.CODE EQ 38 OR TRANSACTION.CODE EQ 7 OR TRANSACTION.CODE EQ 32 OR TRANSACTION.CODE EQ 33 OR TRANSACTION.CODE EQ 111 OR TRANSACTION.CODE EQ 101 OR TRANSACTION.CODE EQ 102 OR TRANSACTION.CODE EQ 24 OR TRANSACTION.CODE EQ 5 OR TRANSACTION.CODE EQ 103 ) AND CURRENCY.2 EQ EGP AND TELLER.ID.1 NE 9999 BY CURRENCY.2 BY TELLER.ID.1 "
***   T.SEL = "SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 9 OR TRANSACTION.CODE EQ 10 OR TRANSACTION.CODE EQ 62 OR TRANSACTION.CODE EQ 42 OR TRANSACTION.CODE EQ 89 OR TRANSACTION.CODE EQ 37 OR TRANSACTION.CODE EQ 38 OR TRANSACTION.CODE EQ 7 OR TRANSACTION.CODE EQ 32 OR TRANSACTION.CODE EQ 33 OR TRANSACTION.CODE EQ 111 OR TRANSACTION.CODE EQ 101 OR TRANSACTION.CODE EQ 102 OR TRANSACTION.CODE EQ 24 OR TRANSACTION.CODE EQ 5 OR TRANSACTION.CODE EQ 103 ) AND CURRENCY.2 EQ EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ ": COMP :" BY CURRENCY.2 BY TELLER.ID.1 "
    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN (9 10 62 42 89 37 38 7 32 33 111 101 102 24 5 103 ) AND CURRENCY.2 EQ EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ ": COMP :" BY CURRENCY.2 BY TELLER.ID.1 "
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
** TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF SELECTED THEN
        TOT.LCY = '' ; LCY.AMT = '' ; CURR1 = ''  ; TELL.ID = '' ; ACCT = ''  ; VALDAT2 = ''
        TOT.LCY.ALL = ''
        CALL F.READ(FN.TELLER,KEY.LIST<1>,R.TELLER,F.TELLER,E1)
        ACCT<1> = R.TELLER<TT.TE.ACCOUNT.2>
        LCY.AMT<1> = R.TELLER<TT.TE.AMOUNT.LOCAL.2>
        CURR1<1>  = R.TELLER<TT.TE.CURRENCY.2>
        TELL.ID<1> = R.TELLER<TT.TE.TELLER.ID.1>
        VALDAT2<1> = R.TELLER<TT.TE.VALUE.DATE.2>
        FMTDAT = VALDAT2<1>[7,2]:'/':VALDAT2<1>[5,2]:"/":VALDAT2<1>[1,4]
        TOT.LCY = TOT.LCY+LCY.AMT<1>
        FOR I =2 TO SELECTED
* OPEN.BAL = '' ; CLOS.BAL = ''
            CALL F.READ(FN.TELLER,KEY.LIST<I>,R.TELLER,F.TELLER,E1)
            ACCT<I> = R.TELLER<TT.TE.ACCOUNT.2>
            LCY.AMT<I> = R.TELLER<TT.TE.AMOUNT.LOCAL.2>
            CURR1<I>  = R.TELLER<TT.TE.CURRENCY.2>
            TELL.ID<I> = R.TELLER<TT.TE.TELLER.ID.1>
            VALDAT2<I> = R.TELLER<TT.TE.VALUE.DATE.2>
            FMTDAT = VALDAT2<I>[7,2]:'/':VALDAT2<I>[5,2]:"/":VALDAT2<I>[1,4]
            IF TELL.ID<I> # TELL.ID<I-1> THEN
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT<I-1> , OPEN.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT<I-1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
OPEN.BAL=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR( 'ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL, ACCT<I-1> , CLOS.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT<I-1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CLOS.BAL=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
                XX<1,ZZ>[3,30] = CURR1<I-1>
                XX<1,ZZ>[15,16] = OPEN.BAL
                XX<1,ZZ>[45,16] = TOT.LCY
                XX<1,ZZ>[84,16] = TOT.LCY
                XX<1,ZZ>[105,10] = FMTDAT
                XX<1,ZZ>[120,4] = TELL.ID<I-1>
                PRINT XX<1,ZZ>

                TOT.LCY.ALL = TOT.LCY.ALL+TOT.LCY

                XX = ''
                TOT.LCY = ''
                ZZ = ZZ+1
                TOT.LCY = TOT.LCY+LCY.AMT<I>

            END ELSE
                TOT.LCY = TOT.LCY+LCY.AMT<I>

            END

            IF I = SELECTED THEN
*TEXT = 'I=':I ; CALL REM
                XX<1,ZZ>[3,30] = CURR1<I>
                XX<1,ZZ>[15,16] = OPEN.BAL
                XX<1,ZZ>[45,16] = TOT.LCY
                XX<1,ZZ>[84,16] = TOT.LCY
                XX<1,ZZ>[105,10] = FMTDAT
                XX<1,ZZ>[120,4] = TELL.ID<I>
* XX<1,ZZ>[120,16] = CLOS.BAL
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1

                TOT.LCY.ALL = TOT.LCY.ALL+TOT.LCY
            END
        NEXT I
        XX<1,ZZ>[45,16] = "--------------"
        PRINT XX<1,ZZ>
        XX = ''
        ZZ = ZZ+1
        XX<1,ZZ>[45,16] = TOT.LCY.ALL
        PRINT XX<1,ZZ>
        XX = ''
        ZZ = ZZ+1

    END
******************************************************************
****UPDATED BY NESSREEN AHMED ON 04/02/2009*******************************
**  N.SEL ="SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 2 OR TRANSACTION.CODE EQ 48 OR TRANSACTION.CODE EQ 41 OR TRANSACTION.CODE EQ 40 OR TRANSACTION.CODE EQ 86 OR TRANSACTION.CODE EQ 84 OR TRANSACTION.CODE EQ 82 OR TRANSACTION.CODE EQ 90 OR TRANSACTION.CODE EQ 91 OR TRANSACTION.CODE EQ 8 OR TRANSACTION.CODE EQ 36 OR TRANSACTION.CODE EQ 34 OR TRANSACTION.CODE EQ 100 OR TRANSACTION.CODE EQ 25 OR TRANSACTION.CODE EQ 23 OR TRANSACTION.CODE EQ 20 OR TRANSACTION.CODE EQ 21 OR TRANSACTION.CODE EQ 92 OR TRANSACTION.CODE EQ 93 OR TRANSACTION.CODE EQ 4 OR TRANSACTION.CODE EQ 17) AND CURRENCY.2 EQ EGP AND TELLER.ID.1 NE 9999 BY CURRENCY.2 BY TELLER.ID.1 "
**  N.SEL ="SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 2 OR TRANSACTION.CODE EQ 48 OR TRANSACTION.CODE EQ 41 OR TRANSACTION.CODE EQ 40 OR TRANSACTION.CODE EQ 86 OR TRANSACTION.CODE EQ 84 OR TRANSACTION.CODE EQ 82 OR TRANSACTION.CODE EQ 90 OR TRANSACTION.CODE EQ 91 OR TRANSACTION.CODE EQ 8 OR TRANSACTION.CODE EQ 36 OR TRANSACTION.CODE EQ 34 OR TRANSACTION.CODE EQ 100 OR TRANSACTION.CODE EQ 25 OR TRANSACTION.CODE EQ 23 OR TRANSACTION.CODE EQ 20 OR TRANSACTION.CODE EQ 21 OR TRANSACTION.CODE EQ 92 OR TRANSACTION.CODE EQ 93 OR TRANSACTION.CODE EQ 4 OR TRANSACTION.CODE EQ 17) AND CURRENCY.2 EQ EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ ": COMP :" BY CURRENCY.2 BY TELLER.ID.1 "
    N.SEL ="SELECT FBNK.TELLER WITH TRANSACTION.CODE IN ( 2 48 41 40 86 84 82 90 91 8 36 34 100 25 23 20 21 92 93 4 17 911 11 12 92 93 14 97 105 107 95 98 106 108 96 ) AND CURRENCY.2 EQ EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ ": COMP :" BY CURRENCY.2 BY TELLER.ID.1 "
    KEY.LIST.N ="" ; SELECTED.N="" ;  ER.MSG.N=""
    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
  **TEXT = 'SELECTED.N=':SELECTED.N ; CALL REM
    IF SELECTED.N THEN
        TOT.LCY.N = '' ; LCY.AMT.N = '' ; TOT.LCY.ALL.N = '' ; CURR1.N = '' ; ACCT.N = '' ; TELL.ID.N = '' ; VALDAT2.N = ''
*****************************
        CALL F.READ(FN.TELLER,KEY.LIST.N<1>,R.TELLER,F.TELLER,E1)
        ACCT.N<1> = R.TELLER<TT.TE.ACCOUNT.2>
        LCY.AMT.N<1> = R.TELLER<TT.TE.AMOUNT.LOCAL.2>
        CURR1.N<1>  = R.TELLER<TT.TE.CURRENCY.1>
        TELL.ID.N<1> = R.TELLER<TT.TE.TELLER.ID.1>
        VALDAT2.N<1> = R.TELLER<TT.TE.VALUE.DATE.2>
        FMTDAT.N = VALDAT2.N<1>[7,2]:'/':VALDAT2.N<1>[5,2]:"/":VALDAT2.N<1>[1,4]
        TOT.LCY.N = TOT.LCY.N+LCY.AMT.N<1>

*****************************
        FOR SS =2 TO SELECTED.N
            CALL F.READ(FN.TELLER,KEY.LIST.N<SS>,R.TELLER,F.TELLER,E1)
            ACCT.N<I> = R.TELLER<TT.TE.ACCOUNT.2>
            LCY.AMT.N<SS> = R.TELLER<TT.TE.AMOUNT.LOCAL.2>
            CURR1.N<SS>  = R.TELLER<TT.TE.CURRENCY.2>
            TELL.ID.N<SS> = R.TELLER<TT.TE.TELLER.ID.1>
            VALDAT2.N<SS> = R.TELLER<TT.TE.VALUE.DATE.2>
            FMTDAT.N = VALDAT2.N<SS>[7,2]:'/':VALDAT2.N<SS>[5,2]:"/":VALDAT2.N<SS>[1,4]
            IF TELL.ID.N<SS> # TELL.ID.N<SS-1> THEN

                XX<1,ZZ>[3,30] = CURR1.N<SS-1>
                XX<1,ZZ>[63,16] = TOT.LCY.N
                XX<1,ZZ>[84,16] = TOT.LCY.N
                XX<1,ZZ>[105,10] = FMTDAT.N
                XX<1,ZZ>[120,4] = TELL.ID.N<SS-1>
                PRINT XX<1,ZZ>

                TOT.LCY.ALL.N = TOT.LCY.ALL.N+TOT.LCY.N

                XX = ''
                TOT.LCY.N = ''
                ZZ = ZZ+1
                TOT.LCY.N = TOT.LCY.N+LCY.AMT.N<SS>

            END ELSE
                TOT.LCY.N = TOT.LCY.N+LCY.AMT.N<SS>

            END

            IF SS = SELECTED.N THEN
*TEXT = 'SS=':SS ; CALL REM
                XX<1,ZZ>[3,30] = CURR1.N<SS>
                XX<1,ZZ>[63,16] = TOT.LCY.N
                XX<1,ZZ>[84,16] = TOT.LCY.N
                XX<1,ZZ>[105,10] = FMTDAT.N
                XX<1,ZZ>[120,4] = TELL.ID.N<SS>
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1

                TOT.LCY.ALL.N = TOT.LCY.ALL.N+TOT.LCY.N
            END
        NEXT SS

        XX<1,ZZ>[63,16] = "--------------"
        PRINT XX<1,ZZ>
        XX = ''
        ZZ = ZZ+1
        XX<1,ZZ>[63,16] = TOT.LCY.ALL.N
        PRINT XX<1,ZZ>
        XX = ''
        ZZ = ZZ+1

    END
********************************************************************
****PRINT TOTAL ****************************************************
    VLTOPEN = '' ; VLTSUM = '' ; VLTCUBAL = ''
    VLTOPEN = ABS(VOPEN.BAL)
    VLTSUM  = VLTOPEN+TOT.LCY.ALL
    VLTCUBAL = SUBS(VLTSUM,TOT.LCY.ALL.N)
    XX<1,ZZ> = STR('_',125)
    PRINT XX<1,ZZ>
    XX = ''
    ZZ = ZZ+1
    XX<1,ZZ>[1,12] = '����� �������'
    XX<1,ZZ>[15,16] = VLTOPEN
    XX<1,ZZ>[45,16] = TOT.LCY.ALL
    XX<1,ZZ>[63,16] = TOT.LCY.ALL.N
    XX<1,ZZ>[84,16] = VLTCUBAL
    PRINT XX<1,ZZ>
    XX = ''
    ZZ = ZZ+1
    XX<1,ZZ> = STR('_',125)
    PRINT XX<1,ZZ>
    XX = ''
    ZZ = ZZ+1


********************************************************************
    TEXT = "��� �������" ; CALL REM
********************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 276 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):" ������ ������ � ������ ������ ������� ���� �����"
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"������":SPACE(35):"������ ������":SPACE(05):"������ �������":SPACE(05):"������ ������":SPACE(10):"����� �������":SPACE(04):"��� ������"
    PR.HD :="'L'":SPACE(1):STR('_',6):SPACE(33):STR('_',16):SPACE(04):STR('_',15):SPACE(04):STR('_',14):SPACE(09):STR('_',15):SPACE(04):STR('_',10)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(10):"������ ��������� ������� ��������"
    PR.HD :="'L'":SPACE(10):STR('_',26)
    HEADING PR.HD
    RETURN
*==============================================================

END
