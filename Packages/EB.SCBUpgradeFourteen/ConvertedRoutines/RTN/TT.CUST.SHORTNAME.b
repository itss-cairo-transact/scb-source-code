* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUyOTE0NTAwMDE6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:24:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
******NESSREEN AHMED*******
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
 SUBROUTINE TT.CUST.SHORTNAME(CUSNAM)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-19
$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*A Routine to select

* CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUSNAM,SHORTNAME)
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
* CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNAM,MYLOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSNAM,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
MYLOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
 ARNAME = MYLOCAL<1,CULR.ARABIC.NAME>
 CUSNAM = ARNAME

 RETURN
 END
