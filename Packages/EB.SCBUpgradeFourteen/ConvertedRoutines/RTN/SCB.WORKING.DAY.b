* @ValidationCode : MjotMTIwNjY2MDE2MzpDcDEyNTI6MTY0NTI5MTQ0NDY0Mzp1c2VyOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:24:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.WORKING.DAY(REQ.DATE,CURR)
************************************************************************
***********by Mahmoud Elhawary ** 3/8/2009 *****************************
************************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.HOLIDAY
************************************************************************
* convert the date required to the working day according to currency
************************************************************************

    D.TYP = ''
    D.TYP1= ''
    SSS   = 13
    TTT   = ''
    TDR   = ''
    TDM   = ''
    TDMM  = ''
    TTD   = ''
    TDY   = ''
    TDYY  = ''
    FDX   = ''
    CUR.HOL = ''
    FN.HOL = 'F.HOLIDAY' ; F.HOL = '' ; R.HOL = ''
    CALL OPF(FN.HOL,F.HOL)
    FN.HOL2 = 'F.HOLIDAY' ; F.HOL2 = '' ; R.HOL2 = ''
    CALL OPF(FN.HOL2,F.HOL2)
    FN.CURR = "FBNK.CURRENCY" ; F.CURR = '' ; R.CURR = ''
    CALL OPF(FN.CURR,F.CURR)

    TDR   = REQ.DATE
    TDY   = TDR[1,4]
    TDM   = TDR[5,2]
    TTD   = TDR[7,2]
    IF TTD[1,1] EQ '0' THEN
        TTT  = TTD[2,1]
    END ELSE
        TTT = TTD
    END
    TDYY = TDY
    TDMM = TDM
    IF TDMM[1,1] EQ '0' THEN
        TDMM  = TDM[2,1]
    END ELSE
        TDMM = TDM
    END

*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('CURRENCY':@FM:EB.CUR.COUNTRY.CODE,CURR,CN.CODE)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CN.CODE=R.ITSS.CURRENCY<EB.CUR.COUNTRY.CODE>
    CUR.HOL = CN.CODE:"00":TDY

    IF CURR EQ 'EGP' THEN
        HOL.ID  = "EG00":TDY
        HOL.ID2 = "EG00":TDY
    END ELSE
        HOL.ID  = "EG00":TDY
        HOL.ID2 = CUR.HOL
    END

    GOSUB WW.DAY

    IF REQ.DATE NE '' AND D.TYP NE '' AND D.TYP2 NE '' THEN
        LOOP
        WHILE D.TYP NE 'W' OR D.TYP2 NE 'W'
            D.TYP  = ''
            D.TYP2 = ''
            TTT = TTT + 1
            IF TTT GT 31 THEN
                TDMM = TDMM + 1
                TTT = 1
            END
            IF TDMM GT 12 THEN
                TTT = 1
                TDMM = 1
                TDYY = TDYY +1
                HOL.ID  = HOL.ID[1,4]:TDYY
                HOL.ID2 = HOL.ID2[1,4]:TDYY
            END
            GOSUB WW.DAY
        REPEAT
    END
    IF LEN(TDMM) EQ 1 THEN
        TDMM = "0":TDMM
    END
    IF LEN(TTT) EQ 1 THEN
        TTT = "0":TTT
    END
    REQ.DATE = TDYY:TDMM:TTT
RETURN
*--------------------------------------------------------
WW.DAY:
    FDX = TDMM + SSS
    CALL F.READ(FN.HOL,HOL.ID,R.HOL,F.HOL,ER.HOL)
    CALL F.READ(FN.HOL2,HOL.ID2,R.HOL2,F.HOL2,ER.HOL2)
    D.TYP  = R.HOL<FDX>[TTT,1]
    D.TYP2 = R.HOL2<FDX>[TTT,1]
RETURN
*--------------------------------------------------------
END
