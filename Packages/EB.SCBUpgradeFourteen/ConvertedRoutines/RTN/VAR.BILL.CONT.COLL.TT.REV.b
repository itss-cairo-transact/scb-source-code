* @ValidationCode : MjoxNzA3MzU4NzI6Q3AxMjUyOjE2NDUzNjIyNzI3ODc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 20 Feb 2022 15:04:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.BILL.CONT.COLL.TT.REV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.CUS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BL.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH.TT

    FN.BR ='FBNK.BILL.REGISTER' ;R.BR ='';F.BR =''
    CALL OPF(FN.BR,F.BR)
    IF V$FUNCTION   = 'I'  THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(BT.TT.OUR.REFERENCE),@VM)
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(BT.TT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            GOSUB COLLECT.REV

        NEXT NUMBERBR.ID
    END
    IF V$FUNCTION   = 'R' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 64 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(BT.TT.OUR.REFERENCE),@VM)
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(BT.TT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            GOSUB COLLECT

        NEXT NUMBERBR.ID
    END
RETURN
*----------
COLLECT:
*----------
    CATEG.CR = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>[11,4]
    CATEG.DR = "1":CATEG.CR
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CO.CODE,R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>,COMP.CO)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,BRLR.CONT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    COMP.CO=R.ITSS.ACCOUNT<@FM:AC.CO.CODE,R.BR<EB.BILL.REG.LOCAL.REF>>
    CO.CODE  = COMP.CO[6,4]
    CURR     = R.BR<EB.BILL.REG.CURRENCY>
    DR.ACCT  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>
    CR.ACCT  = CURR:CATEG.DR:"0001":CO.CODE

*-----
* CR
*-----
    IF CURR NE 'EGP' THEN
        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    END ELSE
        RATE    = ""
    END
    Y.ACCT = CR.ACCT
    AMT  = R.BR<EB.BILL.REG.AMOUNT>

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END

*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY

*-----
* DR
*-----
    Y.ACCT = DR.ACCT
    AMT  = R.BR<EB.BILL.REG.AMOUNT> * -1

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY

RETURN
*------------
COLLECT.REV:
*------------
    CATEG.CR = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>[11,4]
    CATEG.DR = "1":CATEG.CR
*Line [ 160 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CO.CODE,R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>,COMP.CO)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,BRLR.CONT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    COMP.CO=R.ITSS.ACCOUNT<@FM:AC.CO.CODE,R.BR<EB.BILL.REG.LOCAL.REF>>
    CO.CODE  = COMP.CO[6,4]
    CURR     = R.BR<EB.BILL.REG.CURRENCY>
    DR.ACCT  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>
    CR.ACCT  = CURR:CATEG.DR:"0001":CO.CODE

*----
* CR
*----
    IF CURR NE 'EGP' THEN
        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    END ELSE
        RATE    = ""
    END
    Y.ACCT = CR.ACCT
    AMT  = R.BR<EB.BILL.REG.AMOUNT> * -1

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END

*Line [ 183 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 190 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY

*----
* DR
*----
    Y.ACCT = DR.ACCT
    AMT    = R.BR<EB.BILL.REG.AMOUNT>

    IF CURR NE 'EGP' THEN
        LCY.AMT = AMT * RATE
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = AMT
    END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
    END

*Line [ 214 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    GOSUB AC.STMT.ENTRY

RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '790'
    ENTRY<AC.STE.THEIR.REFERENCE>  = BR.ID
    ENTRY<AC.STE.TRANS.REFERENCE>  = BR.ID
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = BR.ID

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

RETURN
END
