* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzMzM3MTY6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:58:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
***NESSREEN AHMED 24/12/2009******************************************
*-----------------------------------------------------------------------------
* <Rating>663</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SHARM.BUY.OFF.N

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 42 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 43 ] Adding EB_SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-19
*Line [ 44 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    TT.ID = '' ; STDAT = '' ; EDDAT = '' ; DX = '' ; NO.DD = ''
    IDTXT = "Enter the teller.id : "
    CALL TXTINP(IDTXT, 4, 22, "12", "A")
    TT.ID = COMI

    STTXT = "Enter the start date : "
    CALL TXTINP(STTXT, 8, 22, "12", "A")
    STDAT = COMI

    EDTXT = "Enter the end date : "
    CALL TXTINP(EDTXT, 8, 22, "12", "A")
    EDDAT = COMI

    OFF.ID = '' ; OFF.BR = '' ; OFF.NAME = ''
    CALL CDD(" ",STDAT,EDDAT,DX)
    TEXT = 'DX=':DX ; CALL REM
    NO.DD = DX + 1
** DATFMT = YYYY:"/":MM:"/":DD
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('TELLER.ID':@FM:TT.TID.TELLER.OFFICE,TT.ID,OFF.ID)
F.ITSS.TELLER.ID = 'FBNK.TELLER.ID'
FN.F.ITSS.TELLER.ID = ''
CALL OPF(F.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID)
CALL F.READ(F.ITSS.TELLER.ID,TT.ID,R.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID,ERROR.TELLER.ID)
OFF.ID=R.ITSS.TELLER.ID<TT.TID.TELLER.OFFICE>
*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OFF.ID,OFF.BR)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,OFF.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
OFF.BR=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    OFF.NAME = FIELD(OFF.BR,'.',2)

    F.TT= '' ; FN.TT = 'F.TELLER$HIS' ; R.TT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.TT,F.TT)

    GG = '' ; SS = '' ; PP = ''  ; YY = '' ; DESC.TOT = ''

    PRINT SPACE(40):"���� ��� ����  ":OFF.NAME:"�������� ������"
    PRINT SPACE(51):"������ ��������"
** PRINT SPACE(40):"-------------------------------------------------------"
    PRINT
    PRINT SPACE(45) : "��" :" ":STDAT[1,4]:"/":STDAT[5,2]:"/":STDAT[7,2]:"   ":"���" :" ":EDDAT[1,4]:"/":EDDAT[5,2]:"/":EDDAT[7,2]
    PRINT
    PRINT SPACE(1):"--------------------------------------------------------------------------------------------------------"
    PRINT SPACE(1):"|             |                                ������ ���� ������                                       |"
    PRINT SPACE(1):"|             |-----------------------------------------------------------------------------------------"
    PRINT SPACE(1):"|   �������   |      USD        |      EUR        |      GBP        |      CHF        |      SAR        |"
    PRINT SPACE(1):"|             |-----------------------------------------------------------------------------------------"
    PRINT SPACE(1):"|             |  NO. | AMOUNT   |  NO. | AMOUNT   |  NO. | AMOUNT   |  NO. | AMOUNT   |  NO. | AMOUNT   |"
    PRINT SPACE(1):"|-------------------------------------------------------------------------------------------------------"

*================================================================
    KEY.LIST = '' ; SELECTED = '' ; CURR.1 = '' ; AMTFCY = '' ; A.DATE = ''
    CURR.USD = '' ; AMT.USD = '' ; NO.USD = '' ; NFUSD = '' ; AFUSD = ''  ; AMT.USD.TOT = '' ; NO.USD.TOT = '' ; NFUSD.TOT = '' ; AFUSD.TOT = ''
    CURR.EUR = '' ; AMT.EUR = '' ; NO.EUR = '' ; NFEUR = '' ; AFEUR = ''  ; AMT.EUR.TOT = '' ; NO.EUR.TOT = '' ; NFEUR.TOT = '' ; AFEUR.TOT = ''
    CURR.GBP = '' ; AMT.GBP = '' ; NO.GBP = '' ; NFGBP = '' ; AFGBP = ''  ; AMT.GBP.TOT = '' ; NO.GBP.TOT = '' ; NFGBP.TOT = '' ; AFGBP.TOT = ''
    CURR.CHF = '' ; AMT.CHF = '' ; NO.CHF = '' ; NFCHF = '' ; AFCHF = ''  ; AMT.CHF.TOT = '' ; NO.CHF.TOT = '' ; NFCHF.TOT = '' ; AFCHF.TOT = ''
    CURR.SAR = '' ; AMT.SAR = '' ; NO.SAR = '' ; NFSAR = '' ; AFSAR = ''  ; AMT.SAR.TOT = '' ; NO.SAR.TOT = '' ; NFSAR.TOT = '' ; AFSAR.TOT = ''
    AUTHD = '' ; SS = 0 ; DAT.AUTH = '' ; XX = '' ; ZZ = '' ; DDC = 0

    CURRNO.U = '' ; CURRAMT.U = ''  ; CURRNO.U.TOT = '' ; CURRAMT.U.TOT = ''
    CURRNO.E = '' ; CURRAMT.E = ''  ; CURRNO.E.TOT = '' ; CURRAMT.E.TOT = ''
    CURRNO.G = '' ; CURRAMT.G = ''  ; CURRNO.G.TOT = '' ; CURRAMT.G.TOT = ''
    CURRNO.C = '' ; CURRAMT.C = ''  ; CURRNO.C.TOT = '' ; CURRAMT.C.TOT = ''
    CURRNO.S = '' ; CURRAMT.S = ''  ; CURRNO.S.TOT = '' ; CURRAMT.S.TOT = ''

    T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN ( 23 67 58) AND CO.CODE EQ ":COMP :" AND TELLER.ID.1 EQ ":TT.ID :" AND (AUTH.DATE GE ":STDAT :" AND AUTH.DATE LE ":EDDAT : ") BY DATE.TIME BY CURRENCY.1 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF SELECTED THEN

        FOR I = 1 TO SELECTED
            CURR.1 = '' ; AMTFCY = '' ;A.DATE = '' ; D.T = '' ; AUTH.D = ''
            FLAG = 0   ; FMT.AD = '' ; AD.YY = '' ; AD.MM = '' ; AD.DD = ''
            AUTHD.1 = ''
            CALL F.READ( FN.TT,KEY.LIST<I>, R.TT, F.TT,ETEXT)
            D.T    = R.TT<TT.TE.DATE.TIME>[1,6]
            AUTH.D = R.TT<TT.TE.AUTH.DATE>[3,6]

            IF D.T NE AUTH.D THEN
**TEXT = 'KEY=':KEY.LIST<I> ; CALL REM
                SS = SS + 1
                CURR.1<I> = R.TT<TT.TE.CURRENCY.1>
                AMTFCY<I> = R.TT<TT.TE.AMOUNT.FCY.1>
                A.DATE<I> = R.TT<TT.TE.AUTH.DATE>
                AUTHD<SS> = A.DATE<I>
                IF SS-1 # 0  THEN
                    FLAG = 1
                END ELSE
                    FLAG = 0
                END
                IF (AUTHD<SS> # AUTHD<SS-1>) AND FLAG = 1 THEN
                    AUTHD.1 = ''
                    GG = SS-1
                    DDC = DDC + 1

                    AUTHD.1 = AUTHD<GG>
                    AD.YY = AUTHD.1[1,4]
                    AD.MM = AUTHD.1[5,2]
                    AD.DD = AUTHD.1[7,2]
                    FMT.AD = AD.YY:"/":AD.MM:"/":AD.DD

                    DAT.AUTH<DDC,1> = " |": STR(" ",2):FMT.AD
                    IF NFUSD > '0' THEN
                        CURRNO.U<DDC,2>   = NFUSD
                        CURRAMT.U<DDC,3>  = AFUSD
                    END ELSE
                        CURRNO.U<DDC,2>   = STR(" ",5):'0'
                        CURRAMT.U<DDC,3>  = STR(" ",9):'0'
                    END
                    IF NFEUR > '0' THEN
                        CURRNO.E<DDC,4>   = NFEUR
                        CURRAMT.E<DDC,5>  = AFEUR
                    END ELSE
                        CURRNO.E<DDC,4>   = STR(" ",5):'0'
                        CURRAMT.E<DDC,5>  = STR(" ",9):'0'
                    END
                    IF NFGBP > '0' THEN
                        CURRNO.G<DDC,6>   = NFGBP
                        CURRAMT.G<DDC,7>  = AFGBP
                    END ELSE
                        CURRNO.G<DDC,6>   = STR(" ",5):'0'
                        CURRAMT.G<DDC,7>  = STR(" ",9):'0'
                    END

                    IF NFCHF > '0' THEN
                        CURRNO.C<DDC,8>   = NFCHF
                        CURRAMT.C<DDC,9>  = AFCHF
                    END ELSE
                        CURRNO.C<DDC,8>   = STR(" ",5):'0'
                        CURRAMT.C<DDC,9>  = STR(" ",9):'0'
                    END
                    IF NFSAR > '0' THEN
                        CURRNO.S<DDC,10>   = NFSAR
                        CURRAMT.S<DDC,11>  = AFSAR
                    END ELSE
                        CURRNO.S<DDC,10>   = STR(" ",5):'0'
                        CURRAMT.S<DDC,11>  = STR(" ",9):'0'
                    END

                    NO.USD.TOT = NO.USD.TOT + NO.USD
                    AMT.USD.TOT = AMT.USD.TOT + AMT.USD
                    NFUSD.TOT = STR(" ",6 - LEN(NO.USD.TOT)) : NO.USD.TOT
                    AFUSD.TOT = STR(" ",10 - LEN(AMT.USD.TOT)) :AMT.USD.TOT

                    NO.EUR.TOT = NO.EUR.TOT + NO.EUR
                    AMT.EUR.TOT = AMT.EUR.TOT + AMT.EUR
                    NFEUR.TOT = STR(" ",6 - LEN(NO.EUR.TOT)) : NO.EUR.TOT
                    AFEUR.TOT = STR(" ",10 - LEN(AMT.EUR.TOT)) : AMT.EUR.TOT

                    NO.GBP.TOT = NO.GBP.TOT + NO.GBP
                    AMT.GBP.TOT = AMT.GBP.TOT + AMT.GBP
                    NFGBP.TOT = STR(" ",6 - LEN(NO.GBP.TOT)):NO.GBP.TOT
                    AFGBP.TOT = STR(" ",10 - LEN(AMT.GBP.TOT)):AMT.GBP.TOT

                    NO.CHF.TOT = NO.CHF.TOT + NO.CHF
                    AMT.CHF.TOT = AMT.CHF.TOT + AMT.CHF
                    NFCHF.TOT = STR(" ",6 - LEN(NO.CHF.TOT)):NO.CHF.TOT
                    AFCHF.TOT = STR(" ",10 - LEN(AMT.CHF.TOT)):AMT.CHF.TOT

                    NO.SAR.TOT = NO.SAR.TOT + NO.SAR
                    AMT.SAR.TOT = AMT.SAR.TOT + AMT.SAR
                    NFSAR.TOT = STR(" ",6 - LEN(NO.SAR.TOT)):NO.SAR.TOT
                    AFSAR.TOT = STR(" ",10 - LEN(AMT.SAR.TOT)):AMT.SAR.TOT

                    AMT.USD = '' ; NO.USD = ''   ; NFUSD = '' ;   AFUSD = ''
                    AMT.EUR = '' ; NO.EUR = ''   ; NFEUR = '' ;   AFEUR = ''
                    AMT.GBP = '' ; NO.GBP = ''   ; NFGBP = '' ;   AFGBP = ''
                    AMT.SAR = '' ; NO.SAR = ''   ; NFSAR = '' ;   AFSAR = ''
                    AMT.CHF = '' ; NO.CHF = ''   ; NFCHF = '' ;   AFCHF = ''
**********************************************************************************************

                    BEGIN CASE
                    CASE CURR.1<I> = 'USD'
                        CURR.USD = CURR.1<I>
                        AMT.USD  = AMT.USD + AMTFCY<I>
                        NO.USD = NO.USD + 1
                        NFUSD = STR(" ",6 - LEN(NO.USD)) : NO.USD
                        AFUSD = STR(" ",10 - LEN(AMT.USD)) :AMT.USD

                    CASE CURR.1<I> = 'EUR'
                        CURR.EUR = CURR.1<I>
                        AMT.EUR  = AMT.EUR + AMTFCY<I>
                        NO.EUR = NO.EUR + 1
                        NFEUR = STR(" ",6 - LEN(NO.EUR)) : NO.EUR
                        AFEUR = STR(" ",10 - LEN(AMT.EUR)) : AMT.EUR

                    CASE CURR.1<I> = 'GBP'
                        CURR.GBP = CURR.1<I>
                        AMT.GBP  = AMT.GBP + AMTFCY<I>
                        NO.GBP = NO.GBP + 1
                        NFGBP = STR(" ",6 - LEN(NO.GBP)):NO.GBP
                        AFGBP = STR(" ",10 - LEN(AMT.GBP)):AMT.GBP

                    CASE CURR.1<I> = 'CHF'
                        CURR.CHF = CURR.1<I>
                        AMT.CHF  = AMT.CHF + AMTFCY<I>
                        NO.CHF = NO.CHF + 1
                        NFCHF = STR(" ",6 - LEN(NO.CHF)):NO.CHF
                        AFCHF = STR(" ",10 - LEN(AMT.CHF)):AMT.CHF

                    CASE CURR.1<I> = 'SAR'
                        CURR.SAR = CURR.1<I>
                        AMT.SAR  = AMT.SAR + AMTFCY<I>
                        NO.SAR = NO.SAR + 1
                        NFSAR = STR(" ",6 - LEN(NO.SAR)):NO.SAR
                        AFSAR = STR(" ",10 - LEN(AMT.SAR)):AMT.SAR

                    END CASE

**********************************************************************************************
                END ELSE      ;*TO PRINT IF AUTHDATE CHANGES
                    BEGIN CASE
                    CASE CURR.1<I> = 'USD'
                        CURR.USD = CURR.1<I>
                        AMT.USD  = AMT.USD + AMTFCY<I>
                        NO.USD = NO.USD + 1
                        NFUSD = STR(" ",6 - LEN(NO.USD)) : NO.USD
                        AFUSD = STR(" ",10 - LEN(AMT.USD)) :AMT.USD

                    CASE CURR.1<I> = 'EUR'
                        CURR.EUR = CURR.1<I>
                        AMT.EUR  = AMT.EUR + AMTFCY<I>
                        NO.EUR = NO.EUR + 1
                        NFEUR = STR(" ",6 - LEN(NO.EUR)) : NO.EUR
                        AFEUR = STR(" ",10 - LEN(AMT.EUR)) : AMT.EUR

                    CASE CURR.1<I> = 'GBP'
                        CURR.GBP = CURR.1<I>
                        AMT.GBP  = AMT.GBP + AMTFCY<I>
                        NO.GBP = NO.GBP + 1
                        NFGBP = STR(" ",6 - LEN(NO.GBP)):NO.GBP
                        AFGBP = STR(" ",10 - LEN(AMT.GBP)):AMT.GBP
                        IF AUTHD<SS> = '20091231' THEN
                            TEXT = 'SS=':SS ; CALL REM
                            TEXT = 'I=':I ; CALL REM
                            TEXT = 'CURR=': CURR.1<I> ; CALL REM
                            TEXT = 'AMT.GBP=':AMT.GBP ; CALL REM
                            TEXT = 'NO.GBP=':NO.GBP ; CALL REM
                        END
                    CASE CURR.1<I> = 'CHF'
                        CURR.CHF = CURR.1<I>
                        AMT.CHF  = AMT.CHF + AMTFCY<I>
                        NO.CHF = NO.CHF + 1
                        NFCHF = STR(" ",6 - LEN(NO.CHF)):NO.CHF
                        AFCHF = STR(" ",10 - LEN(AMT.CHF)):AMT.CHF

                    CASE CURR.1<I> = 'SAR'
                        CURR.SAR = CURR.1<I>
                        AMT.SAR  = AMT.SAR + AMTFCY<I>
                        NO.SAR = NO.SAR + 1
                        NFSAR = STR(" ",6 - LEN(NO.SAR)):NO.SAR
                        AFSAR = STR(" ",10 - LEN(AMT.SAR)):AMT.SAR

                    END CASE

**           END ;*IF AUTHDATE CHANGES
*****************************************
**********************************************************************
**    IF (AUTHD<SS> = EDDAT) AND (I = SELECTED) THEN
                    IF I = SELECTED THEN
                        TEXT = 'SELECTED=':SELECTED ; CALL REM
                        DDC = DDC +1
                        TEXT = 'DDC=':DDC ; CALL REM
                        TEXT = 'SS=':SS ; CALL REM
                        AUTHD.1 = ''
                        AUTHD.1 = AUTHD<SS>
                        AD.YY = AUTHD.1[1,4]
                        AD.MM = AUTHD.1[5,2]
                        AD.DD = AUTHD.1[7,2]
                        FMT.AD = AD.YY:"/":AD.MM:"/":AD.DD

                        DAT.AUTH<DDC,1> = " |": STR(" ",2):FMT.AD
                        TEXT = 'AUTHDA=':DAT.AUTH<DDC,1> ; CALL REM
                        TEXT = 'NFUSD=':NFUSD ; CALL REM
                        TEXT = 'AFUSD=':AFUSD ; CALL REM
                        IF NFUSD > '0' THEN
                            CURRNO.U<DDC,2>   = NFUSD
                            CURRAMT.U<DDC,3>  = AFUSD
                        END ELSE
                            CURRNO.U<DDC,2>   = STR(" ",5):'0'
                            CURRAMT.U<DDC,3>  = STR(" ",9):'0'
                        END
                        IF NFEUR > '0' THEN
                            CURRNO.E<DDC,4>   = NFEUR
                            CURRAMT.E<DDC,5>  = AFEUR
                        END ELSE
                            CURRNO.E<DDC,4>   = STR(" ",5):'0'
                            CURRAMT.E<DDC,5>  = STR(" ",9):'0'
                        END
                        IF NFGBP > '0' THEN
                            CURRNO.G<DDC,6>   = NFGBP
                            CURRAMT.G<DDC,7>  = AFGBP
                        END ELSE
                            CURRNO.G<DDC,6>   = STR(" ",5):'0'
                            CURRAMT.G<DDC,7>  = STR(" ",9):'0'
                        END

                        IF NFCHF > '0' THEN
                            CURRNO.C<DDC,8>   = NFCHF
                            CURRAMT.C<DDC,9>  = AFCHF
                        END ELSE
                            CURRNO.C<DDC,8>   = STR(" ",5):'0'
                            CURRAMT.C<DDC,9>  = STR(" ",9):'0'
                        END
                        IF NFSAR > '0' THEN
                            CURRNO.S<DDC,10>   = NFSAR
                            CURRAMT.S<DDC,11>  = AFSAR
                        END ELSE
                            CURRNO.S<DDC,10>   = STR(" ",5):'0'
                            CURRAMT.S<DDC,11>  = STR(" ",9):'0'
                        END

                        NO.USD.TOT = NO.USD.TOT + NO.USD
                        AMT.USD.TOT = AMT.USD.TOT + AMT.USD
                        NFUSD.TOT = STR(" ",6 - LEN(NO.USD.TOT)) : NO.USD.TOT
                        AFUSD.TOT = STR(" ",10 - LEN(AMT.USD.TOT)) :AMT.USD.TOT

                        NO.EUR.TOT = NO.EUR.TOT + NO.EUR
                        AMT.EUR.TOT = AMT.EUR.TOT + AMT.EUR
                        NFEUR.TOT = STR(" ",6 - LEN(NO.EUR.TOT)) : NO.EUR.TOT
                        AFEUR.TOT = STR(" ",10 - LEN(AMT.EUR.TOT)) : AMT.EUR.TOT

                        NO.GBP.TOT = NO.GBP.TOT + NO.GBP
                        AMT.GBP.TOT = AMT.GBP.TOT + AMT.GBP
                        NFGBP.TOT = STR(" ",6 - LEN(NO.GBP.TOT)):NO.GBP.TOT
                        AFGBP.TOT = STR(" ",10 - LEN(AMT.GBP.TOT)):AMT.GBP.TOT

                        NO.CHF.TOT = NO.CHF.TOT + NO.CHF
                        AMT.CHF.TOT = AMT.CHF.TOT + AMT.CHF
                        NFCHF.TOT = STR(" ",6 - LEN(NO.CHF.TOT)):NO.CHF.TOT
                        AFCHF.TOT = STR(" ",10 - LEN(AMT.CHF.TOT)):AMT.CHF.TOT

                        NO.SAR.TOT = NO.SAR.TOT + NO.SAR
                        AMT.SAR.TOT = AMT.SAR.TOT + AMT.SAR
                        NFSAR.TOT = STR(" ",6 - LEN(NO.SAR.TOT)):NO.SAR.TOT
                        AFSAR.TOT = STR(" ",10 - LEN(AMT.SAR.TOT)):AMT.SAR.TOT

                    END       ;*OF (AUTHD<SS> = EDDAT) AND (I = SELECTED)
**********************************************************************
                END ;*IF AUTHDATE CHANGES
*********************************************
            END     ;*D.T EQ AUTH.D

        NEXT I
        FOR ZZ = 1 TO DDC
            XX<1,ZZ>[1,15]  = DAT.AUTH<ZZ,1> : " |"
            XX<1,ZZ>[17,7] = CURRNO.U<ZZ,2>  : "|"
            XX<1,ZZ>[24,11] = CURRAMT.U<ZZ,3> : "|"
            XX<1,ZZ>[35,7] = CURRNO.E<ZZ,4> : "|"
            XX<1,ZZ>[42,11] = CURRAMT.E<ZZ,5> : "|"
            XX<1,ZZ>[53,7] = CURRNO.G<ZZ,6> : "|"
            XX<1,ZZ>[60,11] = CURRAMT.G<ZZ,7> : "|"
            XX<1,ZZ>[71,7] = CURRNO.C<ZZ,8> : "|"
            XX<1,ZZ>[78,11] = CURRAMT.C<ZZ,9> : "|"
            XX<1,ZZ>[89,7] = CURRNO.S<ZZ,10> : "|"
            XX<1,ZZ>[96,11] = CURRAMT.S<ZZ,11> : "|"
            PRINT  XX<1,ZZ>
            PRINT SPACE(1):"---------------------------------------------------------------------------------------------------------"
        NEXT ZZ     ;*NEW RANGE FOR CURRENCY*

        XX = '' ; YY = ZZ+1

        DESC.TOT<YY,1> = " |": STR(" ",4):"TOTAL"
        IF NFUSD.TOT > '0' THEN
            CURRNO.U.TOT<YY,2>   = NFUSD.TOT
            CURRAMT.U.TOT<YY,3>  = AFUSD.TOT
        END ELSE
            CURRNO.U.TOT<YY,2>   = STR(" ",5):'0'
            CURRAMT.U.TOT<YY,3>  = STR(" ",9):'0'
        END
        IF NFEUR.TOT > '0' THEN
            CURRNO.E.TOT<YY,4>   = NFEUR.TOT
            CURRAMT.E.TOT<YY,5>  = AFEUR.TOT
        END ELSE
            CURRNO.E.TOT<YY,4>   = STR(" ",5):'0'
            CURRAMT.E.TOT<YY,5>  = STR(" ",9):'0'
        END
        IF NFGBP.TOT > '0' THEN
            CURRNO.G.TOT<YY,6>   = NFGBP.TOT
            CURRAMT.G.TOT<YY,7>  = AFGBP.TOT
        END ELSE
            CURRNO.G.TOT<YY,6>   = STR(" ",5):'0'
            CURRAMT.G.TOT<YY,7>  = STR(" ",9):'0'
        END

        IF NFCHF.TOT > '0' THEN
            CURRNO.C.TOT<YY,8>   = NFCHF.TOT
            CURRAMT.C.TOT<YY,9>  = AFCHF.TOT
        END ELSE
            CURRNO.C.TOT<YY,8>   = STR(" ",5):'0'
            CURRAMT.C.TOT<YY,9>  = STR(" ",9):'0'
        END
        IF NFSAR.TOT > '0' THEN
            CURRNO.S.TOT<YY,10>   = NFSAR.TOT
            CURRAMT.S.TOT<YY,11>  = AFSAR.TOT
        END ELSE
            CURRNO.S.TOT<YY,10>   = STR(" ",5):'0'
            CURRAMT.S.TOT<YY,11>  = STR(" ",9):'0'
        END

        XX<1,YY>[1,15]  = DESC.TOT<YY,1> : "    |"
        XX<1,YY>[17,7] = CURRNO.U.TOT<YY,2>  : "|"
        XX<1,YY>[24,11] = CURRAMT.U.TOT<YY,3> : "|"
        XX<1,YY>[35,7] = CURRNO.E.TOT<YY,4> : "|"
        XX<1,YY>[42,11] = CURRAMT.E.TOT<YY,5> : "|"
        XX<1,YY>[53,7] = CURRNO.G.TOT<YY,6> : "|"
        XX<1,YY>[60,11] = CURRAMT.G.TOT<YY,7> : "|"
        XX<1,YY>[71,7] = CURRNO.C.TOT<YY,8> : "|"
        XX<1,YY>[78,11] = CURRAMT.C.TOT<YY,9> : "|"
        XX<1,YY>[89,7] = CURRNO.S.TOT<YY,10> : "|"
        XX<1,YY>[96,11] = CURRAMT.S.TOT<YY,11> : "|"
        PRINT  XX<1,YY>
        PRINT SPACE(1):"---------------------------------------------------------------------------------------------------------"
        PRINT
        PRINT

    END   ;*END OF IF SELECTED

    TOTAL.ALL = NFUSD.TOT + NFEUR.TOT + NFGBP.TOT + NFCHF.TOT + NFSAR.TOT
    XX = ''
    XX<1,YY+1>[5,31] = "������ ��� ������� ���� ������ = "
    XX<1,YY+1>[38,7] = TOTAL.ALL
    PRINT  XX<1,YY+1>

    TEXT = '��� ������� �����' ; CALL REM
    RETURN
*===============================================================
END
