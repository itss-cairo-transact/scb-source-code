* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNjQ4ODc6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
****NESSREEN AHMED*************
*-----------------------------------------------------------------------------
* <Rating>4871</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TELLER.TOT.CD.DR.FCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 48 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 49 ] Adding EB_SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-19
*Line [ 50 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='TELLER.TOT.CD.DR.FCY'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    F.TELLER = '' ; FN.TELLER = 'F.TELLER' ; R.TELLER = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.TELLER,F.TELLER)

    XX = ''
    ZZ = ''

    VOPEN.BAL = ''
    COMP = C$ID.COMPANY
**UPDATED ON 18/11/2008*******************
    VLT.ACCT = "EGP1000001990001"
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, VLT.ACCT , VOPEN.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,VLT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
    XX<1,ZZ>[15,16] = VOPEN.BAL
    PRINT XX<1,ZZ>
    XX = ''
    ZZ = ZZ+1

    F.CURRENCY = '' ; FN.CURRENCY = 'F.CURRENCY' ; R.CURRENCY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURRENCY,F.CURRENCY)
****UPDATED BY NESSREEN AHMED ON 04/02/2009********************
**  T.SEL = "SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 9 OR TRANSACTION.CODE EQ 10 OR TRANSACTION.CODE EQ 62 OR TRANSACTION.CODE EQ 42 OR TRANSACTION.CODE EQ 89 OR TRANSACTION.CODE EQ 37 OR TRANSACTION.CODE EQ 38 OR TRANSACTION.CODE EQ 7 OR TRANSACTION.CODE EQ 32 OR TRANSACTION.CODE EQ 33 OR TRANSACTION.CODE EQ 111 OR TRANSACTION.CODE EQ 101 OR TRANSACTION.CODE EQ 102 OR TRANSACTION.CODE EQ 23 OR TRANSACTION.CODE EQ 5 ) AND CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 BY CURRENCY.1 BY TELLER.ID.1 "
**  T.SEL = "SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 9 OR TRANSACTION.CODE EQ 10 OR TRANSACTION.CODE EQ 62 OR TRANSACTION.CODE EQ 42 OR TRANSACTION.CODE EQ 89 OR TRANSACTION.CODE EQ 37 OR TRANSACTION.CODE EQ 38 OR TRANSACTION.CODE EQ 7 OR TRANSACTION.CODE EQ 32 OR TRANSACTION.CODE EQ 33 OR TRANSACTION.CODE EQ 111 OR TRANSACTION.CODE EQ 101 OR TRANSACTION.CODE EQ 102 OR TRANSACTION.CODE EQ 23 OR TRANSACTION.CODE EQ 5 ) AND CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ " : COMP:" BY CURRENCY.1 BY TELLER.ID.1 "
    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN (9 10 62 42 89 37 38 7 32 33 111 101 102 24 5 103 ) AND CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ " : COMP:" BY CURRENCY.1 BY TELLER.ID.1 "
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
*    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF SELECTED THEN
        TOT.FCY = '' ; FCY.AMT = '' ; CURR1 = ''  ; TELL.ID = '' ; ACCT = ''
        TOT.FCY.ALL = ''   ; TOT.FCY.CURR = '' ; VALDAT2 = '' ; FMTDAT = ''
        CALL F.READ(FN.TELLER,KEY.LIST<1>,R.TELLER,F.TELLER,E1)
        ACCT<1> = R.TELLER<TT.TE.ACCOUNT.2>
        FCY.AMT<1> = R.TELLER<TT.TE.AMOUNT.FCY.1>
        CURR1<1>  = R.TELLER<TT.TE.CURRENCY.1>
        TELL.ID<1> = R.TELLER<TT.TE.TELLER.ID.1>
        TOT.FCY = TOT.FCY+FCY.AMT<1>
        TOT.FCY.CURR = TOT.FCY.CURR + FCY.AMT<1>
        VALDAT2<1> = R.TELLER<TT.TE.VALUE.DATE.2>
        FMTDAT = VALDAT2<1>[7,2]:'/':VALDAT2<1>[5,2]:"/":VALDAT2<1>[1,4]
*****26/7/2007*****************************************
        ACCT<1> = CURR1<1>:10000:TELL.ID<1>[1,2]:99
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT<1> , VOPEN.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT<1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
        XX<1,ZZ>[25,16] = VOPEN.BAL
        PRINT XX<1,ZZ>
        XX = ''
        ZZ = ZZ+1
********************************************************************
        FOR I =2 TO SELECTED
            VOPEN.BAL = ''
            CALL F.READ(FN.TELLER,KEY.LIST<I>,R.TELLER,F.TELLER,E1)
            ACCT<I> = R.TELLER<TT.TE.ACCOUNT.2>
            FCY.AMT<I> = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CURR1<I>  = R.TELLER<TT.TE.CURRENCY.1>
            TELL.ID<I> = R.TELLER<TT.TE.TELLER.ID.1>
            VALDAT2<I> = R.TELLER<TT.TE.VALUE.DATE.2>
            FMTDAT = VALDAT2<I>[7,2]:'/':VALDAT2<I>[5,2]:"/":VALDAT2<I>[1,4]
            ACCT<I> = CURR1<I>:10000:TELL.ID<I>[1,2]:99
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT<I> , VOPEN.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            IF TELL.ID<I> # TELL.ID<I-1> THEN
***********CASE TELL.ID CHANGED AND CURRENCY CHANGED*****************
                IF CURR1<I> # CURR1<I-1> THEN
*****30/7/2007******************************************
                    CALL F.READ(FN.CURRENCY,CURR1<I-1>,R.CURRENCY,F.CURRENCY,E2)
                    CUR.MAR = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 124 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR = TOT.FCY*MIDRAT
*  EQU.CURR.R = DEROUND(52334.668505,2)
                        END
                    NEXT TT
********************************************************************
                    XX<1,ZZ>[3,30] = CURR1<I-1>
                    XX<1,ZZ>[45,16] = TOT.FCY
                    XX<1,ZZ>[84,16] = EQU.CURR
                    XX<1,ZZ>[105,10] = FMTDAT
                    XX<1,ZZ>[120,4] = TELL.ID<I-1>
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1


                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ>[1,12] = "����� ������"
                    XX<1,ZZ>[45,16]= TOT.FCY.CURR
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1

                    TOT.FCY = ''
                    EQU.CURR = ''
                    TOT.FCY.ALL = TOT.FCY.ALL + TOT.FCY.CURR
                    TOT.FCY.CURR = ''
                    TOT.FCY = TOT.FCY+FCY.AMT<I>
                    TOT.FCY.CURR = TOT.FCY.CURR + FCY.AMT<I>
*****************29/7/2007**************************
                    XX<1,ZZ>[25,16] = VOPEN.BAL
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
****************************************************
                END ELSE
*******IF TELL.ID CHANGED AND CURRENCY DIDN'T CHANGED*************************
*****30/7/2007******************************************
                    CALL F.READ(FN.CURRENCY,CURR1<I-1>,R.CURRENCY,F.CURRENCY,E2)
                    CUR.MAR = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 175 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR = TOT.FCY*MIDRAT
                        END
                    NEXT TT
********************************************************************
                    XX<1,ZZ>[3,30] = CURR1<I-1>
                    XX<1,ZZ>[45,16] = TOT.FCY
                    XX<1,ZZ>[84,16] = EQU.CURR
                    XX<1,ZZ>[105,10] = FMTDAT
                    XX<1,ZZ>[120,4] = TELL.ID<I-1>
                    PRINT XX<1,ZZ>

                    TOT.FCY.CURR = TOT.FCY.CURR + FCY.AMT<I>
*TOT.FCY.ALL = TOT.FCY.ALL+TOT.FCY

                    XX = ''
                    TOT.FCY = ''
                    EQU.CURR = ''
                    ZZ = ZZ+1
                    TOT.FCY = TOT.FCY+FCY.AMT<I>
******************************************************************
                END
*************END OF CASE TELL.ID CHANGED***********************************
            END ELSE          ;**IF TELL.ID DIDNOT CHANGE ********************
                IF CURR1<I> # CURR1<I-1> THEN
* CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT<I-1> , VOPEN.BAL)
*****30/7/2007******************************************
                    CALL F.READU(FN.CURRENCY,CURR1<I-1>,R.CURRENCY,F.CURRENCY,E2,RETRY2)
                    CUR.MAR = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 208 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR = TOT.FCY*MIDRAT
                        END
                    NEXT TT
********************************************************************
                    XX<1,ZZ>[3,30] = CURR1<I-1>
                    XX<1,ZZ>[45,16] = TOT.FCY
                    XX<1,ZZ>[84,16] = EQU.CURR
                    XX<1,ZZ>[105,10] = FMTDAT
                    XX<1,ZZ>[120,4] = TELL.ID<I-1>
                    PRINT XX<1,ZZ>
*******25/7/2007**********************************************
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ>[1,12] = "����� ������"
                    XX<1,ZZ>[45,16]= TOT.FCY.CURR
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
**************************************************************
                    XX = ''
                    TOT.FCY = ''
                    EQU.CURR = ''
                    TOT.FCY.ALL = TOT.FCY.ALL + TOT.FCY.CURR

                    TOT.FCY.CURR = ''
                    ZZ = ZZ+1
                    TOT.FCY = TOT.FCY+FCY.AMT<I>
                    TOT.FCY.CURR = TOT.FCY.CURR + FCY.AMT<I>
*****************29/7/2007**************************
                    XX<1,ZZ>[25,16] = VOPEN.BAL
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
****************************************************
                END ELSE
                    TOT.FCY = TOT.FCY+FCY.AMT<I>
                    TOT.FCY.CURR = TOT.FCY.CURR + FCY.AMT<I>
*TOT.FCY.ALL = TOT.FCY.ALL+TOT.FCY
                END
            END

            IF I = SELECTED THEN
*                TEXT = 'I=':I ; CALL REM
*****30/7/2007******************************************
                CALL F.READ(FN.CURRENCY,CURR1<I>,R.CURRENCY,F.CURRENCY,E2)
                CUR.MAR = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 267 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DTC = DCOUNT(CUR.MAR,@VM)
                FOR TT = 1 TO DTC
                    IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                        MIDRAT =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                        EQU.CURR = TOT.FCY*MIDRAT
*TEXT = 'EQU.CURR=':EQU.CURR ; CALL REM
                    END
                NEXT TT
********************************************************************

                XX<1,ZZ>[3,30] = CURR1<I>
                XX<1,ZZ>[45,16] = TOT.FCY
                XX<1,ZZ>[84,16] = EQU.CURR
                XX<1,ZZ>[105,10] = FMTDAT
                XX<1,ZZ>[120,4] = TELL.ID<I>
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
****26/7/2007*************************************************
                XX<1,ZZ> = STR('_',125)
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
                XX<1,ZZ>[1,12] = "����� ������"
                XX<1,ZZ>[45,16]= TOT.FCY.CURR
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
                XX<1,ZZ> = STR('_',125)
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
                TOT.FCY.ALL = TOT.FCY.ALL + TOT.FCY.CURR
**************************************************************
            END
        NEXT I
    END
******************************************************************
****UPDATED BY NESSREEN AHMED ON 04/02/2009****************
**    N.SEL ="SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 2 OR TRANSACTION.CODE EQ 48 OR TRANSACTION.CODE EQ 41 OR TRANSACTION.CODE EQ 40 OR TRANSACTION.CODE EQ 86 OR TRANSACTION.CODE EQ 84 OR TRANSACTION.CODE EQ 82 OR TRANSACTION.CODE EQ 90 OR TRANSACTION.CODE EQ 91 OR TRANSACTION.CODE EQ 8 OR TRANSACTION.CODE EQ 36 OR TRANSACTION.CODE EQ 34 OR TRANSACTION.CODE EQ 100 OR TRANSACTION.CODE EQ 25 OR TRANSACTION.CODE EQ 24 OR TRANSACTION.CODE EQ 92 OR TRANSACTION.CODE EQ 20 OR TRANSACTION.CODE EQ 93 OR TRANSACTION.CODE EQ 4 OR TRANSACTION.CODE EQ 21 OR TRANSACTION.CODE EQ 17) AND CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 BY CURRENCY.1 BY TELLER.ID.1 "
***   N.SEL ="SELECT FBNK.TELLER WITH (TRANSACTION.CODE EQ 2 OR TRANSACTION.CODE EQ 48 OR TRANSACTION.CODE EQ 41 OR TRANSACTION.CODE EQ 40 OR TRANSACTION.CODE EQ 86 OR TRANSACTION.CODE EQ 84 OR TRANSACTION.CODE EQ 82 OR TRANSACTION.CODE EQ 90 OR TRANSACTION.CODE EQ 91 OR TRANSACTION.CODE EQ 8 OR TRANSACTION.CODE EQ 36 OR TRANSACTION.CODE EQ 34 OR TRANSACTION.CODE EQ 100 OR TRANSACTION.CODE EQ 25 OR TRANSACTION.CODE EQ 24 OR TRANSACTION.CODE EQ 92 OR TRANSACTION.CODE EQ 20 OR TRANSACTION.CODE EQ 93 OR TRANSACTION.CODE EQ 4 OR TRANSACTION.CODE EQ 21 OR TRANSACTION.CODE EQ 17) AND CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ " : COMP :"  BY CURRENCY.1 BY TELLER.ID.1 "
    N.SEL ="SELECT FBNK.TELLER WITH TRANSACTION.CODE IN (2 48 41 40 86 84 82 90 91 8 36 34 100 25 23 20 21 92 93 4 17 911 11 12 92 93 14 97 105 107 95 98 106 108 96) AND CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 AND CO.CODE EQ " : COMP :"  BY CURRENCY.1 BY TELLER.ID.1 "
     KEY.LIST.N ="" ; SELECTED.N="" ;  ER.MSG.N=""
    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    TEXT = 'HI' ; CALL REM
*    TEXT = 'SELECTED.N=':SELECTED.N ; CALL REM
    IF SELECTED.N THEN
        TOT.FCY.N = '' ; FCY.AMT.N = '' ; TOT.FCY.ALL.N = '' ; CURR1.N = '' ; ACCT.N = '' ; TELL.ID.N = ''
        TOT.FCY.CURR.N = '' ; VALDAT2.N = '' ; FMTDAT.N = '' ; ACCT.N = '' ;  VOPEN.BAL.N = ''
*****************************
        CALL F.READ(FN.TELLER,KEY.LIST.N<1>,R.TELLER,F.TELLER,E1)
        ACCT.N<1> = R.TELLER<TT.TE.ACCOUNT.2>
        FCY.AMT.N<1> = R.TELLER<TT.TE.AMOUNT.FCY.1>
        CURR1.N<1>  = R.TELLER<TT.TE.CURRENCY.1>
        TELL.ID.N<1> = R.TELLER<TT.TE.TELLER.ID.1>
        TOT.FCY.N = TOT.FCY.N+FCY.AMT.N<1>
        TOT.FCY.CURR.N = TOT.FCY.CURR.N + FCY.AMT.N<1>
        VALDAT2.N<1> = R.TELLER<TT.TE.VALUE.DATE.2>
        FMTDAT.N = VALDAT2.N<1>[7,2]:'/':VALDAT2.N<1>[5,2]:"/":VALDAT2.N<1>[1,4]
* TOT.FCY.ALL.N = TOT.FCY.ALL.N+TOT.FCY.N
*****29/7/2007*****************************************
        ACCT.N<1> = CURR1.N<1>:10000:TELL.ID.N<1>[1,2]:99
*Line [ 349 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT<1> , VOPEN.BAL.N)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT<1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL.N=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
        XX<1,ZZ>[25,16] = VOPEN.BAL.N
        PRINT XX<1,ZZ>
        XX = ''
        ZZ = ZZ+1
********************************************************************
*****************************
        FOR SS =2 TO SELECTED.N
            VOPEN.BAL.N = ''  ; CUR.MAR.N = '' ; MIDRAT.N = '' ;  EQU.CURR.N = '' ; DTC = '' ; TT = ''
            CALL F.READU(FN.TELLER,KEY.LIST.N<SS>,R.TELLER,F.TELLER,E1,RETRY1)
            ACCT.N<SS> = R.TELLER<TT.TE.ACCOUNT.2>
            FCY.AMT.N<SS> = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CURR1.N<SS>  = R.TELLER<TT.TE.CURRENCY.1>
            TELL.ID.N<SS> = R.TELLER<TT.TE.TELLER.ID.1>
            VALDAT2.N<SS> = R.TELLER<TT.TE.VALUE.DATE.2>
            FMTDAT.N = VALDAT2.N<SS>[7,2]:'/':VALDAT2.N<SS>[5,2]:"/":VALDAT2.N<SS>[1,4]
            ACCT.N<SS> = CURR1.N<SS>:10000:TELL.ID.N<SS>[1,2]:99
*Line [ 372 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT.N<SS> , VOPEN.BAL.N)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.N<SS>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL.N=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            IF TELL.ID.N<SS> # TELL.ID.N<SS-1> THEN
***********============================================
***********CASE TELL.ID CHANGED AND CURRENCY CHANGED*****************************
                IF CURR1.N<SS> # CURR1.N<SS-1> THEN
*****30/7/2007******************************************
                    CALL F.READU(FN.CURRENCY,CURR1.N<SS-1>,R.CURRENCY,F.CURRENCY,E2,RETRY2)
                    CUR.MAR.N = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 355 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT.N =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR.N = TOT.FCY.N*MIDRAT.N
                        END
                    NEXT TT
********************************************************************
                    XX<1,ZZ>[3,30] = CURR1.N<SS-1>
                    XX<1,ZZ>[63,16] = TOT.FCY.N
                    XX<1,ZZ>[84,16] = EQU.CURR.N
                    XX<1,ZZ>[105,10] = FMTDAT.N
                    XX<1,ZZ>[120,4] = TELL.ID.N<SS-1>
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1

                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ>[1,12] = "����� ������"
                    XX<1,ZZ>[63,16]= TOT.FCY.CURR.N
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1

                    TOT.FCY.N = ''
                    TOT.FCY.ALL.N = TOT.FCY.ALL.N + TOT.FCY.CURR.N
                    TOT.FCY.CURR.N = ''
                    TOT.FCY.N = TOT.FCY.N+FCY.AMT.N<SS>
                    TOT.FCY.CURR.N = TOT.FCY.CURR.N + FCY.AMT.N<SS>
*****************29/7/2007**************************
                    XX<1,ZZ>[25,16] = VOPEN.BAL.N
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
****************************************************
                END ELSE
*******IF TELL.ID CHANGED AND CURRENCY DIDN'T CHANGED*************************
*****30/7/2007******************************************
                    CALL F.READU(FN.CURRENCY,CURR1.N<SS-1>,R.CURRENCY,F.CURRENCY,E2,RETRY2)
                    CUR.MAR.N = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 403 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT.N =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR.N = TOT.FCY.N*MIDRAT.N
                        END
                    NEXT TT
********************************************************************
*****30/7/2007******************************************
                    CALL F.READU(FN.CURRENCY,CURR1.N<SS-1>,R.CURRENCY,F.CURRENCY,E2,RETRY2)
                    CUR.MAR.N = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 415 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT.N =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR.N = TOT.FCY.N*MIDRAT.N
                        END
                    NEXT TT
********************************************************************
                    XX<1,ZZ>[3,30] = CURR1.N<SS-1>
                    XX<1,ZZ>[63,16] = TOT.FCY.N
                    XX<1,ZZ>[84,16] = EQU.CURR.N
                    XX<1,ZZ>[105,10] = FMTDAT.N
                    XX<1,ZZ>[120,4] = TELL.ID.N<SS-1>
                    PRINT XX<1,ZZ>

                    TOT.FCY.CURR.N = TOT.FCY.CURR.N + FCY.AMT.N<SS>
*TOT.FCY.ALL.N = TOT.FCY.ALL.N+TOT.FCY.N

                    XX = ''
                    TOT.FCY.N = ''
                    ZZ = ZZ+1
                    TOT.FCY.N = TOT.FCY.N+FCY.AMT.N<SS>
******************************************************************
                END
*************END OF CASE TELL.ID CHANGED***********************************
            END ELSE          ;**IF TELL.ID DIDNOT CHANGE ********************
                IF CURR1.N<SS> # CURR1.N<SS-1> THEN
*****30/7/2007******************************************
                    CALL F.READU(FN.CURRENCY,CURR1.N<SS-1>,R.CURRENCY,F.CURRENCY,E2,RETRY2)
                    CUR.MAR.N = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 446 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DTC = DCOUNT(CUR.MAR,@VM)
                    FOR TT = 1 TO DTC
                        IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                            MIDRAT.N =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                            EQU.CURR.N = TOT.FCY.N*MIDRAT.N
                        END
                    NEXT TT
********************************************************************
                    XX<1,ZZ>[3,30] = CURR1.N<SS-1>
                    XX<1,ZZ>[63,16] = TOT.FCY.N
                    XX<1,ZZ>[84,16] = EQU.CURR.N
                    XX<1,ZZ>[105,10] = FMTDAT.N
                    XX<1,ZZ>[120,4] = TELL.ID.N<SS-1>
                    PRINT XX<1,ZZ>
*******25/7/2007**********************************************
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ>[1,12] = "����� ������"
                    XX<1,ZZ>[63,16]= TOT.FCY.CURR.N
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
                    XX<1,ZZ> = STR('_',125)
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
*****************29/7/2007**************************
                    XX<1,ZZ>[25,16] = VOPEN.BAL.N
                    PRINT XX<1,ZZ>
                    XX = ''
                    ZZ = ZZ+1
****************************************************
**************************************************************
                    XX = ''
                    TOT.FCY.N = ''
                    TOT.FCY.ALL.N = TOT.FCY.ALL.N + TOT.FCY.CURR.N
                    TOT.FCY.CURR.N = ''
                    ZZ = ZZ+1
                    TOT.FCY.N = TOT.FCY.N+FCY.AMT.N<SS>
                    TOT.FCY.CURR.N = TOT.FCY.CURR.N + FCY.AMT.N<SS>
                END ELSE
                    TOT.FCY.N = TOT.FCY.N+FCY.AMT.N<SS>
                    TOT.FCY.CURR.N = TOT.FCY.CURR.N + FCY.AMT.N<SS>
*TOT.FCY.ALL.N = TOT.FCY.ALL.N+TOT.FCY.N
                END
            END

            IF SS = SELECTED.N THEN
*****30/7/2007******************************************
                CALL F.READU(FN.CURRENCY,CURR1.N<SS>,R.CURRENCY,F.CURRENCY,E2,RETRY2)
                CUR.MAR.N = R.CURRENCY<EB.CUR.CURRENCY.MARKET>
*Line [ 502 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DTC = DCOUNT(CUR.MAR,@VM)
                FOR TT = 1 TO DTC
                    IF R.CURRENCY<EB.CUR.CURRENCY.MARKET,TT> = 10 THEN
                        MIDRAT.N =  R.CURRENCY<EB.CUR.MID.REVAL.RATE,TT>
                        EQU.CURR.N = TOT.FCY.N*MIDRAT.N
                    END
                NEXT TT
********************************************************************
*TEXT = 'SS=':SS ; CALL REM
                XX<1,ZZ>[3,30] = CURR1.N<SS>
                XX<1,ZZ>[63,16] = TOT.FCY.N
                XX<1,ZZ>[84,16] = EQU.CURR.N
                XX<1,ZZ>[105,10] = FMTDAT.N
                XX<1,ZZ>[120,4] = TELL.ID.N<SS>
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
****26/7/2007*************************************************
                XX<1,ZZ> = STR('_',125)
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
                XX<1,ZZ>[1,12] = "����� ������"
                XX<1,ZZ>[63,16]= TOT.FCY.CURR.N
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
                XX<1,ZZ> = STR('_',125)
                PRINT XX<1,ZZ>
                XX = ''
                ZZ = ZZ+1
                TOT.FCY.ALL.N = TOT.FCY.ALL.N + TOT.FCY.CURR.N
**************************************************************
            END
        NEXT SS
    END
***********31/7/2007*****************************************
    S.SEL = "SELECT FBNK.TELLER WITH CURRENCY.1 NE EGP AND TELLER.ID.1 NE 9999 BY CURRENCY.1"
    KEY.LIST.S ="" ; SELECTED.S="" ;  ER.MSG.S=""
    CALL EB.READLIST(S.SEL,KEY.LIST.S,"",SELECTED.S,ER.MSG.S)
    TEXT = 'SELECTED.S=':SELECTED.S ; CALL REM
    VOPEN.TOT = ''
    IF SELECTED.S THEN
        CURR1.S = '' ; TELL.ID.S = '' ; ACCT.S = ''
        CALL F.READU(FN.TELLER,KEY.LIST.S<1>,R.TELLER,F.TELLER,E1,RETRY1)
        CURR1.S<1>  = R.TELLER<TT.TE.CURRENCY.1>
        TELL.ID.S<1> = R.TELLER<TT.TE.TELLER.ID.1>
        ACCT.S<1> = CURR1.S<1>:10000:TELL.ID.S<1>[1,2]:99
*TEXT = 'ACCT.S<1>=':ACCT.S<1> ; CALL REM
*Line [ 583 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT.S<1> , VOPEN.BAL.S)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.S<1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL.S=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
*TEXT =  'VOPEN=':VOPEN.BAL.S ; CALL REM
        VOPEN.TOT = VOPEN.TOT + VOPEN.BAL.S
* TEXT = 'TOT.VOPEN=':VOPEN.TOT ; CALL REM
        FOR WW= 2 TO SELECTED.S
            VOPEN.BAL.S = ''
            CALL F.READU(FN.TELLER,KEY.LIST.S<WW>,R.TELLER,F.TELLER,E1,RETRY1)
            CURR1.S<WW>  = R.TELLER<TT.TE.CURRENCY.1>
            TELL.ID.S<WW> = R.TELLER<TT.TE.TELLER.ID.1>
            ACCT.S<WW> = CURR1.S<WW>:10000:TELL.ID.S<WW>[1,2]:99

            IF CURR1.S<WW> # CURR1.S<WW-1> THEN
*Line [ 601 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR( 'ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL, ACCT.S<WW> , VOPEN.BAL.S)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.S<WW>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
VOPEN.BAL.S=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
*TEXT = 'ACC=':ACCT.S<WW> ; CALL REM
*TEXT =  'VOPEN=':VOPEN.BAL.S ; CALL REM
                VOPEN.TOT = VOPEN.TOT + VOPEN.BAL.S
* TEXT = 'TOT.VOPEN=':VOPEN.TOT ; CALL REM
            END

        NEXT WW
    END
********************************************************************
    TEXT = "��� �������" ; CALL REM
**********************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 622 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):" ������ ������ � ������ ������ ������� ���� ������"
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

** PR.HD :="'L'":SPACE(1):"������":SPACE(10):"������ ������":SPACE(10):"������ ������":SPACE(05):"������ �������":SPACE(05):"������ ������":SPACE(10):"����� �������":SPACE(04):"��� ������"
    PR.HD :="'L'":SPACE(1):"������":SPACE(35):"������ ������":SPACE(05):"������ �������":SPACE(05):"������ ������":SPACE(10):"����� �������":SPACE(04):"��� ������"
** PR.HD :="'L'":SPACE(1):STR('_',6):SPACE(10):STR('_',13):SPACE(10):STR('_',16):SPACE(04):STR('_',15):SPACE(04):STR('_',14):SPACE(09):STR('_',15):SPACE(04):STR('_',10)
    PR.HD :="'L'":SPACE(1):STR('_',6):SPACE(33):STR('_',16):SPACE(04):STR('_',15):SPACE(04):STR('_',14):SPACE(09):STR('_',15):SPACE(04):STR('_',10)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(10):"������ ��������� ������� ��������"
    PR.HD :="'L'":SPACE(10):STR('_',26)
    HEADING PR.HD
    RETURN
*==============================================================

END
