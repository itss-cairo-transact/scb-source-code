* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUyOTE0NDUzODM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:24:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>345</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* Create By Nahrawy
* Edit By Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE SCR.HISTORY.CHARGE.DATE

*PROGRAM SCR.HISTORY.CHARGE.DATE
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_COMMON
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_EQUATE
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.USER
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.USER.SIGN.ON.NAME
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.DRAWINGS
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.CURRENCY
*Line [ 44 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.CATEGORY
*Line [ 46 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.FT.CHARGE.TYPE
*Line [ 48 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.FT.COMMISSION.TYPE
*Line [ 50 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.LC.ACCOUNT.BALANCES
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "��� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID = 'SCR.HISTORY.CHARGE.DATE'
    CALL PRINTER.ON(REPORT.ID,'')

    OUT.AMT       = 0
    CHARGE.CURR   = ''
    CHARGE.AMOUNT = 0
    RETURN
*========================================================================
PROCESS:
*-------
    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    QQ = COMI
    CALL F.READ(FN.ACC,QQ,R.ACC,F.ACC,E1)

    YTEXT = "Enter the DATE No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    YY = COMI

    TOT = 0
    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '����� �������� ���������':' - ':'SIGHT PAYMENT OF INWORD COLLECTIONS(CHARGE)'
*------------------------------------------------------------------------
    DB.ACC = ''; ACC.NO = ''
*************************MODIFIED ON 18-12-2014 ABEER
    LOCATE YY IN R.ACC<LCAC.CHRG.DATE.DUE,1> SETTING III THEN
        DB.ACC    = R.ACC<LCAC.SETTLE.AC.FROM,III>
        ACC.NO = DB.ACC
    END
********************
    ACC.NO = DB.ACC
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,DB.ACC,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,DB.ACC,CUR.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,DB.ACC,CAT.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*Line [ 123 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BR1=R.ITSS.ACCOUNT<AC.CO.CODE>
    ACC.BR2 = ACC.BR1[8,2]
    ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
    CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME2    = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
    CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>

*Line [ 104 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.CHARGE = DCOUNT(R.ACC<LCAC.CHRG.CODE>,@VM)
    DAT            = R.ACC<LCAC.CHRG.DATE.DUE>
    MAT.DATE       = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    INPUTT    = R.ACC<LCAC.INPUTTER>
    INP       = FIELD(INPUTT,'_',2)
    AUTH.SON  = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 161 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH.SON,AUTH)
F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
FN.F.ITSS.USER.SIGN.ON.NAME = ''
CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH.SON,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
AUTH=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
    AUTHI     =  AUTH
*------------------------------------------------------------------------
*Line [ 170 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACC.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:
    PR.HD :="'L'":NN.TITLE
    PR.HD :="'L'":"SCR.HISTORY.CHARGE.DATE"
    PRINT
    HEADING PR.HD
    PRINT " "
*------------------------------------------------------------------
    XX = SPACE(132)

    XX<1,1>[3,35]  = "��� ������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME2
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = "�������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS2
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS3
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = ACC.NO
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = CATEG
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '����� ������� : '
    YY.NEW = YY[7,2]:'/':YY[5,2]:'/':YY[1,4]
    XX<1,1>[50,35] = YY.NEW
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,15]  = '������         : '
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "
*-------------------------------------------------------------------
    WS.INDX = 0
    T.SEL   = "SELECT ":FN.ACC: " WITH @ID EQ ": QQ
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CUR = ""
    TOT = 0
    IF SELECTED THEN
        CALL F.READ(FN.ACC,KEY.LIST,R.ACC,F.ACC,E1)
*Line [ 191 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAT22.DECOUNT = DCOUNT(R.ACC<LCAC.CHRG.DATE.DUE>,@VM)
        FOR W = 1 TO DAT22.DECOUNT
            DAT22 = R.ACC<LCAC.CHRG.DATE.DUE><1,W>
            IF DAT22 EQ YY THEN
                CHARGCODE      = R.ACC<LCAC.CHRG.CODE>
                CHARGE.CODE    = R.ACC<LCAC.CHRG.CODE><1,W>
                CHARGE.AMOUNT  = R.ACC<LCAC.AMT.REC><1,W>
                CHARGE.CURR    = R.ACC<LCAC.CHRG.CCY><1,W>
*Line [ 261 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHARGE.CODE,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CHARGE.NAME=R.ITSS.FT.CHARGE.TYPE<FT5.DESCRIPTION>

                IF ETEXT THEN
** TEXT = "ETEXT = " : ETEXT ; CALL REM
*Line [ 271 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
FN.F.ITSS.FT.COMMISSION.TYPE = ''
CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,CHARGE.CODE,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
CHARGE.NAME=R.ITSS.FT.COMMISSION.TYPE<FT4.SHORT.DESCR>
                END

                IN.AMOUNT = R.ACC<LCAC.AMT.REC><1,W>
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 282 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CHARGE.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>
                OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                TOT     += CHARGE.AMOUNT

                XX<1,1>[3,35]   = '�������  :'
                XX<1,1>[50,30]  = CHARGE.NAME
                PRINT XX<1,1>
                XX<1,1> = ""

                XX<1,1>[3,35]   = '������     : '
                XX<1,1>[50,30]  = CHARGE.AMOUNT
                PRINT XX<1,1>
                XX<1,1> = ""

                CALL WORDS.ARABIC.DEAL(CHARGE.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                XX<1,1>[3,35]    = '������ ������� : '
                XX<1,1>[50,30]   = OUT.AMT
                PRINT XX<1,1>
                XX<1,1> = ""
                PRINT " "

                WS.INDX ++
            END
        NEXT W

        FOR II = WS.INDX TO 4
            XX<1,1>[3,35]   = '�������  :'
            XX<1,1>[50,30]  = " "
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]   = '������     : '
            XX<1,1>[50,30]  = " "
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]    = '������ ������� : '
            XX<1,1>[50,30]   = " "
            PRINT XX<1,1>
            XX<1,1> = ""
            PRINT " "
        NEXT II

        XX<1,1>[3,35]    = '������     : '
        XX<1,1>[50,35]   = CUR
        PRINT XX<1,1>
        XX<1,1> = ""
        PRINT " "

        XX<1,1>[3,35]    = '������ ��������'
        XX<1,1>[50,30]   = TOT
        PRINT XX<1,1>
        XX<1,1> = ""

        CALL WORDS.ARABIC.DEAL(TOT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        XX<1,1>[3,35]    = '������ ������� : '
        XX<1,1>[50,30]   = OUT.AMT
        PRINT XX<1,1>
        XX<1,1> = ""
        PRINT " "

        XX<1,1>[3,35]   = '������'
        XX<1,1>[50,35]  = INP
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]  = '��� �������'
        XX<1,1>[50,35] = QQ
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]  = '������'
        XX<1,1>[50,35] = AUTHI
        PRINT XX<1,1>
        XX<1,1> = ""
    END
*===============================================================
    RETURN
END
