* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUyOTE0NDU3NTk6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:24:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCR.LC.IMP.CHRG
**PROGRAM  SCR.LC.IMP.CHRG

*Line [ 19 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_COMMON
*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_EQUATE
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.USER
*Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.USER.SIGN.ON.NAME
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.CURRENCY
*Line [ 37 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.CATEGORY
*Line [ 39 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.FT.CHARGE.TYPE
*Line [ 41 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.LETTER.OF.CREDIT
*Line [ 43 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.FT.COMMISSION.TYPE
*Line [ 45 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.LC.ACCOUNT.BALANCES
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_DR.LOCAL.REFS

    GOSUB INITIATE; GOSUB BUILD.DATA
    CALL PRINTER.OFF;CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT CREATED" ; CALL REM
    RETURN
**********************************************************
INITIATE:
    REPORT.ID = 'SCR.LC.IMP.CHRG' ; CALL PRINTER.ON(REPORT.ID,'')
    OUT.AMT  = 0; CHARGE.CURR   = '' ; CHARGE.AMOUNT = 0
    RETURN
**********************************************************
BUILD.DATA:
*-------
    R.LC = ''  ;R.ACC = '' ;R.LCL = ''    ;R.DRL = ''     ;AUTHI = '';INP = ''
    OLD.NO = '';DB.ACC = '';TXT.DESC = '' ; KEY.LIST = '' ; SELECTED = ''
    ER.MSG = '';YY = ''
    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.LC = 'FBNK.LETTER.OF.CREDIT'   ;F.LC='' ;R.LC =''
    CALL OPF(FN.LC,F.LC)

    FN.LC.NAU = 'FBNK.LETTER.OF.CREDIT$NAU'   ;F.LC.NAU='' ;R.LC.NAU =''
    CALL OPF(FN.LC.NAU,F.LC.NAU)

    IF ID.NEW EQ '' THEN
        YTEXT = "Enter the TF No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        LC.ID = COMI
        CALL F.READ(FN.ACC,LC.ID,R.ACC,F.ACC,E1)

        YTEXT = "Enter the DATE No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        YY = COMI
        DB.ACC = ''; ACC.NO = '';TEXT.DESC=''

        LOCATE YY IN R.ACC<LCAC.CHRG.DATE.DUE,1> SETTING III THEN
            DB.ACC    = R.ACC<LCAC.SETTLE.AC.FROM,III>
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DAT.NO = DCOUNT(R.ACC<LCAC.CHRG.DATE.DUE>,@VM)
            ACC.NO = DB.ACC
            CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E11)
            OLD.NO    =  R.LC<TF.LC.OLD.LC.NUMBER>
            INPUTT    =  R.LC<TF.LC.INPUTTER>
            INP       =  FIELD(INPUTT,'_',2)
            AUTHI     =  R.LC<TF.LC.AUTHORISER>
            AUTHI       =  FIELD(AUTHI,'_',2)
            TXT.DESC = '���'
        END
    END ELSE
        IF ID.NEW NE '' THEN
            LC.ID = ID.NEW
            CALL F.READ(FN.LC.NAU,LC.ID,R.LC.NAU,F.LC.NAU,E111)

            DB.ACC = R.LC.NAU<TF.LC.CHARGE.ACCT>

            OLD.NO = R.LC.NAU<TF.LC.OLD.LC.NUMBER>
            INPUTT    = R.LC.NAU<TF.LC.INPUTTER>
            INP       = FIELD(INPUTT,'_',2)
            AUTHI =  OPERATOR
            TXT.DESC = '���'
            ACC.NO =DB.ACC
        END
        ACC.NO =DB.ACC
        YY =TODAY
    END

*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
**** CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*     CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,ACC.BR1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BR1=R.ITSS.ACCOUNT<AC.CO.CODE>
     ACC.BR2 = ACC.BR1[8,2]
     ACC.BR = TRIM(ACC.BR2, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
*Line [ 150 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 157 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
    CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME2    = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
    CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>

    DAT            = R.ACC<LCAC.CHRG.DATE.DUE>
    MAT.DATE       = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

*Line [ 173 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACC.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD   ="'L'":'SCR.LC.IMP.CHRG'
    PR.HD  :="'L'":SPACE(1):"��� ���� ������"
    PR.HD  :="'L'":"������� : ":T.DAY
    PR.HD  :="'L'":"����� : ":YYBRN:

    PRINT

    HEADING PR.HD
    PRINT " "
*------------------------------------------------------------------
    XX = SPACE(132)

    XX<1,1>[3,35]  = "��� ������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.NAME2
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = "�������"
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS2
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = CUST.ADDRESS3
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = ACC.NO
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '��� ������ : '
    XX<1,1>[50,35] = CATEG
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]  = '����� ������� : '
    YY.NEW = YY[7,2]:'/':YY[5,2]:'/':YY[1,4]
    XX<1,1>[50,35] = YY.NEW
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,15]  = '������         : '
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    WS.INDX = '0'; CUR = ''; TOT = 0
    IF ID.NEW EQ '' THEN
*Line [ 195 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAT.NO = DCOUNT(R.ACC<LCAC.CHRG.DATE.DUE>,@VM)
    END ELSE
*Line [ 198 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAT.NO= DCOUNT(R.LC.NAU<TF.LC.CHARGE.CODE>,@VM)
    END
    FOR W = 1 TO DAT.NO
        IF ID.NEW EQ '' THEN
            DAT22          = R.ACC<LCAC.CHRG.DATE.DUE><1,W>
            CHARGCODE      = R.ACC<LCAC.CHRG.CODE>
            CHARGE.CODE    = R.ACC<LCAC.CHRG.CODE><1,W>
            CHARGE.AMOUNT  = R.ACC<LCAC.AMT.REC><1,W>
            CHARGE.CURR    = R.ACC<LCAC.CHRG.CCY><1,W>
            IN.AMOUNT      = R.ACC<LCAC.AMT.REC><1,W>
        END ELSE
            DAT22 = YY
            CHARGE.CODE    = R.LC.NAU<TF.LC.CHARGE.CODE><1,W>
            CHARGE.AMOUNT  = R.LC.NAU<TF.LC.CHARGE.AMOUNT><1,W>
            CHARGE.CURR    = R.LC.NAU<TF.LC.CHARGE.CURRENCY><1,W>
            IN.AMOUNT      = R.LC.NAU<TF.LC.CHARGE.AMOUNT><1,W>
        END
        IF DAT22 EQ YY THEN
*Line [ 273 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHARGE.CODE,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CHARGE.NAME=R.ITSS.FT.CHARGE.TYPE<FT5.DESCRIPTION>

            IF ETEXT THEN
*Line [ 282 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*                CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
FN.F.ITSS.FT.COMMISSION.TYPE = ''
CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,CHARGE.CODE,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
CHARGE.NAME=R.ITSS.FT.COMMISSION.TYPE<FT4.SHORT.DESCR>
            END
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 291 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CHARGE.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>
            OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
            TOT     += CHARGE.AMOUNT

            XX<1,1>[3,35]   = '�������  :'
            XX<1,1>[50,30]  = CHARGE.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]   = '������     : '
            XX<1,1>[50,30]  = CHARGE.AMOUNT
            PRINT XX<1,1>
            XX<1,1> = ""

            CALL WORDS.ARABIC.DEAL(CHARGE.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
            XX<1,1>[3,35]    = '������ ������� : '
            XX<1,1>[50,30]   = OUT.AMT
            PRINT XX<1,1>
            XX<1,1> = ""
            PRINT " "

            WS.INDX ++
        END
    NEXT W

    FOR II = WS.INDX TO 4
        XX<1,1>[3,35]   = '�������  :'
        XX<1,1>[50,30]  = " "
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]   = '������     : '
        XX<1,1>[50,30]  = " "
        PRINT XX<1,1>
        XX<1,1> = ""

        XX<1,1>[3,35]    = '������ ������� : '
        XX<1,1>[50,30]   = " "
        PRINT XX<1,1>
        XX<1,1> = ""
        PRINT " "
    NEXT II
**********************************************
    XX<1,1>[3,35]    = '������     : '
    XX<1,1>[50,35]   = CUR
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]    = '������ ��������'
    XX<1,1>[50,30]   = TOT
    PRINT XX<1,1>
    XX<1,1> = ""

    CALL WORDS.ARABIC.DEAL(TOT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT  = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    XX<1,1>[3,35]    = '������ ������� : '
    XX<1,1>[50,30]   = OUT.AMT
    PRINT XX<1,1>
    XX<1,1> = ""
    PRINT " "

    XX<1,1>[3,35]   = '������'
    XX<1,1>[50,35]  = INP
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = '��� �������'
    XX<1,1>[50,35] = LC.ID
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = '������'
    XX<1,1>[50,35] = AUTHI
    PRINT XX<1,1>
    XX<1,1> = ""


    XX<1,1>[3,35]  = '������'
    XX<1,1>[50,35] =  OLD.NO
    PRINT XX<1,1>
    XX<1,1> = ""

    XX<1,1>[3,35]  = '��� �������'
    XX<1,1>[50,35] = TXT.DESC
    PRINT XX<1,1>
    XX<1,1> = ""

    RETURN
END
