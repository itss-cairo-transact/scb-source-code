* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUyOTE0Mzg5NjY6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:23:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE SCB.REJECT.OVERRIDE

*Line [ 18 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_COMMON
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-19
$INSERT I_F.FUNDS.TRANSFER

    OVE.ARRAY = R.NEW(FT.OVERRIDE)
*Line [ 23 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    Y.CNT = DCOUNT(OVE.ARRAY,@VM)
    Y.ETEXT.ARR = ''
    IF OVE.ARRAY THEN
        FOR I = 1 TO Y.CNT
*TEXT = "OVER.1 = ":FIELD(OVE.ARRAY<1,I>,'}',1) ; CALL REM
            Y.ETEXT.ARR<1,-1> = FIELD(OVE.ARRAY<1,I>,'}',1)
        NEXT I

    END
*Line [ 33 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    NOT.ERROR='ACCT.BAL.LT.LOCKED':@FM: 'ACCT.UNAUTH.OD' :@FM: 'LT.MINIMUM.BAL' :@FM: 'WITHDRAWL.LT.MIN.BAL' :@FM: 'AVAILABLE.FUNDS.EXCESS' :@FM: 'AVAILABLE.BALANCE.OVERDRAFT' :@FM: 'LIMIT.EXPS.BEF.TXN'
*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    Y.OVER.CNT = DCOUNT(Y.ETEXT.ARR,@VM)
    FOR J=1 TO Y.OVER.CNT
        LOCATE Y.ETEXT.ARR<1,J> IN NOT.ERROR<1> SETTING POS THEN

            ETEXT = Y.ETEXT.ARR<1,J>
            CALL STORE.END.ERROR
        END

    NEXT J
    RETURN
