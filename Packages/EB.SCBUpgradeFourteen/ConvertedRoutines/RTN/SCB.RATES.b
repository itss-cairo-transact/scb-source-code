* @ValidationCode : Mjo0NzUzNjEwNjA6Q3AxMjUyOjE2NDUyOTE0Mzg1NTk6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Feb 2022 19:23:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>1326</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.RATES


******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RATES
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE                   ; * Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION         ; * Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID               ; * Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD           ; * Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS      ; * ) For Input
                GOSUB PROCESS.MESSAGE     ; * ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN                             ; * From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS              ; * Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'               ; * Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'               ; * Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE        ; * Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL      ; * Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION    ; * Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE      ; * Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE    ; * Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ; * Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE      ; * Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE    ; * Special Processing after write
            END
        END

    END

RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

*Line [ 201 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*  CALL DBR( 'SCB.PARMS':@FM:1, ID.NEW [1,8], MY.DUMMY)
    F.ITSS.SCB.PARMS = 'F.SCB.PARMS'
    FN.F.ITSS.SCB.PARMS = ''
    CALL OPF(F.ITSS.SCB.PARMS,FN.F.ITSS.SCB.PARMS)
    CALL F.READ(F.ITSS.SCB.PARMS,R.ITSS.SCB.PARMS,FN.F.ITSS.SCB.PARMS,ERROR.SCB.PARMS)
    MY.DUMMY=R.ITSS.SCB.PARMS<1,ID.NEW[1,8]>
    IF ETEXT THEN E = 'THIS ID MUST EXSIST IN PARAMETERS TABLE ' ; CALL ERR ; MESSAGE = 'REPEAT'

    IF ID.NEW [9,8] = '' THEN
        U = ID.NEW [1,8]
        ID.NEW = U:TODAY
    END

RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

    AF = SCB.RAT.INT.RATE.TYPE

    IF COMI = 1 THEN
*Line [ 227 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        IF R.NEW(SCB.RAT.INTEREST.RATE) = ''  THEN ETEXT = ' INTEREST RATE CAN NOT BE '' '
    END

    IF COMI = 3 THEN
*Line [ 232 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        IF R.NEW(SCB.RAT.INTEREST.KEY) = ''  THEN  ETEXT = ' INTEREST KEY CAN NOT BE '' '
*Line [ 234 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        IF R.NEW(SCB.RAT.INTEREST.SPREAD) = ''  THEN  ETEXT = ' INTEREST SPREAD CAN NOT BE '' '
    END

    CALL STORE.END.ERROR



    IF E THEN
        T.SEQU = "IFLD"
*Line [ 244 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END



RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
RETURN                             ; * Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN                ; * Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"               ; * Back to field input

    END
RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************

CHECK.DELETE:


RETURN

*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************
DELIVERY.PREVIEW:

RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN                ; * Said No to override
        CALL TRANSACTION.ABORT          ; * Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"               ; * Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:


RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"    ; * Record status
REM > CALL XX.AUTHORISATION
        CASE R.NEW(V-8)[1,3] = "RNA"    ; * Record status
REM > CALL XX.REVERSAL

    END CASE

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
*Line [ 380 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
        V$FUNCTION = ''
    END

RETURN

*************************************************************************

INITIALISE:

RETURN

*************************************************************************

DEFINE.PARAMETERS:* SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

MAT F = ""  ; MAT N = "" ;  MAT T = ""
MAT CHECKFILE = "" ; MAT CONCATFILE = ""
ID.CHECKFILE = ""
ID.CONCATFILE = ""

ID.F  = "CATEGORY.CCY.DATE"           ; ID.N  = "21"    ; ID.T  = "A"

Z = 0


Z+=1 ; F(Z)= "XX<INTEREST.RATE"       ; N(Z) = "10.1.C"  ;  T(Z)= "ANY"
Z+=1 ; F(Z)= "XX-INTEREST.KEY"        ; N(Z) = "4"       ;  T(Z)= "R"
Z+=1 ; F(Z)= "XX-NEW.INTEREST.RATE"   ; N(Z) = "10"      ;  T(Z)= "R"
Z+=1 ; F(Z)= "XX-NEW.SPREAD"          ; N(Z) = "9"       ;  T(Z)= "R"
Z+=1 ; F(Z)= "XX-INTEREST.SPREAD"     ; N(Z) = "9"       ;  T(Z)= "R"
Z+=1 ; F(Z)= "XX-INT.RATE.TYPE"       ; N(Z) = "1.1.C"   ;  T( Z) = "" ; T( Z)< 2> = "1_2_3_4_5"
Z+=1 ; F(Z)= "XX>STAFF.SPREAD"        ; N(Z) = "9"      ;  T(Z)= "R"


Z+=1 ; F(Z)= "RESERVED"           ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED2"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'


Z+=1 ; F(Z)= "RESERVED3"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED4"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED5"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'


Z+=1 ; F(Z)= "RESERVED6"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED7"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED8"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED9"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'

Z+=1 ; F(Z)= "RESERVED10"          ; N(Z) = "35"     ;  T(Z)= "ANY"
T(Z)<3> = 'NOINPUT'



V = Z + 9
RETURN

*************************************************************************

END
