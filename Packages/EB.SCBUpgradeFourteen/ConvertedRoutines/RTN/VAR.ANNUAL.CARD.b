* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MjcxNDM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*---- CREATE BY NESSMA
    SUBROUTINE VAR.ANNUAL.CARD(DB.ACC,DB.AMT,PAN)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CARD.FEES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CARD.FEES.ALL
*----------------------------------------------
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('CARD.ISSUE':@FM:CARD.IS.CO.CODE,"ATMC.":PAN,PAN.COMPANY)
F.ITSS.CARD.ISSUE = 'FBNK.CARD.ISSUE'
FN.F.ITSS.CARD.ISSUE = ''
CALL OPF(F.ITSS.CARD.ISSUE,FN.F.ITSS.CARD.ISSUE)
CALL F.READ(F.ITSS.CARD.ISSUE,"ATMC.":PAN,R.ITSS.CARD.ISSUE,FN.F.ITSS.CARD.ISSUE,ERROR.CARD.ISSUE)
PAN.COMPANY=R.ITSS.CARD.ISSUE<CARD.IS.CO.CODE>
    GOSUB INIT
    GOSUB PROCESS
    RETURN
*----------------------------------------------
INIT:
*----
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS   = "CARD.FEES3"
    COMP          = PAN.COMPANY
    COMP.CODE     = COMP[8,2]

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "ANNUAL.CARD.":PAN
    COMMA    = ","

    RETURN
*---------------------------------------------------
OFS.RECORD:
*----------
    COMP          = PAN.COMPANY
    OFS.USER.INFO = "AUTO.CHRGE//":COMP
    TRNS.TYPE     = "AC52"
    DR.ACC        = DB.ACC
    DR.CUR        = "EGP"
    CR.ACC        = "PL57040"
    AMOUNT        = DB.AMT

    IF AMOUNT GT 0 THEN
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE="    :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :DR.CUR:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :AMOUNT:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE="   :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "NOTE.DEBITED="        :ID.NEW:COMMA
        OFS.MESSAGE.DATA :=  "VERSION.NAME="        :",CARD.FEES2"

        OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA
        OFS.REC := COMMA:OFS.MESSAGE.DATA

        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN,NEW.FILE ON ERROR  TEXT = " ERROR ";CALL REM
    END
    RETURN
*----------------------------------------------
PROCESS:
*-------
    FN.CF = "F.SCB.ATM.CARD.FEES"   ; F.CF = "" ; R.CF = ""
    CALL OPF(FN.CF, F.CF)

    FN.CF2 = "F.SCB.CARD.FEES.ALL"  ; F.CF2 = "" ; R.CF2 = ""
    CALL OPF(FN.CF2, F.CF2)
*----------------------------------------------
    APP.FEES.ID = ""
    APP.FEES.ID = R.NEW(SCB.VISA.CARD.PRODUCT):".":R.NEW(SCB.VISA.CARD.TYPE)

    TMP.ID = PAN
    CALL F.READ(FN.CF2,TMP.ID,R.CF2,F.CF2,E.CF2)
    IF E.CF2 THEN
        R.CF2<FEES.ALL.APP.ATM.ID>  = PAN
        R.CF2<FEES.ALL.APP.FEES.ID> = ""
        R.CF2<FEES.ALL.AMOUNT>      = DB.AMT
        R.CF2<FEES.ALL.DB.ACCT>     = DB.ACC
        R.CF2<FEES.ALL.CR.ACCT>     = "PL57040"
        R.CF2<FEES.ALL.FLAG>        = "NO"
        R.CF2<FEES.ALL.COMPANY.ID>  = PAN.COMPANY
        CALL F.WRITE(FN.CF2 ,TMP.ID, R.CF2)
        CALL JOURNAL.UPDATE(TMP.ID)

        ACC.BAL = ""
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,DB.ACC,ACC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BAL=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DB.ACC,ACC.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF ACC.CAT GE 6500 AND ACC.CAT LE 6599 THEN
            IF ACC.BAL LT DB.AMT THEN
                CALL EOD.ANNUAL.CARD.FEES(DB.ACC,DB.AMT,PAN)
            END ELSE
                GOSUB OFS.RECORD
            END
        END ELSE
            GOSUB OFS.RECORD
        END
    END ELSE
        TEXT = "EXCUTE BEFORE" ; CALL REM
    END
    RETURN
*------------------------------------------------------------------
END
