* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMDcyMDE6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:56:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>71</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.REPRINT.CD.RE
*   PROGRAM SCB.REPRINT.CD.RE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*=============================================================================*
    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"  ; F.LD = ""
    CALL OPF(FN.LD,F.LD)
*=============================================================================*
*Line [ 45 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
*  CALL TXTINP('Enter Transaction Reference', 8, 23, '13', 'ANY')
*IF COMI THEN
        COMP = ID.COMPANY
        TD=TODAY
        CALL CDT('',TD, '-1W')
        T.SEL="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21001 AND CATEGORY LE 21012) AND SECTOR NE '' AND AMOUNT GT 0 AND LD.RENEW.DATE NE '' AND VALUE.DATE GT  ":TD:" AND CO.CODE EQ ":COMP:" BY CURRENCY BY CUSTOMER.ID "

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        TEXT = "SELECT = " : SELECTED ; CALL REM
        TEXT = "DATE   = " : TD ; CALL REM
 *       CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,ERR.LD)
 *       CATEG = R.LD<LD.CATEGORY>

        TEXT = "KEY.LIST<I> " : KEY.LIST ; CALL REM
        FOR I = 1 TO SELECTED
            TXN.ID = KEY.LIST<I>; ER.MSG = '' ; ETEXT = ''
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,ERR.LD)
            CATEG = R.LD<LD.CATEGORY>

            BEGIN CASE
            CASE TXN.ID[1,2] = 'LD'
                GOSUB SAVE.PARAMETERS
                GOSUB PROCESS.TT.RECORD
                IF NOT(ER.MSG) THEN
                    GOSUB REPRINT.SLIP
                END
                GOSUB RESTORE.PARAMETERS
            CASE OTHERWISE
                ER.MSG = 'Invalid Transaction Reference'
            END CASE
        NEXT I

        IF ER.MSG THEN
            TEXT = ER.MSG
            CALL REM
        END
    END
    GOTO PROGRAM.END
*=============================================================================*
SAVE.PARAMETERS:
*TEXT = "SAVE.PARAMETERS"  ; CALL REM
    SAVE.FUNCTION = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS = '' ; SIZE = ''
    TXN.NO = '' ; PROCESS = ''
    NBG.CODE = '' ; RECEIPT = ''
    RETURN
*=============================================================================*
RESTORE.PARAMETERS:
*TEXT = "RESTORE.PARAMETERS"  ; CALL REM
    MAT R.NEW = MAT R.SAVE
    V$FUNCTION = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW = SAVE.ID.NEW
    RETURN
*===================== PROCESS LD.LOANS.AND.DEPOSITS RECORD =================================*
PROCESS.TT.RECORD:
*TEXT = "PROCESS.TT.RECORD "  ; CALL REM
*    IF TXN.ID[3,5] = R.DATES(EB.DAT.JULIAN.DATE)[5] THEN
    IF TXN.ID # '' THEN
*TEXT = "HHHHHHHHHH = " : TXN.ID ; CALL REM
        VF.LD.LOANS.AND.DEPOSITS = '' ; CALL OPF('F.LD.LOANS.AND.DEPOSITS', VF.LD.LOANS.AND.DEPOSITS)
        ETEXT = ''
        DIM APP.REC(LD.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(LD.AUDIT.DATE.TIME)  ; MAT R.SAVE = ''
*         MAT R.NEW = MAT R.SAVE
        MAT R.SAVE = MAT R.NEW
        SIZE = LD.AUDIT.DATE.TIME
        CALL F.MATREAD('F.LD.LOANS.AND.DEPOSITS',TXN.ID,MAT APP.REC,SIZE,VF.LD.LOANS.AND.DEPOSITS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
***       APPLICATION = 'LD.LOANS.AND.DEPOSITS' ; ID.NEW = COMI ; V$FUNCTION = 'I'
            APPLICATION = 'LD.LOANS.AND.DEPOSITS' ; ID.NEW = KEY.LIST<I> ; V$FUNCTION = 'I'
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>[1,1] # ',' THEN
                VER.NAME = "LD.LOANS.AND.DEPOSITS,":R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
            END
            ELSE
                VER.NAME = "LD.LOANS.AND.DEPOSITS":R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
            END
            GOSUB GET.DSF.NAMES.ARRAY
        END
    END
    ELSE
        VF.LD.LOANS.AND.DEPOSITS$HIS = '' ; CALL OPF('F.LD.LOANS.AND.DEPOSITS$HIS', VF.LD.LOANS.AND.DEPOSITS$HIS)

*    TXN.ID := ';1'
        ETEXT = '' ; DIM APP.REC(LD.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(LD.AUDIT.DATE.TIME) ; MAT R.SAVE = ''
        SIZE = LD.AUDIT.DATE.TIME
        CALL F.MATREAD('F.LD.LOANS.AND.DEPOSITS$HIS',TXN.ID,MAT APP.REC,SIZE,VF.LD.LOANS.AND.DEPOSITS$HIS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''

        END
        ELSE
            IF R.NEW(LD.RECORD.STATUS) # 'REVE' THEN
****    APPLICATION = 'LD.LOANS.AND.DEPOSITS' ; ID.NEW = COMI ; V$FUNCTION = 'I'
                APPLICATION = 'LD.LOANS.AND.DEPOSITS' ; ID.NEW = KEY.LIST<I> ; V$FUNCTION = 'I'
                PARM.ID = 'LD.LOANS.AND.DEPOSITS,'
                PARM.ID := R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
                UPDATE.REC = 'F':R.COMPANY(EB.COM.MNEMONIC):'.LD.LOANS.AND.DEPOSITS$HIS'
                UPDATE.TS = 'YES'
            END
            ELSE
                ER.MSG = 'INVALID RECORD STATUS FOR REPRINT'
            END
        END
    END
    RETURN
*=============================================================================*
REPRINT.SLIP:
*TEXT = "REPRINT " ; CALL REM
* DSF.CNT = DCOUNT(R.DSF,@VM)

*TEXT = "DCOUNT " : DSF.CNT ; CALL REM
*IF DSF.CNT >= 1 THEN
*TEXT = "NEW " : DSF.CNT ; CALL REM
*TEXT = "R.DSF :" : R.DSF ; CALL REM
*  FOR DSF.LP = 1 TO DSF.CNT

*   CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP,1>))
* NEXT DSF.LP
* END
* ELSE
*  ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
* END


**  IF VER.NAME THEN
*TEXT = "VER.NAME " : VER.NAME ; CALL REM
* R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
***    CALL DBR("VERSION":FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
* TEXT = " R.DSF = "  : R.DSF ; CALL REM
* DSF.CNT = DCOUNT(R.DSF,@VM)
* TEXT = " VER.NAME = "  : VER.NAME ; CALL REM
* IF DSF.CNT > 1 THEN
* FOR DSF.LP = 1 TO DSF.CNT
* FOR J=1 TO 3
*TEXT = "H1" ; CALL REM

*    IF CATEG GE '21001' AND CATEG LE '21010' THEN
*        CALL PRODUCE.DEAL.SLIP("LD.OPEN.TERM.RE")
*    END
    IF CATEG EQ '21012' THEN
        CALL PRODUCE.DEAL.SLIP("LD.OPEN.OP9.RE")
    END ELSE
        CALL PRODUCE.DEAL.SLIP("LD.OPEN.TERM.RE")
    END
**** CALL PRODUCE.DEAL.SLIP("LD.OPEN.TERM.P")
*TEXT = "H2" ; CALL REM
* NEXT J
*NEXT DSF.LP
*** END
***   ELSE
***     ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
***   END
    RETURN

*=============================================================================*
GET.DSF.NAMES.ARRAY:
* TEXT = "VER.NAME :" : VER.NAME ; CALL REM
* IF VER.NAME THEN
*   R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
* CALL DBR("VERSION":FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
* IF NOT(ETEXT) THEN
*         TEXT = "ARRAY = ":R.DSF ; CALL REM
*     DSF.CNT = DCOUNT(R.DSF,@VM)
*         TEXT = "DSF COUNT = ":DSF.CNT  ; CALL REM
*  END
*  ELSE
*      ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
*  END
* END

*=================== SUBROUTINE END ... YES !!! ==============================*
PROGRAM.END:
*    NEXT I
    RETURN
END
