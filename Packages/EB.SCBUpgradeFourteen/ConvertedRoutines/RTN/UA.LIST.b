* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODU0Mzg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE UA.LIST(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*---------------------------------------
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF(FN.USR, F.USR)

*   T.SEL = "SELECT F.USER.ABBREVIATION WITH ORIGINAL.TEXT EQ '?426' BY DEPARTMENT.CODE"
    T.SEL = "SELECT F.USER.ABBREVIATION WITH ORIGINAL.TEXT EQ '?426' "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    Z = 1
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            USR.ID = KEY.LIST<II>
            CALL F.READ(FN.USR,USR.ID,R.USR,F.USR,ER.USR)

            DEPT.CODE = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
*TEXT = DEPT.CODE ; CALL REM

            IF DEPT.CODE NE '5100' THEN
                *TEXT = USR.ID : "*" : DEPT.CODE ; CALL REM
                ENQ<2,Z> = '@ID'
                ENQ<3,Z> = 'EQ'
                ENQ<4,Z> = USR.ID
                Z = Z + 1

            END
        NEXT II
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUMMY'
    END
*---------------------------------------
    RETURN
END
