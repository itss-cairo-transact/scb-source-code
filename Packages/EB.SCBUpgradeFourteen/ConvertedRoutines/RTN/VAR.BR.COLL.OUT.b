* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0OTE1NzQ6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:01:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE VAR.BR.COLL.OUT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*-----------------------------------------------------------
    FN.BR ='FBNK.BILL.REGISTER' ;R.BR ='';F.BR =''
    CALL OPF(FN.BR,F.BR)

    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='INAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID> THEN

                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>      = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>   = 7
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.RETURN.REASON>  = R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>    = TODAY
*               R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>      = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE> = TODAY

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
            END
            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>       = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>    = 8
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>     = TODAY
*               R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>       = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE>  = TODAY

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
            END
        NEXT NUMBERBR.ID
    END

    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='RNAU' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)
            IF R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID> THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  =  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.RETURN.REASON> =  ""
                MAT.EX = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                PLACE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
                HH     = "+":PLACE:"W"
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>     = MAT.EX

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*  PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
            END

            IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID>) THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  =  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>
                MAT.EX = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                PLACE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
                HH     = "+":PLACE:"W"
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>     = MAT.EX

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
                CALL F.WRITE(FN.BR,BR.ID, R.BR)
            END

        NEXT NUMBERBR.ID
    END
*-----------------------------------------------------------------
    RETURN
END
