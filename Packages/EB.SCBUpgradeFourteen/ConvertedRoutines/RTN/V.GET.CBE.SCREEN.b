* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzOTQyNTg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE V.GET.CBE.SCREEN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE

    FN.CBE   = 'F.SCB.MONTHLY.PAY.CBE'; F.CBE =''
    CALL OPF(FN.CBE,F.CBE)

    Y.SEL = ''
    TRANS.ID = ID.NEW
    CURR     = R.NEW(FT.DEBIT.CURRENCY)
    AMT.T    = R.NEW(FT.DEBIT.AMOUNT)


    IF V$FUNCTION = 'I' THEN
        INPUT.BUFFER = C.U:" SCB.MONTHLY.PAY.CBE,ENTER.NEW I ":C.F

        LINK.DATA<1> = TRANS.ID
        LINK.DATA<2> = CURR
        LINK.DATA<3> = AMT.T

    END
    IF V$FUNCTION = 'R' THEN
        Y.SEL  = "SELECT F.SCB.MONTHLY.PAY.CBE WITH REFERENCE EQ ":TRANS.ID
        CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            INPUT.BUFFER = C.U:" SCB.MONTHLY.PAY.CBE,ENTER.NEW R ": KEY.LIST<1>
        END
    END
    IF V$FUNCTION = 'D' THEN
        Y.SEL  = "SELECT F.SCB.MONTHLY.PAY.CBE$NAU WITH REFERENCE EQ ":TRANS.ID
        CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            INPUT.BUFFER = C.U:" SCB.MONTHLY.PAY.CBE,ENTER.NEW D ": KEY.LIST<1>

        END
    END
    IF V$FUNCTION = 'A' THEN
        Y.SEL  = "SELECT F.SCB.MONTHLY.PAY.CBE$NAU WITH REFERENCE EQ ":TRANS.ID
        CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            INPUT.BUFFER = C.U:" SCB.MONTHLY.PAY.CBE,ENTER.NEW A ": KEY.LIST<1>

        END

    END

    RETURN
END
