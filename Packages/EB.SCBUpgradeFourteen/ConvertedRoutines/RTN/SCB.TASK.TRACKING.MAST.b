* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMjAzODU6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:57:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>490</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY Mohamed Sabry
****************************
    SUBROUTINE SCB.TASK.TRACKING.MAST

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FILE.CONTROL
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TASK.PRIORITY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TASK.STATUS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TASK.TRACKING.MAST
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TASK.USR.INDEX
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.
*_________________________________________________________________________________________
    E = ''
    V$ERROR = ''
*_________________________________________________________________________________________
    FIR.ID.PART = 'TMS':R.DATES(EB.DAT.JULIAN.DATE)[5]
    IF LEN(COMI) <= 5 THEN
        COMI = STR('0',5 - LEN(COMI)) : COMI
        ID.NEW = FIR.ID.PART:COMI
    END
    IF ID.NEW[1,3] # 'TMS' THEN
        E = 'ENTER FULL ID OR PRESS F3'
        CALL ERR ; V$ERROR = 1
    END

    IF E THEN CALL ERR ; V$ERROR = 1

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

    IF E THEN
        T.SEQU = "IFLD"
*Line [ 249 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL

* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************
DELIVERY.PREVIEW:

    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:
    FN.STUI = "F.SCB.TASK.USR.INDEX"    ; F.STUI = ""
    CALL OPF(FN.STUI,F.STUI)
***************--------------------------- SUPERVISOR USER --------------------------------
*Line [ 360 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CNT.SUP.NEW   = DCOUNT(R.NEW(TMS.SUPERVISOR.USER),@VM)
*Line [ 362 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CNT.SUP.OLD   = DCOUNT(R.OLD(TMS.SUPERVISOR.USER),@VM)

    FOR ISUP.OLD = 1 TO WS.CNT.SUP.OLD
        WS.SUP.USR.OLD = R.OLD(TMS.SUPERVISOR.USER)<1,ISUP.OLD>
        FINDSTR WS.SUP.USR.OLD IN R.NEW(TMS.SUPERVISOR.USER) SETTING FSUPCHK, VSUPCHK THEN
*Line [ 368 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
  CALL F.READ(FN.STUI,WS.SUP.USR.OLD,R.STUI,F.STUI,ER.STUI)
**            READ  R.STUI FROM F.STUI , WS.SUP.USR.OLD THEN
**            END
            FINDSTR ID.NEW IN R.STUI<STUI.SUPERVISOR.TASKS> SETTING FSUPD, VSUPD THEN
                DEL R.STUI<STUI.SUPERVISOR.TASKS,VSUPD>
*                      CALL F.WRITE(FN.STUI,WS.SUP.USR.OLD,R.STUI)
**WRITE R.STUI ON F.STUI , WS.SUP.USR.OLD ON ERROR
**    TEXT = "ERROR DEL SUP" ; CALL REM
** END
                CALL F.WRITE(FN.STUI,WS.SUP.USR.OLD,R.STUI)
            END
        END
    NEXT ISUP.OLD
    IF R.NEW(TMS.SUPERVISOR.USER) NE '' THEN
        FOR ISUP.NEW = 1 TO WS.CNT.SUP.NEW
            WS.SUP.USR.NEW = R.NEW(TMS.SUPERVISOR.USER)<1,ISUP.NEW>
            CALL F.READ(FN.STUI,WS.SUP.USR.NEW,R.STUI,F.STUI,ER.STUI)
**            READ  R.STUI FROM F.STUI , WS.SUP.USR.NEW THEN
**            END
            FINDSTR ID.NEW IN R.STUI<STUI.SUPERVISOR.TASKS> SETTING FISUPCHK, VISUPCHK THEN
*Line [ 391 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                NULL
            END ELSE
*Line [ 394 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.CNT.TSUP = DCOUNT(R.STUI<STUI.SUPERVISOR.TASKS>,@VM)
                WS.CNT.TSUP ++
                R.STUI<STUI.SUPERVISOR.TASKS,WS.CNT.TSUP> = ID.NEW

*   CALL F.WRITE(FN.STUI,WS.SUP.USR.NEW,R.STUI)
**WRITE R.STUI ON F.STUI , WS.SUP.USR.NEW ON ERROR
**    TEXT = "ERROR IN SUP " ; CALL REM
**END
                CALL F.WRITE(FN.STUI,WS.SUP.USR.NEW,R.STUI)
            END
        NEXT ISUP.NEW
    END
**************************************************--------------------------------------------------------
***************--------------------------- OFFICIAL USER --------------------------------
*Line [ 409 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CNT.OFF.NEW   = DCOUNT(R.NEW(TMS.OFFICIAL.USER),@VM)
*Line [ 411 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CNT.OFF.OLD   = DCOUNT(R.OLD(TMS.OFFICIAL.USER),@VM)

    FOR IOFF.OLD = 1 TO WS.CNT.OFF.OLD
        WS.OFF.USR.OLD = R.OLD(TMS.OFFICIAL.USER)<1,IOFF.OLD>
        FINDSTR WS.OFF.USR.OLD IN R.NEW(TMS.OFFICIAL.USER) SETTING FOFFCHK, VOFFCHK THEN
*Line [ 417 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
  CALL F.READ(FN.STUI,WS.OFF.USR.OLD,R.STUI,F.STUI,ER.STUI)
**            READ  R.STUI FROM F.STUI , WS.OFF.USR.OLD THEN
**            END
            FINDSTR ID.NEW IN R.STUI<STUI.OFFICIAL.TASKS> SETTING FOFFD, VOFFD THEN
                DEL R.STUI<STUI.OFFICIAL.TASKS,VOFFD>
*                      CALL F.WRITE(FN.STUI,WS.OFF.USR.OLD,R.STUI)
**WRITE R.STUI ON F.STUI , WS.OFF.USR.OLD ON ERROR
**    TEXT = "ERROR DEL OFFICIAL " ; CALL REM
**END
                CALL F.WRITE(FN.STUI,WS.OFF.USR.OLD,R.STUI)
            END
        END
    NEXT IOFF.OLD
    IF R.NEW(TMS.OFFICIAL.USER) NE '' THEN
        FOR IOFF.NEW = 1 TO WS.CNT.OFF.NEW
            WS.OFF.USR.NEW = R.NEW(TMS.OFFICIAL.USER)<1,IOFF.NEW>
            CALL F.READ(FN.STUI,WS.OFF.USR.NEW,R.STUI,F.STUI,ER.STUI)
**            READ  R.STUI FROM F.STUI , WS.OFF.USR.NEW THEN
**            END
            FINDSTR ID.NEW IN R.STUI<STUI.OFFICIAL.TASKS> SETTING FIOFFCHK, VIOFFCHK THEN
*Line [ 440 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                NULL
            END ELSE
*Line [ 443 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.CNT.TOFF = DCOUNT(R.STUI<STUI.OFFICIAL.TASKS>,@VM)
                WS.CNT.TOFF ++
                R.STUI<STUI.OFFICIAL.TASKS,WS.CNT.TOFF> = ID.NEW

**WRITE R.STUI ON F.STUI , WS.OFF.USR.NEW ON ERROR
**    TEXT = "ERROR IN OFFICIAL " ; CALL REM
**END
                CALL F.WRITE(FN.STUI,WS.OFF.USR.NEW,R.STUI)
            END
        NEXT IOFF.NEW
    END
**************************************************--------------------------------------------------------
***************--------------------------- ASSISTANT USER --------------------------------
*Line [ 457 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CNT.AST.NEW   = DCOUNT(R.NEW(TMS.ASSISTANT.USER),@VM)
*Line [ 459 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CNT.AST.OLD   = DCOUNT(R.OLD(TMS.ASSISTANT.USER),@VM)

    FOR IAST.OLD = 1 TO WS.CNT.AST.OLD
        WS.AST.USR.OLD = R.OLD(TMS.ASSISTANT.USER)<1,IAST.OLD>
        FINDSTR WS.AST.USR.OLD IN R.NEW(TMS.ASSISTANT.USER) SETTING FASTCHK, VASTCHK THEN
*Line [ 465 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
        CALL F.READ(FN.STUI , WS.AST.USR.OLD,R.STUI,F.STUI , ERR1)
**            READ  R.STUI FROM F.STUI , WS.AST.USR.OLD THEN
**            END
            FINDSTR ID.NEW IN R.STUI<STUI.ASSISTANT.TASKS> SETTING FASTD, VASTD THEN
                DEL R.STUI<STUI.ASSISTANT.TASKS,VASTD>
**WRITE R.STUI ON F.STUI , WS.AST.USR.OLD ON ERROR
**    TEXT = "ERROR DEL ASSISTANT " ; CALL REM
**END
                CALL F.WRITE(FN.STUI,WS.AST.USR.OLD,R.STUI)
            END
        END
    NEXT IAST.OLD
    IF R.NEW(TMS.ASSISTANT.USER) NE '' THEN
        FOR IAST.NEW = 1 TO WS.CNT.AST.NEW
            WS.AST.USR.NEW = R.NEW(TMS.ASSISTANT.USER)<1,IAST.NEW>
**            READ  R.STUI FROM F.STUI , WS.AST.USR.NEW THEN
**            END
            CALL F.READ(FN.STUI , WS.AST.USR.NEW,R.STUI,F.STUI , ERR2)
            FINDSTR ID.NEW IN R.STUI<STUI.ASSISTANT.TASKS> SETTING FIASTCHK, VIASTCHK THEN
*Line [ 487 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                NULL
            END ELSE
*Line [ 490 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.CNT.TAST = DCOUNT(R.STUI<STUI.ASSISTANT.TASKS>,@VM)
                WS.CNT.TAST ++
                R.STUI<STUI.ASSISTANT.TASKS,WS.CNT.TAST> = ID.NEW

**WRITE R.STUI ON F.STUI , WS.AST.USR.NEW ON ERROR
**    TEXT = "ERROR IN ASSISTANT " ; CALL REM
** END
                CALL F.WRITE(FN.STUI,WS.AST.USR.NEW,R.STUI)
            END
        NEXT IAST.NEW
    END
**************************************************--------------------------------------------------------
    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
    CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
    CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
*Line [ 527 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = ""
    ID.CONCATFILE = ""

    ID.F  = "SUBJECT.ID"; ID.N  = "20"; ID.T  = "A"

    Z = 0
    Z+=1 ; F(Z)= "XX.TASK.NAME"            ; N(Z) = "40"      ; T(Z)= "A"
    Z+=1 ; F(Z)= "OPEN.DATE"               ; N(Z) = "15"      ; T(Z)= "D"

    Z+=1 ; F(Z)= "XX.TASK.FROM"            ; N(Z) = "04"      ; T(Z)= ""
*Line [ 558 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
    Z+=1 ; F(Z)= "XX.TASK.TO"              ; N(Z) = "04"      ; T(Z)= ""
*Line [ 561 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"

    Z+=1 ; F(Z)= "TASK.STATUS"          ; N(Z) = "05"      ; T(Z)= ""
*Line [ 565 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.TASK.STATUS":@FM:STS.DESCRIPTION:@FM:"L"

*    T( Z)< 2> ='Open_For Information_Under Research_Waiting Data_User Testing_Pending Approvals_Closed'

    Z+=1 ; F(Z)= "PRIORITY"                ; N(Z) = "05"      ; T(Z)= ""
*Line [ 571 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.TASK.PRIORITY":@FM:TP.DESCRIPTION:@FM:"L"

    Z+=1 ; F(Z)= "XX<LETTER.TYPE"          ; N(Z) = "15"      ; T(Z)= ""
    T( Z)< 2> ='Soft Copy In_Hard Copy In_Soft Copy Out_Hard Copy Out'
    Z+=1 ; F(Z)= "XX-XX.LETTER.SUBJECT"    ; N(Z) = "40"      ; T(Z)= "A"
    Z+=1 ; F(Z)= "XX-LETTER.NO"            ; N(Z) = "10"      ; T(Z)= "A"
    Z+=1 ; F(Z)= "XX-LETTER.DATE"          ; N(Z) = "15"      ; T(Z)= "D"
    Z+=1 ; F(Z)= "XX-XX.LETTER.DEPT"       ; N(Z) = "04"      ; T(Z)= ""
*Line [ 580 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
    Z+=1 ; F(Z)= "XX>XX.LETTER.EMP.USR"    ; N(Z) = "16"      ; T(Z)= "A"
*Line [ 583 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "USER":@FM:EB.USE.USER.NAME:@FM:"L"

    Z+=1 ; F(Z)= "SYSTEM.DATE"             ; N(Z) = "15"      ; T(Z)= "D"
    Z+=1 ; F(Z)= "CLOSED.DATE"             ; N(Z) = "15"      ; T(Z)= "D"
    Z+=1 ; F(Z)= "EXPECTED.DATE"           ; N(Z) = "15"      ; T(Z)= "D"

    Z+=1 ; F(Z)= "XX.SUPERVISOR.USER"      ; N(Z) = "16"      ; T(Z)= "A"
*Line [ 591 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "USER":@FM:EB.USE.USER.NAME:@FM:"L"
    Z+=1 ; F(Z)= "XX.OFFICIAL.USER"        ; N(Z) = "16"      ; T(Z)= "A"
*Line [ 594 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "USER":@FM:EB.USE.USER.NAME:@FM:"L"
    Z+=1 ; F(Z)= "XX.ASSISTANT.USER"       ; N(Z) = "16"      ; T(Z)= "A"
*Line [ 597 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "USER":@FM:EB.USE.USER.NAME:@FM:"L"

    Z+=1 ; F(Z)= "XX<NOTE.DATE"            ; N(Z) = "15"      ; T(Z)= "D"
    Z+=1 ; F(Z)= "XX>XX.NOTE"              ; N(Z) = "40"      ; T(Z)= "A"


    Z+=1 ; F(Z)= "XX<XX.DESCRIPTION"       ; N(Z) = "40"      ; T(Z)= "A"
    Z+=1 ; F(Z)= "XX-TEST.ENVIRONMENT"     ; N(Z) = "20"      ; T(Z)= "A"
    Z+=1 ; F(Z)= "XX-REF.TYPE"             ; N(Z) = "20"      ; T(Z)= ""
    T( Z)< 2> ='DATA_TEMPLATE_ROUTINE_VERSION_ENQUIRY_OTHER'
    Z+=1 ; F(Z)= "XX>TECHNICL.REF"         ; N(Z) = "45"      ; T(Z)= "A"

    Z+=1 ; F(Z)= "XX.REL.FILE.CONTROL"     ; N(Z) = "45"      ; T(Z)= "A"
*Line [ 611 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "FILE.CONTROL":@FM:EB.FILE.CONTROL.DESC:@FM:"L"

    Z+=1 ; F(Z)= "XX.RELATED.TASK"         ; N(Z) = "20"      ; T(Z)= "A"
*Line [ 615 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.TASK.TRACKING.MAST":@FM:TMS.TASK.NAME:@FM:"L"

    Z+=1 ; F(Z)= "XX<OFFICIAL.RECV"        ;  N(Z) = "35"    ;  T(Z)= "ANY"
*Line [ 619 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "USER":@FM:EB.USE.USER.NAME:@FM:"L"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "XX>RECV.TIME.D"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED9"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED8"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED7"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "XX.OVERRIDE"           ; N(Z) = "50"      ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9
    RETURN

*************************************************************************

END
