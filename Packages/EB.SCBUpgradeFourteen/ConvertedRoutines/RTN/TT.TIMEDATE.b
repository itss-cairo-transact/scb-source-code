* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODI1Mjg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
******NESSREEN AHMED*******
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.TIMEDATE(TIMU)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

    ETEXT = ''

*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('TELLER':@FM:TT.TE.DATE.TIME,TIMU,DATE.TIME)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,TIMU,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
DATE.TIME=R.ITSS.TELLER<TT.TE.DATE.TIME>
****UPDATED BY NESSREEN 15/08/2008****
    IF ETEXT THEN
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('TELLER$HIS':@FM:TT.TE.DATE.TIME,TIMU,DATE.TIME)
F.ITSS.TELLER$HIS = 'F.TELLER$HIS'
FN.F.ITSS.TELLER$HIS = ''
CALL OPF(F.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS)
CALL F.READ(F.ITSS.TELLER$HIS,TIMU,R.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS,ERROR.TELLER$HIS)
DATE.TIME=R.ITSS.TELLER$HIS<TT.TE.DATE.TIME>
       *** TEXT = "DATIME=":DATE.TIME ; CALL REM
        TIM = DATE.TIME[7,4]
        TIMHS = FMT(TIM,"R##:##")
        HH = TIMHS[4,2]
        SS = TIMHS[1,2]
        TIMU = SS:":":HH
    END ELSE
        TIM = DATE.TIME[7,4]
        TIMHS = FMT(TIM,"R##:##")
        HH = TIMHS[4,2]
        SS = TIMHS[1,2]
        TIMU = SS:":":HH
********
    END
********
    RETURN
END
