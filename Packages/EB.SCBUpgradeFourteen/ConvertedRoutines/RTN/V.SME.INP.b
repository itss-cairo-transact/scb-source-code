* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MTIxMDU6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE V.SME.INP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*----------------------------------------------

    CUS.NO        = ID.NEW

*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,CUS.DATA)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.DATA=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CREDIT.CODE = CUS.DATA<1,CULR.CREDIT.CODE>

    PAID          = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PAID.CAPITAL>
    TURNOVER      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.TURNOVER>
    TOTAL.ASSET   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.TOTAL.ASSETS>
    NO.OF.EMP     = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NO.OF.EMP>
    ESTAB.DATE    = R.NEW(EB.CUS.BIRTH.INCORP.DATE)
    START.DATE    = R.NEW(EB.CUS.LEGAL.ISS.DATE)
    FIN.SETT.DATE = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.FIN.STAT.DATE>

    IF CREDIT.CODE EQ 100 THEN
        IF (PAID EQ '' OR TURNOVER EQ '' OR TOTAL.ASSET EQ '' OR NO.OF.EMP EQ '' OR ESTAB.DATE EQ '' OR START.DATE EQ '' OR  FIN.SETT.DATE EQ '') THEN
            E = '���� ������ ������� �� ��� ����� ������� ������� ������'; CALL ERR  ; MESSAGE = 'REPEAT'
        END
        IF (CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120) THEN
            IF (TURNOVER EQ '' OR FIN.SETT.DATE EQ '') THEN
                E = '��� ����� ��� �������� ������ ���������'; CALL ERR  ; MESSAGE = 'REPEAT'
            END


        END
    END
END
