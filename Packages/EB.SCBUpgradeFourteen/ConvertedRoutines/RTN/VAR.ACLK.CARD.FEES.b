* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODY0MjQwNDI6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 12:00:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE VAR.ACLK.CARD.FEES

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CARD.FEES.ALL
*-------------------------------------------------
    FN.CF2 = "F.SCB.CARD.FEES.ALL"  ; F.CF2 = "" ; R.CF2 = ""
    CALL OPF(FN.CF2, F.CF2)

    TMP.ID = R.NEW(AC.LCK.DESCRIPTION)
    CALL F.READ(FN.CF2,TMP.ID,R.CF2,F.CF2,E.CF2)
    R.CF2<FEES.ALL.ACLK.REF.NO> = ID.NEW
    CALL F.WRITE(FN.CF2 ,TMP.ID, R.CF2)
*-------------------------------------------------
    RETURN
END
