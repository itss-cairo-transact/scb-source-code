* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYyMTAzMDU6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:56:50
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1651</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

    SUBROUTINE SCB.RISK.MAST

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.CLASS.CODE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.
*_________________________________________________________________________________________
    E = ''
    V$ERROR = ''
*_________________________________________________________________________________________

    IF E THEN CALL ERR ; V$ERROR = 1

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

    IF E THEN
        T.SEQU = "IFLD"
*Line [ 226 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL

* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************
DELIVERY.PREVIEW:

    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
    CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
    CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
*Line [ 360 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFourteen.ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = ""
    ID.CONCATFILE = ""

    ID.F  = "CUS.ID.DATE"; ID.N  = "17"; ID.T  = "A"

    Z = 0
    Z+=1 ; F(Z)= "CUSTOMER.ID"           ; N(Z) = "8"      ; T(Z)= ""
    Z+=1 ; F(Z)= "XX.CUSTOMER.NAME"      ; N(Z) = "35"      ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "GROUP.CODE"            ; N(Z) = "6"       ; T(Z)= ""
    Z+=1 ; F(Z)= "GROUP.NAME"            ; N(Z) = "35"      ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.01"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 392 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.02"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 395 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.03"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 398 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.04"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 401 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.05"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 404 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.06"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 407 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.07"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 410 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.08"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 413 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.09"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 416 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.10"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 419 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.11"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 422 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.12"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 425 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.13"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 428 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.14"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 431 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.15"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 434 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.16"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 437 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.17"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 440 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.18"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 443 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.19"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 446 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.20"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 449 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.21"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 452 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.22"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 455 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.23"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 458 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.24"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 461 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"
    Z+=1 ; F(Z)= "CLASS.ITEM.COD.25"     ; N(Z) = "6"       ; T(Z)= "ANY"
*Line [ 464 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.RISK.CLASS.CODE":@FM:RCC.CLASS.NAME.1:@FM:"L"

    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.01"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.02"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.03"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.04"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.05"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.06"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.07"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.08"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.09"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "CLASS.MAIN.ITEM.10"     ; N(Z) = "6"       ; T(Z)= "ANY"
    Z+=1 ; F(Z)= "SYSTEM.DATE"            ; N(Z) = "8"       ; T(Z)= ""

    Z+=1 ; F(Z)= "CUS.BR"                 ; N(Z) = "10"      ; T(Z)= ""
*Line [ 480 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "COMPANY":@FM:EB.COM.COMPANY.NAME:@FM:"L"
    Z+=1 ; F(Z)= "TOT.CUS"                 ; N(Z) = "6"      ; T(Z)= ""
    Z+=1 ; F(Z)= "TOT.ITEM"               ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED02"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED03"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED04"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED05"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED06"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED07"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED08"             ; N(Z) = "10"      ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED09"             ; N(Z) = "10"      ; T(Z)= ""

    Z+=1 ; F(Z)= "RESERVED20"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED21"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED22"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED23"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED24"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED25"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED26"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED27"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED28"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED29"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED30"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED31"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED32"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED33"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED34"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED35"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED36"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED37"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED38"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED39"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED40"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "RESERVED10"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED9"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED8"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED7"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9
    RETURN

*************************************************************************

END
