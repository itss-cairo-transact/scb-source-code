* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODI0NjQ6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
******NESSREEN AHMED*******
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.GET.INPU(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

    ETEXT = ''
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('TELLER':@FM:TT.TE.INPUTTER,ARG,INP)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
INP=R.ITSS.TELLER<TT.TE.INPUTTER>
****UPDATED BY NESSREEN 15/08/2008*****
    IF ETEXT THEN
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('TELLER$HIS':@FM:TT.TE.INPUTTER,ARG,INP)
F.ITSS.TELLER$HIS = 'F.TELLER$HIS'
FN.F.ITSS.TELLER$HIS = ''
CALL OPF(F.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS)
CALL F.READ(F.ITSS.TELLER$HIS,ARG,R.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS,ERROR.TELLER$HIS)
INP=R.ITSS.TELLER$HIS<TT.TE.INPUTTER>
       *** TEXT = 'INP=':INP ; CALL REM
        ARG = FIELD(INP,'_',2)
    END ELSE
***************************************
        ARG = FIELD(INP,'_',2)
********
    END
********
    RETURN
END
