* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzA4MTQ6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*********NESSREEN AHMED 15/4/2008
*-----------------------------------------------------------------------------
* <Rating>-93</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TOT.CUST.ACT.BAL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    F.ACCOUNT = '' ; FN.ACCOUNT = 'FBNK.ACCOUNT' ; R.ACCOUNT = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    F.LD = '' ; FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; R.LD = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.LD,F.LD)

    F.LC = '' ; FN.LC = 'FBNK.LETTER.OF.CREDIT' ; R.LC = '' ; E4 = '' ; RETRY4 = ''
    CALL OPF(FN.LC,F.LC)

    CUST.AC = '' ; CUST.LD = '' ; CUST.LC.IMP = '' ; CUST.LC.EXP = ''
*==================ACCOUNT SELECTION=============================================================
    ACC.SEL = "SELECT FBNK.ACCOUNT WITH ONLINE.ACTUAL.BAL NE '' AND ONLINE.ACTUAL.BAL NE 0 AND CUSTOMER NE ''  BY CUSTOMER"
    KEY.LIST.ACC ="" ; SELECTED.ACC="" ;  ER.MSG.ACC=""
    CALL EB.READLIST(ACC.SEL,KEY.LIST.ACC,"",SELECTED.ACC,ER.MSG.ACC)
    IF SELECTED.ACC THEN
        FOR ACC = 1 TO SELECTED.ACC
            R.ACCOUNT = ''
            CALL F.READ(FN.ACCOUNT,KEY.LIST.ACC<ACC>,R.ACCOUNT,F.ACCOUNT,E2)
            CUST.AC<ACC> = R.ACCOUNT<AC.CUSTOMER>
*** IF CUST.AC<ACC> # CUST.AC<ACC-1> THEN
            R.CUSTOMER = ''
            CALL F.READ(FN.CUSTOMER, CUST.AC<ACC>, R.CUSTOMER, F.CUSTOMER, E1)
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVE.CUST>= 'YES'
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVITY.DATE>= TODAY
            CALL F.WRITE(FN.CUSTOMER,CUST.AC<ACC>, R.CUSTOMER)
            CALL JOURNAL.UPDATE(CUST.AC<ACC>)
            R.CUSTOMER = ''
*** END
        NEXT ACC
    END   ;**SELECTED.ACC**
*==================LD SELECTION ====================================================================
    R.CUSTOMER = ''
    LD.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT NE 0 AND CUSTOMER.ID NE '' AND STATUS NE LIQ BY CUSTOMER.ID"
    KEY.LIST.LD ="" ; SELECTED.LD="" ;  ER.MSG.LD=""
    CALL EB.READLIST(LD.SEL,KEY.LIST.LD,"",SELECTED.LD,ER.MSG.LD)
    IF SELECTED.LD THEN
***TEXT = 'SELECTED.LD=':SELECTED.LD ; CALL REM
        FOR LD = 1 TO SELECTED.LD
            CALL F.READ(FN.LD,KEY.LIST.LD<LD>,R.LD,F.LD,E3)
            CUST.LD<LD> = R.LD<LD.CUSTOMER.ID>
*** IF CUST.LD<LD> # CUST.LD<LD-1> THEN
            R.CUSTOMER = ''
            CALL F.READ(FN.CUSTOMER,CUST.LD<LD>, R.CUSTOMER, F.CUSTOMER, E1)
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVE.CUST>= 'YES'
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVITY.DATE>= TODAY
            CALL F.WRITE(FN.CUSTOMER,CUST.LD<LD>, R.CUSTOMER)
            CALL JOURNAL.UPDATE(CUST.LD<LD>)
***  END
        NEXT LD
    END   ;**END OF SELECTED.LD**
*=================LC IMP SELECTION =================================================================
    R.CUSTOMER = ''
    LC.SEL.IMP = "SELECT FBNK.LETTER.OF.CREDIT WITH APPLICANT.CUSTNO NE '' AND LIABILITY.AMT NE 0 BY APPLICANT.CUSTNO"
    KEY.LIST.LC.IMP ="" ; SELECTED.LC.IMP="" ;  ER.MSG.LC.IMP=""
    CALL EB.READLIST(LC.SEL.IMP,KEY.LIST.LC.IMP,"",SELECTED.LC.IMP,ER.MSG.LC.IMP)
    IF SELECTED.LC.IMP THEN
        FOR LCC = 1 TO SELECTED.LC.IMP
            CALL F.READ(FN.LC,KEY.LIST.LC.IMP<LCC>,R.LC,F.LC,E4)
            CUST.LC.IMP<LCC> = R.LC<TF.LC.APPLICANT.CUSTNO>
***  IF CUST.LC.IMP<LCC> # CUST.LC.IMP<LCC-1> THEN
            R.CUSTOMER = ''
            CALL F.READ(FN.CUSTOMER,CUST.LC.IMP<LCC> , R.CUSTOMER, F.CUSTOMER, E1)
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVE.CUST>= 'YES'
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVITY.DATE>= TODAY
            CALL F.WRITE(FN.CUSTOMER,CUST.LC.IMP<LCC>, R.CUSTOMER)
            CALL JOURNAL.UPDATE(CUST.LC.IMP<LCC>)
*** END
        NEXT LCC
    END
*=================LC EXP SELECTION =================================================================
    R.CUSTOMER = ''
    LC.SEL.EXP = "SELECT FBNK.LETTER.OF.CREDIT WITH BENEFICIARY.CUSTNO NE '' AND LIABILITY.AMT NE 0 BY BENEFICIARY.CUSTNO"
    KEY.LIST.LC.EXP ="" ; SELECTED.LC.EXP="" ;  ER.MSG.LC.EXP=""
    CALL EB.READLIST(LC.SEL.EXP,KEY.LIST.LC.EXP,"",SELECTED.LC.EXP,ER.MSG.LC.EXP)
    IF SELECTED.LC.EXP THEN
        FOR LCEXP = 1 TO SELECTED.LC.EXP
            CALL F.READ(FN.LC,KEY.LIST.LC.EXP<LCEXP>,R.LC,F.LC,E4)
            CUST.LC.EXP<LCEXP> = R.LC<TF.LC.BENEFICIARY.CUSTNO>
***  IF CUST.LC.EXP<LCEXP> # CUST.LC.EXP<LCEXP-1> THEN
            IF CUST.LC.EXP<LCEXP-1> = '1305027' THEN
            END
            R.CUSTOMER = ''
******UPDATED BY NESSREEN AHMED ON 1/3/2009*******************
***  CALL F.READ(FN.CUSTOMER,CUST.LC.IMP<LCC> , R.CUSTOMER, F.CUSTOMER , E1)
            CALL F.READ(FN.CUSTOMER,CUST.LC.EXP<LCEXP> , R.CUSTOMER, F.CUSTOMER , E1)
***************************************************************
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVE.CUST>= 'YES'
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVITY.DATE>= TODAY
            CALL F.WRITE(FN.CUSTOMER,CUST.LC.EXP<LCEXP>, R.CUSTOMER)
******UPDATED BY NESSREEN AHMED ON 1/3/2009*******************
***   CALL JOURNAL.UPDATE(CUST.LC.IMP<LCEXP>)
            CALL JOURNAL.UPDATE(CUST.LC.EXP<LCEXP>)
****************************************************************
***    END
        NEXT LCEXP
    END
*==============================================================
*===CUSTOMER SELECTION =================================================================================
    R.CUSTOMER = ''
****UPDATED ON 10/08/2008*******************
    CUS.SEL = "SELECT FBNK.CUSTOMER WITH ACTIVE.CUST EQ YES AND ACTIVITY.DATE LT ": TODAY
****UPDATED ON 17/08/2008******************
***    CUS.SEL = "SSELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ 1 AND POSTING.RESTRICT NE 80 AND POSTING.RESTRICT NE 90 AND POSTING.RESTRICT NE 99 AND (ACTIVITY.DATE EQ '' OR ACTIVITY.DATE LT ":TODAY:")"
*    CUS.SEL = "SELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ 1 AND POSTING.RESTRICT EQ 90 "
*    CUS.SEL = "SELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ 1 AND POSTING.RESTRICT EQ 99 AND ACTIVE.CUST EQ '' "
****    CUS.SEL = "SELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ 1 AND ACTIVE.CUST EQ '' "
****END OF UPDATED**************************
    KEY.CUS ="" ; SELECTED.CUS="" ;  ER.MSG.CUS=""
    CALL EB.READLIST(CUS.SEL,KEY.CUS,"",SELECTED.CUS,ER.MSG.CUS)
    IF SELECTED.CUS THEN
        FOR CUS = 1 TO SELECTED.CUS
            F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = '' ; E1 = ''
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
            CALL F.READ(FN.CUSTOMER,KEY.CUS<CUS> , R.CUSTOMER, F.CUSTOMER , E1)
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVE.CUST>= 'NO'
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ACTIVITY.DATE>= TODAY
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
            CALL F.WRITE(FN.CUSTOMER,KEY.CUS<CUS>, R.CUSTOMER)
            CALL JOURNAL.UPDATE(KEY.CUS<CUS>)
*****************

**   WRITE  R.CUSTOMER TO F.CUSTOMER , KEY.CUS<CUS> ON ERROR
**     PRINT "CAN NOT WRITE RECORD":KEY.CUS<CUS>:"TO" :FN.CUSTOMER
** END

        NEXT CUS
    END
    RETURN
END
