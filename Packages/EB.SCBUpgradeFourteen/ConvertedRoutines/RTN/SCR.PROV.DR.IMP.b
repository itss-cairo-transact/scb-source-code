* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzMjcyODY6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:58:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*************************FOR DEBIT CHARGE AMMEDMENT(NI7OOOOOOOOO)*************
    SUBROUTINE SCR.PROV.DR.IMP

**      PROGRAM SCR.PROV.DR.IMP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB GET.DATA
    IF MARG.FLG EQ 'Y' THEN
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� �������" ; CALL REM
    END
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCR.PROV.DR.IMP'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
GET.DATA:
*---------------------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG="" ;OUT.AMOUNT=""

    IF ID.NEW EQ '' THEN
        FN.LC = 'FBNK.LETTER.OF.CREDIT'
    END ELSE
        FN.LC = 'FBNK.LETTER.OF.CREDIT$NAU'
    END
    F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    IF ID.NEW EQ '' THEN
        YTEXT = "Enter the TF No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        LC.ID =COMI[1,12]
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E1)
    END ELSE
******************************************
        IF ID.NEW NE '' THEN
            LC.ID =ID.NEW
            CALL F.READ(FN.LC,ID.NEW,R.LC,F.LC,E1)
        END
    END
*******************************************
    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = 'SCR.PROV.DR.IMP'
*------------------------------------------------------------------------
    CHARGE.ACC=R.LC<TF.LC.PROVIS.ACC>
***   MARGIN.ACCOUNT=R.NEW(TF.LC.PROVIS.ACC)

    ACC.NO = CHARGE.ACC
    CUR    = R.LC<TF.LC.CHARGE.CURRENCY>
*** CUR    = CHARGE.ACC[9,2]

    REFERENCE = R.LC<TF.LC.OLD.LC.NUMBER>
*Line [ 104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 125 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
***    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
    BRN.ID  = AC.COMP[2]
    ACC.BR = TRIM(BRN.ID,"0","L")

*Line [ 143 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CUR1)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR1=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
*Line [ 153 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
*****    MARGIN.AMOUNT    = R.LC<TF.LC.PROVIS.AMOUNT>
    MARGIN.AMOUNT    = R.LC<TF.LC.DB.PROV.AMOUNT>
    MARG.FLG = R.LC<TF.LC.PROVISION>

    IN.AMOUNT = MARGIN.AMOUNT

    IF IN.AMOUNT = 0 THEN
        OUT.AMOUNT = ''
    END
    IF IN.AMOUNT NE 0 THEN
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR1 : ' ' : '�����'
    END

    MARGIN.PERC = R.LC<TF.LC.PROVIS.PERCENT>
    IF ID.NEW NE '' THEN
        INPUTT = R.NEW(TF.LC.INPUTTER)
        INP   = FIELD(INPUTT,'_',2)
        AUTHI=OPERATOR
    END ELSE
        INPUTT = R.LC<TF.LC.INPUTTER>
        INP   = FIELD(INPUTT,'_',2)
        AUTH = R.LC<TF.LC.AUTHORISER >
        AUTHI   = FIELD(AUTH,'_',2)


    END
    RETURN
*------------------------------------------------------------------------
PROCESS:
    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

* XX1<1,1>[3,35]   = COMM.AC.NO
    XX1<1,1>[3,35]   = CUST.NAME
    XX2<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = MARGIN.AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[50,15] = '��� ������ : '
    XX2<1,1>[63,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR1

* XX4<1,1>[45,15] = '���� ������� : '
* XX4<1,1>[59,15] = MARGIN.PERC
    XX4<1,1>[45,15] = '������ :'
    XX4<1,1>[59,15] = REFERENCE

    XX6<1,1>[30,15]  = '������'
    XX6<1,1>[1,15] = '��� �������'

    XX7<1,1>[1,15] = LC.ID
    XX7<1,1>[30,15] = INP

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������'
    XX9<1,1>[20,15] = AUTHI

*-------------------------------------------------------------------
*Line [ 230 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACC.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(45) : "����� ������� ��� "
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX<1,1>
* PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
