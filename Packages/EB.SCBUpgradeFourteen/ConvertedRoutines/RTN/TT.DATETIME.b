* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzc1MTM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
******NESSREEN AHMED*******
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.DATETIME(NEWDAT)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

    ETEXT = ''
*******UPDATED BY NESSREEN 25/05/2010***********************************
*****UPDATED BY NESSREEN 23/05/2010*******************************
***** CALL DBR('TELLER':@FM:TT.TE.DATE.TIME,NEWDAT,DATE.TIME)
*******CALL DBR('TELLER':@FM:TT.TE.AUTH.DATE,NEWDAT,AUTH.D)
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('TELLER':@FM:TT.TE.DATE.TIME,NEWDAT,DATE.TIME)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,NEWDAT,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
DATE.TIME=R.ITSS.TELLER<TT.TE.DATE.TIME>
****UPDATED BY NESSREEN 15/08/2008************
    IF ETEXT THEN
*****UPDATED BY NESSREEN 23/05/2010*****************************
***** CALL DBR('TELLER$HIS':@FM:TT.TE.DATE.TIME,NEWDAT,DATE.TIME)
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('TELLER$HIS':@FM: TT.TE.AUTH.DATE,NEWDAT,AUTH.D)
F.ITSS.TELLER$HIS = 'F.TELLER$HIS'
FN.F.ITSS.TELLER$HIS = ''
CALL OPF(F.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS)
CALL F.READ(F.ITSS.TELLER$HIS,NEWDAT,R.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS,ERROR.TELLER$HIS)
AUTH.D=R.ITSS.TELLER$HIS<TT.TE.AUTH.DATE>
        DATE.TIME = AUTH.D<1,1>
        DAT = DATE.TIME[1,8]
        DATU = FMT(DAT,"R##/##/##")
**EDIT BY NESSMA 2011/02/08
*        DATU = "20":DATU
        TOD.D = TODAY
        DATU = TOD.D[1,2]:DATU
*****
        NEWDAT = DATU
    END ELSE
*  DATE.TIME = DATE.TIME<1,1>
*  DAT = DATE.TIME[1,6]
*  DATU = FMT(DAT,"R##/##/##")
*  TOD.D = TODAY
*  DATU1 = TOD.D[1,2]:DATU
*  NEWDAT = DATU1

        DATE.TIME = DATE.TIME<1,1>
        DAT = DATE.TIME[1,6]
        DATU = FMT(DAT,"R##/##/##")
        NEWDAT = DATU

    END
    RETURN
END
