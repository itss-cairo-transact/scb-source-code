* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODAxMTg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
******NESSREEN AHMED 31/5/2010*******
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TT.GET.AMT(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

    ETEXT = '' ; CURR = ''  ; AMT = ''
*Line [ 30 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*    CALL DBR('TELLER':@FM:TT.TE.CURRENCY.1,ARG,CURR)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
CURR=R.ITSS.TELLER<TT.TE.CURRENCY.1>
**    CUR = R.NEW(TT.TE.CURRENCY.1)
    IF CURR = 'EGP' THEN
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*        CALL DBR('TELLER':@FM:TT.TE.AMOUNT.LOCAL.1,ARG,AMT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
AMT=R.ITSS.TELLER<TT.TE.AMOUNT.LOCAL.1>
      **  AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    END ELSE
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*       CALL DBR('TELLER':@FM:TT.TE.AMOUNT.FCY.1,ARG,AMT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ARG,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
AMT=R.ITSS.TELLER<TT.TE.AMOUNT.FCY.1>
      **  AMT = R.NEW(TT.TE.AMOUNT.FCY.1)
    END

    ARG = AMT
    RETURN
END
