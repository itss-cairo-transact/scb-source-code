* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNTY0NTg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
*   SUBROUTINE STMT.ACCT.DR
    PROGRAM STMT.ACCT.DR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*****
    OPENSEQ "&SAVEDLISTS&" , "STMT.ACCT.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"STMT.ACCT.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "STMT.ACCT.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE STMT.ACCT.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create STMT.ACCT.TXT File IN &SAVEDLISTS&'
        END
    END

*****
    FN.AC  = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.ST  = 'FBNK.STMT.ACCT.DR' ; F.ST = '' ; R.ST = ''
    CALL OPF( FN.ST,F.ST)

*---------------------------
    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...20100930 AND GRAND.TOTAL GT 0 "
    T.SEL:= " AND @ID UNLIKE ...1220...20100930 AND @ID UNLIKE ...1221...20100930 AND @ID UNLIKE ...1222...20100930"
    T.SEL:= " AND @ID UNLIKE ...1223...20100930 AND @ID UNLIKE ...1224...20100930 AND @ID UNLIKE ...1225...20100930"
    T.SEL:= " AND @ID UNLIKE ...1226...20100930 AND @ID UNLIKE ...1227...20100930 AND @ID UNLIKE ...9090...20100930"
    T.SEL:= " AND @ID UNLIKE ...1205...20100930 AND @ID UNLIKE ...1206...20100930 AND @ID UNLIKE ...1207...20100930"
    T.SEL:= " AND @ID UNLIKE ...1208...20100930 "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT SELECTED

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ST,KEY.LIST<I>, R.ST, F.ST, ETEXT)
            STMT.ID       = KEY.LIST<I>
            ACCT.ID       = FIELD(STMT.ID,'-',1)
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-19
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.ID,CATEG.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG.ID=R.ITSS.ACCOUNT<AC.CATEGORY>

            IF CATEG.ID NE 1220 AND CATEG.ID NE 1221 AND CATEG.ID NE 1223 AND CATEG.ID NE 1224 AND CATEG.ID NE 1225 AND CATEG.ID NE 1226 AND CATEG.ID NE 1227 AND CATEG.ID NE 9090 AND CATEG.ID NE 1205 AND CATEG.ID NE 1206 AND CATEG.ID NE 1207 AND CATEG.ID NE 1208 THEN
                GRAND.TOT     = R.ST<IC.STMDR.GRAND.TOTAL>
                INT.RATE      = R.ST<IC.STMDR.DR.INT.RATE><1,1>
                HIGHEST.PERC  = R.ST<IC.STMDR.HIGHEST.DR.PERC>

                BB.DATA  = ACCT.ID:'|'
                BB.DATA := GRAND.TOT:'|'
                BB.DATA := INT.RATE:'|'
                BB.DATA := HIGHEST.PERC

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
