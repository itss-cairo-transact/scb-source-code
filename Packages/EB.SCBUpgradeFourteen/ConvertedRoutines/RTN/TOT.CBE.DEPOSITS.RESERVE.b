* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNzA2Nzg6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* CREATE BY MAHMOUD 2010
* EDIT BY NESSMA 26/12/2017
*-----------------------------------------------------------------------------
    SUBROUTINE TOT.CBE.DEPOSITS.RESERVE
*    PROGRAM TOT.CBE.DEPOSITS.RESERVE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.DEPOSITS.RESERVE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TEMP.POS
*-------------------------------------------------------
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 47 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 48 ] Adding EB_SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-19
*Line [ 49 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    K  = 0
    K2 = 0
    DDY.NO = 0
    TDD1 = TODAY
    CALL CDT("",TDD1,'-1W')
    TDD = TDD1[6]
    TDDX = '20100309'
    TDDX1= TDDX
    CALL CDT("",TDDX1,'+13C')
    CB.ID.FIRST = TDDX:"-":TDDX1
    ACC.ID = ''
    LD.ID  = ''
    TDDN = TDD1
    TDDV = TDD1
    CALL CDT("",TDDV,'+1W')
    CRT TDDN :"-":TDDV
    CALL CDD("C",TDDN,TDDV,DDY.NO)
    CRT "NO OF DAYS : ":DDY.NO
    KEY.LIST  = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2 = '' ; SELECTED2 = '' ; ERR.2 = ''
    BAL.1 = ''
    BAL.2 = ''
    BAL.3 = ''
    BAL.4 = ''

    TTMM = R.DATES(EB.DAT.LAST.PERIOD.END)

    RETURN
*=======================================
CALLDB:
*-------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CB = 'F.SCB.CBE.DEPOSITS.RESERVE' ; F.CB = '' ; R.CB = ''
    CALL OPF(FN.CB,F.CB)
    FN.TMP = 'F.SCB.TEMP.POS' ; F.TMP = '' ; R.TMP = ''
    CALL OPF(FN.TMP,F.TMP)

    FN.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET'
    F.MM.MONEY.MARKET  = '' ; R.MM.MONEY.MARKET = ''
    CALL OPF( FN.MM.MONEY.MARKET,F.MM.MONEY.MARKET)
    RETURN
*========================================
PROCESS:
*-------
*----------------------------------AC-------------------------
    AMM = 0
    SEL.CMD  = "SELECT ":FN.ACC:" WITH OPEN.ACTUAL.BAL NE ''"
    SEL.CMD := " AND CURRENCY EQ EGP AND OPEN.ACTUAL.BAL GT 0 "
    SEL.CMD := " AND ( CATEGORY IN ( 3016 3015 3010 3011 3012 3013"
    SEL.CMD := " 3001 6517 6516 6515 1290 1008 1211 1212 1217"
    SEL.CMD := " 1011 1101 1102 1103 1201 1202 1003 3014 3017 1301"
    SEL.CMD := " 1303 6501 6502 6503 6504 6511 6512 1012 1013 1015"
    SEL.CMD := " 1016 16113 1019 16170 3050 1005 1059 1014 6505 6506"
    SEL.CMD := " 6507 6508 6514 3060 2006 3065 6513 1007 1001 1002"
    SEL.CMD := " 1203 1204 1205 1206 1207 1208 1216 1415 1006 2001 )"
    SEL.CMD := "  OR (CATEGORY GE 1100 AND CATEGORY LE 1219)"
    SEL.CMD := "  OR (CATEGORY GE 1228 AND CATEGORY LE 1400)"
    SEL.CMD := "  OR (CATEGORY GE 1446 AND CATEGORY LE 1599)"
    SEL.CMD := "  OR (CATEGORY GE 1401 AND CATEGORY LE 1437))"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.MSG)
    LOOP
        REMOVE ACC.ID FROM KEY.LIST SETTING POSS
    WHILE ACC.ID:POSS
        K++

        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ERR.1)
        ACC.COM = R.ACC<AC.CO.CODE>
        ACC.CAT = R.ACC<AC.CATEGORY>
        ACC.CUR = R.ACC<AC.CURRENCY>
        ACC.BAL = R.ACC<AC.OPEN.ACTUAL.BAL>

        TM.COM = ACC.COM
        TM.CAT = ACC.CAT
        TM.BAL = ACC.BAL
        TM.ID  = ACC.ID
*???        GOSUB ADD.TEMP
*************************************
        IF ACC.CAT EQ 2001 AND ACC.BAL GT 0 THEN
            BAL.2 += ACC.BAL
        END ELSE
            IF ACC.BAL GT 0 THEN
                BAL.1 += ACC.BAL
            END
        END
    REPEAT
*-----------------------------MM-------------------------
    SEL.CMD3  = "SELECT FBNK.MM.MONEY.MARKET WITH MATURITY.DATE GT ":TTMM
    SEL.CMD3 := " AND CATEGORY EQ 21031 AND CURRENCY EQ 'EGP'"
    SEL.CMD3 := " AND PRINCIPAL GT 0"
    CALL EB.READLIST(SEL.CMD3,KEY.LIST3,"",SELECTED3,ER.MSG3.MM)
    IF SELECTED3 THEN
        FOR NN = 1 TO SELECTED3
            CALL F.READ(FN.MM.MONEY.MARKET,KEY.LIST3<NN>,R.MM.MONEY.MARKET,F.MM.MONEY.MARKET, MM.ETEXT)
            AMT    = R.MM.MONEY.MARKET<MM.PRINCIPAL>
            BAL.2 += AMT
        NEXT NN
    END
*-----------------------------LD-------------------------
    SEL.CMD2  = "SELECT ":FN.LD:" WITH CUSTOMER.ID NE ''"
    SEL.CMD2 := " AND CURRENCY EQ EGP"
    SEL.CMD2 := " AND (( VALUE.DATE LE ":TTMM:" AND AMOUNT NE 0 )"
    SEL.CMD2 := "  OR  ( FIN.MAT.DATE GT ":TTMM:" AND AMOUNT EQ 0 ))"
    SEL.CMD2 := " AND (( CATEGORY GE 21001 AND CATEGORY LE 21011 )"
    SEL.CMD2 := " OR ( CATEGORY GE 21017 AND CATEGORY LE 21029 )"
    SEL.CMD2 := " OR ( CATEGORY EQ 21031 OR CATEGORY EQ 21041 )"
    SEL.CMD2 := " OR ( CATEGORY EQ 21042  OR CATEGORY EQ 21014 ))"
    CALL EB.READLIST(SEL.CMD2,KEY.LIST2,'',SELECTED2,ERR.2)
    LOOP
        REMOVE LD.ID FROM KEY.LIST2 SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        LD.COM  = R.LD<LD.CO.CODE>
        LD.CAT  = R.LD<LD.CATEGORY>
        LD.CUR  = R.LD<LD.CURRENCY>
        LD.BAL  = R.LD<LD.AMOUNT>
*************************************
        TM.COM = LD.COM
        TM.CAT = LD.CAT
        TM.BAL = LD.BAL
        TM.ID  = LD.ID
*???        GOSUB ADD.TEMP
*************************************
        IF LD.BAL EQ 0 THEN
            LD.BAL  = R.LD<LD.REIMBURSE.AMOUNT>
        END
        IF ((LD.CAT GE 21017 AND LD.CAT LE 21029) OR ( LD.CAT EQ 21041 OR LD.CAT EQ 21042 )) AND ( LD.BAL GT 0 ) THEN
            BAL.1 += LD.BAL
            BAL.3 += LD.BAL
        END ELSE
            IF LD.BAL GT 0 AND LD.CAT NE 21031 THEN
                BAL.1 += LD.BAL
            END
        END
    REPEAT

    FOR NN = 1 TO DDY.NO
        GOSUB ADD.REC
        CALL CDT("",TDDN,'+1C')
    NEXT NN
    RETURN
*............................................................
ADD.REC:
********
    CB.SEL = "SELECT ":FN.CB:" BY @ID"
    CALL EB.READLIST(CB.SEL,KEY.LIST,'',SELECTED,ERR.MSG)
    CB.ID = KEY.LIST<SELECTED>
    IF CB.ID EQ '' THEN
        CB.ID = CB.ID.FIRST
    END
    END.PERIOD = FIELD(CB.ID,'-',2)
    IF TDDN GT END.PERIOD THEN
        S.PERIOD = END.PERIOD
        CALL CDT("",S.PERIOD,'+1C')
        E.PERIOD = S.PERIOD
        CALL CDT("",E.PERIOD,'+13C')
        CB.ID = S.PERIOD:"-":E.PERIOD
    END
***** WRITE TO TEMP FILE *************************
    CALL F.READ(FN.CB,CB.ID,R.CB,F.CB,ER.CB)
*Line [ 217 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(R.CB<CBE.BAL.DATE>,@VM)
    DD1 = DD + 1
    R.CB<CBE.BAL.DATE,DD1>  = TDDN
    R.CB<CBE.DEP.BAL.1,DD1> = BAL.1
    R.CB<CBE.DEP.BAL.2,DD1> = BAL.2
    R.CB<CBE.DEP.BAL.3,DD1> = BAL.3
    R.CB<CBE.DEP.BAL.4,DD1> = BAL.4
    CALL F.WRITE(FN.CB,CB.ID,R.CB)
    CALL JOURNAL.UPDATE(BR.ID)
    RETURN
*************************************************
ADD.TEMP:
    TMP.ID = TM.ID
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
    R.TMP<TP.TEMP.AMT1> = TM.COM
    R.TMP<TP.TEMP.AMT2> = TM.CAT
    R.TMP<TP.TEMP.AMT3> = TM.BAL
    R.TMP<TP.TEMP.AMT4> = TM.ID
    CALL F.WRITE(FN.TMP,TMP.ID,R.TMP)
    CALL JOURNAL.UPDATE(TMP.ID)
    RETURN
*************************************************
END
