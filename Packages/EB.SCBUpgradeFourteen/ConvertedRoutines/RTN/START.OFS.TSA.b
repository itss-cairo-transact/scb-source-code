* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzNTYyNTE6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE START.OFS.TSA (TSA.ID)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TSA.SERVICE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH

    GOSUB INITIALISE
    GOSUB MAIN.PROCESS

    RETURN
*
**
*
INITIALISE:
*---------

    FN.TSA.SERVICE = "F.TSA.SERVICE"
    F.TSA.SERVICE = ""
    CALL OPF(FN.TSA.SERVICE,F.TSA.SERVICE)

    FN.PGM.FILE = "F.PGM.FILE"
    F.PGM.FILE = ''
    CALL OPF(FN.PGM.FILE,F.PGM.FILE)

    FN.BATCH = "F.BATCH"
    F.BATCH = ""
    CALL OPF(FN.BATCH,F.BATCH)

    RETURN
*
**
*
MAIN.PROCESS:
*-----------

    GOSUB READ.ORIG.DETAILS

    IF PGM.READ.ERR THEN
        ETEXT = PGM.READ.ERR
        RETURN
    END

    IF BATCH.READ.ERR THEN
        ETEXT = BATCH.READ.ERR
        RETURN
    END

    IF TSA.READ.ERR THEN
        ETEXT = TSA.READ.ERR
        RETURN
    END

    NEW.TSA.ID = TSA.ID:".T":TNO
*=============== CREATE PGM.FILE UPDATED BY BAKRY 13/06/2011======
    NEW.TSA.ID.PGM = CHANGE(NEW.TSA.ID,"BNK/","")
*=================================================================
    READ.ERR = ""
    CALL F.READ(FN.PGM.FILE,NEW.TSA.ID.PGM,NEW.R.PGM.FILE,F.PGM.FILE,READ.ERR)
    IF READ.ERR THEN
        NEW.R.PGM.FILE = R.PGM.FILE
        CALL F.WRITE(FN.PGM.FILE,NEW.TSA.ID.PGM,NEW.R.PGM.FILE)
    END

    READ.ERR = ""
    CALL F.READ(FN.BATCH,NEW.TSA.ID,NEW.R.BATCH,F.BATCH,READ.ERR)
    IF READ.ERR THEN
        NEW.R.BATCH = R.BATCH
        NEW.R.BATCH<BAT.JOB.NAME> = NEW.TSA.ID.PGM
        CALL F.WRITE(FN.BATCH,NEW.TSA.ID,NEW.R.BATCH)
    END

    READ.ERR = ""
    CALL F.READ(FN.TSA.SERVICE,NEW.TSA.ID,NEW.R.TSA.SERVICE,F.TSA.SERVICE,READ.ERR)
    IF READ.ERR THEN
        NEW.R.TSA.SERVICE = R.TSA.SERVICE
    END

    NEW.R.TSA.SERVICE<TS.TSM.SERVICE.CONTROL> = "START"
    CALL F.WRITE(FN.TSA.SERVICE,NEW.TSA.ID,NEW.R.TSA.SERVICE)

    RETURN
*
**
*
READ.ORIG.DETAILS:
*----------------

    CALL F.READ(FN.PGM.FILE,TSA.ID,R.PGM.FILE,F.PGM.FILE,PGM.READ.ERR)
    CALL F.READ(FN.BATCH,TSA.ID,R.BATCH,F.BATCH,BATCH.READ.ERR)
    CALL F.READ(FN.TSA.SERVICE,TSA.ID,R.TSA.SERVICE,F.TSA.SERVICE,TSA.READ.ERR)

    RETURN
END
