* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzODg3OTA6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE USER.LIST.COMP(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    FN.USR   = 'FBNK.USER'     ; F.USR  = ''
    CALL OPF(FN.USR,F.USR)

    COMP.ID   = ID.COMPANY
    DEPT.CODE = COMP.ID[8,2]
    IF DEPT.CODE[1,1] EQ 0 THEN
        DEPT.CODE = COMP.ID[9,1]
    END

    TOD = TODAY
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*************************************************************************
    T.SEL  = "SELECT ":FN.USR
    T.SEL := " WITH @ID LIKE SCB... AND"
    T.SEL := " END.DATE.PROFILE GT ":TOD:" AND"
    T.SEL := " SCB.DEPT.CODE NE 2800 AND"
    T.SEL := " DEPARTMENT.CODE EQ ":DEPT.CODE
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,I> = "@ID"
        ENQ<3,I> = "EQ"
        ENQ<4,I> = "DUMMY"
    END
    RETURN
*==============================================================
END
