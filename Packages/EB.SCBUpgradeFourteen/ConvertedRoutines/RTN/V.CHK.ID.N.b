* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDI1ODYzOTE4OTM6dXNlcjotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:59:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFourteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFourteen
*DONE
    SUBROUTINE V.CHK.ID.N

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.INST
*--------------------------------------
    FN.AC = 'FBNK.ACCOUNT'  ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.TMP = 'F.SCB.LOANS.INST'  ; F.TMP = ''
    CALL OPF(FN.TMP,F.TMP)

    SER.NO = ''
*--------------------------------------
    IF V$FUNCTION = 'I' THEN
        IF COMI THEN          ;* ID.NEW would not have been set now
            TEXT = COMI ; CALL REM
            SER.TEMP = FIELD(COMI,".",2)

            TEXT = SER.TEMP ; CALL REM
            IF SER.TEMP EQ '' THEN
                CALL F.READ(FN.AC,COMI,R.AC,F.AC,ER.AC)
                IF ER.AC NE '' THEN
                    E = "INPUT MISSING ACCOUNT NO."
*Line [ 45 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                    CALL EB.SCBUpgradeFourteen.ERR
                    MESSAGE = 'REPEAT'
                    V$ERROR = 1
                END
                ACCT.ID = COMI
                T.SEL = "SELECT F.SCB.LOANS.INST WITH ACCOUNT.NO EQ ":ACCT.ID:" BY @ID"
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF SELECTED THEN
                    ID.TMP  = KEY.LIST<SELECTED>
                    SER.NO  = FIELD(ID.TMP,'.',2) + 1
                END ELSE
                    SER.NO  = 1
                END
                SER.NO = FMT(SER.NO,'R%5')
                ID.NEW = ACCT.ID : "." : SER.NO
                COMI   = ACCT.ID : "." : SER.NO
                SER.NO = ''
            END

            IF SER.TEMP NE '' THEN
                ID.TEMP = COMI
                CALL F.READ(FN.TMP,ID.TEMP,R.TMPP,F.TMP,ER.TMP)
                IF ER.TMP THEN
                    ACCT.ID = COMI
                    CALL F.READ(FN.AC,ACCT.ID,R.AC,F.AC,ER.AC)
                    IF ER.AC NE '' THEN
                        E = "INPUT MISSING ACCOUNT NO."
*Line [ 73 ] Adding EB.SCBUpgradeFourteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                        CALL EB.SCBUpgradeFourteen.ERR
                        MESSAGE = 'REPEAT'
                        V$ERROR = 1
                    END ELSE
                        T.SEL = "SELECT F.SCB.LOANS.INST WITH ACCOUNT.NO EQ ":ACCT.ID:" BY @ID"
                        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                        IF SELECTED THEN
                            ID.TMP  = KEY.LIST<SELECTED>
                            SER.NO  = FIELD(ID.TMP,'.',2) + 1
                        END ELSE
                            SER.NO  = 1
                        END
                        SER.NO = FMT(SER.NO,'R%5')
                        ID.NEW = ACCT.ID : "." : SER.NO
                        COMI   = ACCT.ID : "." : SER.NO
                        SER.NO = ''
                    END
                END
            END
        END
    END
*-------------------------------------------------------------
    RETURN
END
