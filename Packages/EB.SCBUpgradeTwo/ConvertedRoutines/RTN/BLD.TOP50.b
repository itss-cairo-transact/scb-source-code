* @ValidationCode : MjoyMDAwMDQ1NTAwOkNwMTI1MjoxNjQwNzI0MjUwOTA0OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*------------------------CREATED BY REHAM* -------20180910-------*
*----------------------------------------------------------------------------
* <Rating>85</Rating>
*-----------------------------------------------------------------------------

SUBROUTINE BLD.TOP50(ENQ)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] HASHING "$INCLUDE I_F.SCB.TOP50" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TOP50

    COMP = ID.COMPANY

    FN.TP  = "F.SCB.TOP50" ; F.TP   = "" ; R.TP  = ""
    CALL OPF(FN.TP,F.TP)

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC  = "" ; R.ACC  = ""
    CALL OPF(FN.ACC,F.ACC)
    NN = 1
    Y.SEL = "SSELECT F.SCB.TOP50 WITH GROUP.ID EQ '' BY-DSND TOTAL.AMT "
    CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM

    IF SELECTED THEN
        FOR I = 1 TO 50
            ENQ<2,NN> = '@ID'
            ENQ<3,NN> = 'EQ'
            ENQ<4,NN> =  KEY.LIST<I>
            NN ++
        NEXT I
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUM'
    END
**********************
RETURN
END
