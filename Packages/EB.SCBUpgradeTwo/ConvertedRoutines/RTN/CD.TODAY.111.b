* @ValidationCode : Mjo0NjQxMDM0MzY6Q3AxMjUyOjE2NDA3MjQyNTM1ODU6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*---------------------------------NI7OOOOOOOOOOOOOOOO---------------------
SUBROUTINE CD.TODAY.111(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] HASHING "$INCLUDE I_F.SCB.LG.COMM.CORS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.LG.COMM.CORS
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""
    DATE.TO = '...':TODAY
    CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>
    T.SEL  = "SELECT F.SCB.LG.COMM.CORS WITH @ID LIKE ": DATE.TO
*********    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**************************************************
    IF SELECTED  THEN
        FOR KK = 1 TO SELECTED

            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'

        ENQ.ERROR = "NO RESULT FOUND"
    END
RETURN
END
