* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CLOSED.ACCT.TXT
*    PROGRAM  CLOSED.ACCT.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*****
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "CLOSED.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/":' ':"CLOSED.TXT"
        HUSH OFF
    END
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "CLOSED.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CLOSED.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create CLOSED.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END

*****

    FN.AC  = 'FBNK.ACCOUNT.CLOSED' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

*---------------------------
    T.SEL = "SELECT FBNK.ACCOUNT.CLOSED WITH ACCT.CLOSE.DATE GE 20100101 AND ACCT.CLOSE.DATE LE 20100930 BY @ID "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)
            ACCT.ID    = KEY.LIST<I>
            CLOSE.DATE = R.AC<1>
            IF ACCT.ID[1,1] EQ 0 THEN
                CUS.ID = ACCT.ID[2,7]
            END ELSE
                CUS.ID = ACCT.ID[1,8]
            END
            CALL F.READ(FN.CUS,CUS.ID, R.CUS, F.CUS, ETEXT.CUS)
            CUS.NAME = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            CUS.D    = R.CUS<EB.CUS.COMPANY.BOOK>
            CUS.BR   = CUS.D[2]
            CUS.DEPT = TRIM(CUS.BR, "0" , "L")
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,CUS.DEPT,BRN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,CUS.DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            BRN.NAME = FIELD(BRN.NAME,'.',2)

            BB.DATA  = BRN.NAME:'|'
            BB.DATA := ACCT.ID:'|'
            BB.DATA := CUS.NAME:'|'
            BB.DATA := CLOSE.DATE

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
