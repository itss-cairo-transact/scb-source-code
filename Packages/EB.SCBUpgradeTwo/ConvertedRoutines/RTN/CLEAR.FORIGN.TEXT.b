* @ValidationCode : MjotMjA5MDUyMDQwNDpDcDEyNTI6MTY0NTEyNTU4NTM3ODpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 11:19:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-150</Rating>
*-----------------------------------------------------------------------------
** PROGRAM CLEAR.FORIGN.TEXT

SUBROUTINE CLEAR.FORIGN.TEXT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BL.BATCH
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.UPLOAD.FLG
*-----------------------------------------------------
    FN.FLG = 'F.SCB.UPLOAD.FLG' ; F.FLG = '' R.FLG = ''
    CALL OPF(FN.FLG,F.FLG)
    FLG.ID = ID.COMPANY : "-" : TODAY
    CALL F.READ(FN.FLG,FLG.ID,R.FLG,F.FLG,READ.ERR)

*    IF READ.ERR NE '' THEN
    DD.DATA = ''
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*    END ELSE
*        TEXT = "��� �� ������� �� ���" ;CALL REM
*    END
RETURN
*----------------------------------------------------
INITIALISE:
*------------
    FN.IN    = 'FCY.CLEARING'
    F.IN     = 0
    DD.REC   = ""
RETURN
*----------------------------------------------------
BUILD.RECORD:
*------------
    COMP       = C$ID.COMPANY
    USR.DEP    = "1700":COMP[8,2]
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
    F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
    FN.F.ITSS.SCB.BANK.BRANCH = ''
    CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
    CALL F.READ(F.ITSS.SCB.BANK.BRANCH,USR.DEP,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
    LOCT=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.LOCATION>

    DAL.TODAY = TODAY
    IF LOCT EQ '1'  THEN
        CALL CDT('',DAL.TODAY,"+1W")
    END ELSE
        CALL CDT('',DAL.TODAY,"+2W")
    END

    DAL.YEAR        = DAL.TODAY[1,4]
    DAL.MONTH       = DAL.TODAY[5,2]
    DAL.DAY         = DAL.TODAY[7,2]
    DAL.DAY.1       = DAL.TODAY[1,4]:DAL.TODAY[5,2]:DAL.TODAY[7,2]

    DAL.DAY.1.YEAR  = DAL.DAY.1[1,4]
    DAL.DAY.1.MONTH = DAL.DAY.1[5,2]
    DAL.DAY.1.DAY   = DAL.DAY.1[7,2]

    COMP        = C$ID.COMPANY
    COM.CODEE   = COMP[8,2]
    COM.CODE    = TRIM(COM.CODEE, "0", "L")

    RECIVE.DATE = TODAY
    DAY.NOM     = OCONV(DATE(),"DW")
    COMP        = C$ID.COMPANY
    USR.DEP     = "1700":COMP[8,2]
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
    F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
    FN.F.ITSS.SCB.BANK.BRANCH = ''
    CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
    CALL F.READ(F.ITSS.SCB.BANK.BRANCH,USR.DEP,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
    LOCT=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.LOCATION>

    IF LOCT EQ '1'  THEN
        IF (DAY.NOM EQ 7 OR DAY.NOM EQ 2 ) THEN
            CALL CDT('',RECIVE.DATE,"+2W")
        END
        IF (DAY.NOM EQ 1 OR DAY.NOM EQ 3 OR DAY.NOM EQ 4  ) THEN
            CALL CDT('',RECIVE.DATE,"+1W")
        END
    END ELSE
        IF (DAY.NOM EQ 7 OR DAY.NOM EQ 2 OR DAY.NOM EQ 3 ) THEN
            CALL CDT('',RECIVE.DATE,"+2W")
        END
        IF ( DAY.NOM EQ 1 OR DAY.NOM EQ 4 )  THEN
            CALL CDT('',RECIVE.DATE,"+3W")
        END
    END

    OPENSEQ "FCY.CLEARING" , "aib_out.":COM.CODE TO DD ELSE
        CREATE DD THEN
            PRINT 'FILE CREATED IN /life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING'
        END
        ELSE
            STOP 'Cannot create File IN /life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING'
        END
    END

    EOF     = ''
    FN.BR   = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    T.DAY.1 = DAL.DAY.1
    T.DAY.2 = TODAY

    FN.BR  = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF( FN.BR,F.BR)
    TEXT = "DD = ":RECIVE.DATE ; CALL REM
    T.SEL  = "SSELECT FBNK.BILL.REGISTER WITH CURRENCY NE EGP"
    T.SEL := " AND ( BILL.CHQ.STA NE 7 AND BILL.CHQ.STA NE 5 )"
    T.SEL := " AND  CUST.ACCT LIKE 99400500..."
    T.SEL := " AND MATURITY.EXT EQ ":RECIVE.DATE
    T.SEL := " AND CO.CODE EQ ":COMP

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.BR,KEY.LIST<I>, R.BR, F.BR, ETEXT)
            DD.DATA  = "01|"
            T.DAY.1  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
            DD.YEAR  = T.DAY.1[1,4]
            DD.MONTH = T.DAY.1[5,2]
            DD.DATE  = T.DAY.1[7,2]
            DDDD     = DD.DATE:"/":DD.MONTH:"/":DD.YEAR

            DD.DATA := DDDD:"|"
            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BANK>:"|"
            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BANK.BR>[3,4]:"|"

            T.DAY.2 = TODAY
            CALL CDT('',T.DAY.2,"+2W")

            SAM.YEAR  = T.DAY.2[1,4]
            SAM.MONTH = T.DAY.2[5,2]
            SAM.DATE = T.DAY.2[7,2]
            SAM      = SAM.DATE:"/":SAM.MONTH:"/":SAM.YEAR
            DD.DATA := SAM:'|'
*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.BR<EB.BILL.REG.CURRENCY>,CUR.COD)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,R.BR<EB.BILL.REG.CURRENCY>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR.COD=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
            DD.DATA := CUR.COD:"|"
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,R.BR<EB.BILL.REG.CURRENCY>,CUR.RATT)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,R.BR<EB.BILL.REG.CURRENCY>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR.RATT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            CUR.RAT  = CUR.RATT<1,1>
            DD.DATA := CUR.RAT:"|"

            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.NO>:"|"
            MAT.DATE = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MAT.DATE>

            MAT.YEAR  = MAT.DATE[1,4]
            MAT.MONTH = MAT.DATE[5,2]
            MAT.DATE  = MAT.DATE[7,2]

            MATT = MAT.DATE:"/":MAT.MONTH:"/":MAT.YEAR
            DD.DATA := MATT :"|"
            DD.DATA := R.BR<EB.BILL.REG.AMOUNT>:"|"
            EQL.AMT  = (R.BR<EB.BILL.REG.AMOUNT> * CUR.RAT )
            CALL EB.ROUND.AMOUNT ('EGP',EQL.AMT,'',"2")
            DD.DATA := EQL.AMT:"|"
            DD.DATA := "0.00|0.00||001|1|":"|"
            DD.DATA := R.BR<EB.BILL.REG.DATE.TIME>[7,4]:"|"
            DD.DATA := "1|0017":'|'
            DD.DATA := "00":COM.CODEE:"|"
            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>[1,8]:"|"
            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>[11,4]:"|"
*Line [ 214 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>,NME)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,BRLR.LIQ.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            NME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>R.BR<EB.BILL.REG.LOCAL.REF>
            DD.DATA := KEY.LIST<I>:"|"
            DD.DATA := "||"
            F.PATH  = FN.IN

            SEEK DD,0, 2 THEN
                WRITESEQ DD.DATA TO DD ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END

            BR.ID = KEY.LIST<I>
            R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  = '5'
            R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>   = TODAY

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*    PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
            CALL F.WRITE(FN.BR,BR.ID,R.BR)
            CALL JOURNAL.UPDATE(BR.ID)

        NEXT I

        FN.FLG = 'F.SCB.UPLOAD.FLG' ; F.FLG = '' R.FLG = ''
        CALL OPF(FN.FLG,F.FLG)
        FLG.ID = ID.COMPANY : "-" : TODAY
        CALL F.READ(FN.FLG,FLG.ID,R.FLG,F.FLG,READ.ERR)

        R.FLG<SCB.FLG.FLAG1> = "1"

*   WRITE  R.FLG TO F.FLG , FLG.ID ON ERROR
*       PRINT "CAN NOT WRITE RECORD TO" :FN.FLG
*   END

        CALL F.WRITE(FN.FLG,FLG.ID,R.FLG)
        CALL JOURNAL.UPDATE(FLG.ID)

        TEXT = "FINISHED " : SELECTED : " CHQs." ; CALL REM
        TEXT = "ALL TASKS FINISHED" ; CALL REM
    END
RETURN
END
