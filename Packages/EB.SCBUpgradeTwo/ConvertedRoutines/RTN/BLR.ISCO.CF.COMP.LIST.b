* @ValidationCode : MjotMzg1ODM2NTQ4OkNwMTI1MjoxNjQwNzI0MjUxMzc5OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*********************************************
*************WAGDY.MOUNIR****10*09*2009******
*********************************************

SUBROUTINE BLR.ISCO.CF.COMP.LIST(ENQ)
*********************************************
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] HASHING "$INCLUDE I_F.SCB.ISCO.CF" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.ISCO.CF
*********************************************
    COMP = ID.COMPANY
    TAB= 'F.SCB.ISCO.CF'
    XTAB=""
    XCOMP= COMP[2]
****
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    F.ACC    = ""
    R.ACC    = ""
    E11      = ""
****
**** CONVERTING THE XCOMP ****
    IF XCOMP LT 10 THEN
        XCOMP = XCOMP[1]
    END
******************************
    TODAY.DATE = TODAY
    T.MONTH    = TODAY.DATE[5,2]
    T.YEAR     = TODAY.DATE[1,4]

    IF T.MONTH[1,1] EQ 0 THEN
        T.MONTH = T.MONTH[2,1]
    END
    IF T.MONTH EQ 1 THEN
        T.MONTH = 12
        T.YEAR  = T.YEAR - 1
    END
    ELSE
        T.MONTH = T.MONTH - 1
    END
    T.MONTH =  FMT(T.MONTH, "R%2")
    T.DATE     = T.YEAR:T.MONTH

*********************************************
    CALL OPF(TAB,XTAB)
    T.SEL = "SELECT F.SCB.ISCO.CF WITH @ID LIKE ...":T.DATE:"... AND DATA.PROV.BRANCH.ID EQ ":XCOMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    Z=1
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(TAB,KEY.LIST<I>,R.ACC, F.ACC ,E11)
**********************
            ENQ<2,Z> = "@ID"
            ENQ<3,Z> = "EQ"
            ENQ<4,Z> = KEY.LIST<I>
**********************
            Z=Z+1
        NEXT I
    END ELSE
        ENQ.ERROR = "... NO RECORDS FOUND ..."
    END
RETURN
END
***********************************************************
