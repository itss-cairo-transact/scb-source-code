* @ValidationCode : MjotNzEzMDg5NDk1OkNwMTI1MjoxNjQwNzI0MjUyODkzOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*****NESSREEN AHMED 07/02/2011***************
SUBROUTINE C.SELL.RATE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] HASHING "$INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM" - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM      ;*RE.BCP.
*Line [ 29 ] HASHING "$INCLUDE T24.BP I_USER.ENV.COMMON" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE T24.BP I_USER.ENV.COMMON          ;*EB.COM.
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

    COMP = ID.COMPANY

    CUR.COD  = O.DATA

    IF CUR.COD NE 'EGP' THEN

        FN.CUR  = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''
        CALL OPF(FN.CUR,F.CUR)

        CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
        CASH.C = '10'
*Line [ 43 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
        CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
        O.DATA      = CUR.RATE
    END ELSE
        O.DATA     = '1'
    END

RETURN
