* @ValidationCode : MjotMTgyMDEyNDExOkNwMTI1MjoxNjQwNzI0MjUzODM5OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE CNV.ACC.LOCK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] HASHING "$INCLUDE I_AC.LOCAL.REFS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS

    XX = O.DATA

    FN.ACC = "FBNK.ACCOUNT"
    F.ACC  = ''
    R.ACC  = ''
    CALL OPF(FN.ACC,F.ACC)
    LCMT  = ''
    ACLCK = ''
    I     = ''
****************************************************************
    CALL F.READ(FN.ACC,XX,R.ACC,F.ACC,E1)
    ACLCK = R.ACC<AC.LOCKED.AMOUNT>
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    I = DCOUNT(ACLCK,@VM)
    LCMT = R.ACC<AC.LOCKED.AMOUNT,I>
*****************************************************************
    O.DATA = LCMT

RETURN
END
