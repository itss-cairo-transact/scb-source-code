* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CALC.INT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

  *  FN.LOANS = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LOANS = '' ; R.LOANS = ''
 *   CALL OPF( FN.LOANS,F.LOANS)
*    CALL F.READ( FN.LOANS,FLAT.LOAN, R.LOANS, F.LOANS, ETEXT)
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
* CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,O.DATA,RATE)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,O.DATA,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
RATE=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.INTEREST.RATE>

    EXP.DATE     = R.RECORD<LD.FIN.MAT.DATE>
    ISSUE.DATE   = R.RECORD<LD.VALUE.DATE>
    AMMOUNT      = R.RECORD<LD.AMOUNT>
   * INTT         = R.RECORD<LD.INTEREST.RATE>
    DAY          = 'C'
    CALL CDD("",ISSUE.DATE,EXP.DATE , DAY)
*    TEXT = 'INTT' : INTT ; CALL REM
    INTEREST = (AMMOUNT * RATE ) * (DAY / 36000 )

    CALL EB.ROUND.AMOUNT ('EGP',INTEREST,'',"")

    O.DATA = INTEREST

    RETURN
END
