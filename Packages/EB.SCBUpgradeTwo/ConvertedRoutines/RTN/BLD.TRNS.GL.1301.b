* @ValidationCode : MjoxNjYwOTM1NDQ1OkNwMTI1MjoxNjQwNzI0MjUwOTIzOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE BLD.TRNS.GL.1301(ENQ)
*���� ������ ����� ���� ����� ��� 1301 6301
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] HASHING "$INCLUDE I_F.SCB.TRANS.TODAY" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    YYMM = TODAY[1,6]
*  FIRST := 01
    T.SEL = "SELECT F.SCB.TRANS.TODAY WITH BOOKING.DATE LIKE ":YYMM:"... AND PRODUCT.CATEGORY IN ( 11019 11218 11034 11219 11022 11216 11212 11035 11023 16186 16187 11052 11064 16426 3050 16205 11054 11058 ) AND AMOUNT.LCY NE 0 AND AMOUNT.LCY NE '' AND COMAPNY.CO EQ ":COMP:" BY ACCOUNT.OFFICER BY PRODUCT.CATEGORY BY CURRENCY"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*=================================================================
    IF SELECTED >= 1 THEN
        FOR KK = 1 TO SELECTED
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,KK> = '@ID'
        ENQ<3,KK> = 'EQ'
        ENQ<4,KK> = 'DUMMY'
    END
RETURN
END
