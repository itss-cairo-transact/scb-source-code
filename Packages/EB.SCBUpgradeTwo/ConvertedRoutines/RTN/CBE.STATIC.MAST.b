* @ValidationCode : MjotNDE5Nzk0NDcxOkNwMTI1MjoxNjQwNzI0MjUzNTAxOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1362</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

*  SUBROUTINE SCB.TRANS.TODAY
SUBROUTINE CBE.STATIC.MAST
******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 35 ] "HASHING $INCLUDE I_F.SCB.PARMS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.PARMS

*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE          ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL        ;* Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION      ;* Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

*       IF 21000 > ID.NEW[1,5]  OR ID.NEW[1,5] > 21300 THEN
*         E = 'This.Is.Wrong.Category' ; CALL ERR ; MESSAGE = 'REPEAT'
*       END
*         CURR = ''
*        CALL DBR('CURRENCY':@FM:@ID,ID.NEW[6,3],CURR)
*         IF ETEXT THEN E = 'This.Is.Wrong.CURRENCY' ; CALL ERR ; MESSAGE = 'REPEAT'
*         MM = ''
*         MM = TODAY
*         IF NOT(ID.NEW[9,11]) THEN ID.NEW[9,11] = MM
*_________________________________________________________________________________________
    E = ''
    V$ERROR = ''
    IF E THEN CALL ERR ; V$ERROR = 1

RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

*  FOR I = 1 TO DCOUNT(R.NEW(SCB.PAR.MIN.AMT),VM)
*         J = I - 1
*            IF R.NEW(SCB.PARMS.MIN.AMT)<1,I> < R.NEW(SCB.PARMS.MIN.AMT)<1,J> THEN
*             ETEXT = 'Enter.Smaller.Amount'
*            END
*
*        NEXT I

    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL

*     FOR I = 2 TO DCOUNT(R.NEW(SCB.PAR.MIN.AMT),VM)
*         J = I - 1
*            IF R.NEW(SCB.PARMS.MIN.AMT)<1,I> < R.NEW(SCB.PARMS.MIN.AMT)<1,J> THEN
*             ETEXT = 'Enter.Smaller.Amount'
*            END
*
*        NEXT I
*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************

CHECK.DELETE:


RETURN

*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************
DELIVERY.PREVIEW:

RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:


RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
        CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

RETURN

*************************************************************************

INITIALISE:

RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = ""
    ID.CONCATFILE = ""
****  KEY  = CUSTOMER.CODE   (REF OF CLIENT) +  BRANCH OF CLIENT
    ID.F  = "" ; ID.N  = "15" ; ID.T  = ""
*ID.T< 4> = 'R##### ### DDDDDDDD'

    Z = 0
    Z+=1 ; F(Z)= "CBE.CUSTOMER.CODE"        ; N(Z) = "10" ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.BR"                   ; N(Z) = "4"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.NEW.SECTOR"           ; N(Z) = "4"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.INDUSTRY"             ; N(Z) = "4"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.SECTOR"               ; N(Z) = "4"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.LEGAL.FORM"           ; N(Z) = "4"  ; T(Z)= ""
******�������� �������
    Z+=1 ; F(Z)= "CBE.CUR.AC.LE"            ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CUR.AC.LE.DR"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CUR.AC.LE.NO.OF.AC"   ; N(Z) = "3"  ; T(Z)= ""
******����� ����  ��� � ����� � ����� �����
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.LE.1Y"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.LE.2Y"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.LE.3Y"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.LE.MOR3"    ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.LE.NO.OF.AC" ; N(Z) = "3"  ; T(Z)= ""
******�������� ������ � ������ � �����
    Z+=1 ; F(Z)= "CBE.CER.AC.LE.3Y"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CER.AC.LE.5Y"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CER.AC.LE.GOLD"       ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CER.AC.LE.NO.OF.CER"  ; N(Z) = "3"  ; T(Z)= ""
******������ �������
    Z+=1 ; F(Z)= "CBE.SAV.AC.LE"            ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.SAV.AC.LE.DR"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.SAV.AC.LE.NO.OF.AC"   ; N(Z) = "3"  ; T(Z)= ""
******�������  ���� ���� � ������ ������� ������
    Z+=1 ; F(Z)= "CBE.BLOCK.AC.LE"          ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.BLOCK.AC.LE.NO.OF.AC" ; N(Z) = "3"  ; T(Z)= ""
*****���� ���� ��������
    Z+=1 ; F(Z)= "CBE.MARG.LC.LE"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.MARG.LC.LE.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
*****���� ���� ������ ������
    Z+=1 ; F(Z)= "CBE.MARG.LG.LE"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.MARG.LG.LE.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
*****��������� ����������
    Z+=1 ; F(Z)= "CBE.FACLTY.LE"            ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.FACLTY.LE.CR"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.FACLTY.LE.NO.OF.AC"   ; N(Z) = "3"  ; T(Z)= ""
******������   �������  � �������� � ����� �����
    Z+=1 ; F(Z)= "CBE.LOANS.LE.S"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.LE.S.CR"        ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.LE.S.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.LOANS.LE.M"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.LE.M.CR"        ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.LE.M.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.LOANS.LE.L"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.LE.L.CR"        ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.LE.L.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
******������� �������� ��������
    Z+=1 ; F(Z)= "CBE.COMRCL.PAPER.LE"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.COMRCL.PAPER.LE.NO"   ; N(Z) = "3"  ; T(Z)= ""

***************************************DATA ENTRY
    Z+=1 ; F(Z)= "CBE.CUR.AC.EQ"            ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CUR.AC.EQ.DR"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CUR.AC.EQ.NO.OF.AC"   ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.DEPOST.AC.EQ.1Y"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.EQ.2Y"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.EQ.3Y"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.EQ.MOR3"    ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.DEPOST.AC.EQ.NO.OF.AC" ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.CER.AC.EQ.3Y"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CER.AC.EQ.5Y"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CER.AC.EQ.GOLD"       ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.CER.AC.EQ.NO.OF.CER"  ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.SAV.AC.EQ"            ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.SAV.AC.EQ.DR"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.SAV.AC.EQ.NO.OF.AC"   ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.BLOCK.AC.EQ"          ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.BLOCK.AC.EQ.NO.OF.AC" ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.MARG.LC.EQ"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.MARG.LC.EQ.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.MARG.LG.EQ"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.MARG.LG.EQ.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.FACLTY.EQ"            ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.FACLTY.EQ.CR"         ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.FACLTY.EQ.NO.OF.AC"   ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.LOANS.EQ.S"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.S.CR"        ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.S.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.M"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.M.CR"        ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.M.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.L"           ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.L.CR"        ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.LOANS.EQ.L.NO.OF.AC"  ; N(Z) = "3"  ; T(Z)= ""

    Z+=1 ; F(Z)= "CBE.COMRCL.PAPER.EQ"      ; N(Z) = "13" ; T(Z)= "AMT"
    Z+=1 ; F(Z)= "CBE.COMRCL.PAPER.EQ.NO"   ; N(Z) = "3"  ; T(Z)= ""
    Z+=1 ; F(Z)= "CBE.NEW.INDUSTRY"         ; N(Z) = "4"  ; T(Z)= ""
    Z+=1 ; F(Z)= "RESERVED10"            ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED9"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED8"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED7"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9
RETURN

*************************************************************************

END
