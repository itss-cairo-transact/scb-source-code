* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BULD.DPST.BANK(ENQ)
*    PROGRAM BULD.DPST.BANK

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW


    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    COMP = ID.COMPANY
    DAT.ID = COMP
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,TD2)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD2=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD1=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>


    AMT.POS1 = 0 ; KK1 = 0 ; KK2 = 0
    FN.POS = 'F.SCB.CUS.POS.LW'
    F.POS  = ''
    CALL OPF(FN.POS,F.POS)
*=========== CHECK USD AMOUNT =========================
    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
    FROM.VAL = 2000000
*Line [ 56 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE 'USD' IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    LOCAL.USD.AMT = FROM.VAL * R.CUR<RE.BCP.RATE,POS>
*======================================================
*    T.SEL = "SELECT F.SCB.CUS.POS.LW WITH (CATEGORY EQ 5001 OR CATEGORY EQ 21076) AND DEAL.CCY NE EGP AND LCY.AMOUNT NE 0"
*    T.SEL = "SELECT F.SCB.CUS.POS.LW WITH CUSTOMER IN ( 99422700 99423500 99421000 99421800 ) AND DEAL.CCY NE EGP AND "
*    T.SEL = "SELECT F.SCB.CUS.POS.LW WITH CUSTOMER EQ ( 99455300 ) AND DEAL.CCY NE EGP AND "
*=======================================================
    T.SEL = "SELECT F.SCB.CUS.POS.LW WITH DEAL.CCY NE EGP "
    T.SEL := "AND SYS.DATE EQ ":TD2:" AND LCY.AMOUNT NE 0"
    T.SEL := " AND ((CATEGORY EQ 5001 AND LCY.AMOUNT LT 0 )"
    T.SEL := " OR (CATEGORY EQ 21076 AND MATURITY.DATE GT ":TD1:" AND VALUE.DATE LE ":TD1:"))"
    T.SEL := " BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*=======================================================
    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
            AMT.POS = 0 ; SCB.ORG.CUR.RATE = 0
            SCB.ORG.CUR = '' ;
            CALL F.READ(FN.POS,KEY.LIST<KK>,R.POS,F.POS,ERR)
            CUS.ID = R.POS<CUPOS.CUSTOMER>
            IF KK = 1 THEN CCC.NO = CUS.ID ; ST.NO = KK
            IF CCC.NO # CUS.ID THEN
                IF AMT.POS1 GE LOCAL.USD.AMT THEN
                    FOR KK2 = ST.NO TO EN.NO
                        KK1 ++
                        ENQ<2,KK1> = '@ID'
                        ENQ<3,KK1> = 'EQ'
                        ENQ<4,KK1> = KEY.LIST<KK2>
*PRINT KEY.LIST<KK2>
                    NEXT KK2
*PRINT AMT.POS1:"====="CCC.NO:LOCAL.USD.AMT:"===":KEY.LIST<KK2>
                END
                AMT.POS1 = 0 ; ST.NO = KK
            END
            AMT.POS    = R.POS<CUPOS.DEAL.AMOUNT>
            SCB.ORG.CUR = R.POS<CUPOS.DEAL.CCY>
            IF AMT.POS LT 0 THEN AMT.POS = AMT.POS * -1
*Line [ 94 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE SCB.ORG.CUR IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            SCB.ORG.CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
            LOCAL.FORIGN.AMT = AMT.POS * SCB.ORG.CUR.RATE
            AMT.POS1 = AMT.POS1 + LOCAL.FORIGN.AMT
            CCC.NO = CUS.ID
            EN.NO = KK
        NEXT KK
        IF KK = SELECTED THEN

            IF AMT.POS1 GE LOCAL.USD.AMT THEN
                FOR KK2 = ST.NO TO EN.NO
                    KK1 ++
                    ENQ<2,KK1> = '@ID'
                    ENQ<3,KK1> = 'EQ'
                    ENQ<4,KK1> = KEY.LIST<KK2>
                    ST.NO = KK
*PRINT "LAST":KEY.LIST<KK2>
                NEXT KK2
*PRINT AMT.POS1:"=LST="CCC.NO:LOCAL.USD.AMT:"===":KEY.LIST<KK2>
                AMT.POS1 = 0
            END
        END
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
    RETURN
END
