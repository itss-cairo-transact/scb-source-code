* @ValidationCode : Mjo3NzUxNjk2MjU6Q3AxMjUyOjE2NDA3MjI5NjczNDA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:22:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE CHQ.FILL.SERIAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHEQ.APPL

    FN.CHQ  = "F.SCB.CHEQ.APPL"; F.CHQ    = '' ; R.CHQ = ''
    R.LINE  = ''               ; R.CHQ    = '' ; T.SEL ='';
    KEY.LIST=''                ; SELECTED =0

    WS.DATE.YEAR  = TODAY[1,4]
    WS.DATE.MONTH = TODAY[5,2]
    WS.DATE.DAY   = TODAY[7,2]

    WS.FILE.DATE = WS.DATE.DAY:'-':WS.DATE.MONTH:'-':WS.DATE.YEAR
    WS.FILE.NAME = 'ConFile_':WS.FILE.DATE:'.txt'

    DIR.NAME  = "CHEQ"
    TEXT.FILE = WS.FILE.NAME
    VAR.FILE  = ''

    OPENSEQ DIR.NAME,TEXT.FILE TO VAR.FILE ELSE
        TEXT = "ERROR OPEN FILE ":TEXT.FILE ; CALL REM
        RETURN
    END

    CALL OPF(FN.CHQ,F.CHQ)

    EOF = ''

    LOOP WHILE NOT(EOF)
        READSEQ R.LINE  FROM VAR.FILE THEN

            ORDER.ID  =FIELD(R.LINE,',',1)
            STRT.SER  =FIELD(R.LINE,',',2)
            END.SER   =FIELD(R.LINE,',',3)

            T.SEL = "SELECT F.SCB.CHEQ.APPL WITH CHQ.NO.START EQ '' AND @ID EQ ": ORDER.ID
            CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD1)
            CALL F.READ( FN.CHQ,KEY.LIST<1>, R.CHQ, F.CHQ, ETEXT)
            IF SELECTED EQ 1 THEN
                R.CHQ<CHQA.CHQ.NO.START> = STRT.SER
                CALL F.WRITE (FN.CHQ,ORDER.ID,R.CHQ)
                CALL JOURNAL.UPDATE(ORDER.ID)

            END ELSE
                TEXT = 'THIS BATCH RUN BEFORE : ':ORDER.ID ; CALL REM
            END
        END ELSE
            EOF = 'END OF FILE'
        END
    REPEAT
    CLOSESEQ VAR.FILE
    TEXT = "�� ����� ����� �����";CALL REM
RETURN
**************************************************************
