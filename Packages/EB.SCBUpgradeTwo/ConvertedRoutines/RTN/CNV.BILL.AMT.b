* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE CNV.BILL.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER

    XX = O.DATA
    AC.CODE  = XX
    FN.BILL  = "FBNK.BILL.REGISTER" ; F.BILL   = '' ; R.BILL   = '' ; ERR = ''
    BILL.AMT = 0
    CALL OPF(FN.BILL,F.BILL)
    B.SEL  = "SELECT ":FN.BILL:" WITH CONT.ACCT EQ ":AC.CODE
    B.SEL := " WITHOUT BILL.CHQ.STA IN ( 7 8 13 14 15 )"
    CALL EB.READLIST(B.SEL,KEY.LIST, "", SELECTED, ERR)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BILL,KEY.LIST<I>,R.BILL,F.BILL,ER)
            AMT = R.BILL<EB.BILL.REG.AMOUNT>
            BILL.AMT += AMT
        NEXT I
    END
    O.DATA = BILL.AMT
    RETURN
