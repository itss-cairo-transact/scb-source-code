* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>485</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CBE.FAC.EGP.TXT
*    PROGRAM CBE.FAC.EGP.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.FACILITY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*------------------------------------------------------
    TD1 = TODAY
    TD2 = TD1[1,6]
    TD3 = TD2:"01"
    CALL CDT ('',TD3,'-1C')
    TD4 = TD3[1,4]
    TD5 = TD3[5,2]
    TD  = TD5 : TD4
*---------------------------------------------------------------------------------
    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)
    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    Y.SEL = "SELECT F.COMPANY BY @ID"
    CALL EB.READLIST(Y.SEL,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            COMP = KEY.LIST1<I>

            IF COMP EQ "EG0010001" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.cairo" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.cairo"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.cairo" TO BB ELSE
                    CREATE BB THEN
                        PRINT 'FILE scb.egp.cairo CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.abor File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0101'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010006" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.abor" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.abor"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.abor" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.abor CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.abor File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0213'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010013" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.grdn" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.grdn"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.grdn" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.grdn CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.grdn File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0178'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010021" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.burg" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.burg"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.burg" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.burg CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.burg File IN &SAVEDLISTS&'
                    END
                END
                BRN = '1053'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010023" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.amry" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.amry"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.amry" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.amry CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.amry File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0252'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010032" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.mansor" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.mansor"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.mansor" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.mansor CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.mansor File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0506'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010070" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.ramadan" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.ramadan"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.ramadan" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.ramadan CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.ramadan File IN &SAVEDLISTS&'
                    END
                END
                BRN = '4514'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010007" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.orouba" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.orouba"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.orouba" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.orouba CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.orouba File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0196'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010002" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.heliop" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.heliop"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.heliop" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.heliop CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.heliop File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0147'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010005" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.maadi" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.maadi"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.maadi" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.maadi CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.maadi File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0180'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010020" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.alex" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.alex"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.alex" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.alex CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.alex File IN &SAVEDLISTS&'
                    END
                END
                BRN = '1001'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010040" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.ismail" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.ismail"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.ismail" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.ismail CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.ismail File IN &SAVEDLISTS&'
                    END
                END
                BRN = '1601'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010060" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.tanta" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.tanta"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.tanta" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.tanta CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.tanta File IN &SAVEDLISTS&'
                    END
                END
                BRN = '3001'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010004" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.mohand" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.mohand"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.mohand" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.mohand CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.mohand File IN &SAVEDLISTS&'
                    END
                END
                BRN = '5515'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010014" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.toctb" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.toctb"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.toctb" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.toctb CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.toctb File IN &SAVEDLISTS&'
                    END
                END
                BRN = '5531'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010015" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.octb" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.octb"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.octb" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.octb CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.octb File IN &SAVEDLISTS&'
                    END
                END
                BRN = '5557'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010010" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.nasr" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.nasr"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.nasr" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.nasr CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.nasr File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0143'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010009" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.fostat" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.fostat"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.fostat" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.fostat CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.fostat File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0246'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010022" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.smoh" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.smoh"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.smoh" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.smoh CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.smoh File IN &SAVEDLISTS&'
                    END
                END
                BRN = '1029'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010030" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.said" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.said"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.said" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.said CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.said File IN &SAVEDLISTS&'
                    END
                END
                BRN = '1501'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010050" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.suez" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.suez"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.suez" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.suez CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.suez File IN &SAVEDLISTS&'
                    END
                END
                BRN = '1801'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010090" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sadat" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.sadat"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sadat" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.sadat CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.sadat File IN &SAVEDLISTS&'
                    END
                END
                BRN = '3514'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010003" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.giza" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.giza"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.giza" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.giza CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.giza File IN &SAVEDLISTS&'
                    END
                END
                BRN = '5501'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010011" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.dokki" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.dokki"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.dokki" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.dokki CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.dokki File IN &SAVEDLISTS&'
                    END
                END
                BRN = '5524'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010012" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sphinx" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.sphinx"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sphinx" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.sphinxo CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.sphinx File IN &SAVEDLISTS&'
                    END
                END
                BRN = '5537'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010035" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sharm" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.sharm"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sharm" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.sharm CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.sharm File IN &SAVEDLISTS&'
                    END
                END
                BRN = '9952'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010080" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.minya" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.minya"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.minya" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.minya CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.minya File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0508'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010081" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.asyut" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.asyut"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.asyut" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.asyut CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.asyut File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0507'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010031" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.domyat" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.domyat"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.domyat" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.domyat CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.domyat File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0117'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010051" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sokhna" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.sokhna"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sokhna" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.sokhna CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.sokhna File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0107'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010016" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.shobra" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.shobra"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.shobra" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.shobra CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.shobra File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0102'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010017" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.elmanial" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.elmanial"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.elmanial" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.elmanial CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.elmanial File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0108'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010019" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.eltagamoa" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.eltagamoa"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.eltagamoa" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.eltagamoa CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.eltagamoa File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0103'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010052" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.prttwfik" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.prttwfik"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.prttwfik" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.prttwfik CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.prttwfik File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0110'
            END
*---------------------------------------------------------------------------------
            IF COMP EQ "EG0010053" THEN
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sokhecon" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.egp.sokhecon"
                    HUSH OFF
                END
                OPENSEQ "&SAVEDLISTS&" , "scb.egp.sokhecon" TO BB ELSE

                    CREATE BB THEN
                        PRINT 'FILE scb.egp.sokhecon CREATED IN &SAVEDLISTS&'
                    END ELSE
                        STOP 'Cannot create scb.egp.sokhecon File IN &SAVEDLISTS&'
                    END
                END
                BRN = '0510'
            END

            GOSUB INIT0
            GOSUB GETPL

        NEXT I
    END

*---------------------------------------------------------------------------------

* GOSUB INIT0
* GOSUB GETPL

*TEXT = '�� ����� �����' ; CALL REM
    PRINT "DONE"
    RETURN
*====================================================
INIT0:
*----
    HORZ.TOT.CR = 0
    HORZ.TOT.US = 0

    TOT.CR.1  = 0
    TOT.CR.2  = 0
    TOT.CR.3  = 0
    TOT.CR.4  = 0
    TOT.CR.5  = 0
    TOT.CR.6  = 0
    TOT.CR.7  = 0
    TOT.CR.8  = 0
    TOT.CR.9  = 0
    TOT.CR.10 = 0
    TOT.CR.11 = 0
    TOT.CR.12 = 0
    TOT.CR.13 = 0

    TOT.US.1  = 0
    TOT.US.2  = 0
    TOT.US.3  = 0
    TOT.US.4  = 0
    TOT.US.5  = 0
    TOT.US.6  = 0
    TOT.US.7  = 0
    TOT.US.8  = 0
    TOT.US.9  = 0
    TOT.US.10 = 0
    TOT.US.11 = 0
    TOT.US.12 = 0
    TOT.US.13 = 0

    RETURN
*----------------------------------------------------------------
**********************************
GETPL:
*--------
    FN.CRDT = 'F.SCB.CBE.FACILITY'
    F.CRDT = ''
    R.CRDT = ''


    CCC.NO = '' ; LEDG.NO = ''

    CALL OPF(FN.CRDT,F.CRDT)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL  = "SELECT F.SCB.CBE.FACILITY WITH @ID UNLIKE ...EGP AND RESERVED10 EQ 'Y' AND CO.CODE EQ ":COMP:" AND (TOT.US.99 NE 0 OR TOT.CR.99 NE 0) BY CBE.NO"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR REC_NO = 1 TO SELECTED
            CALL F.READ(FN.CRDT,KEY.LIST<REC_NO>,R.CRDT,F.CRDT,E3)

            CURR     = FIELD(KEY.LIST<REC_NO>,'.',2)
            CBE.NO = R.CRDT<CBE.FAC.CBE.NO>
            IF  REC_NO = 1 THEN
                GOSUB HEAD.REC
            END
            GOSUB BUILDREC
        NEXT REC_NO
    END

    RETURN

*****************************************************************************
HEAD.REC:
    BB.DATA = ''
    BB.DATA = '1700'
    BB.DATA := TD
    BB.DATA := STR("0",32)

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
**************************************************************************
BUILDREC:
*-------------
*DIM RF.CODE(13)
*DIM ACCEPT.AMT(13)
*DIM USED.AMT(13)
    RF.CODE = ""
    ACCEPT.AMT = 0
    USED.AMT = 0
*************RF CODES**************************
    IF R.CRDT<CBE.FAC.TOT.US.1> OR R.CRDT<CBE.FAC.TOT.CR.1> THEN
        RF.CODE<1,1>    = '1'
        ACCEPT.AMT<1,1> = R.CRDT<CBE.FAC.TOT.CR.1>
        USED.AMT<1,1>   = R.CRDT<CBE.FAC.TOT.US.1>
    END
    IF R.CRDT<CBE.FAC.TOT.US.2> OR R.CRDT<CBE.FAC.TOT.CR.2> THEN
        RF.CODE<1,2>    = '2'
        ACCEPT.AMT<1,2> = R.CRDT<CBE.FAC.TOT.CR.2>
        USED.AMT<1,2>   = R.CRDT<CBE.FAC.TOT.US.2>
    END
    IF R.CRDT<CBE.FAC.TOT.US.3> OR R.CRDT<CBE.FAC.TOT.CR.3> THEN
        RF.CODE<1,3>    = '3'
        ACCEPT.AMT<1,3> = R.CRDT<CBE.FAC.TOT.CR.3>
        USED.AMT<1,3>   = R.CRDT<CBE.FAC.TOT.US.3>
    END
    IF R.CRDT<CBE.FAC.TOT.US.4> OR R.CRDT<CBE.FAC.TOT.CR.4> THEN
        RF.CODE<1,4>    = '4'
        ACCEPT.AMT<1,4> = R.CRDT<CBE.FAC.TOT.CR.4>
        USED.AMT<1,4>   = R.CRDT<CBE.FAC.TOT.US.4>
    END
    IF R.CRDT<CBE.FAC.TOT.US.5> OR R.CRDT<CBE.FAC.TOT.CR.5> THEN
        RF.CODE<1,5>    = '5'
        ACCEPT.AMT<1,5> = R.CRDT<CBE.FAC.TOT.CR.5>
        USED.AMT<1,5>   = R.CRDT<CBE.FAC.TOT.US.5>
    END
    IF R.CRDT<CBE.FAC.TOT.US.6> OR R.CRDT<CBE.FAC.TOT.CR.6> THEN
        RF.CODE<1,6>    = '6'
        ACCEPT.AMT<1,6> = R.CRDT<CBE.FAC.TOT.CR.6>
        USED.AMT<1,6>   = R.CRDT<CBE.FAC.TOT.US.6>
    END
    IF R.CRDT<CBE.FAC.TOT.US.7> OR R.CRDT<CBE.FAC.TOT.CR.7> THEN
        RF.CODE<1,7>    = '7'
        ACCEPT.AMT<1,7> = R.CRDT<CBE.FAC.TOT.CR.7>
        USED.AMT<1,7>   = R.CRDT<CBE.FAC.TOT.US.7>
    END
    IF R.CRDT<CBE.FAC.TOT.US.8> OR R.CRDT<CBE.FAC.TOT.CR.8> THEN
        RF.CODE<1,8>    = '8'
        ACCEPT.AMT<1,8> = R.CRDT<CBE.FAC.TOT.CR.8>
        USED.AMT<1,8>   = R.CRDT<CBE.FAC.TOT.US.8>
    END
    IF R.CRDT<CBE.FAC.TOT.US.9> OR R.CRDT<CBE.FAC.TOT.CR.9> THEN
        RF.CODE<1,9>    = '9'
        ACCEPT.AMT<1,9> = R.CRDT<CBE.FAC.TOT.CR.9>
        USED.AMT<1,9>   = R.CRDT<CBE.FAC.TOT.US.9>
    END
    IF R.CRDT<CBE.FAC.TOT.US.10> OR R.CRDT<CBE.FAC.TOT.CR.10> THEN

        RF.CODE<1,10>    = '10'
        ACCEPT.AMT<1,10> = R.CRDT<CBE.FAC.TOT.CR.10>
        USED.AMT<1,10>   = R.CRDT<CBE.FAC.TOT.US.10>
    END
    IF R.CRDT<CBE.FAC.TOT.US.11> OR R.CRDT<CBE.FAC.TOT.CR.11> THEN
        RF.CODE<1,11>    = '11'
        ACCEPT.AMT<1,11> = R.CRDT<CBE.FAC.TOT.CR.11>
        USED.AMT<1,11>   = R.CRDT<CBE.FAC.TOT.US.11>
    END
    IF R.CRDT<CBE.FAC.TOT.US.12> OR R.CRDT<CBE.FAC.TOT.CR.12> THEN
        RF.CODE<1,12>    = '12'
        ACCEPT.AMT<1,12> = R.CRDT<CBE.FAC.TOT.CR.12>
        USED.AMT<1,12>   = R.CRDT<CBE.FAC.TOT.US.12>
    END
    IF R.CRDT<CBE.FAC.TOT.US.13> OR R.CRDT<CBE.FAC.TOT.CR.13> THEN
        RF.CODE<1,13>    = '13'
        ACCEPT.AMT<1,13> = R.CRDT<CBE.FAC.TOT.CR.13>
        USED.AMT<1,13>   = R.CRDT<CBE.FAC.TOT.US.13>
    END

***************CURRENCY CODES*******************
    BEGIN CASE
    CASE CURR = 'USD'
        CUR.CODE  = '14'

    CASE CURR = 'EUR'
        CUR.CODE  = '42'
    CASE CURR = 'GBP'
        CUR.CODE  = '11'
    CASE CURR = 'SAR'
        CUR.CODE  = '38'
    CASE CURR = 'CHF'
        CUR.CODE  = '6'
    CASE CURR = 'JPY'
        CUR.CODE  = '17'

    END CASE
***************
    FOR Q = 1 TO 13
        AMT.1 = ACCEPT.AMT<1,Q>
        AMT.2 = USED.AMT<1,Q>

        IF AMT.1 EQ '' THEN
            AMT.1 = 0
        END
        IF AMT.2 EQ '' THEN
            AMT.2 = 0
        END

        LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

            RATE = R.BASE<RE.BCP.RATE,POS>
        END

        AMT.1.EGP = AMT.1 * RATE
        AMT.2.EGP = AMT.2 * RATE

        AMT.1.EGP = DROUND(AMT.1.EGP,0)
        AMT.2.EGP = DROUND(AMT.2.EGP,0)

        IF ( AMT.1.EGP NE 0 OR AMT.2.EGP NE 0) THEN

            BB.DATA = BRN:FMT(R.CRDT<CBE.FAC.CBE.NO>,"R%12"):FMT(RF.CODE<1,Q>,"R%2"):FMT(AMT.1.EGP,"R%10"):FMT(AMT.2.EGP,"R%10"):FMT(CUR.CODE,"R%4")
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            BB.DATA = ""
        END

    NEXT Q

    RETURN
******************************************************
END
