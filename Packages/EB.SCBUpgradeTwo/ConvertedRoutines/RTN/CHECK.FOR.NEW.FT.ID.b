* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1032</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CHECK.FOR.NEW.FT.ID(NEW.FT.ID,ER.MSG)
*
* WRITTEN BY P.KARAKITSOS - 27/08/02
* SO AS TO CHECK IF THE NEW FT ID IS CURRENTLY USED
* AND GET THE NEXT ONE ACCORDINGLY
*
* UPDATED BY P.KARAKITSOS - 25/09/02
* GET CORRECT JULIAN DATE IF INFORMER.CREATE.FT GENERATES
* THE FIRST FT OF THE DAY
*
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AUTO.ID.START
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LOCKING
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*
      GOSUB OPEN.AUTO
*
      R.AUTO = "" ; ER.MSG = ""
      CALL F.READ(FN.AUTO,"CONTRACTS",R.AUTO,F.AUTO,ER.MSG)
      IF NOT(ER.MSG) THEN
         POS = 0
         LOCATE "FUNDS.TRANSFER" IN R.AUTO<AUTID.APPLICATION,1> SETTING POS THEN
*
            IF R.AUTO<AUTID.UNIQUE.NO,POS> = "YES" THEN ; * NEW TYPE USED IF "YES"
               GOSUB OPEN.LOCKING
               LAST.PART = ""
               IF R.USER<EB.USE.PROCESS.DEPT,1> = "" THEN LAST.PART = "001" ; * SYSTEM WOULD USE 001 IF EMPTY
               ELSE LAST.PART = R.USER<EB.USE.PROCESS.DEPT,1>
*
               IF LEN(LAST.PART) < 3 THEN LAST.PART := STR("0",3-LEN(LAST.PART))
*
               CALL F.READU(FN.LOCK,"UNIQUE.CONTRACT.":LAST.PART,R.LOCK,F.LOCK,ER.MSG,"")
               IF NOT(ER.MSG) THEN
*
* PK - 25/09/02
*
                  IF R.DATES(EB.DAT.JULIAN.DATE)[5] # R.LOCK<EB.LOK.CONTENT> THEN
                     NEW.FT.ID = "FT":R.DATES(EB.DAT.JULIAN.DATE)[5]:LAST.PART:"000001"
                  END
                  ELSE
*
* PK - 25/09/02
*
                     IF R.LOCK<EB.LOK.CONTENT,1>[6] < 999999 THEN
                        SERIAL = 0 ; SERIAL = R.LOCK<EB.LOK.CONTENT,1>[6] + 1
                        SERIAL = STR("0",6-LEN(SERIAL)):SERIAL ; * TO GET BACK LEADING ZEROES
                        NEW.FT.ID = R.LOCK<EB.LOK.CONTENT,1>[1,7]:LAST.PART:SERIAL ; * SO AS NOT TO LOSE LEADING ZEROES
                        R.LOCK<EB.LOK.CONTENT,1> = NEW.FT.ID
                        CALL F.WRITE (FN.LOCK,"UNIQUE.CONTRACT.":LAST.PART,R.LOCK)
                        IF NOT(RUNNING.UNDER.BATCH) THEN
                           CALL JOURNAL.UPDATE("UNIQUE.CONTRACT.":LAST.PART)
                        END
                     END
                     ELSE ER.MSG = "LIMIT REACHED FOR THIS DEPARTMENT"
*
* PK - 25/09/02
*
                  END
*
* PK - 25/09/02
*
               END
*Line [ 89 ] Add @VM Instead Of VM & Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
               ELSE ER.MSG = "MISSING & ID=&":@FM:"LOCKING":@VM:"UNIQUE.CONTRACT.":LAST.PART
            END
         END
*Line [ 93 ] Add @VM Instead Of VM & Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
         ELSE ER.MSG = "ERROR LOCATING & IN &":@FM:"FUNDS.TRANSFER":@VM:"AUTO.ID.START"
      END
*Line [ 96 ] Add @VM Instead Of VM & Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      ELSE ER.MSG = "MISSING & ID=&":@FM:"AUTO.ID.START":@VM:"FUNDS.TRANSFER"
      TEXT = NEW.FT.ID ; CALL REM
*
      GOTO PROGRAM.END
**************************************************************************************************************************
OPEN.LOCKING:
*
   FN.LOCK = "F.LOCKING" ; F.LOCK = ""
   CALL OPF(FN.LOCK,F.LOCK)
*
   RETURN
**************************************************************************************************************************
OPEN.AUTO:
*
   FN.AUTO = "F.AUTO.ID.START" ; F.AUTO = ""
   CALL OPF(FN.AUTO,F.AUTO)
*
   RETURN
**************************************************************************************************************************
PROGRAM.END:
*
   RETURN
END
