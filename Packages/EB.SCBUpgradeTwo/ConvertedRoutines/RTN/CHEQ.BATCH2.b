* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
    SUBROUTINE CHEQ.BATCH2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    FN.CR='FBNK.STMT.ENTRY';F.CR=''
    CALL OPF(FN.CR,F.CR)
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.CR,F.CR)

    Z     = 0
    XX    = O.DATA
    CALL F.READ(FN.BATCH,XX,R.BATCH,F.BATCH,E2)
    REF     = R.BATCH<SCB.BT.OUR.REFERENCE>
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    REF1    = DCOUNT(REF,@VM)

    REASON  = R.BATCH<SCB.BT.RETURN.REASON>
*Line [ 55 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    REASON1 = DCOUNT(REASON1,@VM)

    FOR I = 1 TO REF1
        IF REASON<I> NE '' THEN
            Z++
        END
    NEXT I
    O.DATA = Z
**  IF REASON NE '' THEN
**  O.DATA = REASON1
**  END

    RETURN
END
