* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE CNV.AC.DR.SUSP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    XX = O.DATA
    AC.CODE = XX[1,16]
    CAT.CODE = XX[17,6]
    SS = ''
    FN.ACC = "FBNK.ACCOUNT"
    F.ACC  = ''
    R.ACC  = ''
    SUSP = '' 
    CALL OPF(FN.ACC,F.ACC)
    CALL F.READ(FN.ACC,AC.CODE,R.ACC,F.ACC,ER)
*Line [ 37 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY = DCOUNT(R.ACC<AC.ACCR.DR.CATEG>,@VM)
    FOR H = 1 TO YY
        IF R.ACC<AC.ACCR.DR.CATEG,H> EQ CAT.CODE THEN
            SUSP += R.ACC<AC.ACCR.DR.SUSP,H>
        END
    NEXT H
    O.DATA = SUSP
    RETURN
END
