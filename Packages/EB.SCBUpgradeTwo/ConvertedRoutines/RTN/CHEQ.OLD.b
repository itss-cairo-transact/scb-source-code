* @ValidationCode : MjoxNTUyMTEzMzA1OkNwMTI1MjoxNjQwNzIyNjI3Mzc2OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:17:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CHEQ.OLD(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
    $INCLUDE   I_FT.LOCAL.REFS

    ID.NEW = ARG
*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,ID.NEW,NEW.LOC)
F.ITSS.FUNDS.TRANSFER = 'FBNK.FUNDS.TRANSFER'
FN.F.ITSS.FUNDS.TRANSFER = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER)
CALL F.READ(F.ITSS.FUNDS.TRANSFER,ID.NEW,R.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER,ERROR.FUNDS.TRANSFER)
NEW.LOC=R.ITSS.FUNDS.TRANSFER<FT.LOCAL.REF>
    ARG = NEW.LOC<1,FTLR.CHEQUE.NO>
RETURN
END
