* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>88</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.EXP.ADD.LW(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ENT = 'FBNK.CATEG.ENT.LWORK.DAY'; F.ENT = ''; R.CATEG.TODAY = ''
    CALL OPF(FN.ENT,F.ENT)
    FN.CTG = 'FBNK.CATEG.ENTRY'; F.CTG = ''; R.CTG = ''
    CALL OPF(FN.CTG,F.CTG)

    T.SEL  = "SELECT ":FN.ENT:" WITH ( @ID LIKE 50... OR @ID LIKE 6... )"
    T.SEL := " AND CO.CODE EQ ":COMP
    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE CAT.ID FROM K.LIST SETTING POS.CAT
        WHILE CAT.ID:POS.CAT
            CALL F.READ(FN.ENT,CAT.ID,R.ENT,F.ENT,ER.ENT)
            LOOP
                REMOVE CTG.ID FROM R.ENT SETTING POS.CTG
            WHILE CTG.ID:POS.CTG
                CALL F.READ(FN.CTG,CTG.ID,R.CTG,F.CTG,ER.CTG)
                CTG.CO  = R.CTG<AC.CAT.COMPANY.CODE>
                CTG.AMT = R.CTG<AC.CAT.AMOUNT.LCY>
                CTG.OUR = R.CTG<AC.CAT.OUR.REFERENCE>
                CTG.TRF = R.CTG<AC.CAT.TRANS.REFERENCE>
                IF CTG.CO EQ COMP AND CTG.AMT GT 0 AND CTG.OUR[1,3] NE 'NET' AND CTG.TRF[1,3] NE 'NET' THEN
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = CTG.ID
                END ELSE
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = "NOLIST"
                END
            REPEAT
        REPEAT
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
