* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
*****         CREATED BY MOHAMED SABRY           *****
*****              IN 2012/1/19                  *****
******************************************************
    SUBROUTINE BLD.LI.2ND.CLASS(ENQ.DATA)
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    K = 0
    FN.LI      = "FBNK.LIMIT"                    ; F.LI      = ""  ; R.LI     = ""
    CALL OPF (FN.LI,F.LI)
    T.SEL  = "SELECT ":FN.LI:" WITH @ID UNLIKE 994... AND MAXIMUM.TOTAL NE ''  AND LIMIT.PRODUCT NE '' AND PRODUCT.ALLOWED EQ '' "
    T.SEL := " AND ( LIMIT.PRODUCT LT 400 OR LIMIT.PRODUCT GT 701 ) BY @ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*MSABRY
*TEXT = SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            WS.CUS.ID = FIELD (KEY.LIST<I>,'.',1)
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            WS.GROUP.NUM  = LOCAL.REF<1,CULR.GROUP.NUM>
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF WS.CREDIT.CODE LE 100 THEN
                IF WS.GROUP.NUM[1,3] # 999 THEN
                    K ++
                    ENQ.DATA<2,K> = "@ID"
                    ENQ.DATA<3,K> = "EQ"
                    ENQ.DATA<4,K> = KEY.LIST<I>
                END
            END
        NEXT I
    END
    IF K = 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
