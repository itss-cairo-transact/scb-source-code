* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>36</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.STMT.LWORK.DAY.TTT.1(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*    $INCLUDE T24.BP I_F.ACCT.ENT.LWORK.DAY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ENT = "FBNK.ACCT.ENT.LWORK.DAY" ; F.ENT = "" ; R.ENT = "" ; ERR.ENT = ""
    CALL OPF(FN.ENT,F.ENT)

    FN.ENT2 = "FBNK.ACCT.ENT.LWORK.DAY" ; F.ENT2 = "" ; R.ENT2 = "" ; ERR.ENT2 = ""
    CALL OPF(FN.ENT2,F.ENT2)

    FN.STE = "FBNK.STMT.ENTRY" ; F.STE = "" ; R.STE = "" ; ERR.STE = ""
    CALL OPF(FN.STE,F.STE)

    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""

    ENT.SEL  ="SELECT ":FN.ENT
*    ENT.SEL :=" BY @ID"
    KK1 = 0
    TTT.MOV = 0
    CALL EB.READLIST(ENT.SEL,K.LIST,'',SELECTED,ER.MSG)

    LOOP
        REMOVE AC.ID FROM K.LIST SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.ENT,AC.ID,R.ENT,F.ENT,ER.ENT)
        CALL F.READ(FN.ENT2,AC.ID,R.ENT2,F.ENT2,ER.ENT2)
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.ID,AC.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CO.CODE ,AC.ID,AC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER ,AC.ID,AC.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>

        IF AC.COM EQ COMP AND AC.CAT NE 1002 AND AC.CAT NE 12005 AND AC.CUS NE "" THEN

            GOSUB TOT.MOV

            IF TTT.MOV.CR GT 500000 OR TTT.MOV.DR LT -500000 THEN
                LOOP
                    REMOVE STE.ID FROM R.ENT SETTING POS.STE
                WHILE STE.ID:POS.STE
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = STE.ID
                REPEAT
            END ELSE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        END ELSE
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = "NOLIST"
        END
    REPEAT
    RETURN
************************************
TOT.MOV:
*********
    TTT.MOV.DR = 0
    TTT.MOV.CR = 0
    LOOP
        REMOVE STE.ID2 FROM R.ENT2 SETTING POS.STE2
    WHILE STE.ID2:POS.STE2
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('STMT.ENTRY':@FM:AC.STE.AMOUNT.LCY ,STE.ID2,STE.AMT)
F.ITSS.STMT.ENTRY = 'FBNK.STMT.ENTRY'
FN.F.ITSS.STMT.ENTRY = ''
CALL OPF(F.ITSS.STMT.ENTRY,FN.F.ITSS.STMT.ENTRY)
CALL F.READ(F.ITSS.STMT.ENTRY,STE.ID2,R.ITSS.STMT.ENTRY,FN.F.ITSS.STMT.ENTRY,ERROR.STMT.ENTRY)
STE.AMT=R.ITSS.STMT.ENTRY<AC.STE.AMOUNT.LCY>
        IF STE.AMT LT 0 THEN
            TTT.MOV.DR += STE.AMT
        END ELSE
            TTT.MOV.CR += STE.AMT
        END
    REPEAT
    RETURN
************************************
END
