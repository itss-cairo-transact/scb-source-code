* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>295</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.STAF.COL.GIVEN(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*   $INCLUDE T24.BP I_F.CUSTOMER.COLLATERAL

    COMP = ID.COMPANY

    FN.IND.G.COL = "FBNK.CUSTOMER.COLLATERAL"   ; F.IND.G.COL = ""
    CALL OPF(FN.IND.G.COL,F.IND.G.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    K = 0

    T.SEL ="SELECT ":FN.IND.G.COL
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            WS.G.CUS.ID = KEY.LIST<I>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,WS.G.CUS.ID,WS.G.SECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.G.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
WS.G.SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
            CALL F.READ(FN.IND.G.COL,WS.G.CUS.ID,R.IND.G.COL,F.IND.G.COL,ER.IND.G.COL)
            IF WS.G.SECTOR EQ 1100 THEN
                LOOP
                    REMOVE COL.G.ID FROM R.IND.G.COL SETTING POS.G.COL
                WHILE COL.G.ID:POS.G.COL
                    CALL F.READ(FN.COL.R,COL.G.ID,R.COL.R,F.COL.R,EER.R)
                    WS.PRC.NO = DCOUNT(R.COL.R<COLL.RIGHT.PERCENT.ALLOC>,@VM)
                    FOR X = 1 TO WS.PRC.NO
                        WS.REF.CUS= R.COL.R<COLL.RIGHT.LIMIT.REF.CUST,X>
                        IF WS.REF.CUS # WS.G.CUS.ID THEN
                            K ++
                            ENQ.DATA<2,K> = "@ID"
                            ENQ.DATA<3,K> = "EQ"
                            ENQ.DATA<4,K> = COL.G.ID
                        END
                    NEXT X
                REPEAT
            END
            IF WS.G.SECTOR NE 1100 THEN
                LOOP
                    REMOVE COL.G.ID FROM R.IND.G.COL SETTING POS.G.COL
                WHILE COL.G.ID:POS.G.COL
*WS.R.CUS.ID = FIELD(COL.G.ID,"*",1)
                    CALL F.READ(FN.COL.R,COL.G.ID,R.COL.R,F.COL.R,EER.R)
                    WS.PRC.NO = DCOUNT(R.COL.R<COLL.RIGHT.PERCENT.ALLOC>,@VM)
                    FOR XX = 1 TO WS.PRC.NO
                        WS.R.CUS.ID = R.COL.R<COLL.RIGHT.LIMIT.REF.CUST,XX>
*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,WS.R.CUS.ID,WS.R.SECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.R.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
WS.R.SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
                        *IF WS.R.CUS.ID # WS.G.CUS.ID THEN
                            IF WS.R.SECTOR EQ 1100 THEN
                                K ++
                                ENQ.DATA<2,K> = "@ID"
                                ENQ.DATA<3,K> = "EQ"
                                ENQ.DATA<4,K> = COL.G.ID
                            END
                        *END
                    NEXT XX
                REPEAT
            END
        NEXT I
    END
    K ++
    IF K = 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
*-----------------------------------------------------------------------------------------------------
END
