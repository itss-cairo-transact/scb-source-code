* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.Y.POST.LIST(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.STMT.HANDOFF
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
    FN.SCR = 'F.SCB.CUS.POS'
    F.SCR = ''
    R.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    K.LIST ="" ; SELECTED ="" ; ER.MSG =""
    TT = TODAY
    TTY = TT[1,4]
    TTM = TT[5,2]
    TTD = TT[7,2]
    TTX = TTY : TTM : '01'
    CALL CDT("",TTX,'-1W')
    T.SEL  ="SELECT ":FN.SCR:" WITH SYS.DATE EQ ":TTX:" AND CO.CODE EQ ":COMP
    T.SEL :=" AND CUSTOMER NE ''"
    T.SEL :=" AND DEAL.AMOUNT NE 0 AND DEAL.AMOUNT NE ''"
    T.SEL :=" AND CATEGORY NE 1002 1710 1711 9090 1220 1221 1222 1223 1224 1225 1226 1227"
    T.SEL :=" BY CO.CODE BY CUSTOMER"
    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        ACCC = ''
        CUST = ''
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.SCR,K.LIST<I>,R.SCR,F.SCR,ER.MSG1)
            CUST = R.SCR<CUPOS.CUSTOMER>
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,CUST,POST.R)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POST.R=R.ITSS.CUSTOMER<EB.CUS.POSTING.RESTRICT>
            IF POST.R LT 90 THEN
                IF CUST # ACCC THEN
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = CUST
                    ACCC = CUST
                END
            END
        NEXT I
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
END
