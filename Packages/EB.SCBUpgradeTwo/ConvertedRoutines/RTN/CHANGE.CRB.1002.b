* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*==============================================*
* WRITE BY MOHAMED BAKRY                       *
* TO EMPTY LINE AT CATEGORY 1002               *
*==============================================*
    PROGRAM CHANGE.CRB.1002

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.CRB = "&HOLD&" ; F.CRB = ''
    CALL OPF(FN.CRB,F.CRB)

    FN.HC = "F.HOLD.CONTROL" ; F.CRB = ''
    CALL OPF(FN.CRB,F.CRB)

    FN.AC = "FBNK.ACCOUNT" ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG="" ; X.DESC = "" ; WS.DATE.LWD = ''

*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,"EG0010001",WS.DATE.LWD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,"EG0010001",R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.DATE.LWD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    T.SEL = "SELECT ": FN.HC:" WITH REPORT.NAME EQ CRB.GENLED AND BANK.DATE EQ ":WS.DATE.LWD

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CRB,KEY.LIST<I>,R.CRB,F.CRB,E1)

*        READ R.CRB FROM F.CRB,KEY.LIST<I> THEN NULL

*Line [ 53 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        NO.LINES=DCOUNT(R.CRB,@FM)

        FOR K = 1 TO NO.LINES

            LIN  = R.CRB<K>

            IF INDEX(LIN,'10100201',1) THEN
                ST.ACC1 = INDEX(LIN,'10100201',1)
                ST.ACC2 = ST.ACC1 - 8
                SCB.ACC = LIN[ST.ACC2,16]
                CALL F.READ(FN.AC,SCB.ACC,R.AC,F.AC,E.AC)
                IF NOT(E.AC) THEN
                    IF R.AC<AC.CATEGORY> = 1002 THEN

                        E.AC  = ''

                        LIN = SPACE(LEN(LIN))
                        LIN = SPACE(43):"���������� �����������"
                        R.CRB<K> = LIN
                    END
                END
            END

        NEXT K
***-------------------
*******************************UPDATED BY RIHAM R15**********************
*        WRITE  R.CRB TO F.CRB , KEY.LIST<I> ON ERROR
*            PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CRB
*        END
        CALL F.WRITE(FN.CRB, KEY.LIST<I>,R.CRB)
        CALL JOURNAL.UPDATE(KEY.LIST<I>)

        PRINT KEY.LIST<I>
        E1  = ''

    NEXT I
    RETURN

* ==============================================================


END
