* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.INTERBRANCH(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ENT = "FBNK.ACCT.ENT.TODAY" ; F.ENT = "" ; R.ENT = "" ; ERR.ENT = ""
    CALL OPF(FN.ENT,F.ENT)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE = "" ; R.STE = "" ; ERR.STE = ""
    CALL OPF(FN.STE,F.STE)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    ENT.SEL  ="SELECT ":FN.ENT:" WITH CO.CODE EQ ":COMP
    ENT.SEL :=" BY @ID"
    KK1 = 0
    CALL EB.READLIST(ENT.SEL,K.LIST,'',SELECTED,ER.MSG)
    LOOP
        REMOVE AC.ID FROM K.LIST SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.ENT,AC.ID,R.ENT,F.ENT,ER.ENT)
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.ID,AC.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CO.CODE ,AC.ID,AC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
        IF AC.COM EQ COMP THEN
            IF AC.CAT NE 1002 THEN
                LOOP
                    REMOVE STE.ID FROM R.ENT SETTING POS.STE
                WHILE STE.ID:POS.STE
*                    KK1++
                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                    STE.REF  = R.STE<AC.STE.TRANS.REFERENCE>
                    STE.COM  = R.STE<AC.STE.COMPANY.CODE>
                    IF STE.COM EQ COMP THEN
                        IF STE.REF[4] EQ '\BNK' THEN
                            STE.REF = FIELD(STE.REF,'\',1):'\B01'
                        END
                        FINDSTR '\B' IN STE.REF SETTING Y.XX THEN
                            BRR = FIELD(STE.REF,'\',2)
                            IF BRR[2] NE COMP[2] AND BRR[2] NE '99' THEN
                                KK1++
                                ENQ<2,KK1> = "@ID"
                                ENQ<3,KK1> = "EQ"
                                ENQ<4,KK1> = STE.ID
                            END ELSE
                                KK1++
                                ENQ<2,KK1> = "@ID"
                                ENQ<3,KK1> = "EQ"
                                ENQ<4,KK1> = "NOLIST"
                            END
                        END ELSE
                            KK1++
                            ENQ<2,KK1> = "@ID"
                            ENQ<3,KK1> = "EQ"
                            ENQ<4,KK1> = "NOLIST"
                        END
                    END ELSE
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = "NOLIST"
                    END
                REPEAT
            END ELSE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        END ELSE
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = "NOLIST"
        END
    REPEAT
    RETURN
