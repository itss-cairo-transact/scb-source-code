* @ValidationCode : MjozNDQyMzU4ODU6Q3AxMjUyOjE2NDA3MjM5OTY4MDk6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:39:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
************************NI7OOOOOOOOOOOOOO************************
SUBROUTINE CNV.BYAN

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RETIREMENTS

    FN.FT = 'FBNK.FUNDS.TRANSFER' ;  F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.FT.HIS = 'FBNK.FUNDS.TRANSFER$HIS' ;  F.FT.HIS = ''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    FN.AC = 'FBNK.ACCOUNT' ;  F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.AC.HIS = 'FBNK.ACCOUNT$HIS' ;  F.AC.HIS = ''
    CALL OPF(FN.AC.HIS,F.AC.HIS)

    CALL F.READ(FN.FT,O.DATA,R.FT,F.FT,ETEXT)
    IF NOT(ETEXT) THEN
        BYAN   = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
        O.DATA = BYAN
    END
    IF ETEXT THEN
        YY = O.DATA:';1'
        CALL F.READ(FN.FT.HIS,YY,R.FT.HIS,F.FT.HIS,ETEXT2)
        BYAN.HIS   = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
        O.DATA     = BYAN.HIS
    END
RETURN
END
