* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.SCBINTERLI01(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

    COMP = ID.COMPANY

    KEY.LIST="" ;  SELECTED="" ;  ER.MSG=""
    FN.CUR ='F.NUMERIC.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    LOCATE "NOTES" IN ENQ<2,1> SETTING CAT.POS THEN
        ID.ALL  = ENQ<4,CAT.POS>
    END
    ID.LEN =  LEN(ID.ALL)

    IF ID.LEN = 10 THEN
        CUST.ID  = ID.ALL[1,8] + 0
        CUR.ID   = ID.ALL[9,2]
    END

    IF ID.LEN = 9 THEN

        CUST.ID  = ID.ALL[1,7]
        CUR.ID   = ID.ALL[8,2]
    END

    CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,E1)
    CUR.NAME = R.CUR<EB.NCN.CURRENCY.CODE>

    T.SEL  = "SELECT FBNK.LIMIT WITH @ID LIKE ":CUST.ID:"... AND LIMIT.CURRENCY EQ ":CUR.NAME:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*==============================================================
    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
    RETURN
END
