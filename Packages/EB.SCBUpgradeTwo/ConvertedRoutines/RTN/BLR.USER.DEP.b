* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.USER.DEP(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
 *   $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.USR = "FBNK.USER" ; F.USR = "" ; R.USR = "" ; ERR.USR = ""
    CALL OPF(FN.USR,F.USR)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    DEP = COMP[2]
    DEP = DEP * 1
    USR.SEL  ="SELECT ":FN.USR:" WITH DEPARTMENT.CODE EQ ":DEP
    USR.SEL :=" AND @ID LIKE SCB... AND SCB.DEPT.CODE NE 2800"
    USR.SEL :=" BY @ID"
    KK1 = 0
    CALL EB.READLIST(USR.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = K.LIST<I>
        NEXT I
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
