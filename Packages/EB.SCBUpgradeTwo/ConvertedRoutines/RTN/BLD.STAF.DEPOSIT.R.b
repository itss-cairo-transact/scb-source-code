* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.STAF.DEPOSIT.R(ENQ.DATA)
*****    PROGRAM    BLD.STAF.DEPOSIT.R
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD       = ''
    R.LD       = ''
    Y.LD.ID    = ''

    CALL OPF(FN.LD,F.LD)



    SEL.CMD  ="SELECT " :FN.LD:" WITH CATEGORY IN(21001 21002 21003 21004 21005 21006 21007 21008 21009 21010)"

    SEL.CMD :=" AND FIN.MAT.DATE GT ":TODAY
    SEL.CMD :=" AND AMOUNT NE 0"
    SEL.CMD :=" AND CUSTOMER.ID NE 0"
***    SEL.CMD :=" AND CURRENCY EQ 'EGP'"
***    SEL.CMD :=" AND CURRENCY NE 'EGP'"
***    SEL.CMD :=" AND CURRENCY EQ 'SAR'"
***    SEL.CMD :=" AND CURRENCY EQ 'EUR'"
***    SEL.CMD :=" AND CURRENCY IN('SAR' 'EUR')"


    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)


    IF NOREC THEN
        K = 0
        FOR I = 1 TO NOREC
            SECTOR = 0

            CALL F.READ(FN.LD,SELLIST<I>,R.LD,F.LD,ERR.LD)
            CUS.ID = R.LD<LD.CUSTOMER.ID>

*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.ID,SECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>

*******            IF (SECTOR GE 1100 AND SECTOR LE 1400) OR CUS.ID EQ 1304955 THEN
*******            IF CUS.ID EQ 3100141 THEN
*******            IF SELLIST<I> NE '' THEN

            IF (SECTOR GE 1100 AND SECTOR LE 1400) THEN

                K += 1

                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = SELLIST<I> 

            END
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
