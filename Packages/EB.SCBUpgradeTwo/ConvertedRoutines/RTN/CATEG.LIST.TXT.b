* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CATEG.LIST.TXT
 *   PROGRAM CATG.LIST.TXT
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY


*****
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "CATG.LIST.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/":' ':"CATG.LIST.TXT"
        HUSH OFF
    END
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "CATG.LIST.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CATG.LIST.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create CATG.LIST.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END

*****
    FN.CAT  = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = ''
    CALL OPF( FN.CAT,F.CAT)

    T.SEL = "SELECT F.CATEGORY WITH (@ID GE 21001 AND @ID LE 21010) OR (@ID GE 21020 AND @ID LE 21032) "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CAT,KEY.LIST<I>, R.CAT, F.CAT, ETEXT)
            CAT.DESC = R.CAT<EB.CAT.DESCRIPTION><1,2>
            BB.DATA  = KEY.LIST<I>:'|'
            BB.DATA := CAT.DESC

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
