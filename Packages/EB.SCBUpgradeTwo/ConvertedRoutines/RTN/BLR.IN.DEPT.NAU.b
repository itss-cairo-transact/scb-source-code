* @ValidationCode : MjotNDk4NjkwMTgxOkNwMTI1MjoxNjQwNzE4ODY3NzUyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:14:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*****************MAHMOUD 11/7/2012************************
SUBROUTINE BLR.IN.DEPT.NAU(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USR.LOCAL.REF

    COMP = ID.COMPANY
    ZZ = 0
    USER.DEPT = R.USER<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
    IF COMP EQ 'EG0010099' THEN
        FN.INF = "F.INF.MULTI.TXN$NAU" ; F.INF= "" ; R.INF = ""
        CALL OPF(FN.INF,F.INF)
        I.SEL  = "SELECT ":FN.INF:" WITH CO.CODE EQ ":COMP
        CALL EB.READLIST(I.SEL,K.LIST,'',SELECTED,ER.MSG)
        IF SELECTED THEN
            LOOP
                REMOVE IN.ID FROM K.LIST SETTING POS.IN
            WHILE IN.ID:POS.IN
                CALL F.READ(FN.INF,IN.ID,R.INF,F.INF,ER.INF)
                INPUTT  = FIELD(R.INF<INF.MLT.INPUTTER,1>,'_',2)
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT,USR.LCL)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPUTT,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USR.LCL=R.ITSS.USER<EB.USE.LOCAL.REF>
                IN.DEPT = USR.LCL<1,USER.SCB.DEPT.CODE>
*****UPDATED BY MAHMOUD 13/8/2012**********
                IF (IN.DEPT EQ USER.DEPT) OR (USER.DEPT EQ 5100) THEN
*                IF IN.DEPT EQ USER.DEPT THEN
***END OF UPDATE***************************
                    ZZ++
                    ENQ<2,ZZ> = '@ID'
                    ENQ<3,ZZ> = 'EQ'
                    ENQ<4,ZZ> = IN.ID
                END ELSE
                    ZZ++
                    ENQ<2,ZZ> = '@ID'
                    ENQ<3,ZZ> = 'EQ'
                    ENQ<4,ZZ> = "NOLIST"
                END
            REPEAT
        END ELSE
            ZZ++
            ENQ<2,ZZ> = '@ID'
            ENQ<3,ZZ> = 'EQ'
            ENQ<4,ZZ> = "NOLIST"
        END
    END
RETURN
END
