* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
**-------------M.ELSAYED--------------**
*-----------------------------------------------------------------------------
* <Rating>1134</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.SAVE.AC(ENQ)
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    KK = 0
*=============================================================*
    FN.CAC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CAC = '' ;  R.CAC = ''
    CALL OPF(FN.CAC,F.CAC)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)

    CUSTOMER     = R.AC<AC.CUSTOMER>
*Line [ 54 ] Comment DEBUG - ITSS - R21 Upgrade - 2022-02-09
    **DEBUG
*====================================================================*
* T.SEL = "SELECT ":FN.AC:" WITH CO.CODE EQ ":COMP:" AND CATEGORY LIKE 6501 BY @ID"
*  T.SEL = "SELECT ":FN.CU:" WITH COMPANY.BOOK EQ ":COMP:" AND POSTING.RESTRICT LT 90 BY @ID
*    T.SEL = "SELECT ":FN.CAC:" WITH @ID EQ 1100024 AND COMPANY.CO EQ ":COMP:" BY @ID"
    T.SEL = "SELECT ":FN.CAC:" WITH COMPANY.CO EQ ":COMP:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED,ASD)
    IF SELECTED THEN
        FOR Y = 1 TO SELECTED
            WS.SAV.FLG = 0
            WS.OTH.FLG = 0
            CALL F.READ(FN.CAC,KEY.LIST<Y>,R.CAC,F.CAC,E.CAC)

            DD = DCOUNT(R.CAC,@FM)

            IF DD GT 1 THEN
                LOOP
                    REMOVE CAC.ID FROM R.CAC SETTING POS
                WHILE CAC.ID:POS
                    CALL F.READ(FN.AC,CAC.ID,R.AC,F.AC,E.AC )
                    OPN.ACTUAL   = R.AC<AC.OPEN.ACTUAL.BAL>
                    OPN.CLEARD   = R.AC<AC.OPEN.CLEARED.BAL>
                    ONLNE.ACTUAL = R.AC<AC.ONLINE.ACTUAL.BAL>
                    ONLNE.CLEARD = R.AC<AC.ONLINE.CLEARED.BAL>
                    W.BAL        = R.AC<AC.WORKING.BALANCE>
                    CATEGORY     = R.AC<AC.CATEGORY>

                    IF CATEGORY GE 6501 AND CATEGORY LE 6512 THEN
                        WS.SAV.FLG = 1
                    END ELSE
                        IF CATEGORY GE 1100 AND CATEGORY LE 1599 THEN
*IF OPN.ACTUAL GT 0 AND  OPN.CLEARD GT 0 AND ONLNE.ACTUAL GT 0 AND OPN.ACTUAL GT 0 AND W.BAL GT 0 THEN
                            IF ONLNE.ACTUAL EQ 0 OR ONLNE.ACTUAL EQ '' THEN

                                WS.OTH.FLG = 1
                            END ELSE WS.OTH.FLG = 0 ; GOTO NEXTREC
                        END ELSE
                            IF CATEGORY GE 1001 AND CATEGORY LE 1003 THEN
*IF OPN.ACTUAL GT 0 AND  OPN.CLEARD GT 0 AND ONLNE.ACTUAL GT 0 AND OPN.ACTUAL GT 0 AND W.BAL GT 0 THEN
                                IF  ONLNE.ACTUAL EQ 0 OR ONLNE.ACTUAL EQ ''  THEN
                                    WS.OTH.FLG = 1
                                END ELSE WS.OTH.FLG = 0 ; GOTO NEXTREC
                            END
                        END
                    END

                    IF WS.SAV.FLG EQ 1 AND WS.OTH.FLG EQ 1 THEN
                        KK++
                        ENQ<2,KK> = "@ID"
                        ENQ<3,KK> = "EQ"
                        ENQ<4,KK> = KEY.LIST<Y>
                    END
                REPEAT
            END
NEXTREC:
        NEXT Y
    END
    IF KK = 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
    RETURN

*******************************************************************************************
END
