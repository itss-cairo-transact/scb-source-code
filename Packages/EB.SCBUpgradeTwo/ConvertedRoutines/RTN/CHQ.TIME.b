* @ValidationCode : MjoxMjg2NzAzODA1OkNwMTI1MjoxNjQwNzIzMDkxNTU4OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:24:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******NESSREEN AHMED 09/05/2011*******
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CHQ.TIME(TIMU)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHQ.RETURN.NEW

*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

    KEY.ID = TIMU
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR( 'SCB.CHQ.RETURN.NEW':@FM:CHQ.RET.DATE.TIME, KEY.ID , DATT)
F.ITSS.SCB.CHQ.RETURN.NEW = 'F.SCB.CHQ.RETURN.NEW'
FN.F.ITSS.SCB.CHQ.RETURN.NEW = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW,KEY.ID,R.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW,ERROR.SCB.CHQ.RETURN.NEW)
DATT=R.ITSS.SCB.CHQ.RETURN.NEW<CHQ.RET.DATE.TIME>
    TEXT = 'DATT=':DATT ; CALL REM
    TIMUTT = DATT<1,1>
    TEXT = 'DATE=':TIMUTT ; CALL REM
    TIM = TIMUTT[7,4]
    TIMHS = FMT(TIM,"R##:##")
    HH = TIMHS[4,2]
    SS = TIMHS[1,2]
    TIMU = SS:":":HH
    TIMU = TIMU<1,1>
    TEXT = 'TIME=':TIMU ; CALL REM
RETURN
END
