* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*--- CREATE BY MAHMOUD SHAMS
*--- EDIT BY NESSMA ADEL
    SUBROUTINE CNV.CBE.BANK.ID

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*-----------------------------------------
    XXB = O.DATA
    BEGIN CASE
    CASE XXB = "99450303"
        O.DATA = "5300"
    CASE XXB = "99457200"
        O.DATA = "3300"
    CASE XXB = "99400005"
        O.DATA = "4700"
    CASE XXB = "99400006"
        O.DATA = "7700"
    CASE XXB = "99400028"
        O.DATA = "8000"
    CASE XXB = "99400086"
        O.DATA = "4200"
    CASE XXB = "99400087"
        O.DATA = "5400"
    CASE XXB = "99400098"
        O.DATA = "4100"
    CASE XXB = "99400100"
        O.DATA = "200"
    CASE XXB = "99400101"
        O.DATA = "4900"
    CASE XXB = "99400102"
        O.DATA = "4500"
    CASE XXB = "99400103"
        O.DATA = "2000"
    CASE XXB = "99400104"
        O.DATA = "3000"
    CASE XXB = "99400117"
        O.DATA = "4300"
    CASE XXB = "99400118"
        O.DATA = "4600"
    CASE XXB = "99400119"
        O.DATA = "3200"
    CASE XXB = "99400174"
        O.DATA = "3800"
    CASE XXB = "99400200"
        O.DATA = "400"
    CASE XXB = "99400300"
        O.DATA = "100"
    CASE XXB = "99400400"
        O.DATA = "300"
    CASE XXB = "99400600"
        O.DATA = "1300"
    CASE XXB = "99400700"
        O.DATA = "5100"
    CASE XXB = "99400800"
        O.DATA = "2300"
    CASE XXB = "99400911"
        O.DATA = "5000"
    CASE XXB = "99400920"
        O.DATA = "2501"
    CASE XXB = "99400930"
        O.DATA = "2700"
    CASE XXB = "99400940"
        O.DATA = "128"
    CASE XXB = "99400950"
        O.DATA = "2600"
    CASE XXB = "99491101"
        O.DATA = "4400"
    CASE XXB = "99492202"
        O.DATA = "1400"
    CASE XXB = "99465201"
        O.DATA = "2400"
    CASE XXB = "99470103"
        O.DATA = "1500"
    CASE XXB = "99483002"
        O.DATA = "3900"
    CASE XXB = "99400188"
        O.DATA = "5300"
    CASE XXB = "99400500"
        O.DATA = "9700"
    END CASE
    RETURN
