* @ValidationCode : Mjo4NDE2MzkzMDU6Q3AxMjUyOjE2NDA3Mzk1NDcwMTE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:59:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE CHEQ.NO

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_F.BILL.REGISTER
    $INSERT  I_BR.LOCAL.REFS
*-------------------------------------------------------------
    APPL.ID = COMI
    IF APPL.ID MATCH '0N' THEN

        IF LEN(COMI) > 14 THEN
            ETEXT = 'CHEQ NO MUST 14 NO' ;  CALL STORE.END.ERROR
        END

        IF LEN(R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO>) > 14 THEN
            ETEXT = 'CHEQ NO MUST 14 NO' ;  CALL STORE.END.ERROR
        END
    END ELSE
        ETEXT = 'NOT VALID NUMBER'  ;  CALL STORE.END.ERROR
    END
*--------------------------------------------------------------
    IF MESSAGE EQ 'VAL' THEN
        IF PGM.VERSION EQ ',SCB.CHQ.LCY.REG.0017' THEN
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ 0017 THEN
                CALL VVR.BR.CHQ.CHECK(COMI)
            END
        END
    END
*--------------------------------------------------------------
RETURN
END
