* @ValidationCode : MjoxMjQ3NDQ5MjE2OkNwMTI1MjoxNjQwNjM2NTY4NjU5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 12:22:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>78</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CHECK.VERSION.VLD.RTNS(VER.NAME, ER.MSG)

* Written by V.Papadopoulos for INFORMER S.A.

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.VERSION
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER

    ER.MSG = '' ; ETEXT = '' ; VAL.RTNS = '' ; VAL.FLDS = ''

*Line [ 34 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('VERSION':@FM:EB.VER.VALIDATION.RTN, VER.NAME, VAL.RTNS)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
VAL.RTNS=R.ITSS.VERSION<EB.VER.VALIDATION.RTN>
*Line [ 36 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('VERSION':@FM:EB.VER.VALIDATION.FLD, VER.NAME, VAL.FLDS)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
VAL.FLDS=R.ITSS.VERSION<EB.VER.VALIDATION.FLD>
    IF NOT(ETEXT) THEN
        IF VAL.RTNS THEN
            I.A = 1
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOOP WHILE I.A <= DCOUNT(VAL.FLDS<1>, @VM) & NOT(ER.MSG)
                SAVE.MESSAGE = MESSAGE
                MESSAGE = ''
                GOSUB RUN.VLD.RTN
                MESSAGE = 'VAL'
                GOSUB RUN.VLD.RTN
                MESSAGE = SAVE.MESSAGE
                I.A += 1
            REPEAT
        END
    END
    ELSE
        ER.MSG = ETEXT:' .. ':VER.NAME
        ETEXT = ''
    END

    GOTO PROGRAM.END

*------------------------------------------------------------------------------

*--------------- RUN VALIDATION ROUTINE ---------------------------------------
RUN.VLD.RTN:

    FLD.NUM = ''
    CALL GET.SS.FIELD('FUNDS.TRANSFER', VAL.FLDS<1, I.A>, FLD.NUM, 'NUM', ER.MSG)
    IF NOT(ER.MSG) & FLD.NUM THEN
        TELIA = ''
        TELIA = INDEX(FLD.NUM, '.', 1)

        IF TELIA THEN

            F.FM = FLD.NUM[1, TELIA - 1]

            F.VM = FLD.NUM[TELIA + 1, LEN(FLD.NUM) - TELIA]
            TELIA = ''

            TELIA = INDEX(F.VM, '.', 1)
            IF TELIA THEN

                F.SM = F.VM[TELIA + 1, LEN(F.VM) - TELIA]

                F.VM = F.VM[1, TELIA - 1]
            END
            ELSE

                F.SM = ''
            END
        END
        ELSE

            F.FM = FLD.NUM

            F.VM = ''

            F.SM = ''
        END


        COMI = R.NEW(F.FM)<1, F.VM, F.SM>
        SAVE.COMI = COMI
        CALL.ROUTINE = ''
        CALL.ROUTINE = VAL.RTNS<1, I.A>
        CALL @CALL.ROUTINE
        IF NOT(ETEXT) THEN
            IF SAVE.COMI # COMI THEN

                R.NEW(F.FM)<1, F.VM, F.SM> = COMI
            END
        END
        ELSE
            ER.MSG = ETEXT
            ETEXT = ''
        END
    END
    ELSE
        ER.MSG = 'INVALID FIELD NAME'
    END

RETURN
*------------------------------------------------------------------------------

*------------------ SUBROUTINE END ... YES !!! --------------------------------
PROGRAM.END:

RETURN

END
