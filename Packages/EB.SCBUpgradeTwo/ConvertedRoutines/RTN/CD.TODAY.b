* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-24</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CD.TODAY(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""
    DATE.TO = '...':TODAY[3,6]:'...'
*MSABRY 2010/05/03
    DATE.FROM = TODAY[3,6]:'0001'
    CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>
****    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21020 AND CATEGORY LE 21025  AND VERSION.NAME EQ ',SCB.CD.AMEND1' AND DATE.TIME LIKE ": DATE.TO :" AND DEPT.CODE EQ ":CODE.USER
   T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21019 AND CATEGORY LE 21029  AND VERSION.NAME EQ ',SCB.CD.AMEND1' AND DATE.TIME LIKE ": DATE.TO :" AND CO.CODE EQ " : COMP
*MSABRY 2010/05/03
*    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21020 AND CATEGORY LE 21025  AND VERSION.NAME EQ ',SCB.CD.AMEND1' AND DATE.TIME GT ": DATE.FROM :" AND CO.CODE EQ ":COMP
*********    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21020 AND CATEGORY LE 21025  AND VERSION.NAME EQ ',SCB.CD.AMEND1' AND DATE.TIME LIKE 0909... AND CO.CODE EQ " : COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**************************************************
    IF SELECTED  THEN
        FOR KK = 1 TO SELECTED
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
* ENQ.ERROR = "NO RESULT FOUND"
    END
    RETURN
END
