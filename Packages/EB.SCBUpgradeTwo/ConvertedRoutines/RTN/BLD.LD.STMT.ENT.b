* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
**** CREATED BY MOHAMED SABRY 2010/05/03
    SUBROUTINE BLD.LD.STMT.ENT(ENQ)
****    PROGRAM BLD.LD.STMT.ENT
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
* $INCLUDE GLOBUS.BP I_F.ACCT.ENT.LWORK.DAY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    I = 0
*----------------------------------
    FN.STMT = 'FBNK.STMT.ENTRY'
    F.STMT  = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.ENT.TD = 'FBNK.ACCT.ENT.TODAY'
    F.ENT.TD  = ''
    CALL OPF(FN.ENT.TD,F.ENT.TD)

    FN.ENT.LW = 'FBNK.ACCT.ENT.LWORK.DAY'
    F.ENT.LW  = ''
    CALL OPF(FN.ENT.LW,F.ENT.LW)

    DAT.ID    = 'EG0010001'
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.LPE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.LPE=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.LWD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.LWD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

************************* ACCT.ENT.TODAY *************************
    T.SEL2 = "SELECT ":FN.ENT.TD:" WITH CO.CODE EQ ":COMP:" BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",NO.REC,ER.SEL2)
    IF NO.REC THEN
        FOR X = 1 TO NO.REC
            CALL F.READ(FN.ENT.TD,KEY.LIST2<X>,R.ENT.TD,F.ENT.TD,EER.R)
            LOOP
                REMOVE WS.STMT.ID FROM R.ENT.TD SETTING POS
            WHILE WS.STMT.ID:POS
                CALL F.READ(FN.STMT,WS.STMT.ID,R.STMT,F.STMT,ERR.STMT)
                WS.STMT.GL = R.STMT<AC.STE.CRF.PROD.CAT>
                WS.STMT.TR = R.STMT<AC.STE.TRANSACTION.CODE>
                IF (( WS.STMT.GL GE 21020 AND WS.STMT.GL LE 21028 ) OR WS.STMT.GL EQ 21032 ) THEN
                    IF WS.STMT.TR EQ 424 THEN
                        I++
                        ENQ<2,I> = '@ID'
                        ENQ<3,I> = 'EQ'
                        ENQ<4,I> = WS.STMT.ID
                    END
                END
            REPEAT
        NEXT X
    END
************************ ACCT.ENT.LWORK.DAY ***************************
    IF WS.LPE NE WS.LWD THEN
        T.SEL = "SELECT ":FN.ENT.LW:" WITH CO.CODE EQ ":COMP:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",NO.REC.LW,ER.SEL)
        IF NO.REC.LW THEN
            FOR Y = 1 TO NO.REC.LW
                CALL F.READ(FN.ENT.LW,KEY.LIST<Y>,R.ENT.LW,F.ENT.LW,EER.R.LW)
                LOOP
                    REMOVE WS.STMT.ID FROM R.ENT.LW SETTING POS1
                WHILE WS.STMT.ID:POS1
                    CALL F.READ(FN.STMT,WS.STMT.ID,R.STMT,F.STMT,ERR.STMT)
                    WS.STMT.GL = R.STMT<AC.STE.CRF.PROD.CAT>
                    WS.STMT.TR = R.STMT<AC.STE.TRANSACTION.CODE>
                    WS.STMT.VD = R.STMT<AC.STE.VALUE.DATE>
                    IF (( WS.STMT.GL GE 21020 AND WS.STMT.GL LE 21028 ) OR WS.STMT.GL EQ 21032 ) THEN
                        IF WS.STMT.VD GT WS.LWD THEN
                            IF WS.STMT.TR EQ 424 THEN
                                I++
                                ENQ<2,I> = '@ID'
                                ENQ<3,I> = 'EQ'
                                ENQ<4,I> = WS.STMT.ID
                            END
                        END
                    END
                REPEAT
            NEXT Y
        END
    END
************************ IF NO REC IN TODAY AND LWD **********************
    IF I EQ 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'

    END
END
