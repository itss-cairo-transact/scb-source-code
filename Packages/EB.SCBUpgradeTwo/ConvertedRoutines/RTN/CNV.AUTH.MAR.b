* @ValidationCode : MjotMjEyMjM0NjM3NDpDcDEyNTI6MTY0MDcyNDAxNjAyMDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:40:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*************************NI7OOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.AUTH.MAR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE.INPUT

    FN.SAMP = 'F.SCB.DEPT.SAMPLE1' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)

    FN.SAMP.INP = 'F.SCB.DEPT.SAMPLE.INPUT' ; F.SAMP.INP = ''
    CALL OPF(FN.SAMP.INP,F.SAMP.INP)

    XX = '...':O.DATA:'...'
    T.SEL = "SELECT F.SCB.DEPT.SAMPLE.INPUT WITH @ID LIKE ":XX:" AND FUNCTION EQ 99 AND END.DATE EQ 99 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        CALL F.READ(FN.SAMP.INP,KEY.LIST,R.SAMP.INP,F.SAMP.INP,ERR)
        YY = R.SAMP.INP<SAMP.INPUT>
        ZZ = R.SAMP.INP<SAMP.AUTH>

    END
    O.DATA = ZZ
RETURN
END
