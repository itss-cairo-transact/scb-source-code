* @ValidationCode : MjotMjkxNjEwNDY0OkNwMTI1MjoxNjQwNzI0MjUxMzU0OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE BLR.DR.CAT(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.RANGE
*Line [ 28 ] HASHING "$INCLUDE I_F.SCB.TRANS.TODAY" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.TRN = 'F.SCB.TRANS.TODAY' ; F.TRN = '' ; R.TRN = ''
    CALL OPF(FN.TRN,F.TRN)
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.RG = 'F.RE.STAT.RANGE' ; F.RG = '' ; R.RG = ''
    CALL OPF(FN.RG,F.RG)

    TT = TODAY
    KK1 = 0
    MM  = 0
    CATEG = ''
    CAT.CRIT = ''
    XX = ''
    LN.ID = 'GENLED.0100'
    GOSUB GETCATEG
    LN.ID = 'GENLED.0285'
    GOSUB GETCATEG

    T.SEL = "SELECT ":FN.TRN:" WITH BOOKING.DATE EQ ":TT:" AND COMPANY.CO EQ ":COMP
    T.SEL := " AND ACCOUNT.NUMBER NE '' AND AMOUNT.LCY LT 0"
    T.SEL := " AND ( ":CAT.CRIT
    T.SEL := " OR   ( PRODUCT.CATEGORY IN ( ":CATEG:" )))"
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            KK1 += 1
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = K.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
RETURN
***********************************************
GETCATEG:
*********
    CALL F.READ(FN.LN,LN.ID,R.LN,F.LN,ERR.LN)
*Line [ 75 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    PROD.NO = DCOUNT(R.LN<RE.SRL.ASSET1>,@VM)
    FOR XX = 1 TO PROD.NO
        PRODUCT = R.LN<RE.SRL.ASSET1,XX>
        IF PRODUCT[1,1] EQ '*' THEN
            MM++
            GOSUB GETRANGE
        END ELSE
            CATEG = CATEG:" ":R.LN<RE.SRL.ASSET1,XX>
        END
    NEXT XX
RETURN
***********************************************
GETRANGE:
********
    SS = LEN(PRODUCT) - 1
    RG.ID = PRODUCT[2,SS]
    CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
    CATFR = R.RG<RE.RNG.START.RANGE>
    CATTO = R.RG<RE.RNG.END.RANGE>
    IF MM EQ 1 THEN
        ORR = ''
    END ELSE
        ORR = ' OR '
    END
    CAT.CRIT = CAT.CRIT:ORR:" ( PRODUCT.CATEGORY GE ":CATFR:" AND PRODUCT.CATEGORY LE ":CATTO:" )"
RETURN
*******************************
END
