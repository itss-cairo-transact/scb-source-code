* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.CUS.MINOR3(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.CUS = 'FBNK.CUSTOMER'
    F.CUS = ''
    R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    COMP = ID.COMPANY
    CUS.USR = R.USER<EB.USE.DEPARTMENT.CODE>
    ACCC=''
    TT = TODAY
    TY = TT[1,4]
    TM = TT[5,2]
    YM = TY - 21
    NM = TM
    XY = YM : NM : TT[7,2]
*   XY = YM : NM : "..
****    T.SEL = "SELECT ":FN.CUS:" WITH ACCOUNT.OFFICER EQ 32 AND CUSTOMER.STATUS EQ 22 AND BIRTH.INCORP.DATE EQ ":XY
    T.SEL = "SELECT ":FN.CUS:" WITH COMPANY.BOOK EQ ":COMP:" AND CUSTOMER.STATUS EQ 22 AND CONTACT.DATE EQ " :TT
****T.SEL = "SELECT ":FN.CUS:" WITH CO.CODE EQ ":COMP:" AND  CUSTOMER.STATUS EQ 22 AND BIRTH.INCORP.DATE EQ ":XY
****T.SEL="SELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ 32 AND CUSTOMER.STATUS EQ 22 AND BIRTH.INCORP.DATE EQ ":XY

    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS,K.LIST<I>,R.CUS,F.CUS,ERRR)
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = K.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = " NO DATA FOUND "
    END
    IF KK1 = 0 THEN  ENQ.ERROR = " NO DATA FOUND "

    RETURN
END
