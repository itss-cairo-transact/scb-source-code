* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.ONE.LIMIT.CUS(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
    COMP = ID.COMPANY

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LI = "FBNK.LIMIT"
    F.LI = ''
    R.LI=''
    Y.LI.ID=''
    Y.LI.ERR=''
    WS.POS.RES = 0
    K = 0
    WS.LIB.FLG = 0

    CALL OPF(FN.LI,F.LI)
*    SEL.CMD ="SSELECT " :FN.LI:" WITH COMPANY.BOOK EQ ":COMP:" AND MAXIMUM.TOTAL NE '' AND LIMIT.PRODUCT NE '' AND PRODUCT.ALLOWED EQ '' BY LIABILITY.NUMBER"
    SEL.CMD ="SSELECT " :FN.LI:" WITH COMPANY.BOOK EQ ":COMP:" AND LIMIT.PRODUCT LT 9000 AND LIMIT.PRODUCT NE 5700 AND LIMIT.PRODUCT NE 5740 BY LIABILITY.NUMBER"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN

        FOR I = 1 TO NOREC
            CALL F.READ(FN.LI,SELLIST<I>,R.LI,F.LI,E1)
            Y.LIB.NO = R.LI<LI.LIABILITY.NUMBER>
            IF Y.LIB.NO LE 99400000 THEN
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,Y.LIB.NO,WS.POS.RES)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.LIB.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
WS.POS.RES=R.ITSS.CUSTOMER<EB.CUS.POSTING.RESTRICT>
                IF WS.POS.RES LT 90 THEN
                    IF Y.LIB.NO NE WS.LIB.FLG THEN
                        K ++
                        ENQ.DATA<2,K> = "@ID"
                        ENQ.DATA<3,K> = "EQ"
                        ENQ.DATA<4,K> = SELLIST<I>
                    END
                END
                WS.LIB.FLG = Y.LIB.NO
            END ELSE
                ENQ.DATA<2,2> = "@ID"
                ENQ.DATA<3,2> = "EQ"
                ENQ.DATA<4,2> = "DUMMY"

            END
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
