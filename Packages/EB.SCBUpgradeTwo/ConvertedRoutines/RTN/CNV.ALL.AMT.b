* @ValidationCode : MjotNjM2MzYwODYxOkNwMTI1MjoxNjQxOTM0NDE2MTA3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 11 Jan 2022 12:53:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------NI7OOOOOOOOOOO------------------------------------------------------
SUBROUTINE CNV.ALL.AMT
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RETIREMENTS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPOSITS.LOANS.D

    FN.AC = "FBNK.ACCOUNT"
    F.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.DEP = "F.SCB.DEPOSITS.LOANS.D"
    F.DEP = ""
    CALL OPF(FN.DEP,F.DEP)

    TD = TODAY
    CALL CDT("",TD,'-1W')

    XX  = O.DATA
    XXX = FIELD(XX,'.',1)
    YY  = XXX:'.':TD


    CALL F.READ(FN.DEP,YY,R.DEP,F.DEP,E1)

    AMT1 = R.DEP<DEP.LOAN.DEPOSITS>
    AMT2 = R.DEP<DEP.LOAN.DEPOSITS.FCY>
    AMT3 = R.DEP<DEP.LOAN.LOANS>
    AMT4 = R.DEP<DEP.LOAN.LOANS.FCY>

    IF (AMT1 EQ 0 OR AMT1 EQ '') AND (AMT2 EQ 0 OR AMT2 EQ '')  AND (AMT3 EQ 0 OR AMT3 EQ '' ) THEN
        O.DATA = AMT4
    END
    IF (AMT1 EQ 0 OR AMT1 EQ '') AND (AMT2 EQ 0 OR AMT2 EQ '')  AND (AMT4 EQ 0 OR AMT4 EQ '' ) THEN
        O.DATA = AMT3
    END
    IF (AMT1 EQ 0 OR AMT1 EQ '') AND (AMT3 EQ 0 OR AMT3 EQ '') AND (AMT4 EQ 0 OR AMT4 EQ '' ) THEN
        O.DATA = AMT2
    END
    IF (AMT2 EQ 0 OR AMT2 EQ '') AND (AMT3 EQ 0 OR AMT3 EQ '') AND (AMT4 EQ 0 OR AMT4 EQ '') THEN
        O.DATA = AMT1
    END


RETURN
END
