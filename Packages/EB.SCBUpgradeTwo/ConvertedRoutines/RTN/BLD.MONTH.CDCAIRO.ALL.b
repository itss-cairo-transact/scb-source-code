* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.MONTH.CDCAIRO.ALL(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""


    TD = TODAY
    TM = TD[1,6]:'01'
    CALL CDT("",TM,'-1C')
    CALL LINE.DATE(TM)

*---------------------------------------------------------------------------------------------------------

*----------------------------------
    FN.CONT = 'F.RE.STAT.LINE.CONT'
    F.CONT = ''

    CALL OPF(FN.CONT,F.CONT)
*___________________________________________________________
    T.SEL2 = "SELECT FBNK.RE.STAT.LINE.CONT WITH @ID LIKE CDCAIRO..."

    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",NO.LINES,ER.SEL2)

    IF NO.LINES THEN
        FOR I = 1 TO NO.LINES
            PART1 = FIELD(KEY.LIST2<I>,'.',1,1)
            PART2 = FIELD(KEY.LIST2<I>,'.',2,1)
            PART3 = FIELD(KEY.LIST2<I>,'.',3,1)
***********************************
*****   CDCAIRO.0033.EG0010014
*****   CDCAIRO-0033-EGP-20100430*EG0010014
***********************************

            REP.ID = PART1:"-":PART2:"-EGP-":TM:"*":PART3
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = REP.ID
        NEXT I
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
END
