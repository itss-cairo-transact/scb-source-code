* @ValidationCode : MjotMTgxMTMwODIyMDpDcDEyNTI6MTY0MDcyNDI1MDU0NzpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
*-- CREATE BY NESSMA & AMIRA
*-----------------------------------------------------------------------------
SUBROUTINE BLD.PASS.RST(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PASSWORD.RESET
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.USR.LOCAL.REF
*-------------------------
    FN.PASS = "F.PASSWORD.RESET" ; F.PASS = ""
    CALL OPF(FN.PASS, F.PASS)
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF(FN.USR, F.USR)
*-------------------------
    DEP.CODE = "9936" ;  HH = "1"
    USER.SCB.DEPT.CODE=''
    SEL.CMD  = "SELECT " :FN.PASS
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,RTNCD)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.USR,KEY.LIST<II>,R.USR,F.USR,ERRR)
            IF R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE> EQ "9936" THEN
                ENQ.DATA<2,HH> = "@ID"
                ENQ.DATA<3,HH> = "EQ"
                ENQ.DATA<4,HH> = KEY.LIST<II>
                HH ++
            END
        NEXT II
    END ELSE
        ENQ.DATA<2,1> = "@ID"
        ENQ.DATA<3,1> = "EQ"
        ENQ.DATA<4,1> = "DUMMY"
    END
*-------------------------
RETURN
END
