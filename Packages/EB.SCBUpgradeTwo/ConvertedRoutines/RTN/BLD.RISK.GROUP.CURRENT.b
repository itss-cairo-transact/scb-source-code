* @ValidationCode : MjoxMDUwOTQ1MDY4OkNwMTI1MjoxNjQwNzI0MjUwNzMzOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-15</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLD.RISK.GROUP.CURRENT(ENQ.DATA)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] HASHING "$INCLUDE I_F.SCB.RISK.MAST" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.RISK.MAST
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
    CUS.ID.FLAG = 0

    FN.RS = "F.SCB.RISK.MAST"
    F.RS = ''
    R.RS=''
    Y.RS.ID=''
    Y.RS.ERR=''
    K = 0
    YTEXT = "Enter the Group No. : "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.GROUP.NO = COMI
*    YTEXT = "Enter the Year  : "
*    CALL TXTINP(YTEXT, 8, 22, "17", "A")
*    WS.YY = COMI
*WS.YY = 2010

    CALL OPF(FN.RS,F.RS)
*   SEL.CMD ="SELECT ":FN.RS:" WITH GROUP.CODE EQ ":WS.GROUP.NO:" AND SYSTEM.DATE LIKE ":WS.YY:"... BY CUSTOMER.ID BY SYSTEM.DATE"
    SEL.CMD ="SELECT ":FN.RS:" WITH GROUP.CODE EQ ":WS.GROUP.NO:" BY CUSTOMER.ID BY SYSTEM.DATE"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN
        FOR I = 1 TO NOREC
            WS.CUS.ID = FIELD(SELLIST<I>,"*",1)
            WS.DAT.ID = FIELD(SELLIST<I>,"*",2)

            IF CUS.ID.FLAG EQ 0 THEN
                CUS.ID.FLAG =  WS.CUS.ID
            END
            IF CUS.ID.FLAG NE WS.CUS.ID THEN
                K ++
                Y = (I - 1)
                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = SELLIST<Y>
            END
            CUS.ID.FLAG =  WS.CUS.ID
        NEXT I
        K ++
        ENQ.DATA<2,K> = "@ID"
        ENQ.DATA<3,K> = "EQ"
        ENQ.DATA<4,K> = SELLIST<I>

    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
RETURN
END
