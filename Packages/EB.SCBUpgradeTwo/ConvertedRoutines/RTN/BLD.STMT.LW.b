* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>97</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
***M.ELSAYED/2012-10-04**
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.STMT.LW(ENQ)
*    PROGRAM   BLD.STMT.POS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*    $INCLUDE T24.BP I_F.ACCT.ENT.LWORK.DAY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    TRNSS = " 825 205 207 208 209 217 226 401 380 381 "
*=============================================================*
    FN.TRNS  = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = '' ;  R.CUST = ''
    CALL OPF (FN.CUST,F.CUST)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF (FN.AC,F.AC)
    I = 0
*====================================================================*
*    T.SEL = "SELECT FBNK.ACCT.ENT.LWORK.DAY WITH @ID UL 994... AND CO.CODE EQ  ":COMP
    T.SEL = "SELECT FBNK.ACCT.ENT.LWORK.DAY WITH @ID UNLIKE 994... "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        LOOP
            REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
        WHILE ACCT.ID:POS

            CALL F.READ( FN.AC,ACCT.ID, R.AC,F.AC, E.AC)

            WS.AC.CUSTOMER = R.AC<AC.CUSTOMER>
            IF WS.AC.CUSTOMER NE '' THEN
                CALL F.READ( FN.TRNS,ACCT.ID, R.TRNS,F.TRNS, ETEXT)

                LOOP
                    REMOVE STMT.ID  FROM R.TRNS SETTING POS.LW
                WHILE STMT.ID:POS.LW

                    CALL F.READ( FN.STMT,STMT.ID, R.STMT,F.STMT, E.STMT)

                    CUS.ID   = R.STMT<AC.STE.CUSTOMER.ID>
                    V.DATE   = R.STMT<AC.STE.VALUE.DATE>
                    B.DATE   = R.STMT<AC.STE.BOOKING.DATE>
                    AMT.LCY  = R.STMT<AC.STE.AMOUNT.LCY>
                    TRNS.REF = R.STMT<AC.STE.TRANS.REFERENCE>[1,2]
                    REC.STATUS = R.STMT<AC.STE.RECORD.STATUS>
                    TRNS.CODE = R.STMT<AC.STE.TRANSACTION.CODE>

                    FINDSTR " ":TRNS.CODE:" " IN TRNSS SETTING POS.TR THEN GOTO NEXT1 ELSE
                        IF REC.STATUS NE 'REVE' THEN
                            IF ( AMT.LCY LT 0 ) AND ( V.DATE GT B.DATE ) THEN
                                I++
                                ENQ<2,I> = '@ID'
                                ENQ<3,I> = 'EQ'
                                ENQ<4,I> = STMT.ID
                            END
                        END
                    END
NEXT1:
                REPEAT
            END
        REPEAT
    END
    IF I = 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'NO RECORDS'
    END

    RETURN

*******************************************************************************************

END
