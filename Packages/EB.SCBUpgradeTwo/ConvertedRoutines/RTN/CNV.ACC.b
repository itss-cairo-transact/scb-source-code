* @ValidationCode : MjoxNzg4NzYyNTc1OkNwMTI1MjoxNjQwNzIzNDM3NTA1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:30:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.ACC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RETIREMENTS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1

    FN.SAMP = 'F.SCB.DEPT.SAMPLE1' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)

    XX = O.DATA
    CALL F.READ(FN.SAMP,O.DATA,R.SAMP,F.SAMP,E11)
    APP1 = R.SAMP<DEPT.SAMP.CUS.HWALA>
    APP2 = R.SAMP<DEPT.SAMP.CUS.LG11>
    APP3 = R.SAMP<DEPT.SAMP.CUS.LG22>
    APP4 = R.SAMP<DEPT.SAMP.CUS.TEL>
    APP5 = R.SAMP<DEPT.SAMP.CUS.TF1>
    APP6 = R.SAMP<DEPT.SAMP.CUS.TF2>
    APP7 = R.SAMP<DEPT.SAMP.CUS.TF3>
    APP8 = R.SAMP<DEPT.SAMP.CUS.BR>
    APP9 = R.SAMP<DEPT.SAMP.CUS.AC>
    APP10 = R.SAMP<DEPT.SAMP.CUS.WH>

    IF APP1 NE '' THEN
        O.DATA = APP1
    END
    IF APP2 NE '' THEN
        O.DATA = APP2
    END
    IF APP3 NE '' THEN
        O.DATA = APP3
    END
    IF APP4 NE '' THEN
        O.DATA = APP4
    END
    IF APP5 NE '' THEN
        O.DATA = APP5
    END
    IF APP6 NE '' THEN
        O.DATA = APP6
    END
    IF APP7 NE '' THEN
        O.DATA = APP7
    END
    IF APP8 NE '' THEN
        O.DATA = APP8
    END
    IF APP9 NE '' THEN
        O.DATA = APP9
    END
    IF APP10 NE '' THEN
        O.DATA = APP10
    END


RETURN
END
