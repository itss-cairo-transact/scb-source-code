* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE CHANGE.LOAN.DEP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

*-----------------------------------
    CALL "SBM.LOAN.CHANGE.200"
    CALL "SBM.LOAN.CHANGE.500"
    CALL "SBM.LOAN.CHANGE.600"
****    CALL "SBM.LOAN.GE.250"
    CALL "SBM.LOAN.650.Q"
    CALL "SBM.LOAN.650.P"

    CALL "SBM.DEP.CHANGE.210"
    CALL "SBM.DEP.CHANGE.220"
    CALL "SBM.DEP.CHANGE.230"
    CALL "SBM.DEP.CHANGE.250"
    CALL "SBM.DEP.CHANGE.650"
    CALL "SBM.DEP.CHANGE.700"
    CALL "SBM.DEP.CHANGE.8007"
*-----------------------------------


    RETURN
END
