* @ValidationCode : MjoxODIzNDk2NjIwOkNwMTI1MjoxNjQwNzI0MDQ1NzY5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:40:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*************************NI7OOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.APPROVE.NAU2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1

    FN.SAMP = 'F.SCB.DEPT.SAMPLE1$NAU' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)

    XX = O.DATA
    CALL F.READ(FN.SAMP,O.DATA,R.SAMP,F.SAMP,E11)
    APP1 = R.SAMP<DEPT.SAMP.REQ.STA.HWALA>
    APP2 = R.SAMP<DEPT.SAMP.REQ.STA.LG11>
    APP3 = R.SAMP<DEPT.SAMP.REQ.STA.LG22>
    APP4 = R.SAMP<DEPT.SAMP.REQ.STA.TEL>
    APP5 = R.SAMP<DEPT.SAMP.REQ.STA.TF1>
    APP6 = R.SAMP<DEPT.SAMP.REQ.STA.TF2>
    APP7 = R.SAMP<DEPT.SAMP.REQ.STA.TF3>
    APP8 = R.SAMP<DEPT.SAMP.REQ.STA.BR>
    APP9 = R.SAMP<DEPT.SAMP.REQ.STA.AC>

    IF (APP1 EQ 'APPROVED' OR APP1 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP1
    END
    IF (APP2 EQ 'APPROVED' OR APP2 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP2
    END
    IF (APP3 EQ 'APPROVED' OR APP3 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP3
    END
    IF (APP4 EQ 'APPROVED' OR APP4 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP4
    END
    IF (APP5 EQ 'APPROVED' OR APP5 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP5
    END
    IF (APP6 EQ 'APPROVED' OR APP6 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP6
    END
    IF (APP7 EQ 'APPROVED' OR APP7 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP7
    END
    IF (APP8 EQ 'APPROVED' OR APP8 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP8
    END
    IF (APP9 EQ 'APPROVED' OR APP9 EQ 'WAIT OF APPROVED') THEN
        O.DATA = APP9
    END

RETURN
END
