* @ValidationCode : MjoyMDQzMjU1ODE5OkNwMTI1MjoxNjQwNzIzMTM5MTk0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:25:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>559</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CHQISS.P.TXT.NEW
*    PROGRAM CHQISS.P.TXT.NEW
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHEQ.APPL

*****
    YTEXT  = 'Y/N �� ���� ����� �������  '
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG1 = COMI
    IF FLG1 = 'Y' OR FLG1 = 'y' THEN

        VIP = 0
        TXT.FILE = 'CHQ.NEW.TXT'

        YTEXT  = 'TO PRINT VIP PRESS V'
        CALL TXTINP(YTEXT, 8, 22, "1", "A")
        FLG2 = COMI
        IF FLG2 = 'V' OR FLG2 = 'v' THEN
            VIP = 1
            TXT.FILE = 'CHQ.VIP.TXT'
        END
        BR = ''
        BR = R.USER<EB.USE.DEPARTMENT.CODE>

        OPENSEQ "&SAVEDLISTS&" , TXT.FILE TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':TXT.FILE
            HUSH OFF
        END
        OPENSEQ "&SAVEDLISTS&" , TXT.FILE TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE CUST.SECT.ALL CREATED IN &SAVEDLISTS&'
            END
            ELSE
                STOP 'Cannot create CHQ.NEW.TXT File IN &SAVEDLISTS&'
            END
        END

        EOF = ''

        FN.CHQ  = 'F.SCB.CHEQ.APPL' ; F.CHQ = '' ; R.CHQ = ''

        T.SEL   = "SELECT F.SCB.CHEQ.APPL WITH TRN.DATE EQ '' AND CHARGE.DATE NE ''"
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL OPF( FN.CHQ,F.CHQ)
                CALL F.READ( FN.CHQ,KEY.LIST<I>, R.CHQ, F.CHQ, ETEXT)
                CUS.NO = R.CHQ<CHQA.CUST.NO>
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CUSTOMER':@FM:EB.CUS.TARGET,CUS.NO,W.TARGET)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
W.TARGET=R.ITSS.CUSTOMER<EB.CUS.TARGET>

                IF VIP AND (W.TARGET EQ 5800 OR W.TARGET EQ 5850 OR W.TARGET EQ 5860 OR W.TARGET EQ 5870 OR W.TARGET EQ 5900 ) THEN
**START
                    TOT.NAME = ''
                    TOT.NEME = R.CHQ<CHQA.CUST.NAME>:' ':R.CHQ<CHQA.CUST.NAME.2>
                    BB.DATA = "00"
                    BB.DATA := FMT(R.CHQ<CHQA.BRAN.NO>, "R%2")

                    BB.DATA := R.CHQ<CHQA.CUST.NO>
                    BB.DATA := '  '
                    IF R.CHQ<CHQA.ACC>[11,6] EQ '' THEN
                        BB.DATA := '      '
                    END
                    ELSE
                        BB.DATA := R.CHQ<CHQA.ACC>[11,6]
                    END
                    IF LEN(R.CHQ<CHQA.CUST.NO>) EQ 7 THEN
                        BB.DATA := '  '
                    END
                    IF LEN(R.CHQ<CHQA.CUST.NO>) EQ 8 THEN
                        BB.DATA := ' '
                    END

                    CHQ.SPACE = ''
                    CHQ.SPACE = STR(' ',100 - LEN(TOT.NEME))
                    BB.DATA := TOT.NEME
                    BB.DATA := CHQ.SPACE
                    BB.DATA := R.CHQ<CHQA.CHEQ.TYPE>
                    BB.DATA := FMT(R.CHQ<CHQA.APP.DATE>[5,2], "R%2")
                    BB.DATA := FMT(R.CHQ<CHQA.APP.DATE>[7,2], "R%2")
                    BB.DATA := R.CHQ<CHQA.APP.DATE>[1,4]
                    BB.DATA := FMT(R.CHQ<CHQA.NO.OF.BOOKS>,"R%2")
                    BB.DATA := R.CHQ<CHQA.CHEQ.KIND>
                    BB.DATA := KEY.LIST<I>

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                    R.CHQ<CHQA.TRN.DATE> = TODAY

                    CALL F.WRITE(FN.CHQ,KEY.LIST<I>,R.CHQ)
                    CALL JOURNAL.UPDATE(KEY.LIST<I>)
**END
                END
                IF VIP=0 AND (W.TARGET LT 5800 OR W.TARGET GT 5900 ) THEN
                    TOT.NAME = ''
                    TOT.NEME = R.CHQ<CHQA.CUST.NAME>:' ':R.CHQ<CHQA.CUST.NAME.2>
                    BB.DATA = "00"
                    BB.DATA := FMT(R.CHQ<CHQA.BRAN.NO>, "R%2")

                    BB.DATA := R.CHQ<CHQA.CUST.NO>
                    BB.DATA := '  '
                    IF R.CHQ<CHQA.ACC>[11,6] EQ '' THEN
                        BB.DATA := '      '
                    END
                    ELSE
                        BB.DATA := R.CHQ<CHQA.ACC>[11,6]
                    END
                    IF LEN(R.CHQ<CHQA.CUST.NO>) EQ 7 THEN
                        BB.DATA := '  '
                    END
                    IF LEN(R.CHQ<CHQA.CUST.NO>) EQ 8 THEN
                        BB.DATA := ' '
                    END

                    CHQ.SPACE = ''
                    CHQ.SPACE = STR(' ',100 - LEN(TOT.NEME))
                    BB.DATA := TOT.NEME
                    BB.DATA := CHQ.SPACE
                    BB.DATA := R.CHQ<CHQA.CHEQ.TYPE>
                    BB.DATA := FMT(R.CHQ<CHQA.APP.DATE>[5,2], "R%2")
                    BB.DATA := FMT(R.CHQ<CHQA.APP.DATE>[7,2], "R%2")
                    BB.DATA := R.CHQ<CHQA.APP.DATE>[1,4]
                    BB.DATA := FMT(R.CHQ<CHQA.NO.OF.BOOKS>,"R%2")
                    BB.DATA := R.CHQ<CHQA.CHEQ.KIND>
                    BB.DATA := KEY.LIST<I>

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                    R.CHQ<CHQA.TRN.DATE> = TODAY

                    CALL F.WRITE(FN.CHQ,KEY.LIST<I>,R.CHQ)
                    CALL JOURNAL.UPDATE(KEY.LIST<I>)
                END
            NEXT I
        END
        CLOSESEQ BB

        TEXT = '�� ����� �����' ; CALL REM
    END ELSE
        TEXT = '�� ��� ����� �������' ; CALL REM
    END
END
