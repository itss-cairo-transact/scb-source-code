* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>148</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.CUS.ACCT2(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.SCR = 'FBNK.STMT.ACCT.CR' ; F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.CAC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CAC = '' ; R.CAC = '' ; ER.CAC = ''
    CALL OPF(FN.CAC,F.CAC)
    TT = TODAY
    CALL ADD.MONTHS(TT,'-1')
    CALL LAST.DAY(TT)
    TTS = TT[1,6]
    ACCC=''
    KK1 = 0

    T.SEL = "SELECT ":FN.CAC:" WITH COMPANY.CO EQ ":COMP
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE CUS.ID FROM K.LIST SETTING POS.CUS
        WHILE CUS.ID:POS.CUS
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ERRR11)
            IF R.CUS<EB.CUS.SECTOR> NE 1100 AND R.CUS<EB.CUS.SECTOR> NE 1300 THEN
                CALL F.READ(FN.CAC,CUS.ID,R.CAC,F.CAC,ERRR)
                LOOP
                    REMOVE ACC.ID FROM R.CAC SETTING POS.CAC
                WHILE ACC.ID:POS.CAC
                    SCR.ID = ACC.ID:"-":TT
                    CALL F.READ(FN.SCR,SCR.ID,R.SCR,F.SCR,ERRR1)
                    IF R.SCR THEN
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = SCR.ID
                    END ELSE
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = "NOLIST"
                    END
                REPEAT
            END ELSE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        REPEAT
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
END
