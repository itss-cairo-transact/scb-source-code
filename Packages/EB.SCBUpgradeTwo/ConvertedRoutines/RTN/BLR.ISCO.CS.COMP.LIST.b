* @ValidationCode : MjoxODU5MzAzMzI5OkNwMTI1MjoxNjQwNzE5MDM0MjA2OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:17:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*********************************************
*************WAGDY.MOUNIR****10*09*2009******
*********************************************

SUBROUTINE BLR.ISCO.CS.COMP.LIST(ENQ)
*********************************************
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ISCO.CS
*********************************************
*CALL TXTINP("--WELCOME--",8,10,"5","A")
    COMP = ID.COMPANY
    TAB= 'FBNK.SCB.ISCO.CS'
    XTAB=""
    XCOMP= COMP[2]
****
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    F.ACC    = ""
    R.ACC    = ""
    E11      = ""
****
**** CONVERTING THE XCOMP ****
    IF XCOMP LT 10 THEN
        XCOMP = XCOMP[1]
    END
******************************
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END

    T.DATE     = YEAR:NEW.MONTH
*********************************************
    CALL OPF(TAB,XTAB)
    T.SEL = "SELECT F.SCB.ISCO.CS WITH @ID LIKE ...":T.DATE:"... AND DATA.PROV.BRANCH.ID EQ ":XCOMP : " BY DATA.PROV.IDENT.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = T.SEL; CALL REM
    Z=1
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(TAB,KEY.LIST<I>,R.ACC, F.ACC ,E11)

            NEW.ID = R.ACC<ISCO.CS.DATA.PROV.IDENT.CODE>
            IF NEW.ID NE OLD.ID OR I EQ 1 THEN
**********************
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = KEY.LIST<I>
**********************
                Z=Z+1
            END
            OLD.ID = NEW.ID
        NEXT I
    END ELSE
        ENQ.ERROR = "... NO RECORDS FOUND ..."
    END
RETURN
END
***********************************************************
