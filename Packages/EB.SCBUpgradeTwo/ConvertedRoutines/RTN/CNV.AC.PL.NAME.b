* @ValidationCode : MjotMjE0MTExNjQ2MDpDcDEyNTI6MTY0MDY2MjMzNjYxMjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 19:32:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
**************MAHMOUD 17/11/2011************************
SUBROUTINE CNV.AC.PL.NAME

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    XXX1   = ''
    XXX1   = O.DATA
*Line [ 36 ] Initialize "AC.ACCOUNT.TITLE" - ITSS - R21 Upgrade - 2021-12-23
    AC.ACCOUNT.TITLE=''
    IF XXX1[1,2] EQ 'PL' THEN
        XXX1   = FIELD(XXX1,'PL',2)
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XXX1,CC.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XXX1,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CC.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
    END ELSE
        FN.CUS = "FBNK.CUSTOMER" ; F.CUS = '' ; R.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,XXX1,AC.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,XXX1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
        IF AC.CUST NE '' THEN
            CALL F.READ(FN.CUS,AC.CUST,R.CUS,F.CUS,E.RRR)
            CUS.LCL  = R.CUS<EB.CUS.LOCAL.REF>
            CUS.NAME1= CUS.LCL<1,CULR.ARABIC.NAME>
            CUS.NAME2= CUS.LCL<1,CULR.ARABIC.NAME.2>
            CC.NAME = CUS.NAME1:" ":CUS.NAME2
        END ELSE
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE,XXX1,CC.NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,XXX1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CC.NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE>
        END
    END
    O.DATA = CC.NAME
RETURN
END
