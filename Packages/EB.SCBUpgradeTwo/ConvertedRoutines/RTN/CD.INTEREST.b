* @ValidationCode : MjoxNzU5MDg1NjI2OkNwMTI1MjoxNjQwNzIyMTc3MzU5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:09:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CD.INTEREST(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CD.TYPES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS


    RAT = R.NEW(LD.INTEREST.RATE)
    CD.TYP = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CD.TYPES':@FM:CD.TYPES.DESCRIPTION,CD.TYP,DESC)
F.ITSS.SCB.CD.TYPES = 'F.SCB.CD.TYPES'
FN.F.ITSS.SCB.CD.TYPES = ''
CALL OPF(F.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES)
CALL F.READ(F.ITSS.SCB.CD.TYPES,CD.TYP,R.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES,ERROR.SCB.CD.TYPES)
DESC=R.ITSS.SCB.CD.TYPES<CD.TYPES.DESCRIPTION>
    TYP = FIELD(DESC,'-',2)
    ARG = RAT:" ":"%":"����":" ":TYP


RETURN
END
