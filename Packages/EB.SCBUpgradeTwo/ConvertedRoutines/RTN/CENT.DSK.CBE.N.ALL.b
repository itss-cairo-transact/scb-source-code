* @ValidationCode : MjoxNzM2NDE0OTgzOkNwMTI1MjoxNjQwNzI0MjUzNjI0OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE CENT.DSK.CBE.N.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] HASHING "$INCLUDE I_F.SCB.CREDIT.CBE.NEW" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CREDIT.CBE.NEW
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*------------------------------------------------------
    COMP = ID.COMPANY
    TD1 = TODAY
    TD2 = TD1[1,6]
    TD3 = TD2:"01"
    CALL CDT ('',TD3,'-1C')
    TD4 = TD3[1,4]
    TD5 = TD3[5,2]
    TD = TD5 : TD4
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010001" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.cairo" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.cairo"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.cairo" TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE scb.cairo CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.cairo File IN /home/acct/user/oper1'
            END
        END
        BRN = '0101'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010006" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.abor" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.abor"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.abor" TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE scb.abor CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.abor File IN /home/acct/user/oper1'
            END
        END
        BRN = '0213'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010013" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.grdn" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.grdn"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.grdn" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.grdn CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.grdn File IN /home/acct/user/oper1'
            END
        END
        BRN = '0178'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010021" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.burg" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.burg"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.burg" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.burg CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.burg File IN /home/acct/user/oper1'
            END
        END
        BRN = '1053'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010023" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.amry" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.amry"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.amry" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.amry CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.amry File IN /home/acct/user/oper1'
            END
        END
        BRN = '0252'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010032" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.mansor" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.mansor"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.mansor" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.mansor CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.mansor File IN /home/acct/user/oper1'
            END
        END
        BRN = '0506'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010070" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.ramadan" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.ramadan"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.ramadan" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.ramadan CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.ramadan File IN /home/acct/user/oper1'
            END
        END
        BRN = '4514'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010007" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.orouba" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.orouba"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.orouba" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.orouba CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.orouba File IN /home/acct/user/oper1'
            END
        END
        BRN = '0196'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010002" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.heliop" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.cairo"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.heliop" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.heliop CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.heliop File IN /home/acct/user/oper1'
            END
        END
        BRN = '0147'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010005" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.maadi" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.maadi"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.maadi" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.maadi CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.maadi File IN /home/acct/user/oper1'
            END
        END
        BRN = '0180'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010020" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.alex" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.cairo"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.alex" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.alex CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.alex File IN /home/acct/user/oper1'
            END
        END
        BRN = '1001'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010040" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.ismail" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.ismail"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.ismail" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.ismail CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.ismail File IN /home/acct/user/oper1'
            END
        END
        BRN = '1601'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010060" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.tanta" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.tanta"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.tanta" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.tanta CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.tanta File IN /home/acct/user/oper1'
            END
        END
        BRN = '3001'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010004" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.mohand" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.mohand"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.mohand" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.mohand CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.mohand File IN /home/acct/user/oper1'
            END
        END
        BRN = '5515'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010014" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.toctb" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.toctb"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.toctb" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.toctb CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.toctb File IN /home/acct/user/oper1'
            END
        END
        BRN = '5531'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010015" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.octb" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.octb"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.octb" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.octb CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.octb File IN /home/acct/user/oper1'
            END
        END
        BRN = '5557'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010010" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.nasr" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.nasr"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.nasr" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.nasr CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.nasr File IN /home/acct/user/oper1'
            END
        END
        BRN = '0143'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010009" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.fostat" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.fostat"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.fostat" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.fostat CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.fostat File IN /home/acct/user/oper1'
            END
        END
        BRN = '0246'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010022" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.smoh" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.smoh"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.smoh" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.smoh CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.smoh File IN /home/acct/user/oper1'
            END
        END
        BRN = '1029'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010030" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.said" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.said"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.said" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.said CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.said File IN /home/acct/user/oper1'
            END
        END
        BRN = '1501'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010050" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.suez" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.suez"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.suez" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.suez CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.suez File IN /home/acct/user/oper1'
            END
        END
        BRN = '1801'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010090" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.sadat" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.sadat"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.sadat" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.sadat CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.sadat File IN /home/acct/user/oper1'
            END
        END
        BRN = '3514'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010003" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.giza" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.giza"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.giza" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.giza CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.giza File IN /home/acct/user/oper1'
            END
        END
        BRN = '5501'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010011" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.dokki" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.dokki"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.dokki" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.dokki CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.dokki File IN /home/acct/user/oper1'
            END
        END
        BRN = '5524'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010012" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.sphinx" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.sphinx"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.sphinx" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.sphinxo CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.sphinx File IN /home/acct/user/oper1'
            END
        END
        BRN = '5537'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010035" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.sharm" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.sharm"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.sharm" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.sharm CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.sharm File IN /home/acct/user/oper1'
            END
        END
        BRN = '9952'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010080" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.minya" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.minya"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.minya" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.minya CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.minya File IN /home/acct/user/oper1'
            END
        END
        BRN = '0508'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010081" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.asyut" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.asyut"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.asyut" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.asyut CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.asyut File IN /home/acct/user/oper1'
            END
        END
        BRN = '0507'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010031" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.domyat" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.domyat"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.domyat" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.domyat CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.domyat File IN /home/acct/user/oper1'
            END
        END
        BRN = '0117'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010051" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.sokhna" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.sokhna"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.sokhna" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.sokhna CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.sokhna File IN /home/acct/user/oper1'
            END
        END
        BRN = '0107'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010016" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.shobra" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.shobra"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.shobra" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.shobra CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.shobra File IN /home/acct/user/oper1'
            END
        END
        BRN = '0102'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010017" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.elmanial" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.elmanial"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.elmanial" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.elmanial CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.elmanial File IN /home/acct/user/oper1'
            END
        END
        BRN = '0108'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010019" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.eltagamoa" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.eltagamoa"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.eltagamoa" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.eltagamoa CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.eltagamoa File IN /home/acct/user/oper1'
            END
        END
        BRN = '0103'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010052" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.prttwfik" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.prttwfik"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.prttwfik" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.prttwfik CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.prttwfik File IN /home/acct/user/oper1'
            END
        END
        BRN = '0110'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010053" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.sokhecon" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.sokhecon"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.sokhecon" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.sokhecon CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.sokhecon File IN /home/acct/user/oper1'
            END
        END
        BRN = '0510'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010018" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.ahrar" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.ahrar"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.ahrar" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.ahrar CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.ahrar File IN /home/acct/user/oper1'
            END
        END
        BRN = '0109'
    END

*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010024" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.topninety" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.topninety"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.topninety" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.topninety CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.topninety File IN /home/acct/user/oper1'
            END
        END
        BRN = '0513'
    END

*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010025" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.madinty" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.madinty"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.madinty" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.madinty CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.madinty File IN /home/acct/user/oper1'
            END
        END
        BRN = '0511'
    END

*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010041" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.zayed" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.zayed"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.zayed" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.zayed CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.zayed File IN /home/acct/user/oper1'
            END
        END
        BRN = '0522'
    END
*---------------------------------------------------------------------------------

    IF COMP EQ "EG0010026" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.makram" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.makram"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.makram" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.makram CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.makram File IN /home/acct/user/oper1'
            END
        END
        BRN = '0514'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010027" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.hosary" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.hosary"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.hosary" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.hosary CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.hosary File IN /home/acct/user/oper1'
            END
        END
        BRN = '0512'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010029" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.mglsdwla" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.mglsdwla"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.mglsdwla" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.mglsdwla CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.mglsdwla File IN /home/acct/user/oper1'
            END
        END
        BRN = '0515'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010033" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.gleem" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.gleem"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.gleem" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.gleem CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.gleem File IN /home/acct/user/oper1'
            END
        END
        BRN = '0516'
    END
*---------------------------------------------------------------------------------

    IF COMP EQ "EG0010083" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.benisuef" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.benisuef"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.benisuef" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.benisuef CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.benisuef File IN /home/acct/user/oper1'
            END
        END
        BRN = '0517'
    END
*---------------------------------------------------------------------------------

    IF COMP EQ "EG0010034" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.roshdy" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.roshdy"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.roshdy" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.roshdy CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.roshdy File IN /home/acct/user/oper1'
            END
        END
        BRN = '0521'
    END
*---------------------------------------------------------------------------------

    IF COMP EQ "EG0010082" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.qena" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.qena"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.qena" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.qena CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.qena File IN /home/acct/user/oper1'
            END
        END
        BRN = '0518'
    END
*---------------------------------------------------------------------------------

    IF COMP EQ "EG0010036" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.citystars" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.citystars"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.citystars" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.citystars CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.citystars File IN /home/acct/user/oper1'
            END
        END
        BRN = '0520'
    END
*---------------------------------------------------------------------------------
    IF COMP EQ "EG0010045" THEN
        OPENSEQ "/home/acct/user/oper1" , "scb.smash" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"/home/acct/user/oper1":' ':"scb.smash"
            HUSH OFF
        END
        OPENSEQ "/home/acct/user/oper1" , "scb.smash" TO BB ELSE

            CREATE BB THEN
                PRINT 'FILE scb.smash CREATED IN &SAVEDLISTS&'
            END ELSE
                STOP 'Cannot create scb.smash File IN /home/acct/user/oper1'
            END
        END
        BRN = '0523'
    END
*---------------------------------------------------------------------------------
    GROS.TOT.CR = 0
    GROS.TOT.US = 0

    GOSUB CHK.REC

    IF FLG = 0 THEN
        GOSUB INIT0
        GOSUB GETPL
        TEXT = '�� ����� �����' ; CALL REM
    END ELSE
        TEXT = '���� ���� ���� ��� ���� - �� ��� ����� �����' ; CALL REM
    END


RETURN
*====================================================
CHK.REC:
*------
    FN.CBE = 'F.SCB.CREDIT.CBE.NEW' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    T.SEL  = "SELECT F.SCB.CREDIT.CBE.NEW WITH GE30.FLAG EQ 'Y' AND CO.CODE EQ ":COMP:" AND CBE.NO EQ '999999999999' BY CBE.NO"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FLG = 1
    END ELSE
        FLG = 0
    END


*====================================================
INIT0:
*----
    HORZ.TOT.CR = 0
    HORZ.TOT.US = 0

    TOT.CR.1  = 0
    TOT.CR.2  = 0
    TOT.CR.3  = 0
    TOT.CR.4  = 0
    TOT.CR.5  = 0
    TOT.CR.6  = 0
    TOT.CR.7  = 0
    TOT.CR.8  = 0
    TOT.CR.9  = 0
    TOT.CR.10 = 0
    TOT.CR.11 = 0
    TOT.CR.12 = 0
    TOT.CR.13 = 0

    TOT.US.1  = 0
    TOT.US.2  = 0
    TOT.US.3  = 0
    TOT.US.4  = 0
    TOT.US.5  = 0
    TOT.US.6  = 0
    TOT.US.7  = 0
    TOT.US.8  = 0
    TOT.US.9  = 0
    TOT.US.10 = 0
    TOT.US.11 = 0
    TOT.US.12 = 0
    TOT.US.13 = 0


RETURN
*----------------------------------------------------------------
**********************************
GETPL:
*--------
    FN.SCB.CRDT = 'F.SCB.CREDIT.CBE.NEW'
    F.SCB.CRDT = ''
    R.SCB.CRDT = ''


    CCC.NO = '' ; LEDG.NO = ''

    CALL OPF(FN.SCB.CRDT,F.SCB.CRDT)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

* DAT.ID = R.USER<EB.USE.COMPANY.CODE,1>
* BNK.DATE1 = TODAY
* CALL CDT("",BNK.DATE1,'-1C')
* WORK.DATE = BNK.DATE1

    T.SEL  = "SELECT F.SCB.CREDIT.CBE.NEW WITH GE30.FLAG EQ 'Y' AND CO.CODE EQ ":COMP:" BY CBE.NO"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR REC_NO = 1 TO SELECTED
            CALL F.READ(FN.SCB.CRDT,KEY.LIST<REC_NO>,R.SCB.CRDT,F.SCB.CRDT,E3)
            IF  REC_NO = 1 THEN
                GOSUB HEAD_REC
            END
            GOSUB BUILDREC
            IF REC_NO = SELECTED THEN
                GOSUB TAIL_REC
            END
        NEXT REC_NO
    END
RETURN
*****************************************************************************
HEAD_REC:
    BB.DATA = ''
    BB.DATA = '1700'
    BB.DATA := TD   ;*R.SCB.CRDT<SCB.C.CBE.BNK.DATE>[5,2]
*    BB.DATA := R.SCB.CRDT<SCB.C.CBE.BNK.DATE>[1,4]
    BB.DATA := STR("0",287)

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
**************************************************************************
TAIL_REC:
    BB.DATA = ''
    BB.DATA := BRN
    BB.DATA := '099999999997'
    BB.DATA := FMT(TOT.CR.1 ,"R%10")
    BB.DATA := FMT(TOT.US.1 ,"R%10")
    BB.DATA := FMT(TOT.CR.2 ,"R%10")
    BB.DATA := FMT(TOT.US.2 ,"R%10")
    BB.DATA := FMT(TOT.CR.3 ,"R%10")
    BB.DATA := FMT(TOT.US.3 ,"R%10")
    BB.DATA := FMT(TOT.CR.4 ,"R%10")
    BB.DATA := FMT(TOT.US.4 ,"R%10")
    BB.DATA := FMT(TOT.CR.5 ,"R%10")
    BB.DATA := FMT(TOT.US.5 ,"R%10")
    BB.DATA := FMT(TOT.CR.6 ,"R%10")
    BB.DATA := FMT(TOT.US.6 ,"R%10")
    BB.DATA := FMT(TOT.CR.7 ,"R%10")
    BB.DATA := FMT(TOT.US.7 ,"R%10")
    BB.DATA := FMT(TOT.CR.8 ,"R%10")
    BB.DATA := FMT(TOT.US.8 ,"R%10")
    BB.DATA := FMT(TOT.CR.9 ,"R%10")
    BB.DATA := FMT(TOT.US.9 ,"R%10")
    BB.DATA := FMT(TOT.CR.10 ,"R%10")
    BB.DATA := FMT(TOT.US.10 ,"R%10")
    BB.DATA := FMT(TOT.CR.11 ,"R%10")
    BB.DATA := FMT(TOT.US.11 ,"R%10")
    BB.DATA := FMT(TOT.CR.12 ,"R%10")
    BB.DATA := FMT(TOT.US.12 ,"R%10")

    BB.DATA := FMT(GROS.TOT.CR ,"R%10")
    BB.DATA := FMT(GROS.TOT.US ,"R%10")

    BB.DATA := FMT(TOT.CR.13 ,"R%10")
    BB.DATA := FMT(TOT.US.13 ,"R%10")
    BB.DATA := '0'

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
**************************************************************************
BUILDREC:
*-------------

    BB.DATA = ''
    BB.DATA := BRN
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.CBE.NO>   ,"R%12")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.1> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.1> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.2> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.2> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.3> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.3> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.4> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.4> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.5> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.5> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.6> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.6> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.7> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.7> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.8> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.8> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.9> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.9> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.10> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.10> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.11> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.11> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.12> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.12> ,"R%10")

    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.99> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.99> ,"R%10")

    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.CR.13> ,"R%10")
    BB.DATA := FMT(R.SCB.CRDT<SCB.C.CBE.TOT.US.13> ,"R%10")
    BB.DATA := '0'

    TOT.US.1 = TOT.US.1 + R.SCB.CRDT<SCB.C.CBE.TOT.US.1>
    TOT.CR.1 = TOT.CR.1 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.1>

    TOT.US.2 = TOT.US.2 + R.SCB.CRDT<SCB.C.CBE.TOT.US.2>
    TOT.CR.2 = TOT.CR.2 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.2>

    TOT.US.3 = TOT.US.3 + R.SCB.CRDT<SCB.C.CBE.TOT.US.3>
    TOT.CR.3 = TOT.CR.3 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.3>

    TOT.US.4 = TOT.US.4 + R.SCB.CRDT<SCB.C.CBE.TOT.US.4>
    TOT.CR.4 = TOT.CR.4 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.4>


    TOT.US.5 = TOT.US.5 + R.SCB.CRDT<SCB.C.CBE.TOT.US.5>
    TOT.CR.5 = TOT.CR.5 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.5>

    TOT.US.6 = TOT.US.6 + R.SCB.CRDT<SCB.C.CBE.TOT.US.6>
    TOT.CR.6 = TOT.CR.6 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.6>

    TOT.US.7 = TOT.US.7 + R.SCB.CRDT<SCB.C.CBE.TOT.US.7>
    TOT.CR.7 = TOT.CR.7 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.7>

    TOT.US.8 = TOT.US.8 + R.SCB.CRDT<SCB.C.CBE.TOT.US.8>
    TOT.CR.8 = TOT.CR.8 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.8>

    TOT.US.9 = TOT.US.9 + R.SCB.CRDT<SCB.C.CBE.TOT.US.9>
    TOT.CR.9 = TOT.CR.9 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.9>

    TOT.US.10 = TOT.US.10 + R.SCB.CRDT<SCB.C.CBE.TOT.US.10>
    TOT.CR.10 = TOT.CR.10 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.10>

    TOT.US.11 = TOT.US.11 + R.SCB.CRDT<SCB.C.CBE.TOT.US.11>
    TOT.CR.11 = TOT.CR.11 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.11>

    TOT.US.12 = TOT.US.12 + R.SCB.CRDT<SCB.C.CBE.TOT.US.12>
    TOT.CR.12 = TOT.CR.12 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.12>

    TOT.US.13 = TOT.US.13 + R.SCB.CRDT<SCB.C.CBE.TOT.US.13>
    TOT.CR.13 = TOT.CR.13 + R.SCB.CRDT<SCB.C.CBE.TOT.CR.13>

    GROS.TOT.CR += R.SCB.CRDT<SCB.C.CBE.TOT.CR.99>
    GROS.TOT.US += R.SCB.CRDT<SCB.C.CBE.TOT.US.99>

*----------------------------------------------------
*PRINT BB.DATA

    WRITESEQ BB.DATA TO BB ELSE

        PRINT " ERROR WRITE FILE "
    END
*----------------------------------------------------
RETURN
******************************************************
END
