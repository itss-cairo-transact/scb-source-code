* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>479</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CAL.BAL.FMT
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB

    EXECUTE "COMO ON CAL.BAL.FMT_":TODAY:"_":TIME()
    GOSUB INIT
    GOSUB PROCESS
    EXECUTE "COMO OFF CAL.BAL.FMT_":TODAY:"_":TIME()
    RETURN

INIT:

    FN.ACC = 'F.ACCOUNT'
    FV.ACC = ''
    FN.ECB = 'F.EB.CONTRACT.BALANCES'
    FV.ECB = ''
    FN.CAL = 'F.CONSOLIDATE.ASST.LIAB'
    FV.CAL = ''

    CAL.ID = ''
    SEL.CMD = ''
    SEL.LIST = ''
    NO.OF.REC = ''
    NO.OF.CAL = ''
    YERR = ''
    R.CAL = ''
    BAL.REC = ''
    NO.OF.BAL = ''
    ENTRY = ''
    CREDIT.BAL = ''
    NO.OF.CR.BAL = ''
    DEBIT.BAL = ''
    NO.OF.DR.BAL = ''
    SEL.CAL = ''
    CAL.ERR = ''
    ACC.ID = ''

    CALL OPF(FN.ACC,FV.ACC)
    CALL OPF(FN.ECB,FV.ECB)
    CALL OPF(FN.CAL,FV.CAL)

    RETURN

PROCESS:
*   SEL.CAL = "GET.LIST ACC.LIST.CORR"
*   CALL EB.READLIST(SEL.CAL,SEL.LIST,'',NO.OF.CAL,CAL.ERR)
        OPEN '&SAVEDLISTS&' TO F.SL ELSE CRT 'Unable to open Savedlists directory'
        READ SEL.LIST FROM F.SL, "ACC.LIST.CORR" ELSE CRT 'Savedlists file ACC.LIST.CORR not found'
    LOOP
        ACC.ID = "" ; ACC.POS = ""
        REMOVE ACC.ID FROM SEL.LIST SETTING ACC.POS
    WHILE ACC.ID:ACC.POS
        R.ECB = ""
        READ R.ECB FROM FV.ECB,ACC.ID ELSE R.ECB = ""
        YID.CAL = ""
        YID.CAL = R.ECB<ECB.CONSOL.KEY>
        CRT "Processing CAL record ":YID.CAL
        CAL.ID = ""
        CAL.ID = YID.CAL

        READ R.CAL FROM FV.CAL, CAL.ID ELSE R.CAL = ''
        CCNT = DCOUNT(R.CAL<RE.ASL.TYPE>, @VM)

        WRITE.FLAG = 0

        FOR JJ = 1 TO CCNT
            ASST.TYPE = R.CAL<RE.ASL.TYPE, JJ>
            CCY = R.CAL<RE.ASL.CURRENCY>
            IF R.CAL<RE.ASL.BALANCE, JJ> NE '' THEN
                CAL.BAL = R.CAL<RE.ASL.BALANCE, JJ>
                CRT "BEFORE FORMAT: ":CAL.BAL
                CALL EB.ROUND.AMOUNT("CCY", CAL.BAL, "", "")
                CRT "AFTER FORMAT: ":CAL.BAL
                IF CAL.BAL NE R.CAL<RE.ASL.BALANCE, JJ> THEN
                    R.CAL<RE.ASL.BALANCE, JJ> = CAL.BAL
                    WRITE.FLAG = 1
                END
            END
            IF R.CAL<RE.ASL.DEBIT.MOVEMENT, JJ> NE '' THEN
                CAL.BAL = R.CAL<RE.ASL.DEBIT.MOVEMENT, JJ>
                CRT "BEFORE FORMAT: ":CAL.BAL
                CALL EB.ROUND.AMOUNT("CCY", CAL.BAL, "", "")
                CRT "AFTER FORMAT: ":CAL.BAL
                IF CAL.BAL NE R.CAL<RE.ASL.DEBIT.MOVEMENT, JJ> THEN
                    R.CAL<RE.ASL.DEBIT.MOVEMENT, JJ> = CAL.BAL
                    WRITE.FLAG = 1
                END
            END
            IF R.CAL<RE.ASL.CREDIT.MOVEMENT, JJ> NE '' THEN
                CAL.BAL = R.CAL<RE.ASL.CREDIT.MOVEMENT, JJ>
                CRT "BEFORE FORMAT: ":CAL.BAL
                CALL EB.ROUND.AMOUNT("CCY", CAL.BAL, "", "")
                CRT "AFTER FORMAT: ":CAL.BAL
                IF CAL.BAL NE R.CAL<RE.ASL.CREDIT.MOVEMENT, JJ> THEN
                    R.CAL<RE.ASL.CREDIT.MOVEMENT, JJ> = CAL.BAL
                    WRITE.FLAG = 1
                END
            END
        NEXT JJ

        IF R.CAL AND WRITE.FLAG THEN
            WRITE R.CAL TO FV.CAL, CAL.ID
            CRT 'Rounding cal balance for ':CAL.ID
        END

        CRT "*************************************"

    REPEAT
    RETURN
END
