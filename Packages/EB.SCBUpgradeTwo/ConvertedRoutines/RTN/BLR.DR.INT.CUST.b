* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.DR.INT.CUST(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ACC = 'FBNK.STMT.ACCT.DR'
    F.ACC = ''
    R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    TTD  = TODAY
    LAST.MON = TODAY[1,6]:'01'
    CALL CDT("",LAST.MON,'-1C')
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',TTLD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TTLD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TTMX = "...-":TTLD[1,6]:"..."
    KK1 = 0
    T.SEL  = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ":TTMX
    T.SEL := " BY @ID "
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        ACC.ID = ''
        FOR I = 1 TO SELECTED
            KK1 += 1
            CATEG = ''
            ACC.ID = FIELD(K.LIST<I>,'-',1)
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.ID,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CO.CODE ,ACC.ID,ACC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
            IF ACC.COM EQ COMP THEN
                IF (CATEG GE 1100 AND CATEG LE 1219) OR (CATEG GE 1231 AND CATEG LE 1599) THEN
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = K.LIST<I>
                END ELSE
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = "NOLIST"
                END
            END ELSE
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        NEXT I
    END ELSE
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
END
