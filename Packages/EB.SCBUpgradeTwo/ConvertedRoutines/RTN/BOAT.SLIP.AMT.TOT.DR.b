* @ValidationCode : MjotMTk2NzE2ODI4OkNwMTI1MjoxNjQ1MTI0NjYyNjYzOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 17 Feb 2022 11:04:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE BOAT.SLIP.AMT.TOT.DR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*========================================================================
INITIATE:
    REPORT.ID='BOAT.SLIP.AMT.TOT.DR'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    AMT = 0
RETURN
*========================================================================
PROCESS:
*---------------------
    BOT.ID = ID.NEW
    CUR.ID = R.NEW(BOT.CURRENCY)

    IF R.NEW(BOT.BOAT.CODE) NE '50' THEN
        IF COMP EQ 'EG0010002' THEN
            IF CUR.ID EQ 'EGP' THEN
                DEBIT.AC = '0230025410100101'
            END
            IF CUR.ID EQ 'USD' THEN
                DEBIT.AC = '0230025420100101'
            END
        END
        IF COMP EQ 'EG0010020' THEN
            IF CUR.ID EQ 'EGP' THEN
                DEBIT.AC = '2030074610100101'
            END
            IF CUR.ID EQ 'USD' THEN
                DEBIT.AC = '2030074620100101'
            END
        END
        CUST.NAME = '���� ������� �������'
    END ELSE
        DEBIT.AC = '2020097510100101'
        CUST.NAME = '���� ���� �������'
    END

    CATEG.ID   = DEBIT.AC[11,4]
*Line [ 83 ] UPDATE "CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)" TO BE "CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)" - ITSS - R21 Upgrade - 2021-12-23
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG=R.ITSS.CATEGORY<@FM:EB.CAT.DESCRIPTION,2>
*Line [ 85 ] UPDATE "* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)" TO BE "CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)" - ITSS - R21 Upgrade - 2021-12-23
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.(CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<@FM:EB.CUR.CCY.NAME,2>
*Line [ 87 ] UPDATE " CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)" TO BE " CALL DBR (('COMPANY':@FM:EB.COM.COMPANY.NAME),COMP,BRANCH)" - ITSS - R21 Upgrade - 2021-12-23
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('COMPANY':@FM:EB.COM.COMPANY.NAME),COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DAMT = DCOUNT(R.NEW(BOT.TOTAL.AMOUNT),@VM)
    FOR I = 1 TO DAMT
        AMT += R.NEW(BOT.TOTAL.AMOUNT)<1,I>
    NEXT I
    AMOUNT     = AMT

    IN.AMOUNT  = AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT    = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    INPUTTER   = R.NEW(BOT.INPUTTER)
    INP        = FIELD(INPUTTER,'_',2)
    AUTH       = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 121 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
    F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
    FN.F.ITSS.USER.SIGN.ON.NAME = ''
    CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
    CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
    AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>


    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
*--------------------------------------------------------------
    XX<1,1>[3,35]   = CUST.NAME

    XX4<1,1>[45,15]  = '������     : '
    XX4<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = DEBIT.AC

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX5<1,1>[3,35]  = '������ ������� : ':OUT.AMT
    XX6<1,1>[3,35]  = '���������       : '

    XX7<1,1>[1,15]  = '������ : ':INP
    XX7<1,1>[30,15] = '��� ������� : ':BOT.ID
    XX7<1,1>[60,15] = '������ : ':AUTHI
*-------------------------------------------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":"BOAT.SLIP.AMT.TOT.DR"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":BRANCH
    PR.HD :="'L'":"������ ������ "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
