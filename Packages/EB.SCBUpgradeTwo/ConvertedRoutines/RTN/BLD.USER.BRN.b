* @ValidationCode : MjotNzkxMTcxNTEzOkNwMTI1MjoxNjQwNzM4ODI3OTc1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:47:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*--- CREATED BY NESSMA ---*
SUBROUTINE BLD.USER.BRN(ENQ.DATA)

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_ENQUIRY.COMMON
    $INSERT  I_USER.ENV.COMMON
    $INSERT  I_F.USER
**-----------------------------------------------
    FN.USR = "F.USER"      ; F.USR   = ""
    CALL OPF(FN.USR,F.USR)
**-----------------------------------------------
    DEPT.CODE = R.USER<EB.USE.DEPARTMENT.CODE>
    LOCATE "SCB.DEPT.CODE" IN ENQ.DATA<2,1> SETTING CAT.POS THEN
        SCB.DEPT  = ENQ.DATA<4,CAT.POS>
    END

    SEL.CMD  = "SELECT ":FN.USR:" WITH @ID LIKE SCB..."
    SEL.CMD := " AND @ID NE SCB.CAIRO22"
    SEL.CMD := " AND @ID NE SCB.CAIRO8"
    SEL.CMD := " AND @ID UNLIKE SCB.OFS..."
    SEL.CMD := " AND @ID UNLIKE ...AUTO..."
    SEL.CMD := " AND @ID UNLIKE ...ATM..."
    SEL.CMD := " AND @ID UNLIKE ...EIB..."
    SEL.CMD := " AND @ID UNLIKE ...VISA..."
    SEL.CMD := " AND @ID UNLIKE ...INP..."
    SEL.CMD := " AND @ID UNLIKE ...AUTH..."
    SEL.CMD := " AND @ID UNLIKE ...PATCH..."
    SEL.CMD := " AND @ID UNLIKE ...TEM..."
    SEL.CMD := " AND DEPARTMENT.CODE EQ " : DEPT.CODE
    IF SCB.DEPT THEN
        SEL.CMD := " AND SCB.DEPT.CODE EQ "   : SCB.DEPT
    END
    SEL.CMD := " BY END.DATE.PROFILE"
    SEL.CMD := " BY INIT.APPLICATION"
    SEL.CMD := " BY SCB.DEPT.CODE"
    SEL.CMD := " BY @ID"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    TEXT = NOREC ; CALL REM

    IF NOREC THEN
        FOR I = 1 TO NOREC
            ENQ.DATA<2,I> = "@ID"
            ENQ.DATA<3,I> = "EQ"
            ENQ.DATA<4,I> = SELLIST<I>
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
*-------------------------------------------------
RETURN
END
