* @ValidationCode : MjoxMDAxOTY2MDQyOkNwMTI1MjoxNjQwNzI0MjUxMzM2OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>58</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLR.DR.CAT.LW(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.RANGE
*Line [ 35 ] HASHING "$INCLUDE I_F.SCB.TRANS.TODAY" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.TRN = 'F.SCB.TRANS.TODAY' ; F.TRN = '' ; R.TRN = ''
    CALL OPF(FN.TRN,F.TRN)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ENT = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.ENT = '' ; R.ENT = ''
    CALL OPF(FN.ENT,F.ENT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.LN  = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.RG  = 'F.RE.STAT.RANGE' ; F.RG = '' ; R.RG = ''
    CALL OPF(FN.RG,F.RG)

    TT = TODAY
    CALL CDT("",TT,'-1W')
    KK1 = 0
    MM  = 0
    CATEG = ''
    CAT.CRIT = ''
    XX = ''
    T.SEL = ''
    LN.ID = 'GENLED.0100'
    GOSUB GETCATEG
    LN.ID = 'GENLED.0285'
    GOSUB GETCATEG

**** UPDATED BY MAHMOUD 27/12/2012 ***********

*    T.SEL  = "SELECT ":FN.ACC:" WITH CO.CODE EQ ":COMP
*    T.SEL := " AND ( ":CAT.CRIT
*    T.SEL := " OR  ( CATEGORY IN ( ":CATEG:" )))"
*    IF SELECTED THEN
*        LOOP
*            REMOVE ACC.ID FROM K.LIST SETTING POS.AC
*        WHILE ACC.ID:POS.AC
*            CALL F.READ(FN.ENT,ACC.ID,R.ENT,F.ENT,ER.ENT)
*            LOOP
*                REMOVE STE.ID FROM R.ENT SETTING POS.ENT
*            WHILE STE.ID:POS.ENT
*                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
*                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
*                IF STE.AMT LT 0 THEN
*                    KK1++
*                    ENQ<2,KK1> = "@ID"
*                    ENQ<3,KK1> = "EQ"
*                    ENQ<4,KK1> = STE.ID
*                END ELSE
*                    KK1++
*                    ENQ<2,KK1> = "@ID"
*                    ENQ<3,KK1> = "EQ"
*                    ENQ<4,KK1> = "NOLIST"
*                END
*            REPEAT
*        REPEAT
*        KK1++
*        ENQ<2,KK1> = "@ID"
*        ENQ<3,KK1> = "EQ"
*        ENQ<4,KK1> = "NOLIST"
*    END ELSE
*        KK1++
*        ENQ<2,KK1> = "@ID"
*        ENQ<3,KK1> = "EQ"
*        ENQ<4,KK1> = "NOLIST"
*    END

    T.SEL  = "SELECT ":FN.TRN:" WITH BOOKING.DATE EQ ":TT:" AND COMPANY.CO EQ ":COMP
    T.SEL := " AND ACCOUNT.NUMBER NE '' AND AMOUNT.LCY LT 0"
    T.SEL := " AND ( ":CAT.CRIT
    T.SEL := " OR   ( PRODUCT.CATEGORY IN ( ":CATEG:" )))"

    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            KK1++
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = K.LIST<I>
        NEXT I
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END

***** END OF UPDATE ***********************

RETURN

***********************************************
GETCATEG:
*********
    CALL F.READ(FN.LN,LN.ID,R.LN,F.LN,ERR.LN)
*Line [ 135 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    PROD.NO = DCOUNT(R.LN<RE.SRL.ASSET1>,@VM)
    FOR XX = 1 TO PROD.NO
        PRODUCT = R.LN<RE.SRL.ASSET1,XX>
        IF PRODUCT[1,1] EQ '*' THEN
            MM++
            GOSUB GETRANGE
        END ELSE
            CATEG = CATEG:" ":R.LN<RE.SRL.ASSET1,XX>
        END
    NEXT XX
RETURN
***********************************************
GETRANGE:
********
    SS = LEN(PRODUCT) - 1
    RG.ID = PRODUCT[2,SS]
    CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
    CATFR = R.RG<RE.RNG.START.RANGE>
    CATTO = R.RG<RE.RNG.END.RANGE>
    IF MM EQ 1 THEN
        ORR = ''
    END ELSE
        ORR = ' OR '
    END
    CAT.CRIT = CAT.CRIT:ORR:" ( CATEGORY GE ":CATFR:" AND CATEGORY LE ":CATTO:" )"
RETURN
*******************************
END
