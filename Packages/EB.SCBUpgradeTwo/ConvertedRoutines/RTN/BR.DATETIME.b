* @ValidationCode : MjotNTA2NTk1Mjk3OkNwMTI1MjoxNjQwNzI0MjUyNjg0OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BR.DATETIME(NEWDAT)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] HASHING "$INCLUDE I_F.SCB.BR.SLIPS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.BR.SLIPS

*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format


*    CALL DBR('SCB.BR.SLIPS':@FM:SCB.BS.DATE.TIME,NEWDAT,DATE.TIME)
    DATE.TIME = R.NEW(SCB.BS.DATE.TIME)
    DATE.TIME = DATE.TIME<1,1>
    DAT       = DATE.TIME[1,6]
    DATU      = FMT(DAT,"R##/##/##")
    NEWDAT    = DATU

RETURN
END
