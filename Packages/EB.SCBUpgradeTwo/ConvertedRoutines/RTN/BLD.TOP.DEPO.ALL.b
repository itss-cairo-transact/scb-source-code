* @ValidationCode : MjotMTkwNDg1MjYxMDpDcDEyNTI6MTY0MDcyNDI1MDg0NDpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>90</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLD.TOP.DEPO.ALL(ENQ.DATA)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] HASHING $INCLUDE "I_F.SCB.TOPCUS.CR.TOT.LW" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TOPCUS.CR.TOT.LW
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    IF COMP # 'EG0010099' THEN
        WS.CO.CODE = " AND CO.CODE EQ ":COMP
    END ELSE
        WS.CO.CODE = ''
    END
    FN.TOP.TOT = "F.SCB.TOPCUS.CR.TOT.LW"
    F.TOP.TOT = ''
    R.SCB.TOPCUS.CR.TOT=''
    Y.TOP.TOT.ID=''
    Y.TOP.TOT.ERR=''
    K = 0

    CALL OPF(FN.TOP.TOT,F.TOP.TOT)
*MSABRY
    SEL.CMD ="SSELECT " :FN.TOP.TOT:" WITH CCY.CUS EQ 'ALL' ":WS.CO.CODE:" AND TOT.DEPO NE 0 BY TOT.DEPO "
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN
        FOR I = NOREC TO 1 STEP -1
            K ++
            ENQ.DATA<2,K> = "@ID"
            ENQ.DATA<3,K> = "EQ"
            ENQ.DATA<4,K> = SELLIST<I>
            IF K = 25 THEN I = 1
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END

RETURN
END
