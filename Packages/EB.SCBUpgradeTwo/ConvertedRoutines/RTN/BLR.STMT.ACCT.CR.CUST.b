* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******************MAHMOUD 18/10/2011********************
    SUBROUTINE BLR.STMT.ACCT.CR.CUST(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.CR' ; F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

*    CUS.ID = '80200058'
*    TT = '20110811'

    YTEXT = "Enter the Customer No. : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    CUS.ID = COMI
    IF NOT(CUS.ID) THEN
        GOSUB NODATA
        RETURN
    END
    YTEXT = "Enter End of Month Date YYYYMMDD :"
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    TT = COMI
    IF NOT(TT) THEN
        GOSUB NODATA
        RETURN
    END

    IF LEN(TT) NE 8 THEN
        GOSUB NODATA
        RETURN
    END

    RR = TT
    KK1 = 1

    CALL LAST.DAY(TT)
    IF RR NE TT THEN
        CALL ADD.MONTHS(TT,'-1')
    END

    ENQ<2,1> = "@ID"
    ENQ<3,1> = "EQ"
    ENQ<4,1> = TT

    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ERR1)
    LOOP
        REMOVE ACC.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
    WHILE ACC.ID:POS.CUS.ACC
        SCR.ID = ACC.ID:"-":TT
        CALL F.READ(FN.SCR,SCR.ID,R.SCR,F.SCR,ERR2)
*        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,ACC.ID,AC.COM)
*        IF AC.COM EQ COMP THEN
        IF NOT(ERR2) THEN
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = SCR.ID
        END ELSE
            KK1++
            GOSUB NODATA
        END
    REPEAT
    RETURN
********************************************
NODATA:
*------
    ENQ<2,KK1> = "@ID"
    ENQ<3,KK1> = "EQ"
    ENQ<4,KK1> = "NOLIST"
    RETURN
********************************************
END
