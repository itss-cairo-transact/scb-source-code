* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BUILD.CONCAT.FILE(BL.ID)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    BR.REC = ''

    CALL F.READ('F.LD.LOANS.AND.DEPOSITS',BL.ID,BR.REC,F.LD.LOANS.AND.DEPOSITS,ERR)

    CONCAT.CO.CODE = BR.REC<240>
    CONCAT.NEW.KEY = BR.REC<167,42>
    CONCAT.REC = CONCAT.CO.CODE:'*':BL.ID
    CALL F.READ('F.LD.LOANS.AND.DEPOSITS.OLD.NO',CONCAT.NEW.KEY,LD.OLD.NO.REC,F.LD.LOANS.AND.DEPOSITS.OLD.NO,ERR)
    IF LD.OLD.NO.REC = '' THEN
        CALL F.WRITE('F.LD.LOANS.AND.DEPOSITS.OLD.NO',CONCAT.NEW.KEY,CONCAT.REC)
    END

    RETURN
END
