* @ValidationCode : MjotNDU0NDA3NTczOkNwMTI1MjoxNjQxNDM4MjY4OTk0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 05 Jan 2022 19:04:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******************MAHMOUD 13/3/2013********************
*-----------------------------------------------------------------------------
* <Rating>777</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLR.CRNT.ACC.MIN(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHARGE.EXP
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = '' ; ERR.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ERR.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACT = '' ; R.ACT = '' ; ERR.ACT = ''
    CALL OPF(FN.ACT,F.ACT)
    SELECTED = '' ; K.LIST = ''

    TDD1 = TODAY
    EXC.RATE    = 1
    KK1 = 0
    CALL ADD.MONTHS(TDD1,'-1')
    USD.CUR.CODE = "USD"
*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,USD.CUR.CODE,USD.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,USD.CUR.CODE,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
USD.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    USD.RATE = USD.RATE<1,1>
*##########################
    TDY1 = TODAY
    MIN.LCY.AMT = 2000
    MIN.USD.AMT = 500
    IF TDY1 LT '20130701' THEN
        START.DATE  = '20130225'
    END ELSE
        START.DATE  = '19600101'
    END
*##########################

    EXP.CUST = " 40300486 40300075 13300197 13300292 13300122 13300006 13300244 13300310 306306 601010 10001000 "

    A.SEL  = "SELECT ":FN.ACC:" WITH CO.CODE EQ ":COMP
    A.SEL := " AND CATEGORY EQ 1001 "
    A.SEL := " AND OPENING.DATE GT ":START.DATE

    CALL EB.READLIST(A.SEL,K.LIST,"",SELECTED,ERR.ACC)
    IF SELECTED THEN
        LOOP
            REMOVE ACC.ID FROM K.LIST SETTING POS.ACC
        WHILE ACC.ID:POS.ACC
            CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ERRR)
            ACC.CUR  = R.ACC<AC.CURRENCY>
            ACC.OPN  = R.ACC<AC.OPENING.DATE>
            ACC.CUS  = R.ACC<AC.CUSTOMER>
            ACC.BAL  = R.ACC<AC.ONLINE.ACTUAL.BAL>
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.CONTACT.DATE,ACC.CUS,CUS.OPN.DATE)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ACC.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.OPN.DATE=R.ITSS.CUSTOMER<EB.CUS.CONTACT.DATE>
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,ACC.CUS,CUS.SECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ACC.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,ACC.CUS,CUS.LCL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ACC.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LCL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
            CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
*Line [ 115 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.CHARGE.EXP':@FM:'CH.EXP.MIN.BAL.CHARGE',ACC.CUS,CUS.EXP)
F.ITSS.SCB.CHARGE.EXP = 'F.SCB.CHARGE.EXP'
FN.F.ITSS.SCB.CHARGE.EXP = ''
CALL OPF(F.ITSS.SCB.CHARGE.EXP,FN.F.ITSS.SCB.CHARGE.EXP)
CALL F.READ(F.ITSS.SCB.CHARGE.EXP,ACC.CUS,R.ITSS.SCB.CHARGE.EXP,FN.F.ITSS.SCB.CHARGE.EXP,ERROR.SCB.CHARGE.EXP)
CUS.EXP=R.ITSS.SCB.CHARGE.EXP<'CH.EXP.MIN.BAL.CHARGE'>
        

            IF CUS.EXP EQ 'YES' THEN NULL ELSE
**##            FINDSTR ACC.CUS:' ' IN EXP.CUST SETTING EX.CU THEN NULL ELSE
*Line [ 95 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                IF CRD.STA NE '' OR CRD.COD EQ '110' OR CRD.COD EQ '120' THEN NULL ELSE
                    IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN
                        IF CUS.OPN.DATE GT START.DATE THEN
                            GOSUB GET.CCY.MIN
                            ACT.ID = ACC.ID:"-":TDD1[1,6]
                            CALL F.READ(FN.ACT,ACT.ID,R.ACT,F.ACT,ERR1)
                            IF NOT(R.ACT) THEN
                                GOSUB GET.ACT.ID
                            END
                            ACT.MIN.BAL  = MINIMUM(R.ACT<IC.ACT.BALANCE>)
                            IF ACT.MIN.BAL EQ '' THEN ACT.MIN.BAL = ACC.BAL
                            IF ACT.MIN.BAL LT MIN.BAL.AMT THEN
*Line [ 108 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                CNT.ACT = DCOUNT(R.ACT<IC.ACT.BALANCE>,@VM)
                                FOR AA1 = 1 TO CNT.ACT
                                    ACT.BAL.DATE = R.ACT<IC.ACT.DAY.NO,AA1>
                                    ACT.BAL.AMT  = R.ACT<IC.ACT.BALANCE,AA1>
                                    IF ACT.BAL.AMT LT MIN.BAL.AMT THEN
                                        MLT.LOC = AA1
                                        AA1 = CNT.ACT
                                    END
                                NEXT AA1
                                ACT.MIN.BAL = ACT.BAL.AMT
                                ACT.DAY.NO  = ACT.BAL.DATE

                                KK1++
                                ENQ<2,KK1> = "@ID"
                                ENQ<3,KK1> = "EQ"
                                ENQ<4,KK1> = ACT.ID
                            END ELSE
                                KK1++
                                ENQ<2,KK1> = "@ID"
                                ENQ<3,KK1> = "EQ"
                                ENQ<4,KK1> = "NOLIST"
                            END
                        END ELSE
                            KK1++
                            ENQ<2,KK1> = "@ID"
                            ENQ<3,KK1> = "EQ"
                            ENQ<4,KK1> = "NOLIST"
                        END
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = "NOLIST"
                    END
                END
            END
        REPEAT
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
RETURN
****************************************************
GET.CCY.MIN:
*-----------
    IF ACC.CUR EQ 'EGP' THEN
        MIN.BAL.AMT = MIN.LCY.AMT
    END ELSE
        IF ACC.CUR EQ 'USD' THEN
            MIN.BAL.AMT = MIN.USD.AMT
        END ELSE
            CALL F.READ(FN.CUR,ACC.CUR,R.CUR,F.CUR,ERR.CUR)
            CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
            IF ACC.CUR EQ 'JPY' THEN
                CUR.RATE = CUR.RATE /100
            END
            IF CUR.RATE NE 0 AND CUR.RATE NE '' THEN
                EXC.RATE = USD.RATE / CUR.RATE
            END
            MIN.BAL.AMT = MIN.USD.AMT * EXC.RATE
        END
    END

** Note for me :
** Lookup CURRENCY.USD.RATE.FCY

RETURN
****************************************************
GET.ACT.ID:
*---------
    TDDL = TDD1
    CALL MONTHS.BETWEEN(ACC.OPN,TDD1,MONTHS.NO)
    FOR MM1 = 1 TO MONTHS.NO
        CALL ADD.MONTHS(TDDL,'-1')
        ACT.ID = ACC.ID:"-":TDDL[1,6]
        CALL F.READ(FN.ACT,ACT.ID,R.ACT,F.ACT,ERRR1)
        IF NOT(ERRR1) THEN
            MM1 = MONTHS.NO
        END
    NEXT MM1
RETURN
****************************************************
RETURN
END
