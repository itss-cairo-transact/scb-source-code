* @ValidationCode : MjoxMTkzMTMxNjE3OkNwMTI1MjoxNjQwNzI0MjUyNzc1OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:44:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BR.TIMEDATE(TIMU)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] HASHING "$INCLUDE I_F.SCB.BR.SLIPS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.BR.SLIPS

*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

*    CALL DBR('SCB.BR.SLIPS':@FM:SCB.BS.DATE.TIME,TIMU,DATE.TIME)
    DATE.TIME = R.NEW(SCB.BS.DATE.TIME)
    DATE.TIME = TIMU<1,1>
    TIM       = DATE.TIME[7,4]
    TIMHS     = FMT(TIM,"R##:##")
    HH        = TIMHS[4,2]
    SS        = TIMHS[1,2]
    TIMU      = HH:":":SS

RETURN
END
