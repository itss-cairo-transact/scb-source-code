* @ValidationCode : MjotMTY5MTI5NDQyNjpDcDEyNTI6MTY0NTEyNDUyMTYzNzpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 11:02:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
SUBROUTINE BOAT.PAY.ORDER

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BOAT.CUSTOMER
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*========================================================================
INITIATE:
    REPORT.ID='BOAT.PAY.ORDER'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP  = ID.COMPANY
    AC.BR = COMP[2]
RETURN
*========================================================================
PROCESS:
*---------------------
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    BOAT.NAME = R.NEW(BO.BOAT.NAME)
    AMT       = R.NEW(BO.AMOUNT)
    CUR.ID    = R.NEW(BO.CURRENCY)
    CUST.NAME = R.NEW(BO.BF.NAME)
    INPUTTER  = R.NEW(BO.INPUTTER)
    INP       = FIELD(INPUTTER,'_',2)
    AUTH      = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
    F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
    FN.F.ITSS.USER.SIGN.ON.NAME = ''
    CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
    CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
    AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
    IF CUR.ID EQ 'EGP' THEN
        WS.COMM = 10
    END

    IF CUR.ID EQ 'USD' THEN
        WS.COMM = 2
    END

    IDD       = ID.NEW

    IF R.NEW(BO.BOAT.CODE) NE '50' THEN
        COM.NAME = "���� ������� �������"
        IF CUR.ID EQ 'EGP' THEN
            ACCT.DR = 'EGP16188000300':AC.BR
        END
        IF CUR.ID EQ 'USD' THEN
            ACCT.DR = 'USD16188000300':AC.BR
        END
    END ELSE
        COM.NAME = '���� ���� �������'
        ACCT.DR = 'EGP1618800040020'
    END
*Line [ 83 ] UPDATE "* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)" TO BE "CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)" - ITSS - R21 Upgrade - 2021-12-23
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<@FM:EB.CUR.CCY.NAME,2>
* CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    IN.AMOUNT  = AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT    = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10 = SPACE(132)  ; XX41 = SPACE(132)
    XX11 = SPACE(132)  ; XX12 = SPACE(132)
    XX13 = SPACE(132)

    XX1<1,1>[3,35]   = "����� �� ��� ���� ������ ��� / ":BRANCH
    XX2<1,1>[3,35]   = "���� ��� ��� ������ / ":COM.NAME
    XX3<1,1>[3,35]   = "��� ������ : ":BOAT.NAME
    XX4<1,1>[3,35]   = "���� : ":AMT:"   ":CUR
    XX4<1,1>[45,35]  = "���� ����� : ":ACCT.DR
    XX41<1,1>[3,35]  = "���  : ":OUT.AMT
    XX13<1,1>[3,55]  = '�� ��� ����� ����� : ':WS.COMM:"   ":CUR
    XX5<1,1>[3,35]   = "����� : ":CUST.NAME
    XX6<1,1>[3,35]   = "��� �����/���� ��� : "
    XX7<1,1>[3,35]   = "������� : "
    XX8<1,1>[60,35]  = "������� : "
    XX9<1,1>[40,35]  = " ��� ����� ������ �� ������ : "
    XX11<1,1>[1,15]  = "������ : ":INP
    XX11<1,1>[30,15] = "��� ������� : ":IDD
    XX11<1,1>[60,15] = "������ : ":AUTHI
    XX<1,1>[60,35]   = " ........ / ........ "
    XX10<1,1>[60,35] = "���� ��������  /  ������"

*-------------------------------------------------------------------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":"BOAT.PAY.ORDER"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":BRANCH
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"��� ��� ����� ����"
    PR.HD :="'L'":SPACE(40):"--------------------"
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT STR(' ',80)
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',80)
    PRINT XX4<1,1>
    PRINT XX41<1,1>
    PRINT XX13<1,1>
    PRINT STR(' ',80)
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT STR(' ',80)
    PRINT XX7<1,1>
    PRINT STR(' ',80)
    PRINT XX8<1,1>
    PRINT STR(' ',80)
    PRINT XX9<1,1>
    PRINT STR('=',80)
    PRINT XX11<1,1>
    PRINT STR(' ',80)
    PRINT XX<1,1>
    PRINT STR(' ',80)
    PRINT XX10<1,1>

*===============================================================
RETURN
END
