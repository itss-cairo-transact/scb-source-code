* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-------------------------------NI7OOOOOOOOO----------------------------------------------
    SUBROUTINE CD.RENEW(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    FN.CUS='FBNK.LD.LOANS.AND.DEPOSITS'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
TD = TODAY
   ** TD   = '20101017'
***CALL CDT('', TD, '+30C')

    T.SEL="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH RENEW.OR.NO NE ''"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
*TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            CALL F.READ(FN.CUS,KEY.LIST<ENQ.LP>,R.CUS,F.CUS,E2)
            MATDATE = R.CUS<LD.FIN.MAT.DATE>
            CALL CDT('', MATDATE, '-3W')
            TEXT = "MAT : " : MATDATE ; CALL REM
            TEXT = "TODAY : " : TD ; CALL REM
            IF MATDATE EQ TD THEN
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
            END ELSE
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = 'DUMMY'
            END
        NEXT ENQ.LP
    END ELSE
        ENQ<2,ENQ.LP> = '@ID'
        ENQ<3,ENQ.LP> = 'EQ'
        ENQ<4,ENQ.LP> = 'DUMMY'

    END

**********************
    RETURN
END
