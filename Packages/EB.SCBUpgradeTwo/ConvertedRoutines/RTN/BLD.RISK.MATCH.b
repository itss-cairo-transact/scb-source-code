* @ValidationCode : MjoxMTgxNTMzNzY3OkNwMTI1MjoxNjQwNzM4ODkxMTA2OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:48:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLD.RISK.MATCH(ENQ.DATA)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] HASHING "$INSERT TEMENOS.BP I_F.SCB.CREDIT.CBE.NEW" - ITSS - R21 Upgrade - 2021-12-23
* $INSERT  I_F.SCB.CREDIT.CBE.NEW
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.CR.CBE = "F.SCB.CREDIT.CBE.NEW"
    F.CR.CBE = ''
    R.CR.CBE=''
    Y.TOP.TOT.ID=''
    Y.TOP.TOT.ERR=''
    K = 0

    CALL OPF(FN.CR.CBE,F.CR.CBE)
    SEL.CMD ="SSELECT " :FN.CR.CBE:" WITH RISK.RATE NE RESERVED9 AND GE30.FLAG EQ 'Y' AND NEW.SECTOR NE '' AND NEW.SECTOR NE 4650 "
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC  THEN
        FOR I = 1 TO NOREC
            K ++
            ENQ.DATA<2,K> = "@ID"
            ENQ.DATA<3,K> = "EQ"
            ENQ.DATA<4,K> = SELLIST<I>
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
RETURN
END
