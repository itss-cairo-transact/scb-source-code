* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.LD.OLD(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    TD = TODAY[3,6]:"..."
    TDD = TODAY

    ***TD = '100811':'...'
    ***TDD = '20100811'

***    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21001 AND CATEGORY LE 21010) AND STATUS NE 'LIQ' AND INPUTTER UNLIKE ...OFS... AND DATE.TIME LIKE ":TD:" AND VALUE.DATE LT ":TDD:" AND CO.CODE EQ ":COMP
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21001 AND CATEGORY LE 21015) AND RENEW.IND NE 'NO' AND INPUTTER UNLIKE ...OFS... AND DATE.TIME LIKE ":TD:" AND VALUE.DATE LT ":TDD:" AND CO.CODE EQ ":COMP
    ****   T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21001 AND CATEGORY LE 21010) AND RENEW.IND NE 'NO' AND INPUTTER UNLIKE ...OFS... AND DATE.TIME LIKE ...090527... AND VALUE.DATE EQ 20090523 AND CO.CODE EQ 'EG0010001'"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*=================================================================
    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
TEXT = SELECTED ; CALL REM
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,KK> = '@ID'
        ENQ<3,KK> = 'EQ'
        ENQ<4,KK> = 'DUMMY'
    END
TEXT = "END "  ; CALL REM
    RETURN
END
