* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/06/15 ****
********************************************
    SUBROUTINE BLD.RISK.7.RNG.BNK(ENQ.DATA)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY
    CUS.ID.FLAG = 0
    FN.RS = "F.SCB.RISK.MAST"
    F.RS = ''
    R.RS=''
    Y.RS.ID=''
    Y.RS.ERR=''
    K = 0
    LOCATE "SYSTEM.DATE" IN ENQ.DATA<2,1> SETTING YSYSTEM.DATE THEN WS.SYSTEM.DATE = ENQ.DATA<4,YSYSTEM.DATE>
    WS.FROM.DATE = FIELD(WS.SYSTEM.DATE," ",1)
    WS.TO.DATE = FIELD(WS.SYSTEM.DATE," ",2)

    CALL OPF(FN.RS,F.RS)
    SEL.CMD ="SELECT ":FN.RS:" BY CUS.BR BY CUSTOMER.ID BY SYSTEM.DATE"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN
        FOR I = 1 TO NOREC
            WS.CUS.ID = FIELD(SELLIST<I>,"*",1)

            IF CUS.ID.FLAG EQ 0 THEN
                CUS.ID.FLAG =  WS.CUS.ID
            END
            IF CUS.ID.FLAG NE WS.CUS.ID THEN
                GOSUB CHK.SUB
            END
            CUS.ID.FLAG =  WS.CUS.ID
        NEXT I
        GOSUB CHK.SUB
    END
    IF K EQ 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
***************************************************************************************
CHK.SUB:
    WS.RISK.RATE = 0
    WS.Y.RISK = 99
    IF I NE NOREC THEN
        Y = (I - 1)
    END ELSE
        Y = I
    END

    WS.CUS.Y = FIELD(SELLIST<Y>,"*",1)
    WS.DAT.Y = FIELD(SELLIST<Y>,"*",2)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.Y,LOCAL.REF)
    WS.RISK.RATE = LOCAL.REF<1,CULR.RISK.RATE>
    IF WS.RISK.RATE EQ 7 THEN
        IF (WS.DAT.Y GE WS.FROM.DATE) AND (WS.DAT.Y LE WS.TO.DATE) THEN
            Y = (Y - 1)
            WS.CU.Y2 = FIELD(SELLIST<Y>,"*",1)
            IF CUS.ID.FLAG NE WS.CU.Y2 THEN
                K++
                Y++
                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = SELLIST<Y>
                RETURN
            END
            CALL F.READ(FN.RS,SELLIST<Y>,R.RS,F.RS,ER.RS)
            WS.Y.RISK = DROUND(R.RS<RM.TOT.CUS>,'0')
            IF WS.Y.RISK # 7 THEN
                K++
                Y++
                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = SELLIST<Y>
            END
        END
    END
    RETURN
***************************************************************************************
END
