* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*    SUBROUTINE CBE.DEPOSIT.ACCOUNT.PRINT
    PROGRAM CBE.DEPOSIT.ACCOUNT.PRINT

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT TEMENOS.BP I_F.SCB.DEPOSITS.AND.ACCOUNTS

    REPORT.DATE = ''

    FN.DEP.AND.ACC = 'F.SCB.DEPOSITS.AND.ACCOUNTS' ; F.DEP.AND.ACC = ''
    R.DEP.ACC = '' ; DEP.ACC.SEL = '' ; KEY.LIST = '' ; KEY.SELECTED = '' ; KEY.ERR = ''
    CALL OPF( FN.DEP.AND.ACC , F.DEP.AND.ACC )

    APPL.TYPE.ID = '' ; APPL.CUR = '' ; APPL.ISJOINT = '' ; APPL.OPENING.DATE = ''
    APPL.NAT.ID = '' ; APPL.SEC.NAT.ID = '' ; APPL.SEC.NAT.TYPE = '' ; APPL.CLOSE.DATE = ''
    APPL.STATUS.ID = '' ; APPL.STATUS.REASON = '' ; APPL.BRANCH.ID = '' ; APPL.REC.ID = ''
    APPL.LOADING = '' ; APPL.DATE.UPDATE = ''
    FOLDER = '&SAVEDLISTS&' ; FILENAME = 'CBE.DEPOSIT.ACCOUNTS.2.txt' ; BB = '' ; BB.DATA = ''

    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    GOSUB HEADER_PRINT
    GOSUB PROCESS
    CLOSE BB
    RETURN


PROCESS:

    YTEXT = "Enter Month date like(201901)"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    REPORT.DATE = COMI

    YTEXT = "Enter (1) for First load or (2) for Second load or any char for ALL data"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    REPORT.TYPE = COMI
    DEP.ACC.SEL = 'SELECT ':FN.DEP.AND.ACC:' WITH DATE.UPDATE LIKE "':REPORT.DATE:'..."'
    IF REPORT.TYPE EQ '1' THEN
        DEP.ACC.SEL := ' AND LOADING EQ "First_Load"'
    END
    ELSE IF REPORT.TYPE EQ '2' THEN
        DEP.ACC.SEL := ' AND LOADING EQ "Second_Load"'
    END
    CALL EB.READLIST(DEP.ACC.SEL,KEY.LIST,'',KEY.SELECTED,KEY.ERR)
    CRT 'SELECTED : ':KEY.SELECTED
    FOR I=1 TO KEY.SELECTED
        CALL F.READ(FN.DEP.AND.ACC,KEY.LIST<I>,R.DEP.ACC,F.DEP.AND.ACC,ACC.ERR)
        APPL.REC.ID = KEY.LIST<I>
        APPL.TYPE.ID = R.DEP.ACC<CBE.ACC.TYPE.ID>
        APPL.CUR = R.DEP.ACC<CBE.ACC.CUR.ID>
        APPL.BRANCH.ID = R.DEP.ACC<CBE.ACC.BRANCH.ID>
        APPL.ISJOINT = R.DEP.ACC<CBE.ACC.IS.JOINT>
        APPL.OPENING.DATE = R.DEP.ACC<CBE.ACC.OPENING.DATE>
        APPL.NAT.ID = R.DEP.ACC<CBE.ACC.NATION.ID>
        APPL.SEC.NAT.ID = R.DEP.ACC<CBE.ACC.SECONDARY.ID>
        APPL.SEC.NAT.TYPE = R.DEP.ACC<CBE.ACC.SECONDARY.TYPE>
        APPL.CLOSE.DATE = R.DEP.ACC<CBE.ACC.CLOSING.DATE>
        APPL.STATUS.ID =  R.DEP.ACC<CBE.ACC.STATUS.ID>
        APPL.STATUS.REASON = R.DEP.ACC<CBE.ACC.STATUS.REASON>
        APPL.LOADING = R.DEP.ACC<CBE.ACC.LOADING>
        APPL.DATE.UPDATE = R.DEP.ACC<CBE.ACC.DATE.UPDATE>
        GOSUB PRINT_DATA
    NEXT
    RETURN


    PRINT_DATA:


    BB.DATA = APPL.REC.ID:','
    BB.DATA := APPL.TYPE.ID :','
    BB.DATA := APPL.CUR :','
    BB.DATA := APPL.BRANCH.ID : ','
    BB.DATA := APPL.ISJOINT: ','
    BB.DATA := APPL.OPENING.DATE : ','
    IF APPL.NAT.ID EQ '' THEN
        APPL.NAT.ID = '&&'
    END
    BB.DATA := APPL.NAT.ID : ','
    IF APPL.SEC.NAT.ID EQ '' THEN
        APPL.SEC.NAT.ID = '&&'
    END
    BB.DATA := APPL.SEC.NAT.ID : ','
    IF APPL.SEC.NAT.TYPE EQ '' THEN
        APPL.SEC.NAT.TYPE = '&&'
    END
    BB.DATA := APPL.SEC.NAT.TYPE : ','
    IF APPL.CLOSE.DATE EQ '' THEN
        APPL.CLOSE.DATE  = '&&'
    END
    BB.DATA := APPL.CLOSE.DATE : ','
    BB.DATA := APPL.STATUS.ID : ','

    IF APPL.STATUS.REASON EQ '' THEN
        APPL.STATUS.REASON = '&&'
    END
    BB.DATA := APPL.STATUS.REASON:','
    BB.DATA := APPL.LOADING:','
    BB.DATA := APPL.DATE.UPDATE


    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

    HEADER_PRINT:

    BB.HEAD = ''
    BB.HEAD = 'ACCOUNT ID,TYPE ID,CURRENCY ID,BRANCH ID,IS JOINT,OPENNING DATE,NATIONAL ID,SECONDARY ID,SECONDARY ID TYPE,CLOSING DATE,STATUS ID,STATUS REASON'

    WRITESEQ BB.HEAD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN

    RETURN
END
