* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-40</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.TOP.CUS.LW(ENQ.DATA)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TOPCUS.CR.TOT.LW
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    COMP = ID.COMPANY


    FN.TOP.TOT = "F.SCB.TOPCUS.CR.TOT.LW"
    F.TOP.TOT = ''
    R.SCB.TOPCUS.CR.TOT=''
    Y.TOP.TOT.ID=''
    Y.TOP.TOT.ERR=''
    FN.AC      = "FBNK.ACCOUNT"                    ; F.AC      = ""  ; R.AC     = ""
    CALL OPF(FN.TOP.TOT,F.TOP.TOT)
    CALL OPF (FN.AC,F.AC)

    YTEXT = "Enter Amount  : "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.AMT = COMI
    YTEXT = "Enter ( LCY OR FCY OR ALL ) : "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.CY = COMI

    IF WS.CY EQ 'LCY' THEN
       WS.SEL.CY = " AND CURRENCY EQ 'EGP' "
    END
    IF WS.CY EQ 'FCY' THEN
       WS.SEL.CY = " AND CURRENCY NE 'EGP' "
    END
    IF WS.CY EQ 'ALL' THEN
       WS.SEL.CY = ""
    END

    SEL.CMD ="SELECT " :FN.TOP.TOT:" WITH CASH GE ":WS.AMT:" AND @ID LIKE ....":WS.CY:" BY CASH"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',SELECTED,RTNCD)
    IF SELECTED THEN
        T.SEL = "SELECT ":FN.AC:" WITH CUSTOMER IN ( "
        FOR J = 1 TO SELECTED
            WS.CUS.ID =  FIELD(SELLIST<J>,".",1)
            T.SEL := WS.CUS.ID:" "
        NEXT J
        T.SEL := " ) AND CATEGORY EQ 1001 ":WS.SEL.CY:" BY CUSTOMER  "
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED1,E)
        IF SELECTED1 THEN
            FOR I = 1 TO SELECTED1
                ENQ.DATA<2,I> = "@ID"
                ENQ.DATA<3,I> = "EQ"
                ENQ.DATA<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.DATA<2,2> = "@ID"
            ENQ.DATA<3,2> = "EQ"
            ENQ.DATA<4,2> = "DUMMY"
        END
    END  ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END

    RETURN
END
