* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*Line [ 16 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    SUBROUTINE BLD.POSS.ACC.NULL(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS.CU
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS
*-------------------------------------------------------------------------
    FN.POSU = 'F.SCB.POSSESS.CU' ; F.POSU = ''
    CALL OPF(FN.POSU,F.POSU)

    FN.POS = 'F.SCB.POSSESS' ; F.POS = ''
    CALL OPF(FN.POS,F.POS)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG = ""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1= ""

    T.SEL = "SELECT ":FN.POS:" BY CUST.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.POS,KEY.LIST<I>,R.POS,F.POS,E1)
            WS.CUST.CODE = R.POS<POSS.CUST.CODE>

            CALL F.READ(FN.POSU,WS.CUST.CODE,R.POSU,F.POSU,E2)
            WS.ACCT.1 = R.POSU<POSSCU.ACCOUNT.NO>
            WS.ACCT.2 = R.POSU<POSSCU.ACCOUNT.NO2>

            IF WS.ACCT.1 EQ '' AND WS.ACCT.2 EQ '' THEN
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<I>
            END ELSE
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'
            END
        NEXT I
    END
    RETURN
END
