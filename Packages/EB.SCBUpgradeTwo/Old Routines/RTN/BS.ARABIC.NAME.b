* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
****CREATED BY NESSMA
    SUBROUTINE BS.ARABIC.NAME(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*-----------------------------------------------------------------
    FN.CUS = 'FBNK.CUSTOMER'  ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    CUS.ID = ARG
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ERR)

    NAME.1  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME><1,1>
    NAME.2  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME><1,2>
    NAME.3  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>
    ARG     = NAME.1 : SPACE(1) : NAME.2 : SPACE(1): NAME.3
*-----------------------------------------------------------------
    RETURN
END
