* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-- NESSMA --*
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.USER.TIME(ENQ.DATA)
**-----------------------------------------------
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.USER
**-----------------------------------------------
    FN.USR = "F.USER"        ; F.USR   = ""
    CALL OPF(FN.USR,F.USR)
*------------------------------------------------
    SEL.CMD  = "SELECT ":FN.USR:" WITH DATE.TIME LIKE ":TODAY[3,6]:"..."
    SEL.CMD := " AND END.TIME GT 1730"
    SEL.CMD := " AND @ID LIKE SCB..."
    SEL.CMD := " BY DEPARTMENT.CODE"
    SEL.CMD := " BY SCB.DEPT.CODE"
    SEL.CMD := " BY @ID"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    TEXT = "SELECTED = ": NOREC  ; CALL REM

    IF NOREC THEN
        FOR I = 1 TO NOREC
            ENQ.DATA<2,I> = "@ID"
            ENQ.DATA<3,I> = "EQ"
            ENQ.DATA<4,I> = SELLIST<I>
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
*-------------------------------------------------
    RETURN
END
