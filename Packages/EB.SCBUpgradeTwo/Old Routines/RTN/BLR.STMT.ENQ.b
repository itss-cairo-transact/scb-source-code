* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******************MAHMOUD 27/4/2014********************
    SUBROUTINE BLR.STMT.ENQ(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*------------------------------------------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    KK1  = 2
    TTD1 = TODAY
    COMP = ID.COMPANY

*Line [ 39 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE "ACCOUNT.NUMBER" IN ENQ<2,1> SETTING ACC.POS THEN ACCT.ID = ENQ<4,ACC.POS> ELSE NULL
    LOCATE "BOOKING.DATE"   IN ENQ<2,1> SETTING BKD.POS THEN
        BK.DATE = ENQ<4,BKD.POS>
        OPER    = ENQ<3,BKD.POS>
    END

    BEGIN CASE
    CASE OPER EQ 'RG'
        START.DATE = FIELD(BK.DATE," ",1)
        END.DATE   = FIELD(BK.DATE," ",2)
    CASE OPER EQ 'GE'
        START.DATE = BK.DATE
        END.DATE   = TTD1
    CASE OPER EQ 'EQ'
        START.DATE = BK.DATE
        END.DATE   = BK.DATE
    CASE OPER EQ 'GT'
        START.DATE = BK.DATE
        CALL CDT(START.DATE,'','+1C')
        END.DATE   = TTD1
    CASE OTHERWISE
*Line [ 61 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        NULL
    END CASE

    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,START.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        IF STE.ID THEN
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = STE.ID
        END
    REPEAT
    IF KK1 = 2 THEN
        ENQ<2,2> = "@ID"
        ENQ<3,2> = "EQ"
*--- EDIT BY NESSMA ON 2016/11/25
        ENQ<4,2> = ACCT.ID :"*": BK.DATE
    END
    RETURN
********************************************
END
