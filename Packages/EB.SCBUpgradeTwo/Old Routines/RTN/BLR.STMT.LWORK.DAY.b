* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.STMT.LWORK.DAY(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ENT.TODAY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ENT = "FBNK.ACCT.ENT.LWORK.DAY" ; F.ENT = "" ; R.ENT = "" ; ERR.ENT = ""
    CALL OPF(FN.ENT,F.ENT)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE = "" ; R.STE = "" ; ERR.STE = ""
    CALL OPF(FN.STE,F.STE)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    ENT.SEL  ="SELECT ":FN.ENT
    ENT.SEL :=" WITH CO.CODE EQ ":COMP
*    ENT.SEL :=" BY @ID"
    KK1 = 0
    CALL EB.READLIST(ENT.SEL,K.LIST,'',SELECTED,ER.MSG)
    LOOP
        REMOVE AC.ID FROM K.LIST SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.ENT,AC.ID,R.ENT,F.ENT,ER.ENT)
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.ID,AC.CAT)
        CALL DBR('ACCOUNT':@FM:AC.CO.CODE ,AC.ID,AC.COM)
        IF AC.COM EQ COMP AND AC.CAT NE 1002 AND AC.CAT NE 12005 THEN
            LOOP
                REMOVE STE.ID FROM R.ENT SETTING POS.STE
            WHILE STE.ID:POS.STE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = STE.ID
            REPEAT
        END ELSE
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = "NOLIST"
        END
    REPEAT
    RETURN
