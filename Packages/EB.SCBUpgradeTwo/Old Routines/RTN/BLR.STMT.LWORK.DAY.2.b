* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>147</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.STMT.LWORK.DAY.2(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*#    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    FN.ENT = "FBNK.ACCT.ENT.LWORK.DAY" ; F.ENT = "" ; R.ENT = "" ; ERR.ENT = ""
    CALL OPF(FN.ENT,F.ENT)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE = "" ; R.STE = "" ; ERR.STE = ""
    CALL OPF(FN.STE,F.STE)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    ENT.SEL  ="SELECT ":FN.ENT
    ENT.SEL :=" WITH CO.CODE EQ ":COMP
*    ENT.SEL :=" BY @ID"
    KK1 = 0
    CALL EB.READLIST(ENT.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE AC.ID FROM K.LIST SETTING POS.AC
        WHILE AC.ID:POS.AC
            CALL F.READ(FN.ENT,AC.ID,R.ENT,F.ENT,ER.ENT)
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.ID,AC.CAT)
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER ,AC.ID,CUS.ID)

            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            Q.RATE = LOCAL.REF<1,CULR.QLTY.RATE>
            IF Q.RATE EQ '3' THEN
                IF AC.CAT NE 1002 THEN
                    LOOP
                        REMOVE STE.ID FROM R.ENT SETTING POS.STE
                    WHILE STE.ID:POS.STE
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = STE.ID
                    REPEAT
                END ELSE
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = "NOLIST"
                END
            END ELSE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        REPEAT
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END

    RETURN
