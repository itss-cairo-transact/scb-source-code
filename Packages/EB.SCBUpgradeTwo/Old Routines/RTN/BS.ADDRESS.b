* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
****CREATE BY NESSMA
    SUBROUTINE BS.ADDRESS(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    $INSERT  TEMENOS.BP I_F.SCB.BR.SLIPS
*-----------------------------------------------------------------
    FN.CUS = 'FBNK.CUSTOMER'  ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    CUS.ID = ARG
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ERR)

    ADD  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
    GOV  = R.CUS<EB.CUS.CULR.GOVERNORATE>
    REG  = R.CUS<EB.CUS.CULR.REGION>

    ARG = ADD : SPACE(3) : GOV : SPACE(3) : REG
*-----------------------------------------------------------------
    RETURN
END
