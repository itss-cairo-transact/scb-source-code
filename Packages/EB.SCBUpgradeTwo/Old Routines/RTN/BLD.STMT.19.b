* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
***M.ELSAYED/2012-04-29**
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.STMT.19(ENQ)
*    PROGRAM   BLD.STMT.POS
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*    $INCLUDE T24.BP I_F.ACCT.ENT.LWORK.DAY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
*=============================================================*
    FN.TRNS  = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = '' ;  R.CUST = ''
    CALL OPF (FN.CUST,F.CUST)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF (FN.AC,F.AC)
    I = 0
*====================================================================*
    T.SEL = "SELECT FBNK.ACCT.ENT.LWORK.DAY WITH CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        LOOP
            REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
        WHILE ACCT.ID:POS
            CALL F.READ( FN.AC,ACCT.ID, R.AC,F.AC, E.AC)
            WS.AC.CUSTOMER = R.AC<AC.CUSTOMER>
            WS.AC.CATEG    = R.AC<AC.CATEGORY>[1,1]
            IF WS.AC.CUSTOMER NE '' THEN
                IF WS.AC.CATEG NE '9' THEN
                    CALL F.READ( FN.TRNS,ACCT.ID, R.TRNS,F.TRNS, ETEXT)

                    LOOP
                        REMOVE STMT.ID  FROM R.TRNS SETTING POS.LW
                    WHILE STMT.ID:POS.LW

                        CALL F.READ( FN.STMT,STMT.ID, R.STMT,F.STMT, E.STMT)

                        CUS.ID = R.STMT<AC.STE.CUSTOMER.ID>
                        CALL F.READ (FN.CUST, CUS.ID, R.CUST, F.CUST, E1)
                        POS.REST  = R.CUST<EB.CUS.POSTING.RESTRICT>
                        SEC       = R.CUST<EB.CUS.SECTOR>
                        CU.STATUS = R.CUST< EB.CUS.CUSTOMER.STATUS>
                        LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>

                        NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
                        EXP.DATE = LOCAL.REF<1, CULR.ID.EXPIRY.DATE>
                        SCB.POS  = LOCAL.REF<1,CULR.SCB.POSTING>
                        IF ( SCB.POS EQ 19 ) THEN
                            I++
                            ENQ<2,I> = '@ID'
                            ENQ<3,I> = 'EQ'
                            ENQ<4,I> = STMT.ID
                        END
                    REPEAT
                END
            END
        REPEAT
    END
    IF I = 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'NO RECORDS'
    END

    RETURN

*******************************************************************************************

END
