* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*******************MAHMOUD 21/1/2016**************************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.ADI.DR.INT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT

    AC.ID = ""
    AC.ID = O.DATA
    ACC.RATE = ""
    IF AC.ID THEN
        FN.AC = "FBNK.ACCOUNT" ; F.AC = "" ; R.AC = ""
        CALL OPF(FN.AC,F.AC)
        FN.ADI = "FBNK.ACCOUNT.DEBIT.INT" ; F.ADI = "" ; R.ADI = ""
        CALL OPF(FN.ADI,F.ADI)
        CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ERR1)
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        ADI.D.CNT = DCOUNT(R.AC<AC.ACCT.DEBIT.INT>,@VM)
        ADI.ID = AC.ID:"-":R.AC<AC.ACCT.DEBIT.INT,ADI.D.CNT>
        CALL F.READ(FN.ADI,ADI.ID,R.ADI,F.ADI,ERR2)
***
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DB = DCOUNT(R.ADI<IC.ADI.DR.INT.RATE>,@VM)
        FOR X = 1 TO DB

            ADI.INT.R = R.ADI<IC.ADI.DR.INT.RATE,X>
            ADI.BRATE = R.ADI<IC.ADI.DR.BASIC.RATE,X>
            ADI.MRG.O = R.ADI<IC.ADI.DR.MARGIN.OPER,X>
            ADI.MRG.R = R.ADI<IC.ADI.DR.MARGIN.RATE,X>
            ACC.RATE = ADI.INT.R
            IF ADI.MRG.O EQ 'ADD' THEN
                ACC.RATE = ACC.RATE + ADI.MRG.R
            END
            IF ADI.MRG.O EQ 'SUBTRACT' THEN
                ACC.RATE = ACC.RATE - ADI.MRG.R
            END
            IF ADI.MRG.O EQ 'MULTIBLY' THEN
                ACC.RATE = ACC.RATE * ADI.MRG.R
            END

            KK := ACC.RATE:'  '
        NEXT X
        O.DATA = KK
    END
    RETURN
