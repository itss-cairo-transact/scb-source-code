* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>272</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.TOP.INT.LD.MAST(ENQ)
*    PROGRAM BLR.LD.MAX.2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
    COMP = ID.COMPANY
    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.DATE.LPE)
    WS.DATE.LPE =    WS.DATE.LPE[1,6]
    WS.TOT.INT.TMP = 0
    DIM WS.SEL.INT(60)
    DIM WS.SEL.GL(60)
    DIM WS.SEL.CY(60)

*WS.SEL.INT  = 0
*WS.SEL.GL       = 0
    WS.GL.TMP       = 0
    X = 0
*---------------------------------------------------------------------------------------
    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS" ; F.LD = "" ; R.LD = "" ; ERR.LD = ""
    CALL OPF(FN.LD,F.LD)
    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""
*    T.SEL  ="SELECT ":FN.LD:" WITH TOT.INT NE 0 AND TOT.INT NE '' AND INTEREST.SPREAD NE '' AND INTEREST.SPREAD NE '' AND INTEREST.SPREAD NE 0 AND CATEGORY LE 21010 AND FIN.MAT.DATE GT ":TODAY
    T.SEL  ="SELECT ":FN.LD:" WITH TOT.INT NE 0 AND TOT.INT NE ''  AND CATEGORY LE 21010 AND SECTOR NE '1100' AND SECTOR NE '1400' "
    T.SEL := " AND ( CURRENCY EQ 'EGP' OR CURRENCY EQ 'USD' ) WITHOUT CUSTOMER.ID IN ( 1304955 1101254 1101253 )"
    T.SEL := " BY CATEGORY BY CURRENCY BY TOT.INT"
    KK1 = 0

    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
*PRINT SELECTED
        FOR I = 1 TO SELECTED
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,KEY.LIST<I>,WS.INT.RATE)
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.SPREAD,KEY.LIST<I>,WS.SP.RATE)
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,KEY.LIST<I>,WS.GL)
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CURRENCY,KEY.LIST<I>,WS.CY)
            WS.TOT.INT = WS.INT.RATE + WS.SP.RATE
            WS.GL      = WS.GL:WS.CY
*  PRINT WS.TOT.INT
            IF WS.GL.TMP EQ 0 THEN
                WS.GL.TMP = WS.GL
            END
            IF WS.GL.TMP # WS.GL THEN
                X++
                WS.SEL.GL(X)  = WS.GL.TMP[1,5]
                WS.SEL.CY(X)  = WS.GL.TMP[3]
                WS.SEL.INT(X) = WS.TOT.INT.TMP
*PRINT   "CHNG.GL= ": WS.SEL.GL(X)  :" CHNG.INT = ":   WS.SEL.INT(X)
                WS.TOT.INT.TMP  = 0
                WS.GL.TMP       = WS.GL
            END
            IF WS.TOT.INT.TMP EQ 0 THEN
                WS.TOT.INT.TMP = WS.TOT.INT
            END
            IF WS.TOT.INT > WS.TOT.INT.TMP THEN
                WS.TOT.INT.TMP = WS.TOT.INT
*PRINT      WS.TOT.INT.TMP  :" GL= ":WS.GL.TMP
            END

            WS.GL.TMP = WS.GL
        NEXT I
        X++
        WS.SEL.GL(X)  = WS.GL.TMP[1,5]
        WS.SEL.CY(X)  = WS.GL.TMP[3]
        WS.SEL.INT(X) = WS.TOT.INT.TMP
*        T.SEL2  ="SELECT ":FN.LD:" WITH ( VALUE.DATE GE ":WS.DATE.LPE:"01 AND VALUE.DATE LE ":WS.DATE.LPE:"31 ) AND ( "
        T.SEL2  ="SELECT ":FN.LD:" WITH ( "
        FOR Z = 1 TO X
            IF Z # X THEN
                WS.OR = "' ) OR"
            END ELSE
                WS.OR = "' )"
            END
            T.SEL2 := " ( CATEGORY EQ ":WS.SEL.GL(Z):" AND TOT.INT EQ '":WS.SEL.INT(Z):"' AND CURRENCY EQ '":WS.SEL.CY(Z):WS.OR
        NEXT Z
        T.SEL2 := " )"
*PRINT T.SEL2
        KK1 = 0
        CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)
        IF SELECTED2 THEN
*            TEXT = SELECTED2 ; CALL REM
            FOR Y = 1 TO SELECTED2
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = KEY.LIST2<Y>
            NEXT Y
            IF KK1 = 0 THEN
                ENQ<2,2> = "@ID"
                ENQ<3,2> = "EQ"
                ENQ<4,2> = "DUMMY"
            END
        END ELSE
            ENQ<2,2> = "@ID"
            ENQ<3,2> = "EQ"
            ENQ<4,2> = "DUMMY"
        END
    END
    RETURN
