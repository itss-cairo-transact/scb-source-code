* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE CHEQ.NO

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT           I_BR.LOCAL.REFS
*-------------------------------------------------------------
    APPL.ID = COMI
    IF APPL.ID MATCH '0N' THEN

        IF LEN(COMI) > 14 THEN
            ETEXT = 'CHEQ NO MUST 14 NO' ;  CALL STORE.END.ERROR
        END

        IF LEN(R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO>) > 14 THEN
            ETEXT = 'CHEQ NO MUST 14 NO' ;  CALL STORE.END.ERROR
        END
    END ELSE
        ETEXT = 'NOT VALID NUMBER'  ;  CALL STORE.END.ERROR
    END
*--------------------------------------------------------------
    IF MESSAGE EQ 'VAL' THEN
        IF PGM.VERSION EQ ',SCB.CHQ.LCY.REG.0017' THEN
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ 0017 THEN
                CALL VVR.BR.CHQ.CHECK(COMI)
            END
        END
    END
*--------------------------------------------------------------
    RETURN
END
