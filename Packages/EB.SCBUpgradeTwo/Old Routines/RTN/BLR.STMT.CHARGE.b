* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>189</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.STMT.CHARGE(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.CHARGE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    CUS.ID = ''
*Line [ 34 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE "@ID" IN ENQ<2,1> SETTING ID.POS THEN CUS.ID = ENQ<4,ID.POS> ELSE NULL

    FN.SCH = "F.SCB.STMT.CHARGE" ; F.SCH = "" ; R.SCH = "" ; ERR.SCH = ""
    CALL OPF(FN.SCH,F.SCH)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    KK1 = 0
    SEL.CMD  = "SELECT ":FN.SCH:" WITH CO.CODE EQ ":COMP
    SEL.CMD := " AND (STMT.FLG EQ 'YES' OR POST.FLG EQ 'YES')"
    IF CUS.ID THEN
        SEL.CMD := " AND @ID LIKE ":CUS.ID:"...."
    END
    CALL EB.READLIST(SEL.CMD,K.LIST,'',SELECTED,ACCT.ERR)
    IF SELECTED THEN
        LOOP
            REMOVE STC.ID FROM K.LIST SETTING POS
        WHILE STC.ID:POS
            KK1++
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = STC.ID
        REPEAT
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
