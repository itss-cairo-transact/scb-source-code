* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.TLR.OUT3000000(ENQ.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*-----------------------------------

    WS.COMP = ID.COMPANY
    COMP.FLAG = WS.COMP[8,2] * 1
    COMP.1    = COMP.FLAG:"1..."
    COMP.2    = COMP.FLAG:"2..."
    COMP.3    = COMP.FLAG:"3..."

    SYS.DATE   = TODAY
    WORK.DATE   = SYS.DATE
***    CALL CDT('', WORK.DATE, "-1W")
*------------------------
***    FN.TLLR = "FBNK.TELLER$HIS"
    FN.TLLR = "FBNK.TELLER"
    F.TLLR = ''
    R.TELLER = ''
    Y.TLLR = ''
    Y.TLLR.ERR = ''

    CALL OPF(FN.TLLR,F.TLLR)

    SEL.CMD   = "SELECT ":FN.TLLR:" BY CUSTOMER.1 WITH"
*    SEL.CMD  := " AUTH.DATE EQ ":WORK.DATE
    SEL.CMD  := " AND DR.CR.MARKER EQ 'DEBIT'"
    SEL.CMD  := " AND CO.CODE EQ ":WS.COMP
    SEL.CMD  := " AND CUSTOMER.1 LIKE ":COMP.3
*    SEL.CMD  := " AND RECORD.STATUS EQ 'MAT'"


    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)


    IF NOREC >= 1 THEN
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
    RETURN

END
