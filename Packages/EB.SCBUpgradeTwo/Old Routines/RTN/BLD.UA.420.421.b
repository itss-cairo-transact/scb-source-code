* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>521</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.UA.420.421(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*---------------------------------------------------
    WS.COMP = ID.COMPANY[8,2]
    IF WS.COMP[1,1] EQ 0 THEN
        WS.COMP = ID.COMPANY[9,1]
    END
    ENQ.LP  = 0
    FN.US   = 'F.USER'
    FN.UA   = 'F.USER.ABBREVIATION'
    F.US    = '' ; R.US  = ''
    F.UA    = '' ; R.UA = ''
    CALL OPF(FN.US,F.US)
    CALL OPF(FN.UA,F.UA)
*--------
*  T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND DEPARTMENT.CODE EQ 13 AND ( ORIGINAL.TEXT EQ ?438 OR ORIGINAL.TEXT EQ ?436 )"
*  T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND END.DATE.PROFILE GE ":TODAY:" AND DEPARTMENT.CODE EQ 60  BY DEPARTMENT.CODE "
*    T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND END.DATE.PROFILE GE ":TODAY:" AND DEPARTMENT.CODE EQ ":WS.COMP:" BY DEPARTMENT.CODE "
    T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND END.DATE.PROFILE GE ":TODAY:" BY DEPARTMENT.CODE "
*    T.SEL2  = "SELECT ":FN.UA:" WITH @ID IN ( SCB.48801 SCB.4634 SCB.11436 ) BY DEPARTMENT.CODE "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED1,ER.MSG)
*    TEXT  = SELECTED1 ; CALL REM
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1

            CALL DBR('USER':@FM:EB.USE.SIGN.ON.NAME,KEY.LIST2<I>,WS.USER.LIVE)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,KEY.LIST2<I>,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            CALL DBR('USER':@FM:EB.USE.INIT.APPLICATION,KEY.LIST2<I>,WS.INIT)

            *TEXT  = ' WS.INIT ':WS.INIT ; CALL REM
            IF WS.INIT = '?421' THEN
                WS.421 = 'Y'
            END
            IF WS.INIT = '?420' THEN
                WS.420 = 'Y'
            END

            IF (( WS.SCB.DEPT NE 2800 ) AND ( WS.SCB.DEPT NE 2700 ) AND ( WS.SCB.DEPT NE 5100 )) THEN
                IF WS.USER.LIVE NE '' THEN
                    CALL F.READ(FN.UA,KEY.LIST2<I>,R.UA,F.UA,E.UA)
*Line [ 72 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    WS.HMM.CONT = DCOUNT(R.UA<EB.UAB.ORIGINAL.TEXT>,@VM)
                    *TEXT  = KEY.LIST2<I>:' WS.COUNT ':WS.HMM.CONT ; CALL REM
                    FOR I.UA = 1 TO WS.HMM.CONT

                        WS.HMM.NO = R.UA<EB.UAB.ORIGINAL.TEXT><1,I.UA>

                        *TEXT  = ' WS.HMM.NO ':WS.HMM.NO ; CALL REM

                        IF WS.HMM.NO = '?421' THEN
                            WS.421 = 'Y'
                        END
                        IF WS.HMM.NO = '?420' THEN
                            WS.420 = 'Y'
                        END
                        IF WS.420 = 'Y' AND WS.421 = 'Y' THEN
                            I.UA = WS.HMM.CONT
                        END
                    NEXT I.UA

                    IF WS.420 = 'Y' AND WS.421 = 'Y' THEN
                        *TEXT  = ' WS.420 ':WS.420 :" WS.421 ":WS.421 ; CALL REM
                        ENQ.LP ++
                        ENQ.DATA<2,ENQ.LP> = '@ID'
                        ENQ.DATA<3,ENQ.LP> = 'EQ'
                        ENQ.DATA<4,ENQ.LP> = KEY.LIST2<I>
                    END
                    WS.420 = ''
                    WS.421 = ''
                END
            END
        NEXT I
        IF ENQ.LP EQ 0 THEN
            ENQ.DATA<2,2> = '@ID'
            ENQ.DATA<3,2> = 'EQ'
            ENQ.DATA<4,2> = 'DUUMY'
        END
    END ELSE
        ENQ.DATA<2,2> = '@ID'
        ENQ.DATA<3,2> = 'EQ'
        ENQ.DATA<4,2> = 'DUUMY'
    END
TEXT = "NO OF USER PRINT = ": ENQ.LP ; CALL REM
    RETURN
END
