* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.PASSWORD.RESET.HIS(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PASSWORD.UPD
*-------------------------
    FN.PASS = "F.SCB.PASSWORD.UPD" ; F.PASS = ""
    CALL OPF(FN.PASS, F.PASS)
*-------------------------
    DATE.TOD  = TODAY
    CALL ADD.MONTHS(DATE.TOD,'-3')
    FROM.DATE = DATE.TOD[1,6]: "01"

    DATE.TOD.2  = TODAY
    CALL ADD.MONTHS(DATE.TOD.2,'-1')
    END.DATE    = DATE.TOD.2[1,6]: "01"
    CALL LAST.DAY(END.DATE)

*----
    SEL.CMD  = "SELECT " :FN.PASS: " WITH BOOKING.DATE GE ":FROM.DATE
    SEL.CMD := " AND BOOKING.DATE LE ":END.DATE:" BY USER.ID"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,RTNCD)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            ENQ.DATA<2,II> = "@ID"
            ENQ.DATA<3,II> = "EQ"
            ENQ.DATA<4,II> = KEY.LIST<II>
        NEXT II
    END ELSE
        ENQ.DATA<2,1> = "@ID"
        ENQ.DATA<3,1> = "EQ"
        ENQ.DATA<4,1> = "DUMMY"
    END
*-------------------------
    RETURN
END
