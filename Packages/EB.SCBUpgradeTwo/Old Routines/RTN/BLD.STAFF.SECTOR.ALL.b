* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.STAFF.SECTOR.ALL(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TOPCUS.CR.TOT
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
    COMP = ID.COMPANY


    FN.TOP.TOT = "F.SCB.TOPCUS.CR.TOT"
    F.TOP.TOT = ''
    R.SCB.TOPCUS.CR.TOT=''
    Y.TOP.TOT.ID=''
    Y.TOP.TOT.ERR=''
    K = 0

    CALL OPF(FN.TOP.TOT,F.TOP.TOT)
    SEL.CMD ="SSELECT " :FN.TOP.TOT:" WITH CCY.CUS EQ 'ALL' BY TOTAL.CUS"
*MSABRY
*   SEL.CMD ="SSELECT " :FN.TOP.TOT:" WITH CCY.CUS EQ 'ALL' BY TOTAL.CUS "
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN
        K =1
        FOR I = 1 TO NOREC
            SECTOR = 0
            CUS.ID = FIELD (SELLIST<I>,'.',1)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.ID,SECTOR)
            IF (SECTOR GE 1100 AND SECTOR LE 1400) THEN
                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = SELLIST<I>
                K++
            END
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
