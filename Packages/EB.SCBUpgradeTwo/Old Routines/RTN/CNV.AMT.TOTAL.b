* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******************************NI7OOOOOOOOOO********************

    SUBROUTINE CNV.AMT.TOTAL
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUR.STATUS

    FN.CUR  = 'F.CUR.STATUS' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    XX      = O.DATA
    ZZ      = TODAY
     T.SEL  = "SELECT F.CUR.STATUS WITH CURR.NO EQ " : XX :" AND DATE EQ " : ZZ
   **T.SEL  = "SELECT F.CUR.STATUS WITH CURR.NO EQ 8 AND DATE EQ '20100408'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUR,KEY.LIST<I>,R.CUR,F.CUR,ERR1)
        CURRE  = R.CUR<CURSTAT.CURRENCY>
**** IF (CURRE NE 'EGP' OR CURRE NE 'GBP' OR CURRE NE 'USD' OR CURRE NE 'EUR' OR CURRE NE 'SAR' OR CURRE NE 'CHF' OR CURRE NE 'JPY') THEN
        IF (CURRE NE 'EGP' AND CURRE NE 'GBP' AND CURRE NE 'USD' AND CURRE NE 'EUR' AND CURRE NE 'SAR' AND CURRE NE 'CHF' AND CURRE NE 'JPY') THEN
            AMT    = R.CUR<CURSTAT.AMOUNT.LCY>
            AMTF   = R.CUR<CURSTAT.AMOUNT.FCY>
            AMTTOTAL  += AMT
            AMTFTOTAL += AMTF
            WS.COMN  = AMTTOTAL/1000
            WS.COMN2 = FMT(WS.COMN, "R0,")

        END
    NEXT I
    O.DATA = AMTTOTAL

    RETURN
END
