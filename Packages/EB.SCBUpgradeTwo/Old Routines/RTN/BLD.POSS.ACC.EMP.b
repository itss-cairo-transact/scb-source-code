* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.POSS.ACC.EMP(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS.CU
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS
*-------------------------------------------------------------------------
    FN.POSU = 'F.SCB.POSSESS.CU' ; F.POSU = '' ; R.POSU = ''
    CALL OPF(FN.POSU,F.POSU)

    FN.POS = 'F.SCB.POSSESS' ; F.POS = '' ; R.POS = ''
    CALL OPF(FN.POS,F.POS)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG = ""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1= ""

    LOCATE "POSSESS.DATE" IN ENQ<2,1> SETTING DATE.POS THEN
        POSSESS.DATE1  = ENQ<4,DATE.POS>
    END
    LOCATE "CURRENCY" IN ENQ<2,1> SETTING CUR.POS THEN
        CURR  = ENQ<4,CUR.POS>
    END

    T.SEL = "SELECT F.SCB.POSSESS WITH POSSESS.DATE EQ ":POSSESS.DATE1:" AND CURRENCY EQ ":CURR
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.POS,KEY.LIST<I>,R.POS,F.POS,E1)
            WS.CUST.CODE = R.POS<POSS.CUST.CODE>
            CALL F.READ(FN.POSU,WS.CUST.CODE,R.POSU,F.POSU,E2)
            WS.TYPE = R.POSU<POSSCU.CUSTOMER.TYPE>
            ACC1    = R.POSU<POSSCU.ACCOUNT.NO>
            ACC2    = R.POSU<POSSCU.ACCOUNT.NO2>
            IF (ACC1 EQ '' AND ACC2 EQ '') AND WS.TYPE EQ '' THEN

                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<I>
            END ELSE
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'

            END
        NEXT I
    END
    RETURN
END
