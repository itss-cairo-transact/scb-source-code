* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------NI7OOOOOOOOOOOOOOOO------------------------------------------
    SUBROUTINE CARD.ISSUE.DEBIT.OMLA(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
**  $INCLUDE GLOBUS.BP I_F.CARD.ISSUE.ACCOUNT

    COMPY = ID.COMPANY

    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = ''
    CALL OPF(FN.CUST,F.CUST)

    FN.CUS='FBNK.CARD.ISSUE' ; F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

    FN.AC='FBNK.ACCOUNT';F.AC=''
    CALL OPF(FN.AC,F.AC)


    FN.CARD='FBNK.CARD.ISSUE';F.CARD=''
    CALL OPF(FN.CARD,F.CARD)

    FN.CARD.ACC='FBNK.CARD.ISSUE.ACCOUNT';F.CARD.ACC=''
    CALL OPF(FN.CARD.ACC,F.CARD.ACC)

    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    T.SEL=" SELECT FBNK.CUSTOMER WITH (SECTOR EQ 1100 OR SECTOR EQ 1300) AND POSTING.RESTRICT LE 80"
*** T.SEL=" SELECT FBNK.CUSTOMER WITH @ID EQ 1304848 OR @ID EQ 13100499 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SELECTED " : SELECTED ; CALL REM
    FOR I = 1 TO SELECTED

        CALL F.READ( FN.CUS.AC,KEY.LIST<I>, R.CUS.AC, F.CUS.AC,ETEXT1)
****TEXT = " KEY.LIST<I> " : KEY.LIST<I> ; CALL REM
        LOOP
            REMOVE ACC FROM R.CUS.AC  SETTING POS1
        WHILE ACC:POS1
***TEXT = "ACC= " : ACC ; CALL REM
            CALL F.READ(FN.CARD.ACC,ACC,R.CARD.ACC,F.CARD.ACC,ERR11)
            IF ERR11 THEN
                XX = 2
            END
***TEXT = "HXX=  " : XX ; CALL REM
            LOOP
                REMOVE CARD.ACC FROM R.CARD.ACC  SETTING POS11
            WHILE CARD.ACC:POS11
                IF CARD.ACC[1,4] EQ 'ATMC' THEN
                    XX = 1
                END

            REPEAT
        REPEAT

*********************
**TEXT = "HI" ; CALL REM

        IF XX NE '1' THEN
            TEXT = "YES" ; CALL REM
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<I>
        END
    NEXT I
**TEXT = "END" ; CALL REM
**********************
    RETURN
END
