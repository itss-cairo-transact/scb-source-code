* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.LD.DATES.SAR(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
    COMP = C$ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""

    FN.DATE = "F.DATES"
    F.DATE  = ""
    R.DATE = ""
    Y.DATE.ID = ""

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD  = ""

    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.LD,F.LD)

    Y.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,Y.DATE.ID,R.DATE,F.DATE,ERR.DATE)
    TDATE = R.DATE<EB.DAT.LAST.PERIOD.END>

    T.SEL  = "SELECT ":FN.LD:" WITH CURRENCY EQ 'SAR' AND ( CATEGORY GE 21001 AND"
    T.SEL := " CATEGORY LE 21010 )"
    T.SEL := " AND (( VALUE.DATE LE ":TDATE:" AND AMOUNT NE 0 )"
    T.SEL := " OR ( FIN.MAT.DATE GT ":TDATE:" AND AMOUNT EQ 0 ))"
    T.SEL := " BY CO.CODE BY CUSTOMER.ID BY CURRENCY"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,I> = "@ID"
        ENQ<3,I> = "EQ"
        ENQ<4,I> = "DUMMY"

    END


*******************************************************************************************
    RETURN
END
