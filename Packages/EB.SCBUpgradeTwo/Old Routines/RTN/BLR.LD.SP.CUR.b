* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.LD.SP.CUR(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS" ; F.LD  = ""
    CALL OPF (FN.LD,F.LD)

    DAT.ID = "EG0010001"
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TDATE)

    LOCATE "CURRENCY" IN ENQ<2,1> SETTING CUR.POS THEN
        CUR.ID = ENQ<4,CUR.POS>
    END

    T.SEL  = "SELECT ":FN.LD:" WITH ( CATEGORY GE 21001 AND CATEGORY LE 21010 )"
    T.SEL := " AND (( VALUE.DATE LE ":TDATE:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":TDATE:" AND AMOUNT EQ 0 )) AND INTEREST.SPREAD GT 0 AND CURRENCY EQ ":CUR.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,I> = "@ID"
        ENQ<3,I> = "EQ"
        ENQ<4,I> = "DUMMY"
    END


*******************************************************************************************
    RETURN
END
