* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
**********ABEER AS OF 2019-03-14
    SUBROUTINE CALC.CD
*    PROGRAM CAL.CD

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.SCHEDULE.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.SCHEDULES.PAST
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    F.SCH = '' ; FN.SCH = 'FBNK.LD.SCHEDULE.DEFINE'
    R.SCH = ''
    CALL OPF(FN.SCH,F.SCH)

    F.LMM = '' ; FN.LMM = 'FBNK.LMM.ACCOUNT.BALANCES'
    R.LMM = ''
    CALL OPF(FN.LMM,F.LMM)

    F.LD= '' ; FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    R.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.BI.DATE = 'FBNK.BASIC.INTEREST.DATE' ; F.BI.DATE = '';R.BI.DATE=''
    CALL OPF(FN.BI.DATE, F.BI.DATE)

    FN.BI.INT = 'FBNK.BASIC.INTEREST' ; F.BI.INT = '';R.BI.INT=''
    CALL OPF( FN.BI.INT, F.BI.INT)

    F.SCH.PAST = '' ; FN.SCH.PAST = 'FBNK.LMM.SCHEDULES.PAST'
    R.SCH.PAST = ''
    CALL OPF(FN.SCH.PAST,F.SCH.PAST)



    TOT.INT ='0' ; TOT.AMT = '0';PRIN.INT='';FLG = ''

    XX = ''
    CALL JULDATE(TODAY,XX )
*********************************
    FOL.NAME   = "/home/signat"
*   FOL.NAME   ="&SAVEDLISTS&"
    OPENSEQ FOL.NAME , "CALC.CD.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ': FOL.NAME :' ':"CALC.CD.CSV"
        HUSH OFF
    END
    OPENSEQ FOL.NAME , "CALC.CD.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CALC.CD.CSV CREATED IN SIGNAT'
        END ELSE
            STOP 'Cannot create CALC.CD.CSV File IN SIGNAT'
        END
    END
**********************************

    YTEXT = "Enter CATEGORY : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    CATG = COMI
    TOD.DAT = TODAY
    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ ": CATG :" AND STATUS NE LIQ AND VALUE.DATE LT " : TOD.DAT
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT= SELECTED;CALL REM
***********
*PRINT HEADER
    BB.DATA  = " ID ":','
    BB.DATA := " INT DATE ":","
    BB.DATA := " INT AMT ":","
    BB.DATA := " CUST ID ":","
    BB.DATA := " DR ACCT ":","
    BB.DATA := " INT ACCT ":","
    BB.DATA := " VALUE DATE ":","
    BB.DATA := " INT RATE ":","
    BB.DATA := " CUST NAME ":","
    BB.DATA := " PRIN AMT ":","
    BB.DATA := " MAT.DATE ":","



    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            IDD = KEY.LIST<I>
            LMM.ID = KEY.LIST<I>:'00'
            CALL F.READ(FN.LMM,LMM.ID, R.LMM, F.LMM, EXT21)
            CALL F.READ(FN.LD,IDD, R.LD, F.LD, E21)
******************
            CD.KEY   = R.LD<LD.INTEREST.KEY>
            CD.CUR   = R.LD<LD.CURRENCY>
            CD.AMT   = R.LD<LD.AMOUNT>
            VAL.DATE = R.LD<LD.VALUE.DATE>
            MAT.DATE =R.LD<LD.FIN.MAT.DATE>
            INT.ACCT = R.LD<LD.INT.LIQ.ACCT>
            DR.ACCT  = R.LD<LD.DRAWDOWN.ACCOUNT>
            CUST.ID  = R.LD<LD.CUSTOMER.ID>
            LOCAL.REF = R.LD<LD.LOCAL.REF>

            CD.INT.MON =LOCAL.REF<1,LDLR.CD.TYPE>
*PRINT CD.INT.MON
            NO.INT.MON=FIELD(CD.INT.MON,'-',3)
*PRINT NO.INT.MON :'NO.INT.MON'
            NO.INT.MON=FIELD(NO.INT.MON,'M',1)

            CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUST.ID,MYLOCAL)
            CUS.NAME = MYLOCAL<1,CULR.ARABIC.NAME>
*******************
            BI.DATE.ID = CD.KEY:CD.CUR
            CALL F.READ(FN.BI.DATE,BI.DATE.ID,R.BI.DATE,F.BI.DATE,DATE.ERR)

            IF R.BI.DATE NE '' THEN
                BI.DATE = R.BI.DATE<EB.BID.EFFECTIVE.DATE,1>
            END
            IF CD.KEY NE '' THEN
                BI.ID = BI.DATE.ID:BI.DATE
                CALL F.READ(FN.BI.INT,BI.ID,R.BI.INT,F.BI.INT,BI.ERR)
                INT.RATE = R.BI.INT<EB.BIN.INTEREST.RATE>
                INT.100.RATE= INT.RATE/100
            END ELSE
                INT.RATE = R.LD<LD.INTEREST.RATE>
                INT.100.RATE= INT.RATE/100
            END
*****************
            CALL F.READ(FN.SCH,IDD, R.SCH, F.SCH, ETEXT2)

            CD.DAT = R.SCH<LD.SD.DATE>
            CD.FRQ = R.SCH<LD.SD.FREQUENCY>
            CD.SCH = R.SCH<LD.SD.CYCLED.DATES>
            SCH.NO = DCOUNT(CD.SCH,@SM)
**************************************   calc today int imt
            IDD.PAST = KEY.LIST<I>:XX:'00'
            CALL F.READ(FN.SCH.PAST,IDD.PAST,R.SCH.PAST,F.SCH.PAST, ETEXT11)
            DATE.TO = R.SCH.PAST<LD28.DATE.INT.REC>
            INT.AMT = R.SCH.PAST<LD28.INT.RECEIVED>

            IF DATE.TO EQ TODAY THEN
                FLG = '1'
                BB.DATA    = ""
                BB.DATA    = KEY.LIST<I>:","
                BB.DATA   := DATE.TO:","
                BB.DATA   := INT.AMT:","
* BB.DATA   := "   ":","
                BB.DATA   := CUST.ID:","
                BB.DATA   := "'":DR.ACCT:","
                BB.DATA   := "'":INT.ACCT:","
                BB.DATA   := VAL.DATE:","
                BB.DATA   := INT.RATE:","
                BB.DATA   := CUS.NAME:","
                BB.DATA   := CD.AMT:","
                BB.DATA   := MAT.DATE:","
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOT.INT += INT.AMT
            END ELSE
                FLG ='0'
            END
*****************************************
            SCH.BE4 = SCH.NO - 1
            FOR J = 1 TO SCH.NO
                CD.SCH = R.SCH<LD.SD.CYCLED.DATES>
                ACT.DAYS = "C"
                DATE.TO  = CD.SCH<1,1,J>
                IF J = 1 THEN
**
                    INT.AMT = R.LMM<LD27.COMMITTED.INT>
                    DATE.FROM = R.LMM<LD27.END.PERIOD.INT>
                    BB.DATA    = ""
                    IF FLG NE '1' THEN
                        BB.DATA    = KEY.LIST<I>:","
                    END ELSE
                        BB.DATA    = "   ":","
                    END
                    BB.DATA   := DATE.FROM:","
                    BB.DATA   := INT.AMT:","
*    BB.DATA   := "   ":","
                    IF FLG NE '1' THEN
                        BB.DATA   := CUST.ID:","
                        BB.DATA   := "'":DR.ACCT:","
                        BB.DATA   := "'":INT.ACCT:","
                        BB.DATA   := VAL.DATE:","
                        BB.DATA   := INT.RATE:","
                        BB.DATA   := CUS.NAME:","
                        BB.DATA   := CD.AMT:","
                        BB.DATA   := MAT.DATE:","

                    END ELSE
                        BB.DATA   := "   ":","
                    END
                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                    TOT.INT += INT.AMT
                END
                IF J # SCH.NO THEN
                    DATE.FROM = CD.SCH<1,1,J+1>
                END
                IF J LE SCH.NO THEN
                    IF CD.KEY NE '' THEN
                        CALL CDD("",DATE.TO,DATE.FROM,ACT.DAYS)
                        INT.AMT = CD.AMT * INT.100.RATE * ACT.DAYS / 365
                        CALL EB.ROUND.AMOUNT ('EGP',INT.AMT,'',"")
                    END ELSE
                        ACT.DAYS =   NO.INT.MON / 12
                        INT.AMT  = CD.AMT * INT.100.RATE * ACT.DAYS
                    END
*                   BB.DATA   = ''
*                  BB.DATA    = "      ":","
*                 BB.DATA   : =  DATE.FROM:","
*                BB.DATA   : =  INT.AMT :","

                    IF J EQ SCH.BE4 THEN
                        BB.DATA   = ''
                        BB.DATA    = "      ":","
                        BB.DATA   : =  DATE.FROM:","
                        BB.DATA   : =  INT.AMT :","
                        PRIN.INT  = CD.AMT
*                        BB.DATA   := PRIN.INT :","
*        BB.DATA   : =  CUST.ID :","
                    END ELSE
                        BB.DATA   = ''
                        BB.DATA    = "      ":","
                        BB.DATA   : =  DATE.FROM:","
                        BB.DATA   : =  INT.AMT :","
*       BB.DATA   : =" " :","
*       BB.DATA   : =  CUST.ID :","
                    END

                    IF J EQ SCH.NO THEN
                        BB.DATA =''

                    END

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                    TOT.INT += INT.AMT
                END
            NEXT J
            TOT.AMT += CD.AMT
        NEXT I
        BB.DATA    = ''
        BB.DATA    = "      ":","
        BB.DATA   : = " ":","
        BB.DATA   := TOT.INT :","
        BB.DATA   : = " ":","
        BB.DATA   := CATG :","
        BB.DATA   := TOD.DAT :","
        BB.DATA   := SELECTED:","
        BB.DATA   : = "      ":","
        BB.DATA   : = "      ":","
*      BB.DATA   : = "      ":","
        BB.DATA   := TOT.AMT :","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    END
    RETURN
END
