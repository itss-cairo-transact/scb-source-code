* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*** ����� ����� ����� ***
*** CREATED BY KHALED ***
***=================================

    SUBROUTINE BR.SWIFT.1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK

    COMP = ID.COMPANY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='BR.SWIFT.1'
    CALL PRINTER.ON(REPORT.ID,'')

****YTEXT = "Enter BS.NO : "
****CALL TXTINP(YTEXT, 8, 22, "12", "A")
****BS.ID = COMI

    FN.COMP = 'F.COMPANY' ; F.COMP = ''
    CALL OPF(FN.COMP,F.COMP)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    RETURN
*========================================================================
PROCESS:
*---------------------
    T.SEL = "SELECT F.COMPANY WITH @ID EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.COMP,KEY.LIST,R.COMP,F.COMP,E2)
    ADDR   = R.COMP<EB.COM.NAME.ADDRESS>
    ADDR1   = R.COMP<EB.COM.NAME.ADDRESS>
    SWCODE = R.COMP<EB.COM.SWIFT.ADD.COM.REF>
    BR.ID = ID.NEW
*TEXT = "COMI :" : COMI ; CALL REM
*T.SEL = "SELECT ":FN.BR: " WITH SLIP.REFER EQ ":BS.ID
*CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*IF SELECTED THEN
*TEXT = "SELECTED: " SELECTED ; CALL REM
*FOR I = 1 TO SELECTED

*****CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E1)

    REF       = BR.ID
    AMT       = R.NEW(EB.BILL.REG.AMOUNT)
    TEXT = "AMT : " : AMT ; CALL REM
    CUR.ID    = R.NEW(EB.BILL.REG.CURRENCY)
    CHQ.NO    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO>
    BANK.TO   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>
    BRANCH.ID = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PREV.BR>

    CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.TO,BANK.NAME)
    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

    IN.AMOUNT = AMT
     CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'


    XX   = SPACE(132)   ; XX3  = SPACE(132)
    XX1  = SPACE(132)   ; XX4  = SPACE(132)
    XX2  = SPACE(132)   ; XX5  = SPACE(132)
    XX6  = SPACE(132)   ; XX7  = SPACE(132)
    XX8  = SPACE(132)   ; XX9  = SPACE(132)
    XX10 = SPACE(132)   ; XX11 = SPACE(132)
    XX12 = SPACE(132)   ; XX13 = SPACE(132)
    XX14 = SPACE(132)
*--------------------------------------------------------------
    XX1<1,1>[1,35]  = "Enclose here with the following cheques for collection"

    XX2<1,1>[1,15]  = "AMOUNT":"   : ":AMT:" ":CUR.ID
    XX2<1,1>[30,15] = "DRAWN ON":" : ":"YOURSELES"
    XX<1,1>[1,15]   = "CHEQUE NO":"   : ":" ":CHQ.NO
    XX<1,1>[30,15]  = "OUR REF NO.":" : ":REF

* XX3<1,1>[3,15]  = AMT:" ":CUR.ID
* XX3<1,1>[20,15] = "YOURSELES"
* XX3<1,1>[40,15] = CHQ.NO
* XX3<1,1>[60,15] = REF

    XX4<1,1>[3,35]   = OUT.AMT
    XX5<1,1>[3,35]   = "The attached checks are for collection ,please reimburse us as indicated belo"
    XX6<1,1>[41,35]  = "Send us your checks drawn on ( )"
    XX7<1,1>[50,35]  = "Credit our a/c with ( )"
    XX8<1,1>[50,35]  = "Credit our a/c with ( )"
    XX9<1,1>[50,35]  = "under final payment ( )"
    XX10<1,1>[40,35] = "Authorize us to debit your a/c with us"
    XX11<1,1>[3,35]  = "In case of non payement please advise us by swift"
    XX12<1,1>[10,35] = "SUEZ CANAL BANK"
    XX13<1,1>[10,35] = "YOUR FATHILTY"
    XX14<1,1>[10,35] = "SUEZ CANAL BANK"

*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',1)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"SUZE CANAL BANK"
    PR.HD :="'L'":SPACE(1):"BRANCH : ":YYBRN
****    PR.HD :="'L'":SPACE(1):"SWIFT CODE : SUCAEGCX"
    IF COMP NE 'EG0010013' THEN
        PR.HD :="'L'":SPACE(1):"SWIFT CODE :" : SWCODE
    END
    IF COMP EQ 'EG0010013' THEN
        PR.HD :="'L'":SPACE(1):"SWIFT CODE :" : 'SUCAEGCXGRD'
    END
*****    PR.HD :="'L'":SPACE(1):"11 Mohamed Sabri Abu Alam ,Cairo"
    PR.HD :="'L'":SPACE(1):ADDR
***  PR.HD :="'L'":SPACE(1):ADDR1
**** PR.HD :="'L'":SPACE(1):"P.O.BOX : 2620"
    PR.HD :="'L'":SPACE(1):"DATE : ":T.DAY
    PR.HD :="'L'":SPACE(50):"TO : ":BANK.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX<1,1>
* PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT XX7<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
*NEXT I
*END
*===============================================================
    RETURN
END
