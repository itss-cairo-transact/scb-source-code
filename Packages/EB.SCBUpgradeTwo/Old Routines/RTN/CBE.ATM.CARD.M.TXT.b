* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*****NESSREEN AHMED 21/1/2019***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CBE.ATM.CARD.M.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMP.LOCAL.REFS

    TEXT = 'HELLO' ; CALL REM

    F.ATM.APP = '' ; FN.ATM.APP = 'F.SCB.ATM.APP' ; R.ATM.APP= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.ATM.APP,F.ATM.APP)

    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)


    ID.KEY = '' ; MBR = '' ; CR.CARD.NO = '' ; X = 0  ; LST.ST.D = '' ; XX = 0  ; MM = 0
    ST.D = '' ;  ED.D = ''

    YTEXT = "Enter Start.Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.D = COMI
    YTEXT = "Enter End.Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ED.D = COMI
**********OPEN FILE********************************
    OPENSEQ "&SAVEDLISTS&" , "CUST.DEBIT.CARD.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CUST.DEBIT.CARD.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CUST.DEBIT.CARD.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CUST.DEBIT.CARD.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create CUST.DEBIT.CARD.csv File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
    BB.DATA = " CardId ":",":"TypeId":",":"CardSchemaId":",":"BranchId":",":"CurrencyId":",":"AccountId":",":"openingDate":",":"NationalId":",":"secondaryId":",":"secondaryIdType":",":"parentCardId":",":"parentNationalId":",":"parentsecondaryId":",":"parentsecondaryIdType":",":"closingDate":",":"statusId":",":"statusReason"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    S.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC... AND CANCELLATION.DATE EQ '' AND (ISSUE.DATE GE ": ST.D :" AND ISSUE.DATE LE " : ED.D : " ) BY ACCOUNT "
    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)
    TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM
    IF SELECTED.SS THEN
        FOR I = 1 TO SELECTED.SS
            CR.CARD.NO = '' ; ACC.NO = '' ; BR.CODE = '' ; INT.ACC = '' ; CUST.ACCT = ''  ; CARD.CODE = '' ; P.CUST = ''  ; P.NSN.NO = '' ; P.SECOND.ID = '' ; ID.TYPE = ''
            P.ID.TYPE.CO = '' ; P.CR.CARD.NO = '' ; P.BR.CODE = ''   ; P.ISSUE.DATE = '' ; P.CBE.COMP.CODE = ''
            S.CR.CARD.NO = '' ; S.ISSUE.DATE = '' ; S.CBE.COMP.CODE = ''  ; CUS.NAT = ''  ; P.REL.CUS = '' ; S.NSN.NO = ''
            P.CUST = '' ; SECOND.ID = '' ; NSN = '' ; CUS.NAT = ''  ; S.SECOND.ID = '' ; S.ID.TYPE.CO = ''
            CALL F.READ(FN.CARD.ISSUE,KEY.LIST.SS<I>, R.CARD.ISSUE, F.CARD.ISSUE ,E3)
            LOCAL.REF      = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            CR.CARD.NO<I>  = LOCAL.REF<1,LRCI.VISA.NO>
            ACC.NO<I>      = R.CARD.ISSUE<CARD.IS.ACCOUNT><1,1>
            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACC.NO<I>, P.CUST)
            CALL F.READ(FN.CUSTOMER, P.CUST, R.CUSTOMER, F.CUSTOMER , ERR.CUST)
            CUS.NAT = R.CUSTOMER<EB.CUS.NATIONALITY>
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
            NSN = LOCAL.REF.C<1,CULR.NSN.NO>
            SECOND.ID = LOCAL.REF.C<1,CULR.ID.NUMBER>
            IF (CUS.NAT EQ 'EG') AND (NSN NE '' ) THEN
                IF NSN[1,2] NE '00' THEN
                    CUST.ACCT      = ACC.NO<I>[1,10]:"..."
                    BR.CODE<I>     = R.CARD.ISSUE<CARD.IS.CO.CODE>
                    CARD.CODE<I>   = LOCAL.REF<1,LRCI.CARD.CODE>
                    ISSUE.DATE<I>  = R.CARD.ISSUE<CARD.IS.ISSUE.DATE>
                    IF (CARD.CODE<I> EQ 901) OR ((CARD.CODE<I> GE 801) AND (CARD.CODE<I> LT 805)) THEN
****#####In Case of Main Cards####********
                        P.CR.CARD.NO = CR.CARD.NO<I>
                        P.ISSUE.DATE = ISSUE.DATE<I>
                        PR.BR.CODE = BR.CODE<I>
                        CALL DBR( 'COMPANY':@FM:EB.COM.LOCAL.REF,PR.BR.CODE, CO.LOCAL.REF)
                        P.CBE.COMP.CODE = CO.LOCAL.REF<1,CMP.CBE.BR.CODE>
                        CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACC.NO<I>, P.CUST)
                        CALL F.READ(FN.CUSTOMER, P.CUST, R.CUSTOMER, F.CUSTOMER , ERR.CUST)
                        LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
                        IF LOCAL.REF.C<1,CULR.NSN.NO> # '' THEN
                            P.NSN.NO = LOCAL.REF.C<1,CULR.NSN.NO>
                            P.SECOND.ID = ''
                            P.ID.TYPE.CO = ''
                        END ELSE
                            P.NSN.NO = ''
                            P.SECOND.ID = LOCAL.REF.C<1,CULR.ID.NUMBER>
                            P.ID.TYPE =  LOCAL.REF.C<1,CULR.ID.TYPE>
                            BEGIN CASE
                            CASE P.ID.TYPE = 3
                                P.ID.TYPE.CO = 1
                            CASE P.ID.TYPE = 6
                                P.ID.TYPE.CO = 2
                            CASE P.ID.TYPE = 5
                                P.REL.CUS = R.CUSTOMER<EB.CUS.REL.CUSTOMER>
                                CALL F.READ(FN.CUSTOMER, P.REL.CUS, R.CUSTOMER, F.CUSTOMER , ERR.CUST)
                                LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
                                P.NSN.NO = LOCAL.REF.C<1,CULR.NSN.NO>
                            CASE OTHERWISE
                                P.ID.TYPE.CO = 3
                            END CASE
                        END
                        BB.DATA = P.CR.CARD.NO:",":"1":",":"2":",":P.CBE.COMP.CODE :",":'EGP':",":ACC.NO<I>:",":P.ISSUE.DATE:",":P.NSN.NO:",":P.SECOND.ID:",":P.ID.TYPE.CO:",":' ':",":' ':",":' ':",":' ':",":' ':",":'1':",":' '
                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END
****#####In Case of Supplementary Cards####********
                    END ELSE  ;*Else of Main Cards code**
                        IF ((CARD.CODE<I> EQ 810) OR (CARD.CODE<I> EQ 811)) THEN
                            S.CR.CARD.NO = CR.CARD.NO<I>
                            S.ISSUE.DATE = ISSUE.DATE<I>
                            SE.BR.CODE = BR.CODE<I>
                            CALL DBR( 'COMPANY':@FM:EB.COM.LOCAL.REF,SE.BR.CODE, CO.LOCAL.REF)
                            S.CBE.COMP.CODE = CO.LOCAL.REF<1,CMP.CBE.BR.CODE>
                            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER,ACC.NO<I>, P.CUST)
                            CALL F.READ(FN.CUSTOMER, P.CUST, R.CUSTOMER, F.CUSTOMER , ERR.CUST)
                            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
                            IF LOCAL.REF.C<1,CULR.NSN.NO> # '' THEN
                                IF S.NSN.NO[1,2] NE '00' THEN
                                    P.NSN.NO = LOCAL.REF.C<1,CULR.NSN.NO>
                                    P.SECOND.ID = ''
                                    P.ID.TYPE.CO = ''
*****************************************************
** PR.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC... AND CANCELLATION.DATE EQ '' AND (CARD.CODE GE 801 AND CARD.CODE LT 810) AND ACCOUNT EQ " : ACC.NO<I>
*****UPDATED BY NESSREEN AHMED 11/2/2020**********************************************************************************************************************
***                                 PR.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC... AND CANCELLATION.DATE EQ '' AND ((CARD.CODE GE 801 AND CARD.CODE LT 810) OR (CARD.CODE EQ 901)) AND ACCOUNT EQ " : ACC.NO<I>
                                    PR.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC... AND CANCELLATION.DATE EQ '' AND ((CARD.CODE GE 801 AND CARD.CODE LT 810) OR (CARD.CODE EQ 901)) AND ACCOUNT EQ " : ACC.NO<I> : " AND (ISSUE.DATE GE ": ST.D :" AND ISSUE.DATE LE " : ED.D : " ) BY ACCOUNT "
                                    KEY.LIST.PR = '' ; SELECTED.PR = '' ; ER.MSG.PR = ''
                                    CALL EB.READLIST(PR.SEL , KEY.LIST.PR,'',SELECTED.PR,ER.MSG.PR)
                                    IF SELECTED.PR THEN
                                        CALL EB.READLIST(PR.SEL,KEY.LIST.PR,"",SELECTED.PR,ER.MSG.PR)
                                        CALL F.READ(FN.CARD.ISSUE,KEY.LIST.PR<1>, R.CARD.ISSUE, F.CARD.ISSUE ,E4)
                                        LOCAL.REF      = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                                        P.CARD.NO       = LOCAL.REF<1,LRCI.VISA.NO>
                                    END
                                    T.SEL = "SELECT F.SCB.ATM.APP WITH FAST.ACC LIKE " :CUST.ACCT  : " AND CARD.TYPE EQ " : CARD.CODE<I>
                                    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
                                    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
                                    CUST.NO = ''
                                    IF SELECTED THEN
                                        CALL F.READ(FN.ATM.APP,KEY.LIST<1>, R.ATM.APP, F.ATM.APP ,E1)
                                        CUST.NO = R.ATM.APP<SCB.VISA.CUSTOMER>
                                        CALL F.READ(FN.CUSTOMER, CUST.NO, R.CUSTOMER, F.CUSTOMER , ERR.CUST)
                                        LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
                                        IF LOCAL.REF.C<1,CULR.NSN.NO> # '' THEN
                                            S.NSN.NO = LOCAL.REF.C<1,CULR.NSN.NO>
                                            IF S.NSN.NO[1,2] NE '00' THEN

                                                BB.DATA = S.CR.CARD.NO:",":'1':",":'2':",":S.CBE.COMP.CODE :",":'EGP':",":ACC.NO<I>:",":S.ISSUE.DATE:",":S.NSN.NO:",":S.SECOND.ID:",":S.ID.TYPE.CO:",":P.CARD.NO:",":P.NSN.NO:",":P.SECOND.ID:",":P.ID.TYPE.CO:",":' ':",":'1'
                                                WRITESEQ BB.DATA TO BB ELSE
                                                    PRINT " ERROR WRITE FILE "
                                                END
                                            END   ;*End of S.NSN.NO[1,2] NE '00'
                                        END       ;*End of LOCAL.REF.C<1,CULR.NSN.NO>
                                    END ;*End of IF SELECTED
                                END     ;*End of S.NSN.NO[1,2] NE '00'
                            END         ;*End of LOCAL.REF.C<1,CULR.NSN.NO> NE ''
                        END   ;***End of Main Cards **IF (CARD.CODE<I> EQ 810) OR (CARD.CODE<I> EQ 811)
                    END       ;***End of ELSE Main Cards ****IF (CARD.CODE<I> GE 801) AND (CARD.CODE<I> LT 810)
                END ;***End of NSN NE '00000000000'
            END ; *End of Egyptian Customer
        NEXT I
    END ; ***End of main selection from Card.Issue***
    TEXT = '�� �������� �� �������� ' ;CALL REM;
 RETURN
END
