* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>78</Rating>
*-----------------------------------------------------------------------------
      SUBROUTINE CHECK.VERSION.VLD.RTNS(VER.NAME, ER.MSG)

* Written by V.Papadopoulos for INFORMER S.A.

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

      ER.MSG = '' ; ETEXT = '' ; VAL.RTNS = '' ; VAL.FLDS = ''

*Line [ 34 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CALL DBR('VERSION':@FM:EB.VER.VALIDATION.RTN, VER.NAME, VAL.RTNS)
*Line [ 36 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CALL DBR('VERSION':@FM:EB.VER.VALIDATION.FLD, VER.NAME, VAL.FLDS)
      IF NOT(ETEXT) THEN
         IF VAL.RTNS THEN
            I.A = 1
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOOP WHILE I.A <= DCOUNT(VAL.FLDS<1>, @VM) & NOT(ER.MSG)
               SAVE.MESSAGE = MESSAGE
               MESSAGE = ''
               GOSUB RUN.VLD.RTN
               MESSAGE = 'VAL'
               GOSUB RUN.VLD.RTN
               MESSAGE = SAVE.MESSAGE
               I.A += 1
            REPEAT
         END
      END
      ELSE
         ER.MSG = ETEXT:' .. ':VER.NAME
         ETEXT = ''
      END

      GOTO PROGRAM.END

*------------------------------------------------------------------------------

*--------------- RUN VALIDATION ROUTINE ---------------------------------------
RUN.VLD.RTN:

      FLD.NUM = ''
      CALL GET.SS.FIELD('FUNDS.TRANSFER', VAL.FLDS<1, I.A>, FLD.NUM, 'NUM', ER.MSG)
      IF NOT(ER.MSG) & FLD.NUM THEN
         TELIA = ''
         TELIA = INDEX(FLD.NUM, '.', 1)

         IF TELIA THEN
*Line [ 72 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            F.@FM = FLD.NUM[1, TELIA - 1]
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            F.@VM = FLD.NUM[TELIA + 1, LEN(FLD.NUM) - TELIA]
            TELIA = ''
*Line [ 77 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            TELIA = INDEX(F.@VM, '.', 1)
            IF TELIA THEN
*Line [ 80 ] Add @VM Instead Of VM & Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
               F.@SM = F.@VM[TELIA + 1, LEN(F.@VM) - TELIA]
*Line [ 82 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
               F.@VM = F.@VM[1, TELIA - 1]
            END
            ELSE
*Line [ 86 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
               F.@SM = ''
            END
         END
         ELSE
*Line [ 91 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            F.@FM = FLD.NUM
*Line [ 93 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            F.@VM = ''
*Line [ 95 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            F.@SM = ''
         END

*Line [ 99 ] Add @VM Instead Of VM & Add @FM Instead Of FM & Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
         COMI = R.NEW(F.FM)<1, F.@VM, F.@SM>
         SAVE.COMI = COMI
         CALL.ROUTINE = ''
         CALL.ROUTINE = VAL.RTNS<1, I.A>
         CALL @CALL.ROUTINE
         IF NOT(ETEXT) THEN
            IF SAVE.COMI # COMI THEN
*Line [ 107 ] Add @VM Instead Of VM & Add @FM Instead Of FM & Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
               R.NEW(F.FM)<1, F.@VM, F.@SM> = COMI
            END
         END
         ELSE
            ER.MSG = ETEXT
            ETEXT = ''
         END
      END
      ELSE
         ER.MSG = 'INVALID FIELD NAME'
      END

      RETURN
*------------------------------------------------------------------------------

*------------------ SUBROUTINE END ... YES !!! --------------------------------
PROGRAM.END:

      RETURN

   END
