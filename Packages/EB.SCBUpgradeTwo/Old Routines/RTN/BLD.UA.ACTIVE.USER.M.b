* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>38</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.UA.ACTIVE.USER.M(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*---------------------------------------------------
    WS.COMP = ID.COMPANY[8,2]
    IF WS.COMP[1,1] EQ 0 THEN
        WS.COMP = ID.COMPANY[9,1]
    END
    ENQ.LP  = 0
    FN.US   = 'F.USER'
    FN.UA   = 'F.USER.ABBREVIATION'
    F.US    = '' ; R.US  = ''
    F.UA    = '' ; R.UA = ''
    CALL OPF(FN.US,F.US)
    CALL OPF(FN.UA,F.UA)
*--------
   T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND ORIGINAL.TEXT EQ ?420 AND ORIGINAL.TEXT EQ ?421 BY DEPARTMENT.CODE "
*  T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND END.DATE.PROFILE GE ":TODAY:" AND DEPARTMENT.CODE EQ 60  BY DEPARTMENT.CODE "
*  T.SEL2  = "SELECT ":FN.UA:" WITH @ID LIKE SCB... AND END.DATE.PROFILE GE ":TODAY:" AND DEPARTMENT.CODE EQ ":WS.COMP:" BY DEPARTMENT.CODE "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED1,ER.MSG)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL DBR('USER':@FM:EB.USE.SIGN.ON.NAME,KEY.LIST2<I>,WS.USER.LIVE)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,KEY.LIST2<I>,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF (( WS.SCB.DEPT NE 2800 ) AND ( WS.SCB.DEPT NE 2700 ) AND ( WS.SCB.DEPT NE 5100 )) THEN
                IF WS.USER.LIVE NE '' THEN
                    ENQ.LP ++
                    ENQ.DATA<2,ENQ.LP> = '@ID'
                    ENQ.DATA<3,ENQ.LP> = 'EQ'
                    ENQ.DATA<4,ENQ.LP> = KEY.LIST2<I>
                END
            END
        NEXT I
        IF ENQ.LP EQ 0 THEN
            ENQ.DATA<2,2> = '@ID'
            ENQ.DATA<3,2> = 'EQ'
            ENQ.DATA<4,2> = 'DUUMY'
        END 
    END ELSE
        ENQ.DATA<2,2> = '@ID'
        ENQ.DATA<3,2> = 'EQ'
        ENQ.DATA<4,2> = 'DUUMY'
    END 
    RETURN
END
