* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.SCBBOX02(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


    KEY.LIST="" ;  SELECTED="" ;  ER.MSG=""

    FN.AC ='FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    LOCATE "PREV.BAL" IN ENQ<2,1> SETTING CAT.POS THEN
        TRN.BAL  = ENQ<4,CAT.POS>
    END

    LOCATE "ACCOUNT.NUMBER" IN ENQ<2,1> SETTING POS THEN
        ACCT.NO = ENQ<4,POS>
    END

    CALL F.READ(FN.AC,ACCT.NO,R.AC,F.AC,E1)
    WS.WORKING.BAL = R.AC<AC.WORKING.BALANCE>

    BLOCK.BALANCE  = R.AC<AC.LOCKED.AMOUNT>
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BLOCK.NO       = DCOUNT(BLOCK.BALANCE,@VM)
    WS.LOCKED.BAL  = BLOCK.BALANCE<1,BLOCK.NO>

    AVL.BALANCE    = R.AC<AC.AVAILABLE.BAL>
*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AVAL.NO        = DCOUNT(AVL.BALANCE,@VM)
    WS.AVAL.BAL    = AVL.BALANCE<1,AVAL.NO>

    IF WS.AVAL.BAL NE '' THEN
        IF WS.AVAL.BAL LT WS.WORKING.BAL THEN
            WS.BAL = WS.AVAL.BAL - WS.LOCKED.BAL
        END ELSE
            WS.BAL = WS.WORKING.BAL - WS.LOCKED.BAL
        END
    END ELSE
        WS.BAL = WS.WORKING.BAL - WS.LOCKED.BAL
    END

    IF WS.BAL GE TRN.BAL THEN
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = ACCT.NO
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
    RETURN
END
