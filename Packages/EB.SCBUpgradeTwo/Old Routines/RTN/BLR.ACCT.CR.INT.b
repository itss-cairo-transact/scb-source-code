* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******************MAHMOUD 10/3/2013********************
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLR.ACCT.CR.INT(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.CR' ; F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ACD = 'FBNK.ACCOUNT.DATE' ; F.ACD = ''
    CALL OPF(FN.ACD,F.ACD)
    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    KK1 = 0
    TTD1 = '20120101'

    AD.SEL = "SSELECT ":FN.ACD:" WITH CREDIT.DATES NE ''"
    CALL EB.READLIST(AD.SEL, K.LIST, "", SELECTED, ERR.CUS)
    LOOP
        REMOVE ACC.ID FROM K.LIST SETTING POS.ACCT
    WHILE ACC.ID:POS.ACCT
        CALL F.READ(FN.ACD,ACC.ID,R.ACD,F.ACD,EACD2)
*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CNT.CR.DT = DCOUNT(R.ACD<AC.DTE.CREDIT.DATES>,@VM)
        FOR AA1 = 1 TO CNT.CR.DT
            AC.CR.DATE = R.ACD<AC.DTE.CREDIT.DATES,AA1>
            IF AC.CR.DATE GE TTD1 THEN
                ACI.ID = ACC.ID:"-":AC.CR.DATE
                CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,ERR2)
                IF NOT(ERR2) THEN
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = ACI.ID
                END ELSE
                    KK1++
                    GOSUB NODATA
                END
            END ELSE
                KK1++
                GOSUB NODATA
            END
        NEXT AA1
    REPEAT
    RETURN
********************************************
NODATA:
*------
    ENQ<2,KK1> = "@ID"
    ENQ<3,KK1> = "EQ"
    ENQ<4,KK1> = "NOLIST"
    RETURN
********************************************
END
