* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.TF.STMT(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    I = 1

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE= ''
    CALL OPF( FN.STE,F.STE)

    YTEXT = "ENTER ACCOUNT NUMBER"
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    AC.ID   = COMI

    YTEXT = "ENTER FROM DATE"
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DAT.FROM = COMI

    YTEXT = "ENTER TO DATE"
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DAT.TO   = COMI

    CALL EB.ACCT.ENTRY.LIST(AC.ID<1>,DAT.FROM,DAT.TO,STMT.ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM STMT.ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE

        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)

        IF NOT(ER.STE) THEN
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = STE.ID
        END ELSE
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = 'DUMMY'
        END
        I++
    REPEAT

    RETURN
END
