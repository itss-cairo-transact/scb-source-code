* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******NESSREEN AHMED*******
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BR.AUTH.INPU(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

*A Routine to select only the inputter name or authorizer name from Inputter or Authorizer field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

* INPUTTER = (ARG)
* ARG = FIELD(INPUTTER,'_',2)

*   TEXT = 'HI' ; CALL REM
*   TEXT = ARG ; CALL REM
*    CALL DBR('SCB.BR.SLIP':@FM:SCB.BS.INPUTTER,ARG,INP)
*    TEXT = INP ; CALL REM
    INP = R.NEW(SCB.BS.AUTHORISER)
    ARG = FIELD(INP,'_',2)
*    TEXT = "INP":ARG ; CALL REM


    RETURN
END
