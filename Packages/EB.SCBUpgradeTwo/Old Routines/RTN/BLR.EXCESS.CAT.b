* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.EXCESS.CAT(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.POS = 'F.SCB.TRANS.TODAY'
    F.POS = ''
    R.POS = ''
    CALL OPF(FN.POS,F.POS)
    TT = TODAY
    KK1 = 0
****************************UPDATE BY REHAM*******************
    T.SEL = "SELECT ":FN.POS:" WITH BOOKING.DATE EQ ":TT:" AND COMPANY.CO EQ ":COMP
    T.SEL := " AND CUSTOMER.ID NE '' AND ACCOUNT.NUMBER NE '' "
    T.SEL := " AND ( ( PRODUCT.CATEGORY GE 1101  AND PRODUCT.CATEGORY LE 1599 )"
    T.SEL := " OR  ( PRODUCT.CATEGORY GE 21001 AND PRODUCT.CATEGORY LE 21010 )"
    T.SEL := " OR  ( PRODUCT.CATEGORY GE 23000 AND PRODUCT.CATEGORY LE 23799 )"
    T.SEL := " OR (PRODUCT.CATEGORY IN (1001 1002 1205 1208 1015 21053 21096 21021 3005 3011 3012 50054 9002)))"

    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            KK1 += 1
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = K.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
END
