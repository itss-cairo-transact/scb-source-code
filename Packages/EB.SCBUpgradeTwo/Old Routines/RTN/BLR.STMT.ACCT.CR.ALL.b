* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
******************MAHMOUD 14/11/2011********************
    SUBROUTINE BLR.STMT.ACCT.CR.ALL(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*    $INCLUDE T24.BP I_F.ACCOUNT.DATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.CR' ; F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ACD = 'FBNK.ACCOUNT.DATE' ; F.ACD = ''
    CALL OPF(FN.ACD,F.ACD)
    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    YTEXT = "Enter End of Month Date YYYYMMDD :"
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    TT = COMI
    IF NOT(TT) THEN
        GOSUB NODATA
        RETURN
    END

    IF LEN(TT) NE 8 THEN
        GOSUB NODATA
        RETURN
    END

    RR = TT
    KK1 = 1

    CALL LAST.DAY(TT)
    IF RR NE TT THEN
        CALL ADD.MONTHS(TT,'-1')
    END

    ENQ<2,1> = "@ID"
    ENQ<3,1> = "EQ"
    ENQ<4,1> = TT

    AD.SEL = "SSELECT ":FN.ACD:" WITH CREDIT.DATES NE ''"
    CALL EB.READLIST(AD.SEL, K.LIST, "", SELECTED, ERR.CUS)
    LOOP
        REMOVE ACC.ID FROM K.LIST SETTING POS.ACCT
    WHILE ACC.ID:POS.ACCT
        GOSUB ACI.REC
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,EACC2)
        AC.CUR = R.ACC<AC.CURRENCY>
        AC.CAT = R.ACC<AC.CATEGORY>
***IF AC.CAT EQ 1001 THEN
            SCR.ID = ACC.ID:"-":TT
            CALL F.READ(FN.SCR,SCR.ID,R.SCR,F.SCR,ERR2)
***IF AC.CUR EQ LCCY THEN
                IF NOT(ERR2) THEN
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = SCR.ID
                END ELSE
                    KK1++
                    GOSUB NODATA
                END
***END ELSE
***KK1++
***GOSUB NODATA
***END
***END ELSE
***KK1++
***GOSUB NODATA
***END
    REPEAT
    RETURN
********************************************
NODATA:
*------
    ENQ<2,KK1> = "@ID"
    ENQ<3,KK1> = "EQ"
    ENQ<4,KK1> = "NOLIST"
    RETURN
********************************************
ACI.REC:
*-------
    CALL F.READ(FN.ACD,ACC.ID,R.ACD,F.ACD,EDDD)
    ADI.DAT = R.ACD<AC.DTE.CREDIT.DATES>
*Line [ 116 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD1 = DCOUNT(ADI.DAT,@VM)
    FOR AA1 = 1 TO DD1
        AC.DT = R.ACD<AC.DTE.CREDIT.DATES,AA1>
        IF AC.DT GT TT THEN
            SS1 = AA1 - 1
            AC.DT = R.ACD<AC.DTE.CREDIT.DATES,SS1>
            AA1 = DD1
        END
    NEXT AA1
    ACI.ID = ACC.ID:"-":AC.DT
    CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,EACI1)
    ACI.BAS  = R.ACI<IC.ACI.INTEREST.DAY.BASIS>
    IF ACI.BAS EQ 'GENERAL' THEN
        ACC.ID = 0
    END
    RETURN
********************************************
END
