* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.STMT.TRN.CR(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
********ADDED BY MAHMOUD 5/10/2010*************
    COMP = ID.COMPANY
***********************************************
    LOCATE "@ID" IN ENQ<2,1> SETTING TT.POS THEN
        TT.ID = ENQ<4,TT.POS>
    END
***********************************************
    FN.ST = 'FBNK.STMT.ACCT.CR' ; F.ST = ''
    CALL OPF(FN.ST,F.ST)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
**    TD = TODAY
********UPDATED BY MAHMOUD 5/10/2010*************
** TD = "20091231"
    TD   = TODAY
    TDXX = TD[1,6]:'01'
    CALL CDT("",TDXX,'-1C')
    TD = TDXX
*************************************************
    TDD = "...":TD
****TDD = "...":'-20100131'
    T.SEL  = "SELECT FBNK.STMT.ACCT.CR WITH @ID LIKE ":TDD:" AND TOTAL.INTEREST NE ''"
********ADDED BY MAHMOUD 5/10/2010*************
  ******  T.SEL := " AND COMPANY.CODE EQ ":COMP
***********************************************
*********T.SEL = "SELECT FBNK.STMT.ACCT.CR WITH @ID IN ('0110125310100101-20091231' '0110125410100101-20091231')"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**    TEXT = "SELECTED : " : SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ST,KEY.LIST<I>,R.ST,F.ST,E2)
            CR.ID = KEY.LIST<I>
            ACC.NO = FIELD(CR.ID,'-',1)
            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SECTOR)
********ADDED BY MAHMOUD 10/10/2010*************
            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE ,ACC.NO,ACC.COM)
            IF ACC.COM EQ COMP THEN
***********************************************
             **   IF (SECTOR NE '' AND SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300 AND SECTOR NE 1400) AND (CATEG.ID EQ 1001 OR CATEG.ID EQ 2000 OR CATEG.ID EQ 2001 OR CATEG.ID EQ 3005) THEN
                    ENQ<2,I> = '@ID'
                    ENQ<3,I> = 'EQ'
                    ENQ<4,I> = KEY.LIST<I>
              **  END ELSE
                 *   ENQ<2,I> = '@ID'
                 *   ENQ<3,I> = 'EQ'
                 *   ENQ<4,I> = 'DUMMY'
               * END
********ADDED BY MAHMOUD 10/10/2010*************
           * END ELSE
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'
            END
************************************************
        NEXT I
    END
    RETURN
END
