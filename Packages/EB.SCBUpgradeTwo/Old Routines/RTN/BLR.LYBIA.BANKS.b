* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*********************MAHMOUD 5/7/2012***********************
    SUBROUTINE BLR.LYBIA.BANKS(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KK = 0
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    FN.CU = "FBNK.CUSTOMER" ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = "FBNK.ACCOUNT" ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU.AC = "FBNK.CUSTOMER.ACCOUNT" ; F.CU.AC = '' ; R.CU.AC = '' ; ER.CU.AC = ''
    CALL OPF(FN.CU.AC,F.CU.AC)

    T.SEL  = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'LY' "
    T.SEL := " AND POSTING.RESTRICT LT 90 "
    T.SEL := " AND ( SECTOR GE 3000 AND SECTOR LE 3999 )"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE CU.ID FROM KEY.LIST SETTING POS.CU
        WHILE CU.ID:POS.CU
            CALL F.READ(FN.CU.AC,CU.ID,R.CU.AC,F.CU.AC,ER.CU.AC)
            LOOP
                REMOVE AC.ID FROM R.CU.AC SETTING POS.AC
            WHILE AC.ID:POS.AC
                CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
                AC.CAT = R.AC<AC.CATEGORY>
                KK++
                IF AC.CAT NE 5001 AND AC.CAT NE 9002 THEN
                    ENQ<2,KK> = "@ID"
                    ENQ<3,KK> = "EQ"
                    ENQ<4,KK> = AC.ID
                END ELSE
                    ENQ<2,KK> = "@ID"
                    ENQ<3,KK> = "EQ"
                    ENQ<4,KK> = "NOLIST"
                END
            REPEAT
        REPEAT
    END ELSE
        ENQ.ERROR = "NO RECORDS"
    END
******************************************************************************************
    RETURN
END
