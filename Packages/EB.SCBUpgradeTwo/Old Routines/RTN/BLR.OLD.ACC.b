* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.OLD.ACC(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KK1 = 0

    T.SEL  = "SELECT ":FN.ACC:" WITH (ACTIVIYTY EQ ''"
    T.SEL := " OR OLD.ACCOUNT EQ '' OR ALT.ACCT.ID EQ ''"
    T.SEL := " OR ALT.ACCT.ID NE CA...)"
    T.SEL := " AND WORKING.BALANCE NE 0 AND WORKING.BALANCE NE ''"
    T.SEL := " AND ONLINE.ACTUAL.BAL NE ''"
    T.SEL := " WITHOUT CATEGORY IN (1001 1002 6501 6502 6503 6504 6505 1053 12001 14820 11257) AND CO.CODE EQ ":COMP

    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            KK1 += 1
            ENQ<2,KK1> = "@ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = K.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = " NO DATA FOUND "
    END
    RETURN
END
