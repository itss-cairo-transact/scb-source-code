* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.CUS.POS.CRT.MXX(ENQ)
****    PROGRAM BLR.CUS.POS.CRT.MXX
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.POS = "F.SCB.CUS.POS" ; F.POS = "" ; R.POS = "" ; ERR.POS = ""
    CALL OPF(FN.POS,F.POS)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    KK1 = 0
    TDD = '20091130'
*----------------------------------------------------
    SEL.CMD = "GET.LIST CUS.LD.REN.20091130"
    CALL EB.READLIST(SEL.CMD,K.LIST,'',SELECTED,ACCT.ERR)
    LOOP
        REMOVE CUS.ID FROM K.LIST SETTING POS
    WHILE CUS.ID:POS
        P.SEL  = "SELECT ":FN.POS:" WITH @ID LIKE ":CUS.ID:"*...*":TDD
        P.SEL := " AND DEAL.AMOUNT NE '' AND DEAL.AMOUNT NE 0"
        P.SEL := " WITHOUT CATEGORY IN (1002 1710 1711 9090 1220 1221 1222 1223 1224 1225 1226 1227)" 
        CALL EB.READLIST(P.SEL,SEL.LIST,'',SELECTED1,ERR11)
        LOOP
            REMOVE CUS.POS.ID FROM SEL.LIST SETTING POS2
        WHILE CUS.POS.ID:POS2
*            CALL F.READ(FN.POS,CUS.POS.ID,R.POS,F.POS,ERR.POS)
*            CRT CUS.ID:" / ":CUS.POS.ID
            KK1++
            ENQ<2,KK1> = "ID"
            ENQ<3,KK1> = "EQ"
            ENQ<4,KK1> = CUS.POS.ID
        REPEAT
    REPEAT
END
