* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.USER.DATE.TIME(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------

    FN.USR = 'F.USER' ; F.AC = ''
    CALL OPF(FN.USR,F.USR)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CHK.DATE = TODAY
    CALL CDT('', CHK.DATE, '-180C')
TEXT = "Check Date Since = ":CHK.DATE ; CALL REM
    T.SEL = "SELECT ":FN.USR:" WITH DATE.LAST.SIGN.ON LE ":CHK.DATE:" AND @ID UNLIKE AUTHOR... AND @ID UNLIKE INPUTT... BY DEPARTMENT.CODE " ;*BY SCB.DEPT.CODE"
*    T.SEL = "SELECT ":FN.USR:" WITHOUT SCB.DEPT.CODE IN ( 2700 2710 2730 250 2800 '' ) "
*    T.SEL := "AND WITH DATE.LAST.SIGN.ON LE ":CHK.DATE:" BY DEPARTMENT.CODE BY SCB.DEPT.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
TEXT = "No. Of Records = ":SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,I> = '@ID'
        ENQ<3,I> = 'EQ'
        ENQ<4,I> = 'DUMMY'
    END

    RETURN
END
