* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*************************NI7OOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.APPROVE.NAU

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1

    FN.SAMP = 'F.SCB.DEPT.SAMPLE1$NAU' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)

    XX = O.DATA
    CALL F.READ(FN.SAMP,O.DATA,R.SAMP,F.SAMP,E11)
    APP1 = R.SAMP<DEPT.SAMP.REQ.STA.HWALA>
    APP2 = R.SAMP<DEPT.SAMP.REQ.STA.LG11>
    APP3 = R.SAMP<DEPT.SAMP.REQ.STA.LG22>
    APP4 = R.SAMP<DEPT.SAMP.REQ.STA.TEL>
    APP5 = R.SAMP<DEPT.SAMP.REQ.STA.TF1>
    APP6 = R.SAMP<DEPT.SAMP.REQ.STA.TF2>
    APP7 = R.SAMP<DEPT.SAMP.REQ.STA.TF3>
    APP8 = R.SAMP<DEPT.SAMP.REQ.STA.BR>
    APP9 = R.SAMP<DEPT.SAMP.REQ.STA.AC>
    APP10 = R.SAMP<DEPT.SAMP.REQ.STA.WH>

    IF APP1 EQ 'APPROVED' THEN
        O.DATA = APP1
    END
    IF APP2 EQ 'APPROVED' THEN
        O.DATA = APP2
    END
    IF APP3 EQ 'APPROVED' THEN
        O.DATA = APP3
    END
    IF APP4 EQ 'APPROVED' THEN
        O.DATA = APP4
    END
    IF APP5 EQ 'APPROVED' THEN
        O.DATA = APP5
    END
    IF APP6 EQ 'APPROVED' THEN
        O.DATA = APP6
    END
    IF APP7 EQ 'APPROVED' THEN
        O.DATA = APP7
    END
    IF APP8 EQ 'APPROVED' THEN
        O.DATA = APP8
    END
    IF APP9 EQ 'APPROVED' THEN
        O.DATA = APP9
    END
    IF APP10 EQ 'APPROVED' THEN
        O.DATA = APP10
    END


    RETURN
END
