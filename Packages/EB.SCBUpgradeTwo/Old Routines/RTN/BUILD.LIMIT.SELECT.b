* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BUILD.LIMIT.SELECT(ENQ)


*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT


    LIM.SEL = 'SELECT FBNK.LIMIT WITH LIMIT.PRODUCT IN ( 5700 3000 1010 1020 1030 ) AND @ID LIKE 994... AND NOTES UNLIKE ...CREATE...DEFAULT... BY LIABILITY.NUMBER BY LIMIT.CURRENCY BY @ID'
    LIM.KEY.LIST = '' ; LIM.SELECTED = '' ; ER.LIM = '' ; LIM.NOTES = ''
    FLAG = 0
    CALL EB.READLIST(LIM.SEL,LIM.KEY.LIST,"",LIM.SELECTED,ER.LIM)
    J = 1
    IF LIM.SELECTED THEN
        FOR I = 1 TO LIM.SELECTED

            FLAG = 1
            CALL DBR('LIMIT':@FM:LI.NOTES,LIM.KEY.LIST<I>,LIM.NOTES) 

            IF LIM.NOTES<1,1> NE 'CREATED BY SYSTEM DEFAULT' THEN

                ENQ<2,J> = '@ID'
                ENQ<3,J> = 'EQ'
                ENQ<4,J> = LIM.KEY.LIST<I>
                J = J+1
            END
        NEXT I
    END

    IF FLAG EQ 0 THEN
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMM"
    END


END
