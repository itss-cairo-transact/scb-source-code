* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-42</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CLEAR.COLR.IN.LIMIT
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT.COL.ALLOC.WORK
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TSA.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

    GOSUB INIT
    GOSUB CLEAR.COLR

    RETURN

INIT:
    FN.LIMIT='F.LIMIT'
    F.LIMIT=''
    CALL OPF(FN.LIMIT, F.LIMIT)

    FN.COLR='F.COLLATERAL.RIGHT'
    F.COLR=''
    CALL OPF(FN.COLR,F.COLR)
    CNT.LIM.RFS=0

    RETURN

CLEAR.COLR:

    PRINT "***************** CLEAR.COLR ***************************"

    SEL.CMD='SELECT ' : FN.LIMIT :' WITH FIXED.VARIABLE NE "" OR COLLAT.RIGHT NE ""'
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,SEL.ERR)
    PRINT "SEL.CMD : " : SEL.CMD
    PRINT "NO.OF.REC : " : NO.OF.REC

    LOOP
        REMOVE LIM.ID FROM SEL.LIST SETTING LIM.POS
    WHILE LIM.ID:LIM.POS
        CNT +=1
        PRINT "":CNT:"TH ID PROCESSING OUT OF :":NO.OF.REC :": IDS"
        PRINT "LIM.ID : " : LIM.ID
        CALL F.READ(FN.LIMIT,LIM.ID,R.LIMIT.REC,F.LIMIT,LIM.ERR)
*    PRINT "R.LIMIT.REC : " : R.LIMIT.REC
        PRINT "R.LIMIT.REC<LI.COLLAT.RIGHT> : " : R.LIMIT.REC<LI.COLLAT.RIGHT>

        IF R.LIMIT.REC<LI.COLLAT.RIGHT> THEN
            R.LIMIT.REC<LI.COLLAT.RIGHT>=''
            R.LIMIT.REC<LI.SECURED.AMT>=''

            CALL F.WRITE(FN.LIMIT,LIM.ID,R.LIMIT.REC)
            CALL JOURNAL.UPDATE(LIM.ID)
        END
    REPEAT
    PRINT "**************** END OF CLEAR.COLR**********************"

    RETURN
