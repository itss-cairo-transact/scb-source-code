* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CHEQ.AR.ADD(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IDD = ARG

    ID = FIELD(IDD,'.',2)
**CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ID,CUS.ID)
** TEXT = CUS.ID ; CALL REM

**CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,MYLOCAL)

** ARABIC.ADDRESS = MYLOCAL<1,CULR.ARABIC.ADDRESS>

** TEXT = ARABIC.ADDRESS ; CALL REM

** ARG = ARABIC.ADDRESS


    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ID,CUS)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,MYLOCAL)
    ADR = MYLOCAL<1,CULR.ARABIC.ADDRESS>
    ADR1= MYLOCAL<1,CULR.ARABIC.ADDRESS><1,1>
    ADR2= MYLOCAL<1,CULR.ARABIC.ADDRESS><1,2>
    FINDSTR '���' IN ADR SETTING FMS,VMS THEN
        ARG = '���� ��������� ������'
    END ELSE
        ARG = ADR1:" ":ADR2
    END
    CUST.ADDRESS1= MYLOCAL<1,CULR.GOVERNORATE>
    CUST.ADDRESS2= MYLOCAL<1,CULR.REGION>
    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION<2,2>,CUST.ADDRESS1,CUST.ADD2)
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION<2,2>,CUST.ADDRESS2,CUST.ADD1)
    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END



    RETURN
END
