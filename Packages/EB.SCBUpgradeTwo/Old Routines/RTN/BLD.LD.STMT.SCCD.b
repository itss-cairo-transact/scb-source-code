* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>88</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.LD.STMT.SCCD(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    I = 0
*----------------------------------
    FN.STMT = 'FBNK.STMT.ENTRY'
    F.STMT  = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.ENT.TD = 'FBNK.ACCT.ENT.LWORK.DAY'
    F.ENT.TD  = ''
    CALL OPF(FN.ENT.TD,F.ENT.TD)
*--------------------------------------------------------------------
    T.SEL2 = "SELECT ":FN.ENT.TD:" WITH CO.CODE EQ ":COMP:" BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",NO.REC,ER.SEL2)
    IF NO.REC THEN
        FOR X = 1 TO NO.REC
            CALL F.READ(FN.ENT.TD,KEY.LIST2<X>,R.ENT.TD,F.ENT.TD,EER.R)
            LOOP
                REMOVE WS.STMT.ID FROM R.ENT.TD SETTING POS
            WHILE WS.STMT.ID:POS
                CALL F.READ(FN.STMT,WS.STMT.ID,R.STMT,F.STMT,ERR.STMT)
                WS.STMT.GL  = R.STMT<AC.STE.CRF.PROD.CAT>
                WS.STMT.NAR = R.STMT<AC.STE.NARRATIVE>
                WS.ACC      = R.STMT<AC.STE.ACCOUNT.NUMBER>
                IF WS.STMT.GL GE 21103 THEN
                    IF WS.STMT.NAR[1,4] EQ 'SCCD' THEN
*                        IF WS.ACC EQ "0210018110650101" THEN
                        I++
                        ENQ<2,I> = '@ID'
                        ENQ<3,I> = 'EQ'
                        ENQ<4,I> = WS.STMT.ID
*                        END
                    END
                END
            REPEAT
        NEXT X
    END
    IF I EQ 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'

    END
END
