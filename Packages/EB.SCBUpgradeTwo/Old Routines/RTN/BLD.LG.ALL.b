* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLD.LG.ALL(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""
**********
    YTEXT = " Enter Start Date :  "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    ST.DATE = COMI
    YTEXT = " Enter To Date :  "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    EN.DATE = COMI
    YTEXT = " Enter LG TYPE :  "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    LG.TYP = COMI
**********
    IF LG.TYP EQ 'ALL' THEN
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND STATUS NE LIQ AND @ID NE '' AND VALUE.DATE GE ": ST.DATE :" AND VALUE.DATE LE ": EN.DATE
    END ELSE
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND PRODUCT.TYPE EQ ": LG.TYP :" AND STATUS NE LIQ AND @ID NE '' AND VALUE.DATE GE ": ST.DATE :" AND VALUE.DATE LE ": EN.DATE
    END
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>:"00"
        NEXT I
    END ELSE
        ENQ<2,2> = "@ID"
        ENQ<3,2> = "EQ"
        ENQ<4,2> = "DUMMY"

*   ENQ.ERROR = "NO RECORDS"

    END

*******************************************************************************************
    RETURN
END
