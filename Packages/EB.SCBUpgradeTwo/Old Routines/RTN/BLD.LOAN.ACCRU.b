* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.LOAN.ACCRU(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

*-----------------------------------------*

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    I = 0

    FN.LM = 'FBNK.LMM.CUSTOMER'; F.LM = ''
    CALL OPF(FN.LM,F.LM)

    LOCATE "CUSTOMER.ID" IN ENQ<2,1> SETTING CUS.POS THEN
        CUS.ID  = ENQ<4,CUS.POS>
    END

    CALL F.READ(FN.LM,CUS.ID,R.LM,F.LM,ER.LM)

    LOOP
        REMOVE LD.ID FROM R.LM SETTING POS.LD
    WHILE LD.ID:POS.LD

        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
        IF NOT(ER.LD) THEN
            I++
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = LD.ID
        END ELSE
            ENQ<2,2> = '@ID'
            ENQ<3,2> = 'EQ'
            ENQ<4,2> = 'DUMMY'
        END

    REPEAT

*----------------------------------------------------------------------*
    RETURN
END
