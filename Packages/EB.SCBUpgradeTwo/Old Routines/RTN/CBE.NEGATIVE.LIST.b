* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
***NESSREEN AHMED 30/11/2008*********

    SUBROUTINE CBE.NEGATIVE.LIST

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CBE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BRSIN.FLAG
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMP.LOCAL.REFS

* A ROUTINE TO CREATE A TEXT FILE CONTANING ALL THE CASH PAYMENT THAT HAPPENED THROUGH THE DAY

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "scb.msw"
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
*****************************THE HEADER***************************
******************************************************************
    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

 ** DATEH = TODAY
    DATEH = COMI
    CBE.DATA.H = '1700'
    CBE.DATA.H := DATEH[1,4]
    CBE.DATA.H := DATEH[5,2]
    CBE.DATA.H := DATEH[7,2]
    CBE.DATA.H := STR("0",493)

    DIM ZZ(1)
    ZZ(1) = CBE.DATA.H

    WRITESEQ ZZ(1) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZ(1)
    END

******************************************************************
**************************DETAILS*********************************
******************************************************************
****UPDATED BY NESSREEN AHMED 06/09/2010**************************
**  T.SEL = "SELECT F.SCB.VISA.CBE WITH REP.DATE EQ  ": TODAY
    T.SEL = "SELECT F.SCB.VISA.CBE WITH REP.DATE EQ " : COMI

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CARD.NO = '' ; EMB.NAME = '' ;  CURR = '' ; AMT = '' ; PAY.DATE = '' ; REF = ''
            BR.CO.CBE = ''
            FN.CBE = 'F.SCB.VISA.CBE' ; F.CBE = '' ; R.CBE = '' ;  E1 = ''
            CALL OPF(FN.CBE,F.CBE)
            CALL F.READ(FN.CBE,  KEY.LIST<I>, R.CBE, F.CBE, E1)

            FN.COMP = 'F.COMPANY' ; F.COMP = '' ; R.COMP = '' ;  E1.COMP = ''
            CALL OPF(FN.COMP,F.COMP)

            CURR = '0001'
            BR.CODE    = R.CBE<CBE.BRANCH>
            TEXT = 'BR.CODE=':BR.CODE ; CALL REM
            CALL F.READ(FN.COMP, BR.CODE, R.COMP, F.COMP, ERR.D)
            LOCAL.REF.CMP = R.COMP<EB.COM.LOCAL.REF>
            BR.CO.CBE = LOCAL.REF.CMP<1,CMP.CBE.BR.CODE>
            TEXT = 'COMP=':BR.CO.CBE ; CALL REM
            CUST.NO    = R.CBE<CBE.CUSTOMER>
            LATE.CO    = R.CBE<CBE.LATE.CODE>
            BIRTH.D    = R.CBE<CBE.CUS.BIRTH.DATE>
            ID.TYPE    = R.CBE<CBE.CARD.TYPE>
            ID.NO      = R.CBE<CBE.ID.NO>
            ID.ISS.D   = R.CBE<CBE.ID.ISS.DATE>
            ID.EXP.D   = R.CBE<CBE.ID.EXP.DATE>
            HOME.TEL   = R.CBE<CBE.HOME.TEL>
            WORK.TEL   = R.CBE<CBE.WORK.TEL>
            MOB.TEL    = R.CBE<CBE.MOB.TEL>
            CARD.ISS.D = R.CBE<CBE.CARD.ISS.DATE>
            LIMIT      = R.CBE<CBE.LIMIT>
            STOP.DATE  = R.CBE<CBE.STOP.DATE>
            ACCT.BAL   = R.CBE<CBE.USED.LIMIT>
            CUST.SEX   = R.CBE<CBE.CU.SEX>
            CUST.NAME  = R.CBE<CBE.CU.NAME>
            BIRTH.PL   = R.CBE<CBE.BIRTH.PLACE>
            ADDRESS    = R.CBE<CBE.CU.ADDRESS>
            CUST.JOB   = R.CBE<CBE.JOB.DESC>
            ID.ISS.PL  = R.CBE<CBE.ID.ISS.PLACE>
            CUST.EMAIL = R.CBE<CBE.EMAIL>
***************FILLING BRIDGE******************************************************************************

            CBE.DATA  = BR.CO.CBE
            CBE.DATA := STR("0",12 - LEN(CUST.NO)):CUST.NO
            CBE.DATA := '01'
            CBE.DATA := '03'
            CBE.DATA := '01'
            CBE.DATA := '01'
            CBE.DATA := BIRTH.D
            CBE.DATA := '0':ID.TYPE
            CBE.DATA := FMT(ID.NO, "l#15")
            CBE.DATA := ID.ISS.D
            CBE.DATA := ID.EXP.D
            CBE.DATA := FMT(HOME.TEL, "R%15")
            CBE.DATA := FMT(WORK.TEL, "R%15")
            CBE.DATA := FMT(MOB.TEL, "R%15")
            CBE.DATA := CARD.ISS.D
            CBE.DATA := CURR
            LIM.FMT = FIELD(LIMIT, ".", 1)
            CBE.DATA := FMT(LIM.FMT, "R%6")
            CBE.DATA := STOP.DATE
            ACCT.FMT = FIELD(ACCT.BAL, ".", 1)
            CBE.DATA := FMT(ACCT.FMT, "R%6")
            CBE.DATA := STR(" ",12)
            CBE.DATA := CUST.SEX
            TEXT = 'SEX=':CUST.SEX ; CALL REM
            CBE.DATA := FMT(CUST.NAME, "l#100")
            CBE.DATA := FMT(BIRTH.PL, "l#40")
            CBE.DATA := FMT(ADDRESS, "l#60")
            CBE.DATA := FMT(CUST.JOB, "l#60")
            CBE.DATA := FMT(ID.ISS.PL, "l#40")
            CBE.DATA := FMT(CUST.EMAIL, "l#50")

            AA = 1+SELECTED
            DIM XX(AA)
            XX(AA) = CBE.DATA

            WRITESEQ XX(AA) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':XX(AA)
            END

        NEXT I
    END
*************************************************************
    RETURN

END
