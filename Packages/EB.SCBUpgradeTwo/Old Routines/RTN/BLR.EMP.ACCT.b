* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.EMP.ACCT(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.CUS = 'FBNK.CUSTOMER'
    F.CUS = ''
    R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.SCR = 'FBNK.STMT.ACCT.CR'
    F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    TT = TODAY
**    TTLM = TT[1,6]:'01'
**    CALL CDT("",TTLM,'-1W')
    TTS = TT[1,6]
    ACCC=''
    T.SEL = "SELECT ":FN.SCR:" WITH @ID LIKE ...-":TTS:"..."
    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ACC.ID = FIELD(K.LIST<I>,'-',1)
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,ACC.ID,AC.COM)
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC.ID,ACCC)
            IF AC.COM EQ COMP THEN
                CALL F.READ(FN.CUS,ACCC,R.CUS,F.CUS,ERRR)
                IF R.CUS<EB.CUS.SECTOR> = 1100 OR R.CUS<EB.CUS.SECTOR> = 1300 THEN
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = K.LIST<I>
                END ELSE
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = "NOLIST"
                END
            END ELSE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        NEXT I
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END

    RETURN
END
