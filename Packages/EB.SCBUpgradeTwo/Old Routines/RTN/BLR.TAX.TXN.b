* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BLR.TAX.TXN(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_INF.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.TAX
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.SYSTEM.ID
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC = "" ; R.ACC = "" ; ERR.ACC = ""
    CALL OPF(FN.ACC,F.ACC)
    FN.ENT = "FBNK.ACCT.ENT.LWORK.DAY" ; F.ENT = "" ; R.ENT = "" ; ERR.ENT = ""
    CALL OPF(FN.ENT,F.ENT)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE = "" ; R.STE = "" ; ERR.STE = ""
    CALL OPF(FN.STE,F.STE)
    FN.TAX = "F.SCB.CUSTOMER.TAX" ; F.TAX = "" ; R.TAX = "" ; ERR.TAX = ""
    CALL OPF(FN.TAX,F.TAX)
    K.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    K.LIST1 = "" ; SELECTED.TAX = "" ; ER.MSG.TAX = ""

    ENT.SEL  ="SELECT ":FN.ACC:" WITH CO.CODE EQ ":COMP
    ENT.SEL :=" AND CATEGORY EQ 16523 16536"
    ENT.SEL :=" AND CURRENCY EQ EGP"
    ENT.SEL :=" BY @ID"
    KK1 = 0
    CALL EB.READLIST(ENT.SEL,K.LIST,'',SELECTED,ER.MSG)
    LOOP
        REMOVE AC.ID FROM K.LIST SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.ENT,AC.ID,R.ENT,F.ENT,ER.ENT)
        LOOP
            REMOVE STE.ID FROM R.ENT SETTING POS.STE
        WHILE STE.ID:POS.STE
            CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
            STE.REF  = R.STE<AC.STE.TRANS.REFERENCE>
            STE.COM  = R.STE<AC.STE.COMPANY.CODE>
            FINDSTR '\B' IN STE.REF SETTING AAA THEN STE.REF1 = FIELD(STE.REF,'\',1) ELSE STE.REF1 = STE.REF
            TAX.SEL ="SELECT ":FN.TAX:" WITH @ID LIKE ...":STE.REF1[3,10]:"..."
            CALL EB.READLIST(TAX.SEL,K.LIST1,'',SELECTED.TAX,ER.MSG.TAX)
            IF NOT(SELECTED.TAX) THEN
                IF STE.REF[4] EQ '\BNK' THEN
                    STE.REF = FIELD(STE.REF,'\',1):'\B01'
                END
                FINDSTR '\B' IN STE.REF SETTING Y.XX THEN
                    BRR = FIELD(STE.REF,'\',2)
                    IF BRR[2] NE COMP[2] AND BRR[2] NE '99' THEN
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = STE.ID
                    END ELSE
                        KK1++
                        ENQ<2,KK1> = "@ID"
                        ENQ<3,KK1> = "EQ"
                        ENQ<4,KK1> = "NOLIST"
                    END
                END ELSE
                    KK1++
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = "NOLIST"
                END
            END ELSE
                KK1++
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = "NOLIST"
            END
        REPEAT
    REPEAT
    RETURN
