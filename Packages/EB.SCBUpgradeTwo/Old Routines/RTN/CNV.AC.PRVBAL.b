* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE CNV.AC.PRVBAL
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    GOSUB INITIATE
    GOSUB CHECKBAL
    GOSUB PROCESS
    RETURN
**********************************************
INITIATE:
    XX = O.DATA

TEXT = "O.DATA1 : " : O.DATA ; CALL REM
  **  XX = '9942430020200101-20090102'
    FN.ACC = "FBNK.ACCT.ACTIVITY"
    F.ACC  = ''
    R.ACC  = ''
    FN.AC = "FBNK.ACCOUNT"
    F.AC  = ''
    R.AC  = ''
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.AC,F.AC)
    PRVBAL = ''
    RETURN
**********************************************
CHECKBAL:
    AC.CODE    = FIELD(XX,'-',1)
    CALL F.READ(FN.AC,AC.CODE,R.AC,F.AC,ERR.AC)
    AC.BAL = R.AC<AC.ONLINE.ACTUAL.BAL>
    RETURN
**********************************************
PROCESS:
    START.DATE = FIELD(XX,'-',2)
    IF AC.BAL NE '' THEN
        GOSUB GETBAL
        IF YY EQ 0 THEN
            GOSUB GETBAL
        END
    END ELSE
        BAL = 0
    END
    O.DATA = BAL
    RETURN
****************************************
GETBAL:
    LOOP
        GOSUB DATEUP
        AC.ID = AC.CODE:"-":PRV.DATE
        CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,ERR)
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.ACC<IC.ACT.BALANCE>,@VM)
    WHILE ERR
*        IF PRV.DATE LT '199001' THEN
*            EXIT
*        END
    REPEAT
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY = DCOUNT(R.ACC<IC.ACT.BALANCE>,@VM)
    BAL = R.ACC<IC.ACT.BALANCE,YY>
    RETURN
******************************
DATEUP:
*    START.DATE = START.DATE[1,6]:'01'
     START.DATE = '20090101'
    CALL CDT("",START.DATE,'-1C')
    PRV.DATE = START.DATE[1,6]
    RETURN
TEXT = "O.DATA1 : " : O.DATA ; CALL REM
****************************** 
END
