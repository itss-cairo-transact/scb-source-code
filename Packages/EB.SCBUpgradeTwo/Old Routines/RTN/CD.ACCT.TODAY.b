* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CD.ACCT.TODAY

*   PROGRAM CD.ACCT.TODAY

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*   $INCLUDE T24.BP ACCT.ENT.TODAY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TODAY.BAL


    ID.BAL = TODAY
    CALL CDT('',ID.BAL,'+5W')
    TEXT=TODAY:'':ID.BAL;CALL REM

    FN.CD.TODAY.BAL  = 'F.SCB.CD.TODAY.BAL' ; F.CD.TODAY.BAL = '' ; R.CD.TODAY.BAL = ''
    CALL OPF(FN.CD.TODAY.BAL,F.CD.TODAY.BAL)
    CALL F.READ(FN.CD.TODAY.BAL,ID.BAL,R.CD.TODAY.BAL,F.CD.TODAY.BAL,ERRCD)
    AMT.LCY = 0
*    DEBUG

*   FN.TRNS  = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.TRNS = '' ; R.TRNS = ''
    FN.TRNS  = 'FBNK.ACCT.ENT.TODAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    T.SEL  = "SELECT FBNK.ACCT.ENT.TODAY WITH @ID EQ EGP1610200010099 "
*   T.SEL  = "SELECT FBNK.ACCT.ENT.LWORK.DAY WITH @ID EQ EGP1610200010099 "


    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    ENT.NO = ''
    LOOP
        REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
    WHILE ACCT.ID:POS
        CALL F.READ(FN.TRNS,ACCT.ID,R.TRNS,F.TRNS,ERR)
        CLR.AMT = 0
        LOOP

            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)
            CD.CODE = R.STMT<AC.STE.TRANS.REFERENCE>[1,6]
            IF CD.CODE EQ 'SCB.CD' THEN
                AMT.LCY = R.STMT<AC.STE.AMOUNT.LCY> + AMT.LCY
            END
        REPEAT
    REPEAT
*DEBUG

    IF R.CD.TODAY.BAL<CD.BAL.FLAG> NE 'Y' THEN
        R.CD.TODAY.BAL<CD.BAL.TOT.BALANCE> = AMT.LCY
        R.CD.TODAY.BAL<CD.BAL.ACCT> = 'EGP1610200010099'
        R.CD.TODAY.BAL<CD.BAL.BOOKING.DATE> = TODAY
        R.CD.TODAY.BAL<CD.BAL.FLAG> = 'Y'
    END



***    WRITE  R.CD.TODAY.BAL TO F.CD.TODAY.BAL , ID.BAL ON ERROR
***        PRINT "CAN NOT WRITE RECORD":ID.BAL:"TO" :FN.CD.TODAY.BAL
***    END

    CALL F.WRITE(FN.CD.TODAY.BAL,ID.BAL,R.CD.TODAY.BAL)
    CALL JOURNAL.UPDATE(ID.BAL)


**    TEXT = AMT.LCY:'AMT.LCY';CALL REM
    RETURN
END
