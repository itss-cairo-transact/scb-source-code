* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
    SUBROUTINE BOAT.SLIP.BACK.DR.REPRINT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*========================================================================
INITIATE:
    REPORT.ID='BOAT.SLIP.BACK.DR'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    AC.BR = COMP[2]
    AMT = 0
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BO = 'F.SCB.BOAT.CUSTOMER' ; F.BO = ''
    CALL OPF(FN.BO,F.BO)

    YTEXT = "Enter the BO No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    CALL F.READ(FN.BO,COMI,R.BO,F.BO,E1)

    BO.STAT = R.BO<BO.STATUS>
    IF BO.STAT EQ '3' THEN
        BO.ID  = COMI
        CUR.ID = R.BO<BO.CURRENCY>

        IF R.NEW(BO.BOAT.CODE) NE '50' THEN
            IF CUR.ID EQ 'EGP' THEN
                DEBIT.AC = 'EGP16188000300':AC.BR
            END
            IF CUR.ID EQ 'USD' THEN
                DEBIT.AC = 'USD16188000300':AC.BR
            END
        END ELSE
            DEBIT.AC = 'EGP1618800040020'
        END


        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,DEBIT.AC,CUST.NAME)
        CATEG = CUST.NAME

        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
        CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

        AMOUNT     = R.BO<BO.AMOUNT>

        IN.AMOUNT  = AMOUNT
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT    = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        INPUTTER   = R.BO<BO.INPUTTER>
        INP        = FIELD(INPUTTER,'_',2)
        AUTH       = R.BO<BO.AUTHORISER>
        AUTHI      = FIELD(AUTH,'_',2)



        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
*--------------------------------------------------------------
        XX<1,1>[3,35]   = CUST.NAME

        XX4<1,1>[45,15]  = '������     : '
        XX4<1,1>[59,15]  = AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = DEBIT.AC

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR

        XX5<1,1>[3,35]  = '������ ������� : ':OUT.AMT
        XX6<1,1>[3,35]  = '���������       : '

        XX7<1,1>[1,15]  = '������ : ':INP
        XX7<1,1>[30,15] = '��� ������� : ':BO.ID
        XX7<1,1>[60,15] = '������ : ':AUTHI
*-------------------------------------------
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
        PR.HD  ="'L'":SPACE(1):"��� ���� ������"
        PR.HD :="'L'":"������� : ":T.DAY
        PR.HD :="'L'":"����� : ":BRANCH
        PR.HD :="'L'":"����� ��� - ����"
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PRINT
        HEADING PR.HD
*------------------------------------------------------------------
        PRINT XX<1,1>
        PRINT XX1<1,1>
        PRINT XX2<1,1>
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT STR(' ',82)
        PRINT XX5<1,1>
        PRINT STR(' ',82)
        PRINT XX6<1,1>
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR('-',82)
        PRINT XX7<1,1>
    END ELSE
        TEXT = '��� ������� ���� ����' ; CALL REM
    END
*===============================================================
    RETURN
END
