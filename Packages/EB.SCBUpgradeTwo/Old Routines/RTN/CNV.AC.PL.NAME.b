* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
**************MAHMOUD 17/11/2011************************
    SUBROUTINE CNV.AC.PL.NAME

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    XXX1   = ''
    XXX1   = O.DATA
    IF XXX1[1,2] EQ 'PL' THEN
        XXX1   = FIELD(XXX1,'PL',2)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XXX1,CC.NAME)
    END ELSE
        FN.CUS = "FBNK.CUSTOMER" ; F.CUS = '' ; R.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,XXX1,AC.CUST)
        IF AC.CUST NE '' THEN
            CALL F.READ(FN.CUS,AC.CUST,R.CUS,F.CUS,E.RRR)
            CUS.LCL  = R.CUS<EB.CUS.LOCAL.REF>
            CUS.NAME1= CUS.LCL<1,CULR.ARABIC.NAME>
            CUS.NAME2= CUS.LCL<1,CULR.ARABIC.NAME.2>
            CC.NAME = CUS.NAME1:" ":CUS.NAME2
        END ELSE
            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE,XXX1,CC.NAME)
        END
    END
    O.DATA = CC.NAME
    RETURN
END
