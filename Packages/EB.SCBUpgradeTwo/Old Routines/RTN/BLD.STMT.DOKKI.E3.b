* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.STMT.DOKKI.E3(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------

    FN.AC   = 'FBNK.ACCOUNT'    ; F.AC   = ''
    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT = ''

    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.STMT,F.STMT)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    LOCATE 'OPENING.DATE' IN ENQ<2,1> SETTING DAT.POS THEN
        OPENING.DATE = ENQ<4,DAT.POS>
    END

    WS.BOOK.DATE = OPENING.DATE[1,6]:'03'

    T.SEL = "SELECT FBNK.STMT.E3 WITH BOOKING.DATE EQ ":WS.BOOK.DATE:" AND COMPANY.CODE EQ EG0010011 AND PRODUCT.CATEGORY IN (6511 6512)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.STMT,KEY.LIST<I>,R.STMT,F.STMT,E1)
            WS.ACCT.NO      = R.STMT<AC.STE.ACCOUNT.NUMBER>

            CALL F.READ(FN.AC,WS.ACCT.NO,R.AC,F.AC,E2)
            WS.AC.OPEN.DATE = R.AC<AC.OPENING.DATE>

            IF WS.AC.OPEN.DATE EQ WS.BOOK.DATE THEN

                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = WS.ACCT.NO

            END ELSE

                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'

            END
        NEXT I
    END

    RETURN
END
