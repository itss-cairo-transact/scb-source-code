* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.SOLFA.INST

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ��������  '

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        TEXT = "�� �������� �� ��������" ; CALL REM

    END ELSE
        TEXT = '�� ������ �� �������� ' ; CALL REM
    END

    RETURN

*----------------------------------------------------------------
INITIALISE:
    OPENSEQ "MECH" , "HR.SOLFA.INSTALLEMNT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"HR.SOLFA.INSTALLEMNT"
        HUSH OFF
    END
    OPENSEQ "MECH" , "HR.SOLFA.INSTALLEMNT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE HR.SOLFA.INSTALLEMNT CREATED IN MECH'
        END ELSE
            STOP 'Cannot create HR.SOLFA.INSTALLEMNT File IN MECH'
        END
    END

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    COMMA = ","
    V.DATE = TODAY
    RETURN
*----------------------------------------------------
BUILD.RECORD:

    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 1409 AND ONLINE.ACTUAL.BAL NE 0 AND ONLINE.ACTUAL.BAL NE '' BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            DB.ACCT      = KEY.LIST<I>
            CUS.NO       = R.AC<AC.CUSTOMER>
            WS.CUST.NO   = FMT(CUS.NO,'R%8')
            WS.ACCT.1407 = WS.CUST.NO:'10140701'
            WS.ACCT.1408 = WS.CUST.NO:'10140801'
            WS.ACCT.1413 = WS.CUST.NO:'10141301'

            CALL F.READ(FN.AC,WS.ACCT.1407,R.AC1,F.AC,E2)
            CALL F.READ(FN.AC,WS.ACCT.1408,R.AC2,F.AC,E3)
            CALL F.READ(FN.AC,WS.ACCT.1413,R.AC3,F.AC,E4)

            WS.AMT.1409 = R.AC<AC.ONLINE.ACTUAL.BAL>
            IF WS.AMT.1409 LT 0 THEN WS.AMT.1409 = WS.AMT.1409  * -1

            WS.AMT.1407 = R.AC1<AC.ONLINE.ACTUAL.BAL>
            IF WS.AMT.1407 LT 0 THEN WS.AMT.1407 = WS.AMT.1407  * -1

            WS.AMT.1408 = R.AC2<AC.ONLINE.ACTUAL.BAL>
            IF WS.AMT.1408 LT 0 THEN WS.AMT.1408 = WS.AMT.1408  * -1

            WS.AMT.1413 = R.AC3<AC.ONLINE.ACTUAL.BAL>
            IF WS.AMT.1413 LT 0 THEN WS.AMT.1413 = WS.AMT.1413  * -1


            WS.AMT.70 = WS.AMT.1409 * 0.7
            WS.AMT.70 = DROUND(WS.AMT.70,2)
            WS.AMT.30 = WS.AMT.1409 - WS.AMT.70

            IF WS.AMT.70 LE WS.AMT.1408 THEN
                CR.ACCT = WS.ACCT.1408
                WS.AMT  = WS.AMT.70
                GOSUB CREATE.OFS

                IF WS.AMT.30 LE WS.AMT.1413 THEN
                    CR.ACCT = WS.ACCT.1413
                    WS.AMT  = WS.AMT.30
                    GOSUB CREATE.OFS
                END ELSE
                    CR.ACCT = WS.ACCT.1408
                    WS.AMT  = WS.AMT.30
                    GOSUB CREATE.OFS
                END

            END ELSE

                CR.ACCT = WS.ACCT.1408
                WS.AMT  = WS.AMT.1408
                GOSUB CREATE.OFS

                WS.AMT1 = WS.AMT.70 - WS.AMT.1408
                WS.AMT2 = WS.AMT1 + WS.AMT.30

                IF WS.AMT2 LE WS.AMT.1413 THEN
                    CR.ACCT = WS.ACCT.1413
                    WS.AMT  = WS.AMT2
                    GOSUB CREATE.OFS
                END ELSE
                    WS.AMT  = WS.AMT.1413
                    CR.ACCT = WS.ACCT.1413
                    GOSUB CREATE.OFS

                    WS.AMT  = WS.AMT2 - WS.AMT.1413
                    CR.ACCT = WS.ACCT.1407
                    GOSUB CREATE.OFS
                END
            END

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN HR.SOLFA.INSTALLEMNT'
    EXECUTE 'DELETE ':"MECH":' ':"HR.SOLFA.INSTALLEMNT"

    RETURN
***********************************
CREATE.OFS:
*============

    IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//EG0010099,'

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":"EGP":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":WS.AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
    OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"SOLFA.INSTALEMNT":','
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":"SOLFA.INSTALEMNT":','


    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

    WRITESEQ MSG.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
