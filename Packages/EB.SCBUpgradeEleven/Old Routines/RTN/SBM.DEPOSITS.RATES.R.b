* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.DEPOSITS.RATES.R

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------
*Line [ 37 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*=============================================================
INITIATE:
    REPORT.ID='SBM.DEPOSITS.RATES.R'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=============================================================
CALLDB:
    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LDD = ''
    R.LDD = ''
    CALL OPF(FN.LDD,F.LDD)
    KEY.LIST =""
    SELECTED =""
    ER.MSG =""
    KEY.LIST1=""
    SELECTED1=""
    ER.MSG1=""
    RATE.NAME = ''
    BAL1      = 0
    BAL2      = 0
    BAL3      = 0
    BAL.TOT   = 0
    SW        = ''
    BAL1.TOT  = 0
    BAL2.TOT  = 0
    BAL3.TOT  = 0
    BAL.TOTAL = 0
    TDD = TODAY
*=============================================================
    T.SEL  = "SELECT ":FN.LDD:" WITH AMOUNT NE '' AND AMOUNT NE 0 AND CO.CODE EQ ":COMP
    T.SEL := " AND ( CATEGORY GE 21001 AND CATEGORY LE 21010 ) BY INTEREST.RATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED  THEN
        XX1 = SPACE(132) ; XX2 = SPACE(132)
        K1=0 ; K2=1
        LD.ID       = ''
        LD.BAL      = 0
        LD.RATE     = 0
        SELECTED.LD = ''
        ER.LD       = ''
**************************************************************
        FOR I = 1 TO SELECTED
*         K1++
            CALL F.READ(FN.LDD,KEY.LIST<I>,R.LDD,F.LDD,ER.LDD)
            LD.ID  = KEY.LIST<I>
            LD.RATE = R.LDD<LD.INTEREST.RATE,1>
            LD.BAL = R.LDD<LD.AMOUNT,1>
            BEGIN CASE
            CASE LD.RATE LE 5
                RATE.NAME = '��� 5% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '1'
            CASE LD.RATE GT 5 AND LD.RATE LE 7
                IF SW = 1 THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 5% ���� 7% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '2'
            CASE LD.RATE GT 7 AND LD.RATE LE 9
                IF SW = '2' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 7% ���� 9% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '3'
            CASE LD.RATE GT 9 AND LD.RATE LE 11
                IF SW = '3' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 9% ���� 11% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '4'
            CASE LD.RATE GT 11 AND LD.RATE LE 13
                IF SW = '4' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 11% ���� 13% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '5'
            CASE LD.RATE GT 13 AND LD.RATE LE 15
                IF SW = '5' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 13% ���� 15% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '6'
            CASE LD.RATE GT 15 AND LD.RATE LE 17
                IF SW = '6' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 15% ���� 17% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '7'
            CASE LD.RATE GT 17 AND LD.RATE LE 19
                IF SW = '7' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 17% ���� 19% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '8'
            CASE LD.RATE GT 19 AND LD.RATE LE 21
                IF SW = '8' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 19% ���� 21% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '9'
            CASE 1
                IF SW = '9' THEN
                    GOSUB PRINTLD
                END
                RATE.NAME = '���� �� 21% �����'
                BAL.TOT  += (LD.BAL / 1000)
                IF LD.BAL LT 100000 THEN
                    BAL1 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                    BAL2 += (LD.BAL / 1000)
                END
                IF LD.BAL GE 1000000 THEN
                    BAL3 += (LD.BAL / 1000)
                END
                SW = '10'
            END CASE
            BAL.TOTAL += (LD.BAL / 1000)
            IF LD.BAL LT 100000 THEN
                BAL1.TOT += (LD.BAL / 1000)
            END
            IF LD.BAL GE 100000 AND LD.BAL LT 1000000 THEN
                BAL2.TOT += (LD.BAL / 1000)
            END
            IF LD.BAL GE 1000000 THEN
                BAL3.TOT += (LD.BAL / 1000)
            END
        NEXT I
        GOSUB PRINTLD
*=============================================================
        PRINT STR('-',120)
        GOSUB PRINT.TOT
        PRINT STR('=',120)
*=============================================================
    END ELSE
*        TEXT = "NO RECORD EXIST" ; CALL REM
    END
    RETURN
*=============================================================
**************************
PRINTLD:
    BAL1 = DROUND(BAL1,0)
    BAL2 = DROUND(BAL2,0)
    BAL3 = DROUND(BAL3,0)
    BAL.TOT = DROUND(BAL.TOT,0)
    XX1<1,1>[1,30]   = RATE.NAME
    XX1<1,1>[35,15]  = BAL1
    XX1<1,1>[55,15]  = BAL2
    XX1<1,1>[75,15]  = BAL3
    XX1<1,1>[95,15]  = BAL.TOT
    PRINT XX1<1,1>
    RATE.NAME = ''
    BAL1      = 0
    BAL2      = 0
    BAL3      = 0
    BAL.TOT   = 0
    SW        = ''
    XX1       = SPACE(132)
    RETURN
**************************
PRINT.TOT:
    BAL1.TOT  = DROUND(BAL1.TOT,0)
    BAL2.TOT  = DROUND(BAL2.TOT,0)
    BAL3.TOT  = DROUND(BAL3.TOT,0)
    BAL.TOTAL = DROUND(BAL.TOTAL,0)
    XX2<1,K2>[1,30]   = '��������'
    XX2<1,K2>[35,15]  = BAL1.TOT
    XX2<1,K2>[55,15]  = BAL2.TOT
    XX2<1,K2>[75,15]  = BAL3.TOT
    XX2<1,K2>[95,15]  = BAL.TOTAL
    PRINT XX2<1,K2>
    RATE.NAME = ''
    BAL1.TOT  = 0
    BAL2.TOT  = 0
    BAL3.TOT  = 0
    BAL.TOTAL = 0
    RETURN
***********************************
*=============================================================
PRINT.HEAD:
*---------
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

    YYBRN   = BRANCH
    DATY    = TODAY
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(50):"����� ������ ������ �������"
    PR.HD :="'L'":SPACE(48):"���� ������� ������� �� : ":T.DAY:"        ������ ������ ����"
    PR.HD :="'L'":SPACE(45):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������� - ������" :SPACE(10):"��� �� 100 ���" :SPACE(3):"�� 100000 ���� �� �����":SPACE(5):"����� �����" :SPACE(10):"�������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*=============================================================
END
