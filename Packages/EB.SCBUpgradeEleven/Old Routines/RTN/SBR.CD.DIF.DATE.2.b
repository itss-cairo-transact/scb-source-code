* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-120</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.CD.DIF.DATE.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*---------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*---------------------------------------------

INITIATE:
*========
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LMM = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""


    RETURN
*-------------------------------------------------
PROCESS:
*=======
    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ '21021' AND STATUS EQ 'CUR' BY CO.CODE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LMM.ID = KEY.LIST<I>:'00'

            WS.MAT.DATE = R.LD<LD.FIN.MAT.DATE>
            WS.AMT      = R.LD<LD.AMOUNT>

            CALL F.READ(FN.LMM,LMM.ID,R.LMM,F.LMM,E2)
            WS.START.DATE = R.LMM<LD27.START.PERIOD.INT>
            WS.END.DATE   = R.LMM<LD27.END.PERIOD.INT>
            WS.TODAY      = TODAY
            WS.BRN.ID     = R.LD<LD.CO.CODE>
            CALL F.READ(FN.COM,WS.BRN.ID,R.COM,F.COM,E3)
            WS.BRN.NAME   = R.COM<EB.COM.COMPANY.NAME,2>

            DAYS = "C"
            CALL CDD("",WS.START.DATE,WS.END.DATE,DAYS)

            IF DAYS GT 92 THEN

                XX1 = SPACE(120)

                XX1<1,1>[1,15]   = KEY.LIST<I>
                XX1<1,1>[20,10]  = WS.START.DATE
                XX1<1,1>[35,10]  = WS.END.DATE
                XX1<1,1>[50,10]  = WS.MAT.DATE
                XX1<1,1>[70,15]  = WS.AMT
                XX1<1,1>[90,10]  = DAYS
                XX1<1,1>[105,15] = WS.BRN.NAME

                PRINT XX1<1,1>

            END
        NEXT I

        XX2 = SPACE(120)
        XX2<1,1>[55,35] = '********* END.OF.REPORT *********'
        PRINT XX2<1,1>

    END
    RETURN
*--------------------------------------------------------
PRINT.HEAD:
*==========
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(48):"CD.ERROR.GT.92DAY - ":T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ":"CD.ID":SPACE(13):"START.DATE":SPACE(5):"END.DATE":SPACE(7):"MATURITY.DATE":SPACE(7):"AMOUNT":SPACE(15):"DAYS":SPACE(15):"BRANCH"
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD


    RETURN
END
