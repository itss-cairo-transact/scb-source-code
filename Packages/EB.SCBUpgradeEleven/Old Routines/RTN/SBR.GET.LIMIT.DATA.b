* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-178</Rating>
*-----------------------------------------------------------------------------
***** CREATED BY MOHAMED SABRY & KHALED IBRAHIM  *****
*****                IN 2010/11/05               *****
******************************************************
    PROGRAM SBR.GET.LIMIT.DATA
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SCC.INDEX
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT.REFERENCE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    GOSUB OPEN.FILE
    GOSUB CLEAR.DATA
    GOSUB PROCESS
    GOSUB CREATE.DATE.INDEX
    RETURN

*********************** OPENING FILES *****************************
OPEN.FILE:
    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.LPE.DATE)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.LWD.DATE)
*    WS.LWD.DATE = '20110228'

    FN.LI      = "FBNK.LIMIT"                    ; F.LI      = ""  ; R.LI     = ""
    FN.LI.REF  = "FBNK.LIMIT.REFERENCE"          ; F.LI.REF  = ""  ; R.LI.REF = ""
    FN.CG      = "F.SCB.CUSTOMER.GROUP"          ; F.CG      = ""  ; R.CG     = ""
    FN.SCC     = "F.SCB.STATIC.CREDIT.CUSTOMER"  ; F.SCC     = ""  ; R.SCC    = ""
    FN.SCCI    = "F.SCB.SCC.INDEX"               ; F.SCCI    = ""  ; R.SCCI   = ""
    FN.CU      = "FBNK.CUSTOMER"                 ; F.CU      = ""  ; R.CU     = ""
    FN.CCY     = 'FBNK.CURRENCY'                 ; F.CCY     = ''  ; R.CCY    = ''

    CALL OPF (FN.LI,F.LI)
    CALL OPF (FN.LI.REF,F.LI.REF)
    CALL OPF (FN.CG,F.CG)
    CALL OPF (FN.SCC,F.SCC)
    CALL OPF (FN.SCCI,F.SCCI)
    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.CCY,F.CCY)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    WS.CUS.ID.FLAG   = 0
    WS.LI.CY.ID.FLAG = 0
    R.SCC = ''
    WS.C              = 0
    RETURN
*------------------------------------------------------------------
PROCESS:
    T.SEL1 = "SELECT ":FN.LI.REF:" WITH REFERENCE.CHILD NE '' WITHOUT @ID IN ( 2000 5100 400 500 600 700 ) BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    T.SEL = "SELECT ":FN.LI:" WITH LIMIT.PRODUCT IN ( "
    FOR J = 1 TO SELECTED1
        T.SEL := KEY.LIST1<J>:" "
    NEXT J

    T.SEL := " ) AND PRODUCT.ALLOWED NE '' AND LIABILITY.NUMBER UNLIKE 994...  BY LIABILITY.NUMBER BY LIMIT.CURRENCY "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

**** ------------------------- MAIN PROCESS ------------------------- ****
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LI,KEY.LIST<I>,R.LI,F.LI,E1)
*PRINT I:"-":KEY.LIST<I>
            WS.CUS.ID = R.LI<LI.LIABILITY.NUMBER>
            WS.LI.CY.ID  = R.LI<LI.LIMIT.CURRENCY>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,LOCAL.REF)
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF WS.CREDIT.CODE NE 100 THEN
                GO TO NEXT.REC.OF.I
            END
            IF  WS.LI.CY.ID.FLAG = 0 THEN
                WS.LI.CY.ID.FLAG = WS.LI.CY.ID
            END
            IF  WS.LI.CY.ID.FLAG # WS.LI.CY.ID THEN
                GOSUB WHEN.CY.CHANGE
                GOSUB CLEAR.DATA
            END
            IF  WS.CUS.ID.FLAG = 0 THEN
                WS.CUS.ID.FLAG = WS.CUS.ID
            END
            IF  WS.CUS.ID.FLAG # WS.CUS.ID THEN
                GOSUB GET.CUSTOMER.DATA
                GOSUB WHEN.CY.CHANGE
                GOSUB WHEN.CUS.CHANGE
                GOSUB CLEAR.DATA
            END
            WS.CUS.ID.FLAG   = WS.CUS.ID
            WS.LI.CY.ID.FLAG = WS.LI.CY.ID
            GOSUB GET.INT.USED.AMT
            IF I = SELECTED THEN
                GOSUB GET.CUSTOMER.DATA
                GOSUB WHEN.CY.CHANGE
                GOSUB WHEN.CUS.CHANGE
                GOSUB CLEAR.DATA
            END
NEXT.REC.OF.I:
        NEXT I
    END
    RETURN
**** --------------- GET AMOUNT FROM OPEN.ACTUAL.BAL IN ACCOUNT OR LIMIT ---------------------- ****
GET.INT.USED.AMT:
    WS.LI.PRO    = R.LI<LI.LIMIT.PRODUCT>
    IF  WS.LI.PRO LT 2500 OR ( WS.LI.PRO GE 8100 AND WS.LI.PRO LT 9000 ) THEN
        WS.DIR.INT.AMT   += R.LI<LI.INTERNAL.AMOUNT>
        WS.ACCT = R.LI<LI.ACCOUNT>
*Line [ 144 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        ACC.DC  = DCOUNT(WS.ACCT,@VM)
        FOR X = 1 TO ACC.DC
            ACC.ID = WS.ACCT<1,X>
            CALL DBR ('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,ACC.ID,WS.AC.AMT)
            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.ID,WS.AC.CY.ID)
            *IF  WS.AC.AMT LT 0 THEN
                GOSUB CONVERTE.AMT.AC.LI
            *END
        NEXT X
        WS.DIR.USED.AMT.AC +=  WS.TMP.LI.AMT
        WS.TMP.LI.AMT = 0
    END
    IF  WS.LI.PRO EQ 2500 THEN
        WS.INDIR.INT.AMT.LG  += R.LI<LI.INTERNAL.AMOUNT>
        WS.INDIR.USED.AMT.LG += R.LI<LI.COMMT.AMT.AVAIL>
    END
    IF  ( WS.LI.PRO EQ 30000 OR WS.LI.PRO EQ 20000 ) THEN
        WS.INDIR.INT.AMT.LC  += R.LI<LI.INTERNAL.AMOUNT>
        WS.INDIR.USED.AMT.LC += R.LI<LI.TOTAL.OS>
        WS.ACCT.LC = R.LI<LI.ACCOUNT>
*Line [ 165 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        ACC.DC.LC  = DCOUNT(WS.ACCT.LC,@VM)
        FOR X.LC = 1 TO ACC.DC.LC
            ACC.ID.LC = WS.ACCT.LC<1,X.LC>
            CALL DBR ('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,ACC.ID.LC,WS.AC.AMT)
            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.ID.LC,WS.AC.CY.ID)
           * IF  WS.AC.AMT LT 0 THEN
                GOSUB CONVERTE.AMT.AC.LI
           * END
        NEXT X.LC
        WS.DIR.USED.AMT.AC.LC += WS.TMP.LI.AMT
        WS.TMP.LI.AMT          = 0
    END

**** UPDATE BY MOHAMED SABRY 2013/12/25
    IF  ( WS.LI.PRO EQ 6000 OR WS.LI.PRO EQ 6200 ) THEN
        WS.INDIR.INT.AMT.LC  += R.LI<LI.INTERNAL.AMOUNT>
*        WS.INDIR.USED.AMT.LC += R.LI<LI.TOTAL.OS>
        WS.ACCT.LC = R.LI<LI.ACCOUNT>
*        ACC.DC.LC  = DCOUNT(WS.ACCT.LC,VM)
*        FOR X.LC = 1 TO ACC.DC.LC
*            ACC.ID.LC = WS.ACCT.LC<1,X.LC>
*            CALL DBR ('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,ACC.ID.LC,WS.AC.AMT)
*            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.ID.LC,WS.AC.CY.ID)
*           * IF  WS.AC.AMT LT 0 THEN
*                GOSUB CONVERTE.AMT.AC.LI
*           * END
*        NEXT X.LC
        WS.DIR.USED.AMT.AC.LC += R.LI<LI.TOTAL.OS>
        WS.TMP.LI.AMT          = 0
    END
**** END OF UPDATE 2013/12/25
    RETURN
**** ----------------  CONVERTE AMOUNT FROM ACCOUNT TO LIMIT CURRENCY ----------------------- ****
CONVERTE.AMT.AC.LI:

    WS.TMP.CY.ID    = WS.AC.CY.ID
    GOSUB GET.RATE
    WS.TMP.LCY.AMT  = WS.AC.AMT * WS.RATE
    WS.TMP.CY.ID    = WS.LI.CY.ID
    GOSUB GET.RATE
    WS.TMP.LI.AMT  += WS.TMP.LCY.AMT / WS.RATE

    RETURN
**** ---------------------- GET SELL.RATE FROM CURRENCY FILE -------------------- ****
GET.RATE:
    IF  WS.TMP.CY.ID NE 'EGP' THEN
        CALL F.READ(FN.CCY,WS.TMP.CY.ID,R.CCY,F.CCY,E1)
        WS.RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
        IF WS.TMP.CY.ID EQ 'JPY' THEN
            WS.RATE = ( WS.RATE / 100 )
        END
    END ELSE
        WS.RATE = 1
    END
    RETURN
**** ---------------------------------------------------------------------------- ****
GET.CUSTOMER.DATA:

    CALL F.READ(FN.CU,WS.CUS.ID.FLAG,R.CU,F.CU,E1)
    R.SCC<SCC.CUSTOMER.ID>  = WS.CUS.ID.FLAG
    R.SCC<SCC.NEW.SECTOR>   = R.CU<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
    R.SCC<SCC.NEW.INDUSTRY> = R.CU<EB.CUS.LOCAL.REF,CULR.NEW.INDUSTRY>
    R.SCC<SCC.CREDIT.CODE>  = R.CU<EB.CUS.LOCAL.REF,CULR.CREDIT.CODE>
    R.SCC<SCC.GROUP.ID>     = R.CU<EB.CUS.LOCAL.REF,CULR.GROUP.NUM>
    R.SCC<SCC.CREDIT.STATE> = R.CU<EB.CUS.LOCAL.REF,CULR.CREDIT.STAT>
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK, WS.CUS.ID.FLAG,WS.COMP)
    R.SCC<SCC.CO.CODE>      = WS.COMP
    R.SCC<SCC.LPE.DATE>     = WS.LWD.DATE

    RETURN

**** ---------------------------------------------------------------------------- ****
CLEAR.DATA:
    WS.INDIR.INT.AMT.LG    = 0
    WS.INDIR.INT.AMT.LC    = 0
    WS.DIR.INT.AMT         = 0
    WS.INDIR.USED.AMT.LG   = 0
    WS.INDIR.USED.AMT.LC   = 0
    WS.TMP.LCY.AMT         = 0
    WS.AC.AMT              = 0
    WS.RATE                = 0
    WS.TMP.LI.AMT          = 0
    WS.DIR.USED.AMT.AC     = 0
    WS.DIR.USED.AMT.AC.LC  = 0
    WS.TMP.CY.ID           = ''
    RETURN
**** ----------------------  WRITE DATA IN CURRENCY MULTI ----------------------- ****
WHEN.CY.CHANGE:
    TOTAL.USED.CHK = ( WS.DIR.USED.AMT.AC.LC + WS.DIR.USED.AMT.AC + WS.INDIR.USED.AMT.LG + WS.INDIR.USED.AMT.LC ) * -1
    TOTAL.INT.CHK  = WS.DIR.INT.AMT + WS.INDIR.INT.AMT.LG + WS.INDIR.INT.AMT.LC
    TOTAL.CHK      = TOTAL.USED.CHK + TOTAL.INT.CHK
    IF TOTAL.CHK NE 0 THEN
        WS.C++
        R.SCC<SCC.CURRENCY,WS.C>             =  WS.LI.CY.ID.FLAG
        R.SCC<SCC.DIR.INTERNAL.AMT,WS.C>     =  WS.DIR.INT.AMT        + ((WS.DIR.USED.AMT.AC.LC * -1 ))
        R.SCC<SCC.INDIR.INTERNAL.AMT,WS.C>   =  WS.INDIR.INT.AMT.LG   + WS.INDIR.INT.AMT.LC - (( WS.DIR.USED.AMT.AC.LC * -1 ))
        R.SCC<SCC.TOTAL.INTERNAL.AMT,WS.C>   =  R.SCC<SCC.DIR.INTERNAL.AMT,WS.C> + R.SCC<SCC.INDIR.INTERNAL.AMT,WS.C>
        R.SCC<SCC.DIR.USED.AMT,WS.C>         =  WS.DIR.USED.AMT.AC    + WS.DIR.USED.AMT.AC.LC
        R.SCC<SCC.INDIR.USED.AMT,WS.C>       =  WS.INDIR.USED.AMT.LG  + WS.INDIR.USED.AMT.LC
        R.SCC<SCC.TOTAL.USED.AMT,WS.C>       =  R.SCC<SCC.DIR.USED.AMT,WS.C> + R.SCC<SCC.INDIR.USED.AMT,WS.C>
    END
    WS.LI.CY.ID.FLAG = ''
    RETURN
**** ----------------------- WRITE RECORDE IN CUSTOMER CHANGE ------------------- ****
WHEN.CUS.CHANGE:
*   WS.SCC.ID = WS.CUS.ID.FLAG:'.':WS.LPE.DATE[1,6]
    WS.SCC.ID = WS.CUS.ID.FLAG:'.':WS.LWD.DATE
    CALL F.WRITE(FN.SCC,WS.SCC.ID,R.SCC)
    CALL  JOURNAL.UPDATE(WS.SCC.ID)
    GOSUB CREATE.INDEX
    WS.C  = 0
    R.SCC = ''
    RETURN
**** ----------------------- CREATE INDEX FOR CUSTOMER WHEN WRITE SCC RECORED ---------- ****
CREATE.INDEX:

    CALL F.READ(FN.CU,WS.CUS.ID.FLAG,R.CU,F.CU,E1)
    CALL F.READ(FN.SCCI,WS.CUS.ID.FLAG,R.SCCI,F.SCCI,E.SCCI)

    R.SCCI<SCCI.NEW.SECTOR>   = R.CU<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
    R.SCCI<SCCI.NEW.INDUSTRY> = R.CU<EB.CUS.LOCAL.REF,CULR.NEW.INDUSTRY>
    R.SCCI<SCCI.CREDIT.CODE>  = R.CU<EB.CUS.LOCAL.REF,CULR.CREDIT.CODE>
    R.SCCI<SCCI.GROUP.NUM>    = R.CU<EB.CUS.LOCAL.REF,CULR.GROUP.NUM>
    R.SCCI<SCCI.CREDIT.STAT>  = R.CU<EB.CUS.LOCAL.REF,CULR.CREDIT.STAT>
    R.SCCI<SCCI.CO.CODE>      = R.CU<EB.CUS.COMPANY.BOOK>

    IF NOT (E.SCCI) THEN
*Line [ 293 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = DCOUNT(R.SCCI<SCCI.SCC.INDEX>,@VM)
        WS.COUNT ++
        R.SCCI<SCCI.SCC.INDEX,WS.COUNT> = WS.SCC.ID
        CALL F.WRITE(FN.SCCI,WS.CUS.ID.FLAG,R.SCCI)
        CALL  JOURNAL.UPDATE(WS.CUS.ID.FLAG)
    END ELSE

        R.SCCI<SCCI.SCC.INDEX,1> = WS.SCC.ID
        CALL F.WRITE(FN.SCCI,WS.CUS.ID.FLAG,R.SCCI)
        CALL  JOURNAL.UPDATE(WS.CUS.ID.FLAG)
    END

    R.SCCI   = ''
    WS.COUNT = 0

    RETURN
**** ---------------------------------------------------------------------------- ****
CREATE.DATE.INDEX:
    DATE.IND = 'DATE.IND'
    CALL F.READ(FN.SCCI,DATE.IND,R.SCCI,F.SCCI,E.SCCI2)
    IF NOT (E.SCCI2) THEN
*Line [ 315 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = DCOUNT(R.SCCI<SCCI.SCC.INDEX>,@VM)
        WS.COUNT ++
        R.SCCI<SCCI.SCC.INDEX,WS.COUNT> = WS.LWD.DATE
        CALL F.WRITE(FN.SCCI,DATE.IND,R.SCCI)
        CALL JOURNAL.UPDATE(DATE.IND)
    END ELSE
        R.SCCI<SCCI.SCC.INDEX,1> = WS.LWD.DATE
        CALL F.WRITE(FN.SCCI,DATE.IND,R.SCCI)
        CALL JOURNAL.UPDATE(DATE.IND)
    END

    R.SCCI   = ''
    WS.COUNT = 0
    RETURN

**** ---------------------------------------------------------------------------- ****
