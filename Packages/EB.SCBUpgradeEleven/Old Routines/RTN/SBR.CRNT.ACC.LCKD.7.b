* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
************************** BAKRY & MAHMOUD 24/5/2012 *************************
    PROGRAM SBR.CRNT.ACC.LCKD.7

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS

    EXECUTE 'OFLIN'
*================ Read User From Voc
    FN.TM = "VOC" ; F.TM = ''
    CALL OPF(FN.TM,F.TM)
    CALL F.READ(FN.TM,'CHK.LIN',R.TM,F.TM,ER10)
    IF R.TM EQ 'CAIRO' THEN
*==================================================================
        EXECUTE "sh ATM.OF.LINE/OBJ/killswitch.sh"
        EXECUTE "clear"
*==================================================================
        YTEXT  = 'DO YOU WANT TO CONTINUE Y/N'
        CALL TXTINP(YTEXT, 8, 22, "1", "A")
        IF COMI EQ 'Y' OR COMI EQ 'y' THEN
            COMP = ID.COMPANY

            GOSUB INITIATE
*Line [ 56 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            GOSUB OPENFILE
            GOSUB PROCESS
        END
    END ELSE
        CRT ' YOU CANN`T RUN THIS PROGRAM FROM THIS USER '
    END
    RETURN
************************************************************************************
INITIATE:
*--------
    AC.ID         = ''
    CY.ID         = ''
    XX            = ''
    S.SEL         = ''
    KK1           = ''
    BB.DATA       = ''
    PATHNAME1 = "ATM.OF.LINE/IN"
    FILENAME1 = "CRNT.ACC.LCKD"

    TOT.AC.BAL   = 0
    TOT.AC.LCK   = 0

    RETURN
****************************************
CALLDB:
*--------
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC = '' ; R.CU.AC = '' ; ER.CU.AC = ''
    CALL OPF(FN.CU.AC,F.CU.AC)
    FN.LK.EV = 'FBNK.AC.LOCKED.EVENTS' ; F.LK.EV = '' ; R.LK.EV = '' ; ER.LK.EV = ''
    CALL OPF(FN.LK.EV,F.LK.EV)
    RETURN
****************************************
PROCESS:
*-------
    S.SEL  = "SELECT ":FN.AC
    S.SEL := " WITH CATEGORY EQ 1001 AND CURRENCY EQ EGP"
    CALL EB.READLIST(S.SEL,K.LIST,'',SELECTED,ER.MSG)
    TOT.NUM.AC = SELECTED
    CRT "Selected AC     = ":SELECTED
    LOOP
        REMOVE AC.ID FROM K.LIST SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
        GL.ID   = R.AC<AC.CATEGORY>
        CY.ID   = R.AC<AC.CURRENCY>
        AC.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
        TOT.AC.BAL += AC.BAL
    REPEAT
    LK.SEL = "SELECT ":FN.LK.EV
    CALL EB.READLIST(LK.SEL,LK.LIST,'',SELECTED.LK,ER.MSG.LK)
    TOT.NUM.LCKD = SELECTED.LK
    CRT "Selected LOCKED = ":SELECTED.LK
    LOOP
        REMOVE LK.ID FROM LK.LIST SETTING POS.LK
    WHILE LK.ID:POS.LK
        CALL F.READ(FN.LK.EV,LK.ID,R.LK.EV,F.LK.EV,ER.LK.EV)
        AC.ID   = R.LK.EV<AC.LCK.ACCOUNT.NUMBER>
        AC.LCKD = R.LK.EV<AC.LCK.LOCKED.AMOUNT>
        TOT.AC.LCK += AC.LCKD
    REPEAT
    CRT "TOTAL.OPEN.BALANCE  = ": FMT(TOT.AC.BAL,"L2,")
    CRT "TOTAL.LOCKED.AMOUNT = ": FMT(TOT.AC.LCK,"L2,")
    GOSUB WRITE.DATA
    EXECUTE "ls -l ATM.OF.LINE/IN"
    PROMPT "START COPY TABLES TO LIVEBK Y / N ===> " ; INPUT  AA
    IF AA = "Y" OR AA = "y" THEN
        EXECUTE "sh ATM.OF.LINE/OBJ/send.d.to.test.sh"
    END
    RETURN
****************************************
OPENFILE:
*--------
    OPENSEQ PATHNAME1 , FILENAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATHNAME1:' ':FILENAME1
        HUSH OFF
    END
    OPENSEQ PATHNAME1 , FILENAME1 TO BB ELSE
        CREATE BB THEN
            CRT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END
    RETURN
****************************************
WRITE.DATA:
*----------
    BB.DATA  = TOT.NUM.AC:'|'
    BB.DATA := TOT.AC.BAL:'|'
    BB.DATA := TOT.NUM.LCKD:'|'
    BB.DATA := TOT.AC.LCK:'|'

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
****************************************
END
