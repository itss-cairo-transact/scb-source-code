* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1023</Rating>
*-----------------------------------------------------------------------------
*** ���� ������� ���� �������� ***
***

*** CREATED BY KHALED ***
***=================================

    SUBROUTINE SBM.FOR.DEP.IN.EG
*** PROGRAM SBM.FOR.DEP.IN.EG

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY


*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    COMP = ID.COMPANY

    COMP.BR = COMP[2]
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
*Line [ 63 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.FOR.DEP.IN.EG'
    CALL PRINTER.ON(REPORT.ID,'')

    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")

    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')
    T.DAY1  = FMT(DAT,"####/##/##")



    RETURN
*========================================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    HEAD1 = " ����� ������ �������"


*************************************************************************
    CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
    FIX.RATE = RATE<1,1>
*====================================

**    T.SEL1 = "SELECT ":FN.CBE: " WITH CBEM.CATEG IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21020 21021 21022 21023 21024 21025 6501 6502 6503 6504 6511 6512) BY CBEM.CY BY CBEM.CUSTOMER.CODE AND CBEM.BR EQ ":COMP.BR
    T.SEL1 = "SELECT ":FN.CBE: " WITH CBEM.CATEG IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21017 21018 21019 21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032 21041 21042  6501 6502 6503 6504 6511 6512) AND CBEM.BR EQ ":COMP.BR:" BY CBEM.CY BY CBEM.CUSTOMER.CODE BY CBEM.CATEG"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN

        FOR I = 1 TO 2

            REPORT.NO = I


*           -----------
            WS.CY = ''
            T.FCY.AMOUNT = 0
            T.USD.AMOUNT = 0

*           -----------
            IF REPORT.NO EQ 1 THEN
                HEAD2 = " �������� ���� ���  "
            END

            IF REPORT.NO EQ 2 THEN
                HEAD2 = " �������� ���� ���  "

            END
*           -----------
            GOSUB PRINT.HEAD

*           -----------
            FOR J = 1 TO SELECTED1

                CALL F.READ(FN.CBE,KEY.LIST1<J>,R.CBE,F.CBE,E2)
                CUS.ID   = R.CBE<C.CBEM.CUSTOMER.CODE>
                CAT.ID   = R.CBE<C.CBEM.CATEG>

                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
                CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>

                NAT      = R.CU<EB.CUS.NATIONALITY>
                RES      = R.CU<EB.CUS.RESIDENCE>
                POST     = R.CU<EB.CUS.POSTING.RESTRICT>

*-------------------------------------------------------------
                TRUE.FLAG = 'NO'

                IF REPORT.NO EQ 1  THEN
                    IF RES EQ 'EG'  THEN
                        IF NAT NE 'EG'  THEN
                            IF POST LE 80 THEN
                                TRUE.FLAG = 'YES'
                            END
                        END
                    END
                END
                IF REPORT.NO EQ 2  THEN
                    IF RES NE 'EG'  THEN
                        IF NAT NE 'EG'  THEN
                            IF POST LE 80 THEN
                                TRUE.FLAG = 'YES'
                            END
                        END
                    END
                END
*-------------------------------------------------------------
****                IF NAT NE 'EG' AND RES EQ 'EG' AND POST LE 80 THEN
****                IF NAT NE 'EG' AND TRUE.FLAG EQ 'YES' AND POST LE 80 THEN
*-------------------------------------------------------------

                IF TRUE.FLAG EQ 'YES' THEN
*           -----------

                    CUR = R.CBE<C.CBEM.CY>

                    FCY.AMOUNT = R.CBE<C.CBEM.IN.FCY>
                    USD.AMOUNT = R.CBE<C.CBEM.IN.LCY>
                    USD.AMOUNT  = USD.AMOUNT / FIX.RATE

*           -----------
                    IF CUR EQ 'USD' THEN
                        USD.AMOUNT  = FCY.AMOUNT
                    END

*---------------------
                    BEGIN CASE
                    CASE WS.CY EQ ''
                        WS.CY = CUR
                        T.FCY.AMOUNT = FCY.AMOUNT
                        T.USD.AMOUNT = USD.AMOUNT

*---------------------
                    CASE WS.CY EQ CUR
                        T.FCY.AMOUNT += FCY.AMOUNT
                        T.USD.AMOUNT += USD.AMOUNT

*---------------------
                    CASE WS.CY NE CUR

                        XX               = SPACE(140)
                        PRINT XX<1,1>


                        XX<1,1>[51,40]   = "***  ������ ������  ***"
                        XX<1,1>[91,20] = FMT(T.FCY.AMOUNT,"R2,")
                        XX<1,1>[111,20] = FMT(T.USD.AMOUNT,"R2,")
                        PRINT XX<1,1>

                        XX               = SPACE(140)
                        PRINT XX<1,1>
                        PRINT XX<1,1>

                        WS.CY = CUR
                        T.FCY.AMOUNT = FCY.AMOUNT
                        T.USD.AMOUNT = USD.AMOUNT

                    END CASE
*************************************************************************

                    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,CAT.ID,S.NAME)
                    CATEG.NAME = S.NAME
                    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CY.NAME)
                    CURR.NAME = CY.NAME


                    XX               = SPACE(140)
                    XX<1,1>[1,10]    = CUS.ID
                    XX<1,1>[11,40]   = CUST.NAME
                    XX<1,1>[51,20]   = CATEG.NAME
                    XX<1,1>[71,20]   = CURR.NAME
                    XX<1,1>[91,20]   = FMT(FCY.AMOUNT,"R2,")
                    XX<1,1>[111,20]  = FMT(USD.AMOUNT,"R2,")

                    PRINT XX<1,1>


*************************************************************************
                END


            NEXT J

            IF REPORT.NO EQ 1  THEN
                XX               = SPACE(140)
                PRINT XX<1,1>
                XX<1,1>[51,40]   = "***  ������ ������  ***"


                XX<1,1>[91,20] = FMT(T.FCY.AMOUNT,"R2,")
                XX<1,1>[111,20] = FMT(T.USD.AMOUNT,"R2,")

                PRINT XX<1,1>
                XX               = SPACE(140)
                PRINT XX<1,1>
            END

        NEXT I

****        GOSUB PRINT.HEAD

        XX               = SPACE(140)
        PRINT XX<1,1>
        XX<1,1>[51,40]   = "***  ������ ������  ***"


        XX<1,1>[91,20] = FMT(T.FCY.AMOUNT,"R2,")
        XX<1,1>[111,20] = FMT(T.USD.AMOUNT,"R2,")

        PRINT XX<1,1>
        XX               = SPACE(140)
        PRINT XX<1,1>


    END

    XX    = SPACE(140)
    PRINT XX<1,1>

    PRINT STR('-',50):"   �������� ������������    ":STR('-',50)
*************************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(107):REPORT.ID
    PR.HD :="'L'":SPACE(1):HEAD1
    PR.HD :="'L'":SPACE(1):HEAD2
    PR.HD :="'L'":SPACE(50):"���� ���������� ����� ��������"
    PR.HD :="'L'":SPACE(50):"���� : ":T.DAY1:"  ���� ������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
*    PR.HD :="'L'":"����������":SPACE(10):"���� �������":SPACE(7):"������� ��������":SPACE(7):"���� �������":SPACE(7):"������� ��������":SPACE(7):"������ �������"
*    PR.HD :="'L'":SPACE(20):"������ �������":SPACE(7):"��������":SPACE(12):"������� ������":SPACE(6):"�������� �������":SPACE(7):"�������� ��������"

    PR.HD :="'L'":"���":SPACE(10):"���":SPACE(34):"��� �������":SPACE(9):"���������":SPACE(11):"������� �������":SPACE(6):"������� ��������"
    PR.HD :="'L'":"������":SPACE(7):"������"




*    PR.HD :="'L'":SPACE(82):"������� ������"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
