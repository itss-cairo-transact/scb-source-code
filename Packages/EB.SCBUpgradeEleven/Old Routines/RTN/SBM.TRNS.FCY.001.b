* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>160</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.TRNS.FCY.001

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 45 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:

 REPORT.ID='P.FUNCTION'
**********UPDATED BY RIHAM 13/11/2011 TO PRINT IN LANDSCAP **************
 *REPORT.ID='SBM.TRNS.FCY.001'
    CALL PRINTER.ON(REPORT.ID,'')

    XX  = SPACE(120) ; XX1 = SPACE(120)
    COMP = ID.COMPANY

    TD1 = TODAY[1,6]:'01'
    CALL CDT("",TD1,'-1C')

    RETURN
*===============================================================
CALLDB:
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ;  F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""

    OPN.BAL.LCY = 0
    CLS.BAL.LCY = 0
    DB.MOV.LCY  = 0
    CR.MOV.LCY  = 0

    DB.MOV.TOT  = 0
    CR.MOV.TOT  = 0
    CLS.BAL.TOT = 0
    OPN.BAL.TOT = 0


    T.SEL  = "SELECT ":FN.LN:" WITH @ID IN (TRNSFCY.0010 TRNSFCY.0020 TRNSFCY.0030) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,1,1>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 96 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
*************************************************************************
            FOR X = 1 TO POS
                BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":COMP
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                RATE = R.CCY<RE.BCP.RATE,X>

                CUR.ID  = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                DB.MOV  = R.BAL<RE.SLB.DB.MVMT.MTH>
                CR.MOV  = R.BAL<RE.SLB.CR.MVMT.MTH>
                CLS.BAL = R.BAL<RE.SLB.CLOSING.BAL>

                DB.MOV.LCY  += DB.MOV * RATE
                DB.MOV.LCY   = DROUND(DB.MOV.LCY,'2')

                CR.MOV.LCY  += CR.MOV * RATE
                CR.MOV.LCY   = DROUND(CR.MOV.LCY,'2')

                CLS.BAL.LCY += CLS.BAL * RATE
                CLS.BAL.LCY  = DROUND(CLS.BAL.LCY,'2')

                OPN.BAL.LCY  = CLS.BAL.LCY - CR.MOV.LCY - DB.MOV.LCY

                DB.MOV.TOT  += DB.MOV * RATE
                CR.MOV.TOT  += CR.MOV * RATE
                CLS.BAL.TOT += CLS.BAL * RATE
                OPN.BAL.TOT  = CLS.BAL.TOT - CR.MOV.TOT - DB.MOV.TOT

                DB.MOV.TOT   = DROUND(DB.MOV.TOT,'2')
                CR.MOV.TOT   = DROUND(CR.MOV.TOT,'2')
                OPN.BAL.TOT  = DROUND(OPN.BAL.TOT,'2')
                CLS.BAL.TOT  = DROUND(CLS.BAL.TOT,'2')

            NEXT X
            XX<1,I>[1,25]   = Y.DESC
            XX<1,I>[30,15]  = OPN.BAL.LCY
            XX<1,I>[50,15]  = CR.MOV.LCY
            XX<1,I>[70,15]  = DB.MOV.LCY
            XX<1,I>[95,15]  = CLS.BAL.LCY

            PRINT XX<1,I>
            PRINT STR(' ',120)

            OPN.BAL.LCY = 0
            CR.MOV.LCY  = 0
            DB.MOV.LCY  = 0
            CLS.BAL.LCY = 0

        NEXT I

        XX1<1,I>[1,25]   = "��������"
        XX1<1,I>[30,15]  = OPN.BAL.TOT
        XX1<1,I>[50,15]  = CR.MOV.TOT
        XX1<1,I>[70,15]  = DB.MOV.TOT
        XX1<1,I>[95,15]  = CLS.BAL.TOT

        PRINT STR('=',120)
        PRINT XX1<1,I>
        PRINT STR('=',120)

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TD = TODAY
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD1[7,2]:'/':TD1[5,2]:"/":TD1[1,4]
    YYBRN  = BRANCH
    DATY   = TODAY

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� �������� ����� ������ ������� : ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������" :SPACE(20):"���� �������":SPACE(10):"���� ����":SPACE(10):"���� ����":SPACE(15):"������ �������"
    PR.HD :="'L'":STR('_',120)
    HEADING PR.HD
    RETURN
*==============================================================
END
