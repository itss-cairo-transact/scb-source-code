* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.DETAIL.LOAN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

    EXECUTE "CONV.BRANCH.DETAIL.LOAN"
    EXECUTE "CONV.BRANCH.DETAIL.LOAN.COMP"
    EXECUTE "CONV.BRANCH.DETAIL.LOAN.EMP"
    EXECUTE "CONV.BRANCH.DETAIL.LOAN.PR"
    EXECUTE "CONV.CUR.DETAIL.LOAN"
    EXECUTE "CONV.CUR.DETAIL.LOAN.COMP"
    EXECUTE "CONV.CUR.DETAIL.LOAN.EMP"
    EXECUTE "CONV.CUR.DETAIL.LOAN.PR"

    TEXT = '�� ����� �������' ; CALL REM

*-----------------------------------------------------
    RETURN
END
