* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>36</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.EMP.INT.2PRCNT.006
*    PROGRAM SBM.EMP.INT.2PRCNT.006

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMP.INT.2.TEMP
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMP.INT.2.PERM
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*---------------------------------------------------
    FN.TEMP = "F.SCB.EMP.INT.2.TEMP"    ; F.TEMP = ""
*
    FN.AC = "FBNK.ACCOUNT"              ; F.AC = ""
*
    FN.DATE = "F.DATES"                 ; F.DATE  = ""
*
    FN.PERM = "F.SCB.EMP.INT.2.PERM"    ; F.PERM = ""
*----------------------------------------------------
    CALL OPF (FN.TEMP,F.TEMP)
    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.PERM,F.PERM)
    CALL OPF (FN.AC,F.AC)
*-------------------------------------------------
    R.TEMP = ""
*------------------------------------------------
    WS.PERM.INT = 0
*------------------------------------------------
    GOSUB A.10.DATE
    GOSUB A.50.GET.INT
    RETURN
*----------------------------------------
A.10.DATE:
    WS.DATE.ID      = "EG0010001-COB"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.PER.END = R.DATE<EB.DAT.PERIOD.END>
    WS.PE.DATE      = R.DATE<EB.DAT.PERIOD.END>
    WS.LAST.PER.END = WS.PE.DATE
    WS.YY.MM        = WS.LAST.PER.END[1,6]
    WS.DD           = WS.LAST.PER.END[2]
    WS.MM           = WS.YY.MM[5,2] + 0
    WS.LOCAT.DATE   = WS.YY.MM:"01"
    WS.ACC.OFFICER  = ID.COMPANY[2]
    RETURN
*-----------------------------------------
**** ADDED BY MOHAMED SABRY 2012/09/02

A.50.GET.INT:
*------------

    SEL.PERM = "SELECT ":FN.PERM:" WITH CO.CODE EQ ":ID.COMPANY:" BY MONTH.PAY BY @ID "
    CALL EB.READLIST(SEL.PERM,KEY.LIST.PERM,"",SELECTED.PERM,ER.MSG.PERM)
    IF SELECTED.PERM THEN
        FOR I.PERM = 1 TO SELECTED.PERM
            CALL F.READ(FN.PERM,KEY.LIST.PERM<I.PERM>,R.PERM,F.PERM,ER.PERM)

            WS.AC.ID.PERM = KEY.LIST.PERM<I.PERM>

            CALL F.READ(FN.AC,WS.AC.ID.PERM,R.AC,F.AC,ER.AC)
            IF NOT(ER.AC) THEN
                LOCATE WS.YY.MM IN R.PERM<EMP.PERM.YYMM,1> SETTING POS THEN
                    AMT = R.PERM<EMP.PERM.INT><1,POS>
*                    END

                    CAT.POS = R.AC<AC.CATEGORY>[2]
                    IF R.PERM<EMP.PERM.INT><1,POS> GT 0 THEN
                        WS.PERM.INT<1,CAT.POS> += R.PERM<EMP.PERM.INT><1,POS>
                    END
                END
            END
        NEXT I.PERM
    END
    FOR AA.PERM = 1 TO 99

        IF WS.PERM.INT<1,AA.PERM> GT 0 THEN
            WS.PRODUCT.CATEGORY = 65:FMT(AA.PERM,'R%2')
            CUR.CODE   = "EGP"
            WS.AC.CUSTOMER     = ""
            FCY.AMTT   = ""
            REFF       = WS.PRODUCT.CATEGORY
            RATEE      = ""
            LOC.AMTT   = WS.PERM.INT<1,AA.PERM>
            Y.ACCT     = CUR.CODE:"1660600":FMT(AA.PERM,'R%2'):"00":WS.ACC.OFFICER
            GOSUB TRNS.FROM.AC
            PRINT
            PRINT "PERM.IN AC= ":Y.ACCT:"=="::LOC.AMTT
            PRINT
            PRINT WS.PRODUCT.CATEGORY
            PRINT
            LOC.AMTT = WS.PERM.INT<1,AA.PERM> * -1
           GOSUB TRNS.FROM.PL
            PRINT "PERM.IN AC= ":Y.ACCT:"=="::LOC.AMTT
        END
    NEXT AA.PERM


    RETURN
**** END OF ADDED BY MOHAMED SABRY 2012/08/28
*--------------------------------------------
TRNS.FROM.PL:
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
******************************************
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ""
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = "891"
    ENTRY<AC.STE.THEIR.REFERENCE>  = ""
    ENTRY<AC.STE.TRANS.REFERENCE>  = ""
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = "50000"
    ENTRY<AC.STE.AMOUNT.LCY>       = LOC.AMTT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = WS.ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = WS.PRODUCT.CATEGORY
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CUR.CODE
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMTT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATEE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = WS.AC.CUSTOMER
    ENTRY<AC.STE.OUR.REFERENCE>    = REFF

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN
*-------------------------------------------------
TRNS.FROM.AC:
**********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
******************************************
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '891'
    ENTRY<AC.STE.THEIR.REFERENCE>  = REFF
    ENTRY<AC.STE.TRANS.REFERENCE>  = REFF
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LOC.AMTT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = WS.ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = WS.PRODUCT.CATEGORY
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CUR.CODE
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMTT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATEE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = REFF

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN
*-------------------------------------------------
END
