* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.C.PRT.CRT.01

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST
*--------------------------
    FN.NEW = "F.CBE.STATIC.MAST.P"
    F.NEW  = ""

    FN.OLD = "F.CBE.STATIC.MAST"
    F.OLD  = ""
*---------------------
    CALL OPF (FN.NEW,F.NEW)
    CALL OPF (FN.OLD,F.OLD)
*----------------------------------
    R.NEWR      = ""
    R.OLDR      = ""
    WS.NEW.KEY  = ""
    WS.AMT.TMP  =  0
    CLEARFILE F.NEW
*----------------------------------
    GOSUB A.050.PROC
    RETURN
*----------------------------------
A.050.PROC:
    SEL.CMD = "SELECT ":FN.OLD
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.OLD.ID FROM SEL.LIST SETTING POS
    WHILE WS.OLD.ID:POS

        CALL F.READ(FN.OLD,WS.OLD.ID,R.OLDR,F.OLD,MSG.OLD)
        WS.BR    = R.OLDR<C.CBE.BR>
        WS.SECT  =  R.OLDR<C.CBE.NEW.SECTOR>
        WS.INDST =  R.OLDR<C.CBE.NEW.INDUSTRY>

        IF WS.SECT EQ 1110 OR WS.SECT EQ 2110 OR WS.SECT EQ 3110 OR WS.SECT EQ 4110 THEN
            WS.FRST.ID = "BB"
            GOSUB   A.300.CRT
        END
        IF WS.SECT GE 8001 AND WS.SECT LE 8004 THEN
            WS.FRST.ID = "BB"
            GOSUB   A.300.CRT
        END
        IF WS.SECT EQ 1130 OR WS.SECT EQ 2130 OR WS.SECT EQ 3130 OR WS.SECT EQ 4130 THEN
            WS.FRST.ID = "CC"
            GOSUB   A.300.CRT
        END
        IF WS.SECT EQ 1120 OR WS.SECT EQ 2120 OR WS.SECT EQ 3120 OR WS.SECT EQ 4120 THEN
            WS.FRST.ID = "DD"
            GOSUB   A.300.CRT
        END

        WS.SECT.INSURANCE = ""
        IF WS.SECT EQ 1130 OR WS.SECT EQ 2130 OR WS.SECT EQ 3130 OR WS.SECT EQ 4130 THEN
            GOSUB A.100.CHG.INSURANCE
        END
        WS.NEW.KEY = "EG00100":WS.BR:"*":WS.SECT:WS.SECT.INSURANCE
        MSG.NEW    = ""
        CALL F.READ(FN.NEW,WS.NEW.KEY,R.NEWR,F.NEW,MSG.NEW)
        IF MSG.NEW EQ ""  THEN
            GOSUB A.150.EXIST
            GOTO AAAA
        END
        GOSUB A.200.NEW.REC
AAAA:
    REPEAT
BBBB:
    RETURN
*--------------------------------------------------------
*����� ����� ������� ����� �����
A.100.CHG.INSURANCE:

    IF  WS.INDST  EQ 2070 THEN
        WS.SECT.INSURANCE = "A"
    END
    RETURN
*-------------------------------------------------------
A.150.EXIST:
    WS.AMT.TMP = R.NEWR<P.CBE.CUR.AC.LE>
    WS.AMT.TMP = WS.AMT.TMP +  R.OLDR<C.CBE.CUR.AC.LE>
    R.NEWR<P.CBE.CUR.AC.LE> = WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  = R.NEWR<P.CBE.CUR.AC.LE.DR>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CUR.AC.LE.DR>
    R.NEWR<P.CBE.CUR.AC.LE.DR> = WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.CUR.AC.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CUR.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.CUR.AC.LE.NO.OF.AC>  =  WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.DEPOST.AC.LE.1Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.DEPOST.AC.LE.1Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.1Y>  =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.DEPOST.AC.LE.2Y>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.DEPOST.AC.LE.2Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.2Y>  =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.DEPOST.AC.LE.3Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.DEPOST.AC.LE.3Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.3Y>    =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.DEPOST.AC.LE.MOR3>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.DEPOST.AC.LE.MOR3>
    R.NEWR<P.CBE.DEPOST.AC.LE.MOR3>   =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.DEPOST.AC.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.DEPOST.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.DEPOST.AC.LE.NO.OF.AC> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.CER.AC.LE.3Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CER.AC.LE.3Y>
    R.NEWR<P.CBE.CER.AC.LE.3Y>   =      WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =    R.NEWR<P.CBE.CER.AC.LE.5Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CER.AC.LE.5Y>
    R.NEWR<P.CBE.CER.AC.LE.5Y> =  WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.CER.AC.LE.GOLD>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CER.AC.LE.GOLD>
    R.NEWR<P.CBE.CER.AC.LE.GOLD> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.CER.AC.LE.NO.OF.CER>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CER.AC.LE.NO.OF.CER>
    R.NEWR<P.CBE.CER.AC.LE.NO.OF.CER>  =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.SAV.AC.LE>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.SAV.AC.LE>
    R.NEWR<P.CBE.SAV.AC.LE>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.SAV.AC.LE.DR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.SAV.AC.LE.DR>
    R.NEWR<P.CBE.SAV.AC.LE.DR> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.SAV.AC.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.SAV.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.SAV.AC.LE.NO.OF.AC>  = WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.BLOCK.AC.LE>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.BLOCK.AC.LE>
    R.NEWR<P.CBE.BLOCK.AC.LE>  =     WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.BLOCK.AC.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.BLOCK.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.BLOCK.AC.LE.NO.OF.AC> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =    R.NEWR<P.CBE.MARG.LC.LE>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.MARG.LC.LE>
    R.NEWR<P.CBE.MARG.LC.LE>   =  WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.MARG.LC.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.MARG.LC.LE.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LC.LE.NO.OF.AC> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.MARG.LG.LE>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.MARG.LG.LE>
    R.NEWR<P.CBE.MARG.LG.LE> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.MARG.LG.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.MARG.LG.LE.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LG.LE.NO.OF.AC> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.FACLTY.LE>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.FACLTY.LE>
    R.NEWR<P.CBE.FACLTY.LE>  =  WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  = R.NEWR<P.CBE.FACLTY.LE.CR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.FACLTY.LE.CR>
    R.NEWR<P.CBE.FACLTY.LE.CR> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.FACLTY.LE.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.FACLTY.LE.NO.OF.AC>
    R.NEWR<P.CBE.FACLTY.LE.NO.OF.AC> =   WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.LE.S>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.S>
    R.NEWR<P.CBE.LOANS.LE.S> =           WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.S.CR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.S.CR>
    R.NEWR<P.CBE.LOANS.LE.S.CR>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.S.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.S.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.S.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.M>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.LE.M>
    R.NEWR<P.CBE.LOANS.LE.M>       =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.M.CR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.M.CR>
    R.NEWR<P.CBE.LOANS.LE.M.CR>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.M.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.M.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.M.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.L>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.LE.L>
    R.NEWR<P.CBE.LOANS.LE.L>   =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.L.CR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.L.CR>
    R.NEWR<P.CBE.LOANS.LE.L.CR>   =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.LE.L.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.LOANS.LE.L.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.L.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.COMRCL.PAPER.LE>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.COMRCL.PAPER.LE>
    R.NEWR<P.CBE.COMRCL.PAPER.LE>   =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.COMRCL.PAPER.LE.NO>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.COMRCL.PAPER.LE.NO>
    R.NEWR<P.CBE.COMRCL.PAPER.LE.NO>   =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.CUR.AC.EQ>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CUR.AC.EQ>
    R.NEWR<P.CBE.CUR.AC.EQ> =     WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.CUR.AC.EQ.DR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.CUR.AC.EQ.DR>
    R.NEWR<P.CBE.CUR.AC.EQ.DR> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.CUR.AC.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.CUR.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.CUR.AC.EQ.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.DEPOST.AC.EQ.1Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.DEPOST.AC.EQ.1Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.1Y>    =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.DEPOST.AC.EQ.2Y>
    WS.AMT.TMP  = WS.AMT.TMP  +  R.OLDR<C.CBE.DEPOST.AC.EQ.2Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.2Y> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.DEPOST.AC.EQ.3Y>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.DEPOST.AC.EQ.3Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.3Y>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.DEPOST.AC.EQ.MOR3>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.DEPOST.AC.EQ.MOR3>
    R.NEWR<P.CBE.DEPOST.AC.EQ.MOR3> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.DEPOST.AC.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP  +  R.OLDR<C.CBE.DEPOST.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.DEPOST.AC.EQ.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.CER.AC.EQ.3Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CER.AC.EQ.3Y>
    R.NEWR<P.CBE.CER.AC.EQ.3Y> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.CER.AC.EQ.5Y>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.CER.AC.EQ.5Y>
    R.NEWR<P.CBE.CER.AC.EQ.5Y> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.CER.AC.EQ.GOLD>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.CER.AC.EQ.GOLD>
    R.NEWR<P.CBE.CER.AC.EQ.GOLD> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.CER.AC.EQ.NO.OF.CER>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.CER.AC.EQ.NO.OF.CER>
    R.NEWR<P.CBE.CER.AC.EQ.NO.OF.CER> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =    R.NEWR<P.CBE.SAV.AC.EQ>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.SAV.AC.EQ>
    R.NEWR<P.CBE.SAV.AC.EQ>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =    R.NEWR<P.CBE.SAV.AC.EQ.DR>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.SAV.AC.EQ.DR>
    R.NEWR<P.CBE.SAV.AC.EQ.DR> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.SAV.AC.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.SAV.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.SAV.AC.EQ.NO.OF.AC>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.BLOCK.AC.EQ>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.BLOCK.AC.EQ>
    R.NEWR<P.CBE.BLOCK.AC.EQ>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =    R.NEWR<P.CBE.BLOCK.AC.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.BLOCK.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.BLOCK.AC.EQ.NO.OF.AC>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.MARG.LC.EQ>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.MARG.LC.EQ>
    R.NEWR<P.CBE.MARG.LC.EQ>  = WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.MARG.LC.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP +  R.OLDR<C.CBE.MARG.LC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LC.EQ.NO.OF.AC>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.MARG.LG.EQ>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.MARG.LG.EQ>
    R.NEWR<P.CBE.MARG.LG.EQ> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.MARG.LG.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP  +  R.OLDR<C.CBE.MARG.LG.EQ.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LG.EQ.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.FACLTY.EQ>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.FACLTY.EQ>
    R.NEWR<P.CBE.FACLTY.EQ> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.FACLTY.EQ.CR>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.FACLTY.EQ.CR>
    R.NEWR<P.CBE.FACLTY.EQ.CR>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.FACLTY.EQ.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP  + R.OLDR<C.CBE.FACLTY.EQ.NO.OF.AC>
    R.NEWR<P.CBE.FACLTY.EQ.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.EQ.S>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.S>
    R.NEWR<P.CBE.LOANS.EQ.S>   =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.EQ.S.CR>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.S.CR>
    R.NEWR<P.CBE.LOANS.EQ.S.CR>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.EQ.S.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.S.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.S.NO.OF.AC> =     WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.EQ.M>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.M>
    R.NEWR<P.CBE.LOANS.EQ.M> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.EQ.M.CR>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.M.CR>
    R.NEWR<P.CBE.LOANS.EQ.M.CR> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.LOANS.EQ.M.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.M.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.M.NO.OF.AC>  =     WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.EQ.L>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.L>
    R.NEWR<P.CBE.LOANS.EQ.L>     =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.EQ.L.CR>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.L.CR>
    R.NEWR<P.CBE.LOANS.EQ.L.CR>  =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =   R.NEWR<P.CBE.LOANS.EQ.L.NO.OF.AC>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.LOANS.EQ.L.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.L.NO.OF.AC> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.COMRCL.PAPER.EQ>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.COMRCL.PAPER.EQ>
    R.NEWR<P.CBE.COMRCL.PAPER.EQ> =    WS.AMT.TMP
*.......................................................
    WS.AMT.TMP  =  R.NEWR<P.CBE.COMRCL.PAPER.EQ.NO>
    WS.AMT.TMP  = WS.AMT.TMP + R.OLDR<C.CBE.COMRCL.PAPER.EQ.NO>
    R.NEWR<P.CBE.COMRCL.PAPER.EQ.NO>  =    WS.AMT.TMP
*.....................
    CALL F.WRITE(FN.NEW,WS.NEW.KEY,R.NEWR)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)
    RETURN
    RETURN
*--------------------------------------------------------
A.200.NEW.REC:
    R.NEWR<P.CBE.CUSTOMER.CODE> =  WS.NEW.KEY
    R.NEWR<P.CBE.BR> =  WS.BR
    R.NEWR<P.CBE.INDUSTRY> =      ""
    R.NEWR<P.CBE.SECTOR> =        ""
    R.NEWR<P.CBE.LEGAL.FORM> =    ""
    R.NEWR<P.CBE.NEW.SECTOR> = R.OLDR<C.CBE.NEW.SECTOR>
    R.NEWR<P.CBE.NEW.INDUSTRY> =  ""

    R.NEWR<P.CBE.CUR.AC.LE>  = R.OLDR<C.CBE.CUR.AC.LE>
    R.NEWR<P.CBE.CUR.AC.LE.DR> = R.OLDR<C.CBE.CUR.AC.LE.DR>
    R.NEWR<P.CBE.CUR.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.CUR.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.DEPOST.AC.LE.1Y> = R.OLDR<C.CBE.DEPOST.AC.LE.1Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.2Y> = R.OLDR<C.CBE.DEPOST.AC.LE.2Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.3Y> = R.OLDR<C.CBE.DEPOST.AC.LE.3Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.MOR3> = R.OLDR<C.CBE.DEPOST.AC.LE.MOR3>
    R.NEWR<P.CBE.DEPOST.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.DEPOST.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.CER.AC.LE.3Y> = R.OLDR<C.CBE.CER.AC.LE.3Y>
    R.NEWR<P.CBE.CER.AC.LE.5Y> = R.OLDR<C.CBE.CER.AC.LE.5Y>
    R.NEWR<P.CBE.CER.AC.LE.GOLD> = R.OLDR<C.CBE.CER.AC.LE.GOLD>
    R.NEWR<P.CBE.CER.AC.LE.NO.OF.CER> = R.OLDR<C.CBE.CER.AC.LE.NO.OF.CER>
    R.NEWR<P.CBE.SAV.AC.LE> = R.OLDR<C.CBE.SAV.AC.LE>
    R.NEWR<P.CBE.SAV.AC.LE.DR> = R.OLDR<C.CBE.SAV.AC.LE.DR>
    R.NEWR<P.CBE.SAV.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.SAV.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.BLOCK.AC.LE> = R.OLDR<C.CBE.BLOCK.AC.LE>
    R.NEWR<P.CBE.BLOCK.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.BLOCK.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LC.LE> = R.OLDR<C.CBE.MARG.LC.LE>
    R.NEWR<P.CBE.MARG.LC.LE.NO.OF.AC> = R.OLDR<C.CBE.MARG.LC.LE.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LG.LE> = R.OLDR<C.CBE.MARG.LG.LE>
    R.NEWR<P.CBE.MARG.LG.LE.NO.OF.AC> = R.OLDR<C.CBE.MARG.LG.LE.NO.OF.AC>
    R.NEWR<P.CBE.FACLTY.LE> = R.OLDR<C.CBE.FACLTY.LE>
    R.NEWR<P.CBE.FACLTY.LE.CR> = R.OLDR<C.CBE.FACLTY.LE.CR>
    R.NEWR<P.CBE.FACLTY.LE.NO.OF.AC> = R.OLDR<C.CBE.FACLTY.LE.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.S> = R.OLDR<C.CBE.LOANS.LE.S>
    R.NEWR<P.CBE.LOANS.LE.S.CR> = R.OLDR<C.CBE.LOANS.LE.S.CR>
    R.NEWR<P.CBE.LOANS.LE.S.NO.OF.AC> = R.OLDR<C.CBE.LOANS.LE.S.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.M> = R.OLDR<C.CBE.LOANS.LE.M>
    R.NEWR<P.CBE.LOANS.LE.M.CR> = R.OLDR<C.CBE.LOANS.LE.M.CR>
    R.NEWR<P.CBE.LOANS.LE.M.NO.OF.AC> = R.OLDR<C.CBE.LOANS.LE.M.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.L> = R.OLDR<C.CBE.LOANS.LE.L>
    R.NEWR<P.CBE.LOANS.LE.L.CR> = R.OLDR<C.CBE.LOANS.LE.L.CR>
    R.NEWR<P.CBE.LOANS.LE.L.NO.OF.AC> = R.OLDR<C.CBE.LOANS.LE.L.NO.OF.AC>
    R.NEWR<P.CBE.COMRCL.PAPER.LE> = R.OLDR<C.CBE.COMRCL.PAPER.LE>
    R.NEWR<P.CBE.COMRCL.PAPER.LE.NO> = R.OLDR<C.CBE.COMRCL.PAPER.LE.NO>
    R.NEWR<P.CBE.CUR.AC.EQ> = R.OLDR<C.CBE.CUR.AC.EQ>
    R.NEWR<P.CBE.CUR.AC.EQ.DR> = R.OLDR<C.CBE.CUR.AC.EQ.DR>
    R.NEWR<P.CBE.CUR.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.CUR.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.DEPOST.AC.EQ.1Y> = R.OLDR<C.CBE.DEPOST.AC.EQ.1Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.2Y> = R.OLDR<C.CBE.DEPOST.AC.EQ.2Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.3Y> = R.OLDR<C.CBE.DEPOST.AC.EQ.3Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.MOR3> = R.OLDR<C.CBE.DEPOST.AC.EQ.MOR3>
    R.NEWR<P.CBE.DEPOST.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.DEPOST.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.CER.AC.EQ.3Y> = R.OLDR<C.CBE.CER.AC.EQ.3Y>
    R.NEWR<P.CBE.CER.AC.EQ.5Y> = R.OLDR<C.CBE.CER.AC.EQ.5Y>
    R.NEWR<P.CBE.CER.AC.EQ.GOLD> = R.OLDR<C.CBE.CER.AC.EQ.GOLD>
    R.NEWR<P.CBE.CER.AC.EQ.NO.OF.CER> = R.OLDR<C.CBE.CER.AC.EQ.NO.OF.CER>
    R.NEWR<P.CBE.SAV.AC.EQ> = R.OLDR<C.CBE.SAV.AC.EQ>
    R.NEWR<P.CBE.SAV.AC.EQ.DR> = R.OLDR<C.CBE.SAV.AC.EQ.DR>
    R.NEWR<P.CBE.SAV.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.SAV.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.BLOCK.AC.EQ> = R.OLDR<C.CBE.BLOCK.AC.EQ>
    R.NEWR<P.CBE.BLOCK.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.BLOCK.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LC.EQ> = R.OLDR<C.CBE.MARG.LC.EQ>
    R.NEWR<P.CBE.MARG.LC.EQ.NO.OF.AC> = R.OLDR<C.CBE.MARG.LC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LG.EQ> = R.OLDR<C.CBE.MARG.LG.EQ>
    R.NEWR<P.CBE.MARG.LG.EQ.NO.OF.AC> = R.OLDR<C.CBE.MARG.LG.EQ.NO.OF.AC>
    R.NEWR<P.CBE.FACLTY.EQ> = R.OLDR<C.CBE.FACLTY.EQ>
    R.NEWR<P.CBE.FACLTY.EQ.CR> = R.OLDR<C.CBE.FACLTY.EQ.CR>
    R.NEWR<P.CBE.FACLTY.EQ.NO.OF.AC> = R.OLDR<C.CBE.FACLTY.EQ.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.S> = R.OLDR<C.CBE.LOANS.EQ.S>
    R.NEWR<P.CBE.LOANS.EQ.S.CR> = R.OLDR<C.CBE.LOANS.EQ.S.CR>
    R.NEWR<P.CBE.LOANS.EQ.S.NO.OF.AC> = R.OLDR<C.CBE.LOANS.EQ.S.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.M> = R.OLDR<C.CBE.LOANS.EQ.M>
    R.NEWR<P.CBE.LOANS.EQ.M.CR> = R.OLDR<C.CBE.LOANS.EQ.M.CR>
    R.NEWR<P.CBE.LOANS.EQ.M.NO.OF.AC> = R.OLDR<C.CBE.LOANS.EQ.M.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.L> = R.OLDR<C.CBE.LOANS.EQ.L>
    R.NEWR<P.CBE.LOANS.EQ.L.CR> = R.OLDR<C.CBE.LOANS.EQ.L.CR>
    R.NEWR<P.CBE.LOANS.EQ.L.NO.OF.AC> = R.OLDR<C.CBE.LOANS.EQ.L.NO.OF.AC>
    R.NEWR<P.CBE.COMRCL.PAPER.EQ> = R.OLDR<C.CBE.COMRCL.PAPER.EQ>
    R.NEWR<P.CBE.COMRCL.PAPER.EQ.NO> = R.OLDR<C.CBE.COMRCL.PAPER.EQ.NO>
*.....................
    CALL F.WRITE(FN.NEW,WS.NEW.KEY,R.NEWR)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)
    RETURN
*-------------------������� ������ �������� ������� �������
*                   SBM.C.PRT.1900.1    BB
*                   SBM.C.PRT.1900.1A   BB
*                   SBM.C.PRT.2100.1    CC
*                   SBM.C.PRT.2100.2    CC
*                   SBM.C.PRT.2000.1    DD
*                   SBM.C.PRT.2000.2    DD
*                   SBM.C.PRT.2000.3    DD
*                   SBM.C.PRT.2000.4    DD
A.300.CRT:
*    WS.NEW.KEY = "BB":WS.OLD.ID
    WS.NEW.KEY = WS.FRST.ID:WS.OLD.ID
    R.NEWR<P.CBE.CUSTOMER.CODE> = R.OLDR<C.CBE.CUSTOMER.CODE>
    R.NEWR<P.CBE.BR> =  WS.BR
    R.NEWR<P.CBE.INDUSTRY> = R.OLDR<C.CBE.INDUSTRY>
    R.NEWR<P.CBE.SECTOR> = R.OLDR<C.CBE.SECTOR>
    R.NEWR<P.CBE.LEGAL.FORM> = R.OLDR<C.CBE.LEGAL.FORM>
    R.NEWR<P.CBE.NEW.SECTOR> = R.OLDR<C.CBE.NEW.SECTOR>
    R.NEWR<P.CBE.NEW.INDUSTRY> = R.OLDR<C.CBE.NEW.INDUSTRY>

    R.NEWR<P.CBE.CUR.AC.LE>  = R.OLDR<C.CBE.CUR.AC.LE>
    R.NEWR<P.CBE.CUR.AC.LE.DR> = R.OLDR<C.CBE.CUR.AC.LE.DR>
    R.NEWR<P.CBE.CUR.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.CUR.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.DEPOST.AC.LE.1Y> = R.OLDR<C.CBE.DEPOST.AC.LE.1Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.2Y> = R.OLDR<C.CBE.DEPOST.AC.LE.2Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.3Y> = R.OLDR<C.CBE.DEPOST.AC.LE.3Y>
    R.NEWR<P.CBE.DEPOST.AC.LE.MOR3> = R.OLDR<C.CBE.DEPOST.AC.LE.MOR3>
    R.NEWR<P.CBE.DEPOST.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.DEPOST.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.CER.AC.LE.3Y> = R.OLDR<C.CBE.CER.AC.LE.3Y>
    R.NEWR<P.CBE.CER.AC.LE.5Y> = R.OLDR<C.CBE.CER.AC.LE.5Y>
    R.NEWR<P.CBE.CER.AC.LE.GOLD> = R.OLDR<C.CBE.CER.AC.LE.GOLD>
    R.NEWR<P.CBE.CER.AC.LE.NO.OF.CER> = R.OLDR<C.CBE.CER.AC.LE.NO.OF.CER>
    R.NEWR<P.CBE.SAV.AC.LE> = R.OLDR<C.CBE.SAV.AC.LE>
    R.NEWR<P.CBE.SAV.AC.LE.DR> = R.OLDR<C.CBE.SAV.AC.LE.DR>
    R.NEWR<P.CBE.SAV.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.SAV.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.BLOCK.AC.LE> = R.OLDR<C.CBE.BLOCK.AC.LE>
    R.NEWR<P.CBE.BLOCK.AC.LE.NO.OF.AC> = R.OLDR<C.CBE.BLOCK.AC.LE.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LC.LE> = R.OLDR<C.CBE.MARG.LC.LE>
    R.NEWR<P.CBE.MARG.LC.LE.NO.OF.AC> = R.OLDR<C.CBE.MARG.LC.LE.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LG.LE> = R.OLDR<C.CBE.MARG.LG.LE>
    R.NEWR<P.CBE.MARG.LG.LE.NO.OF.AC> = R.OLDR<C.CBE.MARG.LG.LE.NO.OF.AC>
    R.NEWR<P.CBE.FACLTY.LE> = R.OLDR<C.CBE.FACLTY.LE>
    R.NEWR<P.CBE.FACLTY.LE.CR> = R.OLDR<C.CBE.FACLTY.LE.CR>
    R.NEWR<P.CBE.FACLTY.LE.NO.OF.AC> = R.OLDR<C.CBE.FACLTY.LE.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.S> = R.OLDR<C.CBE.LOANS.LE.S>
    R.NEWR<P.CBE.LOANS.LE.S.CR> = R.OLDR<C.CBE.LOANS.LE.S.CR>
    R.NEWR<P.CBE.LOANS.LE.S.NO.OF.AC> = R.OLDR<C.CBE.LOANS.LE.S.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.M> = R.OLDR<C.CBE.LOANS.LE.M>
    R.NEWR<P.CBE.LOANS.LE.M.CR> = R.OLDR<C.CBE.LOANS.LE.M.CR>
    R.NEWR<P.CBE.LOANS.LE.M.NO.OF.AC> = R.OLDR<C.CBE.LOANS.LE.M.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.LE.L> = R.OLDR<C.CBE.LOANS.LE.L>
    R.NEWR<P.CBE.LOANS.LE.L.CR> = R.OLDR<C.CBE.LOANS.LE.L.CR>
    R.NEWR<P.CBE.LOANS.LE.L.NO.OF.AC> = R.OLDR<C.CBE.LOANS.LE.L.NO.OF.AC>
    R.NEWR<P.CBE.COMRCL.PAPER.LE> = R.OLDR<C.CBE.COMRCL.PAPER.LE>
    R.NEWR<P.CBE.COMRCL.PAPER.LE.NO> = R.OLDR<C.CBE.COMRCL.PAPER.LE.NO>
    R.NEWR<P.CBE.CUR.AC.EQ> = R.OLDR<C.CBE.CUR.AC.EQ>
    R.NEWR<P.CBE.CUR.AC.EQ.DR> = R.OLDR<C.CBE.CUR.AC.EQ.DR>
    R.NEWR<P.CBE.CUR.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.CUR.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.DEPOST.AC.EQ.1Y> = R.OLDR<C.CBE.DEPOST.AC.EQ.1Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.2Y> = R.OLDR<C.CBE.DEPOST.AC.EQ.2Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.3Y> = R.OLDR<C.CBE.DEPOST.AC.EQ.3Y>
    R.NEWR<P.CBE.DEPOST.AC.EQ.MOR3> = R.OLDR<C.CBE.DEPOST.AC.EQ.MOR3>
    R.NEWR<P.CBE.DEPOST.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.DEPOST.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.CER.AC.EQ.3Y> = R.OLDR<C.CBE.CER.AC.EQ.3Y>
    R.NEWR<P.CBE.CER.AC.EQ.5Y> = R.OLDR<C.CBE.CER.AC.EQ.5Y>
    R.NEWR<P.CBE.CER.AC.EQ.GOLD> = R.OLDR<C.CBE.CER.AC.EQ.GOLD>
    R.NEWR<P.CBE.CER.AC.EQ.NO.OF.CER> = R.OLDR<C.CBE.CER.AC.EQ.NO.OF.CER>
    R.NEWR<P.CBE.SAV.AC.EQ> = R.OLDR<C.CBE.SAV.AC.EQ>
    R.NEWR<P.CBE.SAV.AC.EQ.DR> = R.OLDR<C.CBE.SAV.AC.EQ.DR>
    R.NEWR<P.CBE.SAV.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.SAV.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.BLOCK.AC.EQ> = R.OLDR<C.CBE.BLOCK.AC.EQ>
    R.NEWR<P.CBE.BLOCK.AC.EQ.NO.OF.AC> = R.OLDR<C.CBE.BLOCK.AC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LC.EQ> = R.OLDR<C.CBE.MARG.LC.EQ>
    R.NEWR<P.CBE.MARG.LC.EQ.NO.OF.AC> = R.OLDR<C.CBE.MARG.LC.EQ.NO.OF.AC>
    R.NEWR<P.CBE.MARG.LG.EQ> = R.OLDR<C.CBE.MARG.LG.EQ>
    R.NEWR<P.CBE.MARG.LG.EQ.NO.OF.AC> = R.OLDR<C.CBE.MARG.LG.EQ.NO.OF.AC>
    R.NEWR<P.CBE.FACLTY.EQ> = R.OLDR<C.CBE.FACLTY.EQ>
    R.NEWR<P.CBE.FACLTY.EQ.CR> = R.OLDR<C.CBE.FACLTY.EQ.CR>
    R.NEWR<P.CBE.FACLTY.EQ.NO.OF.AC> = R.OLDR<C.CBE.FACLTY.EQ.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.S> = R.OLDR<C.CBE.LOANS.EQ.S>
    R.NEWR<P.CBE.LOANS.EQ.S.CR> = R.OLDR<C.CBE.LOANS.EQ.S.CR>
    R.NEWR<P.CBE.LOANS.EQ.S.NO.OF.AC> = R.OLDR<C.CBE.LOANS.EQ.S.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.M> = R.OLDR<C.CBE.LOANS.EQ.M>
    R.NEWR<P.CBE.LOANS.EQ.M.CR> = R.OLDR<C.CBE.LOANS.EQ.M.CR>
    R.NEWR<P.CBE.LOANS.EQ.M.NO.OF.AC> = R.OLDR<C.CBE.LOANS.EQ.M.NO.OF.AC>
    R.NEWR<P.CBE.LOANS.EQ.L> = R.OLDR<C.CBE.LOANS.EQ.L>
    R.NEWR<P.CBE.LOANS.EQ.L.CR> = R.OLDR<C.CBE.LOANS.EQ.L.CR>
    R.NEWR<P.CBE.LOANS.EQ.L.NO.OF.AC> = R.OLDR<C.CBE.LOANS.EQ.L.NO.OF.AC>
    R.NEWR<P.CBE.COMRCL.PAPER.EQ> = R.OLDR<C.CBE.COMRCL.PAPER.EQ>
    R.NEWR<P.CBE.COMRCL.PAPER.EQ.NO> = R.OLDR<C.CBE.COMRCL.PAPER.EQ.NO>
*.....................
    CALL F.WRITE(FN.NEW,WS.NEW.KEY,R.NEWR)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)

    RETURN
END
