* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.MONTHLY.BATCH.2

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
****************************
    EXECUTE "OFSADMINLOGIN"
****************************

** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

**  CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.HNDOF.DEL")

** ��� ������ ������ ������� ������� ������
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.EMP.INT.2PRCNT.007")

** ����� ������ ���� ������
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.RUN.CUSTOMER.STMT")

** ����� ���������
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CREDIT.CBE.BAK2")

** ����� ����� ������� ������� ������
**  CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.MONEYBOX.YOUNG.M")
**
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"CREDIT.CBE.FILL")
**
****** ISCORE CONS
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"ISCO.FILL.TABLES")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"ISCO.FIX.CLOSE.ACCT")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"ISCO.CONS.TXT")
****** ISCORE SME
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"ISCO.SME.FILL.TABLES")

************* MAHMOUD : CBE 6000 , 6001 FILES ****************
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBR.CBE.6000.01")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBR.CBE.6001.01")

************* NOHA PROGRAMS *****************
*--- NOHA Risk Rating
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBR.INS.RISK.RATING")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"RISK.RATE.TXT")

*--- NOHA Credit Facilities
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.CREDIT.CBE.CURR")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"CBE.FAC.ALL.COMP.TXT")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"CBE.FAC.EGP.TXT")
******************MOKHSAS PROGRAMS*****************
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.MOKHSAS.BATCH")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"CONV.RISK.N.ALL")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"CONV.RISK.N.ALL.NEW")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"CONV.RISK.N.ALL.TST")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"CONV.RISK.N.ALL.TST.NEW")
*-----------------------------------------------------------
    RETURN
END
