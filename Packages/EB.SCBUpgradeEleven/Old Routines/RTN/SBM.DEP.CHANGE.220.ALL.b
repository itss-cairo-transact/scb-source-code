* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.DEP.CHANGE.220.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.DEP.CHANGE.220.ALL'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]

    AMT.LCY.CUR   = 0 ;  AMT.FCY.CUR   = 0
    AMT.LCY.DEP   = 0 ;  AMT.FCY.DEP   = 0
    AMT.LCY.BLOCK = 0 ;  AMT.FCY.BLOCK = 0

    AMT.LCY.CUR.TOT   = 0 ;  AMT.FCY.CUR.TOT   = 0
    AMT.LCY.DEP.TOT   = 0 ;  AMT.FCY.DEP.TOT   = 0
    AMT.LCY.BLOCK.TOT = 0 ;  AMT.FCY.BLOCK.TOT = 0

    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
***    T.SEL = "SELECT ":FN.CBE:" WITH RESERVED10 EQ 220 AND CBE.BR EQ ":COMP.BR:" BY CBE.CUSTOMER.CODE"
    T.SEL = "SELECT ":FN.CBE:" WITH RESERVED10 EQ 220 BY CBE.CUSTOMER.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)

************** CURRENT MONTH *********************************

            AMT.LCY.CUR   = R.CBE<ST.CBE.CUR.AC.LE> + R.CBE<ST.CBE.FACLTY.LE.CR> + R.CBE<ST.CBE.LOANS.LE.S.CR> + R.CBE<ST.CBE.LOANS.LE.M.CR> + R.CBE<ST.CBE.LOANS.LE.L.CR>
            AMT.LCY.CUR   = DROUND(AMT.LCY.CUR,'0')

            AMT.LCY.DEP   = R.CBE<ST.CBE.DEPOST.AC.LE.1Y> + R.CBE<ST.CBE.DEPOST.AC.LE.2Y> + R.CBE<ST.CBE.DEPOST.AC.LE.3Y> + R.CBE<ST.CBE.DEPOST.AC.LE.MOR3> + R.CBE<ST.CBE.CER.AC.LE.3Y> + R.CBE<ST.CBE.CER.AC.LE.5Y> + R.CBE<ST.CBE.CER.AC.LE.GOLD> + R.CBE<ST.CBE.SAV.AC.LE>
            AMT.LCY.DEP   = DROUND(AMT.LCY.DEP,'0')

            AMT.LCY.BLOCK = R.CBE<ST.CBE.BLOCK.AC.LE> + R.CBE<ST.CBE.MARG.LC.LE> + R.CBE<ST.CBE.MARG.LG.LE>
            AMT.LCY.BLOCK = DROUND(AMT.LCY.BLOCK,'0')

            AMT.FCY.CUR   = R.CBE<ST.CBE.CUR.AC.EQ> + R.CBE<ST.CBE.FACLTY.EQ.CR> + R.CBE<ST.CBE.LOANS.EQ.S.CR> + R.CBE<ST.CBE.LOANS.EQ.M.CR> + R.CBE<ST.CBE.LOANS.EQ.L.CR>
            AMT.FCY.CUR   = DROUND(AMT.FCY.CUR,'0')

            AMT.FCY.DEP   = R.CBE<ST.CBE.DEPOST.AC.EQ.1Y> + R.CBE<ST.CBE.DEPOST.AC.EQ.2Y> + R.CBE<ST.CBE.DEPOST.AC.EQ.3Y> + R.CBE<ST.CBE.DEPOST.AC.EQ.MOR3> + R.CBE<ST.CBE.CER.AC.EQ.3Y> + R.CBE<ST.CBE.CER.AC.EQ.5Y> + R.CBE<ST.CBE.CER.AC.EQ.GOLD> + R.CBE<ST.CBE.SAV.AC.EQ>
            AMT.FCY.DEP   = DROUND(AMT.FCY.DEP,'0')

            AMT.FCY.BLOCK = R.CBE<ST.CBE.BLOCK.AC.EQ> + R.CBE<ST.CBE.MARG.LC.EQ> + R.CBE<ST.CBE.MARG.LG.EQ>
            AMT.FCY.BLOCK = DROUND(AMT.FCY.BLOCK,'0')
*----------------------------------------------------------
********************** LAST MONTH *********************************

            AMT.LCY.CUR.O   = R.CBE<ST.CBE.CUR.AC.LEO> + R.CBE<ST.CBE.FACLTY.LE.CRO> + R.CBE<ST.CBE.LOANS.LE.S.CRO> + R.CBE<ST.CBE.LOANS.LE.M.CRO> + R.CBE<ST.CBE.LOANS.LE.L.CRO>
            AMT.LCY.CUR.O   = DROUND(AMT.LCY.CUR.O,'0')

            AMT.LCY.DEP.O   = R.CBE<ST.CBE.DEPOST.AC.LE.1YO> + R.CBE<ST.CBE.DEPOST.AC.LE.2YO> + R.CBE<ST.CBE.DEPOST.AC.LE.3YO> + R.CBE<ST.CBE.DEPOST.AC.LE.MOR3O> + R.CBE<ST.CBE.CER.AC.LE.3YO> + R.CBE<ST.CBE.CER.AC.LE.5YO> + R.CBE<ST.CBE.CER.AC.LE.GOLDO> + R.CBE<ST.CBE.SAV.AC.LEO>
            AMT.LCY.DEP.O   = DROUND(AMT.LCY.DEP.O,'0')

            AMT.LCY.BLOCK.O = R.CBE<ST.CBE.BLOCK.AC.LEO> + R.CBE<ST.CBE.MARG.LC.LEO> + R.CBE<ST.CBE.MARG.LG.LEO>
            AMT.LCY.BLOCK.O = DROUND(AMT.LCY.BLOCK.O,'0')

            AMT.FCY.CUR.O   = R.CBE<ST.CBE.CUR.AC.EQO> + R.CBE<ST.CBE.FACLTY.EQ.CRO> + R.CBE<ST.CBE.LOANS.EQ.S.CRO> + R.CBE<ST.CBE.LOANS.EQ.M.CRO> + R.CBE<ST.CBE.LOANS.EQ.L.CRO>
            AMT.FCY.CUR.O   = DROUND(AMT.FCY.CUR.O,'0')

            AMT.FCY.DEP.O   = R.CBE<ST.CBE.DEPOST.AC.EQ.1YO> + R.CBE<ST.CBE.DEPOST.AC.EQ.2YO> + R.CBE<ST.CBE.DEPOST.AC.EQ.3YO> + R.CBE<ST.CBE.DEPOST.AC.EQ.MOR3O> + R.CBE<ST.CBE.CER.AC.EQ.3YO> + R.CBE<ST.CBE.CER.AC.EQ.5YO> + R.CBE<ST.CBE.CER.AC.EQ.GOLDO> + R.CBE<ST.CBE.SAV.AC.EQO>
            AMT.FCY.DEP.O   = DROUND(AMT.FCY.DEP.O,'0')

            AMT.FCY.BLOCK.O = R.CBE<ST.CBE.BLOCK.AC.EQO> + R.CBE<ST.CBE.MARG.LC.EQO> + R.CBE<ST.CBE.MARG.LG.EQO>
            AMT.FCY.BLOCK.O = DROUND(AMT.FCY.BLOCK.O,'0')
*-----------------------------------------------------------
***************** THE CHANGE *******************************

            AMT.LCY.CUR.C   = AMT.LCY.CUR - AMT.LCY.CUR.O
            AMT.LCY.DEP.C   = AMT.LCY.DEP - AMT.LCY.DEP.O
            AMT.LCY.BLOCK.C = AMT.LCY.BLOCK - AMT.LCY.BLOCK.O

            AMT.FCY.CUR.C   = AMT.FCY.CUR - AMT.FCY.CUR.O
            AMT.FCY.DEP.C   = AMT.FCY.DEP - AMT.FCY.DEP.O
            AMT.FCY.BLOCK.C = AMT.FCY.BLOCK - AMT.FCY.BLOCK.O

*-----------------------------------------------------------
            CUS.ID  = R.CBE<ST.CBE.CUSTOMER.CODE>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
*--------------------------------------------------
            IF AMT.LCY.CUR.C LT 0 THEN
                AMT.CURL = AMT.LCY.CUR.C * -1
            END ELSE
                AMT.CURL = AMT.LCY.CUR.C
            END

            IF AMT.LCY.DEP.C LT 0 THEN
                AMT.DEPL = AMT.LCY.DEP.C * -1
            END ELSE
                AMT.DEPL = AMT.LCY.DEP.C
            END

            IF AMT.LCY.BLOCK.C LT 0 THEN
                AMT.BLOCKL = AMT.LCY.BLOCK.C * -1
            END ELSE
                AMT.BLOCKL = AMT.LCY.BLOCK.C
            END

            IF AMT.FCY.CUR.C LT 0 THEN
                AMT.CURF = AMT.FCY.CUR.C * -1
            END ELSE
                AMT.CURF = AMT.FCY.CUR.C
            END

            IF AMT.FCY.DEP.C LT 0 THEN
                AMT.DEPF = AMT.FCY.DEP.C * -1
            END ELSE
                AMT.DEPF = AMT.FCY.DEP.C
            END

            IF AMT.FCY.BLOCK.C LT 0 THEN
                AMT.BLOCKF = AMT.FCY.BLOCK.C * -1
            END ELSE
                AMT.BLOCKF = AMT.FCY.BLOCK.C
            END
*--------------------------------------------------
            IF AMT.CURL GE 1000000 THEN
                GOSUB PRINT.DET
                GO TO EXIT.LOOP
            END

            IF AMT.DEPL GE 1000000 THEN
                GOSUB PRINT.DET
                GO TO EXIT.LOOP
            END

            IF AMT.BLOCKL GE 1000000 THEN
                GOSUB PRINT.DET
                GO TO EXIT.LOOP
            END

            IF AMT.CURF GE 1000000 THEN
                GOSUB PRINT.DET
                GO TO EXIT.LOOP
            END

            IF AMT.DEPF GE 1000000 THEN
                GOSUB PRINT.DET
                GO TO EXIT.LOOP
            END

            IF AMT.BLOCKF GE 1000000 THEN
                GOSUB PRINT.DET
                GO TO EXIT.LOOP
            END
EXIT.LOOP:
        NEXT I
    END
*---------------------------------------------------
    XX2= SPACE(120)
    PRINT STR('=',130)
    XX2<1,1>[1,35]    = "�����������"
    XX2<1,1>[55,20]   = AMT.LCY.CUR.TOT
    XX2<1,1>[65,20]   = AMT.LCY.DEP.TOT
    XX2<1,1>[75,20]   = AMT.LCY.BLOCK.TOT

    XX2<1,1>[90,20]   = AMT.FCY.CUR.TOT
    XX2<1,1>[100,20]  = AMT.FCY.DEP.TOT
    XX2<1,1>[110,20]  = AMT.FCY.BLOCK.TOT

    PRINT XX2<1,1>
    PRINT STR('=',130)

    RETURN
*==============================================================
PRINT.DET:
    XX= SPACE(120)
    XX<1,1>[1,15]   = CUS.ID
    XX<1,1>[15,35]  = CUST.NAME

    XX<1,1>[55,20]  = AMT.LCY.CUR.C
    XX<1,1>[65,20]  = AMT.LCY.DEP.C
    XX<1,1>[75,20]  = AMT.LCY.BLOCK.C
    XX<1,1>[90,20]  = AMT.FCY.CUR.C
    XX<1,1>[100,20] = AMT.FCY.DEP.C
    XX<1,1>[110,20] = AMT.FCY.BLOCK.C

    PRINT XX<1,1>
    PRINT STR('-',130)

    AMT.LCY.CUR.TOT   += AMT.LCY.CUR.C
    AMT.LCY.DEP.TOT   += AMT.LCY.DEP.C
    AMT.LCY.BLOCK.TOT += AMT.LCY.BLOCK.C

    AMT.FCY.CUR.TOT   += AMT.FCY.CUR.C
    AMT.FCY.DEP.TOT   += AMT.FCY.DEP.C
    AMT.FCY.BLOCK.TOT += AMT.FCY.BLOCK.C

    RETURN
*===============================================================
PRINT.HEAD:
*---------
*    P.COMP = "EG0010099"
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,P.COMP,BRANCH)
*    YYBRN  = BRANCH

    YYBRN  = "�� ���� �����"
    NEW.TEXT = " ���� �� ����� "


    DATY   = TODAY

    TDD = TODAY[1,6]
    TD = TDD - 2
    TDF = TDD - 1
    IF TDD[5,2] EQ '01' THEN
        TDX = TDD[1,4] - 1
        TD = TDX : '12'
        TDF = TD - 1
    END

    TD1 = TD[1,4]:'/':TD[5,2]
    TD2 = TDF[1,4]:'/':TDF[5,2]

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������� �� ����":SPACE(1):TD1:SPACE(2):TD2
    PR.HD :="'L'":SPACE(55):"������ ������� ������":NEW.TEXT
    PR.HD :="'L'":SPACE(45):STR('_',45)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(30):"������� �������":SPACE(20):"������� �������� �����"
    PR.HD :="'L'":SPACE(55):"�����":SPACE(5):"�����":SPACE(5):"�����":SPACE(10):"�����":SPACE(5):"�����":SPACE(5):"�����"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
