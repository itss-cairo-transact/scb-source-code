* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
******* NOHA *******

    SUBROUTINE SBM.UNDER.GT.21

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.ACCT.SAVE

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    TEXT = " REPORT.CREATED "   ; CALL REM

    RETURN

*==========================
INITIATE:
    FN.SA  = 'F.SCB.STMT.ACCT.SAVE' ; F.SA = '' ; R.SA = ''
    CALL OPF(FN.SA,F.SA)
    FN.CU  = 'FBNK.CUSTOMER'        ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC  = 'FBNK.ACCOUNT'         ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.ST  = 'FBNK.STMT.ACCT.CR'    ; F.ST = '' ; R.ST = ''
    CALL OPF(FN.ST,F.ST)
    FN.ACR   = 'FBNK.ACCR.ACCT.CR'  ; F.ACR = '' ; R.ACR = ''
    CALL OPF(FN.ACR,F.ACR)

    FN.AC.H  = 'FBNK.ACCOUNT$HIS'         ; F.AC.H = '' ; R.AC.H = ''
    CALL OPF(FN.AC.H,F.AC.H)

    TODAY.DATE = TODAY
    COMP       = ID.COMPANY
    IF MONTH[1,1] EQ 0 THEN
        MONTH = MONTH[2,1]
    END

*============== REPORT CREATION ===========

    REPORT.ID = 'SBM.UNDER.GT.21'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*=====================================================
PROCESS:
*    IF COMP LE 10 THEN
*        COMP = "0":COMP
*   END
**    TEXT = COMP ; CALL REM
    Y.SEL  = "SELECT F.SCB.ACCT.UNDERAGE WITH ACCT.FLAG EQ '21' AND CO.CODE EQ ":COMP
    CALL EB.READLIST(Y.SEL, KEY.LIST, "", SELECTED, ASD1)
    IF  SELECTED NE 0 THEN
**        TEXT = SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            ACCT.NO   = KEY.LIST<I>
            CALL F.READ(FN.AC,ACCT.NO, R.AC, F.AC, AC.ERR)
            CUS.NO    = R.AC<AC.CUSTOMER>
            IF CUS.NO EQ '' THEN
                ACC.NO = ACCT.NO:';1'
                CALL F.READ(FN.AC.H,ACC.NO, R.AC.H, F.AC.H, AC.ERR.H)
                CUS.NO = R.AC.H<AC.CUSTOMER>

            END
            CALL F.READ(FN.CU,CUS.NO, R.CU, F.CU, CU.ERR)
            CUS.NAME  = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
**            TEXT = ACCT.NO ; CALL REM
**            TEXT = COMP ; CALL REM
            S.SEL  = "SELECT F.SCB.STMT.ACCT.SAVE WITH PAY.FLAG EQ 'PAID' AND @ID LIKE ":ACCT.NO:"-... AND CO.CODE EQ ":COMP
            CALL EB.READLIST(S.SEL, KEY.LIST4, "", SELECTED4, ASD4)
            IF  SELECTED4 NE 0 THEN
                TOT.AMT.21 = 0
**                TEXT = SELECTED4 ; CALL REM
**                TEXT = COMP  ; CALL REM
**                TEXT = KEY.LIST4 ; CALL REM
                FOR NN = 1 TO  SELECTED4
                    CALL F.READ( FN.SA,KEY.LIST4<NN>, R.SA, F.SA, ETEXT4)
                    TOT.AMT.21    = TOT.AMT.21 + R.SA<STMT.SAVE.CR.INT.AMT>
**                    TEXT = TOT.AMT.21 ; CALL REM
                NEXT NN
                GOSUB WRITE
            END
***          GOSUB WRITE
        NEXT I
    END

    RETURN
*================ WRITE HEADER  =======================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"������� ��������� ��������-������ ������ �� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(40):"��� ������":SPACE(10):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD

    RETURN
*================== WRITE DATA =========================
WRITE:

    XX3 = SPACE(132)
    XX3 = ''
    XX3<1,1>[1,20]    = CUS.NO
    XX3<1,1>[20,50]   = CUS.NAME
    XX3<1,1>[70,20]   = ACCT.NO
    XX3<1,1>[90,23]   = TOT.AMT.21


    PRINT XX3<1,1>

    RETURN
END
