* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LG.BAL.TOT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*-------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------
*Line [ 46 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*=============================================================
INITIATE:
    REPORT.ID='SBM.LG.BAL.TOT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=============================================================
CALLDB:
*-----
    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LDD = ''
    R.LDD = ''
    CALL OPF(FN.LDD,F.LDD)

    FN.CUR = 'FBNK.CURRENCY'
    F.CUR = ''
    R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.LN = 'F.RE.STAT.REP.LINE'
    F.LN = ''
    R.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.BAL = 'FBNK.RE.STAT.LINE.BAL'
    F.BAL = ''
    R.BAL = ''
    CALL OPF(FN.BAL,F.BAL)

    KEY.LIST =""
    SELECTED =""
    ER.MSG =""

    KEY.LIST1=""
    SELECTED1=""
    ER.MSG1=""

    KEY.LIST2 =""
    SELECTED2 =""
    ER.MSG2 =""

    KEY.LIST3=""
    SELECTED3=""
    ER.MSG3=""

    LD.NAME    = ''
    LD.BAL     = 0
    LD.CUR     = ''
    LD.BALANCE = 0
    LD2.BALANCE= 0
    CUR.RATE   = 0
    AC.NAME    = ''
    AC.BAL     = 0
    AC.BALANCE = 0
    AC.CUR     = ''
    PL.BAL     = 0
    PL.BALANCE = 0
    X          = 0

    XX1 = SPACE(132) ; XX2 = SPACE(132) ; XX3 = SPACE(132) ; XX4 = SPACE(132)
    K1=0 ; K2=2 ; K3=3 ; K4=4

    TDD = TODAY
    TDM = TDD[1,6]
*=============================================================
    T.SEL  = "SELECT ":FN.LDD:" WITH AMOUNT NE '' AND AMOUNT NE 0 AND CO.CODE EQ ":COMP
    T.SEL := " AND CATEGORY EQ 21096 BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED  THEN
**************************************************************
        FOR I = 1 TO SELECTED
            K1++
            CALL F.READ(FN.LDD,KEY.LIST<I>,R.LDD,F.LDD,ER.LDD)
            LD.ID  = KEY.LIST<I>
            LD.CUR = R.LDD<LD.CURRENCY>
            LD.BAL = R.LDD<LD.AMOUNT>
            LD.MARG= R.LDD<LD.LOCAL.REF,LDLR.MARGIN.AMT>
            LD.START.DATE = R.LDD<LD.INT.START.DATE>
            LD.BR.CODE = R.LDD<LD.CO.CODE>
            LD.MARG.CUR= R.LDD<LD.LOCAL.REF,LDLR.ACC.CUR>
            PL.CA.ID = "PLCAIRO-4420-":LD.CUR:"-":TDD:"*":LD.BR.CODE
            LD.MNTH = LD.START.DATE[1,6]
            CALL DBR('RE.STAT.LINE.BAL':@FM:RE.SLB.CLOSING.BAL.LCL,PL.CA.ID,PL.BAL)
            IF LD.MNTH EQ TDM THEN
                X++
            END
            CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,LD.CUR,CUR.RATE)
            CURR.R = CUR.RATE<1,1>
            CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,LD.MARG.CUR,CUR.RATE1)
            CURR.R1 = CUR.RATE1<1,1>
            IF CURR.R EQ '' THEN
                CURR.R = 1
            END
            IF CURR.R1 EQ '' THEN
                CURR.R1 = 1
            END
            LD.BALANCE += LD.BAL * CURR.R
            AC.BALANCE += LD.MARG* CURR.R1
            PL.BALANCE += PL.BAL
        NEXT I
    END
    LD.NAME  = '������ ���� ������ ������ ������� ��������'
    LD2.NAME = '������ ��� ������ ������ ������� ���� �����'
    AC.NAME  = '������ ���� ��������� �������� �� ������ ������'
    PL.NAME = '���� �������� �������'
    GOSUB PRINTLD
    GOSUB PRINTAC
    GOSUB PRINTLD2
    GOSUB PRINTLD3
*=============================================================
*    PRINT STR('-',120)
*    GOSUB PRINT.TOT
    PRINT STR('=',120)
*=============================================================
*    END ELSE
*        TEXT = "NO RECORD EXIST" ; CALL REM
*    END
    RETURN
*=============================================================
**************************
PRINTLD:
    LD.BALANCE = DROUND(LD.BALANCE,2)
    XX1<1,K1>[1,50]   = LD.NAME
    XX1<1,K1>[55,15]  = LD.BALANCE
    PRINT XX1<1,K1>
    PRINT STR('-',120)
    RETURN
**************************
PRINTAC:
    AC.BALANCE = DROUND(AC.BALANCE,2)
    XX2<1,K2>[1,50]   = AC.NAME
    XX2<1,K2>[55,15]  = AC.BALANCE
    PRINT XX2<1,K2>
    PRINT STR('-',120)
    RETURN
**************************
PRINTLD2:
    XX3<1,K3>[1,50]   = LD2.NAME
    XX3<1,K3>[55,15]  = X
    PRINT XX3<1,K3>
    PRINT STR('-',120)
    RETURN
**************************
PRINTLD3:
    PL.BALANCE = DROUND(PL.BALANCE,2)
    XX4<1,K4>[1,50]   = PL.NAME
    XX4<1,K4>[55,15]  = PL.BALANCE
    PRINT XX4<1,K4>
    PRINT STR('-',120)
    RETURN
***********************************
*=============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

    YYBRN   = BRANCH
    DATY    = TODAY
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(50):"����� ������ ������ �������"
    PR.HD :="'L'":SPACE(48):"�� ����� : ":T.DAY
    PR.HD :="'L'":SPACE(45):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',120)
*    PRINT
    HEADING PR.HD
    RETURN
*=============================================================
END
