* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
    PROGRAM  SBM.STMT.PRT.CHK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HNDOF.PRNT.CHK

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    FN.HCHK      = "F.SCB.HNDOF.PRNT.CHK"          ; F.HCHK     = ""  ; R.HCHK     = ""
    CALL OPF (FN.HCHK,F.HCHK)

    KK = 0
    FOR IBR = 100 TO 190
        WS.BR.NO = IBR[2]
        T.SEL ="GET-LIST STMT.":WS.BR.NO
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        *PRINT 'SEL = ':SELECTED
        IF SELECTED THEN
            LOOP
                REMOVE WS.LINE FROM KEY.LIST SETTING POS.ID
            WHILE WS.LINE:POS.ID
                KK++
                WS.AC.ID = WS.LINE[48,16]
                CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,E4)
                IF NOT(E4) THEN
                    CALL F.READ(FN.HCHK,WS.AC.ID,R.HCHK,F.HCHK,ER.HCHK)
                    R.HCHK<HPC.STMT.PRNT>     = 'YES'
                    CALL  F.WRITE(FN.HCHK,WS.AC.ID,R.HCHK)
                    CALL  JOURNAL.UPDATE(WS.AC.ID)
                END
            REPEAT
        END
    NEXT IBR
END
