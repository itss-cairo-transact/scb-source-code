* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LOAN.CHANGE.200.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LOAN.CHANGE.200.ALL'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
    AMT.LCY = 0 ; AMT.FCY = 0 ; SECTOR.ID1 = '' ; SECTOR.ID = ''
    AMT.LCY.TOT = 0 ; AMT.FCY.TOT = 0
    AMT.LCY.TOT1 = 0 ; AMT.FCY.TOT1 = 0
    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
****    T.SEL = "SELECT ":FN.CBE:" WITH (RESERVED10 GE 210 AND RESERVED10 LE 260) AND CBE.BR EQ ":COMP.BR:" BY RESERVED10"
    T.SEL = "SELECT ":FN.CBE:" WITH (RESERVED10 GE 210 AND RESERVED10 LE 260) BY RESERVED10"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            SECTOR.ID = R.CBE<ST.RESERVED10>
            IF I EQ 1 THEN SECTOR.ID1 = SECTOR.ID
            IF SECTOR.ID NE SECTOR.ID1 THEN
                IF AMT.TOTAL NE 0 THEN
                    PRINT STR('=',130)
                    XX3 = SPACE(120)
                    XX3<1,1>[1,35]  = "������ : ":DESC
                    XX3<1,1>[55,20] = AMT.LCY.TOT
                    XX3<1,1>[70,20] = AMT.FCY.TOT
                    PRINT XX3<1,1>
                    PRINT STR('=',130)
                    AMT.LCY.TOT = 0 ; AMT.FCY.TOT = 0
                END
            END

            IF SECTOR.ID EQ 210 THEN
                DESC = "����� ������� ����� ���������"
            END
            IF SECTOR.ID EQ 220 THEN
                DESC = "����� ������� ������"
            END
            IF SECTOR.ID EQ 230 THEN
                DESC = "����� �������"
            END
            IF SECTOR.ID EQ 240 THEN
                DESC = "������ ���������"
            END
            IF SECTOR.ID EQ 250 THEN
                DESC = "����� �����"
            END
            IF SECTOR.ID EQ 260 THEN
                DESC = "����� ���� ����� ������� �����"
            END

            AMT.LCY = R.CBE<ST.CBE.FACLTY.LE> + R.CBE<ST.CBE.CUR.AC.LE.DR>
            AMT.LCY = AMT.LCY * -1
            AMT.LCY = DROUND(AMT.LCY,'0')

            AMT.FCY = R.CBE<ST.CBE.FACLTY.EQ> + R.CBE<ST.CBE.CUR.AC.EQ.DR>
            AMT.FCY = AMT.FCY * -1
            AMT.FCY = DROUND(AMT.FCY,'0')

            AMT.LCY.O = R.CBE<ST.CBE.FACLTY.LEO> + R.CBE<ST.CBE.CUR.AC.LE.DRO>
            AMT.LCY.O = AMT.LCY.O * -1
            AMT.LCY.O = DROUND(AMT.LCY.O,'0')

            AMT.FCY.O = R.CBE<ST.CBE.FACLTY.EQO> + R.CBE<ST.CBE.CUR.AC.EQ.DRO>
            AMT.FCY.O = AMT.FCY.O * -1
            AMT.FCY.O = DROUND(AMT.FCY.O,'0')

            AMT.LCY.C = AMT.LCY - AMT.LCY.O
            AMT.FCY.C = AMT.FCY - AMT.FCY.O

            CUS.ID  = R.CBE<ST.CBE.CUSTOMER.CODE>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

            AMT.TOTAL = AMT.LCY.TOT + AMT.FCY.TOT

            IF AMT.LCY.C LT 0 THEN
                AMTC = AMT.LCY.C * -1
            END ELSE
                AMTC = AMT.LCY.C
            END
            IF AMT.FCY.C LT 0 THEN
                AMTF = AMT.FCY.C * -1
            END ELSE
                AMTF = AMT.FCY.C
            END

            IF AMTC GE 1000000 OR AMTF GE 1000000 THEN
                XX = SPACE(120)
                XX<1,1>[1,15]  = CUS.ID
                XX<1,1>[15,35] = CUST.NAME
                XX<1,1>[55,20] = AMT.LCY.C
                XX<1,1>[70,20] = AMT.FCY.C
                XX<1,1>[95,20] = DESC
                PRINT XX<1,1>
                PRINT STR('-',130)

                AMT.LCY.TOT += AMT.LCY.C
                AMT.FCY.TOT += AMT.FCY.C

                AMT.LCY.TOT1 += AMT.LCY.C
                AMT.FCY.TOT1 += AMT.FCY.C

            END

            SECTOR.ID1 = SECTOR.ID

        NEXT I
        IF I EQ SELECTED THEN
            IF AMT.TOTAL NE 0 THEN
                PRINT STR('=',130)
                XX3 = SPACE(120)
                XX3<1,1>[1,35]  = "������ : ":DESC
                XX3<1,1>[55,20] = AMT.LCY.TOT
                XX3<1,1>[70,20] = AMT.FCY.TOT
                PRINT XX3<1,1>
                PRINT STR('=',130)
            END
        END
        PRINT STR('=',130)
        XX2 = SPACE(120)
        XX2<1,1>[1,35]  = "�����������"
        XX2<1,1>[55,20] = AMT.LCY.TOT1
        XX2<1,1>[70,20] = AMT.FCY.TOT1
        PRINT XX2<1,1>
        PRINT STR('=',130)


    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
*    P.COMP = 'EG0010099'
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,P.COMP,BRANCH)
*    YYBRN  = BRANCH
    YYBRN  = "�� ���� �����"
    NEW.TEXT = " ���� �� ����� "

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TDD = TODAY[1,6]
    TD = TDD - 2
    TDF = TDD - 1
    IF TDD[5,2] EQ '01' THEN
        TDX = TDD[1,4] - 1
        TD = TDX : '12'
        TDF = TD - 1
    END

    TD1 = TD[1,4]:'/':TD[5,2]
    TD2 = TDF[1,4]:'/':TDF[5,2]



    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������ �� ���� ":SPACE(1):TD1:SPACE(2):TD2
    PR.HD :="'L'":SPACE(50):"����� ������� �����":NEW.TEXT
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(20):"������� �������":SPACE(5):"������� ��������":SPACE(10):"������"
    PR.HD :="'L'":SPACE(70):"�����"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
