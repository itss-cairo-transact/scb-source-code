* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM     SBM.C.PRT.2000.2.CSV

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

    OPENSEQ "&SAVEDLISTS&" , "SBM.C.PRT.2000.2.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBM.C.PRT.2000.2.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBM.C.PRT.2000.2.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBM.C.PRT.2000.2.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBM.C.PRT.2000.2.CSV File IN &SAVEDLISTS&'
        END
    END

***"����� ��� "
****  2000
*------------------------------------------
    FN.CUS = "F.CUSTOMER"
    F.CUS  = ""

    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""


    FN.COMP = "F.COMPANY"
    F.COMP = ""

*------------------------------------

    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CUS,F.CUS)

*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
*    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    FLAG.PRT.OK = 0
    WS.TO = 0
    WS.BR = ""
    WS.BRX = ""
    WS.SECTOR.NAME = ""
    WS.CUS.KEY = ""
    MSG.CUS = ""
    WS.CUS.NAME = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.PAG.COUNT = 0
    WS.LIN.COUNT = 0

    WS.1.LE = "0"

    WS.1.EQV = "0"

    WS.1.LE.LIN = 0
    WS.1.EQV.LIN = 0

    WS.1.LE.TOT  = 0
    WS.1.EQV.TOT  = 0

    SUB.A = 0
    WS.LINE.COUNT = 55
    WS.TO.COMPAR = 0
    WS.NAME = ""
    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.INDST.COMP = 0
    WS.HD.T  = "����� ����� ������� � ��������� ���������� � ������� �������� "

    WS.HD.TA = " ����� ��� 2000 "

    WS.HD.T2 = "�������� ������ ���� ������� �����"
    WS.HD.T2A = "����    "
    WS.HD.T3  = "������ ���� ����"

*------------------------------
*-----------------------------------
    WS.HD.4A = "���� �����"
    WS.HD.4B = "���� ������"
    WS.HD.4C = "��������"
*------------------------------
    WS.HD.2  = "��������� ����������"

    WS.HD.3 = "� ������� �������� ��������"


    WS.PRG.1 = "SBM.C.PRT.2000.2"
*------------------------------------------------
    DIM ARRAY1(13,5)

    ARRAY1(1,1) = "������ ������� ����� � ����� � ������ "
    ARRAY1(1,2) = "100"
    ARRAY1(1,3) = "101"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"

    ARRAY1(2,1) = "������ ������� �������                "
    ARRAY1(2,2) = "200"
    ARRAY1(2,3) = "201"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"


    ARRAY1(3,1) = "������ ������� �������� ��������      "
    ARRAY1(3,2) = "300"
    ARRAY1(3,3) = "301"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"


    ARRAY1(4,1) = "������ ������� �������� ���������     "
    ARRAY1(4,2) = "400"
    ARRAY1(4,3) = "401"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"


    ARRAY1(5,1) = "������ ������� �������� ��������      "
    ARRAY1(5,2) = "500"
    ARRAY1(5,3) = "501"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"


    ARRAY1(6,1) = "������ ������� ������� � �������      "
    ARRAY1(6,2) = "600"
    ARRAY1(6,3) = "601"
    ARRAY1(6,4) = "0"
    ARRAY1(6,5) = "0"


    ARRAY1(7,1) = "������ ������� ������� ������ ������   "
    ARRAY1(7,2) = "700"
    ARRAY1(7,3) = "701"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"


    ARRAY1(8,1) = "������ ������� ����� ����� � ������   "
    ARRAY1(8,2) = "800"
    ARRAY1(8,3) = "801"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"


    ARRAY1(9,1) = "������ ������� ������� ����������� ����"
    ARRAY1(9,2) = "900"
    ARRAY1(9,3) = "901"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"


    ARRAY1(10,1) = "������ ������� ���� �������          "
    ARRAY1(10,2) = "1000"
    ARRAY1(10,3) = "1001"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"


    ARRAY1(11,1) = "������ ������� ������� ��� ������    "
    ARRAY1(11,2) = "1100"
    ARRAY1(11,3) = "1101"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"


    ARRAY1(12,1) = "������ ������� �������� � ������� ����"
    ARRAY1(12,2) = "1200"
    ARRAY1(12,3) = "1201"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"


    ARRAY1(13,1) = "������ ������� ����� ����� ���� ���  "
    ARRAY1(13,2) = "1300"
    ARRAY1(13,3) = "1301"
    ARRAY1(13,4) = "0"
    ARRAY1(13,5) = "0"


********************** ********************************
*******************   PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    GOSUB A.050.GET.ALL.BR

*    CALL PRINTER.OFF
*    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*----------------------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE 99 THEN
            GOTO A.050.A
        END
*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        FLAG.PRT = 0
*-----------------------------------------------------------------

        GOSUB A.060.CHK.INDSTRY
        GOSUB A.5000.PRT.HEAD
        GOSUB A.700.PRT.COMPANY

A.050.A:
    REPEAT
    RETURN
*----------------------------------------------------------------
A.060.CHK.INDSTRY:
    FOR SUB.A  = 1 TO 13

        WS.INDST.FRM =  ARRAY1(SUB.A,2)
        WS.INDST.TO  =  ARRAY1(SUB.A,3)
        GOSUB A.100.PROCESS
    NEXT SUB.A
    RETURN

*----------------------------------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE DD... AND CBE.NEW.INDUSTRY GE ":WS.INDST.FRM:" AND CBE.NEW.INDUSTRY LE ":WS.INDST.TO
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS

        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        WS.BRX = R.CBE<P.CBE.BR>
        IF WS.BRX NE WS.BR THEN
            GOTO START.B
        END
A.100.A:

        WS.NEW.SECTOR = R.CBE<P.CBE.NEW.SECTOR>
*                                      ������� �� ����� ���� ������� �����
        IF WS.NEW.SECTOR EQ 1120 OR WS.NEW.SECTOR EQ  2120 OR WS.NEW.SECTOR EQ 3120 OR WS.NEW.SECTOR EQ 4120 THEN
            GOTO START.A
        END

        GOTO START.B


START.A:
        WS.INDSTRY = R.CBE<P.CBE.NEW.INDUSTRY>


START.A1:
        FLAG.PRT = 1
        WS.1.LE = 0
        WS.1.EQV = 0


        WS.1.LE = R.CBE<P.CBE.FACLTY.LE> + R.CBE<P.CBE.COMRCL.PAPER.LE>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.CUR.AC.LE.DR>

        WS.1.EQV = R.CBE<P.CBE.FACLTY.EQ> + R.CBE<P.CBE.COMRCL.PAPER.EQ>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.CUR.AC.EQ.DR>

        GOSUB A.200.ACUMULAT
*-----------------------------------------------------
START.B:
    REPEAT
    RETURN

A.200.ACUMULAT:
    ARRAY1(SUB.A,4) = ARRAY1(SUB.A,4) + WS.1.LE
    ARRAY1(SUB.A,5) = ARRAY1(SUB.A,5) + WS.1.EQV
    RETURN

*--------------------------------------------------------
A.700.PRT.COMPANY:
    IF FLAG.PRT EQ 0 THEN
        RETURN
    END
    FOR SUB.A  = 1 TO 13
        GOSUB A.710.PRT
    NEXT SUB.A
    RETURN
A.710.PRT:
*--------------------------------------------------------
    WS.1.LE.LIN  = ARRAY1(SUB.A,4) / 1000
    WS.1.EQV.LIN = ARRAY1(SUB.A,5) / 1000
    WS.1.LE.TOT = WS.1.LE.TOT + WS.1.LE.LIN
    WS.1.EQV.TOT = WS.1.EQV.TOT + WS.1.EQV.LIN

    XX = SPACE(132)
    XX<1,1>[1,35]   = ";":ARRAY1(SUB.A,1)
    XX<1,1>[42,9]   = ";":FMT(WS.1.LE.LIN, "R0")
    XX<1,1>[55,9]   = ";":FMT(WS.1.EQV.LIN, "R0")
    WS.COMN         = WS.1.LE.LIN + WS.1.EQV.LIN
    XX<1,1>[68,9]   = ";":FMT(WS.COMN, "R0")
    BB.DATA = ""
    BB.DATA = XX<1,1>
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*PRINT XX<1,1>

    WS.1.LE.LIN     = 0
    WS.1.EQV.LIN    = 0
    ARRAY1(SUB.A,4) = 0
    ARRAY1(SUB.A,5) = 0

    IF SUB.A EQ 13 THEN
        GOSUB A.800.TOT
    END

    RETURN
*------------------------------------
A.800.TOT:
    XX = SPACE(132)
    XX<1,1>[42,9]   = "========="
    XX<1,1>[55,9]   = "========="
    XX<1,1>[68,9]   = "========="
*    PRINT XX<1,1>
    BB.DATA = ""
    BB.DATA = XX<1,1>
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    XX = SPACE(132)
    XX<1,1>[1,35]   = ";": "������ ����� ���� ������� �����  "
    XX<1,1>[42,9]   = ";":FMT(WS.1.LE.TOT, "R0")
    XX<1,1>[55,9]   = ";":FMT(WS.1.EQV.TOT, "R0")
    WS.COMN = WS.1.LE.TOT + WS.1.EQV.TOT
    XX<1,1>[68,9] = ";":FMT(WS.COMN, "R0")
*    PRINT XX<1,1>
    BB.DATA = ""
    BB.DATA = XX<1,1>
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    XX = SPACE(132)
    XX<1,1>[42,9]   = "========="
    XX<1,1>[55,9]   = "========="
    XX<1,1>[68,9]   = "========="
*    PRINT XX<1,1>
    BB.DATA = ""
    BB.DATA = XX<1,1>
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*GOSUB A.5100.PRT.SPACE.PAGE
    WS.1.LE.LIN = 0
    WS.1.EQV.LIN = 0
    WS.1.LE.TOT = 0
    WS.1.EQV.TOT = 0
    RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    IF FLAG.PRT EQ 0 THEN
        RETURN
    END
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

    DATY  = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD = "��� ���� ������"
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD = WS.BR.NAME
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD =" ������� : ":T.DAY
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD = ";":WS.HD.T:";":WS.HD.TA
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    WS.PAG.COUNT = WS.PAG.COUNT + 1
    PR.HD =";":WS.HD.T2:";":WS.HD.T2A:";":WS.PAG.COUNT
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD =";":WS.HD.T3
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD = WS.PRG.1
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*    PR.HD = WS.HD.2
*    WRITESEQ PR.HD TO BB ELSE
*        PRINT " ERROR WRITE FILE "
*    END

*    PR.HD = WS.HD.3
*    WRITESEQ PR.HD TO BB ELSE
*        PRINT " ERROR WRITE FILE "
*    END

    PR.HD = ";;":WS.HD.4A:";":WS.HD.4B:";":WS.HD.4C
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
A.5001.PRT.HEAD:
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    WS.PAG.COUNT = WS.PAG.COUNT + 1
    PR.HD :="'L'":SPACE(50):WS.HD.T2:SPACE(28):WS.HD.T2A:SPACE(2):WS.PAG.COUNT
    PR.HD :="'L'":SPACE(110):WS.HD.T3
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(45):WS.HD.2
    PR.HD :="'L'":SPACE(45):WS.HD.3
    PR.HD :="'L'":SPACE(41):WS.HD.4A:SPACE(4):WS.HD.4B:SPACE(3):WS.HD.4C
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
*    HEADING PR.HD
*    PRINT
    RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    RETURN
END
