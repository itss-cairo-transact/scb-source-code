* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBQ.ACCT.MAIN.CHARGE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HOLD.POST.CHECK
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.FEE.EXCP
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    PRINT "THE CHARGE IS DONE"

    RETURN
*------------------------------

INITIALISE:
*----------
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.SCB.STMT.CHARGE = 'F.SCB.STMT.CHARGE' ; F.SCB.STMT.CHARGE = '' ; R.SCB.STMT.CHARGE = ''

    CHG.ID = "ACFEES"
    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID,CR.ACT)


    V.DAT = TODAY
    DB.AMT = 0
    CUS = ''

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CB = 'FBNK.CUSTOMER.ACCOUNT' ; F.CB = ''
    CALL OPF(FN.CB,F.CB)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    FN.CHG = 'FBNK.FT.CHARGE.TYPE' ; F.CHG = '' ; R.CHG = '' ; ERR.CHG = ''
    CALL OPF(FN.CHG,F.CHG)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ERR.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.EXP = 'F.SCB.CUS.FEE.EXCP' ; F.EXP = ''
    CALL OPF(FN.EXP,F.EXP)

*--------------------------

    T.SEL1  = "SELECT FBNK.CUSTOMER WITH SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300 AND SECTOR NE 1400"
    T.SEL1 := " AND POSTING.RESTRICT NE 99 AND POSTING.RESTRICT NE 90 AND POSTING.RESTRICT NE 70 AND CREDIT.CODE NE 110 AND CREDIT.CODE NE 120"
    T.SEL1 := " AND FEES.CHARGE.EXP NE 'Y' AND POSTING.RESTRICT NE 18 AND DRMNT.CODE NE 1 AND TARGET NE 5800 AND TARGET NE 5850 AND TARGET NE 5860 AND TARGET NE 5870 AND TARGET NE 5900 BY @ID"

**************

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.CU,KEY.LIST1<I>,R.CU,F.CU,E1)
            CUST.ID = KEY.LIST1<I>
            CUFLG  = 0
            CUFLG1 = 0
            FLG1 = 0 ; FLG2 = 0

            COMP = R.CU<EB.CUS.COMPANY.BOOK>
            COM.CODE = COMP[8,2]

            WS.TARGET       = R.CU<EB.CUS.TARGET>
            WS.CONT.DATE    = R.CU<EB.CUS.CONTACT.DATE>


            OFS.USER.INFO = "AUTO.CHRGE":"/":"/" :COMP
***********************************************
            WS.REL.CODE     = R.CU<EB.CUS.RELATION.CODE>
            WS.CUS.REL.CODE = R.CU<EB.CUS.REL.CUSTOMER>
            IF WS.REL.CODE EQ '110' THEN
                CALL F.READ(FN.EXP,WS.CUS.REL.CODE,R.EXP,F.EXP,E5)
                WS.AMT.EXP = R.EXP<FEE.MAINTENANCE.FEE>
            END
***********************************************
            CALL F.READ(FN.CB,CUST.ID,R.CB,F.CB,E1)
            IF NOT(E1) THEN
***************************************************************
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF1)
                FN.SCB.STMT.CHARGE = 'F.SCB.STMT.CHARGE' ; F.SCB.STMT.CHARGE = '' ; R.SCB.STMT.CHARGE = ''
***************************************************************
                FLG = 0
                H = 1
                LOOP WHILE FLG = 0
                    IF R.CB<H,EB.CAC.ACCOUNT.NUMBER> = '' THEN
                        FLG = 1
                    END
                    ACT.CB = R.CB<H,EB.CAC.ACCOUNT.NUMBER>
                    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACT.CB,W.BAL)
                    ACT.CUR = ACT.CB[9,2]
                    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,ACT.CUR,CUR.CODE)
                    ACT.CATEG = ACT.CB[11,4]
                    IF ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1005 OR ACT.CATEG EQ 6512 OR ACT.CATEG EQ 6511 OR ACT.CATEG EQ 1006 OR (ACT.CATEG GE 6514 AND ACT.CATEG LE 6517) OR (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR (ACT.CATEG GE 1101 AND ACT.CATEG LE 1204 OR ACT.CATEG GE 1290 AND ACT.CATEG LE 1534 OR ACT.CATEG GE 1536 AND ACT.CATEG LE 1599) THEN

                        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR.CODE,RATE)
                        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACT.CB,LST.BAL)
                        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACT.CB,ACT.CUS)

                        IF CUR.CODE EQ "EGP" THEN
                            RATE = 1
                        END

                        IF WS.REL.CODE EQ '110' THEN
                            DB.AMT = WS.AMT.EXP
                        END ELSE
                            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID,DB.AMT)
                        END

                        IF CUR.CODE NE "EGP" THEN
                            DB.AMT = (DB.AMT / RATE)
                            DB.AMTE = FIELD(DB.AMT,".",2)
                            DB.AMT  = FIELD(DB.AMT,".",1):".":DB.AMTE[1,2]
                        END

*******************************************************************
                        IF CUFLG NE 1 THEN
                            IF LST.BAL GE DB.AMT THEN
                                DEBIT.ACCT = ACT.CB
                                CUS = ACT.CB[1,8]:".":TODAY
                                GOSUB CR.FT.OFS
                                LST.BAL = LST.BAL - DB.AMT
                                CUFLG = 1
                            END ELSE
                                CUFLG = 3
                            END
                        END

*******************************************************************
                    END
                    H = H + 1
                REPEAT
            END
            GOSUB INS.REC
        NEXT I
    END
    RETURN
****************************************************************
INS.REC:
********
    CUS1 = KEY.LIST1<I>:".":TODAY

    IF CUFLG = 3 THEN

        R.SCB.STMT.CHARGE<STM.FEES.FLG> = "YES"
        R.SCB.STMT.CHARGE<STM.CO.CODE>  = COMP

        CALL F.WRITE(FN.SCB.STMT.CHARGE,CUS1,R.SCB.STMT.CHARGE)
        CALL JOURNAL.UPDATE(CUS1)

    END

    RETURN
**************************************************************
CR.FT.OFS:
*********
    DR.ACCT = DEBIT.ACCT
    CURR = DEBIT.ACCT[9,2]
    CR.ACCT = "PL":CR.ACT
    DATEE = TODAY

***** 2018/09/17 ****
    IF ( WS.TARGET EQ '8' AND WS.CONT.DATE GE '20180101' AND WS.CONT.DATE LT '20201015' ) OR WS.TARGET EQ '30' THEN
        DB.AMT = DB.AMT / 2
    END

*******************
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC74":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS[1,7]

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    TER.NO.P = COM.CODE + 1000
    OFS.ID = "T":TRIM(TER.NO.P, '0', 'L'):".P.":CUS

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

    RETURN
************************************************************

END
