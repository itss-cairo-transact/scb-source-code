* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.PL.NO.M

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CATEG.NO.M
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

    ETEXT        ='' ; E            ='' ; CHQ.NOS ='' ; CHQ.RETURN   ='' ; CHQ.STOP   ='' ; LF ='' ; RH  ='' ;
    COUNTS1      ='' ; COUNTS11     ='' ; CHQ.STAT='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS ='' ;
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE ='' ;
    CHQ.AMT      =''
***** CLEAR FILE *****

    FN.CCBE = "F.SCB.CATEG.NO.M"
    F.CCBE  = ''
    CALL OPF(FN.CCBE,F.CCBE)
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**    OPEN FN.CCBE TO FILEVAR ELSE ABORT 201, FN.CCBE
**   CLEARFILE FILEVAR
    CLEARFILE F.CCBE
**********************
    FN.CHQ.PRESENT = 'F.SCB.CATEG.NO.M' ; F.CHQ.PRESENT = '' ; R.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    FN.ACC = 'FBNK.CATEG.ENTRY' ;F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    IDDD="EG0010001-COB"
    CALL DBR('DATES':@FM:EB.DAT.PERIOD.END,IDDD,P.END)

    START.DAY = P.END[1,6]:"01"
    END.DAY   = P.END

    WS.NAR.DAT = TODAY[1,4]:'0101'
    CALL CDT ('',WS.NAR.DAT,'-1C')

    WS.NAR = '...YEAR...END...PL...ENTRY...':WS.NAR.DAT:'...'

    T.SEL = "SELECT FBNK.CATEG.ENTRY WITH (BOOKING.DATE GE ":START.DAY :" AND BOOKING.DATE LE ":END.DAY:" ) AND CURRENCY NE EGP AND (PL.CATEGORY NE 69999 AND PL.CATEGORY NE 54000 AND  PL.CATEGORY NE 54151 AND PL.CATEGORY NE 53000 AND PL.CATEGORY NE 53088) AND NARRATIVE UNLIKE ":WS.NAR:" BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        SCB.CHQ.ID = KEY.LIST<I>
        CALL F.READ(FN.ACC,SCB.CHQ.ID,R.ACC,F.ACC,E1)
        BOOK.DATE = '' ; CUR = '' ; COM.CODE = ''
        BOOK.DATE = R.ACC<AC.CAT.BOOKING.DATE>
        CUR       = R.ACC<AC.CAT.CURRENCY>
        COM.CODE  = R.ACC<AC.CAT.COMPANY.CODE>

        R.CHQ.PRESENT<SCB.CATEG.CURRENCY>     =  CUR
        R.CHQ.PRESENT<SCB.CATEG.BOOKING.DATE> =  BOOK.DATE
        R.CHQ.PRESENT<SCB.CATEG.COMPANY.BOOK> =  COM.CODE

        WRITE  R.CHQ.PRESENT TO F.CHQ.PRESENT , SCB.CHQ.ID ON ERROR
            PRINT "CAN NOT WRITE RECORD":SCB.CHQ.ID:"TO" :FN.CHQ.PRESENT
        END

    NEXT I
    RETURN
END
