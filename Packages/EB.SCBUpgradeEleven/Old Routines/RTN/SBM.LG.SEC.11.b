* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBM.LG.SEC.11

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBM.LG.SECTOR.1
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

*************************************************
    GOSUB CLEARFILE
    GOSUB INITIATE
*PRINT "INT FINISH"

    GOSUB GETAC
*PRINT "AC FINISH"

    GOSUB GETDR
*PRINT "DR FINISH"

    GOSUB GETLCEX
*PRINT "LCEXP FINISH"

    GOSUB GETLCIM
*PRINT "LCIMP FINISH"

    GOSUB GETLD96
*PRINT "LD96 FINISH"

    GOSUB GETLD97
*PRINT "LD97 FINISH"

    GOSUB GETLDSP
*PRINT "LDSP FINISH"

**** COPY FROM YYY ****

    EXECUTE "SELECT YYY"
    EXECUTE "COPY FROM YYY TO F.SBM.LG.SECTOR.1"

    RETURN
*************************************************
CLEARFILE:
    FN.CCBE = "F.SBM.LG.SECTOR.1"
    F.CCBE = ''
    CALL OPF(FN.CCBE,F.CCBE)
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**    OPEN FN.CCBE TO FILEVAR ELSE ABORT 201, FN.CCBE
**    CLEARFILE FILEVAR
    CLEARFILE F.CCBE
*********
    PRINT "FILE IS CLEARED"

    RETURN
************************************************

INITIATE:
    FN.LD  = "FBNK.LD.LOANS.AND.DEPOSITS" ; F.LD   = "" ; R.LD  = ""
    FN.CU  = "FBNK.CUSTOMER"              ; F.CU   = "" ; R.CU  = ""
    FN.BAL = "F.SBM.LG.SECTOR.1"          ; F.BAL  = "" ; R.BAL = ""
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'     ; F.CCY  = '' ; R.CCY = ''
    FN.BR  = "FBNK.BILL.REGISTER"         ; F.BR   = '' ; R.BR = ''
    FN.LC  = "FBNK.LETTER.OF.CREDIT"      ; F.LC   = '' ; R.LC = ''
    FN.DR = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
    FN.AC = 'FBNK.ACCOUNT'; F.AC = '' ; R.AC = ''
    CALL OPF(FN.DR,F.DR)
    CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.BR,F.BR)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.AC,F.AC)
    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1W')

    KEY.LIST  = "" ; SELECTED  = "" ;  ER.MSG  = ""
    KEY.LIST1 = "" ; SELECTED1 = "" ;  ER.MSG1 = ""
    KEY.LIST2 = "" ; SELECTED2 = "" ;  ER.MSG2 = ""
    KEY.LIST3 = "" ; SELECTED3 = "" ;  ER.MSG3 = ""
    KEY.LIST4 = "" ; SELECTED4 = "" ;  ER.MSG4 = ""
    KEY.LIST5 = "" ; SELECTED5 = "" ;  ER.MSG5 = ""

    NEW.SECTOR   = 0 ; LIST        = 0 ; CUR           = 0
    AMT.LOCAL.IN = 0 ; AMT.FOR.IN  = 0 ; AMT.LOCAL.OUT = 0
    AMT.FOR.OUT  = 0 ; AMT.WT.LO   = 0 ; AMT.WT.FO     = 0
    AMT.CONV.LO  = 0 ; AMT.CONV.FO = 0 ; TOT.MARG4     = 0
    LOCAL.IN     = 0 ; FOR.IN      = 0 ; LOCAL.OUT     = 0
    FOR.OUT      = 0 ; FU.BI.LCY4  = 0 ; FU.BI.LCY3    = 0
    FU.BI.LCY2   = 0 ; AMT.LC.LO33 = 0 ; AMT.LC.FO33   = 0

    MRG.500.LCY.1 = 0 ; MRG.500.FCY.1 = 0 ; MRG.110.LCY.1 = 0
    MRG.110.FCY.1 = 0 ; MRG.OTH.LCY.1 = 0 ; MRG.OTH.FCY.1 = 0
    MRG.500.LCY.2 = 0 ; MRG.500.FCY.2 = 0 ; MRG.110.LCY.2 = 0
    MRG.110.FCY.2 = 0 ; MRG.OTH.LCY.2 = 0 ; MRG.OTH.FCY.2 = 0

    RETURN
*=======SELECT FROM ACC========*
GETAC:
    T.SEL9 = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (3010 3011 3012 3013 3014 3015 3016 3017)"
    CALL EB.READLIST (T.SEL9,KEY.LIST9,"",SELECTED9,ER.MSG9)
    IF SELECTED9 THEN
        FOR LL = 1 TO SELECTED9
            CALL F.READ(FN.AC,KEY.LIST9<LL>,R.AC,F.AC,E9)
            AC.CATEG = R.AC<AC.CATEGORY>
            AC.BAL =  R.AC<AC.OPEN.ACTUAL.BAL>
            AC.CUR = R.AC<AC.CURRENCY>
            AC.CUS = R.AC<AC.CUSTOMER>

            CALL F.READ(FN.CU,AC.CUS,R.CU,F.CU,E2)
            AC.NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 155 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE AC.CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE.AC  = R.CCY<RE.BCP.RATE,POS>
            BEGIN CASE
            CASE ( AC.NEW.SEC GE 500 AND AC.NEW.SEC LE 600 )
                IF AC.CUR EQ 'EGP' THEN
                    IF AC.CATEG EQ '3013' THEN
                        MRG.500.LCY.1 += AC.BAL * RATE.AC
                    END ELSE
                        MRG.500.LCY.2 += AC.BAL * RATE.AC
                    END
                END
                IF AC.CUR NE 'EGP' THEN
                    IF AC.CATEG EQ '3013' THEN
                        MRG.500.FCY.1 += AC.BAL * RATE.AC
                    END ELSE
                        MRG.500.FCY.2 += AC.BAL * RATE.AC
                    END
                END

            CASE AC.NEW.SEC EQ 110
                IF AC.CUR EQ 'EGP' THEN
                    IF AC.CATEG EQ '3013' THEN
                        MRG.110.LCY.1 += AC.BAL * RATE.AC
                    END ELSE
                        MRG.110.LCY.2 += AC.BAL * RATE.AC
                    END
                END
                IF AC.CUR NE 'EGP' THEN
                    IF AC.CATEG EQ '3013' THEN
                        MRG.110.FCY.1 += AC.BAL * RATE.AC
                    END ELSE
                        MRG.110.FCY.2 += AC.BAL * RATE.AC
                    END
                END

            CASE OTHERWISE
                IF AC.CUR EQ 'EGP' THEN
                    IF AC.CATEG EQ '3013' THEN
                        MRG.OTH.LCY.1 += AC.BAL * RATE.AC
                    END ELSE
                        MRG.OTH.LCY.2 += AC.BAL * RATE.AC
                    END
                END
                IF AC.CUR NE 'EGP' THEN
                    IF AC.CATEG EQ '3013' THEN
                        MRG.OTH.FCY.1 += AC.BAL * RATE.AC
                    END ELSE
                        MRG.OTH.FCY.2 += AC.BAL * RATE.AC
                    END
                END

            END CASE

        NEXT LL
    END
    BAL.ID = 'MRG'
    GOSUB BUILD.RECORD
    RETURN
**----- SELECT DRAWINGS  -------**
GETDR:
    T.SEL = "SELECT FBNK.DRAWINGS WITH MATURITY.REVIEW GT ":DAT:"  AND (LC.CREDIT.TYPE LIKE LI... OR LC.CREDIT.TYPE LIKE DI...)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            NEW.SECTOR    = 0
            LIST          = 0
            CUR           = 0
            AMT.LOCAL.IN  = 0
            AMT.FOR.IN    = 0
            AMT.LOCAL.OUT = 0
            AMT.FOR.OUT   = 0
            AMT.BR.LO = 0
            AMT.BR.FO = 0
            AMT.WT.LO = 0
            AMT.WT.FO = 0
            AMT.CONV.LO = 0
            AMT.CONV.FO = 0
            LOCAL.IN  = 0
            FOR.IN    = 0
            LOCAL.OUT = 0
            FOR.OUT   = 0
            CURR = 0
            CALL F.READ(FN.DR,KEY.LIST<I>,R.DR,F.DR,E1)
            BR.ID  = KEY.LIST<I>
            CURR = R.DR<TF.DR.DRAW.CURRENCY>
            AMT1 = R.DR<TF.DR.DOCUMENT.AMOUNT>
            CUS.ID1 = R.DR<TF.DR.CUSTOMER.LINK>
* -----------------------------------------------------------------------**
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 245 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE1  = R.CCY<RE.BCP.RATE,POS>
            AMT11   = AMT1 * RATE1
            CALL F.READ(FN.CU,CUS.ID1,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]

            IF CURR EQ 'EGP'  THEN
                AMT.BR.LO = AMT11
            END ELSE
                AMT.BR.FO = AMT11

            END
            CUR = CURR
            LOCAL.IN  =  AMT.BR.LO
            FOR.IN    =  AMT.BR.FO
            LOCAL.OUT = 0
            FOR.OUT   = 0
***************************************
            BEGIN CASE
            CASE ( NEW.SEC GE 500 AND NEW.SEC LE 600 )
                AMT.WT.LO = 0
                AMT.WT.FO = 100
                AMT.CONV.LO = 100
                AMT.CONV.FO = 100

            CASE  NEW.SEC EQ 110
                AMT.WT.LO = 20
                AMT.WT.FO = 100
                AMT.CONV.LO = 100
                AMT.CONV.FO = 100

            CASE OTHERWISE
                AMT.WT.LO = 100
                AMT.WT.FO = 100
                AMT.CONV.LO = 100
                AMT.CONV.FO = 100

            END CASE

            IF AMT11 NE '' THEN
                Y11 = '����� ���� ����� � �������� �����'
            END
            LIST = 1
            BAL.ID = BR.ID:"-":LIST
            GOSUB BUILD.RECORD
        NEXT I
    END
    RETURN
*----------- SELECT LETTER.OF.CREDIT(EXPORT)--------------------------**
GETLCEX:
    T.SEL2  = "SELECT ":FN.LC:" WITH CATEGORY.CODE IN (23100 23102 23103 23150 23152 23153)"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR X = 1 TO SELECTED2
            NEW.SECTOR   = 0 ; LIST       = 0 ; CUR           = 0
            AMT.LOCAL.IN = 0 ; AMT.FOR.IN = 0 ; AMT.LOCAL.OUT = 0
            AMT.FOR.OUT  = 0 ; AMT.LC1.LO = 0 ; AMT.LC2.FO    = 0
            AMT.WT.LO    = 0 ; AMT.WT.FO  = 0 ; AMT.CONV.LO   = 0
            AMT.CONV.FO  = 0 ; LOCAL.IN   = 0 ; FOR.IN        = 0
            LOCAL.OUT    = 0 ; FOR.OUT    = 0

            CALL F.READ(FN.LC,KEY.LIST2<X>,R.LC,F.LC,E2)
            LC.ID1   = KEY.LIST2<X>
            CUS.ID2 = R.LC<TF.LC.APPLICANT.CUSTNO>
            CURR2   = R.LC<TF.LC.LC.CURRENCY>
            AMT2    = R.LC<TF.LC.LIABILITY.AMT>
* -----------------------------------------------------------------------*
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 314 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR2 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE2  = R.CCY<RE.BCP.RATE,POS>
            AMT22  = AMT2 * RATE2
            CALL F.READ(FN.CU,CUS.ID2,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]

            IF CURR2 EQ 'EGP'  THEN
                AMT.LC1.LO = AMT22
            END ELSE
                AMT.LC2.FO = AMT22
            END

            CUR = CURR2
            LOCAL.IN  =  AMT.LC1.LO
            FOR.IN    =  AMT.LC2.FO
            LOCAL.OUT = 0
            FOR.OUT   = 0
***********************************************
            BEGIN CASE
            CASE ( NEW.SEC GE 500 AND NEW.SEC LE 600 )
                AMT.WT.LO = 0
                AMT.WT.FO = 100
                AMT.CONV.LO = 20
                AMT.CONV.FO = 20

            CASE  NEW.SEC EQ 110
                AMT.WT.LO = 20
                AMT.WT.FO = 100
                AMT.CONV.LO = 20
                AMT.CONV.FO = 20

            CASE OTHERWISE
                AMT.WT.LO = 100
                AMT.WT.FO = 100
                AMT.CONV.LO = 20
                AMT.CONV.FO = 20

            END CASE
************************************************
            IF AMT22 NE '' THEN
                Y22 = '�������� ������'
            END
            LIST = 2
            BAL.ID = LC.ID1:"-":LIST
            GOSUB BUILD.RECORD
        NEXT X
    END
    RETURN
*-----------SELECT LETTER.OF.CREDIT(IMPORT)--------------------------*
GETLCIM:
    T.SEL3  = "SELECT ":FN.LC:" WITH CATEGORY.CODE IN (23351 23502 23508 23552 23553 23559)"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    IF SELECTED3 THEN
        FOR Z = 1 TO SELECTED3

            NEW.SECTOR   = 0 ; LIST       = 0 ; CUR           = 0
            AMT.LOCAL.IN = 0 ; AMT.FOR.IN = 0 ; AMT.LOCAL.OUT = 0
            AMT.FOR.OUT  = 0 ; AMT.LC3.LO = 0 ; AMT.LC3.FO    = 0
            AMT.WT.LO    = 0 ; AMT.WT.FO  = 0 ; AMT.CONV.LO   = 0
            AMT.CONV.FO  = 0 ; LOCAL.IN   = 0 ; FOR.IN        = 0
            LOCAL.OUT    = 0 ; FOR.OUT    = 0 ; AMT.LC33.LO   = 0
            AMT.LC33.FO  = 0 ;  AMT.LC.LO44 = 0 ; AMT.LC.FO44 = 0

            CALL F.READ(FN.LC,KEY.LIST3<Z>,R.LC,F.LC,E3)
            LC.ID2   = KEY.LIST3<Z>
            CUS.ID3 = R.LC<TF.LC.APPLICANT.CUSTNO>
            CURR3   = R.LC<TF.LC.LC.CURRENCY>
            AMT3    = R.LC<TF.LC.LIABILITY.AMT>
* -----------------------------------------------------------------------*
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 385 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR3 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE3  = R.CCY<RE.BCP.RATE,POS>
            AMT3   = AMT3 * RATE3
            CALL F.READ(FN.CU,CUS.ID3,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]
            IF CURR3 EQ 'EGP'  THEN
                AMT.LC3.LO = AMT3
            END ELSE
                AMT.LC3.FO = AMT3
            END
**********************************************************
            DR.ID = KEY.LIST3<Z>:"..."
            T.SEL33 = "SELECT FBNK.DRAWINGS WITH MATURITY.REVIEW GE ":DAT:" AND @ID LIKE ":DR.ID
            CALL EB.READLIST(T.SEL33,KEY.LIST33,"",SELECTED33,ER.MSG33)
            IF SELECTED33 THEN
                FOR II = 1 TO SELECTED33
                    CALL F.READ(FN.DR,KEY.LIST33<II>,R.DR,F.DR,E33)
                    LC.TYPE = R.DR<TF.DR.LC.CREDIT.TYPE>[2,1]
                    IF LC.TYPE EQ 'E' THEN
                        CURR33   = R.DR<TF.DR.DRAW.CURRENCY>
                        AMT33    = R.DR<TF.DR.DOCUMENT.AMOUNT>
                        CUS.ID33 = R.DR<TF.DR.CUSTOMER.LINK>
* -----------------------------------------------------------------------**
                        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 410 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                        LOCATE CURR33 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                        RATE33  = R.CCY<RE.BCP.RATE,POS>
                        AMT333  = AMT33 * RATE33
                        CALL F.READ(FN.CU,CUS.ID33,R.CU,F.CU,E2)
                        NEW.SEC33 = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]
                        IF CURR33 EQ 'EGP'  THEN
                            AMT.LC.LO33 = AMT333
                        END ELSE
                            AMT.LC.FO33 = AMT333
                        END

                        AMT.LC3.LO += AMT.LC.LO33
                        AMT.LC3.FO += AMT.LC.FO33
                        AMT.LC.LO33 = 0 ; AMT.LC.FO33 = 0
                    END
                NEXT II
                AMT.LC.LO33 = 0 ; AMT.LC.FO33 = 0
            END
*******************************************
            AMT.WT.LO = 100
            AMT.WT.FO = 100
            AMT.CONV.LO = 100
            AMT.CONV.FO = 100
            IF AMT3 NE '' THEN
                Y33 = '�������� �������'
            END
            CUR = CURR3

            LOCAL.IN  =  AMT.LC3.LO
            FOR.IN    =  AMT.LC3.FO
            LOCAL.OUT = 0
            FOR.OUT   = 0

            LIST = 3
            BAL.ID = LC.ID2 :"-": LIST
            GOSUB BUILD.RECORD
        NEXT Z
    END
    RETURN
*------------LD (21096)------**
GETLD96:
    T.SEL4  = "SELECT ":FN.LD:" WITH CATEGORY EQ 21096 AND WITHOUT @ID IN ( LD0701700072 LD0701800094 LD0701700068 )"
    T.SEL4 := " AND FIN.MAT.DATE GE ":DAT:" AND AMOUNT NE '0' AND STATUS NE 'LIQ'"
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
    IF SELECTED4 THEN
        FOR H = 1 TO SELECTED4
            NEW.SECTOR    = 0
            LIST          = 0
            CUR           = 0
            AMT.LOCAL.IN  = 0
            AMT.FOR.IN    = 0
            AMT.LOCAL.OUT = 0
            AMT.FOR.OUT   = 0
            AMT.LG96.LO = 0
            AMT.LG96.FO = 0
            TOT.DEF4 = 0
            AMT.WT.LO = 0
            AMT.WT.FO = 0
            AMT.CONV.LO = 0
            AMT.CONV.FO = 0
            LOCAL.IN  = 0
            FOR.IN    = 0
            LOCAL.OUT = 0
            FOR.OUT   = 0

            CALL F.READ(FN.LD,KEY.LIST4<H>,R.LD,F.LD,E1)
            LD.ID4  = KEY.LIST4<H>
            CUS.ID4 = R.LD<LD.CUSTOMER.ID>
            CURR4   = R.LD<LD.CURRENCY>
            PROD   = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
            MARG4  = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
            AC.CUR = R.LD<LD.LOCAL.REF><1,LDLR.ACC.CUR>
            AMT4   = R.LD<LD.AMOUNT>
**-----------------------------------------
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 486 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR4 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE4  = R.CCY<RE.BCP.RATE,POS>
            AMT44   = AMT4 * RATE4
            MARG44  = MARG4 * RATE4
            CALL F.READ(FN.CU,CUS.ID4,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]

            REF4   = R.LD<LD.LIMIT.REFERENCE>
            REF44  = REF4[1,4]
            DEF44 = AMT44 - MARG44
*****************************************************
            IF REF44 EQ '2505' OR REF44 EQ '2520' THEN
                TOT.MARG4 = 0
            END ELSE
                IF REF44 EQ '2510' THEN
                    TOT.DEF4 = DEF44
                END
            END
            TOT.96 = TOT.MARG4 + TOT.DEF4
            IF CURR4 EQ 'EGP' THEN
                AMT.LG96.LO = TOT.96
            END ELSE
                AMT.LG96.FO = TOT.96
            END
            CUR      = CURR4
            LOCAL.IN = AMT.LG96.LO
            FOR.IN   = AMT.LG96.FO
            LOCAL.OUT = 0
            FOR.OUT = 0
***********************************************
            BEGIN CASE
            CASE ( NEW.SEC GE 500 AND NEW.SEC LE 600 )
                AMT.WT.LO = 0
                AMT.WT.FO = 100
                AMT.CONV.LO = 50
                AMT.CONV.FO = 50

            CASE  NEW.SEC EQ 110
                AMT.WT.LO = 20
                AMT.WT.FO = 100
                AMT.CONV.LO = 50
                AMT.CONV.FO = 50
            CASE OTHERWISE
                AMT.WT.LO = 100
                AMT.WT.FO = 100
                AMT.CONV.LO = 50
                AMT.CONV.FO = 50

            END CASE
****************************************************
            IF AMT44 NE '' THEN
                Y44 = '������ ������'
            END
            LIST = 4
            BAL.ID   = LD.ID4 :"-": LIST
            GOSUB BUILD.RECORD
        NEXT H
    END
    RETURN
*****************************************************
GETLD97:
*-------LD (21097)------***
    T.SEL5  = "SELECT ":FN.LD:" WITH CATEGORY EQ 21097"
    T.SEL5 := " AND FIN.MAT.DATE GE ":DAT:" AND AMOUNT NE '0' AND STATUS NE 'LIQ'"
    CALL EB.READLIST(T.SEL5,KEY.LIST5,"",SELECTED5,ER.MSG5)
    IF SELECTED5 THEN
        FOR RR = 1 TO SELECTED5
            NEW.SECTOR    = 0
            LIST          = 0
            CUR           = 0
            AMT.LOCAL.IN  = 0
            AMT.FOR.IN    = 0
            AMT.LOCAL.OUT = 0
            AMT.FOR.OUT   = 0
            AMT.LG97.LO = 0
            AMT.LG97.FO = 0
            AMT.WT.LO = 0
            AMT.WT.FO = 0
            AMT.CONV.LO = 0
            AMT.CONV.FO = 0
            LOCAL.IN  = 0
            FOR.IN    = 0
            LOCAL.OUT = 0
            FOR.OUT   = 0

            CALL F.READ(FN.LD,KEY.LIST5<RR>,R.LD,F.LD,E5)
            LD.ID5  = KEY.LIST5<RR>
            CUS.ID5 = R.LD<LD.CUSTOMER.ID>
            CURR5   = R.LD<LD.CURRENCY>
            PROD5   = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
            MARG5  = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
            AC.CUR5 = R.LD<LD.LOCAL.REF><1,LDLR.ACC.CUR>
            AMT5  = R.LD<LD.AMOUNT>
**-----------------------------------------
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 582 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR5 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE5  = R.CCY<RE.BCP.RATE,POS>
            AMT55   = AMT5 * RATE5
            MARG55  = MARG5 * RATE5
            CALL F.READ(FN.CU,CUS.ID5,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]

            REF5   = R.LD<LD.LIMIT.REFERENCE>
            REF55  = REF5[1,4]
            DEF55 = AMT55
            IF REF55 EQ '5740' THEN
                TOT.97 = DEF55
            END
            IF CURR5 EQ 'EGP' THEN
                AMT.LG97.LO = TOT.97
            END ELSE
                AMT.LG97.FO = TOT.97
            END
            CUR = CURR5
            LOCAL.IN  =  0
            FOR.IN    = 0
            LOCAL.OUT = AMT.LG97.LO
            FOR.OUT   = AMT.LG97.FO
***********************************
            BEGIN CASE
            CASE ( NEW.SEC GE 500 AND NEW.SEC LE 600 )
                AMT.WT.LO = 20
                AMT.WT.FO = 20
                AMT.CONV.LO = 50
                AMT.CONV.FO = 50

            CASE NEW.SEC EQ 110
                AMT.WT.LO = 20
                AMT.WT.FO = 20
                AMT.CONV.LO = 50
                AMT.CONV.FO = 50
            CASE OTHERWISE
                AMT.WT.LO = 100
                AMT.WT.FO = 100
                AMT.CONV.LO = 50
                AMT.CONV.FO = 50

            END CASE
**********************************************
            IF TOT.97 NE '' THEN
                Y55 = '������ ���� ����� ���� ��� ��� ���� ������ �� �������'
            END
            LIST = 5
            BAL.ID   = LD.ID5 :"-": LIST
            GOSUB BUILD.RECORD
        NEXT RR
    END
    RETURN
**************************************************
GETLDSP:
*-------LD(SP)------***
    T.SEL6  = "SELECT ":FN.LD:" WITH @ID IN ( LD0701700072 LD0701800094 LD0701700068 )"
    T.SEL6 := " AND FIN.MAT.DATE GE ":DAT:" AND AMOUNT NE '0' AND STATUS NE 'LIQ'"
    CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ER.MSG6)
    IF SELECTED6 THEN
        FOR M = 1 TO SELECTED6
            NEW.SECTOR    = 0
            LIST          = 0
            CUR           = 0
            AMT.LOCAL.IN  = 0
            AMT.FOR.IN    = 0
            AMT.LOCAL.OUT = 0
            AMT.FOR.OUT   = 0
            AMT.LGSP.LO = 0
            AMT.LGSP.FO = 0
            AMT.WT.LO = 0
            AMT.WT.FO = 0
            AMT.CONV.LO = 0
            AMT.CONV.FO = 0
            LOCAL.IN  = 0
            FOR.IN    = 0
            LOCAL.OUT = 0
            FOR.OUT   = 0

            CALL F.READ(FN.LD,KEY.LIST6<M>,R.LD,F.LD,E6)
            LD.ID6  = KEY.LIST6<M>
            CUS.ID6 = R.LD<LD.CUSTOMER.ID>
            CURR6   = R.LD<LD.CURRENCY>
            PROD6   = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
            MARG6   = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
            AC.CUR6 = R.LD<LD.LOCAL.REF><1,LDLR.ACC.CUR>
            AMT6    = R.LD<LD.AMOUNT>
**-----------------------------------------
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 672 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR6 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE6  = R.CCY<RE.BCP.RATE,POS>
            AMT66   = AMT6 * RATE6
            MARG66  = MARG6 * RATE6
            CALL F.READ(FN.CU,CUS.ID6,R.CU,F.CU,E2)

            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>[2,3]
            REF6   = R.LD<LD.LIMIT.REFERENCE>
            REF66  = REF6[1,4]
            DEF66 = AMT66 - MARG66
*****************************
            IF REF66 EQ '2505' OR REF66 EQ '2520' THEN
                TOT.MARG66 = 0
            END ELSE
                IF REF66 EQ '2510' THEN
                    TOT.DEF66 = DEF66
                END
            END
            TOT.SP = TOT.MARG66 + TOT.DEF66

            IF CURR6 EQ 'EGP' THEN
                AMT.LGSP.LO = TOT.SP
            END ELSE
                AMT.LGSP.FO = TOT.SP
            END
            CUR = CURR6
            LOCAL.IN     =  AMT.LGSP.LO
            FOR.IN       =  AMT.LGSP.FO
            LOCAL.OUT    = 0
            FOR.OUT      = 0
******************************
            BEGIN CASE
            CASE ( NEW.SEC GE 500 AND NEW.SEC LE 600 )
                AMT.WT.LO  = 0
                AMT.WT.FO   = 100
                AMT.CONV.LO = 100
                AMT.CONV.FO = 100

            CASE  NEW.SEC EQ 110
                AMT.WT.LO = 20
                AMT.WT.FO = 100
                AMT.CONV.LO = 100
                AMT.CONV.FO = 100
            CASE OTHERWISE
                AMT.WT.LO = 0
                AMT.WT.FO = 100
                AMT.CONV.LO = 100
                AMT.CONV.FO = 100
            END CASE

            IF TOT.SP NE '' THEN
                Y66 = '�������� ����� �� ������ ���� ��������� ���������� � ������ �����'
            END
            LIST = 6
            BAL.ID   = LD.ID6 :"-": LIST
            GOSUB BUILD.RECORD
        NEXT M
    END
    RETURN
**-----------------------------BUILD.RECORD-----------------------------**
BUILD.RECORD:
**-----------
    CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E9)
    IF BAL.ID NE 'MRG' THEN
        GOSUB GET.SEC
        R.BAL<RE.NEW.SECTOR>    = SECC
        R.BAL<RE.LIST>          = LIST
        R.BAL<RE.CUR>           = CUR
        R.BAL<RE.AMT.LOCAL.IN>  = LOCAL.IN
        R.BAL<RE.AMT.FOR.IN>    = FOR.IN
        R.BAL<RE.AMT.LOCAL.OUT> = LOCAL.OUT
        R.BAL<RE.AMT.FOR.OUT>   = FOR.OUT
        R.BAL<RE.AMT.WT.LO>     = AMT.WT.LO
        R.BAL<RE.AMT.WT.FO>     = AMT.WT.FO
        R.BAL<RE.AMT.CONV.LO>   = AMT.CONV.LO
        R.BAL<RE.AMT.CONV.FO>   = AMT.CONV.FO
    END
    IF BAL.ID EQ 'MRG' THEN
        R.BAL<RE.MRG.500.LCY.1>  = MRG.500.LCY.1
        R.BAL<RE.MRG.500.FCY.1>  = MRG.500.FCY.1
        R.BAL<RE.MRG.110.LCY.1>  = MRG.110.LCY.1
        R.BAL<RE.MRG.110.FCY.1>  = MRG.110.FCY.1
        R.BAL<RE.MRG.OTH.LCY.1>  = MRG.OTH.LCY.1
        R.BAL<RE.MRG.OTH.FCY.1>  = MRG.OTH.FCY.1
        R.BAL<RE.MRG.500.LCY.2>  = MRG.500.LCY.2
        R.BAL<RE.MRG.500.FCY.2>  = MRG.500.FCY.2
        R.BAL<RE.MRG.110.LCY.2>  = MRG.110.LCY.2
        R.BAL<RE.MRG.110.FCY.2>  = MRG.110.FCY.2
        R.BAL<RE.MRG.OTH.LCY.2>  = MRG.OTH.LCY.2
        R.BAL<RE.MRG.OTH.FCY.2>  = MRG.OTH.FCY.2
    END
    CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
    CALL  JOURNAL.UPDATE(BAL.ID)
    RETURN
**---------------------------- GET SEC -----------------------------**
GET.SEC:
**-----
    BEGIN CASE
    CASE ( NEW.SEC GE 500 AND NEW.SEC LE 600 )
        SECC = 1
    CASE  NEW.SEC EQ 110
        SECC = 2
    CASE OTHERWISE
        SECC = 3
    END CASE
    RETURN
**---------------------------- GET LIST -----------------------------**
GET.LIST:
**-----
    BEGIN CASE
    CASE Y11 NE ''
        LIST = 1
    CASE Y22 NE ''
        LIST = 2
    CASE Y33 NE ''
        LIST = 3
    CASE Y44 NE ''
        LIST = 4
    CASE Y55 NE ''
        LIST = 5
    CASE OTHERWISE
        LIST = 6
    END CASE
    RETURN
**------------------------- THE HAPPY END ----------------------------**
