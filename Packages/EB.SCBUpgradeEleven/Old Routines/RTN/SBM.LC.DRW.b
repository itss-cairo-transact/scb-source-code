* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LC.DRW

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.LC.DRW
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 44 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LC.DRW.M.2'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.AMOUNT.LCY1 = 0  ; Y.AMOUNT.FCY1 = 0
    Y.AMOUNT.LCY2 = 0  ; Y.AMOUNT.FCY2 = 0
    Y.AMOUNT.LCY3 = 0  ; Y.AMOUNT.FCY3 = 0
    Y.AMOUNT.LCY4 = 0  ; Y.AMOUNT.FCY4 = 0
    DRW.AMT.LCY = 0 ; PROV.AMT.LCY = 0
    MARG.AMT1 = '' ; REL.AMT1 = '' ; DRW.AMTTTT = '' ; XX8 = '' ; HH = 0
    COMP = ID.COMPANY
    TD = TODAY
    RETURN
*========================================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.LC.DRW' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.LC = "FBNK.LETTER.OF.CREDIT"
    F.LC  = ""
    CALL OPF(FN.LC,F.LC)

    FN.DRW = "FBNK.DRAWINGS"
    F.DRW  = ""
    CALL OPF(FN.DRW,F.DRW)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
****************************** LC *******************************************
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LIABILITY.AMT GT 0 AND CATEGORY.CODE IN (23100 23102 23152 23155 23150) AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR X = 1 TO SELECTED
            CALL F.READ(FN.LC,KEY.LIST<X>,R.LC,F.LC,E41)
            CUR = R.LC<TF.LC.LC.CURRENCY>
            Y.AMOUNT.FCY1 = R.LC<TF.LC.LIABILITY.AMT>
            MARG.AMT      = R.LC<TF.LC.PRO.OUT.AMOUNT>
            REL.AMT       = R.LC<TF.LC.PRO.AWAIT.REL>
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 92 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            Y.AMOUNT.LCY1 += Y.AMOUNT.FCY1 * RATE
            MARG.AMT1     += MARG.AMT * RATE
            REL.AMT1      += REL.AMT  * RATE
            Y.AMOUNT.LCY111 = DROUND(Y.AMOUNT.LCY1,'2')
            MARG.AMTTT    = DROUND(MARG.AMT1,'2')
            REL.AMTTT     = DROUND(REL.AMT1,'2')
            DRW.AMTTTTT   = ''
        NEXT X
        NET.MARG = MARG.AMTTT - REL.AMTTT
        Y.AMOUNT.LCY1 = Y.AMOUNT.LCY111 - NET.MARG
    END
********************  DRAWINGS  *****************************************************
    T.SEL1 = "SELECT FBNK.DRAWINGS WITH CUSTOMER.LINK UNLIKE 99...  AND MATURITY.REVIEW GE ":TD:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    FOR H = 1 TO SELECTED1
        CALL F.READ(FN.DRW,KEY.LIST1<H>,R.DRW,F.DRW,MSG.LC)
        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
        CUR.DRW = R.DRW<TF.DR.DRAW.CURRENCY>
*Line [ 113 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE CUR.DRW IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS.DR ELSE NULL
        RATEE = R.CCY<RE.BCP.RATE,POS.DR>

        NET.DRW.AMTT = R.DRW<TF.DR.DOCUMENT.AMOUNT>
        PROV.AMT = R.DRW<TF.DR.PROV.REL.LC.CCY>

        DRW.AMT.LCY  +=  NET.DRW.AMTT * RATEE
        PROV.AMT.LCY += PROV.AMT * RATEE

    NEXT H

    NET.AMT = DRW.AMT.LCY - PROV.AMT.LCY
    DRW.AMT = DROUND(NET.AMT,'2')

    XX1 = SPACE(120) ; XX2 = SPACE(120)
    XX3 = SPACE(120) ; XX4 = SPACE(120)
    XX5 = SPACE(120) ; XX6 = SPACE(120)
    XX7 = SPACE(120)

    XX1<1,1>[1,50]   = "����� ���� ����� ��������� ������ ������"
    XX2<1,1>[1,50]   = "����� ������� ����� �������� �����"
    XX3<1,1>[1,50]   = "������� �������"
    XX4<1,1>[1,50]   = "������� ��������"
    XX5<1,1>[1,50]   = "������ ����"
    XX6<1,1>[1,50]   = "������� �������"
    XX7<1,1>[1,50]   = "������� ��������"
    XX6<1,1>[25,50]  = DRW.AMT
    PRINT XX1<1,1>
    PRINT STR('*',50)
    PRINT XX2<1,1>
    PRINT STR('-',50)
    PRINT XX3<1,1>
    PRINT STR(' ',50)
    PRINT XX4<1,1>
    PRINT STR(' ',50)
    PRINT XX5<1,1>
    PRINT STR('-',50)
    PRINT XX6<1,1>
    PRINT STR(' ',50)
    PRINT XX7<1,1>
    PRINT STR('=',50)

    XX9  = SPACE(120)   ;  XX16  = SPACE(120)
    XX10 = SPACE(120) ;  XX17  = SPACE(120)
    XX11 = SPACE(120) ;  XX18  = SPACE(120)
    XX12 = SPACE(120) ;  XX19  = SPACE(120)
    XX13 = SPACE(120) ;  XX20  = SPACE(120)
    XX14 = SPACE(120) ;  XX21  = SPACE(120)
    XX22 = SPACE(120) ;  XX23 = SPACE(120)
    XX24 = SPACE(120) ;  XX25 = SPACE(120)
    XX26 = SPACE(120) ;  XX15 = SPACE(120)

    XX8<1,1>[1,50]   = "�������� ������� - �������� �������"
    XX9<1,1>[1,50]   = "����� ������� ����� �������� �����"
    XX10<1,1>[1,50]  = "������� �������"
    XX11<1,1>[1,50]  = "������� ��������"
    XX12<1,1>[1,50]  = "������ ����"
    XX13<1,1>[1,50]  = "������� �������"
    XX13<1,1>[25,50] = ''
    XX14<1,1>[1,50]  = "������� �������"
    XX14<1,1>[25,50] =  Y.AMOUNT.LCY1

    PRINT XX8<1,1>
    PRINT STR('-',50)
    PRINT XX9<1,1>
    PRINT STR('-',50)
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT STR(' ',50)
    PRINT XX12<1,1>
    PRINT STR('-',50)
    PRINT XX13<1,1>
    PRINT STR(' ',50)
    PRINT XX14<1,1>
    PRINT STR('=',50)

    XX15<1,1>[1,50]   = "�������� �����"
    XX16<1,1>[1,50]   = "���� ����� ���� ����� ������� ��������� ��������"
    XX17<1,1>[1,50]   = "���� ����� ���� ���� : �������� ���� ���"
    XX18<1,1>[1,50]   = "�������� ���� �� ���"
    XX19<1,1>[1,50]   = "������ ����"
    XX20<1,1>[1,50]   = "����� ������� ����� �������� �����"
    XX21<1,1>[1,50]   = "������� �������"
    XX22<1,1>[1,50]   = "������� ��������"
    XX23<1,1>[1,50]   = "������ ����"
    XX24<1,1>[1,50]   = "������ ���� ����� ���� ��� ��� ������� �� ����"
    XX25<1,1>[1,50]   = "���� ����� ���� ����� ������� ��������� ��������"
    XX26<1,1>[1,50]   = "���� ����� ���� ����"

    PRINT XX15<1,1>
    PRINT STR('*',50)
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT STR('=',50)
    PRINT XX19<1,1>
    PRINT STR('*',50)
    PRINT XX20<1,1>
    PRINT STR('-',50)
    PRINT XX21<1,1>
    PRINT STR(' ',50)
    PRINT XX22<1,1>
    PRINT STR(' ',50)
    PRINT XX23<1,1>
    PRINT STR('-',50)
    PRINT XX24<1,1>
    PRINT XX25<1,1>
    PRINT XX26<1,1>
*------------------------------------------------
*************************************************************************

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� ����������� ������� ���� ������ ������"
    PR.HD :="'L'":SPACE(50):"����� ���� ��� ���� ��� �������� ������ : ":T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',50)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
