* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
* <Rating>-9</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.CUS.CR.CREATE

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STAT.CR.CUS.TOTAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
** --------------------------------------------------------

    OPENSEQ "CREDIT.STATIC" , "SBM.CREDIT.STATIC.TOT.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"CREDIT.STATIC":' ':"SBM.CREDIT.STATIC.TOT.CSV"
        HUSH OFF
    END
    OPENSEQ "CREDIT.STATIC" , "SBM.CREDIT.STATIC.TOT.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBM.CREDIT.STATIC.TOT.CSV CREATED IN CREDIT.STATIC'
        END ELSE
            STOP 'Cannot create SBM.CREDIT.STATIC.TOT.CSV File IN CREDIT.STATIC'
        END
    END

    DATD.CURR = TODAY
    DATM.CURR = DATD.CURR[1,6]
    DAT.CURR  = DATM.CURR:"01"
    CALL CDT ('',DAT.CURR,'-1W')

    DATM.LAST = DAT.CURR[1,6]
    DAT.LAST  = DATM.LAST:"01"
    CALL CDT ('',DAT.LAST,'-1W')

    TT = FMT(DAT.CURR,"####/##/##")
    KK = FMT(DAT.LAST,"####/##/##")
    HEAD.DESC = ",":TT:" ":KK:" ":"���� ����� ������ ����� �������� �� ����"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "�":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� �������":","
    HEAD.DESC := "��������":",,,"
    HEAD.DESC := "��������� ":KK:",,,"
    HEAD.DESC := "��������� ":TT:",,,"
    HEAD.DESC := "������":","
    HEAD.DESC := "���� ������":","
    HEAD.DESC := "���� �������� ������":","
*    HEAD.DESC := "����� ��������":","
*    HEAD.DESC := "���� ������� ������":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**---------------------------------------------------------

    FN.SCC = 'F.SCB.STATIC.CREDIT.CUSTOMER' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)


    FN.SCC.T = 'F.SCB.STAT.CR.CUS.TOTAL' ; F.SCC.T = ''  ; R.SCC.T = ''
    CALL OPF(FN.SCC.T,F.SCC.T)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""





    GOSUB  CREATE.REC
*    GOSUB  GET.DATA
    RETURN
** --------------------------------------------------------

GET.DATA:
*DEBUG
    T.SEL.CRT = "SELECT ":FN.SCC.T:" WITH DATE EQ ":DAT.CURR: " BY TOT.USED.ALL "
*    T.SEL.CRT = "SELECT ":FN.SCC.T:" WITH DATE EQ ":DAT.CURR: " AND GROUP.NO EQ '990045' BY TOT.USED.LCY "
    CALL EB.READLIST(T.SEL.CRT,KEY.LIST.CRT,"",SELECTED.CRT,ER.MSG.CRT)
    IF SELECTED.CRT THEN
        FOR II = 1 TO SELECTED.CRT

*DEBUG

            CALL F.READ(FN.SCC.T,KEY.LIST.CRT<II>,R.SCC.T,F.SCC.T,E1.T)
            CUS.ID = FIELD(KEY.LIST.CRT<II>,'-',1)
            KEY.LIST.LAST = CUS.ID:'-':DAT.LAST[1,6]
            PRINT KEY.LIST.LAST
            CALL F.READ(FN.SCC.T,KEY.LIST.LAST,R.SCC.LAST,F.SCC.T,E1.LAST)
**---------------


            GROUP.ID = R.SCC.T<CUS.CR.GROUP.NO>

            CALL DBR ('SCB.CUSTOMER.GROUP':@FM:CG.GROUP.NAME,GROUP.ID,CUST.NAME)

            IF CUST.NAME EQ '' THEN
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            END ELSE
                CUS.ID = GROUP.ID
            END

            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,COMP.ID)
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)

**---------------
            WS.DIR.INTERNAL.AMT        = R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.ALL>
            WS.INDIR.INTERNAL.AMT      = R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.ALL>
            TOTAL.INTERNAL             = R.SCC.T<CUS.CR.TOT.INT.ALL>
            WS.DIR.USED.AMT.LAST       = R.SCC.LAST<CUS.CR.TOT.DIR.AMT.USED.ALL>
            WS.INDIR.USED.AMT.LAST     = R.SCC.LAST<CUS.CR.TOT.INDIR.AMT.USED.ALL>
            TOTAL.USED.LAST            = R.SCC.LAST<CUS.CR.TOT.USED.ALL>
            WS.DIR.USED.AMT            = R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.ALL>
            WS.INDIR.USED.AMT          = R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.ALL>
            TOTAL.USED                 = R.SCC.T<CUS.CR.TOT.USED.ALL>


            WS.CHANGE     = ( TOTAL.USED - TOTAL.USED.LAST )
            GOSUB DATA.FILE


        NEXT II
    END
    RETURN

**-----------------------------------

**-----------------------------------
******************* WRITE DATA IN FILE **********************
DATA.FILE:


***** DIVIDED BY 1,000 *****
    WS.DIR.INTERNAL.AMT    = WS.DIR.INTERNAL.AMT    / 1000
    WS.INDIR.INTERNAL.AMT  = WS.INDIR.INTERNAL.AMT  / 1000
    TOTAL.INTERNAL         = TOTAL.INTERNAL         / 1000
    WS.DIR.USED.AMT.LAST   = WS.DIR.USED.AMT.LAST   / 1000
    WS.INDIR.USED.AMT.LAST = WS.INDIR.USED.AMT.LAST / 1000
    TOTAL.USED.LAST        = TOTAL.USED.LAST        / 1000
    WS.DIR.USED.AMT        = WS.DIR.USED.AMT        / 1000
    WS.INDIR.USED.AMT      = WS.INDIR.USED.AMT      / 1000
    TOTAL.USED             = TOTAL.USED             / 1000
    WS.CHANGE              = WS.CHANGE              / 1000
*WS.USED.AVG            = WS.USED.AVG            / 1000

    WS.DIR.INTERNAL.AMT    = DROUND(WS.DIR.INTERNAL.AMT,'0')
    WS.INDIR.INTERNAL.AMT  = DROUND(WS.INDIR.INTERNAL.AMT,'0')
    TOTAL.INTERNAL         = DROUND(TOTAL.INTERNAL,'0')
    WS.DIR.USED.AMT.LAST   = DROUND(WS.DIR.USED.AMT.LAST,'0')
    WS.INDIR.USED.AMT.LAST = DROUND(WS.INDIR.USED.AMT.LAST,'0')
    TOTAL.USED.LAST        = DROUND(TOTAL.USED.LAST,'0')
    WS.DIR.USED.AMT        = DROUND(WS.DIR.USED.AMT,'0')
    WS.INDIR.USED.AMT      = DROUND(WS.INDIR.USED.AMT,'0')
    TOTAL.USED             = DROUND(TOTAL.USED,'0')
    WS.CHANGE              = DROUND(WS.CHANGE,'0')
*WS.USED.AVG            = DROUND(WS.USED.AVG,'0')

**************** CALC PERCENTAGE ****************
    IF TOTAL.USED.LAST NE 0 THEN
        WS.CHANGE.PER        = ( WS.CHANGE / TOTAL.USED.LAST ) * 100
    END
    IF TOTAL.INTERNAL NE 0 THEN
        WS.PER.USED.INTERNAL = ( TOTAL.USED / TOTAL.INTERNAL ) * 100
    END
*    IF TOTAL.INTERNAL NE 0 THEN
*        WS.PER.AVG.INTERNAL  = ( WS.USED.AVG / TOTAL.INTERNAL ) * 100
*    END

    WS.CHANGE.PER1          = DROUND(WS.CHANGE.PER,'0')
    WS.PER.USED.INTERNAL1   = DROUND(WS.PER.USED.INTERNAL,'0')
*WS.PER.AVG.INTERNAL1    = DROUND(WS.PER.AVG.INTERNAL,'0')

    WS.CHANGE.PER          = ABS (WS.CHANGE.PER1)
    WS.PER.USED.INTERNAL   = ABS (WS.PER.USED.INTERNAL1)
* WS.PER.AVG.INTERNAL    = ABS (WS.PER.AVG.INTERNAL1)

***************************************************
    TOT.PRINT = ABS (TOTAL.USED) + ABS (TOTAL.USED.LAST) + TOTAL.INTERNAL
    IF TOT.PRINT NE 0 AND TOT.PRINT NE '' THEN

*DEBUG
        KK++
        BB.DATA  = KK:","
        BB.DATA := CUST.NAME:","
        BB.DATA := CUS.ID:","
        BB.DATA := BRANCH.NAME:","

        BB.DATA := WS.DIR.INTERNAL.AMT:","
        BB.DATA := WS.INDIR.INTERNAL.AMT:","
        BB.DATA := TOTAL.INTERNAL:","
        BB.DATA := WS.DIR.USED.AMT.LAST:","
        BB.DATA := WS.INDIR.USED.AMT.LAST:","
        BB.DATA := TOTAL.USED.LAST:","
        BB.DATA := WS.DIR.USED.AMT:","
        BB.DATA := WS.INDIR.USED.AMT:","
        BB.DATA := TOTAL.USED:","
        BB.DATA := WS.CHANGE:","
        BB.DATA := WS.CHANGE.PER:","
        BB.DATA := WS.PER.USED.INTERNAL:","
*BB.DATA := WS.USED.AVG:","
*BB.DATA := WS.PER.AVG.INTERNAL:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
        PRINT BB

    END

    WS.DIR.INTERNAL.AMT.TOT    += WS.DIR.INTERNAL.AMT
    WS.INDIR.INTERNAL.AMT.TOT  += WS.INDIR.INTERNAL.AMT
    TOTAL.INTERNAL.TOT         += TOTAL.INTERNAL
    WS.DIR.USED.AMT.TOT        += WS.DIR.USED.AMT
    WS.INDIR.USED.AMT.TOT      += WS.INDIR.USED.AMT
    TOTAL.USED.TOT             += TOTAL.USED
    WS.DIR.USED.AMT.LAST.TOT   += WS.DIR.USED.AMT.LAST
    WS.INDIR.USED.AMT.LAST.TOT += WS.INDIR.USED.AMT.LAST
    TOTAL.USED.LAST.TOT        += TOTAL.USED.LAST
    WS.CHANGE.TOT     += WS.CHANGE
    WS.CHANGE.PER.TOT += WS.CHANGE.PER
*WS.USED.AVG.TOT   += WS.USED.AVG
    WS.PER.USED.INTERNAL.TOT += WS.PER.USED.INTERNAL
* WS.PER.AVG.INTERNAL.TOT  += WS.PER.AVG.INTERNAL

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.DIR.USED.AMT.LAST   = 0
    WS.INDIR.USED.AMT.LAST = 0
    TOTAL.USED.LAST        = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.CHANGE              = 0
    WS.CHANGE.PER          = 0
    WS.PER.USED.INTERNAL   = 0
*WS.USED.AVG            = 0
*WS.PER.AVG.INTERNAL    = 0

    RETURN


** --------------------------------------------------------

CREATE.REC:

    T.SEL = "SELECT ":FN.SCC:" WITH LPE.DATE EQ ":DAT.CURR:" BY GROUP.ID "
**    T.SEL = "SELECT ":FN.SCC:" WITH LPE.DATE LIKE ...20151129... BY GROUP.ID "
*DEBUG
*    T.SEL = "SELECT ":FN.SCC:" WITH LPE.DATE LIKE ...20151129... AND GROUP.ID EQ 990068 "


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
****----------------

            T.WS.DIR.INTERNAL.AMT         = 0
            T.WS.INDIR.INTERNAL.AMT       = 0
            T.TOTAL.INTERNAL              = 0

            T.WS.DIR.USED.AMT             = 0
            T.WS.INDIR.USED.AMT           = 0
            T.TOTAL.USED                  = 0

            TF.WS.DIR.INTERNAL.AMT        = 0
            TF.WS.INDIR.INTERNAL.AMT      = 0
            TF.TOTAL.INTERNAL             = 0

            TF.WS.DIR.USED.AMT            = 0
            TF.WS.INDIR.USED.AMT          = 0
            TF.TOTAL.USED                 = 0

            ALL.WS.DIR.INTERNAL.AMT       = 0
            ALL.WS.INDIR.INTERNAL.AMT     = 0
            ALL.TOTAL.INTERNAL            = 0


            ALL.WS.DIR.USED.AMT           = 0
            ALL.WS.INDIR.USED.AMT         = 0
            ALL.TOTAL.USED                = 0
            CALL F.READ(FN.SCC,KEY.LIST<I>,R.SCC,F.SCC,E1)

            CUS.ID = R.SCC<SCC.CUSTOMER.ID>
            GRP.ID = R.SCC<CUS.CR.GROUP.NO>

            IF ( GRP.ID EQ '' OR GRP.ID[1,3] EQ '999' ) THEN

                GRP.ID = CUS.ID
            END

*PRINT  GRP.ID
            CUS.ID.TOT = GRP.ID :'-': TODAY[1,6]
            CALL F.READ(FN.SCC.T,CUS.ID.TOT,R.SCC.T,F.SCC.T,E1.T)
**---------------


* CUS.ID = R.SCC.T<CUS.CR.GROUP.NO>
            GROUP.ID = R.SCC.T<CUS.CR.GROUP.NO>

            CALL DBR ('SCB.CUSTOMER.GROUP':@FM:CG.GROUP.NAME,GROUP.ID,CUST.NAME)

            IF CUST.NAME EQ '' THEN
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            END ELSE
                CUS.ID = GROUP.ID
            END

            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,COMP.ID)
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)
**---------------
            WS.CY  = R.SCC<SCC.CURRENCY>
*Line [ 373 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CY.DC  = DCOUNT(WS.CY,@VM)
            FOR X = 1 TO CY.DC
                WS.CY.ID = R.SCC<SCC.CURRENCY,X>
                GOSUB GET.RATE

*************** CURR INTERNAL AND USED*******************************

                WS.DIR.INTERNAL.AMT    = R.SCC<SCC.DIR.INTERNAL.AMT,X>   * WS.RATE
                WS.INDIR.INTERNAL.AMT  = R.SCC<SCC.INDIR.INTERNAL.AMT,X> * WS.RATE
                TOTAL.INTERNAL         = R.SCC<SCC.TOTAL.INTERNAL.AMT,X> * WS.RATE
                WS.DIR.USED.AMT        = R.SCC<SCC.DIR.USED.AMT,X>       * WS.RATE
                WS.INDIR.USED.AMT      = R.SCC<SCC.INDIR.USED.AMT,X>     * WS.RATE
                TOTAL.USED             = R.SCC<SCC.TOTAL.USED.AMT,X>     * WS.RATE

                IF WS.CY.ID EQ 'EGP' THEN
                    T.WS.DIR.INTERNAL.AMT      = WS.DIR.INTERNAL.AMT   + T.WS.DIR.INTERNAL.AMT
                    T.WS.INDIR.INTERNAL.AMT    = WS.INDIR.INTERNAL.AMT + T.WS.INDIR.INTERNAL.AMT
                    T.TOTAL.INTERNAL           = TOTAL.INTERNAL        + T.TOTAL.INTERNAL

                    T.WS.DIR.USED.AMT          = WS.DIR.USED.AMT       + T.WS.DIR.USED.AMT
                    T.WS.INDIR.USED.AMT        = WS.INDIR.USED.AMT     + T.WS.INDIR.USED.AMT
                    T.TOTAL.USED               = TOTAL.USED            + T.TOTAL.USED
                END ELSE
                    TF.WS.DIR.INTERNAL.AMT     = WS.DIR.INTERNAL.AMT   + TF.WS.DIR.INTERNAL.AMT
                    TF.WS.INDIR.INTERNAL.AMT   = WS.INDIR.INTERNAL.AMT + TF.WS.INDIR.INTERNAL.AMT
                    TF.TOTAL.INTERNAL          = TOTAL.INTERNAL        + TF.TOTAL.INTERNAL

                    TF.WS.DIR.USED.AMT         = WS.DIR.USED.AMT       + TF.WS.DIR.USED.AMT
                    TF.WS.INDIR.USED.AMT       = WS.INDIR.USED.AMT     + TF.WS.INDIR.USED.AMT
                    TF.TOTAL.USED              = TOTAL.USED            + TF.TOTAL.USED
                END

                ALL.WS.DIR.INTERNAL.AMT        = WS.DIR.INTERNAL.AMT   + ALL.WS.DIR.INTERNAL.AMT
                ALL.WS.INDIR.INTERNAL.AMT      = WS.INDIR.INTERNAL.AMT + ALL.WS.INDIR.INTERNAL.AMT
                ALL.TOTAL.INTERNAL             = TOTAL.INTERNAL        + ALL.TOTAL.INTERNAL

                ALL.WS.DIR.USED.AMT            = WS.DIR.USED.AMT       +  ALL.WS.DIR.USED.AMT
                ALL.WS.INDIR.USED.AMT          = WS.INDIR.USED.AMT     +  ALL.WS.INDIR.USED.AMT
                ALL.TOTAL.USED                 = TOTAL.USED            +  ALL.TOTAL.USED
            NEXT X

**-------------------------

            R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.LCY>     = T.WS.DIR.INTERNAL.AMT       + R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.LCY>
            R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.LCY>   = T.WS.INDIR.INTERNAL.AMT     + R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.LCY>
            R.SCC.T<CUS.CR.TOT.INT.LCY>             = T.TOTAL.INTERNAL            + R.SCC.T<CUS.CR.TOT.INT.LCY>

            R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.LCY>    = T.WS.DIR.USED.AMT           + R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.LCY>
            R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.LCY>  = T.WS.INDIR.USED.AMT         + R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.LCY>
            R.SCC.T<CUS.CR.TOT.USED.LCY>            = T.TOTAL.USED                + R.SCC.T<CUS.CR.TOT.USED.LCY>

            R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.FCY>     = TF.WS.DIR.INTERNAL.AMT      + R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.FCY>
            R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.FCY>   = TF.WS.INDIR.INTERNAL.AMT    + R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.FCY>
            R.SCC.T<CUS.CR.TOT.INT.FCY>             = TF.TOTAL.INTERNAL           + R.SCC.T<CUS.CR.TOT.INT.FCY>

            R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.FCY>    = TF.WS.DIR.USED.AMT          + R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.FCY>
            R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.FCY>  = TF.WS.INDIR.USED.AMT        + R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.FCY>
            R.SCC.T<CUS.CR.TOT.USED.FCY>            = TF.TOTAL.USED               + R.SCC.T<CUS.CR.TOT.USED.FCY>

            R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.ALL>     = ALL.WS.DIR.INTERNAL.AMT     + R.SCC.T<CUS.CR.TOT.DIR.AMT.INT.ALL>
            R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.ALL>   = ALL.WS.INDIR.INTERNAL.AMT   + R.SCC.T<CUS.CR.TOT.INDIR.AMT.INT.ALL>
            R.SCC.T<CUS.CR.TOT.INT.ALL>             = ALL.TOTAL.INTERNAL          + R.SCC.T<CUS.CR.TOT.INT.ALL>

            R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.ALL>    = ALL.WS.DIR.USED.AMT         + R.SCC.T<CUS.CR.TOT.DIR.AMT.USED.ALL>
            R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.ALL>  = ALL.WS.INDIR.USED.AMT       + R.SCC.T<CUS.CR.TOT.INDIR.AMT.USED.ALL>
            R.SCC.T<CUS.CR.TOT.USED.ALL>            = ALL.TOTAL.USED              + R.SCC.T<CUS.CR.TOT.USED.ALL>
***------------------------------
            R.SCC.T<CUS.CR.DATE>                = DAT.CURR
            R.SCC.T<CUS.CR.GROUP.NO>            = GRP.ID
*--------------
            CALL F.WRITE(FN.SCC.T ,CUS.ID.TOT, R.SCC.T)
            CALL JOURNAL.UPDATE(CUS.ID.TOT)


            R.SCC.T = ''
        NEXT I
    END

    RETURN
**** ---------------------- GET SELL.RATE FROM CURRENCY FILE -------------------- ****
GET.RATE:
    IF  WS.CY.ID NE 'EGP' THEN
        CALL F.READ(FN.CCY,WS.CY.ID,R.CCY,F.CCY,E1)
        WS.RATE = R.CCY<EB.CUR.SELL.RATE,1>
        IF WS.CY.ID EQ 'JPY' THEN
            WS.RATE = ( WS.RATE / 100 )
        END
    END ELSE
        WS.RATE = 1
    END
    RETURN
*------------------------------------------------------------------

    RETURN
END
