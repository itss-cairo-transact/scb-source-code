* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/05/12  ***
********************************************
*    SUBROUTINE SBM.HNDOF.MOD
    PROGRAM SBM.HNDOF.MOD

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS


** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

    WS.XWAIT = ''
    WS.XCON  = ''
****

*---------------------------------------------------------------------------------------------------------
    EXECUTE "cp ../bnk.data/ac/FBNK.AC.S001 ../bnk.data/ac/FBNK.AC.S001.BFR.UPD"

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"DEL.NO.ACTIVE.CUST")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.HNDOF.CHK")

    EXECUTE "cp ../bnk.data/ac/FBNK.AC.S001 ../bnk.data/ac/FBNK.AC.S001.AFT.UPD"

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SCB.POST.LIST.FILE")

    EXECUTE "CLEAR-FILE REP"
    EXECUTE "CLEAR-FILE STMT.DIFF"

*    EXECUTE "COPY FROM REP TO &SAVEDLISTS& ALL OVERWRITING"

    RETURN
*-----------------------------------------------
