* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** ���� �������� �������� ����� ����� ***
*** CREATED BY KHALED ***
***=================================
    SUBROUTINE SBM.INTER.MONTH

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 45 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.INTER.MONTH'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.CLOSE.BAL = 0
    COMP = ID.COMPANY
    YTEXT = "Enter The Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    TD1 = COMI
    RETURN
*===============================================================
CALLDB:
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''

    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    DAT.ID = COMP
*************************************************************************
    T.SEL = "SELECT ":FN.LN: " WITH @ID EQ SCBPLFIN.0070 OR @ID EQ SCBPLFIN.0080 OR @ID EQ SCBPLFIN.0081 OR @ID EQ SCBPLFIN.0090 OR @ID EQ SCBPLFIN.0091 OR @ID EQ SCBPLFIN.0100 OR @ID EQ SCBPLFIN.0195"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC  = R.LN<RE.SRL.DESC,1,1>
            Y.DESC2 = R.LN<RE.SRL.DESC,2,1>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 88 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            FOR X = 1 TO POS
                BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":COMP
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                IF NOT(E2) THEN
                    RATE = R.CCY<RE.BCP.RATE,X>
                    CUR = BAL.ID[15,3]

                    IF CUR NE 'EGP' THEN
** CR.MVMT = R.BAL<RE.SLB.CR.MVMT.MTH>
** DR.MVMT = R.BAL<RE.SLB.DB.MVMT.MTH>

                        Y.CLOSE.BAL = R.BAL<RE.SLB.CLOSING.BAL>
                        Y.CLOSE.BAL = DROUND(Y.CLOSE.BAL,'2')
                        IF Y.CLOSE.BAL NE '' THEN
                            XX  = SPACE(120)
                            XX<1,1>[1,35]   = Y.DESC
                            XX<1,1>[15,35]  = Y.DESC2
                            XX<1,1>[55,15]  = Y.CLOSE.BAL
                            XX<1,1>[85,15]  = CUR
                            PRINT XX<1,1>
                            PRINT STR(' ',120)
                        END
                    END
                END
            NEXT X
*************************************************************************
            Y.DESC = ''
            Y.CLOSE.BAL = 0
        NEXT I
    END
    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  ����� �������  ***'
    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT XX25<1,1>

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

    T.DAY1  = TD1[7,2]:'/':TD1[5,2]:"/":TD1[1,4]

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� �������� �������� �� ���  :  ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������" :SPACE(40):"������ �������":SPACE(20):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
