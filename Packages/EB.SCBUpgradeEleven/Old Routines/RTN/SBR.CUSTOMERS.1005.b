* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
****MAI SAAD 15/05/2021***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.CUSTOMERS.1005
*   PROGRAM SBR.CUSTOMERS.1005

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMP.LOCAL.REFS


    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; RETRY = '' ; ERR.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''  ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC = ''
    CALL OPF(FN.CU.AC,F.CU.AC)
**********OPEN FILE********************************
    DIRC = "&SAVEDLISTS&"
    FILE1 = "CUS.ONLY.1005.CSV"
    FILE2 = "CUS.WITH.1005.CSV"
    OPENSEQ DIRC , FILE1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':DIRC:' ':FILE1
        HUSH OFF
    END
    OPENSEQ DIRC , FILE1 TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE1 CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create File1 IN &SAVEDLISTS&'
        END
    END
    OPENSEQ DIRC , FILE2 TO BB2 THEN
        CLOSESEQ BB2
        HUSH ON
        EXECUTE 'DELETE ':DIRC:' ':FILE2
        HUSH OFF
    END
    OPENSEQ DIRC , FILE2 TO BB2 ELSE
        CREATE BB2 THEN
            PRINT 'FILE2 CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create File2 IN &SAVEDLISTS&'
        END
    END
*****************************************************************
    BB.DATA  = "Customer":",":"Name":",":"Account":",":"Category":",":"Branch":",":"Balance"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE1 "
    END
    WRITESEQ BB.DATA TO BB2 ELSE
        PRINT " ERROR WRITE FILE2 "
    END
    BB.DATA = '';
    AC.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ '1005' BY CO.CODE "

    KEY.LIST.AC = '' ; SELECTED.AC = '' ; ER.MSG.AC = ''
    CALL EB.READLIST(AC.SEL , KEY.LIST.AC,'',SELECTED.AC,ER.MSG.AC)

*PRINT 'SELECTED':SELECTED.AC
    IF SELECTED.AC THEN
        KEY.LIST.CAC = '' ; SELECTED.CUS.AC = '' ; ER.MSG.CUS.AC = ''; R.CU.AC=''
        CUS.NAME=''; CUS.AC= '';ACC.NO = '' ;WORK.BAL='';BRANCH='';CATEG=''; CUS.ID='';
        FOR I = 1 TO SELECTED.AC
            CALL F.READ(FN.ACC,KEY.LIST.AC<I>, R.AC, F.ACC ,E3)
            CUS.ID<I> = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID<I>, R.CU, F.CU ,ERR.CU)
            CUS.NAME = R.CU<EB.CUS.NAME.1,1>
            CUS.NAME = TRIM(CUS.NAME, ',', 'A')
*           CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,CUS.ID<I> , CUS.NAME)
******CUSTOMER.ACCOUNT
            CALL F.READ(FN.CU.AC,CUS.ID<I>,R.CU.AC, F.CU.AC ,E51)
            COUNTER=0
            LOOP
                REMOVE ACC.ID FROM R.CU.AC SETTING SUBVAL
            WHILE ACC.ID:SUBVAL
                COUNTER = COUNTER +1
                CUS.ACC = ACC.ID
                CALL F.READ(FN.ACC,CUS.ACC,R.ACC, F.ACC ,ERR.ACC)
                CATEG = R.ACC<AC.CATEGORY>
                IF CATEG EQ '1002' THEN
                    WORK.BAL = 'NOT ALLOWED'
                END ELSE
                    WORK.BAL = R.ACC<AC.WORKING.BALANCE>
                END
                BRANCH = R.ACC<AC.CO.CODE>
                IF COUNTER GT '1' THEN
                    WRITESEQ BB.DATA TO BB2 ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
                BB.DATA = CUS.ID<I>:",":CUS.NAME:",":CUS.ACC:",":CATEG:",":BRANCH:",":WORK.BAL

            REPEAT
            IF COUNTER EQ '1' THEN

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

            END ELSE
                IF COUNTER GT '1' THEN
                    WRITESEQ BB.DATA TO BB2 ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            END


        NEXT I
    END   ;***End of main selection from Customer***
    TEXT="PROGRAM IS FINISHED"; CALL REM;
    RETURN
*----
END
