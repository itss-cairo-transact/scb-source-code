* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBM.CUS.ALL.EXCL

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.DATES
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT           I_AC.LOCAL.REFS
    $INSERT           I_CU.LOCAL.REFS
    $INSERT           I_F.SCB.TOPCUS.CR.TOT.LW
*---------------------------------------------------------------------
    SHIFT ='' ; ROUND =''
    DEFFUN SHIFT.DATE(STA.DATE, SHIFT, ROUND)

    VAR.TOD = TODAY

    YTEXT = "Enter.From.Date : "
    CALL TXTINP(YTEXT, 10, 10, "12", "A")
    COMI.FF = COMI

    YTEXT = "Enter.End.Date : "
    CALL TXTINP(YTEXT, 10, 10, "12", "A")
    COMI.EE = COMI

    TODAY   = COMI.EE
*****
    OPENSEQ "&SAVEDLISTS&" , "CUSTOMER.EXCEL" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE file'
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CUSTOMER.EXCEL" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ' :'CUSTOMER.EXCEL CREATED'
        END ELSE
            STOP 'Cannot create File '
        END
    END
*****
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS.OPEN
    GOSUB PRINT.ARRAY
    GOSUB LINE.END
    RETURN
*----------------------------- INITIALIZATIONS ------------------------
PRINT.ARRAY:
    FOR ROW.ARR = 1 TO ZZ-1
        XX.LINE = ARRAY.LINE<1,ROW.ARR>
        WRITESEQ XX.LINE TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    NEXT ROW.ARR
    RETURN
*---------------------------------------------------------------------
LINE.END:
    LINE.HD = "��������" : ",":","
    LINE.HD := TOT.4 : ","  : TOT.5  : ","
    LINE.HD := TOT.1 : ","  : TOT.2  : ","
    LINE.HD := TOT.3 : ","

    WRITESEQ LINE.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*----------------------------------------------
    WW = SPACE(100)
    XX.LINE =",": "**��� ������� ��� �� �� ����� ����� ��� ������ **"
    WRITESEQ XX.LINE TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    XX.LINE =",": "** ��� ������� �� ����� �������� �������� ��� �� �� ����� ����� ��� ������"
    XX.DD   = FMT(VAR.TOD, "####/##/##")
    XX.LINE = XX.LINE : SPACE(2) : XX.DD
    WRITESEQ XX.LINE TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    WW<1,1> = ",,,":"********************  ����� �������   ******************"
    WRITESEQ WW<1,1> TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*--------------------------------------------------------------------
INITIATE:
    TOT.1 = 0
    TOT.2 = 0
    TOT.3 = 0
    TOT.4 = 0
    TOT.5 = 0

    ZZ = 1
    HH = 1
    ALL.BAL.NEW     = 0
    ALL.BAL.NEW.FCY = 0
    ARRAY.LINE = SPACE(150)

    FN.CBE = "F.SCB.TOPCUS.CR.TOT.LW"     ; F.CBE = ""
    CALL OPF(FN.CBE,F.CBE)

    FN.CUS = 'FBNK.CUSTOMER'              ; F.CUS = ""
    CALL OPF( FN.CUS,F.CUS)

    FN.CUS.H = 'FBNK.CUSTOMER$HIS'         ; F.CUS.H = ""
    CALL OPF(FN.CUS.H,F.CUS.H)

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    T.DATE    = TODAY
    KEY.CCY   = 'NZD'
    DDD       = TODAY[1,6]:'01'
*-----
    START.DATE   = COMI.FF
    END.DATE     = COMI.EE
    END.DATE1    = TODAY

    COMP.COUNT.2 = 0
    INDV.COUNT.2 = 0
    RETURN
*------------------------ READ FORM TEXT FILE --------------------
PROCESS.OPEN:
    T.SEL  = " SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":START.DATE
    T.SEL := " AND CONTACT.DATE LE ":END.DATE
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
    T.SEL := " AND TEXT UNLIKE BR... BY COMPANY.BOOK BY CONTACT.DATE"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    OLD.CO.CODE  = ''
    INDV.COUNT   = 0
    COMP.COUNT   = 0
    STA.DATE     = COMI.FF

    FOR I = 1 TO SELECTED-1
        CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
        CALL F.READ(FN.CUS,KEY.LIST<I+1>,R.CUS.1,F.CUS,CUS.ERR)

        COM.CODE    = R.CUS<EB.CUS.COMPANY.BOOK>
        COM.CODE.1  = R.CUS.1<EB.CUS.COMPANY.BOOK>

        CONT.DATE   = R.CUS<EB.CUS.CONTACT.DATE>[1,6]
        CONT.DATE.1 = R.CUS.1<EB.CUS.CONTACT.DATE>[1,6]

        NEW.BREAK   =  COM.CODE   : R.CUS<EB.CUS.CONTACT.DATE>[1,6]
        NEW.BREAK.1 =  COM.CODE.1 : R.CUS.1<EB.CUS.CONTACT.DATE>[1,6]

        IF NEW.BREAK EQ NEW.BREAK.1 THEN
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>

            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END
        END ELSE

            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END

            ALL.COUNT = COMP.COUNT + INDV.COUNT
            OLDD      = COM.CODE[8,2]
            IF OLDD[1,1] EQ '0' THEN
                OLDD = OLDD[1]
            END

            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
            BRANCH.NAME  = FIELD(BRAN.NAME,'.',2)
            CBE.BR       = COM.CODE[8,2]

            GOSUB WRITE.LINE

            COMP.COUNT   = 0
            INDV.COUNT   = 0
**---------------HHH-----------------------
            CURNT.DATE = R.CUS.1<EB.CUS.CONTACT.DATE>
            CONT.DATE   = CONT.DATE : "01"
            CONT.DATE.1 = CONT.DATE.1 : "01"

            IF ( CONT.DATE[1,6] NE CURNT.DATE[1,6] ) AND (CURNT.DATE NE '' ) THEN
                IF CONT.DATE LE CONT.DATE.1 THEN
                    CALL MONTHS.BETWEEN(CONT.DATE,CURNT.DATE,NN.COUNT)
                    FOR III = 1 TO NN.COUNT-1
                        CONT.DATE = SHIFT.DATE(CONT.DATE , III:'M', 'UP')

                        DDD = FIELD(ARRAY.LINE<1,ZZ-1>,',',1)
                        PRINT.DD  = DDD :","
                        PRINT.DD := CONT.DATE[1,4]:"/":CONT.DATE[5,2]:","
                        PRINT.DD := "0,0,0,0,0,,"

                        ARRAY.LINE<1,ZZ> = PRINT.DD
                        ZZ = ZZ + 1
                    NEXT III

                    CONT.DATE = SHIFT.DATE(CONT.DATE, '1M', 'UP')

                    IF COM.CODE NE COM.CODE.1 THEN
                        CONT.DATE = CONT.DATE.1
                    END
                END
            END
**---------------HHH-----------------------
        END
    NEXT I
    RETURN
*-------------------------------------------------------------------*
WRITE.LINE:
    ARRAY.LINE<1,ZZ>[1,30]   = BRANCH.NAME:",":NEW.BREAK[10,4]:"/":NEW.BREAK[14,2]:","
*----
    XX = ALL.BAL.NEW
    ARRAY.LINE<1,ZZ>[30,20]  = XX:","

    YY = ALL.BAL.NEW.FCY
    ARRAY.LINE<1,ZZ>[52,20]  = YY:","
*----
    ARRAY.LINE<1,ZZ>[80,10]  = INDV.COUNT:","
    ARRAY.LINE<1,ZZ>[92,10]  = COMP.COUNT:","
    ARRAY.LINE<1,ZZ>[102,10] = ALL.COUNT:","
*----
    ZZ = ZZ + 1

*%%%%%%% TOTALS %%%%%%%%%%%%%%%
    TOT.1 = TOT.1 + INDV.COUNT
    TOT.2 = TOT.2 + COMP.COUNT
    TOT.3 = TOT.3 + ALL.COUNT
    TOT.4 = TOT.4 + ALL.BAL.NEW
    TOT.5 = TOT.5 + ALL.BAL.NEW.FCY
*%%%%%%% TOTALS %%%%%%%%%%%%%%%

    ALL.BAL.NEW     = 0
    ALL.BAL.NEW.FCY = 0
    RETURN
*------------------------------------------------------------------*
CALC.BALANCE:
    CUST.NUM = CUSTO
*-------------------
    CUSTO    = CUST.NUM : ".LCY"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000
    ALL.BAL.NEW = ALL.BAL.NEW +  CBE.IN.LCY
*----------
    CUSTO    = CUST.NUM : ".FCY"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY.FCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000

    ALL.BAL.NEW.FCY = ALL.BAL.NEW.FCY +  CBE.IN.LCY.FCY
    RETURN
************************ PRINT HEAD *******************************
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = VAR.TOD
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    FROM.DATE.2  = COMI.FF
    END.DATE.2   = COMI.EE
    FROM.DATE.2  = FMT(FROM.DATE.2, "####/##/##")
    END.DATE.2   = FMT(END.DATE.2 , "####/##/##")

    PR.HD  = ",,,,,"
    PR.HD := "�������� �������� ���� ���� "
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD  = ",,":" ��"
    PR.HD := ",": FROM.DATE.2
    PR.HD := ",": "���" : "," : END.DATE.2
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD  ="�����":","
    PR.HD := "���-���":","
    PR.HD := "** ������� ������� ������":","
    PR.HD := "** ����� �������� �����":","
    PR.HD := "����� ������":","
    PR.HD := "����� ������" :","
    PR.HD := "������ �������":","

    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*--------------------------------------------------------
END
