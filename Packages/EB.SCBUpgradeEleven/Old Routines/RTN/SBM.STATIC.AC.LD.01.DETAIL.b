* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBM.STATIC.AC.LD.01.DETAIL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD.A
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL

*********************** OPENING FILES *****************************
    FN.AC  = "F.CBE.STATIC.AC.LD.DETAIL" ; F.AC   = "" ; R.AC = ""
    FN.CBE = "F.CBE.MAST.AC.LD"  ; F.CBE  = ""
    FN.CBE.A = "F.CBE.MAST.AC.LD.A"  ; F.CBE.A  = ""

    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CBE.A,F.CBE.A)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATD = TODAY
    DATM = DATD[1,6]
    DAT  = DATM:"01"
    CALL CDT ('',DAT,'-1C')
    WS.TD = DAT[1,6]

*------------------------------------------------------------------

    T.SEL = "SELECT ":FN.CBE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            AC.LD.ID = KEY.LIST<I>
********************** COPY NEW DATA ******************************

            R.AC<STD.CBEM.CUSTOMER.CODE> = R.CBE<C.CBEM.CUSTOMER.CODE>
            R.AC<STD.CBEM.BR>            = R.CBE<C.CBEM.BR>
            R.AC<STD.CBEM.NEW.SECTOR>    = R.CBE<C.CBEM.NEW.SECTOR>
            R.AC<STD.CBEM.OLD.SECTOR>    = R.CBE<C.CBEM.OLD.SECTOR>
            R.AC<STD.CBEM.NEW.INDUSTRY>  = R.CBE<C.CBEM.NEW.INDUSTRY>
            R.AC<STD.CBEM.CATEG>         = R.CBE<C.CBEM.CATEG>
            R.AC<STD.CBEM.CY>            = R.CBE<C.CBEM.CY>
            R.AC<STD.CBEM.IN.LCY>        = R.CBE<C.CBEM.IN.LCY>
            R.AC<STD.CBEM.IN.FCY>        = R.CBE<C.CBEM.IN.FCY>
            R.AC<STD.CBEM.DATE>          = WS.TD
            CALL F.WRITE(FN.AC,AC.LD.ID,R.AC)
            CALL  JOURNAL.UPDATE(AC.LD.ID)

        NEXT I
    END
********************* INTERNAL ACCOUNTS *****************************
    T.SEL = "SELECT ":FN.CBE.A
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE.A,KEY.LIST<I>,R.CBE.A,F.CBE.A,E1)
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            AC.LD.ID = KEY.LIST<I>
********************** COPY NEW DATA ******************************

            R.AC<STD.CBEM.CUSTOMER.CODE> = R.CBE.A<C.CBEM.CUSTOMER.CODE>
            R.AC<STD.CBEM.BR>            = R.CBE.A<C.CBEM.BR>
            R.AC<STD.CBEM.NEW.SECTOR>    = R.CBE.A<C.CBEM.NEW.SECTOR>
            R.AC<STD.CBEM.OLD.SECTOR>    = R.CBE.A<C.CBEM.OLD.SECTOR>
            R.AC<STD.CBEM.NEW.INDUSTRY>  = R.CBE.A<C.CBEM.NEW.INDUSTRY>
            R.AC<STD.CBEM.CATEG>         = R.CBE.A<C.CBEM.CATEG>
            R.AC<STD.CBEM.CY>            = R.CBE.A<C.CBEM.CY>
            R.AC<STD.CBEM.IN.LCY>        = R.CBE.A<C.CBEM.IN.LCY>
            R.AC<STD.CBEM.IN.FCY>        = R.CBE.A<C.CBEM.IN.FCY>
            R.AC<STD.CBEM.DATE>          = WS.TD
            CALL F.WRITE(FN.AC,AC.LD.ID,R.AC)
            CALL  JOURNAL.UPDATE(AC.LD.ID)

        NEXT I
    END


END
