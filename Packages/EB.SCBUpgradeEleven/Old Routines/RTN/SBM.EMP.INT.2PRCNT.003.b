* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>59</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.EMP.INT.2PRCNT.003
*    PROGRAM SBM.EMP.INT.2PRCNT.003
    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT TEMENOS.BP I_F.SCB.EMP.INT.2.TEMP
*---------------------------------------------------
    FN.TEMP = "F.SCB.EMP.INT.2.TEMP"
    F.TEMP = ""
*

*----------------------------------------------------
    CALL OPF (FN.TEMP,F.TEMP)
    R.TEMP = ""
*------------------------------------------------
    GOSUB A.100.PROC
    RETURN
*-----------------------------------------
A.100.PROC:
    SEL.CMDTEMP = "SELECT ":FN.TEMP:" BY RLAT.NO BY RESERVED02"
    CALL EB.READLIST(SEL.CMDTEMP,SEL.LISTTEMP,"",NO.OF.RECTEMP,RET.CODETEMP)
    LOOP
        REMOVE WS.TEMP.ID FROM SEL.LISTTEMP SETTING POSTEMP
    WHILE WS.TEMP.ID:POSTEMP

        CALL F.READ(FN.TEMP,WS.TEMP.ID,R.TEMP,F.TEMP,MSG.TEMP)
        WS.RELATED.NO = R.TEMP<EMP.RLAT.NO>
        IF  WS.RELATED.NO EQ WS.TEMP.ID THEN
            WS.BAL  = R.TEMP<EMP.TOTAL.AMT>
            WS.COMP =  WS.RELATED.NO
            GOTO A.100.REP
        END
        IF  WS.RELATED.NO EQ WS.COMP THEN
            GOSUB A.300.UPDAT.RELATED
        END

A.100.REP:
    REPEAT
    RETURN
*-------------------------------------------------
A.300.UPDAT.RELATED:
    R.TEMP<EMP.TOTAL.AMT> = WS.BAL

    CALL F.WRITE(FN.TEMP,WS.TEMP.ID,R.TEMP)
  **  CALL  JOURNAL.UPDATE(WS.TEMP.ID)
    RETURN
*--------------------------------------------------
END
