* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-22</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.DEP.DAILY.CHANGE.EGP.CAT
 ** PROGRAM SBM.DEP.DAILY.CHANGE.EGP.AA.CAT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPOSITS.LOANS.DETAILS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
**    REPORT.ID='SBM.DEP.DAILY.CHANGE.EGP.CAT'
    REPORT.ID='SBM.DEP.DAILY.CHANGE.EGP'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    T.DATE = TODAY
    DATE.B4 = T.DATE

    CALL CDT('',T.DATE, '-1W')
    CALL CDT('',DATE.B4, '-2W')
*TEXT = T.DATE ; CALL REM
*TEXT = DATE.B4 ; CALL REM
************************
*T.DATE = 20110405
*DATE.B4 = 20110404
*COMP = 'EG0010013'
*COMP = 'EG0010032'

************************
    TOT.DEP.AMT     = 0
    TOT.DEP.AMT.OLD = 0
    TOT.DIFF.AMT    = 0
    DEP.LCY         = 0
    DEP.LCY.OLD     = 0
    DIFF.AMT        = 0
    RETURN
*========================================================================
PROCESS:
    FN.SCB.D = 'F.SCB.DEPOSITS.LOANS.DETAILS' ; F.SCB.D = ''
    CALL OPF(FN.SCB.D,F.SCB.D)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CATG = 'F.CATEGORY' ; F.CATG = ''
    CALL OPF(FN.CATG,F.CATG)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT F.SCB.DEPOSITS.LOANS.DETAILS WITH @ID LIKE ...":T.DATE:" AND CO.CODE EQ ":COMP:" BY @ID"
**T.SEL = "SELECT F.SCB.DEPOSITS.LOANS.DETAILS WITH @ID LIKE 01305002...20120202"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**DEBUG
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            R.SCB.D = ''; R.OLD.D = ''
            CALL F.READ(FN.SCB.D,KEY.LIST<I>,R.SCB.D,F.SCB.D,E1)

            CUS.ID  = FIELD(KEY.LIST<I>,'*',1)
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            IF CUST.NAME EQ '' THEN
                CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.TITLE.1,CUS.ID,CUST.NAME)
                CALL DBR('ACCOUNT':@FM: AC.CATEGORY,CUS.ID,CAT.NAME)
            END

            DEP.LCY   = DROUND(R.SCB.D<DEP.DET.DEPOSITS>/1000,'0')
            CAT.ID    = R.SCB.D<DEP.DET.RESERVED10>
            REF.ID    = R.SCB.D<DEP.DET.RESERVED9>

            IF DEP.LCY EQ '' THEN
                DEP.LCY = 0
            END
            OLD.ID    = CUS.ID[1,16]:'*':DATE.B4

            CALL F.READ(FN.SCB.D,OLD.ID,R.OLD.D,F.SCB.D,E3)
            IF E3 THEN
                DEP.LCY.OLD = 0
            END
            ELSE
                DEP.LCY.OLD   = DROUND(R.OLD.D<DEP.DET.DEPOSITS>/1000,'0')
            END

            DIFF.AMT = DEP.LCY - DEP.LCY.OLD
            ABS.DIFF.AMT = ABS(DIFF.AMT)

            IF ABS.DIFF.AMT GE 1000 THEN
                XX  = SPACE(120)
                XX1 = SPACE(120)
                XX<1,1>[1,20]   = CUS.ID
                XX<1,1>[15,35]  = CUST.NAME
                XX<1,1>[65,20]  = DEP.LCY.OLD
                XX<1,1>[85,20]  = DEP.LCY
                XX<1,1>[105,20] = DIFF.AMT
                XX1<1,1>[1,20]  = CAT.ID:'*':REF.ID

                PRINT XX<1,1>
                PRINT XX1<1,1>
                PRINT STR('-',130)

                TOT.DEP.AMT = TOT.DEP.AMT + DEP.LCY
                TOT.DEP.AMT.OLD = TOT.DEP.AMT.OLD + DEP.LCY.OLD
                TOT.DIFF.AMT = TOT.DIFF.AMT + DIFF.AMT
            END
        NEXT I
    END
*=========================
** Z.SEL = "SELECT ":FN.SCB.D:" WITH @ID LIKE ....":DATE.B4:" AND DEPOSITS NE 0 AND DEPOSITS NE '' AND CO.CODE EQ ":COMP:" BY @ID"
    Z.SEL = "SELECT ":FN.SCB.D:" WITH @ID LIKE ...":DATE.B4:" AND DEPOSITS NE 0 AND DEPOSITS NE '' AND CO.CODE EQ ":COMP:" BY @ID "
    CALL EB.READLIST(Z.SEL,KEY.LIST2,"",SELECTED2,ER.MSG2)
*DEBUG
    IF SELECTED2 THEN
        FOR K = 1 TO SELECTED2
            R.SCB.D = ''; R.OLD.D = ''
            CALL F.READ(FN.SCB.D,KEY.LIST2<K>,R.SCB.D,F.SCB.D,E2)
            CUS.ID  = FIELD(KEY.LIST2<K>,'*',1)
* NEW.ID =  CUS.ID:'*':T.DATE
            NEW.ID =  CUS.ID:'*':DATE.B4
            CALL F.READ(FN.SCB.D,NEW.ID,R.DD,F.SCB.D,E3)
            IF E3 THEN
                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
                CUST.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                IF CUST.NAME EQ '' THEN
                    CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.TITLE.1,CUS.ID,CUST.NAME)
                    CALL DBR('ACCOUNT':@FM: AC.CATEGORY,CUS.ID,CAT.NAME)
                END

                CAT.ID    = R.SCB.D<DEP.DET.RESERVED10>
                REF.ID    = R.SCB.D<DEP.DET.RESERVED9>

                DEP.LCY.OLD = DROUND(R.SCB.D<DEP.DET.DEPOSITS>/1000,'0')
                DEP.LCY = 0
                DIFF.AMT = DEP.LCY - DEP.LCY.OLD

                ABS.DIFF.AMT = ABS(DIFF.AMT)

                IF ABS.DIFF.AMT GE 1000 THEN
                    XX  = SPACE(120)
                    XX1 = SPACE(120)
                    XX<1,1>[1,15]   = CUS.ID
                    XX<1,1>[15,35]  = CUST.NAME
                    XX<1,1>[65,20]  = DEP.LCY.OLD
                    XX<1,1>[85,20]  = DEP.LCY
                    XX<1,1>[105,20] = DIFF.AMT
                    XX1<1,1>[1,20]   = CAT.ID:'*':REF.ID
                    PRINT XX<1,1>
                    PRINT XX1<1,1>
                    PRINT STR('-',130)
                    TOT.DEP.AMT = TOT.DEP.AMT + DEP.LCY
                    TOT.DEP.AMT.OLD = TOT.DEP.AMT.OLD + DEP.LCY.OLD
                    TOT.DIFF.AMT = TOT.DIFF.AMT + DIFF.AMT
                END
            END
        NEXT K
    END
*==========================
    PRINT STR('=',130)
    XX2 = SPACE(120)
    XX2<1,1>[1,35]   = "�����������"
    XX2<1,1>[65,20]  = TOT.DEP.AMT.OLD
    XX2<1,1>[85,20]  = TOT.DEP.AMT
    XX2<1,1>[105,20] = TOT.DIFF.AMT
    PRINT XX2<1,1>
    PRINT STR('=',130)

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH

    T.DAY = T.DATE[7,2]:'/':T.DATE[5,2]:'/':T.DATE[1,4]

    TD1 = T.DATE[5,2]:'/':T.DATE[7,2]
    TD2 = DATE.B4[5,2]:'/':DATE.B4[7,2]


    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������ ����� ������� ������� �������"
    PR.HD :="'L'":SPACE(55):"���� ���� �� ����� ���� - ������ ������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(35):"���� ���":SPACE(10):"���� ���":SPACE(13):"������"
    PR.HD :="'L'":SPACE(65):TD2:SPACE(13):TD1
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
