* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
***************************MAHMOUD 7/9/2014******************************
    PROGRAM SBR.CBE.SCCD.DET.ALL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 41 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB LD.IND.REC
    RETURN
*========================================
INITIATE:
*-------
    KK = 0
    K2 = 0
    ARR.REC = ''
***********************
    TDD1 = TODAY
***********************
    COMMA = ","
    FOLDER   = "&SAVEDLISTS&"
    FILENAME = "SBR.CBE.SCCD.DET.ALL.CSV"
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    HEAD.DESC  = "CUSTOMER NUMBER":COMMA
    HEAD.DESC := "CUSTOMER NAME":COMMA
    HEAD.DESC := "REFERENCE NUMBER":COMMA
    HEAD.DESC := "CURRENCY":COMMA
    HEAD.DESC := "AMOUNT":COMMA
    HEAD.DESC := "MATURITY DATE":COMMA
    HEAD.DESC := "ISSUE DATE":COMMA
    HEAD.DESC := "RATE":COMMA
    HEAD.DESC := "CUSTOMER TYPE":COMMA
    HEAD.DESC := "DAYS TO MATURITY":COMMA
    HEAD.DESC := "LEDGER":COMMA
    HEAD.DESC := "OUR BRANCH NAME":COMMA
    HEAD.DESC := "CUSTOMER BANK NAME":COMMA
    HEAD.DESC := "TYPE OF FUND":COMMA
    HEAD.DESC := "DENOMINATION":COMMA
    HEAD.DESC := "REPORT TIME & DATE: ":TIMEDATE():COMMA

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
***********************
    ARR.DATA = ''
    REC.QTY = 0
    REC.AMT = 0
    FRESH.AMT = 0
    TRANS.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

    RETURN
*========================================
CALLDB:
*-------
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    RETURN
*========================================
LD.IND.REC:
*--------
    SEL.CMD = "SELECT ":FN.IND.LD
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR1)
    CRT SELECTED
    LOOP
        REMOVE CU.ID FROM KEY.LIST SETTING POSS.LD.CU
    WHILE CU.ID:POSS.LD.CU
        CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
        LOOP
            REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
        WHILE LD.ID:POS.LD
            IF LD.ID[1,2] EQ 'LD' THEN
                GOSUB LD.REC
            END
        REPEAT
    REPEAT
    RETURN
****************************************
LD.REC:
*--------
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
    REC.STA     = R.LD<LD.STATUS>
    REC.CAT     = R.LD<LD.CATEGORY>
    IF REC.STA NE 'LIQ' THEN
        FINDSTR REC.CAT IN SCCD.GRP SETTING POS.LD1 THEN
            K2++
            REC.COM  = R.LD<LD.CO.CODE>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,REC.COM,COM.NAM)
            REC.CUS  = R.LD<LD.CUSTOMER.ID>
            REC.CAT  = R.LD<LD.CATEGORY>
            REC.CUR  = R.LD<LD.CURRENCY>
            REC.AMT  = R.LD<LD.AMOUNT>
            REC.VAL  = R.LD<LD.VALUE.DATE>
            REC.MAT  = R.LD<LD.FIN.MAT.DATE>
            REC.DYM = 'C'
            IF REC.MAT NE '' THEN
                CALL CDD("",'20140831',REC.MAT,REC.DYM)
            END
            REC.LCL  = R.LD<LD.LOCAL.REF>
            REC.CNM  = REC.LCL<1,LDLR.IN.RESPECT.OF>
            REC.RAT  = REC.LCL<1,LDLR.PERCENT>
            REC.FND  = REC.LCL<1,LDLR.INVESTED.AMOUNT>
            REC.FLG  = REC.LCL<1,LDLR.CHK.FLG>
            REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
            IF REC.QTY EQ '' THEN REC.QTY = 1
            REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
*Line [ 162 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
            IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
                REC.TYP = "EGP-1000-3M-60M-SUEZ"
            END
            CALL F.READ(FN.CU,REC.CUS,R.CU,F.CU,ERR.CU)
            CU.LCL = R.CU<EB.CUS.LOCAL.REF>
            REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
            IF REC.FLG EQ '' OR REC.FLG NE '�����' THEN
                IF REC.SEC EQ '4650' THEN
                    REC.FLG = '�����'
                END ELSE
                    REC.FLG = '���� ��������'
                END
            END
            REC.BNK = CU.LCL<1,CULR.CU.BANK>
            IF REC.BNK EQ '' THEN
                REC.BNK = '0017'
            END
            CALL DBR('SCB.BANK':@FM:SCB.BAN.BANK.NAME,REC.BNK,REC.BNM)
*===========================================
            GOSUB WRITE.REC.TO.FILE
*===========================================
        END
    END

    RETURN
*****************************************************************
WRITE.REC.TO.FILE:
*----------------
    BB.DATA = REC.CUS:COMMA:REC.CNM:COMMA:LD.ID:COMMA:REC.CUR:COMMA:REC.AMT:COMMA:REC.MAT:COMMA:REC.VAL:COMMA:REC.RAT:COMMA:REC.FLG:COMMA:REC.DYM:COMMA:REC.CAT:COMMA:COM.NAM:COMMA:REC.BNM:COMMA:REC.FND:COMMA:REC.TYP:COMMA
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*****************************************************************
END
