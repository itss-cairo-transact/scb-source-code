* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LOAN.DAILY.CHANGE.EGP
*    PROGRAM SBM.LOAN.DAILY.CHANGE.EGP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPOSITS.LOANS.D
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LOAN.DAILY.CHANGE.EGP'
    CALL PRINTER.ON(REPORT.ID,'')

    COMP = ID.COMPANY
    T.DATE = TODAY
    DATE.B4 = T.DATE

    CALL CDT('',T.DATE, '-1W')
    CALL CDT('',DATE.B4, '-2W')

*******************************
*T.DATE  = 20110405
*DATE.B4 = 20110404
*COMP    = 'EG0010013'
*COMP    = 'EG0010032'
*******************************

    TOT.LOAN.AMT     = 0
    TOT.LOAN.AMT.OLD = 0
    TOT.DIFF.AMT    = 0
    LOAN.LCY         = 0
    LOAN.LCY.OLD     = 0
    DIFF.AMT        = 0
    RETURN
*========================================================================
PROCESS:
    FN.SCB.D = 'F.SCB.DEPOSITS.LOANS.D' ; F.SCB.D = ''
    CALL OPF(FN.SCB.D,F.SCB.D)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CATG = 'F.CATEGORY' ; F.CATG = ''
    CALL OPF(FN.CATG,F.CATG)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT ":FN.SCB.D:" WITH @ID LIKE ....":T.DATE:" AND CO.CODE EQ ":COMP:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            R.SCB.D = ''; R.OLD.D = ''
            CALL F.READ(FN.SCB.D,KEY.LIST<I>,R.SCB.D,F.SCB.D,E1)

            CUS.ID  = FIELD(KEY.LIST<I>,'.',1)
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
******************
            IF CUST.NAME EQ '' THEN
                CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.TITLE.1,CUS.ID,CUST.NAME)
            END
******************
            LOAN.LCY   = DROUND(R.SCB.D<DEP.LOAN.LOANS>/1000,'0')

            IF LOAN.LCY EQ '' THEN
                LOAN.LCY = 0
            END
            OLD.ID    = CUS.ID:'.':DATE.B4

            CALL F.READ(FN.SCB.D,OLD.ID,R.OLD.D,F.SCB.D,E3)
            IF E3 THEN
                LOAN.LCY.OLD = 0
            END
            ELSE
                LOAN.LCY.OLD   = DROUND(R.OLD.D<DEP.LOAN.LOANS>/1000,'0')
            END

            DIFF.AMT = LOAN.LCY - LOAN.LCY.OLD
            ABS.DIFF.AMT = ABS(DIFF.AMT)

            IF ABS.DIFF.AMT GT 1000 THEN
                XX = SPACE(120)
                XX<1,1>[1,15]   = CUS.ID
                XX<1,1>[15,35]  = CUST.NAME
                XX<1,1>[65,20]  = LOAN.LCY.OLD
                XX<1,1>[85,20]  = LOAN.LCY
                XX<1,1>[105,20] = DIFF.AMT
                PRINT XX<1,1>
                PRINT STR('-',130)
                TOT.LOAN.AMT = TOT.LOAN.AMT + LOAN.LCY
                TOT.LOAN.AMT.OLD = TOT.LOAN.AMT.OLD + LOAN.LCY.OLD
                TOT.DIFF.AMT = TOT.DIFF.AMT + DIFF.AMT
            END
        NEXT I
    END
*=========================
    Z.SEL = "SELECT ":FN.SCB.D:" WITH @ID LIKE ....":DATE.B4:" AND LOANS NE 0 AND LOANS NE '' AND CO.CODE EQ ":COMP:" BY @ID"
    CALL EB.READLIST(Z.SEL,KEY.LIST2,"",SELECTED2,ER.MSG2)

    IF SELECTED2 THEN
        FOR K = 1 TO SELECTED2
            R.SCB.D = ''; R.OLD.D = ''
            CALL F.READ(FN.SCB.D,KEY.LIST<K>,R.SCB.D,F.SCB.D,E2)
            CUS.ID  = FIELD(KEY.LIST<K>,'.',1)
            NEW.ID =  CUS.ID:'.':T.DATE
            CALL F.READ(FN.SCB.D,NEW.ID,R.DD,F.SCB.D,E3)
            IF E3 THEN
                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
                CUST.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                IF CUST.NAME EQ '' THEN
                    CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.TITLE.1,CUS.ID,CUST.NAME)
                END

                LAON.LCY.OLD = DROUND(R.SCB.D<DEP.LOAN.LOANS>/1000,'0')
                LOAN.LCY = 0
                DIFF.AMT = LOAN.LCY - LAON.LCY.OLD

                ABS.DIFF.AMT = ABS(DIFF.AMT)

                IF ABS.DIFF.AMT GT 1000 THEN
                    XX = SPACE(120)
                    XX<1,1>[1,15]   = CUS.ID
                    XX<1,1>[15,35]  = CUST.NAME
                    XX<1,1>[65,20]  = LOAN.LCY.OLD
                    XX<1,1>[85,20]  = LOAN.LCY
                    XX<1,1>[105,20] = DIFF.AMT
                    PRINT XX<1,1>
                    PRINT STR('-',130)
                    TOT.LOAN.AMT = TOT.LOAN.AMT + LOAN.LCY
                    TOT.LOAN.AMT.OLD = TOT.LOAN.AMT.OLD + LOAN.LCY.OLD
                    TOT.DIFF.AMT = TOT.DIFF.AMT + DIFF.AMT
                END
            END
        NEXT K
    END
*==========================
    PRINT STR('=',130)
    XX2 = SPACE(120)
    XX2<1,1>[1,35]   = "�����������"
    XX2<1,1>[65,20]  = TOT.LOAN.AMT.OLD
    XX2<1,1>[85,20]  = TOT.LOAN.AMT
    XX2<1,1>[105,20] = TOT.DIFF.AMT
    PRINT XX2<1,1>
    PRINT STR('=',130)

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH

    T.DAY = T.DATE[7,2]:'/':T.DATE[5,2]:'/':T.DATE[1,4]

    TD1 = T.DATE[5,2]:'/':T.DATE[7,2]
    TD2 = DATE.B4[5,2]:'/':DATE.B4[7,2]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������ ����� ������ ������� �������"
    PR.HD :="'L'":SPACE(55):"���� ���� �� ����� ���� - ������ ������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(35):"���� ���":SPACE(10):"���� ���":SPACE(13):"������"
    PR.HD :="'L'":SPACE(65):TD2:SPACE(13):TD1
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
