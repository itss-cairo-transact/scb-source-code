* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** ����� ����� ������� �������� �������� ***
*** CREATED BY KHALED ***
***=================================

    SUBROUTINE SBM.DEP.FCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 42 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.DEP.FCY'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.AMOUNT.LCY1 = 0   ; Y.AMOUNT.LCY11 = 0  ; Y.AMOUNT.LCY39 = 0
    Y.AMOUNT.LCY2 = 0   ; Y.AMOUNT.LCY12 = 0  ; Y.AMOUNT.LCY44 = 0
    Y.AMOUNT.LCY3 = 0   ; Y.AMOUNT.LCY13 = 0  ; Y.AMOUNT.LCY25 = 0
    Y.AMOUNT.LCY4 = 0   ; Y.AMOUNT.LCY14 = 0  ; Y.AMOUNT.LCY26 = 0
    Y.AMOUNT.LCY5 = 0   ; Y.AMOUNT.LCY15 = 0  ; Y.AMOUNT.LCY27 = 0
    Y.AMOUNT.LCY6 = 0   ; Y.AMOUNT.LCY16 = 0  ; Y.AMOUNT.LCY28 = 0
    Y.AMOUNT.LCY7 = 0   ; Y.AMOUNT.LCY17 = 0  ; Y.AMOUNT.LCY29 = 0
    Y.AMOUNT.LCY8 = 0   ; Y.AMOUNT.LCY18 = 0  ; Y.AMOUNT.LCY30 = 0
    Y.AMOUNT.LCY9 = 0   ; Y.AMOUNT.LCY19 = 0  ; Y.AMOUNT.LCY31 = 0
    Y.AMOUNT.LCY10 = 0  ; Y.AMOUNT.LCY20 = 0  ; Y.AMOUNT.LCY32 = 0
    Y.AMOUNT.LCY21 = 0  ; Y.AMOUNT.LCY23 = 0  ; Y.AMOUNT.LCY33 = 0
    Y.AMOUNT.LCY22 = 0  ; Y.AMOUNT.LCY24 = 0  ; Y.AMOUNT.LCY34 = 0
    Y.AMOUNT.LCY35 = 0  ; Y.AMOUNT.LCY40 = 0  ; Y.AMOUNT.LCY45 = 0
    Y.AMOUNT.LCY36 = 0  ; Y.AMOUNT.LCY41 = 0  ; Y.AMOUNT.LCY46 = 0
    Y.AMOUNT.LCY37 = 0  ; Y.AMOUNT.LCY42 = 0  ; Y.AMOUNT.LCY47 = 0
    Y.AMOUNT.LCY38 = 0  ; Y.AMOUNT.LCY43 = 0  ; Y.AMOUNT.LCY48 = 0

    Y.AMOUNT.FCY1 = 0   ; Y.AMOUNT.FCY11 = 0  ; Y.AMOUNT.FCY39 = 0
    Y.AMOUNT.FCY2 = 0   ; Y.AMOUNT.FCY12 = 0  ; Y.AMOUNT.FCY44 = 0
    Y.AMOUNT.FCY3 = 0   ; Y.AMOUNT.FCY13 = 0  ; Y.AMOUNT.FCY25 = 0
    Y.AMOUNT.FCY4 = 0   ; Y.AMOUNT.FCY14 = 0  ; Y.AMOUNT.FCY26 = 0
    Y.AMOUNT.FCY5 = 0   ; Y.AMOUNT.FCY15 = 0  ; Y.AMOUNT.FCY27 = 0
    Y.AMOUNT.FCY6 = 0   ; Y.AMOUNT.FCY16 = 0  ; Y.AMOUNT.FCY28 = 0
    Y.AMOUNT.FCY7 = 0   ; Y.AMOUNT.FCY17 = 0  ; Y.AMOUNT.FCY29 = 0
    Y.AMOUNT.FCY8 = 0   ; Y.AMOUNT.FCY18 = 0  ; Y.AMOUNT.FCY30 = 0
    Y.AMOUNT.FCY9 = 0   ; Y.AMOUNT.FCY19 = 0  ; Y.AMOUNT.FCY31 = 0
    Y.AMOUNT.FCY10 = 0  ; Y.AMOUNT.FCY20 = 0  ; Y.AMOUNT.FCY32 = 0
    Y.AMOUNT.FCY21 = 0  ; Y.AMOUNT.FCY23 = 0  ; Y.AMOUNT.FCY33 = 0
    Y.AMOUNT.FCY22 = 0  ; Y.AMOUNT.FCY24 = 0  ; Y.AMOUNT.FCY34 = 0
    Y.AMOUNT.FCY35 = 0  ; Y.AMOUNT.FCY40 = 0  ; Y.AMOUNT.FCY45 = 0
    Y.AMOUNT.FCY36 = 0  ; Y.AMOUNT.FCY41 = 0  ; Y.AMOUNT.FCY46 = 0
    Y.AMOUNT.FCY37 = 0  ; Y.AMOUNT.FCY42 = 0  ; Y.AMOUNT.FCY47 = 0
    Y.AMOUNT.FCY38 = 0  ; Y.AMOUNT.FCY43 = 0  ; Y.AMOUNT.FCY48 = 0
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
    RETURN
*========================================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*************************************************************************
    T.SEL = "SELECT ":FN.CBE: " WITH CBEM.CATEG IN (1012 1013 1014 1015 1016 3005 3010 3011 3012 3013 1001 6501 6502 6503 6504) AND CBEM.CY NE 'EGP' AND CBEM.BR EQ ":COMP.BR
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR X = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<X>,R.CBE,F.CBE,E4)
            Y.CUR = R.CBE<C.CBEM.CY>
            Y.CATEG = R.CBE<C.CBEM.CATEG>
            Y.SECTOR3 = R.CBE<C.CBEM.NEW.SECTOR>
            SECTOR.ID3 = Y.SECTOR3[2,3]
*************************************************************************
            IF SECTOR.ID3 GE 110 AND SECTOR.ID3 LE 130 THEN
                IF Y.CATEG EQ 1001 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY1 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY1 = DROUND(Y.AMOUNT.LCY1,'2')
                        Y.AMOUNT.FCY1 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY1 = DROUND(Y.AMOUNT.FCY1,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY2 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY2 = DROUND(Y.AMOUNT.LCY2,'2')
                            Y.AMOUNT.FCY2 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY2 = DROUND(Y.AMOUNT.FCY2,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY3 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY3 = DROUND(Y.AMOUNT.LCY3,'2')
                                Y.AMOUNT.FCY3 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY3 = DROUND(Y.AMOUNT.FCY3,'2')
                            END ELSE
                                Y.AMOUNT.LCY4 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY4 = DROUND(Y.AMOUNT.LCY4,'2')
                                Y.AMOUNT.FCY4 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY4 = DROUND(Y.AMOUNT.FCY4,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF Y.CATEG GE 6501 AND Y.CATEG LE 6504 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY5 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY5 = DROUND(Y.AMOUNT.LCY5,'2')
                        Y.AMOUNT.FCY5 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY5 = DROUND(Y.AMOUNT.FCY5,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY6 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY6 = DROUND(Y.AMOUNT.LCY6,'2')
                            Y.AMOUNT.FCY6 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY6 = DROUND(Y.AMOUNT.FCY6,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY7 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY7 = DROUND(Y.AMOUNT.LCY7,'2')
                                Y.AMOUNT.FCY7 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY7 = DROUND(Y.AMOUNT.FCY7,'2')
                            END ELSE
                                Y.AMOUNT.LCY8 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY8 = DROUND(Y.AMOUNT.LCY8,'2')
                                Y.AMOUNT.FCY8 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY8 = DROUND(Y.AMOUNT.FCY8,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF (Y.CATEG EQ 1012 OR Y.CATEG EQ 1013 OR Y.CATEG EQ 1014 OR Y.CATEG EQ 1015 OR Y.CATEG EQ 1016 OR Y.CATEG EQ 3005 OR Y.CATEG EQ 3010 OR Y.CATEG EQ 3011 OR Y.CATEG EQ 3012 OR Y.CATEG EQ 3013) THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY9 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY9 = DROUND(Y.AMOUNT.LCY9,'2')
                        Y.AMOUNT.FCY9 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY9 = DROUND(Y.AMOUNT.FCY9,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY10 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY10 = DROUND(Y.AMOUNT.LCY10,'2')
                            Y.AMOUNT.FCY10 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY10 = DROUND(Y.AMOUNT.FCY10,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY11 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY11 = DROUND(Y.AMOUNT.LCY11,'2')
                                Y.AMOUNT.FCY11 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY11 = DROUND(Y.AMOUNT.FCY11,'2')
                            END ELSE
                                Y.AMOUNT.LCY12 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY12 = DROUND(Y.AMOUNT.LCY12,'2')
                                Y.AMOUNT.FCY12 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY12 = DROUND(Y.AMOUNT.FCY12,'2')
                            END
                        END
                    END
                END
            END
*************************************************************************
            IF SECTOR.ID3 GE 210 AND SECTOR.ID3 LE 260 THEN
                IF Y.CATEG EQ 1001 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY13 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY13 = DROUND(Y.AMOUNT.LCY13,'2')
                        Y.AMOUNT.FCY13 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY13 = DROUND(Y.AMOUNT.FCY13,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY14 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY14 = DROUND(Y.AMOUNT.LCY14,'2')
                            Y.AMOUNT.FCY14 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY14 = DROUND(Y.AMOUNT.FCY14,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY15 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY15 = DROUND(Y.AMOUNT.LCY15,'2')
                                Y.AMOUNT.FCY15 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY15 = DROUND(Y.AMOUNT.FCY15,'2')
                            END ELSE
                                Y.AMOUNT.LCY16 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY16 = DROUND(Y.AMOUNT.LCY16,'2')
                                Y.AMOUNT.FCY16 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY16 = DROUND(Y.AMOUNT.FCY16,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF Y.CATEG GE 6501 AND Y.CATEG LE 6504 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY17 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY17 = DROUND(Y.AMOUNT.LCY17,'2')
                        Y.AMOUNT.FCY17 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY17 = DROUND(Y.AMOUNT.FCY17,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY18 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY18 = DROUND(Y.AMOUNT.LCY18,'2')
                            Y.AMOUNT.FCY18 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY18 = DROUND(Y.AMOUNT.FCY18,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY19 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY19 = DROUND(Y.AMOUNT.LCY19,'2')
                                Y.AMOUNT.FCY19 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY19 = DROUND(Y.AMOUNT.FCY19,'2')
                            END ELSE
                                Y.AMOUNT.LCY20 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY20 = DROUND(Y.AMOUNT.LCY20,'2')
                                Y.AMOUNT.FCY20 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY20 = DROUND(Y.AMOUNT.FCY20,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF (Y.CATEG EQ 1012 OR Y.CATEG EQ 1013 OR Y.CATEG EQ 1014 OR Y.CATEG EQ 1015 OR Y.CATEG EQ 1016 OR Y.CATEG EQ 3005 OR Y.CATEG EQ 3010 OR Y.CATEG EQ 3011 OR Y.CATEG EQ 3012 OR Y.CATEG EQ 3013) THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY21 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY21 = DROUND(Y.AMOUNT.LCY21,'2')
                        Y.AMOUNT.FCY21 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY21 = DROUND(Y.AMOUNT.FCY21,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY22 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY22 = DROUND(Y.AMOUNT.LCY22,'2')
                            Y.AMOUNT.FCY22 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY22 = DROUND(Y.AMOUNT.FCY22,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY23 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY23 = DROUND(Y.AMOUNT.LCY23,'2')
                                Y.AMOUNT.FCY23 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY23 = DROUND(Y.AMOUNT.FCY23,'2')
                            END ELSE
                                Y.AMOUNT.LCY24 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY24 = DROUND(Y.AMOUNT.LCY24,'2')
                                Y.AMOUNT.FCY24 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY24 = DROUND(Y.AMOUNT.FCY24,'2')
                            END
                        END
                    END
                END
            END
*************************************************************************
            IF SECTOR.ID3 GE 650 AND SECTOR.ID3 LE 750 THEN
                IF Y.CATEG EQ 1001 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY25 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY25 = DROUND(Y.AMOUNT.LCY25,'2')
                        Y.AMOUNT.FCY25 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY25 = DROUND(Y.AMOUNT.FCY25,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY26 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY26 = DROUND(Y.AMOUNT.LCY26,'2')
                            Y.AMOUNT.FCY26 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY26 = DROUND(Y.AMOUNT.FCY26,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY27 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY27 = DROUND(Y.AMOUNT.LCY27,'2')
                                Y.AMOUNT.FCY27 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY27 = DROUND(Y.AMOUNT.FCY27,'2')
                            END ELSE
                                Y.AMOUNT.LCY28 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY28 = DROUND(Y.AMOUNT.LCY28,'2')
                                Y.AMOUNT.FCY28 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY28 = DROUND(Y.AMOUNT.FCY28,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF Y.CATEG GE 6501 AND Y.CATEG LE 6504 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY29 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY29 = DROUND(Y.AMOUNT.LCY29,'2')
                        Y.AMOUNT.FCY29 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY29 = DROUND(Y.AMOUNT.FCY29,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY30 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY30 = DROUND(Y.AMOUNT.LCY30,'2')
                            Y.AMOUNT.FCY30 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY30 = DROUND(Y.AMOUNT.FCY30,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY31 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY31 = DROUND(Y.AMOUNT.LCY31,'2')
                                Y.AMOUNT.FCY31 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY31 = DROUND(Y.AMOUNT.FCY31,'2')
                            END ELSE
                                Y.AMOUNT.LCY32 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY32 = DROUND(Y.AMOUNT.LCY32,'2')
                                Y.AMOUNT.FCY32 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY32 = DROUND(Y.AMOUNT.FCY32,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF (Y.CATEG EQ 1012 OR Y.CATEG EQ 1013 OR Y.CATEG EQ 1014 OR Y.CATEG EQ 1015 OR Y.CATEG EQ 1016 OR Y.CATEG EQ 3005 OR Y.CATEG EQ 3010 OR Y.CATEG EQ 3011 OR Y.CATEG EQ 3012 OR Y.CATEG EQ 3013) THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY33 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY33 = DROUND(Y.AMOUNT.LCY33,'2')
                        Y.AMOUNT.FCY33 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY33 = DROUND(Y.AMOUNT.FCY33,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY34 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY34 = DROUND(Y.AMOUNT.LCY34,'2')
                            Y.AMOUNT.FCY34 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY34 = DROUND(Y.AMOUNT.FCY34,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY35 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY35 = DROUND(Y.AMOUNT.LCY35,'2')
                                Y.AMOUNT.FCY35 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY35 = DROUND(Y.AMOUNT.FCY35,'2')
                            END ELSE
                                Y.AMOUNT.LCY36 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY36 = DROUND(Y.AMOUNT.LCY36,'2')
                                Y.AMOUNT.FCY36 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY36 = DROUND(Y.AMOUNT.FCY36,'2')
                            END
                        END
                    END
                END
            END
*************************************************************************
            IF Y.SECTOR3 EQ 7000 THEN
                IF Y.CATEG EQ 1001 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY37 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY37 = DROUND(Y.AMOUNT.LCY37,'2')
                        Y.AMOUNT.FCY37 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY37 = DROUND(Y.AMOUNT.FCY37,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY38 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY38 = DROUND(Y.AMOUNT.LCY38,'2')
                            Y.AMOUNT.FCY38 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY38 = DROUND(Y.AMOUNT.FCY38,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY39 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY39 = DROUND(Y.AMOUNT.LCY39,'2')
                                Y.AMOUNT.FCY39 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY39 = DROUND(Y.AMOUNT.FCY39,'2')
                            END ELSE
                                Y.AMOUNT.LCY40 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY40 = DROUND(Y.AMOUNT.LCY40,'2')
                                Y.AMOUNT.FCY40 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY40 = DROUND(Y.AMOUNT.FCY40,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF Y.CATEG GE 6501 AND Y.CATEG LE 6504 THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY41 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY41 = DROUND(Y.AMOUNT.LCY41,'2')
                        Y.AMOUNT.FCY41 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY41 = DROUND(Y.AMOUNT.FCY41,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY42 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY42 = DROUND(Y.AMOUNT.LCY42,'2')
                            Y.AMOUNT.FCY42 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY42 = DROUND(Y.AMOUNT.FCY42,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY43 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY43 = DROUND(Y.AMOUNT.LCY43,'2')
                                Y.AMOUNT.FCY43 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY43 = DROUND(Y.AMOUNT.FCY43,'2')
                            END ELSE
                                Y.AMOUNT.LCY44 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY44 = DROUND(Y.AMOUNT.LCY44,'2')
                                Y.AMOUNT.FCY44 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY44 = DROUND(Y.AMOUNT.FCY44,'2')
                            END
                        END
                    END
                END
*************************************************************************
                IF (Y.CATEG EQ 1012 OR Y.CATEG EQ 1013 OR Y.CATEG EQ 1014 OR Y.CATEG EQ 1015 OR Y.CATEG EQ 1016 OR Y.CATEG EQ 3005 OR Y.CATEG EQ 3010 OR Y.CATEG EQ 3011 OR Y.CATEG EQ 3012 OR Y.CATEG EQ 3013) THEN
                    IF Y.CUR EQ 'USD' THEN
                        Y.AMOUNT.LCY45 += R.CBE<C.CBEM.IN.LCY>
                        Y.AMOUNT.LCY45 = DROUND(Y.AMOUNT.LCY45,'2')
                        Y.AMOUNT.FCY45 += R.CBE<C.CBEM.IN.FCY>
                        Y.AMOUNT.FCY45 = DROUND(Y.AMOUNT.FCY45,'2')
                    END ELSE
                        IF Y.CUR EQ 'GBP' THEN
                            Y.AMOUNT.LCY46 += R.CBE<C.CBEM.IN.LCY>
                            Y.AMOUNT.LCY46 = DROUND(Y.AMOUNT.LCY46,'2')
                            Y.AMOUNT.FCY46 += R.CBE<C.CBEM.IN.FCY>
                            Y.AMOUNT.FCY46 = DROUND(Y.AMOUNT.FCY46,'2')
                        END ELSE
                            IF Y.CUR EQ 'EUR' THEN
                                Y.AMOUNT.LCY47 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY47 = DROUND(Y.AMOUNT.LCY47,'2')
                                Y.AMOUNT.FCY47 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY47 = DROUND(Y.AMOUNT.FCY47,'2')
                            END ELSE
                                Y.AMOUNT.LCY48 += R.CBE<C.CBEM.IN.LCY>
                                Y.AMOUNT.LCY48 = DROUND(Y.AMOUNT.LCY48,'2')
                                Y.AMOUNT.FCY48 += R.CBE<C.CBEM.IN.FCY>
                                Y.AMOUNT.FCY48 = DROUND(Y.AMOUNT.FCY48,'2')
                            END
                        END
                    END
                END
            END
*************************************************************************
        NEXT X
        AMOUNT.TOTAL.FCY1 =  Y.AMOUNT.FCY1+ Y.AMOUNT.FCY2 + Y.AMOUNT.FCY3 + Y.AMOUNT.FCY4
        AMOUNT.TOTAL.LCY1 =  Y.AMOUNT.LCY1+ Y.AMOUNT.LCY2 + Y.AMOUNT.LCY3 + Y.AMOUNT.LCY4
        AMOUNT.TOTAL.FCY2 =  Y.AMOUNT.FCY5+ Y.AMOUNT.FCY6 + Y.AMOUNT.FCY7 + Y.AMOUNT.FCY8
        AMOUNT.TOTAL.LCY2 =  Y.AMOUNT.LCY5+ Y.AMOUNT.LCY6 + Y.AMOUNT.LCY7 + Y.AMOUNT.LCY8
        AMOUNT.TOTAL.FCY3 =  Y.AMOUNT.FCY9+ Y.AMOUNT.FCY10 + Y.AMOUNT.FCY11 + Y.AMOUNT.FCY12
        AMOUNT.TOTAL.LCY3 =  Y.AMOUNT.LCY9+ Y.AMOUNT.LCY10 + Y.AMOUNT.LCY11 + Y.AMOUNT.LCY12

        AMOUNT.TOTAL1 =  Y.AMOUNT.FCY1+ Y.AMOUNT.FCY5 + Y.AMOUNT.FCY9
        AMOUNT.TOTAL2 =  Y.AMOUNT.FCY2+ Y.AMOUNT.FCY6 + Y.AMOUNT.FCY10
        AMOUNT.TOTAL3 =  Y.AMOUNT.FCY3+ Y.AMOUNT.FCY7 + Y.AMOUNT.FCY11
        AMOUNT.TOTAL4 =  Y.AMOUNT.FCY4+ Y.AMOUNT.FCY8 + Y.AMOUNT.FCY12
        AMOUNT.TOTAL5 =  AMOUNT.TOTAL.FCY1 + AMOUNT.TOTAL.FCY2 + AMOUNT.TOTAL.FCY3
        AMOUNT.TOTAL6 =  AMOUNT.TOTAL.LCY1 + AMOUNT.TOTAL.LCY2 + AMOUNT.TOTAL.LCY3
*-----------------------------------------------------------------------
        AMOUNT.TOTAL.FCY4 =  Y.AMOUNT.FCY13+ Y.AMOUNT.FCY14 + Y.AMOUNT.FCY15 + Y.AMOUNT.FCY16
        AMOUNT.TOTAL.LCY4 =  Y.AMOUNT.LCY13+ Y.AMOUNT.LCY14 + Y.AMOUNT.LCY15 + Y.AMOUNT.LCY16
        AMOUNT.TOTAL.FCY5 =  Y.AMOUNT.FCY17+ Y.AMOUNT.FCY18 + Y.AMOUNT.FCY19 + Y.AMOUNT.FCY20
        AMOUNT.TOTAL.LCY5 =  Y.AMOUNT.LCY17+ Y.AMOUNT.LCY18 + Y.AMOUNT.LCY19 + Y.AMOUNT.LCY20
        AMOUNT.TOTAL.FCY6 =  Y.AMOUNT.FCY21+ Y.AMOUNT.FCY22 + Y.AMOUNT.FCY23 + Y.AMOUNT.FCY24
        AMOUNT.TOTAL.LCY6 =  Y.AMOUNT.LCY21+ Y.AMOUNT.LCY22 + Y.AMOUNT.LCY23 + Y.AMOUNT.LCY24

        AMOUNT.TOTAL7 =  Y.AMOUNT.FCY13+ Y.AMOUNT.FCY17 + Y.AMOUNT.FCY21
        AMOUNT.TOTAL8 =  Y.AMOUNT.FCY14+ Y.AMOUNT.FCY18 + Y.AMOUNT.FCY22
        AMOUNT.TOTAL9 =  Y.AMOUNT.FCY15+ Y.AMOUNT.FCY19 + Y.AMOUNT.FCY23
        AMOUNT.TOTAL10 =  Y.AMOUNT.FCY16+ Y.AMOUNT.FCY20 + Y.AMOUNT.FCY24
        AMOUNT.TOTAL11 =  AMOUNT.TOTAL.FCY4 + AMOUNT.TOTAL.FCY5 + AMOUNT.TOTAL.FCY6
        AMOUNT.TOTAL12 =  AMOUNT.TOTAL.LCY4 + AMOUNT.TOTAL.LCY5 + AMOUNT.TOTAL.LCY6
*-----------------------------------------------------------
        AMOUNT.TOTAL.FCY7 =  Y.AMOUNT.FCY25+ Y.AMOUNT.FCY26 + Y.AMOUNT.FCY27 + Y.AMOUNT.FCY28
        AMOUNT.TOTAL.LCY7 =  Y.AMOUNT.LCY25+ Y.AMOUNT.LCY26 + Y.AMOUNT.LCY27 + Y.AMOUNT.LCY28
        AMOUNT.TOTAL.FCY8 =  Y.AMOUNT.FCY29+ Y.AMOUNT.FCY30 + Y.AMOUNT.FCY31 + Y.AMOUNT.FCY32
        AMOUNT.TOTAL.LCY8 =  Y.AMOUNT.LCY29+ Y.AMOUNT.LCY30 + Y.AMOUNT.LCY31 + Y.AMOUNT.LCY32
        AMOUNT.TOTAL.FCY9 =  Y.AMOUNT.FCY33+ Y.AMOUNT.FCY34 + Y.AMOUNT.FCY35 + Y.AMOUNT.FCY36
        AMOUNT.TOTAL.LCY9 =  Y.AMOUNT.LCY33+ Y.AMOUNT.LCY34 + Y.AMOUNT.LCY35 + Y.AMOUNT.LCY36

        AMOUNT.TOTAL13 =  Y.AMOUNT.FCY25+ Y.AMOUNT.FCY29 + Y.AMOUNT.FCY33
        AMOUNT.TOTAL14 =  Y.AMOUNT.FCY26+ Y.AMOUNT.FCY30 + Y.AMOUNT.FCY34
        AMOUNT.TOTAL15 =  Y.AMOUNT.FCY27+ Y.AMOUNT.FCY31 + Y.AMOUNT.FCY35
        AMOUNT.TOTAL16 =  Y.AMOUNT.FCY28+ Y.AMOUNT.FCY32 + Y.AMOUNT.FCY36
        AMOUNT.TOTAL17 =  AMOUNT.TOTAL.FCY7 + AMOUNT.TOTAL.FCY8 + AMOUNT.TOTAL.FCY9
        AMOUNT.TOTAL18 =  AMOUNT.TOTAL.LCY7 + AMOUNT.TOTAL.LCY8 + AMOUNT.TOTAL.LCY9
*-----------------------------------------------------------------------
        AMOUNT.TOTAL.FCY10 =  Y.AMOUNT.FCY37+ Y.AMOUNT.FCY38 + Y.AMOUNT.FCY39 + Y.AMOUNT.FCY40
        AMOUNT.TOTAL.LCY10 =  Y.AMOUNT.LCY37+ Y.AMOUNT.LCY38 + Y.AMOUNT.LCY39 + Y.AMOUNT.LCY40
        AMOUNT.TOTAL.FCY11 =  Y.AMOUNT.FCY41+ Y.AMOUNT.FCY42 + Y.AMOUNT.FCY43 + Y.AMOUNT.FCY44
        AMOUNT.TOTAL.LCY11 =  Y.AMOUNT.LCY41+ Y.AMOUNT.LCY42 + Y.AMOUNT.LCY43 + Y.AMOUNT.LCY44
        AMOUNT.TOTAL.FCY12 =  Y.AMOUNT.FCY45+ Y.AMOUNT.FCY46 + Y.AMOUNT.FCY47 + Y.AMOUNT.FCY48
        AMOUNT.TOTAL.LCY12 =  Y.AMOUNT.LCY45+ Y.AMOUNT.LCY46 + Y.AMOUNT.LCY47 + Y.AMOUNT.LCY48

        AMOUNT.TOTAL19 =  Y.AMOUNT.FCY37+ Y.AMOUNT.FCY41 + Y.AMOUNT.FCY45
        AMOUNT.TOTAL20 =  Y.AMOUNT.FCY38+ Y.AMOUNT.FCY42 + Y.AMOUNT.FCY46
        AMOUNT.TOTAL21 =  Y.AMOUNT.FCY39+ Y.AMOUNT.FCY43 + Y.AMOUNT.FCY47
        AMOUNT.TOTAL22 =  Y.AMOUNT.FCY40+ Y.AMOUNT.FCY44 + Y.AMOUNT.FCY48
        AMOUNT.TOTAL23 =  AMOUNT.TOTAL.FCY10 + AMOUNT.TOTAL.FCY11 + AMOUNT.TOTAL.FCY12
        AMOUNT.TOTAL24 =  AMOUNT.TOTAL.LCY10 + AMOUNT.TOTAL.LCY11 + AMOUNT.TOTAL.LCY12
*-----------------------------------------------------------
        TOTAL1 = AMOUNT.TOTAL1 + AMOUNT.TOTAL7 + AMOUNT.TOTAL13 + AMOUNT.TOTAL19
        TOTAL2 = AMOUNT.TOTAL2 + AMOUNT.TOTAL8 + AMOUNT.TOTAL14 + AMOUNT.TOTAL20
        TOTAL3 = AMOUNT.TOTAL3 + AMOUNT.TOTAL9 + AMOUNT.TOTAL15 + AMOUNT.TOTAL21
        TOTAL4 = AMOUNT.TOTAL4 + AMOUNT.TOTAL10 + AMOUNT.TOTAL16 + AMOUNT.TOTAL22
        TOTAL5 = AMOUNT.TOTAL5 + AMOUNT.TOTAL11 + AMOUNT.TOTAL17 + AMOUNT.TOTAL23
        TOTAL6 = AMOUNT.TOTAL6 + AMOUNT.TOTAL12 + AMOUNT.TOTAL18 + AMOUNT.TOTAL24
*-----------------------------------------------------------
        XX = SPACE(120)
        XX<1,1>[1,15]   = "�������� ��������"
        PRINT XX<1,1>
        PRINT STR('*',20)
*--------------------------------------------------
        XX1 = SPACE(120)
        XX1<1,1>[1,15]   = "������� ��������"
        XX1<1,1>[25,15]  = Y.AMOUNT.FCY1
        XX1<1,1>[45,15]  = Y.AMOUNT.FCY2
        XX1<1,1>[63,15]  = Y.AMOUNT.FCY3
        XX1<1,1>[80,15]  = Y.AMOUNT.FCY4
        XX1<1,1>[95,15]  = AMOUNT.TOTAL.FCY1
        XX1<1,1>[115,15] = AMOUNT.TOTAL.LCY1
        PRINT XX1<1,1>
*----------------------------------------------
        XX2 = SPACE(120)
        XX2<1,1>[1,1]   = "������ ���������"
        XX2<1,1>[25,15]  = Y.AMOUNT.FCY5
        XX2<1,1>[45,15]  = Y.AMOUNT.FCY6
        XX2<1,1>[63,15]  = Y.AMOUNT.FCY7
        XX2<1,1>[80,15]  = Y.AMOUNT.FCY8
        XX2<1,1>[95,15] = AMOUNT.TOTAL.FCY2
        XX2<1,1>[115,15] = AMOUNT.TOTAL.LCY2
        PRINT XX2<1,1>
*-----------------------------------------------
        XX3 = SPACE(120)
        XX3<1,1>[1,15]   = "�������� �������"
        XX3<1,1>[25,15]  = Y.AMOUNT.FCY9
        XX3<1,1>[45,15]  = Y.AMOUNT.FCY10
        XX3<1,1>[63,15]  = Y.AMOUNT.FCY11
        XX3<1,1>[80,15]  = Y.AMOUNT.FCY12
        XX3<1,1>[95,15]  = AMOUNT.TOTAL.FCY3
        XX3<1,1>[115,15] = AMOUNT.TOTAL.LCY3
        PRINT XX3<1,1>
        PRINT STR('=',130)
*--------------------------------------------------
        XX4 = SPACE(120)
        XX4<1,1>[1,15]   = "����������������"
        XX4<1,1>[25,15]  = AMOUNT.TOTAL1
        XX4<1,1>[45,15]  = AMOUNT.TOTAL2
        XX4<1,1>[63,15]  = AMOUNT.TOTAL3
        XX4<1,1>[80,15]  = AMOUNT.TOTAL4
        XX4<1,1>[95,15]  = AMOUNT.TOTAL5
        XX4<1,1>[115,15] = AMOUNT.TOTAL6
        PRINT XX4<1,1>
        PRINT STR('=',130)
        PRINT STR(' ',130)
*----------------------------------------------------------
        XX5 = SPACE(120)
        XX5<1,1>[1,15]   = "���� ������� �����"
        PRINT XX5<1,1>
        PRINT STR('*',20)
*--------------------------------------------------
        XX6 = SPACE(120)
        XX6<1,1>[1,15]   = "������� ��������"
        XX6<1,1>[25,15]  = Y.AMOUNT.FCY13
        XX6<1,1>[45,15]  = Y.AMOUNT.FCY14
        XX6<1,1>[63,15]  = Y.AMOUNT.FCY15
        XX6<1,1>[80,15]  = Y.AMOUNT.FCY16
        XX6<1,1>[95,15]  = AMOUNT.TOTAL.FCY4
        XX6<1,1>[115,15] = AMOUNT.TOTAL.LCY4
        PRINT XX6<1,1>
*----------------------------------------------
        XX7 = SPACE(120)
        XX7<1,1>[1,1]   = "������ ���������"
        XX7<1,1>[25,15]  = Y.AMOUNT.FCY17
        XX7<1,1>[45,15]  = Y.AMOUNT.FCY18
        XX7<1,1>[63,15]  = Y.AMOUNT.FCY19
        XX7<1,1>[80,15]  = Y.AMOUNT.FCY20
        XX7<1,1>[95,15] = AMOUNT.TOTAL.FCY5
        XX7<1,1>[115,15] = AMOUNT.TOTAL.LCY5
        PRINT XX7<1,1>
*-----------------------------------------------
        XX8 = SPACE(120)
        XX8<1,1>[1,15]   = "�������� �������"
        XX8<1,1>[25,15]  = Y.AMOUNT.FCY21
        XX8<1,1>[45,15]  = Y.AMOUNT.FCY22
        XX8<1,1>[63,15]  = Y.AMOUNT.FCY23
        XX8<1,1>[80,15]  = Y.AMOUNT.FCY24
        XX8<1,1>[95,15]  = AMOUNT.TOTAL.FCY6
        XX8<1,1>[115,15] = AMOUNT.TOTAL.LCY6
        PRINT XX8<1,1>
        PRINT STR('=',130)
*--------------------------------------------------
        XX9 = SPACE(120)
        XX9<1,1>[1,15]   = "����������������"
        XX9<1,1>[25,15]  = AMOUNT.TOTAL7
        XX9<1,1>[45,15]  = AMOUNT.TOTAL8
        XX9<1,1>[63,15]  = AMOUNT.TOTAL9
        XX9<1,1>[80,15]  = AMOUNT.TOTAL10
        XX9<1,1>[95,15]  = AMOUNT.TOTAL11
        XX9<1,1>[115,15] = AMOUNT.TOTAL12
        PRINT XX9<1,1>
        PRINT STR('=',130)
        PRINT STR(' ',130)
*---------------------------------------------------------
        XX10 = SPACE(120)
        XX10<1,1>[1,15]   = "��������� ����������"
        PRINT XX10<1,1>
        PRINT STR('*',20)
*--------------------------------------------------
        XX11 = SPACE(120)
        XX11<1,1>[1,15]   = "������� ��������"
        XX11<1,1>[25,15]  = Y.AMOUNT.FCY25
        XX11<1,1>[45,15]  = Y.AMOUNT.FCY26
        XX11<1,1>[63,15]  = Y.AMOUNT.FCY27
        XX11<1,1>[80,15]  = Y.AMOUNT.FCY28
        XX11<1,1>[95,15]  = AMOUNT.TOTAL.FCY7
        XX11<1,1>[115,15] = AMOUNT.TOTAL.LCY7
        PRINT XX11<1,1>
*----------------------------------------------
        XX12 = SPACE(120)
        XX12<1,1>[1,1]   = "������ ���������"
        XX12<1,1>[25,15]  = Y.AMOUNT.FCY29
        XX12<1,1>[45,15]  = Y.AMOUNT.FCY30
        XX12<1,1>[63,15]  = Y.AMOUNT.FCY31
        XX12<1,1>[80,15]  = Y.AMOUNT.FCY32
        XX12<1,1>[95,15] = AMOUNT.TOTAL.FCY8
        XX12<1,1>[115,15] = AMOUNT.TOTAL.LCY8
        PRINT XX12<1,1>
*-----------------------------------------------
        XX13 = SPACE(120)
        XX13<1,1>[1,15]   = "�������� �������"
        XX13<1,1>[25,15]  = Y.AMOUNT.FCY33
        XX13<1,1>[45,15]  = Y.AMOUNT.FCY34
        XX13<1,1>[63,15]  = Y.AMOUNT.FCY35
        XX13<1,1>[80,15]  = Y.AMOUNT.FCY36
        XX13<1,1>[95,15]  = AMOUNT.TOTAL.FCY9
        XX13<1,1>[115,15] = AMOUNT.TOTAL.LCY9
        PRINT XX13<1,1>
        PRINT STR('=',130)
*--------------------------------------------------
        XX14 = SPACE(120)
        XX14<1,1>[1,15]   = "����������������"
        XX14<1,1>[25,15]  = AMOUNT.TOTAL13
        XX14<1,1>[45,15]  = AMOUNT.TOTAL14
        XX14<1,1>[63,15]  = AMOUNT.TOTAL15
        XX14<1,1>[80,15]  = AMOUNT.TOTAL16
        XX14<1,1>[95,15]  = AMOUNT.TOTAL17
        XX14<1,1>[115,15] = AMOUNT.TOTAL18
        PRINT XX14<1,1>
        PRINT STR('=',130)
        PRINT STR(' ',130)
*----------------------------------------------------------
        XX15 = SPACE(120)
        XX15<1,1>[1,15]   = "�������������"
        PRINT XX15<1,1>
        PRINT STR('*',20)
*--------------------------------------------------
        XX16 = SPACE(120)
        XX16<1,1>[1,15]   = "������� ��������"
        XX16<1,1>[25,15]  = Y.AMOUNT.FCY37
        XX16<1,1>[45,15]  = Y.AMOUNT.FCY38
        XX16<1,1>[63,15]  = Y.AMOUNT.FCY39
        XX16<1,1>[80,15]  = Y.AMOUNT.FCY40
        XX16<1,1>[95,15]  = AMOUNT.TOTAL.FCY10
        XX16<1,1>[115,15] = AMOUNT.TOTAL.LCY10
        PRINT XX16<1,1>
*----------------------------------------------
        XX17 = SPACE(120)
        XX17<1,1>[1,1]   = "������ ���������"
        XX17<1,1>[25,15]  = Y.AMOUNT.FCY41
        XX17<1,1>[45,15]  = Y.AMOUNT.FCY42
        XX17<1,1>[63,15]  = Y.AMOUNT.FCY43
        XX17<1,1>[80,15]  = Y.AMOUNT.FCY44
        XX17<1,1>[95,15] = AMOUNT.TOTAL.FCY11
        XX17<1,1>[115,15] = AMOUNT.TOTAL.LCY11
        PRINT XX17<1,1>
*-----------------------------------------------
        XX18 = SPACE(120)
        XX18<1,1>[1,15]   = "�������� �������"
        XX18<1,1>[25,15]  = Y.AMOUNT.FCY45
        XX18<1,1>[45,15]  = Y.AMOUNT.FCY46
        XX18<1,1>[63,15]  = Y.AMOUNT.FCY47
        XX18<1,1>[80,15]  = Y.AMOUNT.FCY48
        XX18<1,1>[95,15]  = AMOUNT.TOTAL.FCY12
        XX18<1,1>[115,15] = AMOUNT.TOTAL.LCY12
        PRINT XX18<1,1>
        PRINT STR('=',130)
*--------------------------------------------------
        XX19 = SPACE(120)
        XX19<1,1>[1,15]   = "����������������"
        XX19<1,1>[25,15]  = AMOUNT.TOTAL19
        XX19<1,1>[45,15]  = AMOUNT.TOTAL20
        XX19<1,1>[63,15]  = AMOUNT.TOTAL21
        XX19<1,1>[80,15]  = AMOUNT.TOTAL22
        XX19<1,1>[95,15]  = AMOUNT.TOTAL23
        XX19<1,1>[115,15] = AMOUNT.TOTAL24
        PRINT XX19<1,1>
        PRINT STR('=',130)
        PRINT STR(' ',130)
        PRINT STR('*',130)
        XX20 = SPACE(120)
        XX20<1,1>[1,15]   = "��������� �������"
        XX20<1,1>[25,15]  = TOTAL1
        XX20<1,1>[45,15]  = TOTAL2
        XX20<1,1>[63,15]  = TOTAL3
        XX20<1,1>[80,15]  = TOTAL4
        XX20<1,1>[95,15]  = TOTAL5
        XX20<1,1>[115,15] = TOTAL6
        PRINT XX20<1,1>
        PRINT STR('*',130)

    END
*************************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)


    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ����� ������� �������� ��������"
    PR.HD :="'L'":SPACE(60):"**":"  ":T.DAY::"  ":"**"
    PR.HD :="'L'":SPACE(48):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������":SPACE(15):"����� ������":SPACE(7):"���� ��������":SPACE(7):"����":SPACE(7):"����� ����":SPACE(7):"������ �����":SPACE(7):"������� �������"
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
