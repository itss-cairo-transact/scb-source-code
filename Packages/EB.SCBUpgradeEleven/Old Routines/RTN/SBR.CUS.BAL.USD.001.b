* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>508</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.CUS.BAL.USD.001

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND.DEP
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN

*-------------------------------------------------------------------------
INITIATE:
    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBR.CUS.BAL.USD'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.FD = 'F.SCB.FUND.DEP' ; F.FD = ''
    CALL OPF(FN.FD,F.FD)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.NAT = 'F.COUNTRY' ; F.NAT = ''
    CALL OPF(FN.NAT,F.NAT)

    FN.CB = 'FBNK.CUSTOMER.ACCOUNT' ; F.CB = ''
    CALL OPF(FN.CB,F.CB)


    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    AMT.TOTAL = 0 ; DB.BAL = 0

    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,E3)
    RATE.USD = R.CUR<EB.CUR.MID.REVAL.RATE,1>

    RETURN

*========================================================================
PROCESS:
    T.SEL = "SELECT ":FN.FD:" BY CUSTOMER.NO"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FD,KEY.LIST<I>,R.FD,F.FD,E1)
            CALL F.READ(FN.FD,KEY.LIST<I+1>,R.FD1,F.FD,E11)

            CUS.ID    = R.FD<FD.CUSTOMER.NO>
            CUS.ID1   = R.FD1<FD.CUSTOMER.NO>
            CUST.NAME = R.FD<FD.CUSTOMER.NAME>
            WS.CUR    = R.FD<FD.CURRENCY>

            CALL F.READ(FN.CUR,WS.CUR,R.CUR,F.CUR,E4)
            RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>

            IF WS.CUR EQ "EGP" THEN
                RATE = 1
            END

            IF WS.CUR EQ "JPY" THEN
                RATE = RATE / 100
            END

            AMT.TOTAL.O  = R.FD<FD.TOTAL.AMT>
            AMT.TOTAL   += AMT.TOTAL.O * RATE

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

            CUS.COMP = R.CU<EB.CUS.COMPANY.BOOK>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CUS.COMP,CUS.BRN)

            CUS.SECTOR = R.CU<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            CUS.NATION = R.CU<EB.CUS.NATIONALITY>
            CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,CUS.NATION,CUS.COUNTRY)

            IF CUS.ID NE CUS.ID1 THEN

                AMT.TOTAL.USD = AMT.TOTAL / RATE.USD
                AMT.TOTAL.USD = DROUND(AMT.TOTAL.USD,'2')

                IF (CUS.SECTOR EQ '4650' AND AMT.TOTAL.USD GE 1000000) THEN
**** DEBIT ACCOUNT *****
                    CALL F.READ(FN.CB,CUS.ID,R.CB,F.CB,E12)
                    LOOP
                        REMOVE AC.ID FROM R.CB SETTING POS.ACCT
                    WHILE AC.ID:POS.ACCT
                        CALL F.READ(FN.ACCT,AC.ID,R.ACCT,F.ACCT,ER.ACCT)
                        AC.CATEG = R.ACCT<AC.CATEGORY>
                        AC.BAL   = R.ACCT<AC.ONLINE.ACTUAL.BAL>
                        AC.CUR   = R.ACCT<AC.CURRENCY>

                        CALL F.READ(FN.CUR,AC.CUR,R.CUR,F.CUR,E4)
                        AC.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>

                        IF AC.CUR EQ "EGP" THEN
                            AC.RATE = 1
                        END

                        IF AC.CUR EQ "JPY" THEN
                            AC.RATE = AC.RATE / 100
                        END

                        IF (AC.CATEG GE 1100 AND AC.CATEG LE 1599) AND (AC.BAL LT 0) THEN
                            DB.BAL += AC.BAL * AC.RATE
                        END
                    REPEAT
                    DB.AMT.USD = DB.BAL / RATE.USD
                    DB.AMT.USD = DROUND(DB.AMT.USD,'2')

************************
                    AMT.TOTAL.USD = FMT(AMT.TOTAL.USD, "L2,")
                    DB.AMT.USD    = FMT(DB.AMT.USD, "L2,")

                    XX  = SPACE(132)
                    XX<1,I>[1,8]    = CUS.ID
                    XX<1,I>[12,35]  = CUST.NAME
                    XX<1,I>[52,20]  = CUS.BRN
                    XX<1,I>[72,20]  = AMT.TOTAL.USD
                    XX<1,I>[90,20]  = DB.AMT.USD
                    XX<1,I>[105,20] = CUS.COUNTRY

                    PRINT XX<1,I>
                END

                CUS.BRN       = ''
                CUS.SECTOR    = ''
                CUS.COUNTRY   = ''
                CUST.NAME     = ''
                AMT.TOTAL     = 0
                AMT.TOTAL.O   = 0
                AMT.TOTAL.USD = 0
                DB.BAL        = 0

            END
        NEXT I

        XX1 = SPACE(132)
        PRINT STR(' ',120)
        XX1<1,1>[55,30] = "***** ����� �������  *****"
        PRINT XX1<1,1>

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"���� ������ ������� ������� ����� ���� ������� ������� �� ����� ����� ������"
    PR.HD :="'L'":SPACE(55):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(5):"��� ������":SPACE(25):"�����":SPACE(17):"������" :SPACE(8):"����� ���������" :SPACE(5):"�������"
    PR.HD :="'L'":SPACE(72):"���� ��������":SPACE(2):"����� ��������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*===============================================================
END
