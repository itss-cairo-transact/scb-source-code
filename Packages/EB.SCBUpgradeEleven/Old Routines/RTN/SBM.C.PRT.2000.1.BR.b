* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1151</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.2000.1.BR
*    PROGRAM     SBM.C.PRT.2000.1.BR
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
*    $INSERT GLOBUS.BP  I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
***"����� ��� "
****  2000
*------------------------------------------
    FN.CUS = "F.CUSTOMER"
    F.CUS  = ""

    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""
*    FN.BR = "F.DEPT.ACCT.OFFICER"
*    F.BR = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*----------------------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CUS,F.CUS)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.TO = 0
    WS.SECTOR.NAME = ""
    WS.CUS.KEY = ""
    MSG.CUS = ""
    WS.CUS.NAME = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.PAG.COUNT = 0
    WS.LIN.COUNT = 0

    WS.1.LE = "0"
    WS.2.LE = "0"
    WS.3.LE = "0"

    WS.1.EQV = "0"
    WS.2.EQV = "0"
    WS.3.EQV = "0"

    WS.1.LE.LIN = 0
    WS.2.LE.LIN = 0
    WS.3.LE.LIN = 0
    WS.1.EQV.LIN = 0
    WS.2.EQV.LIN = 0
    WS.3.EQV.LIN = 0

    WS.1.LE.TOT  = 0
    WS.2.LE.TOT  = 0
    WS.3.LE.TOT  = 0
    WS.1.EQV.TOT  = 0
    WS.2.EQV.TOT  = 0
    WS.3.EQV.TOT  = 0
    SUB.A = 0

    WS.TO.COMPAR = 0
    WS.NAME = ""
    WS.DPST = "0"
    WS.DPST1 = "0"
    WS.CER = "0"
    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.INDST.COMP = 0
    FLAG.PRT.OK = 0
    WS.HD.T  = "����� ����� ������� � ��������� ���������� � ������� �������� "

    WS.HD.TA = " ����� ��� 2000 "

    WS.HD.T2 = "�������� ������ ���� ������� �����"
    WS.HD.T2A = "����    "
    WS.HD.T3  = "������ ���� ����"

    WS.HD.1  = "���� �����"
    WS.HD.1A = "���� ������"
*------------------------------
    WS.HD.2  = "���"

    WS.HD.2A = "����"

    WS.HD.2B = "�����"

    WS.HD.2D = "�������"

    WS.HD.2E = "��������"

*-----------------------------------
    WS.HD.3 = "�����"

    WS.HD.3A = "�����"

    WS.HD.3B = "������"
*-------------------------------
    WS.HD.4A  = "�������"
*--------------------------------------
    WS.PRG.1 = "SBM.C.PRT.2000.1.BR"
*------------------------------------------------

    DIM ARRAY1(13,9)

    ARRAY1(1,1) = "������ ������� ����� � ����� � ������ "
    ARRAY1(1,2) = "100"
    ARRAY1(1,3) = "101"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"
    ARRAY1(1,6) = "0"
    ARRAY1(1,7) = "0"
    ARRAY1(1,8) = "0"
    ARRAY1(1,9) = "0"

    ARRAY1(2,1) = "������ ������� �������                "
    ARRAY1(2,2) = "200"
    ARRAY1(2,3) = "201"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = "0"
    ARRAY1(2,7) = "0"
    ARRAY1(2,8) = "0"
    ARRAY1(2,9) = "0"


    ARRAY1(3,1) = "������ ������� �������� ��������      "
    ARRAY1(3,2) = "300"
    ARRAY1(3,3) = "301"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"
    ARRAY1(3,8) = "0"
    ARRAY1(3,9) = "0"


    ARRAY1(4,1) = "������ ������� �������� ���������     "
    ARRAY1(4,2) = "400"
    ARRAY1(4,3) = "401"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"
    ARRAY1(4,8) = "0"
    ARRAY1(4,9) = "0"


    ARRAY1(5,1) = "������ ������� �������� ��������      "
    ARRAY1(5,2) = "500"
    ARRAY1(5,3) = "501"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = "0"
    ARRAY1(5,7) = "0"
    ARRAY1(5,8) = "0"
    ARRAY1(5,9) = "0"


    ARRAY1(6,1) = "������ ������� ������� � �������      "
    ARRAY1(6,2) = "600"
    ARRAY1(6,3) = "601"
    ARRAY1(6,4) = "0"
    ARRAY1(6,5) = "0"
    ARRAY1(6,6) = "0"
    ARRAY1(6,7) = "0"
    ARRAY1(6,8) = "0"
    ARRAY1(6,9) = "0"


    ARRAY1(7,1) = "������ ������� ������� ������ ������   "
    ARRAY1(7,2) = "700"
    ARRAY1(7,3) = "701"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"
    ARRAY1(7,6) = "0"
    ARRAY1(7,7) = "0"
    ARRAY1(7,8) = "0"
    ARRAY1(7,9) = "0"


    ARRAY1(8,1) = "������ ������� ����� ����� � ������   "
    ARRAY1(8,2) = "800"
    ARRAY1(8,3) = "801"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = "0"
    ARRAY1(8,7) = "0"
    ARRAY1(8,8) = "0"
    ARRAY1(8,9) = "0"


    ARRAY1(9,1) = "������ ������� ������� ����������� ����"
    ARRAY1(9,2) = "900"
    ARRAY1(9,3) = "901"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = "0"
    ARRAY1(9,7) = "0"
    ARRAY1(9,8) = "0"
    ARRAY1(9,9) = "0"


    ARRAY1(10,1) = "������ ������� ���� �������          "
    ARRAY1(10,2) = "1000"
    ARRAY1(10,3) = "1001"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = "0"
    ARRAY1(10,7) = "0"
    ARRAY1(10,8) = "0"
    ARRAY1(10,9) = "0"


    ARRAY1(11,1) = "������ ������� ������� ��� ������    "
    ARRAY1(11,2) = "1100"
    ARRAY1(11,3) = "1101"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = "0"
    ARRAY1(11,7) = "0"
    ARRAY1(11,8) = "0"
    ARRAY1(11,9) = "0"


    ARRAY1(12,1) = "������ ������� �������� � ������� ����"
    ARRAY1(12,2) = "1200"
    ARRAY1(12,3) = "1201"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = "0"
    ARRAY1(12,7) = "0"
    ARRAY1(12,8) = "0"
    ARRAY1(12,9) = "0"


    ARRAY1(13,1) = "������ ������� ����� ����� ���� ���  "
    ARRAY1(13,2) = "1300"
    ARRAY1(13,3) = "1301"
    ARRAY1(13,4) = "0"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = "0"
    ARRAY1(13,7) = "0"
    ARRAY1(13,8) = "0"
    ARRAY1(13,9) = "0"


********************** ********************************
*******************   PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    WS.BRX = ID.COMPANY
    GOSUB A.050.GET.ALL.BR
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*----------------------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" WITH @ID EQ ":WS.BRX
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END

*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        FLAG.PRT  = 0
*-----------------------------------------------------------------
*?????????

        GOSUB A.060.CHK.INDSTRY
        GOSUB A.5000.PRT.HEAD
        GOSUB A.700.PRT.COMPANY
*
A.050.A:
    REPEAT
    RETURN
*----------------------------------------------------------------
A.060.CHK.INDSTRY:
    FOR SUB.A  = 1 TO 13

        WS.INDST.FRM =  ARRAY1(SUB.A,2)
        WS.INDST.TO  =  ARRAY1(SUB.A,3)
        GOSUB A.100.PROCESS
    NEXT SUB.A
    RETURN
*----------------------------------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE DD... AND CBE.NEW.INDUSTRY GE ":WS.INDST.FRM:" AND CBE.NEW.INDUSTRY LE ":WS.INDST.TO:
    SEL.CMD := " AND CBE.BR EQ ":WS.BR

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS

        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        WS.BRX = R.CBE<P.CBE.BR>

        IF WS.BRX NE WS.BR THEN
            GOTO START.B
        END
A.100.A:
        WS.NEW.SECTOR = R.CBE<P.CBE.NEW.SECTOR>
*                                      ������� �� ����� ���� ������� �����
        IF WS.NEW.SECTOR EQ 1120 OR WS.NEW.SECTOR EQ  2120 OR WS.NEW.SECTOR EQ 3120 OR WS.NEW.SECTOR EQ 4120 THEN
            GOTO START.A
        END

        GOTO START.B


START.A:
        WS.INDSTRY = R.CBE<P.CBE.NEW.INDUSTRY>


START.A1:
        WS.1.LE = 0
        WS.2.LE = 0
        WS.3.LE = 0

        WS.1.EQV = 0
        WS.2.EQV = 0
        WS.3.EQV = 0

        WS.DPST = 0
        WS.DPST1 = 0
        WS.CER = 0

        WS.1.LE = R.CBE<P.CBE.CUR.AC.LE>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.FACLTY.LE.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.S.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.M.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.L.CR>



        WS.DPST  = R.CBE<P.CBE.DEPOST.AC.LE.1Y> + R.CBE<P.CBE.DEPOST.AC.LE.2Y>
        WS.DPST1 = R.CBE<P.CBE.DEPOST.AC.LE.3Y> + R.CBE<P.CBE.DEPOST.AC.LE.MOR3>
        WS.CER  = R.CBE<P.CBE.CER.AC.LE.3Y> + R.CBE<P.CBE.CER.AC.LE.5Y> + R.CBE<P.CBE.CER.AC.LE.GOLD>
        WS.2.LE = WS.DPST + WS.DPST1 + WS.CER + R.CBE<P.CBE.SAV.AC.LE>

        WS.3.LE = R.CBE<P.CBE.BLOCK.AC.LE> + R.CBE<P.CBE.MARG.LC.LE> + R.CBE<P.CBE.MARG.LG.LE>


        WS.1.EQV = R.CBE<P.CBE.CUR.AC.EQ>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.FACLTY.EQ.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.S.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.M.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.L.CR>

        FLAG.PRT = 1
        WS.DPST = 0
        WS.DPST1 = 0
        WS.CER = 0

        WS.DPST  = R.CBE<P.CBE.DEPOST.AC.EQ.1Y> + R.CBE<P.CBE.DEPOST.AC.EQ.2Y>
        WS.DPST1 = R.CBE<P.CBE.DEPOST.AC.EQ.3Y> + R.CBE<P.CBE.DEPOST.AC.EQ.MOR3>
        WS.CER  = R.CBE<P.CBE.CER.AC.EQ.3Y> + R.CBE<P.CBE.CER.AC.EQ.5Y> + R.CBE<P.CBE.CER.AC.EQ.GOLD>
        WS.2.EQV = WS.DPST + WS.DPST1 + WS.CER + R.CBE<P.CBE.SAV.AC.EQ>

        WS.3.EQV = R.CBE<P.CBE.BLOCK.AC.EQ> + R.CBE<P.CBE.MARG.LC.EQ> + R.CBE<P.CBE.MARG.LG.EQ>

        GOSUB A.200.ACUMULAT
*-----------------------------------------------------
START.B:
    REPEAT
    RETURN

A.200.ACUMULAT:
    ARRAY1(SUB.A,4) = ARRAY1(SUB.A,4) + WS.1.LE
    ARRAY1(SUB.A,5) = ARRAY1(SUB.A,5) + WS.2.LE
    ARRAY1(SUB.A,6) = ARRAY1(SUB.A,6) + WS.3.LE

    ARRAY1(SUB.A,7) = ARRAY1(SUB.A,7) + WS.1.EQV
    ARRAY1(SUB.A,8) = ARRAY1(SUB.A,8) + WS.2.EQV
    ARRAY1(SUB.A,9) = ARRAY1(SUB.A,9) + WS.3.EQV

    RETURN

*--------------------------------------------------------
A.700.PRT.COMPANY:
    IF FLAG.PRT EQ 0 THEN
        RETURN
    END
    FOR SUB.A  = 1 TO 13
        GOSUB A.710.PRT
    NEXT SUB.A
    RETURN
A.710.PRT:
*-----------------------------------------

    WS.1.LE.LIN  = ARRAY1(SUB.A,4) / 1000
    WS.2.LE.LIN  = ARRAY1(SUB.A,5) / 1000
    WS.3.LE.LIN  = ARRAY1(SUB.A,6) / 1000

    WS.1.EQV.LIN = ARRAY1(SUB.A,7) / 1000
    WS.2.EQV.LIN = ARRAY1(SUB.A,8) / 1000
    WS.3.EQV.LIN = ARRAY1(SUB.A,9) / 1000

    WS.1.LE.TOT = WS.1.LE.TOT + WS.1.LE.LIN
    WS.2.LE.TOT = WS.2.LE.TOT + WS.2.LE.LIN
    WS.3.LE.TOT = WS.3.LE.TOT + WS.3.LE.LIN

    WS.1.EQV.TOT = WS.1.EQV.TOT + WS.1.EQV.LIN
    WS.2.EQV.TOT = WS.2.EQV.TOT + WS.2.EQV.LIN
    WS.3.EQV.TOT = WS.3.EQV.TOT + WS.3.EQV.LIN

    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(SUB.A,1)
    XX<1,1>[37,9]   = FMT(WS.1.LE.LIN, "R0")
    XX<1,1>[47,9]   = FMT(WS.2.LE.LIN, "R0")
    XX<1,1>[57,9]   = FMT(WS.3.LE.LIN, "R0")

    WS.COMN = WS.1.LE.LIN + WS.2.LE.LIN + WS.3.LE.LIN

    XX<1,1>[67,9]   = FMT(WS.COMN, "R0")
    XX<1,1>[80,9]  = FMT(WS.1.EQV.LIN, "R0")
    XX<1,1>[90,9]  = FMT(WS.2.EQV.LIN, "R0")
    XX<1,1>[99,9]  = FMT(WS.3.EQV.LIN, "R0")

    WS.COMN1 = WS.1.EQV.LIN + WS.2.EQV.LIN + WS.3.EQV.LIN

    XX<1,1>[108,9] = FMT(WS.COMN1, "R0")
    WS.COMN = WS.COMN + WS.COMN1
    XX<1,1>[120,9] = FMT(WS.COMN, "R0")
    PRINT XX<1,1>

    WS.1.LE.LIN = 0
    WS.2.LE.LIN = 0
    WS.3.LE.LIN = 0

    WS.1.EQV.LIN = 0
    WS.2.EQV.LIN = 0
    WS.3.EQV.LIN = 0

    ARRAY1(SUB.A,4) = 0
    ARRAY1(SUB.A,5) = 0
    ARRAY1(SUB.A,6) = 0

    ARRAY1(SUB.A,7) = 0
    ARRAY1(SUB.A,8) = 0
    ARRAY1(SUB.A,9) = 0
    IF SUB.A EQ 13 THEN
        GOSUB A.800.TOT
    END
    RETURN
*------------------------------------
A.800.TOT:
    XX = SPACE(132)
    XX<1,1>[37,9]   = "========="
    XX<1,1>[47,9]   = "========="
    XX<1,1>[57,9]   = "========="
    XX<1,1>[67,9]   = "========="

    XX<1,1>[80,9]  = "========="
    XX<1,1>[90,9]  = "========="
    XX<1,1>[99,9]  = "========="
    XX<1,1>[108,9] = "========="
    XX<1,1>[120,9] = "========="
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[1,35]   = "������ ����� ���� ������� �����  "

    XX<1,1>[37,9]   = FMT(WS.1.LE.TOT, "R0")
    XX<1,1>[47,9]   = FMT(WS.2.LE.TOT, "R0")
    XX<1,1>[57,9]   = FMT(WS.3.LE.TOT, "R0")
    WS.COMN = WS.1.LE.TOT + WS.2.LE.TOT + WS.3.LE.TOT
    XX<1,1>[67,9]   = FMT(WS.COMN, "R0")


    XX<1,1>[80,9]  = FMT(WS.1.EQV.TOT, "R0")
    XX<1,1>[90,9]  = FMT(WS.2.EQV.TOT, "R0")
    XX<1,1>[99,9]  = FMT(WS.3.EQV.TOT, "R0")
    WS.COMN1 = WS.1.EQV.TOT + WS.2.EQV.TOT + WS.3.EQV.TOT
    XX<1,1>[108,9] = FMT(WS.COMN1, "R0")
    WS.COMN = WS.COMN + WS.COMN1
    XX<1,1>[120,9] = FMT(WS.COMN, "R0")
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[37,9]   = "========="
    XX<1,1>[47,9]   = "========="
    XX<1,1>[57,9]   = "========="
    XX<1,1>[67,9]   = "========="

    XX<1,1>[80,9]  = "========="
    XX<1,1>[90,9]  = "========="
    XX<1,1>[99,9]  = "========="
    XX<1,1>[108,9] = "========="
    XX<1,1>[120,9] = "========="
    PRINT XX<1,1>

*    GOSUB A.5100.PRT.SPACE.PAGE
    WS.1.LE.TOT = 0
    WS.2.LE.TOT = 0
    WS.3.LE.TOT = 0
    WS.1.EQV.TOT = 0
    WS.2.EQV.TOT = 0
    WS.3.EQV.TOT = 0
    RETURN
*-------------------------------------------------------------------------
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    IF FLAG.PRT EQ 0 THEN
        RETURN
    END
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

*    CALL F.READ(FN.BR,WS.BR.H,R.BR,F.BR,MSG.BR)
*    WS.BR.NAME = R.BR<EB.DAO.NAME>
***    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
***    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ "
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    WS.PAG.COUNT = WS.PAG.COUNT + 1
    PR.HD :="'L'":SPACE(50):WS.HD.T2:SPACE(28):WS.HD.T2A:SPACE(2):WS.PAG.COUNT
    PR.HD :="'L'":SPACE(110):WS.HD.T3

    PR.HD :="'L'":SPACE(54):WS.HD.1:SPACE(27):WS.HD.1A
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(36):WS.HD.2:SPACE(7):WS.HD.2A:SPACE(6):WS.HD.2B:SPACE(5):WS.HD.2D:SPACE(5):WS.HD.2:SPACE(8):WS.HD.2A:SPACE(5):WS.HD.2B:SPACE(3):WS.HD.2D:SPACE(7):WS.HD.2E
    PR.HD :="'L'":SPACE(36):WS.HD.3:SPACE(5):WS.HD.3A:SPACE(6):WS.HD.3B:SPACE(17):WS.HD.3:SPACE(3):WS.HD.3A:SPACE(3):WS.HD.3B
    PR.HD :="'L'":SPACE(46):WS.HD.4A:SPACE(36):WS.HD.4A
****    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD
    PRINT
    RETURN
A.5001.PRT.HEAD:
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    WS.PAG.COUNT = WS.PAG.COUNT + 1
    PR.HD :="'L'":SPACE(50):WS.HD.T2:SPACE(28):WS.HD.T2A:SPACE(2):WS.PAG.COUNT
    PR.HD :="'L'":SPACE(110):WS.HD.T3
    PR.HD :="'L'":SPACE(54):WS.HD.1:SPACE(27):WS.HD.1A
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(36):WS.HD.2:SPACE(7):WS.HD.2A:SPACE(6):WS.HD.2B:SPACE(5):WS.HD.2D:SPACE(5):WS.HD.2:SPACE(8):WS.HD.2A:SPACE(5):WS.HD.2B:SPACE(3):WS.HD.2D:SPACE(7):WS.HD.2E
    PR.HD :="'L'":SPACE(36):WS.HD.3:SPACE(5):WS.HD.3A:SPACE(6):WS.HD.3B:SPACE(17):WS.HD.3:SPACE(3):WS.HD.3A:SPACE(3):WS.HD.3B
    PR.HD :="'L'":SPACE(46):WS.HD.4A:SPACE(36):WS.HD.4A
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
*????????
A.5100.PRT.SPACE.PAGE:
*    IF FLAG.FRST EQ 0 THEN
*        FLAG.FRST = 1
*        RETURN
*    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
