* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>23074</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.CREDIT.CBE.K2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE.HO
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMP.LOANS
*-----------------------------------------------------------------------
    GOSUB CLEARFILE
    PRINT "ORGINAL FILE CLEARED"

    GOSUB INITIATE
    GOSUB INIT0
    GOSUB INIT0CR

    GOSUB GETAC
    PRINT "AC FINISH"

    GOSUB GETLC
    PRINT "LC FINISH"

    GOSUB GETDR
    PRINT "DR FINISH"

    GOSUB GETLG
    PRINT "LG FINISH"

    GOSUB GETLOAN
    PRINT "LOAN FINISH"

    GOSUB GETPD
    PRINT "PD FINISH"

    GOSUB GETLIMT
    PRINT "LIMT FINISH"

    GOSUB CHKALL
    PRINT "FINISH CHKALL"

***    GOSUB INSGUR
***    PRINT "FINISH INSGUR"

    RETURN

*==============================================================
CLEARFILE:
    FN.CCBE = "F.SCB.CREDIT.CBE.HO"
    F.CCBE = ''
    CALL OPF(FN.CCBE,F.CCBE)
    OPEN FN.CCBE TO FILEVAR ELSE ABORT 201, FN.CCBE
    CLEARFILE FILEVAR

    RETURN
*==============================================================

INITIATE:

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    R.AC = ''

    FN.CUS = 'F.CUSTOMER'
    F.CUS = ''

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)
    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    FN.CRDT.CBE = 'F.SCB.CREDIT.CBE.HO'
    F.CRDT.CBE = ''
    R.CRDT.CBE = ''
    R.CRDT.CBE1 = ''

    FN.LMT = 'F.LIMIT'
    F.LMT = ''

    FN.COL = 'F.COLLATERAL'
    F.COL = ''

    FN.CCY = 'FBNK.CURRENCY' ;   F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CUS,F.CUS)
    CALL OPF(FN.CRDT.CBE,F.CRDT.CBE)
    CALL OPF(FN.LMT,F.LMT)

    CCC.NO = 0
    TOTALTM = 0 ; TOTALTM44= 0 ; TOTAL = 0 ; TOT507 = 0 ; TOT512 = 0
    TOTALTMLC = 0 ; TOTALTMLG = 0
    COLLC = 0 ; COLLG = 0 ; COLLC.AMT = 0 ; COLLG.AMT = 0 ; LMT.AMT = 0 ; RATE = 0

    BNK.DATE1 = TODAY
    CALL CDT("",BNK.DATE1,'-1C')

    RETURN
*==============================================================
INIT0:
    R.AC = "" ; SWCON = 0 ; SWMARG = ''
    RETURN
*==============================================================
INIT0CR:
*----------------- "����� �������� � ������ �� 0 " ------------------
    TOTALTM = 0 ; TOTALTM44= 0 ; TOTAL = 0 ; TOT507 = 0 ; TOT512 = 0
    TOTALTMLC = 0 ; TOTALTMLG = 0
    COLLC = 0 ; COLLG = 0 ; COLLC.AMT = 0 ; COLLG.AMT = 0 ; LMT.AMT = 0 ; RATE = 0
    SWCON = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.US.1>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.2>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.2>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.3>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.3>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.4>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.4>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.5>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.5>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.6>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.6>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.7>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.7>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.8>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.8>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.9>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.9>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> = 0
    RETURN
*==============================================================
INIT0CRGUR:
*----------------- "����� �������� � ������ �� 0 " ------------------
    TOTALTM = 0 ; TOTALTM44= 0 ; TOTAL = 0 ; TOT507 = 0 ; TOT512 = 0
    TOTALTMLC = 0 ; TOTALTMLG = 0
    COLLC = 0 ; COLLG = 0 ; COLLC.AMT = 0 ; COLLG.AMT = 0 ; LMT.AMT = 0 ; RATE = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.US.1>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.1>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.2>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.2>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.3>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.3>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.4>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.4>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.5>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.5>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.6>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.6>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.7>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.7>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.8>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.8>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.9>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.9>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.10> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.10> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.11> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.11> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.12> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.12> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.13> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.13> = 0
    RETURN
*==============================================================
GETAC:
    T.SEL = "SELECT ":FN.AC
    T.SEL := " WITH ((( CATEGORY GE 1101 AND CATEGORY LE 1599 )"
    T.SEL := " OR ( CATEGORY GE 1001 AND CATEGORY LE 1003 )"
    T.SEL := " OR CATEGORY EQ 9090 ) AND OPEN.ACTUAL.BAL LT 0 )"
    T.SEL := " BY CUSTOMER BY CATEGORY"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN

        FOR I = 1 TO SELECTED
            GOSUB INIT0
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*********************************
            CUS.ID = R.AC<AC.CUSTOMER>
            AC.ID = KEY.LIST<I>
            IF I = 1 THEN CCC.NO = CUS.ID
            IF CCC.NO # CUS.ID THEN
                GOSUB CALCAC
                GOSUB INSREC1
            END
*********************************
            CURR = R.AC<AC.CURRENCY>

**  LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**      RATE = R.BASE<RE.BCP.RATE,POS>
**  END

            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.AC<AC.OPEN.ACTUAL.BAL> * RATE

            IF TOTAL LT 0 THEN
                TOTAL = TOTAL * -1
            END
            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)

***************************** ���� ����
            IF ( R.AC<AC.CATEGORY> GE 1501 AND R.AC<AC.CATEGORY> LE 1599 ) OR R.AC<AC.CATEGORY> = 1205 OR R.AC<AC.CATEGORY> = 1207 OR ( R.AC<AC.CATEGORY> GE 1001 AND R.AC<AC.CATEGORY> LE 1003 ) OR R.AC<AC.CATEGORY> = 1220 OR R.AC<AC.CATEGORY> = 1216 OR R.AC<AC.CATEGORY> = 1217 OR R.AC<AC.CATEGORY> = 1230 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1> + TOTAL
            END
***************************** ����� ������
            IF R.AC<AC.CATEGORY> = 9090 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10> + TOTAL
            END
**************************** ����� �� ������
            IF (R.AC<AC.CATEGORY> GE 1301 AND R.AC<AC.CATEGORY> LE 1399) OR R.AC<AC.CATEGORY> EQ 1224 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2> + TOTAL
            END
***************************  ����� �����
            IF (R.AC<AC.CATEGORY> GE 1101 AND R.AC<AC.CATEGORY> LE 1199) OR R.AC<AC.CATEGORY> EQ 1223 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5> + TOTAL
            END
***************************      ��� �����
            IF R.AC<AC.CATEGORY> EQ 1202 OR R.AC<AC.CATEGORY> EQ 1212 OR R.AC<AC.CATEGORY> EQ 1221 OR R.AC<AC.CATEGORY> EQ 1203 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6> + TOTAL
            END
***************************      ��� ����
            IF R.AC<AC.CATEGORY> EQ 1201 OR R.AC<AC.CATEGORY> EQ 1211 OR R.AC<AC.CATEGORY> EQ 1222 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7> + TOTAL
            END
***************************    ����� �����
            IF R.AC<AC.CATEGORY> EQ 1404 OR R.AC<AC.CATEGORY> EQ 1414 OR R.AC<AC.CATEGORY> EQ 1225 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3> + TOTAL
            END
***************************    ������� ������
            IF (R.AC<AC.CATEGORY> GE 1401 AND R.AC<AC.CATEGORY> LE 1499 ) OR R.AC<AC.CATEGORY> EQ 1206 OR R.AC<AC.CATEGORY> EQ 1208 OR R.AC<AC.CATEGORY> EQ 1227 AND R.AC<AC.CATEGORY> NE 1404 AND  R.AC<AC.CATEGORY> NE 1414 AND R.AC<AC.CATEGORY> NE 1416 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8> + TOTAL
            END
****************************
***************************    ����� ������ ������
            IF R.AC<AC.CATEGORY> EQ 1416 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4> + TOTAL
            END
****************************
            CCC.NO = CUS.ID
****************************
        NEXT I
        IF I = SELECTED THEN
            GOSUB CALCAC
            GOSUB INSREC1
        END
    END
    RETURN
*============================================
GETLC:
*-----
    FN.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER.OF.CREDIT = '' ; R.LETTER.OF.CREDIT = ''
    TOTAL= 0  ; T.SEL = ""
    CCC.NO = '' ; CURR = '' ; SWLC = 0 ; SWMARG = ''

    CALL OPF( FN.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT)
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH APPLICANT.CUSTNO NE '' AND LIABILITY.AMT GT 0 AND CATEGORY.CODE NE '23200'"
    T.SEL := " BY APPLICANT.CUSTNO BY LC.CURRENCY BY SCB.TYPE"
***********************************
    KEY.LIST=""
    SELECTED=""
    KEY.LIST = ""
    KEY.LISTLC=""
    T.SELC=""
    SELECTEDLC=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            LC.ID = KEY.LIST<I>
            CALL F.READ(FN.LETTER.OF.CREDIT,KEY.LIST<I>, R.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT, ETEXT)

            APL.NO = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.CUSTNO>
            CUS.ID = APL.NO
*----------------------------------------------------------
            AC.ID = KEY.LISTLC<I>
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLC = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                CONT.ACC1 = 3010 ; CONT.ACC2 = 3012
                LMTREF1 = "0030000";LMTREF2 = "0020000"
                LMTREF3 = "1234567"
                SWMARG = 'LC'
                GOSUB CHKLIMT
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*----------------------------------------------------------
                GOSUB INSREC1
                SWLC = 1
            END
*----------------------------------------------------------
            IF SWLC = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELC)
                IF ELC THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.LETTER.OF.CREDIT<TF.LC.LC.CURRENCY>
*********************************

**  LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**      RATE = R.BASE<RE.BCP.RATE,POS>
**  END
            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.LETTER.OF.CREDIT<TF.LC.LIABILITY.AMT> * RATE

            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.11> += TOTAL
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLC = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED THEN
*----------------------------------------------------------
            CONT.ACC1 = 3010 ; CONT.ACC2 = 3012
            LMTREF1 = "0030000";LMTREF2 = "0020000"
            LMTREF3 = "1234567"
            SWMARG = 'LC'
            GOSUB CHKLIMT
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLC = 1
        END
    END
*----------------------------------------------------------
***********************************
    RETURN
*============================================
CHKLIMT:
*------
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    T.SEL1 = "SELECT ":FN.LMT:" WITH ( @ID LIKE ":CCC.NO:".":LMTREF1
    T.SEL1 := "... OR @ID LIKE ":CCC.NO:".":LMTREF2:"... OR @ID LIKE "
    T.SEL1 := CCC.NO:".":LMTREF3:"... ) AND INTERNAL.AMOUNT NE 0 AND"
    IF LMTREF1 = "0030000" THEN
        T.SEL1 := " NOTES NE 'CREATED BY SYSTEM"
    END ELSE
        T.SEL1 := " PRODUCT.ALLOWED EQ '' AND NOTES NE 'CREATED BY SYSTEM"
    END
    T.SEL1 := " DEFAULT' BY @ID"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF NOT(SELECTED1) THEN
*********************************
        CALL F.READ(FN.CUS.ACC,CCC.NO,R.CUS.ACC,F.CUS.ACC,ETEXT)
        LOOP
            REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
        WHILE AC.ID:POS.ACCT
            CALL F.READ(FN.AC,AC.ID, R.AC,F.AC,ETEXT)
            AC.CATEG = R.AC<AC.CATEGORY>
            AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
            IF (AC.CATEG GE CONT.ACC1 AND AC.CATEG LE CONT.ACC2) AND AC.BAL NE 0 THEN
                CURR = R.AC<AC.CURRENCY>
** LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**     RATE = R.BASE<RE.BCP.RATE,POS>
** END
                CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
                RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

                IF CURR EQ 'EGP' THEN
                    RATE = 1
                END
                IF CURR EQ 'JPY' THEN
                    RATE = RATE / 100
                END

                TOTAL = R.AC<AC.OPEN.ACTUAL.BAL> * RATE
                IF TOTAL LT 0 THEN
                    TOTAL = TOTAL * -1
                END ELSE
                    TOTALT = TOTAL
                END
**************************** MARGIN LETTER OF GRANTE
                IF SWMARG = 'LG' THEN
                    IF R.AC<AC.CATEGORY> EQ 3005 THEN
                        TOTALTMLG = TOTALTMLG + TOTAL
                    END
                END
**************************** MARGINE LETTER OF CREDIT
                IF SWMARG = 'LC' THEN
                    IF R.AC<AC.CATEGORY> = 3010 OR R.AC<AC.CATEGORY> = 3011 OR R.AC<AC.CATEGORY> = 3012 THEN
                        TOTALTMLC = TOTALTMLC + TOTAL
                    END
                END
**************************** MARGINE LETTER OF CREDIT 44 - 45
                IF SWMARG = 'DR' THEN
                    IF R.AC<AC.CATEGORY> = 3013 THEN
                        TOTALTM44 = TOTALTM44 + TOTAL
                    END
                END
****************************
            END
        REPEAT
        TOTALTMLC = TOTALTMLC / 1000   ; TOTALTMLC = DROUND(TOTALTMLC,0)
        TOTALTMLG = TOTALTMLG / 1000   ; TOTALTMLG = DROUND(TOTALTMLG,0)
        TOTALTM44 = TOTALTM44 / 1000   ; TOTALTM44 = DROUND(TOTALTM44,0)
*----------------------------------------------------------
        IF SWMARG = 'LC' THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.11> -= TOTALTMLC
        IF SWMARG = 'DR' THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.13> -= TOTALTM44
        IF SWMARG = 'LG' THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.12> -= TOTALTMLG
*----------------------------------------------------------
    END
    RETURN
*============================================
GETDR:
*-----
    FN.DRAWINGS = 'FBNK.DRAWINGS' ; F.DRAWINGS = '' ; R.DRAWINGS = ''

    TOTAL= 0  ; T.SEL = ""
    CCC.NO = '' ; CURR = '' ; SWDR = 0 ; SWMARG = ''

    CALL OPF( FN.DRAWINGS,F.DRAWINGS)

    T.SEL = "SELECT ":FN.DRAWINGS:" WITH MATURITY.REVIEW GT ":BNK.DATE1
    T.SEL := " AND CUSTOMER.LINK UNLIKE 99... BY CUSTOMER.LINK"
***********************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            DR.ID = KEY.LIST<I>
            CALL F.READ(FN.DRAWINGS,KEY.LIST<I>, R.DRAWINGS,F.DRAWINGS, ETEXT)
*----------------------------------------------------------
            CUS.ID = R.DRAWINGS<TF.DR.CUSTOMER.LINK>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWDR = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                CONT.ACC1 = 3013 ; CONT.ACC2 = 3013
                LMTREF1 = "0030000";LMTREF2 = "0020000"
                LMTREF3 = "1234567"
                SWMARG = 'DR'
                GOSUB CHKLIMT
*----------------------------------------------------------
                GOSUB INSREC1
                SWDR = 1
            END
*----------------------------------------------------------
            IF SWDR = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,EDR)
                IF EDR THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.DRAWINGS<TF.DR.DRAW.CURRENCY>
*********************************************************
** LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**     RATE = R.BASE<RE.BCP.RATE,POS>
** END
            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

            DR.AMT = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>

            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT> * RATE

            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.13> += TOTAL
            R.CRDT.CBE<SCB.C.CBE.APP.FLG>    = 'DR'
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWDR = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED THEN
*----------------------------------------------------------
            CONT.ACC1 = 3013 ; CONT.ACC2 = 3013
            LMTREF1 = "0030000";LMTREF2 = "0020000"
            LMTREF3 = "1234567"
            SWMARG = 'DR'
            GOSUB CHKLIMT
*----------------------------------------------------------
            GOSUB INSREC1
            SWDR = 1
        END
    END
*----------------------------------------------------------
***********************************
    RETURN
*============================================
GETLG:
*-----
    FN.LG = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LG = '' ; R.LG = ''
    TOTAL= 0  ; T.SEL = ""
    CCC.NO = '' ; CURR = '' ; SWLG = 0

    CALL OPF(FN.LG,F.LG)
    T.SEL  = "SELECT ":FN.LG:" WITH  CATEGORY EQ 21096"
    T.SEL := " AND AMOUNT NE 0 AND FIN.MAT.DATE GT ":BNK.DATE1
    T.SEL := " BY CUSTOMER.ID BY CURRENCY"
***********************************
    KEY.LIST=""
    SELECTED=""
    KEY.LIST = ""
    KEY.LISTLG=""
    T.SELC=""
    SELECTEDLG=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN

        FOR I = 1 TO SELECTED
            LG.ID = KEY.LIST<I>
            CALL F.READ(FN.LG,KEY.LIST<I>,R.LG,F.LG, ETEXT)

            CUS.ID = R.LG<LD.CUSTOMER.ID>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLG = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                CONT.ACC1 = 3005 ; CONT.ACC2 = 3005
                LMTREF1 = "0002505";LMTREF2 = "0002510"
                LMTREF3 = "0002520"
                SWMARG = 'LG'
                GOSUB CHKLIMT

*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*----------------------------------------------------------
                GOSUB INSREC1
                SWLG = 1
            END
*----------------------------------------------------------
            IF SWLG = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELG)
                IF ELG THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.LG<LD.CURRENCY>
*********************************

**           LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**               RATE = R.BASE<RE.BCP.RATE,POS>
**           END

            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.LG<LD.AMOUNT> * RATE

            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.12> += TOTAL
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLG = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED THEN
*----------------------------------------------------------
            CONT.ACC1 = 3005 ; CONT.ACC2 = 3005
            LMTREF1 = "0002505";LMTREF2 = "0002510"
            LMTREF3 = "0002520"
            SWMARG = 'LG'
            GOSUB CHKLIMT
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLC = 1
        END
*----------------------------------------------------------
    END
***********************************
    RETURN
*============================================
GETLOAN:
*-----
    FN.LON = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LON = '' ; R.LON = ''
    TOTAL= 0  ; T.SEL.LON = ""
    CCC.NO = '' ; CURR = '' ; SWLON = 0
    CALL OPF(FN.LON,F.LON)
    T.SEL.LON  = "SELECT ":FN.LON:" WITH  CATEGORY IN (21050 21051 21052 21053 21056 21057 21060 21062 21063)"
    T.SEL.LON := " AND AMOUNT NE 0 AND FIN.MAT.DATE GT ":BNK.DATE1
    T.SEL.LON := " BY CUSTOMER.ID BY CURRENCY"
***********************************
    KEY.LIST.LON=""
    SELECTED.LON=""
    KEY.LIST.LON = ""
    SELECTED.LON=""
    ER.MSG.LON=""
    CALL EB.READLIST(T.SEL.LON,KEY.LIST.LON,"",SELECTED.LON,ER.MSG.LON)
    IF KEY.LIST.LON THEN

        FOR I = 1 TO SELECTED.LON
            LON.ID = KEY.LIST.LON<I>
            CALL F.READ(FN.LON,KEY.LIST.LON<I>,R.LON,F.LON, ETEXT)

            CUS.ID = R.LON<LD.CUSTOMER.ID>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLON = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
                GOSUB INSREC1
                SWLON = 1
            END
*----------------------------------------------------------
            IF SWLON = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELON)
                IF ELON THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.LON<LD.CURRENCY>
*********************************
**  LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**      RATE = R.BASE<RE.BCP.RATE,POS>
**  END
            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.LON<LD.AMOUNT> * RATE
            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)

*----------------------------------------------------------
            CATEG.LON = R.LON<LD.CATEGORY>
            IF CATEG.LON = 21050 OR CATEG.LON = 21057  THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.6> += TOTAL
            END
            IF CATEG.LON = 21051 OR CATEG.LON = 21056 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.7> += TOTAL
            END
            IF CATEG.LON = 21052 OR CATEG.LON = 21060 OR CATEG.LON = 21062 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> += TOTAL
            END
            IF CATEG.LON = 21063 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.3> += TOTAL
            END
            IF CATEG.LON = 21053 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.8> += TOTAL
            END

*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLON = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED.LON THEN
*----------------------------------------------------------
            SWMARG = 'LON'
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLON = 1
        END
*----------------------------------------------------------
    END
***********************************
    RETURN

*============================================
GETPD:
*-----
    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD = ''
    TOTAL= 0  ; T.SEL.PD = ""
    CCC.NO = '' ; CURR = '' ; SWLON = 0
    CALL OPF(FN.PD,F.PD)
    T.SEL.PD  = "SELECT ":FN.PD:" WITH CATEGORY IN (21050 21051 21052 21053 21056 21057 21060 21062 21063) AND TOTAL.OVERDUE.AMT GT 0 BY CUSTOMER BY CURRENCY"

***********************************
    KEY.LIST.PD =""
    SELECTED.PD =""
    KEY.LIST.PD =""
    SELECTED.PD =""
    ER.MSG.PD   =""
    CALL EB.READLIST(T.SEL.PD,KEY.LIST.PD,"",SELECTED.PD,ER.MSG.PD)
    IF KEY.LIST.PD THEN
        FOR I = 1 TO SELECTED.PD
            PD.ID = KEY.LIST.PD<I>
            CALL F.READ(FN.PD,KEY.LIST.PD<I>,R.PD,F.PD, ETEXT)

            CUS.ID = R.PD<PD.CUSTOMER>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLON = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
                GOSUB INSREC1
                SWLON = 1
            END
*----------------------------------------------------------
            IF SWLON = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELON)
                IF ELON THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.PD<PD.CURRENCY>
*********************************
**  LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**      RATE = R.BASE<RE.BCP.RATE,POS>
**  END
            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.PD<PD.TOTAL.OVERDUE.AMT> * RATE
            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)

*----------------------------------------------------------
            CATEG.PD = R.PD<PD.CATEGORY>
            IF CATEG.PD = 21050 OR CATEG.PD = 21057  THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.6> += TOTAL
            END
            IF CATEG.PD = 21051 OR CATEG.PD = 21056 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.7> += TOTAL
            END
            IF CATEG.PD = 21052 OR CATEG.PD = 21060 OR CATEG.PD = 21062 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> += TOTAL
            END
            IF CATEG.PD = 21063 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.3> += TOTAL
            END
            IF CATEG.PD = 21053 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.8> += TOTAL
            END

*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLON = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED.PD THEN
*----------------------------------------------------------
            SWMARG = 'PD'
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLON = 1
        END
*----------------------------------------------------------
    END

    RETURN
************************************
GETLIMT:
*-------
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    R.LMT = "" ; CURR = '' ; SWLM = 0
    T.SEL1 = "SELECT ":FN.LMT
    T.SEL1 := " WITH ( INTERNAL.AMOUNT NE 0 AND PRODUCT.ALLOWED EQ ''"
    T.SEL1 := " AND NOTES NE 'CREATED BY SYSTEM DEFAULT' )"
    T.SEL1 := " OR (@ID LIKE ....0030000.... OR @ID LIKE ....0020000....) BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1  THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.LMT,KEY.LIST1<K>,R.LMT,F.LMT,E2)
*----------------------------------------------------------
            CUS.ID = FIELD(KEY.LIST1<K>,".",1)
            IF K = 1 THEN CCC.NO = CUS.ID ; SWLM = 1
            IF CCC.NO # CUS.ID THEN
                GOSUB CALCREC ; GOSUB INSREC
                GOSUB INIT0CR
                SWLM = 1
            END
*----------------------------------------------------------
            IF SWLM = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELM)
                IF ELM THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            LMT.REF = FIELD(KEY.LIST1<K>,".",2)
            LMT.REF = TRIM( LMT.REF, '0', 'L')
*----------------------------------------------------------
            CURR= R.LMT<LI.LIMIT.CURRENCY>

**   LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**   RATE = R.BASE<RE.BCP.RATE,POS>
**   END
            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.LMT<LI.INTERNAL.AMOUNT> * RATE

            TOTAL = TOTAL / 1000
            TOTAL = DROUND(TOTAL,0)
            LMT.AMT = 0 ; LMT.AMT = TOTAL
            GOSUB CALCLMT
            CCC.NO = CUS.ID ; SWLM = 0
        NEXT K
        IF K = SELECTED1 THEN GOSUB CALCREC ; GOSUB INSREC
        GOSUB INIT0CR
    END
    RETURN
*----------------------------------------------------------
CALCLMT:
*-------
    IF LMT.REF = 100 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 101 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 102 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 103 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 104 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 105 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 106 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 108 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 109 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 110 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> + LMT.AMT

    IF LMT.REF = 111 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 112 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 114 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 117 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 118 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 119 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 120 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 121 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 122 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 123 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 124 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 128 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 129 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 131 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 133 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> + LMT.AMT
    IF LMT.REF = 134 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 135 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 200 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 201 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 400 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 405 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 406 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 407 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 500 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 501 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 600 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 601 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 602 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 603 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 700 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 701 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 2010 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 2020 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 2060 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 2080 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 2090 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT

    IF LMT.REF = 2505 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 2510 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 2520 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 30000 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> + LMT.AMT
    IF LMT.REF = 20000 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> + LMT.AMT
    IF LMT.REF = 6010 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 6020 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT

******* ADD NEW LIMIT REF UPDATED BY KHALED 2010/01/24 ************
    IF LMT.REF = 8101 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 8102 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8103 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8104 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 8105 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8106 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8201 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 8202 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8203 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8204 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 8205 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 8206 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8207 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8208 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8209 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8210 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8211 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8212 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8213 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8301 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 8302 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8303 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 8401 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8402 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8501 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8502 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8601 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 408  THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 604  THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
*******************************************************************
    IF LMT.REF = 6201 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6202 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6203 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6006 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 6001 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 6003 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 6002 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> + LMT.AMT
    IF LMT.REF = 6004 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> + LMT.AMT

*******************************************************************
    IF LMT.REF EQ 20000 AND R.LMT<LI.COLLAT.RIGHT> EQ '' THEN
        GOSUB GETCOL20
        SWCON = 1
    END

    IF LMT.REF EQ 30000 OR ( LMT.REF EQ 20000 AND R.LMT<LI.COLLAT.RIGHT> GT 0 ) THEN
        GOSUB GETCOLLC
        SWCON = 1
    END

***********************
    IF LMT.REF = 2505 OR LMT.REF = 2510 OR LMT.REF = 2520 THEN
        GOSUB GETCOLLG
        SWCON = 1
    END
***********************
    RETURN
*---------------------------------------------------------------------
CALCREC:
*------
    IF SWCON = 1 THEN
        GOSUB CALCCONT
    END

    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11> - TOTALTMLC
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - TOTALTMLC
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - TOT507
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - TOT512
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + TOT507
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + TOT512

    COLLC = COLLC / 1000
    COLLC = DROUND(COLLC,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11> - COLLC
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - COLLC

    R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13> - TOTALTM44
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> - TOTALTM44

    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12> - TOTALTMLG
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> - TOTALTMLG

    COLLG = COLLG / 1000
    COLLG = DROUND(COLLG,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12> - COLLG
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> - COLLG

    RETURN
*---------------------------------------------------------------------
CALCCONT:
*------
    CALL F.READ(FN.CUS.ACC,CCC.NO,R.CUS.ACC,F.CUS.ACC,ETEXT)
    LOOP
        REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
    WHILE AC.ID:POS.ACCT
        CALL F.READ(FN.AC,AC.ID, R.AC,F.AC,ETEXT)
        AC.CATEG = R.AC<AC.CATEGORY>
        AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
        IF (AC.CATEG EQ 3005 OR AC.CATEG EQ 3010 OR AC.CATEG EQ 3011 OR AC.CATEG EQ 3012 OR AC.CATEG EQ 3013 OR AC.CATEG EQ 1512 OR AC.CATEG EQ 1507) AND AC.BAL NE 0 THEN
*********************************
            CURR = R.AC<AC.CURRENCY>
** LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
**     RATE = R.BASE<RE.BCP.RATE,POS>
** END
            CALL F.READ(FN.CCY,CURR,R.CCY,F.CCY,E2)
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

            IF CURR EQ 'EGP' THEN
                RATE = 1
            END
            IF CURR EQ 'JPY' THEN
                RATE = RATE / 100
            END

            TOTAL = R.AC<AC.OPEN.ACTUAL.BAL> * RATE
            IF TOTAL LT 0 THEN
                TOTAL = TOTAL * -1
            END ELSE
                TOTALT = TOTAL
            END
**************************** MARGIN LETTER OF GRANTE
            IF R.AC<AC.CATEGORY> EQ 3005 THEN
                TOTALTMLG = TOTALTMLG + TOTAL
            END
**************************** MARGINE LETTER OF CREDIT
            IF R.AC<AC.CATEGORY> = 3010 OR R.AC<AC.CATEGORY> = 3011 OR R.AC<AC.CATEGORY> = 3012 THEN
                TOTALTMLC = TOTALTMLC + TOTAL
            END
**************************** MARGINE LETTER OF CREDIT 44 - 45
            IF R.AC<AC.CATEGORY> = 3013 THEN
                TOTALTM44 = TOTALTM44 + TOTAL
            END
***************************** �������� 507
            IF R.AC<AC.CATEGORY> = 1507 THEN
                TOT507  = TOT507  +  TOTAL
            END
***************************** �������� 512
            IF R.AC<AC.CATEGORY> = 1512 THEN
                TOT512  = TOT512  +  TOTAL
            END
****************************
        END
    REPEAT
    TOTALTMLC = TOTALTMLC / 1000   ; TOTALTMLC = DROUND(TOTALTMLC,0)
    TOTALTMLG = TOTALTMLG / 1000   ; TOTALTMLG = DROUND(TOTALTMLG,0)
    TOTALTM44 = TOTALTM44 / 1000   ; TOTALTM44 = DROUND(TOTALTM44,0)
    TOT507 = TOT507 / 1000   ; TOT507 = DROUND(TOT507,0)
    TOT512 = TOT512 / 1000   ; TOT512 = DROUND(TOT512,0)
*----------------------------------------------------------
    RETURN
*---------------------------------------------------------------------
GETCOLLC:
*--------
    YY1 = 0 ; SS1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; SS2 = 0
    C.SEL1 = "" ; K2.LIST = "" ; SELECTED3 = "" ; ERR2 = "" ; EER = ""
    S = 0
    S.CUR = R.LMT<LI.LIMIT.CURRENCY>
*Line [ 1164 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY1 = DCOUNT(R.LMT<LI.COLLAT.RIGHT>,@VM)
    FOR H1 = 1 TO YY1
        SS1 = R.LMT<LI.COLLAT.RIGHT,H1>
*Line [ 1168 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        YY2 = DCOUNT(R.LMT<LI.COLLAT.RIGHT,H1>,@SM)
        FOR H2 = 1 TO YY2
            SS2   = R.LMT<LI.COLLAT.RIGHT,H1,H2>
            S.AMT = R.LMT<LI.SECURED.AMT,H1,H2>
            IF S.CUR NE 'EGP' THEN
**LOCATE S.CUR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
**RATE = R.BASE<RE.BCP.RATE,POS>
                CALL F.READ(FN.CCY,S.CUR,R.CCY,F.CCY,E2)
                RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

                IF S.CUR EQ 'EGP' THEN
                    RATE = 1
                END
                IF S.CUR EQ 'JPY' THEN
                    RATE = RATE / 100
                END

                COLLC.AMT = S.AMT * RATE
            END ELSE
                COLLC.AMT = S.AMT
            END
            COLLC += COLLC.AMT

        NEXT H2
    NEXT H1
    RETURN
*---------------------------------------------------------------------
GETCOL20:
*--------
    T.SEL2 = "SELECT ":FN.LMT:" WITH CREDIT.LINE EQ '":KEY.LIST1<K>:"' AND PRODUCT.ALLOWED EQ ''"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2  THEN
        FOR KK = 1 TO SELECTED2
            CALL F.READ(FN.LMT,KEY.LIST2<KK>,R.LMT,F.LMT,E2)
            YY1 = 0 ; SS1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; SS2 = 0
            C.SEL1 = "" ; K2.LIST = "" ; SELECTED3 = "" ; ERR2 = "" ; EER = ""
            S = 0
            S.CUR = R.LMT<LI.LIMIT.CURRENCY>
*Line [ 1207 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            YY1 = DCOUNT(R.LMT<LI.COLLAT.RIGHT>,@VM)
            FOR H1 = 1 TO YY1
                SS1 = R.LMT<LI.COLLAT.RIGHT,H1>
*Line [ 1211 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                YY2 = DCOUNT(R.LMT<LI.COLLAT.RIGHT,H1>,@SM)
                FOR H2 = 1 TO YY2
                    SS2   = R.LMT<LI.COLLAT.RIGHT,H1,H2>
                    S.AMT = R.LMT<LI.SECURED.AMT,H1,H2>
                    IF S.CUR NE 'EGP' THEN
**LOCATE S.CUR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
**RATE = R.BASE<RE.BCP.RATE,POS>
                        CALL F.READ(FN.CCY,S.CUR,R.CCY,F.CCY,E2)
                        RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

                        IF S.CUR EQ 'EGP' THEN
                            RATE = 1
                        END
                        IF S.CUR EQ 'JPY' THEN
                            RATE = RATE / 100
                        END

                        COLLC.AMT = S.AMT * RATE
                    END ELSE
                        COLLC.AMT = S.AMT
                    END
                    COLLC += COLLC.AMT
                NEXT H2
            NEXT H1
        NEXT KK
    END
    RETURN
*---------------------------------------------------------------------
GETCOLLG:
*-------
    YY1 = 0 ; SS1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; SS2 = 0
    C.SEL1 = "" ; K2.LIST = "" ; SELECTED3 = "" ; ERR2 = "" ; EER = ""
    S = 0
    S.CUR = R.LMT<LI.LIMIT.CURRENCY>
*Line [ 1246 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY1 = DCOUNT(R.LMT<LI.COLLAT.RIGHT>,@VM)
    FOR H1 = 1 TO YY1
        SS1 = R.LMT<LI.COLLAT.RIGHT,H1>
*Line [ 1250 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        YY2 = DCOUNT(R.LMT<LI.COLLAT.RIGHT,H1>,@SM)
        FOR H2 = 1 TO YY2
            SS2 = R.LMT<LI.COLLAT.RIGHT,H1,H2>
            S.AMT = R.LMT<LI.SECURED.AMT,H1,H2>
            IF S.CUR NE 'EGP' THEN
**LOCATE S.CUR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
**RATE = R.BASE<RE.BCP.RATE,POS>

                CALL F.READ(FN.CCY,S.CUR,R.CCY,F.CCY,E2)
                RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

                IF S.CUR EQ 'EGP' THEN
                    RATE = 1
                END
                IF S.CUR EQ 'JPY' THEN
                    RATE = RATE / 100
                END

                COLLG.AMT = S.AMT * RATE
            END ELSE
                COLLG.AMT = S.AMT
            END
            COLLG += COLLG.AMT

        NEXT H2
    NEXT H1
    RETURN
*---------------------------------------------------------------------
CALCAC:
*------
*----------------- "����� �������� � ������  " --------------------
*----------------- " ����������������������  " --------------------

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.2>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.4>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.5>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.9>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.10>
    RETURN
*----------------------------------------------------------------------
INSREC1:
*------
*------------"������ ������ �� �������� ��� �� 0 "-------------------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.1> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.2> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.3> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.4> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.5> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.6> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.7> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.8> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.9> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.10> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.11> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.12> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.13> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = 0
*----------------- " ����� ��� ����� ������� " --------------------
    CO.CODE1 = ''
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CCC.NO,LOCAL.REF)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CCC.NO,CO.CODE1)
    IF LOCAL.REF<1,CULR.CBE.NO> THEN
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = LOCAL.REF<1,CULR.CBE.NO>
    END ELSE
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = "999999999999"
    END
    R.CRDT.CBE<SCB.C.CBE.BNK.DATE> = BNK.DATE1
    R.CRDT.CBE<SCB.C.CBE.CO.CODE> = CO.CODE1
*--------------------- " ����� ��� ����� ����� ���������� ------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.99> GT 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.US.13> GT 0 THEN
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    IF SW2 = 1 THEN

        CALL F.WRITE(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE)
        CALL JOURNAL.UPDATE(CCC.NO)

        CALL F.RELEASE(FN.CRDT.CBE,CCC.NO,F.CRDT.CBE)
        CLOSE F.CRDT.CBE
    END
    GOSUB INIT0CR
    RETURN
*---------------------------------------------------------------------
INSREC:
*** HASHED @ 20110306 *****
*------------"������ ������ ����� �����"-----------------------------
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = R.CRDT.CBE<SCB.C.CBE.TOT.US.9>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13>
*------------"������ �������� ���� �� ������"-------------------------
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.1> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.2> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.3> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.4> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.5> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.6> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.7> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.8> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.9> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = R.CRDT.CBE<SCB.C.CBE.TOT.US.9>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.10> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.11> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.12> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*    END
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.13> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> THEN
*        R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13>
*    END

*------------"������ ������ �� �������� ��� �� 0 "-------------------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.1> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.2> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.3> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.4> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.5> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.6> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.7> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.8> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.9> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.10> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.11> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.12> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.13> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = 0

    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = 0

*----------------- "����� �������� � ������  " --------------------
*----------------- " ����������������������  " --------------------
    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.2>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.4>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.5>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.9>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.10>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.11>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*----------------- "  ���������������������� " --------------------
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.1>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.2>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.3>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.4>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.5>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.6>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.7>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.8>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.9>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.10>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.11>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.12>
*----------------- " ����� ��� ����� ������� " --------------------
    CO.CODE1 = ''
    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CCC.NO,CO.CODE1)
    R.CRDT.CBE<SCB.C.CBE.CO.CODE> = CO.CODE1
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CCC.NO,LOCAL.REF)
    IF LOCAL.REF<1,CULR.CBE.NO> THEN
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = LOCAL.REF<1,CULR.CBE.NO>
    END ELSE
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = "999999999999"
    END
    R.CRDT.CBE<SCB.C.CBE.BNK.DATE> = BNK.DATE1
*--------------------- " ����� ��� ����� ����� ���������� ------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.99> GE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> GE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.US.13> GE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> GE 0 THEN
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    IF SW2 = 1 THEN
        CALL F.WRITE(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE)
        CALL JOURNAL.UPDATE(CCC.NO)

        CALL F.RELEASE(FN.CRDT.CBE,CCC.NO,F.CRDT.CBE)
        CLOSE F.CRDT.CBE
    END
    GOSUB INIT0CR
    RETURN
*---------------------------------------------------------------------
CHKALL:
*-----
    FN.CRDT.CBE = 'F.SCB.CREDIT.CBE.HO'
    F.CRDT.CBE = ''
    R.CRDT.CBE = ''
    T.SEL = '' ; SELECTED = '' ; KEY.LIST = ''
    T.SEL = "SELECT ":FN.CRDT.CBE:" WITH TOT.US.99 GT TOT.CR.99"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN

        FOR I = 1 TO SELECTED
            CCC.NO = KEY.LIST<I>
            CALL F.READ(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE,F.CRDT.CBE,ELM)
            GOSUB INSREC
            GOSUB INIT0CR
        NEXT I
    END
    RETURN
*---------------------------------------------------------------------
INSGUR:
*------
    GOSUB INIT0
    GOSUB INIT0CR
    SW1 = 0
    GUR.LIST="" ; GUR.SEL="" ;  ER.GUR=""
    SELECTED5 = ""
    R.CUS = ""
*---------------------------------------------------------------------
    GUR.SEL = "SELECT ":FN.CUS:" WITH RELATION.CODE EQ 26 OR RELATION.CODE EQ 27 BY @ID"
    CALL EB.READLIST(GUR.SEL,GUR.LIST,"",SELECTED5,ER.GUR)
    IF SELECTED5  THEN
        FOR U = 1 TO SELECTED5
            GOSUB INIT0CRGUR
            CALL F.READ(FN.CUS,GUR.LIST<U>,R.CUS,F.CUS,E.G1)
*---------------------------------------------------------------------
            CALL F.READ(FN.CRDT.CBE,GUR.LIST<U>,R.CRDT.CBE,F.CRDT.CBE,E11)
            IF E11 THEN GOSUB INIT0CR
*---------------------------------------------------------------------
            REL.COD = R.CUS<EB.CUS.RELATION.CODE>
*Line [ 1552 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            K1 = DCOUNT(REL.COD,@VM)
            FOR J = 1 TO K1
                IF R.CUS<EB.CUS.RELATION.CODE,J> = 26 OR R.CUS<EB.CUS.RELATION.CODE,J> = 27 THEN
                    R.CRDT.CBE1 = ''
                    CUS.ID1 = GUR.LIST<U>
                    REL.ID = R.CUS<EB.CUS.REL.CUSTOMER,J>
                    CALL F.READ(FN.CRDT.CBE,REL.ID,R.CRDT.CBE1,F.CRDT.CBE,E.G21)
                    IF NOT(E.G21) THEN
                        SW1 ++
                        R.CRDT.CBE<SCB.C.CBE.TOT.US.13> += R.CRDT.CBE1<SCB.C.CBE.TOT.US.99>
                        R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> += R.CRDT.CBE1<SCB.C.CBE.TOT.CR.99>
                    END
                END
            NEXT J
*******************************************************************
*--------------------- " ����� ��� ����� ����� ���������� -----------
            IF SW1 GT 0 THEN
                LOCAL.REF1 = R.CUS<EB.CUS.LOCAL.REF>
                R.CRDT.CBE<SCB.C.CBE.CBE.NO> = LOCAL.REF1<1,CULR.CBE.NO>
                R.CRDT.CBE<SCB.C.CBE.BNK.DATE> = BNK.DATE1
                R.CRDT.CBE<SCB.C.CBE.CO.CODE> = R.CUS<EB.CUS.COMPANY.BOOK>

                CALL F.WRITE(FN.CRDT.CBE,CUS.ID1,R.CRDT.CBE)
                CALL JOURNAL.UPDATE(CUS.ID1)

                CALL F.RELEASE(FN.CRDT.CBE,CUS.ID1,F.CRDT.CBE)
                CLOSE F.CRDT.CBE
            END
            SW1 = 0
            GOSUB INIT0CR
            GOSUB INIT0CRGUR
        NEXT U
    END
    RETURN
*---------------------------------------------------------------------
