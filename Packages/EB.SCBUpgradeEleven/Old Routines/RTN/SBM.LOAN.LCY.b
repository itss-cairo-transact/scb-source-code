* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** ����� ������ ��������� ������� ������� ***
*** CREATED BY KHALED ***
***=================================

    SUBROUTINE SBM.LOAN.LCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 42 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LOAN.LCY'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.AMOUNT.LCY8 = 0  ; Y.AMOUNT.LCY12 = 0
    Y.AMOUNT.LCY9 = 0  ; Y.AMOUNT.LCY13 = 0
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
    RETURN
*========================================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*************************************************************************
    T.SEL2 = "SELECT ":FN.CBE: " WITH CBEM.CATEG GE 1101 AND CBEM.CATEG LE 1599 AND CBEM.CY EQ 'EGP' AND CBEM.BR EQ ":COMP.BR
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR K = 1 TO SELECTED2
            CALL F.READ(FN.CBE,KEY.LIST2<K>,R.CBE,F.CBE,E3)
            Y.SECTOR2 = R.CBE<C.CBEM.NEW.SECTOR>
            SECTOR.ID2 = Y.SECTOR2[2,3]
            Y.AMOUNT.LCY12 = Y.AMOUNT.LCY12 + R.CBE<C.CBEM.IN.LCY>
            Y.AMOUNT.LCY12 = DROUND(Y.AMOUNT.LCY12,'2')

            IF SECTOR.ID2 GE 210 AND SECTOR.ID2 LE 260 THEN
                Y.AMOUNT.LCY13 = Y.AMOUNT.LCY13 + R.CBE<C.CBEM.IN.LCY>
                Y.AMOUNT.LCY13 = DROUND(Y.AMOUNT.LCY13,'2')
            END
        NEXT K
        XX3 = SPACE(120)
        XX4 = SPACE(120)
        XX3<1,1>[1,35]   = "������ ����� ������ ��������� ����"
        XX3<1,1>[45,15] = Y.AMOUNT.LCY12
        XX4<1,1>[1,35]   = "������ ������ ������ �����"
        XX4<1,1>[45,15] = Y.AMOUNT.LCY13
        PRINT STR('-',60)
        PRINT XX3<1,1>
        PRINT STR('-',60)
        PRINT STR(' ',60)
        PRINT STR('-',60)
        PRINT XX4<1,1>
        PRINT STR('-',60)
        PRINT STR(' ',60)
    END
*************************************************************************
    T.SEL1 = "SELECT ":FN.CBE: " WITH CBEM.CY NE 'EGP' AND CBEM.CATEG IN IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 1001 1002 1003 6501 6502 6503 6504 1012 1013 1015 1016 3005 3010 3011 3012 3013) AND CBEM.BR EQ ":COMP.BR
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR J = 1 TO SELECTED1
            CALL F.READ(FN.CBE,KEY.LIST1<J>,R.CBE,F.CBE,E2)
            Y.SECTOR1 = R.CBE<C.CBEM.NEW.SECTOR>
            SECTOR.ID1 = Y.SECTOR1[2,3]
            Y.AMOUNT.LCY8 = Y.AMOUNT.LCY8 + R.CBE<C.CBEM.IN.LCY>
            Y.AMOUNT.LCY8 = DROUND(Y.AMOUNT.LCY8,'2')

            IF SECTOR.ID1 GE 650 AND SECTOR.ID1 LE 750 THEN
                Y.AMOUNT.LCY9 = Y.AMOUNT.LCY9 + R.CBE<C.CBEM.IN.LCY>
                Y.AMOUNT.LCY9 = DROUND(Y.AMOUNT.LCY9,'2')
            END
        NEXT J
        XX1 = SPACE(120)
        XX2 = SPACE(120)
        XX1<1,1>[1,35]   = "������ ����� ������� ��������"
        XX1<1,1>[45,15] = Y.AMOUNT.LCY8
        XX2<1,1>[1,35]   = "������ ������� ������ �������"
        XX2<1,1>[45,15] = Y.AMOUNT.LCY9

        PRINT STR('-',60)
        PRINT XX1<1,1>
        PRINT STR('-',60)
        PRINT STR(' ',60)
        PRINT STR('-',60)
        PRINT XX2<1,1>
        PRINT STR('-',60)
        PRINT STR(' ',60)

    END
*************************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������ ��������� ������� �������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
