* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** ���� ������ ����� �������� �������� ***
*** CREATED BY KHALED ***
***=====================================

    SUBROUTINE SBM.REL.CUS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RELATION.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 43 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*==============================================================
INITIATE:
**************UPDATED BY MAHMOUD 6/6/2010*************
*?    REPORT.ID='SBM.REL.CUS'
    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBM.REL.CUS'
******************************************************
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    RETURN
*===============================================================
CALLDB:
    FN.RCU = 'FBNK.RELATION.CUSTOMER' ; F.RCU = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.RCU,F.RCU)
    CALL OPF(FN.CU,F.CU)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.RCU:" WITH IS.RELATION EQ 62 OR IS.RELATION EQ 72 BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    X = 0
    IF SELECTED  THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.RCU,KEY.LIST<I>,R.RCU,F.RCU,E1)
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            CU.COMP = R.CU<EB.CUS.COMPANY.BOOK>
            IF CU.COMP EQ COMP THEN

*******************************************************************
                K2=0
                REL.COD = R.RCU<EB.RCU.IS.RELATION>
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                K1 = DCOUNT(REL.COD,@VM)
                FOR J = 1 TO K1
                    IF R.RCU<EB.RCU.IS.RELATION,J> = 62 OR R.RCU<EB.RCU.IS.RELATION,J> = 72 THEN
                        K2 ++
                        REL.ID = KEY.LIST<I>
                        CUS.ID = R.RCU<EB.RCU.OF.CUSTOMER,J>
                        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
*******************************************************************
                        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,REL.ID,LOCAL.REF)
                        CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME>
                        CUST.NAME2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
                        CBE.NO = LOCAL.REF<1,CULR.CBE.NO>
**                        IF CUS.ID EQ LOCAL.REF<1,CULR.OLD.CUST.ID> THEN
**                            OLD.ID = ' '
**                        END ELSE
                        OLD.ID = LOCAL.REF<1,CULR.OLD.CUST.ID>
**                        END
                        LOCAL.REF = ''
                        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
                        CUST.NAME.GUR1 = LOCAL.REF<1,CULR.ARABIC.NAME>
                        CUST.NAME.GUR2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
                        CBE.NO1 = LOCAL.REF<1,CULR.CBE.NO>
                        OLD.ID1 = LOCAL.REF<1,CULR.OLD.CUST.ID>
                        XX  = SPACE(132)
                        XX1 = SPACE(132)
                        XX2 = SPACE(132)
                        XX<1,K2>[1,8]     = REL.ID
                        XX2<1,K2>[1,8]    = CBE.NO
                        XX<1,K2>[18,35]   = CUST.NAME1
                        XX2<1,K2>[18,35]  = OLD.ID
                        XX1<1,K2>[18,35]  = CUST.NAME2
                        XX<1,K2>[73,10]   = CUS.ID
                        XX2<1,K2>[73,10]  = CBE.NO1
                        XX<1,K2>[91,35]   = CUST.NAME.GUR1
                        XX1<1,K2>[91,35]  = CUST.NAME.GUR2

                        IF K2 GT 1 THEN
                            XX<1,K2>[1,53]   = STR(' ',53)
                            XX1<1,K2>[18,35] = STR(' ',35)
                            XX2<1,K2>[1,8]   = STR(' ',10)
                            XX2<1,K2>[18,35] = STR(' ',35)
                        END
                        PRINT XX<1,K2>
                        PRINT XX1<1,K2>
                        PRINT XX2<1,K2>
                    END
                NEXT J
                PRINT STR('-',120)
                X ++
            END
*===============================================================
        NEXT I
        PRINT STR('=',120)
        PRINT ; PRINT "������� = ":X:" �����"
*===============================================================
    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� ������ ����� �������� �������� "
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������" :SPACE(10):"��� ������":SPACE(35):"��� ������":SPACE(10):"��� ������"
    PR.HD :="'L'":"�.��� �����":SPACE(10):"����� ������":SPACE(31):"�.��� �����"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
