* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MSABRY 2015/2/24
************************************

    PROGRAM  SBM.CU.EGSERV.002

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EGSRV.INDEX


    FN.EGI = 'F.SCB.EGSRV.INDEX'  ; F.EGI = '' ; R.EGI = ''
    CALL OPF(FN.EGI,F.EGI)

    SEQ.FILE.NAME = '&SAVEDLISTS&'
    RECORD.NAME = 'ZipcodeFile_SCBank.TXT'
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        TEXT = 'Unable to Locate ':SEQ.FILE.POINTER  ; CALL REM
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
*-----------------------------------------------------
            WS.CUS.ID = FIELD(Y.MSG,";",1)
            WS.EGSERV.CODE = FIELD(Y.MSG,";",2)
            WS.EGSERV.BAR = FIELD(Y.MSG,";",3)
            GOSUB EGI.WRITE
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER
    RETURN
*----------------------------------------------------
EGI.WRITE:

    CALL F.READ(FN.EGI,WS.CUS.ID,R.EGI,F.EGI,ER.EGI)

    IF ER.EGI THEN
        RETURN
    END

    R.EGI<EGSRV.EGSERV.CODE> = WS.EGSERV.CODE
    R.EGI<EGSRV.EGSERV.BAR> = WS.EGSERV.BAR
    CALL F.WRITE(FN.EGI,WS.CUS.ID,R.EGI)
    CALL JOURNAL.UPDATE(WS.CUS.ID)

    CRT WS.CUS.ID :" ": WS.EGSERV.CODE:" ": WS.EGSERV.BAR

    RETURN
