* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*- COPY FROM SBR.TOP50
*- EDIT BY NESMA & MENNA

    PROGRAM SBM.TOP50.CUR.LIST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TOP50
*-----------------------------------------
    CUR.POS = "" ; CUR.ID = "" ; TOT.AMT = 0

*    YTEXT = " Enter Currency ID :  "
*    CALL TXTINP(YTEXT, 8, 22, "3", "A")
*    CUR.ID  = COMI

    CURR   = "USD.EUR.GBP.SAR.JPY.CHF.DKK.SEK.NOK.ITL.INR.AUD.CAD.KWD.AED.EGP"
    CUR.N  = DCOUNT(CURR,'.')
    FOR NN = 1 TO CUR.N
        TOT.AMT = 0
        CUR.ID = FIELD(CURR,'.',NN)
        DIR.NAME = '/home/signat'
        NEW.FILE = "SBR.TOP50.":CUR.ID:".LIST.CSV"
        OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
            CLOSESEQ V.FILE.IN
            HUSH ON
            EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
            HUSH OFF
*PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
        END
        OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
            CREATE V.FILE.IN THEN
             *   PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
            END ELSE
                STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
            END
        END

        BB.DATA = ""
        BB.DATA = "DATE.CREATED : ,, ":TODAY
        WRITESEQ BB.DATA TO V.FILE.IN ELSE
            PRINT " ERROR WRITE FILE "
        END

        BB.DATA  = ""
        BB.DATA  = "CUSTOMER.ID,"
        BB.DATA := "CUSTOMER.NAME,"
        BB.DATA := "CURRENCY.ID,"
        BB.DATA := "CURRENCY.NAME,"
        BB.DATA := "AMOUNT IN ":CUR.ID:",,,"
        WRITESEQ BB.DATA TO V.FILE.IN ELSE
            PRINT " ERROR WRITE FILE "
        END

        FN.TP  = "F.SCB.TOP50" ; F.TP   = "" ; R.TP  = ""
        CALL OPF(FN.TP,F.TP)

        FN.ACC = "FBNK.ACCOUNT" ; F.ACC  = "" ; R.ACC  = ""
        CALL OPF(FN.ACC,F.ACC)

        Y.SEL   = "SSELECT F.SCB.TOP50 WITH (GROUP.ID EQ '' AND CURRENCY EQ ": CUR.ID : " )"
        Y.SEL  := " OR ( @ID LIKE ...G ) "

        IF CUR.ID EQ 'USD' THEN Y.SEL := " BY-DSND TOTAL.USD"
        IF CUR.ID EQ 'EUR' THEN Y.SEL := " BY-DSND TOTAL.EUR"
        IF CUR.ID EQ 'SAR' THEN Y.SEL := " BY-DSND TOTAL.SAR"
        IF CUR.ID EQ 'GBP' THEN Y.SEL := " BY-DSND TOTAL.GBP"
        IF CUR.ID EQ 'EGP' THEN Y.SEL := " BY-DSND TOTAL.EGP.AMT"
        IF CUR.ID EQ 'JPY' THEN Y.SEL := " BY-DSND TOTAL.JPY"
        IF CUR.ID EQ 'CHF' THEN Y.SEL := " BY-DSND TOTAL.CHF"
        IF CUR.ID EQ 'DKK' THEN Y.SEL := " BY-DSND TOTAL.DKK"
        IF CUR.ID EQ 'SEK' THEN Y.SEL := " BY-DSND TOTAL.SEK"
        IF CUR.ID EQ 'NOK' THEN Y.SEL := " BY-DSND TOTAL.NOK"
        IF CUR.ID EQ 'ITL' THEN Y.SEL := " BY-DSND TOTAL.ITL"
        IF CUR.ID EQ 'INR' THEN Y.SEL := " BY-DSND TOTAL.INR"
        IF CUR.ID EQ 'AUD' THEN Y.SEL := " BY-DSND TOTAL.AUD"
        IF CUR.ID EQ 'CAD' THEN Y.SEL := " BY-DSND TOTAL.CAD"
        IF CUR.ID EQ 'KWD' THEN Y.SEL := " BY-DSND TOTAL.KWD"
        IF CUR.ID EQ 'AED' THEN Y.SEL := " BY-DSND TOTAL.AED"

        CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        AMT = 0
        IF SELECTED THEN
            FOR I = 1 TO 50
                CALL F.READ(FN.TP,KEY.LIST<I>, R.TP, F.TP ,E.TP)
                LOCAL.REF = ''
                CUS.ID = FIELD(KEY.LIST<I>,".",1)
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
                CUS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.ID,CUR.NAME)
*Line [ 119 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                INDX = DCOUNT(R.TP<TP.LINE.NO>,@VM)
                DD = LEN(KEY.LIST<I>)
                IF KEY.LIST<I>[DD,1] NE 'G' THEN
                    FOR HH = 1 TO INDX
                        CUR = R.TP<TP.CURRENCY><1,HH>
                        IF CUR EQ CUR.ID THEN
                            IF CUR NE 'EGP' THEN
                                AMT += R.TP<TP.AMT.FCY><1,HH>
                            END ELSE
                                AMT += R.TP<TP.AMT.LCY><1,HH>
                            END
                        END

                    NEXT HH
                END ELSE
                    IF CUR.ID EQ 'USD' THEN
                        AMT = R.TP<TP.TOTAL.USD>
                    END
                    IF CUR.ID EQ 'EUR' THEN
                        AMT = R.TP<TP.TOTAL.EUR>
                    END
                    IF CUR.ID EQ 'SAR' THEN
                        AMT = R.TP<TP.TOTAL.SAR>
                    END
                    IF CUR.ID EQ 'JPY' THEN
                        AMT = R.TP<TP.TOTAL.JPY>
                    END
                    IF CUR.ID EQ 'GBP' THEN
                        AMT = R.TP<TP.TOTAL.GBP>
                    END
                    IF CUR.ID EQ 'CHF' THEN
                        AMT = R.TP<TP.TOTAL.CHF>
                    END
                    IF CUR.ID EQ 'DKK' THEN
                        AMT = R.TP<TP.TOTAL.DKK>
                    END
                    IF CUR.ID EQ 'SEK' THEN
                        AMT = R.TP<TP.TOTAL.SEK>
                    END
                    IF CUR.ID EQ 'NOK' THEN
                        AMT = R.TP<TP.TOTAL.NOK>
                    END
                    IF CUR.ID EQ 'ITL' THEN
                        AMT = R.TP<TP.TOTAL.ITL>
                    END
                    IF CUR.ID EQ 'INR' THEN
                        AMT = R.TP<TP.TOTAL.INR>
                    END
                    IF CUR.ID EQ 'AUD' THEN
                        AMT = R.TP<TP.TOTAL.AUD>
                    END
                    IF CUR.ID EQ 'CAD' THEN
                        AMT = R.TP<TP.TOTAL.CAD>
                    END
                    IF CUR.ID EQ 'KWD' THEN
                        AMT = R.TP<TP.TOTAL.KWD>
                    END
                    IF CUR.ID EQ 'AED' THEN
                        AMT = R.TP<TP.TOTAL.AED>
                    END
                    IF CUR.ID EQ 'EGP' THEN
                        AMT = R.TP<TP.TOTAL.EGP.AMT>
                    END



                END
                BB.DATA = ""
                BB.DATA  = KEY.LIST<I>:","
                BB.DATA := CUS.NAME:","
                BB.DATA := CUR.ID:","
                BB.DATA := CUR.NAME:","
                BB.DATA := AMT:",,,"

                TOT.AMT + = AMT

                WRITESEQ BB.DATA TO V.FILE.IN ELSE
                    PRINT " ERROR WRITE FILE "
                END

                CUS.NAME = "" ; CUR.NAME = "" ; AMT = 0

            NEXT I
        END

        BB.DATA = ""
        BB.DATA = ",,,TOTAL.AMOUNT,"
        BB.DATA := TOT.AMT
        WRITESEQ BB.DATA TO V.FILE.IN ELSE
            PRINT " ERROR WRITE FILE "
        END
    NEXT NN
********************************************************
    RETURN
END
