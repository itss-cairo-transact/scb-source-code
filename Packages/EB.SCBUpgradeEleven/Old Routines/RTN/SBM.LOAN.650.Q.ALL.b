* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LOAN.650.Q.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LOAN.650.Q.ALL'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]

    AMT.LCY.VISA1 = 0 ;  AMT.FCY.VISA1 = 0
    AMT.LCY.VISA2 = 0 ;  AMT.FCY.VISA2 = 0
    AMT.LCY.CUR1  = 0 ;  AMT.FCY.CUR1  = 0
    AMT.LCY.CUR2  = 0 ;  AMT.FCY.CUR2  = 0
    AMT.LCY.CUR3  = 0 ;  AMT.FCY.CUR3  = 0
    AMT.LCY.CUR4  = 0 ;  AMT.FCY.CUR4  = 0
    AMT.LCY.CUR5  = 0 ;  AMT.FCY.CUR5  = 0

    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    RETURN
*========================================================================
PROCESS:

****    T.SEL = "SELECT ":FN.CBE:" WITH CBEM.NEW.SECTOR EQ 4650 AND CBEM.BR EQ ":COMP.BR:" AND (CBEM.CATEG GE 1100 AND CBEM.CATEG LE 1599) AND CBEM.IN.LCY LT 0 AND CBEM.OLD.SECTOR IN (1100 1200 1300 1400)"
    T.SEL = "SELECT ":FN.CBE:" WITH CBEM.NEW.SECTOR EQ 4650 AND (CBEM.CATEG GE 1100 AND CBEM.CATEG LE 1599) AND CBEM.IN.LCY LT 0 AND CBEM.OLD.SECTOR IN (1100 1200 1300 1400)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)

********************* ���� ���� ���� ******************

            IF R.CBE<C.CBEM.CATEG> EQ 1205 OR R.CBE<C.CBEM.CATEG> EQ 1207 THEN
                IF R.CBE<C.CBEM.CY> EQ 'EGP' THEN
                    AMT.LCY.VISA2   += R.CBE<C.CBEM.IN.LCY>
                    AMT.LCY.VISA2   = DROUND(AMT.LCY.VISA2,'0')
                END
                IF R.CBE<C.CBEM.CY> NE 'EGP' THEN
                    AMT.FCY.VISA2   += R.CBE<C.CBEM.IN.LCY>
                    AMT.FCY.VISA2   = DROUND(AMT.FCY.VISA2,'0')
                END
            END
*--------------------------------------------------------------
********************* ���� ����� **********************

            IF R.CBE<C.CBEM.CATEG> EQ 1206 OR R.CBE<C.CBEM.CATEG> EQ 1208 THEN
                IF R.CBE<C.CBEM.CY> EQ 'EGP' THEN
                    AMT.LCY.VISA1   += R.CBE<C.CBEM.IN.LCY>
                    AMT.LCY.VISA1   = DROUND(AMT.LCY.VISA1,'0')
                END
                IF R.CBE<C.CBEM.CY> NE 'EGP' THEN
                    AMT.FCY.VISA1   += R.CBE<C.CBEM.IN.LCY>
                    AMT.FCY.VISA1   = DROUND(AMT.FCY.VISA1,'0')
                END
            END
*-------------------------------------------------------------------
******************** ������� ����� ���� ���� ���� *****************

            IF R.CBE<C.CBEM.CATEG> NE 1205 AND R.CBE<C.CBEM.CATEG> NE 1207 AND R.CBE<C.CBEM.CATEG> NE 1206 AND R.CBE<C.CBEM.CATEG> NE 1208 AND R.CBE<C.CBEM.CATEG> NE 1404 AND R.CBE<C.CBEM.CATEG> NE 1401 AND R.CBE<C.CBEM.CATEG> NE 1405 AND R.CBE<C.CBEM.CATEG> NE 1406 THEN
                IF R.CBE<C.CBEM.CY> EQ 'EGP' THEN
                    AMT.LCY.CUR2   += R.CBE<C.CBEM.IN.LCY>
                    AMT.LCY.CUR2   = DROUND(AMT.LCY.CUR2,'0')
                END
                IF R.CBE<C.CBEM.CY> NE 'EGP' THEN
                    AMT.FCY.CUR2   += R.CBE<C.CBEM.IN.LCY>
                    AMT.FCY.CUR2   = DROUND(AMT.FCY.CUR2,'0')
                END

            END
*-------------------------------------------------------------------

********************* ������� ����� ���� ����� ����� ����� **********************

            IF R.CBE<C.CBEM.CATEG> EQ 1404 THEN
                IF R.CBE<C.CBEM.CY> EQ 'EGP' THEN
                    AMT.LCY.CUR1   += R.CBE<C.CBEM.IN.LCY>
                    AMT.LCY.CUR1   = DROUND(AMT.LCY.CUR1,'0')
                END
                IF R.CBE<C.CBEM.CY> NE 'EGP' THEN
                    AMT.FCY.CUR1   += R.CBE<C.CBEM.IN.LCY>
                    AMT.FCY.CUR1   = DROUND(AMT.FCY.CUR1,'0')
                END
            END

*-------------------------------------------------------------------

********************* ������� ����� ���� ����� ����� ������� **********************

            IF R.CBE<C.CBEM.CATEG> EQ 1401 OR R.CBE<C.CBEM.CATEG> EQ 1405 OR R.CBE<C.CBEM.CATEG> EQ 1406 THEN
                IF R.CBE<C.CBEM.CY> EQ 'EGP' THEN
                    AMT.LCY.CUR5   += R.CBE<C.CBEM.IN.LCY>
                    AMT.LCY.CUR5   = DROUND(AMT.LCY.CUR5,'0')
                END
                IF R.CBE<C.CBEM.CY> NE 'EGP' THEN
                    AMT.FCY.CUR5   += R.CBE<C.CBEM.IN.LCY>
                    AMT.FCY.CUR5   = DROUND(AMT.FCY.CUR5,'0')
                END
            END
*----------------------------------------------------------------------
        NEXT I
    END
*---------------------------------------------------
***    T.SEL1 = "SELECT ":FN.CBE:" WITH CBEM.NEW.SECTOR EQ 4650 AND CBEM.BR EQ ":COMP.BR:" AND CBEM.CATEG IN (1001 1002) AND CBEM.IN.LCY LT 0 AND CBEM.OLD.SECTOR IN (1100 1200 1300 1400)"
    T.SEL1 = "SELECT ":FN.CBE:" WITH CBEM.NEW.SECTOR EQ 4650 AND CBEM.CATEG IN (1001 1002) AND CBEM.IN.LCY LT 0 AND CBEM.OLD.SECTOR IN (1100 1200 1300 1400)"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR X = 1 TO SELECTED1
            CALL F.READ(FN.CBE,KEY.LIST1<X>,R.CBE,F.CBE,E1)

            IF R.CBE<C.CBEM.CY> EQ 'EGP' THEN
                AMT.LCY.CUR3   += R.CBE<C.CBEM.IN.LCY>
                AMT.LCY.CUR3   = DROUND(AMT.LCY.CUR3,'0')
            END
            IF R.CBE<C.CBEM.CY> NE 'EGP' THEN
                AMT.FCY.CUR3   += R.CBE<C.CBEM.IN.LCY>
                AMT.FCY.CUR3   = DROUND(AMT.FCY.CUR3,'0')
            END

        NEXT X
    END

    AMT.LCY.CUR4 = AMT.LCY.CUR3 + AMT.LCY.CUR2
    AMT.FCY.CUR4 = AMT.FCY.CUR3 + AMT.FCY.CUR2
*---------------------------------------------------
    AMT.LCY.TOT1  = AMT.LCY.VISA1 + AMT.LCY.CUR5
    AMT.LCY.TOT2  = AMT.LCY.CUR1
    AMT.LCY.TOT3  = AMT.LCY.VISA2 + AMT.LCY.CUR4

    AMT.FCY.TOT1  = AMT.FCY.VISA1 + AMT.FCY.CUR5
    AMT.FCY.TOT2  = AMT.FCY.CUR1
    AMT.FCY.TOT3  = AMT.FCY.VISA2 + AMT.FCY.CUR4

    AMT.LCY.TOTAL = AMT.LCY.TOT1 + AMT.LCY.TOT2 + AMT.LCY.TOT3
    AMT.FCY.TOTAL = AMT.FCY.TOT1 + AMT.FCY.TOT2 + AMT.FCY.TOT3


*---------------------------------------------------
    XX= SPACE(120)
    PRINT STR('-',130)
    XX<1,1>[1,35]    = "�������� ����������"
    XX<1,1>[35,20]   = AMT.LCY.VISA1
    XX<1,1>[50,20]   = 0
    XX<1,1>[65,20]   = AMT.LCY.VISA2
    XX<1,1>[85,20]   = AMT.FCY.VISA1
    XX<1,1>[100,20]  = 0
    XX<1,1>[115,20]  = AMT.FCY.VISA2
    PRINT XX<1,1>

    XX2= SPACE(120)
    PRINT STR('-',130)
    XX2<1,1>[1,35]    = "������ ����� �����"
    XX2<1,1>[35,20]   = AMT.LCY.CUR5
    XX2<1,1>[50,20]   = AMT.LCY.CUR1
    XX2<1,1>[65,20]   = AMT.LCY.CUR4
    XX2<1,1>[85,20]   = AMT.FCY.CUR5
    XX2<1,1>[100,20]  = AMT.FCY.CUR1
    XX2<1,1>[115,20]  = AMT.FCY.CUR4
    PRINT XX2<1,1>
    PRINT STR('-',130)


    XX3= SPACE(120)
    PRINT STR('-',130)
    XX3<1,1>[1,35]    = "�����������"
    XX3<1,1>[35,20]   = AMT.LCY.TOT1
    XX3<1,1>[50,20]   = AMT.LCY.TOT2
    XX3<1,1>[65,20]   = AMT.LCY.TOT3
    XX3<1,1>[85,20]   = AMT.FCY.TOT1
    XX3<1,1>[100,20]  = AMT.FCY.TOT2
    XX3<1,1>[115,20]  = AMT.FCY.TOT3

    PRINT XX3<1,1>
    PRINT STR('-',130)
    PRINT STR(' ',130)

    XX4= SPACE(120)
    PRINT STR('=',130)
    XX4<1,1>[1,35]    = "������ ���� �����"
    XX4<1,1>[35,20]   = AMT.LCY.TOTAL
    PRINT XX4<1,1>
    PRINT STR('=',130)
    PRINT STR(' ',130)

    XX5= SPACE(120)
    PRINT STR('=',130)
    XX5<1,1>[1,35]    = "������ ���� ������ �����"
    XX5<1,1>[35,20]   = AMT.FCY.TOTAL
    PRINT XX5<1,1>
    PRINT STR('=',130)

    RETURN
*==============================================================
PRINT.HEAD:
*---------
*    P.COMP = "EG0010099"
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,P.COMP,BRANCH)
*    YYBRN  = BRANCH

    YYBRN  = "�� ���� �����"
    NEW.TEXT = " ���� �� ����� "
1 *


    DATY   = TODAY

    TD = TODAY[1,6]:'01'
    CALL CDT("",TD,'-1C')

    TD1 = TD[1,4]:'/':TD[5,2]

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ��������� ���������� ��  : ":TD1
    PR.HD :="'L'":SPACE(55):"������� �������� - �������� ������":NEW.TEXT
    PR.HD :="'L'":SPACE(45):STR('_',45)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������":SPACE(40):"������� �������":SPACE(25):"������� �������� �����"
    PR.HD :="'L'":SPACE(35):"�����":SPACE(10):"�����":SPACE(10):"����":SPACE(15):"�����":SPACE(10):"�����":SPACE(10):"����"
    PR.HD :="'L'":SPACE(35):"�����":SPACE(10):"�.�����":SPACE(8):"����":SPACE(15):"�����":SPACE(10):"�.�����":SPACE(8):"�����"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
