* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
******************MAHMOUD 11/3/2012**********************
*-----------------------------------------------------------------------------
* <Rating>1834</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.AC.TXN.HIST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


*##    XXZZ = " Start Extracting Data ? (Y/N) "
*##    CALL TXTINP(XXZZ, 8, 23, '1.1', FM:'Y_N')
*##    IF COMI[1,1] NE 'Y' THEN
*##        TEXT = "Canceled" ; CALL REM
*##        RETURN
*##    END
    ACCT.ID = ''
    GOSUB INITIATE
*Line [ 52 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
*##    TEXT = "Done" ; CALL REM
    RETURN
*===========================================
INITIATE:
*--------
    MM  = 0
    ZZ  = 0
    CRN = 0
    DBN = 0
    AST = ","
    NL  = ''
    DB.ARR   = ''
    CR.ARR   = ''
    RET.REC  = ''
    DATA.ARR = ''
    TDD1 = R.DATES(EB.DAT.LAST.WORKING.DAY)
    FROM.DATE = TDD1
    END.DATE  = TDD1
    CALL ADD.MONTHS(FROM.DATE,'-6')
    CRT FROM.DATE:"-":END.DATE

    PATHNAME1 = "EIBHIS"
    FILENAME1 = "ACC.TXN.HIST.CSV"

    OPENSEQ PATHNAME1 , FILENAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATHNAME1:' ':FILENAME1
        HUSH OFF
    END
    OPENSEQ PATHNAME1 , FILENAME1 TO BB ELSE
        CREATE BB THEN
            CRT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END

**    HEAD.1 = "EHPART,EHCARD,EHMBRN,EHACCT,EHABLC,EHPRDT,EHTRDT,EHREFN,EHAPRN,EHTRTY,EHDBCR,EHTRCC,EHTRNA,EHORGN,EHCAID,EHCADS,EHPDS1,EHPDS2,EHUSER,EHMCGC,EHSER"

*    XX.DATA  = HEAD.1
*    WRITESEQ XX.DATA TO BB ELSE
*        CRT " ERROR WRITE FILE "
*    END
    RETURN
*===========================================
CALLDB:
*------
    FN.CU = "FBNK.CUSTOMER"  ; F.CU = ""
    CALL OPF(FN.CU,F.CU)
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)
    FN.CU.AC = "FBNK.CUSTOMER.ACCOUNT"  ; F.CU.AC = ""
    CALL OPF(FN.CU.AC,F.CU.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ENT = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.ENT = ''
    CALL OPF(FN.ENT,F.ENT)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.HIS = ''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    RETURN
*===========================================
PROCESS:
*---------
    S.SEL  = "SELECT ":FN.CU:" WITH INTER.BNK.DATE GE '20140326'"
    S.SEL := " AND POSTING.RESTRICT LT 90"
    CALL EB.READLIST(S.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CRT SELECTED
    LOOP
        REMOVE CU.ID FROM KEY.LIST SETTING POS.CU
    WHILE CU.ID:POS.CU
        CALL F.READ(FN.CU,CU.ID,R.CU,F.CU,ER.CU)
        CU.COM  = R.CU<EB.CUS.COMPANY.BOOK>
        CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
        CRD.STA = CU.LCL<1,CULR.CREDIT.STAT>
        CRD.COD = CU.LCL<1,CULR.CREDIT.CODE>
        EMP.NO  = CU.LCL<1,CULR.EMPLOEE.NO>
        INTER.DATE = CU.LCL<1,CULR.INTER.BNK.DATE>
        SS.ON   = (1000000 + EMP.NO)[2,6]
        IF CRD.STA NE '' OR CRD.COD GE 110 THEN
            GOTO NXT.CUST
        END
        CALL F.READ(FN.CU.AC,CU.ID,R.CU.AC,F.CU.AC,ER.CU.AC)
        LOOP
            REMOVE ACCT.ID FROM R.CU.AC SETTING POS.CU.AC
        WHILE ACCT.ID:POS.CU.AC
            GOSUB GET.DATA
        REPEAT
NXT.CUST:
    REPEAT
    RETURN
*===========================================
GET.DATA:
*---------

    CALL F.READ(FN.AC,ACCT.ID,R.AC,F.AC,ER.AC)
    AC.ONLINE.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
    AC.OPEN.BAL    = R.AC<AC.OPEN.ACTUAL.BAL>
    AC.WORKING.BAL = R.AC<AC.WORKING.BALANCE>
    AC.CUR         = R.AC<AC.CURRENCY>
    AC.CAT         = R.AC<AC.CATEGORY>
    CU.ID          = R.AC<AC.CUSTOMER>

    IF AC.CAT[1,2] NE '90' THEN
*Line [ 161 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR AC.CAT:' ' IN " 1710 1711 1721 1701 5000 5001 5081 5082 5083 5084 5085 5090 5091 1021 1022 5079 5080 21078 21079 " SETTING POS.NOSHOW THEN '' ELSE
**##            FINDSTR AC.CAT:' ' IN " 1002 1407 1408 1413 1445 1455 " SETTING POS.SS THEN NULL ELSE
            IF INTER.DATE EQ TDD1 THEN
                CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
                STE.BAL = OPENING.BAL
            END ELSE
                CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,TDD1,TDD1,ID.LIST,OPENING.BAL,ER)
**##                CALL F.READ(FN.ENT,ACCT.ID,ID.LIST,F.ENT,ER.ENT)
                STE.BAL = OPENING.BAL
            END
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS.STE
            WHILE STE.ID:POS.STE
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.OUR.REF = R.STE<AC.STE.OUR.REFERENCE>
                IF NOT(STE.OUR.REF) THEN STE.OUR.REF = R.STE<AC.STE.TRANS.REFERENCE>
*Line [ 178 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR '\B' IN STE.OUR.REF SETTING Y.XX THEN STE.OUR.REF = FIELD(STE.OUR.REF,'\B',1) ELSE NULL
                STE.AC.NO   = R.STE<AC.STE.ACCOUNT.NUMBER>
                STE.VAL     = R.STE<AC.STE.VALUE.DATE>
                STE.DAT     = R.STE<AC.STE.BOOKING.DATE>
                STE.TR.TYPE = R.STE<AC.STE.TRANSACTION.CODE>
                STE.CUR     = R.STE<AC.STE.CURRENCY>
                IF STE.CUR EQ LCCY THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                END ELSE
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                IF STE.AMT LT 0 THEN STE.D.C = "D" ELSE STE.D.C = "C"
                STE.INP     = FIELD(R.STE<AC.STE.INPUTTER>,'_',2)
                STE.CHQ     = STE.ID
                CALL SCB.CHQ.NO(STE.CHQ)
                IF NOT(NUM(STE.CHQ)) THEN STE.CHQ = ''
                TXN.DESC    = STE.TR.TYPE:"*":STE.AMT:"*":STE.ID
**##                    CALL SCB.GET.STMT.DESC(TXN.DESC)
**##                    STE.POS.DSC  = TXN.DESC[1,40]
**##                    STE.POS.DSC2 = TXN.DESC[41,80]
*----------------------------------------
                FT.TERM.LOC = ''
                IF STE.OUR.REF[1,2] EQ 'FT' THEN
                    CALL F.READ.HISTORY(FN.FT.HIS,STE.OUR.REF,R.FT.HIS,F.FT.HIS,ER.FR.HIS)
                    FT.LCL      = R.FT.HIS<FT.LOCAL.REF>
                    IF ER.FR.HIS THEN
                        CALL F.READ(FN.FT,STE.OUR.REF,R.FT,F.FT,ER.FR)
                        FT.LCL      = R.FT<FT.LOCAL.REF>
                    END
                    FT.TERM.LOC = FT.LCL<1,FTLR.TERMINAL.LOC>
                END
                STE.POS.DSC2  = FT.TERM.LOC[1,40]
                STE.POS.DSC2  = CHANGE(STE.POS.DSC2,'-',' ')
                STE.POS.DSC2  = TRIM(STE.POS.DSC2)
                STE.POS.DSC  = ''
*Line [ 214 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                IF STE.POS.DSC2 EQ '' THEN STE.POS.DSC2 = STE.CHQ ELSE NULL
*----------------------------------------
                STE.BAL += STE.AMT
                STE.POS.BAL = FMT(STE.BAL,"L2")
                STE.POS.BAL = CHANGE(STE.POS.BAL,'.','')
*-----------------------------------
                IF STE.ID THEN
                    GOSUB SEND.REC
                END
*-----------------------------------
            REPEAT
**##            END
        END
    END
    RETURN
*=============================================
SEND.REC:
*---------
    Y.STE.AMT = FMT(ABS(STE.AMT),"L2")
    Y.STE.AMT = CHANGE(Y.STE.AMT,'.','')
    RET.REC = "001":AST:NL:AST:NL:AST:ACCT.ID:AST:AC.CUR:AST:STE.VAL:AST:STE.DAT:AST:STE.TR.TYPE:AST:NL:AST:STE.POS.DSC:AST:STE.D.C:AST:STE.CUR:AST:Y.STE.AMT:AST:NL:AST:NL:AST:NL:AST:STE.POS.DSC2:AST:STE.INP:AST:NL:AST:STE.OUR.REF:AST:STE.POS.BAL

    CRT RET.REC

    XX.DATA  = RET.REC
    WRITESEQ XX.DATA TO BB ELSE
        CRT " ERROR WRITE FILE "
    END

    RETURN
END
