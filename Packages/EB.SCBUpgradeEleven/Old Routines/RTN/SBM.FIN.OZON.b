* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-194</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.FIN.OZON
*    SUBROUTINE SBM.FIN.OZON

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FIN.OZON

*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CLOSESEQ BB

    PRINT "FINISHED"

    RETURN

*-----------------------------INITIALIZATIONS------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "OZON.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"OZON.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "OZON.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE OZON.CSV CREATED IN "&SAVEDLISTS&"'
        END
        ELSE
            STOP 'Cannot create OZON.CSV File IN "&SAVEDLISTS&"'
        END
    END

*************************************************************

    FN.OZ = 'F.SCB.FIN.OZON' ; F.OZ = '' ; R.OZ = ''
    CALL OPF( FN.OZ,F.OZ)


    ETEXT     = ''
    ETEXT1    = ''
    I         = 1
    K         = 1
    J         = 1
    RETURN

*------------------------SELECT ALL OZONS ----------------------
PROCESS:
    T.DATE = TODAY
    T.SEL  = "SELECT F.SCB.FIN.OZON WITH MATURITY.DATE GE ":T.DATE

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ETEXT)
*------------------------WRITE HEADER ----------------------------

    BB.DATA  = '����� �������':','
    BB.DATA := '����� ���������':','
    BB.DATA := '������ �������':','
    BB.DATA := '������ ��������':','
    BB.DATA := '���� �����':','
    BB.DATA := '���� ������':','
    BB.DATA := '�����':','
    BB.DATA := '��� �����':','
    BB.DATA := '������ ������'

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*------------------------GET DATA ---------------------------
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            EZN.PERIOD = "C"

            CALL F.READ( FN.OZ,KEY.LIST<I>, R.OZ, F.OZ, ETEXT1)
            OZON.ID        = KEY.LIST<I>
            ESD.DATE       = R.OZ<OZN.ESDAAR.DATE>
            MAT.DATE       = R.OZ<OZN.MATURITY.DATE>
            RECORD.VALUE   = R.OZ<OZN.RECORDED.VALUE>
            DISCOUNT.VALUE = R.OZ<OZN.DISCOUNTED.VALUE>

*======================== CALCUALTIONS =========================
            CALL CDD("", ESD.DATE,MAT.DATE,EZN.PERIOD)
            RETURN.RATE   = (((RECORD.VALUE - DISCOUNT.VALUE) * 365)/(EZN.PERIOD * DISCOUNT.VALUE))*100
            DISCOUNT.RATE = 100/(((EZN.PERIOD/365) * RETURN.RATE)+1)
            GROSS         = RECORD.VALUE - DISCOUNT.VALUE
            DAILY.RETURN  = GROSS / EZN.PERIOD

            BB.DATA  = ESD.DATE[7,2]:'/':ESD.DATE[5,2]:'/':ESD.DATE[1,4]:','
            BB.DATA := MAT.DATE[7,2]:'/':MAT.DATE[5,2]:'/':MAT.DATE[1,4]:','
            BB.DATA := RECORD.VALUE:','
            BB.DATA := DISCOUNT.VALUE:','
            BB.DATA := DROUND(DISCOUNT.RATE,3):','
            BB.DATA := DROUND(RETURN.RATE,3):','
            BB.DATA := DROUND(GROSS,2):','
            BB.DATA := EZN.PERIOD:','
            BB.DATA := DROUND(DAILY.RETURN,2)

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END
    RETURN


*******************************************************
END
