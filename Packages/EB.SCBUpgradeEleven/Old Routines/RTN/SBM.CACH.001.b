* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.CACH.001

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 44 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.CACH.001'
    CALL PRINTER.ON(REPORT.ID,'')

    XX  = SPACE(120) ; XX1 = SPACE(120)
    COMP = ID.COMPANY

    TD1 = TODAY[1,6]:'01'
    CALL CDT("",TD1,'-1C')

    RETURN
*===============================================================
CALLDB:
    FN.LN  = 'F.RE.STAT.REP.LINE'     ; F.LN  = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL'  ; F.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = ''
    FN.CUR = 'FBNK.CURRENCY'          ; F.CUR = ''

    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.CUR,F.CUR)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""

    OPN.BAL.LCY = 0  ; DB.MOV.TOT  = 0
    CLS.BAL.LCY = 0  ; CR.MOV.TOT  = 0
    DB.MOV.LCY  = 0  ; CLS.BAL.TOT = 0
    CR.MOV.LCY  = 0  ; OPN.BAL.TOT = 0
    DB.MOV      = 0  ; CR.MOV      = 0
    CLS.BAL     = 0  ; OPN.BAL = 0
    AMT         = 0
*---------------------------------------------------
    T.SEL  = "SELECT ":FN.LN:" WITH @ID IN (GENLED.0220 GENLED.0230 GENLED.0231 GENLED.0250 GENLED.0710 GENLED.0730) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
*************************************************************************
            FOR X = 1 TO POS
                BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":COMP
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                RATE = R.CCY<RE.BCP.RATE,X>
                CUR.ID  = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,E2)
                CUR.DESC = R.CUR<EB.CUR.CCY.NAME,2>
*----------------------------------------------------
                DB.MOV<1,X>  += R.BAL<RE.SLB.DB.MVMT.MTH>
                CR.MOV<1,X>  += R.BAL<RE.SLB.CR.MVMT.MTH>
                CLS.BAL<1,X> += R.BAL<RE.SLB.CLOSING.BAL>

                OPN.BAL<1,X> = CLS.BAL<1,X> - CR.MOV<1,X> - DB.MOV<1,X>

                DB.MOV.TOT  += R.BAL<RE.SLB.DB.MVMT.MTH> * RATE
                CR.MOV.TOT  += R.BAL<RE.SLB.CR.MVMT.MTH> * RATE
                CLS.BAL.TOT += R.BAL<RE.SLB.CLOSING.BAL> * RATE
                OPN.BAL.TOT  = CLS.BAL.TOT - CR.MOV.TOT - DB.MOV.TOT

                DB.MOV.TOT   = DROUND(DB.MOV.TOT,'2')
                CR.MOV.TOT   = DROUND(CR.MOV.TOT,'2')
                OPN.BAL.TOT  = DROUND(OPN.BAL.TOT,'2')
                CLS.BAL.TOT  = DROUND(CLS.BAL.TOT,'2')
                IF I EQ SELECTED THEN
                    GOSUB PRINT.DET
                END
            NEXT X
        NEXT I
    END
    XX1<1,I>[1,25]   = "�������� ����"
    XX1<1,I>[30,15]  = OPN.BAL.TOT
    XX1<1,I>[50,15]  = CR.MOV.TOT
    XX1<1,I>[70,15]  = DB.MOV.TOT
    XX1<1,I>[95,15]  = CLS.BAL.TOT

    PRINT STR('=',120)
    PRINT XX1<1,I>
    PRINT STR('=',120)

    RETURN
*-------------------------------------------
PRINT.DET:
    AMT<1,X> = OPN.BAL<1,X> + CR.MOV<1,X> + DB.MOV<1,X> + CLS.BAL<1,X>
    IF AMT<1,X> NE '0' THEN
        XX<1,I>[1,25]   = CUR.DESC
        XX<1,I>[30,15]  = OPN.BAL<1,X>
        XX<1,I>[50,15]  = CR.MOV<1,X>
        XX<1,I>[70,15]  = DB.MOV<1,X>
        XX<1,I>[95,15]  = CLS.BAL<1,X>

        PRINT XX<1,I>
        PRINT STR(' ',120)
        OPN.BAL<1,X> = 0
        CR.MOV<1,X>  = 0
        DB.MOV<1,X>  = 0
        CLS.BAL<1,X> = 0
        XX = ''
    END
    RETURN

*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TD = TODAY
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD1[7,2]:'/':TD1[5,2]:"/":TD1[1,4]
    YYBRN  = BRANCH
    DATY   = TODAY

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"� ������� ������� ������ ��� - ������ ����� : ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������" :SPACE(20):"���� �������":SPACE(10):"���� ����":SPACE(10):"���� ����":SPACE(15):"������ �������"
    PR.HD :="'L'":STR('_',120)
    HEADING PR.HD
    RETURN
*==============================================================
END
