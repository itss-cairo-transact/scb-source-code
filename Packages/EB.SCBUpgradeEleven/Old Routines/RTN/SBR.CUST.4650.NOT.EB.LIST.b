* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    PROGRAM SBR.CUST.4650.NOT.EB.LIST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*----------------------------------------------
    FILE.NAME = "SBR.CUST.4650.NOT.EB.LIST.TXT"
    OPENSEQ "/home/signat/" , FILE.NAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat/":' ':FILE.NAME
        HUSH OFF
    END
    OPENSEQ "/home/signat/" , FILE.NAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CREATED IN /home/signat/'
        END ELSE
            STOP 'Cannot create File IN /home/signat/'
        END
    END

    EOF     = ''
    BB.DATA = "" ; FLG = 0
    FN.AC   = 'FBNK.ACCOUNT'  ; F.AC  = '' ; R.AC  = ''
    CALL OPF(FN.AC,F.AC)
    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.CU.AC = "FBNK.CUSTOMER.ACCOUNT" ; F.CU.AC = '' ; R.CU.AC = ''
    CALL OPF(FN.CU.AC, F.CU.AC)
    FN.CAT = "F.CATEGORY" ; F.CAT = "" ; R.CAT = ""
    CALL OPF(FN.CAT, F.CAT)

    BB.DATA  = "Customer.ID":","
*    BB.DATA := "Customer.Name":","
    BB.DATA := "Account.Number":","
*    BB.DATA := "Category.Name":","
    BB.DATA := "Citizen.ID":","
    BB.DATA := "Client.Type(New.Sector)":","
    BB.DATA := "ENGLISH.Name":","
    BB.DATA := "CIF.Key(Customer.ID)":","
    BB.DATA := "ENGLISH.Address":","
    BB.DATA := "Telephone":","
    BB.DATA := "GOVERNORATE":","
    BB.DATA := "REGION":","
    BB.DATA := "Postal Code":","
    BB.DATA := "EMAIL":","
    BB.DATA := "BIRTH DATE":","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    T.SEL  = "SSELECT FBNK.CUSTOMER WITH NEW.SECTOR EQ 4650 AND CONTACT.DATE GE 20200401 "
    T.SEL := "AND INTER.BNK.DATE EQ '' AND POSTING.RESTRICT EQ '' BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU.AC,KEY.LIST<I>, R.CU.AC, F.CU.AC, E.CU.AC)
            LOOP
                REMOVE ACC.NO FROM R.CU.AC SETTING POS
            WHILE ACC.NO:POS
                BREAK
            REPEAT

            IF ACC.NO THEN
                CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,E.AC)
                CURR = R.AC<AC.CURRENCY>
                CATG = R.AC<AC.CATEGORY>
                CALL F.READ(FN.CAT,CATG, R.CAT, F.CAT, E.CAT)

*    IF CATG GE 1001 AND CATG LE 1099 THEN FLG = '1'
*    IF CATG GE 6500 AND CATG LE 6599 THEN FLG = '1'
*    IF CURR EQ 'EGP' AND FLG EQ '1' THEN

                CUS.IDD  = KEY.LIST<I>
                CUS.IDD  = FMT(CUS.IDD, 'R%8')
*                IF CUS.IDD EQ '01211672' THEN DEBUG

                BB.DATA  = "" ; FLG = 0
                BB.DATA  = CUS.IDD:","
                CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,E.CUS)
*  BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:","
                BB.DATA := ACC.NO :","
*  BB.DATA := R.CAT<EB.CAT.SHORT.NAME>:","
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>:","
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>:","
                NAME.1   = R.CUS<EB.CUS.NAME.1><1,1>
                NAME.1   = TRIM(NAME.1 , ',' , 'A')
                BB.DATA := NAME.1:","
                BB.DATA := CUS.IDD:","
                EN.ADD   = R.CUS<EB.CUS.ADDRESS><1,1>
                BB.DATA := TRIM(EN.ADD,',','A'):","
                PHON     = ""
                PHON     = R.CUS<EB.CUS.SMS.1>
                IF NOT(PHON) THEN
                    TEL = R.CUS<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
*Line [ 131 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    FFF = DCOUNT(TEL,@SM)
                    FOR HH = 1 TO FFF
                        XXX = LEN(TEL<1,1,HH>)
                        IF TEL<1,1,HH>[1,2] EQ "01" AND XXX EQ 11 THEN
                            PHON = TEL<1,1,HH>
                        END
                    NEXT HH
                END

                BB.DATA := PHON:","
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>:","
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.REGION>:",,"
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>:","
                DAT = R.CUS<EB.CUS.BIRTH.INCORP.DATE>
                DAT = FMT(DAT,"####/##/##")
                BB.DATA := DAT:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
*   END
*REPEAT
        NEXT I
    END
    CLOSESEQ BB
    STOP
END
