* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*******---------BAKRY-------2020/06/09-----------******
    PROGRAM SBM.OVRD.DIFF.NEW

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    GOSUB OPENFILES
    GOSUB PROCESS
    RETURN



OPENFILES:
*---------
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.DR'
    F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ADI = "FBNK.ACCOUNT.DEBIT.INT" ; F.ADI = "" ; R.ADI = ""
    CALL OPF(FN.ADI,F.ADI)
    TT = TODAY
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
    TTS = DAT
    START.DAT = DAT[1,6]:'01'
    END.DAT = DAT
    MON.NO.DAYS = "C"
    CALL CDD("",START.DAT,END.DAT,MON.NO.DAYS)
    MON.NO.DAYS ++

    ACCC=''
*    YTEXT = " Enter End Of Month Date :  "
*    CALL TXTINP(YTEXT, 8, 22, "17", "A")
*    TTS = COMI
*============================================================
    FN.ACCT = 'FBNK.ACCOUNT'     ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    FN.STMT.ACCT.DR = 'FBNK.STMT.ACCT.DR'     ; F.STMT.ACCT.DR = ''
    CALL OPF(FN.STMT.ACCT.DR,F.STMT.ACCT.DR)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.BI.DATE = 'FBNK.BASIC.INTEREST.DATE' ; F.BI.DATE = '';R.BI.DATE=''
    CALL OPF(FN.BI.DATE,F.BI.DATE)

*----------------------------------- START OPEN FILE ----------------------------
    OPENSEQ "&SAVEDLISTS&" , "OD.8.DIFF.CBE.LAST.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"OD.8.DIFF.CBE.LAST.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "OD.8.DIFF.CBE.LAST.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE OD.8.DIFF.CBE.LAST.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create OD.8.DIFF.CBE.LAST.csv File IN &SAVEDLISTS&'
        END
    END
*----------------------------------- END OPEN FILE -----------------------------
*----------------------------------- CREATE HEADER -----------------------------
    HEAD.DESC = "REPORT DATE,":DAT
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    HEAD.DESC = ""
    HEAD.DESC = "Contract NO.,"
    HEAD.DESC := "Customer,"
    HEAD.DESC := "Customer Name,"
    HEAD.DESC := "Int. Rate,"
    HEAD.DESC := "Int. Date,"
    HEAD.DESC := "Outstanding,"
    HEAD.DESC := 'Diff. Rate,'
    HEAD.DESC := "Difference,"
    HEAD.DESC := "No Of Days,"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------ END CREATE HEADER -------------------------------
    CHK.WRT.OLD = '' ; CHK.WRT.NEW = '' ; DAYS = 0 ; TOTAL.INT.ACC = 0 ; TOTAL.INT.ALL = 0
    RETURN
*--------------------------------------------------------------------------------
PROCESS:
*-------
    T.SEL.ACCT    = "SELECT ":FN.ACCT:" WITH ((CATEGORY GE 1585 AND CATEGORY LE 1587) OR CATEGORY EQ '1231' OR CATEGORY EQ '1589') BY @ID"
*    T.SEL.ACCT    = "SELECT ":FN.ACCT:" WITH @ID EQ 0130188210158502"
    CALL EB.READLIST(T.SEL.ACCT,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            NET.AMT  = 0
            ACC.NO   = KEY.LIST<HH>
            CALL F.READ(FN.ACCT,KEY.LIST<HH>,R.ACCT,F.ACCT,E1)
            CUS.ID      = R.ACCT<AC.CUSTOMER>
            CATEGORY    = R.ACCT<AC.CATEGORY>
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
            CUS.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            INT.RATE = 8
            INT.RATE.NEXT = 8
*DEBUG
*            WS.AC.DR.CNT = 0 ; WS.AD.DATE = '' ; WS.ADI.ID = ''
*            WS.AC.DR.CNT   = DCOUNT(R.ACCT<AC.ACCT.DEBIT.INT>,VM)
*            WS.AD.DATE     = R.ACCT<AC.ACCT.DEBIT.INT><1,WS.AC.DR.CNT>
*            WS.ADI.ID      = KEY.LIST<HH>:'-':WS.AD.DATE
*            CALL F.READ(FN.ADI,WS.ADI.ID,R.ADI,F.ADI,ERR2)
*            INT.RATE = R.ADI<IC.ADI.DR.INT.RATE,1>
*            INT.RATE.NEXT = R.ADI<IC.ADI.DR.INT.RATE,1>

            CHK.WRT.OLD = '' ; CHK.WRT.NEW = '' ; DAYS = 0
            GOSUB GETREC

            LD.DET.ARR = ''
*            WRITESEQ LD.DET.ARR TO BB ELSE
*                PRINT " ERROR WRITE FILE "
*            END
*-----------------------------------------------------------------------------------------------
            IF TOTAL.INT.ACC NE 0 THEN
                LD.DET.ARR = "'":KEY.LIST<HH>:",Total Account,":CUS.NAME:",":",":",":",":',':TOTAL.INT.ACC

                WRITESEQ LD.DET.ARR TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOTAL.INT.ACC = 0
            END
*-----------------------------------------------------------------------------------------------

        NEXT HH
*-----------------------------------------------------------------------------------------------
        LD.DET.ARR = ''
        LD.DET.ARR = 'Total Sheet,,,,,,,':TOTAL.INT.ALL

        WRITESEQ LD.DET.ARR TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*-----------------------------------------------------------------------------------------------
    END
    RETURN
*============================================================
GETREC:
*------

*-----------------------------------------------------------------------------------------------
    FOR II = 1 TO MON.NO.DAYS
*DEBUG
        ACC.ID = '' ; R.ACCT.REC = '' ; BALANCE.DATE = '' ; YBALANCE = '' ; YBALANCE.NEXT = ''
        ACC.ID = ACC.NO
        CHECK.DATE = START.DAT[1,6]:FMT(II,"R%2")
        CHECK.DATE.NEXT = START.DAT[1,6]:FMT(II+1,"R%2")
        BALANCE.DATE = CHECK.DATE
        BALANCE.DATE.NEXT = CHECK.DATE.NEXT
        RAT = "64EGP":CHECK.DATE
        RAT.NEXT = "64EGP":CHECK.DATE.NEXT
        CALL EB.GET.INTEREST.RATE(RAT,SCB.INT.RATE)
        CALL EB.GET.INTEREST.RATE(RAT.NEXT,SCB.INT.RATE.NEXT)
        SCB.DIF.RAT = SCB.INT.RATE + 2 - INT.RATE
        SCB.DIF.RAT.NEXT = SCB.INT.RATE.NEXT + 2 - INT.RATE.NEXT

        CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT.REC, "VALUE", BALANCE.DATE, "", YBALANCE, CR.MVMT, DR.MVMT, ERR)
        CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT.REC, "VALUE", BALANCE.DATE.NEXT, "", YBALANCE.NEXT, CR.MVMT, DR.MVMT, ERR)
        IF YBALANCE GE 0 THEN
            DAYS = 0
            YBALANCE = 0
        END ELSE
*            YBALANCE = YBALANCE * -1
*        TOTAL.INT = YBALANCE * SCB.DIF.RAT/36000
*        TOTAL.INT = DROUND(TOTAL.INT,2)
            Y.AMT = YBALANCE
            BI.DATE = CHECK.DATE
            DAYS++
        END
*-----------------------------------------------------------------------------------------------

        CHK.WRT.NEW = YBALANCE.NEXT:SCB.DIF.RAT.NEXT
        CHK.WRT.OLD = YBALANCE:SCB.DIF.RAT
        IF (CHK.WRT.NEW # CHK.WRT.OLD AND YBALANCE NE 0 ) OR ( II = MON.NO.DAYS AND YBALANCE NE 0 ) THEN
*DEBUG
            TOTAL.INT = YBALANCE * SCB.DIF.RAT/100 * DAYS/360
            TOTAL.INT = DROUND(TOTAL.INT,2)
            TOTAL.INT.ACC += TOTAL.INT
            TOTAL.INT.ALL += TOTAL.INT
*-----------------------------------------------------------------------------------------------
            LD.DET.ARR = "'":KEY.LIST<HH>:",":CUS.ID:",":CUS.NAME:",":INT.RATE:",":BI.DATE:",":Y.AMT:",":SCB.DIF.RAT:',':TOTAL.INT:",":DAYS

            WRITESEQ LD.DET.ARR TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
*-----------------------------------------------------------------------------------------------
*            CHK.WRT.OLD = Y.AMT:SCB.DIF.RAT
            Y.AMT = 0 ; DAYS = 0
        END
    NEXT II
    RETURN
END
