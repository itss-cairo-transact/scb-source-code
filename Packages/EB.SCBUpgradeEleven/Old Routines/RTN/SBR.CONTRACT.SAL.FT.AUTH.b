* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.CONTRACT.SAL.FT.AUTH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
************ OPEN FILES ****************

    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT  = ""
    CALL OPF (FN.FT,F.FT)

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION = "MECH1"

    OPENSEQ "MECH" , "CONTRCAT.SAL.OUT.A" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"CONTRACT.SAL.OUT.A"
        HUSH OFF
    END
    OPENSEQ "MECH" , "CONTRACT.SAL.OUT.A" TO BB ELSE
        CREATE BB THEN
        END ELSE
            STOP 'Cannot create CONTRACT.SAL.OUT.A File IN MECH'
        END
    END

    COMP       = ID.COMPANY
    COMP.ID    = ID.COMPANY[2]
    AUTH.NAME  = 'AUTHOR':COMP.ID
    AUTH.ID    = AUTH.NAME:'//':COMP

*--------------------------------------------------------------------

    T.SEL1 = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH TRANSACTION.TYPE EQ AC49 BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR NN = 1 TO SELECTED1
            CALL F.READ(FN.FT,KEY.LIST1<NN>,R.FT,F.FT,E4)
            SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/A/PROCESS,":AUTH.ID:",":KEY.LIST1<NN>:","

            OFS.MESSAGE.DATA = ""

            SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA

*            CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END
        NEXT NN
        TEXT = '�� �������' ; CALL REM
    END

    IF NOT(SELECTED1) THEN
        TEXT = '������� �� �� ���' ; CALL REM
    END
*--------------------------------------------------------------------
    RETURN
************************************************************
END
