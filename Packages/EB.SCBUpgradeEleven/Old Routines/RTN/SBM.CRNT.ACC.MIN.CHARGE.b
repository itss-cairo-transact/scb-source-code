* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
******************MAHMOUD 21/4/2013********************
    SUBROUTINE SBM.CRNT.ACC.MIN.CHARGE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHARGE.EXP
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.FEE.EXCP
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    TTDD = TODAY
*============== FOR TESTING ONLY - MUST BE HASHED IN LIVE ENVIRONMENT ======
***####    CALL LAST.WDAY(TTDD)
*================ END OF NOTE  ==============================================

    TTXX = TTDD
    CALL LAST.WDAY(TTXX)
    IF TTDD EQ TTXX THEN
        GOSUB CALL.DB
        GOSUB INITIATE
        GOSUB PROCESS
    END
    RETURN
**********************************************************************
CALL.DB:
*-------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = '' ; ERR.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ERR.FT = '' ; SELECTED.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ERR.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACT = '' ; R.ACT = '' ; ERR.ACT = ''
    CALL OPF(FN.ACT,F.ACT)
    FN.CHG = 'FBNK.FT.CHARGE.TYPE' ; F.CHG = '' ; R.CHG = '' ; ERR.CHG = ''
    CALL OPF(FN.CHG,F.CHG)
    FN.EXP = 'F.SCB.CHARGE.EXP' ; F.EXP = '' ; R.EXP = '' ; ERR.EXP = ''
    CALL OPF(FN.EXP,F.EXP)
    FN.EXCP = 'F.SCB.CUS.FEE.EXCP' ; F.EXCP = '' ; R.EXCP = '' ; ERR.EXCP = ''
    CALL OPF(FN.EXCP,F.EXCP)

    SELECTED = '' ; K.LIST = ''

    RETURN
**********************************************************************
INITIATE:
*---------
    F.PATH  = FN.OFS.IN
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = "OFS.IN"
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
***************************

    CHG.ID = "ACMINCHRG"
    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID,INC.PL)


    TDD1 = TTDD
    CRT TDD1
    EXC.RATE    = 1
    KK1 = 0
    USD.CUR.CODE = "USD"
    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,USD.CUR.CODE,USD.RATE)
    USD.RATE = USD.RATE<1,1>
    CALL DBR('CURRENCY':@FM:EB.CUR.SELL.RATE,USD.CUR.CODE,USD.SELL.RATE)
    USD.SELL.RATE = USD.SELL.RATE<1,1>
*##########################

    TDY1 = TODAY
    MIN.LCY.AMT = 2000
    MIN.USD.AMT = 500
    IF TDY1 LT '20130701' THEN
        START.DATE  = '20130225'
    END ELSE
        START.DATE  = '19600101'
    END
*##########################

    EXP.CUST = " 40300486 40300075 13300197 13300292 13300122 13300006 13300244 13300310 306306 601010 10001000 "

    RETURN
*********************************************************************
PROCESS:
*-------

    FT.SEL  = "SELECT ":FN.FT:" WITH TRANSACTION.TYPE EQ 'AC71' "
    FT.SEL := " AND CO.CODE EQ ":COMP

    CALL EB.READLIST(FT.SEL,FT.LIST,"",SELECTED.FT,ERR.FT)
    IF SELECTED.FT THEN
        TEXT = "�� ������� ���� ����� �� ���" ; CALL REM
        RETURN
    END

    A.SEL  = "SSELECT ":FN.ACC:" WITH CATEGORY IN (1001 1005)"
    A.SEL := " AND OPENING.DATE LE ":TTXX
    A.SEL := " AND CO.CODE EQ ":COMP
    CALL EB.READLIST(A.SEL,K.LIST,"",SELECTED,ERR.ACC)

    IF SELECTED THEN
        LOOP
            REMOVE ACC.ID FROM K.LIST SETTING POS.ACC
        WHILE ACC.ID:POS.ACC
            CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ERRR)
            ACC.CUR  = R.ACC<AC.CURRENCY>
            ACC.OPN  = R.ACC<AC.OPENING.DATE>
            ACC.CUS  = R.ACC<AC.CUSTOMER>
            ACC.COM  = R.ACC<AC.CO.CODE>
            ACC.BAL  = R.ACC<AC.ONLINE.ACTUAL.BAL>
            CALL DBR('CUSTOMER':@FM:EB.CUS.CONTACT.DATE,ACC.CUS,CUS.OPN.DATE)
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,ACC.CUS,CUS.SECTOR)
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,ACC.CUS,CUS.LCL)

            CALL DBR('CUSTOMER':@FM:EB.CUS.RELATION.CODE,ACC.CUS,WS.REL.CODE)
            CALL DBR('CUSTOMER':@FM:EB.CUS.REL.CUSTOMER,ACC.CUS,WS.CUS.REL.CODE)

            IF WS.REL.CODE EQ '110' THEN
                CALL F.READ(FN.EXCP,WS.CUS.REL.CODE,R.EXCP,F.EXCP,E5)
                WS.AMT.EXP = R.EXCP<FEE.MIN.BALANCE.FEE>
            END

            CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
            CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
            CALL DBR('SCB.CHARGE.EXP':@FM:CH.EXP.MIN.BAL.CHARGE,ACC.CUS,CUS.EXP)
*Line [ 181 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            IF CUS.EXP EQ 'YES' THEN '' ELSE
**##            FINDSTR ACC.CUS:' ' IN EXP.CUST SETTING EX.CU THEN NULL ELSE
*Line [ 184 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                IF CRD.STA NE '' OR CRD.COD EQ '110' OR CRD.COD EQ '120' THEN '' ELSE
                    IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN
                        IF CUS.OPN.DATE GT START.DATE THEN
                            GOSUB GET.CCY.MIN
                            ACT.ID = ACC.ID:"-":TDD1[1,6]
                            CALL F.READ(FN.ACT,ACT.ID,R.ACT,F.ACT,ERR1)
                            IF NOT(R.ACT) THEN
                                ACT.MIN.BAL  = ACC.BAL
                                ACT.BAL.DATE = TDD1[1,6]:"01"
                                IF ACT.MIN.BAL LT MIN.BAL.AMT THEN
                                    GOSUB GET.CCY.CHRG
                                    GOSUB OFS.CHARGE.REC
                                END
*GOSUB GET.ACT.ID
                            END ELSE
                                ACT.MIN.BAL  = MINIMUM(R.ACT<IC.ACT.BALANCE>)
                                IF ACT.MIN.BAL EQ '' THEN ACT.MIN.BAL = ACC.BAL
                                IF ACT.MIN.BAL LT MIN.BAL.AMT THEN
*Line [ 203 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                    CNT.ACT = DCOUNT(R.ACT<IC.ACT.BALANCE>,@VM)
                                    FOR AA1 = 1 TO CNT.ACT
                                        ACT.BAL.DATE = R.ACT<IC.ACT.DAY.NO,AA1>
                                        ACT.BAL.AMT  = R.ACT<IC.ACT.BALANCE,AA1>
                                        IF ACT.BAL.AMT LT MIN.BAL.AMT THEN
                                            MLT.LOC = AA1
                                            AA1 = CNT.ACT
                                        END
                                    NEXT AA1
                                    ACT.MIN.BAL = ACT.BAL.AMT
                                    ACT.DAY.NO  = ACT.BAL.DATE
                                    CRT ACC.ID:" - ":ACC.CUR:" - ":ACT.BAL.AMT:" - ":ACT.BAL.DATE
                                    GOSUB GET.CCY.CHRG
                                    GOSUB OFS.CHARGE.REC
                                END
                            END
                        END
                    END
                END
            END
        REPEAT
        TEXT = '�� ����� ������' ; CALL REM
    END ELSE
        TEXT = '�� ���� ���� ���' ; CALL REM
    END
    RETURN
*********************************************************************
GET.CCY.MIN:
*-----------
    IF ACC.CUR EQ 'EGP' THEN
        MIN.BAL.AMT = MIN.LCY.AMT
    END ELSE
        IF ACC.CUR EQ 'USD' THEN
            MIN.BAL.AMT = MIN.USD.AMT
        END ELSE
            CALL F.READ(FN.CUR,ACC.CUR,R.CUR,F.CUR,ERR.CUR)
            CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
            IF ACC.CUR EQ 'JPY' THEN
                CUR.RATE = CUR.RATE /100
            END
            IF CUR.RATE NE 0 AND CUR.RATE NE '' THEN
                EXC.RATE = USD.RATE / CUR.RATE
            END
            MIN.BAL.AMT = MIN.USD.AMT * EXC.RATE
        END
    END

    RETURN
****************************************************
GET.CCY.CHRG:
*-----------
    CALL F.READ(FN.CHG,CHG.ID,R.CHG,F.CHG,ER.CHG)
    CHG.CUR = R.CHG<FT5.CURRENCY>
    IF ACC.CUR EQ 'EGP' THEN
        IF WS.REL.CODE EQ '110' THEN
            DB.AMT = WS.AMT.EXP
        END ELSE
            LOCATE ACC.CUR IN CHG.CUR<1,1> SETTING POS.CHG THEN DB.AMT = R.CHG<FT5.FLAT.AMT,POS.CHG>
        END
    END ELSE
        LOCATE 'USD' IN CHG.CUR<1,1> SETTING POS.CHG THEN DB.AMT = R.CHG<FT5.FLAT.AMT,POS.CHG>
        IF ACC.CUR NE 'USD' THEN
            CALL F.READ(FN.CUR,ACC.CUR,R.CUR,F.CUR,ERR.CUR)
            SELL.RATE = R.CUR<EB.CUR.SELL.RATE,1>
            IF ACC.CUR EQ 'JPY' THEN
                SELL.RATE = SELL.RATE /100
            END
            IF SELL.RATE NE 0 AND SELL.RATE NE '' THEN
                SELL.EXC.RATE = USD.SELL.RATE / SELL.RATE
            END
            DB.AMT = DB.AMT * SELL.EXC.RATE
            IF ACC.CUR EQ 'JPY' THEN
                DB.AMT = DROUND(DB.AMT,0)
            END ELSE
                DB.AMT = DROUND(DB.AMT,2)
            END
        END
    END
    RETURN
****************************************************
GET.ACT.ID:
*---------
    TDDL = TDD1
    CALL MONTHS.BETWEEN(ACC.OPN,TDD1,MONTHS.NO)
    FOR MM1 = 1 TO MONTHS.NO
        CALL ADD.MONTHS(TDDL,'-1')
        ACT.ID = ACC.ID:"-":TDDL[1,6]
        CALL F.READ(FN.ACT,ACT.ID,R.ACT,F.ACT,ERRR1)
        IF NOT(ERRR1) THEN
            MM1 = MONTHS.NO
        END
    NEXT MM1
    RETURN
****************************************************
OFS.CHARGE.REC:
*--------------
    COMMA = ","
    CURR = ACC.ID[9,2]
    CR.PL = "PL":INC.PL
    DATEE = TODAY
    OFS.USER.INFO = "AUTO.CHRGE2":"/":"/":ACC.COM
***********

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC71":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":ACC.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":ACC.CUR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":ACC.ID:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.PL:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ACC.CUS


    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OFS.ID = "MINCHRG.":ACC.ID

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':ACC.COM: ON ERROR  TEXT = " ERROR "
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':ACC.COM: ON ERROR  TEXT = " ERROR "

    RETURN
****************************************************
    RETURN
END
