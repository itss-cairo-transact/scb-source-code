* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-111</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY MOHAMED SABRY 2011/12/01 ****
*********************************************

    SUBROUTINE  SBR.CHG.TELLER.ID

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.USER.99

**************************************************************
    WS.SIGN.ON = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 51 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL DBR("USER.SIGN.ON.NAME":@FM:EB.USO.USER.ID,WS.SIGN.ON,WS.USER.ID)

    FN.USR99      = "F.SCB.USER.99"                    ; F.USR99      = ""  ; R.USR99     = ""
    CALL OPF (FN.USR99,F.USR99)

    CALL F.READ(FN.USR99,WS.USER.ID,R.USR99,F.USR99,E.USR99)

    IF NOT (E.USR99) THEN
        WS.TLR.NO = R.USR99<USR.99.RESERVED9>
        GOSUB INITIALISE
        GOSUB BUILD.CLOSE.RECORD
        GOSUB BUILD.OPEN.RECORD
    END
    RETURN
**************************************************************
INITIALISE:
*----------
    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "TELLER.ID"
    SCB.VERSION = "OFS.MNGR"
    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION
    FN.TID     = "F.TELLER.ID"              ; F.TID     = ""  ; R.TID    = ""
    CALL OPF (FN.TID,F.TID)
    WS.FLD.NO = 0
    RETURN

**************************************************************
BUILD.CLOSE.RECORD:
*------------
    COMMA = ","
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    T.SEL = "SELECT ":FN.TID:" WITH USER EQ ":WS.USER.ID:" AND CO.CODE NE ":ID.COMPANY:" AND STATUS EQ 'OPEN' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
*    PRINT 'SELECTED = ':SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            TEXT = 'TELLER.ID ':KEY.LIST<I>:' WILL BE CLOSE' ; CALL REM
            CALL F.READ(FN.TID,KEY.LIST<I>,R.TID,F.TID,E.TID)
            WS.ID.COMPANY = R.TID<TT.TID.CO.CODE>
            WS.DEPT       = WS.ID.COMPANY[2]

            GOSUB EX.CLOSE

            IF OFS.MESSAGE.DATA NE '' THEN
                PRINT  OFS.MESSAGE.DATA
                SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.DEPT:"//":WS.ID.COMPANY:"," : KEY.LIST<I> : OFS.MESSAGE.DATA
*                CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                WS.FLD.NO = 0
*    GOSUB BUILD.OPEN.RECORD
            END
            OFS.MESSAGE.DATA = ''

        NEXT I
    END

    RETURN
**************************************************************
EX.CLOSE:
*----------
    WS.FLD.NO ++

    OFS.MESSAGE.DATA  :=  ",STATUS:":WS.FLD.NO:":1=":'CLOSE'
    RETURN
**************************************************************
BUILD.OPEN.RECORD:
*-----------------
*** UPDATED BY MOHAMED SABRY 2013/10/01
*********************************************

    COMMA = "," ; KEY.LIST=""  ; SELECTED=""  ;  ER.MSG="" ; R.TID = ""

*    T.SEL = "SELECT ":FN.TID:" WITH USER EQ ":WS.USER.ID:" AND CO.CODE EQ ":ID.COMPANY:" AND STATUS NE 'OPEN' BY @ID"
*    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
*    IF SELECTED THEN
*        FOR I = 1 TO SELECTED

    WS.DEPT     = ID.COMPANY[2]
    WS.TLR.CODE = WS.DEPT : WS.TLR.NO[1,2]

    TEXT = 'TELLER.ID ':WS.TLR.CODE:' WILL BE OPEN' ; CALL REM

*    CALL F.READ(FN.TID,WS.TLR.CODE,R.TID,F.TID,E.TID)

*    WS.ID.COMPANY = R.TID<TT.TID.CO.CODE>
*    WS.DEPT       = WS.ID.COMPANY[2]

    GOSUB EX.OPEN

    IF OFS.MESSAGE.DATA NE '' THEN
        PRINT  OFS.MESSAGE.DATA
        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.DEPT:"//":WS.ID.COMPANY:"," : WS.TLR.CODE : OFS.MESSAGE.DATA
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        WS.FLD.NO = 0
    END
    OFS.MESSAGE.DATA = ''

*        NEXT I
*    END

    RETURN
**************************************************************
EX.OPEN:
*----------
    WS.FLD.NO ++

    OFS.MESSAGE.DATA  :=  ",STATUS:":WS.FLD.NO:":1=":'OPEN'
    OFS.MESSAGE.DATA  :=  ",USER:":WS.FLD.NO:":1=":WS.USER.ID
    RETURN
**************************************************************
