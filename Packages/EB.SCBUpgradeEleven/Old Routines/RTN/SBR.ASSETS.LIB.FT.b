* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*    PROGRAM SBR.ASSETS.LIB.FT
    SUBROUTINE SBR.ASSETS.LIB.FT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ASSETS.LIB
*---------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
TEXT = 'DONE' ; CALL REM
    RETURN

INITIALISE:
    OPENSEQ "MECH" , "ASSETS.LIB.OFS" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"ASSETS.LIB.OFS"
        HUSH OFF
    END
    OPENSEQ "MECH" , "ASSETS.LIB.OFS" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ASSETS.LIB.OFS CREATED IN MECH'
        END ELSE
            STOP 'Cannot create ASSETS.LIB.OFS File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

***** READ FROM FILE *****

    FN.FT = "FBNK.FUNDS.TRANSFER" ; F.FT = ""
    CALL OPF (FN.FT,F.FT)

    FN.DEP = "F.SCB.ASSETS.LIB"  ; F.DEP  = ""
    CALL OPF (FN.DEP,F.DEP)

    FN.USER = "F.USER"  ; F.USER = ""
    CALL OPF (FN.USER,F.USER)

    TD = TODAY

    T.SEL = "SELECT ":FN.DEP:" WITH RECIEVE.DATE EQ ":TD:" AND RUN.DATE EQ '' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DEP.ID     = KEY.LIST<I>
            CALL F.READ(FN.DEP,DEP.ID,R.DEP,F.DEP,E4)
            DEBIT.ACC      = R.DEP<DEP.DEBIT.ACCOUNT>
            CREDIT.ACC     = R.DEP<DEP.CREDIT.ACCOUNT>
            CUR            = R.DEP<DEP.CURRENCY>
            AMT            = R.DEP<DEP.AMOUNT>
            COST.CEN       = R.DEP<DEP.COST.CENTER>
            COMP.CODE      = R.DEP<DEP.COMPANY.CODE>
            DR.THEIR.REF   = R.DEP<DEP.DEBIT.THEIR.REF>
            CR.THEIR.REF   = R.DEP<DEP.CREDIT.THEIR.REF>
            REC.DATE       = R.DEP<DEP.RECIEVE.DATE>
            RUN.DAT        = R.DEP<DEP.RUN.DATE>
            NOTES.1        = R.DEP<DEP.NOTES.1>
            NOTES.2        = R.DEP<DEP.NOTES.2>
            STAT           = R.DEP<DEP.STATUS>
            TWO = "2"
            GOSUB OFS.MSG

*           ERR.FLAG = FIELD(MSG.DATA,'/',3)[0,2]
*           FT.ID    = FIELD(MSG.DATA,'/',1)
*           IF ERR.FLAG NE "-1" AND RUN.DAT EQ '' THEN
            R.DEP<DEP.RUN.DATE>      = TD
*            R.DEP<DEP.TXN.REF>       = FT.ID
            IF STAT EQ '1' THEN
                R.DEP<DEP.STATUS> = '2'
            END
            CALL F.WRITE(FN.DEP ,DEP.ID, R.DEP)
            CALL JOURNAL.UPDATE(DEP.ID)
*           END
        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN ASSETS.LIB.OFS'
    EXECUTE 'DELETE ':"MECH":' ':"ASSETS.LIB.OFS"



    RETURN
**************************
OFS.MSG:
**********
    WS.USER = R.USER<EB.USE.SIGN.ON.NAME>

*    IDD = 'FUNDS.TRANSFER,MECH1,AUTO.CHRGE//':COMP.CODE:','
    IDD = 'FUNDS.TRANSFER,MECH1,':WS.USER:'//':COMP.CODE:','

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":','
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CUR:','
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CUR:','
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DEBIT.ACC:','
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CREDIT.ACC:','
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:','
    OFS.MESSAGE.DATA :=  "PROFIT.CENTRE.DEPT=":COST.CEN:','
    OFS.MESSAGE.DATA :=  "CO.CODE=":COMP.CODE:','
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":REC.DATE:','
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":REC.DATE:','
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":DR.THEIR.REF:','
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":CR.THEIR.REF:','
    OFS.MESSAGE.DATA :=  "LOCAL.REF:41:1=":NOTES.1:','
    OFS.MESSAGE.DATA :=  "LOCAL.REF:41:2=":NOTES.2:','
    OFS.MESSAGE.DATA :=  "LOCAL.REF:42:1=":NOTES.1:','
    OFS.MESSAGE.DATA :=  "LOCAL.REF:42:2=":NOTES.2:','
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":','
    OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
    OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO"


    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

    WRITESEQ MSG.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
END
