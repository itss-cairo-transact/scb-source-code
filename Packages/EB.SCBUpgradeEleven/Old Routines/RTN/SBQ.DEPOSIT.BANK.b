* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1248</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBQ.DEPOSIT.BANK
**    PROGRAM SBQ.DEPOSIT.BANK

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
**    REPORT.ID='SBQ.DEPOSIT.BANK'
    REPORT.ID='P.FUNCTION'

    CALL PRINTER.ON(REPORT.ID,'')

    AMT.CUR = 0 ; AMT.DEP = 0
    AMT.CUR150 = 0 ; AMT.CUR100  = 0 ; AMT.CUR50 = 0 ; AMT.CUR20 = 0 ; AMT.CUR10 = 0 ; AMT.CUR5 = 0 ; AMT.CUR4 = 0
    AMT.DEP150 = 0 ; AMT.DEP100  = 0 ; AMT.DEP50 = 0 ; AMT.DEP20 = 0 ; AMT.DEP10 = 0 ; AMT.DEP5 = 0 ; AMT.DEP4 = 0

    AMT.CUR.TOT   = 0 ; AMT.DEP.TOT   = 0
    NEW.CUS = ''

    GT150 = '���� �� 150 �����'
    GT100 = '���� �� 100 ����� ���� 150'
    GT50  = '���� �� 50 ����� ���� 100'
    GT20  = '���� �� 20 ����� ���� 50'
    GT10  = '���� �� 10 ����� ���� 20'
    GT5   = '���� �� 5 ����� ���� 10'
    LE5   = '��� �� �� ����� 5 �����'

    BANK.COUNT150 = 0 ; BANK.COUNT100 = 0 ; BANK.COUNT50 = 0 ; BANK.COUNT20 = 0
    BANK.COUNT20  = 0 ; BANK.COUNT10  = 0 ; BANK.COUNT5  = 0 ; BANK.COUNT4  = 0

    CUST.NAME150 = GT150 ; CUST.NAME100 = GT100 ; CUST.NAME50 = GT50 ; CUST.NAME20 = GT20
    CUST.NAME10  = GT10  ; CUST.NAME5   = GT5   ; CUST.NAME4  = LE5

    XX= SPACE(120)  ; XX1= SPACE(120) ; XX2= SPACE(120) ; XX3= SPACE(120)
    XX4= SPACE(120) ; XX5= SPACE(120) ; XX6= SPACE(120) ; XX7= SPACE(120) ; XX8= SPACE(120)

    WS.DAT1 = TODAY[1,6]:'01'
    CALL CDT("",WS.DAT1,'-1C')

    WS.DAT = WS.DAT1[1,6]

    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD.DETAIL' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (5001 21075 21076 21078) AND CBEM.OLD.SECTOR EQ 3222 AND CBEM.DATE EQ ":WS.DAT:" AND CBEM.CY NE 'EGP' AND CBEM.IN.FCY NE 0 AND CBEM.IN.FCY NE '' BY CBEM.CUSTOMER.CODE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            CUS.ID  = R.CBE<STD.CBEM.CUSTOMER.CODE>
            CATEG = R.CBE<STD.CBEM.CATEG>
            AMT   = R.CBE<STD.CBEM.IN.LCY>


         IF AMT LT 0 THEN
             AMT = AMT * -1
         END

            IF I EQ 1 THEN NEW.CUS = CUS.ID
            IF ( CUS.ID NE NEW.CUS )  THEN

                CUS.AMT = AMT.CUR + AMT.DEP

                IF CUS.AMT GT 150000000 THEN
                    BANK.COUNT150++
                    AMT.CUR150 += AMT.CUR
                    AMT.DEP150 += AMT.DEP

                END

                IF ( CUS.AMT GT 100000000 AND CUS.AMT LE 150000000 ) THEN
                    BANK.COUNT100++
                    AMT.CUR100 += AMT.CUR
                    AMT.DEP100 += AMT.DEP

                END

                IF ( CUS.AMT GT 50000000 AND CUS.AMT LE 100000000 ) THEN
                    BANK.COUNT50++
                    AMT.CUR50 += AMT.CUR
                    AMT.DEP50 += AMT.DEP

                END

                IF ( CUS.AMT GT 20000000 AND CUS.AMT LE 50000000 ) THEN
                    BANK.COUNT20++
                    AMT.CUR20 += AMT.CUR
                    AMT.DEP20 += AMT.DEP

                END

                IF ( CUS.AMT GT 10000000 AND CUS.AMT LE 20000000 ) THEN
                    BANK.COUNT10++
                    AMT.CUR10 += AMT.CUR
                    AMT.DEP10 += AMT.DEP

                END

                IF ( CUS.AMT GT 5000000 AND CUS.AMT LE 10000000 ) THEN
                    BANK.COUNT5++
                    AMT.CUR5 += AMT.CUR
                    AMT.DEP5 += AMT.DEP

                END

                IF CUS.AMT LE 5000000 THEN
                    BANK.COUNT4++
                    AMT.CUR4 += AMT.CUR
                    AMT.DEP4 += AMT.DEP

                END

                AMT.CUR = 0
                AMT.DEP = 0
            END

            IF CATEG EQ 5001 THEN
                AMT.CUR     += AMT
                AMT.CUR.TOT += AMT
            END

            IF CATEG EQ 21075 OR CATEG EQ 21076 OR CATEG EQ 21078 THEN
                AMT.DEP     += AMT
                AMT.DEP.TOT += AMT
            END

*----------------------------------------------------------
            NEW.CUS = CUS.ID

        NEXT I
        IF I EQ SELECTED THEN
            CUS.AMT = AMT.CUR + AMT.DEP

            IF CUS.AMT GT 150000000 THEN
                BANK.COUNT150++
                AMT.CUR150 += AMT.CUR
                AMT.DEP150 += AMT.DEP

            END

            IF ( CUS.AMT GT 100000000 AND CUS.AMT LE 150000000 ) THEN
                BANK.COUNT100++
                AMT.CUR100 += AMT.CUR
                AMT.DEP100 += AMT.DEP

            END

            IF ( CUS.AMT GT 50000000 AND CUS.AMT LE 100000000 ) THEN
                BANK.COUNT50++
                AMT.CUR50 += AMT.CUR
                AMT.DEP50 += AMT.DEP

            END

            IF ( CUS.AMT GT 20000000 AND CUS.AMT LE 50000000 ) THEN
                BANK.COUNT20++
                AMT.CUR20 += AMT.CUR
                AMT.DEP20 += AMT.DEP

            END

            IF ( CUS.AMT GT 10000000 AND CUS.AMT LE 20000000 ) THEN
                BANK.COUNT10++
                AMT.CUR10 += AMT.CUR
                AMT.DEP10 += AMT.DEP

            END

            IF ( CUS.AMT GT 5000000 AND CUS.AMT LE 10000000 ) THEN
                BANK.COUNT5++
                AMT.CUR5 += AMT.CUR
                AMT.DEP5 += AMT.DEP

            END

            IF CUS.AMT LE 5000000 THEN
                BANK.COUNT4++
                AMT.CUR4 += AMT.CUR
                AMT.DEP4 += AMT.DEP

            END

            AMT.CUR = 0
            AMT.DEP = 0
        END
    END
*BANK.COUNT =  BANK.COUNT150 + BANK.COUNT100 + BANK.COUNT50 + BANK.COUNT20 + BANK.COUNT20 + BANK.COUNT10 + BANK.COUNT5 + BANK.COUNT4

    AMT.CUR.TOT   = AMT.CUR.TOT / 1000
    AMT.DEP.TOT   = AMT.DEP.TOT / 1000
    AMT.CUR.TOT   = DROUND(AMT.CUR.TOT,'0')
    AMT.DEP.TOT   = DROUND(AMT.DEP.TOT,'0')
******************************************************

    AMT.CUR150    = AMT.CUR150 / 1000
    AMT.DEP150    = AMT.DEP150 / 1000
******************************************************
    AMT.CUR100    = AMT.CUR100 / 1000
    AMT.DEP100    = AMT.DEP100 / 1000
******************************************************
    AMT.CUR50     = AMT.CUR50 / 1000
    AMT.DEP50     = AMT.DEP50 / 1000
******************************************************
    AMT.CUR20     = AMT.CUR20 / 1000
    AMT.DEP20     = AMT.DEP20 / 1000
******************************************************
    AMT.CUR10     = AMT.CUR10 / 1000
    AMT.DEP10     = AMT.DEP10 / 1000
******************************************************
    AMT.CUR5      = AMT.CUR5 / 1000
    AMT.DEP5      = AMT.DEP5 / 1000
******************************************************
    AMT.CUR4      = AMT.CUR4 / 1000
    AMT.DEP4      = AMT.DEP4 / 1000
******************************************************
    AMT.CUR150    = DROUND(AMT.CUR150,'0')
    AMT.DEP150    = DROUND(AMT.DEP150,'0')
******************************************************
    AMT.CUR100    = DROUND(AMT.CUR100,'0')
    AMT.DEP100    = DROUND(AMT.DEP100,'0')
******************************************************
    AMT.CUR50     = DROUND(AMT.CUR50,'0')
    AMT.DEP50     = DROUND(AMT.DEP50,'0')
******************************************************
    AMT.CUR20     = DROUND(AMT.CUR20,'0')
    AMT.DEP20     = DROUND(AMT.DEP20,'0')
******************************************************
    AMT.CUR10     = DROUND(AMT.CUR10,'0')
    AMT.DEP10     = DROUND(AMT.DEP10,'0')
*****************************************************
    AMT.CUR5      = DROUND(AMT.CUR5,'0')
    AMT.DEP5      = DROUND(AMT.DEP5,'0')
******************************************************
    AMT.CUR4      = DROUND(AMT.CUR4,'0')
    AMT.DEP4      = DROUND(AMT.DEP4,'0')
******************************************************

    XX<1,1>[10,35]  = CUST.NAME150
    XX<1,1>[55,20]  = BANK.COUNT150
    XX<1,1>[90,20]  = AMT.CUR150
    XX<1,1>[110,20] = AMT.DEP150
    PRINT XX<1,1>
    PRINT STR('-',130)

    XX1<1,1>[10,35]  = CUST.NAME100
    XX1<1,1>[55,20]  = BANK.COUNT100
    XX1<1,1>[90,20]  = AMT.CUR100
    XX1<1,1>[110,20] = AMT.DEP100
    PRINT XX1<1,1>
    PRINT STR('-',130)

    XX3<1,1>[10,35]  = CUST.NAME50
    XX3<1,1>[55,20]  = BANK.COUNT50
    XX3<1,1>[90,20]  = AMT.CUR50
    XX3<1,1>[110,20] = AMT.DEP50
    PRINT XX3<1,1>
    PRINT STR('-',130)

    XX4<1,1>[10,35]  = CUST.NAME20
    XX4<1,1>[55,20]  = BANK.COUNT20
    XX4<1,1>[90,20]  = AMT.CUR20
    XX4<1,1>[110,20] = AMT.DEP20
    PRINT XX4<1,1>
    PRINT STR('-',130)

    XX5<1,1>[10,35]  = CUST.NAME10
    XX5<1,1>[55,20]  = BANK.COUNT10
    XX5<1,1>[90,20]  = AMT.CUR10
    XX5<1,1>[110,20] = AMT.DEP10
    PRINT XX5<1,1>
    PRINT STR('-',130)


    XX6<1,1>[10,35]  = CUST.NAME5
    XX6<1,1>[55,20]  = BANK.COUNT5
    XX6<1,1>[90,20]  = AMT.CUR5
    XX6<1,1>[110,20] = AMT.DEP5
    PRINT XX6<1,1>
    PRINT STR('-',130)

    XX8<1,1>[10,35]  = CUST.NAME4
    XX8<1,1>[55,20]  = BANK.COUNT4
    XX8<1,1>[90,20]  = AMT.CUR4
    XX8<1,1>[110,20] = AMT.DEP4
    PRINT XX8<1,1>
    PRINT STR('-',130)


*---------------------------------------------------
    XX2= SPACE(120)
    PRINT STR('=',130)
    XX2<1,1>[1,35]   = "�����������"
*   XX2<1,1>[55,20]  = BANK.COUNT
    XX2<1,1>[90,20]  = AMT.CUR.TOT
    XX2<1,1>[110,20] = AMT.DEP.TOT

**CRT AMT.DEP.TOT

    PRINT XX2<1,1>
    PRINT STR('=',130)

    RETURN
*---------------------------------------------------
*==============================================================
PRINT.HEAD:
*---------
    YYBRN  = "�� ���� �����"
    DATY   = TODAY

    TDD = TODAY[1,6]
    TD = TDD - 2
    TDF = TDD - 1

    TD1 = TD[1,4]:'/':TD[5,2]
    TD2 = TDF[1,4]:'/':TDF[5,2]

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"��� ���� ������� ����� ��� ������ ��������"
    PR.HD :="'L'":SPACE(45):STR('_',45)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(10):"������":SPACE(35):"��� ������":SPACE(25):"������� �������� �����"
    PR.HD :="'L'":SPACE(75):"":SPACE(15):"�����":SPACE(5):"�����":SPACE(5):""
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
