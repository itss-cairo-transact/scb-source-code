* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBM.STATIC.AC.LD.02

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD

*********************** OPENING FILES *****************************
    FN.AC  = "F.CBE.STATIC.AC.LD" ; F.AC   = ""
    CALL OPF (FN.AC,F.AC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*------------------------------------------------------------------
********************** CLEAR & COPY NEW DATA TO OLD DATA ******************

    T.SEL = "SELECT ":FN.AC
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*------------------------------------------------------

            R.AC<ST.CBE.CUR.AC.LEO>         = R.AC<ST.CBE.CUR.AC.LE>
            R.AC<ST.CBE.CUR.AC.LE.DRO>      = R.AC<ST.CBE.CUR.AC.LE.DR>
            R.AC<ST.CBE.DEPOST.AC.LE.1YO>   = R.AC<ST.CBE.DEPOST.AC.LE.1Y>
            R.AC<ST.CBE.DEPOST.AC.LE.2YO>   = R.AC<ST.CBE.DEPOST.AC.LE.2Y>
            R.AC<ST.CBE.DEPOST.AC.LE.3YO>   = R.AC<ST.CBE.DEPOST.AC.LE.3Y>
            R.AC<ST.CBE.DEPOST.AC.LE.MOR3O> = R.AC<ST.CBE.DEPOST.AC.LE.MOR3>
            R.AC<ST.CBE.CER.AC.LE.3YO>      = R.AC<ST.CBE.CER.AC.LE.3Y>
            R.AC<ST.CBE.CER.AC.LE.5YO>      = R.AC<ST.CBE.CER.AC.LE.5Y>
            R.AC<ST.CBE.CER.AC.LE.GOLDO>    = R.AC<ST.CBE.CER.AC.LE.GOLD>
            R.AC<ST.CBE.SAV.AC.LEO>         = R.AC<ST.CBE.SAV.AC.LE>
            R.AC<ST.CBE.SAV.AC.LE.DRO>      = R.AC<ST.CBE.SAV.AC.LE.DR>
            R.AC<ST.CBE.BLOCK.AC.LEO>       = R.AC<ST.CBE.BLOCK.AC.LE>
            R.AC<ST.CBE.MARG.LC.LEO>        = R.AC<ST.CBE.MARG.LC.LE>
            R.AC<ST.CBE.MARG.LG.LEO>        = R.AC<ST.CBE.MARG.LG.LE>
            R.AC<ST.CBE.FACLTY.LEO>         = R.AC<ST.CBE.FACLTY.LE>
            R.AC<ST.CBE.FACLTY.LE.CRO>      = R.AC<ST.CBE.FACLTY.LE.CR>
            R.AC<ST.CBE.LOANS.LE.SO>        = R.AC<ST.CBE.LOANS.LE.S>
            R.AC<ST.CBE.LOANS.LE.S.CRO>     = R.AC<ST.CBE.LOANS.LE.S.CR>
            R.AC<ST.CBE.LOANS.LE.MO>        = R.AC<ST.CBE.LOANS.LE.M>
            R.AC<ST.CBE.LOANS.LE.M.CRO>     = R.AC<ST.CBE.LOANS.LE.M.CR>
            R.AC<ST.CBE.LOANS.LE.LO>        = R.AC<ST.CBE.LOANS.LE.L>
            R.AC<ST.CBE.LOANS.LE.L.CRO>     = R.AC<ST.CBE.LOANS.LE.L.CR>
            R.AC<ST.CBE.COMRCL.PAPER.LEO>   = R.AC<ST.CBE.COMRCL.PAPER.LE>
            R.AC<ST.CBE.CUR.AC.EQO>         = R.AC<ST.CBE.CUR.AC.EQ>
            R.AC<ST.CBE.CUR.AC.EQ.DRO>      = R.AC<ST.CBE.CUR.AC.EQ.DR>
            R.AC<ST.CBE.DEPOST.AC.EQ.1YO>   = R.AC<ST.CBE.DEPOST.AC.EQ.1Y>
            R.AC<ST.CBE.DEPOST.AC.EQ.2YO>   = R.AC<ST.CBE.DEPOST.AC.EQ.2Y>
            R.AC<ST.CBE.DEPOST.AC.EQ.3YO>   = R.AC<ST.CBE.DEPOST.AC.EQ.3Y>
            R.AC<ST.CBE.DEPOST.AC.EQ.MOR3O> = R.AC<ST.CBE.DEPOST.AC.EQ.MOR3>
            R.AC<ST.CBE.CER.AC.EQ.3YO>      = R.AC<ST.CBE.CER.AC.EQ.3Y>
            R.AC<ST.CBE.CER.AC.EQ.5YO>      = R.AC<ST.CBE.CER.AC.EQ.5Y>
            R.AC<ST.CBE.CER.AC.EQ.GOLDO>    = R.AC<ST.CBE.CER.AC.EQ.GOLD>
            R.AC<ST.CBE.SAV.AC.EQO>         = R.AC<ST.CBE.SAV.AC.EQ>
            R.AC<ST.CBE.SAV.AC.EQ.DRO>      = R.AC<ST.CBE.SAV.AC.EQ.DR>
            R.AC<ST.CBE.BLOCK.AC.EQO>       = R.AC<ST.CBE.BLOCK.AC.EQ>
            R.AC<ST.CBE.MARG.LC.EQO>        = R.AC<ST.CBE.MARG.LC.EQ>
            R.AC<ST.CBE.MARG.LG.EQO>        = R.AC<ST.CBE.MARG.LG.EQ>
            R.AC<ST.CBE.FACLTY.EQO>         = R.AC<ST.CBE.FACLTY.EQ>
            R.AC<ST.CBE.FACLTY.EQ.CRO>      = R.AC<ST.CBE.FACLTY.EQ.CR>
            R.AC<ST.CBE.LOANS.EQ.SO>        = R.AC<ST.CBE.LOANS.EQ.S>
            R.AC<ST.CBE.LOANS.EQ.S.CRO>     = R.AC<ST.CBE.LOANS.EQ.S.CR>
            R.AC<ST.CBE.LOANS.EQ.MO>        = R.AC<ST.CBE.LOANS.EQ.M>
            R.AC<ST.CBE.LOANS.EQ.M.CRO>     = R.AC<ST.CBE.LOANS.EQ.M.CR>
            R.AC<ST.CBE.LOANS.EQ.LO>        = R.AC<ST.CBE.LOANS.EQ.L>
            R.AC<ST.CBE.LOANS.EQ.L.CRO>     = R.AC<ST.CBE.LOANS.EQ.L.CR>
            R.AC<ST.CBE.COMRCL.PAPER.EQO>   = R.AC<ST.CBE.COMRCL.PAPER.EQ>

            CALL F.WRITE(FN.AC,KEY.LIST<I>,R.AC)
            CALL  JOURNAL.UPDATE(KEY.LIST<I>)

        NEXT I
    END
END
