* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBM.STATIC.AC.LD.01

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD

*********************** OPENING FILES *****************************
    FN.AC  = "F.CBE.STATIC.AC.LD" ; F.AC   = "" ; R.AC = ""
    FN.CBE = "F.CBE.STATIC.MAST"  ; F.CBE  = ""

    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.CBE,F.CBE)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATD = TODAY
    DATM = DATD[1,6]
    DAT  = DATM:"01"
    CALL CDT ('',DAT,'-1C')
    WS.TD = DAT[1,6]
*------------------------------------------------------------------

    T.SEL = "SELECT ":FN.CBE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            AC.LD.ID = KEY.LIST<I>
********************** COPY NEW DATA ******************************

            R.AC<ST.CBE.CUSTOMER.CODE>     = R.CBE<C.CBE.CUSTOMER.CODE>
            R.AC<ST.CBE.BR>                = R.CBE<C.CBE.BR>
            R.AC<ST.CBE.NEW.SECTOR>        = R.CBE<C.CBE.NEW.SECTOR>
            R.AC<ST.CBE.NEW.INDUSTRY>      = R.CBE<C.CBE.NEW.INDUSTRY>
            R.AC<ST.CBE.LAST.UP.DATE>      = WS.TD
            R.AC<ST.CBE.CUR.AC.LE>         = R.CBE<C.CBE.CUR.AC.LE>
            R.AC<ST.CBE.CUR.AC.LE.DR>      = R.CBE<C.CBE.CUR.AC.LE.DR>
            R.AC<ST.CBE.DEPOST.AC.LE.1Y>   = R.CBE<C.CBE.DEPOST.AC.LE.1Y>
            R.AC<ST.CBE.DEPOST.AC.LE.2Y>   = R.CBE<C.CBE.DEPOST.AC.LE.2Y>
            R.AC<ST.CBE.DEPOST.AC.LE.3Y>   = R.CBE<C.CBE.DEPOST.AC.LE.3Y>
            R.AC<ST.CBE.DEPOST.AC.LE.MOR3> = R.CBE<C.CBE.DEPOST.AC.LE.MOR3>
            R.AC<ST.CBE.CER.AC.LE.3Y>      = R.CBE<C.CBE.CER.AC.LE.3Y>
            R.AC<ST.CBE.CER.AC.LE.5Y>      = R.CBE<C.CBE.CER.AC.LE.5Y>
            R.AC<ST.CBE.CER.AC.LE.GOLD>    = R.CBE<C.CBE.CER.AC.LE.GOLD>
            R.AC<ST.CBE.SAV.AC.LE>         = R.CBE<C.CBE.SAV.AC.LE>
            R.AC<ST.CBE.SAV.AC.LE.DR>      = R.CBE<C.CBE.SAV.AC.LE.DR>
            R.AC<ST.CBE.BLOCK.AC.LE>       = R.CBE<C.CBE.BLOCK.AC.LE>
            R.AC<ST.CBE.MARG.LC.LE>        = R.CBE<C.CBE.MARG.LC.LE>
            R.AC<ST.CBE.MARG.LG.LE>        = R.CBE<C.CBE.MARG.LG.LE>
            R.AC<ST.CBE.FACLTY.LE>         = R.CBE<C.CBE.FACLTY.LE>
            R.AC<ST.CBE.FACLTY.LE.CR>      = R.CBE<C.CBE.FACLTY.LE.CR>
            R.AC<ST.CBE.LOANS.LE.S>        = R.CBE<C.CBE.LOANS.LE.S>
            R.AC<ST.CBE.LOANS.LE.S.CR>     = R.CBE<C.CBE.LOANS.LE.S.CR>
            R.AC<ST.CBE.LOANS.LE.M>        = R.CBE<C.CBE.LOANS.LE.M>
            R.AC<ST.CBE.LOANS.LE.M.CR>     = R.CBE<C.CBE.LOANS.LE.M.CR>
            R.AC<ST.CBE.LOANS.LE.L>        = R.CBE<C.CBE.LOANS.LE.L>
            R.AC<ST.CBE.LOANS.LE.L.CR>     = R.CBE<C.CBE.LOANS.LE.L.CR>
            R.AC<ST.CBE.COMRCL.PAPER.LE>   = R.CBE<C.CBE.COMRCL.PAPER.LE>
            R.AC<ST.CBE.CUR.AC.EQ>         = R.CBE<C.CBE.CUR.AC.EQ>
            R.AC<ST.CBE.CUR.AC.EQ.DR>      = R.CBE<C.CBE.CUR.AC.EQ.DR>
            R.AC<ST.CBE.DEPOST.AC.EQ.1Y>   = R.CBE<C.CBE.DEPOST.AC.EQ.1Y>
            R.AC<ST.CBE.DEPOST.AC.EQ.2Y>   = R.CBE<C.CBE.DEPOST.AC.EQ.2Y>
            R.AC<ST.CBE.DEPOST.AC.EQ.3Y>   = R.CBE<C.CBE.DEPOST.AC.EQ.3Y>
            R.AC<ST.CBE.DEPOST.AC.EQ.MOR3> = R.CBE<C.CBE.DEPOST.AC.EQ.MOR3>
            R.AC<ST.CBE.CER.AC.EQ.3Y>      = R.CBE<C.CBE.CER.AC.EQ.3Y>
            R.AC<ST.CBE.CER.AC.EQ.5Y>      = R.CBE<C.CBE.CER.AC.EQ.5Y>
            R.AC<ST.CBE.CER.AC.EQ.GOLD>    = R.CBE<C.CBE.CER.AC.EQ.GOLD>
            R.AC<ST.CBE.SAV.AC.EQ>         = R.CBE<C.CBE.SAV.AC.EQ>
            R.AC<ST.CBE.SAV.AC.EQ.DR>      = R.CBE<C.CBE.SAV.AC.EQ.DR>
            R.AC<ST.CBE.BLOCK.AC.EQ>       = R.CBE<C.CBE.BLOCK.AC.EQ>
            R.AC<ST.CBE.MARG.LC.EQ>        = R.CBE<C.CBE.MARG.LC.EQ>
            R.AC<ST.CBE.MARG.LG.EQ>        = R.CBE<C.CBE.MARG.LG.EQ>
            R.AC<ST.CBE.FACLTY.EQ>         = R.CBE<C.CBE.FACLTY.EQ>
            R.AC<ST.CBE.FACLTY.EQ.CR>      = R.CBE<C.CBE.FACLTY.EQ.CR>
            R.AC<ST.CBE.LOANS.EQ.S>        = R.CBE<C.CBE.LOANS.EQ.S>
            R.AC<ST.CBE.LOANS.EQ.S.CR>     = R.CBE<C.CBE.LOANS.EQ.S.CR>
            R.AC<ST.CBE.LOANS.EQ.M>        = R.CBE<C.CBE.LOANS.EQ.M>
            R.AC<ST.CBE.LOANS.EQ.M.CR>     = R.CBE<C.CBE.LOANS.EQ.M.CR>
            R.AC<ST.CBE.LOANS.EQ.L>        = R.CBE<C.CBE.LOANS.EQ.L>
            R.AC<ST.CBE.LOANS.EQ.L.CR>     = R.CBE<C.CBE.LOANS.EQ.L.CR>
            R.AC<ST.CBE.COMRCL.PAPER.EQ>   = R.CBE<C.CBE.COMRCL.PAPER.EQ>

            Y.SECTOR = R.CBE<C.CBE.NEW.SECTOR>
            SECTOR.ID = Y.SECTOR[2,3]
            R.AC<ST.RESERVED10> = SECTOR.ID


            CALL F.WRITE(FN.AC,AC.LD.ID,R.AC)
            CALL  JOURNAL.UPDATE(AC.LD.ID)

        NEXT I
    END
END
