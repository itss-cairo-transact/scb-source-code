* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.FT.RET.VODAFONE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ������  '
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
    END ELSE
        TEXT = '�� ������ �� ��������' ; CALL REM
    END

    RETURN
*---------------------------------------------------------
INITIALISE:
    OPENSEQ "MECH" , "FT.RET.VODAFONE" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"FT.RET.VODAFONE"
        HUSH OFF
    END
    OPENSEQ "MECH" , "FT.RET.VODAFONE" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FT.RET.VODAFONE CREATED IN MECH'
        END ELSE
            STOP 'Cannot create FT.RET.VODAFONE File IN MECH'
        END
    END

    COMMA   = ','
    V.DATE  = TODAY

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME = 'VODRET.csv'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            ACC.NO   = FIELD(Y.MSG,",",1)
            AMT      = FIELD(Y.MSG,",",2)

            CR.ACCT = 'EGP1614000010099'
            DR.ACCT = ACC.NO


            IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//EG0010099,'

            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":'EGP':COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":'EGP':COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"Vodafone.Invoice":COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":"Vodafone.Invoice":COMMA
            OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB"

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN FT.RET.VODAFONE'
    EXECUTE 'DELETE ':"MECH":' ':"FT.RET.VODAFONE"

    TEXT = '�� �������� �� ��������' ; CALL REM

**************************
    RETURN
END
