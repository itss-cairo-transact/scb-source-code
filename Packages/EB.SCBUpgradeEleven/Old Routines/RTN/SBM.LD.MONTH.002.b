* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LD.MONTH.002

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 39 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LD.MONTH.002'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]

    DATD = TODAY
    DATM = DATD[1,6]
    DAT = DATM:"01"
    CALL CDT ('',DAT,'-1C')
    CATEG1 = ''
    AMT.EGP = 0 ; AMT.SAR = 0
    AMT.USD = 0 ; AMT.GBP = 0
    AMT.EUR = 0 ; AMT.CHF = 0
    LCY.CATEG = 0
    COUNT.EGP = 0 ; COUNT.SAR = 0
    COUNT.USD = 0 ; COUNT.GBP = 0
    COUNT.EUR = 0 ; COUNT.CHF = 0

    TOT.AMT.EGP = 0 ; TOT.AMT.SAR = 0
    TOT.AMT.USD = 0 ; TOT.AMT.GBP = 0
    TOT.AMT.EUR = 0 ; TOT.AMT.CHF = 0
    TOT.LCY.CATEG = 0
    TOT.FCY.CATEG = 0
    TOT.COUNT.EGP = 0 ; TOT.COUNT.SAR = 0
    TOT.COUNT.USD = 0 ; TOT.COUNT.GBP = 0
    TOT.COUNT.EUR = 0 ; TOT.COUNT.CHF = 0

    XX  = SPACE(132) ; XX1 = SPACE(132)
    XX2 = SPACE(132) ; XX3 = SPACE(132)
    XX4 = SPACE(132)
    RETURN
*===============================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    T.SEL = "SELECT F.CBE.MAST.AC.LD WITH CBEM.CATEG GE 21001 AND CBEM.CATEG LE 21010 AND CBEM.BR EQ ":COMP.BR:" BY CBEM.CATEG"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E2)
            CATEG = R.CBE<C.CBEM.CATEG>
            IF I EQ 1 THEN CATEG1 = CATEG
****************
            IF CATEG1 NE CATEG THEN
                XX<1,1>[1,15]    = LCY.CATEG
                XX<1,1>[20,15]   = AMT.EGP
                XX<1,1>[35,15]   = AMT.SAR
                XX<1,1>[50,15]   = AMT.USD
                XX<1,1>[65,15]   = AMT.GBP
                XX<1,1>[80,15]   = AMT.EUR
                XX<1,1>[95,15]   = AMT.CHF
                XX<1,1>[115,15]  = DESC.CATEG

                XX1<1,1>[1,15]    = TOT.COUNT
                XX1<1,1>[20,15]   = COUNT.EGP
                XX1<1,1>[35,15]   = COUNT.SAR
                XX1<1,1>[50,15]   = COUNT.USD
                XX1<1,1>[65,15]   = COUNT.GBP
                XX1<1,1>[80,15]   = COUNT.EUR
                XX1<1,1>[95,15]   = COUNT.CHF
                XX1<1,1>[115,15]  = "DEP.NO"

                PRINT STR(' ',130)
                PRINT XX1<1,1>
                PRINT XX<1,1>
                PRINT STR('-',130)

                AMT.EGP = 0 ; AMT.SAR = 0
                AMT.USD = 0 ; AMT.GBP = 0
                AMT.EUR = 0 ; AMT.CHF = 0

                COUNT.EGP = 0 ; COUNT.SAR = 0
                COUNT.USD = 0 ; COUNT.GBP = 0
                COUNT.EUR = 0 ; COUNT.CHF = 0
                LCY.CATEG = 0

                DESC.CATEG = ''
                XX  = '' ; XX1 = ''
                CATEG1 = CATEG
            END
****************
            IF CATEG EQ CATEG1 THEN
                CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,CATEG,DESC.CATEG)
                CUR = R.CBE<C.CBEM.CY>
                IF CUR EQ 'EGP' THEN
                    AMT.EGP += R.CBE<C.CBEM.IN.LCY>
                    AMT.EGP = DROUND(AMT.EGP,'2')
                    COUNT.EGP++

                    TOT.AMT.EGP += R.CBE<C.CBEM.IN.LCY>
                    TOT.AMT.EGP = DROUND(TOT.AMT.EGP,'2')
                    TOT.COUNT.EGP++

                END
                IF CUR EQ 'SAR' THEN
                    AMT.SAR += R.CBE<C.CBEM.IN.FCY>
                    AMT.SAR = DROUND(AMT.SAR,'2')
                    COUNT.SAR++

                    TOT.AMT.SAR += R.CBE<C.CBEM.IN.FCY>
                    TOT.AMT.SAR = DROUND(TOT.AMT.SAR,'2')
                    TOT.COUNT.SAR++

                END

                IF CUR EQ 'USD' THEN
                    AMT.USD += R.CBE<C.CBEM.IN.FCY>
                    AMT.USD = DROUND(AMT.USD,'2')
                    COUNT.USD++

                    TOT.AMT.USD += R.CBE<C.CBEM.IN.FCY>
                    TOT.AMT.USD = DROUND(TOT.AMT.USD,'2')
                    TOT.COUNT.USD++

                END

                IF CUR EQ 'GBP' THEN
                    AMT.GBP += R.CBE<C.CBEM.IN.FCY>
                    AMT.GBP = DROUND(AMT.GBP,'2')
                    COUNT.GBP++

                    TOT.AMT.GBP += R.CBE<C.CBEM.IN.FCY>
                    TOT.AMT.GBP = DROUND(TOT.AMT.GBP,'2')
                    TOT.COUNT.GBP++

                END

                IF CUR EQ 'EUR' THEN
                    AMT.EUR += R.CBE<C.CBEM.IN.FCY>
                    AMT.EUR = DROUND(AMT.EUR,'2')
                    COUNT.EUR++

                    TOT.AMT.EUR += R.CBE<C.CBEM.IN.FCY>
                    TOT.AMT.EUR = DROUND(TOT.AMT.EUR,'2')
                    TOT.COUNT.EUR++

                END

                IF CUR EQ 'CHF' THEN
                    AMT.CHF += R.CBE<C.CBEM.IN.FCY>
                    AMT.CHF = DROUND(AMT.CHF,'2')
                    COUNT.CHF++

                    TOT.AMT.CHF += R.CBE<C.CBEM.IN.FCY>
                    TOT.AMT.CHF = DROUND(TOT.AMT.CHF,'2')
                    TOT.COUNT.CHF++

                END
                LCY.CATEG += R.CBE<C.CBEM.IN.LCY>
                LCY.CATEG = DROUND(LCY.CATEG,'2')

                TOT.LCY.CATEG += R.CBE<C.CBEM.IN.LCY>
                TOT.LCY.CATEG = DROUND(TOT.LCY.CATEG,'2')

                TOT.COUNT = COUNT.EGP + COUNT.SAR + COUNT.USD + COUNT.GBP + COUNT.EUR + COUNT.CHF
                TOTAL.COUNT = TOT.COUNT.EGP + TOT.COUNT.SAR + TOT.COUNT.USD + TOT.COUNT.GBP + TOT.COUNT.EUR + TOT.COUNT.CHF
            END
            IF I EQ SELECTED THEN
                XX<1,1>[1,15]    = LCY.CATEG
                XX<1,1>[20,15]   = AMT.EGP
                XX<1,1>[35,15]   = AMT.SAR
                XX<1,1>[50,15]   = AMT.USD
                XX<1,1>[65,15]   = AMT.GBP
                XX<1,1>[80,15]   = AMT.EUR
                XX<1,1>[95,15]   = AMT.CHF
                XX<1,1>[115,15]  = DESC.CATEG

                XX1<1,1>[1,15]    = TOT.COUNT
                XX1<1,1>[20,15]   = COUNT.EGP
                XX1<1,1>[35,15]   = COUNT.SAR
                XX1<1,1>[50,15]   = COUNT.USD
                XX1<1,1>[65,15]   = COUNT.GBP
                XX1<1,1>[80,15]   = COUNT.EUR
                XX1<1,1>[95,15]   = COUNT.CHF
                XX1<1,1>[115,15]  = "DEP.NO"

                PRINT STR(' ',130)
                PRINT XX1<1,1>
                PRINT XX<1,1>
                PRINT STR('-',130)

                AMT.EGP = 0 ; AMT.SAR = 0
                AMT.USD = 0 ; AMT.GBP = 0
                AMT.EUR = 0 ; AMT.CHF = 0

                COUNT.EGP = 0 ; COUNT.SAR = 0
                COUNT.USD = 0 ; COUNT.GBP = 0
                COUNT.EUR = 0 ; COUNT.CHF = 0
                LCY.CATEG = 0
                DESC.CATEG = ''
                XX  = '' ; XX1 = ''
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    PRINT STR('=',130)
    XX2<1,1>[1,15]    = TOT.LCY.CATEG
    XX2<1,1>[20,15]   = TOT.AMT.EGP
    XX2<1,1>[35,15]   = TOT.AMT.SAR
    XX2<1,1>[50,15]   = TOT.AMT.USD
    XX2<1,1>[65,15]   = TOT.AMT.GBP
    XX2<1,1>[80,15]   = TOT.AMT.EUR
    XX2<1,1>[95,15]   = TOT.AMT.CHF
    XX2<1,1>[115,15]  = "��������"

    TOT.FCY.CATEG = TOT.LCY.CATEG - TOT.AMT.EGP
    TOT.FCY.CATEG = DROUND(TOT.FCY.CATEG,'2')
    XX4<1,1>[1,15]    = "TOTAL.FCY = "
    XX4<1,1>[15,15]   = TOT.FCY.CATEG

    XX3<1,1>[1,15]    = TOTAL.COUNT
    XX3<1,1>[20,15]   = TOT.COUNT.EGP
    XX3<1,1>[35,15]   = TOT.COUNT.SAR
    XX3<1,1>[50,15]   = TOT.COUNT.USD
    XX3<1,1>[65,15]   = TOT.COUNT.GBP
    XX3<1,1>[80,15]   = TOT.COUNT.EUR
    XX3<1,1>[95,15]   = TOT.COUNT.CHF
    XX3<1,1>[115,15]  = "TOTAL.DEP.NO"

    PRINT STR(' ',130)
    PRINT XX3<1,1>
    PRINT XX2<1,1>
    PRINT STR('=',130)
    PRINT XX4<1,1>
    PRINT STR('-',130)
    RETURN

*===============================================================
PRINT.HEAD:
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN = BRANCH
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    T.DAY1 = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������� ���� ���� ������� ����� ��� �������"
    PR.HD :="'L'":SPACE(60):"���� : ":T.DAY1
    PR.HD :="'L'":SPACE(45):STR('_',60)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"TOTAL.EQUIVALENT":SPACE(5):"EGP":SPACE(10):"SAR" :SPACE(10):"USD":SPACE(10):"GBP ":SPACE(10):"EUR ":SPACE(10):"CHF"
    PR.HD :="'L'":SPACE(1):STR('=',130)

    HEADING PR.HD
    RETURN
*==============================================================
END
