* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-25</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/05/15  ***
********************************************
    SUBROUTINE SBM.MONEYBOX.YOUNG.M
*    PROGRAM SBM.MONEYBOX.YOUNG.M

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

    WS.FLG = 0
****

*---------------------------------------------------------------------------------------------------------
*** ���� ����� �������

    GOSUB CHK.OFS.IN.EMPTY

    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"GET.SAVE.ACCT.UNDERAGE")
    GOSUB WAIT.OFS.EMPTY
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.UNDERAGE.SAVE.INTEREST")
    GOSUB WAIT.OFS.EMPTY
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.UNDERAGE.SAVE.OFS")
    GOSUB WAIT.OFS.EMPTY

** ����� ����� ������ ������

    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"GET.SAVE.ACCT.UNDERAGE.D")
    GOSUB WAIT.OFS.EMPTY
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.UNDERAGE.SAVE.INTEREST.D")
    GOSUB WAIT.OFS.EMPTY
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.UNDERAGE.SAVE.OFS.D")
    GOSUB WAIT.OFS.EMPTY

    RETURN
************************************************************
CHK.OFS.IN.EMPTY:
**    T.SEL.IN = "SELECT OFS.IN "
    T.SEL.IN = "SELECT F.OFS.MESSAGE.QUEUE"
    CALL EB.READLIST(T.SEL.IN,KEY.LIST.IN,"",SELECTED.IN,ER.IN)
    IF SELECTED.IN THEN
        PRINT " "
        PRINT "OFS.IN ... ��� ������ �� ��� �� ���� ����� �� "
        PRINT " OFS IN = ":SELECTED.IN
        PRINT " ... Press any key to Continue .... "
        INPUT X
    END
    RETURN
************************************************************
WAIT.OFS.EMPTY:
**  T.SEL.IN = "SELECT OFS.IN "
    T.SEL.IN = "SELECT F.OFS.MESSAGE.QUEUE"
    CALL EB.READLIST(T.SEL.IN,KEY.LIST.IN,"",SELECTED.IN,ER.IN)
    IF SELECTED.IN THEN
        IF WS.FLG = 0 THEN
            PRINT " "
            PRINT "OFS.IN ��� ������ ����� ���� ������� �������� �� "
            WS.FLG = 1
            GO TO WAIT.OFS.EMPTY
        END
        GO TO WAIT.OFS.EMPTY
    END
    RETURN
************************************************************
