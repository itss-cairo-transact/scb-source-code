* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.1621.1
*    PROGRAM     SBM.C.PRT.1621.1
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*    $INSERT GLOBUS.BP  I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

*------------------------------------
    FLG = 0
    YTEXT = " Enter (1-PRINT) OR (2-CBE)"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.NUM  = COMI
    IF WS.NUM EQ 1 THEN
        REPORT.ID='SBM.C.PRT.001'
        FLG = 1
        GOSUB INITIAL
    END
    IF WS.NUM EQ 2 THEN
        REPORT.ID='1621.1'
        FLG = 1
        GOSUB INITIAL
    END
    IF FLG EQ 0 THEN
        TEXT = "INVALID INPUT NUMBER"
        RETURN
    END
    TEXT = "DONE" ; CALL REM
    RETURN
*------------------------------------
INITIAL:
*����� ������� ���

* 1621
*------------------------------------
    FN.AC = "F.CBE.MAST.AC.LD"
    F.AC = ""
*    FN.BR = "F.DEPT.ACCT.OFFICER"
*    F.BR = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*------------------------------------------------------
    CALL OPF(FN.AC,F.AC)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
*    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.AMT = "0"
    WS.COMN.AREA = 0
    MSG.CY = ""
    WS.RATE = 0
    WS.MLT.DIVD = 0

    WS.COL = 0
    WS.RAW = 0
    WS.COMN = 0
    MSG.AC = ""
    WS.AC.ID = ""
    WS.CY = ""
    WS.CATEG = ""
    R.AC = ""
*----------------------------------------
    WS.HD.T  = "������� �������� ������ ����� �������"
*-----------------------------------------

    WS.HD.TA = "����� ��� 1621"
*---------------------------------------------------
*---------------------------------------------------
    WS.HD.T2A = "���� ��� "
    WS.PAGE.NO    = "1"
*----------------------------------------------------
    WS.PRG.1 = "SBM.C.PRT.1621.1"
*------------------------------------------------------
*------------------ REPORT HEADERS --------------------
*-----------------------------------------------------
    HEAD.01 = "���� �����"
*----------------------------------------------------
    HEAD.01.1 = "����� ������    "
*---------------------------------------------------
    HEAD.NO = "��� �������� "
*--------------------------------------------------
    HEAD.AMT = "����"
*--------------------------------------------------
********    ARRAY1 = ""
******                                 ARRAY
*ARRAY SUB 1 = ������
*          2 = ��� �������� ���� �����
*          3 = ������ ���� �����
*          4 = ��� �������� ���� ������
*          5 = ������ ���� ������

    DIM ARRAY1(5,5)

    ARRAY1(1,1) = "5 ���� ���� ����                    "
    ARRAY1(1,2) = "0"
    ARRAY1(1,3) = "0"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"
*-----------------------------------
    ARRAY1(2,1) = "���� �� 5 ���� ���� - 10 ���� ����   "
    ARRAY1(2,2) = "0"
    ARRAY1(2,3) = "0"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
*-----------------------------------
    ARRAY1(3,1) = "���� �� 10 ���� ���� - 50 ��� ����  "
    ARRAY1(3,2) = "0"
    ARRAY1(3,3) = "0"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
*-----------------------------------
    ARRAY1(4,1) = "���� �� 50 ��� ����                "
    ARRAY1(4,2) = "0"
    ARRAY1(4,3) = "0"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
*-----------------------------------
    ARRAY1(5,1) = "��������                            "
    ARRAY1(5,2) = "0"
    ARRAY1(5,3) = "0"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"
*-----------------------------------
*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    GOSUB A.050.GET.ALL.BR
*    GOSUB A.5000.PRT.HEAD
**------------------------------------------START PROCESSING
*    GOSUB A.100.PROCESS
*    GOSUB A.300.PRNT
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*---------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE 99 THEN
            GOTO A.050.A
        END
*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        WS.ARRY.RAW = 1
        WS.ARRY.COL = 1
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0
*            GOSUB A.5100.PRT.SPACE.PAGE
            GOSUB A.5000.PRT.HEAD
            GOSUB A.300.PRNT
        END
*
A.050.A:
    REPEAT
    RETURN
*---------------------------------
A.100.PROCESS:
*    SEL.CMD = "SELECT ":FN.AC:" WITH(CATEGORY GE 6501 AND LE 6504)"
    IF  WS.BR NE 99 THEN
        SEL.CMD = "SELECT ":FN.AC:" WITH CBEM.CATEG IN( 6501 6502 6503 6504 6511) AND CBEM.BR EQ ":WS.BR
    END

    IF  WS.BR EQ 99 THEN
        SEL.CMD = "SELECT ":FN.AC:" WITH CBEM.CATEG IN( 6501 6502 6503 6504 6511)"
    END

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.AC.ID FROM SEL.LIST SETTING POS
    WHILE WS.AC.ID:POS


        CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,MSG.AC)

*        IF   R.AC<AC.ACCOUNT.OFFICER>  NE 1 THEN
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END

        IF   R.AC<C.CBEM.BR> NE WS.BR THEN
            GOTO AAA
        END
A.100.A:
*        WS.AMT = R.AC<AC.ONLINE.ACTUAL.BAL>
        WS.AMT = R.AC<C.CBEM.IN.LCY>
*        WS.CY = R.AC<AC.CURRENCY>
        WS.CY = R.AC<C.CBEM.CY>
*        WS.CATEG = R.AC<AC.CATEGORY>

*        IF WS.CY NE "EGP" THEN
*            GOSUB A.100.FORN
*            WS.AMT = WS.COMN.AREA
*        END

        GOSUB A.200.ACUM
*-----------------------------------------------------
AAA:
    REPEAT
    RETURN

*A.100.FORN:
*    MSG.CY = ""
*    CALL F.READ(FN.CY,WS.CY,R.CY,F.CY,MSG.CY)
*    IF MSG.CY EQ "" THEN
*        GOSUB A.120.CALC.EQV
*    END
*    IF MSG.CY NE "" THEN
*        WS.COMN.AREA = "0"
*    END
*    RETURN

*A.120.CALC.EQV:
*    WS.RATE = R.CY<EB.CUR.MID.REVAL.RATE,1>
*    WS.MLT.DIVD = R.CY<EB.CUR.QUOTATION.CODE>
*
*    IF WS.MLT.DIVD EQ "0" THEN
*        WS.COMN.AREA = WS.AMT * WS.RATE
*    END
*    IF WS.MLT.DIVD EQ "2" THEN
*        WS.COMN.AREA = WS.AMT / WS.RATE
*    END
*    RETURN
*
*-------------------------------------------------
A.200.ACUM:
    IF  WS.AMT LE 5000 THEN
        WS.RAW  = 1
    END

    IF  WS.AMT GT 5000 AND WS.AMT LE 10000 THEN
        WS.RAW  = 2
    END

    IF  WS.AMT GT 10000 AND WS.AMT LE 50000 THEN
        WS.RAW  = 3
    END

    IF  WS.AMT GT 50000 THEN
        WS.RAW  = 4
    END
    IF WS.CY EQ "EGP" THEN
        WS.COL = 2
    END
    IF WS.CY NE "EGP" THEN
        WS.COL = 4
    END
*------------------------------------------------------

    WS.COMN = WS.AMT / 1000

    ARRAY1(WS.RAW,WS.COL) = ARRAY1(WS.RAW,WS.COL) + 1
    ARRAY1(5,WS.COL) = ARRAY1(5,WS.COL) + 1

    WS.COL = WS.COL + 1
    ARRAY1(WS.RAW,WS.COL) = ARRAY1(WS.RAW,WS.COL) + WS.COMN
    ARRAY1(5,WS.COL) = ARRAY1(5,WS.COL) + WS.COMN
    WS.FLAG.PRT = 1
    RETURN
*****                                     ARRAY   ������� ��

A.300.PRNT:
    FOR I = 1 TO 5
        GOSUB A.330.PRT.DTAL
    NEXT I
    RETURN
A.330.PRT.DTAL:
    IF I EQ 5 THEN
        XX = SPACE(132)
        XX<1,1>[40,5]   = "-----"
        XX<1,1>[53,7]   = "-------"

        XX<1,1>[73,5]   = "-----"
        XX<1,1>[84,7]   = "-------"
        PRINT XX<1,1>
    END

    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)

    XX<1,1>[40,5]   = ARRAY1(I,2)
    XX<1,1>[53,7]   = FMT(ARRAY1(I,3), "R0,")

    XX<1,1>[73,5]   = ARRAY1(I,4)
    XX<1,1>[84,7]   = FMT(ARRAY1(I,5), "R0,")

    PRINT XX<1,1>

    IF I EQ 5 THEN
        XX = SPACE(132)
        XX<1,1>[40,5]   = "====="
        XX<1,1>[53,7]   = "======="

        XX<1,1>[73,5]   = "====="
        XX<1,1>[84,7]   = "======="
        PRINT XX<1,1>
    END
    ARRAY1(I,2) = 0
    ARRAY1(I,3) = 0
    ARRAY1(I,4) = 0
    ARRAY1(I,5) = 0

    RETURN

**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

*    CALL F.READ(FN.BR,WS.BR.H,R.BR,F.BR,MSG.BR)
*    WS.BR.NAME = R.BR<EB.DAO.NAME>
***    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PRINT "#1621.1"

    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
***    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ "
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(30):WS.HD.TA
    PR.HD :="'L'":SPACE(107):WS.HD.T2A:SPACE(3):WS.PAGE.NO
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(40):HEAD.01:SPACE(25):HEAD.01.1
    PR.HD :="'L'":SPACE(34):HEAD.NO:SPACE(6):HEAD.AMT:SPACE(10):HEAD.NO:SPACE(4):HEAD.AMT
****    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
*    PRINT

    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
