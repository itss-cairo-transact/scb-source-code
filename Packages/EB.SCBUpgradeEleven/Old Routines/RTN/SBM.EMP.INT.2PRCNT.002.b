* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1594</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.EMP.INT.2PRCNT.002
*    PROGRAM SBM.EMP.INT.2PRCNT.002
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMP.INT.2.TEMP
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*---------------------------------------------------
    FN.TEMP = "F.SCB.EMP.INT.2.TEMP"
    F.TEMP = ""
*
    FN.STMT.CR = "FBNK.STMT.ACCT.CR"
    F.STMT.CR = ""
*
    FN.ACR.CR = "FBNK.ACCR.ACCT.CR"
    F.ACR.CR = ""
*
    FN.CUAC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUAC = ""
*
    FN.AC = "FBNK.ACCOUNT"
    F.AC = ""
*
    FN.DATE = "F.DATES"
    F.DATE  = ""

*----------------------------------------------------
    CALL OPF (FN.TEMP,F.TEMP)
    CALL OPF (FN.STMT.CR,F.STMT.CR)
    CALL OPF (FN.ACR.CR,F.ACR.CR)
    CALL OPF (FN.CUAC,F.CUAC)
    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.DATE,F.DATE)
*-------------------------------------------------
    R.TEMP = ""
*------------------------------------------------
* ����� ��������
    GOSUB A.10.DATE

    GOSUB A.50.GET.STAFF

    RETURN
*-----------------------------------------
A.10.DATE:
    WS.DATE.ID = "EG0010001-COB"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.WRK.DAT = R.DATE<EB.DAT.LAST.WORKING.DAY>
    WS.TODAY.DAT = R.DATE<EB.DAT.TODAY>
    WS.LAST.PER.END = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LPE.DATE     = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.PE.DATE      = R.DATE<EB.DAT.PERIOD.END>

    WS.LAST.PER.END = WS.PE.DATE

    WS.CHK.DAT      = WS.LAST.PER.END[1,6]
    WS.CHK.DAT.MNTH = WS.CHK.DAT[5,2]
    WS.CHK.DAT.MNTH = WS.CHK.DAT[5,2]
    WS.LOCAT.DATE = WS.CHK.DAT:"01"
    RETURN
*-----------------------------------------
**** ADDED BY MOHAMED SABRY 2012/08/28

A.50.GET.STAFF:
    SEL.TEMP = "SELECT ":FN.TEMP:" WITH RECORD.STATUS NE 'D' BY @ID "
    CALL EB.READLIST(SEL.TEMP,KEY.LIST.TEMP,"",SELECTED.TEMP,ER.MSG.TEMP)
    IF SELECTED.TEMP THEN
        FOR I.TEMP = 1 TO SELECTED.TEMP
            CALL F.READ(FN.CUAC,KEY.LIST.TEMP<I.TEMP>,R.CUAC,F.CUAC,ER.CUAC)
            WS.CUSTOMER.TEMP = KEY.LIST.TEMP<I.TEMP>
            LOOP
                REMOVE WS.AC.ID FROM R.CUAC SETTING POSAC
            WHILE WS.AC.ID:POSAC
                CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,ER.AC)

                WS.AC.CY = R.AC<AC.CURRENCY>
                WS.AC.GL = R.AC<AC.CATEGORY>
                WS.AC.CU = R.AC<AC.CUSTOMER>

                IF WS.AC.CY EQ 'EGP' THEN
                    IF WS.AC.GL GE 6501 AND WS.AC.GL LE 6511 THEN
                        WS.REF.NO   = WS.AC.CU

                        WS.STMT.CR.ID = WS.AC.ID:'-':WS.PE.DATE
                        CALL F.READ(FN.STMT.CR,WS.STMT.CR.ID,R.STMT.CR,F.STMT.CR,MSG.STMT.CR)

                        IF NOT(MSG.STMT.CR) THEN
                            GOSUB A.100.STMT.CR
                        END ELSE
                            WS.ACR.CR.ID = WS.AC.ID
                            CALL F.READ(FN.ACR.CR,WS.ACR.CR.ID,R.ACR.CR,F.ACR.CR,MSG.ACR.CR)
                            GOSUB A.200.SAVOTHR
                        END
                    END
                END

            REPEAT
        NEXT I.TEMP
    END

    RETURN
**** END OF ADDED BY MOHAMED SABRY 2012/08/28
*--------------------------------------------
A.100.STMT.CR:

    LOCPOS     = 0
    WS.BAL.TOT = 0
    WS.BAL     = 0
    A.ARY      = R.STMT.CR<IC.STMCR.CR.INT.DATE>
*Line [ 138 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.LOCAT.DATE IN A.ARY<1,1> SETTING LOCPOS ELSE NULL
    WS.BAL.ARY = R.STMT.CR<IC.STMCR.CR.VAL.BALANCE>
    WS.BAL.TOT = WS.BAL.ARY<1,LOCPOS>

    MSG.TEMP = ""
    CALL F.READ(FN.TEMP,WS.REF.NO,R.TEMP,F.TEMP,MSG.TEMP)
    IF  MSG.TEMP NE ""  THEN
        GOTO A.100.REP
    END

    WS.RELATED.NO = R.TEMP<EMP.RLAT.NO>
    IF R.TEMP<EMP.RECORD.STATUS>  EQ "D"  THEN
        GOTO A.100.REP
    END
    IF  WS.RELATED.NO EQ WS.REF.NO   THEN
        WS.EMP   =  WS.RELATED.NO
        GOSUB A.300.UPDAT
        GOTO A.100.REP
    END
*������� ������� ��� ��� ������ �� ���� ��������
    CALL F.READ(FN.TEMP,WS.RELATED.NO,R.TEMP,F.TEMP,MSG.TEMP)
    IF R.TEMP<EMP.RECORD.STATUS>  EQ "D"  THEN
        GOTO A.100.REP
    END
    WS.RELATED.NO = R.TEMP<EMP.RLAT.NO>
    WS.EMP   =  WS.RELATED.NO
    GOSUB A.300.UPDAT

A.100.REP:
    RETURN

*============================================================================================================
*������ ������� ����� ����
*============================================================================================================

A.200.SAVOTHR:

    WS.REF.NO8 = WS.ACR.CR.ID[8]
    WS.REF.NO8.6 = WS.REF.NO8[1,6]

    IF  WS.REF.NO8.6 EQ 106501   THEN
        GOTO A.200.A
    END
    IF  WS.REF.NO8.6 EQ 106503   THEN
        GOTO A.200.A
    END
    IF  WS.REF.NO8.6 EQ 106504   THEN
        GOTO A.200.A
    END
    IF  WS.REF.NO8.6 EQ 106511   THEN
        GOTO A.200.A
    END

    GOTO A.200.REP
A.200.A:
    WS.BAL.TOT = 0
    WS.BAL     = 0
    A.ARY      = R.ACR.CR<IC.ACRCR.CR.INT.DATE>
*Line [ 197 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.LOCAT.DATE IN A.ARY<1,1> SETTING LOCPOS ELSE NULL
    WS.BAL.ARY = R.ACR.CR<IC.ACRCR.CR.VAL.BALANCE>
    WS.BAL.TOT = WS.BAL.ARY<1,LOCPOS>

    MSG.TEMP = ""
    CALL F.READ(FN.TEMP,WS.REF.NO,R.TEMP,F.TEMP,MSG.TEMP)
    IF MSG.TEMP NE "" THEN
        GOTO A.200.REP
    END
    WS.RELATED.NO = R.TEMP<EMP.RLAT.NO>
    IF R.TEMP<EMP.RECORD.STATUS>  EQ "D"  THEN
        GOTO A.200.REP
    END
    IF  WS.RELATED.NO EQ WS.REF.NO   THEN
        WS.EMP   =  WS.RELATED.NO
        GOSUB A.300.UPDAT
        GOTO A.200.REP
    END

    CALL F.READ(FN.TEMP,WS.RELATED.NO,R.TEMP,F.TEMP,MSG.TEMP)
    IF R.TEMP<EMP.RECORD.STATUS>  EQ "D"  THEN
        GOTO A.200.REP
    END
    WS.RELATED.NO = R.TEMP<EMP.RLAT.NO>
    WS.EMP   =  WS.RELATED.NO
    GOSUB A.300.UPDAT

A.200.REP:
    RETURN
*-------------------------------------------------
A.300.UPDAT:
    WS.BAL = R.TEMP<EMP.TOTAL.AMT>
    WS.BAL = WS.BAL + WS.BAL.TOT
    R.TEMP<EMP.TOTAL.AMT>  = WS.BAL

    CALL F.WRITE(FN.TEMP,WS.EMP,R.TEMP)
   ** CALL  JOURNAL.UPDATE(WS.EMP)
    RETURN
*--------------------------------------------------
END
