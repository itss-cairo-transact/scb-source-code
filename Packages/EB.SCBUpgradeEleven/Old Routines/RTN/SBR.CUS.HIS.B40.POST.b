* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>214</Rating>
*-----------------------------------------------------------------------------
*--- CREATE BY NESSMA ON 2013/08/18 ----
*--- UPDATED BY BAKRY & M.ELSAYED ON 2013/08/20 ----
    SUBROUTINE SBR.CUS.HIS.B40.POST
*    PROGRAM SBR.CUS.HIS.B40.POST
*---------------------------------------
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB LINE.END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*----------------------------- INITIALIZATIONS ------------------------
LINE.END:
*--------
    PRINT " "
    WW<1,1> = SPACE(40):"********************  ����� �������   ******************"
    PRINT WW<1,1>
    RETURN
*--------------------------------------------------------------------
INITIATE:
*--------
    COMP = ID.COMPANY

    YTEXT = "Enter From Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    FROM.DATE = COMI
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    END.DATE   = COMI

    FR.DAT = FROM.DATE[3,2]:FROM.DATE[5,2]:FROM.DATE[7,2]:"0000"
    TO.DAT = END.DATE[3,2]:END.DATE[5,2]:END.DATE[7,2]:"9999"

    XX      = ""
    WW      = ""
    VAR.TOD = TODAY

    REPORT.ID='SBR.CUS.HIS.B40.POST'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CUS = 'FBNK.CUSTOMER'              ; F.CUS   = ""
    CALL OPF(FN.CUS,F.CUS)

    FN.USR = 'F.USER'                 ; F.USR    = ""
    CALL OPF(FN.USR,F.USR)

    FN.CUS.H = 'FBNK.CUSTOMER$HIS'        ; F.CUS.H = ""
    CALL OPF(FN.CUS.H,F.CUS.H)

    T.DATE    = TODAY
*TEXT = FROM.DATE : END.DATE ; CALL REM
    RETURN
*---------------------------------------------------------------
PROCESS:
*-------
*DEBUG
    T.SEL  = "SELECT FBNK.CUSTOMER$HIS WITH COMPANY.BOOK EQ ":COMP
*    T.SEL := " AND DRMNT.DATE GE " : FROM.DATE
*    T.SEL := " AND DRMNT.DATE LE " : END.DATE
*    T.SEL := " AND DATE.TIME GE " : FR.DAT
*    T.SEL := " AND DATE.TIME LE " : TO.DAT
    T.SEL := " AND POSTING.RESTRICT EQ 18"
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
    T.SEL := " AND TEXT UNLIKE BR... BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    FOR I = 1 TO SELECTED
        CUS.FLG = 0
        CALL F.READ(FN.CUS.H,KEY.LIST<I>,R.CUS.H,F.CUS.H,CUS.ERR.H)

        DAT.TIM     = R.CUS.H<EB.CUS.DATE.TIME>

        POST.DATE = R.CUS.H<EB.CUS.LOCAL.REF><1,CULR.DRMNT.DATE>

        COM.CODE    = FIELD(KEY.LIST<I>,';',1)
        COM.CODE.1  = FIELD(KEY.LIST<I+1>,';',1)

*        IF COM.CODE = "40210517" THEN DEBUG

        CUR.CODE    = R.CUS.H<EB.CUS.CURR.NO>

        POST.RESTTT = R.CUS.H<EB.CUS.POSTING.RESTRICT>


        IF COM.CODE NE COM.CODE.1 THEN
            IF CUS.FLG EQ 0 THEN
                CALL F.READ(FN.CUS,COM.CODE,R.CUS,F.CUS,CUS.ERR)
                CUR.NO = R.CUS<EB.CUS.CURR.NO>
                FOR NN = CUR.CODE+1 TO CUR.NO
                    CUS.H.ID = COM.CODE :";":NN
                    CUS.H.ID.1 = COM.CODE:";":NN-1
                    CALL F.READ(FN.CUS.H,CUS.H.ID.1,R.CUS.H1,F.CUS.H,CUS.ERR.H1)
                    CALL F.READ(FN.CUS.H,CUS.H.ID,R.H,F.CUS.H,CUS.ERR.H)
                    IF NOT(CUS.ERR.H) THEN
                        POST.REST = R.H<EB.CUS.POSTING.RESTRICT>
                        POST.REST.H1 = R.CUS.H1<EB.CUS.POSTING.RESTRICT>
                        D.T = R.H<EB.CUS.DATE.TIME,2>
                        IF POST.REST NE 18 AND POST.REST.H1 EQ 18 AND ( D.T GE FR.DAT AND D.T LE TO.DAT ) THEN
**MELSAYED**
                            ACTV.DAT = "20":D.T[1,2]:"/":D.T[3,2]:"/":D.T[5,2]
                            CUS.INP  = R.H<EB.CUS.INPUTTER><1,1>
                            CUS.AUTH = R.H<EB.CUS.AUTHORISER>

                            CUS.INP = FIELD (CUS.INP,'_',2)
                            CUS.AUTH = FIELD (CUS.AUTH,'_',2)

                            CALL DBR('USER':@FM: EB.USE.USER.NAME,CUS.INP,INP.NAME)
                            CALL DBR('USER':@FM: EB.USE.USER.NAME,CUS.AUTH,AUTH.NAME)
                            *CRT ACTV.DAT
*CRT AUTH.NAME
                            GOSUB PRINT.CUSTOMER.DATA
                            NN      = CUR.NO + 1
                            CUS.FLG = 1
                        END
                    END
                NEXT NN
            END
        END
        IF CUS.FLG EQ 0 THEN
            CUS.ID = COM.CODE
            CALL F.READ(FN.CUS,CUS.ID,R.CU,F.CUS,CUS.ERR)
            POST.REST.L = R.CU<EB.CUS.POSTING.RESTRICT>
            D.T = R.CU<EB.CUS.DATE.TIME,2>
            CURR.NO.L = CUR.CODE + 1
            IF POST.REST.L NE '18' AND R.CU<EB.CUS.CURR.NO> EQ CURR.NO.L AND ( D.T GE FR.DAT AND D.T LE TO.DAT ) THEN

                ACTV.DAT = "20":D.T[1,2]:"/":D.T[3,2]:"/":D.T[5,2]
                CUS.INP  = R.CU<EB.CUS.INPUTTER><1,1>
                CUS.AUTH = R.CU<EB.CUS.AUTHORISER>

                CUS.INP = FIELD (CUS.INP,'_',2)
                CUS.AUTH = FIELD (CUS.AUTH,'_',2)

                CALL DBR('USER':@FM: EB.USE.USER.NAME,CUS.INP,INP.NAME)
                CALL DBR('USER':@FM: EB.USE.USER.NAME,CUS.AUTH,AUTH.NAME)
                *CRT ACTV.DAT
*CRT INP.NAME
*CRT AUTH.NAME
                GOSUB PRINT.CUSTOMER.DATA
                CUS.FLG = 1
            END
        END
        CUS.FLG = 0
    NEXT I
    RETURN
*----------------------------------------------------------------------
PRINT.CUSTOMER.DATA:
*-------------------
    XX<1,1> = ""
    CUS.ID  = COM.CODE
    CALL F.READ(FN.CUS,CUS.ID,R.CU,F.CUS,CUS.ERR)
    CUS.NAME  = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
*    POST.DATE = "20": DAT.TIM[1,6]
    POST.DATE = FMT(POST.DATE , "####/##/##")
*  ACTV.DAT = FMT(ACTV.DAT , "####/##/##")

    XX<1,1>[1,10]  = CUS.ID
    XX<1,1>[15,40] = CUS.NAME
    XX<1,1>[55,15] = POST.DATE
    XX<1,1>[73,10] = ACTV.DAT
*  XX<1,1>[73,10] = TODAY
    XX<1,1>[85,15] = INP.NAME
    XX<1,1>[110,20]= AUTH.NAME
    PRINT XX<1,1>
    PRINT " "

    XX<1,1>   = ""
    CUS.ID    = ""
    CUS.NAME  = ""
    POST.DATE = ""
    INP.NAME  = ""
    AUTH.NAME = ""
    ACTV.DAT = ""

    RETURN
************************ PRINT HEAD *******************************
PRINT.HEAD:
*----------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRNCH)
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)

    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = VAR.TOD
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(92): "�� � : ":BRNCH
    PR.HD :="'L'":SPACE(1):"����� �������"
    PR.HD :=T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(109):"SBR.CUS.HIS.B40.POST"
    PR.HD :="'L'":" "

    FROM.DATE.2  = FMT(FROM.DATE, "####/##/##")
    END.DATE.2   = FMT(END.DATE , "####/##/##")

    PR.HD :="'L'":SPACE(40) : "���� �������� ���� �� ��� ������ ����"
    PR.HD :="'L'":SPACE(35) : "�� ������" :" ":FROM.DATE.2 :"��� " :END.DATE.2
    PR.HD :=SPACE(2) ::"����� : ":BRNCH

    PR.HD :="'L'" :SPACE(30):STR('_',60)
    PR.HD :="'L'" :" "
    PR.HD :="'L'" :"��� ������"
    PR.HD := SPACE(10) : "��� ������"
    PR.HD := SPACE(20) : "����� ������"
    PR.HD := SPACE(5) : "����� ��� ������ "
    PR.HD := SPACE(10) : "��� ������ "
    PR.HD := SPACE(7) : "��� ������ "

    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'" :" "
    PRINT
    HEADING PR.HD
    RETURN
END
