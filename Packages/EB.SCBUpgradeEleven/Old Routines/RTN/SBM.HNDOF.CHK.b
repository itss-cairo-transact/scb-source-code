* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    PROGRAM  SBM.HNDOF.CHK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.STMT.HANDOFF
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HNDOF.PRNT.CHK

  EXECUTE "CLEAR-FILE F.SCB.HNDOF.PRNT.CHK"

    FN.ACS      = "FBNK.ACCOUNT.STATEMENT"         ; F.ACS      = ""  ; R.ACS     = ""
    CALL OPF (FN.ACS,F.ACS)

    FN.HND      = "FBNK.AC.STMT.HANDOFF"           ; F.HND      = ""  ; R.HND     = ""
    CALL OPF (FN.HND,F.HND)

    FN.HCHK      = "F.SCB.HNDOF.PRNT.CHK"          ; F.HCHK     = ""  ; R.HCHK     = ""
    CALL OPF (FN.HCHK,F.HCHK)


    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

*    T.SEL = "SELECT FBNK.ACCOUNT.STATEMENT BY @ID"
    T.SEL = "SELECT FBNK.ACCOUNT BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR IACS = 1 TO SELECTED
            WS.AC.ID = KEY.LIST<IACS>
            CALL F.READ(FN.ACS,KEY.LIST<IACS>,R.ACS,F.ACS,ER.ACS)
            WS.FQU1      = R.ACS<AC.STA.FQU1.LAST.DATE>
            WS.HNDOFF.ID = WS.AC.ID:'.':WS.FQU1:'.1'
            CALL F.READ(FN.HND,WS.HNDOFF.ID,R.HND,F.HND,ER.HND)
            IF NOT(ER.HND) THEN
                R.HCHK<HPC.FQU1.LAST.DATE> = WS.FQU1
                R.HCHK<HPC.HNDOF.FILE>     = 'YES'

                CALL  F.WRITE(FN.HCHK,WS.AC.ID,R.HCHK)
                CALL  JOURNAL.UPDATE(WS.AC.ID)
            END
        NEXT IACS
    END
END
