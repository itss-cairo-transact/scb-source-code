* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*========================================================
*-----------------------------------------------------------------------------
* <Rating>1088</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.GET.DEPOSITS.LD.FCY.AA.CAT
**    SUBROUTINE  SBM.GET.DEPOSITS.LD.FCY.AA.CAT
*========================================================
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPOSITS.LOANS.DETAILS

*========================================================
    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LD    = ''
    CALL OPF(FN.LD,F.LD)

    FN.SCB.D   = 'F.SCB.DEPOSITS.LOANS.DETAILS'
    F.SCB.D    = ''
    CALL OPF(FN.SCB.D,F.SCB.D)

    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'
    F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.AC  = 'FBNK.ACCOUNT'
    F.AC  = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    SELECTED = ''; ASD = ''
    T.DATE   = TODAY
    CALL CDT('',T.DATE, '-1W')
    MONTH    = T.DATE[5,2]
    YEAR     = T.DATE[1,4]

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.DATE.LPE)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.DATE.LWD)
    T.DATE = WS.DATE.LWD

******************* CHECK IF PROGRAM RUN TWICE *******************
    TT.SEL  = "SELECT F.SCB.DEPOSITS.LOANS.D WITH @ID LIKE ....":T.DATE
    CALL EB.READLIST(TT.SEL, KEY.LISTT, "", SELECTEDT, ASDT)
* IF SELECTEDT THEN
*    RETURN
* END
******************************************************************

    GOSUB GET.LD
    GOSUB GET.AC
    GOSUB GET.AC.DEP
*   GOSUB DEL.PAST.RECORD
    RETURN
*========================= GET DEPOSITS FROM LD TABLE =====================
GET.LD:

    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH  STATUS NE 'FWD' AND (( VALUE.DATE LE ":WS.DATE.LPE:" AND FIN.MAT.DATE GT ":WS.DATE.LWD:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":WS.DATE.LPE:" AND AMOUNT EQ 0 )) AND CATEGORY NE ''"
    T.SEL := " AND CATEGORY IN(21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21020 21026 21021 21022 21027 21032 21028 21023 21024 21025 21029) BY CUSTOMER.ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    AMT = 0
    TOT.AMT = 0
    AMT.FCY = 0
    TOT.AMT.FCY = 0
    PRE.CUST.NO = ''

    FOR I = 1 TO SELECTED
        R.LD = ''; R.D = ''

        CALL F.READ( FN.LD,KEY.LIST<I>, R.LD, F.LD, ETEXT)
*  CUST.NO = R.LD<LD.CUSTOMER.ID>
        CUST.NO = R.LD<LD.PRIN.LIQ.ACCT>
        IF (CUST.NO NE PRE.CUST.NO AND I NE 1 ) OR (I EQ SELECTED) THEN
            R.D<DEP.DET.DEPOSITS>     = TOT.AMT
            R.D<DEP.DET.DEPOSITS.FCY> = TOT.AMT.FCY
            R.D<DEP.DET.CO.CODE>      = CO.CODE
            R.D<DEP.DET.RESERVED10>   = CAT.LD
            R.D<DEP.DET.RESERVED9>    = ID.REF
            D.L.ID = PRE.CUST.NO:'*':T.DATE
            CALL F.WRITE(FN.SCB.D,D.L.ID,R.D)
            CALL JOURNAL.UPDATE(D.L.ID)
            TOT.AMT     = 0
            TOT.AMT.FCY = 0
        END

        IF R.LD<LD.AMOUNT> NE 0 THEN
            AMT = R.LD<LD.AMOUNT>
        END ELSE
            AMT = R.LD<LD.REIMBURSE.AMOUNT>
        END
        CO.CODE = R.LD<LD.CO.CODE>
        CURR.ID = R.LD<LD.CURRENCY>
        CAT.LD  = R.LD<LD.CATEGORY>
        ID.REF  = KEY.LIST<I>
        IF CURR.ID NE 'EGP' THEN
            CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 127 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR.ID IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
            D.AMT = AMT * CUR.RATE
            AMT.FCY  = DROUND(D.AMT,2)
            AMT = 0
        END
        ELSE
            AMT.FCY = 0
        END

        IF (I EQ 1) OR (CUST.NO NE PRE.CUST.NO) THEN
            TOT.AMT     = AMT
            TOT.AMT.FCY = AMT.FCY
        END
        IF CUST.NO EQ PRE.CUST.NO THEN
            TOT.AMT = TOT.AMT + AMT
            TOT.AMT.FCY = TOT.AMT.FCY + AMT.FCY
        END
        PRE.CUST.NO = CUST.NO
    NEXT I
    RETURN
*===================== GET LOANS FROM AC TABLE ===============
GET.AC:
    Y.SEL  = "SELECT FBNK.ACCOUNT WITH OPEN.ACTUAL.BAL LT 0 AND CATEGORY IN (3201 1102 1404 1414 1206 1208 1401 1402 1405 1406 11240 11238 11232 11234 11239 1480 1481 1499 1483 1493"
    Y.SEL := " 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591 1001 1002 1003 1059 1205"
    Y.SEL := " 1207 1216 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1582 1407 1413 1408 6501 6502 6503 6504 6511 1558 1595 1223 1225 1221 1224 1222 1227 1220) BY CUSTOMER"

    CALL EB.READLIST(Y.SEL, KEY.LIST2, "", SELECTED2, ASD2)

    AC.AMT = 0
    AC.TOT.AMT = 0
    AC.AMT.FCY = 0
    AC.TOT.AMT.FCY = 0

    AC.PRE.CUST.NO = ''
    CO.CODE = ''

    FOR J = 1 TO SELECTED2
        R.L.AC = ''; R.AC = ''; R.DD = ''

        CALL F.READ( FN.AC,KEY.LIST2<J>, R.AC, F.AC, ETEXT)
* AC.CUST.NO = R.AC<AC.CUSTOMER>
        AC.CUST.NO = KEY.LIST2<J>
        ACCOUNT.ID = KEY.LIST2<J>


        IF (AC.CUST.NO NE AC.PRE.CUST.NO AND J NE 1 ) OR (J EQ SELECTED2) AND (AC.PRE.CUST.NO NE '') THEN
            R.L.AC = ''; AC.ID = ''
            AC.ID  = AC.PRE.CUST.NO:'*':T.DATE
            CALL F.READ(FN.SCB.D,AC.ID,R.L.AC,F.SCB.D,ERR3)
            R.L.AC<DEP.DET.LOANS> = AC.TOT.AMT
            R.L.AC<DEP.DET.LOANS.FCY> = AC.TOT.AMT.FCY
            R.L.AC<DEP.DET.CO.CODE> = CO.CODE
            CALL F.WRITE(FN.SCB.D,AC.ID,R.L.AC)
            CALL JOURNAL.UPDATE(AC.ID)
            AC.TOT.AMT = 0
            AC.TOT.AMT.FCY = 0
        END

        AC.AMT = R.AC<AC.OPEN.ACTUAL.BAL>
        CO.CODE = R.AC<AC.CO.CODE>
        CURR.ID = R.AC<AC.CURRENCY>

        IF CURR.ID NE 'EGP' THEN
            CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 193 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR.ID IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            CUR.RATE   = R.CUR<RE.BCP.RATE,POS>
            D.AMT      = AC.AMT * CUR.RATE
            AC.AMT.FCY = DROUND(D.AMT,2)
            AC.AMT = 0
        END
        ELSE
            AC.AMT.FCY = 0
        END

        IF (J EQ 1) OR (AC.CUST.NO NE AC.PRE.CUST.NO) THEN
            AC.TOT.AMT = AC.AMT
            AC.TOT.AMT.FCY = AC.AMT.FCY
        END
        IF AC.CUST.NO EQ AC.PRE.CUST.NO THEN
            AC.TOT.AMT = AC.TOT.AMT + AC.AMT
            AC.TOT.AMT.FCY = AC.TOT.AMT.FCY + AC.AMT.FCY
        END
        IF AC.CUST.NO EQ '' THEN
            R.L.AC = ''; AC.ID = ''
            R.L.AC<DEP.DET.LOANS> = AC.AMT
            R.L.AC<DEP.DET.LOANS.FCY> = AC.AMT.FCY
            R.L.AC<DEP.DET.CO.CODE> = R.AC<AC.CO.CODE>
            AC.ID = ACCOUNT.ID:'*':T.DATE
            CALL F.WRITE(FN.SCB.D,AC.ID,R.L.AC)
            CALL JOURNAL.UPDATE(AC.ID)
            AC.TOT.AMT = 0
            AC.TOT.AMT.FCY = 0
        END
        AC.PRE.CUST.NO = AC.CUST.NO
    NEXT J
    RETURN
*==================== GET DEPOSITS FROM AC TABLE ==============
GET.AC.DEP:

    Z.SEL  = "SELECT FBNK.ACCOUNT WITH OPEN.ACTUAL.BAL GT 0 AND CATEGORY IN (1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 1208 1211 1212 1216 1301 1302 1303 1377 1390 1399 1401"
    Z.SEL := " 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 11238 1407 1413 11240"
    Z.SEL := " 11232 1480 1481 1499 1408 1582 1483 1595 1493 1558 6512 6501 6502 6503 6504 6511 16151 16153 16188 16152 11380 16175 1012 1013 1014 1015 1016 1019 16170 3011 3012 3013 3014 3017 3010 3005) BY CUSTOMER"

    CALL EB.READLIST(Z.SEL, KEY.LIST3, "", SELECTED3, ASD3)

    AC.AMT = 0
    AC.TOT.AMT = 0
    AC.AMT.FCY = 0
    AC.TOT.AMT.FCY = 0

    AC.PRE.CUST.NO = ''
    CO.CODE = ''

    FOR K = 1 TO SELECTED3
        R.D.AC = ''; R.AC = ''; R.DD = ''

        CALL F.READ( FN.AC,KEY.LIST3<K>, R.AC, F.AC, ETEXT)
* AC.CUST.NO = R.AC<AC.CUSTOMER>
        AC.CUST.NO = KEY.LIST3<K>
        ACCOUNT.ID = KEY.LIST3<K>


        IF (AC.CUST.NO NE AC.PRE.CUST.NO AND K NE 1 ) OR (K EQ SELECTED3) AND (AC.PRE.CUST.NO NE '') THEN
            R.L.AC = ''; AC.ID = ''
            AC.ID   = AC.PRE.CUST.NO:'*':T.DATE
            CALL F.READ(FN.SCB.D,AC.ID,R.L.AC,F.SCB.D,ERR3)
            DEP.AMT     = R.L.AC<DEP.DET.DEPOSITS>
            DEP.AMT.FCY = R.L.AC<DEP.DET.DEPOSITS.FCY>
            IF DEP.AMT  = '' THEN
                DEP.AMT = 0
            END
            IF DEP.AMT.FCY  = '' THEN
                DEP.AMT.FCY = 0
            END

            R.L.AC<DEP.DET.DEPOSITS> = AC.TOT.AMT + DEP.AMT
            R.L.AC<DEP.DET.DEPOSITS.FCY> = AC.TOT.AMT.FCY + DEP.AMT.FCY
            R.L.AC<DEP.DET.CO.CODE> = CO.CODE
            CALL F.WRITE(FN.SCB.D,AC.ID,R.L.AC)
            CALL JOURNAL.UPDATE(AC.ID)
            AC.TOT.AMT = 0
            AC.TOT.AMT.FCY = 0
        END

        AC.AMT = R.AC<AC.OPEN.ACTUAL.BAL>
        CO.CODE = R.AC<AC.CO.CODE>
        CURR.ID = R.AC<AC.CURRENCY>

        IF CURR.ID NE 'EGP' THEN
            CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 280 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR.ID IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            CUR.RATE = R.CUR<RE.BCP.RATE,POS>
            D.AMT    = AC.AMT * CUR.RATE
            AC.AMT.FCY = DROUND(D.AMT,2)
            AC.AMT     = 0
        END
        ELSE
            AC.AMT.FCY = 0
        END

        IF (K EQ 1) OR (AC.CUST.NO NE AC.PRE.CUST.NO) THEN
            AC.TOT.AMT = AC.AMT
            AC.TOT.AMT.FCY = AC.AMT.FCY
        END
        IF AC.CUST.NO EQ AC.PRE.CUST.NO THEN
            AC.TOT.AMT = AC.TOT.AMT + AC.AMT
            AC.TOT.AMT.FCY = AC.TOT.AMT.FCY + AC.AMT.FCY
        END
        IF AC.CUST.NO EQ '' THEN
            R.L.AC = ''; AC.ID = ''
            AC.ID = ACCOUNT.ID:'*':T.DATE
            CALL F.READ(FN.SCB.D,AC.ID,R.L.AC,F.SCB.D,ERR3)
            DEP.AMT = R.L.AC<DEP.DET.DEPOSITS>
            DEP.AMT.FCY = R.L.AC<DEP.DET.DEPOSITS.FCY>
            IF DEP.AMT = '' THEN
                DEP.AMT = 0
            END
            IF DEP.AMT.FCY = '' THEN
                DEP.AMT.FCY = 0
            END

            R.L.AC<DEP.DET.DEPOSITS> = AC.AMT + DEP.AMT
            R.L.AC<DEP.DET.DEPOSITS.FCY> = AC.AMT.FCY + DEP.AMT.FCY
            R.L.AC<DEP.DET.CO.CODE> = R.AC<AC.CO.CODE>
            CALL F.WRITE(FN.SCB.D,AC.ID,R.L.AC)
            CALL JOURNAL.UPDATE(AC.ID)
            AC.TOT.AMT = 0
            AC.TOT.AMT.FCY = 0

        END
        AC.PRE.CUST.NO = AC.CUST.NO
    NEXT K
    RETURN
*===================== DELETE PAST RECODRS =============
DEL.PAST.RECORD:

    DATE.B4 = T.DATE
    CALL CDT('EG', DATE.B4, '-2W')

    L.SEL  = "SELECT F.SCB.DEPOSITS.LOANS.D WITH @ID LIKE ....":DATE.B4
    CALL EB.READLIST(L.SEL, KEY.LIST4, "", SELECTED4, ASD4)

    IF SELECTED4 THEN
        FOR G = 1 TO SELECTED4
            CALL F.READ( FN.SCB.D,KEY.LIST4<G>, R.D, F.SCB.D, ETEXT)
*DELETE F.SCB.D , KEY.LIST4<G>
            CALL F.DELETE (FN.SCB.D,KEY.LIST4<G>)
        NEXT G
    END

    RETURN
*===================== PROGRAM END =====================
END
