* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.DELETE.BT.NAU

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT        I_F.SCB.BT.BATCH
    $INSERT        I_F.SCCB.BT.PARTIAL
*----------------------------------------------
    FN.TMP = "F.SCCB.BT.PARTIAL" ; F.TMP = ""
    CALL OPF(FN.TMP , F.TMP)
    TMP.ID = ID.COMPANY:".":TODAY

    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
    FLG = R.TMP<SCCB.BT.FLG.AUTH>
    IF FLG NE 'YES' THEN
        FN.BT = "F.SCB.BT.BATCH$NAU" ; F.BT = ""
        CALL OPF(FN.BT , F.BT)
        T.SEL  = "SELECT F.SCB.BT.BATCH$NAU WITH VERSION.NAME EQ ',SCB.BT.BATCH.H'"
        T.SEL := " AND CO.CODE EQ ":ID.COMPANY
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR II = 1 TO SELECTED
                CALL F.READ(FN.BT,KEY.LIST<II>,R.BT,F.BT,READ.ERRBT)
                DELETE F.BT , KEY.LIST<II>
*CALL F.DELETE(FN.BT,KEY.LIST<II>)
            NEXT II
            TEXT = "�� ��� ������ �������" ; CALL REM
        END
        DELETE F.TMP , TMP.ID
*CALL F.DELETE(FN.TMP,TMP.ID)
    END ELSE
        TEXT = "�� ���� ��� ������ ������ ��� ����" ; CALL REM
    END
*----------------------------------------------
    RETURN
END
