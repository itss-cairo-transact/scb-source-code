* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* SUBROUTINE SBM.MOKHSAS.BATCH
    PROGRAM   SBM.MOKHSAS.BATCH
    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE

***------------------Clearing File------------M.ELSAYED--------------***

    EXECUTE  "CLEAR-FILE F.SCB.CUS.POS.TST"
    PRINT "SAIZO >> CLEARING DONE"

***--------------- CREATE FILE  SCB.CUS.POS.TST -----------------------***

    EXECUTE "CR.MOKHSAS.AC"
    PRINT "DONE AC"

    EXECUTE "CR.MOKHSAS.LD"
    PRINT "DONE LD"

    EXECUTE "CR.MOKHSAS.PD"
    PRINT "DONE PD"

    EXECUTE "CR.MOKHSAS.LC"
    PRINT "DONE LC"

    EXECUTE "CR.MOKHSAS.CR"
    PRINT "DONE CR"

    EXECUTE "CR.MOKHSAS.MM"
    PRINT "DONE MM"

    PRINT "DONE ALL"

*-------------------------------------------------------------------
    RETURN
END
