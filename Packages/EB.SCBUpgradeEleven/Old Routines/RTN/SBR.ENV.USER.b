* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
****** CREATED BY Mohamed Sabry  *******
*****      2013/11/24            *******
***** UPDATED BY MOHAMED SABRY   *******
*****      2014/2/5            *******
****************************************

    SUBROUTINE SBR.ENV.USER
*    PROGRAM SBR.ENV.USER

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    GOSUB INITIATE
    GOSUB OPEN.FILES
    GOSUB W.START
    GOSUB SEL.USR
    GOSUB W.END
    RETURN
****************************************
INITIATE:

    WS.3Y.DATE = TODAY
    CALL ADD.MONTHS(WS.3Y.DATE,-36)

    WS.1Y.DATE = TODAY
    CALL ADD.MONTHS(WS.1Y.DATE,-12)

    WS.FLD.PATH = "/Tcs/tcserver/conf/jConman/"

    OPENSEQ WS.FLD.PATH , "jConmanenv.xml" TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':WS.FLD.PATH:' ':"jConmanenv.xml"
        HUSH OFF
    END
    OPENSEQ WS.FLD.PATH , "jConmanenv.xml" TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create jConmanenv.xml File IN':WS.FLD.PATH
        END
    END
    RETURN
****************************************
OPEN.FILES:

    FN.USR = "F.USER"      ; F.USR = "" ; R.USR = '' ; ER.USER = ''
    CALL OPF(FN.USR,F.USR)

    WS.USR.DPT.OLD = ''
    WS.USR.DPT     = ''
    WS.1ST.ENV     = 0
    RETURN
****************************************
SEL.USR:
*    T.SEL = "SELECT ":FN.USR:" WITH DEPARTMENT.CODE IN (1 5 13 14) WITHOUT DEPARTMENT.CODE IN (88 99) BY DEPARTMENT.CODE"

    FOR ISEL = 1 TO 4
        IF ISEL = 1 THEN
*           T.SEL = "SELECT ":FN.USR:" WITH END.DATE.PROFILE GE ":TODAY:" AND ( DEPARTMENT.CODE IN ( 1 88 ) OR  ( DEPARTMENT.CODE EQ 99 AND SCB.DEPT.CODE EQ  5101 )) BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            T.SEL = "SELECT ":FN.USR:" WITH ( DEPARTMENT.CODE IN ( 1 88 ) OR  ( DEPARTMENT.CODE EQ 99 AND SCB.DEPT.CODE EQ  5101 )) WITHOUT SCB.DEPT.CODE EQ 2800 BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        END
        IF ISEL = 2 THEN
*           T.SEL = "SELECT ":FN.USR:" WITH END.DATE.PROFILE GE ":TODAY:" AND DEPARTMENT.CODE IN ( 13 99 ) WITHOUT SCB.DEPT.CODE IN ( 2800 5101)  BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            T.SEL = "SELECT ":FN.USR:" WITH DEPARTMENT.CODE IN ( 13 99 ) WITHOUT SCB.DEPT.CODE IN ( 2800 5101)  BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        END
        IF ISEL = 3 THEN
*           T.SEL = "SELECT ":FN.USR:" WITH END.DATE.PROFILE GE ":TODAY:" WITHOUT DEPARTMENT.CODE IN ( 1 13 88 99 ) BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            T.SEL = "SELECT ":FN.USR:" WITH DEPARTMENT.CODE NE '' WITHOUT DEPARTMENT.CODE IN ( 1 13 88 99 1200 )  BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        END
        IF ISEL = 4 THEN
            T.SEL = "SELECT ":FN.USR:" WITH SCB.DEPT.CODE EQ 2800 WITHOUT DEPARTMENT.CODE EQ '' BY DEPARTMENT.CODE BY SIGN.ON.NAME "
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        END

*PRINT ISEL
*PRINT T.SEL
*PRINT SELECTED
        IF SELECTED THEN
            GOSUB USR.DATA
        END
    NEXT ISEL
    BB.IN.DATA  =  "</environment>"
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END
    RETURN
****************************************
USR.DATA:
    FOR IUSR = 1 TO SELECTED

        CALL F.READ(FN.USR,KEY.LIST<IUSR>,R.USR,F.USR,ER.USR)
        WS.USR.ID  = KEY.LIST<IUSR>
        WS.SON.ID  = R.USR<EB.USE.SIGN.ON.NAME>
        WS.USR.DPT = R.USR<EB.USE.DEPARTMENT.CODE>
        WS.SCB.DPT = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>

        IF WS.SCB.DPT EQ 2800 THEN
            WS.USR.DPT = 2800
        END

        IF WS.USR.DPT EQ 99 AND WS.SCB.DPT EQ 5101 THEN
            WS.USR.DPT = 1
        END

        IF WS.USR.DPT EQ 88 THEN
            WS.USR.DPT = 1
        END

        IF WS.USR.DPT EQ 99 THEN
            WS.USR.DPT = 13
        END

*******???PRINT WS.SON.ID:' ':WS.USR.DPT
        IF WS.USR.DPT.OLD EQ '' THEN
            GOSUB CHK.ENV
            GOSUB PRINT.ENV
            WS.USR.DPT.OLD = WS.USR.DPT
        END
        IF WS.USR.DPT.OLD # WS.USR.DPT THEN
            GOSUB CHK.ENV
            GOSUB PRINT.ENV
            WS.USR.DPT.OLD = WS.USR.DPT
        END

        GOSUB PRINT.USR
        WS.USR.DPT.OLD = WS.USR.DPT
NEXT.USR:
    NEXT IUSR

    RETURN
****************************************
CHK.ENV:

    IF WS.USR.DPT EQ 1 OR WS.USR.DPT EQ 88 OR WS.USR.DPT EQ 99 THEN
        WS.BR = 'CAIRO'
        RETURN
    END
    IF WS.USR.DPT EQ 2 THEN
        WS.BR = 'HELIOP'
        RETURN
    END
    IF WS.USR.DPT EQ 3 THEN
        WS.BR = 'GIZA'
        RETURN
    END
    IF WS.USR.DPT EQ 4 THEN
        WS.BR = 'MOHAND'
        RETURN
    END
    IF WS.USR.DPT EQ 5 THEN
        WS.BR = 'MAADI'
        RETURN
    END
    IF WS.USR.DPT EQ 6 THEN
        WS.BR = 'ABOUR'
        RETURN
    END
    IF WS.USR.DPT EQ 7 THEN
        WS.BR = 'OROUBA'
        RETURN
    END
    IF WS.USR.DPT EQ 9 THEN
        WS.BR = 'FOSTAT'
        RETURN
    END
    IF WS.USR.DPT EQ 10 THEN
        WS.BR = 'NSRCTY'
        RETURN
    END
    IF WS.USR.DPT EQ 11 THEN
        WS.BR = 'DOKKI'
        RETURN
    END
    IF WS.USR.DPT EQ 12 THEN
        WS.BR = 'SPHINX'
        RETURN
    END
    IF WS.USR.DPT EQ 13 THEN
        WS.BR = 'GRDCTY'
        RETURN
    END
    IF WS.USR.DPT EQ 14 THEN
        WS.BR = 'OCTCTY'
        RETURN
    END
    IF WS.USR.DPT EQ 15 THEN
        WS.BR = 'OCTUNV'
        RETURN
    END
    IF WS.USR.DPT EQ 16 THEN
        WS.BR = 'SHOBRA'
        RETURN
    END
****UPDATED BY NESSREEN AHMED 5/11/2015****

    IF WS.USR.DPT EQ 17 THEN
        WS.BR = 'ELMNYL'
        RETURN
    END

****END OF UPDATE 5/11/2015****************

****UPDATED BY HYTHAM 03/02/2016****
    IF WS.USR.DPT EQ 19 THEN
        WS.BR = 'TAGAMOA'
        RETURN
    END


****UPDATED BY HYTHAM 03/02/2016****
    IF WS.USR.DPT EQ 20 THEN
        WS.BR = 'ALEX'
        RETURN
    END
    IF WS.USR.DPT EQ 21 THEN
        WS.BR = 'BURG'
        RETURN
    END
    IF WS.USR.DPT EQ 22 THEN
        WS.BR = 'SMOHA'
        RETURN
    END
    IF WS.USR.DPT EQ 23 THEN
        WS.BR = 'AMRYA'
        RETURN
    END
    IF WS.USR.DPT EQ 30 THEN
        WS.BR = 'PSAID'
        RETURN
    END
    IF WS.USR.DPT EQ 31 THEN
        WS.BR = 'DOMYAT'
        RETURN
    END
    IF WS.USR.DPT EQ 32 THEN
        WS.BR = 'MANSOURA'
        RETURN
    END
    IF WS.USR.DPT EQ 35 THEN
        WS.BR = 'SHARM'
        RETURN
    END
    IF WS.USR.DPT EQ 40 THEN
        WS.BR = 'ISMAILIA'
        RETURN
    END
    IF WS.USR.DPT EQ 50 THEN
        WS.BR = 'SUEZ'
        RETURN
    END
    IF WS.USR.DPT EQ 51 THEN
        WS.BR = 'SOKHNA'
        RETURN
    END

****UPDATED BY NESSREEN AHMED 5/11/2015******
    IF WS.USR.DPT EQ 52 THEN
        WS.BR = 'PRTTWFIK'
        RETURN
    END
****END OF UPDATE 5/11/2015******************
****UPDATED BY NESSREEN AHMED 28/02/2017******
    IF WS.USR.DPT EQ 53 THEN
        WS.BR = 'SOKHECON'
        RETURN
    END
****END OF UPDATE 28/02/2017******************

****UPDATED BY KHALED 07/03/2017******
    IF WS.USR.DPT EQ 18 THEN
        WS.BR = 'MOHAHRAR'
        RETURN
    END
****END OF UPDATE 07/03/2017******************
******* 2017/12/26 BY KHALED ***********

    IF WS.USR.DPT EQ 24 THEN
        WS.BR = 'MPTAGMO'
        RETURN
    END

    IF WS.USR.DPT EQ 25 THEN
        WS.BR = 'MADINTY'
        RETURN
    END

******* 2018/05/14 BY KHALED ***********

    IF WS.USR.DPT EQ 41 THEN
        WS.BR = 'ISMZAYED'
        RETURN
    END

******* 2018/07/17 BY KHALED ***********

    IF WS.USR.DPT EQ 26 THEN
        WS.BR = 'MAKRAM'
        RETURN
    END

****************************

******* 2018/09/06 BY KHALED ***********

    IF WS.USR.DPT EQ 27 THEN
        WS.BR = 'HOSARY'
        RETURN
    END

****************************
***** 2019/01/10 BY KHALED ******
    IF WS.USR.DPT EQ 29 THEN
        WS.BR = 'MGLSDWLA'
        RETURN
    END

    IF WS.USR.DPT EQ 33 THEN
        WS.BR = 'GLEEM'
        RETURN
    END

    IF WS.USR.DPT EQ 34 THEN
        WS.BR = 'ROUSHDY'
        RETURN
    END

****************************
***** 2019/01/17 BY KHALED ******

    IF WS.USR.DPT EQ 82 THEN
        WS.BR = 'QENA'
        RETURN
    END

    IF WS.USR.DPT EQ 83 THEN
        WS.BR = 'BENISUEF'
        RETURN
    END

****************************
***** 2019/10/03 BY KHALED ******

    IF WS.USR.DPT EQ 36 THEN
        WS.BR = 'CTYSTARS'
        RETURN
    END

****************************
***** 2021/02/10 BY KHALED ******

    IF WS.USR.DPT EQ 45 THEN
        WS.BR = 'SMASH'
        RETURN
    END

****************************
***** 2021/12/02 BY KHALED ******

    IF WS.USR.DPT EQ 37 THEN
        WS.BR = 'ARKAN'
        RETURN
    END

****************************

    IF WS.USR.DPT EQ 60 THEN
        WS.BR = 'TANTA'
        RETURN
    END
    IF WS.USR.DPT EQ 70 THEN
        WS.BR = 'RAMADAN'
        RETURN
    END
    IF WS.USR.DPT EQ 80 THEN
        WS.BR = 'MINYA'
        RETURN
    END
    IF WS.USR.DPT EQ 81 THEN
        WS.BR = 'ASYUT'
        RETURN
    END
    IF WS.USR.DPT EQ 90 THEN
        WS.BR = 'SADAT'
        RETURN
    END
    IF WS.USR.DPT EQ 2800 THEN
        WS.BR = 'ITD'
        RETURN
    END
    WS.BR = ''
    RETURN
****************************************
PRINT.ENV:
*    WS.BR = 'WS.BR.':WS.USR.DPT
    IF WS.1ST.ENV NE 0 THEN
        BB.IN.DATA  =  "</environment>"
        WRITESEQ BB.IN.DATA TO BB.IN ELSE
        END
    END

    WS.ENV.NAME = '<environment name="':WS.BR:'">'
    BB.IN.DATA  = WS.ENV.NAME
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END
    WS.1ST.ENV = 1

    RETURN
****************************************
PRINT.USR:
    WS.USR      = '<user type="user">':WS.SON.ID:'</user>'
    BB.IN.DATA  = WS.USR
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END
    RETURN
****************************************
W.START:

    WS.START.F = '<?xml version="1.0" encoding="ISO-8859-1"?>'
    GOSUB W.FILE
    WS.START.F = '<jConman xmlns="http://www.jbase.com/jconman/xml"'
    GOSUB W.FILE
    WS.START.F = '        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    GOSUB W.FILE
    WS.START.F = '        xsi:schemaLocation="http://www.jbase.com/jconman/xml'
    GOSUB W.FILE
    WS.START.F = 'jConmanschema.xsd">'
    GOSUB W.FILE
*    WS.START.F = '<grouproot>'
*    GOSUB W.FILE
*    WS.START.F = '<group name="pioneers">'
*    GOSUB W.FILE
*    WS.START.F = '<list>'
*    GOSUB W.FILE
*    WS.START.F = 'ClementAder OttoLilienthal GustaveLilienthal'
*    GOSUB W.FILE
*    WS.START.F = 'OrvilleWright WilburWright OctaveChanute'
*    GOSUB W.FILE
*    WS.START.F = '</list>'
*    GOSUB W.FILE
*    WS.START.F = '</group>'
*    GOSUB W.FILE
*    WS.START.F = '<group name="adventure">'
*    GOSUB W.FILE
*    WS.START.F = '<list>'
*    GOSUB W.FILE
*    WS.START.F = 'L�onDelagrange HenryFarman LouisBl�riot G�oChavez'
*    GOSUB W.FILE
*    WS.START.F = 'RolandGaros'
*    GOSUB W.FILE
*    WS.START.F = '</list>'
*    GOSUB W.FILE
*    WS.START.F = '</group>'
*    GOSUB W.FILE
*    WS.START.F = '<group name="aeropostal">'
*    GOSUB W.FILE
*    WS.START.F = '<list>SaintExup�ry BahiaBlanca JeanMermoz</list>'
*    GOSUB W.FILE
*    WS.START.F = '</group>'
*    GOSUB W.FILE
*    WS.START.F = '</grouproot>'
*    GOSUB W.FILE
    WS.START.F = '<environmentroot>'
    GOSUB W.FILE
    RETURN
****************************************
W.END:

    WS.START.F = '</environmentroot>'
    GOSUB W.FILE
    WS.START.F = '<default>test</default>'
    GOSUB W.FILE
    WS.START.F = '</jConman>'
    GOSUB W.FILE

    RETURN
****************************************
W.FILE:

    BB.IN.DATA  = WS.START.F
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END

    RETURN
****************************************
END
