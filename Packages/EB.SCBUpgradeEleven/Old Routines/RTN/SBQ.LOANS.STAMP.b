* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBQ.LOANS.STAMP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.STAMP

*---------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN

INITIALISE:
    OPENSEQ "MECH" , "LOANS.STAMP.RETAIL" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LOANS.STAMP.RETAIL"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LOANS.STAMP.RETAIL" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LOANS.STAMP.RETAIL CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LOANS.STAMP.RETAIL File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""
    ARR1    = '' ; CUS.ID1 = ''
    AMT.1 = 0 ; AMT.2 = 0 ; AMT.3 = 0
    LD.ID = ''

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LS = 'F.SCB.LOAN.STAMP' ; F.LS = ''
    CALL OPF(FN.LS,F.LS)

    V.DATE = TODAY

    LW.DAT = TODAY[1,6]:'01'
    CALL CDT("",LW.DAT,'-1W')
    WS.DATE.1 = LW.DAT[1,6]

    LW.DAT.M1 = LW.DAT[1,6]:'01'
    CALL CDT("",LW.DAT.M1,'-1W')
    WS.DATE.2 = LW.DAT.M1[1,6]

    LW.DAT.M2 = LW.DAT.M1[1,6]:'01'
    CALL CDT("",LW.DAT.M2,'-1W')
    WS.DATE.3 = LW.DAT.M2[1,6]

    T.SEL = "SELECT ":FN.LS:" WITH STAMP.EXP NE 'Y' AND ( CATEGORY GE 21050 AND CATEGORY LE 21074 ) AND CATEGORY NE 21055 AND DATE GE ":WS.DATE.3:" AND DATE LE ":WS.DATE.1:" BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LS,KEY.LIST<I>,R.LS,F.LS,E1)
            CALL F.READ(FN.LS,KEY.LIST<I+1>,R.LS1,F.LS,E2)

            ID1 = FIELD(KEY.LIST<I>,".",1)
            ID2 = FIELD(KEY.LIST<I+1>,".",1)

            CUS.ID   = R.LS<SLS.CUSTOMER.ID>
            AMT      = R.LS<SLS.AMOUNT>
            CR.CUR   = R.LS<SLS.CURRENCY>
            WS.DATE  = R.LS<SLS.DATE>
            WS.CATEG = R.LS<SLS.CATEGORY>
            LD.ID    = R.LS<SLS.LD.NO>

            BRN    = R.LS<SLS.BRANCH>
            BRN.ID = BRN[2]
            IDD   = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//':BRN:','

            IF ( WS.CATEG GE 21050 AND WS.CATEG LE 21053 ) OR ( WS.CATEG EQ 21061 ) THEN
                DB.ACCT = CR.CUR:11400000100:BRN.ID
            END ELSE
                CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.LD)
                IF NOT(ERR.LD) THEN
                    DB.ACCT = R.LD<LD.PRIN.LIQ.ACCT>
                END ELSE
                    CALL F.READ.HISTORY(FN.LD.H,LD.ID,R.LD.H,F.LD.H,E1.H)
                    DB.ACCT = R.LD.H<LD.PRIN.LIQ.ACCT>
                END
            END

            IF AMT.1 EQ 0 THEN
                IF WS.DATE EQ WS.DATE.1 THEN
                    AMT.1 = R.LS<SLS.AMOUNT>
                END ELSE
                    AMT.1 = 0
                END
            END

            IF AMT.2 EQ 0 THEN
                IF WS.DATE EQ WS.DATE.2 THEN
                    AMT.2 = R.LS<SLS.AMOUNT>
                END ELSE
                    AMT.2 = 0
                END
            END

            IF AMT.3 EQ 0 THEN
                IF WS.DATE EQ WS.DATE.3 THEN
                    AMT.3 = R.LS<SLS.AMOUNT>
                END ELSE
                    AMT.3 = 0
                END
            END

            IF ID1 NE ID2 THEN
                ARR1<1> = AMT.1
                ARR1<2> = AMT.2
                ARR1<3> = AMT.3

                MAX.AMT = MAXIMUM(ARR1)

                DB.AMT = (MAX.AMT * 0.0005)
                DB.AMT = DROUND(DB.AMT,'2')

                CR.ACCT   = CR.CUR:1620900030099

***** WRITE IN FILE ****
                R.LS<SLS.STAMP.AMOUNT> = DB.AMT
                R.LS<SLS.MAX.DR.AMT>   = MAX.AMT
                CALL F.WRITE(FN.LS,KEY.LIST<I>,R.LS)
                CALL JOURNAL.UPDATE(KEY.LIST<I>)
************************
**************************************************CRAETE FT BY OFS**********************************************
                OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC29":','
                OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CR.CUR:','
                OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CR.CUR:','
                OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:','
                OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:','
                OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:','
                OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:','
                OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:','
                OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":LD.ID[1,12]:','
                OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":LD.ID[1,12]:','
                OFS.MESSAGE.DATA :=  "NOTE.DEBITED=":MAX.AMT:','
                OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB':','
                OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
                OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO"


                MSG.DATA = IDD:",":OFS.MESSAGE.DATA
                IF DB.AMT NE 0 THEN
                    WRITESEQ MSG.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END

                AMT.1  = 0
                AMT.2  = 0
                AMT.3  = 0
                LD.ID  = ''

            END
        NEXT I

    END
**************************
*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LOANS.STAMP.RETAIL'
    EXECUTE 'DELETE ':"MECH":' ':"LOANS.STAMP.RETAIL"

    RETURN
END
