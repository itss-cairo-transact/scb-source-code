* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-112</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY MOHAMED SABRY 2014/12/21 ****
*********************************************

* SUBROUTINE  SBM.CU.EGSERV.001
    PROGRAM SBM.CU.EGSERV.001

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION

**************************************************************

    GOSUB INITIALISE

    GOSUB CU.SEL

    RETURN
**************************************************************
INITIALISE:
*----------

    FN.CU = 'FBNK.CUSTOMER'  ; F.CU = '' ; R.CU= ''
    CALL OPF(FN.CU,F.CU)

    FN.EGI = 'F.SCB.EGSRV.INDEX'  ; F.EGI = '' ; R.EGI = ''
    CALL OPF(FN.EGI,F.EGI)

    FN.REG = 'F.SCB.CUS.REGION'  ; F.REG = '' ; R.REG = ''
    CALL OPF(FN.REG,F.REG)

    FN.GOV = 'F.SCB.CUS.GOVERNORATE'  ; F.GOV = '' ; R.GOV = ''
    CALL OPF(FN.GOV,F.GOV)

    WS.FLD.NO = 0

    RETURN
**************************************************************
**************************************************************
CU.SEL:
    GOSUB INIT.LD.FILE

    T.SEL  = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND CREDIT.STAT EQ '' AND CREDIT.CODE LE 100 AND "
*    T.SEL := "DRMNT.CODE EQ '' AND SCCD.CUSTOMER NE 'YES' AND COMPANY.BOOK EQ 'EG0010015' AND @ID NE 994] "
  *  T.SEL := "DRMNT.CODE EQ '' AND SCCD.CUSTOMER NE 'YES' AND COMPANY.BOOK NE 'EG0010099' AND @ID NE 994] "
    T.SEL := "SCCD.CUSTOMER NE 'YES' AND COMPANY.BOOK NE 'EG0010099' AND @ID NE 994] "
    T.SEL := "WITHOUT SECTOR IN (5010 5020) AND GOVERNORATE NE 98 AND REGION NE 998 "
    T.SEL := " AND GOVERNORATE NE 999 AND REGION NE 999 BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT "SELECTED = ":SELECTED
    IF SELECTED THEN
        FOR ICU = 1 TO SELECTED
            WS.CU.DATA = ''
            WS.GOV.ID  = ''
            WS.REG.ID  = ''
            WS.FIN.ADD = ''
            WS.FIN.TEL = ''
            WS.REG.NAME = ''
            WS.GOV.NAME = ''

            CALL F.READ(FN.CU,KEY.LIST<ICU>,R.CU,F.CU,ER.CU)

            WS.GOV.ID  = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
            CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,WS.GOV.ID,WS.GOV.NAME)

            WS.REG.ID  = R.CU<EB.CUS.LOCAL.REF><1,CULR.REGION>
            CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,WS.REG.ID,WS.REG.NAME)

            WS.ADD     = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
            WS.TEL     = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
*Line [ 104 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            WS.ADD.CNT = DCOUNT(WS.ADD,@SM)
*Line [ 106 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            WS.TEL.CNT = DCOUNT(WS.TEL,@SM)

            FOR IADD = 1 TO WS.ADD.CNT
                WS.FIN.ADD := WS.ADD<1,1,IADD>:' '
            NEXT IADD

            WS.FIN.ADD = WS.FIN.ADD :" ":WS.GOV.NAME:" ":WS.REG.NAME

            FOR ITEL = 1 TO WS.TEL.CNT
                WS.FIN.TEL := WS.TEL<1,1,ITEL>:' '
            NEXT ITEL

            WS.CU.DATA  = R.CU<EB.CUS.LOCAL.REF><1,CULR.EGSRV.SRL>:','
            WS.CU.DATA := R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:" ":R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>:','
            WS.CU.DATA := WS.FIN.ADD:','
            WS.CU.DATA := WS.FIN.TEL:','

            BB.IN.DATA  = WS.CU.DATA
            WRITESEQ BB.IN.DATA TO BB.IN ELSE
            END
        NEXT ICU
    END
    RETURN

**************************************************************
*    BB.IN.DATA  = SCB.OFS.MESSAGE
*    WRITESEQ BB.IN.DATA TO BB.IN ELSE
*    END

    RETURN
**************************************************************
INIT.LD.FILE:

    WS.FILE.NAME = 'SBM.CU.EGSERV.001.CSV'

    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':WS.FILE.NAME
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.IN'
        END
    END

*    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT THEN
*        CLOSESEQ BB.OUT
*        HUSH ON
*        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':WS.FILE.NAME
*        HUSH OFF
*    END
*    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT ELSE
*        CREATE BB.OUT THEN
*        END ELSE
*            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.OUT'
*        END
*    END
    RETURN
**************************************************************
