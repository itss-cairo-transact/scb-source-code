* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*----- COPIED FROM SBR.CR.CUST.DET.ALL
    PROGRAM SBR.CR.CUST.DET.4650

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT.REFERENCE
    $INCLUDE T24.BP I_F.LETTER.OF.CREDIT          ;*TF.LC.
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.TYPES
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GENERAL.CHARGE
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HIGHEST.DEBIT
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 80 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 82 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 84 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
*Line [ 86 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 88 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*-------------------------------------------------------------------------
    HI.PER       = ""
*    CRD.CODES    = "100.110.120."
    CRD.CODES    = "100."
    COD.CNT      = COUNT(CRD.CODES,'.')
    HEAD.DESC    = ""
    LIM.REF.DESC = ""
    FOR AA = 1 TO COD.CNT
        CRD.COD = FIELD(CRD.CODES,'.',AA)
        PRINT "FILE : SBR.CR.CUST.DET.4650.":CRD.COD
        GOSUB INITIATE
        GOSUB PROCESS
    NEXT AA
    RETURN
*==============================================================
INITIATE:
*--------
    STAFF.COD   = ""
    NATIONAL.ID = ""
    HOME.ADD    = ""
    HOME.PHONE  = ""
    MOBILE.NUM  = ""
    COLL.NUM    = ""
    COLL.ID.NUMS = ""
    GL.NAME     = ""
    OPEN.DATE   = ""
    LAST.PAY.DATE     = ""
    LAST.DB.TRAN.DATE = ""
    LAST.PAY.AMT      = ""
    LAST.DB.TRAN.AMT  = ""
    INT.AMTTT         = ""
    INT.RATEEE        = ""
    INT.DATEEE        = ""
    COLL.AMTTT        = ""
    T.COLL.AMTTT      = ""
    T.COLL.AMT.LOCAL  = ""
    EMAIL.CUS         = ""
    OTHR.OVD.ACC      = ""
    GUAR.NAT.ID       = ""
    COLL.DESC         = ""
    SUB.DATA          = ""


    DAT.ID = 'EG0010001'
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,LWD.DATE)
    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,LPERIOD.DATE)

    FOLDER   = "/home/signat"
    FILENAME = "SBR.CR.CUST.DET.4650.":CRD.COD:".CSV"
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    HEAD.DESC  = "Serial":","
    HEAD.DESC := "Customer Name":","
    HEAD.DESC := "Customer No.":","
    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
        HEAD.DESC := "Reference No.":","
    END ELSE
        HEAD.DESC := "Account No.":","
    END
    HEAD.DESC := "GL":","
    HEAD.DESC := "Branch":","
    HEAD.DESC := "CCY":","
    HEAD.DESC := "Available Limits/direct":","
    HEAD.DESC := "Available Limits/indirect":","
    HEAD.DESC := "Available Limits in Local CCY":","
    HEAD.DESC := "Utilization/direct":","
    HEAD.DESC := "Utilization/indirect":","
    HEAD.DESC := "Utilization in Local CCY":","

    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
        HEAD.DESC := "Limit CCY":","
        HEAD.DESC := "Limit No.":","
        HEAD.DESC := "Limit Type":","
    END
    HEAD.DESC := "Limit Maturity":","
    HEAD.DESC := "Rate":","
    HEAD.DESC := "Rate Type":","
    HEAD.DESC := "Customer Type":","

    IF CRD.COD NE 'LG' AND CRD.COD NE 'LC' AND CRD.COD NE 'LG2' AND CRD.COD NE 'LC2' THEN
        HEAD.DESC := "Loan Type":","
    END
    IF CRD.COD EQ 120 THEN
        HEAD.DESC := "Type":","
    END
    HEAD.DESC := "Days to Maturity":","
    HEAD.DESC := "Buket Label":","
    HEAD.DESC := "UnFunded direct":","
    HEAD.DESC := "UnFunded indirect":","
    HEAD.DESC := "UnFunded in Local CCY":","

    IF CRD.COD EQ '100' OR CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
        HEAD.DESC := "Approved Limit":","
        HEAD.DESC := "Approved UnFunded":","
    END
    HEAD.DESC := "Sector":","
    HEAD.DESC := "Collateral maturity":","
    HEAD.DESC := "Secured-Limit-Amount":","
    HEAD.DESC := "New Sector Code":","
    HEAD.DESC := "Highest Debit":","
*-------------------------------------------
    HEAD.DESC := "Staff Code,"
    HEAD.DESC := "National ID number,"
    HEAD.DESC := "Home Address,"
    HEAD.DESC := "Home Landline,"
    HEAD.DESC := "Mobile number,"
    HEAD.DESC := "GL Name / Description,"
    HEAD.DESC := "Opening Date,"
    HEAD.DESC := "Last Payment Date,"
    HEAD.DESC := "Last Debit Transaction Date,"
    HEAD.DESC := "Last Payment Amount,"
    HEAD.DESC := "Last Debit Transaction Amount,"
    HEAD.DESC := "Count Collateral,"
    IF CRD.COD EQ '100' THEN
    HEAD.DESC := "Collateral Id,"
    HEAD.DESC := "Another OVD account number,"
    HEAD.DESC := "Guarantor ID number,"
    END
    HEAD.DESC := "Interest Amount,"
    HEAD.DESC := "Interest Rate,"
    HEAD.DESC := "Interest Date,"
    HEAD.DESC := "Collateral Amount,"
    HEAD.DESC := "Collateral Local Amount,"
    HEAD.DESC := "E-Mail,"

*-------------------------------------------
    HEAD.DESC := ",,Report date ":LPERIOD.DATE:","

    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LG2' THEN
        HEAD.DESC := "LIMIT.REFERENCE":","
        HEAD.DESC := "MARGIN.AMOUNT":","
        HEAD.DESC := "LG.KIND":","
    END

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.GD = "FBNK.GROUP.DATE" ; F.GD = ""
    CALL OPF(FN.GD, F.GD)

    FN.GCI = "FBNK.GROUP.CREDIT.INT" ; F.GCI = ""
    CALL OPF(FN.GCI, F.GCI)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ;  F.BASE = '' ; R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    FN.SCC = 'F.SCB.STATIC.CREDIT.CUSTOMER' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC1 = 'FBNK.ACCOUNT' ; F.ACC1 = ''
    CALL OPF(FN.ACC1,F.ACC1)

    FN.ACC2 = 'FBNK.ACCOUNT' ; F.ACC2 = ''
    CALL OPF(FN.ACC2,F.ACC2)

    FN.ADT = 'FBNK.ACCOUNT.DATE' ; F.ADT = ''
    CALL OPF(FN.ADT,F.ADT)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = ''
    CALL OPF(FN.ADI,F.ADI)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    FN.INT = 'FBNK.BASIC.INTEREST' ; F.INT = ''
    CALL OPF(FN.INT,F.INT)

    FN.INT.D = 'FBNK.BASIC.INTEREST.DATE' ; F.INT.D = ''
    CALL OPF(FN.INT.D,F.INT.D)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = ''
    CALL OPF(FN.PD,F.PD)

    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    FN.IND.LCA = "FBNK.LC.APPLICANT"         ; F.IND.LCA = ""
    CALL OPF(FN.IND.LCA,F.IND.LCA)

    FN.IND.LCB = "FBNK.LC.BENEFICIARY"   ; F.IND.LCB = ""
    CALL OPF(FN.IND.LCB,F.IND.LCB)

    FN.IND.LI = "FBNK.LIMIT.LIABILITY" ; F.IND.LI = ""
    CALL OPF(FN.IND.LI,F.IND.LI)

    FN.LIM = "FBNK.LIMIT" ; F.LIM = "" ; R.LIM = ''  ; ER.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.LI = "FBNK.LIMIT" ; F.LI = "" ; R.LI = ''  ; ER.LI = ''
    CALL OPF(FN.LI,F.LI)

    FN.COL   = "FBNK.COLLATERAL"         ; F.COL  = '' ;  R.COL = '' ; ER.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)

    FN.GEN = 'FBNK.GENERAL.CHARGE' ;  F.GEN = '' ; R.GEN = ''
    CALL OPF(FN.GEN,F.GEN)

    FN.HI = 'FBNK.HIGHEST.DEBIT' ;  F.HI = '' ; R.HI = ''
    CALL OPF(FN.HI,F.HI)

    FN.CAT = "F.CATEGORY" ; F.CAT = ""
    CALL OPF(FN.CAT, F.CAT)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.INT.AMT.LOCAL       = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    APPROVED.AMT = 0
    KK  = 0
    AA1 = 0
    LI.EXP.DATE = ''
    CUS.TYPE = ''
    LI.ONL.AMT = 0
    LI.INT.AMT = 0
    CUS.ID.1 = ''
    ACC.RATE = ''
    PRICING.TYPE = ''
    PRF.TYPE  = ''
    WS.NEXT.DR = 0
    DAYS.NO = ''
    WS.DIR.INTERNAL.AMT    = 0
    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.INT.AMT.LOCAL       = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.INT.AMT.LOCAL       = 0
    USED.AMT.LOCAL         = 0
    INTERNAL.AMT           = 0
    USED.AMT               = 0
    WS.BUCKET.LABEL        = ''
    UNFUNDED.DIRECT        = 0
    UNFUNDED.INDIRECT      = 0
    UNFUNDED.LOCAL         = 0
    CUS.SEC.NAME           = ''
    COLL.MAT               = ''
    COLL.AMT               = 0

    WS.CY.ID = ""
    LI.ID    = ""

    MTL.GRP  = " 1211 1212 1214 1215 1216 1220 1221 1222 1223 1224 1225"
    MTL.GRP := " 1227 1236 1414 1518 1519 21054 21055 21056 21057 21060"
    MTL.GRP := " 21062 21063 21064 21065 21066 21067 21069 21071 21072"

    LTL.GRP  = " 1217 1218 1230 21068 21070"

    SND.GRP  = " 1478 1479 1486 1487 1488 1595 1596 1597 1598"
    SND.GRP := " 21050 21051 21052 21053"

    PRF.GRP  = " 1302335 1302707 4300171 11300443"

    OVR.GRP  = " 1001 1002 1003 1006 1007 1008 1009 1011 1059 1102 1201 1202"
    OVR.GRP := " 1203 1205 1206 1207 1208 1301 1302 1303 1377 1390 1399 1401"
    OVR.GRP := " 1402 1404 1405 1406 1407 1408 1413 1415 1416 1417 1418 1419"
    OVR.GRP := " 1420 1421 1422 1445 1455 1477 1480 1481 1483 1484 1485 1493"
    OVR.GRP := " 1499 1501 1502 1503 1504 1507 1508 1509 1510 1511 1512 1513"
    OVR.GRP := " 1514 1516 1523 1524 1525 1526 1527 1528 1529 1530 1531 1532"
    OVR.GRP := " 1533 1534 1535 1536 1544 1558 1559 1560 1561 1562 1566 1567"
    OVR.GRP := " 1568 1569 1570 1571 1572 1573 1574 1575 1576 1577 1579 1582"
    OVR.GRP := " 1583 1584 1588 1591 1599 3005 3010 3011 3012 3013 3014 3017"
    OVR.GRP := " 3201 6501 6502 6503 6504 6511 11232 11239 11240 11242"

    ALL.CAT  = MTL.GRP:" ":LTL.GRP:" ":SND.GRP:" ":PRF.GRP:" ":OVR.GRP
    RETURN
*========================================================================
PROCESS:
*-------
    T.SEL  = "SSELECT ":FN.CUS
    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' THEN
        T.SEL := " WITH CREDIT.CODE NE ''"
    END ELSE
        IF CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
            IF CRD.COD EQ 'LC2' THEN
                T.SEL := " WITH ( POSTING.RESTRICT LT 89"
                T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
                T.SEL := " AND TEXT UNLIKE BR... ) "
                T.SEL := " OR ( @ID LIKE 994... )"
            END
        END ELSE
            T.SEL := " WITH CREDIT.CODE EQ ":CRD.COD
        END
    END
    T.SEL := " AND SECTOR IN ( 1100 1200 1300 1400 2000 )"
    T.SEL := " BY CO.CODE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CRT CRD.COD : " *** " : SELECTED
**********//////// ADDING THE CATEGORY CUSTOMERS TO CUST ARRAY \\\\\\\\\**********
    IF CRD.COD EQ '100' THEN
        AA.SEL  = "SSELECT ":FN.ACC:" WITH CATEGORY IN ( ":ALL.CAT:" )"
        AA.SEL :=  " AND OPEN.ACTUAL.BAL LT 0"
        AA.SEL :=  " AND OPEN.ACTUAL.BAL NE ''"
        AA.SEL :=  " AND SECTOR IN ( 1100 1200 1300 1400 )"
        CALL EB.READLIST(AA.SEL,KEY.LIST.AC,"",SELECTED.AC,ER.MSG.AC)

        IF SELECTED.AC THEN
            FOR AAX = 1 TO SELECTED.AC
                CALL F.READ(FN.ACC1,KEY.LIST.AC<AAX>,R.ACC1,F.ACC1,ER.ACC1)
                AC.CUS.ID = R.ACC1<AC.CUSTOMER>
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,AC.CUS.ID,AC.CUS.LCL)
                AC.CUS.CRD.CODE = AC.CUS.LCL<1,CULR.CREDIT.CODE>
                IF ( AC.CUS.CRD.CODE LE 100 ) OR ( AC.CUS.CRD.CODE EQ '' ) THEN
*Line [ 442 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE AC.CUS.ID IN KEY.LIST SETTING POS.AC.CUS THEN '' ELSE
                        AA1++
                        KEY.LIST<SELECTED+AA1> = AC.CUS.ID
                    END
                END
            NEXT AAX
            SELECTED = SELECTED + AA1
        END
    END

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,E1)
            CUS.ID    = KEY.LIST<I>
            CUS.LCL   = R.CUS<EB.CUS.LOCAL.REF>
            CUST.NAME = CUS.LCL<1,CULR.ARABIC.NAME>
            CUS.SEC   = CUS.LCL<1,CULR.NEW.SECTOR>
            CUS.SEC.O = R.CUS<EB.CUS.SECTOR>
            CALL DBR('SCB.NEW.SECTOR':@FM:C.SCB.NEW.SECTOR.NAME,CUS.SEC,CUS.SEC.NAME)
            IF CUS.SEC EQ '4650' THEN CUS.TYPE = 'RETAIL' ELSE CUS.TYPE = 'CORPORATE'
*Line [ 463 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            IF CUS.SEC.O EQ '1100' OR CUS.SEC.O EQ '1200' OR CUS.SEC.O EQ '1300' OR CUS.SEC.O EQ '1400' THEN CUS.TYPE = 'STAFF' ELSE NULL
            COMP.ID = R.CUS<EB.CUS.COMPANY.BOOK>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)

            IF CRD.COD NE 'LG' AND CRD.COD NE 'LC' AND CRD.COD NE 'LG2' AND CRD.COD NE 'LC2' THEN
                GOSUB ACC.REC
*************     GOSUB LD.LOANS.REC
            END
            IF CRD.COD EQ 'LG' THEN
                GOSUB LG.REC
            END
            IF CRD.COD EQ 'LG2' THEN
                GOSUB LG.REC
            END
            IF CRD.COD EQ 'LC' THEN
                GOSUB LC.REC
            END
            IF CRD.COD EQ 'LC2' THEN
                GOSUB LC.REC
            END
        NEXT I
    END

    IF CRD.COD EQ '100' THEN
        GOSUB ACC.INTERNAL
    END
    RETURN
*------------------------------------------------------------
ACC.REC:
*-------
*    DEBUG
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)

    LOOP
        REMOVE ACC.ID FROM R.CUS.ACC SETTING POS.AC
    WHILE ACC.ID:POS.AC
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,E1)
        AC.CAT  = R.ACC<AC.CATEGORY>
        FND.POS = ""
        FINDSTR ' ':AC.CAT IN ALL.CAT SETTING FND.POS THEN

            AC.CUR = R.ACC<AC.CURRENCY>
            AC.GDI = R.ACC<AC.CONDITION.GROUP>

*Line [ 508 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AC.ADI.COUNT = DCOUNT(R.ACC<AC.ACCT.DEBIT.INT>,@VM)
            ADI.DAT      = R.ACC<AC.ACCT.DEBIT.INT><1,AC.ADI.COUNT>
            IF CRD.COD EQ 110 OR CRD.COD EQ 120 THEN
                LOAN.TYPE = ""
            END ELSE
                LOAN.TYPE = "Overdraft"
            END

*Line [ 517 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':AC.CAT IN OVR.GRP SETTING POS.O THEN LOAN.TYPE = "Overdraft" ELSE NULL
*Line [ 519 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':AC.CAT IN MTL.GRP SETTING POS.M THEN LOAN.TYPE = "Medium term loans" ELSE NULL
*Line [ 521 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':AC.CAT IN LTL.GRP SETTING POS.M THEN LOAN.TYPE = "Long term loans" ELSE NULL
*Line [ 523 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':AC.CAT IN SND.GRP SETTING POS.S THEN LOAN.TYPE = "Syndicated Loans" ELSE NULL
            IF CRD.COD EQ 120 THEN
                FINDSTR ' ':CUS.ID IN PRF.GRP SETTING POS.P THEN PRF.TYPE = "Performing" ELSE PRF.TYPE = "Non-Performing"
            END
            IF (AC.CAT GE 1100 AND AC.CAT LE 1599) OR ( AC.CAT EQ 1001 OR AC.CAT EQ 1002 OR AC.CAT EQ 1407 OR AC.CAT EQ 1408 OR AC.CAT EQ 1413 OR AC.CAT EQ 1445 OR AC.CAT EQ 1455 ) THEN
                GOSUB GET.ADI.RATE
                IF ACC.RATE NE '' THEN PRICING.TYPE = 'FIXED' ELSE PRICING.TYPE = 'FLOATED' ; GOSUB GET.FLOATED
                GOSUB GETHIGH
            END
            AC.BAL = R.ACC<AC.ONLINE.ACTUAL.BAL>
            IF AC.BAL LT 0 THEN
                AC.LIM = R.ACC<AC.LIMIT.REF>
                IF AC.LIM THEN
                    CHK.LIM = FMT(FIELD(AC.LIM,".",1),'R%7')
                    LI.ID   = CUS.ID:".":CHK.LIM:".":FIELD(AC.LIM,".",2)
                    GOSUB LIM.REC
                END

                OPER.ID     = ACC.ID
                CAT.ID      = AC.CAT

                MM.CURRENCY = AC.CUR
                WS.DIR.INTERNAL.AMT   = LI.ONL.AMT
                WS.INDIR.INTERNAL.AMT = 0
                INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                IF MM.CURRENCY EQ LCCY THEN
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT
                END ELSE
                    GOSUB GET.LOCAL.RATE
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT * MM.CUR.RATE
                END
                WS.DIR.USED.AMT       = AC.BAL
                WS.INDIR.USED.AMT     = 0
                USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                IF MM.CURRENCY EQ LCCY THEN
                    USED.AMT.LOCAL    = USED.AMT
                END ELSE
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                END
                APPROVED.AMT = LI.INT.AMT
                GOSUB DATA.FILE
            END
        END
    REPEAT
    RETURN
*************************************************************
GET.LOCAL.RATE:
*--------------
    MM.CUR.RATE = ''
    CALL DBR('SBD.CURRENCY':@FM:SBD.CURR.MID.RATE,MM.CURRENCY,MM.CUR.RATE)
*Line [ 575 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF MM.CURRENCY EQ 'JPY' THEN MM.CUR.RATE = MM.CUR.RATE/100 ELSE NULL
    RETURN
*************************************************************
GET.FLOATED:
*-----------
    GDI.ID.LK = AC.GDI:AC.CUR:"..."
    G.SEL = "SSELECT ":FN.GDI:" WITH @ID LIKE ":GDI.ID.LK
    CALL EB.READLIST(G.SEL,G.LIST,'',SELECTED.G,ER.MSG.G)
    GDI.ID = G.LIST<SELECTED.G>
    CALL F.READ(FN.GDI,GDI.ID,R.GDI,F.GDI,ERR.G1)

    GDI.BRATE = R.GDI<IC.GDI.DR.BASIC.RATE,1>
    GDI.INT.R = R.GDI<IC.GDI.DR.INT.RATE,1>
    GDI.MRG.O = R.GDI<IC.GDI.DR.MARGIN.OPER,1>
    GDI.MRG.R = R.GDI<IC.GDI.DR.MARGIN.RATE,1>
    CHRG.COD  = R.GDI<IC.GDI.CHARGE.KEY>

    IF GDI.INT.R EQ '' THEN
        INT.ID.LK = GDI.BRATE:AC.CUR:"..."
*I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
*CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
*INT.ID = I.LIST<SELECTED.I>
*--- 20190219
        INT.D = GDI.BRATE:AC.CUR
        CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)
        INT.DATEEE  = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
        INT.ID      = INT.D:INT.DATEEE
*---------------------------------------
        CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
        INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
        ACC.RATE = INT.RATE
    END ELSE
        ACC.RATE = GDI.INT.R
    END
    IF GDI.MRG.O EQ 'ADD' THEN
        ACC.RATE = ACC.RATE + GDI.MRG.R
    END
    IF GDI.MRG.O EQ 'SUBTRACT' THEN
        ACC.RATE = ACC.RATE - GDI.MRG.R
    END
    IF GDI.MRG.O EQ 'MULTIBLY' THEN
        ACC.RATE = ACC.RATE * GDI.MRG.R
    END
    RETURN
*************************************************************
GET.ADI.RATE:
*-----------
    ADI.ID = ACC.ID:"-":ADI.DAT
    CALL F.READ(FN.ADI,ADI.ID,R.ADI,F.ADI,EADD)
    ADI.INT.R = R.ADI<IC.ADI.DR.INT.RATE,1>
    ADI.BRATE = R.ADI<IC.ADI.DR.BASIC.RATE,1>
    ADI.MRG.O = R.ADI<IC.ADI.DR.MARGIN.OPER,1>
    ADI.MRG.R = R.ADI<IC.ADI.DR.MARGIN.RATE,1>
    CHRG.COD  = R.ADI<IC.ADI.CHARGE.KEY>
    IF ADI.INT.R EQ '' THEN
*------ 20190219
*        INT.ID.LK = ADI.BRATE:AC.CUR:"..."
*        I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
*        CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
*        INT.ID = I.LIST<SELECTED.I>
        INT.D = ADI.BRATE:AC.CUR
        CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)
        INT.DATEEE  = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
        INT.ID      = INT.D:INT.DATEEE
*-------------------------------------------------------------------
        CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
        INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
        ACC.RATE = INT.RATE
    END ELSE
        ACC.RATE = ADI.INT.R
    END
    IF ADI.MRG.O EQ 'ADD' THEN
        ACC.RATE = ACC.RATE + ADI.MRG.R
    END
    IF ADI.MRG.O EQ 'SUBTRACT' THEN
        ACC.RATE = ACC.RATE - ADI.MRG.R
    END
    IF ADI.MRG.O EQ 'MULTIBLY' THEN
        ACC.RATE = ACC.RATE * ADI.MRG.R
    END
    RETURN
*---------------------------------------------------
GETHIGH:
*----------------
    HI.DEBIT = '' ; HI.PER = 0
    T.SEL.HI = "SELECT ":FN.GEN:" WITH @ID LIKE ":CHRG.COD:".... BY @ID"
    CALL EB.READLIST(T.SEL.HI,KEY.LIST.HI,"",SELECTED.HI,ER.MSG.HI)
    IF SELECTED.HI THEN
        CALL F.READ(FN.GEN,KEY.LIST.HI<SELECTED.HI>,R.GEN,F.GEN,E1.HI)
        HI.DEBIT = R.GEN<IC.GCH.HIGHEST.DEBIT>:"...."
    END

    T.SEL1.HI = "SELECT ":FN.HI:" WITH @ID LIKE ":HI.DEBIT:" BY @ID"
    CALL EB.READLIST(T.SEL1.HI,KEY.LIST1.HI,"",SELECTED1.HI,ER.MSG1.HI)
    IF SELECTED1.HI THEN
        CALL F.READ(FN.HI,KEY.LIST1.HI<SELECTED1.HI>,R.HI,F.HI,E2.HI)
        HI.PER = R.HI<IC.HDB.PERCENTAGE>
    END
    IF HI.PER NE '' THEN
        HI.PER  ="0" : HI.PER
    END
    RETURN
*---------------------------------------------------
LD.LOANS.REC:
*------------
    CALL F.READ(FN.IND.LD,CUS.ID,R.IND.LD,F.IND.LD,ER.IND.LD)

    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD

        IF LD.ID[1,2] EQ 'LD' THEN
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
            REC.STA = R.LD<LD.STATUS>
            LD.CAT  = R.LD<LD.CATEGORY>
            LD.MAT  = R.LD<LD.FIN.MAT.DATE>

            IF R.LD<LD.INTEREST.RATE> EQ '' AND R.LD<LD.INTEREST.KEY> NE '' THEN
*------ 2019
*                INT.ID.LK.LD = R.LD<LD.INTEREST.KEY>:R.LD<LD.CURRENCY>:"..."
*                I.LD.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK.LD
*                CALL EB.READLIST(I.LD.SEL,I.LD.LIST,'',SELECTED.I.LD,ER.MSG.I.LD)
*                INT.LD.ID = I.LD.LIST<SELECTED.I.LD>
                INT.D = R.LD<LD.INTEREST.KEY>:R.LD<LD.CURRENCY>
                CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)
                INT.DATEEE = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
                INT.LD.ID  = INT.D:INT.DATEEE
*-----------------------------------------------------------
                CALL F.READ(FN.INT,INT.LD.ID,R.INT,F.INT,ERR.I1)
                INT.LD.RATE = R.INT<EB.BIN.INTEREST.RATE>
                ACC.RATE = INT.LD.RATE
            END ELSE

*--------------------------------------------------------------
                ACC.RATE = R.LD<LD.INTEREST.RATE>
            END
            ACC.RATE = ACC.RATE + R.LD<LD.INTEREST.SPREAD>
*Line [ 713 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':LD.CAT IN MTL.GRP SETTING POS.M THEN LOAN.TYPE = "Medium term loans" ELSE NULL
*Line [ 715 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':LD.CAT IN LTL.GRP SETTING POS.M THEN LOAN.TYPE = "Long term loans" ELSE NULL
*Line [ 717 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR ' ':LD.CAT IN SND.GRP SETTING POS.S THEN LOAN.TYPE = "Syndicated Loans" ELSE NULL
*---------------GET PD.AMOUNT EDIT BY NESSMA 2017/04/05 -----
            PD.ID  = "PD":LD.ID
            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,PD.ID,PD.AMT)
            PD.AMT = PD.AMT * -1
            PD.AMT.TOT = ""
            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.AMT.TO.REPAY,PD.ID,PD.AMT.TOT)
*--------------- END GET-------------------------------------
            IF LD.MAT GT LPERIOD.DATE THEN
                FINDSTR ' ':LD.CAT IN ALL.CAT SETTING POS.ALL.CAT THEN
                    LD.CUR = R.LD<LD.CURRENCY>
                    LD.AMT = R.LD<LD.AMOUNT> * -1
                    LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                    IF LD.LIM THEN
                        CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = LD.ID
                        CAT.ID      = LD.CAT
                        MM.CURRENCY = LD.CUR
                        WS.DIR.INTERNAL.AMT   = LI.ONL.AMT
                        WS.INDIR.INTERNAL.AMT = 0
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END
                        WS.DIR.USED.AMT       = LD.AMT + PD.AMT
                        WS.INDIR.USED.AMT     = 0
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                        END
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
            END ELSE          ;* NESSMAAAAA
                IF PD.AMT.TOT NE "0" THEN
                    IF LD.ID[1,2] EQ 'LD' THEN
                        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
                    END
                    REC.STA = R.LD<LD.STATUS>
                    LD.CAT  = R.LD<LD.CATEGORY>
                    LD.MAT  = R.LD<LD.FIN.MAT.DATE>
*Line [ 770 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR ' ':LD.CAT IN MTL.GRP SETTING POS.M THEN LOAN.TYPE = "Medium term loans" ELSE NULL
*Line [ 772 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR ' ':LD.CAT IN LTL.GRP SETTING POS.M THEN LOAN.TYPE = "Long term loans" ELSE NULL
*Line [ 774 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR ' ':LD.CAT IN SND.GRP SETTING POS.S THEN LOAN.TYPE = "Syndicated Loans" ELSE NULL

                    PD.ID  = "PD":LD.ID
                    CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,PD.ID,PD.AMT)
                    PD.AMT = PD.AMT * -1
                    LD.AMT = 0
                    FINDSTR ' ':LD.CAT IN ALL.CAT SETTING POS.ALL.CAT THEN
                        LD.CUR = R.LD<LD.CURRENCY>
                        LD.AMT = R.LD<LD.AMOUNT> * -1
                        LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                        IF LD.LIM THEN
                            CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                            LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                            GOSUB LIM.REC
                            OPER.ID = LD.ID
                            CAT.ID      = LD.CAT
                            MM.CURRENCY = LD.CUR
                            WS.DIR.INTERNAL.AMT   = LI.ONL.AMT
                            WS.INDIR.INTERNAL.AMT = 0
                            INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                            IF MM.CURRENCY EQ LCCY THEN
                                WS.INT.AMT.LOCAL      = INTERNAL.AMT
                            END ELSE
                                GOSUB GET.LOCAL.RATE
                                WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                            END
                            WS.DIR.USED.AMT       = LD.AMT + PD.AMT
                            WS.INDIR.USED.AMT     = 0
                            USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                            LIMIT.CUR             = WS.CY.ID
                            LIMIT.REF             = LI.ID
                            IF MM.CURRENCY EQ LCCY THEN
                                USED.AMT.LOCAL = USED.AMT
                            END ELSE
                                GOSUB GET.LOCAL.RATE
                                USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                            END
                            APPROVED.AMT = LI.INT.AMT
                            GOSUB DATA.FILE
                        END
                    END
                END
            END
        END
    REPEAT
    RETURN
*---------------------------------------------------
ACC.INTERNAL:
*-----------
    IF CRD.COD EQ '100' THEN
        INT.SEL = "SSELECT ":FN.ACC2:" WITH CATEGORY IN ( 11232 11234 11239 11240 11242 ) AND CUSTOMER EQ '' AND ONLINE.ACTUAL.BAL LT 0 AND ONLINE.ACTUAL.BAL NE '' "
        CALL EB.READLIST(INT.SEL,KEY.LIST.INT,"",SELECTED.INT,ER.MSG.INT)
        IF SELECTED.INT THEN
            LOOP
                REMOVE ACC.INT.ID FROM KEY.LIST.INT SETTING POS.INT
            WHILE ACC.INT.ID:POS.INT
                CALL F.READ(FN.ACC2,ACC.INT.ID,R.ACC2,F.ACC2,ERR.ACC2)
                INT.CAT = R.ACC2<AC.CATEGORY>
                INT.CUR = R.ACC2<AC.CURRENCY>
                INT.BAL = R.ACC2<AC.ONLINE.ACTUAL.BAL>
                INT.COM = R.ACC2<AC.CO.CODE>
                LOAN.TYPE = "Overdraft"

                CUS.ID = ''
                CUS.TYPE = ''
                CUS.SEC.NAME = ''
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,INT.COM,BRANCH.NAME)
                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,INT.CAT,CUST.NAME)
                OPER.ID = ACC.INT.ID
                CAT.ID  = INT.CAT
                MM.CURRENCY = INT.CUR
                WS.DIR.INTERNAL.AMT   = 0
                WS.INDIR.INTERNAL.AMT = 0
                INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                WS.DIR.USED.AMT       = INT.BAL
                WS.INDIR.USED.AMT     = 0
                USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                IF MM.CURRENCY EQ LCCY THEN
                    USED.AMT.LOCAL    = USED.AMT
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT
                END ELSE
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT *  MM.CUR.RATE
                END
                APPROVED.AMT = 0
                GOSUB DATA.FILE
            REPEAT
        END
    END
    RETURN
*---------------------------------------------------
LG.REC:
*------
    CALL F.READ(FN.IND.LD,CUS.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
            REC.STA = R.LD<LD.STATUS>
            LD.CAT  = R.LD<LD.CATEGORY>
            IF REC.STA NE 'LIQ' THEN
                IF LD.CAT EQ 21096 OR LD.CAT EQ 21097 THEN
                    LD.CUR = R.LD<LD.CURRENCY>
                    LD.AMT = R.LD<LD.AMOUNT>
                    LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                    LD.MRG = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
                    LD.KND = R.LD<LD.LOCAL.REF><1,LDLR.LG.KIND>
                    IF LD.LIM THEN
                        CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                        LI.ID   = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = LD.ID
                        CAT.ID      = LD.CAT
                        MM.CURRENCY = LD.CUR
                        WS.DIR.INTERNAL.AMT   = 0
                        WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        WS.INT.AMT.LOCAL      = INTERNAL.AMT

                        WS.CY.ID = MM.CURRENCY
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END
                        WS.DIR.USED.AMT       = 0
                        WS.INDIR.USED.AMT     = LD.AMT
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL    = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                        END
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
            END
        END
    REPEAT
    RETURN
*************************************************************
LC.REC:
*-------
    CALL F.READ(FN.IND.LCA,CUS.ID,R.IND.LCA,F.IND.LCA,ER.IND.LCA)
    IF NOT(ER.IND.LCA) THEN
        LOOP
            REMOVE LCA.ID FROM R.IND.LCA SETTING POS.LCA
        WHILE LCA.ID:POS.LCA
            CALL F.READ(FN.LC,LCA.ID,R.LC,F.LC,ER.LC)
            IF NOT(ER.LC) THEN
                LC.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
                IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                    LC.CAT  = R.LC<TF.LC.CATEGORY.CODE>
                    LC.CUR  = R.LC<TF.LC.LC.CURRENCY>
                    LC.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                    LC.LIM  = R.LC<TF.LC.LIMIT.REFERENCE>
                    IF LC.LIM THEN
                        CHK.LIM = FMT(FIELD(LC.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LC.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = LCA.ID
                        CAT.ID      = LC.CAT
                        MM.CURRENCY = LC.CUR
                        WS.DIR.INTERNAL.AMT   = 0
                        WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        WS.INT.AMT.LOCAL = INTERNAL.AMT
                        WS.CY.ID = MM.CURRENCY
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END

                        WS.DIR.USED.AMT       = 0
                        WS.INDIR.USED.AMT     = LC.AMT
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        USED.AMT.LOCAL        = USED.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL    = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                        END
                        WS.CY.ID              = MM.CURRENCY
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
                LC.DR.SUB.ID = LCA.ID
                GOSUB DR.REC
            END
        REPEAT
    END ELSE
        R.DRAW    = ""
        WS.AMOUNT = 0
        BNK.DATE1 = TODAY
        CALL CDT("",BNK.DATE1,'-1C')
        NN.SEL  = "SELECT ":FN.DR:" WITH MATURITY.REVIEW GT ":BNK.DATE1
        NN.SEL := " AND CUSTOMER.LINK EQ ":CUS.ID
        NN.SEL := " AND DOCUMENT.AMOUNT GT 0"
        CALL EB.READLIST(NN.SEL,NN.LIST,"",NN.SELECTED,NN.ER.MSG)
        IF NN.SELECTED THEN
            FOR NN = 1 TO NN.SELECTED
                WS.DR.ID = NN.LIST<NN>
                CALL F.READ(FN.DR,WS.DR.ID,R.DRAW,F.DR,NN.ETEXT)
                DR.CUR   = R.DRAW<TF.DR.DRAW.CURRENCY>
                DR.LIM   = R.DRAW<TF.DR.LIMIT.REFERENCE>
                DR.AMT   = R.DRAW<TF.DR.DOCUMENT.AMOUNT>
                TEMP.IDD = R.DRAW<TF.DR.LC.CREDIT.TYPE>
                CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                RATE = 1
                LOCATE DR.CUR IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                WS.AMOUT  = DR.AMT * RATE
                LC.CAT    = ""          ;* NO LC RECORD FOR THIS DR
                IF DR.LIM THEN
                    LIM.REF.ID = FIELD(DR.LIM,".",1)
                    CHK.LIM    = FMT(FIELD(DR.LIM,".",1),'R%7')
                    LI.ID      = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                    GOSUB LIM.REC
                    OPER.ID               = WS.DR.ID
                    CAT.ID                = LC.CAT
                    MM.CURRENCY           = DR.CUR
                    WS.DIR.INTERNAL.AMT   = 0
                    WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                    INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                    WS.INT.AMT.LOCAL      = INTERNAL.AMT
                    CALL EGP.BASE.CURR(WS.INT.AMT.LOCAL,MM.CURRENCY)
                    WS.DIR.USED.AMT       = 0
                    WS.INDIR.USED.AMT     = DR.AMT
                    USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                    USED.AMT.LOCAL        = USED.AMT
                    IF MM.CURRENCY EQ LCCY THEN
                        USED.AMT.LOCAL    = USED.AMT
                    END ELSE
                        GOSUB GET.LOCAL.RATE
                        USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                    END
                    WS.CY.ID              = MM.CURRENCY
                    LIMIT.CUR             = WS.CY.ID
                    LIMIT.REF             = LI.ID
                    APPROVED.AMT          = LI.INT.AMT
                    FN.TEMPP = 'FBNK.LC.TYPES' ; F.TEMPP = ''
                    CALL OPF(FN.TEMPP, F.TEMPP)
                    CALL F.READ(FN.TEMPP,TEMP.IDD,R.TEMPP,F.TEMPP,ERROR.TEMP)
                    CAT.ID =  R.TEMPP<LC.TYP.CATEGORY.CODE>
                    GOSUB DATA.FILE
                END
            NEXT NN
        END
    END

    CALL F.READ(FN.IND.LCB,CUS.ID,R.IND.LCB,F.IND.LCB,ER.IND.LCB)
    LOOP
        REMOVE LCB.ID FROM R.IND.LCB SETTING POS.LCB
    WHILE LCB.ID:POS.LCB
        CALL F.READ(FN.LC,LCB.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                LC.CAT  = R.LC<TF.LC.CATEGORY.CODE>
                LC.CUR  = R.LC<TF.LC.LC.CURRENCY>
                LC.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                LC.LIM  = R.LC<TF.LC.LIMIT.REFERENCE>

                IF LC.LIM THEN
                    CHK.LIM = FMT(FIELD(LC.LIM,".",1),'R%7')
                    LI.ID   = CUS.ID:".":CHK.LIM:".":FIELD(LC.LIM,".",2)
                    GOSUB LIM.REC
                    OPER.ID = LCB.ID
                    CAT.ID      = LC.CAT
                    MM.CURRENCY = LC.CUR
                    WS.DIR.INTERNAL.AMT   = 0
                    WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                    INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                    WS.INT.AMT.LOCAL      = INTERNAL.AMT
                    IF MM.CURRENCY EQ LCCY THEN
                        WS.INT.AMT.LOCAL      = INTERNAL.AMT
                    END ELSE
                        GOSUB GET.LOCAL.RATE
                        WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                    END

                    WS.DIR.USED.AMT       = 0
                    WS.INDIR.USED.AMT     = LC.AMT
                    USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                    USED.AMT.LOCAL        = USED.AMT
                    IF MM.CURRENCY EQ LCCY THEN
                        USED.AMT.LOCAL    = USED.AMT
                    END ELSE
                        GOSUB GET.LOCAL.RATE
                        USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                    END
                    WS.CY.ID              = MM.CURRENCY
                    LIMIT.CUR             = WS.CY.ID
                    LIMIT.REF             = LI.ID
                    APPROVED.AMT = LI.INT.AMT
                    GOSUB DATA.FILE
                END
            END
            WS.DR.SUB.ID = LCB.ID
            GOSUB DR.REC
        END
    REPEAT
    RETURN
*------------------------------------------------------------------
DR.REC:
*--------
    WS.SRL.DR  = ''
    WS.NEXT.DR = WS.NEXT.DR + 0
    FOR IDR = 1 TO WS.NEXT.DR
        WS.SRL.ID = ( IDR + 100 )[2,2]
        WS.DR.ID  = WS.DR.SUB.ID : WS.SRL.ID
        CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
        IF NOT(ER.DR) THEN
            IF R.DR<TF.DR.MATURITY.REVIEW> GT LPERIOD.DATE THEN
                IF R.DR<TF.DR.DOCUMENT.AMOUNT> GT 0 THEN
                    DR.CUR   = R.DR<TF.DR.DRAW.CURRENCY>
                    DR.AMT   = R.DR<TF.DR.DOCUMENT.AMOUNT>
                    DR.LIM   = R.DR<TF.DR.LIMIT.REFERENCE>
                    IF DR.LIM THEN
                        LIM.REF.ID = FIELD(DR.LIM,".",1)
                        CHK.LIM = FMT(FIELD(DR.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = WS.DR.ID
                        CAT.ID  = LC.CAT
                        MM.CURRENCY = DR.CUR
                        WS.DIR.INTERNAL.AMT   = 0
                        WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END

                        WS.DIR.USED.AMT       = 0
                        WS.INDIR.USED.AMT     = DR.AMT
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        USED.AMT.LOCAL        = USED.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL    = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                        END
                        WS.CY.ID              = MM.CURRENCY
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
            END
        END
    NEXT IDR
    RETURN
******************* WRITE DATA IN FILE **********************
DATA.FILE:
*---------
*    DEBUG
    INTERNAL.AMT      = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
    USED.AMT          = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
    UNFUNDED.DIRECT   = WS.DIR.INTERNAL.AMT + WS.DIR.USED.AMT
    UNFUNDED.INDIRECT = WS.INDIR.INTERNAL.AMT + WS.INDIR.USED.AMT
    UNFUNDED.LOCAL    = WS.INT.AMT.LOCAL + USED.AMT.LOCAL
    UNFUNDED.APPROVED = APPROVED.AMT + WS.DIR.USED.AMT + WS.INDIR.USED.AMT
***************************************************************************
    TOT.PRINT = ABS(USED.AMT) + ABS(INTERNAL.AMT)
    IF TOT.PRINT NE 0 AND TOT.PRINT NE '' THEN
        KK++

        WS.DIR.INTERNAL.AMT    = DROUND(WS.DIR.INTERNAL.AMT,'0')
        WS.INDIR.INTERNAL.AMT  = DROUND(WS.INDIR.INTERNAL.AMT,'0')
        WS.INT.AMT.LOCAL       = DROUND(WS.INT.AMT.LOCAL,'0')
        WS.DIR.USED.AMT        = DROUND(WS.DIR.USED.AMT,'0')
        WS.INDIR.USED.AMT      = DROUND(WS.INDIR.USED.AMT,'0')
        USED.AMT.LOCAL         = DROUND(USED.AMT.LOCAL,'0')
        UNFUNDED.DIRECT        = DROUND(UNFUNDED.DIRECT,'0')
        UNFUNDED.INDIRECT      = DROUND(UNFUNDED.INDIRECT,'0')
        UNFUNDED.LOCAL         = DROUND(UNFUNDED.LOCAL,'0')
        TOTAL.USED             = DROUND(TOTAL.USED,'0')
        APPROVED.AMT           = DROUND(APPROVED.AMT,'0')
        UNFUNDED.APPROVED      = DROUND(UNFUNDED.APPROVED,'0')
        COLL.AMT               = DROUND(COLL.AMT,'0')

        BB.DATA  = KK:","
        BB.DATA := CUST.NAME:","
        BB.DATA := CUS.ID:","
        BB.DATA := "' ":OPER.ID:","
        BB.DATA := CAT.ID:","
        CALL F.READ(FN.CUS,CUS.ID,R.CUSTOMER,F.CUS,ER.CUSTOMER)
        COMP.ID.2     = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
        BRANCH.NAME.2 = ""
        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID.2,BRANCH.NAME.2)
        BB.DATA := BRANCH.NAME.2:","
        BB.DATA := MM.CURRENCY:","
        BB.DATA := WS.DIR.INTERNAL.AMT:","
        BB.DATA := WS.INDIR.INTERNAL.AMT:","
        BB.DATA := WS.INT.AMT.LOCAL:","
        BB.DATA := WS.DIR.USED.AMT:","
        BB.DATA := WS.INDIR.USED.AMT:","
        BB.DATA := USED.AMT.LOCAL:","

        IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
            BB.DATA := WS.CY.ID:","
            BB.DATA := LI.ID:","
            BB.DATA := LIM.REF.DESC:","
        END

        BB.DATA := LI.EXP.DATE:","
        BB.DATA := ACC.RATE:","
        BB.DATA := PRICING.TYPE:","
        BB.DATA := CUS.TYPE:","

        IF CRD.COD NE 'LG' AND CRD.COD NE 'LC' AND CRD.COD NE 'LG2' AND CRD.COD NE 'LC2' THEN
            BB.DATA := LOAN.TYPE:","
        END
        IF CRD.COD EQ 120 THEN
            BB.DATA := PRF.TYPE:","
        END
        BB.DATA := DAYS.NO:","
        BB.DATA := WS.BUCKET.LABEL:","
        BB.DATA := UNFUNDED.DIRECT:","
        BB.DATA := UNFUNDED.INDIRECT:","
        BB.DATA := UNFUNDED.LOCAL:","

        IF CRD.COD EQ '100' OR CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
            BB.DATA := APPROVED.AMT:","
            BB.DATA := UNFUNDED.APPROVED:","
        END
        IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LG2' THEN
            BB.DATA := LD.LIM:","
            BB.DATA := LD.MRG:","
            BB.DATA := LD.KND:","
        END


        BB.DATA := CUS.SEC.NAME:","
        BB.DATA := COLL.MAT:","
        BB.DATA := COLL.AMT:","
        BB.DATA := CUS.SEC:","
        BB.DATA := HI.PER:","
*--------------------------------------------------------------------------
        STAFF.COD   = R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.EMPLOEE.NO>
        NATIONAL.ID = R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>

        HOME1.ADD = R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
        HOME.ADD  = HOME1.ADD<1,1,1>:" ":HOME1.ADD<1,1,2>:" ": HOME1.ADD<1,1,3>

        HOME1.PHONE  = R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

        HOME.PHONE  = HOME1.PHONE<1,1,1>:" / ":HOME1.PHONE<1,1,2>

        MOBILE.NUM  = R.CUSTOMER<EB.CUS.SMS.1>

        CALL F.READ(FN.CAT,CAT.ID,R.CAT,F.CAT,E.CAT)
        GL.NAME           = R.CAT<EB.CAT.DESCRIPTION>
        OPEN.DATE         = R.ACC<AC.OPENING.DATE>
        LAST.PAY.DATE     = R.ACC<AC.DATE.LAST.CR.CUST>
        LAST.DB.TRAN.DATE = R.ACC<AC.DATE.LAST.DR.CUST>
        LAST.PAY.AMT      = R.ACC<AC.AMNT.LAST.CR.CUST>
        LAST.DB.TRAN.AMT  = R.ACC<AC.AMNT.LAST.DR.CUST>
        EMAIL.CUS         = R.CUSTOMER<EB.CUS.EMAIL.1>

        BB.DATA := STAFF.COD:","
        BB.DATA := NATIONAL.ID:","
        BB.DATA := HOME.ADD:","
        BB.DATA := "'":HOME.PHONE:","
        BB.DATA := "'":MOBILE.NUM:","
        BB.DATA := GL.NAME:","
        BB.DATA := OPEN.DATE:","
        BB.DATA := LAST.PAY.DATE:","
        BB.DATA := LAST.DB.TRAN.DATE:","
        BB.DATA := LAST.PAY.AMT:","
        BB.DATA := LAST.DB.TRAN.AMT:","
        BB.DATA := COLL.NUM:","
        IF CRD.COD EQ '100' THEN
        BB.DATA := COLL.ID.NUMS:","
        BB.DATA := OTHR.OVD.ACC:","
        BB.DATA := GUAR.NAT.ID :","
        END
        BB.DATA := INT.AMTTT:","
        BB.DATA := INT.RATEEE:","
        BB.DATA := INT.DATEEE:","
        BB.DATA := T.COLL.AMTTT:","
        BB.DATA := T.COLL.AMT.LOCAL:","
        BB.DATA := EMAIL.CUS:","
*-------------------------------------------------
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    END
    T.COLL.AMTTT = 0
    T.COLL.AMT.LOCAL = 0
    STAFF.COD    = ""
    NATIONAL.ID  = ""
    HOME.ADD = ""
    HOME.PHONE = ""
    MOBILE.NUM = ""
    GL.NAME = ""
    OPEN.DATE = ""
    LAST.PAY.DATE = ""
    LAST.DB.TRAN.DATE = ""
    LAST.PAY.AMT = ""
    LAST.DB.TRAN.AMT = ""
    COLL.NUM = ""
    INT.AMTTT = ""
    INT.RATEEE = ""
    INT.DATEEE = ""
    EMAIL.CUS = ""
    COLL.ID.NUMS = ""
    OTHR.OVD.ACC = ""
    GUAR.NAT.ID = ""

    LI.EXP.DATE = ''
    LI.ONL.AMT  = 0
    LI.INT.AMT  = 0
    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    INTERNAL.AMT           = 0
    WS.INT.AMT.LOCAL       = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    USED.AMT               = 0
    USED.AMT.LOCAL         = 0
    UNFUNDED.INDIRECT      = 0
    UNFUNDED.DIRECT        = 0
    APPROVED.AMT           = 0
    UNFUNDED.APPROVED      = 0
    UNFUNDED.LOCAL         = 0
    COLL.MAT               = ''
    COLL.AMT               = 0
    ACC.RATE               = ''
    PRICING.TYPE           = ''
    WS.NEXT.DR             = 0
    WS.BUCKET.LABEL        = ''
    DAYS.NO                = ''
    HI.DEBIT               = ''
    HI.PER                 = 0

    RETURN
************************************************
LIM.REC:
*--------
*    DEBUG
    ARR = ""
    OVD.ACC = ""
    CALL F.READ(FN.LIM,LI.ID,R.LIM,F.LIM,ER.LIM)
    IF R.LIM THEN
        WS.LI.PROD  = R.LIM<LI.LIMIT.PRODUCT>
        WS.PRO.ALL  = R.LIM<LI.PRODUCT.ALLOWED>
        WS.CY.ID    = R.LIM<LI.LIMIT.CURRENCY>
        LI.ONL.AMT  = R.LIM<LI.ONLINE.LIMIT,1>
        LI.INT.AMT  = R.LIM<LI.INTERNAL.AMOUNT>
        LI.MAX.TOT  = R.LIM<LI.MAXIMUM.TOTAL>
        LI.EXP.DATE = R.LIM<LI.EXPIRY.DATE>
        LI.COLL.RI  = R.LIM<LI.COLLAT.RIGHT>
        LI.SECURED  = R.LIM<LI.SECURED.AMT>
        MM.CURRENCY = R.LIM<LI.LIMIT.CURRENCY>
*Line [ 1355 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        CLL         = DCOUNT(LI.SECURED,@SM)
        MAX.COL.EXP = ''
        FOR BBB = 1 TO CLL
            COLL.AMT     += LI.SECURED<1,1,BBB>
            RIGHT.IND.ID  = R.LIM<LI.COLLAT.RIGHT,1,BBB>



            CALL F.READ(FN.RIGHT.COL,RIGHT.IND.ID,R.RIGHT.COL,F.RIGHT.COL,ER.RIGHT.COL)
*            COLL.NUM = DCOUNT(R.RIGHT.COL, FM)
*Line [ 1366 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
             COLL.NUM = COLL.NUM + DCOUNT(R.RIGHT.COL, @FM)

            LOOP
                REMOVE COL.ID FROM R.RIGHT.COL SETTING POS.COLL
            WHILE COL.ID:POS.COLL
                CALL F.READ(FN.COL,COL.ID,R.COL,F.COL,E.COL.1)
                COLL.ID.NUMS  := COL.ID:"|"

                T.COLL.AMTTT += R.COL<COLL.LOCAL.REF><1,COLR.COLL.AMOUNT>

                IF CRD.COD EQ '100' THEN
                IF R.COL<COLL.APPLICATION.ID>[1,2] EQ 'LD' THEN
                    CALL F.READ(FN.LD,R.COL<COLL.APPLICATION.ID>,R.LD.2,F.LD,ER.LD.2)
                    OVD.ACC  =  R.LD.2<LD.DRAWDOWN.ACCOUNT>
                END ELSE
                    OVD.ACC  = R.COL<COLL.APPLICATION.ID>
                END

                IF ACC.ID NE OTHR.OVD.ACC THEN

                    CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,OVD.ACC,OTHR.OVD.CUS)
                    CUS.LOCAL.REF = ''
                    CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF:@FM:"F",OTHR.OVD.CUS,CUS.LOCAL.REF)
                    GUAR.NAT.ID       = CUS.LOCAL.REF<1,CULR.NSN.NO>
                    COLL.DESC         = R.COL<COLL.DESCRIPTION>
                    OTHR.OVD.ACC  := OVD.ACC:"|"
                END

                OVD.ACC           = ""
                OTHR.OVD.CUS      = ""
                END


                IF COLL.NUM EQ 1 THEN
                    IF R.COL<COLL.APPLICATION.ID>[1,2] EQ 'LD' THEN
                        CALL F.READ(FN.LD,R.COL<COLL.APPLICATION.ID>,R.LDDD,F.LD,ER.LD)
                        INT.AMTTT  = R.LDDD<LD.TOT.INTEREST.AMT>
                        INT.RATEEE = R.LDDD<LD.INTEREST.RATE>

                        IF NOT(INT.RATEEE) THEN
                            INT.D = R.LDDD<LD.INTEREST.KEY>:R.LDDD<LD.CURRENCY>

                            CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)

                            INT.DATEEE  = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
                            INT.LDDD.ID = INT.D:INT.DATEEE
                            CALL F.READ(FN.INT,INT.LDDD.ID,R.INTTT,F.INT,ERR.I111)
                            INT.RATEEE  = R.INTTT<EB.BIN.INTEREST.RATE>
                        END
                    END ELSE
                        ACCCCC = R.COL<COLL.APPLICATION.ID>
                        CALL F.READ(FN.ACC,ACCCCC,R.ACCCCC,F.ACC,E.ACCCCC)
                        ACCCCC.CAT  = R.ACCCCC<AC.CATEGORY>
                        ACCCCC.CUR  = R.ACCCCC<AC.CURRENCY>
                        ACCCCC.GRP  = R.ACCCCC<AC.CONDITION.GROUP>
                        KEY.IDDDD   = ACCCCC.GRP:ACCCCC.CUR

                        IF ACCCCC.CAT[1,2] EQ '65' THEN
                            CALL F.READ(FN.GD,KEY.IDDDD,R.GD,F.GD,E.GD)
                            INT.DATEEE = R.GD<AC.GRD.CREDIT.GROUP.DATE>

                            CALL F.READ(FN.GCI,KEY.IDDDD:INT.DATEEE,R.GCI,F.GCI,E.GCI)
                            INT.RATEEE = R.GCI<IC.GCI.CR.INT.RATE>
                        END
                    END
                END

                COL.EXP = R.COL<COLL.EXPIRY.DATE>
                IF COL.EXP GE MAX.COL.EXP THEN
                    MAX.COL.EXP = COL.EXP
                END
            REPEAT
            IF MM.CURRENCY EQ LCCY THEN
                T.COLL.AMT.LOCAL  = T.COLL.AMTTT
            END ELSE
                GOSUB GET.LOCAL.RATE
                T.COLL.AMT.LOCAL  = T.COLL.AMTTT * MM.CUR.RATE
            END
        NEXT BBB
        COLL.MAT = MAX.COL.EXP

        CALL DBR('LIMIT.REFERENCE':@FM:LI.REF.DESCRIPTION,WS.LI.PROD,LIM.REF.DESC)
        IF LI.EXP.DATE NE '' THEN
            IF COLL.MAT NE '' THEN
                IF LI.EXP.DATE LE COLL.MAT THEN
                    MAT.DATE = LI.EXP.DATE
                END ELSE
                    MAT.DATE = COLL.MAT
                END
                IF MAT.DATE GT LPERIOD.DATE THEN
                    DATE.1 = LPERIOD.DATE
                    DATE.2 = MAT.DATE
                    SS1 = 1
                END ELSE
                    DATE.1 = MAT.DATE
                    DATE.2 = LPERIOD.DATE
                    SS1 = -1
                END
            END ELSE
                IF LI.EXP.DATE GT LPERIOD.DATE THEN
                    DATE.1 = LPERIOD.DATE
                    DATE.2 = LI.EXP.DATE
                    SS1 = 1
                END ELSE
                    DATE.1 = LI.EXP.DATE
                    DATE.2 = LPERIOD.DATE
                    SS1 = -1
                END
            END
            DAYS.NO = 'C'
            CALL CDD("",DATE.1,DATE.2,DAYS.NO)
            DAYS.NO = DAYS.NO * SS1
            MONTHS.NO = 1
            CALL EB.NO.OF.MONTHS(DATE.1,DATE.2,MONTHS.NO)
            IF DAYS.NO LT 0 THEN
                WS.BUCKET.LABEL = 'Maturity Expired'
            END ELSE
                IF DAYS.NO EQ 1 THEN
                    WS.BUCKET.LABEL = 'O/N'
                END ELSE
                    IF DAYS.NO GT 1 AND DAYS.NO LE 7 THEN
                        WS.BUCKET.LABEL = '1.WEEK'
                    END ELSE
                        IF MONTHS.NO EQ 1 THEN
                            WS.BUCKET.LABEL = '1.MONTH'
                        END
                    END
                END
            END

            IF MONTHS.NO GT 1 AND MONTHS.NO LE 3 THEN WS.BUCKET.LABEL = '3.MONTHS'
            IF MONTHS.NO GT 1 AND MONTHS.NO LE 3 THEN WS.BUCKET.LABEL = '3.MONTHS'
            IF MONTHS.NO GT 3 AND MONTHS.NO LE 6 THEN WS.BUCKET.LABEL = '6.MONTHS'
            IF MONTHS.NO GT 6 AND MONTHS.NO LE 9 THEN WS.BUCKET.LABEL = '9.MONTHS'
            IF MONTHS.NO GT 9 AND MONTHS.NO LE 12 THEN WS.BUCKET.LABEL = '1.YEAR'
            IF MONTHS.NO GT 12 AND MONTHS.NO LE 18 THEN WS.BUCKET.LABEL = '1.5.YEAR'
            IF MONTHS.NO GT 18 AND MONTHS.NO LE 24 THEN WS.BUCKET.LABEL = '2.YEAR'
            IF MONTHS.NO GT 24 AND MONTHS.NO LE 36 THEN WS.BUCKET.LABEL = '3.YEAR'
            IF MONTHS.NO GT 36 AND MONTHS.NO LE 48 THEN WS.BUCKET.LABEL = '4.YEAR'
            IF MONTHS.NO GT 48 AND MONTHS.NO LE 60 THEN WS.BUCKET.LABEL = '5.YEAR'
            IF MONTHS.NO GT 60 AND MONTHS.NO LE 72 THEN WS.BUCKET.LABEL = '6.YEAR'
            IF MONTHS.NO GT 72 AND MONTHS.NO LE 84 THEN WS.BUCKET.LABEL = '7.YEAR'
            IF MONTHS.NO GT 84 AND MONTHS.NO LE 96 THEN WS.BUCKET.LABEL = '8.YEAR'
            IF MONTHS.NO GT 96 AND MONTHS.NO LE 108 THEN WS.BUCKET.LABEL = '9.YEAR'
            IF MONTHS.NO GT 108 AND MONTHS.NO LE 120 THEN WS.BUCKET.LABEL = '10.YEAR'
            IF MONTHS.NO GT 120 AND MONTHS.NO LE 132 THEN WS.BUCKET.LABEL = '11.YEAR'
            IF MONTHS.NO GT 132 AND MONTHS.NO LE 144 THEN WS.BUCKET.LABEL = '12.YEAR'
            IF MONTHS.NO GT 144 AND MONTHS.NO LE 156 THEN WS.BUCKET.LABEL = '13.YEAR'
            IF MONTHS.NO GT 156 AND MONTHS.NO LE 168 THEN WS.BUCKET.LABEL = '14.YEAR'
            IF MONTHS.NO GT 168 AND MONTHS.NO LE 180 THEN WS.BUCKET.LABEL = '15.YEAR'
            IF MONTHS.NO GT 180 AND MONTHS.NO LE 192 THEN WS.BUCKET.LABEL = '16.YEAR'
            IF MONTHS.NO GT 192 AND MONTHS.NO LE 204 THEN WS.BUCKET.LABEL = '17.YEAR'
            IF MONTHS.NO GT 204 AND MONTHS.NO LE 216 THEN WS.BUCKET.LABEL = '18.YEAR'
            IF MONTHS.NO GT 216 AND MONTHS.NO LE 228 THEN WS.BUCKET.LABEL = '19.YEAR'
            IF MONTHS.NO GT 228 AND MONTHS.NO LE 240 THEN WS.BUCKET.LABEL = '20.YEAR'
            IF MONTHS.NO GT 240 THEN WS.BUCKET.LABEL = 'MORE.THAN.20.YEAR'
        END
    END
    RETURN
*------------------------------------------------------------
END
