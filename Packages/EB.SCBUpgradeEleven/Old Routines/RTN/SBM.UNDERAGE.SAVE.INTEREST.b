* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>2429</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.UNDERAGE.SAVE.INTEREST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.UNDERAGE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.ACCT.SAVE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS


*==================== OPEN TABLES ================================
    FN.CU  = 'FBNK.CUSTOMER'        ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC  = 'FBNK.ACCOUNT'         ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.ST  = 'FBNK.STMT.ACCT.CR'    ; F.ST = '' ; R.ST = ''
    CALL OPF(FN.ST,F.ST)
    FN.SA  = 'F.SCB.STMT.ACCT.SAVE' ; F.SA = '' ; R.SA = ''
    CALL OPF(FN.SA,F.SA)
    FN.U   = 'F.SCB.ACCT.UNDERAGE'  ; F.U = '' ; R.U = ''
    CALL OPF(FN.U,F.U)
    FN.ACR   = 'FBNK.ACCR.ACCT.CR'  ; F.ACR = '' ; R.ACR = ''
    CALL OPF(FN.ACR,F.ACR)

*========================= DEFINE VARIABLES =========================
    COMP       = ID.COMPANY
    COM.CODE   = COMP[8,2]
    TODAY.DATE = TODAY
    DATE.B     = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    IF MONTH[1,1] EQ 0 THEN
        MONTH = MONTH[2,1]
    END

*========== CALC DATE MINUS ONE WORKING DAY ==========
    IF MONTH EQ 1 THEN
        NEW.MONTH = 12
        NEW.YEAR  = YEAR - 1
        NEW.DAY   = 31
    END
    IF MONTH EQ 5 OR MONTH EQ 7 OR MONTH EQ 10 OR MONTH EQ 12 THEN
        NEW.MONTH = MONTH - 1
        NEW.YEAR  = YEAR
        NEW.DAY   = 30
    END
    IF MONTH EQ 2 OR MONTH EQ 4 OR MONTH EQ 6 OR MONTH EQ 8 OR MONTH EQ 9 OR MONTH EQ 11 THEN
        NEW.MONTH = MONTH - 1
        NEW.YEAR  = YEAR
        NEW.DAY   = 31
    END
    IF MONTH EQ 3 THEN
        NEW.MONTH = MONTH - 1
        NEW.YEAR  = YEAR
        YEAR.FRAC = NEW.YEAR / 4
        FRAC      = FIELD(YEAR.FRAC,'.',2)
        IF FRAC EQ '' THEN
            NEW.DAY   = 29
        END
        ELSE
            NEW.DAY   = 28
        END
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END

    DATE.B4    = NEW.YEAR:NEW.MONTH:NEW.DAY

    DATE.B4.MONTH = DATE.B4[5,2]
    STMT.ERR   = ''
    STMT.ERR1  = ''
    B4.YEAR.MONTH = DATE.B4[1,6]
    STMT.ERR = 1
    STMT.ERR1 = 1
    BALANCE = ' '
*========================= OFS VARIABLES ============================
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

*========================= GET ALL SAVING ACCOUNTS ==================
**  DEBUG
    T.SEL  = "SELECT F.SCB.ACCT.UNDERAGE WITH ACCT.FLAG EQ '' AND CO.CODE NE EG0010011"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*=============== GET ALL CUSTOMERS THAT ARE MORE THAN 21 YEARS OLD ====
    Z.SEL  = "SELECT F.SCB.ACCT.UNDERAGE WITH ACCT.FLAG EQ '21' AND CO.CODE NE EG0010011"
    CALL EB.READLIST(Z.SEL, KEY.LIST3, "", SELECTED3, ASD)

*=========================  ===========================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
**         DEBUG
            STMT.ERR = 1
            STMT.ERR1 = 1
            BALANCE = ' '
            CALL F.READ( FN.U,KEY.LIST<I>, R.U, F.U, ETEXT)
            ACCT.NO  = KEY.LIST<I>
            CUST.NO  = R.U<ACCT.UNDER.CUSTOMER.NO>
            CALL F.READ( FN.CU,CUST.NO, R.CU, F.CU, ETEXT)
            CALL F.READ( FN.AC,ACCT.NO, R.AC, F.AC, ETEXT)
            STMT.ACCT  = ACCT.NO:"-":DATE.B4
            STMT.ACCT2 = ACCT.NO
            ACCT.CATEG = R.AC<AC.CATEGORY>
            DEPT.CODE  = R.CU<EB.CUS.ACCOUNT.OFFICER>
            ACCT.CURR  = R.AC<AC.CURRENCY>
            CUST.COMP  = R.CU<EB.CUS.COMPANY.BOOK>
            IF  ACCT.CATEG EQ '6502' THEN
****** TABLE INTREST
                CALL F.READ( FN.ST,STMT.ACCT, R.ST, F.ST, STMT.ERR)
                BALANCE    = R.ST<IC.STMCR.CR.VAL.BALANCE>
                NO.OF.DAYS = R.ST<IC.STMCR.CR.NO.OF.DAYS>
                INT.CATEG  = R.ST<IC.STMCR.CR.INT.CATEG>
                INT.TR.AC  = R.ST<IC.STMCR.CR.INT.TR.AC>
                INT.TR.PL  = R.ST<IC.STMCR.CR.INT.TR.PL>

            END
            ELSE IF ACCT.CATEG EQ 6501 AND (DATE.B4.MONTH EQ '03' OR DATE.B4.MONTH EQ '06' OR DATE.B4.MONTH EQ '09' OR DATE.B4.MONTH EQ '12') THEN
                CALL F.READ( FN.ST,STMT.ACCT, R.ST, F.ST, STMT.ERR)
                BALANCE    = R.ST<IC.STMCR.CR.VAL.BALANCE>
                NO.OF.DAYS = R.ST<IC.STMCR.CR.NO.OF.DAYS>
                INT.CATEG  = R.ST<IC.STMCR.CR.INT.CATEG>
                INT.TR.AC  = R.ST<IC.STMCR.CR.INT.TR.AC>
                INT.TR.PL  = R.ST<IC.STMCR.CR.INT.TR.PL>

            END
            ELSE IF ACCT.CATEG EQ 6503 AND (DATE.B4.MONTH EQ '06' OR DATE.B4.MONTH EQ '12') THEN
                CALL F.READ( FN.ST,STMT.ACCT, R.ST, F.ST, STMT.ERR)
                BALANCE    = R.ST<IC.STMCR.CR.VAL.BALANCE>
                NO.OF.DAYS = R.ST<IC.STMCR.CR.NO.OF.DAYS>
                INT.CATEG  = R.ST<IC.STMCR.CR.INT.CATEG>
                INT.TR.AC  = R.ST<IC.STMCR.CR.INT.TR.AC>
                INT.TR.PL  = R.ST<IC.STMCR.CR.INT.TR.PL>

            END

            ELSE IF ACCT.CATEG EQ 6504 AND DATE.B4.MONTH EQ '12' THEN
                CALL F.READ( FN.ST,STMT.ACCT, R.ST, F.ST, STMT.ERR)
                BALANCE    = R.ST<IC.STMCR.CR.VAL.BALANCE>
                NO.OF.DAYS = R.ST<IC.STMCR.CR.NO.OF.DAYS>
                INT.CATEG  = R.ST<IC.STMCR.CR.INT.CATEG>
                INT.TR.AC  = R.ST<IC.STMCR.CR.INT.TR.AC>
                INT.TR.PL  = R.ST<IC.STMCR.CR.INT.TR.PL>

            END

            ELSE
******* TABLE ACRUAL
                CALL F.READ( FN.ACR,STMT.ACCT2, R.ACR, F.ACR, STMT.ERR1)
                BALANCE    = R.ACR<IC.ACRCR.CR.VAL.BALANCE>
                NO.OF.DAYS = R.ACR<IC.ACRCR.CR.NO.OF.DAYS>
                INT.CATEG  = R.ACR<IC.ACRCR.CR.INT.CATEG>
                INT.TR.AC  = R.ACR<IC.ACRCR.CR.INT.TR.AC>
                INT.TR.PL  = R.ACR<IC.ACRCR.CR.INT.TR.PL>

            END

            IF NOT(STMT.ERR) OR NOT(STMT.ERR1) THEN
*Line [ 214 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                NO.BAL      = DCOUNT(BALANCE,@VM)
                ACTUAL.BAL  = BALANCE<1,NO.BAL>
                NO.DAYS     = NO.OF.DAYS<1,NO.BAL>
                NO.MONTHS   = R.AC<AC.LOCAL.REF,ACLR.INT.NO.MONTHS>
                TOT.INT.AMT = R.AC<AC.LOCAL.REF,ACLR.INTEREST.AMOUNT>

                IF NO.MONTHS EQ '' THEN
                    NO.MONTHS   = 0
                    TOT.INT.AMT = 0
                END
                ACCT.SAVE = ACCT.NO:'-':B4.YEAR.MONTH

                IF ACTUAL.BAL GE 5000  THEN

                    NO.MONTHS = NO.MONTHS + 1

*==================== UPDATE ONLY THE FIRST TIME =================
                    IF NO.MONTHS EQ 1 THEN
                        START.DATE = DATE.B4
                        R.AC<AC.LOCAL.REF,ACLR.START.SAVE.DATE> = START.DATE
                    END

*==================== EVALUATE RATE ==============================
                    INT.AMT   = (ACTUAL.BAL * NO.DAYS)/36500
                    INT.AMT   = DROUND(INT.AMT,2)
                    INT.RATE  = 1
                    INT.DAYS  = TODAY.DATE[2]
                    TOT.INT.AMT = TOT.INT.AMT + INT.AMT
*==================== INSERT INTO SCB.STMT.ACCT.SAVE TABLE =======

                    R.SA<STMT.SAVE.CR.INT.DATE>    = TODAY.DATE
                    R.SA<STMT.SAVE.CR.NO.OF.DAYS>  = INT.DAYS
                    R.SA<STMT.SAVE.CR.VAL.BALANCE> = ACTUAL.BAL
                    R.SA<STMT.SAVE.CR.INT.RATE>    = INT.RATE
                    R.SA<STMT.SAVE.CR.INT.AMT>     = INT.AMT
                    R.SA<STMT.SAVE.CR.INT.CATEG>   = INT.CATEG
                    R.SA<STMT.SAVE.CR.INT.TR.AC>   = INT.TR.AC
                    R.SA<STMT.SAVE.CR.INT.TR.PL>   = INT.TR.PL
                    R.SA<STMT.SAVE.LIQUIDITY.CCY>  = ACCT.CURR
                    R.SA<STMT.SAVE.PAY.FLAG>       = ''
                    R.SA<STMT.SAVE.CO.CODE>        = CUST.COMP
                    CALL F.WRITE(FN.SA,ACCT.SAVE,R.SA)
                    CALL JOURNAL.UPDATE(ACCT.SAVE)

                END
*====================== ACTUAL.BAL GE 5000 AND NO.MONTHS NE 3
                IF ACTUAL.BAL LT 5000 THEN
                    NO.MONTHS   = ' '
                    START.DATE  = ' '
                    TOT.INT.AMT = ' '
                    R.AC<AC.LOCAL.REF,ACLR.START.SAVE.DATE> = ' '
*======================= GET ALL RECORDS FOR THIS CUSTOMER =======
                    Y.SEL  = "SELECT F.SCB.STMT.ACCT.SAVE WITH PAY.FLAG EQ '' AND @ID LIKE ":ACCT.NO:"... AND CO.CODE EQ ":CUST.COMP
                    CALL EB.READLIST(Y.SEL, KEY.LIST1, "", SELECTED1, ASD1)
                    IF  SELECTED1 NE 0 THEN
*======================= TO SUM ACRUOL TO BACK IT TO PL
                        TOT.CANCEL = 0
                        FOR J = 1 TO  SELECTED1
                            CALL F.READ( FN.SA,KEY.LIST1<J>, R.SA, F.SA, ETEXT)
                            ACCT.SAVE.ID = KEY.LIST1<J>
                            TOT.CANCEL       = TOT.CANCEL + R.SA<STMT.SAVE.CR.INT.AMT>
                            R.SA<STMT.SAVE.PAY.FLAG>       = 'CANCEL'
                            CALL F.WRITE(FN.SA,ACCT.SAVE.ID,R.SA)
                            CALL JOURNAL.UPDATE(ACCT.SAVE.ID)

                        NEXT J
                        OFS.ID = "UN.REV":TNO:".":ACCT.NO:YEAR:MONTH
                        GOSUB WRITE.REVERSE.OFS

                    END
                END
*==================== UPDATE NO OF MONTHS IN ACCOUNT TABLE ======

                R.AC<AC.LOCAL.REF,ACLR.INT.NO.MONTHS>      = NO.MONTHS
                R.AC<AC.LOCAL.REF,ACLR.INTEREST.AMOUNT>    = TOT.INT.AMT
*    CALL F.WRITE(FN.AC,ACCT.NO,R.AC)
*    CALL JOURNAL.UPDATE(ACCT.NO)
                ACCT.21 = ACCT.NO
                GOSUB WRITE.ACCT.21.OFS
                ACCT.21 = ' '
            END

        NEXT I
    END
*===================== OVER THAN 21 YEARS OLD ===== IN THIS MONTH ===============
    IF  SELECTED3 THEN
        FOR O = 1 TO SELECTED3
            TOT.AMT.21 = 0
            ACCT.21    = ''
            R.AC       = ''
            CALL F.READ( FN.U,KEY.LIST3<O>, R.U, F.U, ETEXT)
            ACCT.21 = KEY.LIST3<O>
            S.SEL  = "SELECT F.SCB.STMT.ACCT.SAVE WITH PAY.FLAG EQ '' AND @ID LIKE ":ACCT.21:"..."
            CALL EB.READLIST(S.SEL, KEY.LIST4, "", SELECTED4, ASD4)
            IF  SELECTED4 NE 0 THEN
                FOR NN = 1 TO  SELECTED4
                    R.SA = ''
                    CALL F.READ( FN.AC,ACCT.21, R.AC, F.AC, ETEXT)
                    ACCT.CURR = R.AC<AC.CURRENCY>
                    CALL F.READ( FN.SA,KEY.LIST4<NN>, R.SA, F.SA, ETEXT4)
                    ACCT.PAID.21  = KEY.LIST4<NN>
                    TOT.AMT.21    = TOT.AMT.21 + R.SA<STMT.SAVE.CR.INT.AMT>
                    R.SA<STMT.SAVE.PAY.FLAG>       = 'PAID'
                    CALL F.WRITE(FN.SA,ACCT.PAID.21,R.SA)
                    CALL JOURNAL.UPDATE(ACCT.PAID.21)
                NEXT NN
***  ++++++++ ******
                CUST.NO  = R.AC<AC.CUSTOMER>
                CALL F.READ( FN.CU,CUST.NO, R.CU, F.CU, ETEXT)
                ACCT.CATEG = R.AC<AC.CATEGORY>
                DEPT.CODE  = R.CU<EB.CUS.ACCOUNT.OFFICER>
                ACCT.CURR  = R.AC<AC.CURRENCY>
                CUST.COMP  = R.CU<EB.CUS.COMPANY.BOOK>
***  ++++++++ ******
                R.AC<AC.LOCAL.REF,ACLR.INT.NO.MONTHS>   = ' '
                R.AC<AC.LOCAL.REF,ACLR.START.SAVE.DATE> = ' '
                R.AC<AC.LOCAL.REF,ACLR.INTEREST.AMOUNT> = ' '
                OFS.ID = "UN.21":TNO:".":ACCT.PAID.21:YEAR:MONTH
                GOSUB WRITE.21.OFS
                GOSUB WRITE.ACCT.21.OFS
            END
        NEXT O

    END
    RETURN
*==================== WRITE OFS ==================================
WRITE.REVERSE.OFS:

    COMMA = ","
    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = '0':DEPT.CODE
    END
    BNK.SPARE.ACCT = ACCT.CURR:'11077000100':DEPT.CODE
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
**    OFS.USER.INFO     = "INPUTT":DEPT.CODE:"/":"/":CUST.COMP
    OFS.USER.INFO     = "AUTO.LD":"/":"/":CUST.COMP
    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":'AC27':COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=":ACCT.CURR:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":ACCT.CURR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": BNK.SPARE.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":'PL50000':COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.CANCEL:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB':COMMA
    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":'SCB':COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":DEPT.CODE:COMMA

    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY.DATE

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*   SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*       SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
    RETURN
*==================== WRITE OFS OVER THAN 21 =========================
WRITE.21.OFS:
    COMMA = ","

    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = '0':DEPT.CODE
    END
    BNK.SPARE.ACCT = ACCT.CURR:'11077000100':DEPT.CODE
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
**    OFS.USER.INFO     = "INPUTT":DEPT.CODE:"/":"/":CUST.COMP
    OFS.USER.INFO     = "AUTO.LD":"/":"/":CUST.COMP
    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":'AC19':COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=":ACCT.CURR:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":ACCT.CURR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":BNK.SPARE.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":ACCT.21:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.AMT.21:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB':COMMA
    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":'SCB':COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":DEPT.CODE:COMMA

    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY.DATE

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
    RETURN
*============== WRITE OFS FOR UPDATING ACCOUNT GT 21 IN TIS MONTH ==================
WRITE.ACCT.21.OFS:
    COMMA = ","
    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = '0':DEPT.CODE
    END
    OFS.OPERATION      = "ACCOUNT"
    OFS.OPTIONS        = "SCB.UNDER"
**    OFS.USER.INFO      = "INPUTT":DEPT.CODE:"/":"/":CUST.COMP
    OFS.USER.INFO     = "AUTO.LD":"/":"/":CUST.COMP
    OFS.MESSAGE.DATA   = "INT.NO.MONTHS=":R.AC<AC.LOCAL.REF,ACLR.INT.NO.MONTHS>:COMMA
    OFS.MESSAGE.DATA  := "START.SAVE.DATE=":R.AC<AC.LOCAL.REF,ACLR.START.SAVE.DATE>:COMMA
    OFS.MESSAGE.DATA  := "INTEREST.AMOUNT=":R.AC<AC.LOCAL.REF,ACLR.INTEREST.AMOUNT>:COMMA
    OFS.ID = ACCT.21
    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ACCT.21:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY.DATE

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
    RETURN
END
