* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBR.CUST.PASSPORT.DUP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "PASSPORT.NO.Duplications.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"PASSPORT.NO.Duplications.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "PASSPORT.NO.Duplications.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE PASSPORT.NO.Duplications.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create PASSPORT.NO.Duplications.CSV File IN &SAVEDLISTS&'
        END
    END

    HEAD.DESC  = "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "PASSPORT.NUMBER":","
    HEAD.DESC := "BRANCH":","
    HEAD.DESC := "COUNTRY.NAME":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------------------
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST    = '' ; SELECTED    = ''  ;  ER.MSG = ''
    CBE.ID.NEXT = '' ; CBE.ID.PREV = ''

    RETURN

*========================================================================
PROCESS:
*-------------------------------------------
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY NE EG AND @ID UNLIKE 994... AND POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 WITHOUT SECTOR IN (5010 5020) BY NATIONALITY BY ID.NUMBER"
    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY NE EG AND @ID UNLIKE 994... AND POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 WITHOUT SECTOR IN (5010 5020) BY ID.NUMBER BY NATIONALITY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E2)
            CALL F.READ(FN.CU,KEY.LIST<I+1>,R.CU1,F.CU,E3)
            CALL F.READ(FN.CU,KEY.LIST<I-1>,R.CU2,F.CU,E4)

            CBE.ID      = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
            CBE.ID.NEXT = R.CU1<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
            CBE.ID.PREV = R.CU2<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>

            CUS.ID    = KEY.LIST<I>
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            COMP.ID   = R.CU<EB.CUS.COMPANY.BOOK>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)
            NAT.ID    = R.CU<EB.CUS.NATIONALITY>
            CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,NAT.ID,COUNTRY.NAME)

            IF CBE.ID EQ CBE.ID.NEXT THEN
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := CBE.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END ELSE
                IF CBE.ID EQ CBE.ID.PREV THEN

                    BB.DATA  = CUS.ID:","
                    BB.DATA := CUST.NAME:","
                    BB.DATA := CBE.ID:","
                    BB.DATA := COMP.NAME:","
                    BB.DATA := NAT.ID:","
                    BB.DATA := COUNTRY.NAME:","

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            END
        NEXT I
    END
    RETURN
END
