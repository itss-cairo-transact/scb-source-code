* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.FATCA.STMT.ENTRY.1
*    PROGRAM SBR.FATCA.STMT.ENTRY.1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FATCA.RATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 47 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    GOTO PROGRAM.END
*==============================================================
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBR.FATCA.STMT.ENTRY.1'
    CALL PRINTER.ON(REPORT.ID,'')
    YTEXT = "Enter Customer ID : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")

    CUS.ID    = COMI
******************
    YTEXT = "Enter Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")

    START.DATE    = COMI
******************
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")

    END.DATE    = COMI
    Q = 1
    AMT.862     = 0; AMT.442   = 0; AMT.795   = 0; AMT.443   = 0
    AMT.864     = 0; AMT.869   = 0; AMT.424   = 0; AMT.LCY   = 0
    AMT.TOT     = 0; TOT.442   = 0; TOT.795   = 0; TOT.443   = 0
    TOT.864     = 0; TOT.869   = 0; TOT.424   = 0; TOT.CUS   = 0
    AMT.381     = 0; TOT.381   = 0; TOT.862   = 0
**********************************************

    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = ''; F.CUS.AC = ''
    CALL OPF(FN.CUS.AC,F.CUS.AC)
    FN.CLOSED = 'FBNK.ACCOUNT.CLOSED'; R.CLOSED = ''; F.CLOSED = ''
    CALL OPF(FN.CLOSED,F.CLOSED)
    FN.STMT = 'FBNK.STMT.ENTRY'; F.STMT = ''; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)
    KEY.LIST ="" ; SELECTED = "" ;  ER.MSG=""; ER.STMT = ''
    FN.AC ='FBNK.ACCOUNT';    R.AC = ''; F.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ( FN.CUS.AC,CUS.ID, R.CUS.AC, F.CUS.AC,ETEXT1)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOC.REF)
    CUS.NAME = LOC.REF<1,CULR.ARABIC.NAME>

**********GET CLOSED ACCOUNTS FOR THAT CUSTOMER *******
    CUSS.ID = CUS.ID
    IF LEN(CUS.ID) EQ 7 THEN
        CUSS.ID = '0':CUS.ID
    END
    T.SEL = "SELECT FBNK.ACCOUNT.CLOSED WITH @ID LIKE ":CUSS.ID:"..."
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SEL.CLOSED, ASD)
    IF SEL.CLOSED NE 0 THEN
        FOR Q = 1 TO SEL.CLOSED
            IF Q = 1 AND R.CUS.AC EQ '' THEN
                R.CUS.AC = KEY.LIST<Q>
            END
            ELSE
                R.CUS.AC = R.CUS.AC :@VM:KEY.LIST<Q>
            END
        NEXT Q
    END
    RETURN

************************************************
CALLDB:
********
    LOOP
        REMOVE ACC.ID FROM R.CUS.AC  SETTING POS1
    WHILE ACC.ID:POS1
*********CHECK CATEGORY**********

        AMT.862     = 0; AMT.442   = 0; AMT.795   = 0; AMT.443   = 0
        AMT.864     = 0; AMT.869   = 0; AMT.424   = 0; AMT.381   = 0
        AMT.LCY     = 0; TOT.ACC   = 0;

        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.ID,CATT)
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACC.ID,CUR)
        IF CATT EQ '' THEN
            AC.ID.HIS = ACC.ID:';1'
            CALL DBR('ACCOUNT$HIS':@FM:AC.CATEGORY,AC.ID.HIS,CATT)
            CALL DBR('ACCOUNT$HIS':@FM:AC.CURRENCY,AC.ID.HIS,CUR)
        END
        IF (CATT EQ '1002' OR CATT EQ '1205' OR CATT EQ '1206' OR CATT EQ '1207' OR CATT EQ '1208' OR CATT EQ '1407' OR CATT EQ '1408' OR CATT EQ '1413' OR CATT EQ '1445' OR CATT EQ '1455' OR CATT EQ '9002' OR CATT EQ '1022' OR CATT EQ '1021') THEN CONTINUE
*********************************
        Q = Q + 1
        TOTAL.INT   = 0
*type.of.date = "VALUE"
        type.of.date = "BOOK"
        include.fwd = ""
*Line [ 140 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        ACC.NO = ACC.ID:@FM:type.of.date:@FM:include.fwd

        CALL EB.ACCT.ENTRY.LIST(ACC.NO,START.DATE,END.DATE,ENT.LIST,OP.BAL,STMT.ERR)
        LOOP
            REMOVE STD.ID FROM ENT.LIST SETTING POS
        WHILE STD.ID:POS
            CALL F.READ(FN.STMT,STD.ID,R.STMT,F.STMT,ER.STMT)

            TRN.CODE  = R.STMT<AC.STE.TRANSACTION.CODE>

            AMT.LCY = R.STMT<AC.STE.AMOUNT.LCY>
            IF CUR NE 'EGP' THEN
                AMT.LCY = R.STMT<AC.STE.AMOUNT.FCY>
            END

            BEGIN CASE
            CASE TRN.CODE = '862'
                AMT.862   = AMT.862 + AMT.LCY
            CASE TRN.CODE = '442'
                AMT.442   = AMT.442 + AMT.LCY
            CASE TRN.CODE = '424'
                AMT.424   = AMT.424 + AMT.LCY
            CASE TRN.CODE = '795'
                AMT.795   = AMT.795 + AMT.LCY
            CASE TRN.CODE = '443'
                AMT.443   = AMT.443 + AMT.LCY
            CASE TRN.CODE = '864'
                AMT.864   = AMT.864 + AMT.LCY
            CASE TRN.CODE = '869'
                AMT.869   = AMT.869 + AMT.LCY
            CASE TRN.CODE = '381'
                AMT.381   = AMT.381 + AMT.LCY
            CASE TRN.CODE = '389'
                AMT.381   = AMT.381 + AMT.LCY
            CASE TRN.CODE = '891'
                AMT.381   = AMT.381 + AMT.LCY
            END CASE

            IF AMT.LCY GT 0 THEN
                TOT.ACC = TOT.ACC + AMT.LCY
            END
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOC.REF)
            CUS.NAME = LOC.REF<1,CULR.ARABIC.NAME>

        REPEAT

**********CALC RATE IN USD*******************************

        IF CUR NE 'USD' THEN
            CALL DBR('SCB.FATCA.RATE':@FM:FATCA.USD.RATE,CUR,CUR.RATE)
            AMT.862   = AMT.862/CUR.RATE
            AMT.442   = AMT.442/CUR.RATE
            AMT.424   = AMT.424/CUR.RATE
            AMT.795   = AMT.795/CUR.RATE
*TOTAL.INT = TOTAL.INT/CUR.RATE
            AMT.381   = AMT.381/CUR.RATE
            AMT.864   = AMT.864/CUR.RATE
            AMT.869   = AMT.869/CUR.RATE
            TOT.ACC   = TOT.ACC/CUR.RATE
        END

*********PRINT ACCOUNT DETAILS***************************
        XX  = SPACE(132)
        XX<1,Q>[1,20]   = ACC.ID
        XX<1,Q>[21,10]  = DROUND(AMT.862,2)
        XX<1,Q>[32,10]  = DROUND(AMT.442,2)
        XX<1,Q>[43,10]  = DROUND(AMT.424,2)
        XX<1,Q>[54,10]  = DROUND(AMT.795,2)
        XX<1,Q>[66,10]  = DROUND(AMT.381,2)
        XX<1,Q>[84,10]  = DROUND(AMT.864,2)
        XX<1,Q>[96,10]  = DROUND(AMT.869,2)
        XX<1,Q>[112,10]  = DROUND(TOT.ACC,2)
        PRINT XX<1,Q>

*******TOTALS*********************

        TOT.862 = TOT.862 + DROUND(AMT.862,2)
        TOT.442 = TOT.442 + DROUND(AMT.442,2)
        TOT.424 = TOT.424 + DROUND(AMT.424,2)
        TOT.795 = TOT.795 + DROUND(AMT.795,2)
        TOT.381 = TOT.381 + DROUND(AMT.381,2)
        TOT.864 = TOT.864 + DROUND(AMT.864,2)
        TOT.869 = TOT.869 + DROUND(AMT.869,2)
*TOT.ACC = TOT.ACC + DROUND(TOT.ACC,2)
        TOT.CUS = TOT.CUS + TOT.ACC
    REPEAT
**********************************
    Q = Q + 1
    PRINT STR('_',120)
    Q = Q + 1
    PRINT XX<1,Q>
    Q = Q + 1
    XX<1,Q>[1,10]   = "��������"
    XX<1,Q>[21,10]  = DROUND(TOT.862,2)
    XX<1,Q>[32,10]  = DROUND(TOT.442,2)
    XX<1,Q>[43,10]  = DROUND(TOT.424,2)
    XX<1,Q>[54,10]  = DROUND(TOT.795,2)
    XX<1,Q>[66,10]  = DROUND(TOT.381,2)
    XX<1,Q>[84,10]  = DROUND(TOT.864,2)
    XX<1,Q>[96,10]  = DROUND(TOT.869,2)
    XX<1,Q>[112,10]  = DROUND(TOT.CUS,2)
    PRINT XX<1,Q>

**********PRINT TOT OF TOTALS**********

    PRINT STR('_',120)
    Q = Q + 1

    TOT1 = TOT.442 + TOT.862
    TOT2 = TOT.424 + TOT.795 + TOT.381
    TOT3 = TOT.864 + TOT.869
    PRINT XX<1,Q>
    Q = Q + 1
    XX<1,Q>[1,30]   = "����� ��� ���� ����� �����"
    XX<1,Q>[43,30]  = DROUND(TOT1,2)
    PRINT XX<1,Q>
    Q = Q + 1
    PRINT XX<1,Q>
    Q = Q + 1
    XX<1,Q>[1,30]   = "������� ��������"
    XX<1,Q>[43,30]  = DROUND(TOT2,2)
    PRINT XX<1,Q>
    Q = Q + 1
    PRINT XX<1,Q>
    Q = Q + 1
    XX<1,Q>[1,30]   = "������� �������� �� ����� �����"
    XX<1,Q>[43,30]  = DROUND(TOT3,2)
    PRINT XX<1,Q>
    Q = Q + 1
    PRINT XX<1,Q>
    Q = Q + 1
    XX<1,Q>[1,30]   = "������ ����"
    XX<1,Q>[43,30]  = DROUND((TOT.CUS - (TOT1 + TOT2 + TOT3)),2)
    PRINT XX<1,Q>
    Q = Q + 1
    PRINT XX<1,Q>
    Q = Q + 1
    XX<1,Q>[1,30]   = "������ ������� �������"
    XX<1,Q>[43,30]  = DROUND(TOT.CUS,2)
    PRINT XX<1,Q>
    Q = Q + 1
    PRINT XX<1,Q>
*Q = Q + 1
*XX<1,Q>[1,30]   = "������ �������"
*XX<1,Q>[43,30]  = DROUND((TOT.CUS - (TOT1 + TOT2 + TOT3)),2)
*PRINT XX<1,Q>

    RETURN
*===============================================================
PRINT.HEAD:
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������ "
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(80):"��� ������ : ":"'P'"
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME, R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"SBR.FATCA.STMT.ENTRY.1":SPACE(80):"����������":":":YY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"���� ������ ������� �������� �������"
    PR.HD :="'L'":SPACE(45):STR('_',35)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"��� ������":SPACE(5):CUS.ID:SPACE(10):"��� ������":SPACE(5):CUS.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"��� ������":SPACE(3):"��� �.�����":SPACE(3):"������� �.�����":SPACE(3):"�.�����":SPACE(3):"�.������":SPACE(3):"�.�����/���������":SPACE(3):"�.�������":SPACE(3):"�.����� �����":SPACE(3):"������ �.�����"

    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    RETURN
**********
PROGRAM.END:
    RETURN
*********
END
