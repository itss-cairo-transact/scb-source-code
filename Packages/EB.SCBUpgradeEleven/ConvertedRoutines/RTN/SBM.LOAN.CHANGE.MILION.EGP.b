* @ValidationCode : MjoxMjQwNzM3NTk6Q3AxMjUyOjE2NDQ5MjUwNTc1NzI6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBM.LOAN.CHANGE.MILION.EGP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LOAN.CHANGE.MILION.EGP'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
    AMT.LCY   = 0   ; AMT.LCY.C = 0     ; AMT.LCY.O = 0
    AMT.LCY.TOT = 0 ; AMT.LCY.TOT.C = 0 ; AMT.LCY.TOT.O = 0

RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.CBE.D = 'F.CBE.STATIC.AC.LD.DETAIL' ; F.CBE.D = ''
    CALL OPF(FN.CBE.D,F.CBE.D)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CCY = 'FBNK.CURRENCY'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)


    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    T.SEL = "SELECT ":FN.CBE:" WITH CBE.BR EQ ":COMP.BR:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            CUS.ID  = R.CBE<ST.CBE.CUSTOMER.CODE>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            AMT.LCY = R.CBE<ST.CBE.FACLTY.LE> + R.CBE<ST.CBE.CUR.AC.LE.DR>
            AMT.LCY = AMT.LCY * -1

            AMT.LCY.O = R.CBE<ST.CBE.FACLTY.LEO> + R.CBE<ST.CBE.CUR.AC.LE.DRO>
            AMT.LCY.O = AMT.LCY.O * -1
            GOSUB CALC
        NEXT I
    END

    T.SEL1 = "SELECT ":FN.CBE.D:" WITH CBEM.CATEG EQ 3201 AND CBEM.CY EQ 'EGP' AND CBEM.BR EQ ":COMP.BR
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG2)
    IF SELECTED1 THEN
        FOR X = 1 TO SELECTED1
            CALL F.READ(FN.CBE.D,KEY.LIST1<X>,R.CBE.D,F.CBE.D,E3)

            CUS.ID  = R.CBE.D<STD.CBEM.CUSTOMER.CODE>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            AMT.LCY    = R.CBE.D<STD.CBEM.IN.LCY>
            AMT.LCY    = AMT.LCY * -1

            AMT.LCY.O  = R.CBE.D<STD.CBEM.IN.LCYO>
            AMT.LCY.O  = AMT.LCY.O * -1

            GOSUB CALC
        NEXT X
    END
    PRINT STR('=',130)
    XX2 = SPACE(120)
    XX2<1,1>[1,35]   = "�����������"
    XX2<1,1>[65,20]  = AMT.LCY.TOT.O
    XX2<1,1>[85,20]  = AMT.LCY.TOT
    XX2<1,1>[105,20] = AMT.LCY.TOT.C
    PRINT XX2<1,1>
    PRINT STR('=',130)

RETURN

CALC:
    AMT.LCY.C = AMT.LCY - AMT.LCY.O

    IF AMT.LCY.C LT 0 THEN
        AMTC = AMT.LCY.C * -1
    END ELSE
        AMTC = AMT.LCY.C
    END

    AMT.LCY   = AMT.LCY / 1000
    AMT.LCY.O = AMT.LCY.O / 1000
    AMT.LCY.C = AMT.LCY.C / 1000

    AMT.LCY   = DROUND(AMT.LCY,'0')
    AMT.LCY.O = DROUND(AMT.LCY.O,'0')
    AMT.LCY.C = DROUND(AMT.LCY.C,'0')

    IF AMTC GE 1000000 THEN
        XX = SPACE(120)
        XX<1,1>[1,15]   = CUS.ID
        XX<1,1>[13,35]  = CUST.NAME
        XX<1,1>[65,20]  = AMT.LCY.O
        XX<1,1>[85,20]  = AMT.LCY
        XX<1,1>[105,20] = AMT.LCY.C
        PRINT XX<1,1>
        PRINT STR('-',130)

        AMT.LCY.TOT   += AMT.LCY
        AMT.LCY.TOT.O += AMT.LCY.O
        AMT.LCY.TOT.C += AMT.LCY.C
    END

    AMT.LCY   = 0   ; AMT.LCY.C = 0     ; AMT.LCY.O = 0

RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 159 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TDD = TODAY[1,6]


    DAT1 = TDD:'01'
    CALL CDT("",DAT1,'-1C')

    DAT2 = DAT1[1,6]:'01'
    CALL CDT("",DAT2,'-1C')

    TD1 = DAT1[1,4]:'/':DAT1[5,2]
    TD2 = DAT2[1,4]:'/':DAT2[5,2]



    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������ ����� ������ ��������������"
    PR.HD :="'L'":SPACE(55):"���� ���� �� ����� ����":SPACE(20):"������ ������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(35):"���� ���":SPACE(10):"���� ���":SPACE(13):"������"
    PR.HD :="'L'":SPACE(65):TD2:SPACE(11):TD1
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
