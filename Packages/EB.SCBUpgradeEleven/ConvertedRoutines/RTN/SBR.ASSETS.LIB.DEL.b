* @ValidationCode : MjotMzE1MjQ5MjEzOkNwMTI1MjoxNjQwODU3MzgxNTEwOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:43:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.ASSETS.LIB.DEL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ASSETS.LIB

*---------------------------------------
    FN.DEP = "F.SCB.ASSETS.LIB"  ; F.DEP  = ""
    CALL OPF (FN.DEP,F.DEP)
    COMP  = ID.COMPANY
    WS.TD = TODAY

*--------------------------------------------------------------------

    WS.SEL.ID = "SELECT F.SCB.ASSETS.LIB WITH STATUS EQ '1' AND RECIEVE.DATE EQ ":WS.TD
    CALL EB.READLIST(WS.SEL.ID,KEY.LIST,"",SELECTED,ER.MSG)
    YTEXT  = '����� ���� ��� ��� ���� = ':SELECTED
    YTEXT := 'Y/N �� ���� �����  '
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLAG = COMI
    IF FLAG = 'Y' OR FLAG = 'y' THEN

        EXECUTE WS.SEL.ID
        EXECUTE 'DELETE F.SCB.ASSETS.LIB'
        TEXT = '�� ����� �����' ; CALL REM
    END ELSE
        TEXT = '�� ������ �� ��������' ; CALL REM
    END


RETURN
************************************************************
END
