* @ValidationCode : MjotMTk2NTg2Mzc5MjpDcDEyNTI6MTY0MDg1OTMwNjk4NjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 12:15:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBR.CBE.CREDIT.HO.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CBE.CREDIT.HO
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CREDIT.CBE.HO
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

*********************** OPENING FILES *****************************
    FN.CCBE = "F.SCB.CBE.CREDIT.HO"
    F.CCBE = ''
    CALL OPF(FN.CCBE,F.CCBE)
    OPEN FN.CCBE TO FILEVAR ELSE ABORT 201, FN.CCBE
    CLEARFILE FILEVAR


    PRINT "DAILY FILE CLEARED"

    FN.CU  = "FBNK.CUSTOMER"               ; F.CU   = ""
    FN.CBE = "F.SCB.CBE.CREDIT.HO"         ; F.CBE  = ""
    FN.ORG = "F.SCB.CREDIT.CBE.HO"         ; F.ORG  = ""

    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.ORG,F.ORG)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*------------------------------------------------------------------

    T.SEL = "SELECT ":FN.ORG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ORG,KEY.LIST<I>,R.ORG,F.ORG,E1)
            BNK.DATE = R.ORG<SCB.C.CBE.BNK.DATE>

            CBE.ID = KEY.LIST<I>
            CALL F.READ(FN.CBE,CBE.ID,R.CBE,F.CBE,E2)

********************** COPY NEW DATA ******************************
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
** CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,KEY.LIST<I>,LOCAL.REF)
** CR.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            CR.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>

** CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,KEY.LIST<I>,LOCAL.REF)
** NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            COMP.ID = R.ORG<SCB.C.CBE.CO.CODE>
*---------------------------------------------------------
*** CR DIRECT ***
            TOT.CR.DIR  = R.ORG<SCB.C.CBE.TOT.CR.1>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.2>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.3>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.4>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.5>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.6>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.7>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.8>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.9>
            TOT.CR.DIR += R.ORG<SCB.C.CBE.TOT.CR.10>
******
*** US DIRECT ***
            TOT.US.DIR  = R.ORG<SCB.C.CBE.TOT.US.1>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.2>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.3>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.4>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.5>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.6>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.7>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.8>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.9>
            TOT.US.DIR += R.ORG<SCB.C.CBE.TOT.US.10>
*****
*** CR INDIRECT ***
            DR.FLG = R.ORG<SCB.C.CBE.APP.FLG>
            TOT.CR.INDIR  = R.ORG<SCB.C.CBE.TOT.CR.11>
            TOT.CR.INDIR += R.ORG<SCB.C.CBE.TOT.CR.12>
            IF DR.FLG = 'DR' THEN
                TOT.CR.INDIR += R.ORG<SCB.C.CBE.TOT.CR.13>
            END
****
*** US INDIRECT ***
            TOT.US.INDIR  = R.ORG<SCB.C.CBE.TOT.US.11>
            TOT.US.INDIR += R.ORG<SCB.C.CBE.TOT.US.12>
            IF DR.FLG = 'DR' THEN
                TOT.US.INDIR += R.ORG<SCB.C.CBE.TOT.US.13>
            END
            TOTAL.INTERNAL = TOT.CR.DIR + TOT.CR.INDIR
            TOTAL.USED     = TOT.US.DIR + TOT.US.INDIR

************
            R.CBE<SCH.CUSTOMER.ID>        = KEY.LIST<I>
            R.CBE<SCH.LPE.DATE>           = BNK.DATE
            R.CBE<SCH.NEW.SECTOR>         = NEW.SEC
            R.CBE<SCH.CREDIT.CODE>        = CR.CODE
            R.CBE<SCH.DIR.INTERNAL.AMT>   = TOT.CR.DIR
            R.CBE<SCH.INDIR.INTERNAL.AMT> = TOT.CR.INDIR
            R.CBE<SCH.TOTAL.INTERNAL.AMT> = TOTAL.INTERNAL
            R.CBE<SCH.DIR.USED.AMT>       = TOT.US.DIR
            R.CBE<SCH.INDIR.USED.AMT>     = TOT.US.INDIR
            R.CBE<SCH.TOTAL.USED.AMT>     = TOTAL.USED
            R.CBE<SCH.CO.CODE>            = COMP.ID

            CALL F.WRITE(FN.CBE,CBE.ID,R.CBE)
            CALL  JOURNAL.UPDATE(CBE.ID)

        NEXT I
    END
END
