* @ValidationCode : MjotMzA0NDQ4NTAxOkNwMTI1MjoxNjQ0OTI1MDY3MDc3OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-400</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBR.LAST.TRN.EMP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FOREX
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB GET.FT
    GOSUB GET.LD
    GOSUB GET.TT
    GOSUB GET.IN
    GOSUB GET.ACI
    GOSUB GET.ADI
    GOSUB GET.LC
    GOSUB GET.DR
    GOSUB GET.FX
    GOSUB GET.MM
    GOSUB GET.CU
    GOSUB GET.AC

    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  ����� �������  ***'
    PRINT XX25<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*==============================================================
INITIATE:
    FN.FT    = 'FBNK.FUNDS.TRANSFER$HIS'        ; F.FT    = ''
    FN.TT    = 'FBNK.TELLER$HIS'                ; F.TT    = ''
    FN.LD    = 'FBNK.LD.LOANS.AND.DEPOSITS'     ; F.LD    = ''
    FN.LD.H  = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H  = ''
    FN.LC    = 'FBNK.LETTER.OF.CREDIT'          ; F.LC    = ''
    FN.LC.H  = 'FBNK.LETTER.OF.CREDIT$HIS'      ; F.LC.H  = ''
    FN.DR    = 'FBNK.DRAWINGS'                  ; F.DR    = ''
    FN.DR.H  = 'FBNK.DRAWINGS$HIS'              ; F.DR.H  = ''
    FN.CU    = 'FBNK.CUSTOMER'                  ; F.CU    = ''
    FN.CU.H  = 'FBNK.CUSTOMER$HIS'              ; F.CU.H  = ''
    FN.AC    = 'FBNK.ACCOUNT'                   ; F.AC    = ''
    FN.AC.H  = 'FBNK.ACCOUNT$HIS'               ; F.AC.H  = ''
    FN.FX    = 'FBNK.FOREX'                     ; F.FX    = ''
    FN.FX.H  = 'FBNK.FOREX$HIS'                 ; F.FX.H  = ''
    FN.MM    = 'FBNK.MM.MONEY.MARKET'           ; F.MM    = ''
    FN.MM.H  = 'FBNK.MM.MONEY.MARKET$HIS'       ; F.MM.H  = ''
    FN.ADI   = 'FBNK.ACCOUNT.DEBIT.INT'         ; F.ADI   = ''
    FN.ACI   = 'FBNK.ACCOUNT.CREDIT.INT'        ; F.ACI   = ''
    FN.IN    = 'F.INF.MULTI.TXN'                ; F.IN    = ''
    FN.USR   = 'F.USER'                         ; F.USR   = ''
    FN.USR.H = 'F.USER$HIS'                     ; F.USR.H = ''

    CALL OPF(FN.FT,F.FT)   ; CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.LD,F.LD)   ; CALL OPF(FN.LD.H,F.LD.H)
    CALL OPF(FN.LC,F.LC)   ; CALL OPF(FN.LC.H,F.LC.H)
    CALL OPF(FN.DR,F.DR)   ; CALL OPF(FN.DR.H,F.DR.H)
    CALL OPF(FN.CU,F.CU)   ; CALL OPF(FN.CU.H,F.CU.H)
    CALL OPF(FN.AC,F.AC)   ; CALL OPF(FN.AC.H,F.AC.H)
    CALL OPF(FN.FX,F.FX)   ; CALL OPF(FN.FX.H,F.FX.H)
    CALL OPF(FN.MM,F.MM)   ; CALL OPF(FN.MM.H,F.MM.H)
    CALL OPF(FN.ADI,F.ADI) ; CALL OPF(FN.ACI,F.ACI)
    CALL OPF(FN.IN,F.IN)   ; CALL OPF(FN.USR,F.USR)
    CALL OPF(FN.USR.H,F.USR.H)

    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""

    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "����� ����� ��� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    WS.EMP.NO    = COMI
    EMP.NO.SEL   = '...SCB.':COMI:'...'
    WS.EMP.LOGIN = 'SCB.':COMI

    CALL F.READ(FN.USR,WS.EMP.LOGIN,R.USR,F.USR,E5)
    IF NOT(E5) THEN
        WS.USER.NAME = R.USR<EB.USE.USER.NAME>
        WS.END.DATE  = R.USR<EB.USE.END.DATE.PROFILE>
        WS.LAST.DATE = R.USR<EB.USE.DATE.LAST.SIGN.ON>
        WS.LAST.TIME = R.USR<EB.USE.TIME.LAST.SIGN.ON>

    END ELSE
        CALL F.READ.HISTORY(FN.USR.H,WS.EMP.LOGIN,R.USR.H,F.USR.H,E6)

        WS.USER.NAME = R.USR.H<EB.USE.USER.NAME>
        WS.END.DATE  = R.USR.H<EB.USE.END.DATE.PROFILE>
        WS.LAST.DATE = R.USR.H<EB.USE.DATE.LAST.SIGN.ON>
        WS.LAST.TIME = R.USR.H<EB.USE.TIME.LAST.SIGN.ON>
    END
    WS.END.DATE  = FMT(WS.END.DATE,"####/##/##")
    WS.LAST.DATE = FMT(WS.LAST.DATE,"####/##/##")
    WS.LAST.TIME = FMT(WS.LAST.TIME,"##:##:##")
    WS.EMP.LOGIN = FIELD(WS.EMP.LOGIN,";",1)

    SCB.CO = ID.COMPANY

RETURN
*========================================================================
GET.FT:
*=======

    T.SEL = "SELECT ":FN.FT:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.FT,KEY.LIST<SELECTED>,R.FT,F.FT,E3)
        WS.FT.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.FT<FT.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.FT.DATE = '20':R.FT<FT.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 166 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.FT<FT.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.FT<FT.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.FT.DATE         = '20':WS.DATE.TIME[1,6]
        END

        WS.FT.DATE = FMT(WS.FT.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.FT.ID
        XX1<1,1>[34,20] = WS.FT.DATE
        XX1<1,1>[60,20] = 'FUNDS.TRANSFER'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END

RETURN
*===============================================================
GET.LD:
*=======
    T.SEL = "SELECT ":FN.LD:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.LD,KEY.LIST<SELECTED>,R.LD,F.LD,E3)
        WS.LD.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.LD<LD.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.LD.DATE = '20':R.LD<LD.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 198 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.LD<LD.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.LD<LD.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.LD.DATE         = '20':WS.DATE.TIME[1,6]
        END
        WS.LD.DATE = FMT(WS.LD.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.LD.ID
        XX1<1,1>[34,20] = WS.LD.DATE
        XX1<1,1>[60,20] = 'LOANS.AND.DEPOSITS'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.LD.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.LD.H,KEY.LIST<SELECTED>,R.LD.H,F.LD.H,E3)

            WS.LD.ID   = KEY.LIST<SELECTED>
            FINDSTR WS.EMP.LOGIN IN R.LD.H<LD.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.LD.DATE = '20':R.LD.H<LD.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 225 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.LD.H<LD.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.LD.H<LD.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.LD.DATE         = '20':WS.DATE.TIME[1,6]
            END
            WS.LD.DATE = FMT(WS.LD.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.LD.ID
            XX1<1,1>[34,20] = WS.LD.DATE
            XX1<1,1>[60,20] = 'LOANS.AND.DEPOSITS'

            PRINT XX1<1,1>
            PRINT STR('-',130)
        END
    END
RETURN
*===============================================================
GET.TT:
*=======
    T.SEL = "SELECT ":FN.TT:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.TT,KEY.LIST<SELECTED>,R.TT,F.TT,E3)

        WS.TT.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.TT<TT.TE.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.TT.DATE = '20':R.TT<TT.TE.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 256 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.TT<TT.TE.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.TT<TT.TE.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.TT.DATE         = '20':WS.DATE.TIME[1,6]
        END

        WS.TT.DATE = FMT(WS.TT.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.TT.ID
        XX1<1,1>[34,20] = WS.TT.DATE
        XX1<1,1>[60,20] = 'TELLER'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END

RETURN
*===============================================================
GET.IN:
*=======
    T.SEL = "SELECT ":FN.IN:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.IN,KEY.LIST<SELECTED>,R.IN,F.IN,E3)

        WS.IN.ID   = KEY.LIST<SELECTED>
        FINDSTR WS.EMP.LOGIN IN R.IN<INF.MLT.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.IN.DATE = '20':R.IN<INF.MLT.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 288 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.IN<INF.MLT.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.IN<INF.MLT.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.IN.DATE = '20':WS.DATE.TIME[1,6]
        END
        WS.IN.DATE = FMT(WS.IN.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.IN.ID
        XX1<1,1>[34,20] = WS.IN.DATE
        XX1<1,1>[60,20] = 'INF.MULTI.TXN'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END

RETURN
*===============================================================
GET.ACI:
*=======
    T.SEL = "SELECT ":FN.ACI:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY DATE.TIME"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.ACI,KEY.LIST<SELECTED>,R.ACI,F.ACI,E3)

        WS.ACI.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.ACI<IC.ACI.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.ACI.DATE = '20':R.ACI<IC.ACI.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 320 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.ACI<IC.ACI.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.ACI<IC.ACI.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.ACI.DATE = '20':WS.DATE.TIME[1,6]

        END

        WS.ACI.DATE = FMT(WS.ACI.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.ACI.ID
        XX1<1,1>[34,20] = WS.ACI.DATE
        XX1<1,1>[60,20] = 'ACCOUNT.CREDIT.INT'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END

RETURN
*===============================================================
GET.ADI:
*=======
    T.SEL = "SELECT ":FN.ADI:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY DATE.TIME"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.ADI,KEY.LIST<SELECTED>,R.ADI,F.ADI,E3)

        WS.ADI.ID   = KEY.LIST<SELECTED>
        FINDSTR WS.EMP.LOGIN IN R.ADI<IC.ADI.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.ADI.DATE = '20':R.ADI<IC.ADI.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 353 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.ADI<IC.ADI.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.ADI<IC.ADI.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.ADI.DATE = '20':WS.DATE.TIME[1,6]
        END

        WS.ADI.DATE = FMT(WS.ADI.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.ADI.ID
        XX1<1,1>[34,20] = WS.ADI.DATE
        XX1<1,1>[60,20] = 'ACCOUNT.DEBIT.INT'
        PRINT XX1<1,1>
        PRINT STR('-',130)

    END

RETURN
*===============================================================
GET.LC:
*=======
    T.SEL = "SELECT ":FN.LC:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.LC,KEY.LIST<SELECTED>,R.LC,F.LC,E3)

        WS.LC.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.LC<TF.LC.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.LC.DATE = '20':R.LC<TF.LC.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 385 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.LC<TF.LC.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.LC<TF.LC.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.LC.DATE = '20':WS.DATE.TIME[1,6]
        END

        WS.LC.DATE = FMT(WS.LC.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.LC.ID
        XX1<1,1>[34,20] = WS.LC.DATE
        XX1<1,1>[60,20] = 'LETTER.OF.CREDIT'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.LC.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.LC.H,KEY.LIST<SELECTED>,R.LC.H,F.LC.H,E3)

            WS.LC.ID   = KEY.LIST<SELECTED>

            FINDSTR WS.EMP.LOGIN IN R.LC.H<TF.LC.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.LC.DATE = '20':R.LC.H<TF.LC.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 414 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.LC.H<TF.LC.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.LC.H<TF.LC.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.LC.DATE = '20':WS.DATE.TIME[1,6]
            END


            WS.LC.DATE = FMT(WS.LC.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.LC.ID
            XX1<1,1>[34,20] = WS.LC.DATE
            XX1<1,1>[60,20] = 'LETTER.OF.CREDIT'

            PRINT XX1<1,1>
            PRINT STR('-',130)

        END
    END
RETURN
*===============================================================
GET.DR:
*=======
    T.SEL = "SELECT ":FN.DR:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.DR,KEY.LIST<SELECTED>,R.DR,F.DR,E3)

        WS.DR.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.DR<TF.DR.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.DR.DATE = '20':R.DR<TF.DR.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 448 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.DR<TF.DR.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.DR<TF.DR.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.DR.DATE = '20':WS.DATE.TIME[1,6]
        END


        WS.DR.DATE = FMT(WS.DR.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.DR.ID
        XX1<1,1>[34,20] = WS.DR.DATE
        XX1<1,1>[60,20] = 'DRAWINGS'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.DR.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.DR.H,KEY.LIST<SELECTED>,R.DR.H,F.DR.H,E3)

            WS.DR.ID   = KEY.LIST<SELECTED>

            FINDSTR WS.EMP.LOGIN IN R.DR.H<TF.DR.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.DR.DATE = '20':R.DR.H<TF.DR.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 478 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.DR.H<TF.DR.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.DR.H<TF.DR.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.DR.DATE = '20':WS.DATE.TIME[1,6]
            END


            WS.DR.DATE = FMT(WS.DR.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.DR.ID
            XX1<1,1>[34,20] = WS.DR.DATE
            XX1<1,1>[60,20] = 'DRAWINGS'

            PRINT XX1<1,1>
            PRINT STR('-',130)

        END
    END
RETURN
*===============================================================
GET.MM:
*=======
    T.SEL = "SELECT ":FN.MM:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.MM,KEY.LIST<SELECTED>,R.MM,F.MM,E3)

        WS.MM.ID   = KEY.LIST<SELECTED>
        FINDSTR WS.EMP.LOGIN IN R.MM<MM.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.MM.DATE = '20':R.MM<MM.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 511 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.MM<MM.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.MM<MM.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.MM.DATE = '20':WS.DATE.TIME[1,6]
        END

        WS.MM.DATE = FMT(WS.MM.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.MM.ID
        XX1<1,1>[34,20] = WS.MM.DATE
        XX1<1,1>[60,20] = 'MONEY.MARKET'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.MM.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.MM.H,KEY.LIST<SELECTED>,R.MM.H,F.MM.H,E3)

            WS.MM.ID   = KEY.LIST<SELECTED>

            FINDSTR WS.EMP.LOGIN IN R.MM.H<MM.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.MM.DATE = '20':R.MM.H<MM.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 540 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.MM.H<MM.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.MM.H<MM.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.MM.DATE = '20':WS.DATE.TIME[1,6]
            END

            WS.MM.DATE = FMT(WS.MM.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.MM.ID
            XX1<1,1>[34,20] = WS.MM.DATE
            XX1<1,1>[60,20] = 'MONEY.MARKET'

            PRINT XX1<1,1>
            PRINT STR('-',130)

        END
    END
RETURN
*===============================================================
GET.FX:
*=======
    T.SEL = "SELECT ":FN.FX:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.FX,KEY.LIST<SELECTED>,R.FX,F.FX,E3)

        WS.FX.ID   = KEY.LIST<SELECTED>
        FINDSTR WS.EMP.LOGIN IN R.FX<FX.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.FX.DATE = '20':R.FX<FX.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 572 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.FX<FX.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.FX<FX.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.FX.DATE = '20':WS.DATE.TIME[1,6]
        END

        WS.FX.DATE = FMT(WS.FX.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.FX.ID
        XX1<1,1>[34,20] = WS.FX.DATE
        XX1<1,1>[60,20] = 'FOREX'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.FX.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.FX.H,KEY.LIST<SELECTED>,R.FX.H,F.FX.H,E3)

            WS.FX.ID   = KEY.LIST<SELECTED>

            FINDSTR WS.EMP.LOGIN IN R.FX.H<FX.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.FX.DATE = '20':R.FX.H<FX.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 601 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.FX.H<FX.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.FX.H<FX.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.FX.DATE = '20':WS.DATE.TIME[1,6]
            END

            WS.FX.DATE = FMT(WS.FX.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.FX.ID
            XX1<1,1>[34,20] = WS.FX.DATE
            XX1<1,1>[60,20] = 'FOREX'

            PRINT XX1<1,1>
            PRINT STR('-',130)

        END
    END
RETURN
*===============================================================
GET.CU:
*=======
    T.SEL = "SELECT ":FN.CU:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY DATE.TIME"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.CU,KEY.LIST<SELECTED>,R.CU,F.CU,E3)

        WS.CU.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.CU<EB.CUS.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.CU.DATE = '20':R.CU<EB.CUS.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 634 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.CU<EB.CUS.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.CU<EB.CUS.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.CU.DATE = '20':WS.DATE.TIME[1,6]
        END

        WS.CU.DATE = FMT(WS.CU.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.CU.ID
        XX1<1,1>[34,20] = WS.CU.DATE
        XX1<1,1>[60,20] = 'CUSTOMER'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.CU.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY DATE.TIME"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.CU.H,KEY.LIST<SELECTED>,R.CU.H,F.CU.H,E3)

            WS.CU.ID   = KEY.LIST<SELECTED>

            FINDSTR WS.EMP.LOGIN IN R.CU.H<EB.CUS.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.CU.DATE = '20':R.CU.H<EB.CUS.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 663 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.CU.H<EB.CUS.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.CU.H<EB.CUS.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.CU.DATE = '20':WS.DATE.TIME[1,6]
            END

            WS.CU.DATE = FMT(WS.CU.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.CU.ID
            XX1<1,1>[34,20] = WS.CU.DATE
            XX1<1,1>[60,20] = 'CUSTOMER'
            PRINT XX1<1,1>
            PRINT STR('-',130)

        END
    END
RETURN
*===============================================================
GET.AC:
*=======
    T.SEL = "SELECT ":FN.AC:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY DATE.TIME"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.AC,KEY.LIST<SELECTED>,R.AC,F.AC,E3)
        WS.AC.ID   = KEY.LIST<SELECTED>

        FINDSTR WS.EMP.LOGIN IN R.AC<AC.INPUTTER> SETTING FM1,VM1,SM1 THEN
            WS.AC.DATE = '20':R.AC<AC.DATE.TIME,VM1>[1,6]
        END ELSE
*Line [ 694 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.DATE.TIME.COUNT = DCOUNT(R.AC<AC.DATE.TIME>,@VM)
            WS.DATE.TIME       = R.AC<AC.DATE.TIME><1,WS.DATE.TIME.COUNT>
            WS.AC.DATE = '20':WS.DATE.TIME[1,6]
        END

        WS.AC.DATE = FMT(WS.AC.DATE,"####/##/##")

        XX1 = SPACE(132)
        XX1<1,1>[1,20]  = WS.AC.ID
        XX1<1,1>[34,20] = WS.AC.DATE
        XX1<1,1>[60,20] = 'ACCOUNT'

        PRINT XX1<1,1>
        PRINT STR('-',130)

    END ELSE

        T.SEL = "SELECT ":FN.AC.H:" WITH INPUTTER LIKE ":EMP.NO.SEL:" OR AUTHORISER LIKE ":EMP.NO.SEL:" BY DATE.TIME"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.AC.H,KEY.LIST<SELECTED>,R.AC.H,F.AC.H,E3)
            WS.AC.ID   = KEY.LIST<SELECTED>

            FINDSTR WS.EMP.LOGIN IN R.AC.H<AC.INPUTTER> SETTING FM1,VM1,SM1 THEN
                WS.AC.DATE = '20':R.AC.H<AC.DATE.TIME,VM1>[1,6]
            END ELSE
*Line [ 722 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.DATE.TIME.COUNT = DCOUNT(R.AC.H<AC.DATE.TIME>,@VM)
                WS.DATE.TIME       = R.AC.H<AC.DATE.TIME><1,WS.DATE.TIME.COUNT>
                WS.AC.DATE = '20':WS.DATE.TIME[1,6]

            END
            WS.AC.DATE = FMT(WS.AC.DATE,"####/##/##")

            XX1 = SPACE(132)
            XX1<1,1>[1,20]  = WS.AC.ID
            XX1<1,1>[34,20] = WS.AC.DATE
            XX1<1,1>[60,20] = 'ACCOUNT'

            PRINT XX1<1,1>
            PRINT STR('-',130)

        END
    END
RETURN
*===============================================================
PRINT.HEAD:
*---------

*Line [ 745 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,SCB.CO,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    TD1 = TODAY[1,4]
    TD2 = TODAY[5,2]
    TD3 = TODAY[7,2]
    TD = TD1:"/":TD2:"/":TD3

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ��� ���� ��� ������ ������ ���  : ":WS.EMP.NO
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������ : ":WS.USER.NAME:SPACE(20):"����� ����� �������� : ":WS.END.DATE
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ��� ���� ��� ������: ":WS.LAST.DATE:SPACE(5):WS.LAST.TIME
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(20):"����� �������":SPACE(20):"��� �����"

    PR.HD :="'L'":STR('_',130)
    HEADING PR.HD
    PRINT
RETURN
*==============================================================
END
