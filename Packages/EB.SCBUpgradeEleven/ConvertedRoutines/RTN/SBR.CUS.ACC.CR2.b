* @ValidationCode : Mjo2Mzk4MTI0NzU6Q3AxMjUyOjE2NDQ5MjUwNjUzODA6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
****************MAHMOUD ***********************

SUBROUTINE SBR.CUS.ACC.CR2
***    PROGRAM    SBR.CUS.ACC.CR2
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT     ;*IC.ACI.
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY          ;*IC.ACT.
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    CC = "A"
    CUST = ''
    FRDT = ''
    TODT = ''
    CUTXT = "INPUT CUSTOMER NUMBER: "
    FRTXT = "�� ��� :"
    TOTXT = "��� ��� :"
*    CALL TXTINP(CUTXT, 8, 22, 10,CC)
*    CUST = COMI
    CALL TXTINP(FRTXT, 8, 22, 10,CC)
    FRDT = COMI
    CALL TXTINP(TOTXT, 8, 22, 10,CC)
    TODT = COMI

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 68 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    PRINT STR('_',120)
    PRINT STR('-',30):" ����� ������� ":STR('-',30)
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.NAME='SBR.CUS.ACC.CR2'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    CUST.NAME = ''
    Y.OPEN.BAL = 0
    Y.LAST.BAL = 0
    Y.TOT.DR = 0
    Y.TOT.CR = 0
    Y.MAX.DR.BAL = 0
    Y.MIN.DR.BAL = 0
    Y.COMM   = 0
    K = 0
    KK= 0
    PRVBAL = 0
    PRVBAL1= 0
    BAL1 = 0
    BAL2 = 0
    DB.MOV = 0
    DB.MOV2= 0
    CR.MOV = 0
    CR.MOV2= 0
    BAL.AC = 0
    MAXBAL = 0
    MINBAL = 0
    AVRBAL = 0
    TTTBAL = 0
    BB = 0
    ACC.NX = ''
    CUS.NX = ''
    JJ = 0
    XX = SPACE(130)
    XX1= SPACE(130)
RETURN
*===============================================================
CALLDB:
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    FN.ACC     = 'FBNK.ACCOUNT' ; F.ACC = ''
    FN.ACI     = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    FN.ACI2    = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI2= ''
    FN.ACC.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACC.ACT = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.ACI,F.ACI)
    CALL OPF(FN.ACI2,F.ACI2)
    CALL OPF(FN.ACC.ACT,F.ACC.ACT)
    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    TD1 = TODAY
*************************************************************************
*========================================================================
    SEL.CMD  ="SELECT ":FN.ACI:" WITH INTEREST.DAY.BASIS NE 'NONE'"
    SEL.CMD :=" AND CR.INT.RATE NE 0"
    SEL.CMD :=" AND CO.CODE EQ ":COMP
    SEL.CMD :=" BY @ID"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NUMREC,RTNCD)
    LOOP
        REMOVE Y.ACI.ID FROM SELLIST SETTING SUBVAL
    WHILE Y.ACI.ID:SUBVAL
        KK++
        CALL F.READ(FN.ACI,Y.ACI.ID,R.ACI,F.ACI,Y.ACI.ERR)
        Y.ACC.ID = FIELD(Y.ACI.ID,'-',1)
        CC.SEL  = "SELECT ":FN.ACI2:" WITH @ID LIKE ":Y.ACC.ID:"-... BY @ID"
        CALL EB.READLIST(CC.SEL,SELREC,'',NUMSEL,RTNRR)
        IF NUMSEL THEN
            CALL F.READ(FN.ACI2,SELREC<NUMSEL>,R.ACI2,F.ACI2,ACI.ER2)
            INT.BAS = R.ACI2<IC.ACI.INTEREST.DAY.BASIS>
            INT.RET = R.ACI2<IC.ACI.CR.INT.RATE>
        END ELSE
            INT.BAS = ''
            INT.RET = ''
        END
        IF INT.BAS EQ 'NONE' OR INT.RET EQ 0 THEN
            NULL
        END ELSE
            IF Y.ACC.ID NE ACC.NX THEN
                CALL F.READ(FN.ACC,Y.ACC.ID,R.ACC,F.ACC,Y.ACC.ERR)
                Y.CURR =R.ACC<AC.CURRENCY>
                Y.CAT  =R.ACC<AC.CATEGORY>
                CUS.ID =R.ACC<AC.CUSTOMER>
*************************************
                IF Y.CAT EQ 1001 OR Y.CAT EQ 2001 THEN
*Line [ 159 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,CUST.LCL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.LCL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                    CUST.NAME  = CUST.LCL<1,CULR.ARABIC.NAME>
                    CUST.NAME2 = CUST.LCL<1,CULR.ARABIC.NAME.2>
                    XX1<1,KK>[1,100] =CUS.ID:"  ":CUST.NAME:" ":CUST.NAME2
                    IF CUS.ID NE CUS.NX THEN
                        PRINT STR('-',120)
                        PRINT XX1<1,KK>
                        PRINT SPACE(120)
                    END
*************************************
                    IF TODT[1,6] GT FRDT[1,6] THEN
                        M.SEL  ="SELECT ":FN.ACC.ACT:" WITH @ID GE ":Y.ACC.ID:"-":FRDT[1,6]
                        M.SEL :=" AND @ID LE ":Y.ACC.ID:"-":TODT[1,6]
                        M.SEL :=" BY @ID"
                    END ELSE
                        M.SEL  ="SELECT ":FN.ACC.ACT:" WITH @ID EQ ":Y.ACC.ID:"-":FRDT[1,6]
                    END
                    CALL EB.READLIST(M.SEL,KEY.LIST,'',SELECTED,ER.MSG)
                    IF SELECTED THEN
                        K++
                        FOR I = 1 TO SELECTED
                            JJ++
                            MINBAL = 0
                            MAXBAL = 0
                            AVRBAL = 0
                            TTTBAL = 0
                            HXB = 0
                            DAY.AFT  = 0
                            DAY.REC  = 0
                            DAYS.BAL = 0
                            BAL.DAY  = 0
                            ACT.DAYS = 0
                            EOMBAL   = 0
                            TOT.BAL.DAYS = ''

                            CALL F.READ(FN.ACC.ACT,KEY.LIST<I>,R.ACC.ACT,F.ACC.ACT,Y.ERR1)
                            ACMNTH = FIELD(KEY.LIST<I>,'-',2)
                            DB.MOV = R.ACC.ACT<IC.ACT.TURNOVER.DEBIT>
                            CR.MOV = R.ACC.ACT<IC.ACT.TURNOVER.CREDIT>
                            BAL.AC = R.ACC.ACT<IC.ACT.BALANCE>
                            DAY.NO = R.ACC.ACT<IC.ACT.DAY.NO>
*Line [ 199 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            BB     = DCOUNT(BAL.AC,@VM)
                            LDAYM = ACMNTH:'01'
                            CALL LAST.DAY(LDAYM)
                            NXTDAY = LDAYM
                            CALL CDT("",NXTDAY,'+1W')
                            NO.OF.DAYS = LDAYM[2]
                            ACT.DAYS   = NO.OF.DAYS - DAY.NO<1,1>
*EOMBAL     = R.ACC.ACT<IC.ACT.BALANCE,BB>
                            CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,NXTDAY,NXTDAY,ID.LIST,EOMBAL,ER)
                            IF BB GT 1 THEN
                                FOR HX = 1 TO BB
                                    IF HX NE BB THEN
                                        HXB = HX + 1
                                        DAY.AFT  = DAY.NO<1,HXB>
                                        DAY.REC  = DAY.NO<1,HX>
                                        DAYS.BAL = DAY.AFT - DAY.REC
                                        BAL.DAY  = BAL.AC<1,HX>
                                        TOT.BAL.DAYS = BAL.DAY * DAYS.BAL
                                    END ELSE
                                        TOT.BAL.DAYS = BAL.AC<1,HX> * ( NO.OF.DAYS - DAY.NO<1,HX> )
                                    END
                                    TTTBAL += TOT.BAL.DAYS
                                NEXT HX
                            END ELSE
                                TTTBAL = BAL.AC<1,BB> * ACT.DAYS
                            END

                            IF BAL.AC GE 0 THEN
                                MAXBAL = MAXIMUM(BAL.AC)
                                MINBAL = MINIMUM(BAL.AC)
                                IF ACT.DAYS NE '0' AND ACT.DAYS NE '' AND NUM(ACT.DAYS) THEN
                                    AVRBAL = TTTBAL / ACT.DAYS
                                END ELSE
                                    AVRBAL = 0
                                END
                            END ELSE
                                MAXBAL = 0
                                MINBAL = 0
                            END
                            GOSUB PRINTLN
                        NEXT I
                    END ELSE
                        ZZ = 0
                    END
                END
            END
        END
        ACC.NX = Y.ACC.ID
        CUS.NX = CUS.ID
    REPEAT
RETURN
*...................................
PRINTLN:
    CRT Y.ACC.ID:"-":ACMNTH
    XX  = SPACE(120)
    XX<1,K>[1,16]   = Y.ACC.ID
    XX<1,K>[20,10]  = FMT(ACMNTH,"####/##")
    XX<1,K>[35,15]  = FMT(MAXBAL,"L2,")
    XX<1,K>[52,15]  = FMT(MINBAL,"L2,")
    XX<1,K>[69,15]  = FMT(AVRBAL,"L2,")
    XX<1,K>[86,15]  = FMT(EOMBAL,"L2,")
*    XX<1,K>[86,15]  = MINBAL2
*    XX<1,K>[103,15] = TOTAL.INT
*    XX<1,K>[120,15] = TOTAL.COM
    PRINT XX<1,K>
RETURN
***********************************************************
PRINT.HEAD:
*---------
*Line [ 276 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    TD = TODAY
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")
    FRDTM  = FMT(FRDT,"####/##")
    TODTM  = FMT(TODT,"####/##")
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(30):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(35):"��� ������ : ":"'P'"
    PR.HD :="'L'":REPORT.NAME
    PR.HD :="'L'":SPACE(10):"�������� ������� ������� �������� ���� ����"
    PR.HD :="'L'":SPACE(10):"�� ��� ":FRDTM:"   ��� ��� ":TODTM
    PR.HD :="'L'":SPACE(8):STR('_',40)
    PR.HD :="'L'":""
    PR.HD :="'L'":"��� ������":SPACE(7):"�����":SPACE(5):"���� ���� ����":SPACE(6):"��� ���� ����":SPACE(5):"����� ������":SPACE(10):"���� ����� �����"
    PR.HD :="'L'":SPACE(66):"���� �����"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
