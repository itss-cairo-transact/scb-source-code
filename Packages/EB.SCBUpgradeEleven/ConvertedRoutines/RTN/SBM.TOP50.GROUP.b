* @ValidationCode : MjotMzQ3NDI3NzA2OkNwMTI1MjoxNjQwODUyMzYyNTI2OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 10:19:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBM.TOP50.GROUP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TOP50
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TOP50.GROUP

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.TOP = 'F.SCB.TOP50'      ; F.TOP = ''
    CALL OPF(FN.TOP,F.TOP)

    FN.TP.G = 'F.SCB.TOP50.GROUP' ; F.TP.G =''
    CALL OPF(FN.TP.G,F.TP.G)



    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>


    TOT.AMT.CHF = 0
    TOT.AMT.DKK = 0
    TOT.AMT.SEK = 0
    TOT.AMT.NOK = 0
    TOT.AMT.ITL = 0

    TOT.AMT.INR = 0
    TOT.AMT.AUD = 0
    TOT.AMT.CAD = 0
    TOT.AMT.GBP = 0
    TOT.AMT.KWD = 0
    TOT.AMT.AED = 0

    TOT.AMT.USD = 0
    TOT.AMT.EUR = 0
    TOT.AMT.JPY = 0

    TOT.AMT.SAR = 0
    TOT.AMT.EGP = 0
    TOT.AMT.FCY = 0
    TOTAL.ALL = 0
    TOTAL.FCY =0

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
*============================================================
    T.SEL.CONT    = "SELECT ":FN.TP.G:" BY @ID"
    CALL EB.READLIST(T.SEL.CONT,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            TP.ID     = KEY.LIST<HH>
            CALL F.READ(FN.TOP,TP.ID,R.TOP,F.TOP,ETEXT.TOP)
            R.TOP<TP.GROUP.ID> = TP.ID
            CALL F.WRITE(FN.TOP,TP.ID,R.TOP)
            CALL JOURNAL.UPDATE(TP.ID)
**********************************************************
            TP.G.ID     = KEY.LIST<HH>
            CALL F.READ(FN.TP.G,TP.G.ID,R.TP.G,F.TP.G,E1)
*Line [ 95 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.CONT = DCOUNT(R.TP.G<GR.CUSTOMER.ID>,@VM)
            FOR I = 1 TO DECOUNT.CONT
                TP.ID2     = R.TP.G<GR.CUSTOMER.ID,I>
                CALL F.READ(FN.TOP,TP.ID2,R.TOP,F.TOP,ETEXT.TOP)
                R.TOP<TP.GROUP.ID> = TP.ID
                CALL F.WRITE(FN.TOP,TP.ID2,R.TOP)
                CALL JOURNAL.UPDATE(TP.ID2)
            NEXT I
        NEXT HH
    END
************************************************************************
    T.SEL.GROUP    = "SELECT ":FN.TOP:" WITH GROUP.ID NE '' BY GROUP.ID BY @ID"
    CALL EB.READLIST(T.SEL.GROUP,KEY.LIST,"",SELECTED1,ER.MSG)

    IF SELECTED THEN
        FOR KK = 1 TO SELECTED1
            CALL F.READ(FN.TOP,KEY.LIST<KK>,R.TOP,F.TOP,ETEXT)
            CALL F.READ(FN.TOP,KEY.LIST<KK+1>,R.TOP2,F.TOP,ETEXT)
            GR.ID  = R.TOP<TP.GROUP.ID>
            GR.ID2 = R.TOP2<TP.GROUP.ID>
*            IF GR.ID EQ '50300570' THEN DEBUG
            IF GR.ID EQ GR.ID2 THEN

                TOT.AMT.CHF += R.TOP<TP.TOTAL.CHF>
                TOT.AMT.DKK += R.TOP<TP.TOTAL.DKK>
                TOT.AMT.SEK += R.TOP<TP.TOTAL.SEK>
                TOT.AMT.NOK += R.TOP<TP.TOTAL.NOK>
                TOT.AMT.ITL += R.TOP<TP.TOTAL.ITL>

                TOT.AMT.INR += R.TOP<TP.TOTAL.INR>
                TOT.AMT.AUD += R.TOP<TP.TOTAL.AUD>
                TOT.AMT.CAD += R.TOP<TP.TOTAL.CAD>
                TOT.AMT.GBP += R.TOP<TP.TOTAL.GBP>
                TOT.AMT.KWD += R.TOP<TP.TOTAL.KWD>
                TOT.AMT.AED += R.TOP<TP.TOTAL.AED>

                TOT.AMT.USD += R.TOP<TP.TOTAL.USD>
                TOT.AMT.EUR += R.TOP<TP.TOTAL.EUR>
                TOT.AMT.JPY += R.TOP<TP.TOTAL.JPY>

                TOT.AMT.SAR += R.TOP<TP.TOTAL.SAR>
                TOT.AMT.EGP += R.TOP<TP.TOTAL.EGP.AMT>
                TOT.AMT.FCY += R.TOP<TP.TOTAL.FCY.AMT>
                TOTAL.ALL      = TOT.AMT.EGP + TOT.AMT.FCY
            END
            IF GR.ID NE GR.ID2 THEN
                TOTAL.CHF =  TOT.AMT.CHF + R.TOP<TP.TOTAL.CHF>
                TOTAL.DKK =  TOT.AMT.DKK + R.TOP<TP.TOTAL.DKK>
                TOTAL.SEK =  TOT.AMT.SEK + R.TOP<TP.TOTAL.SEK>
                TOTAL.NOK =  TOT.AMT.NOK + R.TOP<TP.TOTAL.NOK>
                TOTAL.ITL =  TOT.AMT.ITL + R.TOP<TP.TOTAL.ITL>
                TOTAL.INR =  TOT.AMT.INR + R.TOP<TP.TOTAL.INR>
                TOTAL.AUD =  TOT.AMT.AUD + R.TOP<TP.TOTAL.AUD>
                TOTAL.CAD =  TOT.AMT.CAD + R.TOP<TP.TOTAL.CAD>
                TOTAL.GBP =  TOT.AMT.GBP + R.TOP<TP.TOTAL.GBP>
                TOTAL.KWD =  TOT.AMT.KWD + R.TOP<TP.TOTAL.KWD>
                TOTAL.AED =  TOT.AMT.AED + R.TOP<TP.TOTAL.AED>
                TOTAL.USD =  TOT.AMT.USD + R.TOP<TP.TOTAL.USD>
                TOTAL.EUR =  TOT.AMT.EUR + R.TOP<TP.TOTAL.EUR>
                TOTAL.JPY =  TOT.AMT.JPY + R.TOP<TP.TOTAL.JPY>
                TOTAL.SAR =  TOT.AMT.SAR + R.TOP<TP.TOTAL.SAR>

                TOTAL.EGP = TOT.AMT.EGP + R.TOP<TP.TOTAL.EGP.AMT>
                TOTAL.FCY = TOT.AMT.FCY + R.TOP<TP.TOTAL.FCY.AMT>
                TOTAL.ALL   = TOTAL.EGP + TOTAL.FCY
*  TOTAL.LCY = TOT.AMT + R.TOP<TP.TOTAL.AMT>
                TP.ID     = GR.ID : "." : "G"
                CALL F.READ(FN.TOP,TP.ID,R.TOP,F.TOP,ETEXT.TOP)

*   R.TOP<TP.TOTAL.AMT> = TOTAL.LCY

                R.TOP<TP.TOTAL.CHF>  = TOTAL.CHF
                R.TOP<TP.TOTAL.DKK>  = TOTAL.DKK
                R.TOP<TP.TOTAL.SEK>  = TOTAL.SEK
                R.TOP<TP.TOTAL.NOK>  = TOTAL.NOK
                R.TOP<TP.TOTAL.ITL>  = TOTAL.ITL
                R.TOP<TP.TOTAL.INR>  = TOTAL.INR
                R.TOP<TP.TOTAL.AUD>  = TOTAL.AUD
                R.TOP<TP.TOTAL.CAD>  = TOTAL.CAD
                R.TOP<TP.TOTAL.GBP>  = TOTAL.GBP
                R.TOP<TP.TOTAL.KWD>  = TOTAL.KWD
                R.TOP<TP.TOTAL.AED>  = TOTAL.AED
                R.TOP<TP.TOTAL.USD>  = TOTAL.USD
                R.TOP<TP.TOTAL.EUR>  = TOTAL.EUR
                R.TOP<TP.TOTAL.JPY>  = TOTAL.JPY
                R.TOP<TP.TOTAL.SAR>  = TOTAL.SAR
                R.TOP<TP.TOTAL.EGP.AMT>= TOTAL.EGP
                R.TOP<TP.TOTAL.FCY.AMT>=TOTAL.FCY
                R.TOP<TP.TOTAL.AMT>    =TOTAL.ALL
                CALL F.WRITE(FN.TOP,TP.ID,R.TOP)
                CALL JOURNAL.UPDATE(TP.ID)
*  TOT.AMT = 0
                TOT.AMT.CHF = 0
                TOT.AMT.DKK = 0
                TOT.AMT.SEK = 0
                TOT.AMT.NOK = 0
                TOT.AMT.ITL = 0

                TOT.AMT.INR = 0
                TOT.AMT.AUD = 0
                TOT.AMT.CAD = 0
                TOT.AMT.GBP = 0
                TOT.AMT.KWD = 0
                TOT.AMT.AED = 0

                TOT.AMT.USD = 0
                TOT.AMT.EUR = 0
                TOT.AMT.JPY = 0
                TOT.AMT.SAR = 0
                TOT.AMT.EGP = 0
                TOT.AMT.FCY = 0

                TOTAL.CHF = 0
                TOTAL.DKK = 0
                TOTAL.SEK = 0
                TOTAL.NOK = 0
                TOTAL.ITL = 0
                TOTAL.INR = 0
                TOTAL.AUD = 0
                TOTAL.CAD = 0
                TOTAL.GBP = 0
                TOTAL.KWD = 0
                TOTAL.AED = 0
                TOTAL.USD = 0
                TOTAL.EUR = 0
                TOTAL.JPY = 0
                TOTAL.SAR = 0
                TOTAL.EGP = 0
                TOTAL.FCY = 0
                TOTAL.ALL   =0
                R.TOP<TP.TOTAL.CHF>  = 0
                R.TOP<TP.TOTAL.DKK>   = 0
                R.TOP<TP.TOTAL.SEK>   = 0
                R.TOP<TP.TOTAL.NOK>   = 0
                R.TOP<TP.TOTAL.ITL>   = 0
                R.TOP<TP.TOTAL.INR>   = 0
                R.TOP<TP.TOTAL.AUD>   = 0
                R.TOP<TP.TOTAL.CAD>   = 0
                R.TOP<TP.TOTAL.GBP>   = 0
                R.TOP<TP.TOTAL.KWD>   = 0
                R.TOP<TP.TOTAL.AED>   = 0
                R.TOP<TP.TOTAL.USD>   = 0
                R.TOP<TP.TOTAL.EUR>   = 0
                R.TOP<TP.TOTAL.JPY>    = 0
                R.TOP<TP.TOTAL.SAR>    = 0
                R.TOP<TP.TOTAL.EGP.AMT>= 0
                R.TOP<TP.TOTAL.FCY.AMT> = 0
                R.TOP<TP.TOTAL.AMT> = 0
            END
        NEXT KK
    END
RETURN
