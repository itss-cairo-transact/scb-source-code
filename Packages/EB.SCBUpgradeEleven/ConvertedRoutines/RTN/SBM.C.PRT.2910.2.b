* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.2910.2
*    PROGRAM  SBM.C.PRT.2910.2
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
*** PRINT CENTRAL BANK REPORT 2910 PAGE 1
*-----------------------------------------------
    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""
    FN.COMP = "F.COMPANY"
    F.COMP = ""
*--------------------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""

    WS.FLAG.PRT = 0
    WS.BR = 0
    WS.BRX = 0
    WS.ARRY.RAW = ""
    WS.ARRY.COL = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    FLAG.FRST = 0
    WS.T = ""
    WS.1.LE = "0"
    WS.2.LE = "0"
    WS.3.LE = "0"
    WS.1.EQV = "0"
    WS.2.EQV = "0"
    WS.3.EQV = "0"
    WS.DPST = "0"
    WS.DPST1 = "0"
    WS.CER = "0"
    WS.INDSTRYA = ""
    WS.INDSTRY  = ""
    WS.HD.T   = "������� �������� ��������� ����������"
    WS.HD.T1  = "� ������� �������� �������� ������� ��������"
    WS.HD.TA  = "����� ��� 2910"
    WS.HD.T2A = "����  1"
    WS.HD.T3  = "������ ����� ����"
    WS.PRG.1  = "SBM.C.PRT.2910.2"

    WS.HD1  = "���� ������� "
    WS.HD1A = "������ �����"
    WS.HD1B = "���� �������"
    WS.HD1C = "������ �������"
    WS.HD1D = "������ �������"
    WS.HD1E = "��������"
    WS.HD2  = "�����"
    WS.HD2B = "�����"

********    ARRAY1 = ""
********    ARRAY

    DIM ARRAY1(38,7)

    ARRAY1(1,1) = "�������"
    ARRAY1(1,2) = "D"
    ARRAY1(1,3) = "0"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"
    ARRAY1(1,6) = "0"
    ARRAY1(1,7) = "0"

    ARRAY1(2,1) = "����������"
    ARRAY1(2,2) = "D"
    ARRAY1(2,3) = "0"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = "0"
    ARRAY1(2,7) = "0"


    ARRAY1(3,1) = "����� ������"
    ARRAY1(3,2) = "H"
    ARRAY1(3,3) = "0"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"

    ARRAY1(4,1) = "�������"
    ARRAY1(4,2) = "D"
    ARRAY1(4,3) = "0"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"

    ARRAY1(5,1) = "�������"
    ARRAY1(5,2) = "D"
    ARRAY1(5,3) = "0"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = "0"
    ARRAY1(5,7) = "0"

    ARRAY1(6,1) = "��� �����"
    ARRAY1(6,2) = "D"
    ARRAY1(6,3) = "0"
    ARRAY1(6,4) = "0"
    ARRAY1(6,5) = "0"
    ARRAY1(6,6) = "0"
    ARRAY1(6,7) = "0"

    ARRAY1(7,1) = "��������"
    ARRAY1(7,2) = "D"
    ARRAY1(7,3) = "0"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"
    ARRAY1(7,6) = "0"
    ARRAY1(7,7) = "0"

    ARRAY1(8,1) = "�����"
    ARRAY1(8,2) = "D"
    ARRAY1(8,3) = "0"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = "0"
    ARRAY1(8,7) = "0"

    ARRAY1(9,1) = "�������"
    ARRAY1(9,2) = "D"
    ARRAY1(9,3) = "0"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = "0"
    ARRAY1(9,7) = "0"

    ARRAY1(10,1) = "��������"
    ARRAY1(10,2) = "D"
    ARRAY1(10,3) = "0"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = "0"
    ARRAY1(10,7) = "0"

    ARRAY1(11,1) = "���������"
    ARRAY1(11,2) = "D"
    ARRAY1(11,3) = "0"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = "0"
    ARRAY1(11,7) = "0"

    ARRAY1(12,1) = "������ ����� ������"
    ARRAY1(12,2) = "T"
    ARRAY1(12,3) = "0"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = "0"
    ARRAY1(12,7) = "0"

    ARRAY1(13,1) = "����� ������"
    ARRAY1(13,2) = "H"
    ARRAY1(13,3) = "0"
    ARRAY1(13,4) = "0"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = "0"
    ARRAY1(13,7) = "0"

    ARRAY1(14,1) = "������"
    ARRAY1(14,2) = "D"
    ARRAY1(14,3) = "0"
    ARRAY1(14,4) = "0"
    ARRAY1(14,5) = "0"
    ARRAY1(14,6) = "0"
    ARRAY1(14,7) = "0"

    ARRAY1(15,1) = "������"
    ARRAY1(15,2) = "D"
    ARRAY1(15,3) = "0"
    ARRAY1(15,4) = "0"
    ARRAY1(15,5) = "0"
    ARRAY1(15,6) = "0"
    ARRAY1(15,7) = "0"

    ARRAY1(16,1) = "��� ����"
    ARRAY1(16,2) = "D"
    ARRAY1(16,3) = "0"
    ARRAY1(16,4) = "0"
    ARRAY1(16,5) = "0"
    ARRAY1(16,6) = "0"
    ARRAY1(16,7) = "0"

    ARRAY1(17,1) = "������"
    ARRAY1(17,2) = "D"
    ARRAY1(17,3) = "0"
    ARRAY1(17,4) = "0"
    ARRAY1(17,5) = "0"
    ARRAY1(17,6) = "0"
    ARRAY1(17,7) = "0"

    ARRAY1(18,1) = "�����"
    ARRAY1(18,2) = "D"
    ARRAY1(18,3) = "0"
    ARRAY1(18,4) = "0"
    ARRAY1(18,5) = "0"
    ARRAY1(18,6) = "0"
    ARRAY1(18,7) = "0"

    ARRAY1(19,1) = "�����"
    ARRAY1(19,2) = "D"
    ARRAY1(19,3) = "0"
    ARRAY1(19,4) = "0"
    ARRAY1(19,5) = "0"
    ARRAY1(19,6) = "0"
    ARRAY1(19,7) = "0"

    ARRAY1(20,1) = "��� "
    ARRAY1(20,2) = "D"
    ARRAY1(20,3) = "0"
    ARRAY1(20,4) = "0"
    ARRAY1(20,5) = "0"
    ARRAY1(20,6) = "0"
    ARRAY1(20,7) = "0"

    ARRAY1(21,1) = "�����"
    ARRAY1(21,2) = "D"
    ARRAY1(21,3) = "0"
    ARRAY1(21,4) = "0"
    ARRAY1(21,5) = "0"
    ARRAY1(21,6) = "0"
    ARRAY1(21,7) = "0"

    ARRAY1(22,1) = "������ ����� ������"
    ARRAY1(22,2) = "T"
    ARRAY1(22,3) = "0"
    ARRAY1(22,4) = "0"
    ARRAY1(22,5) = "0"
    ARRAY1(22,6) = "0"
    ARRAY1(22,7) = "0"

    ARRAY1(23,1) = "����� ������"
    ARRAY1(23,2) = "H"
    ARRAY1(23,3) = "0"
    ARRAY1(23,4) = "0"
    ARRAY1(23,5) = "0"
    ARRAY1(23,6) = "0"
    ARRAY1(23,7) = "0"

    ARRAY1(24,1) = "������"
    ARRAY1(24,2) = "D"
    ARRAY1(24,3) = "0"
    ARRAY1(24,4) = "0"
    ARRAY1(24,5) = "0"
    ARRAY1(24,6) = "0"
    ARRAY1(24,7) = "0"

    ARRAY1(25,1) = "����������"
    ARRAY1(25,2) = "D"
    ARRAY1(25,3) = "0"
    ARRAY1(25,4) = "0"
    ARRAY1(25,5) = "0"
    ARRAY1(25,6) = "0"
    ARRAY1(25,7) = "0"

    ARRAY1(26,1) = "�������"
    ARRAY1(26,2) = "D"
    ARRAY1(26,3) = "0"
    ARRAY1(26,4) = "0"
    ARRAY1(26,5) = "0"
    ARRAY1(26,6) = "0"
    ARRAY1(26,7) = "0"

    ARRAY1(27,1) = "������ ����� ������"
    ARRAY1(27,2) = "T"
    ARRAY1(27,3) = "0"
    ARRAY1(27,4) = "0"
    ARRAY1(27,5) = "0"
    ARRAY1(27,6) = "0"
    ARRAY1(27,7) = "0"

    ARRAY1(28,1) = "������� ����"
    ARRAY1(28,2) = "H"
    ARRAY1(28,3) = "0"
    ARRAY1(28,4) = "0"
    ARRAY1(28,5) = "0"
    ARRAY1(28,6) = "0"
    ARRAY1(28,7) = "0"

    ARRAY1(29,1) = "���� �����"
    ARRAY1(29,2) = "D"
    ARRAY1(29,3) = "0"
    ARRAY1(29,4) = "0"
    ARRAY1(29,5) = "0"
    ARRAY1(29,6) = "0"
    ARRAY1(29,7) = "0"

    ARRAY1(30,1) = "����� ������"
    ARRAY1(30,2) = "D"
    ARRAY1(30,3) = "0"
    ARRAY1(30,4) = "0"
    ARRAY1(30,5) = "0"
    ARRAY1(30,6) = "0"
    ARRAY1(30,7) = "0"

    ARRAY1(31,1) = "���� �����"
    ARRAY1(31,2) = "D"
    ARRAY1(31,3) = "0"
    ARRAY1(31,4) = "0"
    ARRAY1(31,5) = "0"
    ARRAY1(31,6) = "0"
    ARRAY1(31,7) = "0"

    ARRAY1(32,1) = "���� �����"
    ARRAY1(32,2) = "D"
    ARRAY1(32,3) = "0"
    ARRAY1(32,4) = "0"
    ARRAY1(32,5) = "0"
    ARRAY1(32,6) = "0"
    ARRAY1(32,7) = "0"

    ARRAY1(33,1) = "������ ������"
    ARRAY1(33,2) = "D"
    ARRAY1(33,3) = "0"
    ARRAY1(33,4) = "0"
    ARRAY1(33,5) = "0"
    ARRAY1(33,6) = "0"
    ARRAY1(33,7) = "0"

    ARRAY1(34,1) = "�����"
    ARRAY1(34,2) = "D"
    ARRAY1(34,3) = "0"
    ARRAY1(34,4) = "0"
    ARRAY1(34,5) = "0"
    ARRAY1(34,6) = "0"
    ARRAY1(34,7) = "0"

    ARRAY1(35,1) = "������"
    ARRAY1(35,2) = "D"
    ARRAY1(35,3) = "0"
    ARRAY1(35,4) = "0"
    ARRAY1(35,5) = "0"
    ARRAY1(35,6) = "0"
    ARRAY1(35,7) = "0"


    ARRAY1(36,1) = "������� ����"
    ARRAY1(36,2) = "D"
    ARRAY1(36,3) = "0"
    ARRAY1(36,4) = "0"
    ARRAY1(36,5) = "0"
    ARRAY1(36,6) = "0"
    ARRAY1(36,7) = "0"

    ARRAY1(37,1) = "������ ������� ����"
    ARRAY1(37,2) = "T"
    ARRAY1(37,3) = "0"
    ARRAY1(37,4) = "0"
    ARRAY1(37,5) = "0"
    ARRAY1(37,6) = "0"
    ARRAY1(37,7) = "0"

    ARRAY1(38,1) = "�������� "
    ARRAY1(38,2) = "T"
    ARRAY1(38,3) = "0"
    ARRAY1(38,4) = "0"
    ARRAY1(38,5) = "0"
    ARRAY1(38,6) = "0"
    ARRAY1(38,7) = "0"

********************** ********************************
*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
*------------------------------------------START PROCESSING
    WS.TT = 0
    WS.A3 = 0
    WS.A4 = 0
    WS.A5 = 0
    WS.A6 = 0
    WS.A7 = 0
    WS.A3T = 0
    WS.A4T = 0
    WS.A5T = 0
    WS.A6T = 0
    WS.A7T = 0

    GOSUB A.5000.PRT.HEAD
    GOSUB A.100.PROCESS
    GOSUB A.300.PRNT
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*------------------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE EG001..."
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS
        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)

        WS.NSECTOR = R.CBE<P.CBE.NEW.SECTOR>
        IF WS.NSECTOR EQ 0 THEN
            GOTO AAA
        END
        IF WS.NSECTOR EQ "" THEN
            GOTO AAA
        END

        WS.BRX = R.CBE<P.CBE.BR>

        IF WS.BRX = 99 THEN
            GOTO AAA
        END
A.100.A:
        WS.SEC.3 = WS.NSECTOR
        IF WS.NSECTOR LT 4500   THEN
            WS.SEC.3  = WS.NSECTOR[3]
        END
        WS.AMOUNT   = R.CBE<P.CBE.FACLTY.EQ>
        WS.AMOUNT  += R.CBE<P.CBE.CUR.AC.EQ.DR>
        WS.AMOUNT  += R.CBE<P.CBE.LOANS.EQ.L> 
*-------------------------------------------------------
        GOSUB A.200.PREPBR
        WS.COD.ERR = 0
        GOSUB A.205.PREPSEC
        GOSUB A.210.ACUM
AAA:
    REPEAT
BBB:
    RETURN
*--------------------------------------------------------------
*PREPARE ARRAY NO
A.200.PREPBR:
    IF WS.BRX EQ 19 OR WS.BRX EQ 17 OR WS.BRX EQ 16 THEN
        WS.SUB = 1
    END
    IF WS.BRX EQ 18 OR WS.BRX EQ 24 OR WS.BRX EQ 25 THEN
        WS.SUB = 1
    END
    IF WS.BRX EQ 52 OR WS.BRX EQ 53 THEN
        WS.SUB = 24
    END
    IF WS.BRX EQ 01 OR WS.BRX EQ 02 OR WS.BRX EQ 06 OR WS.BRX EQ 07 THEN
        WS.SUB = 1
        WS.SUBTOT = 0
    END
    IF WS.BRX EQ 09 OR WS.BRX EQ 10 OR WS.BRX EQ 13 THEN
        WS.SUB = 1
    END
    IF WS.BRX EQ 03 OR WS.BRX EQ 04 OR WS.BRX EQ 11 OR WS.BRX EQ 12 THEN
        WS.SUB = 14
    END
    IF WS.BRX  EQ 14 OR WS.BRX EQ 15 THEN
        WS.SUB = 35
    END
    IF WS.BRX EQ 05 THEN
        WS.SUB = 34
    END
    IF WS.BRX EQ 20 OR WS.BRX EQ 21 OR WS.BRX EQ 22 OR WS.BRX EQ 23 THEN
        WS.SUB = 2
    END
    IF WS.BRX EQ 30 THEN
        WS.SUB = 26
    END
    IF WS.BRX EQ 40 THEN
        WS.SUB = 25
    END
    IF WS.BRX EQ 50 THEN
        WS.SUB = 24
    END
    IF WS.BRX EQ 31 THEN
        WS.SUB = 8
    END
    IF WS.BRX EQ 32 THEN
        WS.SUB = 10
    END
    IF WS.BRX EQ 35 THEN
        WS.SUB = 32
    END
    IF WS.BRX EQ 60 THEN
        WS.SUB = 5
    END
    IF WS.BRX EQ 60 THEN
        WS.SUB = 5
    END
    IF WS.BRX EQ 70 THEN
        WS.SUB = 9
    END
    IF WS.BRX EQ 80 THEN
        WS.SUB = 17
    END
    IF WS.BRX EQ 81 THEN
        WS.SUB = 18
    END
    IF WS.BRX EQ 90 THEN
        WS.SUB = 4
    END
    RETURN
*------------------------------------
A.205.PREPSEC:
    IF WS.SEC.3 GE 4500 AND WS.SEC.3 LE 4600 THEN
        WS.SUB1 = 3
        RETURN
    END
    IF WS.SEC.3 GE 110 AND WS.SEC.3 LE 130 THEN
        WS.SUB1 = 4
        RETURN
    END
    IF WS.SEC.3 GE 210 AND WS.SEC.3 LE 260 THEN
        WS.SUB1 = 5
        RETURN
    END
    IF WS.SEC.3 GE 8001 AND WS.SEC.3 LE 8011 THEN
        WS.SUB1 = 5
        RETURN
    END
    IF WS.SEC.3 GE 4650 AND WS.SEC.3 LE 4750 THEN
        WS.SUB1 = 6
        RETURN
    END
    IF WS.SEC.3 EQ 6000  THEN
        WS.SUB1 = 7
        RETURN
    END
    IF WS.SEC.3 EQ 7000  THEN
        WS.SUB1 = 7
        RETURN
    END

    WS.COD.ERR = 9


    RETURN
*---------------------------------------------
****        ARRAY          ������� ��� ��

A.210.ACUM:
    IF  WS.COD.ERR GT  0  THEN
        RETURN
    END
    WS.ACUM.AMT = ARRAY1(WS.SUB,WS.SUB1)
    WS.BEF      = ARRAY1(WS.SUB,WS.SUB1)
    WS.ACUM.AMT = WS.ACUM.AMT + WS.AMOUNT
    ARRAY1(WS.SUB,WS.SUB1) =  WS.ACUM.AMT
    IF WS.SUB1 EQ 5 THEN
    END
    RETURN
*****                                     ARRAY   ������� ��

A.300.PRNT:
    FOR I = 1 TO 38
        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.310.PRT.HD
        END

        IF WS.H.D.T = "T" THEN
            GOSUB A.320.PRT.TOT
        END

        IF WS.H.D.T = "D" THEN
            GOSUB A.330.PRT.DTAL
        END

    NEXT I
    RETURN
A.310.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,25]   = ARRAY1(I,1)
    PRINT XX<1,1>
    XX<1,1>[1,25]   =  STR('-',25)
    PRINT XX<1,1>
    RETURN

A.320.PRT.TOT:
    WS.A3 = ARRAY1(I,3)
    WS.A4 = ARRAY1(I,4)
    WS.A5 = ARRAY1(I,5)
    WS.A6 = ARRAY1(I,6)
    WS.A7 = ARRAY1(I,7)
*-----------------------BANK TOT
    IF I EQ 38 THEN
        WS.A3 = WS.A3T
        WS.A4 = WS.A4T
        WS.A5 = WS.A5T
        WS.A6 = WS.A6T
        WS.A7 = WS.A7T
    END

    WS.TOT = WS.A3 + WS.A4 + WS.A5 + WS.A6 + WS.A7
    XX = SPACE(132)
    XX<1,1>[1,25]   = ARRAY1(I,1)
    XX<1,1>[27,15]   = FMT(WS.A3, "R0,")
    XX<1,1>[43,15]   = FMT(WS.A4, "R0,")
    XX<1,1>[59,15]   = FMT(WS.A5, "R0,")
    XX<1,1>[75,15]   = FMT(WS.A6, "R0,")
    XX<1,1>[91,15]  = FMT(WS.A7, "R0,")
    XX<1,1>[110,15]  = FMT(WS.TOT, "R0,")
    PRINT XX<1,1>

*    XX = SPACE(132)
*    XX<1,1>[1,132]   = STR('-',132)
*    PRINT XX<1,1>
    RETURN
*--------------------------------------------------
A.330.PRT.DTAL:
    WS.A3 = ARRAY1(I,3) / 1000
    WS.A4 = ARRAY1(I,4) / 1000
    WS.A5 = ARRAY1(I,5) / 1000
    WS.A6 = ARRAY1(I,6) / 1000
    WS.A7 = ARRAY1(I,7) / 1000
    WS.TOT = WS.A3 + WS.A4 + WS.A5 + WS.A6 + WS.A7
*----------------------------������� ������� �������
    WS.A3T = WS.A3T + WS.A3
    WS.A4T = WS.A4T + WS.A4
    WS.A5T = WS.A5T + WS.A5
    WS.A6T = WS.A6T + WS.A6
    WS.A7T = WS.A7T + WS.A7
*-----------------�����  ARRAY ����� ���������
*-----------------��� ����� ������ ����� ����� ������
    IF  I GE 4   AND I LE 11  THEN
        WS.SUBTOT = 12
        GOSUB A.400.SUB.TOT
    END
    IF  I GE 14   AND I LE 21  THEN
        WS.SUBTOT = 22
        GOSUB A.400.SUB.TOT
    END
    IF  I GE 24   AND I LE 26  THEN
        WS.SUBTOT = 27
        GOSUB A.400.SUB.TOT
    END
    IF  I GE 29   AND I LE 36  THEN
        WS.SUBTOT = 37
        GOSUB A.400.SUB.TOT
    END
    XX = SPACE(132)
    XX<1,1>[1,25]   = ARRAY1(I,1)
    XX<1,1>[27,15]   = FMT(WS.A3, "R0,")
    XX<1,1>[43,15]   = FMT(WS.A4, "R0,")
    XX<1,1>[59,15]   = FMT(WS.A5, "R0,")
    XX<1,1>[75,15]   = FMT(WS.A6, "R0,")
    XX<1,1>[91,15]  = FMT(WS.A7, "R0,")
    XX<1,1>[110,15]  = FMT(WS.TOT, "R0,")
    PRINT XX<1,1>
*    XX = SPACE(132)
*    XX<1,1>[1,132]   = STR('-',132)
*    PRINT XX<1,1>
    RETURN
*--------------------------------------------------------
A.400.SUB.TOT:
    WS.AMT = ARRAY1(WS.SUBTOT,3)
    WS.AMT = WS.AMT + WS.A3
    ARRAY1(WS.SUBTOT,3) =  WS.AMT

    WS.AMT = ARRAY1(WS.SUBTOT,4)
    WS.AMT = WS.AMT + WS.A4
    ARRAY1(WS.SUBTOT,4) =  WS.AMT

    WS.AMT = ARRAY1(WS.SUBTOT,5)
    WS.AMT = WS.AMT + WS.A5
    ARRAY1(WS.SUBTOT,5) =  WS.AMT

    WS.AMT = ARRAY1(WS.SUBTOT,6)
    WS.AMT = WS.AMT + WS.A6
    ARRAY1(WS.SUBTOT,6) =  WS.AMT

    WS.AMT = ARRAY1(WS.SUBTOT,7)
    WS.AMT = WS.AMT + WS.A7
    ARRAY1(WS.SUBTOT,7) =  WS.AMT

    RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
*    WS.BR.H = WS.BR
*    IF WS.BR LT 10 THEN
*        WS.BR.H = WS.COMP.ID[1]
*    END

    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������":SPACE(21):WS.HD.T:SPACE(15):WS.HD.TA
*    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(15):WS.HD.T1:SPACE(25):WS.HD.T2A
*    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
*    PR.HD :="'L'":SPACE(34):WS.HD.T1:SPACE(15):WS.HD.T2A
    PR.HD :="'L'":SPACE(110):WS.HD.T3
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(25):WS.HD1:"   ":WS.HD1A:"   ":WS.HD1B:"   ":WS.HD1C:"   ":WS.HD1D:"   ":WS.HD1E
    PR.HD :="'L'":SPACE(25):WS.HD2:"   ":SPACE(20):"   ":WS.HD2B
    PR.HD :="'L'":STR('_',132)
    PRINT
    HEADING PR.HD
*   PRINT
    RETURN
*-----------------------------------------------------------------
*A.5100.PRT.SPACE.PAGE:
*    IF FLAG.FRST EQ 0 THEN
*        FLAG.FRST = 1
*        RETURN
*    END
*    PR.HD ="'L'":SPACE(132)
*    PRINT
*    HEADING PR.HD
*    RETURN
END
