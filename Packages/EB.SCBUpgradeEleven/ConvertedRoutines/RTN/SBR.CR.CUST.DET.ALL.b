* @ValidationCode : MjoxMjA0NDUxMTI1OkNwMTI1MjoxNjQ0OTI5ODUzNTEyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 14:57:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-- CREATE BY MAHMOUD 22/8/2011
*-- EDIT BY NESSMA
PROGRAM SBR.CR.CUST.DET.ALL

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*    $INCLUDE T24.BP I_F.ACCOUNT.DATE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT.REFERENCE
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT          ;*TF.LC.
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 67 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 69 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LC.TYPES
*Line [ 71 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GENERAL.CHARGE
*Line [ 73 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.HIGHEST.DEBIT
*Line [ 75 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 77 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 79 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 81 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.GROUP
*Line [ 83 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 85 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY
*-------------------------------------------------------------------------
************************************
    EXECUTE "OFSADMINLOGIN"
************************************
    PRINT TIME()
    HI.PER = "" ; INT.DATEEE = ""
    CRD.CODES    = "100.110.120.LC.LC2.LG.LG2."

    COD.CNT      = COUNT(CRD.CODES,'.')
    HEAD.DESC    = ""
    LIM.REF.DESC = ""
    FOR AA = 1 TO COD.CNT
        CRD.COD = FIELD(CRD.CODES,'.',AA)
        PRINT "FILE : SBR.CR.CUST.DET.":CRD.COD
        GOSUB INITIATE
        GOSUB PROCESS
    NEXT AA
RETURN
*==============================================================
INITIATE:
*--------
    DAT.ID = 'EG0010001'
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,LWD.DATE)
    F.ITSS.DATES = 'F.DATES'
    FN.F.ITSS.DATES = ''
    CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
    CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
    LWD.DATE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,LPERIOD.DATE)
    F.ITSS.DATES = 'F.DATES'
    FN.F.ITSS.DATES = ''
    CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
    CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
    LPERIOD.DATE=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

**** 20180628    FOLDER   = "&SAVEDLISTS&"
    FOLDER   = "/home/signat"
    FILENAME = "SBR.CR.CUST.DET.":CRD.COD:".CSV"
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    HEAD.DESC  = "Serial":","
    HEAD.DESC := "Customer Name":","
    HEAD.DESC := "Customer No.":","
    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
        HEAD.DESC := "Reference No.":","
    END ELSE
        HEAD.DESC := "Account No.":","
    END
    HEAD.DESC := "GL":","
    HEAD.DESC := "GL Gb Description":","
    HEAD.DESC := "GL Ar Description":","
    HEAD.DESC := "Branch":","
    HEAD.DESC := "CCY":","
    HEAD.DESC := "Available Limits/direct":","
    HEAD.DESC := "Available Limits/indirect":","
    HEAD.DESC := "Available Limits in Local CCY":","
    HEAD.DESC := "Utilization/direct":","
    HEAD.DESC := "Utilization/indirect":","
    HEAD.DESC := "Utilization in Local CCY":","

    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
        HEAD.DESC := "Limit CCY":","
        HEAD.DESC := "Limit No.":","
        HEAD.DESC := "Limit Type":","
    END
    HEAD.DESC := "Limit Maturity":","
    HEAD.DESC := "Rate":","
    HEAD.DESC := "Rate Type":","
    HEAD.DESC := "Customer Type":","

    IF CRD.COD NE 'LG' AND CRD.COD NE 'LC' AND CRD.COD NE 'LG2' AND CRD.COD NE 'LC2' THEN
        HEAD.DESC := "Loan Type":","
    END
    IF CRD.COD EQ 120 THEN
        HEAD.DESC := "Type":","
    END
    HEAD.DESC := "Days to Maturity":","
    HEAD.DESC := "Buket Label":","
    HEAD.DESC := "UnFunded direct":","
    HEAD.DESC := "UnFunded indirect":","
    HEAD.DESC := "UnFunded in Local CCY":","

    IF CRD.COD EQ '100' OR CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
        HEAD.DESC := "Approved Limit":","
        HEAD.DESC := "Approved UnFunded":","
    END
    HEAD.DESC := "Sector":","
    HEAD.DESC := "Collateral maturity":","
    HEAD.DESC := "Collateral amount":","
    HEAD.DESC := "New Sector Code":","
    HEAD.DESC := "Highest Debit":","
    HEAD.DESC := "Report date ":LPERIOD.DATE:","

    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LG2' THEN
        HEAD.DESC := "LIMIT.REFERENCE":","
        HEAD.DESC := "MARGIN.AMOUNT":","
        HEAD.DESC := "LG.KIND":","
    END

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ;  F.BASE = '' ; R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    FN.SCC = 'F.SCB.STATIC.CREDIT.CUSTOMER' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC1 = 'FBNK.ACCOUNT' ; F.ACC1 = ''
    CALL OPF(FN.ACC1,F.ACC1)

    FN.ACC2 = 'FBNK.ACCOUNT' ; F.ACC2 = ''
    CALL OPF(FN.ACC2,F.ACC2)

    FN.ADT = 'FBNK.ACCOUNT.DATE' ; F.ADT = ''
    CALL OPF(FN.ADT,F.ADT)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = ''
    CALL OPF(FN.ADI,F.ADI)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    FN.INT = 'FBNK.BASIC.INTEREST' ; F.INT = ''
    CALL OPF(FN.INT,F.INT)

    FN.INT.D = 'FBNK.BASIC.INTEREST.DATE' ; F.INT.D = ''
    CALL OPF(FN.INT.D,F.INT.D)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = ''
    CALL OPF(FN.PD,F.PD)

    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    FN.IND.LCA = "FBNK.LC.APPLICANT"         ; F.IND.LCA = ""
    CALL OPF(FN.IND.LCA,F.IND.LCA)

    FN.IND.LCB = "FBNK.LC.BENEFICIARY"   ; F.IND.LCB = ""
    CALL OPF(FN.IND.LCB,F.IND.LCB)

    FN.IND.LI = "FBNK.LIMIT.LIABILITY" ; F.IND.LI = ""
    CALL OPF(FN.IND.LI,F.IND.LI)

    FN.LIM = "FBNK.LIMIT" ; F.LIM = "" ; R.LIM = ''  ; ER.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.LI = "FBNK.LIMIT" ; F.LI = "" ; R.LI = ''  ; ER.LI = ''
    CALL OPF(FN.LI,F.LI)

    FN.COL   = "FBNK.COLLATERAL"         ; F.COL  = '' ;  R.COL = '' ; ER.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)

    FN.GEN = 'FBNK.GENERAL.CHARGE' ;  F.GEN = '' ; R.GEN = ''
    CALL OPF(FN.GEN,F.GEN)

    FN.HI = 'FBNK.HIGHEST.DEBIT' ;  F.HI = '' ; R.HI = ''
    CALL OPF(FN.HI,F.HI)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.INT.AMT.LOCAL       = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    APPROVED.AMT = 0
    KK  = 0
    AA1 = 0
    LI.EXP.DATE = ''
    CUS.TYPE = ''
    LI.ONL.AMT = 0
    LI.INT.AMT = 0
    CUS.ID.1 = ''
    ACC.RATE = ''
    PRICING.TYPE = ''
    PRF.TYPE  = ''
    WS.NEXT.DR = 0
    DAYS.NO = ''
    WS.DIR.INTERNAL.AMT    = 0
    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.INT.AMT.LOCAL       = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.INT.AMT.LOCAL       = 0
    USED.AMT.LOCAL         = 0
    INTERNAL.AMT           = 0
    USED.AMT               = 0
    WS.BUCKET.LABEL        = ''
    UNFUNDED.DIRECT        = 0
    UNFUNDED.INDIRECT      = 0
    UNFUNDED.LOCAL         = 0
    CUS.SEC.NAME           = ''
    COLL.MAT               = ''
    COLL.AMT               = 0
****
    WS.CY.ID= ""
    LI.ID= ""
    MTL.GRP  = " 1211 1212 1214 1215 1216 1220 1221 1222 1223 1224 1225"
    MTL.GRP := " 1227 1236 1262 1414 1518 1519 21054 21055 21056 21057 21058 21059 21060"
    MTL.GRP := " 21062 21063 21064 21065 21066 21067 21069 21071 21072 21073 21074"

    LTL.GRP  = " 1217 1218 1230 21068 21070"

    SND.GRP  = " 1478 1479 1486 1487 1488 1595 1596 1597 1598"
    SND.GRP := " 21050 21051 21052 21053 21061"

    PRF.GRP  = " 1302335 1302707 4300171 11300443"

    OVR.GRP  = " 1001 1002 1003 1006 1007 1008 1009 1011 1059 1102 1201 1202"
    OVR.GRP := " 1203 1205 1206 1207 1208 1301 1302 1303 1377 1390 1399 1401"
    OVR.GRP := " 1402 1404 1405 1406 1407 1408 1413 1415 1416 1417 1418 1419"
    OVR.GRP := " 1420 1421 1422 1445 1455 1477 1480 1481 1483 1484 1485 1493"
    OVR.GRP := " 1499 1501 1502 1503 1504 1507 1508 1509 1510 1511 1512 1513"
    OVR.GRP := " 1514 1516 1523 1524 1525 1526 1527 1528 1529 1530 1531 1532"
    OVR.GRP := " 1533 1534 1535 1536 1540 1544 1558 1559 1560 1561 1562 1566 1567"
    OVR.GRP := " 1568 1569 1570 1571 1572 1573 1574 1575 1576 1577 1579 1582"
    OVR.GRP := " 1583 1584 1585 1586 1587 1588 1591 1599 3005 3010 3011 3012 3013 3014 3017"
    OVR.GRP := " 3201 6501 6502 6503 6504 6511 11232 11239 11240 11242 1538 1515 1005 1539"

    ALL.CAT  = MTL.GRP:" ":LTL.GRP:" ":SND.GRP:" ":PRF.GRP:" ":OVR.GRP
    CAT.DESC = ''
RETURN
*========================================================================
PROCESS:
*-------
    T.SEL = "SSELECT ":FN.CUS
    IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' THEN
        T.SEL := " WITH CREDIT.CODE NE ''"
    END ELSE
        IF CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
            IF CRD.COD EQ 'LC2' THEN
                T.SEL := " WITH ( POSTING.RESTRICT LT 89"
                T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
                T.SEL := " AND TEXT UNLIKE BR... ) "
                T.SEL := " OR ( @ID LIKE 994... )"
            END
        END ELSE
            T.SEL := " WITH CREDIT.CODE EQ ":CRD.COD
        END
    END
    T.SEL := " BY CO.CODE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CRT CRD.COD : " *** " : SELECTED
**********//////// ADDING THE CATEGORY CUSTOMERS TO CUST ARRAY \\\\\\\\\**********
    IF CRD.COD EQ '100' THEN
        AA.SEL  = "SSELECT ":FN.ACC:" WITH CATEGORY IN ( ":ALL.CAT:" )"
        AA.SEL :=  " AND OPEN.ACTUAL.BAL LT 0"
        AA.SEL :=  " AND OPEN.ACTUAL.BAL NE ''"
        CALL EB.READLIST(AA.SEL,KEY.LIST.AC,"",SELECTED.AC,ER.MSG.AC)

        IF SELECTED.AC THEN
            FOR AAX = 1 TO SELECTED.AC
                CALL F.READ(FN.ACC1,KEY.LIST.AC<AAX>,R.ACC1,F.ACC1,ER.ACC1)
                AC.CUS.ID = R.ACC1<AC.CUSTOMER>
*Line [ 392 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,AC.CUS.ID,AC.CUS.LCL)
                F.ITSS.CUSTOMER = 'F.CUSTOMER'
                FN.F.ITSS.CUSTOMER = ''
                CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                CALL F.READ(F.ITSS.CUSTOMER,AC.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                AC.CUS.LCL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                AC.CUS.CRD.CODE = AC.CUS.LCL<1,CULR.CREDIT.CODE>
                IF ( AC.CUS.CRD.CODE LE 100 ) OR ( AC.CUS.CRD.CODE EQ '' ) THEN
                    LOCATE AC.CUS.ID IN KEY.LIST SETTING POS.AC.CUS THEN NULL ELSE
                        AA1++
                        KEY.LIST<SELECTED+AA1> = AC.CUS.ID
                    END
                END
            NEXT AAX
            SELECTED = SELECTED + AA1
        END
    END

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,E1)
            CUS.ID    = KEY.LIST<I>
            CUS.LCL   = R.CUS<EB.CUS.LOCAL.REF>
            CUST.NAME = CUS.LCL<1,CULR.ARABIC.NAME>
            CUS.SEC   = CUS.LCL<1,CULR.NEW.SECTOR>
            CUS.SEC.O = R.CUS<EB.CUS.SECTOR>
*Line [ 419 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('SCB.NEW.SECTOR':@FM:C.SCB.NEW.SECTOR.NAME,CUS.SEC,CUS.SEC.NAME)
            F.ITSS.SCB.NEW.SECTOR = 'F.SCB.NEW.SECTOR'
            FN.F.ITSS.SCB.NEW.SECTOR = ''
            CALL OPF(F.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR)
            CALL F.READ(F.ITSS.SCB.NEW.SECTOR,CUS.SEC,R.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR,ERROR.SCB.NEW.SECTOR)
            CUS.SEC.NAME=R.ITSS.SCB.NEW.SECTOR<C.SCB.NEW.SECTOR.NAME>
            IF CUS.SEC EQ '4650' THEN CUS.TYPE = 'RETAIL' ELSE CUS.TYPE = 'CORPORATE'
*Line [ 403 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            IF CUS.SEC.O EQ '1100' OR CUS.SEC.O EQ '1200' OR CUS.SEC.O EQ '1300' OR CUS.SEC.O EQ '1400' THEN CUS.TYPE = 'STAFF' ELSE NULL

            COMP.ID = R.CUS<EB.CUS.COMPANY.BOOK>
            
*Line [ 432 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)
            F.ITSS.COMPANY = 'F.COMPANY'
            FN.F.ITSS.COMPANY = ''
            CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
            CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
            BRANCH.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

            IF CRD.COD NE 'LG' AND CRD.COD NE 'LC' AND CRD.COD NE 'LG2' AND CRD.COD NE 'LC2' THEN
                GOSUB ACC.REC
                GOSUB LD.LOANS.REC
            END
            IF CRD.COD EQ 'LG' THEN
                GOSUB LG.REC
            END
            IF CRD.COD EQ 'LG2' THEN
                GOSUB LG.REC
            END
            IF CRD.COD EQ 'LC' THEN
                GOSUB LC.REC
            END
            IF CRD.COD EQ 'LC2' THEN
                GOSUB LC.REC
            END
        NEXT I
    END

    IF CRD.COD EQ '100' THEN
        GOSUB ACC.INTERNAL
    END
RETURN
*------------------------------------------------------------
ACC.REC:
*-------
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
    LOOP
        REMOVE ACC.ID FROM R.CUS.ACC SETTING POS.AC
    WHILE ACC.ID:POS.AC
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,E1)
        AC.CAT  = R.ACC<AC.CATEGORY>
        FND.POS = ""
        FINDSTR ' ':AC.CAT IN ALL.CAT SETTING FND.POS THEN

            AC.CUR = R.ACC<AC.CURRENCY>
            AC.GDI = R.ACC<AC.CONDITION.GROUP>

*Line [ 448 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AC.ADI.COUNT = DCOUNT(R.ACC<AC.ACCT.DEBIT.INT>,@VM)
            ADI.DAT      = R.ACC<AC.ACCT.DEBIT.INT><1,AC.ADI.COUNT>
            IF CRD.COD EQ 110 OR CRD.COD EQ 120 THEN
                LOAN.TYPE = ""
            END ELSE
                LOAN.TYPE = "Overdraft"
            END


            FINDSTR ' ':AC.CAT IN OVR.GRP SETTING POS.O THEN LOAN.TYPE = "Overdraft" ELSE NULL
            FINDSTR ' ':AC.CAT IN MTL.GRP SETTING POS.M THEN LOAN.TYPE = "Medium term loans" ELSE NULL
            FINDSTR ' ':AC.CAT IN LTL.GRP SETTING POS.M THEN LOAN.TYPE = "Long term loans" ELSE NULL
            FINDSTR ' ':AC.CAT IN SND.GRP SETTING POS.S THEN LOAN.TYPE = "Syndicated Loans" ELSE NULL
            IF CRD.COD EQ 120 THEN
                FINDSTR ' ':CUS.ID IN PRF.GRP SETTING POS.P THEN PRF.TYPE = "Performing" ELSE PRF.TYPE = "Non-Performing"
            END
            IF (AC.CAT GE 1100 AND AC.CAT LE 1599) OR ( AC.CAT EQ 1001 OR AC.CAT EQ 1002 OR AC.CAT EQ 1407 OR AC.CAT EQ 1408 OR AC.CAT EQ 1413 OR AC.CAT EQ 1445 OR AC.CAT EQ 1455 ) THEN
                GOSUB GET.ADI.RATE
                IF ACC.RATE NE '' THEN PRICING.TYPE = 'FIXED' ELSE PRICING.TYPE = 'FLOATED' ; GOSUB GET.FLOATED
                GOSUB GETHIGH
            END
            AC.BAL = R.ACC<AC.ONLINE.ACTUAL.BAL>
            IF AC.BAL LT 0 THEN
                AC.LIM = R.ACC<AC.LIMIT.REF>
                IF AC.LIM THEN
                    CHK.LIM = FMT(FIELD(AC.LIM,".",1),'R%7')
                    LI.ID   = CUS.ID:".":CHK.LIM:".":FIELD(AC.LIM,".",2)
                    GOSUB LIM.REC
                END

                OPER.ID     = ACC.ID
                CAT.ID      = AC.CAT

                MM.CURRENCY = AC.CUR
                WS.DIR.INTERNAL.AMT   = LI.ONL.AMT
                WS.INDIR.INTERNAL.AMT = 0
                INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                IF MM.CURRENCY EQ LCCY THEN
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT
                END ELSE
                    GOSUB GET.LOCAL.RATE
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT * MM.CUR.RATE
                END
                WS.DIR.USED.AMT       = AC.BAL
                WS.INDIR.USED.AMT     = 0
                USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                IF MM.CURRENCY EQ LCCY THEN
                    USED.AMT.LOCAL    = USED.AMT
                END ELSE
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                END
                APPROVED.AMT = LI.INT.AMT
                GOSUB DATA.FILE
            END
        END
    REPEAT
RETURN
*************************************************************
GET.LOCAL.RATE:
*--------------
    MM.CUR.RATE = ''
*Line [ 541 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SBD.CURRENCY':@FM:SBD.CURR.MID.RATE,MM.CURRENCY,MM.CUR.RATE)
    F.ITSS.SBD.CURRENCY = 'F.SBD.CURRENCY'
    FN.F.ITSS.SBD.CURRENCY = ''
    CALL OPF(F.ITSS.SBD.CURRENCY,FN.F.ITSS.SBD.CURRENCY)
    CALL F.READ(F.ITSS.SBD.CURRENCY,MM.CURRENCY,R.ITSS.SBD.CURRENCY,FN.F.ITSS.SBD.CURRENCY,ERROR.SBD.CURRENCY)
    MM.CUR.RATE=R.ITSS.SBD.CURRENCY<SBD.CURR.MID.RATE>
    IF MM.CURRENCY EQ 'JPY' THEN MM.CUR.RATE = MM.CUR.RATE/100 ELSE NULL
RETURN
*************************************************************
GET.FLOATED:
*-----------
    GDI.ID.LK = AC.GDI:AC.CUR:"..."
    G.SEL = "SSELECT ":FN.GDI:" WITH @ID LIKE ":GDI.ID.LK
    CALL EB.READLIST(G.SEL,G.LIST,'',SELECTED.G,ER.MSG.G)
    GDI.ID = G.LIST<SELECTED.G>
    CALL F.READ(FN.GDI,GDI.ID,R.GDI,F.GDI,ERR.G1)

    GDI.BRATE = R.GDI<IC.GDI.DR.BASIC.RATE,1>
    GDI.INT.R = R.GDI<IC.GDI.DR.INT.RATE,1>
    GDI.MRG.O = R.GDI<IC.GDI.DR.MARGIN.OPER,1>
    GDI.MRG.R = R.GDI<IC.GDI.DR.MARGIN.RATE,1>
    CHRG.COD  = R.GDI<IC.GDI.CHARGE.KEY>

    IF GDI.INT.R EQ '' THEN
        INT.ID.LK = GDI.BRATE:AC.CUR:"..."
*        I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
*        CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
*        INT.ID = I.LIST<SELECTED.I>
*--- 20190219
        INT.D = GDI.BRATE:AC.CUR
        CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)
        INT.DATEEE  = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
        INT.ID      = INT.D:INT.DATEEE
*---------------------------------------
        CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
        INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
        ACC.RATE = INT.RATE
    END ELSE
        ACC.RATE = GDI.INT.R
    END
    IF GDI.MRG.O EQ 'ADD' THEN
        ACC.RATE = ACC.RATE + GDI.MRG.R
    END
    IF GDI.MRG.O EQ 'SUBTRACT' THEN
        ACC.RATE = ACC.RATE - GDI.MRG.R
    END
    IF GDI.MRG.O EQ 'MULTIBLY' THEN
        ACC.RATE = ACC.RATE * GDI.MRG.R
    END
RETURN
*************************************************************
GET.ADI.RATE:
*-----------
***###    CALL F.READ(FN.ADT,ACC.ID,R.ADT,F.ADT,EDDD)
*??    ADI.DAT = R.ADT<AC.DTE.DEBIT.DATES>
***###    ADI.DAT = R.ADT<1>

***###    DD1 = DCOUNT(ADI.DAT,VM)
    ADI.ID = ACC.ID:"-":ADI.DAT
    CALL F.READ(FN.ADI,ADI.ID,R.ADI,F.ADI,EADD)
    ADI.INT.R = R.ADI<IC.ADI.DR.INT.RATE,1>
    ADI.BRATE = R.ADI<IC.ADI.DR.BASIC.RATE,1>
    ADI.MRG.O = R.ADI<IC.ADI.DR.MARGIN.OPER,1>
    ADI.MRG.R = R.ADI<IC.ADI.DR.MARGIN.RATE,1>
    CHRG.COD  = R.ADI<IC.ADI.CHARGE.KEY>
    IF ADI.INT.R EQ '' THEN
*------ 20190219
*        INT.ID.LK = ADI.BRATE:AC.CUR:"..."
*        I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
*        CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
*        INT.ID = I.LIST<SELECTED.I>
        INT.D = ADI.BRATE:AC.CUR
        CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)
        INT.DATEEE  = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
        INT.ID      = INT.D:INT.DATEEE
*-------------------------------------------------------------------
        CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
        INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
        ACC.RATE = INT.RATE
    END ELSE
        ACC.RATE = ADI.INT.R
    END
    IF ADI.MRG.O EQ 'ADD' THEN
        ACC.RATE = ACC.RATE + ADI.MRG.R
    END
    IF ADI.MRG.O EQ 'SUBTRACT' THEN
        ACC.RATE = ACC.RATE - ADI.MRG.R
    END
    IF ADI.MRG.O EQ 'MULTIBLY' THEN
        ACC.RATE = ACC.RATE * ADI.MRG.R
    END
RETURN
*---------------------------------------------------
GETHIGH:
*----------------
    HI.DEBIT = '' ; HI.PER = 0
    T.SEL.HI = "SELECT ":FN.GEN:" WITH @ID LIKE ":CHRG.COD:".... BY @ID"
    CALL EB.READLIST(T.SEL.HI,KEY.LIST.HI,"",SELECTED.HI,ER.MSG.HI)
    IF SELECTED.HI THEN
        CALL F.READ(FN.GEN,KEY.LIST.HI<SELECTED.HI>,R.GEN,F.GEN,E1.HI)
        HI.DEBIT = R.GEN<IC.GCH.HIGHEST.DEBIT>:"...."
    END

    T.SEL1.HI = "SELECT ":FN.HI:" WITH @ID LIKE ":HI.DEBIT:" BY @ID"
    CALL EB.READLIST(T.SEL1.HI,KEY.LIST1.HI,"",SELECTED1.HI,ER.MSG1.HI)
    IF SELECTED1.HI THEN
        CALL F.READ(FN.HI,KEY.LIST1.HI<SELECTED1.HI>,R.HI,F.HI,E2.HI)
        HI.PER = R.HI<IC.HDB.PERCENTAGE>
    END
    IF HI.PER NE '' THEN
        HI.PER  ="0" : HI.PER
    END
RETURN
*---------------------------------------------------
LD.LOANS.REC:
*------------
    CALL F.READ(FN.IND.LD,CUS.ID,R.IND.LD,F.IND.LD,ER.IND.LD)

    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD

        IF LD.ID[1,2] EQ 'LD' THEN
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
            REC.STA = R.LD<LD.STATUS>
            LD.CAT  = R.LD<LD.CATEGORY>
            LD.MAT  = R.LD<LD.FIN.MAT.DATE>

            IF R.LD<LD.INTEREST.RATE> EQ '' AND R.LD<LD.INTEREST.KEY> NE '' THEN
*----- 20190219
*                INT.ID.LK.LD = R.LD<LD.INTEREST.KEY>:R.LD<LD.CURRENCY>:"..."
*                I.LD.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK.LD
*                CALL EB.READLIST(I.LD.SEL,I.LD.LIST,'',SELECTED.I.LD,ER.MSG.I.LD)
*                INT.LD.ID = I.LD.LIST<SELECTED.I.LD>

                INT.D = R.LD<LD.INTEREST.KEY>:R.LD<LD.CURRENCY>
                CALL F.READ(FN.INT.D,INT.D,R.INT.D,F.INT.D,ERR.I111)
                INT.DATEEE = R.INT.D<EB.BID.EFFECTIVE.DATE,1>
                INT.LD.ID  = INT.D:INT.DATEEE
*---------------------------------------------------------------------
                CALL F.READ(FN.INT,INT.LD.ID,R.INT,F.INT,ERR.I1)
                INT.LD.RATE = R.INT<EB.BIN.INTEREST.RATE>
                ACC.RATE = INT.LD.RATE
            END ELSE
                ACC.RATE = R.LD<LD.INTEREST.RATE>
            END
            ACC.RATE = ACC.RATE + R.LD<LD.INTEREST.SPREAD>
            FINDSTR ' ':LD.CAT IN MTL.GRP SETTING POS.M THEN LOAN.TYPE = "Medium term loans" ELSE NULL
            FINDSTR ' ':LD.CAT IN LTL.GRP SETTING POS.M THEN LOAN.TYPE = "Long term loans" ELSE NULL
            FINDSTR ' ':LD.CAT IN SND.GRP SETTING POS.S THEN LOAN.TYPE = "Syndicated Loans" ELSE NULL

            PD.ID  = "PD":LD.ID
*Line [ 694 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,PD.ID,PD.AMT)
            F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
            FN.F.ITSS.PD.PAYMENT.DUE = ''
            CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
            CALL F.READ(F.ITSS.PD.PAYMENT.DUE,PD.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
            PD.AMT=R.ITSS.PD.PAYMENT.DUE<PD.TOTAL.OVERDUE.AMT>
            PD.AMT = PD.AMT * -1
            PD.AMT.TOT = ""
*Line [ 703 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.AMT.TO.REPAY,PD.ID,PD.AMT.TOT)
            F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
            FN.F.ITSS.PD.PAYMENT.DUE = ''
            CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
            CALL F.READ(F.ITSS.PD.PAYMENT.DUE,PD.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
            PD.AMT.TOT=R.ITSS.PD.PAYMENT.DUE<PD.TOTAL.AMT.TO.REPAY>
*--------------- END GET-------------------------------------
            IF LD.MAT GT LPERIOD.DATE THEN
                FINDSTR ' ':LD.CAT IN ALL.CAT SETTING POS.ALL.CAT THEN
                    LD.CUR = R.LD<LD.CURRENCY>
                    LD.AMT = R.LD<LD.AMOUNT> * -1
                    LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                    IF LD.LIM THEN
                        CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = LD.ID
                        CAT.ID      = LD.CAT
                        MM.CURRENCY = LD.CUR
                        WS.DIR.INTERNAL.AMT   = LI.ONL.AMT
                        WS.INDIR.INTERNAL.AMT = 0
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END
                        WS.DIR.USED.AMT       = LD.AMT + PD.AMT
                        WS.INDIR.USED.AMT     = 0
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                        END
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
            END ELSE          ;* NESSMAAAAA
                IF PD.AMT.TOT NE "0" THEN
                    IF LD.ID[1,2] EQ 'LD' THEN
                        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
                    END
                    REC.STA = R.LD<LD.STATUS>
                    LD.CAT  = R.LD<LD.CATEGORY>
                    LD.MAT  = R.LD<LD.FIN.MAT.DATE>
                    FINDSTR ' ':LD.CAT IN MTL.GRP SETTING POS.M THEN LOAN.TYPE = "Medium term loans" ELSE NULL
                    FINDSTR ' ':LD.CAT IN LTL.GRP SETTING POS.M THEN LOAN.TYPE = "Long term loans" ELSE NULL
                    FINDSTR ' ':LD.CAT IN SND.GRP SETTING POS.S THEN LOAN.TYPE = "Syndicated Loans" ELSE NULL

                    PD.ID  = "PD":LD.ID
*Line [ 760 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,PD.ID,PD.AMT)
                    F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
                    FN.F.ITSS.PD.PAYMENT.DUE = ''
                    CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
                    CALL F.READ(F.ITSS.PD.PAYMENT.DUE,PD.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
                    PD.AMT=R.ITSS.PD.PAYMENT.DUE<PD.TOTAL.OVERDUE.AMT>
                    PD.AMT = PD.AMT * -1
                    LD.AMT = 0
                    FINDSTR ' ':LD.CAT IN ALL.CAT SETTING POS.ALL.CAT THEN
                        LD.CUR = R.LD<LD.CURRENCY>
                        LD.AMT = R.LD<LD.AMOUNT> * -1
                        LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                        IF LD.LIM THEN
                            CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                            LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                            GOSUB LIM.REC
                            OPER.ID = LD.ID
                            CAT.ID      = LD.CAT
                            MM.CURRENCY = LD.CUR
                            WS.DIR.INTERNAL.AMT   = LI.ONL.AMT
                            WS.INDIR.INTERNAL.AMT = 0
                            INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                            IF MM.CURRENCY EQ LCCY THEN
                                WS.INT.AMT.LOCAL      = INTERNAL.AMT
                            END ELSE
                                GOSUB GET.LOCAL.RATE
                                WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                            END
                            WS.DIR.USED.AMT       = LD.AMT + PD.AMT
                            WS.INDIR.USED.AMT     = 0
                            USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                            LIMIT.CUR             = WS.CY.ID
                            LIMIT.REF             = LI.ID
                            IF MM.CURRENCY EQ LCCY THEN
                                USED.AMT.LOCAL = USED.AMT
                            END ELSE
                                GOSUB GET.LOCAL.RATE
                                USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                            END
                            APPROVED.AMT = LI.INT.AMT
                            GOSUB DATA.FILE
                        END
                    END
                END
            END
        END
    REPEAT
RETURN
*---------------------------------------------------
ACC.INTERNAL:
*-----------
    IF CRD.COD EQ '100' THEN
        INT.SEL = "SSELECT ":FN.ACC2:" WITH CATEGORY IN ( 11232 11234 11239 11240 11242 ) AND CUSTOMER EQ '' AND ONLINE.ACTUAL.BAL LT 0 AND ONLINE.ACTUAL.BAL NE '' "
        CALL EB.READLIST(INT.SEL,KEY.LIST.INT,"",SELECTED.INT,ER.MSG.INT)
        IF SELECTED.INT THEN
            LOOP
                REMOVE ACC.INT.ID FROM KEY.LIST.INT SETTING POS.INT
            WHILE ACC.INT.ID:POS.INT
                CALL F.READ(FN.ACC2,ACC.INT.ID,R.ACC2,F.ACC2,ERR.ACC2)
                INT.CAT = R.ACC2<AC.CATEGORY>
                INT.CUR = R.ACC2<AC.CURRENCY>
                INT.BAL = R.ACC2<AC.ONLINE.ACTUAL.BAL>
                INT.COM = R.ACC2<AC.CO.CODE>
                LOAN.TYPE = "Overdraft"

                CUS.ID = ''
                CUS.TYPE = ''
                CUS.SEC.NAME = ''
*Line [ 829 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,INT.COM,BRANCH.NAME)
                F.ITSS.COMPANY = 'F.COMPANY'
                FN.F.ITSS.COMPANY = ''
                CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
                CALL F.READ(F.ITSS.COMPANY,INT.COM,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
                BRANCH.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*Line [ 836 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,INT.CAT,CUST.NAME)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,INT.CAT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                CUST.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
                OPER.ID = ACC.INT.ID
                CAT.ID  = INT.CAT
                MM.CURRENCY = INT.CUR
                WS.DIR.INTERNAL.AMT   = 0
                WS.INDIR.INTERNAL.AMT = 0
                INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                WS.DIR.USED.AMT       = INT.BAL
                WS.INDIR.USED.AMT     = 0
                USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                IF MM.CURRENCY EQ LCCY THEN
                    USED.AMT.LOCAL    = USED.AMT
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT
                END ELSE
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LOCAL    = USED.AMT * MM.CUR.RATE
                    WS.INT.AMT.LOCAL  = INTERNAL.AMT *  MM.CUR.RATE
                END
                APPROVED.AMT = 0
                GOSUB DATA.FILE
            REPEAT
        END
    END
RETURN
*---------------------------------------------------
LG.REC:
*------
    CALL F.READ(FN.IND.LD,CUS.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
            REC.STA = R.LD<LD.STATUS>
            LD.CAT  = R.LD<LD.CATEGORY>
            IF REC.STA NE 'LIQ' THEN
                IF LD.CAT EQ 21096 OR LD.CAT EQ 21097 THEN
                    LD.CUR = R.LD<LD.CURRENCY>
                    LD.AMT = R.LD<LD.AMOUNT>
                    LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                    LD.MRG = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
                    LD.KND = R.LD<LD.LOCAL.REF><1,LDLR.LG.KIND>
                    IF LD.LIM THEN
                        CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                        LI.ID   = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = LD.ID
                        CAT.ID      = LD.CAT
                        MM.CURRENCY = LD.CUR
                        WS.DIR.INTERNAL.AMT   = 0
                        WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        WS.INT.AMT.LOCAL      = INTERNAL.AMT

                        WS.CY.ID = MM.CURRENCY
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END
                        WS.DIR.USED.AMT       = 0
                        WS.INDIR.USED.AMT     = LD.AMT
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL    = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                        END
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
            END
        END
    REPEAT
RETURN
*************************************************************
LC.REC:
*-------
    CALL F.READ(FN.IND.LCA,CUS.ID,R.IND.LCA,F.IND.LCA,ER.IND.LCA)
    IF NOT(ER.IND.LCA) THEN
        LOOP
            REMOVE LCA.ID FROM R.IND.LCA SETTING POS.LCA
        WHILE LCA.ID:POS.LCA
            CALL F.READ(FN.LC,LCA.ID,R.LC,F.LC,ER.LC)
            IF NOT(ER.LC) THEN
                LC.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
                IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                    LC.CAT  = R.LC<TF.LC.CATEGORY.CODE>
                    LC.CUR  = R.LC<TF.LC.LC.CURRENCY>
                    LC.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                    LC.LIM  = R.LC<TF.LC.LIMIT.REFERENCE>
                    IF LC.LIM THEN
                        CHK.LIM = FMT(FIELD(LC.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LC.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = LCA.ID
                        CAT.ID      = LC.CAT
                        MM.CURRENCY = LC.CUR
                        WS.DIR.INTERNAL.AMT   = 0
                        WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        WS.INT.AMT.LOCAL = INTERNAL.AMT
                        WS.CY.ID = MM.CURRENCY
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END

                        WS.DIR.USED.AMT       = 0
                        WS.INDIR.USED.AMT     = LC.AMT
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        USED.AMT.LOCAL        = USED.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL    = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                        END
                        WS.CY.ID              = MM.CURRENCY
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
                LC.DR.SUB.ID = LCA.ID
                GOSUB DR.REC
            END
        REPEAT
    END ELSE
        R.DRAW    = ""
        WS.AMOUNT = 0
        BNK.DATE1 = TODAY
        CALL CDT("",BNK.DATE1,'-1C')
        NN.SEL  = "SELECT ":FN.DR:" WITH MATURITY.REVIEW GT ":BNK.DATE1
        NN.SEL := " AND CUSTOMER.LINK EQ ":CUS.ID
        NN.SEL := " AND DOCUMENT.AMOUNT GT 0"
        CALL EB.READLIST(NN.SEL,NN.LIST,"",NN.SELECTED,NN.ER.MSG)
        IF NN.SELECTED THEN
            FOR NN = 1 TO NN.SELECTED
                WS.DR.ID = NN.LIST<NN>
                CALL F.READ(FN.DR,WS.DR.ID,R.DRAW,F.DR,NN.ETEXT)
                DR.CUR   = R.DRAW<TF.DR.DRAW.CURRENCY>
                DR.LIM   = R.DRAW<TF.DR.LIMIT.REFERENCE>
                DR.AMT   = R.DRAW<TF.DR.DOCUMENT.AMOUNT>
                TEMP.IDD = R.DRAW<TF.DR.LC.CREDIT.TYPE>
                CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                RATE = 1
                LOCATE DR.CUR IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                WS.AMOUT  = DR.AMT * RATE
                LC.CAT    = ""          ;* NO LC RECORD FOR THIS DR
                IF DR.LIM THEN
                    LIM.REF.ID = FIELD(DR.LIM,".",1)
                    CHK.LIM    = FMT(FIELD(DR.LIM,".",1),'R%7')
                    LI.ID      = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                    GOSUB LIM.REC
                    OPER.ID               = WS.DR.ID
                    CAT.ID                = LC.CAT
                    MM.CURRENCY           = DR.CUR
                    WS.DIR.INTERNAL.AMT   = 0
                    WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                    INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                    WS.INT.AMT.LOCAL      = INTERNAL.AMT
                    CALL EGP.BASE.CURR(WS.INT.AMT.LOCAL,MM.CURRENCY)
                    WS.DIR.USED.AMT       = 0
                    WS.INDIR.USED.AMT     = DR.AMT
                    USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                    USED.AMT.LOCAL        = USED.AMT
                    IF MM.CURRENCY EQ LCCY THEN
                        USED.AMT.LOCAL    = USED.AMT
                    END ELSE
                        GOSUB GET.LOCAL.RATE
                        USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                    END
                    WS.CY.ID              = MM.CURRENCY
                    LIMIT.CUR             = WS.CY.ID
                    LIMIT.REF             = LI.ID
                    APPROVED.AMT          = LI.INT.AMT
                    FN.TEMPP = 'FBNK.LC.TYPES' ; F.TEMPP = ''
                    CALL OPF(FN.TEMPP, F.TEMPP)
                    CALL F.READ(FN.TEMPP,TEMP.IDD,R.TEMPP,F.TEMPP,ERROR.TEMP)
                    CAT.ID =  R.TEMPP<LC.TYP.CATEGORY.CODE>
                    GOSUB DATA.FILE
                END
            NEXT NN
        END
    END

    CALL F.READ(FN.IND.LCB,CUS.ID,R.IND.LCB,F.IND.LCB,ER.IND.LCB)
    LOOP
        REMOVE LCB.ID FROM R.IND.LCB SETTING POS.LCB
    WHILE LCB.ID:POS.LCB
        CALL F.READ(FN.LC,LCB.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                LC.CAT  = R.LC<TF.LC.CATEGORY.CODE>
                LC.CUR  = R.LC<TF.LC.LC.CURRENCY>
                LC.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                LC.LIM  = R.LC<TF.LC.LIMIT.REFERENCE>

                IF LC.LIM THEN
                    CHK.LIM = FMT(FIELD(LC.LIM,".",1),'R%7')
                    LI.ID   = CUS.ID:".":CHK.LIM:".":FIELD(LC.LIM,".",2)
                    GOSUB LIM.REC
                    OPER.ID = LCB.ID
                    CAT.ID      = LC.CAT
                    MM.CURRENCY = LC.CUR
                    WS.DIR.INTERNAL.AMT   = 0
                    WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                    INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                    WS.INT.AMT.LOCAL      = INTERNAL.AMT
                    IF MM.CURRENCY EQ LCCY THEN
                        WS.INT.AMT.LOCAL      = INTERNAL.AMT
                    END ELSE
                        GOSUB GET.LOCAL.RATE
                        WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                    END

                    WS.DIR.USED.AMT       = 0
                    WS.INDIR.USED.AMT     = LC.AMT
                    USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                    USED.AMT.LOCAL        = USED.AMT
                    IF MM.CURRENCY EQ LCCY THEN
                        USED.AMT.LOCAL    = USED.AMT
                    END ELSE
                        GOSUB GET.LOCAL.RATE
                        USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                    END
                    WS.CY.ID              = MM.CURRENCY
                    LIMIT.CUR             = WS.CY.ID
                    LIMIT.REF             = LI.ID
                    APPROVED.AMT = LI.INT.AMT
                    GOSUB DATA.FILE
                END
            END
            WS.DR.SUB.ID = LCB.ID
            GOSUB DR.REC
        END
    REPEAT
RETURN
*------------------------------------------------------------------
DR.REC:
*--------
    WS.SRL.DR  = ''
    WS.NEXT.DR = WS.NEXT.DR + 0
    FOR IDR = 1 TO WS.NEXT.DR
        WS.SRL.ID = ( IDR + 100 )[2,2]
        WS.DR.ID  = WS.DR.SUB.ID : WS.SRL.ID
        CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
        IF NOT(ER.DR) THEN
            IF R.DR<TF.DR.MATURITY.REVIEW> GT LPERIOD.DATE THEN
                IF R.DR<TF.DR.DOCUMENT.AMOUNT> GT 0 THEN
                    DR.CUR   = R.DR<TF.DR.DRAW.CURRENCY>
                    DR.AMT   = R.DR<TF.DR.DOCUMENT.AMOUNT>
                    DR.LIM   = R.DR<TF.DR.LIMIT.REFERENCE>
                    IF DR.LIM THEN
                        LIM.REF.ID = FIELD(DR.LIM,".",1)
                        CHK.LIM = FMT(FIELD(DR.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                        GOSUB LIM.REC
                        OPER.ID = WS.DR.ID
                        CAT.ID  = LC.CAT
                        MM.CURRENCY = DR.CUR
                        WS.DIR.INTERNAL.AMT   = 0
                        WS.INDIR.INTERNAL.AMT = LI.ONL.AMT
                        INTERNAL.AMT          = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
                        WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            WS.INT.AMT.LOCAL      = INTERNAL.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            WS.INT.AMT.LOCAL    = INTERNAL.AMT * MM.CUR.RATE
                        END

                        WS.DIR.USED.AMT       = 0
                        WS.INDIR.USED.AMT     = DR.AMT
                        USED.AMT              = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
                        USED.AMT.LOCAL        = USED.AMT
                        IF MM.CURRENCY EQ LCCY THEN
                            USED.AMT.LOCAL    = USED.AMT
                        END ELSE
                            GOSUB GET.LOCAL.RATE
                            USED.AMT.LOCAL = USED.AMT * MM.CUR.RATE
                        END
                        WS.CY.ID              = MM.CURRENCY
                        LIMIT.CUR             = WS.CY.ID
                        LIMIT.REF             = LI.ID
                        APPROVED.AMT = LI.INT.AMT
                        GOSUB DATA.FILE
                    END
                END
            END
        END
    NEXT IDR
RETURN
******************* WRITE DATA IN FILE **********************
DATA.FILE:
*---------
    INTERNAL.AMT      = WS.DIR.INTERNAL.AMT + WS.INDIR.INTERNAL.AMT
    USED.AMT          = WS.DIR.USED.AMT + WS.INDIR.USED.AMT
    UNFUNDED.DIRECT   = WS.DIR.INTERNAL.AMT + WS.DIR.USED.AMT
    UNFUNDED.INDIRECT = WS.INDIR.INTERNAL.AMT + WS.INDIR.USED.AMT
    UNFUNDED.LOCAL    = WS.INT.AMT.LOCAL + USED.AMT.LOCAL
    UNFUNDED.APPROVED = APPROVED.AMT + WS.DIR.USED.AMT + WS.INDIR.USED.AMT

    TOT.PRINT = ABS(USED.AMT) + ABS(INTERNAL.AMT)
    IF TOT.PRINT NE 0 AND TOT.PRINT NE '' THEN
        KK++

        WS.DIR.INTERNAL.AMT    = DROUND(WS.DIR.INTERNAL.AMT,'0')
        WS.INDIR.INTERNAL.AMT  = DROUND(WS.INDIR.INTERNAL.AMT,'0')
        WS.INT.AMT.LOCAL       = DROUND(WS.INT.AMT.LOCAL,'0')
        WS.DIR.USED.AMT        = DROUND(WS.DIR.USED.AMT,'0')
        WS.INDIR.USED.AMT      = DROUND(WS.INDIR.USED.AMT,'0')
        USED.AMT.LOCAL         = DROUND(USED.AMT.LOCAL,'0')
        UNFUNDED.DIRECT        = DROUND(UNFUNDED.DIRECT,'0')
        UNFUNDED.INDIRECT      = DROUND(UNFUNDED.INDIRECT,'0')
        UNFUNDED.LOCAL         = DROUND(UNFUNDED.LOCAL,'0')
        TOTAL.USED             = DROUND(TOTAL.USED,'0')
        APPROVED.AMT           = DROUND(APPROVED.AMT,'0')
        UNFUNDED.APPROVED      = DROUND(UNFUNDED.APPROVED,'0')
        COLL.AMT               = DROUND(COLL.AMT,'0')

        BB.DATA  = KK:","
        BB.DATA := CUST.NAME:","
        BB.DATA := CUS.ID:","
        BB.DATA := "' ":OPER.ID:","
        BB.DATA := CAT.ID:","
        CAT.DESC = ''
*Line [ 1183 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION:@FM:'F',CAT.ID,CAT.DESC)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CAT.DESC=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        BB.DATA := CAT.DESC<1,1>:","
        BB.DATA := CAT.DESC<1,2>:","
        CALL F.READ(FN.CUS,CUS.ID,R.CUSTOMER,F.CUS,ER.CUSTOMER)

        COMP.ID.2     = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
        
        BRANCH.NAME.2 = ""
*Line [ 1197 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID.2,BRANCH.NAME.2)
        F.ITSS.COMPANY = 'F.COMPANY'
        FN.F.ITSS.COMPANY = ''
        CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
        CALL F.READ(F.ITSS.COMPANY,COMP.ID.2,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
        BRANCH.NAME.2=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        BB.DATA := BRANCH.NAME.2:","
        BB.DATA := MM.CURRENCY:","
        BB.DATA := WS.DIR.INTERNAL.AMT:","
        BB.DATA := WS.INDIR.INTERNAL.AMT:","
        BB.DATA := WS.INT.AMT.LOCAL:","
        BB.DATA := WS.DIR.USED.AMT:","
        BB.DATA := WS.INDIR.USED.AMT:","
        BB.DATA := USED.AMT.LOCAL:","

        IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
            BB.DATA := WS.CY.ID:","
            BB.DATA := LI.ID:","
            BB.DATA := LIM.REF.DESC:","
        END

        BB.DATA := LI.EXP.DATE:","
        BB.DATA := ACC.RATE:","
        BB.DATA := PRICING.TYPE:","
        BB.DATA := CUS.TYPE:","

        IF CRD.COD NE 'LG' AND CRD.COD NE 'LC' AND CRD.COD NE 'LG2' AND CRD.COD NE 'LC2' THEN
            BB.DATA := LOAN.TYPE:","
        END
        IF CRD.COD EQ 120 THEN
            BB.DATA := PRF.TYPE:","
        END
        BB.DATA := DAYS.NO:","
        BB.DATA := WS.BUCKET.LABEL:","
        BB.DATA := UNFUNDED.DIRECT:","
        BB.DATA := UNFUNDED.INDIRECT:","
        BB.DATA := UNFUNDED.LOCAL:","

        IF CRD.COD EQ '100' OR CRD.COD EQ 'LG' OR CRD.COD EQ 'LC' OR CRD.COD EQ 'LG2' OR CRD.COD EQ 'LC2' THEN
            BB.DATA := APPROVED.AMT:","
            BB.DATA := UNFUNDED.APPROVED:","
        END
        IF CRD.COD EQ 'LG' OR CRD.COD EQ 'LG2' THEN
            BB.DATA := LD.LIM:","
            BB.DATA := LD.MRG:","
            BB.DATA := LD.KND:","
        END

        BB.DATA := CUS.SEC.NAME:","
        BB.DATA := COLL.MAT:","
        BB.DATA := COLL.AMT:","
        BB.DATA := CUS.SEC:","
        BB.DATA := HI.PER:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    END

    LI.EXP.DATE = ''
    LI.ONL.AMT  = 0
    LI.INT.AMT  = 0
    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    INTERNAL.AMT           = 0
    WS.INT.AMT.LOCAL       = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    USED.AMT               = 0
    USED.AMT.LOCAL         = 0
    UNFUNDED.INDIRECT      = 0
    UNFUNDED.DIRECT        = 0
    APPROVED.AMT           = 0
    UNFUNDED.APPROVED      = 0
    UNFUNDED.LOCAL         = 0
    COLL.MAT               = ''
    COLL.AMT               = 0
    ACC.RATE               = ''
    PRICING.TYPE           = ''
    WS.NEXT.DR             = 0
    WS.BUCKET.LABEL        = ''
    DAYS.NO                = ''
    HI.DEBIT               = ''
    HI.PER                 = 0
    INT.DATEEE             = ""

RETURN
************************************************
LIM.REC:
*--------
    CALL F.READ(FN.LIM,LI.ID,R.LIM,F.LIM,ER.LIM)
    IF R.LIM THEN
        WS.LI.PROD  = R.LIM<LI.LIMIT.PRODUCT>
        WS.PRO.ALL  = R.LIM<LI.PRODUCT.ALLOWED>
        WS.CY.ID    = R.LIM<LI.LIMIT.CURRENCY>
        LI.ONL.AMT  = R.LIM<LI.ONLINE.LIMIT,1>
        LI.INT.AMT  = R.LIM<LI.INTERNAL.AMOUNT>
        LI.MAX.TOT  = R.LIM<LI.MAXIMUM.TOTAL>
        LI.EXP.DATE = R.LIM<LI.EXPIRY.DATE>
        LI.COLL.RI  = R.LIM<LI.COLLAT.RIGHT>
        LI.SECURED  = R.LIM<LI.SECURED.AMT>
       
*Line [ 1224 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        CLL         = DCOUNT(LI.SECURED,@SM)
        MAX.COL.EXP = ''
        FOR BBB = 1 TO CLL
            COLL.AMT += LI.SECURED<1,1,BBB>
            RIGHT.IND.ID = R.LIM<LI.COLLAT.RIGHT,1,BBB>
            CALL F.READ(FN.RIGHT.COL,RIGHT.IND.ID,R.RIGHT.COL,F.RIGHT.COL,ER.RIGHT.COL)
            LOOP
                REMOVE COL.ID FROM R.RIGHT.COL SETTING POS.COLL
            WHILE COL.ID:POS.COLL
                CALL F.READ(FN.COL,COL.ID,R.COL,F.COL,E.COL.1)
                COL.EXP = R.COL<COLL.EXPIRY.DATE>
                IF COL.EXP GE MAX.COL.EXP THEN
                    MAX.COL.EXP = COL.EXP
                END
            REPEAT
        NEXT BBB
        COLL.MAT = MAX.COL.EXP

*Line [ 1321 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('LIMIT.REFERENCE':@FM:LI.REF.DESCRIPTION,WS.LI.PROD,LIM.REF.DESC)
        F.ITSS.LIMIT.REFERENCE = 'F.LIMIT.REFERENCE'
        FN.F.ITSS.LIMIT.REFERENCE = ''
        CALL OPF(F.ITSS.LIMIT.REFERENCE,FN.F.ITSS.LIMIT.REFERENCE)
        CALL F.READ(F.ITSS.LIMIT.REFERENCE,WS.LI.PROD,R.ITSS.LIMIT.REFERENCE,FN.F.ITSS.LIMIT.REFERENCE,ERROR.LIMIT.REFERENCE)
        LIM.REF.DESC=R.ITSS.LIMIT.REFERENCE<LI.REF.DESCRIPTION>
        IF LI.EXP.DATE NE '' THEN
            IF COLL.MAT NE '' THEN
                IF LI.EXP.DATE LE COLL.MAT THEN
                    MAT.DATE = LI.EXP.DATE
                END ELSE
                    MAT.DATE = COLL.MAT
                END
                IF MAT.DATE GT LPERIOD.DATE THEN
                    DATE.1 = LPERIOD.DATE
                    DATE.2 = MAT.DATE
                    SS1 = 1
                END ELSE
                    DATE.1 = MAT.DATE
                    DATE.2 = LPERIOD.DATE
                    SS1 = -1
                END
            END ELSE
                IF LI.EXP.DATE GT LPERIOD.DATE THEN
                    DATE.1 = LPERIOD.DATE
                    DATE.2 = LI.EXP.DATE
                    SS1 = 1
                END ELSE
                    DATE.1 = LI.EXP.DATE
                    DATE.2 = LPERIOD.DATE
                    SS1 = -1
                END
            END
            DAYS.NO = 'C'
            CALL CDD("",DATE.1,DATE.2,DAYS.NO)
            DAYS.NO = DAYS.NO * SS1
            MONTHS.NO = 1
            CALL EB.NO.OF.MONTHS(DATE.1,DATE.2,MONTHS.NO)
            IF DAYS.NO LT 0 THEN
                WS.BUCKET.LABEL = 'Maturity Expired'
            END ELSE
                IF DAYS.NO EQ 1 THEN
                    WS.BUCKET.LABEL = 'O/N'
                END ELSE
                    IF DAYS.NO GT 1 AND DAYS.NO LE 7 THEN
                        WS.BUCKET.LABEL = '1.WEEK'
                    END ELSE
                        IF MONTHS.NO EQ 1 THEN
                            WS.BUCKET.LABEL = '1.MONTH'
                        END
                    END
                END
            END

            IF MONTHS.NO GT 1 AND MONTHS.NO LE 3 THEN WS.BUCKET.LABEL = '3.MONTHS'
            IF MONTHS.NO GT 1 AND MONTHS.NO LE 3 THEN WS.BUCKET.LABEL = '3.MONTHS'
            IF MONTHS.NO GT 3 AND MONTHS.NO LE 6 THEN WS.BUCKET.LABEL = '6.MONTHS'
            IF MONTHS.NO GT 6 AND MONTHS.NO LE 9 THEN WS.BUCKET.LABEL = '9.MONTHS'
            IF MONTHS.NO GT 9 AND MONTHS.NO LE 12 THEN WS.BUCKET.LABEL = '1.YEAR'
            IF MONTHS.NO GT 12 AND MONTHS.NO LE 18 THEN WS.BUCKET.LABEL = '1.5.YEAR'
            IF MONTHS.NO GT 18 AND MONTHS.NO LE 24 THEN WS.BUCKET.LABEL = '2.YEAR'
            IF MONTHS.NO GT 24 AND MONTHS.NO LE 36 THEN WS.BUCKET.LABEL = '3.YEAR'
            IF MONTHS.NO GT 36 AND MONTHS.NO LE 48 THEN WS.BUCKET.LABEL = '4.YEAR'
            IF MONTHS.NO GT 48 AND MONTHS.NO LE 60 THEN WS.BUCKET.LABEL = '5.YEAR'
            IF MONTHS.NO GT 60 AND MONTHS.NO LE 72 THEN WS.BUCKET.LABEL = '6.YEAR'
            IF MONTHS.NO GT 72 AND MONTHS.NO LE 84 THEN WS.BUCKET.LABEL = '7.YEAR'
            IF MONTHS.NO GT 84 AND MONTHS.NO LE 96 THEN WS.BUCKET.LABEL = '8.YEAR'
            IF MONTHS.NO GT 96 AND MONTHS.NO LE 108 THEN WS.BUCKET.LABEL = '9.YEAR'
            IF MONTHS.NO GT 108 AND MONTHS.NO LE 120 THEN WS.BUCKET.LABEL = '10.YEAR'
            IF MONTHS.NO GT 120 AND MONTHS.NO LE 132 THEN WS.BUCKET.LABEL = '11.YEAR'
            IF MONTHS.NO GT 132 AND MONTHS.NO LE 144 THEN WS.BUCKET.LABEL = '12.YEAR'
            IF MONTHS.NO GT 144 AND MONTHS.NO LE 156 THEN WS.BUCKET.LABEL = '13.YEAR'
            IF MONTHS.NO GT 156 AND MONTHS.NO LE 168 THEN WS.BUCKET.LABEL = '14.YEAR'
            IF MONTHS.NO GT 168 AND MONTHS.NO LE 180 THEN WS.BUCKET.LABEL = '15.YEAR'
            IF MONTHS.NO GT 180 AND MONTHS.NO LE 192 THEN WS.BUCKET.LABEL = '16.YEAR'
            IF MONTHS.NO GT 192 AND MONTHS.NO LE 204 THEN WS.BUCKET.LABEL = '17.YEAR'
            IF MONTHS.NO GT 204 AND MONTHS.NO LE 216 THEN WS.BUCKET.LABEL = '18.YEAR'
            IF MONTHS.NO GT 216 AND MONTHS.NO LE 228 THEN WS.BUCKET.LABEL = '19.YEAR'
            IF MONTHS.NO GT 228 AND MONTHS.NO LE 240 THEN WS.BUCKET.LABEL = '20.YEAR'
            IF MONTHS.NO GT 240 THEN WS.BUCKET.LABEL = 'MORE.THAN.20.YEAR'
        END
    END
RETURN
*------------------------------------------------------------
END
