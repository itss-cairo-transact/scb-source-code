* @ValidationCode : MjotMTU5MDYxNzI3NzpDcDEyNTI6MTY0NDkyNTA2NTE1NDpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>64</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/09/09 ***
*******************************************

SUBROUTINE SBR.CU.SCCD.STATIC
*    PROGRAM SBR.CU.SCCD.STATIC
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 37 ] Hashing $INCLUDE I_F.SCB.NEW.SECTOR - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB GET.LD.DATA
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.CU  = "FBNK.CUSTOMER"                         ; F.CU   = ""
    FN.LD  = "FBNK.LD.LOANS.AND.DEPOSITS"            ; F.LD   = ""

    WS.CU.AMT.NEW.4650 = 0
    WS.CU.NUM.NEW.4650 = 0

    WS.CU.AMT.NEW.0000 = 0
    WS.CU.NUM.NEW.0000 = 0

    WS.CU.AMT.4650.TOT = 0
    WS.CU.NUM.4650.TOT = 0
*---------------------------------
    WS.CU.AMT.OLD.4650 = 0
    WS.CU.NUM.OLD.4650 = 0

    WS.CU.AMT.OLD.0000 = 0
    WS.CU.NUM.OLD.0000 = 0

    WS.CU.AMT.0000.TOT = 0
    WS.CU.NUM.0000.TOT = 0
*---------------------------------

    WS.CU.ID.FLG = 0
    WS.CU.ID     = 0

    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.CU,F.CU)


RETURN
*-----------------------------------------------------
GET.LD.DATA:
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21101 AND CATEGORY LE 21103) AND STATUS EQ 'CUR' BY CUSTOMER.ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    PRINT SELECTED
    IF SELECTED THEN
        FOR ILD =1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<ILD>,R.LD,F.LD,ER.LD)
            WS.CU.ID    = R.LD<LD.CUSTOMER.ID>
            WS.LD.AMT   = R.LD<LD.AMOUNT>
            WS.ONE      = 0

            GOSUB GET.CU.DATA

            IF WS.NEW.SECTOR EQ 4650 THEN
                IF WS.CONT.DATE GE 20140904 THEN
                    WS.CU.AMT.NEW.4650 += WS.LD.AMT
                    WS.CU.NUM.NEW.4650 += WS.ONE
                    WS.CU.AMT.4650.TOT += WS.LD.AMT
                    WS.CU.NUM.4650.TOT += WS.ONE
                END
                IF WS.CONT.DATE LT 20140904 THEN
                    WS.CU.AMT.OLD.4650 += WS.LD.AMT
                    WS.CU.NUM.OLD.4650 += WS.ONE
                    WS.CU.AMT.4650.TOT += WS.LD.AMT
                    WS.CU.NUM.4650.TOT += WS.ONE
                END
            END
            IF WS.NEW.SECTOR NE 4650 THEN
                IF WS.CONT.DATE GE 20140904 THEN
                    WS.CU.AMT.NEW.0000 += WS.LD.AMT
                    WS.CU.NUM.NEW.0000 += WS.ONE
                    WS.CU.AMT.0000.TOT += WS.LD.AMT
                    WS.CU.NUM.0000.TOT += WS.ONE
                END

                IF WS.CONT.DATE LT 20140904 THEN
                    WS.CU.AMT.OLD.0000 += WS.LD.AMT
                    WS.CU.NUM.OLD.0000 += WS.ONE
                    WS.CU.AMT.0000.TOT += WS.LD.AMT
                    WS.CU.NUM.0000.TOT += WS.ONE
                END
            END

            WS.CU.ID.FLG = WS.CU.ID
        NEXT ILD
    END
    GOSUB PRINT.SCCD.STATIC
RETURN
*-----------------------------------------------------
GET.CU.DATA:

    IF WS.CU.ID.FLG = 0 THEN
        WS.CU.ID.FLG = WS.CU.ID
        WS.ONE = 1
    END
    IF WS.CU.ID.FLG # WS.CU.ID THEN
        WS.CU.ID.FLG = WS.CU.ID
        WS.ONE = 1
    END

    CALL F.READ(FN.CU,WS.CU.ID,R.CU,F.CU,E.CU)
*    WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
*    WS.CUSTOMER.STATUS  = R.NEW(EB.CUS.CUSTOMER.STATUS)

    WS.CONT.DATE        = R.CU<EB.CUS.CONTACT.DATE>

    WS.NEW.SECTOR       = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

RETURN
*-----------------------------------------------------
PRINT.SCCD.STATIC:

    REPORT.ID='SBR.CU.SCCD.STATIC'
    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD
*    PR.HD  =REPORT.ID
*    HEADING PR.HD

*    PRINT WS.HED.DESC
    PRINT " "
    PRINT " "
    WS.P.4650         = ''
    WS.P.4650[1,12]   = "�������"
    WS.P.4650[20,15]  = WS.CU.NUM.OLD.4650
    WS.P.4650[38,18]  = FMT(WS.CU.AMT.OLD.4650,"L0,")
    WS.P.4650[58,15]  = WS.CU.NUM.NEW.4650
    WS.P.4650[75,18]  = FMT(WS.CU.AMT.NEW.4650,"L0,")
    WS.P.4650[95,15]  = WS.CU.NUM.4650.TOT
    WS.P.4650[115,20] = FMT(WS.CU.AMT.4650.TOT,"L0,")
    PRINT WS.P.4650
    PRINT " "
    PRINT " "
    WS.P.0000         = ''
    WS.P.0000[1,12]   = "���� ��������"
    WS.P.0000[20,15]  = WS.CU.NUM.OLD.0000
    WS.P.0000[38,18]  = FMT(WS.CU.AMT.OLD.0000,"L0,")
    WS.P.0000[58,15]  = WS.CU.NUM.NEW.0000
    WS.P.0000[75,18]  = FMT(WS.CU.AMT.NEW.0000,"L0,")
    WS.P.0000[95,15]  = WS.CU.NUM.0000.TOT
    WS.P.0000[115,20] = FMT(WS.CU.AMT.0000.TOT,"L0,")
    PRINT WS.P.0000
    PRINT " "
    PRINT " "
    PRINT STR('_',132)
    PRINT " "
    PRINT " "
    WS.P = ''
    WS.P[1,12]   = "���������"
    WS.P[20,15]  = WS.CU.NUM.OLD.0000 + WS.CU.NUM.OLD.4650
    WS.P[38,18]  = FMT((WS.CU.AMT.OLD.0000+WS.CU.AMT.OLD.4650),"L0,")
    WS.P[58,15]  = WS.CU.NUM.NEW.0000 + WS.CU.NUM.NEW.4650
    WS.P[75,18]  = FMT((WS.CU.AMT.NEW.0000+WS.CU.AMT.NEW.4650),"L0,")
    WS.P[95,15]  = WS.CU.NUM.0000.TOT + WS.CU.NUM.4650.TOT
    WS.P[115,20] = FMT((WS.CU.AMT.0000.TOT+WS.CU.AMT.4650.TOT),"L0,")
    PRINT WS.P
    PRINT " "
    PRINT " "
    PRINT SPACE(45):"-----------------  ����� ������� --------------------"
RETURN
*-----------------------------------------------------
PRINT.HEAD:
    WS.HED.DESC         = ''
    WS.HED.DESC[20,15]  = "��� �������"
    WS.HED.DESC[38,18]  = "����� �������"
    WS.HED.DESC[58,15]  = "��� ����� ���"
    WS.HED.DESC[75,18]  = "����� ����� ���"
    WS.HED.DESC[95,15] = "������ ��� �����"
    WS.HED.DESC[115,20] = "������ �������"
*Line [ 213 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ID.COMPANY,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,ID.COMPANY,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    TD = TODAY
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"SBR.CU.SCCD.STATIC"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������� �� ������ ������ ���� ������"
    PR.HD :="'L'":SPACE(50):"������� ������ ��� - ����� -���� ��������"
*    PR.HD :="'L'":SPACE(50):" ������� : ":T.DAY
    PR.HD :="'L'":SPACE(50):" ��� ����� : ":T.DAY
    WS.TIME           = TIME()
    WS.TIME           = OCONV(WS.TIME, "MTS")

    PR.HD :="'L'":SPACE(50):" �������� : ":WS.TIME

    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ":WS.HED.DESC
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD

RETURN
*-----------------------------------------------------
