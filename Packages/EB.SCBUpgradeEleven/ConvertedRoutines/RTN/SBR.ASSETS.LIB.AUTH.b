* @ValidationCode : MjotMTY0ODE1MjIzNTpDcDEyNTI6MTY0MDg1NzIwMjUxMjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:40:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.ASSETS.LIB.AUTH
*    PROGRAM    SBR.ASSETS.LIB.AUTH
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ASSETS.LIB
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USR.LOCAL.REF

    EXECUTE "COMO ON SBR.ASSETS.LIB.AUTH"
    FILE.NAME = "ASSETS.LIB.OFS.A"
    DIR.NAME  = "MECH"
    TD = TODAY
    OPENSEQ DIR.NAME , FILE.NAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':FILE.NAME
        HUSH OFF
    END
    OPENSEQ DIR.NAME , FILE.NAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILE.NAME:' CREATED IN ':DIR.NAME
        END ELSE
            STOP 'Cannot create ':FILE.NAME:' File IN ':DIR.NAME
        END
    END

    FN.FT = "FBNK.FUNDS.TRANSFER$NAU"   ; F.FT = ""
    CALL OPF(FN.FT, F.FT)
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')

    FN.USER = 'F.USER' ; F.USER = ''
    CALL OPF(FN.USER , F.USER)
    FN.OFS.IN  = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    CALL TXTINP('Enter sign on name', 8, 23, '15', 'ANY')
*    INP = "...":COMI:"..."
    IF COMI EQ '' THEN
        TEXT = 'You Must Enter User Number Please Try Again' ; CALL REM
        RETURN
    END

*    USER.SEL = 'SELECT F.USER WITH SIGN.ON.NAME EQ ':COMI
*    CALL EB.READLIST(USER.SEL,USER.KEY.LIST,"",USER.SELECTED,USER.ER.MSG)

    INP = "SCB.":COMI ; R.USR = '' ; ERR.USER = ''
    CALL F.READ(FN.USER,INP,R.USR,F.USER,ERR.USER)
    IF R.USR THEN

        INT.TEXT = '"':R.USR<EB.USE.USER.NAME>:'".press "Y" to continue or any other character to exit'
        CALL TXTINP(INT.TEXT,8,23,'15','ANY')
        IF COMI NE 'Y' THEN
            TEXT =  "PROGRAM EXIT" ; CALL REM
            RETURN
        END
        IF R.USR<EB.USE.LOCAL.REF,USER.SCB.DEPT.CODE> NE '9907' THEN
            TEXT = "User isn't in Financial Department" ; CALL REM
            RETURN
        END
        IF R.USR<EB.USE.END.DATE.PROFILE> LT TODAY THEN
            TEXT = 'End Date Profile is less than Today for this user'  ; CALL REM
            RETURN
        END
        INP = '..._':INP:'_...'
    END ELSE
        TEXT = 'This User Is Not Exist Please Try Again' ; CALL REM
        RETURN
    END


    SCB.VERSION = "MECH1"
    T.SEL  = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH INPUTTER LIKE ":INP
    CRT T.SEL
*    T.SEL  = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH @ID EQ FT2000833416 "
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        TEXT = 'SELECTED = ': SELECTED ; CALL REM
        FOR NN = 1 TO SELECTED
            BB.DATA  = ''
            FT.ID    = KEY.LIST<NN>
            CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ERR.FT)
            COOO     = R.FT<FT.CO.CODE>
*            COOO     = R.FT<FT.DEBIT.COMP.CODE>
            BB.DATA  = "FUNDS.TRANSFER,MECH1/A/PROCESS,":R.USER<EB.USE.SIGN.ON.NAME>:"//EG00100":COOO[2]:",":FT.ID



            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT NN
    END
    EXECUTE 'COPY FROM MECH TO OFS.IN ASSETS.LIB.OFS.A'
    EXECUTE 'DELETE ':"MECH":' ':"ASSETS.LIB.OFS.A"

    CRT 'END'
    EXECUTE "COMO OFF SBR.ASSETS.LIB.AUTH"
PROGRAM.END:
    TEXT = 'Program ended' ; CALL REM
END
