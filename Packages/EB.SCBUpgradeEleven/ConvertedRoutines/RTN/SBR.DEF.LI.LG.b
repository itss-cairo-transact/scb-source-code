* @ValidationCode : Mjo1MDEwNTQ0NzpDcDEyNTI6MTY0NDkyNTA2NjQ1MzpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** CREATED BY MOHAMED SABRY 2011/01/20 ***
***========================================
SUBROUTINE SBR.DEF.LI.LG

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB MAIN.PROC
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:

    FN.LI = "FBNK.LIMIT"                    ; F.LI = ""
    CALL OPF(FN.LI,F.LI)

    FN.LMM = "FBNK.LMM.CUSTOMER"            ; F.LMM = ""
    CALL OPF(FN.LMM,F.LMM)

    FN.LD  = "FBNK.LD.LOANS.AND.DEPOSITS"   ; F.LD  = ""
    CALL OPF(FN.LD, F.LD)

    REPORT.ID='P.FUNCTION'
    WS.REPORT.NAME='SBR.DEF.LI.LG'
    CALL PRINTER.ON(REPORT.ID,'')

    COMP = ID.COMPANY
    WS.CUS.ID.FLAG = 0
    WS.COUNT       = 0
RETURN
*===============================================================
MAIN.PROC:
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    DAT.ID = COMP
*************************************************************************
*   T.SEL = "SELECT FBNK.LIMIT WITH @ID UNLIKE 994... AND LIMIT.PRODUCT IN ( 2505 2510 2520 ) AND LIABILITY.NUMBER IN ( 1303328 1301320 1303432 ) AND COMMT.AMT.AVAIL NE '' BY LIABILITY.NUMBER BY LIMIT.PRODUCT BY LIMIT.CURRENCY "
    T.SEL = "SELECT FBNK.LIMIT WITH @ID UNLIKE 994... AND COMPANY.CO EQ ":COMP:" AND LIMIT.PRODUCT IN ( 2505 2510 2520 ) AND COMMT.AMT.AVAIL NE '' BY LIABILITY.NUMBER BY LIMIT.PRODUCT BY LIMIT.CURRENCY "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            WS.ESC.ID = FIELD(KEY.LIST<I>,".",4)
            IF  WS.ESC.ID NE '' THEN
                GO TO NEX.REC.I
            END

            CALL F.READ(FN.LI,KEY.LIST<I>,R.LI,F.LI,E1)
            WS.LI.CUS.ID     = FIELD(KEY.LIST<I>,".",1)
            WS.LI.SRL        = FIELD(KEY.LIST<I>,".",3)
            WS.LI.CY         = R.LI<LI.LIMIT.CURRENCY>
            WS.LI.ONLINE.LI  = R.LI<LI.ONLINE.LIMIT>
            WS.LI.ALL.USED   = R.LI<LI.COMMT.AMT.AVAIL>
            
*  IF WS.LI.ONLINE.LI EQ ( WS.LI.ALL.USED * -1 ) THEN
*      GO TO NEX.REC.I
*  END
            WS.LI.PRODUCT  = R.LI<LI.LIMIT.PRODUCT>
            WS.LI.PRD.SRL  = WS.LI.PRODUCT:'.':WS.LI.SRL


            IF  WS.CUS.ID.FLAG = 0 THEN
                WS.CUS.ID.FLAG = WS.LI.CUS.ID
*Line [ 103 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.LI.CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.LI.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                WS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                XX               = SPACE(132)
                XX<1,1>[1,10]    = WS.LI.CUS.ID
                XX<1,1>[12,35]   = WS.NAME
                PRINT XX<1,1>
                PRINT STR(' ',132)
            END

            IF  WS.CUS.ID.FLAG # WS.LI.CUS.ID THEN
                WS.CUS.ID.FLAG = WS.LI.CUS.ID
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.LI.CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.LI.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                WS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                PRINT STR('-',132)
                XX               = SPACE(132)
                XX<1,1>[1,10]    = WS.LI.CUS.ID
                XX<1,1>[12,35]   = WS.NAME
                PRINT XX<1,1>
                PRINT STR(' ',132)
            END
            XX               = SPACE(132)
            XX<1,1>[20,9]   = WS.LI.PRD.SRL
            XX<1,1>[30,3]    = WS.LI.CY
            XX<1,1>[35,18]   = FMT(WS.LI.ONLINE.LI,"L2,")
            XX<1,1>[55,18]   = FMT(WS.LI.ALL.USED,"L2,")
            PRINT XX<1,1>
            PRINT STR(' ',132)

            WS.CY.COUNT    = DCOUNT(R.LI<LI.COMMITM.CCY>,@SM)
            FOR X = 1 TO WS.CY.COUNT
                WS.LI.USED.CY  = R.LI<LI.COMMITM.CCY><1,1,X>
                WS.LI.USED.AMT = R.LI<LI.COMMITM.AMT><1,1,X>
                WS.LG.AMT      = 0
                GOSUB            GET.LG.AMT
                IF WS.LG.AMT NE WS.LI.USED.AMT THEN
                    XX               = SPACE(132)
                    XX<1,1>[1,35]    = ''
                    XX<1,1>[70,3]    = WS.LI.USED.CY
                    XX<1,1>[75,18]   = FMT(WS.LI.USED.AMT,"L2,")
                    XX<1,1>[95,18]   = FMT(WS.LG.AMT,"L2,")
                    XX<1,1>[115,18]  = FMT((WS.LI.USED.AMT - WS.LG.AMT ),"L2,")
                    WS.COUNT ++
                    PRINT XX<1,1>
                    PRINT STR(' ',132)
                END
            NEXT X
            WS.CUS.ID.FLAG = WS.LI.CUS.ID
NEX.REC.I:
        NEXT I
    END
    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  ����� �������  ***'
    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT STR('=',132)
    PRINT XX25<1,1>
    TEXT = "��� ������ ���� ��� ���� ":"= ":WS.COUNT ; CALL REM
RETURN
*===============================================================
PRINT.HEAD:
*---------

*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  =  FMT(TODAY,"####/##/##")
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):WS.REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� ����� ���������� ������ ������ "
*  PR.HD :="'L'":SPACE(48):STR('_',40)
*    PR.HD :="'L'":" "
    PR.HD :="'L'":"���":SPACE(9):"������ ���������":SPACE(20)
    PR.HD :="'L'":SPACE(20):"��� ����":" CY ":SPACE(2):"���� ������":SPACE(10):"��������� ����� �� ����":SPACE(17):"���� ��������� �������"
    PR.HD :="'L'":SPACE(70):"CCY":SPACE(3):"������ ��":SPACE(10):"������ �/�":SPACE(13):"������"
    PR.HD :="'L'":STR('-',132)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
GET.LG.AMT:
*------
    CALL F.READ(FN.LMM,WS.LI.CUS.ID,R.LMM,F.LMM,ER.LMM)
    LOOP
        REMOVE CONTRACT.ID FROM R.LMM SETTING POS.LMM
    WHILE CONTRACT.ID:POS.LMM
        IF CONTRACT.ID[1,2] EQ 'LD' THEN
            LD.ID = CONTRACT.ID
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ETEXT.LD)
            WS.LG.GL     = R.LD<LD.CATEGORY>
            IF  WS.LG.GL EQ 21096 THEN
                WS.LG.LIMIT = R.LD<LD.LIMIT.REFERENCE>
                IF  WS.LI.PRD.SRL EQ WS.LG.LIMIT THEN
                    WS.LG.CY  = R.LD<LD.CURRENCY>
                    IF  WS.LG.CY EQ WS.LI.USED.CY THEN
                        WS.STATUS = R.LD<LD.STATUS>
                        IF WS.STATUS NE 'LIQ' THEN
                            WS.LG.AMT += R.LD<LD.AMOUNT>
                        END
                    END
                END
            END
        END
    REPEAT
    WS.LG.AMT = WS.LG.AMT * -1
RETURN
*-----------------------------------------------------
END
