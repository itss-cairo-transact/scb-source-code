* @ValidationCode : MjozMjAyMjE3NDE6Q3AxMjUyOjE2NDE4MDkyNzE1MjE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:07:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>648</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBM.UNDERAGE.SAVE.OFS.D

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCR.ACCT.CR
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ACCT.UNDERAGE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STMT.ACCT.SAVE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS


*==================== OPEN TABLES ================================
    FN.CU  = 'FBNK.CUSTOMER'        ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC  = 'FBNK.ACCOUNT'         ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.ST  = 'FBNK.STMT.ACCT.CR'    ; F.ST = '' ; R.ST = ''
    CALL OPF(FN.ST,F.ST)
    FN.SA  = 'F.SCB.STMT.ACCT.SAVE' ; F.SA = '' ; R.SA = ''
    CALL OPF(FN.SA,F.SA)
    FN.U   = 'F.SCB.ACCT.UNDERAGE'  ; F.U = '' ; R.U = ''
    CALL OPF(FN.U,F.U)
    FN.ACR   = 'FBNK.ACCR.ACCT.CR'  ; F.ACR = '' ; R.ACR = ''
    CALL OPF(FN.ACR,F.ACR)

*========================= DEFINE VARIABLES =========================
    COMP       = ID.COMPANY
    COM.CODE   = COMP[8,2]
    TODAY.DATE = TODAY
    DATE.B    = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    CALL CDT("",DATE.B,'-1W')
    DATE.B4    = DATE.B
    B4.YEAR.MONTH = DATE.B4[1,6]

*========================= OFS VARIABLES ============================
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

*========================= GET ALL SAVING ACCOUNTS ==================

    T.SEL  = "SELECT F.SCB.STMT.ACCT.SAVE WITH PAY.FLAG EQ '' AND @ID LIKE ...-":B4.YEAR.MONTH:" AND CO.CODE EQ EG0010011"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*=========================  ===========================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.SA,KEY.LIST<I>, R.SA, F.SA, ETEXT)
            ACCT.NO    = KEY.LIST<I>[1,16]
            LIQ.CCY    = R.SA<STMT.SAVE.LIQUIDITY.CCY>
            INT.AMT    = R.SA<STMT.SAVE.CR.INT.AMT>

            CALL F.READ( FN.AC,ACCT.NO, R.AC, F.AC, ETEXT)
            CUST.NO     = R.AC<AC.CUSTOMER>
            CALL F.READ( FN.CU,CUST.NO, R.CU, F.CU, ETEXT)

            CUST.COMP   = R.CU<EB.CUS.COMPANY.BOOK>
            
            DEPT.CODE   = R.CU<EB.CUS.ACCOUNT.OFFICER>
            NO.MONTHS   = R.AC<AC.LOCAL.REF,ACLR.INT.NO.MONTHS>
            TOT.AMT     = 0
            ACCT.SAVE   = ACCT.NO:YEAR:MONTH
            ACCT        = KEY.LIST<I>
            ACCT.SPARE  = ACCT.NO:'-':DATE.B4

            IF NO.MONTHS GE 3 THEN

                Z.SEL  = "SELECT F.SCB.STMT.ACCT.SAVE WITH PAY.FLAG EQ '' AND @ID LIKE ":ACCT.NO:"... AND CO.CODE EQ ":CUST.COMP
                CALL EB.READLIST(Z.SEL, KEY.LIST2, "", SELECTED2, ASD2)

                IF  SELECTED2 NE 0 THEN
                    FOR K = 1 TO  SELECTED2
                        CALL F.READ( FN.SA,KEY.LIST2<K>, R.SA, F.SA, ETEXT3)
                        ACCT.COMPLETE = KEY.LIST2<K>
                        TOT.AMT       = TOT.AMT + R.SA<STMT.SAVE.CR.INT.AMT>
                        R.SA<STMT.SAVE.PAY.FLAG>       = 'PAID'
                        CALL F.WRITE(FN.SA,ACCT.COMPLETE,R.SA)
                        CALL JOURNAL.UPDATE(ACCT.COMPLETE)

                    NEXT K
                END

*=========================== OFS ======================
                OFS.ID = "UN.D.":TNO:".":ACCT.NO:YEAR:MONTH
                GOSUB WRITE.OFS

*==================== UPDATE NO OF MONTHS IN ACCOUNT TABLE ======
                START.DATE = DATE.B4
*=================
                R.AC<AC.LOCAL.REF,ACLR.INT.NO.MONTHS>   = ' '
                R.AC<AC.LOCAL.REF,ACLR.START.SAVE.DATE> = ' '
                R.AC<AC.LOCAL.REF,ACLR.INTEREST.AMOUNT> = ' '
                GOSUB WRITE.ACCT.OFS
            END

            OFS.ID = "UN.D.SA":TNO:".":ACCT.NO:YEAR:MONTH
            GOSUB WRITE.SPARE.OFS

        NEXT I
    END

RETURN
*========================== WRITE OFS ===========================
WRITE.OFS:

    COMMA = ","
    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = '0':DEPT.CODE
    END
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
    BNK.SPARE.ACCT = LIQ.CCY:'11077000200':DEPT.CODE
**    OFS.USER.INFO     = "INPUTT":DEPT.CODE:"/":"/":CUST.COMP
    OFS.USER.INFO     = "AUTO.LD":"/":"/":CUST.COMP

    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":'AC19':COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": LIQ.CCY:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":LIQ.CCY:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":BNK.SPARE.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":ACCT.NO:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.AMT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB':COMMA
    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":'SCB':COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":DEPT.CODE:COMMA

    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY.DATE

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
RETURN

*======================= WRITE OFS FOR SPARE =========================
WRITE.SPARE.OFS:

    COMMA = ","
    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = '0':DEPT.CODE
    END
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
    BNK.SPARE.ACCT = LIQ.CCY:'11077000200':DEPT.CODE
**    OFS.USER.INFO     = "INPUTT":DEPT.CODE:"/":"/":CUST.COMP
    OFS.USER.INFO     = "AUTO.LD":"/":"/":CUST.COMP

    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":'AC18':COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": LIQ.CCY:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":LIQ.CCY:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":'PL50000':COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":BNK.SPARE.ACCT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": INT.AMT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY.DATE:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB':COMMA
    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":'SCB':COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":DEPT.CODE:COMMA

    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY.DATE

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
RETURN

*======================= WRITE OFS FOR UPDATING ACCOUNT ==================
WRITE.ACCT.OFS:
    COMMA = ","
    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = '0':DEPT.CODE
    END
    OFS.OPERATION      = "ACCOUNT"
    OFS.OPTIONS        = "SCB.UNDER"
**    OFS.USER.INFO      = "INPUTT":DEPT.CODE:"/":"/":CUST.COMP
    OFS.USER.INFO      = "AUTO.LD":"/":"/":CUST.COMP

    OFS.MESSAGE.DATA   ="INT.NO.MONTHS=":' ':COMMA
    OFS.MESSAGE.DATA  :="START.SAVE.DATE=":' ':COMMA
    OFS.MESSAGE.DATA  :="INTEREST.AMOUNT=":' ':COMMA

    OFS.ID = 'D.':ACCT.NO
    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ACCT.NO:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY.DATE

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
RETURN
END
