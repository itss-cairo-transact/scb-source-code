* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNjM4MTI6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.CHQ.AGENDA

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
** REPORT.ID='SBR.CHQ.AGENDA'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CA = 'F.SCB.CHEQ.APPL' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    WS.CHQ.COUNT = 0

    T.SEL2 = "SELECT F.COMPANY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN

        FOR NN = 1 TO SELECTED2
            COMP = KEY.LIST2<NN>
            TOTAL.BOOKS = 0
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,COM.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COM.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

            WS.DESC.1 = '����� ���� ��� ���� ������'
            WS.DESC.2 = '��� :' :COM.NAME
            WS.DESC.3 = '���� ���� ���� ,,,'
            WS.DESC.4 = '����� ��� ���� �������� ����� ������� �������� ���� ���� ������ ������ �����'

            GOSUB PROCESS
        NEXT NN
    END

    RETURN
*========================================================================
PROCESS:

    T.SEL = "SELECT ":FN.CA:" WITH SEND.TO.BRN.DATE EQ '' AND CHARGE.DATE NE '' AND CO.CODE EQ ":COMP:" AND CHQ.NO.START NE '' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED GT 0 THEN
        FOR I = 1 TO SELECTED

            IF I = 1 THEN GOSUB PRINT.HEAD
            IF I = 16 THEN
                WS.DESC.1 = ''
                WS.DESC.2 = ''
                WS.DESC.3 = ''
                WS.DESC.4 = ''
                GOSUB PRINT.HEAD
            END
            CALL F.READ(FN.CA,KEY.LIST<I>,R.CA,F.CA,E1)
            CUS.ID          = R.CA<CHQA.CUST.NO>
            CUST.NAME       = R.CA<CHQA.CUST.NAME>
            WS.BOOKS.NO     = R.CA<CHQA.NO.OF.BOOKS>
            WS.APP.DATE     = R.CA<CHQA.APP.DATE>
            WS.APP.DATE     = FMT(WS.APP.DATE,"####/##/##")
            WS.CHQ.TYPE     = R.CA<CHQA.CHEQ.TYPE>
            TOTAL.BOOKS    += R.CA<CHQA.NO.OF.BOOKS>

            XX  = SPACE(132)
            XX<1,1>[1,15]   = CUS.ID
            XX<1,1>[20,35]  = CUST.NAME
            XX<1,1>[60,10]  = WS.BOOKS.NO
            XX<1,1>[80,10]  = WS.CHQ.TYPE
            XX<1,1>[97,20]  = WS.APP.DATE

            PRINT XX<1,1>

            WS.CHQ.COUNT = 0

            XX = SPACE(132)

            FOR X = 1 TO WS.BOOKS.NO
                WS.CHQ.NO.START1 = R.CA<CHQA.CHQ.NO.START> + WS.CHQ.COUNT
                WS.CHQ.NO.START  = FMT(WS.CHQ.NO.START1,'R%11')
                XX<1,1>[119,20]  = WS.CHQ.NO.START
                PRINT XX<1,1>
                WS.CHQ.COUNT += WS.CHQ.TYPE
            NEXT X

            PRINT STR('-',130)

            R.CA<CHQA.SEND.TO.BRN.DATE> = TODAY
            CALL F.WRITE(FN.CA,KEY.LIST<I>,R.CA)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)

        NEXT I

        XX1  = SPACE(132)
        PRINT STR('=',130)
        XX1<1,1>[20,35]  = '��� ������� ������� : ':TOTAL.BOOKS
        PRINT XX1<1,1>
        PRINT STR('=',130)
        PRINT STR(' ',130)

        XX6 = SPACE(132) ;   XX7  = SPACE(132) ;   XX8 = SPACE(132)
        XX9 = SPACE(132) ;   XX10 = SPACE(132)

        XX6<1,1>[1,80]   = '����� ������ ������� ������� �������� ������� ��� �������� ������'
        XX7<1,1>[1,60]   = '�������� ��� ���� �������� ��� ������ ������� �����'
        PRINT STR(' ',130)
        XX8<1,1>[55,35]  = '������� ����� ���� ��������'
        PRINT STR(' ',130)
        XX9<1,1>[85,15]  = '������ �� : ':T.DAY
        PRINT STR(' ',130)
        PRINT STR(' ',130)
        XX10<1,1>[85,15] = '���� ����� �������'

        PRINT XX6<1,1>
        PRINT XX7<1,1>
        PRINT XX8<1,1>
        PRINT XX9<1,1>
        PRINT XX10<1,1>

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        CALL PRINTER.ON(REPORT.ID,'')

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ����� ����� ":SPACE(1):T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":WS.DESC.1
    PR.HD :="'L'":WS.DESC.2
    PR.HD :="'L'":WS.DESC.3
    PR.HD :="'L'":WS.DESC.4
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(25):"��� �������":SPACE(10):"��� ������":SPACE(10):"����� �����":SPACE(10):"����� �������"
    PR.HD :="'L'":STR('_',130)

    HEADING PR.HD

    RETURN
*==============================================================
END
