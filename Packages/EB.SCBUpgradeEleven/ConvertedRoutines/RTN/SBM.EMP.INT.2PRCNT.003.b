* @ValidationCode : MjotMTI2NzEyNzgyMTpDcDEyNTI6MTY0MDc4NjMwMTQwMDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 15:58:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>59</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBM.EMP.INT.2PRCNT.003
*    PROGRAM SBM.EMP.INT.2PRCNT.003
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SCB.EMP.INT.2.TEMP
*---------------------------------------------------
    FN.TEMP = "F.SCB.EMP.INT.2.TEMP"
    F.TEMP = ""
*

*----------------------------------------------------
    CALL OPF (FN.TEMP,F.TEMP)
    R.TEMP = ""
*------------------------------------------------
    GOSUB A.100.PROC
RETURN
*-----------------------------------------
A.100.PROC:
    SEL.CMDTEMP = "SELECT ":FN.TEMP:" BY RLAT.NO BY RESERVED02"
    CALL EB.READLIST(SEL.CMDTEMP,SEL.LISTTEMP,"",NO.OF.RECTEMP,RET.CODETEMP)
    LOOP
        REMOVE WS.TEMP.ID FROM SEL.LISTTEMP SETTING POSTEMP
    WHILE WS.TEMP.ID:POSTEMP

        CALL F.READ(FN.TEMP,WS.TEMP.ID,R.TEMP,F.TEMP,MSG.TEMP)
        WS.RELATED.NO = R.TEMP<EMP.RLAT.NO>
        IF  WS.RELATED.NO EQ WS.TEMP.ID THEN
            WS.BAL  = R.TEMP<EMP.TOTAL.AMT>
            WS.COMP =  WS.RELATED.NO
            GOTO A.100.REP
        END
        IF  WS.RELATED.NO EQ WS.COMP THEN
            GOSUB A.300.UPDAT.RELATED
        END

A.100.REP:
    REPEAT
RETURN
*-------------------------------------------------
A.300.UPDAT.RELATED:
    R.TEMP<EMP.TOTAL.AMT> = WS.BAL

    CALL F.WRITE(FN.TEMP,WS.TEMP.ID,R.TEMP)
**  CALL  JOURNAL.UPDATE(WS.TEMP.ID)
RETURN
*--------------------------------------------------
END
