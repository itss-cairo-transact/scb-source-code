* @ValidationCode : MjotOTg0NjIxNzUxOkNwMTI1MjoxNjQwODYzOTYxNDg5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 13:32:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
****CREATED BY NOHA 26-12-2016 ******
SUBROUTINE SBR.DEL.ACH
*PROGRAM SBR.DEL.ACH

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ACH.PACS008
*--------------------------------------

    FN.ACH = 'F.SCB.ACH.PACS008' ; F.ACH = ''
    CALL OPF(FN.ACH,F.ACH)
    YTEXT = "Enter ACH Date: "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    ACH.DATE = COMI
    YTEXT = "Enter Value Date: "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    SETT.DATE = COMI

*--------------------------------------
    SET.DATE = SETT.DATE[1,4]:'-':SETT.DATE[5,2]:'-':SETT.DATE[7,2]

    T.SEL = "SELECT F.SCB.ACH.PACS008 WITH PACS.TYPE EQ 1 AND REQ.SETTLEMENT.DATE EQ ":SET.DATE:" AND @ID LIKE ":ACH.DATE:"... AND TRN.DATE EQ ''"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    TEXT = "There are ":SELECTED:" ACH records"; CALL REM
    YTEXT = "1.DELETE  2.CANCEL"
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    OP.CODE = COMI

    IF OP.CODE EQ 1  THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACH,KEY.LIST<I>,R.ACH,F.ACH,ERR.ACH)
            CALL F.DELETE (FN.ACH,KEY.LIST<I>)
        NEXT I
    END

*--------------------------------------
RETURN
END
