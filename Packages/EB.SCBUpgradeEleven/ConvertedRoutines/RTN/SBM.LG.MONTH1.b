* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNTY4MDM6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.LG.MONTH1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB CALL.FOREIGN
*Line [ 46 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 47 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALL.LOCAL
*Line [ 48 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 50 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LG.MONTH1'
    CALL PRINTER.ON(REPORT.ID,'')

    LG.CATEG = ''
    LG.AMT = ''
    CUR =''
    LOCAL.AMT.LOCAL = ''
    LOCAL.AMT = ''
    LOCAL.AMT.FOREIGN = 0
    WW = 0
    KK = 0
    SS = 0

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)
    COMP = ID.COMPANY
    RETURN
*===============================================================
CALL.FOREIGN:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    CUR.SEL = "SELECT FBNK.CURRENCY"
    CUR.KEY.LIST ="" ; CUR.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUR.SEL,CUR.KEY.LIST,"",CUR.SELECTED,ER.MSG)
    TDD = TODAY
    FOR K = 1 TO CUR.SELECTED
        T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ '21096' AND STATUS NE 'LIQ' AND WITH CURRENCY EQ ":CUR.KEY.LIST<K>:" AND CO.CODE EQ ":COMP
        KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED > 0 THEN GOSUB GET.RECORDS.FOREIGN
    NEXT K

    RETURN
*-----------------------------------
GET.RECORDS.FOREIGN:
    TOT.LG.LOCAL = ''
    TOT.LG.FOREIGN = ''

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LG.CATEG<I> =R.LD<LD.CATEGORY>
            LG.AMT<I> =R.LD<LD.AMOUNT>
            CUR<I>=R.LD<LD.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 106 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR<I> IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            MYRATE = R.CCY<RE.BCP.RATE,POS>

            IF CUR<1> = CUR<I> THEN
* IF LG.CATEG<I> = "21096" THEN TOT.LG.LOCAL = TOT.LG.LOCAL + LG.AMT<I>
                IF LG.CATEG<I> = "21097" THEN TOT.LG.FOREIGN = TOT.LG.FOREIGN + LG.AMT<I>
            END

        NEXT I
        QQ = TOT.LG.LOCAL + TOT.LG.FOREIGN
        HH = QQ * MYRATE
        LOCAL.AMT.FOREIGN = LOCAL.AMT.FOREIGN + HH


    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END


    RETURN
*==============================================================
CALL.LOCAL:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    CUR.SEL = "SELECT FBNK.CURRENCY"
    CUR.KEY.LIST ="" ; CUR.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUR.SEL,CUR.KEY.LIST,"",CUR.SELECTED,ER.MSG)

    FOR K = 1 TO CUR.SELECTED
        COMP = ID.COMPANY
        T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ '21096' AND STATUS NE 'LIQ' AND WITH CURRENCY EQ ":CUR.KEY.LIST<K>:" AND CO.CODE EQ ":COMP
        KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED > 0 THEN GOSUB GET.RECORDS.LOCAL
    NEXT K

    RETURN
*-----------------------------------
GET.RECORDS.LOCAL:

    TOT.LG.LOCAL = ''
    TOT.LG.FOREIGN = ''

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LG.CATEG<I> =R.LD<LD.CATEGORY>
            LG.AMT<I> =R.LD<LD.AMOUNT>
            CUR<I>=R.LD<LD.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 160 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR<I> IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            MYRATE = R.CCY<RE.BCP.RATE,POS>

            IF CUR<1> = CUR<I> THEN
                IF LG.CATEG<I> = "21096" THEN TOT.LG.FOREIGN = TOT.LG.FOREIGN + LG.AMT<I>
            END

        NEXT I
        QQ = TOT.LG.LOCAL + TOT.LG.FOREIGN
        HH = QQ * MYRATE
        LOCAL.AMT.LOCAL = LOCAL.AMT.LOCAL + HH


    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END


    RETURN
*===============================================================
CALLDB:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    CUR.SEL = "SELECT FBNK.CURRENCY"
    CUR.KEY.LIST ="" ; CUR.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUR.SEL,CUR.KEY.LIST,"",CUR.SELECTED,ER.MSG)

    FOR K = 1 TO CUR.SELECTED
        COMP = ID.COMPANY
        T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ '21096' AND STATUS NE 'LIQ' AND CURRENCY EQ ":CUR.KEY.LIST<K>:" AND CO.CODE EQ ":COMP

        KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED > 0 THEN GOSUB GET.RECORDS
    NEXT K

    PRINT XX<50,4>
    PRINT XX<50,8>
    RETURN
*-----------------------------------
GET.RECORDS:
    TOT.LG.LOCAL = ''
    TOT.LG.FOREIGN = ''


    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LG.CATEG<I> =R.LD<LD.CATEGORY>
            LG.AMT<I> =R.LD<LD.AMOUNT>
            CUR<I>=R.LD<LD.CURRENCY>
*Line [ 216 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR<I>,CUR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR<I>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 217 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR<I> IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            MYRATE = R.CCY<RE.BCP.RATE,POS>

            IF CUR<1> = CUR<I> THEN
                IF LG.CATEG<I> = "21096" THEN TOT.LG.LOCAL = TOT.LG.LOCAL + LG.AMT<I>
                IF LG.CATEG<I> = "21097" THEN TOT.LG.FOREIGN = TOT.LG.FOREIGN + LG.AMT<I>
            END

            QQ = TOT.LG.LOCAL + TOT.LG.FOREIGN
            HH = QQ * MYRATE
            LOCAL.AMT = LOCAL.AMT + HH

            WW = LOCAL.AMT.FOREIGN
            TT = LOCAL.AMT.LOCAL
* NET.AMT = HH - ( WW + TT )
            NET.AMT1 = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
            NET.AMT2 = NET.AMT1 * MYRATE<1,1>
            RR  = TOT.LG.LOCAL * MYRATE<1,1>
        NEXT I
        KK += RR
        SS += NET.AMT2
        NET.AMT = SS - KK

        XX = SPACE(120)
        XX<1,1>[1,10]   = CUR.NAME
        XX<1,1>[15,10]  = TOT.LG.LOCAL
        XX<1,1>[30,10]  = TOT.LG.FOREIGN
        XX<1,1>[55,10]  = TOT.LG.LOCAL
        XX<1,1>[70,5]   = MYRATE
        XX<1,1>[80,10]  = RR
        XX<50,4>[1,20]  = " ������� ���� ���� = ":KK

* XX<50,5>[1,20] = " ������ ���������� =":NET.AMT
* XX<50,6>[1,20] = "FOREIGN = ":WW
* XX<50,7>[1,20] = "LOCAL = ":TT
        XX<50,8>[1,20] =" ������ ���������� =":KK
        PRINT XX<1,1>
        PRINT

    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 272 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):"  ������ ������ ������ ��������"
    PR.HD :="'L'":SPACE(28):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):" ������" :SPACE(3):" �������� �������" :SPACE(5):" �������� ���������" :SPACE(5):" ��������" :SPACE(5): "�����  " :SPACE(5):"������� "
    PR.HD :="'L'":SPACE(1):STR('_',90)

    HEADING PR.HD
    RETURN
*======================================

END
