* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNjI0OTg6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>449</Rating>
*-----------------------------------------------------------------------------
*------------------------
*** Create By Nessma ***
*------------------------
    PROGRAM SBR.CALC.SAV.BAL.1000.50000

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CALC.SAV.BAL.ALL
*---------------------------------------------------
    GOSUB INTIAL
    GOSUB PROCESS
    GOSUB CALC.TOTALS
    GOSUB WRITE.TEMP
    RETURN
*----------------------------------------------------------
WRITE.TEMP:
*-----------
    WRITE R.TMP TO F.TMP , TMP.ID ON ERROR
    END
    RETURN
*----------------------------------------------------------
CALC.TOTALS:
*------------
    CATEGORY.LIST<1,1> = '6501'
    CATEGORY.LIST<1,2> = '6502'
    CATEGORY.LIST<1,3> = '6503'
    CATEGORY.LIST<1,4> = '6504'
    CATEGORY.LIST<1,5> = '6511'

    YY       = ""
    NUM.CUR  = 0
    CUR.COD  = ""
    CUR.NAME = ""
    CUR.NAMR = ""
    CURR     = ""
    CURRR    = ""
    CATEGORY.N = ""
    CATEGORY.C = ""
    NUM.COUNT  = 0

    CUR.SEL  = "SELECT ":FN.CNUM:" BY @ID"
    CALL EB.READLIST(CUR.SEL,CUR.LIST,"",SELECTED.CUR,ER.MSG)

    IF SELECTED.CUR THEN
        FOR KK = 1 TO 5
            FOR QQ = 1 TO SELECTED.CUR
                CUR.INDX   = CUR.LIST<QQ>
                CATEGORY.C = CATEGORY.LIST<1,KK>

*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.INDX,CUR)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CUR.INDX,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>

                IF CUR EQ 'EGP' THEN
                    NUM.CUR += TOT.BL.CATEG<CUR.INDX,CATEGORY.C>
                    NUM.COUNT += TOT.NO.CATEG<CUR.INDX,CATEGORY.C>
                END ELSE
                    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ECAA)
                    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    NUM.CUR += TOT.BL.CATEG<CUR.INDX,CATEGORY.C> * CUR.RATE
                    NUM.COUNT += TOT.NO.CATEG<CUR.INDX,CATEGORY.C>
                END
            NEXT QQ

            IF CATEGORY.LIST<1,KK> EQ '6501' THEN
                R.TMP<SAV.BAL.MONTH.6501.COUNT> = NUM.CUR
            END
            IF CATEGORY.LIST<1,KK> EQ '6502' THEN
                R.TMP<SAV.BAL.MONTH.6502.COUNT> = NUM.CUR
            END
            IF CATEGORY.LIST<1,KK> EQ '6503' THEN
                R.TMP<SAV.BAL.MONTH.6503.COUNT> = NUM.CUR
            END
            IF CATEGORY.LIST<1,KK> EQ '6504' THEN
                R.TMP<SAV.BAL.MONTH.6504.COUNT> = NUM.CUR
            END
            IF CATEGORY.LIST<1,KK> EQ '6511' THEN
                R.TMP<SAV.BAL.MONTH.6511.COUNT> = NUM.CUR
            END
            NUM.CUR = 0
        NEXT KK
    END

    XX1  = R.TMP<SAV.BAL.MONTH.6501.COUNT>
    XX2  = R.TMP<SAV.BAL.MONTH.6502.COUNT>
    XX3  = R.TMP<SAV.BAL.MONTH.6503.COUNT>
    XX4  = R.TMP<SAV.BAL.MONTH.6504.COUNT>
    XX11 = R.TMP<SAV.BAL.MONTH.6511.COUNT>

    TOT.XX = XX1 + XX2 + XX3 + XX4 + XX11
    R.TMP<SAV.BAL.MONTH.ALL.COUNT> = TOT.XX
    R.TMP<SAV.BAL.NO.OF.ACCOUNTS>  = NUM.COUNT
    RETURN
*-------------------------------------------------------------------
INTIAL:
*------
    FN.AC  = 'FBNK.ACCOUNT'        ;  F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER'       ;  F.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.COMP = 'F.COMPANY'          ;  F.COMP  = ''
    CALL OPF(FN.COMP,F.COMP)

    FN.CCC  = 'FBNK.CURRENCY'      ;  F.CCC  = ''
    CALL OPF(FN.CCC,F.CCC)

    FN.CNUM  = 'F.NUMERIC.CURRENCY'     ;  F.CNUM  = ''
    CALL OPF(FN.CNUM,F.CNUM)

    FN.CUR  = 'FBNK.CURRENCY'      ; F.CUR  = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.TMP  = 'F.SCB.CALC.SAV.BAL.ALL'    ; F.TMP  = ''
    CALL OPF(FN.TMP,F.TMP)

    ENQ.LP   = 0
    CUR.RATE = ""
    COMP.OLD = ""
    COMP.NEW = ""
    CATEG    = ""
    CUR      = ""
    TOT.NO   = 0
    TOT.BAL  = 0
    BAL      = 0
    BREAK.2  = ""
    BREAK.1  = ""

    XX = ""
    YY = ""
    ZZ = ""
    NN = 1

    TOT.NO.CUR    = 0
    TOT.BL.CUR    = 0
    TOT.NO.CATEG  = 0
    TOT.BL.CATEG  = 0
    CATEGORY.LIST = ""

    TMP.ID         = ""
    TOTAL.SELECTED = 0
    RETURN
*---------------------------------------------------
PROCESS:
*-------
    ALL.BAL = 0
    ALL.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (6501 6502 6503 6511)"
    CALL EB.READLIST(ALL.SEL,ALL.LIST,"",ALL.SELECTED,ER.ALL)

    IF ALL.SEL THEN
        FOR NN = 1 TO ALL.SELECTED
            CALL F.READ(FN.AC,ALL.LIST<NN>,R.AC.NN,F.AC,E.AC.NN)
            ALL.BAL += R.AC.NN<AC.OPEN.ACTUAL.BAL>
        NEXT NN
    END

    T.SEL   = "SELECT ":FN.AC:" WITH CATEGORY"
    T.SEL  := " IN ( 6501 6502 6503 6504 6511 )"
    T.SEL  := " BY CO.CODE BY CATEGORY BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
*           TMP.ID = "SBR.1000.50000"
            TMP.ID = "E"
            CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,E.TMP)
            R.TMP<SAV.BAL.BAYAN.TXT>        = "���� �� 1,000 ���� �� 50,000"
            R.TMP<SAV.BAL.NUMBER.ALL.COUNT> = ALL.SELECTED
            R.TMP<SAV.BAL.TOTAL.ALL.COUNT>  = ALL.BAL
            R.TMP<SAV.BAL.RUN.DATE>         = TODAY

            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E.AC)
            BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            CUR  = R.AC<AC.CURRENCY>
            CUS  = R.AC<AC.CUSTOMER>
            CATG = R.AC<AC.CATEGORY>
            CUR.CODE = KEY.LIST<I>[9,2]

            COMP.NEW  = R.AC<AC.CO.CODE>
            BREAK.2   = COMP.NEW :"*": CATG :"*": CUR

            IF I EQ 1 THEN
                BREAK.1 = BREAK.2
            END

            IF BREAK.1 EQ BREAK.2 THEN
                IF CUR EQ 'EGP' THEN
                    IF BAL GT 1000 AND BAL LE 50000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END ELSE
                    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ECAA)
                    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    BAL      = BAL * CUR.RATE

                    IF BAL GT 1000 AND BAL LE 50000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END
            END ELSE
*-------------------------------------
                GOSUB PRINT.LINE
*-------------------------------------
                IF CUR EQ 'EGP' THEN
                    IF BAL GT 1000 AND BAL LE 50000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END ELSE
                    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ECAA)
                    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    BAL      = BAL * CUR.RATE

                    IF BAL GT 1000 AND BAL LE 50000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END
            END

            BREAK.1 = BREAK.2
        NEXT I
    END ELSE
*        PRINT "NO DATA"
    END
    RETURN
*-------------------------------------------------------
PRINT.LINE:
*----------
    XX = ""

    COMP.ID = FIELD(BREAK.1 ,"*", 1)
*Line [ 295 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COMP.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    XX<1,1>[1,20] = COMP.NAME

    CATEG.CODE    = FIELD(BREAK.1 ,"*", 2)
*Line [ 305 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG.CODE,CATEG.CODE.2)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG.CODE,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.CODE.2=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
    XX<1,1>[22,6] = CATEG.CODE.2

    CUR.ID = FIELD(BREAK.1 ,"*", 3)
*Line [ 315 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.ID,CUR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    XX<1,1>[53,15]  = CUR.NAME
    XX<1,1>[85,20]  = TOT.NO
    TOT.BFR = TOT.BAL
    TOT.BAL = FMT(TOT.BAL, "L2,")
    XX<1,1>[110,20] = TOT.BAL

    IF TOT.NO GT 0 THEN
*       PRINT XX<1,1>
*       PRINT " "
        XX<1,1> = ""
    END

    TOT.NO  = 0
    TOT.BAL = 0
    RETURN
*-------------------------------------------------------
END
