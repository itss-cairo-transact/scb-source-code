* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.1900.1A
*    PROGRAM     SBM.C.PRT.1900.1A
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
***����� ��� 1900
***����
*** 1 AND 2
*------------------------------------------
    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ""
    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""
    FN.COMP = "F.COMPANY"
    F.COMP = ""
*------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CUS,F.CUS)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.FRST = 0
    WS.CUS.KEY = ""
    MSG.CUS = ""
    WS.CUS.NAME = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.FLAG.PRT = 0
    WS.1.LE = "0"
    WS.2.LE = "0"
    WS.3.LE = "0"
    WS.4.LE = "0"

    WS.1.EQV = "0"
    WS.2.EQV = "0"
    WS.3.EQV = "0"
    WS.4.EQV = "0"
    WS.4.TOT.LE = 0
    WS.4.TOT.EQV = 0
    WS.LINE.TOT.GRND = 0
    WS.4.ARRAY.TOT.LE = 0
    WS.4.ARRAY.TOT.EQV = 0
    WS.ARRAY.LINE.TOT = 0

    WS.DPST = "0"
    WS.DPST1 = "0"
    WS.CER = "0"
    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.HD.T  = "����� ����� ������� � ��������� ���������� � ������� ��������"
    WS.HD.TA = " ����� ��� 1900 "

    WS.HD.T2 = "�������� ������� ������ ����������"
    WS.HD.T2A = "����    9"
    WS.HD.T2A = "1":"����    "
    WS.HD.T3  = "������ ����� ���  "

    WS.HD.1  = "���� �����"
    WS.HD.1A = "���� ������"
    WS.HD.1B = "��������"
*------------------------------

    WS.HD.2  = "��������� ����������"
*-----------------------------------
    WS.HD.3 = "� ������� �������� ��������"
*--------------------------------------
    WS.PRG.1 = "SBM.C.PRT.1900.1A"
*------------------------------------------------
*����� ������� ��������
*------------------------------------------------
********    ARRAY1 = ""
    DIM ARRAY1(4,10)


    ARRAY1(1,1)  = " ������ ������� ������� ���������  "
    ARRAY1(1,2) = "8001"
    ARRAY1(1,3) = "0"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"
    ARRAY1(1,6) = "0"
    ARRAY1(1,7) = "0"
    ARRAY1(1,8) = "0"
    ARRAY1(1,9) = "0"
    ARRAY1(1,10) = "0"

    ARRAY1(2,1)  = "  ���� ��� ���� ���������          "
    ARRAY1(2,2) = "8002"
    ARRAY1(2,3) = "0"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = "0"
    ARRAY1(2,7) = "0"
    ARRAY1(2,8) = "0"
    ARRAY1(2,9) = "0"
    ARRAY1(2,10) = "0"

    ARRAY1(3,1)  = "������ ������� ������             "
    ARRAY1(3,2) = "8003"
    ARRAY1(3,3) = "0"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"
    ARRAY1(3,8) = "0"
    ARRAY1(3,9) = "0"
    ARRAY1(3,10) = "0"

    ARRAY1(4,1)  = "������ ������� ������� ��� �������"
    ARRAY1(4,2) = "8004"
    ARRAY1(4,3) = "0"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"
    ARRAY1(4,8) = "0"
    ARRAY1(4,9) = "0"
    ARRAY1(4,10) = "0"

********************** ********************************
*******************   PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    GOSUB A.050.GET.ALL.BR
*------------------------------------------START PROCESSING
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*-------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE 99 THEN
            GOTO A.050.A
        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        WS.ARRY.RAW = 1
        WS.ARRY.COL = 1
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0

            GOSUB A.500.PRT.TOT
            GOSUB A.510.PRT.ARRAY
            WS.FRST = 0
        END
A.050.A:
    REPEAT
    RETURN
*-------------------------------------------------
A.100.PROCESS:
    IF WS.BR NE 99  THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE BB... AND CBE.BR EQ ":WS.BR
    END
    IF WS.BR EQ 99  THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE BB..."
    END

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS
        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        WS.TMP = R.CBE<P.CBE.BR>

        IF WS.BR = 99 THEN
            GOTO A.100.A
        END

        IF WS.TMP NE WS.BR THEN
            GOTO START.B
        END
A.100.A:
        WS.INDSTRYA = R.CBE<P.CBE.NEW.SECTOR>
        IF WS.INDSTRYA EQ 0 THEN
            GOTO START.B
        END

        IF   WS.INDSTRYA EQ 1110 OR WS.INDSTRYA EQ 2110 OR WS.INDSTRYA EQ 3110 OR WS.INDSTRYA EQ 4110 THEN
            GOTO  START.A
        END

        IF   WS.INDSTRYA EQ 8001 OR WS.INDSTRYA EQ 8002 OR WS.INDSTRYA EQ 8003 OR WS.INDSTRYA EQ 8004 THEN
            GOTO  START.A
        END
        GOTO START.B
START.A:
        WS.1.LE = 0
        WS.2.LE = 0
        WS.3.LE = 0
        WS.4.LE = 0

        WS.1.EQV = 0
        WS.2.EQV = 0
        WS.3.EQV = 0
        WS.4.EQV = 0

        WS.DPST = 0
        WS.DPST1 = 0
        WS.CER = 0
*-------------------------------------------------
        WS.1.LE = R.CBE<P.CBE.CUR.AC.LE>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.FACLTY.LE.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.S.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.M.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.L.CR>

        WS.DPST  = R.CBE<P.CBE.DEPOST.AC.LE.1Y> + R.CBE<P.CBE.DEPOST.AC.LE.2Y>
        WS.DPST1 = R.CBE<P.CBE.DEPOST.AC.LE.3Y> + R.CBE<P.CBE.DEPOST.AC.LE.MOR3>
        WS.CER  = R.CBE<P.CBE.CER.AC.LE.3Y> + R.CBE<P.CBE.CER.AC.LE.5Y> + R.CBE<P.CBE.CER.AC.LE.GOLD>
        WS.2.LE = WS.DPST + WS.DPST1 + WS.CER + R.CBE<P.CBE.SAV.AC.LE>

        WS.3.LE = R.CBE<P.CBE.BLOCK.AC.LE> + R.CBE<P.CBE.MARG.LC.LE> + R.CBE<P.CBE.MARG.LG.LE>
        WS.4.LE = R.CBE<P.CBE.FACLTY.LE> + R.CBE<P.CBE.COMRCL.PAPER.LE>
*------------------------------------------------
        WS.1.EQV = R.CBE<P.CBE.CUR.AC.EQ>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.FACLTY.EQ.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.S.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.M.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.L.CR>
        WS.4.EQV = WS.4.EQV + R.CBE<P.CBE.LOANS.EQ.L>

        WS.DPST  = 0
        WS.DPST1 = 0
        WS.CER   = 0

        WS.DPST  = R.CBE<P.CBE.DEPOST.AC.EQ.1Y> + R.CBE<P.CBE.DEPOST.AC.EQ.2Y>
        WS.DPST1 = R.CBE<P.CBE.DEPOST.AC.EQ.3Y> + R.CBE<P.CBE.DEPOST.AC.EQ.MOR3>
        WS.CER   = R.CBE<P.CBE.CER.AC.EQ.3Y> + R.CBE<P.CBE.CER.AC.EQ.5Y> + R.CBE<P.CBE.CER.AC.EQ.GOLD>
        WS.2.EQV = WS.DPST + WS.DPST1 + WS.CER + R.CBE<P.CBE.SAV.AC.EQ>

        WS.3.EQV = R.CBE<P.CBE.BLOCK.AC.EQ> + R.CBE<P.CBE.MARG.LC.EQ> + R.CBE<P.CBE.MARG.LG.EQ>
        WS.4.EQV += R.CBE<P.CBE.FACLTY.EQ> + R.CBE<P.CBE.COMRCL.PAPER.EQ>
        IF   WS.INDSTRYA EQ 8001 OR WS.INDSTRYA EQ 8002 OR WS.INDSTRYA EQ 8003 OR WS.INDSTRYA EQ 8004 THEN
            GOSUB A.210.ACUM.WASTA.MALION
            GOTO  START.B
        END
        IF WS.FRST EQ 0 THEN
            GOSUB A.5000.PRT.HEAD
            WS.FRST = 1
        END
        GOSUB A.200.PRNT
*-----------------------------------------------------
START.B:
    REPEAT
BBB:
    RETURN
A.200.PRNT:
    MSG.CUS = ""
    WS.CUS.KEY = R.CBE<P.CBE.CUSTOMER.CODE>
    CALL F.READ(FN.CUS,WS.CUS.KEY,R.CUS,F.CUS,MSG.CUS)
    IF MSG.CUS EQ "" THEN
        WS.CUS.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
    END
    IF MSG.CUS NE "" THEN
        WS.CUS.NAME = "***********************************"
    END
    WS.1.LE = WS.1.LE / 1000
    WS.2.LE = WS.2.LE / 1000
    WS.3.LE = WS.3.LE / 1000
    WS.4.LE = WS.4.LE / 1000

    WS.4.TOT.LE = WS.4.TOT.LE + WS.4.LE

    WS.1.EQV = WS.1.EQV / 1000
    WS.2.EQV = WS.2.EQV / 1000
    WS.3.EQV = WS.3.EQV / 1000
    WS.4.EQV = WS.4.EQV / 1000

    WS.4.TOT.EQV = WS.4.TOT.EQV + WS.4.EQV

*    WS.LINE.TOT = WS.1.LE + WS.2.LE + WS.3.LE + WS.4.LE
*    WS.LINE.TOT = WS.LINE.TOT + WS.1.EQV + WS.2.EQV + WS.3.EQV + WS.4.EQV
    WS.LINE.TOT = WS.4.LE + WS.4.EQV

    WS.LINE.TOT.GRND = WS.LINE.TOT.GRND + WS.LINE.TOT
    XX = SPACE(132)
    XX<1,1>[1,35]   = WS.CUS.NAME
    XX<1,1>[37,9]   = FMT(WS.4.LE, "R0")
    XX<1,1>[47,9]   = FMT(WS.4.EQV, "R0")
    XX<1,1>[57,9]   = FMT(WS.LINE.TOT, "R0")

    PRINT XX<1,1>
    WS.FLAG.PRT = 1
    RETURN
A.210.ACUM.WASTA.MALION:
    FOR I = 1 TO 4
        GOSUB A.220.CHK.INDSTRY
    NEXT I
    RETURN

A.220.CHK.INDSTRY:
    IF  WS.INDSTRYA NE ARRAY1(I,1) THEN
        RETURN
    END
****        ARRAY          ������� ��� ��
    WS.1.LE = WS.1.LE / 1000
    WS.2.LE = WS.2.LE / 1000
    WS.3.LE = WS.3.LE / 1000
    WS.4.LE = WS.4.LE / 1000

    WS.1.EQV = WS.1.EQV / 1000
    WS.2.EQV = WS.2.EQV / 1000
    WS.3.EQV = WS.3.EQV / 1000
    WS.4.EQV = WS.4.EQV / 1000


    ARRAY1(I,3) = ARRAY1(I,3) + WS.1.LE
    ARRAY1(I,4) = ARRAY1(I,4) + WS.2.LE
    ARRAY1(I,5) = ARRAY1(I,5) + WS.3.LE
    ARRAY1(I,9) = ARRAY1(I,9) + WS.4.LE

    ARRAY1(I,6) = ARRAY1(I,6) + WS.1.EQV
    ARRAY1(I,7) = ARRAY1(I,7) + WS.2.EQV
    ARRAY1(I,8) = ARRAY1(I,8) + WS.3.EQV
    ARRAY1(I,10) = ARRAY1(I,10) + WS.4.EQV
    RETURN

A.500.PRT.TOT:
    XX = SPACE(132)
    XX<1,1>[37,9]   = "---------"
    XX<1,1>[47,9]   = "---------"
    XX<1,1>[57,9]   = "---------"
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[1,35]   = "������ ������� ������ ����������"
    XX<1,1>[37,9]   = FMT(WS.4.TOT.LE, "R0")
    XX<1,1>[47,9]   = FMT(WS.4.TOT.EQV, "R0")
    XX<1,1>[57,9]   = FMT(WS.LINE.TOT.GRND, "R0")

    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[37,9]   = "---------"
    XX<1,1>[47,9]   = "---------"
    XX<1,1>[57,9]   = "---------"
    PRINT XX<1,1>
    RETURN
*--------------------------------------------------------------------
A.510.PRT.ARRAY:
    XX = SPACE(132)
    XX<1,1>[1,35]   = "������� ��������               "
    PRINT XX<1,1>
    GOSUB A.520.PRT.FROM.ARRAY

    XX = SPACE(132)
    XX<1,1>[37,9]   = "---------"
    XX<1,1>[47,9]   = "---------"
    XX<1,1>[57,9]   = "---------"
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[1,35]   = "������ ������� ��������        "
    XX<1,1>[37,9]   = FMT(WS.4.ARRAY.TOT.LE, "R0")
    XX<1,1>[47,9]   = FMT(WS.4.ARRAY.TOT.EQV, "R0")
    XX<1,1>[57,9]   = FMT(WS.ARRAY.LINE.TOT, "R0")
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[37,9]   = "========="
    XX<1,1>[47,9]   = "========="
    XX<1,1>[57,9]   = "========="
    PRINT XX<1,1>
    WS.FINAL.4.LE = WS.4.TOT.LE + WS.4.ARRAY.TOT.LE
    WS.FINAL.4.EQV = WS.4.TOT.EQV + WS.4.ARRAY.TOT.EQV
    WS.FINAL.LINE = WS.LINE.TOT.GRND + WS.ARRAY.LINE.TOT

    XX = SPACE(132)
    XX<1,1>[1,35]   = "��������                        "
    XX<1,1>[37,9]   = FMT(WS.FINAL.4.LE, "R0")
    XX<1,1>[47,9]   = FMT(WS.FINAL.4.EQV, "R0")
    XX<1,1>[57,9]   = FMT(WS.FINAL.LINE, "R0")
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[37,9]   = "========="
    XX<1,1>[47,9]   = "========="
    XX<1,1>[57,9]   = "========="
    PRINT XX<1,1>

    WS.4.TOT.LE = 0
    WS.4.TOT.EQV = 0
    WS.LINE.TOT.GRND = 0
    WS.4.ARRAY.TOT.LE = 0
    WS.4.ARRAY.TOT.EQV = 0
    WS.ARRAY.LINE.TOT = 0
    WS.FINAL.4.LE = 0
    WS.FINAL.4.EQV = 0
    WS.FINAL.LINE = 0
    RETURN

A.520.PRT.FROM.ARRAY:
    FOR I = 1 TO 4
        XX = SPACE(132)
        WS.LE.LINE.TOT = 0
        WS.EQV.LINE.TOT = 0
        WS.4.ARRAY.TOT.LE = WS.4.ARRAY.TOT.LE + ARRAY1(I,9)
        WS.4.ARRAY.TOT.EQV = WS.4.ARRAY.TOT.EQV + ARRAY1(I,10)
        WS.ARRAY.LINE =  ARRAY1(I,3) + ARRAY1(I,4) + ARRAY1(I,5)
        WS.ARRAY.LINE =  WS.ARRAY.LINE +  ARRAY1(I,6) + ARRAY1(I,7) + ARRAY1(I,8)
        WS.ARRAY.LINE =  WS.ARRAY.LINE +  ARRAY1(I,9) + ARRAY1(I,10)
        WS.ARRAY.LINE.TOT = WS.ARRAY.LINE.TOT + WS.ARRAY.LINE

        XX<1,1>[1,35]   = ARRAY1(I,1)
        XX<1,1>[37,9]   = FMT(ARRAY1(I,9), "R0")
        XX<1,1>[47,9]   = FMT(ARRAY1(I,10), "R0")
        XX<1,1>[57,9]   = FMT(WS.ARRAY.LINE, "R0")
        PRINT XX<1,1>
    NEXT I
    RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    PR.HD :="'L'":SPACE(50):WS.HD.T2:SPACE(28):WS.HD.T2A
    PR.HD :="'L'":SPACE(110):WS.HD.T3
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(36):WS.HD.2
    PR.HD :="'L'":SPACE(36):WS.HD.3
    PR.HD :="'L'":SPACE(36):WS.HD.1:SPACE(5):WS.HD.1A:SPACE(3):WS.HD.1B
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN
END
