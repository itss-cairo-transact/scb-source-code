* @ValidationCode : Mjo3ODQ4OTM3NjI6Q3AxMjUyOjE2NDQ5MjUwNDkwMzE6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>25638</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBM.CREDIT.CBE.BAK2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CREDIT.CBE
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EMP.LOANS
*-----------------------------------------------------------------------
    GOSUB CLEARFILE
    PRINT "FILE IS CLEARED"

    GOSUB INITIATE
    GOSUB INIT0
    GOSUB INIT0CR

    GOSUB GETAC
    PRINT "AC FINISH"
    GOSUB GETLC
    PRINT "LC FINISH"
    GOSUB GETDR
    PRINT "DR FINISH"
    GOSUB GETLG
    PRINT "LG FINISH"
    GOSUB GETLOAN
    PRINT "LON FINISH"
    GOSUB GETPD
    PRINT "PD FINISH"
    GOSUB GETLIMT
    PRINT "LIMT FINISH"
    GOSUB CHKALL
    PRINT "FINISH CHKALL"
    GOSUB INSGUR
    PRINT "FINISH INSGUR"
RETURN
*==============================================================
CLEARFILE:
*=========
    FN.CCBE = "F.SCB.CREDIT.CBE"
    F.CCBE = ''
    CALL OPF(FN.CCBE,F.CCBE)
    OPEN FN.CCBE TO FILEVAR ELSE ABORT 201, FN.CCBE
    CLEARFILE FILEVAR

RETURN
*==============================================================
INITIATE:
*========
    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    R.AC = ''

    FN.CUS = 'F.CUSTOMER'
    F.CUS = ''

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CUS.AL = 'FBNK.CUSTOMER' ; F.CUS.AL = '' ; R.CUS.AL = '' ; ER.CUS.AL = ''
    CALL OPF(FN.CUS.AL,F.CUS.AL)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)
    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    FN.CRDT.CBE = 'F.SCB.CREDIT.CBE'
    F.CRDT.CBE = ''
    R.CRDT.CBE = ''
    R.CRDT.CBE1 = ''

    FN.LMT = 'F.LIMIT'
    F.LMT = ''

    FN.COL = 'F.COLLATERAL'
    F.COL = ''


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CUS,F.CUS)
    CALL OPF(FN.CRDT.CBE,F.CRDT.CBE)
    CALL OPF(FN.LMT,F.LMT)

    CCC.NO = 0
    TOTALTM = 0 ; TOTALTM44= 0 ; TOTAL = 0 ; TOT1571 = 0 ; TOT507 = 0 ; TOT512 = 0
    TOTALTMLC = 0 ; TOTALTMLG = 0
    COLLC = 0 ; COLLG = 0 ; COLLC.AMT = 0 ; COLLG.AMT = 0 ; LMT.AMT = 0 ; RATE = 0

    BNK.DATE1 = TODAY
    CALL CDT("",BNK.DATE1,'-1C')

RETURN
*==============================================================
INIT0:
    R.AC = "" ; SWCON = 0 ; SWMARG = ''
RETURN
*==============================================================
INIT0CR:
*----------------- "����� �������� � ������ �� 0 " ------------------
    TOTALTM = 0 ; TOTALTM44= 0 ; TOTAL = 0 ; TOT1571 = 0 ; TOT507 = 0 ; TOT512 = 0
    TOTALTMLC = 0 ; TOTALTMLG = 0
    COLLC = 0 ; COLLG = 0 ; COLLC.AMT = 0 ; COLLG.AMT = 0 ; LMT.AMT = 0 ; RATE = 0
    SWCON = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.US.1>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.2>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.2>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.3>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.3>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.4>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.4>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.5>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.5>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.6>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.6>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.7>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.7>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.8>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.8>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.9>  = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.9>  = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.US.45> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = 0

RETURN
*==============================================================
INIT0CRGUR:
*----------------- "����� �������� � ������ �� 0 " ------------------
    TOTALTM = 0 ; TOTALTM44= 0 ; TOTAL = 0 ; TOT1571 = 0 ; TOT507 = 0 ; TOT512 = 0
    TOTALTMLC = 0 ; TOTALTMLG = 0
    COLLC = 0 ; COLLG = 0 ; COLLC.AMT = 0 ; COLLG.AMT = 0 ; LMT.AMT = 0 ; RATE = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.US.1>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.1>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.2>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.2>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.3>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.3>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.4>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.4>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.5>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.5>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.6>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.6>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.7>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.7>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.8>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.8>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.9>  = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.9>  = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.10> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.10> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.11> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.11> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.12> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.12> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.13> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.13> = 0

    R.CRDT.CBE1<SCB.C.CBE.TOT.US.45> = 0
    R.CRDT.CBE1<SCB.C.CBE.TOT.CR.45> = 0

RETURN
*==============================================================
GETAC:
    T.SEL = "SELECT ":FN.AC
    T.SEL := " WITH ((( CATEGORY GE 1101 AND CATEGORY LE 1599 )"
    T.SEL := " OR ( CATEGORY GE 1001 AND CATEGORY LE 1003 )"
    T.SEL := " OR ( CATEGORY GE 1601 AND CATEGORY LE 1604 )"
    T.SEL := " OR CATEGORY EQ 9090 OR CATEGORY EQ 9091 OR CATEGORY EQ 3201 ) AND OPEN.ACTUAL.BAL LT 0 )"
    T.SEL := " BY CUSTOMER BY CATEGORY"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN

        FOR I = 1 TO SELECTED
            GOSUB INIT0
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*********************************
            CUS.ID = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CUS.AL,CUS.ID,R.CUS.AL,F.CUS.AL,ETEXT)
            LOCAL.REF.CUS = R.CUS.AL<EB.CUS.LOCAL.REF>

            AC.ID = KEY.LIST<I>
            IF I = 1 THEN CCC.NO = CUS.ID
            IF CCC.NO # CUS.ID THEN
                GOSUB CALCAC
                GOSUB INSREC1
            END
*********************************
            CURR = R.AC<AC.CURRENCY>

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            TOTAL = R.AC<AC.OPEN.ACTUAL.BAL> * RATE

            IF TOTAL LT 0 THEN
                TOTAL = TOTAL * -1
            END

***************************** ���� ����
            IF (( R.AC<AC.CATEGORY> GE 1501 AND R.AC<AC.CATEGORY> LE 1595 ) AND R.AC<AC.CATEGORY> NE 1561 AND R.AC<AC.CATEGORY> NE 1562 AND R.AC<AC.CATEGORY> NE 1589 ) OR R.AC<AC.CATEGORY> = 1599 OR R.AC<AC.CATEGORY> = 1205 OR R.AC<AC.CATEGORY> = 1207 OR ( R.AC<AC.CATEGORY> GE 1001 AND R.AC<AC.CATEGORY> LE 1003 ) OR R.AC<AC.CATEGORY> = 1220 OR R.AC<AC.CATEGORY> = 1215 OR R.AC<AC.CATEGORY> = 1216 OR R.AC<AC.CATEGORY> = 1217 OR R.AC<AC.CATEGORY> = 1486 OR R.AC<AC.CATEGORY> = 1230 OR R.AC<AC.CATEGORY> = 1418 OR R.AC<AC.CATEGORY> = 1420 OR R.AC<AC.CATEGORY> =1530 OR R.AC<AC.CATEGORY> =1568 OR R.AC<AC.CATEGORY> =1572 OR R.AC<AC.CATEGORY> =1531 OR R.AC<AC.CATEGORY> =1574 OR R.AC<AC.CATEGORY> =1584 OR R.AC<AC.CATEGORY> =1236 OR R.AC<AC.CATEGORY> =1262 OR R.AC<AC.CATEGORY> =1231 OR ( R.AC<AC.CATEGORY> GE 1601 AND R.AC<AC.CATEGORY> LE 1604 ) THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1> + TOTAL
*        IF R.AC<AC.CATEGORY> = 1220 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> += TOTAL
                END
            END
            IF R.AC<AC.CATEGORY> EQ 1407 OR R.AC<AC.CATEGORY> EQ 1408 OR R.AC<AC.CATEGORY> EQ 1413 OR R.AC<AC.CATEGORY> EQ 1445 OR R.AC<AC.CATEGORY> EQ 1455 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> += TOTAL
                R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> += TOTAL
            END
***************************** ����� ������
            IF R.AC<AC.CATEGORY> = 9090 OR R.AC<AC.CATEGORY> = 9091 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10> + TOTAL
                R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> + TOTAL
            END
**************************** ����� �� ������
            IF (R.AC<AC.CATEGORY> GE 1301 AND R.AC<AC.CATEGORY> LE 1399) OR R.AC<AC.CATEGORY> EQ 1224 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2> + TOTAL
*                IF R.AC<AC.CATEGORY> = 1224 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> += TOTAL
                END
            END
***************************  ����� �����
            IF (R.AC<AC.CATEGORY> GE 1101 AND R.AC<AC.CATEGORY> LE 1199) OR R.AC<AC.CATEGORY> EQ 1223 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5> + TOTAL
*               IF R.AC<AC.CATEGORY> = 1223 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> += TOTAL
                END
            END
***************************      ��� �����
            IF R.AC<AC.CATEGORY> EQ 1202 OR R.AC<AC.CATEGORY> EQ 1212 OR R.AC<AC.CATEGORY> EQ 1221 OR R.AC<AC.CATEGORY> EQ 1598 OR R.AC<AC.CATEGORY> EQ 1203 OR R.AC<AC.CATEGORY> EQ 1488 OR R.AC<AC.CATEGORY> EQ 1561 OR R.AC<AC.CATEGORY> EQ 1562 OR R.AC<AC.CATEGORY> EQ 1589 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6> + TOTAL
*                IF R.AC<AC.CATEGORY> = 1221 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> += TOTAL
                END
            END
***************************      ��� ����
            IF R.AC<AC.CATEGORY> EQ 1201 OR R.AC<AC.CATEGORY> EQ 1211 OR R.AC<AC.CATEGORY> EQ 1222 OR R.AC<AC.CATEGORY> EQ 1596 OR R.AC<AC.CATEGORY> EQ 1487 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7> + TOTAL
*               IF R.AC<AC.CATEGORY> = 1222 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> += TOTAL
                END
            END
***************************    ����� �����
            IF R.AC<AC.CATEGORY> EQ 1404 OR R.AC<AC.CATEGORY> EQ 1414 OR R.AC<AC.CATEGORY> EQ 1225 OR R.AC<AC.CATEGORY> EQ 1597 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3> + TOTAL
*                IF R.AC<AC.CATEGORY> = 1225 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> += TOTAL
                END
            END
***************************    ������� ������
            IF ( R.AC<AC.CATEGORY> GE 1401 AND R.AC<AC.CATEGORY> LE 1499 ) OR R.AC<AC.CATEGORY> EQ 1206 OR R.AC<AC.CATEGORY> EQ 1208 OR R.AC<AC.CATEGORY> EQ 1227 OR R.AC<AC.CATEGORY> EQ 1218 AND R.AC<AC.CATEGORY> NE 1404 AND  R.AC<AC.CATEGORY> NE 1414 AND R.AC<AC.CATEGORY> NE 1416 AND R.AC<AC.CATEGORY> NE 1407 AND R.AC<AC.CATEGORY> NE 1408 AND R.AC<AC.CATEGORY> NE 1413 AND R.AC<AC.CATEGORY> NE 1445 AND R.AC<AC.CATEGORY> NE 1455 AND R.AC<AC.CATEGORY> NE 1486 AND R.AC<AC.CATEGORY> NE 1487 AND R.AC<AC.CATEGORY> NE 1488 AND R.AC<AC.CATEGORY> NE 1418 AND R.AC<AC.CATEGORY> NE 1420 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8> + TOTAL
*                IF R.AC<AC.CATEGORY> = 1227 THEN
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> += TOTAL
                END
            END
****************************
            IF ( R.AC<AC.CATEGORY> EQ 3201 ) THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = R.CRDT.CBE<SCB.C.CBE.TOT.US.9> + TOTAL
                R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> + TOTAL
            END
***************************    ����� ������ ������
            IF R.AC<AC.CATEGORY> EQ 1416 OR R.AC<AC.CATEGORY> EQ 1214 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4> + TOTAL
                IF LOCAL.REF.CUS<1,CULR.CREDIT.CODE> = 110 AND R.AC<AC.LIMIT.REF> = '' THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> + TOTAL
                END
            END
****************************
            CCC.NO = CUS.ID
****************************
        NEXT I
        IF I = SELECTED THEN
            GOSUB CALCAC
            GOSUB INSREC1
        END
    END
RETURN
*============================================
GETLC:
*-----
    FN.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER.OF.CREDIT = '' ; R.LETTER.OF.CREDIT = ''
    TOTAL= 0  ; T.SEL = ""
    CCC.NO = '' ; CURR = '' ; SWLC = 0 ; SWMARG = ''

    CALL OPF( FN.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT)
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH APPLICANT.CUSTNO NE '' AND LIABILITY.AMT GT 0 AND CATEGORY.CODE NE '23200' AND LIMIT.REFERENCE UNLIKE 25120... AND LIMIT.REFERENCE UNLIKE 35120..."
    T.SEL := " BY APPLICANT.CUSTNO BY LC.CURRENCY BY SCB.TYPE"
***********************************
    KEY.LIST=""
    SELECTED=""
    KEY.LIST = ""
    KEY.LISTLC=""
    T.SELC=""
    SELECTEDLC=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            LC.ID = KEY.LIST<I>
            CALL F.READ(FN.LETTER.OF.CREDIT,KEY.LIST<I>, R.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT, ETEXT)

            APL.NO = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.CUSTNO>
            CUS.ID = APL.NO
*----------------------------------------------------------
            AC.ID = KEY.LISTLC<I>
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLC = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                CONT.ACC1 = 3010 ; CONT.ACC2 = 3012
                LMTREF1 = "0030000";LMTREF2 = "0020000"
                LMTREF3 = "1234567"
                SWMARG = 'LC'
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*----------------------------------------------------------
                GOSUB INSREC1
                SWLC = 1
            END
*----------------------------------------------------------
            IF SWLC = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELC)
                IF ELC THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.LETTER.OF.CREDIT<TF.LC.LC.CURRENCY>
*********************************

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            TOTAL = R.LETTER.OF.CREDIT<TF.LC.LIABILITY.AMT> * RATE

*----------------------------------------------------------
*--------- CHNAGE WAY TO READ PROVISGN FROM CONTRACT
*--------- UPDATE BY BAKRY 2011/10/23 --------------------
            TOTALTMLCA = 0
            TOTALTMLCA = R.LETTER.OF.CREDIT<TF.LC.PRO.OUT.AMOUNT> * RATE
            TOTALTMLC += TOTALTMLCA
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.11> += TOTAL
            R.CRDT.CBE<SCB.C.CBE.TOT.US.11> -= TOTALTMLCA
*            R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> -= TOTALTMLCA
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLC = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED THEN
*----------------------------------------------------------
            CONT.ACC1 = 3010 ; CONT.ACC2 = 3012
            LMTREF1 = "0030000";LMTREF2 = "0020000"
            LMTREF3 = "1234567"
            SWMARG = 'LC'
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLC = 1
        END
    END
*----------------------------------------------------------
***********************************
RETURN
*============================================
CHKLIMT:
*------
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    T.SEL1 = "SELECT ":FN.LMT:" WITH ( @ID LIKE ":CCC.NO:".":LMTREF1
    T.SEL1 := "... OR @ID LIKE ":CCC.NO:".":LMTREF2:"... OR @ID LIKE "
    T.SEL1 := CCC.NO:".":LMTREF3:"... ) AND INTERNAL.AMOUNT NE 0 AND"
    IF LMTREF1 = "0030000" THEN
        T.SEL1 := " NOTES NE 'CREATED BY SYSTEM"
    END ELSE
        T.SEL1 := " PRODUCT.ALLOWED EQ '' AND NOTES NE 'CREATED BY SYSTEM"
    END
    T.SEL1 := " DEFAULT' BY @ID"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF NOT(SELECTED1) THEN
*********************************
*        CALL F.READ(FN.CUS.ACC,CCC.NO,R.CUS.ACC,F.CUS.ACC,ETEXT)
        LOOP
            REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
        WHILE AC.ID:POS.ACCT
            CALL F.READ(FN.AC,AC.ID, R.AC,F.AC,ETEXT)
            AC.CATEG = R.AC<AC.CATEGORY>
            AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
            IF (AC.CATEG GE CONT.ACC1 AND AC.CATEG LE CONT.ACC2) AND AC.BAL NE 0 THEN
                CURR = R.AC<AC.CURRENCY>
                LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END
                TOTAL = R.AC<AC.OPEN.ACTUAL.BAL> * RATE
                IF TOTAL LT 0 THEN
                    TOTAL = TOTAL * -1
                END ELSE
                    TOTALT = TOTAL
                END
**************************** MARGIN LETTER OF GRANTE
                IF SWMARG = 'LG' THEN
                    IF R.AC<AC.CATEGORY> EQ 3005 THEN
                        TOTALTMLG = TOTALTMLG + TOTAL
                    END
                END
**************************** MARGINE LETTER OF CREDIT
                IF SWMARG = 'LC' THEN
                    IF R.AC<AC.CATEGORY> = 3010 OR R.AC<AC.CATEGORY> = 3011 OR R.AC<AC.CATEGORY> = 3012 THEN
                        TOTALTMLC = TOTALTMLC + TOTAL
                    END
                END
**************************** MARGINE LETTER OF CREDIT 44 - 45
                IF SWMARG = 'DR' THEN
                    IF R.AC<AC.CATEGORY> = 3013 THEN
                        TOTALTM44 = TOTALTM44 + TOTAL
                    END
                END
****************************
            END
        REPEAT
*----------------------------------------------------------
        IF SWMARG = 'LC' THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.11> -= TOTALTMLC
        IF SWMARG = 'DR' THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.US.13> -= TOTALTM44
            R.CRDT.CBE<SCB.C.CBE.TOT.US.45> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13>
**********UPDATE 20140327 FOR CR *****************************
*            R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> += TOTALTM44
*            R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> += TOTALTM44
        END
        IF SWMARG = 'LG' THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.12> -= TOTALTMLG

*----------------------------------------------------------
    END
RETURN
*============================================
GETDR:
*-----
    FN.DRAWINGS = 'FBNK.DRAWINGS' ; F.DRAWINGS = '' ; R.DRAWINGS = ''

    TOTAL= 0  ; T.SEL = ""
    CCC.NO = '' ; CURR = '' ; SWDR = 0 ; SWMARG = ''

    CALL OPF( FN.DRAWINGS,F.DRAWINGS)

    T.SEL = "SELECT ":FN.DRAWINGS:" WITH MATURITY.REVIEW GT ":BNK.DATE1
    T.SEL := " AND CUSTOMER.LINK UNLIKE 99... BY CUSTOMER.LINK"
***********************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            DR.ID = KEY.LIST<I>
            CALL F.READ(FN.DRAWINGS,KEY.LIST<I>, R.DRAWINGS,F.DRAWINGS, ETEXT)

****** GET LIMIT REF FOR LC *****
            LC.IDD = KEY.LIST<I>[1,12]
            FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
            CALL OPF(FN.LC,F.LC)
            CALL F.READ(FN.LC,LC.IDD,R.LC,F.LC,ETEXT)
            LC.LMT.REF = R.LC<TF.LC.LIMIT.REFERENCE>[1,5]
*            IF LC.LMT.REF NE '25120' THEN
*----------------------------------------------------------
            CUS.ID = R.DRAWINGS<TF.DR.CUSTOMER.LINK>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWDR = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                CONT.ACC1 = 3013 ; CONT.ACC2 = 3013
                LMTREF1 = "0030000";LMTREF2 = "0020000"
                LMTREF3 = "1234567"
                SWMARG = 'DR'
*----------------------------------------------------------
                GOSUB INSREC1
                SWDR = 1
            END
*----------------------------------------------------------
            IF SWDR = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,EDR)
                IF EDR THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.DRAWINGS<TF.DR.DRAW.CURRENCY>
*********************************************************
            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END
            DR.AMT = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>
            TOTAL = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT> * RATE
*----------------------------------------------------------
*--------- CHNAGE WAY TO READ PROVISGN FROM CONTRACT
*--------- UPDATE BY BAKRY 2011/10/23 --------------------
            TOTALTM44A = 0
            TOTALTM44A = R.DRAWINGS<TF.DR.PROV.AMT.REL> * RATE
            TOTALTM44  += TOTALTM44A
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.13> += TOTAL
**********UPDATE 20140327 FOR CR *****************************
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> += TOTAL
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> += TOTAL
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> -= TOTAL
            R.CRDT.CBE<SCB.C.CBE.TOT.US.13> -= TOTALTM44A
**********UPDATE 20140327 FOR CR *****************************
*            R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> -= TOTALTM44A

            R.CRDT.CBE<SCB.C.CBE.TOT.US.45> += TOTAL
            R.CRDT.CBE<SCB.C.CBE.TOT.US.45> -= TOTALTM44A
**********UPDATE 20140327 FOR CR *****************************
*            R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> -= TOTALTM44A

*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWDR = 0
*--------------------------------------------------------
*   END
        NEXT I
        IF I = SELECTED THEN
*----------------------------------------------------------
            CONT.ACC1 = 3013 ; CONT.ACC2 = 3013
            LMTREF1 = "0030000";LMTREF2 = "0020000"
            LMTREF3 = "1234567"
            SWMARG = 'DR'
*----------------------------------------------------------
            GOSUB INSREC1
            SWDR = 1
        END
    END
*----------------------------------------------------------
***********************************
RETURN
*============================================
GETLG:
*-----
    FN.LG = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LG = '' ; R.LG = ''
    TOTAL= 0  ; T.SEL = ""
    CCC.NO = '' ; CURR = '' ; SWLG = 0

    CALL OPF(FN.LG,F.LG)
    T.SEL  = "SELECT ":FN.LG:" WITH  CATEGORY EQ 21096 AND LIMIT.REFERENCE UNLIKE 2520... AND LIMIT.REFERENCE UNLIKE 2530... AND LIMIT.REFERENCE UNLIKE 2530 "
    T.SEL := " AND AMOUNT NE 0 AND FIN.MAT.DATE GT ":BNK.DATE1
    T.SEL := " BY CUSTOMER.ID BY CURRENCY"
***********************************
    KEY.LIST=""
    SELECTED=""
    KEY.LIST = ""
    KEY.LISTLG=""
    T.SELC=""
    SELECTEDLG=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN

        FOR I = 1 TO SELECTED
            LG.ID = KEY.LIST<I>
            CALL F.READ(FN.LG,KEY.LIST<I>,R.LG,F.LG, ETEXT)

            CUS.ID = R.LG<LD.CUSTOMER.ID>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLG = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                CONT.ACC1 = 3005 ; CONT.ACC2 = 3005
                LMTREF1 = "0002505";LMTREF2 = "0002510"
                LMTREF3 = "0002520"
                SWMARG = 'LG'
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*----------------------------------------------------------
                GOSUB INSREC1
                SWLG = 1
            END
*----------------------------------------------------------
            IF SWLG = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELG)
                IF ELG THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.LG<LD.CURRENCY>
*********************************

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            TOTAL = R.LG<LD.AMOUNT> * RATE
*----------------------------------------------------------
*--------- CHNAGE WAY TO READ PROVISGN FROM CONTRACT
*--------- UPDATE BY BAKRY 2011/10/23 --------------------
            TOTALTMLGA = 0
            TOTALTMLGA = R.LG<LD.LOCAL.REF><1,LDLR.MARGIN.AMT> * RATE
            TOTALTMLG += TOTALTMLGA
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.12> += TOTAL
            R.CRDT.CBE<SCB.C.CBE.TOT.US.12> -= TOTALTMLGA
*            R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> -= TOTALTMLGA
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLG = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED THEN
*----------------------------------------------------------
            CONT.ACC1 = 3005 ; CONT.ACC2 = 3005
            LMTREF1 = "0002505";LMTREF2 = "0002510"
            LMTREF3 = "0002520"
            SWMARG = 'LG'
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLC = 1
        END
*----------------------------------------------------------
    END
***********************************
RETURN
*============================================
GETLOAN:
*-----
    FN.LON = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LON = '' ; R.LON = ''
    TOTAL= 0  ; T.SEL.LON = ""
    CCC.NO = '' ; CURR = '' ; SWLON = 0
    CALL OPF(FN.LON,F.LON)
    T.SEL.LON  = "SELECT ":FN.LON:" WITH  CATEGORY IN (21050 21051 21052 21053 21054 21055 21056 21057 21058 21059 21060 21061 21062 21063 21064 21065 21066 21067 21068 21069 21070 21071 21072 21073 21074)"
    T.SEL.LON := " AND AMOUNT NE 0 AND FIN.MAT.DATE GT ":BNK.DATE1
    T.SEL.LON := " BY CUSTOMER.ID BY CURRENCY"
***********************************
    KEY.LIST.LON=""
    SELECTED.LON=""
    KEY.LIST.LON = ""
    SELECTED.LON=""
    ER.MSG.LON=""
    CALL EB.READLIST(T.SEL.LON,KEY.LIST.LON,"",SELECTED.LON,ER.MSG.LON)
    IF KEY.LIST.LON THEN

        FOR I = 1 TO SELECTED.LON
            LON.ID = KEY.LIST.LON<I>
            CALL F.READ(FN.LON,KEY.LIST.LON<I>,R.LON,F.LON, ETEXT)

            CUS.ID = R.LON<LD.CUSTOMER.ID>
*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWLON = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*----------------------------------------------------------
                GOSUB INSREC1
                SWLON = 1
            END
*----------------------------------------------------------
            IF SWLON = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELON)
                IF ELON THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.LON<LD.CURRENCY>
*********************************
            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            TOTAL = R.LON<LD.AMOUNT> * RATE
*----------------------------------------------------------
            CATEG.LON = R.LON<LD.CATEGORY>
            IF CATEG.LON = 21050 OR CATEG.LON = 21057 OR CATEG.LON = 21064 OR CATEG.LON = 21069 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.6> += TOTAL
            END
            IF CATEG.LON = 21051 OR CATEG.LON = 21056 OR CATEG.LON = 21059 OR CATEG.LON = 21074 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.7> += TOTAL
            END
            IF CATEG.LON = 21052 OR CATEG.LON = 21054 OR CATEG.LON = 21055 OR CATEG.LON = 21058 OR CATEG.LON = 21060 OR CATEG.LON = 21061 OR CATEG.LON = 21062 OR CATEG.LON = 21066 OR CATEG.LON = 21067 OR CATEG.LON = 21068 OR CATEG.LON = 21070 OR CATEG.LON = 21071 OR CATEG.LON = 21072 OR CATEG.LON = 21073 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> += TOTAL
                IF CATEG.LON = 21055 THEN
                    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> += TOTAL
                END
            END
            IF CATEG.LON = 21063 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> += TOTAL
            END
            IF CATEG.LON = 21053 OR  CATEG.LON = 21065 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.8> += TOTAL
            END
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWLON = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED.LON THEN
*----------------------------------------------------------
            SWMARG = 'LON'
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*----------------------------------------------------------
            GOSUB INSREC1
            SWLON = 1
        END
*----------------------------------------------------------
    END
***********************************
RETURN
*============================================
GETPD:
*=====
    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD = ''
    TOTAL= 0  ; T.SEL.PD = ""
    CCC.NO = '' ; CURR = '' ; SWPD = 0
    CALL OPF(FN.PD,F.PD)
    T.SEL.PD  = "SELECT ":FN.PD:" WITH CATEGORY GE 21050 AND CATEGORY LE 21074"
    T.SEL.PD := " BY CUSTOMER BY CURRENCY"
***********************************
    KEY.LIST.PD=""
    SELECTED.PD=""
    KEY.LIST.PD = ""
    SELECTED.PD=""
    ER.MSG.PD=""
    CALL EB.READLIST(T.SEL.PD,KEY.LIST.PD,"",SELECTED.PD,ER.MSG.PD)
    IF KEY.LIST.PD THEN

        FOR I = 1 TO SELECTED.PD
            PD.ID = KEY.LIST.PD<I>
            CALL F.READ(FN.PD,KEY.LIST.PD<I>,R.PD,F.PD, ETEXT)

            CUS.ID = R.PD<PD.CUSTOMER>

*----------------------------------------------------------
            IF I = 1 THEN CCC.NO = CUS.ID ; SWPD = 1
            IF CCC.NO # CUS.ID THEN
*----------------------------------------------------------
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
                R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*----------------------------------------------------------
                GOSUB INSREC1
                SWPD = 1
            END
*----------------------------------------------------------
            IF SWPD = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,EPD)
                IF EPD THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            CURR= R.PD<PD.CURRENCY>
*********************************
            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            TOTAL = R.PD<PD.TOTAL.OVERDUE.AMT> * RATE
*----------------------------------------------------------
            CATEG.PD = R.PD<PD.CATEGORY>
            IF CATEG.PD = 21050 OR CATEG.PD = 21057 OR CATEG.PD = 21064 OR CATEG.PD = 21069 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.6> += TOTAL
            END
            IF CATEG.PD = 21051 OR CATEG.PD = 21056 OR CATEG.PD = 21074 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.7> += TOTAL
            END
            IF CATEG.PD = 21052 OR CATEG.PD = 21054 OR CATEG.PD = 21060 OR CATEG.PD = 21061 OR CATEG.PD = 21062 OR CATEG.PD = 21063 OR CATEG.PD = 21066 OR CATEG.PD = 21067 OR CATEG.PD = 21068 OR ( CATEG.PD GE 21070 AND CATEG.PD LE 21073 ) THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.1> += TOTAL
            END
*            IF CATEG.PD = 21063 THEN
*                R.CRDT.CBE<SCB.C.CBE.TOT.US.3> += TOTAL
*            END
            IF CATEG.PD = 21053 THEN
                R.CRDT.CBE<SCB.C.CBE.TOT.US.8> += TOTAL
            END
*----------------------------------------------------------
            CCC.NO = CUS.ID ; SWPD = 0
*--------------------------------------------------------
        NEXT I
        IF I = SELECTED.PD THEN
*----------------------------------------------------------
            SWMARG = 'PD'
*----------------------------------------------------------
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
            R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*----------------------------------------------------------
            GOSUB INSREC1
            SWPD = 1
        END
*----------------------------------------------------------
    END
***********************************
RETURN
*============================================
GETLIMT:
*-------
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    R.LMT = "" ; CURR = '' ; SWLM = 0
    LMT.AMT.5120 = 0
    T.SEL1 = "SELECT ":FN.LMT
    T.SEL1 := " WITH ( INTERNAL.AMOUNT NE 0 AND PRODUCT.ALLOWED EQ ''"
    T.SEL1 := " AND NOTES NE 'CREATED BY SYSTEM DEFAULT' )"
    T.SEL1 := " OR (@ID LIKE ....0030000.... OR @ID LIKE ....0020000....) BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1  THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.LMT,KEY.LIST1<K>,R.LMT,F.LMT,E2)
            LMT.AMT.5120 = 0
*----------------------------------------------------------
            CUS.ID = FIELD(KEY.LIST1<K>,".",1)
            IF K = 1 THEN CCC.NO = CUS.ID ; SWLM = 1
            IF CCC.NO # CUS.ID THEN
                GOSUB CALCREC ; GOSUB INSREC
                GOSUB INIT0CR
                SWLM = 1
            END
*----------------------------------------------------------
            IF SWLM = 1 THEN
                CALL F.READ(FN.CRDT.CBE,CUS.ID,R.CRDT.CBE,F.CRDT.CBE,ELM)
                IF ELM THEN GOSUB INIT0CR
            END
*----------------------------------------------------------
            LMT.REF = FIELD(KEY.LIST1<K>,".",2)
            LMT.REF = TRIM( LMT.REF, '0', 'L')
*----------------------------------------------------------
            CURR= R.LMT<LI.LIMIT.CURRENCY>
*            IF LMT.REF EQ 20000 OR LMT.REF EQ 30000 THEN
*                T.SEL2 = "SELECT ":FN.LMT:" WITH CREDIT.LINE EQ '":KEY.LIST1<K>:"' AND LIMIT.PRODUCT EQ 5120"
*                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
*                IF SELECTED2  THEN
*                    FOR KK = 1 TO SELECTED2
*                        CALL F.READ(FN.LMT,KEY.LIST2<KK>,R.LMT,F.LMT,E2)
*                        LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
*                            RATE = R.BASE<RE.BCP.RATE,POS>
*                        END
*                        LMT.AMT.5120 = R.LMT<LI.INTERNAL.AMOUNT> * RATE
*                    NEXT KK
*                END
*            END

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

                RATE = R.BASE<RE.BCP.RATE,POS>
            END
            TOTAL = R.LMT<LI.INTERNAL.AMOUNT> * RATE
*            IF (LMT.REF = 2505 OR LMT.REF = 2510 OR LMT.REF = 20000 OR LMT.REF = 30000) THEN
*                TOTAL = R.LMT<LI.ONLINE.LIMIT> * RATE
*            END ELSE
*                TOTAL = R.LMT<LI.INTERNAL.AMOUNT> * RATE
*            END
            LMT.AMT = 0 ; LMT.AMT = TOTAL - LMT.AMT.5120
            GOSUB CALCLMT

            CCC.NO = CUS.ID ; SWLM = 0
        NEXT K
        IF K = SELECTED1 THEN GOSUB CALCREC ; GOSUB INSREC
        GOSUB INIT0CR
    END
RETURN
*----------------------------------------------------------
CALCLMT:
*-------
    IF LMT.REF = 100 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 101 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 102 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 103 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 104 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 105 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 106 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 108 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 109 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 110 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> + LMT.AMT
    IF LMT.REF = 111 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 112 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 141 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 114 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 115 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 117 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 118 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 134 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 135 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 136 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 119 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 120 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 121 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 122 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 123 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 124 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 128 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 129 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 131 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 138 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 133 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> + LMT.AMT
    IF LMT.REF = 139 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 200 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 201 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 400 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 405 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 406 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 407 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 500 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 501 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 600 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 601 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 602 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 603 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 700 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 701 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 2010 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 2020 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 32060 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 32060 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - LMT.AMT
    IF LMT.REF = 2080 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 2090 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT

    IF LMT.REF = 2505 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 2510 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 2520 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 2530 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> + LMT.AMT
    IF LMT.REF = 30000 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> + LMT.AMT
    IF LMT.REF = 20000 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> + LMT.AMT
    IF LMT.REF = 6010 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 6020 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT

******* ADD NEW LIMIT REF UPDATED BY KHALED 2010/01/24 ************
    IF LMT.REF = 8101 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 8102 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8103 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8104 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 8105 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8106 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8201 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 8202 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8203 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8204 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 8205 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 8206 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8207 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8208 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8209 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8210 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8211 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 8212 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8213 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8301 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> + LMT.AMT
    IF LMT.REF = 8302 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 8303 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> + LMT.AMT
    IF LMT.REF = 8401 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8402 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8501 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8502 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 8601 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 408  THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 604  THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
*******************************************************************
******* ADD NEW LIMIT REF UPDATED BY BAKRY 2013/11/24 ************
*******************************************************************
    IF LMT.REF = 6201 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6202 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6203 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6204 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + LMT.AMT
    IF LMT.REF = 6006 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> + LMT.AMT
    IF LMT.REF = 6001 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 6003 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> + LMT.AMT
    IF LMT.REF = 6002 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> + LMT.AMT
    IF LMT.REF = 6004 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> + LMT.AMT
    IF LMT.REF = 6008 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
    IF LMT.REF = 6009 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> + LMT.AMT
*******************************************************
******* END OF UPDATED BY BAKRY 2013/11/24 ************
*******************************************************

    IF LMT.REF EQ 20000 AND R.LMT<LI.COLLAT.RIGHT> EQ '' THEN
        GOSUB GETCOL20
        SWCON = 1
    END

    IF LMT.REF EQ 30000 OR ( LMT.REF EQ 20000 AND R.LMT<LI.COLLAT.RIGHT> GT 0 ) THEN
        GOSUB GETCOLLC
        SWCON = 1
    END

***********************
*----------- UPDATED BY BAKRY 2011/10/23
**    IF LMT.REF = 2505 OR LMT.REF = 2510 OR LMT.REF = 2520 THEN
*---------------------------------------------------------------
    IF LMT.REF = 2505 OR LMT.REF = 2510 THEN
        GOSUB GETCOLLG
        SWCON = 1
    END
***********************
RETURN
*---------------------------------------------------------------------
CALCREC:
*------
    IF SWCON = 1 THEN
        GOSUB CALCCONT
    END

*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - TOT1571
*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - TOT507
*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - TOT512
*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + TOT507
*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> + TOT512

    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11> - COLLC
*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> - COLLC

    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12> - COLLG
*    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> - COLLG

RETURN
*---------------------------------------------------------------------
CALCCONT:
*------
    CALL F.READ(FN.CUS.ACC,CCC.NO,R.CUS.ACC,F.CUS.ACC,ETEXT)
    LOOP
        REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
    WHILE AC.ID:POS.ACCT
        CALL F.READ(FN.AC,AC.ID, R.AC,F.AC,ETEXT)
        AC.CATEG = R.AC<AC.CATEGORY>
        AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
*        IF (AC.CATEG EQ 3005 OR AC.CATEG EQ 3010 OR AC.CATEG EQ 3011 OR AC.CATEG EQ 3012 OR AC.CATEG EQ 3013 OR AC.CATEG EQ 1512 OR AC.CATEG EQ 1507 OR AC.CATEG EQ 1571 OR AC.CATEG EQ 1572 OR AC.CATEG EQ 1587 ) AND AC.BAL NE 0 THEN
        IF (AC.CATEG EQ 3005 OR AC.CATEG EQ 3010 OR AC.CATEG EQ 3011 OR AC.CATEG EQ 3012 OR AC.CATEG EQ 3013 OR AC.CATEG EQ 1512 OR AC.CATEG EQ 1507 ) AND AC.BAL NE 0 THEN
*********************************
            CURR = R.AC<AC.CURRENCY>
            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END
            TOTAL = R.AC<AC.OPEN.ACTUAL.BAL> * RATE
            IF TOTAL LT 0 THEN
                TOTAL = TOTAL * -1
            END ELSE
                TOTALT = TOTAL
            END
***************************** �������� 507
            IF R.AC<AC.CATEGORY> = 1507 THEN
                TOT507  = TOT507  +  TOTAL
            END
***************************** �������� 512
            IF R.AC<AC.CATEGORY> = 1512 THEN
                TOT512  = TOT512  +  TOTAL
            END
***************************** 1571 1572 1587
*            IF R.AC<AC.CATEGORY> = 1571 OR R.AC<AC.CATEGORY> = 1572 OR R.AC<AC.CATEGORY> = 1587 THEN
*                TOT1571  = TOT1571  +  TOTAL
*            END
****************************
        END
    REPEAT
*----------------------------------------------------------
RETURN
*---------------------------------------------------------------------
GETCOLLC:
*--------
    YY1 = 0 ; SS1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; SS2 = 0
    C.SEL1 = "" ; K2.LIST = "" ; SELECTED3 = "" ; ERR2 = "" ; EER = ""
    S = 0

    S.CUR = R.LMT<LI.LIMIT.CURRENCY>
*Line [ 1170 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY1 = DCOUNT(R.LMT<LI.COLLAT.RIGHT>,@VM)
    FOR H1 = 1 TO YY1
        SS1 = R.LMT<LI.COLLAT.RIGHT,H1>
*Line [ 1174 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        YY2 = DCOUNT(R.LMT<LI.COLLAT.RIGHT,H1>,@SM)
        FOR H2 = 1 TO YY2
            SS2   = R.LMT<LI.COLLAT.RIGHT,H1,H2>
            S.AMT = R.LMT<LI.SECURED.AMT,H1,H2>
            IF S.CUR NE 'EGP' THEN
                LOCATE S.CUR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                RATE = R.BASE<RE.BCP.RATE,POS>
                COLLC.AMT = S.AMT * RATE
            END ELSE
                COLLC.AMT = S.AMT
            END
            COLLC += COLLC.AMT

        NEXT H2
    NEXT H1
RETURN
*---------------------------------------------------------------------
GETCOL20:
*--------
    T.SEL2 = "SELECT ":FN.LMT:" WITH CREDIT.LINE EQ '":KEY.LIST1<K>:"' AND PRODUCT.ALLOWED EQ ''"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2  THEN
        FOR KK = 1 TO SELECTED2
            CALL F.READ(FN.LMT,KEY.LIST2<KK>,R.LMT,F.LMT,E2)
            YY1 = 0 ; SS1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; SS2 = 0
            C.SEL1 = "" ; K2.LIST = "" ; SELECTED3 = "" ; ERR2 = "" ; EER = ""
            S = 0

            S.CUR = R.LMT<LI.LIMIT.CURRENCY>
*Line [ 1203 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            YY1 = DCOUNT(R.LMT<LI.COLLAT.RIGHT>,@VM)
            FOR H1 = 1 TO YY1
                SS1 = R.LMT<LI.COLLAT.RIGHT,H1>
*Line [ 1208 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                YY2 = DCOUNT(R.LMT<LI.COLLAT.RIGHT,H1>,@SM)
                FOR H2 = 1 TO YY2
                    SS2   = R.LMT<LI.COLLAT.RIGHT,H1,H2>
                    S.AMT = R.LMT<LI.SECURED.AMT,H1,H2>
                    IF S.CUR NE 'EGP' THEN
                        LOCATE S.CUR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                        RATE = R.BASE<RE.BCP.RATE,POS>
                        COLLC.AMT = S.AMT * RATE
                    END ELSE
                        COLLC.AMT = S.AMT
                    END
                    COLLC += COLLC.AMT
                NEXT H2
            NEXT H1
        NEXT KK
    END
RETURN
*---------------------------------------------------------------------
GETCOLLG:
*-------
    YY1 = 0 ; SS1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; SS2 = 0
    C.SEL1 = "" ; K2.LIST = "" ; SELECTED3 = "" ; ERR2 = "" ; EER = ""
    S = 0

    S.CUR = R.LMT<LI.LIMIT.CURRENCY>
*Line [ 1234 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY1 = DCOUNT(R.LMT<LI.COLLAT.RIGHT>,@VM)
    FOR H1 = 1 TO YY1
        SS1 = R.LMT<LI.COLLAT.RIGHT,H1>
*Line [ 1238 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        YY2 = DCOUNT(R.LMT<LI.COLLAT.RIGHT,H1>,@SM)
        FOR H2 = 1 TO YY2
            SS2 = R.LMT<LI.COLLAT.RIGHT,H1,H2>
            S.AMT = R.LMT<LI.SECURED.AMT,H1,H2>
            IF S.CUR NE 'EGP' THEN
                LOCATE S.CUR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                RATE = R.BASE<RE.BCP.RATE,POS>
                COLLG.AMT = S.AMT * RATE
            END ELSE
                COLLG.AMT = S.AMT
            END
            COLLG += COLLG.AMT

        NEXT H2
    NEXT H1
RETURN
*---------------------------------------------------------------------
CALCAC:
*------
*----------------- "����� �������� � ������  " --------------------
*----------------- " ����������������������  " --------------------

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.2>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.4>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.5>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.9>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.10>
RETURN
*----------------------------------------------------------------------
INSREC1:
*------
*------------"������ ������ �� �������� ��� �� 0 "-------------------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.1> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.2> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.3> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.4> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.5> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.6> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.7> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.8> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.9> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.10> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.11> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.12> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.13> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.45> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.45> = 0
*----------------- " ����� ��� ����� ������� " --------------------
    CO.CODE1 = ''
*Line [ 1301 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CCC.NO,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CCC.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 1308 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CCC.NO,CO.CODE1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CCC.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CO.CODE1=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    IF LOCAL.REF<1,CULR.CBE.NO> THEN
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = LOCAL.REF<1,CULR.CBE.NO>
    END ELSE
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = "999999999999"
    END
    R.CRDT.CBE<SCB.C.CBE.BNK.DATE> = BNK.DATE1
    R.CRDT.CBE<SCB.C.CBE.CO.CODE> = CO.CODE1
*--------------------- " ����� ��� ����� ����� ���������� ------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.99> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.US.13> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.US.11> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.US.12> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> NE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> NE 0 THEN
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    IF SW2 = 1 THEN

        CALL F.WRITE(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE)
        CALL JOURNAL.UPDATE(CCC.NO)

        CALL F.RELEASE(FN.CRDT.CBE,CCC.NO,F.CRDT.CBE)
        CLOSE F.CRDT.CBE
    END
    GOSUB INIT0CR
RETURN
*---------------------------------------------------------------------
INSRECC:
*------------ "������ ������ ���� " ----------------------------------
    R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.1>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.1>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.2>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.2>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.3>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.3>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.4>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.4>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.5>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.5>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.6>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.6>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.7>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.7>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.8>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.8>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = R.CRDT.CBE<SCB.C.CBE.TOT.US.9> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.9>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.9>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.10>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.10>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.11>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.11>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.12>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.12>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> = R.CRDT.CBE<SCB.C.CBE.TOT.US.99> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.99>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.99>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.13>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.13>,0)

    R.CRDT.CBE<SCB.C.CBE.TOT.US.45> = R.CRDT.CBE<SCB.C.CBE.TOT.US.45> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.US.45> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.US.45>,0)
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> / 1000
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = DROUND(R.CRDT.CBE<SCB.C.CBE.TOT.CR.45>,0)

*------------"������ ������ ����� �����"-----------------------------
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = R.CRDT.CBE<SCB.C.CBE.TOT.US.9>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13>
*    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = R.CRDT.CBE<SCB.C.CBE.TOT.US.45>
*------------"������ �������� ���� �� ������ ������ �������� ���"-------------------------
    LOCAL.REF.1 = ''
*Line [ 1433 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CCC.NO,LOCAL.REF.1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CCC.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF.1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    IF LOCAL.REF.1<1,CULR.CREDIT.CODE> = 110 THEN
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.1> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = R.CRDT.CBE<SCB.C.CBE.TOT.US.1>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.2> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = R.CRDT.CBE<SCB.C.CBE.TOT.US.2>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.3> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = R.CRDT.CBE<SCB.C.CBE.TOT.US.3>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.4> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = R.CRDT.CBE<SCB.C.CBE.TOT.US.4>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.5> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = R.CRDT.CBE<SCB.C.CBE.TOT.US.5>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.6> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = R.CRDT.CBE<SCB.C.CBE.TOT.US.6>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.7> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = R.CRDT.CBE<SCB.C.CBE.TOT.US.7>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.8> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = R.CRDT.CBE<SCB.C.CBE.TOT.US.8>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.9> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = R.CRDT.CBE<SCB.C.CBE.TOT.US.9>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.10> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = R.CRDT.CBE<SCB.C.CBE.TOT.US.10>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.11> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = R.CRDT.CBE<SCB.C.CBE.TOT.US.11>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.12> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
        END
        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.13> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = R.CRDT.CBE<SCB.C.CBE.TOT.US.13>
        END

        IF R.CRDT.CBE<SCB.C.CBE.TOT.US.45> GT R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> THEN
            R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = R.CRDT.CBE<SCB.C.CBE.TOT.US.45>
        END
    END
*------------"������ ������ �� �������� ��� �� 0 "-------------------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.1> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.1> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.2> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.2> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.3> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.3> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.4> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.4> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.5> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.5> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.6> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.6> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.7> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.7> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.8> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.8> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.9> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.9> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.10> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.10> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.11> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.11> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.12> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.12> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.13> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.13> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.45> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.US.45> = 0

    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.1> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.2> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.3> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.4> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.5> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.6> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.7> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.8> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.9> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.10> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.11> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.12> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> = 0
    IF R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> LT 0 THEN R.CRDT.CBE<SCB.C.CBE.TOT.CR.45> = 0
*====================================================================
    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> = 0
    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.1>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.2>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.3>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.4>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.5>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.6>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.7>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.8>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.9>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.10>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.11>

    R.CRDT.CBE<SCB.C.CBE.TOT.US.99> += R.CRDT.CBE<SCB.C.CBE.TOT.US.12>
*----------------- "  ���������������������� " --------------------
    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> = 0

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.1>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.2>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.3>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.4>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.5>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.6>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.7>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.8>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.9>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.10>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.11>

    R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> += R.CRDT.CBE<SCB.C.CBE.TOT.CR.12>
    CALL F.WRITE(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE)
    CALL JOURNAL.UPDATE(CCC.NO)

    CALL F.RELEASE(FN.CRDT.CBE,CCC.NO,F.CRDT.CBE)
    CLOSE F.CRDT.CBE
RETURN
*----------------- "����� �������� � ������  " --------------------
*----------------- " ����������������������  " --------------------
INSREC:
*------
*----------------- " ����� ��� ����� ������� " --------------------
    CO.CODE1 = ''
*Line [ 1578 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CCC.NO,CO.CODE1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CCC.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CO.CODE1=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    R.CRDT.CBE<SCB.C.CBE.CO.CODE> = CO.CODE1
*Line [ 1586 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CCC.NO,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CCC.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    IF LOCAL.REF<1,CULR.CBE.NO> THEN
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = LOCAL.REF<1,CULR.CBE.NO>
    END ELSE
        R.CRDT.CBE<SCB.C.CBE.CBE.NO> = "999999999999"
    END
    R.CRDT.CBE<SCB.C.CBE.BNK.DATE> = BNK.DATE1
*--------------------- " ����� ��� ����� ����� ���������� ------------
    IF R.CRDT.CBE<SCB.C.CBE.TOT.US.99> GE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.99> GE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.US.13> GE 0 OR R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> GE 0 THEN
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    IF SW2 = 1 THEN
        CALL F.WRITE(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE)
        CALL JOURNAL.UPDATE(CCC.NO)

        CALL F.RELEASE(FN.CRDT.CBE,CCC.NO,F.CRDT.CBE)
        CLOSE F.CRDT.CBE
    END
    GOSUB INIT0CR
RETURN
*---------------------------------------------------------------------
CHKALL:
*-----
    FN.CRDT.CBE = 'F.SCB.CREDIT.CBE'
    F.CRDT.CBE = ''
    R.CRDT.CBE = ''
    T.SEL = '' ; SELECTED = '' ; KEY.LIST = ''
***** BAKRY 2011/10/24    T.SEL = "SELECT ":FN.CRDT.CBE:" WITH TOT.US.99 GT TOT.CR.99"
    T.SEL = "SELECT ":FN.CRDT.CBE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN

        FOR I = 1 TO SELECTED
            CCC.NO = KEY.LIST<I>
            CALL F.READ(FN.CRDT.CBE,CCC.NO,R.CRDT.CBE,F.CRDT.CBE,ELM)
            GOSUB INSRECC
*            GOSUB INSREC
            GOSUB INIT0CR
        NEXT I
    END
RETURN
*---------------------------------------------------------------------
INSGUR:
*------
    GOSUB INIT0
    GOSUB INIT0CR
    SW1 = 0
    GUR.LIST="" ; GUR.SEL="" ;  ER.GUR=""
    SELECTED5 = ""
    R.CUS = ""
*---------------------------------------------------------------------
    GUR.SEL = "SELECT ":FN.CUS:" WITH RELATION.CODE EQ 26 OR RELATION.CODE EQ 27 BY @ID"
    CALL EB.READLIST(GUR.SEL,GUR.LIST,"",SELECTED5,ER.GUR)
    IF SELECTED5  THEN
        FOR U = 1 TO SELECTED5
            GOSUB INIT0CRGUR
            CALL F.READ(FN.CUS,GUR.LIST<U>,R.CUS,F.CUS,E.G1)
*---------------------------------------------------------------------
            CALL F.READ(FN.CRDT.CBE,GUR.LIST<U>,R.CRDT.CBE,F.CRDT.CBE,E11)
            IF E11 THEN GOSUB INIT0CR
*---------------------------------------------------------------------
            REL.COD = R.CUS<EB.CUS.RELATION.CODE>
*Line [ 1627 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            K1 = DCOUNT(REL.COD,@VM)
            FOR J = 1 TO K1
                IF R.CUS<EB.CUS.RELATION.CODE,J> = 26 OR R.CUS<EB.CUS.RELATION.CODE,J> = 27 THEN
                    R.CRDT.CBE1 = ''
                    CUS.ID1 = GUR.LIST<U>
                    REL.ID = R.CUS<EB.CUS.REL.CUSTOMER,J>
                    CALL F.READ(FN.CRDT.CBE,REL.ID,R.CRDT.CBE1,F.CRDT.CBE,E.G21)
                    IF NOT(E.G21) THEN
                        SW1 ++
                        IF R.CUS<EB.CUS.LOCAL.REF><1,CULR.BIC.CODE.NO> GT 0 THEN
                            R.CRDT.CBE<SCB.C.CBE.TOT.US.13> += R.CUS<EB.CUS.LOCAL.REF><1,CULR.BIC.CODE.NO>
                            R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> += R.CUS<EB.CUS.LOCAL.REF><1,CULR.BIC.CODE.NO>
                        END ELSE
                            R.CRDT.CBE<SCB.C.CBE.TOT.US.13> += R.CRDT.CBE1<SCB.C.CBE.TOT.US.99> + R.CRDT.CBE1<SCB.C.CBE.TOT.US.45>
                            R.CRDT.CBE<SCB.C.CBE.TOT.CR.13> += R.CRDT.CBE1<SCB.C.CBE.TOT.CR.99> + R.CRDT.CBE1<SCB.C.CBE.TOT.CR.45>
                        END
                    END
                END
            NEXT J
*******************************************************************
*--------------------- " ����� ��� ����� ����� ���������� -----------
            IF SW1 GT 0 THEN
                LOCAL.REF1 = R.CUS<EB.CUS.LOCAL.REF>
                R.CRDT.CBE<SCB.C.CBE.CBE.NO> = LOCAL.REF1<1,CULR.CBE.NO>
                R.CRDT.CBE<SCB.C.CBE.BNK.DATE> = BNK.DATE1
                R.CRDT.CBE<SCB.C.CBE.CO.CODE> = R.CUS<EB.CUS.COMPANY.BOOK>
                CALL F.WRITE(FN.CRDT.CBE,CUS.ID1,R.CRDT.CBE)
                CALL JOURNAL.UPDATE(CUS.ID1)

                CALL F.RELEASE(FN.CRDT.CBE,CUS.ID1,F.CRDT.CBE)
                CLOSE F.CRDT.CBE
            END
            SW1 = 0
            GOSUB INIT0CR
            GOSUB INIT0CRGUR
        NEXT U
    END
RETURN
*---------------------------------------------------------------------
