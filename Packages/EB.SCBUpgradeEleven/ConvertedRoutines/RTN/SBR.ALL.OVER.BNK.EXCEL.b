* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNjIzMzI6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*    PROGRAM SBR.ALL.OVER.BNK.EXCEL
    SUBROUTINE SBR.ALL.OVER.BNK.EXCEL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "SBR.ALL.OVER.BNK.EXCEL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBR.ALL.OVER.BNK.EXCEL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBR.ALL.OVER.BNK.EXCEL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBR.ALL.OVER.BNK.EXCEL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBR.ALL.OVER.BNK.EXCEL.CSV File IN &SAVEDLISTS&'
        END
    END
*------------------------------------------------
    FN.COM = 'F.COMPANY'  ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    T.SEL1  = "SELECT ":FN.COM:" WITH @ID NE 'EG0010088'"
    T.SEL1 := " AND @ID NE 'EG0010099' BY @ID"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            COMP = KEY.LIST1<I>

            GOSUB INITIATE
            GOSUB PRINT.HEAD
*Line [ 69 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 70 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
            GOSUB CALLDB

*            CALL PRINTER.OFF
*            CALL PRINTER.CLOSE(REPORT.ID,0,'')

        NEXT I
    END
    TEXT = "REPORT CREATED" ; CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
*    CALL PRINTER.ON(REPORT.ID,'')
    FLAG    = 0
    RETURN
*========================================================================
CALLDB:
    FN.AC = 'FBNK.ACCOUNT'      ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER'     ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT  = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.ENT.LW = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.ENT.LW  = ''
    CALL OPF(FN.ENT.LW,F.ENT.LW)

    DAT.ID    = 'EG0010001'
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.LWD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.LWD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

*************************************************************************

    T.SEL = "SELECT ":FN.ENT.LW:" WITH CO.CODE EQ ":COMP:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",NO.REC.LW,ER.SEL)
    IF NO.REC.LW THEN
        FOR Y = 1 TO NO.REC.LW
            CALL F.READ(FN.ENT.LW,KEY.LIST<Y>,R.ENT.LW,F.ENT.LW,EER.R.LW)
            LOOP
                REMOVE WS.STMT.ID FROM R.ENT.LW SETTING POS1
            WHILE WS.STMT.ID:POS1
                CALL F.READ(FN.STMT,WS.STMT.ID,R.STMT,F.STMT,ERR.STMT)

                WS.STMT.REF.ID = R.STMT<AC.STE.TRANS.REFERENCE>[1,2]

                IF WS.STMT.REF.ID EQ 'FT' OR WS.STMT.REF.ID EQ 'TT' OR WS.STMT.REF.ID EQ 'LD' OR WS.STMT.REF.ID EQ 'TF' THEN

                    WS.STMT.REF    = R.STMT<AC.STE.TRANS.REFERENCE>[1,12]
                    WS.REF.LEN     = LEN(R.STMT<AC.STE.TRANS.REFERENCE>)

                    IF WS.STMT.REF.ID EQ 'TF' THEN
                        IF WS.REF.LEN EQ 18 THEN WS.STMT.REF = R.STMT<AC.STE.TRANS.REFERENCE>[1,14]
                        IF WS.REF.LEN EQ 14 THEN WS.STMT.REF = R.STMT<AC.STE.TRANS.REFERENCE>[1,14]
                        IF WS.REF.LEN EQ 12 THEN WS.STMT.REF = R.STMT<AC.STE.TRANS.REFERENCE>[1,12]
                        IF WS.REF.LEN EQ 16 THEN WS.STMT.REF = R.STMT<AC.STE.TRANS.REFERENCE>[1,12]
                    END

                    WS.STMT.AC     = R.STMT<AC.STE.ACCOUNT.NUMBER>
                    WS.STMT.CUR    = R.STMT<AC.STE.CURRENCY>
                    IF WS.STMT.CUR EQ 'EGP' THEN
                        WS.STMT.AMT    = R.STMT<AC.STE.AMOUNT.LCY>
                    END ELSE
                        WS.STMT.AMT    = R.STMT<AC.STE.AMOUNT.FCY>
                    END

                    WS.STMT.CUS    = R.STMT<AC.STE.CUSTOMER.ID>
                    WS.STMT.TIME   = R.STMT<AC.STE.DATE.TIME>[7,4]
                    WS.TIME        = FMT(WS.STMT.TIME,"R##:##")
                    WS.STMT.DATE   = R.STMT<AC.STE.VALUE.DATE>
                    WS.DATE        = FMT(WS.STMT.DATE,"####/##/##")
                    WS.STMT.AUTH   = R.STMT<AC.STE.AUTHORISER>

                    WS.AUTH = FIELD(WS.STMT.AUTH,"_",2)
*Line [ 153 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR ('USER':@FM:EB.USE.USER.NAME,WS.AUTH,AUTH.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.AUTH,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
AUTH.NAME=R.ITSS.USER<EB.USE.USER.NAME>

*Line [ 161 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.STMT.CUS,LOCAL.REF.NE)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.STMT.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF.NE=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                    WS.CUS.NAME = LOCAL.REF.NE<1,CULR.ARABIC.NAME>
                    STE.OVER    = R.STMT<AC.STE.OVERRIDE>
                    FLAG        = 0

                    FINDSTR 'Unauthorised overdraft' IN STE.OVER SETTING FMS,VMS THEN
                        FLAG    = 1
                        IN.OVER = 'OVERDRAFT'
                    END

                    FINDSTR 'You have an Excess' IN STE.OVER SETTING FMS,VMS THEN
                        FLAG    = 1
                        IN.OVER = 'EXCEED.LIMIT'
                    END

                    CALL F.READ(FN.AC,WS.STMT.AC,R.AC,F.AC,E2)
                    CUR.BAL = R.AC<AC.WORKING.BALANCE>
                    OLD.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
                    OLD.REF = R.AC<AC.ALT.ACCT.ID>
*===============================================================
                    IF FLAG = 1 THEN
                        XX   = SPACE(132)
                        XX4  = SPACE(132)

                        XX<1,1>[1,20]     = ";":WS.STMT.AC
                        XX4<1,1>[1,20]    = ";":OLD.REF
                        XX<1,1>[20,35]    = ";":WS.CUS.NAME
                        XX4<1,1>[22,35]   = ";":WS.STMT.REF
                        XX<1,1>[57,10]    = ";":WS.STMT.CUR
                        XX<1,1>[72,10]    = ";":WS.STMT.AMT
                        XX4<1,1>[72,10]   = ";":CUR.BAL
                        XX<1,1>[85,10]    = ";":WS.TIME
                        XX4<1,1>[85,10]   = ";":OLD.BAL
                        XX<1,1>[97,10]    = ";":WS.DATE
                        XX<1,1>[113,35]   = ";":AUTH.NAME
                        XX4<1,1>[113,35]  = ";":IN.OVER

*PRINT XX<1,1>
*PRINT XX4<1,1>
*PRINT STR('-',130)
                        BB.DATA = ""
                        BB.DATA = XX<1,1>
                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END

                        BB.DATA = ""
                        BB.DATA = XX4<1,1>
                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END
                    END
                END
            REPEAT
        NEXT Y
    END
*************************************************************************

    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  ����� �������  ***'
*    PRINT XX25<1,1>
    BB.DATA = ""
    BB.DATA = XX25<1,1>
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*************************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*--------- 
*Line [ 238 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    DAT.ID.H = 'EG0010001'
*Line [ 247 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID.H,TD.LW)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID.H,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD.LW=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    TD1 = TD.LW[1,4]
    TD2 = TD.LW[5,2]
    TD3 = TD.LW[7,2]
    TD  = TD1:"/":TD2:"/":TD3

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  = "��� ���� ������"
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD = "��� :" :YYBRN
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD =" ������� : ":T.DAY
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD = "��� ������ : ":"'P'"

    PR.HD = "SBR.ALL.OVER.BNK.EXCEL"
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD = '���� ��� ����� ��������'
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD =";;;":"���� �������� ��� ��� �������� �� : ":TD
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD =";;;;"
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD  = ";":"��� ������"
    PR.HD := ";":"�����"
    PR.HD := ";":"������"
    PR.HD := ";":"������"
    PR.HD := ";":"�����"
    PR.HD := ";":"�������"
    PR.HD := ";":"���� �������"
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PR.HD   = ";":"�����  ������"
    PR.HD  := ";":"������"
    PR.HD  := ";":"����� �����"
    PR.HD  := ";":"����� ���"
    PR.HD  := ";":"��� �������"
    WRITESEQ PR.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*==============================================================
END
