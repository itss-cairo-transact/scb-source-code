* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNTYzMTY6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1269</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.LD.MONTH.BANK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB EVAL.RATE
    GOSUB PRINT.HEAD
*Line [ 47 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 48 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
**REPORT.ID='SBM.LD.MONTH'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    DATD = TODAY
*    DATM = DATD[1,6]
*    DAT = DATM:"01"
*    CALL CDT ('',DAT,'-1C')
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,COMP,DAT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,COMP,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    COUNT.EGP = 0 ; COUNT.SAR = 0
    COUNT.USD = 0 ; COUNT.GBP = 0
    COUNT.EUR = 0 ; COUNT.CHF = 0

    TOT.AMT.EGP = 0 ; TOT.AMT.SAR = 0
    TOT.AMT.USD = 0 ; TOT.AMT.GBP = 0
    TOT.AMT.EUR = 0 ; TOT.AMT.CHF = 0
    TOT.EQU = 0

    RETURN
*===============================================================
EVAL.RATE:
    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''
    CALL OPF(FN.BASE,F.BASE)
    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
*Line [ 79 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    XX = DCOUNT(R.BASE<RE.BCP.ORIGINAL.CCY>,@VM)
    FOR POS = 1 TO XX
        RATE = R.BASE<RE.BCP.RATE,POS>
        CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY,POS>
        BEGIN CASE
        CASE CURR.BASE = 'SAR'
            RATE.SAR = RATE
        CASE CURR.BASE = 'USD'
            RATE.USD = RATE
        CASE CURR.BASE = 'GBP'
            RATE.GBP = RATE
        CASE CURR.BASE = 'EUR'
            RATE.EUR = RATE
        CASE CURR.BASE = 'CHF'
            RATE.CHF = RATE
        CASE OTHERWISE
            RATE = 1
        END CASE
    NEXT POS
    RETURN
*===============================================================
CALLDB:
    DESC.CATG = ""
    TOT.EGP = '0'
    TOT.SAR = '0'
    TOT.USD = '0'
    TOT.GBP = '0'
    TOT.EUR = '0'
    TOT.CHF = '0'
    TOT.EQU.ALL = '0'

    TOT.COUNT.EGP = '0'
    TOT.COUNT.SAR = '0'
    TOT.COUNT.USD = '0'
    TOT.COUNT.GBP = '0'
    TOT.COUNT.EUR = '0'
    TOT.COUNT.CHF = '0'
    TOT.DEP.COUNT = '0'
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FOR TEMP.CATG = 21001 TO 21010
*        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ ":TEMP.CATG :" AND CO.CODE EQ ":COMP:" AND (( VALUE.DATE LE ":DAT:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":DAT:" AND AMOUNT EQ 0 )) BY CURRENCY"
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ ":TEMP.CATG :" AND (( VALUE.DATE LE ":DAT:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":DAT:" AND AMOUNT EQ 0 )) BY CURRENCY"
        KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            YY = ""
            TOT.AMT.EGP = '0'
            TOT.AMT.SAR = '0'
            TOT.AMT.USD = '0'
            TOT.AMT.GBP = '0'
            TOT.AMT.EUR = '0'
            TOT.AMT.CHF = '0'

            COUNT.EGP = '0'
            COUNT.SAR = '0'
            COUNT.USD = '0'
            COUNT.GBP = '0'
            COUNT.EUR = '0'
            COUNT.CHF = '0'
            TOT.COUNT = '0'
            TOT.EQU = '0'
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)

                IF R.LD<LD.AMOUNT> EQ 0 THEN
                    R.LD<LD.AMOUNT> = R.LD<LD.REIMBURSE.AMOUNT>
                END

                SS = R.LD<LD.CURRENCY>
                BEGIN CASE
                CASE R.LD<LD.CURRENCY> = 'EGP'
                    TOT.AMT.EGP = TOT.AMT.EGP + R.LD<LD.AMOUNT>
                    TOT.EQU = TOT.EQU + R.LD<LD.AMOUNT>
                    COUNT.EGP = COUNT.EGP + 1
                CASE R.LD<LD.CURRENCY> = 'SAR'

                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; UNEQU.COMM = "" ; CURR = ""
                    MARKET = ""
                    UNEQU.COMM = R.LD<LD.AMOUNT>
                    CURR = R.LD<LD.CURRENCY>
                    LCOMM = UNEQU.COMM * RATE.SAR
                    TOT.EQU = TOT.EQU + LCOMM
                    TOT.AMT.SAR = TOT.AMT.SAR + R.LD<LD.AMOUNT>
                    COUNT.SAR = COUNT.SAR + 1
                CASE R.LD<LD.CURRENCY> = 'USD'
                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; UNEQU.COMM = "" ; CURR = ""
                    MARKET = ""
                    UNEQU.COMM = R.LD<LD.AMOUNT>
                    CURR = R.LD<LD.CURRENCY>
                    LCOMM = UNEQU.COMM * RATE.USD
                    TOT.EQU = TOT.EQU + LCOMM
                    TOT.AMT.USD = TOT.AMT.USD + R.LD<LD.AMOUNT>
                    COUNT.USD = COUNT.USD + 1
                CASE R.LD<LD.CURRENCY> = 'GBP'
                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; UNEQU.COMM = "" ; CURR = ""
                    MARKET = ""
                    UNEQU.COMM = R.LD<LD.AMOUNT>
                    CURR = R.LD<LD.CURRENCY>
                    LCOMM = UNEQU.COMM * RATE.GBP
                    TOT.EQU = TOT.EQU + LCOMM
                    TOT.AMT.GBP = TOT.AMT.GBP + R.LD<LD.AMOUNT>
                    COUNT.GBP = COUNT.GBP + 1
                CASE R.LD<LD.CURRENCY> = 'EUR'
                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; UNEQU.COMM = "" ; CURR = ""
                    MARKET = ""
                    UNEQU.COMM = R.LD<LD.AMOUNT>
                    CURR = R.LD<LD.CURRENCY>
                    LCOMM = UNEQU.COMM * RATE.EUR
                    TOT.EQU = TOT.EQU + LCOMM
                    TOT.AMT.EUR = TOT.AMT.EUR + R.LD<LD.AMOUNT>
                    COUNT.EUR = COUNT.EUR + 1
                CASE R.LD<LD.CURRENCY> = 'CHF'
                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; UNEQU.COMM = "" ; CURR = ""
                    MARKET = ""
                    UNEQU.COMM = R.LD<LD.AMOUNT>
                    CURR = R.LD<LD.CURRENCY>
                    LCOMM = UNEQU.COMM * RATE.CHF
                    TOT.EQU = TOT.EQU + LCOMM
                    TOT.AMT.CHF = TOT.AMT.CHF + R.LD<LD.AMOUNT>
                    COUNT.CHF = COUNT.CHF + 1
                CASE OTHERWISE
                    ERR.AMOUNT = R.LD<LD.AMOUNT>
                END CASE

            NEXT I
        END ELSE
            ENQ.ERROR = "NO RECORDS FOUND"
        END
        PRINT ''
*Line [ 218 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,TEMP.CATG,DESC.CATG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,TEMP.CATG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
DESC.CATG=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
        TOT.COUNT = COUNT.EGP + COUNT.SAR + COUNT.USD + COUNT.GBP + COUNT.EUR + COUNT.CHF
        YY = ""
        YY<1,1>[1,15]   = TOT.COUNT
        YY<1,1>[20,15]  = COUNT.EGP
        YY<1,1>[37,15]  = COUNT.SAR
        YY<1,1>[52,15]  = COUNT.USD
        YY<1,1>[67,15]  = COUNT.GBP
        YY<1,1>[82,15]  = COUNT.EUR
        YY<1,1>[97,15]  = COUNT.CHF
        YY<1,1>[112,15] = "DEP NO"
        PRINT YY<1,1>
        YY = ""
        YY<1,1>[1,15]   = FMT(TOT.EQU,"R2,")
        YY<1,1>[20,15]  = FMT(TOT.AMT.EGP,"R2,")
        YY<1,1>[37,15]  = FMT(TOT.AMT.SAR,"R2,")
        YY<1,1>[52,15]  = FMT(TOT.AMT.USD,"R2,")
        YY<1,1>[67,15]  = FMT(TOT.AMT.GBP,"R2,")
        YY<1,1>[82,15]  = FMT(TOT.AMT.EUR,"R2,")
        YY<1,1>[97,15]  = FMT(TOT.AMT.CHF,"R2,")
        YY<1,1>[112,15] = DESC.CATG
        PRINT YY<1,1>

        PRINT STR('-',120)
        TOT.COUNT.EGP = TOT.COUNT.EGP + COUNT.EGP
        TOT.COUNT.SAR = TOT.COUNT.SAR + COUNT.SAR
        TOT.COUNT.USD = TOT.COUNT.USD + COUNT.USD
        TOT.COUNT.GBP = TOT.COUNT.GBP + COUNT.GBP
        TOT.COUNT.EUR = TOT.COUNT.EUR + COUNT.EUR
        TOT.COUNT.CHF = TOT.COUNT.CHF + COUNT.CHF

        TOT.EGP = TOT.EGP + TOT.AMT.EGP
        TOT.SAR = TOT.SAR + TOT.AMT.SAR
        TOT.USD = TOT.USD + TOT.AMT.USD
        TOT.GBP = TOT.GBP + TOT.AMT.GBP
        TOT.EUR = TOT.EUR + TOT.AMT.EUR
        TOT.CHF = TOT.CHF + TOT.AMT.CHF
        TOT.EQU.ALL = TOT.EQU.ALL + TOT.EQU
        YY = ""
        TOT.AMT.EGP = '0'
        TOT.AMT.SAR = '0'
        TOT.AMT.USD = '0'
        TOT.AMT.GBP = '0'
        TOT.AMT.EUR = '0'
        TOT.AMT.CHF = '0'
        TOT.EQU = '0'
        COUNT.EGP = '0'
        COUNT.SAR = '0'
        COUNT.USD = '0'
        COUNT.GBP = '0'
        COUNT.EUR = '0'
        COUNT.CHF = '0'
        TOT.COUNT = '0'

    NEXT TEMP.CATG
    TOT.DEP.COUNT = TOT.COUNT.EGP+TOT.COUNT.SAR+TOT.COUNT.USD+TOT.COUNT.GBP+TOT.COUNT.EUR+TOT.COUNT.CHF
    YY = ""
    YY<1,1>[1,15]   = TOT.DEP.COUNT
    YY<1,1>[20,15]  = TOT.COUNT.EGP
    YY<1,1>[35,15]  = TOT.COUNT.SAR
    YY<1,1>[50,15]  = TOT.COUNT.USD
    YY<1,1>[65,15]  = TOT.COUNT.GBP
    YY<1,1>[80,15]  = TOT.COUNT.EUR
    YY<1,1>[95,15]  = TOT.COUNT.CHF
    YY<1,1>[110,15] = "TOTAL DEP NO"
    PRINT YY<1,1>
    YY = ""
    YY<1,1>[1,15]   = FMT(TOT.EQU.ALL,"R2,")
    YY<1,2>[20,15]  = FMT(TOT.EGP,"R2,")
    YY<1,1>[35,15]  = FMT(TOT.SAR,"R2,")
    YY<1,2>[50,15]  = FMT(TOT.USD,"R2,")
    YY<1,1>[65,15]  = FMT(TOT.GBP,"R2,")
    YY<1,2>[80,15]  = FMT(TOT.EUR,"R2,")
    YY<1,1>[95,15]  = FMT(TOT.CHF,"R2,")
    YY<1,1>[110,15] = "TOTAL"
    PRINT YY<1,1>
    PRINT YY<1,2>
    PRINT STR('=',120)
*    PRINT "TOTAL FCY = ":DROUND((TOT.EQU.ALL-TOT.EGP),2)
    PRINT "TOTAL FCY = ":FMT(TOT.EQU.ALL-TOT.EGP,"R2,")

    RETURN

*===============================================================
PRINT.HEAD:
*Line [ 309 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
*T.DAY1 = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    T.DAY1 = T.DAY

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"SBM.LD.MONTH.BANK"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������� ���� ���� ������� ����� ��� �������"
    PR.HD :="'L'":SPACE(60):"���� : ":T.DAY1
    PR.HD :="'L'":SPACE(45):STR('_',60)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"TOTAL EQUIVALENT":SPACE(5):"EGP":SPACE(12):"SAR" :SPACE(10):"USD":SPACE(12):"GBP ":SPACE(10):"EUR ":SPACE(10):"CHF"
    PR.HD :="'L'":SPACE(1):STR('=',120)

    HEADING PR.HD
    RETURN
*==============================================================
END
