* @ValidationCode : MjotMjY1NzQzNTY1OkNwMTI1MjoxNjQ0OTI1MDUwNDc1OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-135</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY MOHAMED SABRY 2015/8/31 ****
*********************************************

* SUBROUTINE  SBM.CU.EGSERV.003
PROGRAM SBM.CU.EGSERV.003

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.POST.LIST
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EGSRV.INDEX

**************************************************************

    GOSUB INITIALISE

    GOSUB CU.SEL

RETURN
**************************************************************
INITIALISE:
*----------

    FN.CU = 'FBNK.CUSTOMER'  ; F.CU = '' ; R.CU= ''
    CALL OPF(FN.CU,F.CU)

    FN.EGI = 'F.SCB.EGSRV.INDEX'  ; F.EGI = '' ; R.EGI = ''
    CALL OPF(FN.EGI,F.EGI)

    FN.REG = 'F.SCB.CUS.REGION'  ; F.REG = '' ; R.REG = ''
    CALL OPF(FN.REG,F.REG)

    FN.GOV = 'F.SCB.CUS.GOVERNORATE'  ; F.GOV = '' ; R.GOV = ''
    CALL OPF(FN.GOV,F.GOV)

    FN.SPL = 'F.SCB.POST.LIST'  ; F.SPL = '' ; R.SPL= ''
    CALL OPF(FN.SPL,F.SPL)

    FN.EGI = 'F.SCB.EGSRV.INDEX'  ; F.EGI = '' ; R.EGI = ''
    CALL OPF(FN.EGI,F.EGI)

    WS.FLD.NO = 0

RETURN
**************************************************************
**************************************************************
CU.SEL:
    GOSUB INIT.LD.FILE

*    T.SEL  = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND CREDIT.STAT EQ '' AND CREDIT.CODE LE 100 AND "
*    T.SEL := "DRMNT.CODE EQ '' AND SCCD.CUSTOMER NE 'YES' AND COMPANY.BOOK EQ 'EG0010015' AND @ID NE 994] "
*    T.SEL := "DRMNT.CODE EQ '' AND SCCD.CUSTOMER NE 'YES' AND COMPANY.BOOK NE 'EG0010099' AND @ID NE 994] "
*    T.SEL := "WITHOUT SECTOR IN (5010 5020) AND GOVERNORATE NE 98 AND REGION NE 998 "
*    T.SEL := " AND GOVERNORATE NE 999 AND REGION NE 999 BY @ID"

    T.SEL  = "SELECT ":FN.SPL:" WITH COMPANY.CODE NE 'EG0010099' AND GOV.CODE NE 98 AND GOV.CODE NE 999 BY @ID "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT "SELECTED = ":SELECTED
    IF SELECTED THEN
        FOR ICU = 1 TO SELECTED
            WS.CU.DATA = ''
            WS.GOV.ID  = ''
            WS.REG.ID  = ''
            WS.FIN.ADD = ''
            WS.FIN.TEL = ''
            WS.REG.NAME = ''
            WS.GOV.NAME = ''

            CALL F.READ(FN.CU,KEY.LIST<ICU>,R.CU,F.CU,ER.CU)

            WS.EGI.ID = R.CU<EB.CUS.LOCAL.REF><1,CULR.EGSRV.SRL>

            CALL F.READ(FN.EGI,WS.EGI.ID,R.EGI,F.EGI,ER.EGI)

            WS.GOV.ID  = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,WS.GOV.ID,WS.GOV.NAME)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,WS.GOV.ID,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
WS.GOV.NAME=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>

            WS.REG.ID  = R.CU<EB.CUS.LOCAL.REF><1,CULR.REGION>
*Line [ 121 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,WS.REG.ID,WS.REG.NAME)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,WS.REG.ID,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
WS.REG.NAME=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>

            WS.ADD     = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
            WS.TEL     = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
*Line [ 119 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            WS.ADD.CNT = DCOUNT(WS.ADD,@SM)
*Line [ 121 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            WS.TEL.CNT = DCOUNT(WS.TEL,@SM)

            FOR IADD = 1 TO WS.ADD.CNT
                WS.FIN.ADD := WS.ADD<1,1,IADD>:' '
            NEXT IADD

            WS.FIN.ADD = WS.FIN.ADD :" ":WS.GOV.NAME:" ":WS.REG.NAME

            FOR ITEL = 1 TO WS.TEL.CNT
                WS.FIN.TEL := WS.TEL<1,1,ITEL>:' '
            NEXT ITEL

            WS.CU.DATA  = R.CU<EB.CUS.LOCAL.REF><1,CULR.EGSRV.SRL>:','
            WS.CU.DATA := R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:" ":R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>:','
            WS.CU.DATA := WS.FIN.ADD:','
            WS.CU.DATA := WS.FIN.TEL:','
            WS.CU.DATA := R.EGI<EGSRV.EGSERV.CODE>:','
            WS.CU.DATA := R.EGI<EGSRV.EGSERV.BAR>:','
            BB.IN.DATA  = WS.CU.DATA
            WRITESEQ BB.IN.DATA TO BB.IN ELSE
            END
        NEXT ICU
    END
RETURN

**************************************************************
*    BB.IN.DATA  = SCB.OFS.MESSAGE
*    WRITESEQ BB.IN.DATA TO BB.IN ELSE
*    END

RETURN
**************************************************************
INIT.LD.FILE:

    WS.FILE.NAME = 'SBM.CU.EGSERV.003.CSV'

    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':WS.FILE.NAME
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.IN'
        END
    END

*    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT THEN
*        CLOSESEQ BB.OUT
*        HUSH ON
*        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':WS.FILE.NAME
*        HUSH OFF
*    END
*    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT ELSE
*        CREATE BB.OUT THEN
*        END ELSE
*            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.OUT'
*        END
*    END
RETURN
**************************************************************
