* @ValidationCode : MjotMTM5ODczODkwMjpDcDEyNTI6MTY0NDkyNTA2NTk2MTpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>253</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBR.CUS.DEP.Q.002

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
    REPORT.ID = 'SBR.CUS.DEP.Q'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter CUSTOMER No. : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    ID = COMI

    YTEXT    = "Enter Date from YYYYMMDD : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATF     = COMI
    DAT.FROM = DATF
    CALL CDT("",DAT.FROM,'-1W')

    YTEXT    = "Enter Date to YYYYMMDD : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATT     = COMI
    DAT.TO   = DATT
    CALL LAST.WDAY(DAT.TO)

    DAT.TO.AC = DAT.TO
    CALL CDT("",DAT.TO.AC,'+1W')

    FN.LD.H  = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD    = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.CTE = 'FBNK.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)


    FN.POS = 'F.SCB.CUS.POS'
    F.POS = '' ; R.POS = ''
    CALL OPF(FN.POS,F.POS)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""

    XX  = SPACE(132)
    XX1 = SPACE(132)
    XX2 = SPACE(132)
    XX3 = SPACE(132)
    XX4 = SPACE(132)

    AMT.LD.LCY    = 0
    AMT.LD.H.LCY  = 0
    AMT.LD.H.LCY1 = 0
    AMT.LD.H.LCY2 = 0
    AMT.PL        = 0

    DATD = TODAY
    DATM = DATD[1,6]
    DAT = DATM:"01"
    CALL CDT ('',DAT,'-1C')

RETURN
*========================================================================
PROCESS:
*** ������� ��� ������ ***
*--------------------------

    T.SEL = "SELECT ":FN.POS: " WITH CUSTOMER EQ ":ID:" AND SYS.DATE EQ ":DAT.TO:" AND ((CATEGORY GE 21001 AND CATEGORY LE 21010) OR (CATEGORY GE 21020 AND CATEGORY LE 21032))"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.POS,KEY.LIST<Z>,R.POS,F.POS,E1)
            AMT.LD = R.POS<CUPOS.DEAL.AMOUNT>

            CUR.LD = R.POS<CUPOS.DEAL.CCY>
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 157 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR.LD IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE.LD = R.CCY<RE.BCP.RATE,POS>
            AMT.LD.LCY += AMT.LD * RATE.LD

        NEXT Z
    END

    CALL F.READ(FN.CUS.ACC,ID,R.CUS.ACC,F.CUS.ACC,ETEXT)

    LOOP
        REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
    WHILE AC.ID:POS.ACCT
        CALL F.READ(FN.ACCT,AC.ID,R.ACCT,F.ACCT,ER.ACCT)
        AC.CATEG = R.ACCT<AC.CATEGORY>
        AC.CUR   = R.ACCT<AC.CURRENCY>
        IF (AC.CATEG GE 1001 AND AC.CATEG LE 1003) OR (AC.CATEG GE 6501 AND AC.CATEG LE 6504) OR (AC.CATEG GE 3010 AND AC.CATEG LE 3017) OR (AC.CATEG EQ 6511 OR AC.CATEG EQ 6512 OR AC.CATEG EQ 3005) THEN
            CALL EB.ACCT.ENTRY.LIST(AC.ID<1>,DAT.TO.AC,DAT.TO.AC,STMT.ID.LIST,OPENING.BAL,ER)

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 177 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE AC.CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE.AC = R.CCY<RE.BCP.RATE,POS>
            AMT.LD.LCY += OPENING.BAL * RATE.AC

        END
    REPEAT
**********************************************************************************
*** ������� ��� ������ ***
*--------------------------

    T.SEL1 = "SELECT ":FN.POS: " WITH CUSTOMER EQ ":ID:" AND SYS.DATE EQ ":DAT.FROM:" AND ((CATEGORY GE 21001 AND CATEGORY LE 21010) OR (CATEGORY GE 21020 AND CATEGORY LE 21032))"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.POS,KEY.LIST1<I>,R.POS,F.POS,E1)
            AMT.LD.H      = R.POS<CUPOS.DEAL.AMOUNT>
            CUR.LD.H = R.POS<CUPOS.DEAL.CCY>
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 197 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR.LD.H IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE.LD.H = R.CCY<RE.BCP.RATE,POS>
            AMT.LD.H.LCY += AMT.LD.H * RATE.LD.H
        NEXT I
    END

    CALL F.READ(FN.CUS.ACC,ID,R.CUS.ACC,F.CUS.ACC,ETEXT)

    LOOP
        REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
    WHILE AC.ID:POS.ACCT
        CALL F.READ(FN.ACCT,AC.ID,R.ACCT,F.ACCT,ER.ACCT)
        AC.CATEG = R.ACCT<AC.CATEGORY>
        AC.CUR   = R.ACCT<AC.CURRENCY>
        IF (AC.CATEG GE 1001 AND AC.CATEG LE 1003) OR (AC.CATEG GE 6501 AND AC.CATEG LE 6504) OR (AC.CATEG GE 3010 AND AC.CATEG LE 3017) OR (AC.CATEG EQ 6511 OR AC.CATEG EQ 6512 OR AC.CATEG EQ 3005) THEN
            CALL EB.ACCT.ENTRY.LIST(AC.ID<1>,DAT.FROM,DAT.FROM,STMT.ID.LIST,OPENING.BAL,ER)

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 216 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE AC.CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE.AC = R.CCY<RE.BCP.RATE,POS>
            AMT.LD.H.LCY += OPENING.BAL * RATE.AC

        END
    REPEAT

**********************************************************************************
*** ������� ���� �� ����� ���� ������ ***
*---------------------------------------
    AMT = AMT.LD.LCY - AMT.LD.H.LCY
    IF AMT GT 0 THEN
        AMT.LD.H.LCY1 = AMT
    END
******************************************************************

*** ������� �������� ���� ������ ***
*---------------------------------------
    IF AMT LT 0 THEN
        AMT.LD.H.LCY2 = AMT
    END
***********************************************************************
*** ����� ������� ***
*---------------------
    T.SEL3  = "SELECT ":FN.CTE:" WITH CUSTOMER.ID EQ ":ID:" AND PL.CATEGORY GE 50000 AND PL.CATEGORY LE 50999"
    T.SEL3 := " AND BOOKING.DATE GE ":DAT.FROM:" AND BOOKING.DATE LE ":DAT.TO
    T.SEL3 := " AND (( PRODUCT.CATEGORY GE 21001 AND PRODUCT.CATEGORY LE 21010 )"
    T.SEL3 := " OR ( PRODUCT.CATEGORY GE 21020 AND PRODUCT.CATEGORY LE 21032 )"
    T.SEL3 := " OR ( PRODUCT.CATEGORY GE 1001 AND PRODUCT.CATEGORY LE 1003 )"
    T.SEL3 := " OR ( PRODUCT.CATEGORY GE 6501 AND PRODUCT.CATEGORY LE 6504 )"
    T.SEL3 := " OR ( PRODUCT.CATEGORY GE 3010 AND PRODUCT.CATEGORY LE 3017 )"
    T.SEL3 := " OR PRODUCT.CATEGORY IN (3005 6511 6512))"

    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    IF SELECTED3 THEN
        FOR KK = 1 TO SELECTED3
            CALL F.READ(FN.CTE,KEY.LIST3<KK>,R.CTE,F.CTE,ER.CTE)
            AMT.PL += R.CTE<AC.CAT.AMOUNT.LCY>
        NEXT KK
    END
***********************************************************************
    XX<1,1>[1,35]   = "������� �� ��� ������"
    XX<1,1>[40,20]  =  AMT.LD.H.LCY

    XX1<1,1>[1,35]  = "������� ���� �� ����� ���� ������"
    XX1<1,1>[40,20] = AMT.LD.H.LCY1

    XX2<1,1>[1,35]  = "������� �������� ���� ������"
    XX2<1,1>[40,20] = AMT.LD.H.LCY2

    XX3<1,1>[1,35]  = "������� �� ��� ������"
    XX3<1,1>[40,20] = AMT.LD.LCY

    XX4<1,1>[1,35]  = "����� �������"
    XX4<1,1>[40,20] = AMT.PL


    PRINT XX<1,1>
    PRINT STR('-',55)
    PRINT XX1<1,1>
    PRINT STR('-',55)
    PRINT XX2<1,1>
    PRINT STR('-',55)
    PRINT XX3<1,1>
    PRINT STR('-',55)
    PRINT XX4<1,1>
    PRINT STR(' ',55)
*===============================================================
    PRINT STR('=',55)
*===============================================================
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 291 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,ID,COMP)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
*Line [ 298 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    DATF1  = DATF[7,2]:'/':DATF[5,2]:"/":DATF[1,4]
    DATT1  = DATT[7,2]:'/':DATT[5,2]:"/":DATT[1,4]

    CUST.ID = ID
*Line [ 313 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"���� ������� �������"
    PR.HD :="'L'":SPACE(55):"��":" ":DATF1:" ":"���":" ":DATT1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������� : ":CUST.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������� : ":CUST.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":"������� �������":SPACE(25):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
