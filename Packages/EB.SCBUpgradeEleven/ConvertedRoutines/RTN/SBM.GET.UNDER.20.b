* @ValidationCode : Mjo0ODE5OTg3MjU6Q3AxMjUyOjE2NDQ5MjUwNTU0MDQ6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
******* NOHA *******

SUBROUTINE SBM.GET.UNDER.20

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCR.ACCT.CR
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 42 ] Hashing I_F.SCB.STMT.ACCT.SAVE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.STMT.ACCT.SAVE

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    TEXT = " REPORT.CREATED "   ; CALL REM

RETURN

*==========================
INITIATE:
    TOT.BAL = 0
    NO.OF.ACC = 0

    FN.CU  = 'FBNK.CUSTOMER'           ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC  = 'FBNK.ACCOUNT'            ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.BR  = 'F.DEPT.ACCT.OFFICER'  ; F.BR = '' ; R.BR = ''
    CALL OPF(FN.BR,F.BR)

    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    COMP       = ID.COMPANY
    IF MONTH[1,1] EQ 0 THEN
        MONTH = MONTH[2,1]
    END

    FTEXT = "Date from. : "
    CALL TXTINP(FTEXT, 8, 22, "8", "D")
    DATE.FROM = COMI
    TTEXT = "Date to. : "
    CALL TXTINP(TTEXT, 8, 22, "8", "D")
    DATE.TO = COMI


*============== REPORT CREATION ===========

    REPORT.ID = 'SBM.GET.UNDER.20'
    CALL PRINTER.ON(REPORT.ID,'')

RETURN
*=====================================================
PROCESS:
    Y.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (6501 6502 6503 6504)"
*****    Y.SEL := " AND POSTING.RESTRICT LT 90 AND OPENING.DATE GE ":DATE.FROM:" AND OPENING.DATE LE ":DATE.TO:" BY ACCOUNT.OFFICER"
    Y.SEL := " AND POSTING.RESTRICT LT 90 AND OPENING.DATE GE ":DATE.FROM:" AND OPENING.DATE LE ":DATE.TO:" BY CO.CODE"

    CALL EB.READLIST(Y.SEL, KEY.LIST, "", SELECTED, ASD1)
    IF  SELECTED NE 0 THEN
        FOR I = 1 TO SELECTED

            ACCT.NO   = KEY.LIST<I>
            CALL F.READ( FN.AC,ACCT.NO, R.AC, F.AC, AC.ERR)

            IF AC.ERR EQ '' THEN
                CUS.NO     = R.AC<AC.CUSTOMER>
                CALL F.READ(FN.CU,CUS.NO, R.CU, F.CU, CU.ERR)
*        TEXT = "CUS.NO : ":CUS.NO ; CALL REM
                CU.BIRTH.DATE =  ' '
                CU.AGE        =  ' '
                DAYS          =  'C'
                CU.BIRTH.DATE =  R.CU<EB.CUS.BIRTH.INCORP.DATE>
*    IF CU.BIRTH.DATE LT 19880101 THEN DEBUG
                CALL CDD("", CU.BIRTH.DATE,TODAY.DATE,DAYS)
                CU.AGE        = DAYS/365

*        TEXT = "AGE ":CU.AGE ; CALL REM
*        TEXT = "CUS.NO : ":CUS.NO ; CALL REM
*        TEXT = "BIR.DATE : ":CU.BIRTH.DATE ; CALL REM
*        TEXT = "DAYS    : ":DAYS ; CALL REM
                IF CU.AGE LE 20 THEN
                    OLD.BAL    = R.AC<AC.ONLINE.ACTUAL.BAL>
                    CUS.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                    DEPT.CODE  = R.AC<AC.CO.CODE>[2]
                    DEPT.CODE  = TRIM(DEPT.CODE,"0","L")

                    CALL F.READ(FN.BR,DEPT.CODE, R.BR, F.BR, BR.ERR)
                    DEPT.NAME.1 = R.BR<EB.DAO.NAME>
                    DEPT.NAME   = FIELD(DEPT.NAME.1,'.',2)
                    NO.OF.ACC = NO.OF.ACC + 1
                    TOT.BAL   = TOT.BAL + OLD.BAL
                    GOSUB WRITE
                END
            END
        NEXT I
    END
    GOSUB TAIL.1
RETURN
*================ WRITE HEADER  =======================
PRINT.HEAD:
    COMP = COMP[2]
    IF COMP[1,1] EQ 0 THEN
        COMP = COMP[2,1]
    END

*Line [ 144 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,COMP,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,COMP,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� " :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������ ������� ����� (��� ��� )"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�����":SPACE(10):"��� ������":SPACE(10):"��� ������":SPACE(30):"��� ������":SPACE(10):"������":SPACE(10):"� �������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

RETURN
*================== WRITE DATA =========================
WRITE:
    T.DAY1  = CU.BIRTH.DATE[7,2]:'/':CU.BIRTH.DATE[5,2]:"/":CU.BIRTH.DATE[1,4]
    XX3 = SPACE(132)
    XX3 = ''
    XX3<1,1>[1,15]    = DEPT.NAME
    XX3<1,1>[15,10]   = CUS.NO
    XX3<1,1>[30,30]   = CUS.NAME
    XX3<1,1>[75,20]   = ACCT.NO
    XX3<1,1>[95,10]   = OLD.BAL
    XX3<1,1>[110,10]  = T.DAY1

    PRINT XX3<1,1>
    PRINT
RETURN

*================= TAIL ================================
TAIL.1:

    PRINT STR('=',125)
    PRINT ; PRINT SPACE(5):" ������ ��� �������� = ":NO.OF.ACC:SPACE(15):" ������ �������=  ":TOT.BAL
    PRINT STR('=',125)
RETURN
END
