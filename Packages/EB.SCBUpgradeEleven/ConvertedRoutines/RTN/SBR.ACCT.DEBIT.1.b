* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*------------------------CREATED BY REHA* -------  21/11/2010-------*
*----------------------------------------------------------------------------
* <Rating>85</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SBR.ACCT.DEBIT.1(ENQ)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    RR = 0
    FN.DB  = "FBNK.ACCOUNT.DEBIT.INT" ; F.DB   = "" ; R.DB  = ""
    CALL OPF(FN.DB,F.DB)

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC  = "" ; R.ACC  = ""
    CALL OPF(FN.ACC,F.ACC)

    Y.SEL  = "SELECT FBNK.ACCOUNT.DEBIT.INT WITH ( INTEREST.DAY.BASIS NE 'NONE' AND INTEREST.DAY.BASIS NE 'GENERAL' )"
    Y.SEL := " AND DR.BASIC.RATE EQ '' AND CO.CODE EQ ":COMP:" BY @ID "

* Y.SEL  =  "SELECT FBNK.ACCOUNT.DEBIT.INT WITH ( @ID LIKE 0120094110140401... OR @ID LIKE 0110010510140502... OR @ID LIKE 0130148410150204... OR @ID LIKE 0130148410150205...) BY @ID"

    CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

*  TEXT = "SELECTED" : SELECTED ;CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.DB,KEY.LIST<I>,R.DB,F.DB,READ.ERR)
            CALL F.READ(FN.DB,KEY.LIST<I+1>,R.DB,F.DB,READ.ERR2)
            AC.ID = KEY.LIST<I>
            AC.ID.1 = KEY.LIST<I+1>

ACC.ID = KEY.LIST<I>[1,16]

            IF AC.ID[1,16] NE AC.ID.1[1,16] THEN

                CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,READ.ERR3)
                CATEG = R.ACC<AC.CATEGORY>
               * REC = R.ACC<AC.RECORD.STATUS>

                IF CATEG NE '1401' AND CATEG NE '1220' AND CATEG NE '9090' AND CATEG NE '1402' AND CATEG NE '1403' AND CATEG NE '1405' AND CATEG NE '1406' AND CATEG NE '1499' AND CATEG NE '' THEN

*   TEXT = "CATEG" : CATEG ; CALL REM
*   TEXT = "KK " : KEY.LIST<I> ;CALL REM
                    RR = RR + 1
                    ENQ<2,RR> = '@ID'
                    ENQ<3,RR> = 'EQ'
                    ENQ<4,RR> =  KEY.LIST<I>
                END ELSE
                    RR++
                    ENQ<2,RR> = '@ID'
                    ENQ<3,RR> = 'EQ'
                    ENQ<4,RR> =  'DUM'

                END
            END

        NEXT I
      END ELSE
        RR++
        ENQ<2,RR> = '@ID'
        ENQ<3,RR> = 'EQ'
        ENQ<4,RR> = 'DUMMY'


    END

**********************
    RETURN
END
