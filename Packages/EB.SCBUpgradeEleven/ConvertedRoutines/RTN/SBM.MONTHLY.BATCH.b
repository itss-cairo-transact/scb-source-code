* @ValidationCode : MjotMTcwNDE4MTI4NjpDcDEyNTI6MTY0MDc5NDgxMTg5NDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:20:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMMED SABRY 2010/07/19
*-----------------------------------------------------------------------------
PROGRAM SBM.MONTHLY.BATCH
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TOPCUS.CR.TOT
******************************
    EXECUTE "OFSADMINLOGIN"
******************************

** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

***--------------- CREATE FILE  SCB.CUS.POS -----------------------***

    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"VVR.CUS.REC.H")

***---------------        CREATE CBE FILES      -------------------***
***---------------    FOR MR. Mohsen Programs   -------------------***

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.1A")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.1B")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.1")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.5")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.3")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.4")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.00.PD")
***---------------     CREATE FILE STATIC.MAST -------------------***

*   CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.01")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.03")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.MAST.02")

***---------------     CREATE FILE CATEG.MAS     -------------------***

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBD.CRT.TRIAL.02")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBD.CRT.TRIAL.01")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBD.CRT.TRIAL.03")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBD.CRT.TRIAL.04")

***-----------------------------------------------------------------***

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.PRT.CRT.01")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.1620.1")

***--------------- CREATE FILE CBE.BANK.FIN.POS -------------------***

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.FIN.POS.01")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.FIN.POS.03")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.FIN.POS.02")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.FIN.POS.05")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.FIN.POS.06")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.C.CRT.FIN.POS.07.NEW")

***---------------    CHANGES IN LOANS AND DEPOSITS PROGRAMS -------------------***
***---------------               Khaled Ibrahim              -------------------***
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.STATIC.AC.LD.02")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.STATIC.AC.LD.03")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.STATIC.AC.LD.02.DETAIL")
*** CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.STATIC.AC.LD.03.DETAIL")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.STATIC.AC.LD.01")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.STATIC.AC.LD.01.DETAIL")

***--------------- CLEAR FILE SCB.TOPCUS.CR.TOT -------------------***
***---------------        Mohammed Sabry        -------------------***

    FN.TOP.TOT = "F.SCB.TOPCUS.CR.TOT"
    F.TOP.TOT = ''
    CALL OPF(FN.TOP.TOT,F.TOP.TOT)
    OPEN FN.TOP.TOT TO FILEVAR ELSE ABORT 201, FN.TOP.TOT
    CLEARFILE FILEVAR

***--------------- CREATE FILE SCB.TOPCUS.CR.TOT  ------------------***
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"E.TOPCUS.ALL")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"E.TOPCUS.LCY")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"E.TOPCUS.FCY")
***------------- Reham Yousif ---------------------***
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.LG.SEC.11")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.LG.SEC.1")
***--------------CREATE FILES OF TOP50------------***
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.TOP50")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.TOP50.GET.TOTALS")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.TOP50.GROUP")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.TOP50.CUR.LIST")
*-----------------------------------------------------
***              HARES M. MAHMOUD
***  DRMNT PROGRAM TO CRAETE DRMNT FLAG IN CUSTOMER FILE
*** STOPED IN 2014/01/21 AND REPLACED ON SBW.WEEKLY.BATCH BY MOHAMED SABRY
*** CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"SBM.DRMNT.UPD.10.ALL")
***---------------  ����� �������� �������� ������ ������� ������ ��������- -------------****
***---------------                MOHAMED SABRY 2011/06/30              ---------------****
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.GET.LIMIT.DATA")
***--------------- ������ ������� ������ �������� ����  ------------------***
*** ADD BY MOHAMED SABRY 2011/08/03
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CREDIT.STATIC")

*** ADD BY Ahmed Hafez  2016/02/29
***------- ���� ����� ����� ������� ------------*******
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CUS.CR.CREATE")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CUS.CR.TOT")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CUS.CR.TOT.FCY")

*-------------------------------------------------------------------
****** ����� 6002 ����� ����� ������� ********
*** ADD BY KHALED IBRAHIM *******
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.6002.EGP")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.6002.USD")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.6002.EUR")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.6002.CCY")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.1600.USD")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.1600.USD.2")

*-------------------------------------------------------------------
**--- EDIT BY NESSMA ON 2018/03/27
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBD.CUSTOMER.LIST.ALL")
*-------------------------------------------------------------------
**** ������ ������� ***** 2018/10/03  *****

    WS.DAT = TODAY[5,2]

    IF WS.DAT EQ '01' OR WS.DAT EQ '04' OR WS.DAT EQ '07' OR WS.DAT EQ '10' THEN

        CALL FSCB.RUN.JOB(WS.P,WS.Q,WS.A,"SBQ.LOAN.STAMP.EXP")
        CALL FSCB.RUN.JOB(WS.P,WS.Q,WS.A,"SBQ.LOANS.STAMP")
    END
*-------------------------------------------------------------------
**** 2019/03/13 **** ACCOUNTS DAYS PAST DUE *****

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.ACCT.PD.FILE")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.ACCT.PD.DPD")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.ACCT.PD.TXT")

*-------------------------------------------------------------------
**** 2020/03/02 **** ECL FILES

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.ECL.FILES")

*-------------------------------------------------------------------
**** 2020/06/30 **** CBE 8% **** BAKRY

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.LOAN.8.DIFF.CBE")

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.DIFF.NEW")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.DIFF.NEW.21064")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.OVRD.DIFF.NEW")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.DIFF.NEW.21066")
    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.CBE.DIFF.NEW.21068")
*----------------------------------------------
*** 2021/06/03 **** SEGM AVRG ***

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.GENLEDALL.SEGM.AL.AVRG.REPORT")

*--------------------------------------
*** 2021/07/14 **** ELITE CUSTOMER ***

    CALL FSCB.RUN.JOB(WS.P,WS.M,WS.A,"SBM.VIP.CUS.CHARGE")

*---------------------------------------
RETURN

END
