* @ValidationCode : MjotMjg1NzcxMzAyOkNwMTI1MjoxNjQ0OTI1MDYwMDQwOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
******* NOHA *******

SUBROUTINE SBM.UNDER.NEW.CUS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCR.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STMT.ACCT.SAVE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
***************NESSMA***************************
    IF XX3 = '' THEN
        XX3<1,1> = "******************�� ���� ������*****************"
        PRINT XX3<1,1>

    END
************************************************
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    TEXT = " REPORT.CREATED "   ; CALL REM

RETURN

*==========================
INITIATE:
    XX3 = SPACE(132)
    XX3 = ''
    FN.SA  = 'F.SCB.STMT.ACCT.SAVE' ; F.SA = '' ; R.SA = ''
    CALL OPF(FN.SA,F.SA)
    FN.CU  = 'FBNK.CUSTOMER'        ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC  = 'FBNK.ACCOUNT'         ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.ST  = 'FBNK.STMT.ACCT.CR'    ; F.ST = '' ; R.ST = ''
    CALL OPF(FN.ST,F.ST)
    FN.ACR   = 'FBNK.ACCR.ACCT.CR'  ; F.ACR = '' ; R.ACR = ''
    CALL OPF(FN.ACR,F.ACR)

    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    COMP       = ID.COMPANY

*TEXT = COMP; CALL REM
    IF MONTH[1,1] EQ 0 THEN
        MONTH = MONTH[2,1]
    END

*========== CALC DATE MINUS ONE WORKING DAY ==========

    IF MONTH EQ 1 THEN
        NEW.MONTH  = 12
        NEW.YEAR   = YEAR - 1
        LAST.MONTH = 11
        NEW.DAY    = 31
    END
    IF MONTH EQ 5 OR MONTH EQ 7 OR MONTH EQ 10 OR MONTH EQ 12 THEN
        NEW.MONTH  = MONTH - 1
        NEW.YEAR   = YEAR
        NEW.DAY    = 30
        LAST.MONTH = MONTH - 2
    END
    IF MONTH EQ 4 OR MONTH EQ 6 OR MONTH EQ 8 OR MONTH EQ 9 OR MONTH EQ 11 THEN
        NEW.MONTH  = MONTH - 1
        NEW.YEAR   = YEAR
        NEW.DAY    = 31
        LAST.MONTH = MONTH - 2
    END
    IF MONTH EQ 3 THEN
        NEW.MONTH  = MONTH - 1
        NEW.YEAR   = YEAR
        YEAR.FRAC  = NEW.YEAR / 4
        FRAC       = FIELD(YEAR.FRAC,'.',2)
        IF FRAC EQ '' THEN
            NEW.DAY   = 29
        END
        ELSE
            NEW.DAY   = 28
        END
        LAST.MONTH = MONTH - 2
    END
    IF MONTH EQ 2 THEN
        NEW.MONTH  = MONTH - 1
        NEW.YEAR   = YEAR
        LAST.MONTH = 12
        NEW.DAY    = 31
    END

    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
    IF LEN(LAST.MONTH) EQ 1 THEN
        LAST.MONTH = '0':LAST.MONTH
    END

    DATE.B4    = NEW.YEAR:NEW.MONTH
    LAST.DATE  = NEW.YEAR:LAST.MONTH

*============== REPORT CREATION ===========

    REPORT.ID = 'SBM.UNDER.NEW.CUS'
    CALL PRINTER.ON(REPORT.ID,'')

RETURN
*=====================================================
PROCESS:
    TEXT = COMP  ; CALL REM
    Y.SEL  = "SELECT F.SCB.STMT.ACCT.SAVE WITH PAY.FLAG EQ ''"
    Y.SEL := " AND @ID LIKE ...-":DATE.B4:" AND CO.CODE EQ ":COMP

    CALL EB.READLIST(Y.SEL, KEY.LIST, "", SELECTED, ASD1)
    IF  SELECTED NE 0 THEN
        FOR I = 1 TO SELECTED
            CUS.NO = ''; CUS.NAME = ''; ACCT.NO = ''; NEW.BAL = ''
            SAVE.ID   = KEY.LIST<I>
            ACCT.NO   = SAVE.ID[1,16]
***********NESSMA*******************************************
            CALL F.READ( FN.AC,ACCT.NO, R.AC, F.AC, ERR.AC)
            VAR.N = R.AC<AC.LOCAL.REF><1,ACLR.INT.NO.MONTHS>
            IF NOT (ERR.AC) AND VAR.N EQ 1 THEN
************************************************************
                CALL F.READ( FN.SA,SAVE.ID, R.SA, F.SA, STMT.ERR)
                NEW.BAL = R.SA<STMT.SAVE.CR.VAL.BALANCE>
                CALL F.READ(FN.AC,ACCT.NO, R.AC, F.AC, AC.ERR)
                CUS.NO    = R.AC<AC.CUSTOMER>
                CALL F.READ(FN.CU,CUS.NO, R.CU, F.CU, CU.ERR)
                CUS.NAME  = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                GOSUB WRITE
            END
        NEXT I
    END
RETURN
*================ WRITE HEADER  =======================
PRINT.HEAD:
*Line [ 176 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� ������ ����� ������� �����-������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(40):"��� ������":SPACE(10):"������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

RETURN
*================== WRITE DATA =========================
WRITE:

** XX3 = SPACE(132)
    XX3 = ''
    XX3<1,1>[1,20]    = CUS.NO
    XX3<1,1>[20,50]   = CUS.NAME
    XX3<1,1>[70,20]   = ACCT.NO
    XX3<1,1>[90,10]  = NEW.BAL


    PRINT XX3<1,1>

RETURN
END
