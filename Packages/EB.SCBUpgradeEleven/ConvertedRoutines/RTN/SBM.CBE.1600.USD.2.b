* @ValidationCode : MjotMTM0MTk3OTEyMzpDcDEyNTI6MTY0MDc2ODY2Mzc4MDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 11:04:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBM.CBE.1600.USD.2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB PRINT.HEAD
    GOSUB OPEN.FILES

    GOSUB SEL.1
    GOSUB SEL.2
    GOSUB SEL.3

RETURN
*==============================================================
PRINT.HEAD:
*==========
    OPENSEQ "&SAVEDLISTS&" , "CBE.1600.USD.2.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CBE.1600.USD.2.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CBE.1600.USD.2.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CBE.1600.USD.2.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create CBE.1600.USD.2.CSV File IN &SAVEDLISTS&'
        END
    END

    TXT.LINE1  = '����� ����� ������ ������'
    TXT.LINE2  = '������ ���������� ������� �������'
    TXT.LINE3  = '�������'
    TXT.LINE4  = '������'

    WS.LINE.1  = '���� �������'
    WS.LINE.2  = '���� �������'
    WS.LINE.3  = '���� �������'
    WS.LINE.4  = '���� �������'
    WS.LINE.5  = '������ ����'


    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####-##-##")

    HEAD1 = "����� ������ ����������� ���� ������ ������� ������� ������ ���������"
    HEAD.DESC = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "����":","
    HEAD.DESC := "�������� ��������":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "������� ����������":","
    HEAD.DESC := "����� ���� ����� ��������":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*==============================================================
OPEN.FILES:
*==========
    FN.CBE = 'F.SCB.CUS.POS' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    CALL F.READ(FN.CCY,'USD',R.CCY,F.CCY,E1)
    WS.RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>

    WS.DAT       = DAT

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    CATEG.LINE1  = '(3201)'


    CATEG.LINE2  = '(1001 1002 1003 1007 1008 1011 1059 1102 1201 1202 1203 1205 1206 1207 1208 1211 1212 1214 1215 1216'
    CATEG.LINE2 := ' 1217 1218 1220 1221 1222 1223 1224 1225 1227 1230 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405'
    CATEG.LINE2 := ' 1406 1407 1408 1413 1414 1415 1416 1417 1418 1419 1420 1421 1445 1455 1477 1478 1479 1480 1481 1483'
    CATEG.LINE2 := ' 1484 1485 1486 1487 1488 1493 1499 1501 1502 1503 1504 1507 1508 1509 1510 1511 1512 1513 1514 1516'
    CATEG.LINE2 := ' 1518 1519 1523 1524 1525 1526 1534 1544 1558 1559 1560 1566 1577 1579 1582 1588 1591 1595 1596 1597'
    CATEG.LINE2 := ' 1598 1599 3005 3010 3011 3012 3013 3014 3017 6501 6502 6503 6504 6511 11232 11239 11240 11242 21050'
    CATEG.LINE2 := ' 21051 21052 21053 21054 21055 21056 21057 21060 21062 21063 21066)'


    CATEG.LINE3  = '(1001 1002 1003 1005 1007 1008 1011 1012 1013 1014 1015 1016 1019 1059 1102 1201 1202 1205 1206 1207'
    CATEG.LINE3 := ' 1208 1211 1212 1214 1215 1216 1217 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1407 1408'
    CATEG.LINE3 := ' 1413 1414 1415 1416 1417 1418 1419 1420 1421 1445 1455 1477 1480 1481 1483 1484 1485 1493 1499 1501'
    CATEG.LINE3 := ' 1502 1503 1504 1507 1508 1509 1510 1511 1512 1513 1514 1516 1518 1519 1523 1524 1525 1526 1534 1544'
    CATEG.LINE3 := ' 1558 1559 1560 1566 1577 1579 1582 1588 1595 1596 1597 1599 2006 3005 3010 3011 3012 3013 3014 3017'
    CATEG.LINE3 := ' 3050 3060 3065 6501 6502 6503 6504 6505 6506 6507 6508 6511 6512 6513 6514 6515 6516 6517 11232 11239'
    CATEG.LINE3 := ' 11240 11242 16170 21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21013 21017 21018'
    CATEG.LINE3 := ' 21019 21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032 21041 21042)'

    WS.AMT.USD.1 = 0 ; WS.AMT.EUR.1 = 0
    WS.AMT.GBP.1 = 0 ; WS.AMT.CCY.1 = 0

    WS.AMT.USD.2 = 0 ; WS.AMT.EUR.2 = 0
    WS.AMT.GBP.2 = 0 ; WS.AMT.CCY.2 = 0

    WS.AMT.USD.3 = 0 ; WS.AMT.EUR.3 = 0
    WS.AMT.GBP.3 = 0 ; WS.AMT.CCY.3 = 0

    WS.AMT.USD.4 = 0 ; WS.AMT.EUR.4 = 0
    WS.AMT.GBP.4 = 0 ; WS.AMT.CCY.4 = 0

    WS.AMT.USD.5 = 0 ; WS.AMT.EUR.5 = 0
    WS.AMT.GBP.5 = 0 ; WS.AMT.CCY.5 = 0

    WS.AMT.USD.TOT = 0 ; WS.AMT.EUR.TOT = 0
    WS.AMT.GBP.TOT = 0 ; WS.AMT.CCY.TOT = 0

RETURN
*==============================================================
CLEAR.DETAIL:
*============

    WS.AMT.USD.1 = 0 ; WS.AMT.EUR.1 = 0
    WS.AMT.GBP.1 = 0 ; WS.AMT.CCY.1 = 0

    WS.AMT.USD.2 = 0 ; WS.AMT.EUR.2 = 0
    WS.AMT.GBP.2 = 0 ; WS.AMT.CCY.2 = 0

    WS.AMT.USD.3 = 0 ; WS.AMT.EUR.3 = 0
    WS.AMT.GBP.3 = 0 ; WS.AMT.CCY.3 = 0

    WS.AMT.USD.4 = 0 ; WS.AMT.EUR.4 = 0
    WS.AMT.GBP.4 = 0 ; WS.AMT.CCY.4 = 0

    WS.AMT.USD.5 = 0 ; WS.AMT.EUR.5 = 0
    WS.AMT.GBP.5 = 0 ; WS.AMT.CCY.5 = 0

    WS.AMT.USD.TOT = 0 ; WS.AMT.EUR.TOT = 0
    WS.AMT.GBP.TOT = 0 ; WS.AMT.CCY.TOT = 0

RETURN
*==============================================================
SEL.1:
*=====
    LINE.DESC = TXT.LINE1

    BB.DATA  = LINE.DESC:","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    T.SEL  = "SELECT ":FN.CBE:" WITH SYS.DATE EQ ":WS.DAT:" AND DEAL.CCY NE 'EGP' AND CATEGORY IN ":CATEG.LINE1:" AND DEAL.AMOUNT LT 0"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I
    END

    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

RETURN
*-------------------------------------------------------
SEL.2:
*=====
    LINE.DESC = TXT.LINE2

    BB.DATA  = LINE.DESC:","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    T.SEL  = "SELECT ":FN.CBE:" WITH SYS.DATE EQ ":WS.DAT:" AND DEAL.CCY NE 'EGP' AND CATEGORY IN ":CATEG.LINE2:" AND DEAL.AMOUNT LT 0"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I
    END

    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

RETURN
*-------------------------------------------------------
SEL.3:
*=====
    LINE.DESC = TXT.LINE3

    BB.DATA  = LINE.DESC:","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    T.SEL  = "SELECT ":FN.CBE:" WITH SYS.DATE EQ ":WS.DAT:" AND DEAL.CCY NE 'EGP' AND CATEGORY IN ":CATEG.LINE3:" AND DEAL.AMOUNT GT 0"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I
    END

    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

RETURN
*-------------------------------------------------------
RETURN
*==============================================================
GET.DETAIL:
*==========
    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
    WS.CUS.ID = R.CBE<CUPOS.CUSTOMER>

    CALL F.READ(FN.CU,WS.CUS.ID,R.CU,F.CU,E2)
    WS.SECTOR   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

    WS.CATEG    = R.CBE<CUPOS.CATEGORY>
    WS.DATE     = R.CBE<CUPOS.SYS.DATE>
    WS.CCY      = R.CBE<CUPOS.DEAL.CCY>
    WS.AMT.FCY  = R.CBE<CUPOS.DEAL.AMOUNT>
    WS.AMT.LCY  = R.CBE<CUPOS.LCY.AMOUNT>
*-------------------------------------------------------
**** ���� ������� ****
    IF WS.SECTOR EQ '1260' OR WS.SECTOR EQ '1250' OR WS.SECTOR EQ '1240' OR WS.SECTOR EQ '1230' OR WS.SECTOR EQ '1220' OR WS.SECTOR EQ '1210' OR WS.SECTOR EQ '1130' OR WS.SECTOR EQ '1120' OR WS.SECTOR EQ '1110' THEN
        IF WS.CCY EQ 'USD' THEN
            WS.AMT.USD.1 += R.CBE<CUPOS.DEAL.AMOUNT>
            WS.AMT.USD.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
        END ELSE
            IF WS.CCY EQ 'EUR' THEN
                WS.AMT.EUR.1 += R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.EUR.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
            END ELSE
                IF WS.CCY EQ 'GBP' THEN
                    WS.AMT.GBP.1 += R.CBE<CUPOS.DEAL.AMOUNT>
                    WS.AMT.GBP.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
                END ELSE
                    WS.AMT.CCY.1 += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                    WS.AMT.CCY.TOT += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                END
            END
        END
    END

*-------------------------------------------------------
**** ���� ������� ****
    IF WS.SECTOR EQ '2260' OR WS.SECTOR EQ '2250' OR WS.SECTOR EQ '2240' OR WS.SECTOR EQ '2230' OR WS.SECTOR EQ '2220' OR WS.SECTOR EQ '2210' OR WS.SECTOR EQ '2130' OR WS.SECTOR EQ '2120' OR WS.SECTOR EQ '2110' THEN
        IF WS.CCY EQ 'USD' THEN
            WS.AMT.USD.2 += R.CBE<CUPOS.DEAL.AMOUNT>
            WS.AMT.USD.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
        END ELSE
            IF WS.CCY EQ 'EUR' THEN
                WS.AMT.EUR.2 += R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.EUR.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
            END ELSE
                IF WS.CCY EQ 'GBP' THEN
                    WS.AMT.GBP.2 += R.CBE<CUPOS.DEAL.AMOUNT>
                    WS.AMT.GBP.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
                END ELSE
                    WS.AMT.CCY.2 += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                    WS.AMT.CCY.TOT += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                END
            END
        END
    END

*-------------------------------------------------------
**** ���� ������� ****
    IF WS.SECTOR EQ '3260' OR WS.SECTOR EQ '3250' OR WS.SECTOR EQ '3240' OR WS.SECTOR EQ '3230' OR WS.SECTOR EQ '3220' OR WS.SECTOR EQ '3210' OR WS.SECTOR EQ '3130' OR WS.SECTOR EQ '3120' OR WS.SECTOR EQ '3110' THEN
        IF WS.CCY EQ 'USD' THEN
            WS.AMT.USD.3 += R.CBE<CUPOS.DEAL.AMOUNT>
            WS.AMT.USD.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
        END ELSE
            IF WS.CCY EQ 'EUR' THEN
                WS.AMT.EUR.3 += R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.EUR.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
            END ELSE
                IF WS.CCY EQ 'GBP' THEN
                    WS.AMT.GBP.3 += R.CBE<CUPOS.DEAL.AMOUNT>
                    WS.AMT.GBP.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
                END ELSE
                    WS.AMT.CCY.3 += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                    WS.AMT.CCY.TOT += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                END
            END
        END
    END

*-------------------------------------------------------
**** ���� ������� ****
    IF WS.SECTOR EQ '4260' OR WS.SECTOR EQ '4250' OR WS.SECTOR EQ '4240' OR WS.SECTOR EQ '4230' OR WS.SECTOR EQ '4220' OR WS.SECTOR EQ '4210' OR WS.SECTOR EQ '4130' OR WS.SECTOR EQ '4120' OR WS.SECTOR EQ '4110' THEN
        IF WS.CCY EQ 'USD' THEN
            WS.AMT.USD.4 += R.CBE<CUPOS.DEAL.AMOUNT>
            WS.AMT.USD.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
        END ELSE
            IF WS.CCY EQ 'EUR' THEN
                WS.AMT.EUR.4 += R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.EUR.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
            END ELSE
                IF WS.CCY EQ 'GBP' THEN
                    WS.AMT.GBP.4 += R.CBE<CUPOS.DEAL.AMOUNT>
                    WS.AMT.GBP.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
                END ELSE
                    WS.AMT.CCY.4 += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                    WS.AMT.CCY.TOT += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                END
            END
        END
    END

*-------------------------------------------------------
**** ������ ���� ****

    IF WS.SECTOR EQ '6000' OR WS.SECTOR EQ '7000' OR WS.SECTOR EQ '8001' OR WS.SECTOR EQ '8002' OR WS.SECTOR EQ '8003' OR WS.SECTOR EQ '8004' OR WS.SECTOR EQ '8005' OR WS.SECTOR EQ '8006' OR WS.SECTOR EQ '8007' OR WS.SECTOR EQ '8008' OR WS.SECTOR EQ '8011' OR WS.SECTOR EQ '4750' OR WS.SECTOR EQ '4700' OR WS.SECTOR EQ '4650' OR WS.SECTOR EQ '4500' OR WS.SECTOR EQ '4550' OR WS.SECTOR EQ '4600' THEN
        IF WS.CCY EQ 'USD' THEN
            WS.AMT.USD.5 += R.CBE<CUPOS.DEAL.AMOUNT>
            WS.AMT.USD.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
        END ELSE
            IF WS.CCY EQ 'EUR' THEN
                WS.AMT.EUR.5 += R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.EUR.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
            END ELSE
                IF WS.CCY EQ 'GBP' THEN
                    WS.AMT.GBP.5 += R.CBE<CUPOS.DEAL.AMOUNT>
                    WS.AMT.GBP.TOT += R.CBE<CUPOS.DEAL.AMOUNT>
                END ELSE
                    WS.AMT.CCY.5 += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                    WS.AMT.CCY.TOT += R.CBE<CUPOS.LCY.AMOUNT> / WS.RATE
                END
            END
        END
    END

*-------------------------------------------------------
RETURN
*==============================================================
PRINT.DETAIL:
*==========

    WS.AMT.USD.1 = DROUND(WS.AMT.USD.1,'0') ; WS.AMT.EUR.1 = DROUND(WS.AMT.EUR.1,'0')
    WS.AMT.GBP.1 = DROUND(WS.AMT.GBP.1,'0') ; WS.AMT.CCY.1 = DROUND(WS.AMT.CCY.1,'0')

    WS.AMT.USD.2 = DROUND(WS.AMT.USD.2,'0') ; WS.AMT.EUR.2 = DROUND(WS.AMT.EUR.2,'0')
    WS.AMT.GBP.2 = DROUND(WS.AMT.GBP.2,'0') ; WS.AMT.CCY.2 = DROUND(WS.AMT.CCY.2,'0')


    WS.AMT.USD.3 = DROUND(WS.AMT.USD.3,'0') ; WS.AMT.EUR.3 = DROUND(WS.AMT.EUR.3,'0')
    WS.AMT.GBP.3 = DROUND(WS.AMT.GBP.3,'0') ; WS.AMT.CCY.3 = DROUND(WS.AMT.CCY.3,'0')

    WS.AMT.USD.4 = DROUND(WS.AMT.USD.4,'0') ; WS.AMT.EUR.4 = DROUND(WS.AMT.EUR.4,'0')
    WS.AMT.GBP.4 = DROUND(WS.AMT.GBP.4,'0') ; WS.AMT.CCY.4 = DROUND(WS.AMT.CCY.4,'0')

    WS.AMT.USD.5 = DROUND(WS.AMT.USD.5,'0') ; WS.AMT.EUR.5 = DROUND(WS.AMT.EUR.5,'0')
    WS.AMT.GBP.5 = DROUND(WS.AMT.GBP.5,'0') ; WS.AMT.CCY.5 = DROUND(WS.AMT.CCY.5,'0')

    WS.AMT.USD.TOT = DROUND(WS.AMT.USD.TOT,'0') ; WS.AMT.EUR.TOT = DROUND(WS.AMT.EUR.TOT,'0')
    WS.AMT.GBP.TOT = DROUND(WS.AMT.GBP.TOT,'0') ; WS.AMT.CCY.TOT = DROUND(WS.AMT.CCY.TOT,'0')

    BB.DATA  = WS.LINE.1:","
    BB.DATA := WS.AMT.USD.1:","
    BB.DATA := WS.AMT.EUR.1:","
    BB.DATA := WS.AMT.GBP.1:","
    BB.DATA := WS.AMT.CCY.1:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = WS.LINE.2:","
    BB.DATA := WS.AMT.USD.2:","
    BB.DATA := WS.AMT.EUR.2:","
    BB.DATA := WS.AMT.GBP.2:","
    BB.DATA := WS.AMT.CCY.2:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA  = WS.LINE.3:","
    BB.DATA := WS.AMT.USD.3:","
    BB.DATA := WS.AMT.EUR.3:","
    BB.DATA := WS.AMT.GBP.3:","
    BB.DATA := WS.AMT.CCY.3:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = WS.LINE.4:","
    BB.DATA := WS.AMT.USD.4:","
    BB.DATA := WS.AMT.EUR.4:","
    BB.DATA := WS.AMT.GBP.4:","
    BB.DATA := WS.AMT.CCY.4:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = WS.LINE.5:","
    BB.DATA := WS.AMT.USD.5:","
    BB.DATA := WS.AMT.EUR.5:","
    BB.DATA := WS.AMT.GBP.5:","
    BB.DATA := WS.AMT.CCY.5:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA  = TXT.LINE4:' ':LINE.DESC:","
    BB.DATA := WS.AMT.USD.TOT:","
    BB.DATA := WS.AMT.EUR.TOT:","
    BB.DATA := WS.AMT.GBP.TOT:","
    BB.DATA := WS.AMT.CCY.TOT:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*===================================================
