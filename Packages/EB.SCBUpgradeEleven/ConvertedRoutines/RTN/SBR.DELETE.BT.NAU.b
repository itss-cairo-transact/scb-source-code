* @ValidationCode : MjoxMjg1NjYyNTE3OkNwMTI1MjoxNjQwODY0OTUzMDM3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 13:49:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.DELETE.BT.NAU
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_USER.ENV.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SCB.BT.BATCH
    $INSERT I_F.SCCB.BT.PARTIAL
*----------------------------------------------
    FN.TMP = "F.SCCB.BT.PARTIAL" ; F.TMP = ""
    CALL OPF(FN.TMP , F.TMP)
    TMP.ID = ID.COMPANY:".":TODAY

    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
    FLG = R.TMP<SCCB.BT.FLG.AUTH>
    IF FLG NE 'YES' THEN
        FN.BT = "F.SCB.BT.BATCH$NAU" ; F.BT = ""
        CALL OPF(FN.BT , F.BT)
        T.SEL  = "SELECT F.SCB.BT.BATCH$NAU WITH VERSION.NAME EQ ',SCB.BT.BATCH.H'"
        T.SEL := " AND CO.CODE EQ ":ID.COMPANY
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR II = 1 TO SELECTED
                CALL F.READ(FN.BT,KEY.LIST<II>,R.BT,F.BT,READ.ERRBT)
                DELETE F.BT , KEY.LIST<II>
*CALL F.DELETE(FN.BT,KEY.LIST<II>)
            NEXT II
            TEXT = "�� ��� ������ �������" ; CALL REM
        END
        DELETE F.TMP , TMP.ID
*CALL F.DELETE(FN.TMP,TMP.ID)
    END ELSE
        TEXT = "�� ���� ��� ������ ������ ��� ����" ; CALL REM
    END
*----------------------------------------------
RETURN
END
