* @ValidationCode : MjotMTc5OTYwODM2NzpDcDEyNTI6MTY0NDkyNTA2MzczNDpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.CHK.SAL.NOTMOV

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 39 ] Hashing $INCLUDE I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS

*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*-----------------------------INITIALIZATIONS------------------------
INITIATE:

    REPORT.ID = 'SBR.CHK.SAL.NOTMOV'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.ACTY = 'FBNK.ACCT.ACTIVITY' ; F.ACTY = '' ; R.ACTY = ''
    CALL OPF( FN.ACTY,F.ACTY)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    DAT = TODAY
    CALL ADD.MONTHS(DAT,'-1')

    DAT.TO     = FMT(TODAY,"####/##/##")
    DAT.FROM   = FMT(DAT,"####/##/##")

    CUST.NO1 = ''
    FLAG   = 0
    FLAGK  = 0
    AC.AMT = 0
RETURN
*------------------------SELECT ALL CUSTOMERS ----------------------
PROCESS:
    T.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (1407 1408 1413) BY CUSTOMER"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ETEXT)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CUST.NO  = R.AC<AC.CUSTOMER>
            CATEG.NO = R.AC<AC.CATEGORY>

            IF I EQ 1 THEN CUST.NO1 = CUST.NO

            IF CUST.NO NE CUST.NO1 THEN
                IF FLAG EQ 3 THEN
                    CALL F.READ(FN.CUS,CUST.NO1,R.CUS,F.CUS,E2)
                    CUST.NAME  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                    CUS.OLD.NO = R.CUS<EB.CUS.LOCAL.REF><1,CULR.OLD.CUST.ID>
                    CUS.NEW.NO = CUST.NO1
                    GOSUB WRITE
                END
                FLAG    = 0
                AC.AMT  = 0
            END
            DAT.CR.CUST = R.AC<AC.DATE.LAST.CR.CUST>
            DAT.CR.BANK = R.AC<AC.DATE.LAST.CR.BANK>
            AC.AMT     += R.AC<AC.ONLINE.ACTUAL.BAL>

            IF (DAT.CR.CUST LT DAT) AND (DAT.CR.BANK LT DAT) THEN
                FLAG++
            END

            CUST.NO1 = CUST.NO
        NEXT I
        IF I EQ SELECTED THEN
            IF FLAG EQ 3 THEN
                CALL F.READ(FN.CUS,CUST.NO1,R.CUS,F.CUS,E2)
                CUST.NAME  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                CUS.OLD.NO = R.CUS<EB.CUS.LOCAL.REF><1,CULR.OLD.CUST.ID>
                CUS.NEW.NO = CUST.NO1
                GOSUB WRITE
            END
        END
        FLAG    = 0
        AC.AMT  = 0
    END

RETURN
*--------------------------- WRITE -------------------
WRITE:
*-----
    XX3 = SPACE(132)
    XX3 = ''
    PRINT XX3<1,1>
    XX3<1,1>[1,10]   = CUS.OLD.NO
    XX3<1,1>[20,10]  = CUS.NEW.NO
    XX3<1,1>[40,35]  = CUST.NAME
    XX3<1,1>[80,20]  = AC.AMT

    PRINT XX3<1,1>
    XX3<1,1> = ""

RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    COMP = ID.COMPANY
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"������ �������� ���� �� ��� ����� ����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"�� :":SPACE(2):DAT.FROM:SPACE(2):"��� :":DAT.TO
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� ������":SPACE(7):"����� ������":SPACE(15):"��� ������":SPACE(25):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
END
