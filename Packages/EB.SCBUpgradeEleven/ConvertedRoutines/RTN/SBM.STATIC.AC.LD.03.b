* @ValidationCode : Mjo0MjE1MzQzNzQ6Q3AxMjUyOjE2NDA4NTEyNDc3MDE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 10:00:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBM.STATIC.AC.LD.03

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD

*********************** OPENING FILES *****************************
    FN.AC  = "F.CBE.STATIC.AC.LD" ; F.AC   = ""
    CALL OPF (FN.AC,F.AC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*------------------------------------------------------------------
********************** CLEAR & COPY NEW DATA TO OLD DATA ******************

    T.SEL = "SELECT ":FN.AC
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*------------------------------------------------------

            R.AC<ST.CBE.CUR.AC.LE>         = 0
            R.AC<ST.CBE.CUR.AC.LE.DR>      = 0
            R.AC<ST.CBE.DEPOST.AC.LE.1Y>   = 0
            R.AC<ST.CBE.DEPOST.AC.LE.2Y>   = 0
            R.AC<ST.CBE.DEPOST.AC.LE.3Y>   = 0
            R.AC<ST.CBE.DEPOST.AC.LE.MOR3> = 0
            R.AC<ST.CBE.CER.AC.LE.3Y>      = 0
            R.AC<ST.CBE.CER.AC.LE.5Y>      = 0
            R.AC<ST.CBE.CER.AC.LE.GOLD>    = 0
            R.AC<ST.CBE.SAV.AC.LE>         = 0
            R.AC<ST.CBE.SAV.AC.LE.DR>      = 0
            R.AC<ST.CBE.BLOCK.AC.LE>       = 0
            R.AC<ST.CBE.MARG.LC.LE>        = 0
            R.AC<ST.CBE.MARG.LG.LE>        = 0
            R.AC<ST.CBE.FACLTY.LE>         = 0
            R.AC<ST.CBE.FACLTY.LE.CR>      = 0
            R.AC<ST.CBE.LOANS.LE.S>        = 0
            R.AC<ST.CBE.LOANS.LE.S.CR>     = 0
            R.AC<ST.CBE.LOANS.LE.M>        = 0
            R.AC<ST.CBE.LOANS.LE.M.CR>     = 0
            R.AC<ST.CBE.LOANS.LE.L>        = 0
            R.AC<ST.CBE.LOANS.LE.L.CR>     = 0
            R.AC<ST.CBE.COMRCL.PAPER.LE>   = 0
            R.AC<ST.CBE.CUR.AC.EQ>         = 0
            R.AC<ST.CBE.CUR.AC.EQ.DR>      = 0
            R.AC<ST.CBE.DEPOST.AC.EQ.1Y>   = 0
            R.AC<ST.CBE.DEPOST.AC.EQ.2Y>   = 0
            R.AC<ST.CBE.DEPOST.AC.EQ.3Y>   = 0
            R.AC<ST.CBE.DEPOST.AC.EQ.MOR3> = 0
            R.AC<ST.CBE.CER.AC.EQ.3Y>      = 0
            R.AC<ST.CBE.CER.AC.EQ.5Y>      = 0
            R.AC<ST.CBE.CER.AC.EQ.GOLD>    = 0
            R.AC<ST.CBE.SAV.AC.EQ>         = 0
            R.AC<ST.CBE.SAV.AC.EQ.DR>      = 0
            R.AC<ST.CBE.BLOCK.AC.EQ>       = 0
            R.AC<ST.CBE.MARG.LC.EQ>        = 0
            R.AC<ST.CBE.MARG.LG.EQ>        = 0
            R.AC<ST.CBE.FACLTY.EQ>         = 0
            R.AC<ST.CBE.FACLTY.EQ.CR>      = 0
            R.AC<ST.CBE.LOANS.EQ.S>        = 0
            R.AC<ST.CBE.LOANS.EQ.S.CR>     = 0
            R.AC<ST.CBE.LOANS.EQ.M>        = 0
            R.AC<ST.CBE.LOANS.EQ.M.CR>     = 0
            R.AC<ST.CBE.LOANS.EQ.L>        = 0
            R.AC<ST.CBE.LOANS.EQ.L.CR>     = 0
            R.AC<ST.CBE.COMRCL.PAPER.EQ>   = 0

            CALL F.WRITE(FN.AC,KEY.LIST<I>,R.AC)
            CALL  JOURNAL.UPDATE(KEY.LIST<I>)

        NEXT I
    END
END
