* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNTc4Mzg6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** ������� �������� ������� ������ ***
*** CREATED BY KHALED ***
***=================================

    SUBROUTINE SBM.LOAN.DEP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 44 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 45 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LOAN.DEP'
    CALL PRINTER.ON(REPORT.ID,'')

    Y.CLOSE.BAL = 0
    Y.TOT.CLOSE.BAL = 0

    XX20    = SPACE(120)   ;  XX51 = SPACE(120)
    XX30    = SPACE(120)   ;  XX52 = SPACE(120)
    XX31    = SPACE(120)   ;  XX53 = SPACE(120)
    XX50    = SPACE(120)   ;  XX54 = SPACE(120)
    XX60    = SPACE(120)   ;  XX55 = SPACE(120)
    XXTOT1  = SPACE(120)   ;  XX56 = SPACE(120)
    XXTOT2  = SPACE(120)
    COMP = ID.COMPANY
    RETURN
*===============================================================
CALLDB:
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''

    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    TD1 = TODAY
    CALL CDT("",TD1,'-1C')
*************************************************************************
    T.SEL = "SELECT ":FN.LN: " WITH @ID EQ GENLED.0020 OR @ID EQ GENLED.0030 OR @ID EQ GENLED.0031 OR @ID EQ GENLED.0050 OR @ID EQ GENLED.0060 OR @ID EQ GENLED.0220 OR @ID EQ GENLED.0230 OR @ID EQ GENLED.0231 OR @ID EQ GENLED.0250 OR @ID EQ GENLED.0260 OR @ID EQ GENLED.0510 OR @ID EQ GENLED.0520 OR @ID EQ GENLED.0530 OR @ID EQ GENLED.0540 OR @ID EQ GENLED.0550 OR @ID EQ GENLED.0560 OR @ID EQ GENLED.0710 OR @ID EQ GENLED.0720 OR @ID EQ GENLED.0730 OR @ID EQ GENLED.0740 OR @ID EQ GENLED.0750 OR @ID EQ GENLED.0760"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,2,2>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":"LOCAL":"-":TD1:"*":COMP
*************************************************************************
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
            Y.CLOSE.BAL = R.BAL<RE.SLB.CLOSING.BAL.LCL>
*************************************************************************
            Y.TOT.CLOSE.BAL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
            IF DESC.ID EQ "0020" THEN
                XX20<1,1>[1,35] = Y.DESC
                XX20<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0220" THEN
                XX20<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT STR ('�������� ��������� :',1)
                PRINT STR ('=',20)
                PRINT XX20<1,1>
            END
            IF DESC.ID EQ "0030" THEN
                XX30<1,1>[1,35] = Y.DESC
                XX30<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0230" THEN
                XX30<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT XX30<1,1>
            END
            IF DESC.ID EQ "0031" THEN
                XX31<1,1>[1,35] = Y.DESC
                XX31<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0231" THEN
                XX31<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT XX31<1,1>
            END
            IF DESC.ID EQ "0050" THEN
                XX50<1,1>[1,35] = Y.DESC
                XX50<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0250" THEN
                XX50<1,1>[85,15] = Y.CLOSE.BAL
                PRINT XX50<1,1>
            END
            IF DESC.ID EQ "0060" THEN
                XX60<1,1>[1,35] = Y.DESC
                XX60<1,1>[45,15]  = Y.CLOSE.BAL
                XXTOT1<1,1>[1,35]   = "������ ������"
                XXTOT1<1,1>[45,15]  = Y.TOT.CLOSE.BAL
                Y.TOT.CLOSE.BAL = 0
            END
            IF DESC.ID EQ "0260" THEN
                XX60<1,1>[85,15]  = Y.CLOSE.BAL
                XXTOT1<1,1>[85,15]  = Y.TOT.CLOSE.BAL
                PRINT XX60<1,1>
                PRINT STR(' ',120)
                PRINT STR('-',120)
                PRINT XXTOT1<1,1>
                PRINT STR('-',120)
                PRINT STR(' ',120)
                Y.TOT.CLOSE.BAL = 0
            END
*************************************************************************
            IF DESC.ID EQ "0510" THEN
                XX51<1,1>[1,35] = Y.DESC
                XX51<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0710" THEN
                XX51<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT STR ('�������� ���������� : ',1)
                PRINT STR ('=',20)

                PRINT XX51<1,1>
            END
            IF DESC.ID EQ "0520" THEN
                XX52<1,1>[1,35] = Y.DESC
                XX52<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0720" THEN
                XX52<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT XX52<1,1>
            END
            IF DESC.ID EQ "0530" THEN
                XX53<1,1>[1,35] = Y.DESC
                XX53<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0730" THEN
                XX53<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT XX53<1,1>
            END
            IF DESC.ID EQ "0540" THEN
                XX54<1,1>[1,35] = Y.DESC
                XX54<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0740" THEN
                XX54<1,1>[85,15] = Y.CLOSE.BAL
                PRINT XX54<1,1>
            END
            IF DESC.ID EQ "0550" THEN
                XX55<1,1>[1,35] = Y.DESC
                XX55<1,1>[45,15]  = Y.CLOSE.BAL
            END
            IF DESC.ID EQ "0750" THEN
                XX55<1,1>[85,15]  = Y.CLOSE.BAL
                PRINT XX55<1,1>
            END
            IF DESC.ID EQ "0560" THEN
                XX56<1,1>[1,35] = Y.DESC
                XX56<1,1>[45,15]  = Y.CLOSE.BAL
                XXTOT2<1,1>[1,35]   = "������ �������"
                XXTOT2<1,1>[45,15]  = Y.TOT.CLOSE.BAL
                Y.TOT.CLOSE.BAL = 0
            END
            IF DESC.ID EQ "0760" THEN
                XX56<1,1>[85,15]  = Y.CLOSE.BAL
                XXTOT2<1,1>[85,15]  = Y.TOT.CLOSE.BAL
                PRINT XX56<1,1>
                PRINT STR(' ',120)
                PRINT STR('-',120)
                PRINT XXTOT2<1,1>
                PRINT STR('-',120)
                PRINT STR(' ',120)
                Y.TOT.CLOSE.BAL = 0
            END
**************************************************************
        NEXT I
    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
*CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 216 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    TD = TODAY
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������� �������� ������� ������ �� : ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������" :SPACE(35):"������� �������":SPACE(20):"������� ��������"
    PR.HD :="'L'":SPACE(80):"����� �������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
