* @ValidationCode : Mjo4MzA0ODM5NjpDcDEyNTI6MTY0NDkyNTA1ODM2OTpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBM.NEG.LEGS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TRANS.LWORK.DAY
*   $INCLUDE           I_F.SCB.TRANS.TODAY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Adding I_F.COMPANY - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 43 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
* DEBUG
    REPORT.ID='SBM.NEG.LEGS'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.STT = 'F.SCB.TRANS.LWORK.DAY' ; F.STT = ''
    CALL OPF(FN.STT,F.STT)
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
*   KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    Y.AMOUNT  = 0
    Y.NEW.SECTOR=0

    Y.AMOUNT   = 0
    Y.CATEGORY = 0
    Y.CURRENCY = 0

    WS.CASH.LCY      = 0
    WS.DEPOST.LCY    = 0
    WS.CASH.LCY.110      = 0
    WS.DEPOST.LCY.110    = 0
    WS.CASH.FCY.110      = 0
    WS.DEPOST.FCY.110    = 0

    WS.TOT.FCY       = 0
    WS.TOT.LCY       = 0

    WS.SAVE.LCY      = 0
    WS.OTHER.AMT.LCY = 0
    WS.CASH.FCY      = 0
    WS.DEPOST.FCY    = 0
    WS.SAVE.FCY      = 0
    WS.OTHER.AMT.FCY = 0
    XX   =SPACE(132)
    XX1  =SPACE(132)
    XX2  =SPACE(132)
    XX3  =SPACE(132)
    TOTAL=SPACE(132)
    HEAD  =SPACE(132)
    HEAD1  =SPACE(132)
    HEAD2  =SPACE(132)
    DETAIL.LINE=SPACE(132)
    DETAIL.LINE1=SPACE(132)
    COMP = ID.COMPANY
RETURN
*========================================================================
CALLDB:
*    DEBUG

    DAT.ID = "EG0010001"
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,COMP,TD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,COMP,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
*   TD = TODAY
    TM = TD[1,6]:"..."

    T.SEL  = "SELECT ":FN.STT:" WITH BOOKING.DATE LIKE ":TM:" AND AMOUNT.LCY LT 0 AND COMPANY.CO EQ ":COMP
    T.SEL := " AND PRODUCT.CATEGORY IN (1202 1201 1220 1221 1222 1223 1224 1225 1226 1227 9090 2001 11041 "
    T.SEL := " 1001 1002 6501 6502 6503 6504 21001 21002 21003 21004 21005 21006 21007 21008 1015 1016 3005 11282 11283 11284  5101 5102 16188 2000)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    KK = 0
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            Y.NEW.SECTOR=0
            CALL F.READ(FN.STT,KEY.LIST<I>,R.STT,F.STT,E1)

            CUS.ID = R.STT<TRANS.CUSTOMER.ID>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            Y.NEW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>[2,3]

            Y.AMOUNT   =  R.STT<TRANS.AMOUNT.LCY>
            Y.CATEGORY =  R.STT<TRANS.PRODUCT.CATEGORY>
            Y.CURRENCY =  R.STT<TRANS.CURRENCY>

            IF  Y.CURRENCY EQ 'EGP' THEN
                IF  Y.NEW.SECTOR  EQ 110   THEN
                    IF  Y.CATEGORY EQ 1001 OR Y.CATEGORY EQ 1002 THEN
                        WS.CASH.LCY.110 = WS.CASH.LCY.110 + Y.AMOUNT
                        WS.TOT.LCY = WS.TOT.LCY + Y.AMOUNT
                        Y.AMOUNT = 0
                    END

                    IF  Y.CATEGORY GE 21001 AND Y.CATEGORY LE 21008 THEN
                        WS.DEPOST.LCY.110 = WS.DEPOST.LCY.110 + Y.AMOUNT
                        WS.TOT.LCY = WS.TOT.LCY + Y.AMOUNT
                        Y.AMOUNT = 0
                    END
                END ELSE
                    IF  Y.CATEGORY EQ 1001 OR Y.CATEGORY EQ 1002 THEN
                        WS.CASH.LCY = WS.CASH.LCY + Y.AMOUNT
                        WS.TOT.LCY = WS.TOT.LCY + Y.AMOUNT
                        Y.AMOUNT = 0
                    END

                    IF  Y.CATEGORY GE 21001 AND Y.CATEGORY LE 21008 THEN
                        WS.DEPOST.LCY = WS.DEPOST.LCY + Y.AMOUNT
                        WS.TOT.LCY = WS.TOT.LCY + Y.AMOUNT
                        Y.AMOUNT = 0
                    END


                    IF  Y.CATEGORY GE 6501 AND Y.CATEGORY LE 6504 THEN
                        WS.SAVE.LCY = WS.SAVE.LCY + Y.AMOUNT
                        WS.TOT.LCY = WS.TOT.LCY + Y.AMOUNT
                        Y.AMOUNT = 0
                    END

                    WS.OTHER.AMT.LCY = WS.OTHER.AMT.LCY + Y.AMOUNT
                    WS.TOT.LCY = WS.TOT.LCY + Y.AMOUNT
                    Y.AMOUNT = 0
                END
*  WS.TOT.LCY       = WS.TOT.LCY +Y.AMOUNT
            END
*-------------------
            IF  Y.NEW.SECTOR  EQ 110   THEN
                IF  Y.CATEGORY EQ 1001 OR Y.CATEGORY EQ 1002 THEN
                    WS.CASH.FCY.110 = WS.CASH.FCY.110 + Y.AMOUNT
                    WS.TOT.FCY = WS.TOT.FCY + Y.AMOUNT
                    Y.AMOUNT = 0
                END

                IF  Y.CATEGORY GE 21001 AND Y.CATEGORY LE 21008 THEN
                    WS.DEPOST.FCY.110 = WS.DEPOST.FCY.110 + Y.AMOUNT
                    WS.TOT.FCY = WS.TOT.FCY + Y.AMOUNT
                    Y.AMOUNT = 0

                END
            END ELSE

                IF  Y.CATEGORY EQ 1001 OR Y.CATEGORY EQ 1002 THEN
                    WS.CASH.FCY = WS.CASH.FCY + Y.AMOUNT
                    WS.TOT.FCY = WS.TOT.FCY + Y.AMOUNT
                    Y.AMOUNT = 0
                END

                IF  Y.CATEGORY GE 21001 AND Y.CATEGORY LE 21008 THEN
                    WS.DEPOST.FCY = WS.DEPOST.FCY + Y.AMOUNT
                    WS.TOT.FCY = WS.TOT.FCY + Y.AMOUNT
                    Y.AMOUNT = 0
                END

                IF  Y.CATEGORY GE 6501 AND Y.CATEGORY LE 6504 THEN
                    WS.SAVE.FCY = WS.SAVE.FCY + Y.AMOUNT
                    WS.TOT.FCY = WS.TOT.FCY + Y.AMOUNT
                    Y.AMOUNT = 0
                END
                WS.OTHER.AMT.FCY = WS.OTHER.AMT.FCY + Y.AMOUNT
                WS.TOT.FCY = WS.TOT.FCY + Y.AMOUNT

            END
*      WS.TOT.FCY       = WS.TOT.FCY + Y.AMOUNT

        NEXT I
        GOSUB PRINT.DATA
    END
RETURN
*--------------------------------------------------------------------
PRINT.DATA:
    HEAD<1,1>[1,50]  ="������ ������ �� �������� �������"
    HEAD1<1,1>[1,50] ="������� ������"

    DETAIL.LINE<1,1>[1,50] = "������ �����"
    DETAIL.LINE<1,1>[52,20]= WS.CASH.LCY.110
    DETAIL.LINE<1,1>[75,20]= WS.CASH.FCY.110

    DETAIL.LINE1<1,1>[1,50] = "����� ����"
    DETAIL.LINE1<1,1>[52,20]= WS.DEPOST.LCY.110
    DETAIL.LINE1<1,1>[75,20]= WS.DEPOST.FCY.110

    HEAD2<1,1>[1,50]= "����������"

    XX<1,1>[1,50] = "������ �����"
    XX<1,1>[52,20]= WS.CASH.LCY
    XX<1,1>[75,20]= WS.CASH.FCY

    XX1<1,1>[1,50]= "����� ���� "
    XX1<1,1>[52,20]= WS.DEPOST.LCY
    XX1<1,1>[75,20]= WS.DEPOST.FCY

    XX2<1,1>[1,50]= "������ �������"
    XX2<1,1>[52,20]= WS.SAVE.LCY
    XX2<1,1>[75,20]= WS.SAVE.FCY

    XX3<1,1>[1,50] = "������ ����"
    XX3<1,1>[52,20]= WS.OTHER.AMT.LCY
    XX3<1,1>[75,20]= WS.OTHER.AMT.FCY

    TOTAL<1,1>[1,50] ="����������"
    TOTAL<1,1>[52,20]= WS.TOT.LCY
    TOTAL<1,1>[75,20]= WS.TOT.FCY

    PRINT HEAD
    PRINT STR('-',38)
    PRINT HEAD1
    PRINT STR('-',15)
    PRINT DETAIL.LINE
    PRINT DETAIL.LINE1
    PRINT STR('-',130)
    PRINT HEAD2
    PRINT STR('-',15)
    PRINT XX
    PRINT XX1
    PRINT XX2
    PRINT XX3
    PRINT STR('=',132)
    PRINT TOTAL
    PRINT STR('=',132)
RETURN
*===============================================================
PRINT.HEAD:
*---------
*  CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)

*Line [ 272 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    DAT.ID1 = "EG0010001"
*Line [ 284 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,COMP,TD1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,COMP,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD1=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    T.DAY1  = TD1[7,2]:'/':TD1[5,2]:"/":TD1[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������ ������� �� ������ ������� :":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"         " :SPACE(15):"           ":SPACE(15):"�������":SPACE(20):"����� ����"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
