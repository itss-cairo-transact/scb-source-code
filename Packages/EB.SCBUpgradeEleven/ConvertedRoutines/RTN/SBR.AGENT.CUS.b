* @ValidationCode : MjotMTU3NjI2MzMyOkNwMTI1MjoxNjQ0OTI1MDYyMjY0OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.AGENT.CUS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RELATION.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 39 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 40 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBR.AGENT.CUS'
******************************************************
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
RETURN
*===============================================================
CALLDB:
    FN.RCU = 'FBNK.RELATION.CUSTOMER' ; F.RCU = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.RCU,F.RCU)
    CALL OPF(FN.CU,F.CU)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.RCU:" WITH IS.RELATION IN (22 29 71 81 91 93) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    X  = 0
    KK = 0
    IF SELECTED  THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.RCU,KEY.LIST<I>,R.RCU,F.RCU,E1)
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)

            CU.COMP = R.CU<EB.CUS.COMPANY.BOOK>
            
            IF CU.COMP EQ COMP THEN
                KK++
*******************************************************************
                K2=0
                REL.COD = R.RCU<EB.RCU.IS.RELATION>
*Line [ 77 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                K1 = DCOUNT(REL.COD,@VM)
                FOR J = 1 TO K1
                    RC.COUNT = R.RCU<EB.RCU.IS.RELATION,J>
                    IF RC.COUNT = 11 OR RC.COUNT = 17 OR RC.COUNT = 18 OR RC.COUNT = 19 OR RC.COUNT = 22 OR RC.COUNT = 29 OR RC.COUNT = 71 OR RC.COUNT = 81 OR RC.COUNT = 91 OR RC.COUNT = 93 THEN
                        K2 ++
                        REL.ID = KEY.LIST<I>
                        CUS.ID = R.RCU<EB.RCU.OF.CUSTOMER,J>
                        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
*******************************************************************
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,REL.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,REL.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                        CUST.NAME1  = LOCAL.REF<1,CULR.ARABIC.NAME>
                        TD = LOCAL.REF<1,CULR.ID.EXPIRY.DATE>
                        ID.EXP.DATE = FMT(TD,"####/##/##")
                        LOCAL.REF = ''
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                        CUST.NAME.GUR1 = LOCAL.REF<1,CULR.ARABIC.NAME>

                        XX  = SPACE(132)
                        XX1 = SPACE(132)

                        XX<1,K2>[1,8]     = REL.ID
                        XX<1,K2>[18,35]   = CUST.NAME1
                        XX<1,K2>[55,10]   = ID.EXP.DATE
                        XX<1,K2>[73,10]   = CUS.ID
                        XX<1,K2>[91,35]   = CUST.NAME.GUR1

                        IF K2 GT 1 THEN
                            XX<1,K2>[1,53]   = STR(' ',53)
                        END

                        PRINT XX<1,K2>
                    END
                NEXT J
                PRINT STR('-',120)
                X ++
            END
        NEXT I

        PRINT STR(' ',120)
        XX1<1,1>[1,15]  = "������� : "
        XX1<1,1>[20,15] = KK

        PRINT STR(' ',120)
        XX1<1,1>[55,30] = "***** ����� �������  *****"
        PRINT XX1<1,1>
    END
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ���� ����� ������� �������� ������� ������ �������� "
    PR.HD :="'L'":SPACE(55):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(20):"����� ��������":SPACE(8):"��� ������" :SPACE(10):"��� ������"

    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
