* @ValidationCode : MjotMjA5MjM0OTgyMzpDcDEyNTI6MTY0NDkzMjMxMDkxMDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:38:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>2515</Rating>
*-----------------------------------------------------------------------------
*** ���� ������� ���� �������� ***
*** CREATED BY KHALED ***
***=================================

SUBROUTINE SBM.FOR.DEP

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    COMP.BR = COMP[2]
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 52 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.FOR.DEP'
    CALL PRINTER.ON(REPORT.ID,'')
    XX   = SPACE(120) ; XX1  = SPACE(120) ; XX2  = SPACE(120) ; XX3  = SPACE(120)
    XX4  = SPACE(120) ; XX5  = SPACE(120) ; XX6  = SPACE(120) ; XX7  = SPACE(120)
    XX8  = SPACE(120) ; XX9  = SPACE(120) ; XX10 = SPACE(120) ; XX11 = SPACE(120)
    XX12 = SPACE(120) ; XX13 = SPACE(120) ; XX14  = SPACE(120) ; XX15  = SPACE(120)
    XX16 = SPACE(120) ; XX17 = SPACE(120) ; XX18  = SPACE(120) ; XX19  = SPACE(120)
    XX61 = SPACE(120) ; XX62 = SPACE(120) ; XX131 = SPACE(120) ; XX132 = SPACE(120)
    AMOUNT.USD.IN = 0 ; AMOUNT.USD.OUT = 0 ; TOTAL.USDEQV = 0
    AMOUNT.EUR.IN = 0 ; AMOUNT.EUR.OUT = 0 ; AMOUNT.EUR.EQVEGP = 0 ; AMOUNT.EUR.EQVUSD     = 0 ; AMOUNT.EUR.OUT.EQVEGP = 0 ; AMOUNT.EUR.OUT.EQVUSD = 0
    AMOUNT.EGP.IN = 0 ; AMOUNT.EGP.OUT = 0 ; AMOUNT.EGP.EQVUSD = 0 ; AMOUNT.EGP.OUT.EQVUSD = 0
    AMOUNT.GBP.IN = 0 ; AMOUNT.GBP.OUT = 0 ; AMOUNT.GBP.EQVEGP = 0 ; AMOUNT.GBP.EQVUSD     = 0 ; AMOUNT.GBP.OUT.EQVEGP = 0 ; AMOUNT.GBP.OUT.EQVUSD = 0
    AMOUNT.SAR.IN = 0 ; AMOUNT.SAR.OUT = 0 ; AMOUNT.SAR.EQVEGP = 0 ; AMOUNT.SAR.EQVUSD     = 0 ; AMOUNT.SAR.OUT.EQVEGP = 0 ; AMOUNT.SAR.OUT.EQVUSD = 0
    AMOUNT.CHF.IN = 0 ; AMOUNT.CHF.OUT = 0 ; AMOUNT.CHF.EQVEGP = 0 ; AMOUNT.CHF.EQVUSD     = 0 ; AMOUNT.CHF.OUT.EQVEGP = 0 ; AMOUNT.CHF.OUT.EQVUSD = 0

RETURN
*========================================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*************************************************************************
    T.SEL1 = "SELECT ":FN.CBE: " WITH CBEM.CATEG IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21017 21018 21019 21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032 21041 21042 6501 6502 6503 6504 6511 6512) AND CBEM.BR EQ ":COMP.BR
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR J = 1 TO SELECTED1
            CALL F.READ(FN.CBE,KEY.LIST1<J>,R.CBE,F.CBE,E2)
            CUS.ID = R.CBE<C.CBEM.CUSTOMER.CODE>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NAT = R.CU<EB.CUS.NATIONALITY>
            RES = R.CU<EB.CUS.RESIDENCE>
            POST = R.CU<EB.CUS.POSTING.RESTRICT>
            IF NAT NE 'EG' AND RES EQ 'EG' AND POST LE 80 THEN
                CUR = R.CBE<C.CBEM.CY>
                IF CUR EQ 'USD' THEN
                    AMOUNT.USD.IN += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.USD.IN  = DROUND(AMOUNT.USD.IN,'0')
                END

                IF CUR EQ 'EUR' THEN
                    AMOUNT.EUR.IN += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.EUR.IN  = DROUND(AMOUNT.EUR.IN,'0')
                    AMOUNT.EUR.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.EUR.EQVEGP  = DROUND(AMOUNT.EUR.EQVEGP,'0')
*Line [ 107 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.EUR.EQVUSD  = AMOUNT.EUR.EQVEGP / RATE<1,1>
                    AMOUNT.EUR.EQVUSD  = DROUND(AMOUNT.EUR.EQVUSD,'0')

                END

                IF CUR EQ 'GBP' THEN
                    AMOUNT.GBP.IN += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.GBP.IN  = DROUND(AMOUNT.GBP.IN,'0')
                    AMOUNT.GBP.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.GBP.EQVEGP  = DROUND(AMOUNT.GBP.EQVEGP,'0')
*Line [ 120 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.GBP.EQVUSD  = AMOUNT.GBP.EQVEGP / RATE<1,1>
                    AMOUNT.GBP.EQVUSD  = DROUND(AMOUNT.GBP.EQVUSD,'0')

                END
                IF CUR EQ 'SAR' THEN
                    AMOUNT.SAR.IN += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.SAR.IN  = DROUND(AMOUNT.SAR.IN,'0')
                    AMOUNT.SAR.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.SAR.EQVEGP  = DROUND(AMOUNT.SAR.EQVEGP,'0')
*Line [ 132 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 146 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.SAR.EQVUSD  = AMOUNT.SAR.EQVEGP / RATE<1,1>
                    AMOUNT.SAR.EQVUSD  = DROUND(AMOUNT.SAR.EQVUSD,'0')

                END
                IF CUR EQ 'CHF' THEN
                    AMOUNT.CHF.IN += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.CHF.IN  = DROUND(AMOUNT.CHF.IN,'0')
                    AMOUNT.CHF.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.CHF.EQVEGP  = DROUND(AMOUNT.CHF.EQVEGP,'0')
*Line [ 144 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 164 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.CHF.EQVUSD  = AMOUNT.CHF.EQVEGP / RATE<1,1>
                    AMOUNT.CHF.EQVUSD  = DROUND(AMOUNT.CHF.EQVUSD,'0')

                END

                IF CUR EQ 'EGP' THEN
                    AMOUNT.EGP.IN += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.EGP.IN  = DROUND(AMOUNT.EGP.IN,'0')
*Line [ 155 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.EGP.EQVUSD  = AMOUNT.EGP.IN / RATE<1,1>
                    AMOUNT.EGP.EQVUSD  = DROUND(AMOUNT.EGP.EQVUSD,'0')
                END
            END
*************************************************************************
            IF NAT NE 'EG' AND RES NE 'EG' AND POST LE 80 THEN
                CUR = R.CBE<C.CBEM.CY>
                IF CUR EQ 'USD' THEN
                    AMOUNT.USD.OUT += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.USD.OUT  = DROUND(AMOUNT.USD.OUT,'0')
                END

                IF CUR EQ 'EUR' THEN
                    AMOUNT.EUR.OUT += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.EUR.OUT  = DROUND(AMOUNT.EUR.OUT,'0')
                    AMOUNT.EUR.OUT.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.EUR.OUT.EQVEGP  = DROUND(AMOUNT.EUR.OUT.EQVEGP,'0')
*Line [ 175 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 207 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.EUR.OUT.EQVUSD =  AMOUNT.EUR.OUT.EQVEGP / RATE<1,1>
                    AMOUNT.EUR.OUT.EQVUSD = DROUND(AMOUNT.EUR.OUT.EQVUSD,'0')

                END

                IF CUR EQ 'GBP' THEN
                    AMOUNT.GBP.OUT += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.GBP.OUT  = DROUND(AMOUNT.GBP.OUT,'0')
                    AMOUNT.GBP.OUT.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.GBP.OUT.EQVEGP  = DROUND(AMOUNT.GBP.OUT.EQVEGP,'0')
*Line [ 188 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 226 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.GBP.OUT.EQVUSD = AMOUNT.GBP.OUT.EQVEGP / RATE<1,1>
                    AMOUNT.GBP.OUT.EQVUSD = DROUND(AMOUNT.GBP.OUT.EQVUSD,'0')

                END
                IF CUR EQ 'SAR' THEN
                    AMOUNT.SAR.OUT += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.SAR.OUT  = DROUND(AMOUNT.SAR.OUT,'0')
                    AMOUNT.SAR.OUT.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.SAR.OUT.EQVEGP  = DROUND(AMOUNT.SAR.OUT.EQVEGP,'0')
*Line [ 200 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 244 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.SAR.OUT.EQVUSD = AMOUNT.SAR.OUT.EQVEGP / RATE<1,1>
                    AMOUNT.SAR.OUT.EQVUSD = DROUND(AMOUNT.SAR.OUT.EQVUSD,'0')

                END
                IF CUR EQ 'CHF' THEN
                    AMOUNT.CHF.OUT += R.CBE<C.CBEM.IN.FCY> / 1000
                    AMOUNT.CHF.OUT  = DROUND(AMOUNT.GBP.OUT,'0')
                    AMOUNT.CHF.OUT.EQVEGP += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.CHF.OUT.EQVEGP  = DROUND(AMOUNT.CHF.OUT.EQVEGP,'0')
*Line [ 212 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 262 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.CHF.OUT.EQVUSD = AMOUNT.CHF.OUT.EQVEGP / RATE<1,1>
                    AMOUNT.CHF.OUT.EQVUSD = DROUND(AMOUNT.CHF.OUT.EQVUSD,'0')

                END

                IF CUR EQ 'EGP' THEN
                    AMOUNT.EGP.OUT += R.CBE<C.CBEM.IN.LCY> / 1000
                    AMOUNT.EGP.OUT  = DROUND(AMOUNT.EGP.OUT,'0')
*Line [ 223 ] Update DBR- ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE<1,1>,'USD',RATE)
*Line [ 279 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR (('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE)<1,1>,'USD',RATE)
                    F.ITSS.CURRENCY = 'F.CURRENCY'
                    FN.F.ITSS.CURRENCY = ''
                    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                    CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                    RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE,1>
                    AMOUNT.EGP.OUT.EQVUSD  = AMOUNT.EGP.OUT / RATE<1,1>
                    AMOUNT.EGP.OUT.EQVUSD  = DROUND(AMOUNT.EGP.OUT.EQVUSD,'0')
                END
            END
        NEXT J

        TOTAL.COLUMN.2 = AMOUNT.USD.IN + AMOUNT.USD.OUT + AMOUNT.EUR.EQVUSD + AMOUNT.EUR.OUT.EQVUSD + AMOUNT.GBP.EQVUSD + AMOUNT.GBP.OUT.EQVUSD + AMOUNT.SAR.EQVUSD + AMOUNT.SAR.OUT.EQVUSD + AMOUNT.CHF.EQVUSD + AMOUNT.CHF.OUT.EQVUSD
        TOTAL.COLUMN.3 = AMOUNT.EGP.IN + AMOUNT.EGP.OUT
        TOTAL.COLUMN.4 = AMOUNT.EGP.EQVUSD + AMOUNT.EGP.OUT.EQVUSD
        TOTAL.COLUMN.5 = AMOUNT.USD.IN + AMOUNT.USD.OUT + AMOUNT.EUR.EQVUSD + AMOUNT.EUR.OUT.EQVUSD + AMOUNT.GBP.EQVUSD + AMOUNT.GBP.OUT.EQVUSD + AMOUNT.EGP.EQVUSD + AMOUNT.EGP.OUT.EQVUSD + AMOUNT.SAR.EQVUSD + AMOUNT.SAR.OUT.EQVUSD + AMOUNT.CHF.EQVUSD + AMOUNT.CHF.OUT.EQVUSD

        XX<1,1>[1,15]    = "����� ������ �������"
        XX1<1,1>[1,15]   = "�������� ���� ���"
        XX2<1,1>[1,15]   = "�������� �������"
        XX3<1,1>[1,15]   = "���� ����"
        XX3<1,1>[25,15]  = AMOUNT.EGP.IN
        XX3<1,1>[45,15]  = 0
        XX3<1,1>[65,15]  = AMOUNT.EGP.IN
        XX3<1,1>[85,15]  = AMOUNT.EGP.EQVUSD
        XX3<1,1>[110,15] = AMOUNT.EGP.EQVUSD

        XX4<1,1>[1,15]   = "����� ������"
        XX4<1,1>[25,15]  = AMOUNT.USD.IN
        XX4<1,1>[45,15]  = AMOUNT.USD.IN
        XX4<1,1>[65,15]  = 0
        XX4<1,1>[85,15]  = 0
        XX4<1,1>[110,15] = AMOUNT.USD.IN

        XX5<1,1>[1,15]   = "����"
        XX5<1,1>[25,15]  = AMOUNT.EUR.IN
        XX5<1,1>[45,15]  = AMOUNT.EUR.EQVUSD
        XX5<1,1>[65,15]  = 0
        XX5<1,1>[85,15]  = 0
        XX5<1,1>[110,15] = AMOUNT.EUR.EQVUSD

        XX6<1,1>[1,15]   = "���� ��������"
        XX6<1,1>[25,15]  = AMOUNT.GBP.IN
        XX6<1,1>[45,15]  = AMOUNT.GBP.EQVUSD
        XX6<1,1>[65,15]  = 0
        XX6<1,1>[85,15]  = 0
        XX6<1,1>[110,15] = AMOUNT.GBP.EQVUSD

        XX61<1,1>[1,15]   = "���� �����"
        XX61<1,1>[25,15]  = AMOUNT.SAR.IN
        XX61<1,1>[45,15]  = AMOUNT.SAR.EQVUSD
        XX61<1,1>[65,15]  = 0
        XX61<1,1>[85,15]  = 0
        XX61<1,1>[110,15] = AMOUNT.SAR.EQVUSD

        XX62<1,1>[1,15]   = "���� ������"
        XX62<1,1>[25,15]  = AMOUNT.CHF.IN
        XX62<1,1>[45,15]  = AMOUNT.CHF.EQVUSD
        XX62<1,1>[65,15]  = 0
        XX62<1,1>[85,15]  = 0
        XX62<1,1>[110,15] = AMOUNT.CHF.EQVUSD


        XX7<1,1>[1,15]    = "����� ������ �������"
        XX8<1,1>[1,15]    = "�������� ���� ���"
        XX9<1,1>[1,15]    = "�������� �������"
        XX10<1,1>[1,15]   = "���� ����"
        XX10<1,1>[25,15]  = AMOUNT.EGP.OUT
        XX10<1,1>[45,15]  = 0
        XX10<1,1>[65,15]  = AMOUNT.EGP.OUT
        XX10<1,1>[85,15]  = AMOUNT.EGP.OUT.EQVUSD
        XX10<1,1>[110,15] = AMOUNT.EGP.OUT.EQVUSD

        XX11<1,1>[1,15]   = "����� ������"
        XX11<1,1>[25,15]  = AMOUNT.USD.OUT
        XX11<1,1>[45,15]  = AMOUNT.USD.OUT
        XX11<1,1>[65,15]  = 0
        XX11<1,1>[85,15]  = 0
        XX11<1,1>[110,15] = AMOUNT.USD.OUT

        XX12<1,1>[1,15]   = "����"
        XX12<1,1>[25,15]  = AMOUNT.EUR.OUT
        XX12<1,1>[45,15]  = AMOUNT.EUR.OUT.EQVUSD
        XX12<1,1>[65,15]  = 0
        XX12<1,1>[85,15]  = 0
        XX12<1,1>[110,15] = AMOUNT.EUR.OUT.EQVUSD

        XX13<1,1>[1,15]   = "���� ��������"
        XX13<1,1>[25,15]  = AMOUNT.GBP.OUT
        XX13<1,1>[45,15]  = AMOUNT.GBP.OUT.EQVUSD
        XX13<1,1>[65,15]  = 0
        XX13<1,1>[85,15]  = 0
        XX13<1,1>[110,15] = AMOUNT.GBP.OUT.EQVUSD

        XX131<1,1>[1,15]   = "���� �����"
        XX131<1,1>[25,15]  = AMOUNT.SAR.OUT
        XX131<1,1>[45,15]  = AMOUNT.SAR.OUT.EQVUSD
        XX131<1,1>[65,15]  = 0
        XX131<1,1>[85,15]  = 0
        XX131<1,1>[110,15] = AMOUNT.SAR.OUT.EQVUSD

        XX132<1,1>[1,15]   = "���� ������"
        XX132<1,1>[25,15]  = AMOUNT.CHF.OUT
        XX132<1,1>[45,15]  = AMOUNT.CHF.OUT.EQVUSD
        XX132<1,1>[65,15]  = 0
        XX132<1,1>[85,15]  = 0
        XX132<1,1>[110,15] = AMOUNT.CHF.OUT.EQVUSD


        XX14<1,1>[1,15]   = "�������� ������"
        XX14<1,1>[45,15]  =  TOTAL.COLUMN.2
        XX14<1,1>[65,15]  =  TOTAL.COLUMN.3
        XX14<1,1>[85,15]  =  TOTAL.COLUMN.4
        XX14<1,1>[110,15] =  TOTAL.COLUMN.5



        XX15<1,1>[1,15] = "��� ������� ������ ������ : "
        XX15<1,1>[85,15] = "����� ������� : "


        XX16<1,1>[1,15] = "������ �� :   /  /   "
        XX16<1,1>[85,15] = "���� �����    : "


        XX17<1,1>[1,35] = "* ��� ��� ������ ������ ������� �� ����� �� ��� "


        XX18<1,1>[1,35] = "** ���� ���� ����� ������ �� ������� ���"
        XX19<1,1>[1,15] = "�������� �������"

        PRINT XX<1,1>
        PRINT XX1<1,1>
        PRINT XX2<1,1>

        PRINT STR('-',130)
        PRINT XX3<1,1>
        PRINT STR('-',130)
        PRINT XX4<1,1>
        PRINT STR('-',130)
        PRINT XX5<1,1>
        PRINT STR('-',130)
        PRINT XX6<1,1>
        PRINT STR('-',130)
        PRINT XX61<1,1>
        PRINT STR('-',130)
        PRINT XX62<1,1>
        PRINT STR('-',130)

        PRINT XX19<1,1>
        PRINT STR('*',130)

        PRINT XX7<1,1>
        PRINT XX8<1,1>
        PRINT XX9<1,1>
        PRINT STR('-',130)
        PRINT XX10<1,1>
        PRINT STR('-',130)
        PRINT XX11<1,1>
        PRINT STR('-',130)
        PRINT XX12<1,1>
        PRINT STR('-',130)
        PRINT XX13<1,1>
        PRINT STR('-',130)
        PRINT XX131<1,1>
        PRINT STR('-',130)
        PRINT XX132<1,1>
        PRINT STR('-',130)

        PRINT XX19<1,1>

        PRINT STR('=',130)
        PRINT XX14<1,1>
        PRINT STR('=',130)

        PRINT XX15<1,1>
        PRINT XX16<1,1>
        PRINT STR('-',60)
        PRINT XX17<1,1>
        PRINT XX18<1,1>
    END
*************************************************************************
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 466 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')
    T.DAY1  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(107):REPORT.ID
    PR.HD :="'L'":SPACE(1):"��� / ����� ������� ������"
    PR.HD :="'L'":SPACE(1):"������� ������ ������ ��������"
    PR.HD :="'L'":SPACE(50):"���� ���������� ����� ��������":SPACE(20):"������� �������"
    PR.HD :="'L'":SPACE(50):"���� : ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":"����������":SPACE(10):"���� �������":SPACE(7):"������� ��������":SPACE(7):"���� �������":SPACE(7):"������� ��������":SPACE(7):"������ �������"
    PR.HD :="'L'":SPACE(20):"������ �������":SPACE(7):"��������":SPACE(12):"������� ������":SPACE(6):"�������� �������":SPACE(7):"�������� ��������"
    PR.HD :="'L'":SPACE(82):"������� ������"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
