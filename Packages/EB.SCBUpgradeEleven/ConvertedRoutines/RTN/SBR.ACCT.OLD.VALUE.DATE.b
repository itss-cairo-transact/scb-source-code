* @ValidationCode : MjoyODQ4NjAwNzU6Q3AxMjUyOjE2NDQ5MzAwMzEzNzM6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:00:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-100</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBR.ACCT.OLD.VALUE.DATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 40 ] Hashing $INCLUDE I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
    REPORT.ID = 'SBR.ACCT.OLD.VALUE.DATE'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT= ''
    CALL OPF(FN.STMT,F.STMT)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU= ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC= ''
    CALL OPF(FN.AC,F.AC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    TD  = TODAY[2]
    TDD = TODAY

    APP.DAT = TRIM(TD, "0", "L")
    APP.ID  = 'FBNK.STMT.E':APP.DAT

    COMP = ID.COMPANY
    XX1 = SPACE(132) ; XX2 = SPACE(132) ; XX3 = SPACE(132) ; XX4 = SPACE(132) ; XX5 = SPACE(132)

RETURN
*========================================================================
PROCESS:

    T.SEL = "SELECT ":APP.ID: " WITH (PRODUCT.CATEGORY GE 6511 AND PRODUCT.CATEGORY LE 6513) AND BOOKING.DATE EQ ":TDD:" AND VALUE.DATE LT ":TDD:" AND COMPANY.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.STMT,KEY.LIST<Z>,R.STMT,F.STMT,E1)

            DAT = R.STMT<AC.STE.VALUE.DATE>
            V.DATE  = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]

            AMT = R.STMT<AC.STE.AMOUNT.LCY>
            IF AMT LT 0 THEN
                TRNS.DESC = '���'
            END ELSE
                TRNS.DESC = '�����'
            END

            CATEG.ID = R.STMT<AC.STE.PRODUCT.CATEGORY>
*Line [ 100 ] Update DBR - ITSS - R21 Upgrade - 2021-12-23
*CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
            ACC.ID = R.STMT<AC.STE.ACCOUNT.NUMBER>
            CUS.ID.T = ACC.ID[1,8]
            CUS.ID = TRIM(CUS.ID.T, "0", "L")

*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

            XX1<1,1>[1,10]   = CUS.ID
            XX1<1,1>[15,35]  = CUS.NAME
            XX1<1,1>[45,10]  = V.DATE
            XX1<1,1>[60,15]  = AMT
            XX1<1,1>[75,15]  = CATEG
            XX1<1,1>[95,15] = TRNS.DESC
            PRINT XX1<1,1>
            XX1 = ''
        NEXT Z
    END ELSE
        XX4<1,1>[50,35] = '***  �� ���� ������  ***'
        PRINT XX4<1,1>
    END
*===============================================================
    PRINT STR('=',120)
    XX5<1,1>[50,35] = '***  ����� �������  ***'
    PRINT XX5<1,1>

*===============================================================
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 144 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ����� �������� �������� ����������� �������� ������ �� ����"
    PR.HD :="'L'":SPACE(60):"���� : ":T.DAY
    PR.HD :="'L'":SPACE(48):STR('-',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('-',120)
    PR.HD :="'L'":"��� ������":SPACE(5):"��� ������" :SPACE(20):"����� ����":SPACE(5):"������" :SPACE(10):"��� ������":SPACE(5):"��� ������"
    PR.HD :="'L'":STR('-',120)
    HEADING PR.HD
RETURN
*==============================================================
END
