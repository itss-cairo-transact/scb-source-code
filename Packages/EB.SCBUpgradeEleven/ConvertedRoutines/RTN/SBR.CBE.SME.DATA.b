* @ValidationCode : MjotNzE3NjEzNTc3OkNwMTI1MjoxNjQxNzQyOTQxNjYwOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:42:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
****MAI SAAD 9 FEB 2020***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-------------------------------------------------------------------------
SUBROUTINE SBR.CBE.SME.DATA

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CREDIT.CBE.NEW

* TEXT = 'BEGIN' ; CALL REM
    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ;
    RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CREDIT.CBE = 'F.SCB.CREDIT.CBE.NEW' ; F.CREDIT.CBE = '' ; R.CREDIT.CBE = '' ;
    RETRY = '' ; ERR.CREDIT.CBE = ''
    CALL OPF(FN.CREDIT.CBE,F.CREDIT.CBE)

    YTEXT = "Enter REPORT Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    HEAD.DATE =  COMI
******************* OPEN FILE *******************************
    PATH='/home/signat'
    OPENSEQ PATH , "CBE.SME.DATA.txt" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATH:' ':"CBE.SME.DATA.txt"
        HUSH OFF
    END
    OPENSEQ PATH , "CBE.SME.DATA.txt" TO BB ELSE
        CREATE BB THEN
            TEXT =  'CBE.SME.DATA.txt CREATED ' ; CALL REM;
        END ELSE
            STOP 'Cannot create CBE.SME.DATA.txt File IN HOME/SIGNAT'
        END
    END
*****************************************************************
    BB.DATA = "1700":HEAD.DATE:FMT("0",'R%67')

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    S.SEL  ="SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT LT '70' AND POSTING.RESTRICT NE '12' AND POSTING.RESTRICT NE '13' AND POSTING.RESTRICT NE '16')"
    S.SEL := " AND (NEW.SECTOR GE '1110' AND NEW.SECTOR LE 4600 ) BY COMPANY.BOOK"

    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)
*    TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM

    IF SELECTED.SS THEN
        FOR I = 1 TO SELECTED.SS
            CBE.NO ='' ;SME.COMP.CODE= '' ; NAT_ID='';SALES.VOLUME = '';B.S.DATE = ''
            ACTIVITY.DATE = '';CAPITAL = '' ; ACT.CODE4 = '' ; COMP_TYPE=''
            OWNER_CODE = ''; OWNER_MEM_COUNT=''

            CUS.ID = KEY.LIST.SS<I>
            CALL F.READ(FN.CREDIT.CBE ,CUS.ID, R.CREDIT.CBE, F.CREDIT.CBE ,E31)

            IS.CBE.CU = R.CREDIT.CBE<SCB.C.CBE.GE30.FLAG>
            IF IS.CBE.CU EQ 'Y' THEN
                CALL F.READ(FN.CUSTOMER,KEY.LIST.SS<I>, R.CUSTOMER, F.CUSTOMER ,E3)
                LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
                CBE.NO  =   LOCAL.REF.C<1,CULR.CBE.NO>

                CAP  =  LOCAL.REF.C<1,CULR.PAID.CAPITAL>
                CAP  = CAP/1000
                CAPITAL = FIELD(CAP, ".", 1)

                SAL.VOL = LOCAL.REF.C<1,CULR.TURNOVER>
                SAL.VOL =   SAL.VOL /1000
                SALES.VOL = FIELD(SAL.VOL, ".", 1)

                SME.CATEGORY  =  LOCAL.REF.C<1,CULR.SME.CATEGORY>

                IF SME.CATEGORY NE '' THEN
                    IF SME.CATEGORY EQ 101 OR SME.CATEGORY EQ 201 THEN
                        SME.COMP.CODE = '1'
                    END
                    IF SME.CATEGORY EQ 105 OR SME.CATEGORY EQ 205 THEN
                        SME.COMP.CODE = '3'
                    END
                    IF SME.CATEGORY EQ 106 OR SME.CATEGORY EQ 206 THEN
                        SME.COMP.CODE = '4'
                    END

                END ELSE
                    CREDIT.CODE  =  LOCAL.REF.C<1,CULR.CREDIT.CODE>
                    IF CREDIT.CODE EQ '110' OR CREDIT.CODE EQ '120' THEN
                        SME.COMP.CODE  = '6'
                    END ELSE
************************EDIT IN SEP2019
                        IF CAPITAL EQ '' AND SALES.VOLUME EQ '' THEN
                            SME.COMP.CODE  = '7'
                        END
                    END
                END
                ACT.DATE = R.CUSTOMER<EB.CUS.LEGAL.ISS.DATE>
                IF ACT.DATE THEN
                    ACT.YEAR = ACT.DATE[1,4]
                    ACT.MN = ACT.DATE[5,2]
                    ACT.DAY = ACT.DATE[7,2]
                    ACTIVITY.DATE = ACT.DAY:ACT.MN:ACT.YEAR
                END
                B.S.DT = LOCAL.REF.C<1,CULR.FIN.STAT.DATE>
                IF B.S.DT THEN
                    B.S.YEAR = B.S.DT[1,4]
                    B.S.MN = B.S.DT[5,2]
                    B.S.DAY = B.S.DT[7,2]
                    B.S.DATE = B.S.DAY:B.S.MN:B.S.YEAR
                END
                ACT.CODE4 =  LOCAL.REF.C<1,CULR.CBE.ACTIVITY>
                NEW.SECTOR =  LOCAL.REF.C<1,CULR.NEW.SECTOR>

                IF NEW.SECTOR GE '2110' AND NEW.SECTOR LE '2260' THEN
                    COMP_TYPE = 1
                END ELSE
                    IF NEW.SECTOR GE '1110' AND NEW.SECTOR LE '1260' THEN
                        COMP_TYPE = 3
                    END ELSE
                        COMP_TYPE = 2
                    END
                END

                NAT_ID =LOCAL.REF.C<1,CULR.NSN.NO>

                OWNER_CODE = 3 ; OWNER_MEM_COUNT = 0

                REL_CUSTOMER = R.CUSTOMER<EB.CUS.REL.CUSTOMER>
*Line [ 165 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                REL.COUNT= DCOUNT(REL_CUSTOMER,@VM)
                FOR J = 1 TO REL.COUNT
                    REL.CODE =  R.CUSTOMER<EB.CUS.RELATION.CODE,J>
                    PARTICIPATION.PER = R.CUSTOMER<EB.CUS.ROLE.MORE.INFO,J>
                    ROLE = R.CUSTOMER<EB.CUS.ROLE,J>
                    IF REL.CODE EQ '89' THEN
                        IF PARTICIPATION.PER NE '' THEN
                            IF PARTICIPATION.PER GE '51' THEN
                                OWNER_CODE = 1
                                OWNER_MEM_COUNT += 1
                            END ELSE
                                IF PARTICIPATION.PER LT '51' AND PARTICIPATION.PER GE '20' THEN
                                    IF ROLE NE '' THEN
                                        OWNER_CODE = 2
                                        OWNER_MEM_COUNT += 1
                                    END
                                END ELSE
                                    OWNER_CODE = 3
                                END
                            END
                        END
                    END
                NEXT J

                CBE.NO = FMT(CBE.NO,'R%12')
                SME.COMP.CODE = FMT(SME.COMP.CODE,'R%2')
                CAPITAL = FMT(CAPITAL,'R%12')
                SALES.VOL = FMT(SALES.VOL,'R%12')
                ACTIVITY.DATE = FMT(ACTIVITY.DATE,'R%8')
                B.S.DATE = FMT(B.S.DATE,'R%8')
                ACT.CODE4 = FMT(ACT.CODE4,'R%4')
                COMP_TYPE = FMT(COMP_TYPE,'R%1')
                NAT_ID = FMT(NAT_ID,'R%14')
                OWNER_CODE = FMT(OWNER_CODE,'R%2')
                OWNER_MEM_COUNT = FMT(OWNER_MEM_COUNT,'R%4')

****WRITE TO FILE
                BB.DATA = CBE.NO:SME.COMP.CODE:CAPITAL:SALES.VOL:ACTIVITY.DATE:B.S.DATE:ACT.CODE4:COMP_TYPE:NAT_ID:OWNER_CODE:OWNER_MEM_COUNT
                WRITESEQ BB.DATA TO BB ELSE
                    TEXT = " ERROR WRITE FILE " ; CALL REM;
                END
            END
        NEXT I

    END   ;***End of main selection from CUSTOMER ***
    TEXT = '�� �������� �� �������� ' ;CALL REM
RETURN
END
