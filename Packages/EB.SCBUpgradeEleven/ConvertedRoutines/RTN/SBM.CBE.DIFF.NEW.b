* @ValidationCode : MjotMTYzNjg1MTc1MDpDcDEyNTI6MTY0NDkyNTA0ODc2MzpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
***---BAKRY---20200305---***
*    SUBROUTINE SBM.CBE.DIFF.NEW(LD.DET.ARR)
PROGRAM SBM.CBE.DIFF.NEW
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_COMMON
    $INSERT I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_EQUATE
    $INSERT I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.CUSTOMER
    $INSERT I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.CUSTOMER.ACCOUNT
    $INSERT I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.LMM.ACCOUNT.BALANCES
    $INSERT I_F.LMM.ACCOUNT.BALANCES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.LMM.SCHEDULES.PAST
    $INSERT I_F.LMM.SCHEDULES.PAST
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT I_ENQUIRY.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_LD.ENQ.COMMON
    $INSERT I_LD.ENQ.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_LD.SCH.LIST
    $INSERT I_LD.SCH.LIST
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PD.BALANCES
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    GOSUB INIT
    GOSUB OPENFILES
    GOSUB PROCESS
RETURN

*----
INIT:
*----
    FN.LM = 'F.LMM.ACCOUNT.BALANCES'
    F.LM = ''
    Y.ID=''
    R.LM=''
    LM.ERR1=''
    Y.TOTAL=0
    SEL.CMD=''
    SEL.LIST=''
    RET.CODE=''
    NO.OF.REC = 0
    PD.AMT = 0
    TOTAL.INT.LD = 0
    TOTAL.INT.ALL = 0
    SEP=","
    FN.LP='F.LMM.SCHEDULES.PAST'
    Y.CUSTOMER=''
    TEMP=0
    YF.ENQ = "F.ENQUIRY"
    F.ENQ = ""
    FN.CUSTOMER.ACCOUNT = 'F.CUSTOMER.ACCOUNT'
    FV.CUSTOMER.ACCOUNT = ''
    FV.CUSTOMER.ACCOUNT = ''
    FN.CUS = 'FBNK.CUSTOMER'
    FV.CUS = '' ; R.CUS = '' ; ER.CUS = ''

    FN.BI.DATE = 'FBNK.BASIC.INTEREST.DATE' ; F.BI.DATE = '';R.BI.DATE=''
    CALL OPF(FN.BI.DATE, F.BI.DATE)

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD = ''
    CALL OPF(FN.PD,F.PD)
    TOTAL.PD= ''  ; T.SEL.PD = ""

    FN.PDB = 'FBNK.PD.BALANCES' ; F.PDB = '' ; R.PDB = '' ; ER.PDB = ''
    CALL OPF(FN.PDB,F.PDB)

    BI.DATE.ID = "64EGP"
    CALL F.READ(FN.BI.DATE,BI.DATE.ID,R.BI.DATE,F.BI.DATE,DATE.ERR)

*    IF R.BI.DATE NE '' THEN
*        BI.DATE = R.BI.DATE<EB.BID.EFFECTIVE.DATE,1>
*        BI.DATE.1 = R.BI.DATE<EB.BID.EFFECTIVE.DATE,1>
*        CALL CDT("",BI.DATE,'-1C')
*    END


*============================= Check Quarter Date  ========================================================
*CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
    CHK.DAT = 0
    END.DAT = TODAY[1,6]:"01"
    CALL CDT("",END.DAT,'-1C')

    CHK.DAT = 1

    START.DAT = END.DAT[1,6]:"01"
    MON.NO.DAYS = "C"
    CALL CDD("",START.DAT,END.DAT,MON.NO.DAYS)
    MON.NO.DAYS ++

*==========================================================================================================
*----------------------------------- START OPEN FILE ----------------------------
    OPENSEQ "&SAVEDLISTS&" , "LOAN.8.DIFF.NEW.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"LOAN.8.DIFF.NEW.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "LOAN.8.DIFF.NEW.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LOAN.8.DIFF.NEW.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create LOAN.8.DIFF.NEW.csv File IN &SAVEDLISTS&'
        END
    END
*----------------------------------- END OPEN FILE -----------------------------
*----------------------------------- CREATE HEADER -----------------------------
    HEAD.DESC = "Contract NO.,"
    HEAD.DESC := "Customer,"
    HEAD.DESC := "Customer Name,"
    HEAD.DESC := "Category,"
    HEAD.DESC := "Int. Rate,"
    HEAD.DESC := "Int. Date,"
    HEAD.DESC := "Outstanding,"
*    HEAD.DESC := "Interest,"
    HEAD.DESC := 'Diff. Rate,'
    HEAD.DESC := "Difference,"
*    HEAD.DESC := "No Of Days,"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------ END CREATE HEADER -------------------------------

RETURN


OPENFILES:
*---------
    CALL OPF(FN.LM,F.LM)
    CALL OPF(YF.ENQ,F.ENQ)
    CALL OPF(FN.CUSTOMER.ACCOUNT,FV.CUSTOMER.ACCOUNT)
    CALL OPF(FN.CUS,FV.CUS)
RETURN

*--------------------------------------------------------------------------------
PROCESS:
*-------
    IF CHK.DAT = 1 THEN
        READ R.ENQ FROM F.ENQ,"LD.BALANCES.FULL" ELSE R.ENQ = ""
        SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG EQ 21058 OR LD.CATEG EQ 21059 OR LD.CATEG EQ 21061 BY LD.CATEG BY @ID "
**        SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG EQ 21064 BY LD.CATEG BY @ID "
*        SEL.CMD="SELECT ":FN.LM:" WITH @ID EQ  LD202920090500 BY @ID "
*        SEL.CMD="SELECT ":FN.LM:" WITH @ID EQ  LD202248983300 BY @ID "
*        SEL.CMD="SELECT ":FN.LM:" WITH @ID EQ LD200838000000 BY @ID "
        CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,RET.CODE)
        LOOP
            REMOVE Y.ID FROM SEL.LIST SETTING POS
        WHILE Y.ID:POS
            LMM.ID = Y.ID

            READ R.RECORD FROM F.LM,LMM.ID ELSE R.RECORD = ""
            CALL LD.ENQ.INT.I ;* Open files and store in common

            ID = LMM.ID
            CALL E.LD.SCHED.LIST
            TOTAL.INT = 0

            INT.RATE    = R.RECORD<LD.SL.CURRENT.RATE>
            CUS.ID      = R.RECORD<LD.SL.CUSTOMER.NO>
            Y.CUSTOMER = R.RECORD<LD.SL.CUSTOMER.NO>
            Y.CATEGORY = R.RECORD<LD.SL.CATEGORY>
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,FV.CUS,ER.CUS)
            CUS.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
*-----------------------------------------------------------------------------------------------
            LD.DET.ARR = Y.ID:",":Y.CUSTOMER:",":CUS.NAME:',':Y.CATEGORY:',':INT.RATE

            WRITESEQ LD.DET.ARR TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            LD.DET.ARR = ''
*-----------------------------------------------------------------------------------------------
            FOR II = 1 TO MON.NO.DAYS
                CHECK.DATE = START.DAT[1,6]:FMT(II,"R%2")
                S.INT.DATE = R.RECORD<LD.SL.EVENT.DATE,1>
*-----------------------------------------------------------------------------------------------------------------------------------
*Line [ 208 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                FOR VM1 = 1 TO DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM)
                    DATE.1 = R.RECORD<LD.SL.EVENT.DATE,VM1>
                    DATE.2 = R.RECORD<LD.SL.EVENT.DATE,VM1>
                    DATE.PREV = R.RECORD<LD.SL.EVENT.DATE,VM1-1>
                    DATE.NEXT = R.RECORD<LD.SL.EVENT.DATE,VM1+1>
                    AMT.PREV = R.RECORD<LD.SL.RUNNING.BAL,VM1-1>
                    AMT.NEXT = R.RECORD<LD.SL.RUNNING.BAL,VM1+1>
                    INT.AMOUNT = R.RECORD<LD.SL.INT.AMOUNT,VM1>
                    EVENT.DATE  = R.RECORD<LD.SL.EVENT.DATE,VM1>
                    DAYS = 0
                    Y.AMT = R.RECORD<LD.SL.RUNNING.BAL,VM1>
                    IF  (CHECK.DATE GE DATE.1 AND CHECK.DATE LT DATE.NEXT) THEN
                        Y.AMT = R.RECORD<LD.SL.RUNNING.BAL,VM1>
*Line [ 222 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        VM1 = DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM)
                    END ELSE
                        Y.AMT = AMT.PREV
                    END
                NEXT VM1
                IF CHECK.DATE GE S.INT.DATE THEN
*                    PRINT ID
                    PD.AMT = 0
*PRINT Y.AMT:"=====":PD.AMT
                    GOSUB GETPD
                    IF PD.AMT = '' THEN PD.AMT = 0
                    Y.AMT -= PD.AMT
*PRINT CHECK.DATE:"=====":Y.AMT:"=====":PD.AMT
                    RAT = "64EGP":CHECK.DATE
                    CALL EB.GET.INTEREST.RATE(RAT,SCB.INT.RATE)
                    SCB.DIF.RAT = SCB.INT.RATE + 2 - INT.RATE
                    TOTAL.INT = Y.AMT * SCB.DIF.RAT/36000
                    TOTAL.INT = DROUND(TOTAL.INT,2)
                    TOTAL.INT.LD += TOTAL.INT
                    TOTAL.INT.ALL += TOTAL.INT
*---------------------------------------- CALC INTEREST ----------------------------------------------------------


*                LD.DET.ARR = Y.ID:",":Y.CUSTOMER:",":CUS.NAME:",":CHECK.DATE:",":Y.AMT:",":INT.AMOUNT:",":SCB.DIF.RAT:',':TOTAL.INT
                    LD.DET.ARR = ",,,,,":CHECK.DATE:",":Y.AMT:",":SCB.DIF.RAT:',':TOTAL.INT

                    WRITESEQ LD.DET.ARR TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                    LD.DET.ARR = ''
                END
            NEXT II
*---------------------------------------- PRINT TOTAL ----------------------------------------------------------
            IF TOTAL.INT.LD NE 0 THEN
                LD.DET.ARR = "Total Loan,,,,,,,,":TOTAL.INT.LD

                WRITESEQ LD.DET.ARR TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOTAL.INT.LD = 0
            END
*---------------------------------------- PRINT TOTAL ----------------------------------------------------------
        REPEAT
*---------------------------------------- PRINT TOTAL ----------------------------------------------------------
        IF TOTAL.INT.ALL NE 0 THEN
            LD.DET.ARR = "Total Sheet,,,,,,,,":TOTAL.INT.ALL

            WRITESEQ LD.DET.ARR TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        END
*---------------------------------------- PRINT TOTAL ----------------------------------------------------------

    END
RETURN
*----------------------------------------------------------------------------
GETPD:
*=====
    PD.ID = "PD":ID[1,12]
    CALL F.READ(FN.PD,PD.ID,R.PD,F.PD, PD.ER)
    IF NOT(PD.ER) THEN

        IF R.PD<PD.TOTAL.OVERDUE.AMT> NE '' OR R.PD<PD.TOTAL.OVERDUE.AMT> NE 0 THEN
            CHK.PD.DAT.CNT = DCOUNT(R.PD<PD.PAYMENT.DTE.DUE>,@VM)
            FOR KK = 1 TO CHK.PD.DAT.CNT
*IF CHECK.DATE EQ 20201130 THEN DEBUG
                IF R.PD<PD.PAYMENT.DTE.DUE,KK> LE CHECK.DATE THEN
                    CHK.PD.TYP = R.PD<PD.PAY.TYPE>
                    LOCATE "PR" IN CHK.PD.TYP<1,1,1> SETTING K THEN
                        PD.AMT += R.PD<PD.PAY.AMT.OUTS><1,1,K>
                    END
                    LOCATE "IN" IN CHK.PD.TYP<1,1,1> SETTING H THEN
                        PD.AMT += R.PD<PD.PAY.AMT.OUTS><1,1,H>
                    END
                END
            NEXT KK
        END
    END
RETURN
*----------------------------------------------------------------------------
*GETPDB:
**=====
*        WS.PD.BALANCE.ID = "PD":ID[1,12]:"-":CHECK.DATE
*        CALL F.READ(FN.PDB,WS.PD.BALANCE.ID,R.PDB,F.PDB,ER.PDB)
*        IF NOT(ER.PDB) THEN
*
**        IF R.PD<PD.TOTAL.OVERDUE.AMT> NE '' OR R.PD<PD.TOTAL.OVERDUE.AMT> NE 0 THEN
**            TOTAL.PD = R.PD<PD.TOTAL.OVERDUE.AMT>
**        END
*        END
*        RETURN
*----------------------------------------------------------------------------

END
