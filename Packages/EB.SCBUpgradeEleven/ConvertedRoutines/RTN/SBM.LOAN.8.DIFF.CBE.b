* @ValidationCode : MjoyMTIxNTE5MTQxOkNwMTI1MjoxNjQ0OTI1MDU2OTk5OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
***---BAKRY---20200305---***
*    SUBROUTINE SBM.LOAN.8.DIFF.CBE(LD.DET.ARR)
PROGRAM SBM.LOAN.8.DIFF.CBE
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CUSTOMER.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.LMM.ACCOUNT.BALANCES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.LMM.SCHEDULES.PAST
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_LD.ENQ.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_LD.SCH.LIST
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    GOSUB INIT
    GOSUB OPENFILES
    GOSUB PROCESS
RETURN

*----
INIT:
*----
    FN.LM = 'F.LMM.ACCOUNT.BALANCES'
    F.LM = ''
    Y.ID=''
    R.LM=''
    LM.ERR1=''
    Y.TOTAL=0
    SEL.CMD=''
    SEL.LIST=''
    RET.CODE=''
    NO.OF.REC = 0
    SEP=","
    FN.LP='F.LMM.SCHEDULES.PAST'
    Y.CUSTOMER=''
    TEMP=0
    YF.ENQ = "F.ENQUIRY"
    F.ENQ = ""
    FN.CUSTOMER.ACCOUNT = 'F.CUSTOMER.ACCOUNT'
    FV.CUSTOMER.ACCOUNT = ''
    FV.CUSTOMER.ACCOUNT = ''
    FN.CUS = 'FBNK.CUSTOMER'
    FV.CUS = '' ; R.CUS = '' ; ER.CUS = ''


*============================= Check Quarter Date  ========================================================
*DEBUG
*CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
    CHK.DAT = 0
    DAT1 = TODAY[1,6]:"01"
    CALL CDT("",DAT1,'-1C')
*    IF DAT1[5,4] = "0331" OR DAT1[5,4] = "0531" OR DAT1[5,4] = "0930" OR DAT1[5,4] = "1231" THEN CHK.DAT = "1"
    CHK.DAT = 1
*    IF CHK.DAT = 1 THEN
    START.DAT = DAT1[1,6]:"01"
*    END
*==========================================================================================================
*----------------------------------- START OPEN FILE ----------------------------
    OPENSEQ "&SAVEDLISTS&" , "LOAN.8.DIFF.CBE.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"LOAN.8.DIFF.CBE.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "LOAN.8.DIFF.CBE.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LOAN.8.DIFF.CBE.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create LOAN.8.DIFF.CBE.csv File IN &SAVEDLISTS&'
        END
    END
*----------------------------------- END OPEN FILE -----------------------------
*----------------------------------- CREATE HEADER -----------------------------
    HEAD.DESC = "Contract NO.,"
    HEAD.DESC := "Customer,"
    HEAD.DESC := "Customer Name,"
    HEAD.DESC := "Int. Rate,"
    HEAD.DESC := "Int. Date,"
    HEAD.DESC := "Outstanding,"
    HEAD.DESC := "Interest,"
    HEAD.DESC := 'Diff. Rate,'
    HEAD.DESC := "Difference,"
    HEAD.DESC := "No Of Days,"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------ END CREATE HEADER -------------------------------

RETURN


OPENFILES:
*---------
    CALL OPF(FN.LM,F.LM)
    CALL OPF(YF.ENQ,F.ENQ)
    CALL OPF(FN.CUSTOMER.ACCOUNT,FV.CUSTOMER.ACCOUNT)
    CALL OPF(FN.CUS,FV.CUS)
RETURN

*--------------------------------------------------------------------------------
PROCESS:
*-------
    IF CHK.DAT = 1 THEN
        READ R.ENQ FROM F.ENQ,"LD.BALANCES.FULL" ELSE R.ENQ = ""

        SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG EQ 21058 OR LD.CATEG EQ 21059 OR LD.CATEG EQ 21061 BY @ID "
*        SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG EQ 21061 BY @ID "
        CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,RET.CODE)
        LOOP
            REMOVE Y.ID FROM SEL.LIST SETTING POS
        WHILE Y.ID:POS
            LMM.ID = Y.ID

            READ R.RECORD FROM F.LM,LMM.ID ELSE R.RECORD = ""
            CALL LD.ENQ.INT.I ;* Open files and store in common

            ID = LMM.ID
            CALL E.LD.SCHED.LIST
            TOTAL.INT = 0

            INT.RATE    = R.RECORD<LD.SL.CURRENT.RATE>
            CUS.ID      = R.RECORD<LD.SL.CUSTOMER.NO>
            CATEGORY = R.RECORD<LD.SL.CATEGORY>
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,FV.CUS,ER.CUS)
            CUS.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

*Line [ 145 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR VM1 = 1 TO DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM)
*DEBUG
                IF VM1 = 1 THEN
                    DATE.1 = R.RECORD<LD.SL.EVENT.DATE,VM1>
                END ELSE
                    DATE.1 = R.RECORD<LD.SL.EVENT.DATE,VM1-1>
                END
                DATE.2 = R.RECORD<LD.SL.EVENT.DATE,VM1>
                INT.AMOUNT = R.RECORD<LD.SL.INT.AMOUNT,VM1>
                EVENT.DATE  = R.RECORD<LD.SL.EVENT.DATE,VM1>
                DAYS = 0
                IF (DATE.2 GE START.DAT AND DATE.2 LE DAT1) THEN
                    RAT = "64EGP":EVENT.DATE
                    CALL EB.GET.INTEREST.RATE(RAT,SCB.INT.RATE)
                    SCB.DIF.RAT = SCB.INT.RATE + 2 - INT.RATE
                    IF VM1 = 1 AND R.RECORD<LD.SL.EVENT.DATE,VM1+1> GT DAT1 THEN
                        DAYS = "C"
                        CALL CDD("",DATE.1,DAT1,DAYS)
                        TOTAL.INT = R.RECORD<LD.SL.RUNNING.BAL,VM1> * SCB.DIF.RAT /100 * DAYS/360
                        Y.AMT = 0
                        Y.AMT = R.RECORD<LD.SL.RUNNING.BAL,VM1>
                    END ELSE
                        IF VM1 GT 1 AND R.RECORD<LD.SL.EVENT.DATE,VM1+1> GT DAT1 AND  R.RECORD<LD.SL.EVENT.DATE,VM1> NE DAT1 THEN
                            DAYS = "C"
                            CALL CDD("",DATE.1,DAT1,DAYS)
                            TOTAL.INT = R.RECORD<LD.SL.RUNNING.BAL,VM1-1> * SCB.DIF.RAT /100 * DAYS/360
                            Y.AMT = 0
                            Y.AMT = R.RECORD<LD.SL.RUNNING.BAL,VM1-1>
                        END ELSE
                            DAYS = "C"
                            CALL CDD("",DATE.1,DATE.2,DAYS)
                            TOTAL.INT = R.RECORD<LD.SL.RUNNING.BAL,VM1-1> * SCB.DIF.RAT /100 * DAYS/360
                            Y.AMT = 0
                            Y.AMT = R.RECORD<LD.SL.RUNNING.BAL,VM1-1>
                        END
                    END


                    TOTAL.INT = DROUND(TOTAL.INT,2)
                    IF TOTAL.INT LT 0 THEN TOTAL.INT = TOTAL.INT * -1
                    Y.CUSTOMER = R.RECORD<LD.SL.CUSTOMER.NO>

                    CALL F.READ(FN.LM,Y.ID,R.LM,F.LM,LM.ERR1)
                    I=0


                    LD.DET.ARR = Y.ID:",":Y.CUSTOMER:",":CUS.NAME:",":INT.RATE:",":DATE.2:",":Y.AMT:",":INT.AMOUNT:",":SCB.DIF.RAT:',':TOTAL.INT:",":DAYS

                    WRITESEQ LD.DET.ARR TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
* END
                END
                LD.DET.ARR = ''
            NEXT VM1
        REPEAT
    END
RETURN
END
