* @ValidationCode : MjoyMDY3OTE4OTM3OkNwMTI1MjoxNjQ0OTI1MDYwMjUwOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBQ.DRMNT.CHARGE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

RETURN

*------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    CHG.ID.GE = "CHGDRMTGE3Y"
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID.GE,CR.ACT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID.GE,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CR.ACT=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

**    CHG.ID.LT = "ACCHGDRMT3Y"
**    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID.LT,CR.ACT)

**    V.DAT = TODAY
**    CALL ADD.MONTHS(V.DAT,'-36')
    W.BAL = 0 ; DB.AMT = 0
RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CB = 'FBNK.CUSTOMER.ACCOUNT' ; F.CB = ''
    CALL OPF(FN.CB,F.CB)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    T.SEL1 = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT LT 89 AND DRMNT.CODE EQ 1 AND SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300 AND SECTOR NE 1400 BY @ID"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.CU,KEY.LIST1<I>,R.CU,F.CU,E1)
            CUST.ID = KEY.LIST1<I>
            COMP = R.CU<EB.CUS.COMPANY.BOOK>
            COM.CODE = COMP[8,2]

            OFS.USER.INFO = "AUTO.CHRGE":"/":"/" :COMP

            DRMT.DATE = R.CU<EB.CUS.LOCAL.REF,CULR.DRMNT.DATE>

** IF DRMT.DATE GE V.DAT THEN
**     CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID.LT,DB.AMT)
** END ELSE
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID.GE,DB.AMT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID.GE,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DB.AMT=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>
** END

            CALL F.READ(FN.CB,CUST.ID,R.CB,F.CB,E1)
            IF NOT(E1) THEN
                FLG = 0
                H   = 1
                LOOP WHILE FLG = 0
                    IF R.CB<H,EB.CAC.ACCOUNT.NUMBER> = '' THEN
                        FLG = 1
                    END
                    ACT.CB = R.CB<H,EB.CAC.ACCOUNT.NUMBER>
                    CALL F.READ(FN.ACCT,ACT.CB,R.ACCT,F.ACCT,EAC)
                    IF NOT(EAC) THEN
                        ACT.CATEG = R.ACCT<AC.CATEGORY>
                        IF ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1006 OR ACT.CATEG EQ 6512 OR ACT.CATEG EQ 6511 OR (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR (ACT.CATEG GE 1101 AND ACT.CATEG LE 1204 OR ACT.CATEG GE 1290 AND ACT.CATEG LE 1599) THEN
                            W.BAL = R.ACCT<AC.WORKING.BALANCE>
                            IF W.BAL GT 0 THEN
                                CUR.CODE  = R.ACCT<AC.CURRENCY>
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                                CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR.CODE,RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.CODE,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>

                                IF CUR.CODE EQ "EGP" THEN
                                    RATE = 1
                                END

                                IF CUR.CODE NE "EGP" THEN
                                    DB.AMT  = (DB.AMT / RATE)
                                    DB.AMTE = FIELD(DB.AMT,".",2)
                                    DB.AMT  = FIELD(DB.AMT,".",1):".":DB.AMTE[1,2]
                                END

                                IF W.BAL LE DB.AMT THEN
                                    DEBIT.ACCT = ACT.CB
                                    CUS = ACT.CB[1,8]:".":TODAY
                                    GOSUB CR.FT.OFS1
************************************
                                    IF CUR.CODE NE 'EGP' THEN
                                        W.BAL  = W.BAL  * RATE
                                        DB.AMT = DB.AMT * RATE
                                    END
************************************
                                    DB.AMT = DB.AMT - W.BAL

                                    IF DB.AMT = 0 THEN FLG = 1
                                END ELSE
                                    DEBIT.ACCT = ACT.CB
                                    CUS = ACT.CB[1,8]:".":TODAY
                                    GOSUB CR.FT.OFS
                                    FLG = 1
                                END
                            END
                        END
                    END
                    H = H + 1
                REPEAT
            END
        NEXT I
    END
RETURN
****************************************************************
CR.FT.OFS:
*********
    DR.ACCT = DEBIT.ACCT
    CURR    = DEBIT.ACCT[9,2]
    CR.ACCT = "PL":CR.ACT
    DATEE   = TODAY
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC58":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS[1,7]

    F.PATH   = FN.OFS.IN
    OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    TER.NO.P = COM.CODE + 1000
    OFS.ID   = "T":TRIM(TER.NO.P, '0', 'L'):".P.":CUS:".":H

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
RETURN
****************************************************************
CR.FT.OFS1:
*********
    DR.ACCT = DEBIT.ACCT
    CURR    = DEBIT.ACCT[9,2]
    CR.ACCT = "PL":CR.ACT
    DATEE   = TODAY
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC58":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":W.BAL:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS[1,7]

    F.PATH   = FN.OFS.IN
    OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    TER.NO.P = COM.CODE + 1000
    OFS.ID   = "T":TRIM(TER.NO.P, '0', 'L'):".P.":CUS:".":H

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
RETURN
END
