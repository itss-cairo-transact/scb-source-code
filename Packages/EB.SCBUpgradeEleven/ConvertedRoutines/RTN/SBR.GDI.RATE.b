* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNjY2OTQ6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.GDI.RATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.GEN.CONDITION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 42 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 43 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBR.GDI.RATE'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY

    RETURN
*===============================================================
CALLDB:
    FN.GD   = 'FBNK.GROUP.DEBIT.INT'  ; F.GD  = ''
    FN.BAS  = 'FBNK.BASIC.INTEREST' ; F.BAS = ''
    FN.AGC  = 'FBNK.ACCT.GEN.CONDITION'; F.AGC = '' ; R.AGC = ''
    FN.CURR = 'FBNK.CURRENCY' ; F.CURR = '' ; R.CURR = ''

    CALL OPF(FN.GD,F.GD)
    CALL OPF(FN.BAS,F.BAS)
    CALL OPF(FN.AGC,F.AGC)
    CALL OPF(FN.CURR,F.CURR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""
*************************************************************************
    T.SEL = "SELECT ":FN.AGC:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AGC,KEY.LIST<I>,R.AGC,F.AGC,E1)
            Y.DESC = R.AGC<EB.AGC.DESCRIPTION,1>
            T.SEL1 = "SELECT ":FN.CURR:" WITH @ID EQ 'EGP'"
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 THEN
                FOR X = 1 TO SELECTED1

                    GDI.ID = KEY.LIST<I>:KEY.LIST1<X>:"..."


                    T.SEL2 = "SELECT ":FN.GD: " WITH @ID LIKE ":GDI.ID:" AND DR.BASIC.RATE IN (50 51 52 53 54 55 60 63 64 65 71 72 73 76 79) BY @ID"
                    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
                    IF SELECTED2 THEN
                        CALL F.READ(FN.GD,KEY.LIST2<SELECTED2>,R.GD,F.GD,E1)

                        BAS.RATE = R.GD<IC.GDI.DR.BASIC.RATE>
                        BAS.MRGN = R.GD<IC.GDI.DR.MARGIN.RATE>
                        OPR = R.GD<IC.GDI.DR.MARGIN.OPER>
                        INT.ID = BAS.RATE : KEY.LIST1<X> : "..."

                        T.SEL3 = "SELECT ":FN.BAS: " WITH @ID LIKE ":INT.ID:" BY @ID"
                        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
                        IF SELECTED3 THEN
                            CALL F.READ(FN.BAS,KEY.LIST3<SELECTED3>,R.BAS,F.BAS,E2)

                            RAT = R.BAS<EB.BIN.INTEREST.RATE>
                            TOT = RAT + BAS.MRGN

                            XX   = SPACE(120)
                            XX1  = SPACE(120)
                            XX<1,1>[1,20]   = KEY.LIST2<SELECTED2>
                            XX1<1,1>[1,40]  = Y.DESC
                            XX<1,1>[25,20]  = BAS.RATE
                            XX<1,1>[50,20]  = RAT
                            XX<1,1>[75,20]  = OPR
                            XX<1,1>[100,20] = BAS.MRGN
                            XX<1,1>[120,20] = TOT
                            PRINT XX<1,1>
                            PRINT XX1<1,1>
                            PRINT STR(' ',120)

                        END
                    END
                NEXT X
            END
        NEXT I
    END
*************************************************************************

    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  END.OF.REPORT  ***'
    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT XX25<1,1>

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    T.M  = DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"SUEZ.CANAL.BANK"
    PR.HD :="'L'":SPACE(1):" DATE:":T.DAY:SPACE(85):"PAGE.NO:":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"GROUP.DEBIT.INTEREST--Local.Currency"
    PR.HD :="'L'":SPACE(50):"FOR MONTH:":T.M
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"ID":SPACE(20):"BASIC.RATE.ID":SPACE(10):"INTEREST.BASIC.RATE":SPACE(10):"ADD/SUB":SPACE(10):"MARGIN.RATE":SPACE(10):"APPLIED.INTEREST.RATES"
    PR.HD :="'L'":"GROUP.NAME"
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
