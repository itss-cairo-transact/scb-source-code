* @ValidationCode : MjoxMjI2OTEyOTQ3OkNwMTI1MjoxNjQwNzg2MzYwNDI0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 15:59:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>973</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBM.EMP.INT.2PRCNT.004
*  PROGRAM SBM.EMP.INT.2PRCNT.004
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCR.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EMP.INT.2.TEMP
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EMP.INT.2.PERM
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*---------------------------------------------------
    FN.TEMP = "F.SCB.EMP.INT.2.TEMP"    ; F.TEMP = ""
*
    FN.STMT.CR = "FBNK.STMT.ACCT.CR"    ; F.STMT.CR = ""
*
    FN.ACR.CR = "FBNK.ACCR.ACCT.CR"     ; F.ACR.CR = ""
*
    FN.CUAC = "FBNK.CUSTOMER.ACCOUNT"   ; F.CUAC = ""
*
    FN.AC = "FBNK.ACCOUNT"              ; F.AC = ""
*
    FN.DATE = "F.DATES"                 ; F.DATE  = ""
*
    FN.PERM = "F.SCB.EMP.INT.2.PERM"    ; F.PERM = ""
*----------------------------------------------------
    CALL OPF (FN.TEMP,F.TEMP)
    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.PERM,F.PERM)
    CALL OPF (FN.STMT.CR,F.STMT.CR)
    CALL OPF (FN.ACR.CR,F.ACR.CR)
    CALL OPF (FN.CUAC,F.CUAC)
    CALL OPF (FN.AC,F.AC)

*============================================================================================================

    R.TEMP = ""
    GOSUB A.10.DATE
    GOSUB A.50.GET.STAFF
RETURN

*============================================================================================================
A.10.DATE:
    WS.DATE.ID      = "EG0010001-COB"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.WRK.DAT = R.DATE<EB.DAT.LAST.WORKING.DAY>
    WS.TODAY.DAT    = R.DATE<EB.DAT.TODAY>
    WS.LAST.PER.END = R.DATE<EB.DAT.LAST.PERIOD.END>

    WS.LPE.DATE     = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.PE.DATE      = R.DATE<EB.DAT.PERIOD.END>
    WS.LAST.PER.END = WS.PE.DATE

    WS.YY.MM        = WS.LAST.PER.END[1,6]
    WS.DD           = WS.LAST.PER.END[2]
    WS.MM           = WS.YY.MM[5,2] + 0
    WS.LOCAT.DATE   = WS.YY.MM:"01"
RETURN
*-----------------------------------------
**** ADDED BY MOHAMED SABRY 2012/08/28

A.50.GET.STAFF:
    SEL.TEMP = "SELECT ":FN.TEMP:" WITH RECORD.STATUS NE 'D' BY @ID "
    CALL EB.READLIST(SEL.TEMP,KEY.LIST.TEMP,"",SELECTED.TEMP,ER.MSG.TEMP)
    IF SELECTED.TEMP THEN
        FOR I.TEMP = 1 TO SELECTED.TEMP
            CALL F.READ(FN.CUAC,KEY.LIST.TEMP<I.TEMP>,R.CUAC,F.CUAC,ER.CUAC)
            WS.CUSTOMER.TEMP = KEY.LIST.TEMP<I.TEMP>
*           CALL DBR ('SCB.EMP.INT.2.TEMP':@FM:EMP.RLAT.NO,WS.CUSTOMER.TEMP,WS.RLAT.NO)
            LOOP
                REMOVE WS.AC.ID FROM R.CUAC SETTING POSAC
            WHILE WS.AC.ID:POSAC
                CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,ER.AC)
                WS.AC.CY = R.AC<AC.CURRENCY>
                WS.AC.GL = R.AC<AC.CATEGORY>
                WS.AC.CU = R.AC<AC.CUSTOMER>
                WS.AC.CO = R.AC<AC.CO.CODE>
                IF WS.AC.CY EQ 'EGP' THEN
                    IF WS.AC.GL GE 6501 AND WS.AC.GL LE 6511 THEN
                        WS.REF.NO   = WS.AC.CU

                        WS.STMT.CR.ID = WS.AC.ID:'-':WS.PE.DATE
                        WS.ACR.CR.ID  = WS.AC.ID
                        CALL F.READ(FN.STMT.CR,WS.STMT.CR.ID,R.STMT.CR,F.STMT.CR,MSG.STMT.CR)

                        IF NOT(MSG.STMT.CR) THEN
                            GOSUB A.100.STMT.CR
                        END ELSE
                            CALL F.READ(FN.ACR.CR,WS.ACR.CR.ID,R.ACR.CR,F.ACR.CR,MSG.ACR.CR)
                            GOSUB A.200.SAVOTHR
                        END
                    END
                END

            REPEAT
        NEXT I.TEMP
    END

RETURN
**** END OF ADDED BY MOHAMED SABRY 2012/08/28
*--------------------------------------------
A.100.STMT.CR:

    IF WS.AC.GL EQ 6502 THEN
********WS.COMN.ID = WS.STMT.CR.ID
        WS.COMN.ID = WS.ACR.CR.ID
    END ELSE
        WS.COMN.ID = WS.ACR.CR.ID
    END

    LOCPOS     = 0
    WS.BAL.TOT = 0
    WS.BAL     = 0
    A.ARY      = R.STMT.CR<IC.STMCR.CR.INT.DATE>
*Line [ 142 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.LOCAT.DATE IN A.ARY<1,1> SETTING LOCPOS ELSE NULL
    WS.BAL.ARY = R.STMT.CR<IC.STMCR.CR.VAL.BALANCE>
    WS.BAL.TOT = WS.BAL.ARY<1,LOCPOS>

    GOSUB A.105.CHK.TMP
    IF MSG.TEMP NE ""  THEN
        GOTO A.100.REP
    END
    MSG.PERM = ""
    CALL F.READ(FN.PERM,WS.COMN.ID,R.PERM,F.PERM,MSG.PERM)
    GOSUB A.110.CRT.PERM.STMT.CR

A.100.REP:
RETURN
*-------------------------------------------------
A.105.CHK.TMP:
    MSG.TEMP = ""
    WS.GRND.TOTAL = 0
    CALL F.READ(FN.TEMP,WS.REF.NO,R.TEMP,F.TEMP,MSG.TEMP)
    IF MSG.TEMP NE ""  THEN
        RETURN
    END
    IF R.TEMP<EMP.RECORD.STATUS>  EQ "D"  THEN
        RETURN
    END
    WS.GRND.TOTAL = R.TEMP<EMP.TOTAL.AMT>
RETURN
*-------------------------------------------
A.110.CRT.PERM.STMT.CR:
    GOSUB A.120.CALC.INT

    IF WS.AC.GL EQ 6502 THEN
        R.PERM<EMP.PAY.TYPE>  = 1
        R.PERM<EMP.MONTH.PAY> = WS.MM
    END
    IF WS.AC.GL EQ 6504 THEN
        R.PERM<EMP.PAY.TYPE>  = 12
        R.PERM<EMP.MONTH.PAY> = 12
    END
    IF WS.AC.GL EQ 6503 THEN
        R.PERM<EMP.PAY.TYPE>  = 6
        IF WS.MM GE 1 AND WS.MM LE 6 THEN
            R.PERM<EMP.MONTH.PAY> = 6
        END
        IF WS.MM GE 7 AND WS.MM LE 12 THEN
            R.PERM<EMP.MONTH.PAY> = 12
        END
    END
    IF WS.AC.GL EQ 6501 OR WS.AC.GL EQ 6511 THEN
        R.PERM<EMP.PAY.TYPE>  = 3
        IF WS.MM EQ 1 OR WS.MM EQ 2 OR WS.MM EQ 3 THEN
            R.PERM<EMP.MONTH.PAY> = 3
        END
        IF WS.MM EQ 4 OR WS.MM EQ 5 OR WS.MM EQ 6 THEN
            R.PERM<EMP.MONTH.PAY> = 6
        END
        IF WS.MM EQ 7 OR WS.MM EQ 8 OR WS.MM EQ 9 THEN
            R.PERM<EMP.MONTH.PAY> = 9
        END
        IF WS.MM EQ 10 OR WS.MM EQ 11 OR WS.MM EQ 12 THEN
            R.PERM<EMP.MONTH.PAY> = 12
        END
    END
    R.PERM<EMP.CO.CODE>                =  WS.AC.CO
    R.PERM<EMP.CU.RLAT.NO>                =  R.TEMP<EMP.RLAT.NO>      ;*WS.RLAT.NO

    R.PERM<EMP.PERM.TOTAL,LOCPOS>      =  WS.GRND.TOTAL
    R.PERM<EMP.PERM.LOWST.BAL,LOCPOS>  =  WS.BAL.TOT
    R.PERM<EMP.PERM.INT,LOCPOS>        =  DROUND(WS.INT.CALC,2)
    R.PERM<EMP.PERM.YYMM,LOCPOS>       =  WS.PE.DATE[1,6]
    R.PERM<EMP.PERM.STATUS,LOCPOS>     =  ""

    CALL F.WRITE(FN.PERM,WS.COMN.ID,R.PERM)
**  CALL  JOURNAL.UPDATE(WS.COMN.ID)
RETURN
*-------------------------------------------------
A.120.CALC.INT:
    IF  WS.GRND.TOTAL GT 100000 THEN
        WS.INT.CALC = (WS.BAL.TOT * 2 * WS.DD * 100000) / (36500 * WS.GRND.TOTAL)
        RETURN
    END
    WS.INT.CALC = (WS.BAL.TOT * 2 * WS.DD) / 36500
RETURN
*-------------------------------------------------
*A.130.CALC:
*    WS.INT.CALC = (WS.BAL.TOT * 2 * WS.DD * 100000) / (36500 * WS.GRND.TOTAL)
*RETURN

*============================================================================================================
*������ ������� ����� ����
*============================================================================================================
A.200.SAVOTHR:

    WS.COMN.ID = WS.ACR.CR.ID
    WS.REF.NO8 = WS.ACR.CR.ID[8]
    WS.REF.NO8.6 = WS.REF.NO8[1,6]

    IF  WS.REF.NO8.6 EQ 106501   THEN
        GOTO A.200.A
    END
    IF  WS.REF.NO8.6 EQ 106503   THEN
        GOTO A.200.A
    END
    IF  WS.REF.NO8.6 EQ 106504   THEN
        GOTO A.200.A
    END
    IF  WS.REF.NO8.6 EQ 106511   THEN
        GOTO A.200.A
    END

    GOTO A.200.REP
A.200.A:
    WS.BAL.TOT = 0
    WS.BAL = 0
    A.ARY =  R.ACR.CR<IC.ACRCR.CR.INT.DATE>
*Line [ 258 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.LOCAT.DATE IN A.ARY<1,1> SETTING LOCPOS ELSE NULL
    WS.BAL.ARY = R.ACR.CR<IC.ACRCR.CR.VAL.BALANCE>
    WS.BAL.TOT = WS.BAL.ARY<1,LOCPOS>

    GOSUB A.105.CHK.TMP
    IF MSG.TEMP NE ""  THEN
        GOTO A.200.REP
    END
    MSG.PERM = ""
    CALL F.READ(FN.PERM,WS.COMN.ID,R.PERM,F.PERM,MSG.PERM)
    GOSUB A.110.CRT.PERM.STMT.CR

A.200.REP:
RETURN
*-------------------------------------------------
END
