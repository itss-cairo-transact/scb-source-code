* @ValidationCode : MjotMTAyNTE1NDI4NDpDcDEyNTI6MTY0NDkyNTA1Mjk5NzpsYXA6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBM.DEP.CHANGE.MILION.EGP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.DEP.CHANGE.MILION.EGP'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
    AMT.LCY   = 0   ; AMT.LCY.C = 0     ; AMT.LCY.O = 0
    AMT.LCY.TOT = 0 ; AMT.LCY.TOT.C = 0 ; AMT.LCY.TOT.O = 0
    NEW.CATEG = ''
RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.CBE.D = 'F.CBE.STATIC.AC.LD.DETAIL' ; F.CBE.D = ''
    CALL OPF(FN.CBE.D,F.CBE.D)


    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CATG = 'F.CATEGORY' ; F.CATG = ''
    CALL OPF(FN.CATG,F.CATG)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.CBE:" WITH CBE.BR EQ ":COMP.BR:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            CUS.ID  = R.CBE<ST.CBE.CUSTOMER.CODE>

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            AMT.LCY.CUR   = R.CBE<ST.CBE.CUR.AC.LE> + R.CBE<ST.CBE.FACLTY.LE.CR> + R.CBE<ST.CBE.LOANS.LE.S.CR> + R.CBE<ST.CBE.LOANS.LE.M.CR> + R.CBE<ST.CBE.LOANS.LE.L.CR>
            AMT.LCY.DEP   = R.CBE<ST.CBE.DEPOST.AC.LE.1Y> + R.CBE<ST.CBE.DEPOST.AC.LE.2Y> + R.CBE<ST.CBE.DEPOST.AC.LE.3Y> + R.CBE<ST.CBE.DEPOST.AC.LE.MOR3> + R.CBE<ST.CBE.CER.AC.LE.3Y> + R.CBE<ST.CBE.CER.AC.LE.5Y> + R.CBE<ST.CBE.CER.AC.LE.GOLD> + R.CBE<ST.CBE.SAV.AC.LE>
            AMT.LCY.BLOCK = R.CBE<ST.CBE.BLOCK.AC.LE> + R.CBE<ST.CBE.MARG.LC.LE> + R.CBE<ST.CBE.MARG.LG.LE>


            AMT.LCY.CUR.O   = R.CBE<ST.CBE.CUR.AC.LEO> + R.CBE<ST.CBE.FACLTY.LE.CRO> + R.CBE<ST.CBE.LOANS.LE.S.CRO> + R.CBE<ST.CBE.LOANS.LE.M.CRO> + R.CBE<ST.CBE.LOANS.LE.L.CRO>
            AMT.LCY.DEP.O   = R.CBE<ST.CBE.DEPOST.AC.LE.1YO> + R.CBE<ST.CBE.DEPOST.AC.LE.2YO> + R.CBE<ST.CBE.DEPOST.AC.LE.3YO> + R.CBE<ST.CBE.DEPOST.AC.LE.MOR3O> + R.CBE<ST.CBE.CER.AC.LE.3YO> + R.CBE<ST.CBE.CER.AC.LE.5YO> + R.CBE<ST.CBE.CER.AC.LE.GOLDO> + R.CBE<ST.CBE.SAV.AC.LEO>
            AMT.LCY.BLOCK.O = R.CBE<ST.CBE.BLOCK.AC.LEO> + R.CBE<ST.CBE.MARG.LC.LEO> + R.CBE<ST.CBE.MARG.LG.LEO>

            AMT.LCY = AMT.LCY.CUR + AMT.LCY.DEP + AMT.LCY.BLOCK
            AMT.LCY.O = AMT.LCY.CUR.O + AMT.LCY.DEP.O + AMT.LCY.BLOCK.O
            GOSUB CALC
        NEXT I
    END

    T.SEL1 = "SELECT ":FN.CBE.D:" WITH CBEM.CATEG IN (16151 16153 16188) AND CBEM.CY EQ 'EGP' AND CBEM.BR EQ ":COMP.BR:" BY CBEM.CATEG"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG2)
    IF SELECTED1 THEN
        FOR X = 1 TO SELECTED1
            CALL F.READ(FN.CBE.D,KEY.LIST1<X>,R.CBE.D,F.CBE.D,E3)
            CUS.ID  = R.CBE.D<STD.CBEM.CATEG>
            IF X = 1 THEN NEW.CATEG = CUS.ID
            IF CUS.ID NE NEW.CATEG THEN
                AMT.LCY   = 0
                AMT.LCY.O = 0
            END
            IF CUS.ID EQ NEW.CATEG THEN
                CALL F.READ(FN.CATG,CUS.ID,R.CATG,F.CATG,E2)
                CUST.NAME = R.CATG<EB.CAT.DESCRIPTION,1>

                AMT.LCY   += R.CBE.D<STD.CBEM.IN.LCY>
                AMT.LCY    = AMT.LCY * -1

                AMT.LCY.O += R.CBE.D<STD.CBEM.IN.LCYO>
                AMT.LCY.O  = AMT.LCY.O * -1

                GOSUB CALC
            END
            NEW.CATEG = CUS.ID
        NEXT X
    END

    PRINT STR('=',130)
    XX2 = SPACE(120)
    XX2<1,1>[1,35]   = "�����������"
    XX2<1,1>[65,20]  = AMT.LCY.TOT.O
    XX2<1,1>[85,20]  = AMT.LCY.TOT
    XX2<1,1>[105,20] = AMT.LCY.TOT.C
    PRINT XX2<1,1>
    PRINT STR('=',130)



CALC:
    AMT.LCY.C = AMT.LCY - AMT.LCY.O

    IF AMT.LCY.C LT 0 THEN
        AMTC = AMT.LCY.C * -1
    END ELSE
        AMTC = AMT.LCY.C
    END

    AMT.LCY   = AMT.LCY / 1000
    AMT.LCY.O = AMT.LCY.O / 1000
    AMT.LCY.C = AMT.LCY.C / 1000

    AMT.LCY   = DROUND(AMT.LCY,'0')
    AMT.LCY.O = DROUND(AMT.LCY.O,'0')
    AMT.LCY.C = DROUND(AMT.LCY.C,'0')

    IF AMTC GE 1000000 THEN
        XX = SPACE(120)
        XX<1,1>[1,15]   = CUS.ID
        XX<1,1>[13,35]  = CUST.NAME
        XX<1,1>[65,20]  = AMT.LCY.O
        XX<1,1>[85,20]  = AMT.LCY
        XX<1,1>[105,20] = AMT.LCY.C
        PRINT XX<1,1>
        PRINT STR('-',130)

        AMT.LCY.TOT   += AMT.LCY
        AMT.LCY.TOT.O += AMT.LCY.O
        AMT.LCY.TOT.C += AMT.LCY.C
    END
    AMT.LCY   = 0   ; AMT.LCY.C = 0     ; AMT.LCY.O = 0
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TDD = TODAY[1,6]


    DAT1 = TDD:'01'
    CALL CDT("",DAT1,'-1C')

    DAT2 = DAT1[1,6]:'01'
    CALL CDT("",DAT2,'-1C')

    TD1 = DAT1[1,4]:'/':DAT1[5,2]
    TD2 = DAT2[1,4]:'/':DAT2[5,2]



    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������ ����� ������� ������� �������"
    PR.HD :="'L'":SPACE(55):"���� ���� �� ����� ����":SPACE(20):"������ ������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(35):"���� ���":SPACE(10):"���� ���":SPACE(13):"������"
    PR.HD :="'L'":SPACE(65):TD2:SPACE(11):TD1
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
