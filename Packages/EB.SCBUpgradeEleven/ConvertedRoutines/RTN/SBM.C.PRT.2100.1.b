* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1151</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.2100.1
*    PROGRAM     SBM.C.PRT.2100.1
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
***"����� ��� "
****  2100
*------------------------------------------
    FN.CUS = "F.CUSTOMER"
    F.CUS  = ""

    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*---------------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CUS,F.CUS)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.TO = 0
    WS.SECTOR.NAME = ""
    WS.CUS.KEY = ""
    MSG.CUS = ""
    WS.CUS.NAME = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.PAG.COUNT = 0
    WS.LIN.COUNT = 55
    WS.1.LE = "0"
    WS.2.LE = "0"
    WS.3.LE = "0"

    WS.1.EQV = "0"
    WS.2.EQV = "0"
    WS.3.EQV = "0"

    WS.1.LE.SEC.TOT = 0
    WS.2.LE.SEC.TOT = 0
    WS.3.LE.SEC.TOT = 0
    WS.1.LE.GRND.TOT = 0
    WS.2.LE.GRND.TOT = 0
    WS.3.LE.GRND.TOT = 0
    WS.1.EQV.SEC.TOT = 0
    WS.2.EQV.SEC.TOT = 0
    WS.3.EQV.SEC.TOT = 0
    WS.1.EQV.GRND.TOT = 0
    WS.2.EQV.GRND.TOT = 0
    WS.3.EQV.GRND.TOT = 0

    WS.1.LE.TOT = 0
    WS.2.LE.TOT = 0
    WS.3.LE.TOT = 0
    WS.1.EQV.TOT = 0
    WS.2.EQV.TOT = 0
    WS.3.EQV.TOT = 0
    WS.1.LE.W = 0
    WS.2.LE.W = 0
    WS.3.LE.W = 0
    WS.1.EQV.W = 0
    WS.2.EQV.W = 0
    WS.3.EQV.W = 0
    WS.LE.TOT.LINE = 0
    WS.EQV.TOT.LINE = 0
    WS.TOT.LINE.ALL = 0
    WS.TO.COMPAR = 0
    WS.DPST = "0"
    WS.DPST1 = "0"
    WS.CER = "0"
    WS.LAST = 0
    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.HD.T  = "����� ����� ������� � ��������� ���������� � ������� �������� "

    WS.HD.TA = " ����� ��� 2100 "

    WS.HD.T2 = "�������� ������ ������ ����� "
    WS.HD.T2A = "����    "
    WS.HD.T3  = "������ ���� ����"

    WS.HD.1  = "���� �����"
    WS.HD.1A = "���� ������"
*------------------------------
    WS.HD.2  = "���"

    WS.HD.2A = "����"

    WS.HD.2B = "�����"

    WS.HD.2D = "�������"

    WS.HD.2E = "��������"

*-----------------------------------
    WS.HD.3 = "�����"

    WS.HD.3A = "�����"

    WS.HD.3B = "������"
*-------------------------------
    WS.HD.4A  = "�������"
*--------------------------------------
    WS.PRG.1 = "SBM.C.PRT.2100.1"
*------------------------------------------------

********************** ********************************
*******************   PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    GOSUB A.050.GET.ALL.BR
*    GOSUB A.5000.PRT.HEAD
*------------------------------------------START PROCESSING
*    GOSUB A.100.PROCESS
*    WS.LAST = "1"
*    GOSUB A.500.PRT.SECTOR.TOT
*    GOSUB A.600.ALL.SECTOR.TOT
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*----------------------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE 99 THEN
            GOTO A.050.A
        END
        IF WS.BR LT 10 THEN
            WS.BR = WS.COMP.ID[1]
        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0
*-----------------------------------------------------------------
*            GOSUB  A.5100.PRT.SPACE.PAGE


*            GOSUB A.5000.PRT.HEAD
*            GOSUB A.300.PRNT
            WS.LAST = "1"
            GOSUB A.500.PRT.SECTOR.TOT
            GOSUB A.600.ALL.SECTOR.TOT
            WS.LIN.COUNT = 55
            WS.TO.COMPAR = 0
        END
*
A.050.A:
    REPEAT
    RETURN
*----------------------------------------------------------------
A.100.PROCESS:



*    SEL.CMD = "SELECT ":FN.CBE:" BY CBE.INDUSTRY"
*                                            ������� �� ����� ������ �����
    IF WS.BR EQ 99 THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE CC... AND CBE.NEW.SECTOR IN(1130 2130 3130 4130) BY CBE.NEW.INDUSTRY"
    END
    IF WS.BR NE 99 THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE CC... AND CBE.NEW.SECTOR IN(1130 2130 3130 4130)"
        SEL.CMD := " AND CBE.BR EQ ":WS.BR:" BY CBE.NEW.INDUSTRY"
    END

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS

        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        WS.BRX = R.CBE<P.CBE.BR>
        IF WS.BRX NE WS.BR THEN
            GOTO START.B
        END
A.100.A:
        WS.NEW.SECTOR = R.CBE<P.CBE.NEW.SECTOR>


START.A:
        WS.INDSTRY = R.CBE<P.CBE.NEW.INDUSTRY>
*                         ����� ��     ���� ���� ������� �����    INDUSTRY

        IF WS.INDSTRY GE 2000 AND WS.INDSTRY LE 2070 THEN
            WS.TO = WS.INDSTRY
            GOTO START.A1
        END




        GOTO START.B
START.A1:
        WS.1.LE = 0
        WS.2.LE = 0
        WS.3.LE = 0

        WS.1.EQV = 0
        WS.2.EQV = 0
        WS.3.EQV = 0
        WS.DPST = 0
        WS.DPST1 = 0
        WS.CER = 0
*-------------------------------------------------
        WS.1.LE = R.CBE<P.CBE.CUR.AC.LE>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.FACLTY.LE.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.S.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.M.CR>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.LOANS.LE.L.CR>

        WS.DPST  = R.CBE<P.CBE.DEPOST.AC.LE.1Y> + R.CBE<P.CBE.DEPOST.AC.LE.2Y>
        WS.DPST1 = R.CBE<P.CBE.DEPOST.AC.LE.3Y> + R.CBE<P.CBE.DEPOST.AC.LE.MOR3>
        WS.CER  = R.CBE<P.CBE.CER.AC.LE.3Y> + R.CBE<P.CBE.CER.AC.LE.5Y> + R.CBE<P.CBE.CER.AC.LE.GOLD>
        WS.2.LE = WS.DPST + WS.DPST1 + WS.CER + R.CBE<P.CBE.SAV.AC.LE>

        WS.3.LE = R.CBE<P.CBE.BLOCK.AC.LE> + R.CBE<P.CBE.MARG.LC.LE> + R.CBE<P.CBE.MARG.LG.LE>

*----------------------------------------------
        WS.1.EQV = R.CBE<P.CBE.CUR.AC.EQ>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.FACLTY.EQ.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.S.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.M.CR>
        WS.1.EQV = WS.1.EQV + R.CBE<P.CBE.LOANS.EQ.L.CR>


        WS.DPST = 0
        WS.DPST1 = 0
        WS.CER = 0

        WS.DPST  = R.CBE<P.CBE.DEPOST.AC.EQ.1Y> + R.CBE<P.CBE.DEPOST.AC.EQ.2Y>
        WS.DPST1 = R.CBE<P.CBE.DEPOST.AC.EQ.3Y> + R.CBE<P.CBE.DEPOST.AC.EQ.MOR3>
        WS.CER  = R.CBE<P.CBE.CER.AC.EQ.3Y> + R.CBE<P.CBE.CER.AC.EQ.5Y> + R.CBE<P.CBE.CER.AC.EQ.GOLD>
        WS.2.EQV = WS.DPST + WS.DPST1 + WS.CER + R.CBE<P.CBE.SAV.AC.EQ>

        WS.3.EQV = R.CBE<P.CBE.BLOCK.AC.EQ> + R.CBE<P.CBE.MARG.LC.EQ> + R.CBE<P.CBE.MARG.LG.EQ>

        GOSUB A.200.PRNT
*-----------------------------------------------------
START.B:
    REPEAT
    RETURN

A.200.PRNT:
*                                             ������ �������� ���   RECORD
    IF WS.TO.COMPAR = 0  THEN
        WS.TO.COMPAR = WS.TO
        GOSUB A.400.PRT.HEAD.OF.SECTOR
    END
*                            "����� ������ ������ �� ���� ������ ������"
    IF WS.TO GT WS.TO.COMPAR THEN
        GOSUB A.500.PRT.SECTOR.TOT
        WS.TO.COMPAR = WS.TO
        GOSUB A.400.PRT.HEAD.OF.SECTOR
    END
*                           CUSTOMER ������ ��� ��� ������ �� ��� �������
    MSG.CUS = ""
    WS.CUS.KEY = R.CBE<P.CBE.CUSTOMER.CODE>
    CALL F.READ(FN.CUS,WS.CUS.KEY,R.CUS,F.CUS,MSG.CUS)
    IF MSG.CUS EQ "" THEN
        WS.CUS.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
    END
    IF MSG.CUS NE "" THEN
        WS.CUS.NAME = "***********************************"
    END
    WS.1.LE = WS.1.LE / 1000
    WS.2.LE = WS.2.LE / 1000
    WS.3.LE = WS.3.LE / 1000
    WS.LE.TOT.LINE = WS.1.LE + WS.2.LE + WS.3.LE

    WS.1.LE.SEC.TOT = WS.1.LE.SEC.TOT + WS.1.LE
    WS.2.LE.SEC.TOT = WS.2.LE.SEC.TOT + WS.2.LE
    WS.3.LE.SEC.TOT = WS.3.LE.SEC.TOT + WS.3.LE

    WS.1.LE.GRND.TOT = WS.1.LE.GRND.TOT + WS.1.LE
    WS.2.LE.GRND.TOT = WS.2.LE.GRND.TOT + WS.2.LE
    WS.3.LE.GRND.TOT = WS.3.LE.GRND.TOT + WS.3.LE

    WS.1.EQV = WS.1.EQV / 1000
    WS.2.EQV = WS.2.EQV / 1000
    WS.3.EQV = WS.3.EQV / 1000
    WS.EQV.TOT.LINE = WS.1.EQV + WS.2.EQV + WS.3.EQV

    WS.1.EQV.SEC.TOT = WS.1.EQV.SEC.TOT + WS.1.EQV
    WS.2.EQV.SEC.TOT = WS.2.EQV.SEC.TOT + WS.2.EQV
    WS.3.EQV.SEC.TOT = WS.3.EQV.SEC.TOT + WS.3.EQV

    WS.1.EQV.GRND.TOT = WS.1.EQV.GRND.TOT + WS.1.EQV
    WS.2.EQV.GRND.TOT = WS.2.EQV.GRND.TOT + WS.2.EQV
    WS.3.EQV.GRND.TOT = WS.3.EQV.GRND.TOT + WS.3.EQV
    IF WS.LIN.COUNT GT  45 THEN
        GOSUB A.5000.PRT.HEAD
        WS.LIN.COUNT = 0
    END
    XX = SPACE(132)
    XX<1,1>[1,35]   = WS.CUS.NAME
    XX<1,1>[37,9]   = FMT(WS.1.LE, "R0")
    XX<1,1>[47,9]   = FMT(WS.2.LE, "R0")
    XX<1,1>[57,9]   = FMT(WS.3.LE, "R0")
    XX<1,1>[67,9]   = FMT(WS.LE.TOT.LINE, "R0")


    XX<1,1>[80,9]  = FMT(WS.1.EQV, "R0")
    XX<1,1>[90,9]  = FMT(WS.2.EQV, "R0")
    XX<1,1>[99,9]  = FMT(WS.3.EQV, "R0")
    XX<1,1>[108,9] = FMT(WS.EQV.TOT.LINE, "R0")
    WS.COMN = WS.LE.TOT.LINE + WS.EQV.TOT.LINE
    XX<1,1>[120,9] = FMT(WS.COMN, "R0")
    PRINT XX<1,1>
    WS.FLAG.PRT = 1
    WS.LIN.COUNT = WS.LIN.COUNT + 1
    RETURN


*--------------------------------------------------------
A.400.PRT.HEAD.OF.SECTOR:
    IF WS.LIN.COUNT GT 45 THEN
        GOSUB A.5000.PRT.HEAD
        WS.LIN.COUNT = 0
    END
    IF WS.TO EQ 2000      THEN
        WS.SECTOR.NAME = "���� ������� � ������                "
    END
    IF WS.TO EQ 2010       THEN
        WS.SECTOR.NAME = "���� ������� ����� �������� ��������  "
    END
    IF WS.TO EQ 2020     THEN
        WS.SECTOR.NAME = "���� �������                         "
    END
    IF WS.TO EQ 2030     THEN
        WS.SECTOR.NAME = "���� ������� ������                   "
    END
    IF WS.TO EQ 2040     THEN
        WS.SECTOR.NAME = "���� ��������                        "
    END
    IF WS.TO EQ 2050     THEN
        WS.SECTOR.NAME = "���� ���� ���� ������                "
    END
    IF WS.TO EQ 2060     THEN
        WS.SECTOR.NAME = "���� ������� � �������                 "
    END
    IF WS.TO EQ 2070     THEN
        WS.SECTOR.NAME = "���� �������                         "
    END
    XX = SPACE(132)
    XX<1,1>[1,35]   = WS.SECTOR.NAME
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[1,35]   = "-----------------------------------"
    PRINT XX<1,1>
    WS.LIN.COUNT = WS.LIN.COUNT + 2
    RETURN
*---------------------------------------------------------
A.500.PRT.SECTOR.TOT:
    IF WS.LIN.COUNT GT 45 THEN
        GOSUB A.5000.PRT.HEAD
        WS.LIN.COUNT = 0
    END
    XX = SPACE(132)
    XX<1,1>[37,9]   = "---------"
    XX<1,1>[47,9]   = "---------"
    XX<1,1>[57,9]   = "---------"
    XX<1,1>[67,9]   = "---------"
    XX<1,1>[80,9]  =  "---------"
    XX<1,1>[90,9]  =  "---------"
    XX<1,1>[99,9]  =  "---------"
    XX<1,1>[108,9] =  "---------"
    XX<1,1>[120,9] =  "---------"
    PRINT XX<1,1>


    XX = SPACE(132)
    XX<1,1>[1,35]   = "������ ������                      "
    XX<1,1>[37,9]   = FMT(WS.1.LE.SEC.TOT, "R0")
    XX<1,1>[47,9]   = FMT(WS.2.LE.SEC.TOT, "R0")
    XX<1,1>[57,9]   = FMT(WS.3.LE.SEC.TOT, "R0")
    WS.COMN = WS.1.LE.SEC.TOT + WS.2.LE.SEC.TOT + WS.3.LE.SEC.TOT
    XX<1,1>[67,9]  = FMT(WS.COMN, "R0")
    XX<1,1>[80,9]  = FMT(WS.1.EQV.SEC.TOT, "R0")
    XX<1,1>[90,9]  = FMT(WS.2.EQV.SEC.TOT, "R0")
    XX<1,1>[99,9]  = FMT(WS.3.EQV.SEC.TOT, "R0")
    WS.COMN1 = WS.1.EQV.SEC.TOT + WS.2.EQV.SEC.TOT + WS.3.EQV.SEC.TOT
    XX<1,1>[108,9] = FMT(WS.COMN1, "R0")

    WS.COMN = WS.COMN + WS.COMN1
    XX<1,1>[120,9] = FMT(WS.COMN, "R0")
    PRINT XX<1,1>
    IF WS.LAST EQ 0 THEN
        XX = SPACE(132)
        XX<1,1>[37,9]   = "---------"
        XX<1,1>[47,9]   = "---------"
        XX<1,1>[57,9]   = "---------"
        XX<1,1>[67,9]   = "---------"
        XX<1,1>[80,9]  =  "---------"
        XX<1,1>[90,9]  =  "---------"
        XX<1,1>[99,9]  =  "---------"
        XX<1,1>[108,9] =  "---------"
        XX<1,1>[120,9] =  "---------"
        PRINT XX<1,1>
    END

    WS.1.LE.SEC.TOT = 0
    WS.2.LE.SEC.TOT = 0
    WS.3.LE.SEC.TOT = 0
    WS.1.EQV.SEC.TOT = 0
    WS.2.EQV.SEC.TOT = 0
    WS.3.EQV.SEC.TOT = 0
    WS.LIN.COUNT = WS.LIN.COUNT + 3
    RETURN
*---------------------------------------------------------
A.600.ALL.SECTOR.TOT:
    IF WS.LIN.COUNT GT 45 THEN
        GOSUB A.5000.PRT.HEAD
        WS.LIN.COUNT = 0
    END

    XX = SPACE(132)
    XX<1,1>[37,9]   = "========="
    XX<1,1>[47,9]   = "========="
    XX<1,1>[57,9]   = "========="
    XX<1,1>[67,9]   = "========="
    XX<1,1>[80,9]  =  "========="
    XX<1,1>[90,9]  =  "========="
    XX<1,1>[99,9]  =  "========="
    XX<1,1>[108,9] =  "========="
    XX<1,1>[120,9] =  "========="
    PRINT XX<1,1>

*--- 2017-12-06 NESSMAAAA
*    WS.1.EQV.GRND.TOT = WS.1.EQV.GRND.TOT + WS.1.EQV
*    WS.2.EQV.GRND.TOT = WS.2.EQV.GRND.TOT + WS.2.EQV
*    WS.3.EQV.GRND.TOT = WS.3.EQV.GRND.TOT + WS.3.EQV

    XX = SPACE(132)
    XX<1,1>[1,35]  = "������ ��������                    "
    XX<1,1>[37,9]  = FMT(WS.1.LE.GRND.TOT, "R0")
    XX<1,1>[47,9]  = FMT(WS.2.LE.GRND.TOT, "R0")
    XX<1,1>[57,9]  = FMT(WS.3.LE.GRND.TOT, "R0")
    WS.COMN = WS.1.LE.GRND.TOT + WS.2.LE.GRND.TOT + WS.3.LE.GRND.TOT
    XX<1,1>[67,9]  = FMT(WS.COMN, "R0")
    XX<1,1>[80,9]  = FMT(WS.1.EQV.GRND.TOT, "R0")
    XX<1,1>[90,9]  = FMT(WS.2.EQV.GRND.TOT, "R0")
    XX<1,1>[99,9]  = FMT(WS.3.EQV.GRND.TOT, "R0")
    WS.COMN1 = WS.1.EQV.GRND.TOT + WS.2.EQV.GRND.TOT + WS.3.EQV.GRND.TOT
    XX<1,1>[108,9] = FMT(WS.COMN1, "R0")
    WS.COMN = WS.COMN + WS.COMN1
    XX<1,1>[120,9] = FMT(WS.COMN, "R0")
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[37,9]   = "========="
    XX<1,1>[47,9]   = "========="
    XX<1,1>[57,9]   = "========="
    XX<1,1>[67,9]   = "========="
    XX<1,1>[80,9]  =  "========="
    XX<1,1>[90,9]  =  "========="
    XX<1,1>[99,9]  =  "========="
    XX<1,1>[108,9] =  "========="
    XX<1,1>[120,9] =  "========="
    PRINT XX<1,1>

    WS.1.LE.SEC.TOT = 0
    WS.2.LE.SEC.TOT = 0
    WS.3.LE.SEC.TOT = 0
    WS.1.EQV.SEC.TOT = 0
    WS.2.EQV.SEC.TOT = 0
    WS.3.EQV.SEC.TOT = 0
    WS.1.LE.GRND.TOT = 0
    WS.2.LE.GRND.TOT =  0
    WS.3.LE.GRND.TOT  = 0
    WS.1.EQV.GRND.TOT = 0
    WS.2.EQV.GRND.TOT = 0
    WS.3.EQV.GRND.TOT = 0
    RETURN
*----------------------------------
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.LIN.COUNT = 0
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

    DATY  = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    WS.PAG.COUNT = WS.PAG.COUNT + 1
    PR.HD :="'L'":SPACE(50):WS.HD.T2:SPACE(28):WS.HD.T2A:SPACE(2):WS.PAG.COUNT
    PR.HD :="'L'":SPACE(110):WS.HD.T3

    PR.HD :="'L'":SPACE(54):WS.HD.1:SPACE(27):WS.HD.1A
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(36):WS.HD.2:SPACE(7):WS.HD.2A:SPACE(6):WS.HD.2B:SPACE(5):WS.HD.2D:SPACE(5):WS.HD.2:SPACE(8):WS.HD.2A:SPACE(5):WS.HD.2B:SPACE(3):WS.HD.2D:SPACE(7):WS.HD.2E
    PR.HD :="'L'":SPACE(36):WS.HD.3:SPACE(5):WS.HD.3A:SPACE(6):WS.HD.3B:SPACE(17):WS.HD.3:SPACE(3):WS.HD.3A:SPACE(3):WS.HD.3B
    PR.HD :="'L'":SPACE(46):WS.HD.4A:SPACE(36):WS.HD.4A
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    WS.LIN.COUNT = 0
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
