* @ValidationCode : MjoxOTIxMTczNDYwOkNwMTI1MjoxNjQ0OTI1MDUzOTY3OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*** ����� ������ ����� ������� ***
*** CREATED BY KHALED ***
***=================================

SUBROUTINE SBM.DEP.OWNER

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 42 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 43 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.DEP.OWNER'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.AMOUNT.LCY1 = 0  ; Y.AMOUNT.LCY11 = 0  ; Y.AMOUNT.LCY21 = 0
    Y.AMOUNT.LCY2 = 0  ; Y.AMOUNT.LCY12 = 0  ; Y.AMOUNT.LCY22 = 0
    Y.AMOUNT.LCY3  = 0  ; Y.AMOUNT.LCY13 = 0  ; Y.AMOUNT.LCY23 = 0
    Y.AMOUNT.LCY4  = 0  ; Y.AMOUNT.LCY14 = 0  ; Y.AMOUNT.LCY24 = 0
    Y.AMOUNT.LCY5  = 0  ; Y.AMOUNT.LCY15 = 0  ; Y.AMOUNT.LCY25 = 0
    Y.AMOUNT.LCY6  = 0  ; Y.AMOUNT.LCY16 = 0  ; Y.AMOUNT.LCY26 = 0
    Y.AMOUNT.LCY7  = 0  ; Y.AMOUNT.LCY17 = 0  ; Y.AMOUNT.LCY27 = 0
    Y.AMOUNT.LCY8  = 0  ; Y.AMOUNT.LCY18 = 0  ; Y.AMOUNT.LCY28 = 0
    Y.AMOUNT.LCY9  = 0  ; Y.AMOUNT.LCY19 = 0  ; Y.AMOUNT.LCY29 = 0
    Y.AMOUNT.LCY10 = 0  ; Y.AMOUNT.LCY20 = 0  ; Y.AMOUNT.LCY30 = 0
    Y.AMOUNT.LCY31 = 0  ; Y.AMOUNT.LCY35 = 0  ; Y.AMOUNT.LCY39 = 0
    Y.AMOUNT.LCY32 = 0  ; Y.AMOUNT.LCY36 = 0  ; Y.AMOUNT.LCY40 = 0
    Y.AMOUNT.LCY33 = 0  ; Y.AMOUNT.LCY37 = 0  ; Y.AMOUNT.LCY41 = 0
    Y.AMOUNT.LCY34 = 0  ; Y.AMOUNT.LCY38 = 0  ; Y.AMOUNT.LCY42 = 0
    Y.AMOUNT.LCY43 = 0  ; Y.AMOUNT.LCY51 = 0  ; Y.AMOUNT.LCY59 = 0
    Y.AMOUNT.LCY44 = 0  ; Y.AMOUNT.LCY52 = 0  ; Y.AMOUNT.LCY60 = 0
    Y.AMOUNT.LCY45 = 0  ; Y.AMOUNT.LCY53 = 0  ; Y.AMOUNT.LCY61 = 0
    Y.AMOUNT.LCY46 = 0  ; Y.AMOUNT.LCY54 = 0  ; Y.AMOUNT.LCY62 = 0
    Y.AMOUNT.LCY47 = 0  ; Y.AMOUNT.LCY55 = 0  ; Y.AMOUNT.LCY63 = 0
    Y.AMOUNT.LCY48 = 0  ; Y.AMOUNT.LCY56 = 0  ; Y.AMOUNT.LCY64 = 0
    Y.AMOUNT.LCY49 = 0  ; Y.AMOUNT.LCY57 = 0  ; Y.AMOUNT.LCY65 = 0
    Y.AMOUNT.LCY50 = 0  ; Y.AMOUNT.LCY58 = 0  ; Y.AMOUNT.LCY66 = 0
    Y.AMOUNT.LCY67 = 0  ; Y.AMOUNT.LCY71 = 0  ; Y.AMOUNT.LCY75 = 0
    Y.AMOUNT.LCY68 = 0  ; Y.AMOUNT.LCY72 = 0  ; Y.AMOUNT.LCY76 = 0
    Y.AMOUNT.LCY69 = 0  ; Y.AMOUNT.LCY73 = 0  ; Y.AMOUNT.LCY77 = 0
    Y.AMOUNT.LCY70 = 0  ; Y.AMOUNT.LCY74 = 0  ; Y.AMOUNT.LCY78 = 0
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
RETURN
*========================================================================
CALLDB:
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*************************************************************************
    T.SEL = "SELECT ":FN.CBE: " WITH CBEM.CY EQ 'EGP' AND CBEM.CATEG IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 1001 1002 1003 6501 6502 6503 6504 1012 1013 1015 1016 3005 3010 3011 3012 3013) AND CBEM.BR EQ ":COMP.BR
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    K1=0  ; K2=0  ; K3=0  ; K4=0  ; K5=0  ; K6=0
    K7=0  ; K8=0  ; K9=0  ; K10=0 ; K11=0 ; K12=0
    K13=0 ; K14=0 ; K15=0 ; K16=0 ; K17=0 ; K18=0
    K19=0 ; K20=0 ; K21=0 ; K22=0 ; K23=0 ; K24=0
    K25=0 ; K26=0 ; K27=0 ; K28=0 ; K29=0 ; K30=0
    K31=0 ; K32=0 ; K33=0 ; K34=0 ; K35=0 ; K36=0
    K37=0 ; K38=0 ; K39=0 ; K40=0 ; K41=0 ; K42=0
    K43=0 ; K44=0 ; K45=0 ; K46=0 ; K47=0 ; K48=0
    K49=0 ; K50=0 ; K51=0 ; K52=0 ; K53=0 ; K54=0
    K55=0 ; K56=0 ; K57=0 ; K58=0 ; K59=0 ; K60=0
    K61=0 ; K62=0 ; K63=0 ; K64=0 ; K65=0 ; K66=0
    K67=0 ; K68=0 ; K69=0 ; K70=0 ; K71=0 ; K72=0
    K73=0 ; K74=0 ; K75=0 ; K76=0 ; K77=0 ; K78=0
    IF SELECTED THEN
        FOR X = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<X>,R.CBE,F.CBE,E4)
            Y.AMOUNT = R.CBE<C.CBEM.IN.LCY>
            Y.SECTOR3 = R.CBE<C.CBEM.NEW.SECTOR>
            SECTOR.ID3 = Y.SECTOR3[2,3]
*************************************************************************
            IF SECTOR.ID3 EQ 110 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY1 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY1 = DROUND(Y.AMOUNT.LCY1,'2')
                    K1++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY2 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY2 = DROUND(Y.AMOUNT.LCY2,'2')
                    K2++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY3 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY3 = DROUND(Y.AMOUNT.LCY3,'2')
                    K3++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY4 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY4 = DROUND(Y.AMOUNT.LCY4,'2')
                    K4++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY5 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY5 = DROUND(Y.AMOUNT.LCY5,'2')
                    K5++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY6 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY6 = DROUND(Y.AMOUNT.LCY6,'2')
                    K6++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 130 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY7 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY7 = DROUND(Y.AMOUNT.LCY7,'2')
                    K7++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY8 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY8 = DROUND(Y.AMOUNT.LCY8,'2')
                    K8++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY9 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY9 = DROUND(Y.AMOUNT.LCY9,'2')
                    K9++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY10 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY10 = DROUND(Y.AMOUNT.LCY10,'2')
                    K10++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY11 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY11 = DROUND(Y.AMOUNT.LCY11,'2')
                    K11++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY12 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY12 = DROUND(Y.AMOUNT.LCY12,'2')
                    K12++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 210 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY13 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY13 = DROUND(Y.AMOUNT.LCY13,'2')
                    K13++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY14 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY14 = DROUND(Y.AMOUNT.LCY14,'2')
                    K14++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY15 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY15 = DROUND(Y.AMOUNT.LCY15,'2')
                    K15++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY16 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY16 = DROUND(Y.AMOUNT.LCY16,'2')
                    K16++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY17 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY17 = DROUND(Y.AMOUNT.LCY17,'2')
                    K17++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY18 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY18 = DROUND(Y.AMOUNT.LCY18,'2')
                    K18++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 220 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY19 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY19 = DROUND(Y.AMOUNT.LCY19,'2')
                    K19++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY20 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY20 = DROUND(Y.AMOUNT.LCY20,'2')
                    K20++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY21 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY21 = DROUND(Y.AMOUNT.LCY21,'2')
                    K21++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY22 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY22 = DROUND(Y.AMOUNT.LCY22,'2')
                    K22++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY23 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY23 = DROUND(Y.AMOUNT.LCY23,'2')
                    K23++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY24 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY24 = DROUND(Y.AMOUNT.LCY24,'2')
                    K24++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 230 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY25 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY25 = DROUND(Y.AMOUNT.LCY25,'2')
                    K25++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY26 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY26 = DROUND(Y.AMOUNT.LCY26,'2')
                    K26++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY27 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY27 = DROUND(Y.AMOUNT.LCY27,'2')
                    K27++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY28 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY28 = DROUND(Y.AMOUNT.LCY28,'2')
                    K28++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY29 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY29 = DROUND(Y.AMOUNT.LCY29,'2')
                    K29++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY30 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY30 = DROUND(Y.AMOUNT.LCY30,'2')
                    K30++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 240 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY31+= R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY31 = DROUND(Y.AMOUNT.LCY31,'2')
                    K31++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY32 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY32 = DROUND(Y.AMOUNT.LCY32,'2')
                    K32++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY33 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY33 = DROUND(Y.AMOUNT.LCY33,'2')
                    K33++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY34 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY34 = DROUND(Y.AMOUNT.LCY34,'2')
                    K34++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY35 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY35 = DROUND(Y.AMOUNT.LCY35,'2')
                    K35++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY36 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY36 = DROUND(Y.AMOUNT.LCY36,'2')
                    K36++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 250 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY37 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY37 = DROUND(Y.AMOUNT.LCY37,'2')
                    K37++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY38 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY38 = DROUND(Y.AMOUNT.LCY38,'2')
                    K38++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY39 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY39 = DROUND(Y.AMOUNT.LCY39,'2')
                    K39++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY40 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY40 = DROUND(Y.AMOUNT.LCY40,'2')
                    K40++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY41 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY41 = DROUND(Y.AMOUNT.LCY41,'2')
                    K41++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY42 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY42 = DROUND(Y.AMOUNT.LCY42,'2')
                    K42++
                END
            END
*************************************************************************
            IF SECTOR.ID3 GE 500 AND SECTOR.ID3 LE 600 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY43 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY43 = DROUND(Y.AMOUNT.LCY43,'2')
                    K43++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY44 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY44 = DROUND(Y.AMOUNT.LCY44,'2')
                    K44++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY45 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY45 = DROUND(Y.AMOUNT.LCY45,'2')
                    K45++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY46 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY46 = DROUND(Y.AMOUNT.LCY46,'2')
                    K46++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY47 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY47 = DROUND(Y.AMOUNT.LCY47,'2')
                    K47++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY48 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY48 = DROUND(Y.AMOUNT.LCY48,'2')
                    K48++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 650 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY49 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY49 = DROUND(Y.AMOUNT.LCY49,'2')
                    K49++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY50 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY50 = DROUND(Y.AMOUNT.LCY50,'2')
                    K50++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY51 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY51 = DROUND(Y.AMOUNT.LCY51,'2')
                    K51++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY52 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY52 = DROUND(Y.AMOUNT.LCY52,'2')
                    K52++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY53 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY53 = DROUND(Y.AMOUNT.LCY53,'2')
                    K53++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY54 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY54 = DROUND(Y.AMOUNT.LCY54,'2')
                    K54++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 700 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY55 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY55 = DROUND(Y.AMOUNT.LCY55,'2')
                    K55++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY56 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY56 = DROUND(Y.AMOUNT.LCY56,'2')
                    K56++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY57 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY57 = DROUND(Y.AMOUNT.LCY57,'2')
                    K57++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY58 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY58 = DROUND(Y.AMOUNT.LCY58,'2')
                    K58++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY59 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY59 = DROUND(Y.AMOUNT.LCY59,'2')
                    K59++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY60 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY60 = DROUND(Y.AMOUNT.LCY60,'2')
                    K60++
                END
            END
*************************************************************************
            IF SECTOR.ID3 EQ 750 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY61 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY61 = DROUND(Y.AMOUNT.LCY61,'2')
                    K61++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY62 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY62 = DROUND(Y.AMOUNT.LCY62,'2')
                    K62++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY63 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY63 = DROUND(Y.AMOUNT.LCY63,'2')
                    K63++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY64 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY64 = DROUND(Y.AMOUNT.LCY64,'2')
                    K64++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY65 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY65 = DROUND(Y.AMOUNT.LCY65,'2')
                    K65++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY66 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY66 = DROUND(Y.AMOUNT.LCY66,'2')
                    K66++
                END
            END
*************************************************************************
            IF Y.SECTOR3 EQ 7000 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY67 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY67 = DROUND(Y.AMOUNT.LCY67,'2')
                    K67++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY68 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY68 = DROUND(Y.AMOUNT.LCY68,'2')
                    K68++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY69 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY69 = DROUND(Y.AMOUNT.LCY69,'2')
                    K69++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY70 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY70 = DROUND(Y.AMOUNT.LCY70,'2')
                    K70++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY71 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY71 = DROUND(Y.AMOUNT.LCY71,'2')
                    K71++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY72 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY72 = DROUND(Y.AMOUNT.LCY72,'2')
                    K72++
                END
            END
*************************************************************************
            IF Y.SECTOR3 GE 8000 AND Y.SECTOR3 LE 8011 THEN
                IF Y.AMOUNT LT 5000 THEN
                    Y.AMOUNT.LCY73 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY73 = DROUND(Y.AMOUNT.LCY73,'2')
                    K73++
                END
                IF Y.AMOUNT GE 5000 AND Y.AMOUNT LT 25000 THEN
                    Y.AMOUNT.LCY74 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY74 = DROUND(Y.AMOUNT.LCY74,'2')
                    K74++
                END
                IF Y.AMOUNT GE 25000 AND Y.AMOUNT LT 100000 THEN
                    Y.AMOUNT.LCY75 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY75 = DROUND(Y.AMOUNT.LCY75,'2')
                    K75++
                END
                IF Y.AMOUNT GE 100000 AND Y.AMOUNT LT 500000 THEN
                    Y.AMOUNT.LCY76 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY76 = DROUND(Y.AMOUNT.LCY76,'2')
                    K76++
                END
                IF Y.AMOUNT GE 500000 AND Y.AMOUNT LT 1000000 THEN
                    Y.AMOUNT.LCY77 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY77 = DROUND(Y.AMOUNT.LCY77,'2')
                    K77++
                END
                IF Y.AMOUNT GE 1000000 THEN
                    Y.AMOUNT.LCY78 += R.CBE<C.CBEM.IN.LCY> / 1000
                    Y.AMOUNT.LCY78 = DROUND(Y.AMOUNT.LCY78,'2')
                    K78++
                END
            END
*************************************************************************
        NEXT X
        AMOUNT.TOTAL1  =  Y.AMOUNT.LCY1 + Y.AMOUNT.LCY2 + Y.AMOUNT.LCY3 + Y.AMOUNT.LCY4 + Y.AMOUNT.LCY5 + Y.AMOUNT.LCY6
        AMOUNT.TOTAL2  =  Y.AMOUNT.LCY7 + Y.AMOUNT.LCY8 + Y.AMOUNT.LCY9 + Y.AMOUNT.LCY10 + Y.AMOUNT.LCY11 + Y.AMOUNT.LCY12
        AMOUNT.TOTAL3  =  Y.AMOUNT.LCY13 + Y.AMOUNT.LCY14 + Y.AMOUNT.LCY15 + Y.AMOUNT.LCY16 + Y.AMOUNT.LCY17 + Y.AMOUNT.LCY18
        AMOUNT.TOTAL4  =  Y.AMOUNT.LCY19 + Y.AMOUNT.LCY20 + Y.AMOUNT.LCY21 + Y.AMOUNT.LCY22 + Y.AMOUNT.LCY23 + Y.AMOUNT.LCY24
        AMOUNT.TOTAL5  =  Y.AMOUNT.LCY25 + Y.AMOUNT.LCY26 + Y.AMOUNT.LCY27 + Y.AMOUNT.LCY28 + Y.AMOUNT.LCY29 + Y.AMOUNT.LCY30
        AMOUNT.TOTAL6  =  Y.AMOUNT.LCY31 + Y.AMOUNT.LCY32 + Y.AMOUNT.LCY33 + Y.AMOUNT.LCY34 + Y.AMOUNT.LCY35 + Y.AMOUNT.LCY36
        AMOUNT.TOTAL7  =  Y.AMOUNT.LCY37 + Y.AMOUNT.LCY38 + Y.AMOUNT.LCY39 + Y.AMOUNT.LCY40 + Y.AMOUNT.LCY41 + Y.AMOUNT.LCY42
        AMOUNT.TOTAL8  =  Y.AMOUNT.LCY43 + Y.AMOUNT.LCY44 + Y.AMOUNT.LCY45 + Y.AMOUNT.LCY46 + Y.AMOUNT.LCY47 + Y.AMOUNT.LCY48
        AMOUNT.TOTAL9  =  Y.AMOUNT.LCY49 + Y.AMOUNT.LCY50 + Y.AMOUNT.LCY51 + Y.AMOUNT.LCY52 + Y.AMOUNT.LCY53 + Y.AMOUNT.LCY54
        AMOUNT.TOTAL10 =  Y.AMOUNT.LCY55 + Y.AMOUNT.LCY56 + Y.AMOUNT.LCY57 + Y.AMOUNT.LCY58 + Y.AMOUNT.LCY59 + Y.AMOUNT.LCY60
        AMOUNT.TOTAL11 =  Y.AMOUNT.LCY61 + Y.AMOUNT.LCY62 + Y.AMOUNT.LCY63 + Y.AMOUNT.LCY64 + Y.AMOUNT.LCY65 + Y.AMOUNT.LCY66
        AMOUNT.TOTAL12 =  Y.AMOUNT.LCY67 + Y.AMOUNT.LCY68 + Y.AMOUNT.LCY69 + Y.AMOUNT.LCY70 + Y.AMOUNT.LCY71 + Y.AMOUNT.LCY72
        AMOUNT.TOTAL13 =  Y.AMOUNT.LCY73 + Y.AMOUNT.LCY74 + Y.AMOUNT.LCY75 + Y.AMOUNT.LCY76 + Y.AMOUNT.LCY77 + Y.AMOUNT.LCY78

        TOTAL.COUNT1  = K1  +  K2  + K3  +  K4  + K5  +  K6
        TOTAL.COUNT2  = K7  +  K8  + K9  +  K10 + K11 +  K12
        TOTAL.COUNT3  = K13 +  K14 + K15 +  K16 + K17 +  K18
        TOTAL.COUNT4  = K19 +  K20 + K21 +  K22 + K23 +  K24
        TOTAL.COUNT5  = K25 +  K26 + K27 +  K28 + K29 +  K30
        TOTAL.COUNT6  = K31 +  K32 + K33 +  K34 + K35 +  K36
        TOTAL.COUNT7  = K37 +  K38 + K39 +  K40 + K41 +  K42
        TOTAL.COUNT8  = K43 +  K44 + K45 +  K46 + K47 +  K48
        TOTAL.COUNT9  = K49 +  K50 + K51 +  K52 + K53 +  K54
        TOTAL.COUNT10 = K55 +  K56 + K57 +  K58 + K59 +  K60
        TOTAL.COUNT11 = K61 +  K62 + K63 +  K64 + K65 +  K66
        TOTAL.COUNT12 = K67 +  K68 + K69 +  K70 + K71 +  K72
        TOTAL.COUNT13 = K73 +  K74 + K75 +  K76 + K77 +  K78
*-------------------------------------------------------------
        AMT.TOTAL1  =  Y.AMOUNT.LCY1 + Y.AMOUNT.LCY7  + Y.AMOUNT.LCY13 + Y.AMOUNT.LCY19 + Y.AMOUNT.LCY25 + Y.AMOUNT.LCY31  + Y.AMOUNT.LCY37 + Y.AMOUNT.LCY43 + Y.AMOUNT.LCY49 + Y.AMOUNT.LCY55 + Y.AMOUNT.LCY61 + Y.AMOUNT.LCY67 + Y.AMOUNT.LCY73
        AMT.TOTAL2  =  Y.AMOUNT.LCY2 + Y.AMOUNT.LCY8  + Y.AMOUNT.LCY14 + Y.AMOUNT.LCY20 + Y.AMOUNT.LCY26 + Y.AMOUNT.LCY32  + Y.AMOUNT.LCY38 + Y.AMOUNT.LCY44 + Y.AMOUNT.LCY50 + Y.AMOUNT.LCY56 + Y.AMOUNT.LCY62 + Y.AMOUNT.LCY68 + Y.AMOUNT.LCY74
        AMT.TOTAL3  =  Y.AMOUNT.LCY3 + Y.AMOUNT.LCY9  + Y.AMOUNT.LCY15 + Y.AMOUNT.LCY21 + Y.AMOUNT.LCY27 + Y.AMOUNT.LCY33  + Y.AMOUNT.LCY39 + Y.AMOUNT.LCY45 + Y.AMOUNT.LCY51 + Y.AMOUNT.LCY57 + Y.AMOUNT.LCY63 + Y.AMOUNT.LCY69 + Y.AMOUNT.LCY75
        AMT.TOTAL4  =  Y.AMOUNT.LCY4 + Y.AMOUNT.LCY10 + Y.AMOUNT.LCY16 + Y.AMOUNT.LCY22 + Y.AMOUNT.LCY28 + Y.AMOUNT.LCY34  + Y.AMOUNT.LCY40 + Y.AMOUNT.LCY46 + Y.AMOUNT.LCY52 + Y.AMOUNT.LCY58 + Y.AMOUNT.LCY64 + Y.AMOUNT.LCY70 + Y.AMOUNT.LCY76
        AMT.TOTAL5  =  Y.AMOUNT.LCY5 + Y.AMOUNT.LCY11 + Y.AMOUNT.LCY17 + Y.AMOUNT.LCY23 + Y.AMOUNT.LCY29 + Y.AMOUNT.LCY35  + Y.AMOUNT.LCY41 + Y.AMOUNT.LCY47 + Y.AMOUNT.LCY53 + Y.AMOUNT.LCY59 + Y.AMOUNT.LCY65 + Y.AMOUNT.LCY71 + Y.AMOUNT.LCY77
        AMT.TOTAL6  =  Y.AMOUNT.LCY6 + Y.AMOUNT.LCY12 + Y.AMOUNT.LCY18 + Y.AMOUNT.LCY24 + Y.AMOUNT.LCY30 + Y.AMOUNT.LCY36  + Y.AMOUNT.LCY42 + Y.AMOUNT.LCY48 + Y.AMOUNT.LCY54 + Y.AMOUNT.LCY60 + Y.AMOUNT.LCY66 + Y.AMOUNT.LCY72 + Y.AMOUNT.LCY78
        AMT.TOTAL7  =  AMOUNT.TOTAL1 + AMOUNT.TOTAL2 + AMOUNT.TOTAL3 + AMOUNT.TOTAL4 + AMOUNT.TOTAL5 + AMOUNT.TOTAL6 + AMOUNT.TOTAL7 + AMOUNT.TOTAL8 + AMOUNT.TOTAL9 + AMOUNT.TOTAL10 + AMOUNT.TOTAL11 + AMOUNT.TOTAL12 + AMOUNT.TOTAL13

        TOT.COUNT1  = K1  +  K7  + K13  +  K19  + K25  +  K31 + K37 + K43 + K49 + K55 + K61 + K67 + K73
        TOT.COUNT2  = K2  +  K8  + K14  +  K20  + K26  +  K32 + K38 + K44 + K50 + K56 + K62 + K68 + K74
        TOT.COUNT3  = K3  +  K9  + K15  +  K21  + K27  +  K33 + K39 + K45 + K51 + K57 + K63 + K69 + K75
        TOT.COUNT4  = K4  +  K10 + K16  +  K22  + K28  +  K34 + K40 + K46 + K52 + K58 + K64 + K70 + K76
        TOT.COUNT5  = K5  +  K11 + K17  +  K23  + K29  +  K35 + K41 + K47 + K53 + K59 + K65 + K71 + K77
        TOT.COUNT6  = K6  +  K12 + K18  +  K24  + K30  +  K36 + K42 + K48 + K54 + K60 + K66 + K72 + K78
        TOT.COUNT7  = TOTAL.COUNT1 + TOTAL.COUNT2 + TOTAL.COUNT3 + TOTAL.COUNT4 + TOTAL.COUNT5 + TOTAL.COUNT6 + TOTAL.COUNT7 + TOTAL.COUNT8 + TOTAL.COUNT9 + TOTAL.COUNT10 + TOTAL.COUNT11 + TOTAL.COUNT12 + TOTAL.COUNT13
*-------------------------------------------------------------
        XX1 = SPACE(120)
        XX1<1,1>[1,15]   = "������� ������"
        XX1<1,1>[20,6]   = K1
        XX1<1,1>[25,15]  = Y.AMOUNT.LCY1
        XX1<1,1>[35,15]  = K2
        XX1<1,1>[40,15]  = Y.AMOUNT.LCY2
        XX1<1,1>[50,15]  = K3
        XX1<1,1>[55,15]  = Y.AMOUNT.LCY3
        XX1<1,1>[65,15]  = K4
        XX1<1,1>[70,15]  = Y.AMOUNT.LCY4
        XX1<1,1>[80,15]  = K5
        XX1<1,1>[85,15]  = Y.AMOUNT.LCY5
        XX1<1,1>[95,15]  = K6
        XX1<1,1>[100,15] = Y.AMOUNT.LCY6
        XX1<1,1>[110,15] = TOTAL.COUNT1
        XX1<1,1>[115,15] = AMOUNT.TOTAL1

        PRINT XX1<1,1>
        PRINT STR('-',130)
*------------------------------------------------
        XX2 = SPACE(120)
        XX2<1,1>[1,15]   = "����� ������ �����"
        XX2<1,1>[20,6]   = K7
        XX2<1,1>[25,15]  = Y.AMOUNT.LCY7
        XX2<1,1>[35,15]  = K8
        XX2<1,1>[40,15]  = Y.AMOUNT.LCY8
        XX2<1,1>[50,15]  = K9
        XX2<1,1>[55,15]  = Y.AMOUNT.LCY9
        XX2<1,1>[65,15]  = K10
        XX2<1,1>[70,15]  = Y.AMOUNT.LCY10
        XX2<1,1>[80,15]  = K11
        XX2<1,1>[85,15]  = Y.AMOUNT.LCY11
        XX2<1,1>[95,15]  = K12
        XX2<1,1>[100,15] = Y.AMOUNT.LCY12
        XX2<1,1>[110,15] = TOTAL.COUNT2
        XX2<1,1>[115,15] = AMOUNT.TOTAL2

        PRINT XX2<1,1>
        PRINT STR('-',130)
*--------------------------------------------------
        XX3 = SPACE(120)
        XX3<1,1>[1,15]   = "����� �������"
        XX3<1,1>[20,6]   = K13
        XX3<1,1>[25,15]  = Y.AMOUNT.LCY13
        XX3<1,1>[35,15]  = K14
        XX3<1,1>[40,15]  = Y.AMOUNT.LCY14
        XX3<1,1>[50,15]  = K15
        XX3<1,1>[55,15]  = Y.AMOUNT.LCY15
        XX3<1,1>[65,15]  = K16
        XX3<1,1>[70,15]  = Y.AMOUNT.LCY16
        XX3<1,1>[80,15]  = K17
        XX3<1,1>[85,15]  = Y.AMOUNT.LCY17
        XX3<1,1>[95,15]  = K18
        XX3<1,1>[100,15] = Y.AMOUNT.LCY18
        XX3<1,1>[110,15] = TOTAL.COUNT3
        XX3<1,1>[115,15] = AMOUNT.TOTAL3
        PRINT XX3<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX4 = SPACE(120)
        XX4<1,1>[1,15]   = "����� ����� ����"
        XX4<1,1>[20,6]   = K19
        XX4<1,1>[25,15]  = Y.AMOUNT.LCY19
        XX4<1,1>[35,15]  = K20
        XX4<1,1>[40,15]  = Y.AMOUNT.LCY20
        XX4<1,1>[50,15]  = K21
        XX4<1,1>[55,15]  = Y.AMOUNT.LCY21
        XX4<1,1>[65,15]  = K22
        XX4<1,1>[70,15]  = Y.AMOUNT.LCY22
        XX4<1,1>[80,15]  = K23
        XX4<1,1>[85,15]  = Y.AMOUNT.LCY23
        XX4<1,1>[95,15]  = K24
        XX4<1,1>[100,15] = Y.AMOUNT.LCY24
        XX4<1,1>[110,15] = TOTAL.COUNT4
        XX4<1,1>[115,15] = AMOUNT.TOTAL4
        PRINT XX4<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX5 = SPACE(120)
        XX5<1,1>[1,15]   = "����� �������"
        XX5<1,1>[20,6]   = K25
        XX5<1,1>[25,15]  = Y.AMOUNT.LCY25
        XX5<1,1>[35,15]  = K26
        XX5<1,1>[40,15]  = Y.AMOUNT.LCY26
        XX5<1,1>[50,15]  = K27
        XX5<1,1>[55,15]  = Y.AMOUNT.LCY27
        XX5<1,1>[65,15]  = K28
        XX5<1,1>[70,15]  = Y.AMOUNT.LCY28
        XX5<1,1>[80,15]  = K29
        XX5<1,1>[85,15]  = Y.AMOUNT.LCY29
        XX5<1,1>[95,15]  = K30
        XX5<1,1>[100,15] = Y.AMOUNT.LCY30
        XX5<1,1>[110,15] = TOTAL.COUNT5
        XX5<1,1>[115,15] = AMOUNT.TOTAL5

        PRINT XX5<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX6 = SPACE(120)
        XX6<1,1>[1,15]   = "�������� ���������"
        XX6<1,1>[20,6]   = K31
        XX6<1,1>[25,15]  = Y.AMOUNT.LCY31
        XX6<1,1>[35,15]  = K32
        XX6<1,1>[40,15]  = Y.AMOUNT.LCY32
        XX6<1,1>[50,15]  = K33
        XX6<1,1>[55,15]  = Y.AMOUNT.LCY33
        XX6<1,1>[65,15]  = K34
        XX6<1,1>[70,15]  = Y.AMOUNT.LCY34
        XX6<1,1>[80,15]  = K35
        XX6<1,1>[85,15]  = Y.AMOUNT.LCY35
        XX6<1,1>[95,15]  = K36
        XX6<1,1>[100,15] = Y.AMOUNT.LCY36
        XX6<1,1>[110,15] = TOTAL.COUNT6
        XX6<1,1>[115,15] = AMOUNT.TOTAL6
        PRINT XX6<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX7 = SPACE(120)
        XX7<1,1>[1,15]   = "����� �����"
        XX7<1,1>[20,6]   = K37
        XX7<1,1>[25,15]  = Y.AMOUNT.LCY37
        XX7<1,1>[35,15]  = K38
        XX7<1,1>[40,15]  = Y.AMOUNT.LCY38
        XX7<1,1>[50,15]  = K39
        XX7<1,1>[55,15]  = Y.AMOUNT.LCY39
        XX7<1,1>[65,15]  = K40
        XX7<1,1>[70,15]  = Y.AMOUNT.LCY40
        XX7<1,1>[80,15]  = K41
        XX7<1,1>[85,15]  = Y.AMOUNT.LCY41
        XX7<1,1>[95,15]  = K42
        XX7<1,1>[100,15] = Y.AMOUNT.LCY42
        XX7<1,1>[110,15] = TOTAL.COUNT7
        XX7<1,1>[115,15] = AMOUNT.TOTAL7
        PRINT XX7<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX8 = SPACE(120)
        XX8<1,1>[1,15]   = "���� ������� �����"
        XX8<1,1>[20,6]   = K43
        XX8<1,1>[25,15]  = Y.AMOUNT.LCY43
        XX8<1,1>[35,15]  = K44
        XX8<1,1>[40,15]  = Y.AMOUNT.LCY44
        XX8<1,1>[50,15]  = K45
        XX8<1,1>[55,15]  = Y.AMOUNT.LCY45
        XX8<1,1>[65,15]  = K46
        XX8<1,1>[70,15]  = Y.AMOUNT.LCY46
        XX8<1,1>[80,15]  = K47
        XX8<1,1>[85,15]  = Y.AMOUNT.LCY47
        XX8<1,1>[95,15]  = K48
        XX8<1,1>[100,15] = Y.AMOUNT.LCY48
        XX8<1,1>[110,15] = TOTAL.COUNT8
        XX8<1,1>[115,15] = AMOUNT.TOTAL8
        PRINT XX8<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX9 = SPACE(132)
        XX9<1,1>[1,15]   = "����� ������"
        XX9<1,1>[20,6]   = K49
        XX9<1,1>[25,15]  = Y.AMOUNT.LCY49
        XX9<1,1>[35,15]  = K50
        XX9<1,1>[40,15]  = Y.AMOUNT.LCY50
        XX9<1,1>[50,15]  = K51
        XX9<1,1>[55,15]  = Y.AMOUNT.LCY51
        XX9<1,1>[65,15]  = K52
        XX9<1,1>[70,15]  = Y.AMOUNT.LCY52
        XX9<1,1>[80,15]  = K53
        XX9<1,1>[85,15]  = Y.AMOUNT.LCY53
        XX9<1,1>[95,15]  = K54
        XX9<1,1>[100,15] = Y.AMOUNT.LCY54
        XX9<1,1>[110,15] = TOTAL.COUNT9
        XX9<1,1>[117,15] = AMOUNT.TOTAL9
        PRINT XX9<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX10 = SPACE(120)
        XX10<1,1>[1,15]   = "����� �����"
        XX10<1,1>[20,6]   = K55
        XX10<1,1>[25,15]  = Y.AMOUNT.LCY55
        XX10<1,1>[35,15]  = K56
        XX10<1,1>[40,15]  = Y.AMOUNT.LCY56
        XX10<1,1>[50,15]  = K57
        XX10<1,1>[55,15]  = Y.AMOUNT.LCY57
        XX10<1,1>[65,15]  = K58
        XX10<1,1>[70,15]  = Y.AMOUNT.LCY58
        XX10<1,1>[80,15]  = K59
        XX10<1,1>[85,15]  = Y.AMOUNT.LCY59
        XX10<1,1>[95,15]  = K60
        XX10<1,1>[100,15] = Y.AMOUNT.LCY60
        XX10<1,1>[110,15] = TOTAL.COUNT10
        XX10<1,1>[115,15] = AMOUNT.TOTAL10
        PRINT XX10<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX11 = SPACE(120)
        XX11<1,1>[1,15]   = "����� ������"
        XX11<1,1>[20,6]   = K61
        XX11<1,1>[25,15]  = Y.AMOUNT.LCY61
        XX11<1,1>[35,15]  = K62
        XX11<1,1>[40,15]  = Y.AMOUNT.LCY62
        XX11<1,1>[50,15]  = K63
        XX11<1,1>[55,15]  = Y.AMOUNT.LCY63
        XX11<1,1>[65,15]  = K64
        XX11<1,1>[70,15]  = Y.AMOUNT.LCY64
        XX11<1,1>[80,15]  = K65
        XX11<1,1>[85,15]  = Y.AMOUNT.LCY65
        XX11<1,1>[95,15]  = K66
        XX11<1,1>[100,15] = Y.AMOUNT.LCY66
        XX11<1,1>[110,15] = TOTAL.COUNT11
        XX11<1,1>[115,15] = AMOUNT.TOTAL11

        PRINT XX11<1,1>
        PRINT STR('-',130)
*------------------------------------------------
        XX12 = SPACE(120)
        XX12<1,1>[1,15]   = "�����"
        XX12<1,1>[20,6]   = K67
        XX12<1,1>[25,15]  = Y.AMOUNT.LCY67
        XX12<1,1>[35,15]  = K68
        XX12<1,1>[40,15]  = Y.AMOUNT.LCY68
        XX12<1,1>[50,15]  = K69
        XX12<1,1>[55,15]  = Y.AMOUNT.LCY69
        XX12<1,1>[65,15]  = K70
        XX12<1,1>[70,15]  = Y.AMOUNT.LCY70
        XX12<1,1>[80,15]  = K71
        XX12<1,1>[85,15]  = Y.AMOUNT.LCY71
        XX12<1,1>[95,15]  = K72
        XX12<1,1>[100,15] = Y.AMOUNT.LCY72
        XX12<1,1>[110,15] = TOTAL.COUNT12
        XX12<1,1>[115,15] = AMOUNT.TOTAL12
        PRINT XX12<1,1>
        PRINT STR('-',130)

*--------------------------------------------------
        XX13 = SPACE(120)
        XX13<1,1>[1,15]   = "������� ��������"
        XX13<1,1>[20,6]   = K73
        XX13<1,1>[25,15]  = Y.AMOUNT.LCY73
        XX13<1,1>[35,15]  = K74
        XX13<1,1>[40,15]  = Y.AMOUNT.LCY74
        XX13<1,1>[50,15]  = K75
        XX13<1,1>[55,15]  = Y.AMOUNT.LCY75
        XX13<1,1>[65,15]  = K76
        XX13<1,1>[70,15]  = Y.AMOUNT.LCY76
        XX13<1,1>[80,15]  = K77
        XX13<1,1>[85,15]  = Y.AMOUNT.LCY77
        XX13<1,1>[95,15]  = K78
        XX13<1,1>[100,15] = Y.AMOUNT.LCY78
        XX13<1,1>[110,15] = TOTAL.COUNT13
        XX13<1,1>[115,15] = AMOUNT.TOTAL13
        PRINT XX13<1,1>
        PRINT STR('=',130)

        XX14 = SPACE(120)
        XX14<1,1>[1,15]   = "��������������"
        XX14<1,1>[20,6]   = TOT.COUNT1
        XX14<1,1>[25,15]  = AMT.TOTAL1
        XX14<1,1>[35,15]  = TOT.COUNT2
        XX14<1,1>[40,15]  = AMT.TOTAL2
        XX14<1,1>[50,15]  = TOT.COUNT3
        XX14<1,1>[55,15]  = AMT.TOTAL3
        XX14<1,1>[65,15]  = TOT.COUNT4
        XX14<1,1>[70,15]  = AMT.TOTAL4
        XX14<1,1>[80,15]  = TOT.COUNT5
        XX14<1,1>[85,15]  = AMT.TOTAL5
        XX14<1,1>[95,15]  = TOT.COUNT6
        XX14<1,1>[100,15] = AMT.TOTAL6
        XX14<1,1>[110,15] = TOT.COUNT7
        XX14<1,1>[117,15] = AMT.TOTAL7
        PRINT XX14<1,1>
        PRINT STR('=',130)
    END
*************************************************************************
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 868 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������ ����� ������� �� : ":T.DAY:SPACE(20):"������ ������ ����"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��������������":SPACE(6):"��� �� 5000":SPACE(6):"�� 5000":SPACE(6):"�� 25000":SPACE(6):"�� 100000":SPACE(6):"�� 500000":SPACE(6):"���� ��":SPACE(8):"�������"
    PR.HD :="'L'":"              ":SPACE(6):"           ":SPACE(4):"��� 25000":SPACE(5):"��� 100000":SPACE(5):"��� 500000":SPACE(5):"��� 1000000":SPACE(5):"1000000"
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":SPACE(20):"���":SPACE(3):"����":SPACE(5):"���":SPACE(3):"����":SPACE(5):"���":SPACE(3):"����":SPACE(5):"���":SPACE(3):"����":SPACE(5):"���":SPACE(3):"����":SPACE(5):"���":SPACE(3):"����":SPACE(5):"���":SPACE(3):"����"
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
