* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.HR.COST.OFS.SCSF

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HR.COST.CENTER
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ������  '
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN

        GOSUB CHECKEXIST
        IF SW2 = 0 THEN

            GOSUB INITIALISE
            GOSUB PROCESS
        END
    END ELSE
        TEXT = '�� ������ �� ��������' ; CALL REM
    END


    RETURN
*-------------------------------------------
CHECKEXIST:
    KEY.LIST="" ; SELECTED="" ;  ER.FT="" ; SW2 = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH DEBIT.THEIR.REF EQ 'SCSF'"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    RETURN

*-------------------------------------------
INITIALISE:
    OPENSEQ "MECH" , "HR.COST.SCSF" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"HR.COST.SCSF"
        HUSH OFF
    END
    OPENSEQ "MECH" , "HR.COST.SCSF" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE HR.COST.SCSF CREATED IN MECH'
        END ELSE
            STOP 'Cannot create HR.COST.SCSF File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
PROCESS:

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.SH = 'F.SCB.HR.COST.CENTER' ; F.SH = ''
    CALL OPF(FN.SH,F.SH)

    V.DATE = TODAY

    T.SEL = "SELECT ":FN.SH:" WITH FILE.SOURCE EQ 'SCSF'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.SH,KEY.LIST<I>,R.SH,F.SH,E1)
            CUST.ID = R.SH<SHC.CUSTOMER.ID>

            DB.ACCT = 'PL60062'

            AMT     = R.SH<SHC.AMOUNT>
            WS.DEPT = R.SH<SHC.DEP.CODE>
            WS.BRN  = R.SH<SHC.BRANCH>
            CR.ACCT = 'EGP1641000010099'

            IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//EG00100':WS.BRN:','

            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":','
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":'EGP':','
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":'EGP':','
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:','
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:','
            OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:','
            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:','
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:','
            OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":'SCSF':','
            OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":CUST.ID:','
            OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
            OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":','
            OFS.MESSAGE.DATA :=  "PROFIT.CENTRE.DEPT=":WS.DEPT:','
            OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB':','


            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN HR.COST.SCSF'
    EXECUTE 'DELETE ':"MECH":' ':"HR.COST.SCSF"

    TEXT = 'DONE' ; CALL REM

    RETURN
END
