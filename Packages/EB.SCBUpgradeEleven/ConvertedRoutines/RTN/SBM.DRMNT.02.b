* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNTQ1MDI6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-220</Rating>
*-----------------------------------------------------------------------------
    PROGRAM    SBM.DRMNT.02
***    SUBROUTINE SBM.DRMNT.02

*   ------------------------------
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.POS.TODAY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


*                                                  ����� �� ��������
*               ����� ������� ���� �� ����� �������� �� ��� �����
*                             ����� ������� ���� �� ����� �������
*                                 ���� ��� ������ ����� �������


    GOSUB INIT

    WS.COMP = ID.COMPANY
***    WS.COMP = 'EG0010001'



    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')


    GOSUB PRINT.HEAD

    GOSUB OPENFILES

*-------------------------------
***    GOSUB READ.CUSTOMER.FILE
    GOSUB READ.CUST.POS.FILE

*-------------------------------
    XX = SPACE(120)
    PRINT XX<1,1>
    XX = STR('_',120)
    PRINT XX<1,1>

    XX = SPACE(120)
    PRINT XX<1,1>

    XX<1,1>[1,50]   = "**** ������ ��� �������  ****"
    XX<1,1>[70,20]  = FMT(NO.DRMNT.CUST,"R0,")
    PRINT XX<1,1>

    XX = SPACE(120)
    PRINT XX<1,1>
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)
*-------------------------------


    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')



    RETURN
*-------------------------------------------------------------------------
INIT:


    PROGRAM.ID = "SBM.DRMNT.02"

    NO.DRMNT.CUST = 0

    FN.CUS.POS = "F.SCB.CUST.POS.TODAY"
    F.CUS.POS  = ""
    R.CUS.POS  = ""
    Y.CUS.POS.ID   = ""

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC  = ""
    R.CUS.ACC = ""
    Y.CUS.ACC.ID   = ""

    FN.CUSTOMER = "FBNK.CUSTOMER"
    F.CUSTOMER  = ""
    R.CUSTOMER  = ""
    Y.CUST.ID   = ""


    FN.ACC = "FBNK.ACCOUNT"
    F.ACC  = ""
    R.ACCOUNT = ""
    Y.ACC.ID = ""

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD  = ""
    R.LD = ""
    Y.LD.ID = ""

    FN.LC = "FBNK.LETTER.OF.CREDIT"
    F.LC  = ""
    R.LC = ""
    Y.LC.ID = ""

    OLD.CUST = 0
    CUST.NAME = ''
    NO.OF.LD   = ''
    NO.OF.LC   = ''
*---------------------------------------
    SYS.DATE = TODAY
    SYS.YYMM = SYS.DATE[1,6]

    WRK.DATE = SYS.DATE

*---------------------------------------
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]
*    WRK.MM   = WRK.MM + 3
    WRK.MM   = WRK.MM + 2



    IF WRK.MM GT 12 THEN
        WRK.YY = WRK.YY + 1
        WRK.MM = WRK.MM - 12
    END


    WRK.DD   = 01

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")

    WRK.DATE = WRK.YY:WRK.MM:WRK.DD

    CALL CDT('',WRK.DATE,'-1C')
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]

*    PRINT WRK.DATE

*---------------------------------------

    OLD1.YY   = WRK.DATE[1,4] - 1
    OLD1.MM   = WRK.DATE[5,2]
    OLD1.DD   = WRK.DATE[7,2]
    OLD1.DATE = OLD1.YY:OLD1.MM:OLD1.DD
    OLD1.YYMM = OLD1.DATE[1,6]
*---------------------------------------
    OLD3.YY   = WRK.DATE[1,4] - 3
    OLD3.MM   = WRK.DATE[5,2]
    OLD3.DD   = WRK.DATE[7,2]
    OLD3.DATE = OLD3.YY:OLD3.MM:OLD3.DD
    OLD3.YYMM = OLD3.DATE[1,6]
*---------------------------------------
    P.DATE   = FMT(SYS.DATE,"####/##/##")

*   PRINT OLD1.DATE:" ":OLD3.DATE
*---------------------------------------
    DIM ARY.X(12)
    ARY.X(1) = "������"
    ARY.X(2) = "������"
    ARY.X(3) = "������"
    ARY.X(4) = "������"
    ARY.X(5) = "������"
    ARY.X(6) = "������"
    ARY.X(7) = "������"
    ARY.X(8) = "������"
    ARY.X(9) = "������"
    ARY.X(10) = "������"
    ARY.X(11) = "������"
    ARY.X(12) = "������"
    MON = ARY.X(WRK.MM)

*-----------------------
    LINE.NO = 0
    MAX.LINE.NO = 30
*-----------------------
    WS.POSTING.RESTRICT = 90
*-----------------------



    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)

    RETURN
*--------------------------------------------------------------------------
READ.CUST.POS.FILE:

    SEL.CUS.POS = "SELECT ":FN.CUS.POS:" WITH"
    SEL.CUS.POS :=" CO.CODE EQ ":WS.COMP
    SEL.CUS.POS :=" AND @ID LIKE ...":SYS.DATE
    SEL.CUS.POS :=" AND AC.CATEG EQ 1019"
    SEL.CUS.POS :=" BY @ID"



    CALL EB.READLIST(SEL.CUS.POS,SEL.LIST.CUS.POS,'',NO.OF.CUS.POS,ERR.CUS.POS)
    LOOP
        REMOVE Y.CUS.POS.ID FROM SEL.LIST.CUS.POS SETTING POS.CUS.POS
    WHILE Y.CUS.POS.ID:POS.CUS.POS
        CALL F.READ(FN.CUS.POS,Y.CUS.POS.ID,R.CUS.POS,F.CUS.POS,ERR.CUS.POS)



        Y.CUST.ID = FIELD(Y.CUS.POS.ID,"-",1)


        PRINT.CUST.NAME = 0


*Line [ 263 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
        OLD.DRMNT.CODE = LOCAL.REF<1,CULR.DRMNT.CODE>
        OLD.DRMNT.DATE = LOCAL.REF<1,CULR.DRMNT.DATE>


*------------------------------
        IF OLD.DRMNT.CODE GT 0 THEN
            GO TO NEXT.CUSTOMER
        END
*------------------------------


        Y.CUS.ACC.ID = Y.CUST.ID

        NO.DRMNT.CUST += 1



**                 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**         &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
***             R.CUSTOMER = ''
***             CALL F.READ(FN.CUSTOMER, Y.CUST.ID, R.CUSTOMER, F.CUSTOMER, E1)
***             R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.DRMNT.CODE>= 'YES'
***             R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.DRMNT.DATE>= TODAY
***             CALL F.WRITE(FN.CUSTOMER,Y.CUST.ID, R.CUSTOMER)
***             CALL JOURNAL.UPDATE(Y.CUST.ID)
***             R.CUSTOMER = ''
**                                         GO TO NEXT.CUSTOMER
** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**         &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**                 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&





        GOSUB READ.CUSTOMER.ACCOUNT.2:

*       **************************************
*-----------

NEXT.CUSTOMER:


*---------------
    REPEAT
    RETURN
*--------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT.2:

    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)

    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)

*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>
        Y.CUST.ID = Y.CUS.ACC.ID


*---------------
        GOSUB READ.ACCOUNT.FILE.2
*---------------
    NEXT Z


    GOSUB READ.LD.FILE.2





****************
    RETURN
*-------------------------------------------------------------------------
READ.ACCOUNT.FILE.2:
    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACCOUNT,F.ACC,ERR.ACC)

    Y.CATEG.ID   = R.ACCOUNT<AC.CATEGORY>
*-----------
    OPENING.DATE    = R.ACCOUNT<AC.OPENING.DATE>

    CUST.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.CUST>
    CUST.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.CUST>
    BANK.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.BANK>
    BANK.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.BANK>
    AUTO.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.AUTO>
    AUTO.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.AUTO>

    W.BAL           = R.ACCOUNT<AC.WORKING.BALANCE>
    WS.CY           = R.ACCOUNT<AC.CURRENCY>

*-------------------

    LAST.DATE = ''

    LAST.DATE = OPENING.DATE


    IF CUST.DATE.CR GT LAST.DATE THEN
        LAST.DATE = CUST.DATE.CR
    END

    IF CUST.DATE.DR GT LAST.DATE THEN
        LAST.DATE   = CUST.DATE.DR
    END


*-----------

*Line [ 381 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,Y.CATEG.ID,S.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,Y.CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
S.NAME=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
    CATEG.NAME = S.NAME

*Line [ 390 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,WS.CY,CY.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,WS.CY,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CY.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    CURR.NAME = CY.NAME

*-----------
    GOSUB PRINT.REPORT
*-------------------------------
    RETURN
*-------------------------------------------------------------------------
PRINT.REPORT:
    IF PRINT.CUST.NAME EQ 0 THEN
        GOSUB WRITE.ARY.1
    END


    GOSUB WRITE.ARY.2

    RETURN
*------------------------------------------------------------------------
WRITE.ARY.1:

    IF LINE.NO GT MAX.LINE.NO THEN
        GOSUB PRINT.HEAD
    END

    XX = STR('_',120)
    PRINT XX<1,1>

    XX = SPACE(120)
*------------
*------------

    XX<1,1>[1,10]   = Y.CUST.ID
    XX<1,1>[12,40]  = CUST.NAME

    PRINT XX<1,1>

    LINE.NO = +1

    PRINT.CUST.NAME = 1

    RETURN
*------------------------------------------------------------------------
WRITE.ARY.2:

    IF LINE.NO GT MAX.LINE.NO THEN
        GOSUB PRINT.HEAD
    END


    XX = SPACE(120)
    XX<1,1>[1,20]   = Y.ACC.ID
    XX<1,1>[33,6]   = Y.CATEG.ID
    XX<1,1>[40,12]  = CATEG.NAME
    XX<1,1>[70,3]   = WS.CY
    XX<1,1>[75,12]  = CURR.NAME


    XX<1,1>[90,10]  = FMT(LAST.DATE,"####/##/##")


*   XX<1,1>[90,15]  = W.BAL
    XX<1,1>[105,20] = FMT(W.BAL,"R2,")

    PRINT XX<1,1>

    LINE.NO = +1

    RETURN
*------------------------------------------------------------------------
READ.LD.FILE.2:

    NO.OF.LD = ''

    SEL.LD  = "SELECT ":FN.LD:" WITH CUSTOMER.ID EQ ":Y.CUST.ID
    SEL.LD := " AND AMOUNT GT 0"

    CALL EB.READLIST(SEL.LD,SEL.LIST.LD,'',NO.OF.LD,ERR.LD)


    LOOP
        REMOVE Y.LD.ID FROM SEL.LIST.LD SETTING POS.LD
    WHILE Y.LD.ID:POS.LD
        CALL F.READ(FN.LD,Y.LD.ID,R.LD,F.LD,ERR.LD)

        LD.CONT.NO = Y.LD.ID
        LD.CATEG   = R.LD<LD.CATEGORY>
        LD.CURR    = R.LD<LD.CURRENCY>
        LD.AMT     = R.LD<LD.AMOUNT>
        LD.LAST.D  = R.LD<LD.FIN.MAT.DATE>

****************
*Line [ 487 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,LD.CATEG,S.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,LD.CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
S.NAME=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
        CATEG.NAME = S.NAME
****************
*Line [ 496 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LD.CURR,CY.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,LD.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CY.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        CURR.NAME = CY.NAME
****************
        XX = SPACE(120)
        XX<1,1>[1,20]   = LD.CONT.NO
        XX<1,1>[33,6]   = LD.CATEG
        XX<1,1>[40,12]  = CATEG.NAME
        XX<1,1>[70,3]   = LD.CURR
        XX<1,1>[75,12]  = CURR.NAME


        XX<1,1>[90,10]  = FMT(LD.LAST.D,"####/##/##")


        XX<1,1>[105,20] = FMT(LD.AMT,"R2,")

        PRINT XX<1,1>

        LINE.NO = +1


    REPEAT


*******************************************************************************

    RETURN
*--------------------------------------------------------------------------
PRINT.HEAD:
*Line [ 531 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH


    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):PROGRAM.ID
    PR.HD :="'L'":SPACE(35):"����� ������� ������� ���� ��� ���� ���� 1019 "
******    PR.HD :="'L'":SPACE(40):"���� ����� ������ ����"
    PR.HD :="'L'":SPACE(40):"������� �� ��������� ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(05):" ��� ������ "

    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(25):" �������":SPACE(25):"������":SPACE(13):"��� ����":SPACE(10):"������"



    HEADING PR.HD
    LINE.NO = 0
    RETURN
*===============================================================
END
