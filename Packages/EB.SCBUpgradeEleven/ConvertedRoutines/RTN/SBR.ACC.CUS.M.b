* @ValidationCode : MjoxNzA5OTA4OTE5OkNwMTI1MjoxNjQ0OTI1MDYyMDk4OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-60</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBR.ACC.CUS.M

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
*   DEBUG
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 47 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 48 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBR.ACC.CUS.M'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    FN.CUS = 'FBNK.CUSTOMER'
    F.CUS = ''
    R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.AC = 'FBNK.ACCOUNT'
    F.AC  = ''
    R.AC = ''
    CALL OPF(FN.AC,F.AC)
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    KEY.LIST1=""
    SELECTED1=""
    ER.MSG1=""
*=================================================================
    T.SEL = "SELECT ":FN.AC:" WITH CO.CODE EQ":COMP:" AND CUSTOMER NE '' AND CATEGORY NE 3050"
    T.SEL := " AND @ID UNLIKE 016... AND @ID UNLIKE 99... AND"
    T.SEL := " @ID UNLIKE 88... BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    X = 0
    IF SELECTED  THEN
        K2=0
        CCC = ''
        MML = ''
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*******************************************************************
            ACT.CUS = R.AC<AC.LOCAL.REF><1,ACLR.ACTIVIYTY>
            ACC.OLD.CUS = FIELD(ACT.CUS,'/',1)

            IF ACC.OLD.CUS THEN
                CALL F.READ(FN.CUS,R.AC<AC.CUSTOMER>,R.CUS,F.CUS,ERR)
                POS.RES = R.CUS<EB.CUS.POSTING.RESTRICT>
                LOC = R.CUS<EB.CUS.LOCAL.REF>
                OLD.CUS = LOC<1,CULR.OLD.CUST.ID>
*******************************************************************
                OLD.CUS1 = OLD.CUS[1,6]
                ACC.OLD.CUS1 = ACC.OLD.CUS[1,6]
                IF OLD.CUS1 NE ACC.OLD.CUS1 AND POS.RES NE 99 THEN
                    CUS.CUST.NAME = R.CUS<EB.CUS.NAME.1>
                    ACC.ACCT.NAME = R.AC<AC.ACCOUNT.TITLE.1>
                    XX = SPACE(132)
                    XX1 = SPACE(132)
                    XX2 = SPACE(132)
                    XX<1,K2>[5,8]     = R.AC<AC.CUSTOMER>
                    XX1<1,K2>[5,8]    = OLD.CUS1
                    XX<1,K2>[22,35]   = LOC<1,CULR.ARABIC.NAME>
                    XX<1,K2>[78,10]   = KEY.LIST<I>
                    XX1<1,K2>[78,10]  = ACC.OLD.CUS1
                    XX<1,K2>[95,35]   = ACC.ACCT.NAME
                    IF R.AC<AC.CUSTOMER> NE CCC THEN
                        PRINT STR('-',120)
                        X ++
                    END
                    PRINT XX<1,K2>
                    PRINT XX1<1,K2>
*                    PRINT XX2<1,K2>
                    CCC = R.AC<AC.CUSTOMER>
                END
            END
*===============================================================
        NEXT I
        PRINT STR('=',120)
        IF X GT 0 THEN
            PRINT ; PRINT "������� = ":X:" �����"
        END ELSE
            PRINT ; PRINT " �� ���� ��������  "
        END
*===============================================================
    END ELSE
        X = 0
    END
RETURN
*===============================================================
PRINT.HEAD:
*---------
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 138 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    TIMEE = TIMEDATE()
    HHH = FIELD(TIMEE, ":", 1)
    MIN = FIELD(TIMEE,":", 2)
    PART3 = FIELD(TIMEE,":", 3)
    SEC = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"SBR.ACC.CUS.M"
    PR.HD :="'L'":SPACE(50):"���� ����������� �� ����� �������"
    PR.HD :="'L'":SPACE(50):"�� ����� :":T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',35)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������" :SPACE(10):"��� ������":SPACE(40):"��� ������":SPACE(20):"��� ���� ������"
    PR.HD :="'L'":"����� ������":SPACE(58):"����� ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
