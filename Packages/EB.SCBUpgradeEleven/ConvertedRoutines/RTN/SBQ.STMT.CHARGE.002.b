* @ValidationCode : MjoxODg2ODE1MjA3OkNwMTI1MjoxNjQ0OTI1MDYwNjk5OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBQ.STMT.CHARGE.002

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STMT.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.HOLD.POST.CHECK
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.FEE.EXCP
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    GOSUB CHECK.WORK
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    GOSUB ADD.REC.HOLD

    PRINT "THE CHARGE IS DONE"

RETURN
*------------------------------
CHECK.WORK:
*-----------
    CHK.WORK = 0
    POST.ID = "POST.HOLD-":TODAY
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SCB.HOLD.POST.CHECK':@FM:HOLD.POS.CHEK,POST.ID,CHEK.NO)
F.ITSS.SCB.HOLD.POST.CHECK = 'F.SCB.HOLD.POST.CHECK'
FN.F.ITSS.SCB.HOLD.POST.CHECK = ''
CALL OPF(F.ITSS.SCB.HOLD.POST.CHECK,FN.F.ITSS.SCB.HOLD.POST.CHECK)
CALL F.READ(F.ITSS.SCB.HOLD.POST.CHECK,POST.ID,R.ITSS.SCB.HOLD.POST.CHECK,FN.F.ITSS.SCB.HOLD.POST.CHECK,ERROR.SCB.HOLD.POST.CHECK)
CHEK.NO=R.ITSS.SCB.HOLD.POST.CHECK<HOLD.POS.CHEK>
    IF CHEK.NO NE '' THEN
        CHK.WORK = 1
    END ELSE

        IDDD="EG0010001"
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,IDDD,NEXT.W.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
NEXT.W.DAY=R.ITSS.DATES<EB.DAT.NEXT.WORKING.DAY>

        MON1=TODAY[5,2]
        MON2=NEXT.W.DAY[5,2]

        IF MON1 = '03'  AND MON2 = '04'  THEN
            IF CHEK.NO = '' THEN
                CHK.WORK = 2
            END
        END
        IF MON1 = '06'  AND MON2 = '07'  THEN
            IF CHEK.NO = '' THEN
                CHK.WORK = 2
            END
        END

        IF MON1 = '09'  AND MON2 = '10'  THEN
            IF CHEK.NO = '' THEN
                CHK.WORK = 2
            END
        END

        IF MON1 = '12'  AND MON2 = '01'  THEN
            IF CHEK.NO = '' THEN
                CHK.WORK = 2
            END
        END
        IF CHK.WORK = 2 THEN
            FN.SCB.HOLD.POST.CHECK = 'F.SCB.HOLD.POST.CHECK' ; F.SCB.HOLD.POST.CHECK = '' ; R.SCB.HOLD.POST.CHECK = ''
            R.SCB.HOLD.POST.CHECK<HOLD.POS.CHEK> = 1

            CALL F.WRITE(FN.SCB.HOLD.POST.CHECK,POST.ID,R.SCB.HOLD.POST.CHECK)
            CALL JOURNAL.UPDATE(POST.ID)

            CALL F.RELEASE(FN.SCB.HOLD.POST.CHECK,POST.ID,F.SCB.HOLD.POST.CHECK)
            CLOSE F.SCB.HOLD.POST.CHECK
        END
    END
RETURN
*------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.SCB.STMT.CHARGE = 'F.SCB.STMT.CHARGE' ; F.SCB.STMT.CHARGE = '' ; R.SCB.STMT.CHARGE = ''

    CHG.ID = "ACRESPOSTAC"
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID,CR.ACT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CR.ACT=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

    CHG.ID1 = "ACRESMAILAC"
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID1,CR.ACT1)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID1,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CR.ACT1=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

    CHG.ID.SAV = "ACPOSTSAV"
*Line [ 165 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID.SAV,CR.ACT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID.SAV,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CR.ACT=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

    V.DAT = TODAY
    DB.AMT = 0

RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CB = 'FBNK.CUSTOMER.ACCOUNT' ; F.CB = ''
    CALL OPF(FN.CB,F.CB)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    FN.CHG = 'FBNK.FT.CHARGE.TYPE' ; F.CHG = '' ; R.CHG = '' ; ERR.CHG = ''
    CALL OPF(FN.CHG,F.CHG)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ERR.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.EXP = 'F.SCB.CUS.FEE.EXCP' ; F.EXP = ''
    CALL OPF(FN.EXP,F.EXP)

*--------------------------
***** SECOND DAY SELECTION *****

    T.SEL1  = "SELECT FBNK.CUSTOMER WITH SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300 AND SECTOR NE 1400 AND WITHOUT COMPANY.BOOK IN (EG0010088 EG0010099 EG0010077 EG0010050 EG0010020 EG0010001 EG0010040 EG0010011)"
    T.SEL1 := " AND POSTING.RESTRICT NE 99 AND POSTING.RESTRICT NE 90 AND POSTING.RESTRICT NE 70 AND CREDIT.CODE NE 110 AND CREDIT.CODE NE 120"
    T.SEL1 := " AND STMT.CHARGE.EXP NE 'Y' AND POSTING.RESTRICT NE 18 AND DRMNT.CODE NE 1 AND TARGET NE 5700 AND TARGET NE 5800 AND TARGET NE 5850 AND TARGET NE 5860 AND TARGET NE 5870 AND TARGET NE 5900 BY @ID"

**************

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.CU,KEY.LIST1<I>,R.CU,F.CU,E1)
            CUST.ID = KEY.LIST1<I>
            CUFLG  = 0
            CUFLG1 = 0
            FLG1 = 0 ; FLG2 = 0
            COMP = R.CU<EB.CUS.COMPANY.BOOK>
            COM.CODE = COMP[8,2]

            WS.TARGET       = R.CU<EB.CUS.TARGET>
            WS.CONT.DATE    = R.CU<EB.CUS.CONTACT.DATE>

            OFS.USER.INFO = "AUTO.CHRGE":"/":"/" :COMP
***********************************************
            WS.REL.CODE     = R.CU<EB.CUS.RELATION.CODE>
            WS.CUS.REL.CODE = R.CU<EB.CUS.REL.CUSTOMER>
            IF WS.REL.CODE EQ '110' THEN
                CALL F.READ(FN.EXP,WS.CUS.REL.CODE,R.EXP,F.EXP,E5)
                WS.AMT.EXP = R.EXP<FEE.STATEMENT.FEE>
            END

***********************************************
            CALL F.READ(FN.CB,CUST.ID,R.CB,F.CB,E1)
            IF NOT(E1) THEN
***************************************************************
*Line [ 235 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                FN.SCB.STMT.CHARGE = 'F.SCB.STMT.CHARGE' ; F.SCB.STMT.CHARGE = '' ; R.SCB.STMT.CHARGE = ''
***************************************************************
                FLG = 0
                H = 1
                LOOP WHILE FLG = 0
                    IF R.CB<H,EB.CAC.ACCOUNT.NUMBER> = '' THEN
                        FLG = 1
                    END
                    ACT.CB = R.CB<H,EB.CAC.ACCOUNT.NUMBER>
*Line [ 251 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACT.CB,W.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
W.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                    ACT.CUR = ACT.CB[9,2]
*Line [ 259 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,ACT.CUR,CUR.CODE)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,ACT.CUR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR.CODE=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
                    ACT.CATEG = ACT.CB[11,4]
                    IF ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1005 OR ACT.CATEG EQ 6512 OR ACT.CATEG EQ 6511 OR ACT.CATEG EQ 1006 OR (ACT.CATEG GE 6514 AND ACT.CATEG LE 6517) OR (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR (ACT.CATEG GE 1101 AND ACT.CATEG LE 1204 OR ACT.CATEG GE 1290 AND ACT.CATEG LE 1534 OR ACT.CATEG GE 1536 AND ACT.CATEG LE 1599) THEN

*Line [ 269 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR.CODE,RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.CODE,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
*Line [ 276 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACT.CB,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
*Line [ 283 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACT.CB,ACT.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACT.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
                        LIM.ID = ACT.CUS:".":"0000":LIM.REF
*Line [ 291 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM.ID,AVIL.BAL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVIL.BAL=R.ITSS.LIMIT<LI.AVAIL.AMT>
                        END.BAL.L22 = W.BAL + AVIL.BAL

************************ UPDATED BY KHALED *** 2009/03/11 ***************************
*Line [ 301 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACT.CB,LOC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOC.BAL=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 230 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        BALC = DCOUNT(LOC.BAL,@VM)
                        BALCC = LOC.BAL<1,BALC>
                        END.BAL.L = END.BAL.L22 - BALCC
*************************************************************************************

                        IF CUR.CODE EQ "EGP" THEN
                            RATE = 1
                        END

                        USD.CUR.CODE = "USD"
                        CALL F.READ(FN.CUR,USD.CUR.CODE,R.CUR,F.CUR,ERR.CUR)
                        USD.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>

*Line [ 322 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID1,DB.AMT1)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID1,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DB.AMT1=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>

**** 2018/03/06 ********************************************************

                        CALL F.READ(FN.CHG,CHG.ID,R.CHG,F.CHG,ER.CHG)
                        CHG.CUR = R.CHG<FT5.CURRENCY>
                        IF CUR.CODE EQ 'EGP' THEN
                            IF WS.REL.CODE EQ '110' THEN
                                DB.AMT = WS.AMT.EXP
                            END ELSE
                                LOCATE CUR.CODE IN CHG.CUR<1,1> SETTING POS.CHG THEN DB.AMT = R.CHG<FT5.FLAT.AMT,POS.CHG>
                            END
                        END ELSE
                            LOCATE 'USD' IN CHG.CUR<1,1> SETTING POS.CHG THEN DB.AMT = R.CHG<FT5.FLAT.AMT,POS.CHG>
                            IF CUR.CODE NE 'USD' THEN
                                CALL F.READ(FN.CUR,CUR.CODE,R.CUR,F.CUR,ERR.CUR)
                                SELL.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
                                IF CUR.CODE EQ 'JPY' THEN
                                    SELL.RATE = SELL.RATE / 100
                                END
                                IF SELL.RATE NE 0 AND SELL.RATE NE '' THEN
                                    SELL.EXC.RATE = USD.RATE / SELL.RATE
                                END
                                DB.AMT = DB.AMT * SELL.EXC.RATE
                                DB.AMT = DROUND(DB.AMT,'2')
                            END
                        END

*********************************************************************************

**** SELECT SAVING ACCOUNT - 2011/10/24 ****
**** UPDATED BY BAKRY - 2012/08/21 ****
                        LOOP
                            REMOVE AC.ID FROM R.CB SETTING POS.ACCT
                        WHILE AC.ID:POS.ACCT
                            CALL F.READ(FN.ACCT,AC.ID,R.ACCT,F.ACCT,ER.ACCT)
                            AC.CATEG = R.ACCT<AC.CATEGORY>

                            IF AC.CATEG EQ 1001 OR AC.CATEG EQ 1005 OR AC.CATEG EQ 6512 OR AC.CATEG EQ 1006 OR (AC.CATEG GE 6514 AND AC.CATEG LE 6517) OR (AC.CATEG GE 1101 AND AC.CATEG LE 1204 OR AC.CATEG GE 1290 AND AC.CATEG LE 1534 OR AC.CATEG GE 1536 AND AC.CATEG LE 1599) THEN
                                FLG2 = 2
                            END
                        REPEAT

                        IF FLG2 EQ 2 THEN
****** 2018/03/06 ***********************************************************
                            CALL F.READ(FN.CHG,CHG.ID,R.CHG,F.CHG,ER.CHG)
                            CHG.CUR = R.CHG<FT5.CURRENCY>
                            IF CUR.CODE EQ 'EGP' THEN
                                IF WS.REL.CODE EQ '110' THEN
                                    DB.AMT = WS.AMT.EXP
                                END ELSE
                                    LOCATE CUR.CODE IN CHG.CUR<1,1> SETTING POS.CHG THEN DB.AMT = R.CHG<FT5.FLAT.AMT,POS.CHG>
                                END
                            END ELSE
                                LOCATE 'USD' IN CHG.CUR<1,1> SETTING POS.CHG THEN DB.AMT = R.CHG<FT5.FLAT.AMT,POS.CHG>
                                IF CUR.CODE NE 'USD' THEN
                                    CALL F.READ(FN.CUR,CUR.CODE,R.CUR,F.CUR,ERR.CUR)
                                    SELL.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
                                    IF CUR.CODE EQ 'JPY' THEN
                                        SELL.RATE = SELL.RATE /100
                                    END
                                    IF SELL.RATE NE 0 AND SELL.RATE NE '' THEN
                                        SELL.EXC.RATE = USD.RATE / SELL.RATE
                                    END
                                    DB.AMT = DB.AMT * SELL.EXC.RATE
                                    DB.AMT = DROUND(DB.AMT,'2')
                                END
                            END
                        END
*****************************************************************************

                        IF CUR.CODE NE "EGP" THEN
                            DB.AMT1 = (DB.AMT1 / RATE)
                            DB.AMTE = FIELD(DB.AMT1,".",2)
                            DB.AMT1 = FIELD(DB.AMT1,".",1):".":DB.AMTE[1,2]
                        END
                        LST.BAL = END.BAL.L
*******************************************************************
                        IF CUFLG NE 1 THEN
                            IF LST.BAL GE DB.AMT THEN
                                DEBIT.ACCT = ACT.CB
                                CUS = ACT.CB[1,8]:".":TODAY
                                GOSUB CR.FT.OFS
                                LST.BAL = LST.BAL - DB.AMT
                                CUFLG = 1
                            END ELSE
                                CUFLG = 3
                            END
                        END
*******************************************************************
                        IF LOC.REF1<1,CULR.GOVERNORATE> = '98' THEN
                            IF LST.BAL GE DB.AMT1 THEN
                                DEBIT.ACCT = ACT.CB
                                CUS = ACT.CB[1,8]:".":TODAY
                                GOSUB CR.FT.OFS1
                                CUFLG1 = 2
                            END ELSE
                                CUFLG1 = 4
                            END
                        END
                        IF CUFLG = 1 AND LOC.REF1<1,CULR.GOVERNORATE> NE '98' THEN
                            FLG = 1
                        END ELSE IF CUFLG1 = 2 THEN FLG = 1
*******************************************************************
                    END
                    H = H + 1
                REPEAT
            END

            GOSUB INS.REC
        NEXT I
    END
RETURN
****************************************************************
INS.REC:
********
    IF CUFLG = 3 OR CUFLG1 = 4 THEN
        FLG3 = 0

        CALL F.READ(FN.CB,CUST.ID,R.CB,F.CB,E1)
        LOOP
            REMOVE ACC.NO FROM R.CB SETTING POS
        WHILE ACC.NO:POS
            CALL F.READ(FN.ACCT,ACC.NO,R.ACCT,F.ACCT,ER.ACCT)

            AC.CATEG = R.ACCT<AC.CATEGORY>
            AC.CUR   = R.ACCT<AC.CURRENCY>

            IF FLG3 NE 2 THEN
                IF (AC.CATEG EQ 1001 OR AC.CATEG EQ 1005 OR AC.CATEG EQ 6512 OR AC.CATEG EQ 1006 OR (AC.CATEG GE 6514 AND AC.CATEG LE 6517) OR (AC.CATEG GE 1101 AND AC.CATEG LE 1204 OR AC.CATEG GE 1290 AND AC.CATEG LE 1534 OR AC.CATEG GE 1536 AND AC.CATEG LE 1599)) AND AC.CUR EQ 'EGP' THEN
                    FLG3 = 2
                    ACT.CB = ACC.NO
                END
            END
        REPEAT

        IF FLG3 EQ 2 THEN

            CALL F.READ(FN.ACCT,ACT.CB,R.ACCT,F.ACCT,ER.ACCT)
            ACT.CUR = R.ACCT<AC.CURRENCY>

*Line [ 469 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID1,DB.AMT1)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID1,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DB.AMT1=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>

            IF CUFLG = 3 THEN
                DEBIT.ACCT = ACT.CB
                CUS = ACT.CB[1,8]:".":TODAY
                GOSUB CR.FT.OFS
            END

            IF CUFLG1 = 4 THEN
                DEBIT.ACCT = ACT.CB
                CUS = ACT.CB[1,8]:".":TODAY
                GOSUB CR.FT.OFS1
            END

        END ELSE

            CUS = KEY.LIST1<I>:".":TODAY
            IF CUFLG = 3 THEN
                R.SCB.STMT.CHARGE<STM.POST.FLG> = "YES"
                R.SCB.STMT.CHARGE<STM.CO.CODE>  = COMP
            END
            IF CUFLG1 = 4 THEN
                R.SCB.STMT.CHARGE<STM.STMT.FLG> = "YES"
                R.SCB.STMT.CHARGE<STM.CO.CODE>  = COMP
            END

            CALL F.WRITE(FN.SCB.STMT.CHARGE,CUS,R.SCB.STMT.CHARGE)
            CALL JOURNAL.UPDATE(CUS)

            CALL F.RELEASE(FN.SCB.STMT.CHARGE,CUS,F.SCB.STMT.CHARGE)
            CLOSE F.SCB.STMT.CHARGE
        END

    END
RETURN
**************************************************************
CR.FT.OFS:
*********
    DR.ACCT = DEBIT.ACCT
    CURR = DEBIT.ACCT[9,2]
    CR.ACCT = "PL":CR.ACT
    DATEE = TODAY

***** 2018/09/17 ****
    IF ( WS.TARGET EQ '8' AND WS.CONT.DATE GE '20180101' AND WS.CONT.DATE LT '20201015' ) OR WS.TARGET EQ '30' THEN
        DB.AMT = DB.AMT / 2
    END

*******************
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC50":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS[1,7]

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    TER.NO.P = COM.CODE + 1000
    OFS.ID = "T":TRIM(TER.NO.P, '0', 'L'):".P.":CUS

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
RETURN
************************************************************
CR.FT.OFS1:
*********
    DR.ACCT = DEBIT.ACCT
    CURR = DEBIT.ACCT[9,2]
    CR.ACCT1 = "PL":CR.ACT1
    DATEE = TODAY

***** 2018/09/17 ****
    IF ( WS.TARGET EQ '8' AND WS.CONT.DATE GE '20180101' AND WS.CONT.DATE LT '20201015' ) OR WS.TARGET EQ '30' THEN
        DB.AMT1 = DB.AMT1 / 2
    END

*******************
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC51":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT1:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT1:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS[1,7]


    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    TER.NO.H = COM.CODE + 1000
    OFS.ID = "T":TRIM(TER.NO.H, '0', 'L'):".H.":CUS

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
RETURN
************************************************************
ADD.REC.HOLD:
*-----------
    IF CHK.WORK = 2 THEN
        R.SCB.HOLD.POST.CHECK<HOLD.POS.CHEK.OUT> = 1

        CALL F.WRITE(FN.SCB.HOLD.POST.CHECK,POST.ID,R.SCB.HOLD.POST.CHECK)
        CALL JOURNAL.UPDATE(POST.ID)

        CALL F.RELEASE(FN.SCB.HOLD.POST.CHECK,POST.ID,F.SCB.HOLD.POST.CHECK)
        CLOSE F.SCB.HOLD.POST.CHECK
    END
RETURN
************************************************************
END
