* @ValidationCode : MjoyMDM1OTkxNDAxOkNwMTI1MjoxNjQ0OTI1MDY2MjY1OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBR.CUST.NATIONAL.ID.DUP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COUNTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "NATIONAL.NO.Duplications.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"NATIONAL.NO.Duplications.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "NATIONAL.NO.Duplications.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE NATIONAL.NO.Duplications.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create NATIONAL.NO.Duplications.CSV File IN &SAVEDLISTS&'
        END
    END

    HEAD.DESC  = "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "NATIONAL.NUMBER":","
    HEAD.DESC := "BRANCH":","
    HEAD.DESC := "COUNTRY.NAME":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------------------
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST    = '' ; SELECTED    = ''  ;  ER.MSG = ''
    CBE.ID.NEXT = '' ; CBE.ID.PREV = ''

RETURN

*========================================================================
PROCESS:
*-------------------------------------------
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY NE EG AND @ID UNLIKE 994... AND POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 WITHOUT SECTOR IN (5010 5020) BY NATIONALITY BY ID.NUMBER"
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'EG' AND @ID UNLIKE 994... AND POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NSN.NO NE '' WITHOUT SECTOR IN (5010 5020) BY NSN.NO BY NATIONALITY"
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'EG' AND POSTING.RESTRICT LT 90 AND @ID UNLIKE 994... AND CUSTOMER.STATUS NE 3 AND NSN.NO NE '' AND ID.TYPE NE 5 BY NSN.NO BY NATIONALITY"
    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'EG' AND POSTING.RESTRICT LT 90 AND @ID UNLIKE 994... AND CUSTOMER.STATUS NE 3 AND NSN.NO NE '' AND ID.TYPE NE 5 WITHOUT SECTOR IN ( 5010 5020 ) BY NSN.NO BY NATIONALITY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E2)
            CALL F.READ(FN.CU,KEY.LIST<I+1>,R.CU1,F.CU,E3)
            CALL F.READ(FN.CU,KEY.LIST<I-1>,R.CU2,F.CU,E4)

            CBE.ID      = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            CBE.ID.NEXT = R.CU1<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            CBE.ID.PREV = R.CU2<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>

            CUS.ID    = KEY.LIST<I>
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            WS.POSTING  = R.CU<EB.CUS.POSTING.RESTRICT>
            COMP.ID   = R.CU<EB.CUS.COMPANY.BOOK>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COMP.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
            NAT.ID    = R.CU<EB.CUS.NATIONALITY>
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,NAT.ID,COUNTRY.NAME)
F.ITSS.COUNTRY = 'F.COUNTRY'
FN.F.ITSS.COUNTRY = ''
CALL OPF(F.ITSS.COUNTRY,FN.F.ITSS.COUNTRY)
CALL F.READ(F.ITSS.COUNTRY,NAT.ID,R.ITSS.COUNTRY,FN.F.ITSS.COUNTRY,ERROR.COUNTRY)
COUNTRY.NAME=R.ITSS.COUNTRY<EB.COU.COUNTRY.NAME>

            IF CBE.ID EQ CBE.ID.NEXT THEN
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := CBE.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","
                BB.DATA := WS.POSTING:","
                BB.DATA := R.CU<EB.CUS.CUSTOMER.STATUS>:","
                BB.DATA := R.CU<EB.CUS.LOCAL.REF><1,CULR.EMPLOEE.NO>:","
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END ELSE
                IF CBE.ID EQ CBE.ID.PREV THEN

                    BB.DATA  = CUS.ID:","
                    BB.DATA := CUST.NAME:","
                    BB.DATA := CBE.ID:","
                    BB.DATA := COMP.NAME:","
                    BB.DATA := NAT.ID:","
                    BB.DATA := COUNTRY.NAME:","
                    BB.DATA := WS.POSTING:","
                    BB.DATA := R.CU<EB.CUS.CUSTOMER.STATUS>:","
                    BB.DATA := R.CU<EB.CUS.LOCAL.REF><1,CULR.EMPLOEE.NO>:","

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            END
        NEXT I
    END
RETURN
END
