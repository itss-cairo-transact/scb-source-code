* @ValidationCode : MjotNjUwNjg0NTY1OkNwMTI1MjoxNjQ0OTI1MDU5NTQ3OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBM.TOP50

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.DATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TOP50


    COMP = ID.COMPANY
    DAT.1 = TODAY[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')

    DAT.ID = 'EG0010001'
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,DAT.2)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT.2=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    FN.SCR = 'FBNK.STMT.ACCT.DR'
    F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CONT = 'F.RE.STAT.LINE.CONT'     ; F.CONT = ''
    CALL OPF(FN.CONT,F.CONT)

    FN.LN   = 'F.RE.STAT.REP.LINE'      ; F.LN   = ''
    CALL OPF(FN.LN,F.LN)

    FN.CONS.ACC = 'F.RE.CONSOL.CONTRACT' ; F.CONS.ACC = ''
    CALL OPF(FN.CONS.ACC,F.CONS.ACC)


    FN.CONS.ACC.SEQ = 'F.RE.CONSOL.CONTRACT.SEQU' ; F.CONS.ACC.SEQ = ''
    CALL OPF(FN.CONS.ACC.SEQ,F.CONS.ACC.SEQ)


    FN.TOP = 'F.SCB.TOP50'      ; F.TOP = ''  ; R.TOP = ''
    CALL OPF(FN.TOP,F.TOP)

    FN.ECB = 'FBNK.EB.CONTRACT.BALANCES' ; F.ECB = ''
    CALL OPF(FN.ECB,F.ECB)



    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = ''
    CALL OPF(FN.GCI,F.GCI)

    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)


    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.BID = 'FBNK.BASIC.INTEREST.DATE' ; F.BID = ''
    CALL OPF(FN.BID,F.BID)

    FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)


    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    NET.AMT     = 0
    NET.AMT.500 = 0
    SER = 0
    KK1 = 0
    EXECUTE "CLEAR-FILE F.SCB.TOP50"
*============================================================
    T.SEL.CONT    = "SELECT ":FN.CONT:" WITH @ID LIKE ...GENLEDALL.0360.... OR @ID LIKE ...GENLEDALL.0365.... OR @ID LIKE ...GENLEDALL.0370.... OR @ID LIKE ...GENLEDALL.0372.... OR @ID LIKE ...GENLEDALL.0375.... OR @ID LIKE ...GENLEDALL.0379.... OR @ID LIKE ...GENLEDALL.0380.... OR @ID LIKE ...GENLEDALL.0381.... OR @ID LIKE ...GENLEDALL.0382.... OR @ID LIKE ...GENLEDALL.0383.... OR @ID LIKE ...GENLEDALL.0384....  OR @ID LIKE ...GENLEDALL.0385.... OR @ID LIKE ...GENLEDALL.0390.... OR @ID LIKE ...GENLEDALL.0393.... OR @ID LIKE ...GENLEDALL.0395.... OR @ID LIKE ...GENLEDALL.0400.... OR @ID LIKE ...GENLEDALL.0410.... OR @ID LIKE ...GENLEDALL.0415.... OR @ID LIKE ...GENLEDALL.0420.... BY @ID"
*   T.SEL.CONT    = "SELECT ":FN.CONT:" WITH @ID LIKE ...GENLEDALL.0365.... BY @ID"
    CALL EB.READLIST(T.SEL.CONT,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO SELECTED

            NET.AMT  = 0
            LINE     = KEY.LIST<HH>
            CALL F.READ(FN.CONT,KEY.LIST<HH>,R.CONT,F.CONT,E1)
*Line [ 148 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
            FOR I = 1 TO DECOUNT.CONT
                CONS.ID     = R.CONT<RE.SLC.ASST.CONSOL.KEY,I>
*IF  CONS.ID     = 'LD.1.TR.EGP.21005.10.250..EG.20...3..4420.6M.EG0010010' THEN DEBUG
                CALL F.READ(FN.CONS.ACC,CONS.ID,R.CONS.ACC,F.CONS.ACC,E.CONS.ACC)
                LOOP
                    REMOVE ACC.NO FROM R.CONS.ACC SETTING POS
                WHILE ACC.NO:POS
                    GOSUB GETREC
                REPEAT

                CALL F.READ(FN.CONS.ACC.SEQ,CONS.ID,R.CONS.ACC.SEQ,F.CONS.ACC.SEQ,E.CONS.ACC.SEQ)
                LOOP
                    REMOVE ACC.NO.SEQ FROM R.CONS.ACC.SEQ SETTING POS.SEQ
                WHILE ACC.NO.SEQ:POS.SEQ
                    ID.SEQU = CONS.ID:';':ACC.NO.SEQ
                    CALL F.READ(FN.CONS.ACC,ID.SEQU,R.CONS.ACC,F.CONS.ACC,E.CONS.ACC)
                    LOOP
                        REMOVE ACC.NO FROM R.CONS.ACC SETTING POS
                    WHILE ACC.NO:POS
                        GOSUB GETREC
                    REPEAT
                REPEAT

            NEXT I
        NEXT HH
    END
RETURN
*============================================================
GETREC:
*ECB.ID = "01030067310100101"
*DEBUG
    ECB.ID = ACC.NO ; EADD = ""

    OPEN.ACTUAL.BAL = ''
    VAL.DAT         = ''
    CALL F.READ(FN.ECB,ECB.ID,R.ECB,F.ECB,EADD)
    CURR            = R.ECB<ECB.CURRENCY>
    CUST.ID         = R.ECB<ECB.CUSTOMER>
    LD.ID           = ECB.ID
    IF ECB.ID[1,2] EQ 'LD' THEN
        CALL F.READ(FN.LD,ECB.ID,R.LD,F.LD,LD.ERR)
        AMT             = R.LD<LD.AMOUNT>
        VAL.DATE        = R.LD<LD.VALUE.DATE>
        MAT.DATE        = R.LD<LD.FIN.MAT.DATE>
        REMB.AMT        = R.LD<LD.REIMBURSE.AMOUNT>
*        IF VAL.DATE LT TODAY AND AMT NE '0' THEN
*            OPEN.ACTUAL.BAL = AMT
*        END

*        IF VAL.DATE LT TODAY AND AMT EQ '0' THEN
*            OPEN.ACTUAL.BAL = REMB.AMT
*        END
*        IF VAL.DATE EQ TODAY AND AMT NE '0' THEN
*            OPEN.ACTUAL.BAL = '0'
*         END


        IF VAL.DATE LE DAT.1 AND AMT NE "0" THEN
            OPEN.ACTUAL.BAL = AMT
        END ELSE
            IF (MAT.DATE LE TODAY AND MAT.DATE GT DAT.1) AND AMT EQ '0' THEN
                OPEN.ACTUAL.BAL = REMB.AMT
            END
        END

        INT.RATE        = R.ECB<ECB.INTEREST.RATE>
    END ELSE
*********** SELECT FROM ACI *****

        OPEN.ACTUAL.BAL = R.ECB<ECB.OPEN.ACTUAL.BAL>

        CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E1)
*Line [ 222 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE CURR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
        AMT.RATE     = R.BASE<RE.BCP.RATE,POS>

        IF CURR NE 'EGP' THEN
            WS.AMT   = OPEN.ACTUAL.BAL * AMT.RATE
        END ELSE
            WS.AMT   = OPEN.ACTUAL.BAL
        END


        CALL F.READ(FN.AC,ECB.ID,R.AC,F.AC,EAC)
        GROUP.ID        = R.AC<AC.CONDITION.GROUP>
        WS.VAL.DATE     = R.AC<AC.OPENING.DATE>
        CALL F.READ(FN.GC,GROUP.ID,R.GC,F.GC,E1)
        WS.MAT.DATE     = R.GC<IC.GCP.CR.CAP.FREQUENCY>[1,8]


*Line [ 240 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.AC.COUNT     = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
        AD.DATE         = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
        ACI.ID          = ECB.ID:'-':AD.DATE

        CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 246 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
        WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>

        IF WS.RATE EQ '' THEN

**** SELECT FROM BASIC.INTEREST ****
            ACI.BI.KEY        = R.ACI<IC.ACI.CR.BASIC.RATE>
            ACI.BI.ID.KEY     = ACI.BI.KEY:CURR
            ACI.OPER          = R.ACI<IC.ACI.CR.MARGIN.OPER>
            ACI.MRG.RATE      = R.ACI<IC.ACI.CR.MARGIN.RATE>

            CALL F.READ(FN.BID,ACI.BI.ID.KEY,R.BID,F.BID,E4)
            ACI.BI.DATE       = R.BID<EB.BID.EFFECTIVE.DATE,1>
            ACI.BI.ID         = ACI.BI.ID.KEY:ACI.BI.DATE

            CALL F.READ(FN.BI,ACI.BI.ID,R.BI,F.BI,E3)

            WS.RATE = R.BI<EB.BIN.INTEREST.RATE>
            IF ACI.OPER EQ 'SUBTRACT' THEN
                WS.RATE = WS.RATE - ACI.MRG.RATE
            END
            IF ACI.OPER EQ 'ADD' THEN
                WS.RATE = WS.RATE + ACI.MRG.RATE
            END


            IF WS.RATE EQ '' THEN

**** SELECT RATE FROM GCI ****

                GD.ID   = GROUP.ID:CURR
                CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
*Line [ 279 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.GD.DATE      = DCOUNT(R.GD<AC.GRD.CREDIT.DATES>,@VM)
                GD.DATE         = R.GD<AC.GRD.CREDIT.DATES><1,WS.GD.DATE>
                GCI.ID  = GD.ID:GD.DATE

                CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
*Line [ 285 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.RATE.COUNT   = DCOUNT(R.GCI<IC.GCI.CR.INT.RATE>,@VM)

                FOR KK = 1 TO WS.RATE.COUNT
                    WS.AMT.POS   = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK>
                    IF KK = 1 THEN
                        WS.AMT.POS.1 = 0
                    END ELSE
                        WS.AMT.POS.1 = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK-1>
                    END
                    IF WS.AMT GT WS.AMT.POS.1 AND WS.AMT LE WS.AMT.POS THEN
                        WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,KK>
                    END
                    WS.LAST.AMT = R.GCI<IC.GCI.CR.LIMIT.AMT><1,WS.RATE.COUNT-1>
                    IF WS.AMT GT WS.LAST.AMT THEN
                        WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,WS.RATE.COUNT>
                    END
                NEXT KK
            END
        END
        INT.RATE        = WS.RATE
    END
*****************************************************
    IF OPEN.ACTUAL.BAL GE '0' THEN
        CALL F.READ(FN.TOP,CUST.ID,R.TOP,F.TOP,ETEXT.TOP)
        IF NOT(R.TOP) THEN
            NEW.ID = CUST.ID
            R.TOP<TP.LINE.NO,1>       = LINE
            R.TOP<TP.RECORD.ID,1>     = ACC.NO
            R.TOP<TP.CURRENCY,1>      = CURR
            R.TOP<TP.RATE,1>          = INT.RATE

            IF CURR EQ 'EGP' THEN
                R.TOP<TP.AMT.LCY,1 >      =  OPEN.ACTUAL.BAL
                R.TOP<TP.AMT.FCY,1>       = '0'
                TOTAL.LCY = OPEN.ACTUAL.BAL
                R.TOP<TP.TOTAL.EGP.AMT>  = TOTAL.LCY
            END ELSE
                R.TOP<TP.AMT.LCY,1>       = '0'
                R.TOP<TP.AMT.FCY,1>       = OPEN.ACTUAL.BAL
                LOCATE CURR IN CURR.BASE<1,1> SETTING CUR.POS THEN
                    CURR.RATE = R.BASE<RE.BCP.RATE,CUR.POS>
                END
                TOTAL.FCY = OPEN.ACTUAL.BAL * CURR.RATE
                TOTAL.FCY = DROUND(TOTAL.FCY,'2')
                R.TOP<TP.TOTAL.FCY.AMT>  = TOTAL.FCY
            END

            R.TOP<TP.TOTAL.AMT> =   R.TOP<TP.TOTAL.EGP.AMT> + R.TOP<TP.TOTAL.FCY.AMT>
            CALL F.WRITE(FN.TOP,NEW.ID,R.TOP)
            CALL JOURNAL.UPDATE(NEW.ID)

        END ELSE


*Line [ 340 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            QQ = DCOUNT(R.TOP<TP.LINE.NO>,@VM)
            QQ = QQ + 1
            NEW.ID = CUST.ID
            R.TOP<TP.LINE.NO,QQ>           = LINE
            R.TOP<TP.RECORD.ID,QQ>         = ACC.NO
            R.TOP<TP.CURRENCY,QQ>          = CURR
            R.TOP<TP.RATE,QQ>              = INT.RATE
            IF CURR EQ 'EGP' THEN
                R.TOP<TP.AMT.LCY,QQ >      =  OPEN.ACTUAL.BAL
                R.TOP<TP.AMT.FCY,QQ >      =  '0'
                TOTAL.LCY =  R.TOP<TP.TOTAL.EGP.AMT> + R.TOP<TP.AMT.LCY,QQ>
                R.TOP<TP.TOTAL.EGP.AMT> = TOTAL.LCY
            END ELSE
                R.TOP<TP.AMT.LCY,QQ>       =  '0'
                R.TOP<TP.AMT.FCY,QQ >      =  OPEN.ACTUAL.BAL

                CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E1)
*Line [ 358 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CURR IN R.BASE<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                CURR.RATE     = R.BASE<RE.BCP.RATE,POS>

                TOTAL.FCY = (R.TOP<TP.AMT.FCY,QQ> * CURR.RATE) + R.TOP<TP.TOTAL.FCY.AMT>
                TOTAL.FCY = DROUND(TOTAL.FCY,'2')
                R.TOP<TP.TOTAL.FCY.AMT>  = TOTAL.FCY
            END

            R.TOP<TP.TOTAL.AMT> =   R.TOP<TP.TOTAL.EGP.AMT> + R.TOP<TP.TOTAL.FCY.AMT>
            CALL F.WRITE(FN.TOP,NEW.ID,R.TOP)
            CALL JOURNAL.UPDATE(NEW.ID)

            OPEN.ACTUAL.BAL = 0
        END
    END
RETURN
