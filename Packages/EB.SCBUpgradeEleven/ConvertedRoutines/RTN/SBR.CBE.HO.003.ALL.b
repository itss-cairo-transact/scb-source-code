* @ValidationCode : MjotNzQ3NjMyNzM1OkNwMTI1MjoxNjQ0OTI1MDYzNDMyOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
******* CREATED BY KHALED IBRAHIM *******

** SUBROUTINE SBR.CBE.HO.003.ALL
PROGRAM SBR.CBE.HO.003.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CBE.CREDIT.HO

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

RETURN
*==============================================================
INITIATE:

    DAT.CURR  = TODAY
    CALL CDT ('',DAT.CURR,'-1C')

    DAT.ID = 'EG0010001'
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,INT.DAT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
INT.DAT=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    TT = FMT(TODAY,"####/##/##")

    OPENSEQ "CREDIT.STATIC" , "SBR.CBE.HO.ALL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"CREDIT.STATIC":' ':"SBR.CBE.HO.ALL.CSV"
        HUSH OFF
    END
    OPENSEQ "CREDIT.STATIC" , "SBR.CBE.HO.ALL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBR.CBE.HO.ALL.CSV CREATED IN CREDIT.STATIC'
        END ELSE
            STOP 'Cannot create SBR.CBE.HO.ALL.CSV File IN CREDIT.STATIC'
        END
    END

    HEAD.DESC = ",":TT:"���� ����� �������� �� ��� "

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "�":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� �������":","

    HEAD.DESC := "������ ":TT:",,,"
    HEAD.DESC := "��������� ":TT

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.SCC = 'F.SCB.CBE.CREDIT.HO' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.DIR.INTERNAL.AMT.LAST    = 0
    WS.INDIR.INTERNAL.AMT.LAST  = 0
    TOTAL.INTERNAL.LAST         = 0
    WS.DIR.USED.AMT.LAST   = 0
    WS.INDIR.USED.AMT.LAST = 0
    TOTAL.USED.LAST        = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.CHANGE              = 0
    WS.CHANGE.PER          = 0
    WS.PER.USED.INTERNAL   = 0
    WS.USED.AVG            = 0
    WS.PER.AVG.INTERNAL    = 0
    GROUP.ID.TMP = 0
    KK = 0
****

    WS.DIR.INTERNAL.AMT.TOT    = 0
    WS.INDIR.INTERNAL.AMT.TOT  = 0
    TOTAL.INTERNAL.TOT         = 0
    WS.DIR.USED.AMT.LAST.TOT   = 0
    WS.INDIR.USED.AMT.LAST.TOT = 0

    WS.DIR.INTERNAL.AMT.LAST.TOT   = 0
    WS.INDIR.INTERNAL.AMT.LAST.TOT = 0
    TOTAL.INTERNAL.LAST.TOT        = 0

    TOTAL.USED.LAST.TOT        = 0
    WS.DIR.USED.AMT.TOT        = 0
    WS.INDIR.USED.AMT.TOT      = 0
    TOTAL.USED.TOT             = 0
    WS.CHANGE.TOT              = 0
    WS.CHANGE.INT.TOT          = 0
****
RETURN
*========================================================================
PROCESS:
    T.SEL = "SELECT ":FN.SCC:" WITH CREDIT.CODE EQ 100 BY-DSND TOTAL.INTERNAL.AMT"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.SCC,KEY.LIST<I>,R.SCC,F.SCC,E1)

            GROUP.ID = R.SCC<SCH.CUSTOMER.ID>

            IF I EQ 1 THEN GROUP.ID.TMP = GROUP.ID
            IF GROUP.ID NE GROUP.ID.TMP THEN
                GOSUB DATA.FILE
            END
            CUS.ID   = R.SCC<SCH.CUSTOMER.ID>

*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

            CUS.ID = GROUP.ID
            COMP.ID = R.SCC<SCH.CO.CODE>
*Line [ 188 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*---------------------------------------------------------
            DAT = R.SCC<SCH.LPE.DATE>

************** CURRENT MONTH *********************************

            IF DAT EQ DAT.CURR THEN
                WS.DIR.INTERNAL.AMT   += R.SCC<SCH.DIR.INTERNAL.AMT>
                WS.INDIR.INTERNAL.AMT += R.SCC<SCH.INDIR.INTERNAL.AMT>
                TOTAL.INTERNAL        += R.SCC<SCH.TOTAL.INTERNAL.AMT>


                WS.DIR.USED.AMT       += R.SCC<SCH.DIR.USED.AMT>
                WS.INDIR.USED.AMT     += R.SCC<SCH.INDIR.USED.AMT>
                TOTAL.USED            += R.SCC<SCH.TOTAL.USED.AMT>
            END

            GROUP.ID.TMP = GROUP.ID

        NEXT I
        IF I EQ SELECTED THEN GOSUB DATA.FILE
    END

**** PRINT TOTALS **************

    BB.DATA  = ","
    BB.DATA := "��������":","
    BB.DATA := ","
    BB.DATA := ","

    BB.DATA := WS.DIR.INTERNAL.AMT.TOT:","
    BB.DATA := WS.INDIR.INTERNAL.AMT.TOT:","
    BB.DATA := TOTAL.INTERNAL.TOT:","

    BB.DATA := WS.DIR.USED.AMT.TOT:","
    BB.DATA := WS.INDIR.USED.AMT.TOT:","
    BB.DATA := TOTAL.USED.TOT:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
********************************

RETURN
*------------------------------------------------------------
******************* WRITE DATA IN FILE **********************
DATA.FILE:

    KK++
    BB.DATA  = KK:","
    BB.DATA := CUST.NAME:","
    BB.DATA := CUS.ID:","
    BB.DATA := BRANCH.NAME:","
    BB.DATA := WS.DIR.INTERNAL.AMT:","
    BB.DATA := WS.INDIR.INTERNAL.AMT:","
    BB.DATA := TOTAL.INTERNAL:","
    BB.DATA := WS.DIR.USED.AMT:","
    BB.DATA := WS.INDIR.USED.AMT:","
    BB.DATA := TOTAL.USED:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    WS.DIR.INTERNAL.AMT.TOT    += WS.DIR.INTERNAL.AMT
    WS.INDIR.INTERNAL.AMT.TOT  += WS.INDIR.INTERNAL.AMT
    TOTAL.INTERNAL.TOT         += TOTAL.INTERNAL

    WS.DIR.USED.AMT.TOT        += WS.DIR.USED.AMT
    WS.INDIR.USED.AMT.TOT      += WS.INDIR.USED.AMT
    TOTAL.USED.TOT             += TOTAL.USED


    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.DIR.INTERNAL.AMT.LAST    = 0
    WS.INDIR.INTERNAL.AMT.LAST  = 0
    TOTAL.INTERNAL.LAST         = 0
    WS.DIR.USED.AMT.LAST   = 0
    WS.INDIR.USED.AMT.LAST = 0
    TOTAL.USED.LAST        = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.CHANGE              = 0
    WS.CHANGE.INT          = 0

*************************************************************
RETURN
*---------------------------------------------------
END
