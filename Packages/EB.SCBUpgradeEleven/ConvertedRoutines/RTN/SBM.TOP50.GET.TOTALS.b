* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*--- COPY FROM SBR.TOP50
*--- EDIT BY NESMA & MENNA

    PROGRAM SBM.TOP50.GET.TOTALS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TOP50
*-----------------------------------------
    CUR.POS = "" ; CUR.ID = ""

    CURR  = "USD.EUR.GBP.SAR.JPY.CHF.DKK.SEK.NOK.ITL.INR.AUD.CAD.KWD.AED"
    CUR.N = DCOUNT(CURR,'.')

    FN.TP  = "F.SCB.TOP50" ; F.TP   = "" ; R.TP  = ""
    CALL OPF(FN.TP,F.TP)

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC  = "" ; R.ACC  = ""
    CALL OPF(FN.ACC,F.ACC)

    FOR NN = 1 TO CUR.N
        CUR.ID = FIELD(CURR,'.',NN)
        Y.SEL  = "SSELECT F.SCB.TOP50 WITH GROUP.ID EQ '' AND CURRENCY EQ ": CUR.ID

        CALL EB.READLIST(Y.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        FCY.AMT = 0
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.TP,KEY.LIST<I>, R.TP, F.TP ,E.TP)
*Line [ 58 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                INDX = DCOUNT(R.TP<TP.LINE.NO>,@VM)
                FOR HH = 1 TO INDX
                    CUR = R.TP<TP.CURRENCY><1,HH>
                    IF CUR EQ CUR.ID THEN
                        FCY.AMT += R.TP<TP.AMT.FCY><1,HH>
                    END
                NEXT HH

                IF CUR.ID EQ 'USD' THEN
                    R.TP<TP.TOTAL.USD> = FCY.AMT
                END
                IF CUR.ID EQ 'EUR' THEN
                    R.TP<TP.TOTAL.EUR> = FCY.AMT
                END
                IF CUR.ID EQ 'SAR' THEN
                    R.TP<TP.TOTAL.SAR> = FCY.AMT
                END
                IF CUR.ID EQ 'GBP' THEN
                    R.TP<TP.TOTAL.GBP> = FCY.AMT
                END
                IF CUR.ID EQ 'JPY' THEN
                    R.TP<TP.TOTAL.JPY> = FCY.AMT
                END
                IF CUR.ID EQ 'CHF' THEN
                    R.TP<TP.TOTAL.CHF> = FCY.AMT
                END
                IF CUR.ID EQ 'DKK' THEN
                    R.TP<TP.TOTAL.DKK> = FCY.AMT
                END
                IF CUR.ID EQ 'SEK' THEN
                    R.TP<TP.TOTAL.SEK> = FCY.AMT
                END
                IF CUR.ID EQ 'NOK' THEN
                    R.TP<TP.TOTAL.NOK> = FCY.AMT
                END
                IF CUR.ID EQ 'ITL' THEN
                    R.TP<TP.TOTAL.ITL> = FCY.AMT
                END
                IF CUR.ID EQ 'INR' THEN
                    R.TP<TP.TOTAL.INR> = FCY.AMT
                END
                IF CUR.ID EQ 'AUD' THEN
                    R.TP<TP.TOTAL.AUD> = FCY.AMT
                END
                IF CUR.ID EQ 'CAD' THEN
                    R.TP<TP.TOTAL.CAD> = FCY.AMT
                END
                IF CUR.ID EQ 'KWD' THEN
                    R.TP<TP.TOTAL.KWD> = FCY.AMT
                END
                IF CUR.ID EQ 'AED' THEN
                    R.TP<TP.TOTAL.AED> = FCY.AMT
                END
                CALL F.WRITE(FN.TP ,KEY.LIST<I>, R.TP)
                CALL JOURNAL.UPDATE(KEY.LIST<I>)

                FCY.AMT = 0
            NEXT I
        END
    NEXT NN
********************************************************
    RETURN
END
