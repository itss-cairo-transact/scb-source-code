* @ValidationCode : MjoxMTgwMDM5ODYwOkNwMTI1MjoxNjQ0OTI1MDYzMjMxOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>3163</Rating>
*-----------------------------------------------------------------------------
*******MAHMOUD 10/11/2014*******

PROGRAM SBR.CBE.6000.01

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*Line [ 61 ] Adding $INCLUDE I_F.SBD.CURRENCY - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY

*-------------------------------------------------------------------------

    HEAD.DESC  = ""
    HEAD.DESC2 = ""

*Line [ 69 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    GOSUB INITIATE
    FOR CATGRP = 1 TO 1
        CUR.SEL = "SELECT FBNK.CURRENCY WITH @ID EQ EGP USD EUR "
        CALL EB.READLIST(CUR.SEL,CUR.LIST,"",SELECTED.CUR,ER.MSG.CUR)
        FOR AA = 1 TO SELECTED.CUR
            CUR.CODE = CUR.LIST<AA>
            CURR1 = CUR.CODE
            GOSUB OPEN.FILES
            GOSUB SEL.LISTS
            MM = DCOUNT(XXA.H,@FM)
**##            CRT "MM=":MM
            FOR LL1 = 1 TO MM
                SS = DCOUNT(XXA<LL1>,@VM)
**##                CRT "SS=":SS
                FOR LLS = 1 TO SS
                    GOSUB PROCESS
                NEXT LLS
            NEXT LL1
        NEXT AA
    NEXT CATGRP
*---------------------------------------
*        CUR1.SEL = "SELECT FBNK.CURRENCY WITH @ID NE EGP USD EUR "
*        CALL EB.READLIST(CUR1.SEL,CUR1.LIST,"",SELECTED.CUR1,ER.MSG.CUR1)
*        FOR AA1 = 1 TO SELECTED.CUR1
*            CUR.CODE1 = CUR1.LIST<AA1>
*            CURR1 = CURR:" ":CUR.CODE1
*        NEXT AA
*    FOR CATGRP = 1 TO 4
*            GOSUB SEL.LISTS
*            GOSUB OPEN.FILES
*            MM = DCOUNT(XXA.H,@FM)
*            CRT "MM=":MM
*            FOR LL1 = 1 TO MM
*                SS = DCOUNT(XXA<LL1>,@VM)
*                CRT "SS=":SS
*                FOR LLS = 1 TO SS
*                    GOSUB PROCESS
*                NEXT SS
*            NEXT MM
*        NEXT AA
*    NEXT CATGRP
*---------------------------------------
RETURN
*==============================================================
OPEN.FILES:
*-----------
    FOLDER   = "&SAVEDLISTS&"
    FILENAME = "SBR.CBE.6000.":FMT(CATGRP,'R%2'):".":CUR.CODE:".CSV"
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    HEAD.DESC  = ",,,":" ������ ���������� �������� �������� �������� ":",":CURR1:","
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "����":","
    HEAD.DESC := "������ ������ �� ����� ����� ������":",,"
    HEAD.DESC := "������� ������� ���� �����":",,,,,"
    HEAD.DESC := ","
    HEAD.DESC := "������ ���� �����":","
    HEAD.DESC := "������ �������� ���� �����":","
    HEAD.DESC := "������ ������ �� ����� �����":",,"
    HEAD.DESC := "Report date ":LWD.DATE:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC2  = " ":","
    HEAD.DESC2 := "����":","
    HEAD.DESC2 := "��� ������ ������":","
    HEAD.DESC2 := "��� ������ �������":","
    HEAD.DESC2 := "���� ������":","
    HEAD.DESC2 := "��� ������ ������":","
    HEAD.DESC2 := "��� ������ ������":","
    HEAD.DESC2 := "���� ��� ����":","
    HEAD.DESC2 := "���� ��� ����":","
    HEAD.DESC2 := " ":","
    HEAD.DESC2 := " ":","
    HEAD.DESC2 := "����":","
    HEAD.DESC2 := "��� ������ ������":","

    BB.DATA = HEAD.DESC2
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------------------------------------
RETURN
*==============================================================
INITIATE:
*-----------
    DAT.ID = 'EG0010001'
*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,LWD.DATE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LWD.DATE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    LAST.MNTH = LWD.DATE
    CALL ADD.MONTHS(LAST.MNTH,'-1')
**##    CRT LWD.DATE

    CAT.GROUP  = ""
    XX.NEW.SEC = ""
    XX.OLD.SEC = ""
    XXA.H = ""
    XXA.R = ""
    XXA   = ""
    XX.CAT = ""
    CUS.NEW.SEC1 = ""
    CUS.EXP.SEC1 = ""
    XXA.H<1> = " ���� ������� ����� "
    XXA<1,1> = " ������ ����� ����� "
    XXA<1,2> = " ���� ����� ���� ��� "
    XXA<1,3> = " ���� ����� ��� ��� ��� ���� ����� "
    XXA<1,4> = " ���� ����� ��� ���� ����� "

    XXA.R<2> = " ������ ������� "
    XXA.H<2> = " ����� ������� "
    XXA<2,1> = " ���� ���� ������ ��������� ������ "
    XXA<2,2> = " ���� ����� ���� ����� "
    XXA<2,3> = " ���� ����� "
    XXA<2,4> = " ������ �������� "
    XXA<2,5> = " ���� ������ ����� ����� "
    XXA<2,6> = " ���� ���� "

    XXA<2,7> = " ����� ����� �� ���� ����� "
    XXA<2,8> = " ����� ������ ���� �� ��� "

    XXA.H<3> = " ���� ���� (1) "
    XXA<3,1> = " ������ ����� ����� "
    XXA<3,2> = " ���� ����� ���� ��� "
    XXA<3,3> = " ���� ����� ��� ��� ��� ���� ����� "
    XXA<3,4> = " ���� ����� ��� ���� ����� "

    XXA.H<4> = " ������ ���������� �������� �������� �������� ������� ������� ��������� ���� ������� "
    XXA<4,1> = " ��������� ����� ������� ������� ����� ��������� "
    XXA<4,2> = " ��������� ����� ���� ����� ������ ������ "
    XXA<4,3> = " ��������� ����� ����� �����"
    XXA<4,4> = " ��������� ����� ��� �����"
    XXA<4,5> = " ��������� ����� ����� ������"
    XXA<4,6> = " ��������� ������� ����� ���� "
    XXA<4,7> = " ��������� ���� ���� "

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*----------------------------------------------------------------------------------------------
    XX.NEW.SEC<1>  = " 1210 1220 1230 1240 1250 1260 2210 2220 2230 2240 2250 2260 3210 3220 3230 3240 3250 3260 4210 4220 4230 4240 4250 4260 "
    XX.NEW.SEC<2>  = " 4650 4700 4750 "
    XX.NEW.SEC<3>  = " 4650 4700 4750 "
    XX.NEW.SEC<4>  = " 4650 "
*--------------------------------------------------------------------------------------------------

    DR.CRNT.GRP  = " 1001 1002 1003 1011 1059 1102 1201 1202 1203 1205 1206 1207 1208 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1415 1416 1417 1418 1419 1420 1421 1477 1499 1501 1502 1503 1504 1507 1508 1509 1512 1513 1514 1516 1523 1524 1525 1534 1566 1577 1579 1588 1599 3005 3010 3011 3012 3013 3014 3017 6501 6502 6503 6504 6511 11231 11232 11234 11239 11240 11242 "
    LOANS.1Y.GRP = " 1214 1215 21066 "
    LOANS.3Y.GRP = " 1216 1414 1480 1481 1483 1484 1485 1493 1510 1511 1544 1558 1559 1560 1582 1591 21056 21057 21063 "
    LOANS.GT3Y.GRP = " 1211 1212 1217 1218 1220 1221 1222 1223 1224 1225 1227 1407 1408 1413 1445 1455 1518 1519 1595 1596 1597 1598 21050 21051 21052 21060 21062 "

    ALL.GROUPS1 = DR.CRNT.GRP:LOANS.1Y.GRP:LOANS.3Y.GRP:LOANS.GT3Y.GRP

    XX.CAT<1,1> = DR.CRNT.GRP ; CUS.NEW.SEC1<1,1> = XX.NEW.SEC<1>
    XX.CAT<1,2> = LOANS.1Y.GRP  ; CUS.NEW.SEC1<1,2> = XX.NEW.SEC<1>
    XX.CAT<1,3> = LOANS.3Y.GRP  ; CUS.NEW.SEC1<1,3> = XX.NEW.SEC<1>
    XX.CAT<1,4> = LOANS.GT3Y.GRP ; CUS.NEW.SEC1<1,4> = XX.NEW.SEC<1>

    XX.CAT<2,1> = ""
    XX.CAT<2,2> = ""
    XX.CAT<2,3> = " 1591 1560 " ; CUS.NEW.SEC1<2,3> = " 4650 "
    XX.CAT<2,4> = " 1205 1206 1207 1208 11231 11232 11234 11239 11240 11242 " ; CUS.NEW.SEC1<2,4> = " 4650 "
    XX.CAT<2,5> = " 1225 1597 1414 1404 21063 " ; CUS.NEW.SEC1<2,5> = " 4650 "
    XX.CAT<2,6> = " 1001 1002 1003 1011 1059 1102 1201 1202 1203 1301 1302 1303 1377 1390 1399 1401 1402 1405 1406 1415 1416 1417 1418 1419 1420 1421 1477 1499 1501 1502 1503 1504 1507 1508 1509 1512 1513 1514 1516 1523 1524 1525 1534 1566 1577 1579 1588 1599 3005 3010 3011 3012 3013 3014 3017 6501 6502 6503 6504 6511  1214 1215 21066 1216 1480 1481 1483 1484 1485 1493 1510 1511 1544 1558 1559 1582 21056 21057 1211 1212 1217 1218 1220 1221 1222 1223 1224 1227 1407 1408 1413 1445 1455 1518 1519 1595 1596 1598 21050 21051 21052  " ; CUS.NEW.SEC1<2,6> = " 4650 "
    XX.CAT<2,7> = " 1001 1002 1003 1011 1059 1102 1201 1202 1203 1205 1206 1207 1208 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1415 1416 1417 1418 1419 1420 1421 1477 1499 1501 1502 1503 1504 1507 1508 1509 1512 1513 1514 1516 1523 1524 1525 1534 1566 1577 1579 1588 1599 3005 3010 3011 3012 3013 3014 3017 6501 6502 6503 6504 6511 11231 11232 11234 11239 11240 11242  1214 1215 21066 1216 1414 1480 1481 1483 1484 1485 1493 1510 1511 1544 1558 1559 1560 1582 1591 21056 21057 21063 1211 1212 1217 1218 1220 1221 1222 1223 1224 1225 1227 1407 1408 1413 1445 1455 1518 1519 1595 1596 1597 1598 21050 21051 21052  " ; CUS.NEW.SEC1<2,7> = " 4700 "
    XX.CAT<2,8> = " 1001 1002 1003 1011 1059 1102 1201 1202 1203 1205 1206 1207 1208 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1415 1416 1417 1418 1419 1420 1421 1477 1499 1501 1502 1503 1504 1507 1508 1509 1512 1513 1514 1516 1523 1524 1525 1534 1566 1577 1579 1588 1599 3005 3010 3011 3012 3013 3014 3017 6501 6502 6503 6504 6511 11231 11232 11234 11239 11240 11242  1214 1215 21066 1216 1414 1480 1481 1483 1484 1485 1493 1510 1511 1544 1558 1559 1560 1582 1591 21056 21057 21063 1211 1212 1217 1218 1220 1221 1222 1223 1224 1225 1227 1407 1408 1413 1445 1455 1518 1519 1595 1596 1597 1598 21050 21051 21052  " ; CUS.NEW.SEC1<2,8> = " 4750 "

    XX.CAT<3,1> = " 1001 1002 1003 1011 1059 1102 1201 1202 1203 1205 1206 1207 1208 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1415 1416 1417 1418 1419 1420 1421 1477 1499 1501 1502 1503 1504 1507 1508 1509 1512 1513 1514 1516 1523 1524 1525 1534 1566 1577 1579 1588 1599 3005 3010 3011 3012 3013 3014 3017 6501 6502 6503 6504 6511 11231 11232 11234 11239 11240 11242 " ; CUS.EXP.SEC1<3,1> = " 4650 4700 4750 ":XX.NEW.SEC<1>
    XX.CAT<3,2> = " 1214 1215 21066 " ; CUS.EXP.SEC1<3,2> = " 4650 4700 4750 ":XX.NEW.SEC<1>
    XX.CAT<3,3> = " 1216 1414 1480 1481 1483 1484 1485 1493 1510 1511 1544 1558 1559 1560 1582 1591 21056 21057 21063 " ; CUS.EXP.SEC1<3,3> = " 4650 4700 4750 ":XX.NEW.SEC<1>
    XX.CAT<3,4> = " 1211 1212 1217 1218 1220 1221 1222 1223 1224 1225 1227 1407 1408 1413 1445 1455 1518 1519 1595 1596 1597 1598 21050 21051 21052 21060 21062 " ; CUS.EXP.SEC1<3,4> = " 4650 4700 4750 ":XX.NEW.SEC<1>

    XX.CAT<4,1> = " 1206 1208 1401 1405 1406 11240 11232 11234 11231 11239 1480 1481 1499 1483 1493 11242 1477 1218 21066 1421 1484 1485 1419 1420 1417 1418 " ; CUS.NEW.SEC1<4,1> = " 4650 "
    XX.CAT<4,2> = " 1415 " ; CUS.NEW.SEC1<4,2> = " 4650 "
    XX.CAT<4,3> = " 1404 21063 1414 1597 1225 " ; CUS.NEW.SEC1<4,3> = " 4650 "
    XX.CAT<4,4> = " 1202 1203 21057 1212 1598 21050 1221 " ; CUS.NEW.SEC1<4,4> = " 4650 "
    XX.CAT<4,5> = " 1102 1416 1214 1223 1509 " ; CUS.NEW.SEC1<4,5> = " 4650 "
    XX.CAT<4,6> = " 1402 3005 3010 3017 3014 3013 3012 3011 21060 1211 21056 1596 21051 1224 1222 1227 1301 1302 1303 1377 1390 1399 1201 " ; CUS.NEW.SEC1<4,6> = " 4650 "
    XX.CAT<4,7> = " 1502 1503 1501 1560 1566 1577 1588 1599 1591 1516 1523 1524 1205 1207 1525 1504 1507 1508 1510 1511 1512 1579 1513 1514 1534 1544 1559 1582 1558 21062 1216 1217 1518 1519 1215 1595 21052 1220 1001 1002 1003 1059 1011 1407 1413 1408 1445 6501 1455 6501 1455 6502 6503 6504 6511 " ; CUS.NEW.SEC1<4,7> = " 4650 "

    ALL.CATT = " "
    MMR = DCOUNT(XX.CAT,@FM)
**##    CRT "MMR=":MMR
    FOR LL1R = 1 TO MMR
        SSR = DCOUNT(XX.CAT<LL1R>,@VM)
**##        CRT "SSR=":SSR
        FOR LLSR = 1 TO SSR
            ALL.CATT := XX.CAT<LL1R,LLSR>:" "
        NEXT LLSR
    NEXT LL1R

    GOSUB CLEAR.VAR
RETURN
*----------------------------------------------------------------
CLEAR.VAR:
*-------------
    KK             = 0
    LL1            = 0
    ACC.RATE       = 0
    LD.DATE        = 0
    CBE.RATE       = 0
    ACCMNTH        = 0
    ACLMNTH        = 0
    TOT.LMNTH.VAL  = 0
    TOT.LMNTH.INT  = 0
    TOT.CMNTH.USED = 0
    TOT.CMNTH.INT  = 0
    TOT.SMP.RATE   = 0
    AVR.CMNTH.RATE = 0
    AVR.LMNTH.RATE = 0
    Y.EMNTH.VAL    = 0
    Y.EMNTH.RATE   = 0
    TOT.EMNTH.VAL  = 0
    TOT.EMNTH.INT  = 0
    AVR.EMNTH.RATE = 0
    Y.LMNTH.VAL    = 0
    Y.LMNTH.RATE   = 0
    Y.CMNTH.USED   = 0
    Y.CMNTH.RATE   = 0
    Y.SMP.RATE     = 0
    Y.MIN.RATE     = 0
    Y.MAX.RATE     = 0
    Y.MNTH.PAIED   = 0
    Y.CNCL.DPT     = 0
    Y.EMNTH.VAL    = 0
    Y.EMNTH.RATE   = 0
    Y.MNTH.DB      = 0
    Y.DR.CNT       = 0
    TOT.DR.CNT     = 0
    TOT.MNTH.PAIED = 0
*-------------------------------------------------------------
    R.AGR.LIST     = ""
    R.IND.LIST     = ""
    R.TRD.LIST     = ""
    R.SRV.LIST     = ""
*-------------------------------------------------------------
RETURN
*========================================================================
SEL.LISTS:
*-------

    CBE.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":LWD.DATE[1,6]:" OR CBEM.DATE EQ ":LAST.MNTH[1,6]:" )"
    CBE.SEL := " AND CBEM.CY IN ( ":CURR1:" ) "
    CBE.SEL := " AND CBEM.CATEG IN ( ":ALL.CATT:" )"
    CBE.SEL := " AND ( CBEM.IN.LCY LT 0 OR CBEM.IN.LCYO LT 0 )"
    CBE.SEL := " AND ( CBEM.IN.LCY NE '' AND CBEM.IN.LCYO NE '' )"
    CALL EB.READLIST(CBE.SEL,KEY.CBE.LIST,"",SELECTED.CBE,ER.MSG.CBE)
**##    CRT CBE.SEL
**##    CRT SELECTED.CBE
RETURN
**************************************************************
CALLDB:
*------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC.H = 'FBNK.ACCOUNT$HIS' ; F.ACC.H = ''
    CALL OPF(FN.ACC.H,F.ACC.H)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = ''
    CALL OPF(FN.ADI,F.ADI)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACT = ''
    CALL OPF(FN.ACT,F.ACT)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    FN.INT = 'FBNK.BASIC.INTEREST' ; F.INT = ''
    CALL OPF(FN.INT,F.INT)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    FN.LMM = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.LMM = '' ; R.LMM = '' ; ER.LMM = ''
    CALL OPF(FN.LMM,F.LMM)

    FN.LMM.H = 'FBNK.LMM.ACCOUNT.BALANCES.HIST' ; F.LMM.H = '' ; R.LMM.H = '' ; ER.LMM.H = ''
    CALL OPF(FN.LMM.H,F.LMM.H)

    FN.CBE = 'F.CBE.STATIC.AC.LD.DETAIL' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

RETURN
*========================================================================
PROCESS:
*-------
    CBE.LIST = KEY.CBE.LIST
    LOOP
        REMOVE CBE.ID FROM CBE.LIST SETTING POS.CBE.1
    WHILE CBE.ID:POS.CBE.1
        CALL F.READ(FN.CBE,CBE.ID,R.CBE,F.CBE,ERR.CBE)
        CBE.NEW.SEC     = R.CBE<STD.CBEM.NEW.SECTOR>
        CBE.OLD.SEC     = R.CBE<STD.CBEM.OLD.SECTOR>
        CBE.CAT         = R.CBE<STD.CBEM.CATEG>
        CBE.DATE        = R.CBE<STD.CBEM.DATE>
        CBE.AMT         = R.CBE<STD.CBEM.IN.LCY>
        CBE.PREV.AMT    = R.CBE<STD.CBEM.IN.LCYO>
        CBE.CUR         = R.CBE<STD.CBEM.CY>
        IF CBE.CUR NE CURR1 THEN GOTO NXT.CBE.REC ELSE NULL
        IF CBE.CUR NE LCCY THEN
            CBE.AMT         = R.CBE<STD.CBEM.IN.FCY>
            CBE.PREV.AMT    = R.CBE<STD.CBEM.IN.FCYO>
        END
        IF CBE.DATE EQ LAST.MNTH[1,6] THEN
            CBE.PREV.AMT = CBE.AMT
            CBE.AMT      = 0
        END
        IF CBE.AMT EQ '' THEN CBE.AMT = 0 ELSE NULL
        IF CBE.PREV.AMT EQ '' THEN CBE.PREV.AMT = 0 ELSE NULL
        IF CBE.AMT GE 0 AND CBE.PREV.AMT GE 0 THEN GOTO NXT.CBE.REC ELSE NULL

        FINDSTR CBE.CAT:" " IN XX.CAT<LL1,LLS> SETTING POS.CBE.CAT THEN NULL ELSE GOTO NXT.CBE.REC
        IF CBE.NEW.SEC EQ '' THEN GOTO NXT.CBE.REC ELSE NULL
        IF CUS.NEW.SEC1<LL1,LLS> NE '' THEN

            FINDSTR CBE.NEW.SEC:" "  IN CUS.NEW.SEC1<LL1,LLS> SETTING POS.XX.H THEN NULL ELSE GOTO NXT.CBE.REC
        END
        FINDSTR CBE.NEW.SEC:" "  IN CUS.EXP.SEC1<LL1,LLS> SETTING POS.XX.E THEN GOTO NXT.CBE.REC ELSE NULL
        IF CBE.PREV.AMT EQ '' THEN CBE.PREV.AMT = 0 ELSE NULL
        IF CBE.ID[1,2] EQ 'LD' THEN
            LD.ID = CBE.ID
            GOSUB LD.REC
            CBE.RATE   = LD.RATE
            CBE.RATE.O = LD.RATE.O
        END ELSE
            ACC.ID = CBE.ID
            GOSUB AC.REC
            CBE.RATE   = ACC.RATE
            CBE.RATE.O = ACC.RATE.O
        END
        Y.LMNTH.VAL  = CBE.PREV.AMT
        Y.LMNTH.RATE = CBE.RATE.O
        Y.CMNTH.USED = Y.MNTH.DB
        Y.CMNTH.RATE = CBE.RATE
        Y.EMNTH.VAL  = Y.LMNTH.VAL + Y.CMNTH.USED + Y.MNTH.PAIED + Y.CNCL.DPT
        Y.EMNTH.RATE = CBE.RATE
        TOT.MNTH.PAIED += Y.MNTH.PAIED

**##        CRT CBE.ID:" * ":Y.LMNTH.VAL:" * ":Y.CMNTH.USED:" * ":Y.MNTH.PAIED:" * ":Y.EMNTH.VAL:" * ":Y.CNCL.DPT
        TOT.LMNTH.VAL  += Y.LMNTH.VAL
        TOT.LMNTH.INT  += (Y.LMNTH.VAL * Y.LMNTH.RATE)/100
        TOT.CMNTH.USED += Y.CMNTH.USED
        TOT.CMNTH.INT  += (Y.CMNTH.USED * Y.CMNTH.RATE / 100 )
        TOT.SMP.RATE   += Y.CMNTH.RATE
        TOT.EMNTH.VAL  += Y.EMNTH.VAL
        TOT.EMNTH.INT  += (Y.EMNTH.VAL * Y.EMNTH.RATE)/100
        IF Y.MAX.RATE EQ 0 OR Y.MAX.RATE EQ ''  THEN
            Y.MAX.RATE = Y.CMNTH.RATE
        END
        IF Y.MIN.RATE EQ 0 OR Y.MIN.RATE EQ ''  THEN
            Y.MIN.RATE = Y.CMNTH.RATE
        END
        IF Y.CMNTH.RATE NE '' AND Y.CMNTH.RATE NE 0 THEN
            IF Y.MAX.RATE LT Y.CMNTH.RATE THEN
                Y.MAX.RATE = Y.CMNTH.RATE
            END
            IF Y.MIN.RATE GT Y.CMNTH.RATE AND Y.MIN.RATE NE '' AND Y.MIN.RATE NE 0 THEN
                Y.MIN.RATE = Y.CMNTH.RATE
            END
        END
        TOT.DR.CNT += Y.DR.CNT
NXT.CBE.REC:
    REPEAT
    GOSUB CALC.TOT
    GOSUB DATA.FILE
RETURN
*--------------------------------------------------------------------------------
CALC.TOT:
*-------------
    IF TOT.LMNTH.VAL NE '' AND TOT.LMNTH.VAL NE 0 THEN
        AVR.LMNTH.RATE = TOT.LMNTH.INT / TOT.LMNTH.VAL * 100
    END
    IF TOT.CMNTH.USED NE '' AND TOT.CMNTH.USED NE 0 THEN
        AVR.CMNTH.RATE = TOT.CMNTH.INT / TOT.CMNTH.USED * 100
    END
    IF ACCMNTH NE '' AND ACCMNTH NE 0 THEN
        Y.SMP.RATE = TOT.SMP.RATE / ACCMNTH * 100
    END
    IF TOT.EMNTH.VAL NE '' AND TOT.EMNTH.VAL NE 0 THEN
        AVR.EMNTH.RATE = TOT.EMNTH.INT / TOT.EMNTH.VAL * 100
    END
    IF Y.MNTH.PAIED LT 0 THEN
        Y.MNTH.PAIED = 0
    END
RETURN
*--------------------------------------------------------------------------------
*---------------------- ACC.REC --------------------------------------
AC.REC:
*------
    ACC.ID = CBE.ID
    CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ER.ACC)
    IF ER.ACC THEN
        CALL F.READ.HISTORY(FN.ACC.H,ACC.ID,R.ACC,F.ACC.H,ER.ACC.H)
        ACC.ID = FIELD(ACC.ID,';',1)
        IF ER.ACC.H THEN GOTO NXT.ACREC
    END
    AC.CAT = R.ACC<AC.CATEGORY>
    AC.CUR = R.ACC<AC.CURRENCY>
    AC.CUS.ID = R.ACC<AC.CUSTOMER>
    AC.BAL = R.ACC<AC.ONLINE.ACTUAL.BAL>
    AC.OP.DAT = R.ACC<AC.OPENING.DATE>
    AC.GDI = R.ACC<AC.CONDITION.GROUP>
*Line [ 503 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AC.ADI.COUNT = DCOUNT(R.ACC<AC.ACCT.DEBIT.INT>,@VM)
    ADI.DAT = R.ACC<AC.ACCT.DEBIT.INT><1,AC.ADI.COUNT>
    IF ADI.DAT LE LAST.MNTH THEN
        ADI.PRV.DAT = ADI.DAT
    END ELSE
        FOR ADX = AC.ADI.COUNT TO 1 STEP -1
            LDAT = R.ACC<AC.ACCT.DEBIT.INT><1,ADX>
            IF LDAT LE LAST.MNTH THEN
                ADX = 1
                ADI.PRV.DAT = LDAT
            END
        NEXT ADX
    END
    IF ADI.DAT EQ ADI.PRV.DAT  THEN
        GOSUB GET.ADI.RATE
        IF ACC.RATE EQ '' THEN GOSUB GET.FLOATED ELSE NULL
        ACC.RATE.O = ACC.RATE
    END ELSE
        ORG.ADI.DAT = ADI.DAT
        ADI.DAT = ADI.PRV.DAT
        GOSUB GET.ADI.RATE
        IF ACC.RATE EQ '' THEN GOSUB GET.FLOATED ELSE NULL
        ACC.RATE.O = ACC.RATE
        ADI.DAT = ORG.ADI.DAT
        GOSUB GET.ADI.RATE
        IF ACC.RATE EQ '' THEN GOSUB GET.FLOATED ELSE NULL
    END
    IF AC.OP.DAT LE LAST.MNTH THEN
        ACLMNTH++
    END ELSE
        ACCMNTH++
    END
    GOSUB GET.MNTH.PAIED
    Y.MNTH.PAIED = TOT.PD
    Y.MNTH.DB    = TOT.DB
    Y.DR.CNT     = DR.CNT
NXT.ACREC:
RETURN
**********\\\\\\\\--------------------------------------------/////////**********
*------------------------LD.REC---------------------------
LD.REC:
*-----------
    LD.ID = CBE.ID
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
    IF ER.LD THEN
        CALL F.READ.HISTORY(FN.LD.H,LD.ID,R.LD,F.LD.H,ER.LD.H)
        LD.ID = FIELD(LD.ID,';',1)
    END
    LD.CAT  = R.LD<LD.CATEGORY>
    LD.CUS  = R.LD<LD.CUSTOMER.ID>
    LD.VAL  = R.LD<LD.VALUE.DATE>
    LD.AMT  = R.LD<LD.REIMBURSE.AMOUNT>
    LD.RATE = R.LD<LD.INTEREST.RATE> + R.LD<LD.INTEREST.SPREAD>
    LD.RATE.O = LD.RATE
NXT.LD:
********************************
RETURN
*************************************************************
GET.MNTH.PAIED:
*-------------
    TOT.PD = 0
    TOT.DB = 0
    DR.CNT = 0
    ACT.ID = ACC.ID:"-":LWD.DATE[1,6]
    CALL F.READ(FN.ACT,ACT.ID,R.ACT,F.ACT,ER.ACT1)
    IF NOT(ER.ACT1) THEN
        ACT.DYN = R.ACT<IC.ACT.BK.DAY.NO>
        ACT.TCR = R.ACT<IC.ACT.BK.CREDIT.MVMT>
        ACT.TDB = R.ACT<IC.ACT.BK.DEBIT.MVMT>
        PPP = DCOUNT(ACT.DYN,@VM)
        DR.CNT = DCOUNT(ACT.TDB,@VM)
        FOR DYN1 = 1 TO PPP
            TOT.PD += ACT.TCR<1,DYN1>
            TOT.DB += ACT.TDB<1,DYN1>
        NEXT DYN1
    END
RETURN
*************************************************************
GET.CUS.SEC:
*-------------
    AC.CUS.NEW.SEC  = ""
    AC.CUS.OLD.SEC  = ""
    CALL F.READ(FN.CUS,AC.CUS.ID,R.CUS,F.CUS,ER.CUS)
    AC.CUS.OLD.SEC  = R.CUS<EB.CUS.SECTOR>
    AC.CUS.LCL      = R.CUS<EB.CUS.LOCAL.REF>
    AC.CUS.NEW.SEC  = AC.CUS.LCL<1,CULR.NEW.SECTOR>
    AC.CUS.CRD.CODE = AC.CUS.LCL<1,CULR.CREDIT.CODE>
RETURN
*************************************************************
GET.LOCAL.RATE:
*-------------
*Line [ 602 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SBD.CURRENCY':@FM:SBD.CURR.MID.RATE,MM.CURRENCY,MM.CUR.RATE)
F.ITSS.SBD.CURRENCY = 'F.SBD.CURRENCY'
FN.F.ITSS.SBD.CURRENCY = ''
CALL OPF(F.ITSS.SBD.CURRENCY,FN.F.ITSS.SBD.CURRENCY)
CALL F.READ(F.ITSS.SBD.CURRENCY,MM.CURRENCY,R.ITSS.SBD.CURRENCY,FN.F.ITSS.SBD.CURRENCY,ERROR.SBD.CURRENCY)
MM.CUR.RATE=R.ITSS.SBD.CURRENCY<SBD.CURR.MID.RATE>

    IF MM.CURRENCY EQ 'JPY' THEN MM.CUR.RATE = MM.CUR.RATE/100 ELSE NULL
RETURN
*************************************************************
GET.FLOATED:
*-----------
    GDI.ID.LK = AC.GDI:AC.CUR:"..."
    G.SEL = "SSELECT ":FN.GDI:" WITH @ID LIKE ":GDI.ID.LK
    CALL EB.READLIST(G.SEL,G.LIST,'',SELECTED.G,ER.MSG.G)
    GDI.ID = G.LIST<SELECTED.G>
    CALL F.READ(FN.GDI,GDI.ID,R.GDI,F.GDI,ERR.G1)
    GDI.BRATE = R.GDI<IC.GDI.DR.BASIC.RATE,1>
    GDI.INT.R = R.GDI<IC.GDI.DR.INT.RATE,1>
    GDI.MRG.O = R.GDI<IC.GDI.DR.MARGIN.OPER,1>
    GDI.MRG.R = R.GDI<IC.GDI.DR.MARGIN.RATE,1>
    IF GDI.INT.R EQ '' THEN
        INT.ID.LK = GDI.BRATE:AC.CUR:"..."
        I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
        CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
        INT.ID = I.LIST<SELECTED.I>
        CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
        INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
        ACC.RATE = INT.RATE
    END ELSE
        ACC.RATE = GDI.INT.R
    END
    IF GDI.MRG.O EQ 'ADD' THEN
        ACC.RATE = ACC.RATE + GDI.MRG.R
    END
    IF GDI.MRG.O EQ 'SUBTRACT' THEN
        ACC.RATE = ACC.RATE - GDI.MRG.R
    END
    IF GDI.MRG.O EQ 'MULTIBLY' THEN
        ACC.RATE = ACC.RATE * GDI.MRG.R
    END
RETURN
*************************************************************
GET.ADI.RATE:
*-----------
    ACC.RATE = ''
    ADI.ID = ACC.ID:"-":ADI.DAT
    CALL F.READ(FN.ADI,ADI.ID,R.ADI,F.ADI,EADD)
    ADI.INT.R = R.ADI<IC.ADI.DR.INT.RATE,1>
    ADI.BRATE = R.ADI<IC.ADI.DR.BASIC.RATE,1>
    ADI.MRG.O = R.ADI<IC.ADI.DR.MARGIN.OPER,1>
    ADI.MRG.R = R.ADI<IC.ADI.DR.MARGIN.RATE,1>
    IF ADI.INT.R EQ '' THEN
        INT.ID.LK = ADI.BRATE:AC.CUR:"..."
        I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
        CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
        INT.ID = I.LIST<SELECTED.I>
        CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
        INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
        ACC.RATE = INT.RATE
    END ELSE
        ACC.RATE = ADI.INT.R
    END
    IF ADI.MRG.O EQ 'ADD' THEN
        ACC.RATE = ACC.RATE + ADI.MRG.R
    END
    IF ADI.MRG.O EQ 'SUBTRACT' THEN
        ACC.RATE = ACC.RATE - ADI.MRG.R
    END
    IF ADI.MRG.O EQ 'MULTIBLY' THEN
        ACC.RATE = ACC.RATE * ADI.MRG.R
    END
RETURN
*---------------------------------------------------
******************* WRITE DATA IN FILE **********************
DATA.FILE:
*---------

    IF LLS EQ 1 THEN
        BB.DATA  = XXA.H<LL1>:","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    END
    BB.DATA  = XXA<LL1,LLS>:","
    BB.DATA := FMT((ABS(TOT.LMNTH.VAL)/1000),"L0"):","
    BB.DATA := AVR.LMNTH.RATE:","
    BB.DATA := TOT.DR.CNT:","
    BB.DATA := FMT((ABS(TOT.CMNTH.USED)/1000),"L0"):","
    BB.DATA := AVR.CMNTH.RATE:","
    BB.DATA := Y.SMP.RATE:","
    BB.DATA := Y.MIN.RATE:","
    BB.DATA := Y.MAX.RATE:","
    BB.DATA := FMT((ABS(TOT.MNTH.PAIED)/1000),"L0"):","
    BB.DATA := Y.CNCL.DPT:","
    BB.DATA := FMT((ABS(TOT.EMNTH.VAL)/1000),"L0"):","
    BB.DATA := AVR.EMNTH.RATE:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**##    CRT BB.DATA
    KK = 0
    ACCMNTH = 0
    ACLMNTH = 0
    ACC.ALL.LIST = ""
    TOT.LMNTH.VAL  = 0
    TOT.LMNTH.INT  = 0
    TOT.CMNTH.USED = 0
    TOT.CMNTH.INT  = 0
    TOT.SMP.RATE = 0
    AVR.CMNTH.RATE = 0
    AVR.LMNTH.RATE = 0
    Y.EMNTH.VAL  = 0
    Y.EMNTH.RATE = 0
    TOT.EMNTH.VAL = 0
    TOT.EMNTH.INT = 0
    AVR.EMNTH.RATE = 0
    Y.LMNTH.VAL  = 0
    Y.LMNTH.RATE = 0
    Y.CMNTH.USED = 0
    Y.CMNTH.RATE = 0
    Y.SMP.RATE   = 0
    Y.MIN.RATE   = 0
    Y.MAX.RATE   = 0
    Y.MNTH.PAIED = 0
    Y.CNCL.DPT = 0
    Y.EMNTH.VAL    = 0
    Y.EMNTH.RATE   = 0
    Y.MNTH.DB      = 0
    Y.MNTH.PAIED      = 0
    Y.DR.CNT       = 0
    TOT.DR.CNT     = 0
    TOT.MNTH.PAIED = 0
*******************************
RETURN

************************************************
END
