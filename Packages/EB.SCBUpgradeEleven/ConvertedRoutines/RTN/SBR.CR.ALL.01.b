* @ValidationCode : MjoxNDM1NzQ2MjgxOkNwMTI1MjoxNjQ0OTI1MDY0MTgwOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>2636</Rating>
*-----------------------------------------------------------------------------
*******MAHMOUD 5/6/2012*******

SUBROUTINE SBR.CR.ALL.01

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT.REFERENCE
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT          ;*TF.LC.
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 67 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 69 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 71 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 73 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.GROUP
*Line [ 75 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 77 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY
*-------------------------------------------------------------------------

    HEAD.DESC  = ""

    LWD.DATE = R.DATES(EB.DAT.LAST.WORKING.DAY)
    TTXX = LWD.DATE
    CALL LAST.WDAY(TTXX)
*    IF LWD.DATE EQ TTXX THEN
    GOSUB INITIATE
    GOSUB PROCESS
*    END ELSE
*        CRT "NOT EOM NEXT DATE"
*    END
RETURN
*==============================================================
INITIATE:
*--------
    FOLDER   = "&SAVEDLISTS&"
    FILENAME = "SBR.CR.ALL.01.CSV"
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END


    HEAD.DESC  = "��� ������":","
    HEAD.DESC := "��� �������":","
    HEAD.DESC := "����� �������":","
    HEAD.DESC := "���� ���������":","
    HEAD.DESC := "��� ����":","
    HEAD.DESC := "���� ����":","
    HEAD.DESC := "���� ������ �������":","
    HEAD.DESC := "�������� �������":","
    HEAD.DESC := "���� ������ �������� ��":","
    HEAD.DESC := "�������� �������� ��":","
    HEAD.DESC := "���� �������":","
    HEAD.DESC := "���� �������":","
    HEAD.DESC := "���� ������� ����":","
    HEAD.DESC := "Total Overdue Amount":","
    HEAD.DESC := "Report date : ":LWD.DATE:","

    BB.DATA = HEAD.DESC

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.SCC = 'F.SCB.STATIC.CREDIT.CUSTOMER' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC1 = 'FBNK.ACCOUNT' ; F.ACC1 = ''
    CALL OPF(FN.ACC1,F.ACC1)

    FN.ADT = 'FBNK.ACCOUNT.DATE' ; F.ADT = ''
    CALL OPF(FN.ADT,F.ADT)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = ''
    CALL OPF(FN.ADI,F.ADI)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = '' ; R.CCY = ""
    CALL OPF(FN.CCY,F.CCY)

    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    FN.INT = 'FBNK.BASIC.INTEREST' ; F.INT = ''
    CALL OPF(FN.INT,F.INT)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD="" ; E.PD=""
    CALL OPF(FN.PD,F.PD)

    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    FN.IND.LCA = "FBNK.LC.APPLICANT"         ; F.IND.LCA = ""
    CALL OPF(FN.IND.LCA,F.IND.LCA)

    FN.IND.LCB = "FBNK.LC.BENEFICIARY"   ; F.IND.LCB = ""
    CALL OPF(FN.IND.LCB,F.IND.LCB)

    FN.IND.LI = "FBNK.LIMIT.LIABILITY" ; F.IND.LI = ""
    CALL OPF(FN.IND.LI,F.IND.LI)

    FN.LIM = "FBNK.LIMIT" ; F.LIM = "" ; R.LIM = ''  ; ER.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.LI = "FBNK.LIMIT" ; F.LI = "" ; R.LI = ''  ; ER.LI = ''
    CALL OPF(FN.LI,F.LI)

    FN.CUST.COL = "FBNK.COLLATERAL.RIGHT.CUST"   ; F.CUST.COL = "" ; R.CUST.COL = "" ; ER.CUST.COL = ""
    CALL OPF(FN.CUST.COL,F.CUST.COL)

    FN.COL   = "FBNK.COLLATERAL"         ; F.COL  = '' ;  R.COL = '' ; ER.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)



    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST.PD="" ; SELECTED.PD="" ; ER.MSG.PD=""

    OPER.ID  = ''
    CAT.ID   = ''
    CAT.NAME = ''
    CUR.ID   = ''
    LI.ID    = ''
    LIMIT.CURR = ''
    ONLINE.LIMIT.AMT = 0
    USED.AMT  = 0
    PD.AMT = 0
    LIMIT.AMT.LCY = 0
    USED.AMT.LCY  = 0
    COLL.AMT = 0
    WS.NEXT.DR = ''
    LI.SEC.AMT    = 0
    COL.CY = ''
    LI.SEC.AMT.LCY = 0
    KK  = 0
    AA1 = 0
    MM.CURRENCY = ''
    LD.CATEG = " 21096 21050 21051 21052 21053 21054 21055 21056 21057 21058 21060 21061 21062 21063 21064 21065 21066 21067 21068 21069 21070 21071 21072 21073 21074 21078 21079"
    BANNED.CATEG = "1021 1022 1701 1710 1711 1721 5000 5001 5081 5082 5083 5084 5085 5090 5091 1021 1022 5079 5080 1589"
    OVERDUE.AMT = ""
****
RETURN
*========================================================================
PROCESS:
*-------
    T.SEL  = "SSELECT ":FN.CUS:" WITH POSTING.RESTRICT LT 90 "
*###    T.SEL := " AND COMPANY.BOOK EQ EG0010013"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CRT SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,E1)
            CUS.ID = KEY.LIST<I>
            CUS.LCL = R.CUS<EB.CUS.LOCAL.REF>
            CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
            CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
            CUST.NAME = CUS.LCL<1,CULR.ARABIC.NAME>
*##            IF CRD.STA NE '' OR CRD.COD GE 110 THEN NULL ELSE
            GOSUB AC.REC
            GOSUB LG.REC
            GOSUB PD.REC
            GOSUB LC.REC
*##            END
        NEXT I
    END
RETURN
*------------------------------------------------------------
AC.REC:
*-------
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
    LOOP
        REMOVE ACC.ID FROM R.CUS.ACC SETTING POS.AC
    WHILE ACC.ID:POS.AC
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,E1)
        AC.CAT = R.ACC<AC.CATEGORY>
        AC.BAL = R.ACC<AC.OPEN.ACTUAL.BAL>
        AC.LIM = R.ACC<AC.LIMIT.REF>
***##        IF (AC.CAT GE 1100 AND AC.CAT LE 1599) OR (AC.CAT GE 3005 AND AC.CAT LE 3017) THEN
***##            IF AC.CAT NE 1002 AND AC.CAT NE 1407 AND AC.CAT NE 1408 AND AC.CAT NE 1413 AND AC.CAT NE 1445 AND AC.CAT NE 1455 THEN
        IF AC.BAL LT 0 OR ((AC.CAT GE 3005 AND AC.CAT LE 3017) OR AC.CAT = 3065) OR AC.CAT EQ 3060 OR AC.LIM NE '' THEN

            FINDSTR AC.CAT:" " IN BANNED.CATEG SETTING POS.NOSHOW THEN NULL ELSE
                AC.CUR = R.ACC<AC.CURRENCY>
                AC.GDI = R.ACC<AC.CONDITION.GROUP>
                IF AC.LIM THEN
                    CHK.LIM = FMT(FIELD(AC.LIM,".",1),'R%7')
                    LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(AC.LIM,".",2)
                    GOSUB LIM.REC
                END ELSE
                    WS.CY.ID = ''
                    LI.ONL.AMT = 0
                END
***********************************************
                OPER.ID            = CUS.ID
                CAT.ID             = AC.CAT
                CUR.ID             = AC.CUR
                LIMIT.CURR         = WS.CY.ID
                ONLINE.LIMIT.AMT   = LI.ONL.AMT
                USED.AMT           = AC.BAL
*----------------------
                LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                IF WS.CY.ID NE '' THEN
*                    CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                    IF LIMIT.CURR NE 'EGP' THEN
                        MM.CURRENCY = LIMIT.CURR
                        GOSUB GET.LOCAL.RATE
                        LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                    END
                END
                USED.AMT.LCY      = USED.AMT
*                CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                IF CUR.ID NE 'EGP' THEN
                    MM.CURRENCY = CUR.ID
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                END

*----------------------
                GOSUB DATA.FILE
            END
        END
    REPEAT
******************************
RETURN
*************************************************************
LG.REC:
*------
    CALL F.READ(FN.IND.LD,CUS.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
            REC.STA = R.LD<LD.STATUS>
            LD.CAT  = R.LD<LD.CATEGORY>
            IF REC.STA NE 'LIQ' THEN
*## IF LD.CAT EQ 21096 OR LD.CAT EQ 21050 OR LD.CAT EQ 21051 OR LD.CAT EQ 21052 OR LD.CAT EQ 21056 OR LD.CAT EQ 21057 OR LD.CAT EQ 21060 OR LD.CAT EQ 21062 OR LD.CAT EQ 21063 OR LD.CAT EQ 21066 THEN
                FINDSTR LD.CAT:" " IN LD.CATEG SETTING POSS.LD.CATEG THEN
                    LD.CUR = R.LD<LD.CURRENCY>
                    LD.AMT = R.LD<LD.AMOUNT>
                    LD.LIM = R.LD<LD.LIMIT.REFERENCE>
                    IF LD.LIM THEN
                        CHK.LIM = FMT(FIELD(LD.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LD.LIM,".",2)
                        GOSUB LIM.REC
                    END ELSE
                        WS.CY.ID = ''
                        LI.ONL.AMT = 0
                    END
                    OPER.ID = CUS.ID
                    CAT.ID  = LD.CAT
                    CUR.ID  = LD.CUR
                    LIMIT.CURR         = WS.CY.ID
                    ONLINE.LIMIT.AMT   = LI.ONL.AMT
                    USED.AMT           = LD.AMT
*----------------------
                    LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                    IF WS.CY.ID NE '' THEN
*                        CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                        IF LIMIT.CURR NE 'EGP' THEN
                            MM.CURRENCY = LIMIT.CURR
                            GOSUB GET.LOCAL.RATE
                            LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                        END

                    END
                    USED.AMT.LCY      = USED.AMT
*                    CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                    IF CUR.ID NE 'EGP' THEN
                        MM.CURRENCY = CUR.ID
                        GOSUB GET.LOCAL.RATE
                        USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                    END
*----------------------
                    PD.ID = "PD":LD.ID
*                    CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,PD.ID,PD.AMT)
*######################
                    GOSUB DATA.FILE
                END
            END
        END
    REPEAT
RETURN
*************************************************************
PD.REC:
*------
    T.SEL.PD = "SELECT ":FN.PD:" WITH CATEGORY IN ( ":LD.CATEG:" ) AND CUSTOMER EQ ":CUS.ID
    CALL EB.READLIST(T.SEL.PD,KEY.LIST.PD,"",SELECTED.PD,ER.MSG.PD)
    IF SELECTED.PD THEN
        FOR J=1 TO SELECTED.PD
            CALL F.READ(FN.PD,KEY.LIST.PD<J>,R.PD,F.PD,E.PD)
            OVERDUE.AMT = R.PD<PD.TOTAL.OVERDUE.AMT>
            IF OVERDUE.AMT NE "" OR OVERDUE.AMT NE "0" THEN
*              DEBUG
                OPER.ID = CUS.ID
                CAT.ID  = R.PD<PD.CATEGORY>
                CUR.ID  = R.PD<PD.CURRENCY>
                PD.AMT = OVERDUE.AMT
                GOSUB DATA.FILE
            END
            OVERDUE.AMT = ""
*Line [ 396 ] NEXT I to NEXT J - ITSS - R21 Upgrade - 2021-12-26
*NEXT I
        NEXT J
    END


RETURN
*************************************************************
LC.REC:
*------
    CALL F.READ(FN.IND.LCA,CUS.ID,R.IND.LCA,F.IND.LCA,ER.IND.LCA)
    LOOP
        REMOVE LCA.ID FROM R.IND.LCA SETTING POS.LCA
    WHILE LCA.ID:POS.LCA
        CALL F.READ(FN.LC,LCA.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            LC.CAT  = R.LC<TF.LC.CATEGORY.CODE>
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                LC.CUR  = R.LC<TF.LC.LC.CURRENCY>
                LC.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                LC.LIM  = R.LC<TF.LC.LIMIT.REFERENCE>
*-------
                IF LC.LIM THEN
                    CHK.LIM = FMT(FIELD(LC.LIM,".",1),'R%7')
                    LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LC.LIM,".",2)
                    GOSUB LIM.REC
                END ELSE
                    WS.CY.ID = ''
                    LI.ONL.AMT = 0
                END
                OPER.ID     = CUS.ID
                CAT.ID      = LC.CAT
                CUR.ID      = LC.CUR
                LIMIT.CURR         = WS.CY.ID
                ONLINE.LIMIT.AMT   = LI.ONL.AMT
                USED.AMT           = LC.AMT
*----------------------
                LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                IF WS.CY.ID NE '' THEN
*                    CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                    IF LIMIT.CURR NE 'EGP' THEN
                        MM.CURRENCY = LIMIT.CURR
                        GOSUB GET.LOCAL.RATE
                        LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                    END
                END
                USED.AMT.LCY      = USED.AMT
*                CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                IF CUR.ID NE 'EGP' THEN
                    MM.CURRENCY = CUR.ID
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                END
*----------------------

                GOSUB DATA.FILE
*-------
            END
            WS.DR.SUB.ID = LCA.ID
*########################################
            WS.SRL.DR = ''
            WS.NEXT.DR = WS.NEXT.DR + 0
            FOR IDR = 1 TO WS.NEXT.DR
                WS.SRL.ID = ( IDR + 100 )[2,2]
                WS.DR.ID = WS.DR.SUB.ID : WS.SRL.ID
                CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
                IF NOT(ER.DR) THEN
                    IF R.DR<TF.DR.MATURITY.REVIEW> GT LWD.DATE THEN
                        IF R.DR<TF.DR.DOCUMENT.AMOUNT> GT 0 THEN
                            DR.CUR   = R.DR<TF.DR.DRAW.CURRENCY>
                            DR.CAT   = LC.CAT
                            DR.AMT   = R.DR<TF.DR.DOCUMENT.AMOUNT>
                            DR.LIM   = R.DR<TF.DR.LIMIT.REFERENCE>
*------------
                            IF DR.LIM THEN
                                LIM.REF.ID = FIELD(DR.LIM,".",1)
                                CHK.LIM = FMT(FIELD(DR.LIM,".",1),'R%7')
                                LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                                GOSUB LIM.REC
                            END ELSE
                                WS.CY.ID = ''
                                LI.ONL.AMT = 0
                            END
                            OPER.ID = CUS.ID
                            CAT.ID  = DR.CAT
                            CUR.ID  = DR.CUR
                            LIMIT.CURR         = WS.CY.ID
                            ONLINE.LIMIT.AMT   = LI.ONL.AMT
                            USED.AMT           = DR.AMT
*----------------------
                            LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                            IF WS.CY.ID NE '' THEN
*                                CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                                IF LIMIT.CURR NE 'EGP' THEN
                                    MM.CURRENCY = LIMIT.CURR
                                    GOSUB GET.LOCAL.RATE
                                    LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                                END
                            END
                            USED.AMT.LCY      = USED.AMT
*                            CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                            IF CUR.ID NE 'EGP' THEN
                                MM.CURRENCY = CUR.ID
                                GOSUB GET.LOCAL.RATE
                                USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                            END
*----------------------
                            GOSUB DATA.FILE
                        END
                    END
                END
            NEXT IDR
*########################################
        END
    REPEAT

    CALL F.READ(FN.IND.LCB,CUS.ID,R.IND.LCB,F.IND.LCB,ER.IND.LCB)
    LOOP
        REMOVE LCB.ID FROM R.IND.LCB SETTING POS.LCB
    WHILE LCB.ID:POS.LCB
        CALL F.READ(FN.LC,LCB.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            LC.CAT  = R.LC<TF.LC.CATEGORY.CODE>
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                LC.CUR  = R.LC<TF.LC.LC.CURRENCY>
                LC.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                LC.LIM  = R.LC<TF.LC.LIMIT.REFERENCE>
*-------
                IF LC.LIM THEN
                    CHK.LIM = FMT(FIELD(LC.LIM,".",1),'R%7')
                    LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(LC.LIM,".",2)
                    GOSUB LIM.REC
                END ELSE
                    WS.CY.ID = ''
                    LI.ONL.AMT = 0
                END
                OPER.ID  = CUS.ID
                CAT.ID   = LC.CAT
                CUR.ID   = LC.CUR
                LIMIT.CURR         = WS.CY.ID
                ONLINE.LIMIT.AMT   = LI.ONL.AMT
                USED.AMT           = LC.AMT
*----------------------
                LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                IF WS.CY.ID NE '' THEN
*                    CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                    IF LIMIT.CURR NE 'EGP' THEN
                        MM.CURRENCY = LIMIT.CURR
                        GOSUB GET.LOCAL.RATE
                        LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                    END
                END
                USED.AMT.LCY      = USED.AMT
*                CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                IF CUR.ID NE 'EGP' THEN
                    MM.CURRENCY = CUR.ID
                    GOSUB GET.LOCAL.RATE
                    USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                END
*----------------------
                GOSUB DATA.FILE
*-------
            END
            WS.DR.SUB.ID = LCB.ID
*########################################
            WS.SRL.DR = ''
            WS.NEXT.DR = WS.NEXT.DR + 0
            FOR IDR = 1 TO WS.NEXT.DR
                WS.SRL.ID = ( IDR + 100 )[2,2]
                WS.DR.ID = WS.DR.SUB.ID : WS.SRL.ID
                CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
                IF NOT(ER.DR) THEN
                    IF R.DR<TF.DR.MATURITY.REVIEW> GT LWD.DATE THEN
                        IF R.DR<TF.DR.DOCUMENT.AMOUNT> GT 0 THEN
                            DR.CUR   = R.DR<TF.DR.DRAW.CURRENCY>
                            DR.CAT   = LC.CAT
                            DR.AMT   = R.DR<TF.DR.DOCUMENT.AMOUNT>
                            DR.LIM   = R.DR<TF.DR.LIMIT.REFERENCE>
*------------
                            IF DR.LIM THEN
                                LIM.REF.ID = FIELD(DR.LIM,".",1)
                                CHK.LIM = FMT(FIELD(DR.LIM,".",1),'R%7')
                                LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                                GOSUB LIM.REC
                            END ELSE
                                WS.CY.ID = ''
                                LI.ONL.AMT = 0
                            END
                            OPER.ID = CUS.ID
                            CAT.ID  = DR.CAT
                            CUR.ID  = DR.CUR
                            LIMIT.CURR         = WS.CY.ID
                            ONLINE.LIMIT.AMT   = LI.ONL.AMT
                            USED.AMT           = DR.AMT
*----------------------
                            LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                            IF WS.CY.ID NE '' THEN
*                                CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                                IF LIMIT.CURR NE 'EGP' THEN
                                    MM.CURRENCY = LIMIT.CURR
                                    GOSUB GET.LOCAL.RATE
                                    LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                                END
                            END
                            USED.AMT.LCY      = USED.AMT
*                            CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                            IF CUR.ID NE 'EGP' THEN
                                MM.CURRENCY = CUR.ID
                                GOSUB GET.LOCAL.RATE
                                USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                            END
*----------------------
                            GOSUB DATA.FILE
                        END
                    END
                END
            NEXT IDR
*########################################
        END
    REPEAT
RETURN
****************************************
SUB.DR:
*-----------
    WS.SRL.DR = ''
    WS.NEXT.DR = WS.NEXT.DR + 0
    FOR IDR = 1 TO WS.NEXT.DR
        WS.SRL.ID = ( IDR + 100 )[2,2]
        WS.DR.ID = WS.DR.SUB.ID : WS.SRL.ID
        CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
        IF NOT(ER.DR) THEN
            IF R.DR<TF.DR.MATURITY.REVIEW> GT LWD.DATE THEN
                IF R.DR<TF.DR.DOCUMENT.AMOUNT> GT 0 THEN
                    DR.CUR   = R.DR<TF.DR.DRAW.CURRENCY>
                    DR.CAT   = LC.CAT
                    DR.AMT   = R.DR<TF.DR.DOCUMENT.AMOUNT>
                    DR.LIM   = R.DR<TF.DR.LIMIT.REFERENCE>
*------------
                    IF DR.LIM THEN
                        LIM.REF.ID = FIELD(DR.LIM,".",1)
                        CHK.LIM = FMT(FIELD(DR.LIM,".",1),'R%7')
                        LI.ID  = CUS.ID:".":CHK.LIM:".":FIELD(DR.LIM,".",2)
                        GOSUB LIM.REC
                    END ELSE
                        WS.CY.ID = ''
                        LI.ONL.AMT = 0
                    END
                    OPER.ID = CUS.ID
                    CAT.ID  = DR.CAT
                    CUR.ID  = DR.CUR
                    LIMIT.CURR         = WS.CY.ID
                    ONLINE.LIMIT.AMT   = LI.ONL.AMT
                    USED.AMT           = DR.AMT
*----------------------
                    LIMIT.AMT.LCY     = ONLINE.LIMIT.AMT
                    IF WS.CY.ID NE '' THEN
*                        CALL EGP.BASE.CURR(LIMIT.AMT.LCY,LIMIT.CURR)
                        IF LIMIT.CURR NE 'EGP' THEN
                            MM.CURRENCY = LIMIT.CURR
                            GOSUB GET.LOCAL.RATE
                            LIMIT.AMT.LCY = LIMIT.AMT.LCY * MM.CUR.RATE
                        END
                    END
                    USED.AMT.LCY      = USED.AMT
*                    CALL EGP.BASE.CURR(USED.AMT.LCY,CUR.ID)
                    IF CUR.ID NE 'EGP' THEN
                        MM.CURRENCY = CUR.ID
                        GOSUB GET.LOCAL.RATE
                        USED.AMT.LCY = USED.AMT.LCY * MM.CUR.RATE
                    END
*----------------------
                    GOSUB DATA.FILE
                END
            END
        END
    NEXT IDR

RETURN
************************************************

******************* WRITE DATA IN FILE **********************
DATA.FILE:
*---------
************************************************
    TOT.PRINT = ABS(ONLINE.LIMIT.AMT) + ABS(USED.AMT) + ABS(PD.AMT)
    IF TOT.PRINT NE 0 AND TOT.PRINT NE '' THEN
        KK++
*Line [ 684 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CAT.ID,CAT.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CAT.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

        BB.DATA  = OPER.ID:","
        BB.DATA := CAT.ID:","
        BB.DATA := CAT.NAME:","
        BB.DATA := CUR.ID:","
        BB.DATA := LI.ID:","
        BB.DATA := LIMIT.CURR:","
        BB.DATA := FMT(ONLINE.LIMIT.AMT,"L2"):","
        BB.DATA := FMT(USED.AMT,"L2"):","
        BB.DATA := FMT(LIMIT.AMT.LCY,"L2"):","
        BB.DATA := FMT(USED.AMT.LCY,"L2"):","
        BB.DATA := FMT(LI.SEC.AMT,"L2"):","
        BB.DATA := COL.CY:","
        BB.DATA := FMT(LI.SEC.AMT.LCY,"L2"):","
        BB.DATA := FMT(PD.AMT,"L2"):","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    END
*----------------
    OPER.ID  = ''
    CAT.ID   = ''
    CAT.NAME   = ''
    CUR.ID     = ''
    LI.ID      = ''
    LIMIT.CURR = ''
    ONLINE.LIMIT.AMT = 0
    USED.AMT       = 0
    LIMIT.AMT.LCY = 0
    USED.AMT.LCY  = 0
    LI.SEC.AMT    = 0
    LI.SEC.AMT.LCY = 0
    COL.CY = ''
    PD.AMT = 0
RETURN

************************************************
LIM.REC:
*--------
    LI.ONL.AMT = 0
    CALL F.READ(FN.LIM,LI.ID,R.LIM,F.LIM,ER.LIM)
    IF R.LIM THEN

        WS.LI.PROD  = R.LIM<LI.LIMIT.PRODUCT>
        WS.PRO.ALL  = R.LIM<LI.PRODUCT.ALLOWED>
        WS.CY.ID    = R.LIM<LI.LIMIT.CURRENCY>
        LI.ONL.AMT  = R.LIM<LI.ONLINE.LIMIT,1>
        LI.INT.AMT  = R.LIM<LI.INTERNAL.AMOUNT>
        LI.MAX.TOT  = R.LIM<LI.MAXIMUM.TOTAL>
        LI.EXP.DATE = R.LIM<LI.EXPIRY.DATE>
        LI.COL.R    = R.LIM<LI.COLLAT.RIGHT>
        
*#========================================================
*#        YY1 = DCOUNT(R.LIM<LI.COLLAT.RIGHT>,VM)
*#        FOR AA = 1 TO YY1
*#            YY2 = DCOUNT(R.LIM<LI.COLLAT.RIGHT,AA>,SM)
*#            FOR SS = 1 TO YY2
*#                LI.SEC.AMT += R.LIM<LI.SECURED.AMT,AA,SS>
*#            NEXT YY2
*#        NEXT YY1
*#========================================================
        CALL F.READ(FN.CUST.COL,CUS.ID,R.CUST.COL,F.CUST.COL,ER.CUST.COL)
        LOOP
            REMOVE CUST.COL.ID FROM R.CUST.COL SETTING POS.CUST.COL
        WHILE CUST.COL.ID:POS.CUST.COL
            CALL F.READ(FN.COL.R,CUST.COL.ID,R.COL.R,F.COL.R,EER.R)
            WS.LIMIT.NO = DCOUNT(R.COL.R<COLL.RIGHT.LIMIT.REFERENCE>,@VM)
            FOR XY1 = 1 TO WS.LIMIT.NO
                WS.LI.ID.C.R =  R.COL.R<COLL.RIGHT.LIMIT.REFERENCE,XY1>
                IF WS.LI.ID.C.R EQ LI.ID THEN
                    WS.COL.ID =  CUST.COL.ID
                    WS.PRC =  R.COL.R<COLL.RIGHT.PERCENT.ALLOC,XY1>
                    WS.PRC.MIA = WS.PRC / 100
*--------
                    COL.AMT.P = 0
                    COL.AMOUNT.LCY = 0
                    CALL F.READ(FN.RIGHT.COL,WS.COL.ID,R.RIGHT.COL,F.RIGHT.COL,ER.RIGHT.COL)
                    LOOP
                        REMOVE RIGHT.COL.ID FROM R.RIGHT.COL SETTING POS.RIGHT.COL
                    WHILE RIGHT.COL.ID:POS.RIGHT.COL
                        CALL F.READ(FN.COL,RIGHT.COL.ID,R.COL,F.COL,EER)
                        CY.CODE =  R.COL<COLL.CURRENCY>
                        WS.APP.ID  =  R.COL<COLL.APPLICATION.ID>[1,2]
                        WS.EXP.DATE=  R.COL<COLL.EXPIRY.DATE>
                        COL.AMT =  R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
                        IF CY.CODE NE 'EGP' THEN
                            CALL F.READ(FN.CCY,CY.CODE,R.CCY,F.CCY,E1)
                            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
                            LCY.COL.AMT = RATE * COL.AMT
                        END ELSE
                            LCY.COL.AMT = COL.AMT
                        END
                        COL.AMOUNT.LCY += LCY.COL.AMT
                        COL.AMT.P += COL.AMT
                    REPEAT

*--------
                    COL.CY = CY.CODE
                    LI.SEC.AMT +=  COL.AMT.P  * WS.PRC.MIA
                    LI.SEC.AMT.LCY += COL.AMOUNT.LCY * WS.PRC.MIA
                END
            NEXT XY1
        REPEAT
*#========================================================
    END ELSE
        WS.CY.ID    = ''
        LI.ONL.AMT  = 0
    END
RETURN
**********************************************************
GET.LOCAL.RATE:
*--------------
    MM.CUR.RATE = ''
*Line [ 804 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SBD.CURRENCY':@FM:SBD.CURR.MID.RATE,MM.CURRENCY,MM.CUR.RATE)
F.ITSS.SBD.CURRENCY = 'F.SBD.CURRENCY'
FN.F.ITSS.SBD.CURRENCY = ''
CALL OPF(F.ITSS.SBD.CURRENCY,FN.F.ITSS.SBD.CURRENCY)
CALL F.READ(F.ITSS.SBD.CURRENCY,MM.CURRENCY,R.ITSS.SBD.CURRENCY,FN.F.ITSS.SBD.CURRENCY,ERROR.SBD.CURRENCY)
MM.CUR.RATE=R.ITSS.SBD.CURRENCY<SBD.CURR.MID.RATE>
    IF MM.CURRENCY EQ 'JPY' THEN MM.CUR.RATE = MM.CUR.RATE/100 ELSE NULL
RETURN
************************************************
END
