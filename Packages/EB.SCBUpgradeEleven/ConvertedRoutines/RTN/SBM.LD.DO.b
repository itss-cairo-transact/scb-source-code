* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNTYwNzk6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>2658</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.LD.DO

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 46 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 47 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.LD.DO'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]
    DATD = TODAY
    DATM = DATD[1,6]
    DAT = DATM:"01"
    CALL CDT ('',DAT,'-1C')

*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,COMP,TD1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,COMP,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD1=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

*************************************************
    CATEG1 = ''
    AMT.EGP = 0 ; AMT.SAR = 0
    AMT.USD = 0 ; AMT.GBP = 0
    AMT.EUR = 0 ; AMT.CHF = 0
    LCY.CATEG = 0
    COUNT.EGP = 0 ; COUNT.SAR = 0
    COUNT.USD = 0 ; COUNT.GBP = 0
    COUNT.EUR = 0 ; COUNT.CHF = 0

    TOT.AMT.EGP = 0 ; TOT.AMT.SAR = 0
    TOT.AMT.USD = 0 ; TOT.AMT.GBP = 0
    TOT.AMT.EUR = 0 ; TOT.AMT.CHF = 0
    TOT.LCY.CATEG = 0
    TOT.FCY.CATEG = 0
    TOT.COUNT.EGP = 0 ; TOT.COUNT.SAR = 0
    TOT.COUNT.USD = 0 ; TOT.COUNT.GBP = 0
    TOT.COUNT.EUR = 0 ; TOT.COUNT.CHF = 0

    CATEG2 = ''
    AMT.EGP2 = 0 ; AMT.SAR2 = 0
    AMT.USD2 = 0 ; AMT.GBP2 = 0
    AMT.EUR2 = 0 ; AMT.CHF2 = 0
    LCY.CATEG2 = 0
    COUNT.EGP2 = 0 ; COUNT.SAR2 = 0
    COUNT.USD2 = 0 ; COUNT.GBP2 = 0
    COUNT.EUR2 = 0 ; COUNT.CHF2 = 0

    TOT.AMT.EGP2 = 0 ; TOT.AMT.SAR2 = 0
    TOT.AMT.USD2 = 0 ; TOT.AMT.GBP2 = 0
    TOT.AMT.EUR2 = 0 ; TOT.AMT.CHF2 = 0
    TOT.FCY.CATEG2 = 0
    TOT.COUNT.EGP2 = 0 ; TOT.COUNT.SAR2 = 0
    TOT.COUNT.USD2 = 0 ; TOT.COUNT.GBP2 = 0
    TOT.COUNT.EUR2 = 0 ; TOT.COUNT.CHF2 = 0
    TOT.LCY.CATEG2 = 0
    TOTAL.COUNT2 = 0
    XX  = SPACE(132) ; XX1 = SPACE(132)
    XX2 = SPACE(132) ; XX3 = SPACE(132)
    XX4 = SPACE(132) ; XX5 = SPACE(132)
    XX6 = SPACE(132)
    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''  ; R.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'    ; F.CCY  = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.CCY = ''
    CALL OPF(FN.AC,F.AC)

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21001 AND CATEGORY LE 21010 BY CATEGORY"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E2)
            CATEG = R.LD<LD.CATEGORY>
            IF I EQ 1 THEN CATEG1 = CATEG
****************
            IF CATEG1 NE CATEG THEN
                XX<1,1>[1,15]    = LCY.CATEG
                XX<1,1>[20,15]   = AMT.EGP
                XX<1,1>[35,15]   = AMT.SAR
                XX<1,1>[50,15]   = AMT.USD
                XX<1,1>[65,15]   = AMT.GBP
                XX<1,1>[80,15]   = AMT.EUR
                XX<1,1>[95,15]   = AMT.CHF
                XX<1,1>[115,15]  = DESC.CATEG

                XX1<1,1>[1,15]    = TOT.COUNT
                XX1<1,1>[20,15]   = COUNT.EGP
                XX1<1,1>[35,15]   = COUNT.SAR
                XX1<1,1>[50,15]   = COUNT.USD
                XX1<1,1>[65,15]   = COUNT.GBP
                XX1<1,1>[80,15]   = COUNT.EUR
                XX1<1,1>[95,15]   = COUNT.CHF
                XX1<1,1>[115,15]  = "DEP.NO"

                PRINT STR(' ',130)
                PRINT XX1<1,1>
                PRINT XX<1,1>
                PRINT STR('-',130)

                AMT.EGP = 0 ; AMT.SAR = 0
                AMT.USD = 0 ; AMT.GBP = 0
                AMT.EUR = 0 ; AMT.CHF = 0

                COUNT.EGP = 0 ; COUNT.SAR = 0
                COUNT.USD = 0 ; COUNT.GBP = 0
                COUNT.EUR = 0 ; COUNT.CHF = 0
                LCY.CATEG = 0

                DESC.CATEG = ''
                XX  = '' ; XX1 = ''
                CATEG1 = CATEG
            END
****************
            IF CATEG EQ CATEG1 THEN
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,CATEG,DESC.CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
DESC.CATEG=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
                CUR = R.LD<LD.CURRENCY>
**********************

                CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 170 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                RATE  = R.CCY<RE.BCP.RATE,POS>
***********************
                IF CUR EQ 'EGP' THEN
                    AMT.EGP += R.LD<LD.AMOUNT>
                    AMT.EGP = DROUND(AMT.EGP,'2')
                    COUNT.EGP++

                    TOT.AMT.EGP += R.LD<LD.AMOUNT>
                    TOT.AMT.EGP = DROUND(TOT.AMT.EGP,'2')
                    TOT.COUNT.EGP++

                END
                IF CUR EQ 'SAR' THEN
                    AMT.SAR += R.LD<LD.AMOUNT>
                    AMT.SAR = DROUND(AMT.SAR,'2')
                    COUNT.SAR++

                    TOT.AMT.SAR += R.LD<LD.AMOUNT>
                    TOT.AMT.SAR = DROUND(TOT.AMT.SAR,'2')
                    TOT.COUNT.SAR++

                END

                IF CUR EQ 'USD' THEN
                    AMT.USD += R.LD<LD.AMOUNT>
                    AMT.USD = DROUND(AMT.USD,'2')
                    COUNT.USD++

                    TOT.AMT.USD += R.LD<LD.AMOUNT>
                    TOT.AMT.USD = DROUND(TOT.AMT.USD,'2')
                    TOT.COUNT.USD++

                END

                IF CUR EQ 'GBP' THEN
                    AMT.GBP += R.LD<LD.AMOUNT>
                    AMT.GBP = DROUND(AMT.GBP,'2')
                    COUNT.GBP++

                    TOT.AMT.GBP += R.LD<LD.AMOUNT>
                    TOT.AMT.GBP = DROUND(TOT.AMT.GBP,'2')
                    TOT.COUNT.GBP++

                END

                IF CUR EQ 'EUR' THEN
                    AMT.EUR += R.LD<LD.AMOUNT>
                    AMT.EUR = DROUND(AMT.EUR,'2')
                    COUNT.EUR++

                    TOT.AMT.EUR += R.LD<LD.AMOUNT>
                    TOT.AMT.EUR = DROUND(TOT.AMT.EUR,'2')
                    TOT.COUNT.EUR++

                END

                IF CUR EQ 'CHF' THEN
                    AMT.CHF += R.LD<LD.AMOUNT>
                    AMT.CHF = DROUND(AMT.CHF,'2')
                    COUNT.CHF++

                    TOT.AMT.CHF += R.LD<LD.AMOUNT>
                    TOT.AMT.CHF = DROUND(TOT.AMT.CHF,'2')
                    TOT.COUNT.CHF++

                END
                LCY.CATEG += R.LD<LD.AMOUNT> * RATE
                LCY.CATEG = DROUND(LCY.CATEG,'2')

                TOT.LCY.CATEG += R.LD<LD.AMOUNT> * RATE
                TOT.LCY.CATEG = DROUND(TOT.LCY.CATEG,'2')

                TOT.COUNT = COUNT.EGP + COUNT.SAR + COUNT.USD + COUNT.GBP + COUNT.EUR + COUNT.CHF
                TOTAL.COUNT = TOT.COUNT.EGP + TOT.COUNT.SAR + TOT.COUNT.USD + TOT.COUNT.GBP + TOT.COUNT.EUR + TOT.COUNT.CHF
            END
            IF I EQ SELECTED THEN
                XX<1,1>[1,15]    = LCY.CATEG
                XX<1,1>[20,15]   = AMT.EGP
                XX<1,1>[35,15]   = AMT.SAR
                XX<1,1>[50,15]   = AMT.USD
                XX<1,1>[65,15]   = AMT.GBP
                XX<1,1>[80,15]   = AMT.EUR
                XX<1,1>[95,15]   = AMT.CHF
                XX<1,1>[115,15]  = DESC.CATEG

                XX1<1,1>[1,15]    = TOT.COUNT
                XX1<1,1>[20,15]   = COUNT.EGP
                XX1<1,1>[35,15]   = COUNT.SAR
                XX1<1,1>[50,15]   = COUNT.USD
                XX1<1,1>[65,15]   = COUNT.GBP
                XX1<1,1>[80,15]   = COUNT.EUR
                XX1<1,1>[95,15]   = COUNT.CHF
                XX1<1,1>[115,15]  = "DEP.NO"

                PRINT STR(' ',130)
                PRINT XX1<1,1>
                PRINT XX<1,1>
                PRINT STR('-',130)

                AMT.EGP = 0 ; AMT.SAR = 0
                AMT.USD = 0 ; AMT.GBP = 0
                AMT.EUR = 0 ; AMT.CHF = 0

                COUNT.EGP = 0 ; COUNT.SAR = 0
                COUNT.USD = 0 ; COUNT.GBP = 0
                COUNT.EUR = 0 ; COUNT.CHF = 0
                LCY.CATEG = 0
                DESC.CATEG = ''
                XX  = '' ; XX1 = ''
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END
******************ACCOUNT DOKKI **************
    T.SEL2 = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 6512"
    KEY.LIST2 ="" ; SELECTED2="" ;  ER.MSG2=""
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR Z = 1 TO SELECTED2
            CALL F.READ(FN.AC,KEY.LIST2<Z>,R.AC,F.AC,E3)
            CUR2 = R.AC<AC.CURRENCY>
**********************
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 296 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR2 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE2  = R.CCY<RE.BCP.RATE,POS>
***********************
            IF CUR2 EQ 'EGP' THEN
                AMT.EGP2 += R.AC<AC.OPEN.ACTUAL.BAL>
                AMT.EGP2 = DROUND(AMT.EGP2,'2')
                COUNT.EGP2++

                TOT.AMT.EGP2 += R.AC<AC.OPEN.ACTUAL.BAL>
                TOT.AMT.EGP2 = DROUND(TOT.AMT.EGP2,'2')
                TOT.COUNT.EGP2++

            END
            IF CUR2 EQ 'SAR' THEN
                AMT.SAR2 += R.AC<AC.OPEN.ACTUAL.BAL>
                AMT.SAR2 = DROUND(AMT.SAR2,'2')
                COUNT.SAR2++

                TOT.AMT.SAR2 += R.AC<AC.OPEN.ACTUAL.BAL>
                TOT.AMT.SAR2 = DROUND(TOT.AMT.SAR2,'2')
                TOT.COUNT.SAR2++

            END

            IF CUR2 EQ 'USD' THEN
                AMT.USD2 += R.AC<AC.OPEN.ACTUAL.BAL>
                AMT.USD2 = DROUND(AMT.USD2,'2')
                COUNT.USD2++

                TOT.AMT.USD2 += R.AC<AC.OPEN.ACTUAL.BAL>
                TOT.AMT.USD2 = DROUND(TOT.AMT.USD2,'2')
                TOT.COUNT.USD2++

            END

            IF CUR2 EQ 'GBP' THEN
                AMT.GBP2 += R.AC<AC.OPEN.ACTUAL.BAL>
                AMT.GBP2 = DROUND(AMT.GBP,'2')
                COUNT.GBP2++

                TOT.AMT.GBP2 += R.AC<AC.OPEN.ACTUAL.BAL>
                TOT.AMT.GBP2 = DROUND(TOT.AMT.GBP2,'2')
                TOT.COUNT.GBP2++

            END

            IF CUR2 EQ 'EUR' THEN
                AMT.EUR2 += R.AC<AC.OPEN.ACTUAL.BAL>
                AMT.EUR2 = DROUND(AMT.EUR2,'2')
                COUNT.EUR2++

                TOT.AMT.EUR2 += R.AC<AC.OPEN.ACTUAL.BAL>
                TOT.AMT.EUR2 = DROUND(TOT.AMT.EUR2,'2')
                TOT.COUNT.EUR2++

            END

            IF CUR2 EQ 'CHF' THEN
                AMT.CHF2 += R.AC<AC.OPEN.ACTUAL.BAL>
                AMT.CHF2 = DROUND(AMT.CHF2,'2')
                COUNT.CHF2++

                TOT.AMT.CHF2 += R.AC<AC.OPEN.ACTUAL.BAL>
                TOT.AMT.CHF2 = DROUND(TOT.AMT.CHF2,'2')
                TOT.COUNT.CHF2++

            END

            LCY.CATEG2 += R.AC<AC.OPEN.ACTUAL.BAL> * RATE2
            LCY.CATEG2 = DROUND(LCY.CATEG2,'2')

            TOT.LCY.CATEG2 += R.AC<AC.OPEN.ACTUAL.BAL> * RATE2
            TOT.LCY.CATEG2 = DROUND(TOT.LCY.CATEG2,'2')

            TOT.COUNT2 = COUNT.EGP2 + COUNT.SAR2 + COUNT.USD2 + COUNT.GBP2 + COUNT.EUR2 + COUNT.CHF2
            TOTAL.COUNT2 = TOT.COUNT.EGP2 + TOT.COUNT.SAR2 + TOT.COUNT.USD2 + TOT.COUNT.GBP2 + TOT.COUNT.EUR2 + TOT.COUNT.CHF2
            IF Z EQ SELECTED2 THEN
                XX2<1,1>[1,15]    = LCY.CATEG2
                XX2<1,1>[20,15]   = AMT.EGP2
                XX2<1,1>[35,15]   = AMT.SAR2
                XX2<1,1>[50,15]   = AMT.USD2
                XX2<1,1>[65,15]   = AMT.GBP2
                XX2<1,1>[80,15]   = AMT.EUR2
                XX2<1,1>[95,15]   = AMT.CHF2
                XX2<1,1>[115,15]  = "����� ��������"

                XX3<1,1>[1,15]    = TOT.COUNT2
                XX3<1,1>[20,15]   = COUNT.EGP2
                XX3<1,1>[35,15]   = COUNT.SAR2
                XX3<1,1>[50,15]   = COUNT.USD2
                XX3<1,1>[65,15]   = COUNT.GBP2
                XX3<1,1>[80,15]   = COUNT.EUR2
                XX3<1,1>[95,15]   = COUNT.CHF2
                XX3<1,1>[115,15]  = "DEP.NO"

                PRINT STR(' ',130)
                PRINT XX3<1,1>
                PRINT XX2<1,1>
                PRINT STR('-',130)
            END
*   AMT.EGP2 = 0 ; AMT.SAR2 = 0
*  AMT.USD2 = 0 ; AMT.GBP2 = 0
* AMT.EUR2 = 0 ; AMT.CHF2 = 0

* COUNT.EGP2 = 0 ; COUNT.SAR2 = 0
* COUNT.USD2 = 0 ; COUNT.GBP2 = 0
*COUNT.EUR2 = 0 ; COUNT.CHF2 = 0
* LCY.CATEG2 = 0
* DESC.CATEG2 = ''
* XX2  = '' ; XX3 = ''
        NEXT Z
*  END ELSE
*     ENQ.ERROR = "NO RECORDS FOUND"
    END

    PRINT STR('=',130)
    XX4<1,1>[1,15]    = TOT.LCY.CATEG + LCY.CATEG2
    XX4<1,1>[20,15]   = TOT.AMT.EGP   + AMT.EGP2
    XX4<1,1>[35,15]   = TOT.AMT.SAR   + AMT.SAR2
    XX4<1,1>[50,15]   = TOT.AMT.USD   + AMT.USD2
    XX4<1,1>[65,15]   = TOT.AMT.GBP   + AMT.GBP2
    XX4<1,1>[80,15]   = TOT.AMT.EUR   + AMT.EUR2
    XX4<1,1>[95,15]   = TOT.AMT.CHF   + AMT.CHF2
    XX4<1,1>[115,15]  = "��������"

    TOT.FCY.CATEG = TOT.LCY.CATEG + TOT.LCY.CATEG2 - TOT.AMT.EGP - TOT.AMT.EGP2
    TOT.FCY.CATEG = DROUND(TOT.FCY.CATEG,'2')
    XX6<1,1>[1,15]    = "TOTAL.FCY = "
    XX6<1,1>[15,15]   = TOT.FCY.CATEG

    XX5<1,1>[1,15]    = TOTAL.COUNT + TOTAL.COUNT2
    XX5<1,1>[20,15]   = TOT.COUNT.EGP + TOT.COUNT.EGP2
    XX5<1,1>[35,15]   = TOT.COUNT.SAR + TOT.COUNT.SAR2
    XX5<1,1>[50,15]   = TOT.COUNT.USD + TOT.COUNT.USD2
    XX5<1,1>[65,15]   = TOT.COUNT.GBP + TOT.COUNT.GBP2
    XX5<1,1>[80,15]   = TOT.COUNT.EUR + TOT.COUNT.EUR2
    XX5<1,1>[95,15]   = TOT.COUNT.CHF + TOT.COUNT.CHF2
    XX5<1,1>[115,15]  = "TOTAL.DEP.NO"

    PRINT STR(' ',130)
    PRINT XX5<1,1>
    PRINT XX4<1,1>
    PRINT STR('=',130)
    PRINT XX6<1,1>
    PRINT STR('-',130)
    RETURN

*===============================================================
PRINT.HEAD:
*Line [ 459 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
**    YYBRN = BRANCH
    YYBRN = "�� ���� �����"
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    T.DAY1 = TD1[7,2]:'/':TD1[5,2]:"/":TD1[1,4]

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������� ���� ���� ������� ����� ��� �������"
    PR.HD :="'L'":SPACE(60):"���� : ":T.DAY1
    PR.HD :="'L'":SPACE(45):STR('_',60)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"TOTAL.EQUIVALENT":SPACE(5):"EGP":SPACE(10):"SAR" :SPACE(10):"USD":SPACE(10):"GBP ":SPACE(10):"EUR ":SPACE(10):"CHF"
    PR.HD :="'L'":SPACE(1):STR('=',130)

    HEADING PR.HD
    RETURN
*==============================================================
END
