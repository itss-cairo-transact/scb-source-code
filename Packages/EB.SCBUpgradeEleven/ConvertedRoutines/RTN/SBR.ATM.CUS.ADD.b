* @ValidationCode : MjoxMTIxMDgxMzAyOkNwMTI1MjoxNjQ0OTI1MDYyMzgwOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
****   MAI  10 APRIL 2019***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-------------------------------------------------------------------------
SUBROUTINE SBR.ATM.CUS.ADD
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CI.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.APP


    TEXT = 'BEGIN' ; CALL REM
    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
******************* OPEN FILE *******************************
    OPENSEQ "&SAVEDLISTS&" , "CARD.CUST.ADD.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CARD.CUST.ADD.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CARD.CUST.ADD.csv" TO BB ELSE
        CREATE BB THEN
*PRINT 'FILE CARD.CUST.ADD.csv CREATED IN &SAVEDLISTS&'
            TEXT =  'FILE CARD.CUST.ADD.csv CREATED ' ; CALL REM;
        END ELSE
            STOP 'Cannot create CARD.CUST.ADD.csv File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
*    BB.DATA = "CUST.ID":",": "STREET":" , ":"AR.ADD":" , ":"J.ADD"
    BB.DATA = "CUST.ID":",":"SMS":" , ":"AR.ADD.1":",":"AR.ADD.2":",":"AR.ADD.3":",":"AR.ADD.4"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    S.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC... AND CANCELLATION.DATE EQ ''  BY ACCOUNT "
    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)
    TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM
    IF SELECTED.SS THEN
        FOR I = 1 TO SELECTED.SS

            CALL F.READ(FN.CARD.ISSUE,KEY.LIST.SS<I>, R.CARD.ISSUE, F.CARD.ISSUE ,E3)
            ACC.NO<I>      = R.CARD.ISSUE<CARD.IS.ACCOUNT><1,1>
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACC.NO<I>, P.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
P.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
            CALL F.READ(FN.CUSTOMER, P.CUST, R.CUSTOMER, F.CUSTOMER , ERR.CUST)
            CUST.ID = P.CUST
            SMS = R.CUSTOMER<EB.CUS.SMS.1>
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>

            AR.ADD.1 = LOCAL.REF.C<1,CULR.ARABIC.ADDRESS,1>
            AR.ADD.2 = LOCAL.REF.C<1,CULR.ARABIC.ADDRESS,2>
            AR.ADD.3 = LOCAL.REF.C<1,CULR.ARABIC.ADDRESS,3>
            AR.ADD.4 = LOCAL.REF.C<1,CULR.ARABIC.ADDRESS,4>

            BB.DATA =  P.CUST:",'":SMS:" , ":AR.ADD.1:",":AR.ADD.2:",":AR.ADD.3:",":AR.ADD.4
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
                TEXT = " ERROR WRITE FILE " ; CALL REM;
            END

        NEXT I
    END   ;***End of main selection from Card.Issue***
    TEXT = '�� �������� �� �������� ' ;CALL REM
RETURN
END
