* @ValidationCode : MjotMjEzMjM2MTMxOkNwMTI1MjoxNjQ0OTI1MDYxMDg4OmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>20</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBQ.STMT.CO.999
**    PROGRAM SBQ.STMT.CO.999

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CUSTOMER.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SECTOR
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SCB.CUS.LEGAL.FORM
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.INDUSTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_USER.ENV.COMMON
*-----------------------------------------------------------------------------

    GOSUB INIT

    WS.COMP = ID.COMPANY
**    WS.COMP = 'EG0010004'

*-----------------------------------------------------------------------------
    REPORT.ID = 'SBQ.STMT.CO.999'
    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD

    GOSUB READ.CUSTOMER.FILE

    PR.LINE = STR('_',132)
    PRINT PR.LINE

    XX = SPACE(132)
    XX<1,1>[10,35]   = '����������� ������� ������� :  '
    XX<1,1>[47,20]   = NO.CUST
    PRINT XX<1,1>
    XX = SPACE(132)
    PRINT XX<1,1>


    XX<1,1>[50,40]   = "*****  �������������  ����������   *****"
    PRINT XX<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*----------------------------------------------------------------------------
READ.CUSTOMER.FILE:


    SEL.CUST = "SELECT ":FN.CUST:" WITH"
    SEL.CUST :=" COMPANY.BOOK EQ ":WS.COMP
    SEL.CUST :=" AND NEW.SECTOR NE 4650"
    SEL.CUST :=" BY @ID"


    CALL EB.READLIST(SEL.CUST,CUST.LIST,'',NO.OF.CUST,ERR.CUST)

    LOOP
        REMOVE Y.CUST.ID FROM CUST.LIST SETTING CUST.POS
    WHILE Y.CUST.ID:CUST.POS
        CALL F.READ(FN.CUST,Y.CUST.ID,R.CUSTOMER,F.CUST,ERR.CUST.ERR)


        Y.SEC.ID  = R.CUSTOMER<EB.CUS.SECTOR>
        Y.IND.ID  = R.CUSTOMER<EB.CUS.INDUSTRY>

*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
        Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>

        Y.CUS.ACC.ID = Y.CUST.ID

        GOSUB READ.CUSTOMER.ACCOUNT.FILE



    REPEAT

RETURN
*----------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT.FILE:

    DR.AMT    = 0
    CR.AMT    = 0
    T.DR.AMT  = 0
    T.CR.AMT  = 0
    WS.DR.AMT = 0
    WS.CR.AMT = 0



    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)

    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)

*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>

*---------------

        GOSUB READ.STMT.ENTRY

*---------------
    NEXT Z


    GOSUB   SELECT.THE.CUST


RETURN
*----------------------------------------------------------------------------
READ.STMT.ENTRY:
    SEL.LIST = ''
    CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,STR.DATE,END.DATE,SEL.LIST,OPENING.BAL,ER)

*--------------------------------------


    LOOP
        REMOVE REC.ID FROM SEL.LIST SETTING POS.CUST
    WHILE REC.ID:POS.CUST
        CALL F.READ(FN.STMT,REC.ID,R.STMT,F.STMT,ERR.STMT)
****************
        SS.ID = R.STMT<AC.STE.SYSTEM.ID>
        RR.ST = R.STMT<AC.STE.RECORD.STATUS>
****************
        PL.CATEG  = R.STMT<AC.STE.PL.CATEGORY>
        PRD.CATEG = R.STMT<AC.STE.PRODUCT.CATEGORY>
        IF PRD.CATEG GE 9000 AND PRD.CATEG LE 9999 THEN
            GOTO NEXT.STMT
        END
****************
        IF SS.ID EQ 'LD' THEN
            GOTO NEXT.STMT
        END
        IF RR.ST EQ 'REVE' THEN
            GOTO NEXT.STMT
        END
****************

        CUST.ID = R.STMT<AC.STE.CUSTOMER.ID>
        DR.AMT  = 0
        CR.AMT  = 0
        AMT = R.STMT<AC.STE.AMOUNT.LCY>
****************
*Line [ 186 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.STMT<AC.STE.AMOUNT.LCY>,@VM)
        FOR X = 1 TO YY

            AMT = R.STMT<AC.STE.AMOUNT.LCY,1,X>
            IF    AMT LT 0    THEN
                DR.AMT  = DR.AMT + AMT
            END
            IF    AMT GT 0    THEN
                CR.AMT  = CR.AMT + AMT
            END

        NEXT X
****************
        T.DR.AMT   += DR.AMT
        T.CR.AMT   += CR.AMT


NEXT.STMT:

    REPEAT


RETURN

*-------------------------------------------------------------------------
SELECT.THE.CUST:

    WS.DR.AMT = ABS(T.DR.AMT)
    WS.CR.AMT = ABS(T.CR.AMT)

    IF WS.DR.AMT GE LOW.AMT  THEN
        IF WS.DR.AMT LT HIGH.AMT THEN
            GOSUB PRINT.ONE.LINE
            GO TO END.SEL.CUST
        END
    END


    IF WS.CR.AMT GE LOW.AMT  THEN
        IF WS.CR.AMT LT HIGH.AMT THEN
            GOSUB PRINT.ONE.LINE
        END
    END

END.SEL.CUST:
RETURN
*-------------------------------------------------------------------------
PRINT.ONE.LINE:

    Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>

    CALL F.READ(FN.SEC,Y.SEC.ID,R.SECTOR,F.SEC,ERR.SEC)
    SEC.NAME = R.SECTOR<EB.SEC.DESCRIPTION,2>



    CALL F.READ(FN.LGL,Y.LGL.ID,R.LEGAL,F.LGL,ERR.LGL)
    LGL.NAME = R.LEGAL<LEG.DESCRIPTION,2>
*---------------
    GOSUB WRITE.LINE.01

*---------------
RETURN

*-----------------------------------------------------------------------
INIT:
    PGM.NAME = "SBQ.STMT.CO.999"
    SAVE.CUST  = 0
*------------------------
    CUST.TYPE = "�����������"
***    CUST.TYPE = "���������"

    NO.CUST = 0
*------------------------
    WRK.DATE   = TODAY

******   WRK.DATE = 20100815


    WRK.YY     = WRK.DATE[1,4]
    WRK.MM     = WRK.DATE[5,2]
    WRK.DD     = WRK.DATE[7,2]

*------------------------
    BEGIN CASE
        CASE     WRK.MM GE 01 AND WRK.MM LE 03
            END.YY = WRK.YY - 1
            END.MM = 12
            END.DD = 31

        CASE     WRK.MM GE 04 AND WRK.MM LE 06
            END.YY = WRK.YY
            END.MM = 03
            END.DD = 31

        CASE     WRK.MM GE 07 AND WRK.MM LE 09
            END.YY = WRK.YY
            END.MM = 06
            END.DD = 30


        CASE     WRK.MM GE 10 AND WRK.MM LE 12
            END.YY = WRK.YY
            END.MM = 09
            END.DD = 30

    END CASE


    END.MM = FMT(END.MM,"R%2")
    END.DD = FMT(END.DD,"R%2")

    END.DATE = END.YY:END.MM:END.DD

*----------------------------------------------------------------------
    STR.YY = END.YY
    STR.MM = END.MM - 2
    STR.DD = "01"

    STR.MM = FMT(STR.MM,"R%2")
    STR.DD = FMT(STR.DD,"R%2")
    STR.DATE = STR.YY:STR.MM:STR.DD
*------------------------
***** CALL CDT('', STR.DATE, "-1C")
*------------------------
    P.DATE   = FMT(TODAY,"####/##/##")
    P.STR.DATE   = FMT(STR.DATE,"####/##/##")
    P.END.DATE   = FMT(END.DATE,"####/##/##")
*------------------------

    LOW.AMT  = 6000000
    HIGH.AMT = 999999999999


    P.LOW.AMT = FMT(LOW.AMT,"15L,")
    P.HIGH.AMT = FMT(HIGH.AMT,"17L,")

*------------------------
    FN.STMT = "FBNK.STMT.ENTRY"
    F.STMT = ''
    R.STMT=''
    Y.STMT=''
    Y.STMT.ERR=''
*------------------------
    CALL OPF(FN.STMT,F.STMT)
*------------------------
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUSTOMER = ""
    Y.CUST.ID   = ""
*-------------------------------
    CALL OPF(FN.CUST,F.CUST)
*-------------------------------
    FN.SEC = "FBNK.SECTOR"
    F.SEC  = ""
    R.SECTOR = ""
    Y.SEC.ID   = ""
*-------------------------------
    CALL OPF(FN.SEC,F.SEC)
*-------------------------------
    FN.LGL = "F.SCB.CUS.LEGAL.FORM"
    F.LGL  = ""
    R.LEGAL = ""
    Y.LGL.ID   = ""
*-------------------------------
    CALL OPF(FN.LGL,F.LGL)
*-------------------------------
    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC  = ""
    R.CUS.ACC = ""
    Y.CUS.ACC.ID   = ""
*-------------------------------
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
*-------------------------------


    T.DR.AMT   = 0
    T.CR.AMT   = 0





RETURN
*------------------------------------------------------------------------
WRITE.LINE.01:

    XX = SPACE(132)
    XX<1,1>[1,9]     = Y.CUST.ID
    XX<1,1>[10,35]   = CUST.NAME
*    XX<1,1>[50,20]   = SEC.NAME
*    XX<1,1>[50,20]   = LGL.NAME
    XX<1,1>[47,25] = LEFT(LGL.NAME, 25)

    P.DR.AMT  = FMT(T.DR.AMT,"17L2")
    P.CR.AMT  = FMT(T.CR.AMT,"17L2")
    T.NT.AMT  = T.CR.AMT + T.DR.AMT
    P.NT.AMT  = FMT(T.NT.AMT,"17L2")

    XX<1,1>[75,17]   = P.DR.AMT
    XX<1,1>[95,17]  = P.CR.AMT
    XX<1,1>[115,17]  = P.NT.AMT
    PRINT XX<1,1>

    NO.CUST = NO.CUST + 1

*    PR.LINE = STR('_',132)
*    PRINT PR.LINE

    NO.OF.LINE = NO.OF.LINE + 1
    IF NO.OF.LINE GT 39  THEN
        GOSUB PRINT.HEAD
    END

RETURN
*===============================================================
PRINT.HEAD:
*Line [ 410 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH


    T.DAY = P.DATE
    PR.HD ="'L'":SPACE(1):"��� ���� ������ "  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY:SPACE(86):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(11):SPACE(100):PGM.NAME
    PR.HD :="'L'":SPACE(50):"������ ������ ������� �� ������� ���� ��� �������"
    PR.HD :="'L'":SPACE(55):"������� �� �������":" ":CUST.TYPE
    PR.HD :="'L'":SPACE(50):"�� ":P.LOW.AMT:" �� ":"  ��� ��� �� ":P.HIGH.AMT:" �� "
    PR.HD :="'L'":SPACE(57):"�� �� ������ �� ������� ��������"
    PR.HD :="'L'":SPACE(55):"������ �� ":P.STR.DATE:"   ��� ":P.END.DATE

    PR.HD :="'L'":SPACE(50):STR('_',50)
    PR.HD :="'L'":" "
***********************************************************
    PR.HD :="'L'":SPACE(1)
    PR.HD :="'L'":SPACE(0):" ��� ������" :SPACE(11):" ��� ������ " :SPACE(12):"����� ��������":SPACE(14):"������ ���������":SPACE(4):"������ ����������"
    PR.HD :="'L'":SPACE(74):"�������             �������":SPACE(13):"������"
    PR.HD :="'L'":STR('_',132)
***********************
    HEADING PR.HD
    NO.OF.LINE = 0
RETURN



END
