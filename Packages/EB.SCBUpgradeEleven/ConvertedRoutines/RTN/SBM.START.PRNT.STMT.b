* @ValidationCode : MjotMTU2MjU4OTYxNzpDcDEyNTI6MTY0MDg1MDkyNTE1OTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 09:55:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-118</Rating>
*-----------------------------------------------------------------------------
****** CREATED BY Mohamed Sabry  *******
*****      2014/05/13            *******
****************************************

*    SUBROUTINE SBM.START.PRNT.STMT
PROGRAM SBM.START.PRNT.STMT

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TSA.SERVICE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 41 ] Hashing $INCLUDE I_OFS.SOURCE.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_OFS.SOURCE.LOCAL.REFS
***** SCB R15 UPG 20160627  - S
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_OFS.MESSAGE.SERVICE.COMMON
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_GTS.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.OFS.SOURCE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_GTS.COMMON
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.TSA.SERVICE
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SPF
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_IO.EQUATE

    FN.OFS = "F.OFS.SOURCE"
    F.OFS = ''
    CALL OPF(FN.OFS,F.OFS)

    FN.ORQ = "F.OFS.RESPONSE.QUEUE"
    F.ORQ = ''

    OFS$SOURCE.ID = "SCBONLINE"

    ERR.OFSS = ''
    CALL F.READ(FN.OFS,OFS$SOURCE.ID,OFS$SOURCE.REC,F.OFS,ERR.OFSS)
    CALL OFS.MESSAGE.SERVICE.LOAD

***** SCB R15 UPG 20160627  - E


    GOSUB INITIATE
    GOSUB OPEN.FILES
    GOSUB RUN.TSA.OFS
    GOSUB CHK.ALL.TSA.STOPED
    GOSUB GET.ORG.PROFILE

RETURN
****************************************
INITIATE:

    EXECUTE "uname -a" CAPTURING WS.NAME.TXT
    PRINT WS.NAME.TXT
    WS.NAME.TXT = FIELD(WS.NAME.TXT," ",2)
    PRINT WS.NAME.TXT

***** SCB R15 UPG 20160627  - S
*#* CU.SCB.OFS.SOURCE  = "TESTOFS"
    CU.SCB.OFS.SOURCE  = "SCBONLINE"
***** SCB R15 UPG 20160627  - E

    CU.SCB.APPL        = "TSA.SERVICE"
    CU.SCB.VERSION     = "NEW"

    IF WS.NAME.TXT = 'P550_Bkup' THEN
        CU.SCB.VERSION     = "SCB"
    END
    CU.SCB.OFS.HEADER  = CU.SCB.APPL : "," : CU.SCB.VERSION

    WS.ORG.USR = R.USER<EB.USE.SIGN.ON.NAME>
    WS.TSA.STARTED = 0
    WS.FLG = 0
RETURN
****************************************
OPEN.FILES:
*Master files

    FN.USER = "F.USER"
    FV.USER = ""
    CALL OPF(FN.USER,FV.USER)

    FN.USER.SIGN.ON.NAME = "F.USER.SIGN.ON.NAME"
    FV.USER.SIGN.ON.NAME = ""
    CALL OPF(FN.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME)

    FN.TSA.SERVICE = "F.TSA.SERVICE" ;  F.TSA.SERVICE  = "" ;  R.TSA.SERVICE = ''
    CALL OPF(FN.TSA.SERVICE,F.TSA.SERVICE)

RETURN
****************************************
****************************************
GEN.OFS.USER:

    OPERATOR        = "INPUTT":WS.BR.CODE
    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")

RETURN
*------------------------------------------------------------------------------------------
RUN.TSA.OFS:
    FOR IBR = 101 TO 199
        WS.BR.CODE    = IBR[2]
        IF WS.BR.CODE EQ '01' THEN
            WS.TSA.ID = "BNK^PRINT.STATEMENT.":WS.BR.CODE
            WS.TSA    = "BNK/PRINT.STATEMENT.":WS.BR.CODE
        END ELSE
            WS.TSA.ID = "B":WS.BR.CODE:"^PRINT.STATEMENT.":WS.BR.CODE
            WS.TSA    = "B":WS.BR.CODE:"/PRINT.STATEMENT.":WS.BR.CODE
        END

        CALL F.READ(FN.TSA.SERVICE,WS.TSA,NEW.R.TSA.SERVICE,F.TSA.SERVICE,READ.ERR)

        IF READ.ERR THEN
            GO TO NEXT.REC.IBR
        END

*        CU.OFS.MESSAGE.DATA  =  ",SERVER.NAME:1:1=":WS.NAME.TXT
        CU.OFS.MESSAGE.DATA =  ",SERVICE.CONTROL::=":"START"
*       CU.OFS.MESSAGE.DATA :=  ",SERVICE.CONTROL::=":"STOP"

        CU.SCB.OFS.MESSAGE = CU.SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.BR.CODE:"//EG00100":WS.BR.CODE:",":WS.TSA.ID :CU.OFS.MESSAGE.DATA

        GOSUB GEN.OFS.USER

*        PRINT CU.SCB.OFS.MESSAGE

***** SCB R15 UPG 20160815  - S
*#*        CALL OFS.GLOBUS.MANAGER(CU.SCB.OFS.SOURCE, CU.SCB.OFS.MESSAGE)

        SCB.OFS.SOURCE = "SCBONLINE"
        OFS.STRING = CU.SCB.OFS.MESSAGE
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, OFS.STRING)
        CU.SCB.OFS.MESSAGE = OFS.STRING
***** SCB R15 UPG 20160815  - E

*        PRINT CU.SCB.OFS.MESSAGE

        WS.DONE  = FIELD(CU.SCB.OFS.MESSAGE,"/",4)
        WS.DONE  = FIELD(WS.DONE,",",1)

        IF WS.DONE = '1' THEN
            PRINT " TSA = ":WS.TSA:" START "
            WS.BR.0 = WS.BR.CODE + 0
            WS.TSA.STARTED<WS.BR.0> = WS.TSA
        END

        GOSUB TSA.STOP.CHK

NEXT.REC.IBR:
    NEXT IBR

RETURN
*------------------------------------------------------------------------------------------
TSA.STOP.CHK:

*    EXECUTE "COUNT F.TSA.SERVICE WITH @ID EQ [/PRINT.STATEMENT.] AND SERVICE.CONTROL NE 'START' " CAPTURING TEMP.TXT

    T.SEL = "SELECT F.TSA.SERVICE WITH @ID EQ [/PRINT.STATEMENT.] AND SERVICE.CONTROL EQ 'START'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*    PRINT SELECTED

    WS.START.NO = SELECTED

    IF WS.START.NO GE 14 THEN
        GO TO TSA.STOP.CHK
    END ELSE
        RETURN
    END

RETURN
*------------------------------------------------------------------------------------------
CHK.ALL.TSA.STOPED:

    T.SEL2 = "SELECT F.TSA.SERVICE WITH @ID EQ [/PRINT.STATEMENT.] AND SERVICE.CONTROL EQ 'START'"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
*    PRINT SELECTED2
    IF SELECTED2 THEN
        IF WS.FLG = 0 THEN
            PRINT " "
            PRINT " ��� �������� ��� ������ ����� ����� ���� ������"
            WS.FLG = 1
        END
        GO TO CHK.ALL.TSA.STOPED
    END ELSE
        RETURN
    END

RETURN
*------------------------------------------------------------------------------------------
GET.ORG.PROFILE:
    OPERATOR        = WS.ORG.USR
    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")
*    CU.SCB.OFS.MESSAGE = CU.SCB.OFS.HEADER : "/I/PROCESS,":OPERATOR:"//EG0010099"
    OPERATOR        = R.USER.SIGN.ON.NAME

    CU.SCB.OFS.MESSAGE = CU.SCB.OFS.HEADER : "/I/PROCESS,":R.USER.SIGN.ON.NAME:"//EG0010099"
*    CALL OFS.GLOBUS.MANAGER(CU.SCB.OFS.SOURCE, CU.SCB.OFS.MESSAGE)
    SCB.OFS.SOURCE = "SCBONLINE"
    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, CU.SCB.OFS.MESSAGE)

RETURN
****************************************
END
