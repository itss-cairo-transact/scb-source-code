* @ValidationCode : MjotMTY4ODM4NDE5MzpDcDEyNTI6MTY0MDg1MzM4Nzk0NTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 10:36:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBQ.LOAN.STAMP.EXP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LOAN.STAMP
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS

*---------------------------------------
    FN.SLS = "F.SCB.LOAN.STAMP"  ; F.SLS  = ""
    CALL OPF (FN.SLS,F.SLS)

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"  ; F.LD  = ""
    CALL OPF (FN.LD,F.LD)

    T.SEL1 = "SELECT ":FN.LD:" WITH CATEGORY GE 21050 AND CATEGORY LE 21074 AND STATUS NE 'LIQ' AND STAMP.EXP EQ 'Y'"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.LD,KEY.LIST1<K>,R.LD,F.LD,E2)
            WS.STAMP.EXP = R.LD<LD.LOCAL.REF><1,LDLR.STAMP.EXP>
            WS.LD.NO     = KEY.LIST1<K>

            T.SEL = "SELECT ":FN.SLS:" WITH LD.NO EQ ":WS.LD.NO
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    CALL F.READ(FN.SLS,KEY.LIST<I>,R.SLS,F.SLS,E4)

                    R.SLS<SLS.STAMP.EXP> = WS.STAMP.EXP

                    CALL F.WRITE(FN.SLS,KEY.LIST<I>,R.SLS)
                    CALL JOURNAL.UPDATE(KEY.LIST<I>)

                NEXT I
            END

        NEXT K
    END

*****************************
RETURN
************************************************************
END
