* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-- CREATE BY NESSMA ON 1/10/2015

    SUBROUTINE SBR.FT.VISA.LIST

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.VISA.LIST
*---------------------------------------------
    PRINT "ON"
    GOSUB INITIAL
    GOSUB PROCESS
    PRINT "OFF"
    RETURN
*---------------------------------------------
INITIAL:
*-------
    FN.ACC = "FBNK.ACCOUNT"  ; F.ACC = ""
    CALL OPF(FN.ACC , F.ACC)

    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)

    FN.TMP = "F.SCB.FT.VISA.LIST"        ; F.TMP  = ""
    CALL OPF(FN.TMP,F.TMP)

    EXECUTE  "CLEAR-FILE F.SCB.FT.VISA.LIST"

    YTEXT  = " Enter First Date:  "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    DATE.1 = COMI

    YTEXT  = " Enter Second Date:  "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    DATE.2 = COMI

    RETURN
*---------------------------------------------
PROCESS:
*-------
    N.SEL  = " SELECT FBNK.FUNDS.TRANSFER$HIS WITH"
    N.SEL := " TRANSACTION.TYPE LIKE AC3..."
    N.SEL := " AND DEBIT.CURRENCY EQ 'EGP'"
    N.SEL := " AND PROCESSING.DATE GE ":DATE.1
    N.SEL := " AND PROCESSING.DATE LE ":DATE.2
    N.SEL := " AND TERMINAL.LOC NE ''"
    N.SEL := " AND TERMINAL.ID NE ''"

    CALL EB.READLIST(N.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT "SELECTED = ": SELECTED

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FT.ID    = KEY.LIST<I>
            FT.ID    = FIELD(FT.ID , ";" , 1)
            TEMP.ID  = FT.ID

            CALL F.READ(FN.FT.H,KEY.LIST<I>,R.FT.H,F.FT.H,ERR1)
            DB.AMT   = R.FT.H<FT.DEBIT.AMOUNT>
            PRO.DAT  = R.FT.H<FT.PROCESSING.DATE>
            TRNS.TYP = R.FT.H<FT.TRANSACTION.TYPE>
            DB.ACC   = R.FT.H<FT.DEBIT.ACCT.NO>
            PRINT "ACC= ":DB.ACC
            CALL F.READ(FN.ACC,DB.ACC,R.ACC,F.ACC,ERR2)
            COMP = R.ACC<AC.CO.CODE>

            CALL F.READ(FN.TMP,TEMP.ID,R.TMP,F.TMP,ERR3)
            R.TMP<FT.LIST.FT.ID>        = TEMP.ID
            R.TMP<FT.LIST.DB.AMT>       = DB.AMT
            R.TMP<FT.LIST.PROCESS.DATE> = PRO.DAT
            R.TMP<FT.LIST.TRANS.TYPE>   = TRNS.TYP
            R.TMP<FT.LIST.CO.CODE>      = COMP

            CALL F.WRITE(FN.TMP,TEMP.ID,R.TMP)
            CALL JOURNAL.UPDATE(TEMP.ID)
        NEXT I
    END
    RETURN
*---------------------------------------------
END
