* @ValidationCode : MjozNzM4NTA1Njc6Q3AxMjUyOjE2NDA4NjU4NDU0OTA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:04:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>86</Rating>
*-----------------------------------------------------------------------------

SUBROUTINE SBR.INS.RISK.RATING
*    PROGRAM SBR.INS.RISK.RATING

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CREDIT.CBE.NEW
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RISK.RAT

*--------CLEAR FILE -----------------------------------
    EXECUTE "CLEAR-FILE F.SCB.RISK.RAT"
*******************************************************

    FN.CU = 'FBNK.CUSTOMER'; F.CU = ''; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.RISK = 'F.SCB.RISK.RAT'; F.RISK = ''
    CALL OPF(FN.RISK,F.RISK)
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'; F.CCY  = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    T.DATE  = TODAY
    OLD.CUS = ''
    CBE.NO = ''
    TOTAL.CUS.BAL = 0
    GOSUB GETACC
*----------------------------------------------------------------
RETURN
**********************************
GETACC:
*-----
    T.SEL = "SELECT FBNK.ACCOUNT WITH ((INT.NO.BOOKING EQ 'SUSPENSE' AND CATEGORY NE 1002 AND (ACCR.DR.SUSP NE 0 AND ACCR.DR.SUSP NE '')) OR (CATEGORY IN (1710 1711) AND (ONLINE.ACTUAL.BAL NE ''))) BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            AC.ID = KEY.LIST<I>
            CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
            AC.CUR = R.AC<AC.CURRENCY>
            CUS.ID = R.AC<AC.CUSTOMER>
            AC.CAT = R.AC<AC.CATEGORY>

            IF ((OLD.CUS NE CUS.ID) AND (I NE 1) AND (RISK.RATE EQ 8 OR RISK.RATE EQ 9 OR RISK.RATE EQ 10) AND (CBE.NO NE '')) THEN
                GOSUB WRITEDATA
                TOTAL.CUS.BAL = 0
            END

            CALL F.READ( FN.CU,CUS.ID,R.CU, F.CU, CUSERR)
            CBE.NO    = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
            NAB.DATE  = R.CU<EB.CUS.LOCAL.REF><1,CULR.NAB.DATE>
            RISK.RATE = R.CU<EB.CUS.LOCAL.REF><1,CULR.RISK.RATE>
            COORP.IND = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF (RISK.RATE EQ 8 OR RISK.RATE EQ 9 OR RISK.RATE EQ 10) AND (COORP.IND NE 4650) THEN
                GOSUB GET.RATE

                IF AC.CAT EQ 1710 OR AC.CAT EQ 1711 THEN
                    AC.BAL   = R.AC<AC.OPEN.ACTUAL.BAL>
                    TOTAL.CUS.BAL += AC.BAL * CUR.RATE
                END

                IF R.AC<AC.ACCR.DR.SUSP> # '' THEN
                    SUS.AMT.NO = DCOUNT(R.AC<AC.ACCR.DR.SUSP>,@VM)
                    FOR J = 1 TO SUS.AMT.NO
                        SUS.BAL = R.AC<AC.ACCR.DR.SUSP,J>
                        TOTAL.CUS.BAL += SUS.BAL * CUR.RATE
                    NEXT J
                END
***************************************
*IF ((RISK.RATE EQ 8 OR RISK.RATE EQ 9 OR RISK.RATE EQ 10) AND (TOTAL.CUS.BAL EQ 0 OR TOTAL.CUS.BAL EQ '')) THEN
*                DEBUG
*               TOTAL.CUS.BAL = 1
*              NAB.DATE = '19010101'
*         END

                OLD.CUS = CUS.ID
            END
        NEXT I
        IF (I EQ SELECTED) AND (RISK.RATE EQ 8 OR RISK.RATE EQ 9 OR RISK.RATE EQ 10) AND (CBE.NO NE '') THEN
            GOSUB WRITEDATA
        END
    END
    CALL SBR.RISK.RATING
RETURN
**************************************************************************
WRITEDATA:
*---------
*PRINT "CUS: ":OLD.CUS:" NAB.BAL:":FMT(TOTAL.CUS.BAL,"R2"); CALL REM
*DEBUG

    CALL F.READ(FN.RISK,CBE.NO,R.RISK.OLD,F.RISK,ERR.OLD)
    NAB.BAL.OLD = R.RISK.OLD<RISK.NAB.BAL>

    IF ERR.OLD EQ '' THEN
        TOTAL.CUS.BAL = TOTAL.CUS.BAL + NAB.BAL.OLD
    END

    R.RISK = ''
    R.RISK<RISK.CUST.NO>     = OLD.CUS
    R.RISK<RISK.GADARA.CODE> = RISK.RATE
    R.RISK<RISK.NAB.DATE>    = NAB.DATE
    R.RISK<RISK.NAB.BAL>     = FMT(TOTAL.CUS.BAL,"R2")
    CALL F.WRITE(FN.RISK,CBE.NO,R.RISK)
    CALL JOURNAL.UPDATE(CBE.NO)
RETURN
***********************************************************************
GET.RATE:
*--------
    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 152 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE AC.CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    CUR.RATE = R.CCY<RE.BCP.RATE,POS>
RETURN
END
