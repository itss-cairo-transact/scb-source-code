* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBR.BUS.LINE.AL.EGP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========

    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.EGP.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"BUSINESS.LINE.AL.EGP.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.EGP.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE BUSINESS.LINE.AL.EGP.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create BUSINESS.LINE.AL.EGP.CSV File IN &SAVEDLISTS&'
        END
    END
*---------------------------------

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")


    HEAD.DESC  = DAT.HED:","
    HEAD.DESC := "CORPORATE":","
    HEAD.DESC := "RETAIL":","
    HEAD.DESC := "TREASURY":","
    HEAD.DESC := "HQ":","
    HEAD.DESC := "TOTAL":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*--------------------------------------
    FN.BU = 'F.SCB.BUS.LINE' ; F.BU = ''
    CALL OPF(FN.BU,F.BU)

    FN.CONT = 'F.RE.STAT.LINE.CONT' ; F.CONT = ''
    CALL OPF(FN.CONT,F.CONT)

    FN.CONS = 'F.CONSOLIDATE.ASST.LIAB' ; F.CONS = ''
    CALL OPF(FN.CONS,F.CONS)

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    CONT.BAL  = 0 ; WS.BAL.RETAIL = 0 ; WS.BAL.CORP = 0 ; WS.BAL.ALL = 0
    WS.BAL.HQ = 0 ; WS.BAL.TRE = 0

    WS.BAL.RETAIL.TOT = 0 ; WS.BAL.CORP.TOT = 0
    WS.BAL.HQ.TOT = 0 ; WS.BAL.TRE.TOT = 0

    RETURN
*-----------------------------------------------------------------------------
PROCESS:
*=======
    T.SEL = "SELECT ":FN.BU:" WITH FILE.TYPE EQ 'AL' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BU,KEY.LIST<I>,R.BU,F.BU,E1)
            WS.ROW.TXT = R.BU<SBL.ROW.DESC>
*Line [ 109 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.COL = DCOUNT(R.BU<SBL.COLUMN.DESC>,@VM)
            FOR X = 1 TO DECOUNT.COL
                WS.COL.TXT = R.BU<SBL.COLUMN.DESC,X>
                WS.SECTOR  = R.BU<SBL.SECTOR,X>
                WS.CATEG   = R.BU<SBL.P.CATEGORY,X>

*Line [ 116 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                LINE.COUNT = DCOUNT(R.BU<SBL.LINE,X>,@SM)
                FOR K = 1 TO LINE.COUNT
                    WS.LINE.ID = R.BU<SBL.LINE,X,K>

                    CONT.ID = WS.LINE.ID:'...'
                    T.SEL1 = "SELECT ":FN.CONT:" WITH @ID LIKE ":CONT.ID:" BY @ID"
                    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
                    IF SELECTED1 THEN
                        FOR KM = 1 TO SELECTED1
                            CALL F.READ(FN.CONT,KEY.LIST1<KM>,R.CONT,F.CONT,E5)
*Line [ 127 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
                            FOR KM1 = 1 TO DECOUNT.CONT
                                CONS.ID    = R.CONT<RE.SLC.ASST.CONSOL.KEY,KM1>
                                ASST.TYPE = R.CONT<RE.SLC.ASSET.TYPE,KM1>
*Line [ 132 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                                ASST.TYPE.COUNT = DCOUNT(ASST.TYPE,@SM)

                                CALL F.READ(FN.CONS,CONS.ID,R.CONS,F.CONS,E2)

                                WS.CONS.SECTOR = R.CONS<RE.ASL.VARIABLE.11>
                                WS.CONS.CATEG  = R.CONS<RE.ASL.VARIABLE.1>
                                CONT.CUR       = R.CONS<RE.ASL.CURRENCY>

                                CALL F.READ(FN.CCY,CONT.CUR,R.CCY,F.CCY,E1)

                                IF CONT.CUR EQ 'EGP' THEN
                                    RATE = 1
                                END ELSE
                                    IF CONT.CUR EQ 'JPY' THEN
                                        RATE = R.CCY<SBD.CURR.MID.RATE> / 100
                                    END ELSE
                                        RATE = R.CCY<SBD.CURR.MID.RATE>
                                    END
                                END

                                FOR II = 1 TO ASST.TYPE.COUNT
                                    ASST.TYPE1 =  R.CONT<RE.SLC.ASSET.TYPE,KM1,II>
*Line [ 155 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                                    LOCATE ASST.TYPE1 IN R.CONS<RE.ASL.TYPE,1> SETTING TYP ELSE NULL
                                    CONT.BAL = R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
                                    IF CONT.CUR EQ 'EGP' THEN
                                        IF WS.COL.TXT EQ 'HQ' THEN
                                            WS.BAL.HQ += CONT.BAL * RATE
                                        END

                                        IF WS.COL.TXT EQ 'Retail' THEN
                                            IF WS.CATEG EQ '' THEN
                                                IF WS.CONS.SECTOR EQ WS.SECTOR THEN
                                                    WS.BAL.RETAIL += CONT.BAL * RATE
                                                END
                                            END ELSE
                                                IF WS.CONS.SECTOR EQ WS.SECTOR AND WS.CONS.CATEG EQ WS.CATEG THEN
                                                    WS.BAL.RETAIL += CONT.BAL * RATE
                                                END
                                            END
                                        END

                                        IF WS.COL.TXT EQ 'Treasury' THEN
                                            WS.BAL.TRE += CONT.BAL * RATE
                                        END

                                        IF WS.COL.TXT EQ 'Corporate' THEN
                                            IF WS.CATEG EQ '' THEN
                                                IF WS.SECTOR EQ '' THEN
                                                    WS.BAL.ALL += CONT.BAL * RATE
                                                END
                                            END ELSE
                                                IF WS.SECTOR EQ '' AND WS.CONS.CATEG EQ WS.CATEG THEN
                                                    WS.BAL.ALL += CONT.BAL * RATE
                                                END

                                            END
                                        END

                                        WS.BAL.CORP = WS.BAL.ALL - WS.BAL.RETAIL
                                    END
                                NEXT II
                            NEXT KM1
                        NEXT KM
                    END
                NEXT K

            NEXT X

            WS.TOTAL = WS.BAL.CORP + WS.BAL.RETAIL + WS.BAL.TRE + WS.BAL.HQ

            WS.BAL.CORP.TOT   += WS.BAL.CORP
            WS.BAL.RETAIL.TOT += WS.BAL.RETAIL
            WS.BAL.TRE.TOT    += WS.BAL.TRE
            WS.BAL.HQ.TOT     += WS.BAL.HQ

            BB.DATA  = WS.ROW.TXT:","
            BB.DATA := WS.BAL.CORP:","
            BB.DATA := WS.BAL.RETAIL:","
            BB.DATA := WS.BAL.TRE:","
            BB.DATA := WS.BAL.HQ:","
            BB.DATA := WS.TOTAL:","

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            CONT.BAL      = 0
            WS.BAL.RETAIL = 0
            WS.BAL.CORP   = 0
            WS.BAL.ALL    = 0
            WS.BAL.HQ     = 0
            WS.BAL.TRE    = 0

        NEXT I

        WS.TOTAL.ALL = WS.BAL.CORP.TOT + WS.BAL.RETAIL.TOT + WS.BAL.TRE.TOT + WS.BAL.HQ.TOT

        BB.DATA  = 'TOTAL':","
        BB.DATA := WS.BAL.CORP.TOT:","
        BB.DATA := WS.BAL.RETAIL.TOT:","
        BB.DATA := WS.BAL.TRE.TOT:","
        BB.DATA := WS.BAL.HQ.TOT:","
        BB.DATA := WS.TOTAL.ALL:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    END
    RETURN
*====================================================
