* @ValidationCode : MjotMTU2ODA2NDMyMzpDcDEyNTI6MTY0MDg2MDg4MTQ2MTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 12:41:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.CONTRACT.SAL.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
    FN.MCH = "F.SCB.SUEZ.MECH.SAL"  ; F.MCH  = ""
    CALL OPF (FN.MCH,F.MCH)

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    TD = TODAY[1,6]

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME = 'contract.csv'
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN

            EMP.NO   = FIELD(Y.MSG,",",1)
            EMP.NAME = FIELD(Y.MSG,",",2)
            CATEG    = FIELD(Y.MSG,",",3)
            CUR      = FIELD(Y.MSG,",",4)
            CUS.NO1  = FIELD(Y.MSG,",",5)
            AMT      = FIELD(Y.MSG,",",6)

            SAL.DATE = TODAY[1,6]
            PAY.DATE = TODAY
            CUS.NO   = FMT(CUS.NO1,'R%8')

            ACCT.NO  = CUS.NO:CUR:CATEG
            CALL F.READ(FN.AC,ACCT.NO,R.AC,F.AC,E5)
            COMP = R.AC<AC.CO.CODE>
            MCH.ID   = CUS.NO:"-":SAL.DATE
*------------------------------------------------------------------
            CALL F.READ(FN.MCH,MCH.ID,R.MCH,F.MCH,E4)
            IF NOT(E4) THEN
                GO TO MSG.TXT.ERR
            END
            IF E4 THEN
                R.MCH<MCH.EMP.NO>       = EMP.NO
                R.MCH<MCH.EMP.NAME>     = EMP.NAME
                R.MCH<MCH.CUS.NO>       = CUS.NO
                R.MCH<MCH.CURRENCY>     = "EGP"
                R.MCH<MCH.CATEGORY>     = CATEG
                R.MCH<MCH.ACCOUNT.NO>   = ACCT.NO
                R.MCH<MCH.AMOUNT>       = AMT
                R.MCH<MCH.SALARY.DATE>  = SAL.DATE
                R.MCH<MCH.RECEIVE.DATE> = PAY.DATE
                R.MCH<MCH.STATUS>       = "1"
                R.MCH<MCH.CO.CODE>      = COMP
                R.MCH<MCH.TRN.CODE>     = "AC49"

                CALL F.WRITE(FN.MCH,MCH.ID,R.MCH)
                CALL JOURNAL.UPDATE(MCH.ID)
            END
*----------------------------------
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER
    EXECUTE "sh > MECH/contract.csv"

MSG.TXT.DONE:
    TEXT = "�� ����� �����" ; CALL REM
    GO TO EXIT.PROG

MSG.TXT.ERR:
    TEXT = "�� ����� ����� �� ���" ; CALL REM


EXIT.PROG:
RETURN
************************************************************
END
