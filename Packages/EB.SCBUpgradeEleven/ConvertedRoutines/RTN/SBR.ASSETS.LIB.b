* @ValidationCode : MjotMTg3NTAwMDY5MDpDcDEyNTI6MTY0MDg1NzMwMzIxNDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:41:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.ASSETS.LIB
*    PROGRAM SBR.ASSETS.LIB

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ASSETS.LIB

*---------------------------------------
    FN.DEP = "F.SCB.ASSETS.LIB"  ; F.DEP  = ""
    CALL OPF (FN.DEP,F.DEP)
    WS.SER = ''
    FN.USER = "F.USER"     ; F.USER  = ""
    CALL OPF (FN.USER,F.USER)

*    CALL F.READ(FN.USER,USER.ID,R.USER,F.USER,E00)

    TD = TODAY

    T.SEL = "SELECT ":FN.DEP:" WITH RECIEVE.DATE EQ ": TD :" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    PRINT 'SEL = ' :SELECTED
    WS.SER   = SELECTED + 1
    WS.SER   = FMT(WS.SER,'R%5')
    EXECUTE "chmod 775 MECH/depr.csv"

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME = 'depr.csv'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            DEBIT.ACC      = FIELD(Y.MSG,",",1)
            CREDIT.ACC     = FIELD(Y.MSG,",",2)
            CUR            = FIELD(Y.MSG,",",3)
            AMT            = FIELD(Y.MSG,",",4)
            COST.CEN       = FIELD(Y.MSG,",",5)
            COMP.CODE      = FIELD(Y.MSG,",",6)
            DR.THEIR.REF   = FIELD(Y.MSG,",",7)
            CR.THEIR.REF   = FIELD(Y.MSG,",",8)
            NOTES.1        = FIELD(Y.MSG,",",9)
            NOTES.2        = FIELD(Y.MSG,",",10)

**DEBUG
*           WS.SER = SELECTED + 1
*           WS.SER   = FMT(WS.SER,'R%5')

            DEP.ID = TD:".":WS.SER
*------------------------------------------------------------------
            CALL F.READ(FN.DEP,DEP.ID,R.DEP,F.DEP,E4)

            R.DEP<DEP.DEBIT.ACCOUNT>     = DEBIT.ACC
            R.DEP<DEP.CREDIT.ACCOUNT>    = CREDIT.ACC
            R.DEP<DEP.CURRENCY>          = CUR
            R.DEP<DEP.AMOUNT>            = AMT
            R.DEP<DEP.COST.CENTER>       = COST.CEN
            R.DEP<DEP.COMPANY.CODE>      = COMP.CODE
            R.DEP<DEP.DEBIT.THEIR.REF>   = DR.THEIR.REF
            R.DEP<DEP.CREDIT.THEIR.REF>  = CR.THEIR.REF
            R.DEP<DEP.RECIEVE.DATE>      = TODAY
            R.DEP<DEP.NOTES.1>           = NOTES.1
            R.DEP<DEP.NOTES.2>           = NOTES.2
            R.DEP<DEP.STATUS>            = "1"
            R.DEP<DEP.USER>              = R.USER<EB.USE.SIGN.ON.NAME>

            CALL F.WRITE(FN.DEP,DEP.ID,R.DEP)
            CALL JOURNAL.UPDATE(DEP.ID)
            WS.SER++
            WS.SER  = FMT(WS.SER,'R%5')

*----------------------------------
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER
    EXECUTE "sh > MECH/depr.csv"

MSG.TXT.DONE:
**  TEXT = "�� ����� �����" ; CALL REM
    GO TO EXIT.PROG

**MSG.TXT.ERR:
**    TEXT = "�� ����� ����� �� ���" ; CALL REM


EXIT.PROG:
RETURN
************************************************************
END
