* @ValidationCode : MjoxMjQ5MTEzNjQxOkNwMTI1MjoxNjQ0OTI1MDUwMjgzOmxhcDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
******* CREATED BY KHALED IBRAHIM *******
*** UPDATED BY MOHAMED SABRY 2011/08/02

** SUBROUTINE SBM.CREDIT.STATIC
PROGRAM SBM.CREDIT.STATIC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.GROUP
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

RETURN
*==============================================================
INITIATE:
    DATD.CURR = TODAY
    DATM.CURR = DATD.CURR[1,6]
    DAT.CURR  = DATM.CURR:"01"
    CALL CDT ('',DAT.CURR,'-1W')

    DATM.LAST = DAT.CURR[1,6]
    DAT.LAST  = DATM.LAST:"01"
    CALL CDT ('',DAT.LAST,'-1W')

    DAT.ID = 'EG0010001'
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,INT.DAT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
INT.DAT=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    TT = FMT(DAT.CURR,"####/##/##")
    KK = FMT(DAT.LAST,"####/##/##")

    OPENSEQ "CREDIT.STATIC" , "SBM.CREDIT.STATIC.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"CREDIT.STATIC":' ':"SBM.CREDIT.STATIC.CSV"
        HUSH OFF
    END
    OPENSEQ "CREDIT.STATIC" , "SBM.CREDIT.STATIC.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBM.CREDIT.STATIC.CSV CREATED IN CREDIT.STATIC'
        END ELSE
            STOP 'Cannot create SBM.CREDIT.STATIC.CSV File IN CREDIT.STATIC'
        END
    END

    HEAD.DESC = ",":TT:" ":KK:" ":"���� ����� ������ ����� �������� �� ����"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "�":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� �������":","
    HEAD.DESC := "��������":",,,"
    HEAD.DESC := "��������� ":KK:",,,"
    HEAD.DESC := "��������� ":TT:",,,"
    HEAD.DESC := "������":","
    HEAD.DESC := "���� ������":","
    HEAD.DESC := "���� �������� ������":","
*    HEAD.DESC := "����� ��������":","
*    HEAD.DESC := "���� ������� ������":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.SCC = 'F.SCB.STATIC.CREDIT.CUSTOMER' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.DIR.USED.AMT.LAST   = 0
    WS.INDIR.USED.AMT.LAST = 0
    TOTAL.USED.LAST        = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.CHANGE              = 0
    WS.CHANGE.PER          = 0
    WS.PER.USED.INTERNAL   = 0
*WS.USED.AVG            = 0
*WS.PER.AVG.INTERNAL    = 0
    GROUP.ID.TMP = 0
    KK = 0
****

    WS.DIR.INTERNAL.AMT.TOT    = 0
    WS.INDIR.INTERNAL.AMT.TOT  = 0
    TOTAL.INTERNAL.TOT         = 0
    WS.DIR.USED.AMT.LAST.TOT   = 0
    WS.INDIR.USED.AMT.LAST.TOT = 0
    TOTAL.USED.LAST.TOT        = 0
    WS.DIR.USED.AMT.TOT        = 0
    WS.INDIR.USED.AMT.TOT      = 0
    TOTAL.USED.TOT             = 0
    WS.CHANGE.TOT              = 0
    WS.CHANGE.PER.TOT          = 0
    WS.PER.USED.INTERNAL.TOT   = 0
*WS.USED.AVG.TOT            = 0
*WS.PER.AVG.INTERNAL.TOT    = 0
****
RETURN
*========================================================================
PROCESS:

*    T.SEL = "SELECT ":FN.SCC:" WITH LPE.DATE EQ ":DAT.LAST:" OR LPE.DATE EQ ":DAT.CURR:" OR LPE.DATE EQ ":INT.DAT:" BY GROUP.ID BY CO.CODE"
    T.SEL = "SELECT ":FN.SCC:" WITH LPE.DATE EQ ":DAT.CURR:" BY GROUP.ID BY CO.CODE "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.SCC,KEY.LIST<I>,R.SCC,F.SCC,E1)
*MSABRY TMP 2011/07/02
            GROUP.ID = R.SCC<SCC.GROUP.ID>
            IF GROUP.ID[1,3] NE 999 THEN
                GROUP.ID = R.SCC<SCC.CUSTOMER.ID>
            END

*    GROUP.ID = R.SCC<SCC.CUSTOMER.ID>
            IF GROUP.ID EQ '' THEN
                GROUP.ID = R.SCC<SCC.CUSTOMER.ID>
            END

            IF I EQ 1 THEN GROUP.ID.TMP = GROUP.ID
            IF GROUP.ID NE GROUP.ID.TMP THEN
                GOSUB DATA.FILE
            END
            CUS.ID   = R.SCC<SCC.CUSTOMER.ID>
*Line [ 204 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('SCB.CUSTOMER.GROUP':@FM:CG.GROUP.NAME,GROUP.ID,CUST.NAME)
F.ITSS.SCB.CUSTOMER.GROUP = 'F.SCB.CUSTOMER.GROUP'
FN.F.ITSS.SCB.CUSTOMER.GROUP = ''
CALL OPF(F.ITSS.SCB.CUSTOMER.GROUP,FN.F.ITSS.SCB.CUSTOMER.GROUP)
CALL F.READ(F.ITSS.SCB.CUSTOMER.GROUP,GROUP.ID,R.ITSS.SCB.CUSTOMER.GROUP,FN.F.ITSS.SCB.CUSTOMER.GROUP,ERROR.SCB.CUSTOMER.GROUP)
CUST.NAME=R.ITSS.SCB.CUSTOMER.GROUP<CG.GROUP.NAME>

            IF CUST.NAME EQ '' THEN
*Line [ 213 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            END ELSE
                CUS.ID = GROUP.ID
            END

*Line [ 225 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,COMP.ID)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.ID=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
            
*Line [ 233 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*---------------------------------------------------------
            DAT = R.SCC<SCC.LPE.DATE>
            WS.CY  = R.SCC<SCC.CURRENCY>
*Line [ 213 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CY.DC  = DCOUNT(WS.CY,@VM)
            FOR X = 1 TO CY.DC
                WS.CY.ID = R.SCC<SCC.CURRENCY,X>
                GOSUB GET.RATE
*************** CURR INTERNAL AND USED*******************************
                WS.DIR.INTERNAL.AMT   += R.SCC<SCC.DIR.INTERNAL.AMT,X>   * WS.RATE
                WS.INDIR.INTERNAL.AMT += R.SCC<SCC.INDIR.INTERNAL.AMT,X> * WS.RATE
                TOTAL.INTERNAL        += R.SCC<SCC.TOTAL.INTERNAL.AMT,X> * WS.RATE
                WS.DIR.USED.AMT       += R.SCC<SCC.DIR.USED.AMT,X>       * WS.RATE
                WS.INDIR.USED.AMT     += R.SCC<SCC.INDIR.USED.AMT,X>     * WS.RATE
                TOTAL.USED            += R.SCC<SCC.TOTAL.USED.AMT,X>     * WS.RATE
            NEXT X

            GOSUB GET.LAST.USED
            WS.CHANGE     = ( TOTAL.USED - TOTAL.USED.LAST )

            GROUP.ID.TMP = GROUP.ID

        NEXT I
        IF I EQ SELECTED THEN GOSUB DATA.FILE
    END
**** PRINT TOTALS **************

    BB.DATA  = ","
    BB.DATA := "��������":","
    BB.DATA := ","
    BB.DATA := ","

    BB.DATA := WS.DIR.INTERNAL.AMT.TOT:","
    BB.DATA := WS.INDIR.INTERNAL.AMT.TOT:","
    BB.DATA := TOTAL.INTERNAL.TOT:","
    BB.DATA := WS.DIR.USED.AMT.LAST.TOT:","
    BB.DATA := WS.INDIR.USED.AMT.LAST.TOT:","
    BB.DATA := TOTAL.USED.LAST.TOT:","
    BB.DATA := WS.DIR.USED.AMT.TOT:","
    BB.DATA := WS.INDIR.USED.AMT.TOT:","
    BB.DATA := TOTAL.USED.TOT:","
    BB.DATA := WS.CHANGE.TOT:","
    BB.DATA := WS.CHANGE.PER.TOT:","
    BB.DATA := WS.PER.USED.INTERNAL.TOT:","
*    BB.DATA := WS.USED.AVG.TOT:","
*    BB.DATA := WS.PER.AVG.INTERNAL.TOT:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*------------------------------------------------------------
******************* WRITE DATA IN FILE **********************
DATA.FILE:
***** DIVIDED BY 1,000 *****
    WS.DIR.INTERNAL.AMT    = WS.DIR.INTERNAL.AMT    / 1000
    WS.INDIR.INTERNAL.AMT  = WS.INDIR.INTERNAL.AMT  / 1000
    TOTAL.INTERNAL         = TOTAL.INTERNAL         / 1000
    WS.DIR.USED.AMT.LAST   = WS.DIR.USED.AMT.LAST   / 1000
    WS.INDIR.USED.AMT.LAST = WS.INDIR.USED.AMT.LAST / 1000
    TOTAL.USED.LAST        = TOTAL.USED.LAST        / 1000
    WS.DIR.USED.AMT        = WS.DIR.USED.AMT        / 1000
    WS.INDIR.USED.AMT      = WS.INDIR.USED.AMT      / 1000
    TOTAL.USED             = TOTAL.USED             / 1000
    WS.CHANGE              = WS.CHANGE              / 1000
*WS.USED.AVG            = WS.USED.AVG            / 1000

    WS.DIR.INTERNAL.AMT    = DROUND(WS.DIR.INTERNAL.AMT,'0')
    WS.INDIR.INTERNAL.AMT  = DROUND(WS.INDIR.INTERNAL.AMT,'0')
    TOTAL.INTERNAL         = DROUND(TOTAL.INTERNAL,'0')
    WS.DIR.USED.AMT.LAST   = DROUND(WS.DIR.USED.AMT.LAST,'0')
    WS.INDIR.USED.AMT.LAST = DROUND(WS.INDIR.USED.AMT.LAST,'0')
    TOTAL.USED.LAST        = DROUND(TOTAL.USED.LAST,'0')
    WS.DIR.USED.AMT        = DROUND(WS.DIR.USED.AMT,'0')
    WS.INDIR.USED.AMT      = DROUND(WS.INDIR.USED.AMT,'0')
    TOTAL.USED             = DROUND(TOTAL.USED,'0')
    WS.CHANGE              = DROUND(WS.CHANGE,'0')
*WS.USED.AVG            = DROUND(WS.USED.AVG,'0')

**************** CALC PERCENTAGE ****************
    IF TOTAL.USED.LAST NE 0 THEN
        WS.CHANGE.PER        = ( WS.CHANGE / TOTAL.USED.LAST ) * 100
    END
    IF TOTAL.INTERNAL NE 0 THEN
        WS.PER.USED.INTERNAL = ( TOTAL.USED / TOTAL.INTERNAL ) * 100
    END
*    IF TOTAL.INTERNAL NE 0 THEN
*        WS.PER.AVG.INTERNAL  = ( WS.USED.AVG / TOTAL.INTERNAL ) * 100
*    END

    WS.CHANGE.PER1          = DROUND(WS.CHANGE.PER,'0')
    WS.PER.USED.INTERNAL1   = DROUND(WS.PER.USED.INTERNAL,'0')
*WS.PER.AVG.INTERNAL1    = DROUND(WS.PER.AVG.INTERNAL,'0')

    WS.CHANGE.PER          = ABS (WS.CHANGE.PER1)
    WS.PER.USED.INTERNAL   = ABS (WS.PER.USED.INTERNAL1)
* WS.PER.AVG.INTERNAL    = ABS (WS.PER.AVG.INTERNAL1)

***************************************************
    TOT.PRINT = ABS (TOTAL.USED) + ABS (TOTAL.USED.LAST) + TOTAL.INTERNAL
    IF TOT.PRINT NE 0 AND TOT.PRINT NE '' THEN
        KK++
        BB.DATA  = KK:","
        BB.DATA := CUST.NAME:","
        BB.DATA := CUS.ID:","
        BB.DATA := BRANCH.NAME:","

        BB.DATA := WS.DIR.INTERNAL.AMT:","
        BB.DATA := WS.INDIR.INTERNAL.AMT:","
        BB.DATA := TOTAL.INTERNAL:","
        BB.DATA := WS.DIR.USED.AMT.LAST:","
        BB.DATA := WS.INDIR.USED.AMT.LAST:","
        BB.DATA := TOTAL.USED.LAST:","
        BB.DATA := WS.DIR.USED.AMT:","
        BB.DATA := WS.INDIR.USED.AMT:","
        BB.DATA := TOTAL.USED:","
        BB.DATA := WS.CHANGE:","
        BB.DATA := WS.CHANGE.PER:","
        BB.DATA := WS.PER.USED.INTERNAL:","
*BB.DATA := WS.USED.AVG:","
*BB.DATA := WS.PER.AVG.INTERNAL:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    END
    WS.DIR.INTERNAL.AMT.TOT    += WS.DIR.INTERNAL.AMT
    WS.INDIR.INTERNAL.AMT.TOT  += WS.INDIR.INTERNAL.AMT
    TOTAL.INTERNAL.TOT         += TOTAL.INTERNAL
    WS.DIR.USED.AMT.TOT        += WS.DIR.USED.AMT
    WS.INDIR.USED.AMT.TOT      += WS.INDIR.USED.AMT
    TOTAL.USED.TOT             += TOTAL.USED
    WS.DIR.USED.AMT.LAST.TOT   += WS.DIR.USED.AMT.LAST
    WS.INDIR.USED.AMT.LAST.TOT += WS.INDIR.USED.AMT.LAST
    TOTAL.USED.LAST.TOT        += TOTAL.USED.LAST
    WS.CHANGE.TOT     += WS.CHANGE
    WS.CHANGE.PER.TOT += WS.CHANGE.PER
*WS.USED.AVG.TOT   += WS.USED.AVG
    WS.PER.USED.INTERNAL.TOT += WS.PER.USED.INTERNAL
* WS.PER.AVG.INTERNAL.TOT  += WS.PER.AVG.INTERNAL

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.DIR.USED.AMT.LAST   = 0
    WS.INDIR.USED.AMT.LAST = 0
    TOTAL.USED.LAST        = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.CHANGE              = 0
    WS.CHANGE.PER          = 0
    WS.PER.USED.INTERNAL   = 0
*WS.USED.AVG            = 0
*WS.PER.AVG.INTERNAL    = 0

RETURN
**** ---------------------- GET SELL.RATE FROM CURRENCY FILE -------------------- ****
GET.RATE:
    IF  WS.CY.ID NE 'EGP' THEN
        CALL F.READ(FN.CCY,WS.CY.ID,R.CCY,F.CCY,E1)
        WS.RATE = R.CCY<EB.CUR.SELL.RATE,1>
        IF WS.CY.ID EQ 'JPY' THEN
            WS.RATE = ( WS.RATE / 100 )
        END
    END ELSE
        WS.RATE = 1
    END
RETURN
********************** LAST MONTH *********************************
GET.LAST.USED:
*** MOHAMED SABRY 2011/09/15
    IF  GROUP.ID.TMP # GROUP.ID THEN
        WS.DIR.USED.AMT.LAST   = 0
        WS.INDIR.USED.AMT.LAST = 0
        TOTAL.USED.LAST        = 0
    END

    WS.LAST.CU.ID = R.SCC<SCC.CUSTOMER.ID>:'.':DAT.LAST
    CALL F.READ(FN.SCC,WS.LAST.CU.ID,R.SCC.LAST,F.SCC.LAST,E1.LAST)

    WS.CY.LAST  = R.SCC.LAST<SCC.CURRENCY>
*Line [ 393 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CY.DC.LAST  = DCOUNT(WS.CY.LAST,@VM)
    FOR X.LAST = 1 TO CY.DC.LAST
        WS.CY.ID = R.SCC.LAST<SCC.CURRENCY,X.LAST>
        GOSUB GET.RATE
        WS.DIR.USED.AMT.LAST   += R.SCC.LAST<SCC.DIR.USED.AMT,X.LAST>   * WS.RATE
        WS.INDIR.USED.AMT.LAST += R.SCC.LAST<SCC.INDIR.USED.AMT,X.LAST> * WS.RATE
        TOTAL.USED.LAST        += R.SCC.LAST<SCC.TOTAL.USED.AMT,X.LAST> * WS.RATE
*     CRT WS.DIR.USED.AMT.LAST :"*":WS.CY.ID:"*":R.SCC.LAST<SCC.DIR.USED.AMT,X.LAST>:"*":WS.RATE:"*":WS.LAST.CU.ID
    NEXT X.LAST
RETURN
*---------------------------------------------------
END
