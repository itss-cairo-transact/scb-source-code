* @ValidationCode : Mjo2NzY4MDUwMTg6Q3AxMjUyOjE2NDQ5MjUwNTM4MzE6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBM.DEP.GE.250

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.DEP.GE.250'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    COMP.BR = COMP[2]

    AMT.LCY.TOT = 0 ; AMT.LCY.TOT.C = 0 ; AMT.LCY.TOT.O = 0
    CUS.ID1 = ''
RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.CBE:" WITH CBE.BR EQ ":COMP.BR:" BY CBE.CUSTOMER.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
*---------------------------------
            CUS.ID        = R.CBE<ST.CBE.CUSTOMER.CODE>
*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
            AMT.LCY.DEP   = R.CBE<ST.CBE.DEPOST.AC.LE.1Y>  + R.CBE<ST.CBE.DEPOST.AC.LE.2Y>  + R.CBE<ST.CBE.DEPOST.AC.LE.3Y>  + R.CBE<ST.CBE.DEPOST.AC.LE.MOR3>
            AMT.LCY.DEP   = DROUND(AMT.LCY.DEP,'0')

            AMT.LCY.DEP.O = R.CBE<ST.CBE.DEPOST.AC.LE.1YO> + R.CBE<ST.CBE.DEPOST.AC.LE.2YO> + R.CBE<ST.CBE.DEPOST.AC.LE.3YO> + R.CBE<ST.CBE.DEPOST.AC.LE.MOR3O>
            AMT.LCY.DEP.O   = DROUND(AMT.LCY.DEP.O,'0')

            AMT.FCY.DEP   = R.CBE<ST.CBE.DEPOST.AC.EQ.1Y>  + R.CBE<ST.CBE.DEPOST.AC.EQ.2Y>  + R.CBE<ST.CBE.DEPOST.AC.EQ.3Y>  + R.CBE<ST.CBE.DEPOST.AC.EQ.MOR3>
            AMT.FCY.DEP   = DROUND(AMT.FCY.DEP,'0')

            AMT.FCY.DEP.O = R.CBE<ST.CBE.DEPOST.AC.EQ.1YO> + R.CBE<ST.CBE.DEPOST.AC.EQ.2YO> + R.CBE<ST.CBE.DEPOST.AC.EQ.3YO> + R.CBE<ST.CBE.DEPOST.AC.EQ.MOR3O>
            AMT.FCY.DEP.O = DROUND(AMT.FCY.DEP.O,'0')

            AMT.DEP   = AMT.LCY.DEP   + AMT.FCY.DEP
            AMT.DEP.O = AMT.LCY.DEP.O + AMT.FCY.DEP.O

            AMT.LCY.DEP.C = AMT.DEP - AMT.DEP.O

            IF AMT.LCY.DEP.C LT 0 THEN
                AMT.DEPL = AMT.LCY.DEP.C * -1
            END ELSE
                AMT.DEPL = AMT.LCY.DEP.C
            END

*---------------------------------
            IF AMT.DEPL GE 250000 THEN
                XX = SPACE(120)
                XX<1,1>[1,15]   = CUS.ID
                XX<1,1>[13,35]  = CUST.NAME
                XX<1,1>[65,20]  = AMT.DEP.O
                XX<1,1>[85,20]  = AMT.DEP
                XX<1,1>[105,20] = AMT.LCY.DEP.C

                PRINT XX<1,1>
                PRINT STR('-',130)

                AMT.LCY.TOT   += AMT.DEP
                AMT.LCY.TOT.O += AMT.DEP.O
                AMT.LCY.TOT.C += AMT.LCY.DEP.C


            END
            AMT.DEP   = 0   ; AMT.LCY.DEP.C = 0     ; AMT.DEP.O = 0
        NEXT I

        PRINT STR('=',130)
        XX2 = SPACE(120)
        XX2<1,1>[1,35]   = "�����������"
        XX2<1,1>[65,20]  = AMT.LCY.TOT.O
        XX2<1,1>[85,20]  = AMT.LCY.TOT
        XX2<1,1>[105,20] = AMT.LCY.TOT.C
        PRINT XX2<1,1>
        PRINT STR('=',130)

    END
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TDD = TODAY[1,6]
    TD = TDD - 2
    TDF = TDD - 1
    IF TDD[5,2] EQ '01' THEN
        TDX = TDD[1,4] - 1
        TD = TDX : '12'
        TDF = TD - 1
    END

    TD1 = TD[1,4]:'/':TD[5,2]
    TD2 = TDF[1,4]:'/':TDF[5,2]



    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ �� ������� ������� ������� - �����":SPACE(1):TD1:SPACE(2):TD2
    PR.HD :="'L'":SPACE(55):"���� ���� �� 250 ��� ����"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(31):"������ ������":SPACE(10):"������������":SPACE(10):"������"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
