* @ValidationCode : Mjo4OTcxMDgzNjc6Q3AxMjUyOjE2NDE1NTg5ODQ5MDU6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 07 Jan 2022 14:36:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
* SUBROUTINE SBM.EMP.INT.2PRCNT.007
PROGRAM SBM.EMP.INT.2PRCNT.007

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EMP.INT.2.LOG
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EMP.INT.2.PERM

***** SCB R15 UPG 20160627  - S
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_OFS.MESSAGE.SERVICE.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_GTS.COMMON
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.OFS.SOURCE
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_GTS.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.TSA.SERVICE
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SPF
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_IO.EQUATE

    FN.OFS = "F.OFS.SOURCE"
    F.OFS = ''
    CALL OPF(FN.OFS,F.OFS)

    FN.ORQ = "F.OFS.RESPONSE.QUEUE"
    F.ORQ = ''

    OFS$SOURCE.ID = "SCBONLINE"

    ERR.OFSS = ''
    CALL F.READ(FN.OFS,OFS$SOURCE.ID,OFS$SOURCE.REC,F.OFS,ERR.OFSS)
    CALL OFS.MESSAGE.SERVICE.LOAD

***** SCB R15 UPG 20160627  - E

*---------------------------------------------------
*    EXECUTE "ITTRNSLOGING"
*---------------------------------------------------
    FN.PLOG  = "F.SCB.EMP.INT.2.LOG"    ; F.PLOG = ""
*
    FN.AC   = "FBNK.ACCOUNT"            ; F.AC = ""
*
    FN.DATE = "F.DATES"                 ; F.DATE  = ""
*
    FN.PERM = "F.SCB.EMP.INT.2.PERM"    ; F.PERM = ""
*----------------------------------------------------
    CALL OPF (FN.PLOG,F.PLOG)
    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.PERM,F.PERM)
    CALL OPF (FN.AC,F.AC)

    FN.USER = "F.USER"
    FV.USER = ""
    CALL OPF(FN.USER,FV.USER)

    FN.USER.SIGN.ON.NAME = "F.USER.SIGN.ON.NAME"
    FV.USER.SIGN.ON.NAME = ""
    CALL OPF(FN.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME)

    WS.ORG.USR = R.USER<EB.USE.SIGN.ON.NAME>
*-------------------------------------------------
    R.TEMP = ""
*------------------------------------------------
    GOSUB A.10.DATE
    GOSUB A.50.GET.INT
*    GOSUB GET.ORG.PROFILE
RETURN
*----------------------------------------
A.10.DATE:

    WS.DATE.ID      = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.PER.END = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LPE.DATE      = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LAST.PER.END = WS.LPE.DATE
    WS.YY.MM        = WS.LAST.PER.END[1,6]
    WS.DD           = WS.LAST.PER.END[2]
    WS.MM           = WS.YY.MM[5,2] + 0
    WS.LOCAT.DATE   = WS.YY.MM:"01"
    WS.ACC.OFFICER  = ID.COMPANY[2]
    WS.FT.REF       = ''

    WS.TRNS.DATE = TODAY[1,6]:"01"

***** SCB R15 UPG 20160627  - S
*#*    SCB.OFS.SOURCE = "TESTOFS"
    SCB.OFS.SOURCE = "SCBONLINE"
***** SCB R15 UPG 20160627  - E

    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION  = "1"

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    OPENSEQ "OFS.MNGR.IN" , "EMP.2.INT.PERM.IN":TODAY TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"EMP.2.INT.PERM.IN":TODAY
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , "EMP.2.INT.PERM.IN":TODAY TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create EMP.2.INT.PERM.IN":TODAY:" File IN OFS.MNGR.IN'
        END
    END

    OPENSEQ "OFS.MNGR.OUT" , "EMP.2.INT.PERM.OUT":TODAY TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"EMP.2.INT.PERM.OUT":TODAY
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "EMP.2.INT.PERM.OUT":TODAY TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create EMP.2.INT.PERM.OUT":TODAY:" File IN OFS.MNGR.OUT'
        END
    END


RETURN
*-----------------------------------------
**** ADDED BY MOHAMED SABRY 2012/09/09

A.50.GET.INT:
*------------
*    SEL.PERM = "SELECT ":FN.PERM:" WITH MONTH.PAY EQ ":WS.MM:" AND CO.CODE EQ ":ID.COMPANY:" BY @ID "
    SEL.PERM = "SELECT ":FN.PERM:" WITH MONTH.PAY EQ ":WS.MM:" BY @ID "
*   SEL.PERM = "SELECT ":FN.PERM:" WITH CO.CODE EQ ":ID.COMPANY:" BY MONTH.PAY BY @ID "
    CALL EB.READLIST(SEL.PERM,KEY.LIST.PERM,"",SELECTED.PERM,ER.MSG.PERM)
*PRINT SELECTED.PERM
    IF SELECTED.PERM THEN
        FOR I.PERM = 1 TO SELECTED.PERM
            CALL F.READ(FN.PERM,KEY.LIST.PERM<I.PERM>,R.PERM,F.PERM,ER.PERM)

            WS.AC.ID.PERM = KEY.LIST.PERM<I.PERM>

            CALL F.READ(FN.AC,WS.AC.ID.PERM,R.AC,F.AC,ER.AC)
            IF NOT(ER.AC) THEN

*Line [ 178 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.COUNT.INT = DCOUNT(R.PERM<EMP.PERM.INT>,@VM)

                WS.TOT.AC.INT = 0

                IF WS.COUNT.INT GE 1 THEN

                    FOR I.COUNT = 1 TO WS.COUNT.INT
                        WS.TOT.AC.INT +=  R.PERM<EMP.PERM.INT><1,I.COUNT>
                    NEXT I.COUNT

                    IF WS.TOT.AC.INT GT 0 THEN
                        WS.PRODUCT.CATEGORY = R.AC<AC.CATEGORY>
                        WS.AC.CY            = R.AC<AC.CURRENCY>
                        WS.AC.CUSTOMER      = R.AC<AC.CUSTOMER>
                        WS.AC.INT.ACC       = R.AC<AC.INTEREST.LIQU.ACCT>
                        WS.BR.CODE          = R.AC<AC.CO.CODE>[8,2]
                        GOSUB FT.INT
                    END
                END
            END
            GOSUB SCB.MOVE.TO.LOG
        NEXT I.PERM
    END

RETURN


*------------------------------------------------------------------
FT.INT:
*DEBUG
*------------------------------------------------------------------
    CR.ACCT = WS.AC.ID.PERM

    IF WS.PRODUCT.CATEGORY EQ 6511 THEN
        CR.ACCT  =  WS.AC.INT.ACC
    END

    DB.ACCT = WS.AC.CY:"1660600":WS.PRODUCT.CATEGORY[2]:"00":WS.BR.CODE

    OFS.MESSAGE.DATA  =  ",TRANSACTION.TYPE::=":"AC59"
    OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":WS.AC.CY
    OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":WS.AC.CY
    OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
    OFS.MESSAGE.DATA :=  ",DEBIT.THEIR.REF::=":CR.ACCT
    OFS.MESSAGE.DATA :=  ",CREDIT.THEIR.REF::=":WS.AC.ID.PERM
    OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
    OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":WS.TOT.AC.INT
    OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":WS.TRNS.DATE
    OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":WS.TRNS.DATE
    OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
*    OFS.MESSAGE.DATA :=  ",SEND.PAYMENT.Y.N::=":"NO"
    OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
    OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

*    SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.BR.CODE:"//EG00100":WS.BR.CODE:"," : ACCT.ID
    SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.BR.CODE:"//EG00100":WS.BR.CODE:"," : OFS.MESSAGE.DATA

    BB.IN.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END
** ADDED BY MSABRY 2014/05/15
    GOSUB GEN.OFS.USER
**
***** SCB R15 UPG 20160627  - S

*#*  CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
*20160810 - S/E
    SCB.OFS.SOURCE = "SCBONLINE"
    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
*----
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*     CALL OFS.POST.MESSAGE(SCB.OFS.MESSAGE, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)

*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,SCB.OFS.MESSAGE,'')
*    CALL OFS.MESSAGE.SERVICE(SCB.OFS.ID)

*    R.ORQ = ''; ERR.ORQ = ''
*    Y.ORQ.ID = FIELD(SCB.OFS.ID,"-",1):".1"
*    CALL F.READ(FN.ORQ,Y.ORQ.ID,R.ORQ,F.OFQ,ERR.ORQ)
*    SCB.OFS.MESSAGE = ''
*    IF R.ORQ THEN
*        SCB.OFS.MESSAGE.FT.ID = R.ORQ<3>
*        SCB.OFS.MESSAGE.ARRAY = R.ORQ<2>
*        SCB.OFS.MESSAGE.FLAG  = R.ORQ<1>
*
*        SCB.OFS.MESSAGE = SCB.OFS.MESSAGE.FT.ID:"/":OFS$SOURCE.REC<OFS.SRC.DET.PREFIX>:Y.ORQ.ID[1,10]:"/":SCB.OFS.MESSAGE.FLAG:",":SCB.OFS.MESSAGE.ARRAY

*    END
***** SCB R15 UPG 20160627  - E


    BB.OUT.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
    END
    WS.FT.REF  = FIELD(SCB.OFS.MESSAGE,"/",1)

RETURN
*-------------------------------------------------
SCB.MOVE.TO.LOG:

    R.PLOG       =  R.PERM

    IF  WS.FT.REF NE '' THEN
        R.PLOG<EPLOG.OUR.REFERENCE>   = WS.FT.REF
        R.PLOG<EPLOG.PAYMENT.ACCOUNT> = CR.ACCT
        WS.FT.REF                     = ''
    END ELSE
        R.PLOG<EPLOG.OUR.REFERENCE>   = 'NOT.PAID'
        WS.FT.REF                     = ''
        R.PLOG<EPLOG.PAYMENT.ACCOUNT> = ''
    END


    PLOG.ID = WS.AC.ID.PERM :'-':WS.TRNS.DATE

    CALL F.WRITE(FN.PLOG,PLOG.ID,R.PLOG)
    CALL JOURNAL.UPDATE(PLOG.ID)
    CALL F.RELEASE(FN.PLOG,PLOG.ID,F.PLOG)

    CALL F.DELETE(FN.PERM,WS.AC.ID.PERM)
    CALL JOURNAL.UPDATE(WS.AC.ID.PERM)
    CALL F.RELEASE(FN.PERM,WS.AC.ID.PERM,F.PERM)
RETURN
*-------------------------------------------------
** ADDED BY MSABRY 2014/05/15
****************************************
GEN.OFS.USER:

    OPERATOR        = "INPUTT":WS.BR.CODE
    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")

*Line [ 314 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CONT = DCOUNT(R.USER<EB.USE.OVERRIDE.CLASS>,@VM)
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "DRMT"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "NPOS"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "APOS"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "LKAM"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "OVER"

RETURN
****************************************
GET.ORG.PROFILE:
    OPERATOR        = WS.ORG.USR
    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")

*    CU.SCB.OFS.MESSAGE = CU.SCB.OFS.HEADER : "/I/PROCESS,":OPERATOR:"//EG0010099"
    OPERATOR        = R.USER.SIGN.ON.NAME

    SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,":R.USER.SIGN.ON.NAME:"//EG0010099"
*    CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
*20160810 - S/E
    SCB.OFS.SOURCE = "SCBONLINE"
    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

RETURN

****************************************
END
