* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-85</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.CONTRACT.SAL.FT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
************ OPEN FILES ****************

    FN.MCH = "F.SCB.SUEZ.MECH.SAL"  ; F.MCH  = ""
    CALL OPF (FN.MCH,F.MCH)
    TOT.AMT = 0
    WS.AMT  = 0  ; WS.GE300 = 0 ; WS.LT300 = 0

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION  = "MECH"
    SCB.VERSION1 = "MECH1"

    SCB.OFS.HEADER  = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,,,"
    SCB.OFS.HEADER1 = SCB.APPL : "," : SCB.VERSION1 : "/I/PROCESS,,,"

    OPENSEQ "MECH" , "CONTRACT.SAL.OUT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"CONTRACT.SAL.OUT"
        HUSH OFF
    END
    OPENSEQ "MECH" , "CONTRACT.SAL.OUT" TO BB ELSE
        CREATE BB THEN
        END ELSE
            STOP 'Cannot create CONTRACT.SAL.OUT File IN MECH'
        END
    END

*--------------------------------------------------------------------
**************** DEBIT FROM COMPANY CREDIT TO MIGRATION  ************

    T.SEL1 = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '1' AND TRN.CODE EQ 'AC49'"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR NN = 1 TO SELECTED1
            CALL F.READ(FN.MCH,KEY.LIST1<NN>,R.MCH,F.MCH,E4)
            TOT.AMT += R.MCH<MCH.AMOUNT>
            R.MCH<MCH.STATUS> = "2"

            WS.AMT = R.MCH<MCH.AMOUNT>
            IF WS.AMT GE 300 THEN
                WS.GE300++
            END ELSE
                WS.LT300++
            END

            CALL F.WRITE(FN.MCH,KEY.LIST1<NN>,R.MCH)
            CALL JOURNAL.UPDATE(KEY.LIST1<NN>)

        NEXT NN

        DB.ACCT     = "0130008910100101"
        CR.ACCT.MRG = "EGP1200100010001"

        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC49"
        OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":"EGP"
        OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":"EGP"
        OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
        OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT.MRG
        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":TOT.AMT
        OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
*        OFS.MESSAGE.DATA :=  ",SEND.PAYMENT.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

        SCB.OFS.MESSAGE = SCB.OFS.HEADER1 : OFS.MESSAGE.DATA
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END
***** CALC COMMISSION ****

        WS.COM.AMT.1 = WS.GE300 * 3
        WS.COM.AMT.2 = WS.LT300 * 2
        WS.COM.AMT   = WS.COM.AMT.1 + WS.COM.AMT.2

        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC49"
        OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":"EGP"
        OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":"EGP"
        OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
        OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":"PL52157"
        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":WS.COM.AMT
        OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
*        OFS.MESSAGE.DATA :=  ",SEND.PAYMENT.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

        SCB.OFS.MESSAGE = SCB.OFS.HEADER1 : OFS.MESSAGE.DATA
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END
**************************
********** CALC MAIL CHARGE *****

        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC49"
        OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":"EGP"
        OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":"EGP"
        OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
        OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":"PL52014"
        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":"10"
        OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
*        OFS.MESSAGE.DATA :=  ",SEND.PAYMENT.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

        SCB.OFS.MESSAGE = SCB.OFS.HEADER1 : OFS.MESSAGE.DATA

*********************************

*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END

        TEXT = '�� ����� �� ���� ������ �������� ������ ������' ; CALL REM
        TEXT = '�� ��� ������� �� ���� ������' ; CALL REM
        TEXT = '�� ��� ������ ������ �� ���� ������' ; CALL REM

    END

    IF NOT(SELECTED1) THEN
        TEXT = '����� ����� �� ���� ������ ��� �� ���' ; CALL REM
    END
*--------------------------------------------------------------------
**************** DEBIT FROM MIGRATION CREDIT TO CLIENTS ************

    T.SEL = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '2' AND TRN.CODE EQ 'AC49'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
            CALL F.READ(FN.MCH,KEY.LIST<KK>,R.MCH,F.MCH,E4)
            AMT      = R.MCH<MCH.AMOUNT>
            CR.ACCT  = R.MCH<MCH.ACCOUNT.NO>
            DB.ACCT.MRG = "EGP1200100010001"
*------------------------------------------------------------------
            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC49"
            OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":"EGP"
            OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":"EGP"
            OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT.MRG
            OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
            OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":AMT
            OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
            OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
            OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
*            OFS.MESSAGE.DATA :=  ",SEND.PAYMENT.Y.N::=":"NO"
            OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
            OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

            SCB.OFS.MESSAGE = SCB.OFS.HEADER1 : OFS.MESSAGE.DATA

*            CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END

            ERR.FLAG = FIELD(SCB.OFS.MESSAGE,'/',3)[0,2]
            FT.ID    = FIELD(SCB.OFS.MESSAGE,'/',1)

            IF ERR.FLAG NE "-1" THEN
                R.MCH<MCH.STATUS> = "3"
                R.MCH<MCH.REF>    = FT.ID
                CALL F.WRITE(FN.MCH,KEY.LIST<KK>,R.MCH)
                CALL JOURNAL.UPDATE(KEY.LIST<KK>)
            END

        NEXT KK
        TEXT = '�� ����� �� ������ ������ �������� ������� �������' ; CALL REM
    END
    IF NOT(SELECTED) THEN
        TEXT = '����� ����� �� ������ ������ ��� �� ���' ; CALL REM
    END

*----------------------------------

    RETURN
************************************************************
END
