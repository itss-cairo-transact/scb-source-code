* @ValidationCode : Mjo2MTk5MjcyMzc6Q3AxMjUyOjE2NDA3Njc4OTAwNDI6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 10:51:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBM.C.PRT.2000.FILL.2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.MAST.P
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.1900.FILL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CBE.CODES
*------------------------------------------
    FN.TMP = "F.SCB.1900.FILL"   ; F.TMP    = ""
    CALL OPF(FN.TMP, F.TMP)
    FN.TMP.1 = "F.SCB.CBE.CODES" ; F.TMP.1  = ""
    CALL OPF(FN.TMP.1, F.TMP.1)
    TMP.SERIAL = "50000"         ; CBE.CODE = ""
*-------------------------------------------
    WS.OK  = ""
    FN.CUS = "F.CUSTOMER"
    F.CUS  = ""

    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*------------------------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CUS,F.CUS)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------- CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
*    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    WS.FRST.TIME = 0
    WS.TO = 0
    WS.SECTOR.NAME = ""
    WS.CUS.KEY = ""
    MSG.CUS = ""
    WS.CUS.NAME = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.PAG.COUNT = 0
    WS.LIN.COUNT = 0
    WS.1.LE = "0"
    WS.1.EQV = "0"
    WS.1.LE.LIN = 0
    WS.1.EQV.LIN = 0
    WS.1.LE.GRND.TOT  = 0
    WS.1.EQV.GRND.TOT  = 0

    SUB.A = 0
    WS.LAST = 0
    WS.TO.COMPAR = 0
    WS.NAME = ""
    WS.DPST = "0"
    WS.DPST1 = "0"
    WS.CER = "0"
    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.INDST.COMP = 0
    WS.HD.T  = "����� ����� ������� � ��������� ���������� � ������� �������� "

    WS.HD.TA = " ����� ��� 2000 "

    WS.HD.T2 = "�������� ������ ���� ������� �����"
    WS.HD.T2A = "����    "
    WS.HD.T3  = "������ ���� ����"

    WS.HD.1  = "���� �����"
    WS.HD.1A = "���� ������"
*------------------------------
    WS.HD.4A = "���� �����"
    WS.HD.4B = "���� ������"
    WS.HD.4C = "��������"
*------------------------------
    WS.HD.2  = "��������� ����������"
    WS.HD.3 = "� ������� �������� ��������"
*--------------------------------------
    WS.PRG.1 = "SBM.C.PRT.2000.4"
*------------------------------------------------
*��� ������ �������
*��� ������� �������
    DIM ARRAY1(13,7)

    ARRAY1(1,1) = "������ ������� ����� � ����� � ������ "
    ARRAY1(1,2) = "100"
    ARRAY1(1,3) = "101"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"
    ARRAY1(1,6) = ""
    ARRAY1(1,7) = ""

    ARRAY1(2,1) = "������ ������� �������                "
    ARRAY1(2,2) = "200"
    ARRAY1(2,3) = "201"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = ""
    ARRAY1(2,7) = ""

    ARRAY1(3,1) = "������ ������� �������� ��������      "
    ARRAY1(3,2) = "300"
    ARRAY1(3,3) = "301"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = ""
    ARRAY1(3,7) = ""

    ARRAY1(4,1) = "������ ������� �������� ���������     "
    ARRAY1(4,2) = "400"
    ARRAY1(4,3) = "401"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = ""
    ARRAY1(4,7) = ""

    ARRAY1(5,1) = "������ ������� �������� ��������      "
    ARRAY1(5,2) = "500"
    ARRAY1(5,3) = "501"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = ""
    ARRAY1(5,7) = ""

    ARRAY1(6,1) = "������ ������� ������� � �������      "
    ARRAY1(6,2) = "600"
    ARRAY1(6,3) = "601"
    ARRAY1(6,4) = "0"
    ARRAY1(6,5) = "0"
    ARRAY1(6,6) = ""
    ARRAY1(6,7) = ""


    ARRAY1(7,1) = "������ ������� ������� ������ ������   "
    ARRAY1(7,2) = "700"
    ARRAY1(7,3) = "701"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"
    ARRAY1(7,6) = ""
    ARRAY1(7,7) = ""

    ARRAY1(8,1) = "������ ������� ����� ����� � ������   "
    ARRAY1(8,2) = "800"
    ARRAY1(8,3) = "801"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = ""
    ARRAY1(8,7) = ""

    ARRAY1(9,1) = "������ ������� ������� ����������� ����"
    ARRAY1(9,2) = "900"
    ARRAY1(9,3) = "901"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = ""
    ARRAY1(9,7) = ""

    ARRAY1(10,1) = "������ ������� ���� �������          "
    ARRAY1(10,2) = "1000"
    ARRAY1(10,3) = "1001"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = ""
    ARRAY1(10,7) = ""

    ARRAY1(11,1) = "������ ������� ������� ��� ������    "
    ARRAY1(11,2) = "1100"
    ARRAY1(11,3) = "1101"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = ""
    ARRAY1(11,7) = ""

    ARRAY1(12,1) = "������ ������� �������� � ������� ����"
    ARRAY1(12,2) = "1200"
    ARRAY1(12,3) = "1201"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = ""
    ARRAY1(12,7) = ""

    ARRAY1(13,1) = "������ ������� ����� ����� ���� ���  "
    ARRAY1(13,2) = "1300"
    ARRAY1(13,3) = "1301"
    ARRAY1(13,4) = "0"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = ""
    ARRAY1(13,7) = ""

********************** ********************************
*******************   PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    GOSUB A.050.GET.ALL.BR
    GOSUB ADD.CODES
RETURN
ADD.CODES:
*--- --- GET ALL MISSING CODES
    TMP.SERIAL++
    TMP.SERIAL++
    SS.SEL = "" ; SEL.LIST.S = "" ;SELECTED.S = 0 ; ERR.S = ""
    N.SEL = ""  ; SEL.LIST.N = "" ;SELECTED.N = 0 ; ERR.N1 = ""
    SS.SEL = "SELECT F.SCB.CBE.CODES WITH @ID GE 500 AND @ID LT 1000 BY @ID"
    CALL EB.READLIST(SS.SEL,SEL.LIST.S,"",SELECTED.S,ERR.S)
    IF SELECTED.S THEN
        FOR NN = 1 TO SELECTED.S
            N.SEL = "SELECT F.SCB.1900.FILL WITH CBE.CODE EQ ":SEL.LIST.S<NN>
            CALL EB.READLIST(N.SEL,SEL.LIST.N,"",SELECTED.N,ERR.N1)
            IF SELECTED.N EQ '' OR SELECTED.N EQ 0 THEN
                R.TMP = "" ; MSG.TMP = ""
                CALL F.READ(FN.TMP,TMP.SERIAL,R.TMP,F.TMP,MSG.TMP)
                R.TMP<CBE.1900.CBE.CODE> = SEL.LIST.S<NN>
                R.TMP<CBE.1900.AMT.1>  = 0
                R.TMP<CBE.1900.AMT.2>  = 0
                R.TMP<CBE.1900.AMT.3>  = 0
                R.TMP<CBE.1900.AMT.4>  = 0
                R.TMP<CBE.1900.AMT.5>  = 0
                R.TMP<CBE.1900.AMT.6>  = 0
                R.TMP<CBE.1900.AMT.7>  = 0
                R.TMP<CBE.1900.AMT.8>  = 0
                R.TMP<CBE.1900.AMT.9>  = 0
                R.TMP<CBE.1900.AMT.10> = 0
                R.TMP<CBE.1900.AMT.11> = 0
                R.TMP<CBE.1900.AMT.12> = 0
                CALL F.WRITE(FN.TMP,TMP.SERIAL,R.TMP)
                CALL JOURNAL.UPDATE(TMP.SERIAL)
                TMP.SERIAL++
            END
        NEXT NN
    END
RETURN
*----------------------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE 99 THEN
            GOTO A.050.A
        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        GOSUB A.150.PROCESS
        IF WS.FLAG.PRT = 1 THEN
            WS.FLAG.PRT = 0

            WS.LAST = 1
            GOSUB A.300.PRT.TOT
            WS.S = 1
            GOSUB A.1000.CLR
        END
A.050.A:
    REPEAT
RETURN
*----------------------------------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE DD... AND CBE.NEW.INDUSTRY NE ''"
    SEL.CMD := " AND CBE.NEW.INDUSTRY LT 2000"
    SEL.CMD := " AND CBE.NEW.SECTOR IN ( 1120 2120 3120 4120 ) BY CBE.NEW.INDUSTRY"

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS
        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        WS.BRX = R.CBE<P.CBE.BR>
        IF WS.BRX NE WS.BR THEN
            GOTO START.B
        END
A.100.A:
        WS.NEW.SECTOR = R.CBE<P.CBE.NEW.SECTOR>
*                ������� �� ����� ���� ������� �����
        IF WS.NEW.SECTOR EQ 1120 OR WS.NEW.SECTOR EQ  2120 OR WS.NEW.SECTOR EQ 3120 OR WS.NEW.SECTOR EQ 4120 THEN
            GOTO START.A
        END
        GOTO START.B
START.A:
        WS.INDSTRY = R.CBE<P.CBE.NEW.INDUSTRY>
*           ����� ��     ���� ���� ������� �����    INDUSTRY
        IF WS.INDST.COMP = 0 THEN
            WS.INDST.COMP = WS.INDSTRY
        END

        WS.OK = 0
        GOSUB A.050.CHK.INDSTRY
        IF WS.OK = 0  THEN
            GOTO START.B
        END
START.A1:
        WS.1.LE  = 0
        WS.1.EQV = 0

        WS.1.LE = R.CBE<P.CBE.FACLTY.LE> + R.CBE<P.CBE.COMRCL.PAPER.LE>
        WS.1.LE = WS.1.LE + R.CBE<P.CBE.CUR.AC.LE.DR>

        WS.1.EQV = R.CBE<P.CBE.FACLTY.EQ> + R.CBE<P.CBE.COMRCL.PAPER.EQ>
        WS.1.EQV = WS.1.EQV +  R.CBE<P.CBE.CUR.AC.EQ.DR>
*** ADD BY BAKRY & NESMA 20171024 **** START
        WS.1.EQV += R.CBE<P.CBE.LOANS.LE.L>
*** ADD BY BAKRY & NESMA 20171024 **** END

        ARRAY1(WS.SUB.ACUM,4) = ARRAY1(WS.SUB.ACUM,4) + WS.1.LE
        ARRAY1(WS.SUB.ACUM,5) = ARRAY1(WS.SUB.ACUM,5) + WS.1.EQV
        WS.FLAG.PRT = 1
*-----------------------------------------------------
START.B:
    REPEAT
RETURN
A.050.CHK.INDSTRY:
    FOR SUB.A  = 1 TO 13
        IF WS.INDSTRY EQ ARRAY1(SUB.A,2) THEN
            WS.SUB.ACUM = SUB.A
            WS.OK = 1
            RETURN
        END
    NEXT SUB.A
RETURN
**************����� ������� �������
A.150.PROCESS:
    SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE DD... AND CBE.NEW.INDUSTRY NE ''"
    SEL.CMD := " AND CBE.NEW.INDUSTRY LT 2000"
    SEL.CMD := " AND CBE.NEW.SECTOR IN ( 1120 2120 3120 4120 ) BY CBE.NEW.INDUSTRY"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS

        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        IF WS.BR = 99 THEN
            GOTO A.150.A
        END
        WS.BRX = R.CBE<P.CBE.BR>
        IF WS.BRX NE WS.BR THEN
            GOTO START.150.B
        END
A.150.A:

        WS.NEW.SECTOR = R.CBE<P.CBE.NEW.SECTOR>
*                             ������� �� ����� ���� ������� �����
        IF WS.NEW.SECTOR EQ 1120 OR WS.NEW.SECTOR EQ  2120 OR WS.NEW.SECTOR EQ 3120 OR WS.NEW.SECTOR EQ 4120 THEN
            GOTO START.150.A
        END

        GOTO START.150.B
START.150.A:
        WS.INDSTRY = R.CBE<P.CBE.NEW.INDUSTRY>
*           ����� ��     ���� ���� ������� �����    INDUSTRY

        WS.OK = 0
        GOSUB A.170.CHK.INDSTRY
        IF WS.OK = 0  THEN
            GOTO START.150.B
        END

        WS.1.LE = 0
        WS.1.EQV = 0

        WS.1.LE   = R.CBE<P.CBE.FACLTY.LE> + R.CBE<P.CBE.COMRCL.PAPER.LE>
        WS.1.EQV  = R.CBE<P.CBE.FACLTY.EQ> + R.CBE<P.CBE.COMRCL.PAPER.EQ>
*---- 20171206
        WS.1.EQV += R.CBE<P.CBE.LOANS.LE.L>
        WS.FLAG.PRT = 1
        GOSUB A.200.PRT.DTAL
START.150.B:
    REPEAT
RETURN
*-----------------------------------------------------
A.170.CHK.INDSTRY:
    FOR SUB.A  = 1 TO 13
        IF WS.INDSTRY EQ ARRAY1(SUB.A,2) THEN
            GOSUB A.190.PRT.COMP.HEAD
            RETURN
        END
        IF WS.INDSTRY GT ARRAY1(SUB.A,2) AND WS.INDSTRY EQ ARRAY1(SUB.A,3) THEN
            GOSUB A.190.PRT.COMP.HEAD
            WS.OK = 1
            RETURN
        END
        IF WS.INDSTRY GT ARRAY1(SUB.A,3)  THEN
            GOSUB A.300.PRT.TOT
        END

    NEXT SUB.A
RETURN
*-------------------------------------------------
A.190.PRT.COMP.HEAD:
    IF ARRAY1(SUB.A,6) EQ "P"    THEN
        RETURN
    END
    GOSUB  A.5000.PRT.HEAD
    XX = SPACE(132)
    XX<1,1>[1,35]     = ARRAY1(SUB.A,1)
    WS.1.LE.LINE      = ARRAY1(SUB.A,4) / 1000
    WS.1.EQV.LINE     = ARRAY1(SUB.A,5) / 1000
    WS.1.LE.GRND.TOT  = WS.1.LE.GRND.TOT + WS.1.LE.LINE
    WS.1.EQV.GRND.TOT = WS.1.EQV.GRND.TOT + WS.1.EQV.LINE
    XX<1,1>[42,9]     = FMT(WS.1.LE.LINE, "R0")
    XX<1,1>[55,9]     = FMT(WS.1.EQV.LINE, "R0")
    WS.COMN           = WS.1.LE.LINE + WS.1.EQV.LINE
    XX<1,1>[68,9]     = FMT(WS.COMN, "R0")

    CALL F.READ(FN.TMP,TMP.SERIAL,R.TMP,F.TMP,E.TMP)
    VAR.1 = FMT(WS.1.LE.LINE, "R0")
    VAR.2 = FMT(WS.1.EQV.LINE, "R0")
    VAR.3 = FMT(WS.COMN, "R0")

    IF VAR.1 EQ '' THEN VAR.1 = 0
    IF VAR.2 EQ '' THEN VAR.2 = 0
    IF VAR.3 EQ '' THEN VAR.3 = 0

    R.TMP<CBE.1900.AMT.10> = VAR.1
    R.TMP<CBE.1900.AMT.11> = VAR.2
    R.TMP<CBE.1900.AMT.12> = VAR.3
    CALL F.WRITE(FN.TMP,TMP.SERIAL,R.TMP)
    CALL JOURNAL.UPDATE(TMP.SERIAL)
    TMP.SERIAL++

**    PRINT XX<1,1>
    ARRAY1(SUB.A,6) = "P"
RETURN
*-------------------------------------------------
A.200.PRT.DTAL:
    WS.1.LE = WS.1.LE / 1000

    WS.1.EQV = WS.1.EQV / 1000

    WS.1.LE.GRND.TOT = WS.1.LE.GRND.TOT + WS.1.LE
    WS.1.EQV.GRND.TOT = WS.1.EQV.GRND.TOT + WS.1.EQV

    WS.CUS.ID = R.CBE<P.CBE.CUSTOMER.CODE>
    CALL F.READ(FN.CUS,WS.CUS.ID,R.CUS,F.CUS,MSG.CUS)
    WS.NAME = "********************"
    IF MSG.CUS EQ "" THEN
        WS.NAME =  R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
    END
    XX = SPACE(132)
    XX<1,1>[1,35]  = WS.NAME
    XX<1,1>[42,9]  = FMT(WS.1.LE, "R0")
    XX<1,1>[55,9]  = FMT(WS.1.EQV, "R0")
    WS.COMN        = WS.1.LE + WS.1.EQV
    XX<1,1>[68,9]  = FMT(WS.COMN, "R0")
*===========
    CALL F.READ(FN.TMP,TMP.SERIAL,R.TMP,F.TMP,E.TMP)
    VAR.1 = FMT(WS.1.LE, "R0")
    VAR.2 = FMT(WS.1.EQV, "R0")
    VAR.3 = FMT(WS.COMN, "R0")

    IF VAR.1 EQ '' THEN VAR.1 = 0
    IF VAR.2 EQ '' THEN VAR.2 = 0
    IF VAR.3 EQ '' THEN VAR.3 = 0

    R.TMP<CBE.1900.AMT.10> = VAR.1
    R.TMP<CBE.1900.AMT.11> = VAR.2
    R.TMP<CBE.1900.AMT.12> = VAR.3
    CALL F.WRITE(FN.TMP,TMP.SERIAL,R.TMP)
    CALL JOURNAL.UPDATE(TMP.SERIAL)
    TMP.SERIAL++
**PRINT XX<1,1>
    WS.FLAG.PRT = 1
RETURN
*-----------------------------------------------------
A.300.PRT.TOT:
    IF  ARRAY1(SUB.A,7) = "P"  THEN
        RETURN
    END
A.300.A:
    XX = SPACE(132)
    XX<1,1>[42,9]   = "========="
    XX<1,1>[55,9]  =  "========="
    XX<1,1>[68,9] =  "========="
**    PRINT XX<1,1>
********************************************
    XX = SPACE(132)
    XX<1,1>[1,35]   = "������ ������ ������� �������� �������"
*
    XX<1,1>[42,9]   = FMT(WS.1.LE.GRND.TOT, "R0")

    XX<1,1>[55,9]  = FMT(WS.1.EQV.GRND.TOT, "R0")
    WS.COMN = WS.1.LE.GRND.TOT + WS.1.EQV.GRND.TOT
    XX<1,1>[68,9] = FMT(WS.COMN, "R0")
**PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[42,9]   = "========="
    XX<1,1>[55,9]  =  "========="
    XX<1,1>[68,9] =  "========="
**PRINT XX<1,1>
    WS.1.LE.GRND.TOT =  0
    WS.1.EQV.GRND.TOT = 0

*   IF WS.LAST = 1 THEN
*       RETURN
*   END
    ARRAY1(SUB.A,7) = "P"
RETURN
*------------------------------------------------------
A.1000.CLR:
    IF WS.S GT  13 THEN
        RETURN
    END
    ARRAY1(WS.S,4) = "0"
    ARRAY1(WS.S,5) = "0"
    ARRAY1(WS.S,6) = ""
    ARRAY1(WS.S,7) = ""
    WS.S = WS.S + 1
    GOTO A.1000.CLR
RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

    DATY  = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    WS.PAG.COUNT = WS.PAG.COUNT + 1
    PR.HD :="'L'":SPACE(50):WS.HD.T2:SPACE(28):WS.HD.T2A:SPACE(2):WS.PAG.COUNT
    PR.HD :="'L'":SPACE(110):WS.HD.T3
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(45):WS.HD.2
    PR.HD :="'L'":SPACE(45):WS.HD.3
    PR.HD :="'L'":SPACE(41):WS.HD.4A:SPACE(4):WS.HD.4B:SPACE(3):WS.HD.4C

    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
**PRINT
**HEADING PR.HD
RETURN
A.5100.PRT.SPACE.PAGE:
    PR.HD ="'L'":SPACE(132)
**PRINT
**HEADING PR.HD
RETURN
END
