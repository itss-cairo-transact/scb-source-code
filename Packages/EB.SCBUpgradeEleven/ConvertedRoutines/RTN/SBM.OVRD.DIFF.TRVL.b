* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDQ5MjUwNTg1ODc6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*******---------BAKRY-------2020/06/09-----------******
    SUBROUTINE SBM.OVRD.DIFF.TRVL(Y.RET.DATA)
*    PROGRAM SBM.OVRD.DIFF.TRVL

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    GOSUB OPENFILES
    GOSUB PROCESS
    RETURN



OPENFILES:
*---------
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.DR'
    F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ADI = "FBNK.ACCOUNT.DEBIT.INT" ; F.ADI = "" ; R.ADI = ""
    CALL OPF(FN.ADI,F.ADI)
    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)
    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    TT = TODAY
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    TTS = DAT
    START.DAT = DAT[1,6]:'01'
    END.DAT = DAT
    MON.NO.DAYS = "C"
    CALL CDD("",START.DAT,END.DAT,MON.NO.DAYS)
    MON.NO.DAYS ++

    ACCC=''
*============================================================

    FN.ACCT = 'FBNK.ACCOUNT'     ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    FN.STMT.ACCT.DR = 'FBNK.STMT.ACCT.DR'     ; F.STMT.ACCT.DR = ''
    CALL OPF(FN.STMT.ACCT.DR,F.STMT.ACCT.DR)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.BI.DATE = 'FBNK.BASIC.INTEREST.DATE' ; F.BI.DATE = '';R.BI.DATE=''
    CALL OPF(FN.BI.DATE,F.BI.DATE)

*------------------------------ END CREATE HEADER -------------------------------
    CHK.WRT.OLD = '' ; CHK.WRT.NEW = '' ; DAYS = 0
    RETURN
*--------------------------------------------------------------------------------
PROCESS:
*-------
*    T.SEL.ACCT    = "SELECT ":FN.ACCT:" WITH (CATEGORY GE 1585 AND CATEGORY LE 1587) BY @ID"
    T.SEL.ACCT    = "SELECT ":FN.ACCT:" WITH (CATEGORY EQ 1423) BY @ID"
*    T.SEL.ACCT    = "SELECT ":FN.ACCT:" WITH @ID EQ 0130188210158502"
    CALL EB.READLIST(T.SEL.ACCT,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            NET.AMT  = 0
            ACC.NO   = KEY.LIST<HH>
            CALL F.READ(FN.ACCT,KEY.LIST<HH>,R.ACCT,F.ACCT,E1)
            CUS.ID      = R.ACCT<AC.CUSTOMER>
            CATEGORY    = R.ACCT<AC.CATEGORY>
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
            CUS.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

*Line [ 119 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.AC.DR.CNT   = DCOUNT(R.ACCT<AC.ACCT.DEBIT.INT>,@VM)
            WS.AD.DATE     = R.ACCT<AC.ACCT.DEBIT.INT><1,WS.AC.DR.CNT>
            WS.ADI.ID      = KEY.LIST<HH>:'-':WS.AD.DATE

**** CHECK ACCOUNT INTEREST
            CALL F.READ(FN.ADI,WS.ADI.ID,R.ADI,F.ADI,ERR2)
            INT.RATE = R.ADI<IC.ADI.DR.INT.RATE,1>
            INT.RATE.NEXT = R.ADI<IC.ADI.DR.INT.RATE,1>
**** SELECT RATE FROM GDI ****
            IF NOT(INT.RATE) THEN
                GROUP.ID  = R.ACCT<AC.CONDITION.GROUP>
                GD.ID   = GROUP.ID:R.ACCT<AC.CURRENCY>
                CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
                GD.DATE = R.GD<AC.GRD.DEBIT.GROUP.DATE>
                IF GD.DATE EQ '' THEN
                    GD.DATE = R.GD<AC.GRD.DEBIT.DATES,1>
                END
                GDI.ID  = GD.ID:GD.DATE

                CALL F.READ(FN.GDI,GDI.ID,R.GDI,F.GDI,E2)
*Line [ 140 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.RATE.COUNT   = DCOUNT(R.GDI<IC.GDI.DR.INT.RATE>,@VM)

*                FOR KK = 1 TO WS.RATE.COUNT
*                    WS.AMT.POS   = R.GDI<IC.GDI.DR.LIMIT.AMT><1,KK>
*                    IF KK = 1 THEN
*                        WS.AMT.POS.1 = 0
*                    END ELSE
*                        WS.AMT.POS.1 = R.GDI<IC.GDI.DR.LIMIT.AMT><1,KK-1>
*                    END
*                    IF WS.AMT GT WS.AMT.POS.1 AND WS.AMT LE WS.AMT.POS THEN
*                        WS.RATE = R.GDI<IC.GDI.DR.INT.RATE><1,KK>
*                    END
*                    WS.LAST.AMT = R.GDI<IC.GDI.DR.LIMIT.AMT><1,WS.RATE.COUNT-1>
*                    IF WS.AMT GT WS.LAST.AMT THEN
                INT.RATE = R.GDI<IC.GDI.DR.INT.RATE><1,WS.RATE.COUNT>
                INT.RATE.NEXT = R.GDI<IC.GDI.DR.INT.RATE><1,WS.RATE.COUNT>
*                    END
*                NEXT KK


            END
*************************************************************************
            CHK.WRT.OLD = '' ; CHK.WRT.NEW = '' ; DAYS = 0
            GOSUB GETREC

            LD.DET.ARR = ''

        NEXT HH
    END
    RETURN
*============================================================
GETREC:
*------

*-----------------------------------------------------------------------------------------------
    FOR II = 1 TO MON.NO.DAYS
        ACC.ID = '' ; R.ACCT.REC = '' ; BALANCE.DATE = '' ; YBALANCE = '' ; YBALANCE.NEXT = ''
        ACC.ID = ACC.NO
        CHECK.DATE = START.DAT[1,6]:FMT(II,"R%2")
        CHECK.DATE.NEXT = START.DAT[1,6]:FMT(II+1,"R%2")
        BALANCE.DATE = CHECK.DATE
        BALANCE.DATE.NEXT = CHECK.DATE.NEXT
        RAT = "64EGP":CHECK.DATE
        RAT.NEXT = "64EGP":CHECK.DATE.NEXT
        CALL EB.GET.INTEREST.RATE(RAT,SCB.INT.RATE)
        CALL EB.GET.INTEREST.RATE(RAT.NEXT,SCB.INT.RATE.NEXT)
        SCB.DIF.RAT = SCB.INT.RATE + 2 - INT.RATE
        SCB.DIF.RAT.NEXT = SCB.INT.RATE.NEXT + 2 - INT.RATE.NEXT

        CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT.REC, "VALUE", BALANCE.DATE, "", YBALANCE, CR.MVMT, DR.MVMT, ERR)
        CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT.REC, "VALUE", BALANCE.DATE.NEXT, "", YBALANCE.NEXT, CR.MVMT, DR.MVMT, ERR)
        IF YBALANCE GE 0 THEN
            DAYS = 0
            YBALANCE = 0
        END ELSE
            Y.AMT = YBALANCE
            BI.DATE = CHECK.DATE

*-----------------------------------------------------------------------------------------------
            DAYS++
        END
        PRINT CHECK.DATE : "===== " :DAYS
        CHK.WRT.NEW = YBALANCE.NEXT:SCB.DIF.RAT.NEXT
        CHK.WRT.OLD = YBALANCE:SCB.DIF.RAT
        IF (CHK.WRT.NEW # CHK.WRT.OLD AND YBALANCE NE 0 ) OR ( II = MON.NO.DAYS AND YBALANCE NE 0 ) THEN
            TOTAL.INT = YBALANCE * SCB.DIF.RAT/100 * DAYS/360
            TOTAL.INT = DROUND(TOTAL.INT,2)
*-----------------------------------------------------------------------------------------------
            Y.RET.DATA<-1> = KEY.LIST<HH>:'*':CUS.ID:'*':CUS.NAME:'*':Y.AMT:'*':TOTAL.INT:'*':INT.RATE:'*':DAYS:'*':BI.DATE:'*':SCB.DIF.RAT
*-----------------------------------------------------------------------------------------------
            Y.AMT = 0 ; DAYS = 0
        END
    NEXT II
    RETURN
END
