* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBR.CUS.DATA.REPORT.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    TEXT = 'Program Complete' ; CALL REM

    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "CLIENT.DATA.REPORT.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CLIENT.DATA.REPORT.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CLIENT.DATA.REPORT.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CLIENT.DATA.REPORT.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create CLIENT.DATA.REPORT.CSV File IN &SAVEDLISTS&'
        END
    END


    HEAD.DESC  = "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "NATIONAL.ID":","
    HEAD.DESC := "TELEPHONE":","
    HEAD.DESC := "ADDRESS":","
    HEAD.DESC := "EMAIL":","
    HEAD.DESC := "OPENING.DATE":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    YTEXT = "Enter.Opening.Date Or Null.For.All.Data : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    WS.DAT = COMI

    RETURN

*========================================================================
PROCESS:


    IF WS.DAT EQ '' THEN
        T.SEL = "SELECT ":FN.CU:" WITH SECTOR EQ 2000 AND POSTING.RESTRICT LT 80 BY @ID"
    END ELSE
        T.SEL = "SELECT ":FN.CU:" WITH CONTACT.DATE EQ ":WS.DAT:" AND SECTOR EQ 2000 AND POSTING.RESTRICT LT 80 BY @ID"
    END

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.CUS.ID  = KEY.LIST<I>
            WS.CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            WS.NAT.ID    = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            WS.TEL       = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
            WS.ADDRESS   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
            WS.EMAIL     = R.CU<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
            WS.OPN.DATE  = R.CU<EB.CUS.CONTACT.DATE>

            BB.DATA  = WS.CUS.ID:","
            BB.DATA := WS.CUST.NAME:","
            BB.DATA := "'":WS.NAT.ID:"'":","
            BB.DATA := WS.TEL:","
            BB.DATA := WS.ADDRESS:","
            BB.DATA := WS.EMAIL:","
            BB.DATA := WS.OPN.DATE:","

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END
    RETURN
*==============================================================
