* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    PROGRAM SBM.CBE.6002.EUR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL

*-------------------------------------------------------------------------
    GOSUB PRINT.HEAD
    GOSUB OPEN.FILES

    GOSUB SEL.1
    GOSUB SEL.2
    GOSUB SEL.3
    GOSUB SEL.4
    GOSUB SEL.5
    GOSUB SEL.6
    GOSUB SEL.7
    GOSUB SEL.8
    GOSUB SEL.9
    GOSUB SEL.10
    GOSUB SEL.11
    GOSUB SEL.12
    GOSUB SEL.13

    GOSUB PRINT.TOTAL

    RETURN
*==============================================================
PRINT.HEAD:
*==========
    OPENSEQ "&SAVEDLISTS&" , "DEPOSITS.CBE.6002.EUR.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DEPOSITS.CBE.6002.EUR.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DEPOSITS.CBE.6002.EUR.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DEPOSITS.CBE.6002.EUR.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create DEPOSITS.CBE.6002.EUR.CSV File IN &SAVEDLISTS&'
        END
    END

    TXT.LINE1  = '����� ��� �����'
    TXT.LINE2  = '����� ���� ��� ����'
    TXT.LINE3  = '����� ��� ��� ���� �� ��� ��� ����� ����'
    TXT.LINE4  = '����� ��� ��� ���� �� ����� ���� ��� ��� ����'
    TXT.LINE5  = '����� ��� ��� ���� �� ��� ���� ��� ���'
    TXT.LINE6  = '����� ��� ��� ���� �� ��� ���� ��� �����'
    TXT.LINE7  = '����� ��� ��� ���� �� �����'
    TXT.LINE8  = '����� ������� ��� ��� ���� �����'
    TXT.LINE9  = '����� ������� ��� ��� �� ���� ����� ��� ��� �����'
    TXT.LINE10 = '����� ������� ��� ��� �� ��� ����� ��� ��� �����'
    TXT.LINE11 = '����� ������� ��� ��� �� ��� ����� ��� ���� �����'
    TXT.LINE12 = '����� �����'
    TXT.LINE13 = '����� ����'

    TXT.AMT    = '����'
    TXT.RATE   = '��� ������ ������'
    TXT.HIRATE = '���� ��� ����'
    TXT.LORATE = '���� ��� ����'

    TXT.PERS = '����� �������'
    TXT.CORP = '����� ��������'

    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')

    TD = DAT
    DAT.HED = FMT(TD,"####-##-##")

    HEAD1 = "������� - ��� ���� ������ - �������"
    HEAD.DESC = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "����":","
    HEAD.DESC := "������ ������ �� ����� ����� ������":",,,,"
    HEAD.DESC := "������� ������� ���� �����":",,,,,,,,"
    HEAD.DESC := "��������� �� ������� ���� �����":",,"
    HEAD.DESC := "������ ������ �� ����� �����":",,,,"
    HEAD.DESC := "��� �����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = ",":TXT.PERS:",,"
    HEAD.DESC := TXT.CORP:",,"
    HEAD.DESC := TXT.PERS:",,,,"
    HEAD.DESC := TXT.CORP:",,,,"
    HEAD.DESC := TXT.PERS:","
    HEAD.DESC := TXT.CORP:","
    HEAD.DESC := TXT.PERS:",,"
    HEAD.DESC := TXT.CORP:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = ",":TXT.AMT:","
    HEAD.DESC := TXT.RATE:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.RATE:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.RATE:","
    HEAD.DESC := TXT.LORATE:","
    HEAD.DESC := TXT.HIRATE:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.RATE:","
    HEAD.DESC := TXT.LORATE:","
    HEAD.DESC := TXT.HIRATE:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.RATE:","
    HEAD.DESC := TXT.AMT:","
    HEAD.DESC := TXT.RATE:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*==============================================================
OPEN.FILES:
*==========
    FN.CBE = 'F.CBE.STATIC.AC.LD.DETAIL' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.ACTV = 'FBNK.ACCT.ACTIVITY' ; F.ACTV = ''
    CALL OPF(FN.ACTV,F.ACTV)

    FN.ACCLS = 'FBNK.ACCOUNT.CLOSED' ; F.ACCLS = ''
    CALL OPF(FN.ACCLS,F.ACCLS)

    FN.LMM.H = 'FBNK.LMM.ACCOUNT.BALANCES.HIST' ; F.LMM.H = ''
    CALL OPF(FN.LMM.H,F.LMM.H)

    FN.AC.H = 'FBNK.ACCOUNT$HIS' ; F.AC.H = ''
    CALL OPF(FN.AC.H,F.AC.H)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = ''
    CALL OPF(FN.GCI,F.GCI)

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)


    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
    CCY.TXT  = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 236 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CCY.POS  = DCOUNT(CCY.TXT,@VM)
    CCY.RATE = R.CCY<RE.BCP.RATE,3>

    WS.DAT       = DAT[1,6]
    WS.DAT.PREV1 = WS.DAT:'01'

    CALL CDT("",WS.DAT.PREV1,'-1C')

    WS.DAT.PREV = WS.DAT.PREV1[1,6]

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""

    CATEG.LINE1  = '(1001 1002 1003 1005 1006 1011 1059 1102 1201 1202 1205 1206 1207 1208 1211 1212 1214 1215 1216 1217'
    CATEG.LINE1 := ' 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1407 1408 1413 1414 1415 1416 1417 1418'
    CATEG.LINE1 := ' 1419 1420 1421 1445 1455 1477 1480 1481 1483 1484 1485 1493 1499 1501 1502 1503 1504 1507 1508'
    CATEG.LINE1 := ' 1509 1510 1511 1512 1513 1514 1516 1518 1519 1523 1524 1525 1534 1544 1558 1559 1560 1566 1577'
    CATEG.LINE1 := ' 1579 1582 1588 1595 1596 1597 1599 11231 11232 11239 11240 11242 2006 1007 1008 1009)'

    CATEG.LINE2  = '(21001 21002 21003 21015)'
    CATEG.LINE3  = '(21004 21005 6512)'
    CATEG.LINE4  = '(21006)'
    CATEG.LINE5  = '(21007)'
    CATEG.LINE6  = '(21008)'
    CATEG.LINE7  = '(21009 21010 21013)'
    CATEG.LINE8  = '(21020 21026 21021 21022 21027 21032 21017 21028 21041 21019 21018 21036 21025)'
    CATEG.LINE9  = '(21042 21023 21024 21029)'
    CATEG.LINE10 = ''
    CATEG.LINE11 = ''
    CATEG.LINE12 = '(6501 6502 6503 6504 6511 6505 6506 6507 6508 6513 6514 6515 6516 6517)'
    CATEG.LINE13 = '(1012 1013 1014 1015 1016 1019 3005 3010 3011 3012 3013 3014 3017 3050 3206 3208 16113 16170 3060 3065)'

    WS.RATE     = 0 ; WS.SPREAD   = 0 ; WS.LD.RATE = 0 ; WS.LD.SPREAD = 0 ; WS.OPER = '' ; LINE.FLAG = '' ; SEL.CATEG = 0
    STE.AMT.FCY = 0 ; STE.AMT.LCY = 0

    WS.AMT.PREV.PERS   = 0 ; WS.AMT.PREV.CORP   = 0 ; WS.RATE.PREV.PERS  = 0  ; WS.RATE.PREV.CORP  = 0
    WS.AMT.END.PERS    = 0 ; WS.AMT.END.CORP    = 0 ; WS.RATE.END.PERS   = 0  ; WS.RATE.END.CORP   = 0
    WS.AMT.CUR.PERS    = 0 ; WS.AMT.CUR.CORP    = 0 ; WS.RATE.CUR.PERS   = 0  ; WS.RATE.CUR.CORP   = 0
    WS.HIRATE.CUR.PERS = 0 ; WS.HIRATE.CUR.CORP = 0 ; WS.LORATE.CUR.PERS = 99 ; WS.LORATE.CUR.CORP = 99
    WS.AMT.DRW.PERS    = 0 ; WS.AMT.DRW.CORP    = 0

    WS.AMT.PREV.PERS.TOTAL = 0 ; WS.AMT.PREV.CORP.TOTAL = 0 ; WS.RATE.PREV.PERS.TOTAL = 0 ; WS.RATE.PREV.CORP.TOTAL = 0
    WS.AMT.CUR.PERS.TOTAL  = 0 ; WS.AMT.CUR.CORP.TOTAL  = 0 ; WS.RATE.CUR.PERS.TOTAL  = 0 ; WS.RATE.CUR.CORP.TOTAL  = 0
    WS.AMT.END.PERS.TOTAL  = 0 ; WS.AMT.END.CORP.TOTAL  = 0 ; WS.RATE.END.PERS.TOTAL  = 0 ; WS.RATE.END.CORP.TOTAL  = 0
    WS.AMT.DRW.PERS.TOTAL  = 0 ; WS.AMT.DRW.CORP.TOTAL  = 0


    WS.AMT.PREV.PERS.TOTAL.ALL  = 0 ; WS.AMT.PREV.CORP.TOTAL.ALL = 0 ; WS.RATE.PREV.PERS.TOTAL.ALL = 0
    WS.AMT.CUR.PERS.TOTAL.ALL   = 0 ; WS.AMT.CUR.CORP.TOTAL.ALL  = 0 ; WS.RATE.CUR.PERS.TOTAL.ALL  = 0
    WS.AMT.END.PERS.TOTAL.ALL   = 0 ; WS.AMT.END.CORP.TOTAL.ALL  = 0 ; WS.RATE.END.PERS.TOTAL.ALL  = 0
    WS.RATE.PREV.CORP.TOTAL.ALL = 0 ; WS.RATE.CUR.CORP.TOTAL.ALL = 0 ; WS.RATE.END.CORP.TOTAL.ALL  = 0
    WS.AMT.DRW.PERS.TOTAL.ALL   = 0 ; WS.AMT.DRW.CORP.TOTAL.ALL  = 0

    WS.AMT.CUR.PERS.TOTALL = 0 ; WS.AMT.CUR.CORP.TOTALL  = 0

    WS.AMT.DEBIT.LCY = 0 ; WS.AMT.DEBIT.FCY = 0 ; WS.AMT.CREDIT.LCY = 0 ; WS.AMT.CREDIT.FCY = 0

    WS.END.MONTH = TODAY[1,6]:'01'
    CALL CDT("",WS.END.MONTH,'-1C')

    WS.START.MONTH = WS.END.MONTH[1,6]:'01'

    KK = 1

    RETURN
*==============================================================
CLEAR.DETAIL:
*============

    WS.RATE = 0 ; WS.SPREAD = 0 ; WS.LD.RATE = 0 ; WS.LD.SPREAD = 0 ; WS.OPER = '' ; LINE.FLAG = '' ; SEL.CATEG = 0

    WS.RATE.PERS.OLD = 0 ; WS.RATE.PERS.NEW = 0 ; WS.RATE.CORP.OLD = 0 ; WS.RATE.CORP.NEW = 0

    WS.AMT.PREV.PERS   = 0 ; WS.AMT.PREV.CORP   = 0 ; WS.RATE.PREV.PERS  = 0  ; WS.RATE.PREV.CORP  = 0
    WS.AMT.END.PERS    = 0 ; WS.AMT.END.CORP    = 0 ; WS.RATE.END.PERS   = 0  ; WS.RATE.END.CORP   = 0
    WS.AMT.CUR.PERS    = 0 ; WS.AMT.CUR.CORP    = 0 ; WS.RATE.CUR.PERS   = 0  ; WS.RATE.CUR.CORP   = 0
    WS.HIRATE.CUR.PERS = 0 ; WS.HIRATE.CUR.CORP = 0 ; WS.LORATE.CUR.PERS = 99 ; WS.LORATE.CUR.CORP = 99
    WS.AMT.DRW.PERS    = 0 ; WS.AMT.DRW.CORP    = 0 ; WS.INT.CUR.PERS    = 0  ; WS.INT.CUR.CORP    = 0

    WS.AMT.PREV.PERS.TOTAL = 0 ; WS.AMT.PREV.CORP.TOTAL = 0 ; WS.RATE.PREV.PERS.TOTAL = 0 ; WS.RATE.PREV.CORP.TOTAL = 0
    WS.AMT.CUR.PERS.TOTAL  = 0 ; WS.AMT.CUR.CORP.TOTAL  = 0 ; WS.RATE.CUR.PERS.TOTAL  = 0 ; WS.RATE.CUR.CORP.TOTAL  = 0
    WS.AMT.END.PERS.TOTAL  = 0 ; WS.AMT.END.CORP.TOTAL  = 0 ; WS.RATE.END.PERS.TOTAL  = 0 ; WS.RATE.END.CORP.TOTAL  = 0
    WS.AMT.DRW.PERS.TOTAL  = 0 ; WS.AMT.DRW.CORP.TOTAL  = 0

    WS.AMT.DEBIT.LCY = 0 ; WS.AMT.DEBIT.FCY = 0 ; WS.AMT.CREDIT.LCY = 0 ; WS.AMT.CREDIT.FCY = 0

    RETURN
*==============================================================
SEL.1:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE1
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'AC'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE1
    SEL.CATEG = CATEG.LINE1
    GOSUB SEL.AC
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.2:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE2
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE2
    SEL.CATEG = CATEG.LINE2
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.3:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE3
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            WS.CATEG  = R.CBE<STD.CBEM.CATEG>

            IF WS.CATEG EQ '6512' THEN
                LINE.FLAG = 'AC'
            END ELSE
                LINE.FLAG = 'LD'
            END

            SEL.CATEG = CATEG.LINE3

            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE3
    SEL.CATEG = '(21004 21005)'
    GOSUB SEL.LD

    SEL.CATEG = '(6512)'
    GOSUB SEL.AC

    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.4:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE4
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE4
    SEL.CATEG = CATEG.LINE4
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.5:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE5
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE5
    SEL.CATEG = CATEG.LINE5
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.6:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE6
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE6
    SEL.CATEG = CATEG.LINE6
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.7:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE7
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE7
    SEL.CATEG = CATEG.LINE7
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.8:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE8
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE8
    SEL.CATEG = CATEG.LINE8
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.9:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE9
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE9
    SEL.CATEG = CATEG.LINE9
    GOSUB SEL.LD
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.10:
*======
    LINE.DESC = TXT.LINE10
    GOSUB PRINT.DETAIL

    RETURN
*-------------------------------------------------------
SEL.11:
*======
    LINE.DESC = TXT.LINE11
    GOSUB PRINT.DETAIL

    RETURN
*-------------------------------------------------------
SEL.12:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE12
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'AC'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE12
    SEL.CATEG = CATEG.LINE12
    GOSUB SEL.AC
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*-------------------------------------------------------
SEL.13:
*=====
    T.SEL  = "SELECT ":FN.CBE:" WITH ( CBEM.DATE EQ ":WS.DAT:" OR CBEM.DATE EQ ":WS.DAT.PREV:" ) AND CBEM.CY EQ 'EUR' AND CBEM.CATEG IN ":CATEG.LINE13
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LINE.FLAG = 'AC'
            GOSUB GET.DETAIL
        NEXT I
    END
    LINE.DESC = TXT.LINE13
    SEL.CATEG = CATEG.LINE13
    GOSUB SEL.AC
    GOSUB PRINT.DETAIL
    GOSUB CLEAR.DETAIL

    RETURN
*==============================================================
SEL.LD:
*======
    T.SEL  = "SELECT ":FN.LD:" WITH VALUE.DATE GE ":WS.START.MONTH:" AND VALUE.DATE LE ":WS.END.MONTH:" AND CURRENCY EQ 'EUR' AND CATEGORY IN ":SEL.CATEG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            WS.STATUS = R.LD<LD.STATUS>
            IF WS.STATUS NE 'LIQ' THEN
                CUS.ID       = R.LD<LD.CUSTOMER.ID>
                WS.CATEG     = R.LD<LD.CATEGORY>
                WS.DATE      = R.LD<LD.VALUE.DATE>
                WS.CCY       = R.LD<LD.CURRENCY>
                WS.AMOUNT    = R.LD<LD.AMOUNT>
                WS.LD.SPREAD = R.LD<LD.INTEREST.SPREAD>
                WS.LD.RATE   = R.LD<LD.INTEREST.RATE>
                WS.LD.KEY    = R.LD<LD.INTEREST.KEY>
                WS.RATE      = WS.LD.RATE + WS.LD.SPREAD
            END ELSE
                CALL F.READ.HISTORY(FN.LD.H,KEY.LIST<I>,R.LD.H,F.LD.H,E1.H)
                CUS.ID       = R.LD.H<LD.CUSTOMER.ID>
                WS.CATEG     = R.LD.H<LD.CATEGORY>
                WS.DATE      = R.LD.H<LD.VALUE.DATE>
                WS.CCY       = R.LD.H<LD.CURRENCY>
                WS.AMOUNT    = R.LD.H<LD.REIMBURSE.AMOUNT>
                WS.LD.SPREAD = R.LD.H<LD.INTEREST.SPREAD>
                WS.LD.RATE   = R.LD.H<LD.INTEREST.RATE>
                WS.LD.KEY    = R.LD.H<LD.INTEREST.KEY>
                WS.RATE      = WS.LD.RATE + WS.LD.SPREAD
            END
            IF WS.LD.RATE EQ '' THEN
                BI.ID  = WS.LD.KEY:WS.CCY:'...'
                T.SEL2 = "SELECT ":FN.BI:" WITH @ID LIKE ":BI.ID:" BY @ID"
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
                IF SELECTED2 THEN
                    CALL F.READ(FN.BI,KEY.LIST2<SELECTED2>,R.BI,F.BI,E3)
                    WS.RATE = WS.LD.SPREAD + R.BI<EB.BIN.INTEREST.RATE>
                END
            END

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            WS.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF WS.SECTOR EQ '4650' THEN
                WS.AMT.CUR.PERS           += WS.AMOUNT
                WS.AMT.CUR.PERS.TOTAL     += WS.AMOUNT
                WS.AMT.CUR.PERS.TOTAL.ALL += WS.AMOUNT
                WS.RATE.CUR.PERS          += WS.AMOUNT * WS.RATE

                WS.AMT.CUR.PERS.TOTALL += WS.AMOUNT

                IF WS.RATE GT WS.HIRATE.CUR.PERS THEN
                    WS.HIRATE.CUR.PERS = WS.RATE
                END

                IF WS.RATE LT WS.LORATE.CUR.PERS THEN
                    WS.LORATE.CUR.PERS = WS.RATE
                END

            END ELSE
                WS.AMT.CUR.CORP           += WS.AMOUNT
                WS.AMT.CUR.CORP.TOTAL     += WS.AMOUNT
                WS.AMT.CUR.CORP.TOTAL.ALL += WS.AMOUNT
                WS.RATE.CUR.CORP          += WS.AMOUNT * WS.RATE

                WS.AMT.CUR.CORP.TOTALL += WS.AMOUNT

                IF WS.RATE GT WS.HIRATE.CUR.CORP THEN
                    WS.HIRATE.CUR.CORP = WS.RATE
                END

                IF WS.RATE LT WS.LORATE.CUR.CORP THEN
                    WS.LORATE.CUR.CORP = WS.RATE
                END

            END
        NEXT I
    END
*----------- LD HISTORY FOR NEW LDs --------------------
    LMM.ID = 'LD...'
    T.SEL  = "SELECT ":FN.LMM.H:" WITH @ID LIKE ":LMM.ID:" AND START.PERIOD.INT GE ":WS.START.MONTH:" AND START.PERIOD.INT LE ":WS.END.MONTH
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LD.HIS.ID = KEY.LIST<I>[1,12]:';1'

            CALL F.READ(FN.LD.H,LD.HIS.ID,R.LD.H,F.LD.H,E2.H)
            WS.CATEG  = R.LD.H<LD.CATEGORY>
            FINDSTR WS.CATEG IN SEL.CATEG SETTING POS THEN
                WS.CCY       = R.LD.H<LD.CURRENCY>
                IF WS.CCY EQ 'EUR' THEN
                    CUS.ID       = R.LD.H<LD.CUSTOMER.ID>
                    WS.DATE      = R.LD.H<LD.VALUE.DATE>
                    WS.AMOUNT    = R.LD.H<LD.AMOUNT>
                    WS.LD.SPREAD = R.LD.H<LD.INTEREST.SPREAD>
                    WS.LD.RATE   = R.LD.H<LD.INTEREST.RATE>
                    WS.LD.KEY    = R.LD.H<LD.INTEREST.KEY>
                    WS.RATE      = WS.LD.RATE + WS.LD.SPREAD

                    IF WS.LD.RATE EQ '' THEN
                        BI.ID  = WS.LD.KEY:WS.CCY:'...'
                        T.SEL2 = "SELECT ":FN.BI:" WITH @ID LIKE ":BI.ID:" BY @ID"
                        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
                        IF SELECTED2 THEN
                            CALL F.READ(FN.BI,KEY.LIST2<SELECTED2>,R.BI,F.BI,E3)
                            WS.RATE = WS.LD.SPREAD + R.BI<EB.BIN.INTEREST.RATE>
                        END
                    END

                    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
                    WS.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

                    IF WS.SECTOR EQ '4650' THEN
                        WS.AMT.CUR.PERS           += WS.AMOUNT
                        WS.AMT.CUR.PERS.TOTAL     += WS.AMOUNT
                        WS.AMT.CUR.PERS.TOTAL.ALL += WS.AMOUNT
                        WS.RATE.CUR.PERS          += WS.AMOUNT * WS.RATE

                        WS.AMT.CUR.PERS.TOTALL += WS.AMOUNT

                        IF WS.RATE GT WS.HIRATE.CUR.PERS THEN
                            WS.HIRATE.CUR.PERS = WS.RATE
                        END

                        IF WS.RATE LT WS.LORATE.CUR.PERS THEN
                            WS.LORATE.CUR.PERS = WS.RATE
                        END

                    END ELSE
                        WS.AMT.CUR.CORP           += WS.AMOUNT
                        WS.AMT.CUR.CORP.TOTAL     += WS.AMOUNT
                        WS.AMT.CUR.CORP.TOTAL.ALL += WS.AMOUNT
                        WS.RATE.CUR.CORP          += WS.AMOUNT * WS.RATE

                        WS.AMT.CUR.CORP.TOTALL += WS.AMOUNT

                        IF WS.RATE GT WS.HIRATE.CUR.CORP THEN
                            WS.HIRATE.CUR.CORP = WS.RATE
                        END

                        IF WS.RATE LT WS.LORATE.CUR.CORP THEN
                            WS.LORATE.CUR.CORP = WS.RATE
                        END
                    END
                END
            END
        NEXT I
    END
    RETURN
*==============================================================
SEL.AC:
*======
    T.SEL  = "SELECT ":FN.AC:" WITH OPENING.DATE GE ":WS.START.MONTH:" AND OPENING.DATE LE ":WS.END.MONTH:" AND CURRENCY EQ 'EUR' AND OPEN.ACTUAL.BAL GT 0 AND CATEGORY IN ":SEL.CATEG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CUS.ID = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

            WS.SECTOR   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            WS.CATEG    = R.AC<AC.CATEGORY>
            WS.DATE     = R.AC<AC.OPENING.DATE>
            WS.CCY      = R.AC<AC.CURRENCY>
            GROUP.ID    = R.AC<AC.CONDITION.GROUP>
*Line [ 718 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.AC.COUNT = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
            AD.DATE     = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>

            ACI.ID = KEY.LIST<I>:'-':AD.DATE

            CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 725 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
            WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>
            IF WS.RATE EQ '' THEN

                GD.ID   = GROUP.ID:WS.CCY
                CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
                GD.DATE = R.GD<AC.GRD.CREDIT.GROUP.DATE>
                GCI.ID  = GD.ID:GD.DATE

                CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
                WS.RATE   = R.GCI<IC.GCI.CR.INT.RATE>
                WS.OPER   = R.GCI<IC.GCI.CR.MARGIN.OPER>
                WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE>

                IF WS.OPER EQ 'ADD' THEN
                    WS.RATE = WS.RATE + WS.SPREAD
                END

                IF WS.OPER EQ 'SUBTRACT' THEN
                    WS.RATE = WS.RATE - WS.SPREAD
                END

            END

            IF WS.SECTOR EQ '4650' THEN
                WS.AMT.CUR.PERS           += R.AC<AC.OPEN.ACTUAL.BAL>
                WS.AMT.CUR.PERS.TOTAL     += R.AC<AC.OPEN.ACTUAL.BAL>
                WS.AMT.CUR.PERS.TOTAL.ALL += R.AC<AC.OPEN.ACTUAL.BAL>
                WS.RATE.CUR.PERS          += R.AC<AC.OPEN.ACTUAL.BAL> * WS.RATE

                WS.AMT.CUR.PERS.TOTALL += R.AC<AC.OPEN.ACTUAL.BAL>

                IF WS.RATE GT WS.HIRATE.CUR.PERS THEN
                    WS.HIRATE.CUR.PERS = WS.RATE
                END

                IF WS.RATE LT WS.LORATE.CUR.PERS THEN
                    WS.LORATE.CUR.PERS = WS.RATE
                END

            END ELSE
                WS.AMT.CUR.CORP           += R.AC<AC.OPEN.ACTUAL.BAL>
                WS.AMT.CUR.CORP.TOTAL     += R.AC<AC.OPEN.ACTUAL.BAL>
                WS.AMT.CUR.CORP.TOTAL.ALL += R.AC<AC.OPEN.ACTUAL.BAL>
                WS.RATE.CUR.CORP          += R.AC<AC.OPEN.ACTUAL.BAL> * WS.RATE

                WS.AMT.CUR.CORP.TOTALL += R.AC<AC.OPEN.ACTUAL.BAL>

                IF WS.RATE GT WS.HIRATE.CUR.CORP THEN
                    WS.HIRATE.CUR.CORP = WS.RATE
                END

                IF WS.RATE LT WS.LORATE.CUR.CORP THEN
                    WS.LORATE.CUR.CORP = WS.RATE
                END
            END
        NEXT I
    END

*---------- CREDIT & DEBIT MOVEMENTS -------------

    T.SEL  = "SELECT ":FN.AC:" WITH CURRENCY EQ 'EUR' AND OPEN.ACTUAL.BAL GT 0 AND CATEGORY IN ":SEL.CATEG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            ACTV.ID = KEY.LIST<I>:'-':WS.START.MONTH[1,6]

            CALL F.READ(FN.ACTV,ACTV.ID,R.ACTV,F.ACTV,ERR.ACTV)
            IF NOT(ERR.ACTV) THEN

                CR.MOV = R.ACTV<IC.ACT.TURNOVER.CREDIT>
*Line [ 798 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                CR = DCOUNT(CR.MOV,@VM)
                FOR J = 1 TO CR
                    WS.AMT.CREDIT.LCY += CR.MOV<1,J>
                NEXT J
            END

            CUS.ID = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

            WS.SECTOR   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            WS.CATEG    = R.AC<AC.CATEGORY>
            WS.DATE     = R.AC<AC.OPENING.DATE>
            WS.CCY      = R.AC<AC.CURRENCY>

            IF WS.SECTOR EQ '4650' THEN
                WS.AMT.CUR.PERS           += WS.AMT.CREDIT.LCY
                WS.AMT.CUR.PERS.TOTAL.ALL += WS.AMT.CREDIT.LCY

            END ELSE
                WS.AMT.CUR.CORP           += WS.AMT.CREDIT.LCY
                WS.AMT.CUR.CORP.TOTAL.ALL += WS.AMT.CREDIT.LCY

            END

            WS.AMT.DEBIT.LCY  = 0
            WS.AMT.CREDIT.LCY = 0

        NEXT I
    END
*-------------------------------------------------
    RETURN
*==============================================================
GET.DETAIL:
*==========
    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
    WS.SECTOR = R.CBE<STD.CBEM.NEW.SECTOR>
    WS.CATEG  = R.CBE<STD.CBEM.CATEG>
    WS.DATE   = R.CBE<STD.CBEM.DATE>
    WS.CCY    = R.CBE<STD.CBEM.CY>
    WS.AMT.1  = R.CBE<STD.CBEM.IN.FCY>
    WS.AMT.2  = R.CBE<STD.CBEM.IN.FCYO>

    IF LINE.FLAG EQ 'LD' THEN
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,ER.LD)

        IF NOT(ER.LD) THEN
            WS.LD.SPREAD = R.LD<LD.INTEREST.SPREAD>
            WS.LD.RATE   = R.LD<LD.INTEREST.RATE>
            WS.LD.KEY    = R.LD<LD.INTEREST.KEY>
            WS.RATE      = WS.LD.RATE + WS.LD.SPREAD
        END ELSE
            LD.HIS.ID = KEY.LIST<I>:';1'
            CALL F.READ(FN.LD.H,LD.HIS.ID,R.LD.H,F.LD.H,E2.H)

            WS.LD.SPREAD = R.LD.H<LD.INTEREST.SPREAD>
            WS.LD.RATE   = R.LD.H<LD.INTEREST.RATE>
            WS.LD.KEY    = R.LD.H<LD.INTEREST.KEY>
            WS.RATE      = WS.LD.RATE + WS.LD.SPREAD
        END

        IF WS.LD.RATE EQ '' THEN
            BI.ID     = WS.LD.KEY:WS.CCY:'...'
            T.SEL2 = "SELECT ":FN.BI:" WITH @ID LIKE ":BI.ID:" BY @ID"
            CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
            IF SELECTED2 THEN
                CALL F.READ(FN.BI,KEY.LIST2<SELECTED2>,R.BI,F.BI,E3)
                WS.RATE = WS.LD.SPREAD + R.BI<EB.BIN.INTEREST.RATE>
            END
        END
    END

    IF LINE.FLAG EQ 'AC' THEN
        CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,ER.AC)
        IF NOT(ER.AC) THEN
            GROUP.ID    = R.AC<AC.CONDITION.GROUP>
*Line [ 874 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.AC.COUNT = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
            AD.DATE     = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
        END ELSE
            CALL F.READ.HISTORY(FN.AC.H,KEY.LIST<I>,R.AC.H,F.AC.H,ER.AC.H)
            GROUP.ID    = R.AC.H<AC.CONDITION.GROUP>
*Line [ 880 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.AC.COUNT = DCOUNT(R.AC.H<AC.ACCT.CREDIT.INT>,@VM)
            AD.DATE     = R.AC.H<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
        END

        ACI.ID = KEY.LIST<I>:'-':AD.DATE

        CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 888 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
        WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>
        IF WS.RATE EQ '' THEN

            GD.ID   = GROUP.ID:WS.CCY
            CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
            GD.DATE = R.GD<AC.GRD.CREDIT.GROUP.DATE>
            GCI.ID  = GD.ID:GD.DATE

            CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
            WS.RATE   = R.GCI<IC.GCI.CR.INT.RATE>
            WS.OPER   = R.GCI<IC.GCI.CR.MARGIN.OPER>
            WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE>

            IF WS.OPER EQ 'ADD' THEN
                WS.RATE = WS.RATE + WS.SPREAD
            END

            IF WS.OPER EQ 'SUBTRACT' THEN
                WS.RATE = WS.RATE - WS.SPREAD
            END

        END
    END

    IF WS.SECTOR EQ '4650' THEN
        IF WS.DATE EQ WS.DAT.PREV THEN
            IF WS.AMT.1 GT 0 THEN
                WS.AMT.PREV.PERS           += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.PREV.PERS.TOTAL     += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.PREV.PERS.TOTAL.ALL += R.CBE<STD.CBEM.IN.FCY>
                WS.RATE.PREV.PERS          += R.CBE<STD.CBEM.IN.FCY> * WS.RATE
            END
        END ELSE
            IF WS.AMT.2 GT 0 THEN
                WS.AMT.PREV.PERS           += R.CBE<STD.CBEM.IN.FCYO>
                WS.AMT.PREV.PERS.TOTAL     += R.CBE<STD.CBEM.IN.FCYO>
                WS.AMT.PREV.PERS.TOTAL.ALL += R.CBE<STD.CBEM.IN.FCYO>
                WS.RATE.PREV.PERS          += R.CBE<STD.CBEM.IN.FCYO> * WS.RATE
            END
            IF WS.AMT.1 GT 0 THEN
                WS.AMT.END.PERS            += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.END.PERS.TOTAL      += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.END.PERS.TOTAL.ALL  += R.CBE<STD.CBEM.IN.FCY>
                WS.RATE.END.PERS           += R.CBE<STD.CBEM.IN.FCY> * WS.RATE
            END
        END

    END ELSE
        IF WS.DATE EQ WS.DAT.PREV THEN
            IF WS.AMT.1 GT 0 THEN
                WS.AMT.PREV.CORP           += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.PREV.CORP.TOTAL     += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.PREV.CORP.TOTAL.ALL += R.CBE<STD.CBEM.IN.FCY>
                WS.RATE.PREV.CORP          += R.CBE<STD.CBEM.IN.FCY> * WS.RATE
            END
        END ELSE
            IF WS.AMT.2 GT 0 THEN
                WS.AMT.PREV.CORP           += R.CBE<STD.CBEM.IN.FCYO>
                WS.AMT.PREV.CORP.TOTAL     += R.CBE<STD.CBEM.IN.FCYO>
                WS.AMT.PREV.CORP.TOTAL.ALL += R.CBE<STD.CBEM.IN.FCYO>
                WS.RATE.PREV.CORP          += R.CBE<STD.CBEM.IN.FCYO> * WS.RATE
            END
            IF WS.AMT.1 GT 0 THEN
                WS.AMT.END.CORP            += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.END.CORP.TOTAL      += R.CBE<STD.CBEM.IN.FCY>
                WS.AMT.END.CORP.TOTAL.ALL  += R.CBE<STD.CBEM.IN.FCY>
                WS.RATE.END.CORP           += R.CBE<STD.CBEM.IN.FCY> * WS.RATE
            END
        END
    END

    RETURN

*==============================================================
PRINT.DETAIL:
*==========
    IF WS.AMT.PREV.PERS.TOTAL NE 0 THEN
        WS.RATE.PREV.PERS = WS.RATE.PREV.PERS / WS.AMT.PREV.PERS.TOTAL
    END

    IF WS.AMT.END.PERS.TOTAL NE 0 THEN
        WS.RATE.END.PERS  = WS.RATE.END.PERS  / WS.AMT.END.PERS.TOTAL
    END

    IF WS.AMT.PREV.CORP.TOTAL NE 0 THEN
        WS.RATE.PREV.CORP = WS.RATE.PREV.CORP / WS.AMT.PREV.CORP.TOTAL
    END

    IF WS.AMT.END.CORP.TOTAL NE 0 THEN
        WS.RATE.END.CORP  = WS.RATE.END.CORP  / WS.AMT.END.CORP.TOTAL
    END

    IF WS.AMT.CUR.PERS.TOTAL NE 0 THEN
        WS.RATE.CUR.PERS  = WS.RATE.CUR.PERS  / WS.AMT.CUR.PERS.TOTAL
    END

    IF WS.AMT.CUR.CORP.TOTAL NE 0 THEN
        WS.RATE.CUR.CORP  = WS.RATE.CUR.CORP  / WS.AMT.CUR.CORP.TOTAL
    END

    WS.RATE.PREV.PERS.TOTAL.ALL += WS.AMT.PREV.PERS.TOTAL * WS.RATE.PREV.PERS
    WS.RATE.PREV.CORP.TOTAL.ALL += WS.AMT.PREV.CORP.TOTAL * WS.RATE.PREV.CORP

    WS.RATE.CUR.PERS.TOTAL.ALL += WS.AMT.CUR.PERS.TOTAL * WS.RATE.CUR.PERS
    WS.RATE.CUR.CORP.TOTAL.ALL += WS.AMT.CUR.CORP.TOTAL * WS.RATE.CUR.CORP

    WS.RATE.END.PERS.TOTAL.ALL += WS.AMT.END.PERS.TOTAL * WS.RATE.END.PERS
    WS.RATE.END.CORP.TOTAL.ALL += WS.AMT.END.CORP.TOTAL * WS.RATE.END.CORP

*** DRW ***

    WS.AMT.DRW.PERS = ( WS.AMT.PREV.PERS + WS.AMT.CUR.PERS ) - WS.AMT.END.PERS
    WS.AMT.DRW.CORP = ( WS.AMT.PREV.CORP + WS.AMT.CUR.CORP ) - WS.AMT.END.CORP

    WS.AMT.DRW.PERS.TOTAL.ALL += WS.AMT.DRW.PERS
    WS.AMT.DRW.CORP.TOTAL.ALL += WS.AMT.DRW.CORP

***********

    WS.AMT.PREV.PERS   = DROUND(WS.AMT.PREV.PERS,'1')   ; WS.RATE.PREV.PERS  = DROUND(WS.RATE.PREV.PERS,'3')
    WS.AMT.PREV.CORP   = DROUND(WS.AMT.PREV.CORP,'1')   ; WS.RATE.PREV.CORP  = DROUND(WS.RATE.PREV.CORP,'3')
    WS.AMT.CUR.PERS    = DROUND(WS.AMT.CUR.PERS,'1')    ; WS.RATE.CUR.PERS   = DROUND(WS.RATE.CUR.PERS,'3')
    WS.LORATE.CUR.PERS = DROUND(WS.LORATE.CUR.PERS,'3') ; WS.HIRATE.CUR.PERS = DROUND(WS.HIRATE.CUR.PERS,'3')
    WS.AMT.CUR.CORP    = DROUND(WS.AMT.CUR.CORP,'1')    ; WS.RATE.CUR.CORP   = DROUND(WS.RATE.CUR.CORP,'3')
    WS.LORATE.CUR.CORP = DROUND(WS.LORATE.CUR.CORP,'3') ; WS.HIRATE.CUR.CORP = DROUND(WS.HIRATE.CUR.CORP,'3')
    WS.AMT.DRW.PERS    = DROUND(WS.AMT.DRW.PERS,'1')    ; WS.AMT.DRW.CORP    = DROUND(WS.AMT.DRW.CORP,'1')
    WS.AMT.END.PERS    = DROUND(WS.AMT.END.PERS,'1')    ; WS.RATE.END.PERS   = DROUND(WS.RATE.END.PERS,'3')
    WS.AMT.END.CORP    = DROUND(WS.AMT.END.CORP,'1')    ; WS.RATE.END.CORP   = DROUND(WS.RATE.END.CORP,'3')

    BB.DATA  = LINE.DESC:","
    BB.DATA := WS.AMT.PREV.PERS:","
    BB.DATA := WS.RATE.PREV.PERS:","
    BB.DATA := WS.AMT.PREV.CORP:","
    BB.DATA := WS.RATE.PREV.CORP:","
    BB.DATA := WS.AMT.CUR.PERS:","
    BB.DATA := WS.RATE.CUR.PERS:","

    IF WS.LORATE.CUR.PERS EQ 99 THEN
        WS.LORATE.CUR.PERS = 0
    END

    BB.DATA := WS.LORATE.CUR.PERS:","
    BB.DATA := WS.HIRATE.CUR.PERS:","
    BB.DATA := WS.AMT.CUR.CORP:","
    BB.DATA := WS.RATE.CUR.CORP:","

    IF WS.LORATE.CUR.CORP EQ 99 THEN
        WS.LORATE.CUR.CORP = 0
    END

    BB.DATA := WS.LORATE.CUR.CORP:","

    BB.DATA := WS.HIRATE.CUR.CORP:","
    BB.DATA := WS.AMT.DRW.PERS:","
    BB.DATA := WS.AMT.DRW.CORP:","
    BB.DATA := WS.AMT.END.PERS:","
    BB.DATA := WS.RATE.END.PERS:","
    BB.DATA := WS.AMT.END.CORP:","
    BB.DATA := WS.RATE.END.CORP:","

    IF KK EQ 1 THEN
        BB.DATA := CCY.RATE:","
    END

    KK++

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*===================================================
PRINT.TOTAL:
*===========
    IF WS.AMT.PREV.PERS.TOTAL.ALL NE 0 THEN
        WS.RATE.PREV.PERS.TOTAL.ALL = WS.RATE.PREV.PERS.TOTAL.ALL / WS.AMT.PREV.PERS.TOTAL.ALL
    END

    IF WS.AMT.PREV.CORP.TOTAL.ALL NE 0 THEN
        WS.RATE.PREV.CORP.TOTAL.ALL = WS.RATE.PREV.CORP.TOTAL.ALL / WS.AMT.PREV.CORP.TOTAL.ALL
    END

    IF WS.AMT.CUR.PERS.TOTALL NE 0 THEN
        WS.RATE.CUR.PERS.TOTAL.ALL = WS.RATE.CUR.PERS.TOTAL.ALL / WS.AMT.CUR.PERS.TOTALL
    END

    IF WS.AMT.CUR.CORP.TOTALL NE 0 THEN
        WS.RATE.CUR.CORP.TOTAL.ALL = WS.RATE.CUR.CORP.TOTAL.ALL / WS.AMT.CUR.CORP.TOTALL
    END

    IF WS.AMT.END.PERS.TOTAL.ALL NE 0 THEN
        WS.RATE.END.PERS.TOTAL.ALL = WS.RATE.END.PERS.TOTAL.ALL / WS.AMT.END.PERS.TOTAL.ALL
    END

    IF WS.AMT.END.CORP.TOTAL.ALL NE 0 THEN
        WS.RATE.END.CORP.TOTAL.ALL = WS.RATE.END.CORP.TOTAL.ALL / WS.AMT.END.CORP.TOTAL.ALL
    END

    WS.RATE.PREV.PERS.TOTAL.ALL  = DROUND(WS.RATE.PREV.PERS.TOTAL.ALL,'3')
    WS.RATE.PREV.CORP.TOTAL.ALL  = DROUND(WS.RATE.PREV.CORP.TOTAL.ALL,'3')

    WS.RATE.CUR.PERS.TOTAL.ALL  = DROUND(WS.RATE.CUR.PERS.TOTAL.ALL,'3')
    WS.RATE.CUR.CORP.TOTAL.ALL  = DROUND(WS.RATE.CUR.CORP.TOTAL.ALL,'3')

    WS.RATE.END.PERS.TOTAL.ALL  = DROUND(WS.RATE.END.PERS.TOTAL.ALL,'3')
    WS.RATE.END.CORP.TOTAL.ALL  = DROUND(WS.RATE.END.CORP.TOTAL.ALL,'3')

    BB.DATA  = "��������":","
    BB.DATA := WS.AMT.PREV.PERS.TOTAL.ALL:","
    BB.DATA := WS.RATE.PREV.PERS.TOTAL.ALL:","
    BB.DATA := WS.AMT.PREV.CORP.TOTAL.ALL:","
    BB.DATA := WS.RATE.PREV.CORP.TOTAL.ALL:","
    BB.DATA := WS.AMT.CUR.PERS.TOTAL.ALL:","
    BB.DATA := WS.RATE.CUR.PERS.TOTAL.ALL:","
    BB.DATA := ","
    BB.DATA := ","
    BB.DATA := WS.AMT.CUR.CORP.TOTAL.ALL:","
    BB.DATA := WS.RATE.CUR.CORP.TOTAL.ALL:","
    BB.DATA := ","
    BB.DATA := ","
    BB.DATA := WS.AMT.DRW.PERS.TOTAL.ALL:","
    BB.DATA := WS.AMT.DRW.CORP.TOTAL.ALL:","
    BB.DATA := WS.AMT.END.PERS.TOTAL.ALL:","
    BB.DATA := WS.RATE.END.PERS.TOTAL.ALL:","
    BB.DATA := WS.AMT.END.CORP.TOTAL.ALL:","
    BB.DATA := WS.RATE.END.CORP.TOTAL.ALL:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*==============================================================
