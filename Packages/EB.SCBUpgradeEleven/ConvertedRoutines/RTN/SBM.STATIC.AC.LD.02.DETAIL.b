* @ValidationCode : Mjo2MjUzNzkyNTU6Q3AxMjUyOjE2NDA4NTEyMTczMzI6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 10:00:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
PROGRAM SBM.STATIC.AC.LD.02.DETAIL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL

*********************** OPENING FILES *****************************
    FN.AC  = "F.CBE.STATIC.AC.LD.DETAIL" ; F.AC   = ""
    CALL OPF (FN.AC,F.AC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*------------------------------------------------------------------
********************** CLEAR & COPY NEW DATA TO OLD DATA ******************

    T.SEL = "SELECT ":FN.AC
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*------------------------------------------------------

            R.AC<STD.CBEM.IN.LCYO> = R.AC<STD.CBEM.IN.LCY>
            R.AC<STD.CBEM.IN.FCYO> = R.AC<STD.CBEM.IN.FCY>

            CALL F.WRITE(FN.AC,KEY.LIST<I>,R.AC)
            CALL  JOURNAL.UPDATE(KEY.LIST<I>)

        NEXT I
    END
END
