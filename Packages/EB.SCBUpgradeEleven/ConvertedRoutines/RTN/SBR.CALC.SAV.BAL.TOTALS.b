* @ValidationCode : Mjo5MTA0NDIxMTU6Q3AxMjUyOjE2NDA4NTgwNzA5NzY6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:54:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
***----------------------------
***---- CREATE BY NESSMA ------
***----------------------------
PROGRAM SBR.CALC.SAV.BAL.TOTALS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CALC.SAV.BAL.ALL
*-------------------------------------------------
    XX1  = 0 ; XX2  = 0 ; XX3  = 0 ; XX4  = 0
    XX5  = 0 ; XX6  = 0 ; XX7  = 0 ; XX8  = 0 ; XX9  = 0

    FN.TMP  = 'F.SCB.CALC.SAV.BAL.ALL'    ; F.TMP  = ''
    CALL OPF(FN.TMP,F.TMP)

    ALL.SEL = "SELECT F.SCB.CALC.SAV.BAL.ALL BY @ID"
    CALL EB.READLIST(ALL.SEL,ALL.LIST,"",ALL.SELECTED,ER.ALL)

    IF ALL.SEL THEN
        FOR NN = 1 TO ALL.SELECTED
            CALL F.READ(FN.TMP,ALL.LIST<NN>,R.TMP,F.TMP,E.TMP)
            XX1 += R.TMP<SAV.BAL.NO.OF.ACCOUNTS>
            XX2 += R.TMP<SAV.BAL.MONTH.6502.COUNT>
            XX3 += R.TMP<SAV.BAL.MONTH.6501.COUNT>
            XX4 += R.TMP<SAV.BAL.MONTH.6503.COUNT>
            XX5 += R.TMP<SAV.BAL.MONTH.6504.COUNT>
            XX6 += R.TMP<SAV.BAL.MONTH.6511.COUNT>
            XX7 += R.TMP<SAV.BAL.MONTH.ALL.COUNT>
            XX8 += R.TMP<SAV.BAL.NUMBER.ALL.COUNT>
            XX9 += R.TMP<SAV.BAL.TOTAL.ALL.COUNT>
        NEXT NN
    END

    TMP.ID = "Z"
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,E.TMP)
    R.TMP<SAV.BAL.BAYAN.TXT> = "������"
    R.TMP<SAV.BAL.RUN.DATE>  = TODAY
    R.TMP<SAV.BAL.NO.OF.ACCOUNTS>   = XX1
    R.TMP<SAV.BAL.MONTH.6502.COUNT> = XX2
    R.TMP<SAV.BAL.MONTH.6501.COUNT> = XX3
    R.TMP<SAV.BAL.MONTH.6503.COUNT> = XX4
    R.TMP<SAV.BAL.MONTH.6504.COUNT> = XX5
    R.TMP<SAV.BAL.MONTH.6511.COUNT> = XX6
    R.TMP<SAV.BAL.MONTH.ALL.COUNT>  = XX7
    R.TMP<SAV.BAL.NUMBER.ALL.COUNT> = XX8
    R.TMP<SAV.BAL.TOTAL.ALL.COUNT>  = XX9

    WRITE R.TMP TO F.TMP , TMP.ID ON ERROR
    END
*----------------------------------------------
RETURN
END
