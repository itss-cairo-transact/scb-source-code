* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
    SUBROUTINE SBM.CACH(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    ID = "GENLED":"...":COMP
    DD = TODAY[1,6]
**    T.SEL = "SELECT FBNK.RE.STAT.LINE.BAL WITH ( @ID LIKE GENLED...710...EG0010001... OR  @ID LIKE GENLED...730...EG0010001... OR @ID LIKE GENLED...220...EG0010001... OR @ID LIKE GENLED...230...EG0010001... OR @ID LIKE GENLED...231...EG0010001... OR @ID LIKE GENLED...250...EG0010001... ) AND (@ID UNLIKE  ...LOCA... AND @ID UNLIKE ...EGP... )  AND SYSTEM.DATE EQ 20081130 BY CURRENCY"
    T.SEL = "SELECT FBNK.RE.STAT.LINE.BAL WITH (@ID LIKE GENLED...710...":COMP:" OR @ID LIKE GENLED...730...":COMP:" OR @ID LIKE GENLED...220...":COMP:" OR @ID LIKE GENLED...230...":COMP:" OR @ID LIKE GENLED...231...":COMP:" OR @ID LIKE GENLED...250...":COMP:") AND (@ID UNLIKE ...LOCA... AND @ID UNLIKE ...EGP... AND @ID UNLIKE ...GENLEDMN...) AND SYSTEM.DATE EQ 20101130 BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*=================================================================
    IF SELECTED >= 1 THEN
        FOR KK = 1 TO SELECTED
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,KK> = '@ID'
        ENQ<3,KK> = 'EQ'
        ENQ<4,KK> = 'DUMMY'
    END
    RETURN
END
