* @ValidationCode : MjotNzkzMTE2NzQ5OkNwMTI1MjoxNjQwNzk0NjIzODcyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:17:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*-----------------------------------------------------------------------------
* SUBROUTINE SBM.MOKHSAS.BATCH
PROGRAM   SBM.MOKHSAS.BATCH
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE

***------------------Clearing File------------M.ELSAYED--------------***

    EXECUTE  "CLEAR-FILE F.SCB.CUS.POS.TST"
    PRINT "SAIZO >> CLEARING DONE"

***--------------- CREATE FILE  SCB.CUS.POS.TST -----------------------***

    EXECUTE "CR.MOKHSAS.AC"
    PRINT "DONE AC"

    EXECUTE "CR.MOKHSAS.LD"
    PRINT "DONE LD"

    EXECUTE "CR.MOKHSAS.PD"
    PRINT "DONE PD"

    EXECUTE "CR.MOKHSAS.LC"
    PRINT "DONE LC"

    EXECUTE "CR.MOKHSAS.CR"
    PRINT "DONE CR"

    EXECUTE "CR.MOKHSAS.MM"
    PRINT "DONE MM"

    PRINT "DONE ALL"

*-------------------------------------------------------------------
RETURN
END
