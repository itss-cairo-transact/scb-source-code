* @ValidationCode : Mjo5NzY5NDM3NTI6Q3AxMjUyOjE2NDQ5MjUwNjAzNjM6bGFwOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 13:37:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
*****************MAHMOUD 24/3/2014 *********************************
* TO GET THE TAX FOR HIGHST DEBIT BALANCE THROUGH THE LAST QUARTER *
********************************************************************
SUBROUTINE SBQ.MAX.DR.TAX

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CREDIT.STAMP.EXP
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LOAN.STAMP
*------------------------------------------------
*Line [ 52 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Are you sure to run the program ??', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        TTDD = TODAY
        COMP = ID.COMPANY
        TTXX = TTDD

        CALL LAST.WDAY(TTXX)
        IF TTDD EQ TTXX THEN
            IF TTXX[5,2] EQ '3' OR TTXX[5,2] EQ '6' OR TTXX[5,2] EQ '9' OR TTXX[5,2] EQ '12' THEN
                GOSUB INITIATE
*Line [ 63 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 64 ] Adding EB.SCBUpgradeEleven. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
                GOSUB CALLDB
                GOSUB PROCESS
            END ELSE
                CRT "NOT A QUARTER END OF MONTH"
                RETURN
            END
        END ELSE
            CRT "NOT END OF MONTH DATE"
        END
    END
RETURN
*==============================================================
INITIATE:
*--------
    F.PATH        = FN.OFS.IN
    FN.OFS.SOURCE = "F.OFS.SOURCE"
    F.OFS.SOURCE  = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = "OFS.IN"
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    FN.OMQ = "F.OFS.MESSAGE.QUEUE"
    F.OMQ  = ""
    CALL OPF(FN.OMQ,F.OMQ)
    TD1  = TODAY
    TODT = TD1
    FRDT = TODT[1,6]:'01'
    CALL ADD.MONTHS(FRDT,'-2')

    CRT FRDT:" - ":TODT
    CUST.NAME    = ''
    Y.OPEN.BAL   = 0
    Y.MIN.BAL    = ''
    XX           = SPACE(130)
    K            = 0
*#    CAT.GRP  = " 3201 1102 1404 1414 1206 1208 1401 1402 1405 1406 1415 11240 11232 11231 1480 1481 1202 1203 "
*#    CAT.GRP := " 1212 1502 1503 1214 1416 1201 1211 1477 11242 1501 1588 1599 1001 1516 1002 1523 1217 1524 1205 "
*#    CAT.GRP := " 1207 1525 1216 1215 1504 1507 1508 1512 1514 1518 1519 1582 1558 1598 1595 1596 1597 1577 "
*#    CAT.GRP := " 1499 1483 1493 1301 1302 1303 1377 1390 1399 1566 1591 "
*#    CAT.GRP := " 1003 1059 1011 1509 1510 1511 1513 1579 1534 1544 1559 "
*#    CAT.GRP := " 1407 1413 1408 1445 1455 "

    BANNED.GROUP = " 1002 1205 1206 1207 1208 1701 "
RETURN
*===============================================================
CALLDB:
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.ACC     = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ACC1     = 'FBNK.ACCOUNT' ; F.ACC1 = ''
    CALL OPF(FN.ACC1,F.ACC1)
    FN.ACC.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACC.ACT = ''
    CALL OPF(FN.ACC.ACT,F.ACC.ACT)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ERR.FT = '' ; SELECTED.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.EXP = 'F.SCB.CREDIT.STAMP.EXP' ; F.EXP = '' ; R.EXP = '' ; ERR.EXP = '' ; SELECTED.EXP = ''
    CALL OPF(FN.EXP,F.EXP)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
RETURN
*************************************************************************
PROCESS:
*-------
    FT.SEL  = "SELECT ":FN.FT:" WITH TRANSACTION.TYPE EQ 'AC29' AND CO.CODE NE EG0010099"
    CALL EB.READLIST(FT.SEL,FT.LIST,"",SELECTED.FT,ERR.FT)
    IF SELECTED.FT THEN
        TEXT = "�� ������� �� ���" ; CALL REM
        RETURN
    END
*-------------------------------------------------------------------
    S.SEL  = "SSELECT FBNK.CUSTOMER.ACCOUNT WITH COMPANY.CO NE EG0010099"
    S.SEL := " BY COMPANY.CO "
    CALL EB.READLIST(S.SEL,KEY.LIST,'',SELECTED,ER.MSG)
    CRT "*** Total Selected *** ":SELECTED
    LOOP
        REMOVE CUS.ID FROM KEY.LIST SETTING POS.CUS
    WHILE CUS.ID:POS.CUS
*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,CUS.LCL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LCL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CRD.STA  = CUS.LCL<1,CULR.CREDIT.STAT>
        CRD.COD  = CUS.LCL<1,CULR.CREDIT.CODE>
        ACCT.CR  = ''
        ACCT.EXP = ''
*******************************************************************
        CALL F.READ(FN.EXP,CUS.ID,R.EXP,F.EXP,ERR.EXP)
        IF NOT(ERR.EXP) THEN
            ACCT.EXP = R.EXP<CHR.EXP.ACCOUNT.NUMBER>
            ACCT.CR  = R.EXP<CHR.EXP.CREDIT.ACCT>
        END
********************************************************************
        IF (CRD.COD GE 110) OR (CRD.STA NE '') THEN
            R.CUS.ACC = ACCT.CR
        END ELSE
            CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,CUS.ACC.ERR)
        END
        LOOP
            REMOVE Y.ACC.ID FROM R.CUS.ACC SETTING POSS1
        WHILE Y.ACC.ID:POSS1

            LOCATE Y.ACC.ID IN ACCT.EXP<1,1> SETTING POS.EXP THEN NULL ELSE
                CALL F.READ(FN.ACC,Y.ACC.ID,R.ACC,F.ACC,Y.ACC.ERR)
                Y.CURR    = R.ACC<AC.CURRENCY>
                Y.CAT     = R.ACC<AC.CATEGORY>
                Y.ACC.COM = R.ACC<AC.CO.CODE>
*************************************
                IF (Y.CAT[1,2] NE '90') AND (Y.ACC.COM NE 'EG0010099') THEN

                    FINDSTR Y.CAT:" " IN BANNED.GROUP SETTING POS.BAN THEN NULL ELSE
*************************************
                        KK1 = 0
                        CALL GET.ENQ.BALANCE(Y.ACC.ID,FRDT,Y.OPEN.BAL)
                        KK1++
                        Y.MIN.BAL<KK1> = Y.OPEN.BAL
                        ACT.DATE = FRDT
                        LOOP
                        WHILE ACT.DATE LE TODT
                            ACT.MNTH = ACT.DATE[1,6]
                            ACC.ACT.ID = Y.ACC.ID:"-":ACT.MNTH
****************************************************
                            CALL F.READ(FN.ACC.ACT,ACC.ACT.ID,R.ACC.ACT,F.ACC.ACT,Y.ERR1)
                            IF NOT(Y.ERR1) THEN
                                MIN.BAL.AC = MINIMUM(R.ACC.ACT<IC.ACT.BALANCE>)
                                IF MIN.BAL.AC NE '' AND MIN.BAL.AC LT 0 THEN
                                    KK1++
                                    Y.MIN.BAL<KK1> = MIN.BAL.AC
                                END
                            END
****************************************************
                            CALL ADD.MONTHS(ACT.DATE,'1')
                        REPEAT
                        MIN.DR.BAL = MINIMUM(Y.MIN.BAL)
                        IF MIN.DR.BAL NE '' AND MIN.DR.BAL LT 0 THEN
                            DR.CHRG = MIN.DR.BAL * (0.05 / 100) * -1
                            IF Y.CURR NE "JPY" THEN
                                DR.CHRG = DROUND(DR.CHRG,2)
                            END ELSE
                                DR.CHRG = DROUND(DR.CHRG,0)
                            END
                            IF DR.CHRG GT 0 THEN
                                GOSUB OFS.CHRG.REC
                                CRT Y.ACC.ID:"-":CR.AC:"-":DR.CHRG
                            END
                            Y.MIN.BAL = ''
                        END
                    END
                END
            END
        REPEAT
    REPEAT

OFS.CHECK:
*##    IN.SEL = "SELECT OFS.IN WITH @ID EQ [STMP.CHRG]"
*##    CALL EB.READLIST(IN.SEL,IN.LIST,'',SELECTED.IN,ER.MSG.IN)
*##    IF SELECTED.IN THEN
*##        GOTO OFS.CHECK
*##    END
    LOOP
        SELECT F.OMQ TO 1
    WHILE READNEXT Y.ID FROM 1 DO
        CRT FN.OMQ:" not empty, retrying again in 10 seconds..."
        SLEEP 10
    REPEAT
    CRT FN.OMQ:" is empty"
    GOSUB COLLECT.TO.HQ
RETURN
****************************************************
OFS.CHRG.REC:
*------------
    COMMA = ","
    CURR = Y.ACC.ID[9,2]
    CR.AC = Y.CURR:"16209000300":Y.ACC.COM[2]
    DATEE = TODAY
    OFS.USER.INFO = "AUTO.CHRGE":"/":"/":Y.ACC.COM

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC29":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":Y.CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":Y.CURR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":Y.ACC.ID:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.AC:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DR.CHRG:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS.ID

    F.PATH  = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OFS.ID  = "STMP.CHRG.":Y.ACC.ID

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':Y.ACC.COM: ON ERROR  TEXT = " ERROR "
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':Y.ACC.COM: ON ERROR  TEXT = " ERROR "

    GOSUB WRITE.STMP
RETURN
*****************************************************************
WRITE.STMP:
*------------
    FN.AC = "FBNK.ACCOUNT"      ; F.AC = ""  ; R.AC  = ""
    CALL OPF(FN.AC, F.AC)

    FN.TMP = "F.SCB.LOAN.STAMP" ; F.TMP = "" ; R.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    TD = TODAY
****CALL ADD.MONTHS(TD,'-1')

    TMP.ID = Y.ACC.ID : "." : TD[1,6]
    CALL F.READ(FN.AC,Y.ACC.ID ,R.AC,F.AC,ERR.AC)

    CALL F.READ(FN.TMP,TMP.ID ,R.TMP,F.TMP,ERR.TMP)
    R.TMP<SLS.CATEGORY>     = R.AC<AC.CATEGORY>
    R.TMP<SLS.CURRENCY>     = R.AC<AC.CURRENCY>
    R.TMP<SLS.DATE>         = TD[1,6]
    R.TMP<SLS.BRANCH>       = R.AC<AC.CO.CODE>
    R.TMP<SLS.CUSTOMER.ID>  = R.AC<AC.CUSTOMER>
    R.TMP<SLS.STAMP.AMOUNT> = DR.CHRG
    R.TMP<SLS.MAX.DR.AMT>   = MIN.DR.BAL

    CALL F.WRITE(FN.TMP ,TMP.ID , R.TMP)
    CALL JOURNAL.UPDATE(TMP.ID)

RETURN
*****************************************************************
COLLECT.TO.HQ:
*-------------
    H.SEL  = " SSELECT FBNK.ACCOUNT WITH CATEGORY EQ 16209 AND @ID LIKE ...16209000300... AND CO.CODE NE EG0010099 "
    H.SEL := " BY CURRENCY "
    CALL EB.READLIST(H.SEL,KEY.LIST1,'',SELECTED1,ER.MSG1)

    LOOP
        REMOVE Y.ACC.ID1 FROM KEY.LIST1 SETTING POSS2
    WHILE Y.ACC.ID1:POSS2
        CALL F.READ(FN.ACC1,Y.ACC.ID1,R.ACC1,F.ACC1,Y.ACC.ERR1)
        Y.CURR1    = R.ACC1<AC.CURRENCY>
        Y.CAT1     = R.ACC1<AC.CATEGORY>
        Y.ACC.COM1 = R.ACC1<AC.CO.CODE>
        Y.ACC.BAL1 = R.ACC1<AC.ONLINE.ACTUAL.BAL>
        IF Y.ACC.BAL1 NE 0 AND Y.ACC.BAL1 NE '' THEN
            COMMA = ","
            CURR = Y.CURR1
            CR.AC1 = Y.CURR1:"1620900030099"
            DATEE = TODAY
            OFS.USER.INFO = "AUTO.CHRGE":"/":"/":Y.ACC.COM1

            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC29":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":Y.CURR1:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":Y.CURR1:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":Y.ACC.ID1:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.AC1:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":Y.ACC.BAL1:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA
            OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA

            F.PATH = FN.OFS.IN
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "STMP.CHRG.":Y.ACC.ID1

            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':Y.ACC.COM1: ON ERROR  TEXT = " ERROR "
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':Y.ACC.COM1: ON ERROR  TEXT = " ERROR "
        END
    REPEAT
RETURN
*==============================================================
END
