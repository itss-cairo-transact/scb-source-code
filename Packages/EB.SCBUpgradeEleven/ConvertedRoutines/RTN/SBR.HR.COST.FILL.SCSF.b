* @ValidationCode : MjotMzk0MDI3MDA3OkNwMTI1MjoxNjQwODY1NTc4MTcyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 13:59:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEleven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEleven
*DONE
SUBROUTINE SBR.HR.COST.FILL.SCSF

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.HR.COST.CENTER

*---------------------------------------
    EXECUTE 'SELECT F.SCB.HR.COST.CENTER WITH FILE.SOURCE EQ SCSF'
    EXECUTE 'DELETE F.SCB.HR.COST.CENTER'
    TEXT = '�� ����� ����� ������' ; CALL REM

*---------------------------------------
    FN.SH = "F.SCB.HR.COST.CENTER"  ; F.SH  = ""
    CALL OPF (FN.SH,F.SH)

    TD = TODAY

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME   = 'SCSF.csv'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            CUST.ID   = FIELD(Y.MSG,",",1)
            EMP.ID    = FIELD(Y.MSG,",",2)
            FILE.SRC  = FIELD(Y.MSG,",",3)
            WS.AMT    = FIELD(Y.MSG,",",4)
            WS.BRN1   = FIELD(Y.MSG,",",5)
            WS.DEP    = FIELD(Y.MSG,",",6)

            WS.BRN    = FMT(WS.BRN1,'R%2')

            SHC.ID = CUST.ID:'.05'
*------------------------------------------------------------------
            CALL F.READ(FN.SH,SHC.ID,R.SH,F.SH,E4)

            IF E4 THEN
                R.SH<SHC.CUSTOMER.ID> = CUST.ID
                R.SH<SHC.EMP.ID>      = EMP.ID
                R.SH<SHC.FILE.SOURCE> = FILE.SRC
                R.SH<SHC.AMOUNT>      = WS.AMT
                R.SH<SHC.DATE>        = TODAY
                R.SH<SHC.BRANCH>      = WS.BRN
                R.SH<SHC.DEP.CODE>    = WS.DEP

                CALL F.WRITE(FN.SH,SHC.ID,R.SH)
                CALL JOURNAL.UPDATE(SHC.ID)
            END
*----------------------------------
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

    TEXT = '�� ����� ����� ������' ; CALL REM


RETURN
************************************************************
END
