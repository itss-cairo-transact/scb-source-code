* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-111</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/12/23 ***
*******************************************

* SUBROUTINE EOY.MOD.BTCH.AFT.COB.20
    PROGRAM EOY.MOD.BTCH.AFT.COB.20

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*---------------------------------------------------
*
    FN.DATE = "F.DATES"                 ; F.DATE  = ""
*
    FN.BTCH = "F.BATCH"                ; F.BTCH = ""
*----------------------------------------------------
    CALL OPF (FN.BTCH,F.BTCH)
    CALL OPF (FN.DATE,F.DATE)
*-------------------------------------------------
    R.TEMP = ""
*------------------------------------------------
    GOSUB A.10.DATE
    GOSUB A.50.GET.BTCH.20
    RETURN
*----------------------------------------
A.10.DATE:

**    EXECUTE "OFSADMINLOGIN"
    WS.DATE.ID      = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.PER.END = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LPE.DATE     = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LAST.PER.END = WS.LPE.DATE
    WS.YY.MM        = WS.LAST.PER.END[1,6]
    WS.DD           = WS.LAST.PER.END[2]
    WS.MM           = WS.YY.MM[5,2] + 0
    WS.LOCAT.DATE   = WS.YY.MM:"01"
    WS.ACC.OFFICER  = ID.COMPANY[2]

    WS.TRNS.DATE = TODAY[1,6]:"01"

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "BATCH"
    SCB.VERSION  = "EOY"
    OFS.MESSAGE.DATA = ''

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    OPENSEQ "OFS.MNGR.IN" , "SBY.MOD.BTCH.AFT.COB.20" TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"SBY.MOD.BTCH.AFT.COB.20"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , "SBY.MOD.BTCH.AFT.COB.20" TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create SBY.MOD.BTCH.AFT.COB.20 File IN OFS.MNGR.IN'
        END
    END

    OPENSEQ "OFS.MNGR.OUT" , "SBY.MOD.BTCH.AFT.COB.20.OUT" TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"SBY.MOD.BTCH.AFT.COB.20.OUT"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "SBY.MOD.BTCH.AFT.COB.20.OUT" TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create SBY.MOD.BTCH.AFT.COB.20.OUT File IN OFS.MNGR.OUT'
        END
    END


    RETURN
**=====================================================================================================================
A.50.GET.BTCH.20:
**=====================================================================================================================
    SEL.BTCH.20 = "SELECT ":FN.BTCH:" WITH @ID LIKE .../SCB.EOY.13300 BY @ID"
    CALL EB.READLIST(SEL.BTCH.20,KEY.BTCH.20,"",SELECTED.BTCH.20,ER.MSG.BTCH.20)
*PRINT SELECTED.BTCH.20
    IF SELECTED.BTCH.20 THEN
        FOR I.BTCH.20 = 1 TO SELECTED.BTCH.20
            CALL F.READ(FN.BTCH,KEY.BTCH.20<I.BTCH.20>,R.BTCH.20,F.BTCH.20,ER.BTCH.20)

            WS.BATCH.ID.BTCH.20 = KEY.BTCH.20<I.BTCH.20>
            WS.BATCH.ID         = KEY.BTCH.20<I.BTCH.20>

            IF NOT(ER.BTCH.20) THEN
                WS.BR.CODE = R.BTCH.20<BAT.CO.CODE>[2]
                R.BATCH    = R.BTCH.20
                GOSUB RE.READ.BATCH.20
            END
        NEXT I.BTCH.20
    END
    RETURN
**=====================================================================================================================
RE.READ.BATCH.20:
**=====================================================================================================================
    WS.FRQ.D = 'D'
    WS.FRQ.A = 'A'

    OFS.MESSAGE.DATA  :=  ",BATCH.STAGE::=":R.BTCH.20<BAT.BATCH.STAGE>
    OFS.MESSAGE.DATA  :=  ",BATCH.ENVIRONMENT::=":R.BTCH.20<BAT.BATCH.ENVIRONMENT>

*Line [ 136 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT.BATCH.20 = DCOUNT(R.BTCH.20<BAT.JOB.NAME>,@VM)
    IF WS.COUNT.BATCH.20 GE 1 THEN
        FOR I.COUNT.BATCH.20 = 1 TO WS.COUNT.BATCH.20
            WS.OFS.COUNT = I.COUNT.BATCH.20
            IF R.BTCH.20<BAT.JOB.NAME><1,I.COUNT.BATCH.20> EQ 'EOY.ACC.13300' THEN
                OFS.MESSAGE.DATA  :=  ",JOB.NAME:":WS.OFS.COUNT:":1:=":R.BTCH.20<BAT.JOB.NAME><1,I.COUNT.BATCH.20>
                OFS.MESSAGE.DATA  :=  ",VERIFICATION:":WS.OFS.COUNT:":1:=":R.BTCH.20<BAT.VERIFICATION><1,I.COUNT.BATCH.20>
                OFS.MESSAGE.DATA  :=  ",FREQUENCY:":WS.OFS.COUNT:":1:=":WS.FRQ.A
                OFS.MESSAGE.DATA  :=  ",NEXT.RUN.DATE:":WS.OFS.COUNT:":1:=":" "
            END ELSE
                OFS.MESSAGE.DATA  :=  ",JOB.NAME:":WS.OFS.COUNT:":1:=":R.BTCH.20<BAT.JOB.NAME><1,I.COUNT.BATCH.20>
                OFS.MESSAGE.DATA  :=  ",VERIFICATION:":WS.OFS.COUNT:":1:=":R.BTCH.20<BAT.VERIFICATION><1,I.COUNT.BATCH.20>
                OFS.MESSAGE.DATA  :=  ",FREQUENCY:":WS.OFS.COUNT:":1:=":R.BTCH.20<BAT.FREQUENCY><1,I.COUNT.BATCH.20>
                OFS.MESSAGE.DATA  :=  ",NEXT.RUN.DATE:":WS.OFS.COUNT:":1:=":R.BTCH.20<BAT.NEXT.RUN.DATE><1,I.COUNT.BATCH.20>
            END
        NEXT I.COUNT.BATCH.20
    END
    GOSUB RUN.OFS.SUB
    WS.OFS.COUNT = 0
    RETURN
**=====================================================================================================================

BATCH.FRQ.MOD:
    WS.FRQ.D = 'D'
    WS.FRQ.A = 'A'
    OFS.MESSAGE.DATA  :=  ",FREQUENCY:":WS.OFS.COUNT:":1:=":WS.FRQ.D

    WS.OFS.COUNT = 0
    RETURN
**=====================================================================================================================
RUN.OFS.SUB:
**=====================================================================================================================
    IF OFS.MESSAGE.DATA NE '' THEN

        WS.BATCH.ID1 =  FIELD(WS.BATCH.ID,"/",1)
        WS.BATCH.ID2 =  FIELD(WS.BATCH.ID,"/",2)
        WS.BATCH.ID = WS.BATCH.ID1:"^":WS.BATCH.ID2

**        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.BR.CODE:"//EG00100":WS.BR.CODE:",":WS.BATCH.ID:OFS.MESSAGE.DATA
        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,,":WS.BATCH.ID:OFS.MESSAGE.DATA

        BB.IN.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.IN.DATA TO BB.IN ELSE
        END

* SCB R15 UPG 20160717 - S
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

* SCB R15 UPG 20160717 - E

        BB.OUT.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
        END
    END
    OFS.MESSAGE.DATA = ''
    RETURN
**=====================================================================================================================
END
