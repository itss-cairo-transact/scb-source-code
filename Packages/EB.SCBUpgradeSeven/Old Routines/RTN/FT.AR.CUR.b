* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.AR.CUR(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*----------------------------------------
    FN.FT = "FBNK.FUNDS.TRANSFER" ; F.FT = ""
    CALL OPF(FN.FT, F.FT)

    FT.ID = ARG
    CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
    LANGUAGE.NUMBER = R.FT<FT.LOCAL.REF><1,FTLR.LANGUAGE>
    CUR             =  R.FT<FT.DEBIT.CURRENCY>

    IF LANGUAGE.NUMBER EQ '2' THEN
        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
    END

    IF LANGUAGE.NUMBER EQ '1' THEN
        FN.CUR = "F.CURRENCY"  ; F.CUR = ""
        CALL OPF(FN.CUR,F.CUR)

        CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ER.CUR)
        CURR = R.CUR<EB.CUR.CCY.NAME><1,1>
    END
*-- DEFAULT CASE WITH OTHER VERSIONS
*-- EDIT BY NESSMA 20140908
    IF LANGUAGE.NUMBER EQ '' THEN
        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
    END
    ARG = CURR
*------------------------------------------------------
    RETURN
END
