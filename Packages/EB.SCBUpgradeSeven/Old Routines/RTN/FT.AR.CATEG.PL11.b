* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
    SUBROUTINE FT.AR.CATEG.PL11(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    CATEG=R.NEW(FT.DEBIT.ACCT.NO)

    IF CATEG = R.NEW(FT.DEBIT.ACCT.NO) THEN
        CATT = CATEG [11,4]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
        ARG  = CATTT
    END
    IF R.NEW(FT.DEBIT.ACCT.NO)[1,2] EQ 'PL'  THEN
        VV=LEN(R.NEW(FT.DEBIT.ACCT.NO)) - 2
        XX=R.NEW(FT.DEBIT.ACCT.NO)[3,5]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,XX1)
        ARG= XX1
    END
    IF R.NEW(FT.DEBIT.ACCT.NO)[1,3] EQ 'EGP'  THEN
        VV=LEN(R.NEW(FT.DEBIT.ACCT.NO)) - 3
        XX2=R.NEW(FT.DEBIT.ACCT.NO)[4,5]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX2,XX3)
        ARG= XX3
    END

    IF R.NEW(FT.DEBIT.ACCT.NO)[1,3] EQ 'USD'  THEN
        VV=LEN(R.NEW(FT.DEBIT.ACCT.NO)) - 3
        XX2=R.NEW(FT.DEBIT.ACCT.NO)[4,5]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX2,XX4)
        ARG= XX4
    END

    IF R.NEW(FT.DEBIT.ACCT.NO)[1,3] EQ 'EUR'  THEN
        VV=LEN(R.NEW(FT.DEBIT.ACCT.NO)) - 3
        XX5=R.NEW(FT.DEBIT.ACCT.NO)[4,5]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX5,XX6)
        ARG= XX6
    END
    IF R.NEW(FT.DEBIT.ACCT.NO)[1,3] EQ 'GBP'  THEN
        VV=LEN(R.NEW(FT.DEBIT.ACCT.NO)) - 3
        XX7=R.NEW(FT.DEBIT.ACCT.NO)[4,5]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX7,XX8)
        ARG= XX8
    END
    RETURN
END
