* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-------------------------------------AHMED NAHRAWY (FOR EVER)----------------------------------------
    SUBROUTINE FT.AMT.TOTAL(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

*    AMOUNT  = R.NEW(FT.AMOUNT.DEBITED)
    AMOUNT1 = R.NEW(FT.TOTAL.CHARGE.AMOUNT)
    NN = LEN(AMOUNT1)-3
    IN.AMOUNT=AMOUNT1[4,NN]

*    CUR = R.NEW(FT.CREDIT.CURRENCY)
    CUR = R.NEW(FT.TOTAL.CHARGE.AMOUNT)
    CUR1= CUR[1,3]
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR1,CURR)


*    MMM = LEN(ARG) - 3
* NET.AMOUNT = ARG[4,MMM]
*    IN.AMOUNT = ARG[4,MMM]
*    TEXT = "NN = " : IN.AMOUNT ; CALL REM
* TEXT = "MM = ":AMOUNT :IN.AMOUNT; CALL REM

    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"
*    TEXT = 'OUT = ' :OUT.AMOUNT ; CALL REM
    RETURN
END
