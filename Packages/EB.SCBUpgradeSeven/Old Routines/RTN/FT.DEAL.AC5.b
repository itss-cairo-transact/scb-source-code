* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.DEAL.AC5

*1-TO CALL DAEL SLIP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    CALL PRODUCE.DEAL.SLIP("FT.DB.CRD")
    *CALL PRODUCE.DEAL.SLIP("FT.DB.DBT")

***IF (R.NEW(FT.DEBIT.CURRENCY) = 'EGP') AND (R.NEW(FT.CREDIT.CURRENCY) = 'EGP') THEN
***CALL PRODUCE.DEAL.SLIP("FT.TRANS.AC")
*  CALL PRODUCE.DEAL.SLIP("FT.DB.STMP.AC")
***END ELSE
***CALL PRODUCE.DEAL.SLIP("FT.TRANS.FCY")
* CALL PRODUCE.DEAL.SLIP("FT.DB.STMP.AC")

***END
    RETURN
END
