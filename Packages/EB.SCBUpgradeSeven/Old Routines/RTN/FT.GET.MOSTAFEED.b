* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------
** CREATE BY NESSMA
*------------------
    SUBROUTINE FT.GET.MOSTAFEED(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*----------------------------------------------------------
    BENEFICIARY.CUS.EN  = R.NEW(FT.BEN.CUSTOMER)<1,1>
    BENEFICIARY.CUS.EN := " " : R.NEW(FT.BEN.CUSTOMER)<1,2>
    BENEFICIARY.CUS.EN := " " : R.NEW(FT.BEN.CUSTOMER)<1,3>

    BENEFICIARY.CUS.AR  = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1>
    BENEFICIARY.CUS.AR := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2>
    BENEFICIARY.CUS.AR := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,3>

    CREDIT.NOTE         = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,1>
    CREDIT.NOTE        := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,2>
    CREDIT.NOTE        := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,3>

    LANGUAGE.NUMBER     = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE>

    IF LANGUAGE.NUMBER EQ '2' THEN
        ARG = BENEFICIARY.CUS.AR
    END

    IF LANGUAGE.NUMBER EQ '1' THEN
        IF BENEFICIARY.CUS.EN THEN
            ARG = BENEFICIARY.CUS.EN
        END ELSE
            IF CREDIT.NOTE THEN
                ARG = CREDIT.NOTE
            END
        END
    END
*-------------------------------------------
    RETURN
END
