* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM FILL.OLD.ACCOUNT

    SUBROUTINE FILL.OLD.ACCOUNT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
    $INSERT  TEMENOS.BP I_AC.LOCAL.REFS

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    FN.AC    = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ETEXT = ''
    CALL OPF( FN.AC,F.AC)

    T.SEL = "SELECT FBNK.ACCOUNT WITH ACTIVIYTY NE '' AND (OLD.ACCOUNT EQ '' OR ALT.ACCT.ID '')"

    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ER.MSG)

    PRINT " SELECTED =":SELECTED

    FOR I =1 TO SELECTED

        CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,ETEXT)

*1037720/24/7101/ 1317.63

        SAM  = R.AC<AC.LOCAL.REF,ACLR.ACTIVIYTY>
        SAM1 = FIELD(SAM,"/",1)
        SAM2 = FIELD(SAM,"/",2)
        SAM3 = FIELD(SAM,"/",3)

        R.AC<AC.LOCAL.REF,ACLR.OLD.ACCOUNT> = "CA":SAM1:"/":SAM2:"/":SAM3
        R.AC<AC.ALT.ACCT.ID>                = "CA":SAM1:"/":SAM2:"/":SAM3

        CALL F.WRITE (FN.AC, KEY.LIST<I>, R.AC)

***        WRITE R.AC TO F.AC , KEY.LIST<I> ON ERROR
***            PRINT "CAN NOT WRITE RECORD ":KEY.LIST<I> :" TO ":FN.AC
**        END

    NEXT I

*STOP
    RETURN

END
