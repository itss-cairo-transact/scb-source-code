* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*****NESSREEN AHMED 24/6/2020***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TR.ATM.FIX.ANNUAL.FEES.LVE2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    TEXT = 'HELLO' ; CALL REM

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
    CALL OPF(FN.FT,F.FT)


**********OPEN FILE********************************
    OPENSEQ "&SAVEDLISTS&" , "FT.REP.ATM.FIX.ANNUAL.FEE.LVE2" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"FT.REP.ATM.FIX.ANNUAL.FEE.LVE2"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "FT.REP.ATM.FIX.ANNUAL.FEE.LVE2.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FT.REP.ATM.FIX.ANNUAL.FEE.LVE2 CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create FT.REP.ATM.FIX.ANNUAL.FEE.LVE2 File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
    BB.DATA = "FT Ref.NO":",":"Debit Customer ID ":",":"Cust.Arabic Name":",":"Debit Acct No":",":"Credit Acct No":",":"Amount Debited":",":"Processing Date":",":"Card Number":",":"Credit Their Ref":",":"Branch Code" :",":"Branch Name":",":"Override Message"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

        *YTEXT = "Enter Start.Date : "
        *CALL TXTINP(YTEXT, 8, 22, "12", "A")
        *ST.D = COMI
        *YTEXT = "Enter End.Date : "
        *CALL TXTINP(YTEXT, 8, 22, "12", "A")
        *ED.D = COMI

   N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC52' AND PAYMENT.DETAILS LIKE Annual... BY CO.CODE"
*    N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC52' AND DATE.TIME LIKE ...200708... BY CO.CODE"
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)

    TEXT = 'SELECTED.N=':SELECTED.N ; CALL REM
    IF SELECTED.N THEN
        FOR I = 1 TO SELECTED.N
            AR.NAME = '' ; DB.ACCT = '' ; CR.ACCT = '' ; DB.CUST = ''   ; DB.AMT.N = ''  ; DB.AMT = ''  ;NOTE.DB = ''  ; DB.TH.REF = '' ; CR.TH.REF = ''  ; PR.DATE = '' ; BR.CODE = '' ; BRANCH.NAME = '' ; OVERR.M = ''

            FT.ID = KEY.LIST.N<I>
            CALL F.READ(FN.FT, FT.ID, R.FT, F.FT, E2)
            LOCAL.REF.FT = R.FT<FT.LOCAL.REF>

            DB.CUST<I> = R.FT<FT.DEBIT.CUSTOMER>
            CALL F.READ(FN.CUSTOMER,DB.CUST<I>, R.CUSTOMER, F.CUSTOMER ,E3)
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
            AR.NAME = LOCAL.REF.C<1,CULR.ARABIC.NAME>

            DB.ACCT<I>   = R.FT<FT.DEBIT.ACCT.NO>
            CR.ACCT<I>   = R.FT<FT.CREDIT.ACCT.NO>
            DB.AMT.N<I>  = R.FT<FT.AMOUNT.DEBITED>
            LENN    = LEN(DB.AMT.N<I>)
            AMT.LEN = LENN - 3
            DB.AMT<I>    = DB.AMT.N<I>[4,AMT.LEN]
            PR.DATE<I>   = R.FT<FT.PROCESSING.DATE>
            NOTE.DB<I>   = LOCAL.REF.FT<1,FTLR.NOTE.DEBITED>
            DB.TH.REF<I> = R.FT<FT.DEBIT.THEIR.REF>
            CR.TH.REF<I> = R.FT<FT.CREDIT.THEIR.REF>
            BR.CODE<I>   = R.FT<FT.CO.CODE>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BR.CODE<I>,BRANCH.NAME)
            OVERR.M<I>   = R.FT<FT.OVERRIDE>

            BB.DATA = FT.ID :",":DB.CUST<I>:",":AR.NAME:",": DB.ACCT<I>:",":CR.ACCT<I>:",":DB.AMT<I>:",":PR.DATE<I>:",":DB.TH.REF<I>:",":CR.TH.REF<I>:",":BR.CODE<I> :",":BRANCH.NAME:",":OVERR.M<I>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END   ;***End of main selection of FT records***
    TEXT = '�� �������� �� �������� ' ;CALL REM;
    RETURN
END
