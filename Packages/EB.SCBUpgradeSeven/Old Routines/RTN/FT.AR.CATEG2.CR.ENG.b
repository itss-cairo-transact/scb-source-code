* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.AR.CATEG2.CR.ENG(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    CATEG=R.NEW(FT.CREDIT.ACCT.NO)

    IF CATEG = R.NEW(FT.CREDIT.ACCT.NO) THEN
        CATT = CATEG [11,4]
        CALL F.READ(FN.CAT,CATT,R.CAT,F.CAT,ERR)
***********        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTTT)
        CATTT = R.CAT<EB.CAT.DESCRIPTION><1,1>

        TEXT = "CATTT : "  : CATTT ; CALL REM
        ARG  = CATTT
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,2] EQ 'PL'  THEN
        VV=LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 2
        XX=R.NEW(FT.CREDIT.ACCT.NO)[2,VV]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
        ARG= XX
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'EGP'  THEN
*VV=LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 2
        XX1=R.NEW(FT.CREDIT.ACCT.NO)
*CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,XX1,ACC)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,ACC,CATTT)
    END
    IF R.NEW(FT.CREDIT.ACCT.NO) NE 'EGP' OR R.NEW(FT.CREDIT.ACCT.NO) NE 'PL' THEN
        XXX1= R.NEW(FT.CREDIT.ACCT.NO)
        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,XXX1,ACC1)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,ACC1,CATTT)
    END
    RETURN
END
