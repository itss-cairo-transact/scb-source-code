* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE F.SCB.CHK.DR.BANK.CHQ(WS.DR.AC,WS.CHQ.NUM,WS.CHQ.AMT,WS.MSG)
************************************************************************
***********by Mohamed Sabry ** 22/9/2011 *****************************
************************************************************************
    INCLUDE  T24.BP  I_COMMON
    INCLUDE  T24.BP  I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
************************************************************************
* this routine check
* **** ������ �� ����� ��� ������� �������� �� ���� ������� ****
************************************************************************
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.CHQ = "F.SCB.FT.DR.CHQ" ; F.CHQ = '' ; R.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)
* WS.LCY.AMT = ABS(WS.LCY.AMT)
    WS.MSG = ''
    CALL F.READ(FN.AC,WS.DR.AC,R.AC,F.AC,ER.AC)
    WS.CATEGORY    = R.AC<AC.CATEGORY>
    IF  WS.CATEGORY EQ 16151 THEN
        IF WS.CHQ.NUM EQ '' THEN
            WS.MSG = "MISSING CHEQUE NUMBER FOR CHEQUE ACCOUNT"
            RETURN
        END
        WS.CHQ.ID = WS.DR.AC:".":WS.CHQ.NUM
        CALL F.READ(FN.CHQ,WS.CHQ.ID,R.CHQ,F.CHQ,ERR.CHQ)
        IF (ERR.CHQ) THEN
            WS.MSG = "Cheque Number NoT Exist"
            RETURN
        END ELSE
            WS.CHQ.STAT = R.CHQ<DR.CHQ.CHEQ.STATUS>
            WS.CHQ.AMT  = R.CHQ<DR.CHQ.AMOUNT>
            IF WS.CHQ.STAT # 1 THEN
                WS.MSG = "Can't cash this Cheque"
                RETURN
            END
            IF WS.CHQ.AMT # WS.CHQ.AMT THEN
                WS.MSG = "Can't cash this Cheque error amount"
                RETURN
            END
        END
    END
    RETURN
