* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
    SUBROUTINE FT.CHQ.NO1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

*****


    XX     = O.DATA[1,2]
    TEXT = O.DATA ; CALL REM
    TEXT = XX     ; CALL REM

    FN.FT   = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''

    FN.BR   = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    FN.BR.H = 'FBNK.BILL.REGISTER$HIS' ; F.BR.H = '' ; R.BR.H = ''

    FN.IN   = 'F.INF.MULTI.TXN' ; F.IN= '' ; R.IN = ''

    FN.TT   = 'FBNK.TELLER'   ; F.TT = '' ; R.TT = ''

    FN.SCB  = 'F.SCB.TRANS.TODAY' ; F.SCB= '' ; R.SCB = ''

    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.FT.H,F.FT.H)
    CALL OPF( FN.BR,F.BR)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.SCB,F.SCB)

    BEGIN CASE
    CASE O.DATA[1,2] EQ 'FT'
       TEXT = O.DATA ; CALL REM
***        CALL DBR('FUNDS.TRANSFER':@FM:FT.CHEQUE.NUMBER,O.DATA,CHQ.NO)
        CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.CHEQUE.NUMBER,O.DATA,CHQ.NO)
        TEXT = CHQ.NO ; CALL REM
        O.DATA = CHQ.NO
    CASE XX EQ 'TT'
        CALL DBR('TELLER':@FM:TT.TE.CHEQUE.NUMBER,O.DATA,CHQ.NO1)
        CALL DBR('TELLER':@FM:TT.TE.SERIAL.NO,O.DATA,CHQ.NO7)
        IF  CHQ.NO1 NE '' THEN
            O.DATA = CHQ.NO1
        END
        IF CHQ.NO1 = '' THEN
            O.DATA = CHQ.NO7
        END
    CASE XX EQ 'IN'
        CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.CHEQUE.NUMBER,O.DATA,CHQ.NO2)
        O.DATA = CHQ.NO2

    END CASE
END
