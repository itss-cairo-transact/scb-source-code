* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------------------------AHMED NAHRAWY (FOR EVER)-----------------------------------------
    SUBROUTINE FT.AMT.DEBIT.TOTAL(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    ACCTCH    =R.NEW(FT.CHARGES.ACCT.NO)
    DEBITACCT =R.NEW(FT.DEBIT.ACCT.NO)
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCTCH,CUR1)
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DEBITACCT,CUR2)

    AMOUNT    = R.NEW(FT.AMOUNT.DEBITED)
    NN = LEN(AMOUNT)-3
    TOT    = R.NEW(FT.TOTAL.CHARGE.AMOUNT)
    TOT1   = TOT[4,13]
    IF ACCTCH NE '' THEN
        IF CUR1 EQ CUR2 THEN
            AMOUNT = AMOUNT[4,NN]+TOT1
            IN.AMOUNT = AMOUNT
            CUR = R.NEW(FT.DEBIT.CURRENCY)
            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            ARG = OUT.AMOUNT :"   ":CURR:" ":"�� ���"
        END
        IF CUR1 NE CUR2 THEN
            AMOUNT = AMOUNT[4,NN]
            IN.AMOUNT = AMOUNT
            CUR = R.NEW(FT.DEBIT.CURRENCY)
            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            ARG = OUT.AMOUNT :"   ":CURR:" ":"�� ���"
        END
    END

    IF ACCTCH EQ '' THEN
        AMOUNT = AMOUNT[4,NN]
        IN.AMOUNT = AMOUNT
        CUR = R.NEW(FT.DEBIT.CURRENCY)
        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        ARG = OUT.AMOUNT :"   ":CURR:" ":"�� ���"
****ARG = "��� �������� ������� ��� � �������� � ���� ���   ���� ���� �� ���"
    END
    RETURN
END
