* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TOT.AMT2.CREDIT.1(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

*    FT.AMT = R.NEW(FT.DEBIT.AMOUNT,1)
*    FT.COMM = R.NEW(FT.COMMISION.AMT)<1,1>
*    FT.POS1 = R.NEW(FT.CHARGE.AMT)<1,1>
*    FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
*    FT.FEES =FT.AMT+ FT.COMM + FT.POS1 +FT.POS2

    XX= R.NEW(FT.AMOUNT.CREDITED)
    VV= LEN(R.NEW(FT.AMOUNT.CREDITED)) - 3
    BB = R.NEW(FT.AMOUNT.CREDITED)[4,VV]
*    TEXT = " BB = " : BB ; CALL REM
*FT.AMT = R.NEW(FT.AMOUNT.DEBITED,1)
    FT.AMT  = BB

    XX1 = R.NEW(FT.COMMISSION.AMT)<1,1>
    VV1 = LEN (R.NEW(FT.COMMISSION.AMT)<1,1>) - 3
    BB1 = R.NEW(FT.COMMISSION.AMT)<1,1>[4,VV1]
*   TEXT = " BB1 = " : BB1 ; CALL REM
    FT.COMM = BB1

    XX2  = R.NEW(FT.CHARGE.AMT)<1,1>
    VV2  = LEN (R.NEW(FT.CHARGE.AMT)<1,1>) - 3
    BB2  = R.NEW(FT.CHARGE.AMT)<1,1>[4,VV2]
*   TEXT = " BB2 = " : BB2 ; CALL REM
    FT.POS1 = BB2

    XX3  = R.NEW(FT.CHARGE.AMT)<1,2>
    VV3  = LEN (R.NEW(FT.CHARGE.AMT)<1,2>) - 3
    BB3  = R.NEW(FT.CHARGE.AMT)<1,2>[4,VV3]
    **TEXT = " BB3 = " : BB3 ; CALL REM
    FT.POS2 = BB3

* FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
    FT.FEES = FT.COMM + FT.POS1 +FT.POS2
    *** FT.FEES = FT.COMM + FT.POS1
    ARG = FT.FEES
    RETURN
END
