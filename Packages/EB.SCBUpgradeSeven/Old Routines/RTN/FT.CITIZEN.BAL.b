* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
****************************
    SUBROUTINE FT.CITIZEN.BAL
***************************************************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL  PRINTER.OFF
    CALL  PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
**********************************************
INITIATE:
*-------
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    ACCT.ID1 = '9949990010321801'
    ACCT.ID2 = 'EGP1611400080099'

    TD1 = TODAY
    CALL CDT("",TD1,'-1W')

    TD2 = TODAY
    CALL CDT("",TD2,'-2W')

    FROM.DATE1 = TD1
    END.DATE1  = TD1

    FROM.DATE2 = TD2
    END.DATE2  = TD2

    AMT.TOTAL1 = ''
    AMT.TOTAL2 = ''
    AMT.TOTAL3 = ''
    AMT.TOTAL4 = ''
    CLOS.BAL1  = ''
    DAYS = ''
    DEBIT.AMT1  = ''
    STE.B.DATE1 = ''
    STE.V.DATE1 = ''
    STE.B.DATE1 = ''

    RETURN
**********************************************
CALLDB:
*--------
    FN.ACC = "FBNK.ACCOUNT"              ; F.ACC  = '' ; R.ACC  = ''    ; CALL OPF(FN.ACC,F.ACC)
    FN.STE1 = "FBNK.STMT.ENTRY"          ; F.STE1  = '' ; R.STE1  = '' ; CALL OPF(FN.STE1,F.STE1)
    FN.STE2 = "FBNK.STMT.ENTRY"          ; F.STE2  = '' ; R.STE2  = '' ; CALL OPF(FN.STE2,F.STE2)
    FN.STE3 = "FBNK.STMT.ENTRY"          ; F.STE3  = '' ; R.STE3  = '' ; CALL OPF(FN.STE3,F.STE3)
    FN.STE4 = "FBNK.STMT.ENTRY"          ; F.STE4  = '' ; R.STE4  = '' ; CALL OPF(FN.STE4,F.STE4)
    RETURN
**********************************************
PROCESS:
*-------
    DAYS = 'C'
    CALL CDD("",TD1,TODAY,DAYS)
    FOR KK = DAYS TO 1 STEP -1
        FROM.DATE1 = TODAY
        END.DATE1 = TODAY
        CW = '-':KK:'W'
        CALL CDT("",FROM.DATE1,CW)
        CALL CDT("",END.DATE1,CW)
        CALL AWD("EG00",FROM.DATE1,HOL)
        IF HOL = "W" THEN
*           TEXT = 'FROM.DATE = ': FROM.DATE1 ; CALL REM;
*           TEXT = 'END.DATE = ': END.DATE1 ; CALL REM;
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID1<1>,FROM.DATE1,END.DATE1,ID.LIST1,OPENING.BAL1,ER1)
            LOOP
                REMOVE STE.ID1 FROM ID.LIST1 SETTING POS.STE1
            WHILE STE.ID1:POS.STE1
                CALL F.READ(FN.STE1,STE.ID1,R.STE1,F.STE1,ER.STE1)
                DEBIT.AMT1 = 0
                IF (R.STE1<AC.STE.BOOKING.DATE> NE R.STE1<AC.STE.VALUE.DATE>)  THEN
                    STE.V.DATE1    = R.STE1<AC.STE.VALUE.DATE>
                    STE.B.DATE1    = R.STE1<AC.STE.BOOKING.DATE>
                    COMP.ID1       = R.STE1<AC.STE.COMPANY.CODE>
                    DEBIT.AMT1     = R.STE1<AC.STE.AMOUNT.LCY>
                    CLOS.BAL1      += DEBIT.AMT1
                    AMT.TOTAL1     += DEBIT.AMT1
                END
            REPEAT
            TOTAL1 = AMT.TOTAL1
*            TEXT="AMT.TOTAL=":TOTAL1;CALL REM;
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,'9949990010321801',CLOS.BAL1)
            IF NOT(STE.B.DATE1) THEN STE.B.DATE1 = FROM.DATE1
            IF NOT(STE.V.DATE1) THEN STE.V.DATE1 = END.DATE1
            IF AMT.TOTAL1 EQ '' THEN
                TOTAL1      = '0.00'
            END
*            TEXT="CLOS.BAL1=":CLOS.BAL1;CALL REM;
           GOSUB PRNT.REC
        END
    NEXT KK
    RETURN
*--------------------------------------------------------------------------------
PRNT.REC:
*--------
    XX1 = SPACE(132)
    XX1<1,1>[1,16]    = FMT(STE.B.DATE1,"####/##/##")
    XX1<1,1>[30,20]   = FMT(TOTAL1,"L2,")
    XX1<1,1>[65,20]   = FMT(STE.V.DATE1,"####/##/##")
    XX1<1,1>[93,20]   = FMT(CLOS.BAL1,"L2,")
    PRINT XX1<1,1>
    PRINT STR(' ',132)
    FROM.DATE1   = ''
    END.DATE     = ''
    TOTAL1       = 0
    AMT.TOTAL1   = 0
    OPEN.TOT1    = 0
    OPENING.BAL1 = 0
    OPENING.BAL2 = 0
    CLOS.BAL1    = 0
    STE.B.DATE1  = ''
    STE.V.DATE1  = ''
    RETURN

*---------------------------PRINT HEAD-----------------
  PRINT.HEAD:
*-------------
    COMP = ID.COMPANY
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'FT.CITIZEN.BAL'
    PR.HD :="'L'":" "
    WS.DAT = TODAY
    WS.DAT = FMT(WS.DAT,"####/##/##")

    PR.HD :="'L'":SPACE(45):"���� ������� ���� ������ �������   : ":WS.DAT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� �������":SPACE(15):"���� �����":SPACE(25):"����� �������":SPACE(15):"���� ������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
END
