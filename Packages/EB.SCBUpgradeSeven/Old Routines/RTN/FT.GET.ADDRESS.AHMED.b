* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------AHMED NAHRAWY (FOR EVER)------------------------------------------
    SUBROUTINE FT.GET.ADDRESS.AHMED(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF ARG[1,2] EQ 'PL'  THEN
        VV=LEN(ARG) - 2
        XX=ARG[3,VV]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
        ARG = CATTT1
*        TEXT = " NAME = " : CATTT1 ; CALL REM
    END ELSE
*  IF ARG[1,2] NE 'PL' THEN
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ARG,CUS)
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,MYLOCAL)
        ADR = MYLOCAL<1,CULR.ARABIC.ADDRESS,1>
        ARG = ADR
    END
    CALL DBR('CUSTOMER':@FM:EB.CUS.STREET,CUS,NAMEENG)
    IF ADR EQ '' THEN
        ARG = NAMEENG
    END
*    TEXT ="ARG = " : ARG ; CALL REM

    RETURN
END
