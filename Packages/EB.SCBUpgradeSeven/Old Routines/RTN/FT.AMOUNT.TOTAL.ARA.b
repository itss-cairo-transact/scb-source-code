* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------------------AHMED NAHRAWY (FOR EVER)---------------
    SUBROUTINE FT.AMOUNT.TOTAL.ARA(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*  DD = DCOUNT(ARG,VM)
    ACCTCH=R.NEW(FT.CHARGES.ACCT.NO)
    TEXT = "ACC :" :ACCTCH ; CALL REM
    TOT    = R.NEW(FT.TOTAL.CHARGE.AMOUNT)[4,10]
    TOT1   = LEN(TOT) -3
    MMM    = LEN(ARG) -3
    CUR    = R.NEW(FT.DEBIT.CURRENCY)
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)

    IF ACCTCH NE '' THEN
        AMOUNT = ARG[4,MMM]+TOT
        IN.AMOUNT  = AMOUNT
        TEXT = IN.AMOUNT ; CALL REM
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"
      ***  TEXT = ARG ; CALL REM
    END

    IF ACCTCH EQ '' THEN
        AMOUNT = ARG[4,MMM]
        IN.AMOUNT  = AMOUNT
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"

    END
          TEXT = "END " ; CALL REM
    RETURN
END
