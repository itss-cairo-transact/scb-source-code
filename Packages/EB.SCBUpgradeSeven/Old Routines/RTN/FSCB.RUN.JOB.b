* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE FSCB.RUN.JOB(WS.TYPE,WS.FREQUENCY,WS.STAGE,WS.PROGRAM.ID)
************************************************************************
***********by Mohamed Sabry ** 1/10/2012 *****************************
************************************************************************
    INCLUDE  T24.BP  I_COMMON
    INCLUDE  T24.BP  I_EQUATE
    INCLUDE  T24.BP  I_F.REPORT.TIMES
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.JOB.TIMES
************************************************************************
* this routine Writing to log file SCB.JOB.TIMES to check if run before or not and writing start and end time
************************************************************************
    FN.SJT= "F.SCB.JOB.TIMES"    ; F.SJT = ""
    CALL OPF (FN.SJT,F.SJT)



    WS.SJT.ID = WS.TYPE:'.':WS.FREQUENCY:'.':WS.STAGE:'-':WS.PROGRAM.ID:'-':TODAY
    CALL F.READ(FN.SJT,WS.SJT.ID,R.SJT,F.SJT,ER.SJT)
    IF NOT(ER.SJT) THEN
        PRINT WS.SJT.ID:" this program was run before "
    END ELSE

        R.SJT<SJT.PROGRAM.NAME> = WS.PROGRAM.ID

        WS.TIME.START           = TIME()
        WS.TIME.START.F         = WS.TIME.START
        R.SJT<SJT.START.TIME>   = OCONV(WS.TIME.START.F, "MTS")

        R.SJT<SJT.FREQUENCY>    = WS.FREQUENCY
        R.SJT<SJT.STAGE>        = WS.STAGE
        R.SJT<SJT.TYPE>         = WS.TYPE
        R.SJT<SJT.SYS.DATE>     = TODAY

        CALL F.WRITE(FN.SJT,WS.SJT.ID,R.SJT)
        CALL JOURNAL.UPDATE(WS.SJT.ID)
        CALL F.RELEASE(FN.SJT,WS.SJT.ID,F.SJT)

        IF WS.TYPE EQ 'S' THEN
            PRINT " >>>> Calling Subroutine " : WS.PROGRAM.ID
            CALL @WS.PROGRAM.ID
        END

        IF WS.TYPE EQ 'P' THEN
            PRINT " >>>> Calling Program... " : WS.PROGRAM.ID
            EXECUTE WS.PROGRAM.ID
        END

        CALL F.READ(FN.SJT,WS.SJT.ID,R.SJT,F.SJT,ER.SJT)
        WS.TIME.END             = TIME()

        IF  WS.TIME.START LE WS.TIME.END THEN
            WS.TIME.DIF          = WS.TIME.END - WS.TIME.START
            R.SJT<SJT.TIME.DIF>  = OCONV(WS.TIME.DIF, "MTS")
        END

        R.SJT<SJT.END.TIME>     = OCONV(WS.TIME.END, "MTS")

        CALL F.WRITE(FN.SJT,WS.SJT.ID,R.SJT)
        CALL JOURNAL.UPDATE(WS.SJT.ID)
        CALL F.RELEASE(FN.SJT,WS.SJT.ID,F.SJT)
    END

    RETURN
