* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-102</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FATCA.US.PERSON
*   PROGRAM FATCA.US.PERSON

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='FATCA.US.PERSON'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)


    KEY.LIST  = ""  ; SELECTED=""  ;  ER.MSG=""
    TOT.AMT   = 0
    COUNT.NO  = 0
    TOTAL.BAL = 0

    RETURN

*========================================================================
PROCESS:

*------------------ACCOUT SELECTION-------------------------
******* SELECTION WITH SPECIFIC SECTORS AND CATEGORIES *************
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT LT 90)"
    T.SEL := " AND (ACTIVE.CUST EQ 'YES') AND (NATIONALITY EQ 'US')"
    T.SEL := " OR ( OTHER.NATIONALITY EQ 'US' )"
    T.SEL := " BY @ID"
*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            CUS.ID     = KEY.LIST<I>
            CUS.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            CO.CODE    = R.CU<EB.CUS.COMPANY.BOOK>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,CUS.BRN)

            GOSUB REPORT.WRITE
        NEXT I
        CALL EB.ROUND.AMOUNT ('USD',TOTAL.BAL,'',"2")
        YY             = ''
        PRINT YY
        YY<1,1>[1,20]  = STR('=',20)
        PRINT YY<1,1>
        YY             = ''
        YY<1,1>[1,40]  = "��������   ":COUNT.NO
        PRINT YY<1,1>
        YY             = ''
        YY<1,1>[1,20]  = STR('=',20)
        PRINT YY<1,1>

        YY             = ''
        PRINT YY
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END

    RETURN

*===============================================================
REPORT.WRITE:
    COUNT.NO = COUNT.NO + 1

    XX  = SPACE(132)
    XX<1,I>[1,7]    = COUNT.NO
    XX<1,I>[12,8]   = CUS.ID
    XX<1,I>[27,35]  = CUS.NAME
    XX<1,I>[65,20]  = CUS.BRN
    PRINT XX<1,I>

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� �������� ����� ������� ���������"
*   PR.HD :="'L'":SPACE(40):""
    PR.HD :="'L'":SPACE(50):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������":SPACE(4):"��� ������":SPACE(5):"��� ������":SPACE(28):"�����"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
END
