* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*----------------------------------NI7OOOOOOOOOOOOOOOOOO-------------------------------------------
    SUBROUTINE FT.GET.NAME(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    BEGIN CASE
    CASE ARG[1,2] EQ 'PL'
        VV=LEN(ARG) - 2
        XX=ARG[3,VV]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
        ARG = CATTT1

    CASE ARG[1,3] EQ 'EGP'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC)
        NAME1=ACC
        ARG=NAME1

    CASE ARG[1,3] EQ 'EUR'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC11)
        NAME2=ACC11
        ARG=NAME2

    CASE ARG[1,3] EQ 'USD'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC12)
        NAME3=ACC12
        ARG=NAME3

    CASE ARG[1,3] EQ 'GBP'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC12)
        NAME4=ACC12
        ARG=NAME4

    CASE ARG[1,3] EQ 'SAR'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC17)
        NAME4=ACC17
        ARG=NAME4

**    CASE ARG[1,2] EQ 99
**      CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC18)
**    NAME4 =ACC18
**  ARG=NAME4
*  CASE ARG[1,2] EQ 99
*       CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS1)
*        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS1,LOCAL1)
*     NAME6 = LOCAL1<1,CULR.ARABIC.NAME>
*    ARG = NAME6
    CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP'
*XS = ARG[2,7]
        CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS10)
        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS10,LOCAL)
        NAME10 = LOCAL<1,CULR.ARABIC.NAME>
        ARG  = NAME10
****CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP'
***XS = ARG[2,7]
*CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS10)
*** CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,XS,LOCAL)
***TEXT = XS    ; CALL REM
***NAME10 = LOCAL<1,CULR.ARABIC.NAME>
***TEXT = NAME10    ; CALL REM
***ARG  = NAME10

    END CASE
    RETURN
END
