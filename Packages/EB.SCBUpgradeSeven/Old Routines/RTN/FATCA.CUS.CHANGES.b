* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-23</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FATCA.CUS.CHANGES
*    PROGRAM FATCA.CUS.CHANGES

*-------- CREATED BY NOHA HAMED 24/11/2014 ---

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='FATCA.CUS.CHANGES'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.FA  = 'FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO'; F.FA = ''
    R.FA = '' ; F.FA = ''
    CALL OPF(FN.FA,F.FA)

    FN.HIS = 'FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO$HIS'; F.HIS = ''
    R.HIS = '' ; F.HIS = ''
    CALL OPF(FN.HIS,F.HIS)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FLAG1 = ''
    FLAG2 = ''
    KEY.LIST  = ""  ; SELECTED=""  ;  ER.MSG=""
    DATEE = ''
    HDATE = ''
    RETURN

*========================================================================
PROCESS:

*------------------TODAY FATCA SELECTION-------------------------
*    DEBUG
    DATEE    = TODAY[3,6]
*   DATEE    = 141208
    CO.CODE  = C$ID.COMPANY
    DEP.CODE = CO.CODE[8,2]

    T.SEL  = "SELECT FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO WITH DATE.TIME LIKE ":DATEE:"... AND DEPT.CODE EQ ":DEP.CODE:" BY @ID"

*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CUS.ID     = KEY.LIST<I>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,ER.CU)
            CUS.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            CALL F.READ(FN.FA,CUS.ID,R.FA,F.FA,ER.FA)
            CALL GET.LOC.REF("FATCA.CUSTOMER.SUPPLEMENTARY.INFO","FA.POST.REST",POS.REST)
            POST.REST  = R.FA<FA.FI.LOCAL.REF><1,POS.REST>

            CUS.ID.HIS = CUS.ID:";1"
            CALL F.READ(FN.HIS,CUS.ID.HIS,R.HIS,F.HIS,ER.HIS)
            HDATE  = R.HIS<FA.FI.DATE.TIME>[1,6]
            IF ER.HIS THEN
                FLAG1 = '�����'
                FLAG2 = ''
            END

            ELSE
                IF HDATE EQ DATEE THEN
                    FLAG1 = '�����'
                    FLAG2 = '�����'
                END ELSE
                    FLAG1 = ''
                    FLAG2 = '�����'
                END
            END
            IF POST.REST EQ 90 THEN
                FLAG1 = '�����'
                FLAG2 = ''
            END
00000000000000000000000       *
            GOSUB  REPORT.WRITE
        NEXT I

        YY             = ''
        PRINT YY
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END

    RETURN

*===============================================================
REPORT.WRITE:

    XX  = SPACE(132)
    XX<1,I>[1,8]    = CUS.ID
    XX<1,I>[15,35]  = CUS.NAME
    XX<1,I>[57,5]  = FLAG1
    IF FLAG2 NE '' THEN
        XX<1,I>[62,5]  = ' � ':FLAG2
    END
    PRINT XX<1,I>

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):"����� ���� ���������� ���� ��� ��� ������ ������� ������ ��� FATCA"
*   PR.HD :="'L'":SPACE(40):""
    PR.HD :="'L'":SPACE(40):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(5):"��� ������":SPACE(28):"��� �������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
END
