* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>273</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FATCA.INDV.GT.50000
*    PROGRAM FATCA.INDV.GT.50000

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='FATCA.INDV.GT.50000'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LMM = 'FBNK.LMM.CUSTOMER' ; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUS.POS = 'F.SCB.CUS.POS' ; F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,E3)
    RATE.USD = R.CUR<EB.CUR.MID.REVAL.RATE,1>


    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    TOT.AMT = 0
    COUNT.NO  = 0
    COUNT.BRN = 0
    BRN.BAL   = 0
    TOTAL.BAL = 0
    CUS.ID1   = ''
    DAT = TODAY
    CALL CDT("",DAT,'-1C')
    TD = TODAY
    CALL CDT ('',TD,'-1W')

    YTEXT = "Enter Date. : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DATE.NO = COMI
    DATT = DATE.NO[1,6]

    YTEXT = "Enter Branch Code. : "
    CALL TXTINP(YTEXT, 8, 22, "9", "A")
    CO.CODE = COMI
    RETURN

*========================================================================
PROCESS:

******* SELECTION WITH SPECIFIC CATEGORIES AND INDIVIDUALS ONLY*************
    T.SEL = "SELECT F.SCB.CUS.POS WITH (CATEGORY IN (2002 2003 2000 5021 5000 21030 2001 5001 5020 21031 2004"
    T.SEL := " 1001 1002 21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21013 6512"
    T.SEL := " 21020 21026 21021 21022 21027 21032 21017 21028 21041 21019 21018 21042 21023 21024 21029 21025"
    T.SEL := " 6501 6502 6503 6504 6511 3011 3012 3013 3014 3017 3010 3005))"
    T.SEL := " AND SECTOR IN (1100 1200 1300 1400 2000) AND CUSTOMER EQ '1301877'"
*T.SEL := " AND SYS.DATE EQ ":DATE.NO
    T.SEL := " AND SYS.DATE LIKE ":DATT:"..."
    T.SEL := " AND COMPANY.BOOK EQ ":CO.CODE
    T.SEL := " BY CUSTOMER"

*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,E1)
            CUS.ID     = R.CUS.POS<CUPOS.CUSTOMER>
            AMT.LCY    = R.CUS.POS<CUPOS.LCY.AMOUNT>
            CURR       = R.CUS.POS<CUPOS.DEAL.CCY>
            CALL F.READ(FN.CU,CUS.ID,R.CU1,F.CU,ER.CU1)

            IF I EQ 1 THEN
                CUS.ID1 = CUS.ID
                CO.CODE1 = CO.CODE
            END

            IF CUS.ID NE CUS.ID1 THEN
                IF (TOT.AMT GT 50000) THEN
                    GOSUB REPORT.WRITE
                END
                TOT.AMT = 0
            END

            AMT.LCY.2 = AMT.LCY / RATE.USD

            TOT.AMT += AMT.LCY.2
            CUS.ID1 = CUS.ID

        NEXT I
        IF (I EQ SELECTED) THEN
            CO.CODE.NEW1 = ''
            IF (TOT.AMT GT 50000) THEN
                GOSUB REPORT.WRITE
            END
        END
        CALL EB.ROUND.AMOUNT ('USD',TOTAL.BAL,'',"2")

        IF CO.CODE.NEW1 EQ '' THEN
            CALL EB.ROUND.AMOUNT ('USD',BRN.BAL,'',"2")
            XX.BRN            = ''
            XX.BRN<1,1>[1,10] = STR('=',20)
            XX.BRN<1,1>[65,10]= STR('=',20)
            PRINT XX.BRN
            XX.BRN            = ''
            XX.BRN<1,1>[1,40] = "������ �����   ":COUNT.BRN
            XX.BRN<1,1>[65,40]= "������ ���� �����   "
            XX.BRN<1,1>[85,15]= BRN.BAL
            PRINT XX.BRN
            XX.BRN            = ''
            XX.BRN<1,1>[1,10] = STR('=',20)
            XX.BRN<1,1>[65,10]= STR('=',20)
            PRINT XX.BRN
            COUNT.BRN = 0
            BRN.BAL   = 0
        END

        YY             = ''
        PRINT YY
        YY<1,1>[1,20]  = STR('=',20)
        YY<1,1>[65,20] = STR('=',20)
        PRINT YY<1,1>
        YY             = ''
        YY<1,1>[1,40]  = "��������   ":COUNT.NO
        YY<1,1>[65,40] = "������ ������   "
        YY<1,1>[85,15] = TOTAL.BAL
        PRINT YY<1,1>
        YY             = ''
        YY<1,1>[1,20]  = STR('=',20)
        YY<1,1>[65,20] = STR('=',20)
        PRINT YY<1,1>

        YY             = ''
        PRINT YY
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END
    RETURN

*===============================================================
REPORT.WRITE:

    XX        = SPACE(132)
    XX.BRN    =  SPACE(132)
    TOTAL.BAL = TOTAL.BAL + TOT.AMT
    COUNT.NO  = COUNT.NO + 1

    CALL F.READ(FN.CU,CUS.ID1,R.CU,F.CU,ER.CU)
    CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
    CO.CODE.NEW  = R.CU<EB.CUS.COMPANY.BOOK>

    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE.NEW,CUS.BRN)
    CALL EB.ROUND.AMOUNT ('USD',TOT.AMT,'',"2")

    IF COUNT.NO EQ 1 THEN
        CO.CODE.NEW1 = CO.CODE.NEW
    END

    IF CO.CODE.NEW1 NE CO.CODE.NEW THEN
        CALL EB.ROUND.AMOUNT ('USD',BRN.BAL,'',"2")
        XX.BRN            = ''
        XX.BRN<1,1>[1,10] = STR('=',20)
        XX.BRN<1,1>[65,10]= STR('=',20)
        PRINT XX.BRN
        XX.BRN            = ''
        XX.BRN<1,1>[1,40] = "������ �����   ":COUNT.BRN
        XX.BRN<1,1>[65,40]= "������ ���� �����   "
        XX.BRN<1,1>[85,15]= BRN.BAL
        PRINT XX.BRN
        XX.BRN            = ''
        XX.BRN<1,1>[1,10] = STR('=',20)
        XX.BRN<1,1>[65,10]= STR('=',20)
        PRINT XX.BRN
        COUNT.BRN = 0
        BRN.BAL   = 0
    END

    XX<1,I>[1,7]    = COUNT.NO
    XX<1,I>[12,8]   = CUS.ID1
    XX<1,I>[27,35]  = CUS.NAME
    XX<1,I>[65,25]  = TOT.AMT
    PRINT XX<1,I>

    BRN.BAL   = BRN.BAL + TOT.AMT
    COUNT.BRN = COUNT.BRN + 1
    CO.CODE.NEW1 = CO.CODE.NEW

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    SYS.DATE = DATE.NO[7,2]:'/':DATE.NO[5,2]:"/":DATE.NO[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(20):"���� �������� ����� ������ ������ ������� �������� 50000 ����� ���� �������� ��������"
    PR.HD :="'L'":SPACE(45):"������ ":SYS.DATE
    PR.HD :="'L'":SPACE(30):STR('_',70)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������":SPACE(4):"��� ������":SPACE(5):"��� ������":SPACE(28):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
END
