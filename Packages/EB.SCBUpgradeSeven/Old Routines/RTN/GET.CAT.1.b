* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE GET.CAT.1(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*-----------------------------------
*    ARG = R.NEW(LD.CATEGORY)

    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,ARG,CATEG)

    WS.DESC = ""
    WS.D1   = CATEG
    WS.D2   = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    TEXT = WS.D2 ; CALL REM
    IF WS.D2 EQ "1W" THEN
        WS.D3 = " ����� "
    END
    IF WS.D2 EQ "2W" THEN
        WS.D3 = " ������� "
    END 
    IF WS.D2 EQ "1M" THEN
        WS.D3 = " ��� "
    END
    IF WS.D2 EQ "2M" THEN
        WS.D3 = " ����� "
    END
    IF WS.D2 EQ "3M" THEN
        WS.D3 = "3 ���� "
    END
    IF WS.D2 EQ "6M" THEN
        WS.D3 = " 6 ���� "
    END
    IF WS.D2 EQ "9M" THEN
        WS.D3 = " 9 ���� "
    END
    IF WS.D2 EQ "12M" THEN
        WS.D3 = " ��� "
    END

    IF WS.D3 EQ '' THEN
        WS.DESC = WS.D1
    END ELSE
        WS.DESC = WS.D1 :" ���� ": WS.D3
    END
    TEXT = WS.DESC ; CALL REM
    ARG = WS.DESC
    RETURN
END
