* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
** ----- NESSREEN AHMED 05/02/2018 -----
*-----------------------------------------------------------------------------
* <Rating>1217</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FILL.TELLER.D.W.NEW

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TELLER.D.W
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

    FN.TT.D.W = 'F.SCB.TELLER.D.W'  ; F.TT.D.W = '' ; R.TT.D.W = ''
    FN.TELLER = 'F.TELLER'          ; F.TELLER = '' ; R.TELLER = ''
    FN.FT     = 'F.FUNDS.TRANSFER'  ; F.FT = ''     ; R.FT = ''
    FN.CURR   = 'F.CURRENCY' ; F.CURR = '' ;R.CURR = '' ; ERR1 = '' ; RETRY1= ''
    FN.CCY    = 'FBNK.RE.BASE.CCY.PARAM'

    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF( FN.TT.D.W,F.TT.D.W)
    CALL OPF( FN.TELLER,F.TELLER)
    CALL OPF(FN.CURR,F.CURR)
    CALL OPF(FN.CCY,F.CCY)

    TT.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN(9 62 53 42 66 37 38 7 32 33 71 101 102 901 910 2 54 41 40 86 84 82 8 36 34 25 100 20 63 21 17 11 12 92 14 97 64 105 95 107 98 65 106 108 96 912 81)  "
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
*TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = ''
            CALL F.READ( FN.TELLER,KEY.LIST.TT<TTI>, R.TELLER, F.TELLER, ETEXT.TT)
            TR.CODE   = R.TELLER<TT.TE.TRANSACTION.CODE>
            CURR      = R.TELLER<TT.TE.CURRENCY.1>
            AMT.LCY   = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            AMT.FCY   = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CUST.ID   = R.TELLER<TT.TE.CUSTOMER.1>
            RATE      = R.TELLER<TT.TE.RATE.1>
            TRN.DAT   = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO.1 = R.TELLER<TT.TE.ACCOUNT.1>
            ACCT.NO.2 = R.TELLER<TT.TE.ACCOUNT.2>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>

            BEGIN CASE
            CASE TR.CODE EQ 2 OR TR.CODE EQ 54 OR TR.CODE EQ 41 OR TR.CODE EQ 40 OR TR.CODE EQ 86 OR TR.CODE EQ 84 OR TR.CODE EQ 82 OR TR.CODE EQ 8 OR TR.CODE EQ 36 OR TR.CODE EQ 34 OR TR.CODE EQ 25 OR TR.CODE EQ 100 OR TR.CODE EQ 20 OR TR.CODE EQ 63 OR TR.CODE EQ 21 OR TR.CODE EQ 17 OR TR.CODE EQ 11 OR TR.CODE EQ 12 OR TR.CODE EQ 92 OR TR.CODE EQ 14 OR TR.CODE EQ 97 OR TR.CODE EQ 64 OR TR.CODE EQ 105 OR TR.CODE EQ 95 OR TR.CODE EQ 107 OR TR.CODE EQ 98 OR TR.CODE EQ 65 OR TR.CODE EQ 106 OR TR.CODE EQ 108 OR TR.CODE EQ 96 OR TR.CODE EQ 912 OR TR.CODE EQ 81
                DW.FLAG = 'W'
            CASE TR.CODE EQ 9 OR TR.CODE EQ 62 OR TR.CODE EQ 53 OR TR.CODE EQ 42 OR TR.CODE EQ 66 OR TR.CODE EQ 37 OR TR.CODE EQ 38 OR TR.CODE EQ 7 OR TR.CODE EQ 32 OR TR.CODE EQ 33 OR TR.CODE EQ 71 OR TR.CODE EQ 101 OR TR.CODE EQ 102 OR TR.CODE EQ 901 OR TR.CODE EQ 910
                DW.FLAG = 'D'
            END CASE

            IF CUST.ID # '' THEN
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
                NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
                IF NEW.SEC = '4650' THEN
                    SC.FLAG = '2'
                END ELSE
                    SC.FLAG = '1'
                END
            END ELSE
                SC.FLAG = '1'
            END
            BR = COMP.CODE[2]
            V.ACC = CURR:'10000':BR:'9900':BR
            KEY.TO.USE = KEY.LIST.TT<TTI>
            CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
            R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
            R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.TT<TTI>
            R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR
            R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
            R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
            R.TT.D.W<TELL.D.W.RATE>       = RATE
            R.TT.D.W<TELL.D.W.NEW.SECTOR> = NEW.SEC
            R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
            R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
            R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
            R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
            R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
            R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

            CALL F.WRITE(FN.TT.D.W,KEY.TO.USE,R.TT.D.W)
** CALL JOURNAL.UPDATE(KEY.TO.USE)
        NEXT TTI
    END
*****FOR BUY AND SELL **********************************
    CC.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN(23 67 58 24 68)  "
**************************************************
    KEY.LIST.CC=""
    SELECTED.CC=""
    ER.MSG.CC=""

    CALL EB.READLIST(CC.SEL,KEY.LIST.CC,"",SELECTED.CC,ER.MSG.CC)
*TEXT = 'SELECTED.CC=':SELECTED.CC ; CALL REM
    IF SELECTED.TT THEN
        FOR SS = 1 TO SELECTED.CC
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = ''
            CALL F.READ( FN.TELLER,KEY.LIST.CC<SS>, R.TELLER, F.TELLER, ETEXT.TT)
            TR.CODE   = R.TELLER<TT.TE.TRANSACTION.CODE>
            CURR.1    = R.TELLER<TT.TE.CURRENCY.1>
            CURR.2    = R.TELLER<TT.TE.CURRENCY.2>
            AMT.LCY   = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            AMT.FCY   = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CUST.ID   = R.TELLER<TT.TE.CUSTOMER.1>
            RATE      = R.TELLER<TT.TE.RATE.1>
            TRN.DAT   = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO.1 = R.TELLER<TT.TE.ACCOUNT.1>
            ACCT.NO.2 = R.TELLER<TT.TE.ACCOUNT.2>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>

            BEGIN CASE
            CASE TR.CODE EQ 23 OR TR.CODE EQ 67 OR TR.CODE EQ 58
                KEY.TO.USE.C = KEY.LIST.CC<SS> : '-W'
                DW.FLAG = 'W'
                SC.FLAG = '2'
                BR = COMP.CODE[2]
                V.ACC = CURR.2:'10000':BR:'9900':BR
                CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.CC<SS>
                R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.2
                R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                R.TT.D.W<TELL.D.W.RATE>       = RATE
                R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
**  CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                KEY.TO.USE.C = ''
                KEY.TO.USE.C = KEY.LIST.CC<SS> : '-D'
                DW.FLAG = 'D'
                SC.FLAG = '2'
                BR = COMP.CODE[2]
                V.ACC = CURR.1:'10000':BR:'9900':BR
                CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.CC<SS>
                R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.1
                R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                R.TT.D.W<TELL.D.W.RATE>       = RATE
                R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
**  CALL JOURNAL.UPDATE(KEY.TO.USE.C)

            CASE TR.CODE EQ 24 OR TR.CODE EQ 68
                KEY.TO.USE.C = KEY.LIST.CC<SS> : '-W'
                DW.FLAG = 'W'
                SC.FLAG = '2'
                BR = COMP.CODE[2]
                V.ACC = CURR.1:'10000':BR:'9900':BR
                CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.TT<TTI>
                R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.1
                R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                R.TT.D.W<TELL.D.W.RATE>       = RATE
                R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
**  CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                KEY.TO.USE.C = ''
                KEY.TO.USE.C = KEY.LIST.CC<SS> : '-D'
                DW.FLAG = 'D'
                SC.FLAG = '2'
                BR = COMP.CODE[2]
                V.ACC = CURR.2:'10000':BR:'9900':BR
                CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.TT<TTI>
                R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.2
                R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                R.TT.D.W<TELL.D.W.RATE>       = RATE
                R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
** CALL JOURNAL.UPDATE(KEY.TO.USE.C)
            END CASE
        NEXT SS
    END
***#######FT SELECTION#####################
**UPDATED BY NESSREEN AHMED 5/3/2018*****************
**    FT.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE IN(ACIE ACIO ACCI ACBI ACIB ACIL ACCO ACOC)  "
      FT.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE IN(ACIE ACIO ACCI ACBI ACIB ACIL ACCO ACOC ACCS ACBO)  "
**END OF UPDATE 5/3/2018*********************************
**************************************************
    KEY.LIST.FT=""
    SELECTED.FT=""
    ER.MSG.FT=""

    CALL EB.READLIST(FT.SEL,KEY.LIST.FT,"",SELECTED.FT,ER.MSG.FT)
    TEXT = 'SELECTED.FT=':SELECTED.FT ; CALL REM
    IF SELECTED.FT THEN
        FOR FF = 1 TO SELECTED.FT
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = '' ; TYPE.CL = '' ; DAT.1 = '' ; NXWD = ''; TYPE.CL = '' ; RATE = ''

            CALL F.READ( FN.FT,KEY.LIST.FT<FF>, R.FT, F.FT, ETEXT.FT)
            TR.CODE   = R.FT<FT.TRANSACTION.TYPE>
            CURR.1    = R.FT<FT.DEBIT.CURRENCY>
            CURR.2    = R.FT<FT.CREDIT.CURRENCY>
            IF CURR.1 = 'EGP' THEN
                AMT.LCY =  R.FT<FT.DEBIT.AMOUNT>
                AMT.FCY = 0
            END ELSE
                AMT.LCY   = R.FT<FT.LOC.AMT.DEBITED>
                AMT.FCY   = R.FT<FT.DEBIT.AMOUNT>
                DAT.1   = TODAY
                DAT.M   = DAT.1[5,2]
                DAT.ID  = 'EG0010001'
                CALL DBR ('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,DAT.ID,NXWD)
                NXWD.M  = NXWD[5,2]
                IF DAT.M = NXWD.M THEN
                    CALL F.READ(FN.CURR,CURR.1,R.CURR,F.CURR,ERR2)
                    LOCATE '1' IN R.CURR<EB.CUR.CURRENCY.MARKET,1> SETTING NN THEN
                        MID.REV.RATE = R.CURR<EB.CUR.MID.REVAL.RATE, NN>
                        RATE = MID.REV.RATE
                    END
                END ELSE
                    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
                    LOCATE CURR.1 IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS THEN
                        RATE = R.CCY<RE.BCP.RATE,POS>
                    END
                END
*CUST.ID   = R.FT<TT.TE.CUSTOMER.1>
            END
*  RATE    = R.FT<TT.TE.RATE.1>
            TRN.DAT   = R.FT<FT.PROCESSING.DATE>
            ACCT.NO.1 = R.FT<FT.DEBIT.ACCT.NO>
            ACCT.NO.2 = R.FT<FT.CREDIT.ACCT.NO>
            COMP.CODE = R.FT<FT.CO.CODE>

            BEGIN CASE
            CASE TR.CODE EQ 'ACIE'
                TYPE.CL = 'INCB'
                DW.FLAG = 'D'
**UPDATED BY NESSREEN AHMED 5/3/2018*****************
**          CASE TR.CODE EQ 'ACIO' OR TR.CODE EQ 'ACCI'
            CASE TR.CODE EQ 'ACIO' OR TR.CODE EQ 'ACCI' OR TR.CODE EQ 'ACCS'
**END OF UPDATE 5/3/2018*********************************
                TYPE.CL = 'INOB'
                DW.FLAG = 'D'
            CASE TR.CODE EQ 'ACBI'
                TYPE.CL = 'INOT'
                DW.FLAG = 'D'
            CASE TR.CODE EQ 'ACIB'
                TYPE.CL = 'OTCB'
                DW.FLAG = 'W'
            CASE TR.CODE EQ 'ACIL' OR TR.CODE EQ 'ACCO'
                TYPE.CL = 'OTOB'
                DW.FLAG = 'W'
**UPDATED BY NESSREEN AHMED 5/3/2018*****************
        **  CASE TR.CODE EQ 'ACOC'
            CASE TR.CODE EQ 'ACOC' OR TR.CODE EQ 'ACBO'
**END OF UPDATE 5/3/2018*********************************
                TYPE.CL = 'OTOT'
                DW.FLAG = 'W'
            END CASE

            KEY.TO.USE.FT = KEY.LIST.FT<FF>
            BR = COMP.CODE[2]
* V.ACC = CURR.2:'10000':BR:'9900':BR
            CALL F.READ( FN.TT.D.W,KEY.TO.USE.FT, R.TT.D.W, F.TT.D.W, ERR1)
            R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
            R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.FT<FF>
            R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.1
            R.TT.D.W<TELL.D.W.AMT.LCY>     = AMT.LCY
            R.TT.D.W<TELL.D.W.AMT.FCY>     = AMT.FCY
            R.TT.D.W<TELL.D.W.RATE>        = RATE
            R.TT.D.W<TELL.D.W.NEW.SECTOR>  = ''
            R.TT.D.W<TELL.D.W.SEC.FLAG>    = ''
            R.TT.D.W<TELL.D.W.ACCOUNT.1>   = ACCT.NO.1
            R.TT.D.W<TELL.D.W.ACCOUNT.2>   = ACCT.NO.2
* R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
            R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
            R.TT.D.W<TELL.D.W.COMP.CO>     = COMP.CODE
            R.TT.D.W<TELL.D.W.TRANS.TYPE>  = TR.CODE
            R.TT.D.W<TELL.D.W.TR.TYPE.CL>  = TYPE.CL
            CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.FT,R.TT.D.W)
            CALL JOURNAL.UPDATE(KEY.TO.USE.FT)
        NEXT FF
    END   ;*End of SELECTED.FT
**###############END OF FT SELECTION#####################
    RETURN
END
