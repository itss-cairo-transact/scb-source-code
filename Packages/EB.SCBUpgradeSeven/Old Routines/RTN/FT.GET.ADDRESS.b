* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOOO--------------------------------------
    SUBROUTINE FT.GET.ADDRESS(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION

    BEGIN CASE
    CASE ARG[1,2] EQ 'PL'
        VV=LEN(ARG) - 2
        XX=ARG[3,VV]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
        ARG = CATTT1
    CASE ARG[1,3] EQ 'EGP'
        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ARG,ACC)
        ARG = ACC
    CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP'
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ARG,CUS)
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,MYLOCAL)
        CALL DBR('CUSTOMER':@FM:EB.CUS.STREET,CUS,NAMEENG)
        ADR    = MYLOCAL<1,CULR.ARABIC.ADDRESS>
        ADR1   = MYLOCAL<1,CULR.ARABIC.ADDRESS><1,1>
        ADR2   = MYLOCAL<1,CULR.ARABIC.ADDRESS><1,2>
        FINDSTR '���' IN ADR SETTING FMS,VMS THEN
            ARG = '���� ��������� ������'
        END ELSE
            ARG = ADR1:" ":ADR2
        END
        IF ADR EQ '' THEN
            ARG = NAMEENG
        END

        CUST.ADDRESS1= MYLOCAL<1,CULR.GOVERNORATE>
        CUST.ADDRESS2= MYLOCAL<1,CULR.REGION>
        CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION<2,2>,CUST.ADDRESS1,CUST.ADD2)
        CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION<2,2>,CUST.ADDRESS2,CUST.ADD1)
        IF CUST.ADDRESS1 = 98 THEN
            CUST.ADD1 = ''
        END
        IF CUST.ADDRESS2 = 998 THEN
            CUST.ADD2 = ''
        END
        IF CUST.ADDRESS1 = 999 THEN
            CUST.ADD1 = ''
        END
        IF CUST.ADDRESS2 = 999 THEN
            CUST.ADD2 = ''
        END


    END CASE
    RETURN
END
