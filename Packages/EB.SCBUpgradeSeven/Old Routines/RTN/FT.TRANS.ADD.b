* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* CREATED BY RIHAM YOUSSEF 3/10/2016
    PROGRAM FT.TRANS.ADD
*-----------------------------------------------------------------------------
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.TRANS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    FN.HW   = 'F.SCB.FT.TRANS'    ; F.HW   = '' ; R.HW  = ''
    CALL OPF(FN.HW,F.HW)

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.FT   = 'FBNK.FUNDS.TRANSFER$HIS'    ; F.FT   = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    COMP = ID.COMPANY

    KEY.LIST  = "" ; SELECTED  = "" ; ETEXT.R  = ""
    KEY.LIST2 = "" ; SELECTED2 = "" ; ETEXT.R2 = ""

    TD1 = TODAY
    CALL CDT('',TD1,'-1W')
    TD2 = TODAY
    CALL CDT('',TD2,'-1W')


* T.SEL = "SELECT ":FN.FT:" WITH TRANSACTION.TYPE EQ 'AC' AND VERSION.NAME IN (,SCB.AC4 ,SCB.AC.FCY ,SCB.AC.CURR) AND PROCESSING.DATE GE ":TD1:" AND PROCESSING.DATE LE ":TD2:" AND RECORD.STATUS EQ 'MAT' AND CREDIT.CURRENCY NE 'EGP' BY @ID BY DATE.TIME "
  T.SEL = "SELECT ":FN.FT:" WITH PROCESSING.DATE GE ":TD1:" AND PROCESSING.DATE LE ":TD2:" AND TRANSACTION.TYPE EQ 'AC' AND (DEBIT.CURRENCY NE EGP OR CREDIT.CURRENCY NE EGP) AND DEBIT.CUSTOMER AND CREDIT.CUSTOMER AND DEBIT.CUSTOMER NE CREDIT.CUSTOMER BY @ID BY DATE.TIME"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<Z>,R.FT,F.FT,ETEXT.R)
            REF           = KEY.LIST<Z>[1,12]
            FT.TYPE       = R.FT<FT.TRANSACTION.TYPE>
            PROC.DATE     = R.FT<FT.PROCESSING.DATE>
******************DEBIT FIELD**************************************
            DEB.ACC       = R.FT<FT.DEBIT.ACCT.NO>
            DEB.CUR       = R.FT<FT.DEBIT.CURRENCY>
            COM.TYPE      = R.FT<FT.COMMISSION.CODE>
            TOT.CHRG      = R.FT<FT.TOTAL.CHARGE.AMOUNT>[4,10]
            COM.CR        = 'CREDIT LESS CHARGES'
            AMOUNT.DEBITED   = R.FT<FT.AMOUNT.DEBITED>[4,10]
            AMOUNT.CREDITED  = R.FT<FT.AMOUNT.CREDITED>[4,10]
            DEB.AMT          = AMOUNT.DEBITED
            CRE.AMT          = AMOUNT.CREDITED
            DEB.DATE      = R.FT<FT.DEBIT.VALUE.DATE>
            DEB.CUST.NO   = R.FT<FT.DEBIT.ACCT.NO>[1,8]
            DEB.THER.REF  = R.FT<FT.DEBIT.THEIR.REF>
            DEB.NOTE      = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
**************************CREDIT FIELD******************************
            CRE.ACC       = R.FT<FT.CREDIT.ACCT.NO>
            CRE.CUR       = R.FT<FT.CREDIT.CURRENCY>
            CRE.DATE      = R.FT<FT.CREDIT.VALUE.DATE>
            CRE.CUST.NO   = R.FT<FT.CREDIT.ACCT.NO>[1,8]
            CRE.THER.REF  = R.FT<FT.CREDIT.THEIR.REF>
            CRE.NOTE      = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
            REC.STATUS    = R.FT<FT.RECORD.STATUS>
            CO.CODE       = R.FT<FT.CO.CODE>
            INPT          = R.FT<FT.INPUTTER>
            AUTH          = R.FT<FT.AUTHORISER>
*********************************************************************

            R.HW<TR.OUR.REF>                  = REF
            R.HW<TR.TRANSACTION.TYPE>         = FT.TYPE
            R.HW<TR.PROCESSING.DATE>          = PROC.DATE
            R.HW<TR.DEBIT.ACCT.NO>            = DEB.ACC
            R.HW<TR.DEBIT.CURRENCY>           = DEB.CUR
            R.HW<TR.DEBIT.AMOUNT>             = DEB.AMT
            R.HW<TR.DEBIT.VALUE.DATE>         = DEB.DATE
            R.HW<TR.DEBIT.CUSTOMER>           = DEB.CUST.NO
            R.HW<TR.DEBIT.THEIR.REF>          = DEB.THER.REF
            R.HW<TR.DEBIT.NOTE>               = DEB.NOTE

            R.HW<TR.CREDIT.CUSTOMER>          = CRE.CUST.NO
            R.HW<TR.CREDIT.ACCT.NO>           = CRE.ACC
            R.HW<TR.CREDIT.CURRENCY>          = CRE.CUR
            R.HW<TR.CREDIT.AMOUNT>            = CRE.AMT
            R.HW<TR.CREDIT.VALUE.DATE>        = CRE.DATE
            R.HW<TR.CREDIT.THEIR.REF>         = CRE.THER.REF
            R.HW<TR.CREDIT.NOTE>              = CRE.NOTE

            R.HW<TR.REC.STATUS>               = REC.STATUS
            R.HW<TR.COMPANY>                  = CO.CODE
            R.HW<TR.INPUTTER>                 = INPT
            R.HW<TR.AUTHORISER>               = AUTH
            R.HW<TR.REF.NO>                   = '1'
            IDD     = REF:"-":DEB.CUR:"-":CRE.CUST.NO
            CALL F.WRITE (FN.HW, IDD, R.HW)
            CALL JOURNAL.UPDATE(IDD)

        NEXT Z
    END
    RETURN
END
