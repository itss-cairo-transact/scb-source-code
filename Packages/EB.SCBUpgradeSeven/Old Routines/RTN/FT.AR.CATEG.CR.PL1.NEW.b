* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
    SUBROUTINE FT.AR.CATEG.CR.PL1.NEW(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    CATEG=R.NEW(FT.CREDIT.ACCT.NO)
    BEGIN CASE
*  CASE CATEG = R.NEW(FT.DEBIT.ACCT.NO)
*      CATT   = CATEG [11,4]
*      CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
*      ARG    = CATTT
    CASE CATEG[1,2] EQ 'PL'
*VV  = LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 2
        XX  = R.NEW(FT.CREDIT.ACCT.NO)[3,10]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT)
        ARG = CATTT
    CASE CATEG[1,3] EQ 'EGP'
        XX1 =R.NEW(FT.CREDIT.ACCT.NO)[4,5]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX1,CATNAME)
        TEXT = "YES " : CATNAME ;  CALL REM
        ARG=CATNAME
    CASE CATEG NE 'PL' OR CATEG NE 'EGP'
        CATT   = CATEG[11,4]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTTT)
        TEXT = "NAME " : CATTT ; CALL REM
        ARG    = CATTTT
*   TEXT = "YES " ; CALL REM


    END CASE
    RETURN
END
