* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TIMEDATE(TIMU)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER


*A Routine to select only the Date or Time from Date.Time field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

    CALL DBR('FUNDS.TRANSFER':@FM:FT.DATE.TIME,TIMU,DAT.TIME)
*TEXT = "DATTIME=":DAT.TIME ; CALL REM
*TEXT = "TIMU=":TIMU ; CALL REM
    TIM = DAT.TIME[7,4]
*TEXT = "TIM=":TIM ; CALL REM
    TIMHS = FMT(TIM,"R##:##")
    HH = TIMHS[4,2]
    SS = TIMHS[1,2]
    TIMU = SS:":":HH

    RETURN
END
