* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------------------------AHMED ELNAHRAWY (FOR EVER)-----------------------------------------
    SUBROUTINE GET.ADDRESS.AHMED.LD2(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION

    ARG = R.NEW(LD.CUSTOMER.ID)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,ARG,MYLOCAL)
   ******* ADR = MYLOCAL<1,CULR.ARABIC.ADDRESS,1>
* ARG = ADR<1,1>
    CUST.ADDRESS1= MYLOCAL<1,CULR.GOVERNORATE>
    *****TEXT = "GOV = ":CUST.ADDRESS1 ; CALL REM
    CUST.ADDRESS2= MYLOCAL<1,CULR.REGION>
    ****TEXT = "REG = ": CUST.ADDRESS2 ; CALL REM
    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION<1,1>,CUST.ADDRESS1,CUST.ADD2)
    ****TEXT = "GOV = ":CUST.ADD2 ; CALL REM
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION<1,1>,CUST.ADDRESS2,CUST.ADD1)
    *****TEXT = "REG = ": CUST.ADD1 ; CALL REM

    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END

    ARG  = CUST.ADD2 :' ': CUST.ADD1
    ****TEXT = "ARG.LD2 :" : ARG ; CALL REM
    RETURN
END
