* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
************REHAM YOUSSIF 20150510*****************
    SUBROUTINE FT.CUSTOMS
***************************************************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
**********************************************
INITIATE:
*-------
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    ACCT.ID = '9949990010321101'
    TD = TODAY
    CALL CDT("",TD,'-1W')

    FROM.DATE = TD
    END.DATE  = TODAY
    AMT.TOTAL = ''

    RETURN
**********************************************
CALLDB:
*--------
    FN.ACC = "FBNK.ACCOUNT"           ; F.ACC  = '' ; R.ACC  = '' ; CALL OPF(FN.ACC,F.ACC)
    FN.FT  = "FBNK.FUNDS.TRANSFER$HIS"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
*    FN.FT  = "FBNK.FUNDS.TRANSFER"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
    RETURN
**********************************************
PROCESS:
*-------
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.V.DATE = R.STE<AC.STE.VALUE.DATE>
        STE.REF    = R.STE<AC.STE.OUR.REFERENCE>
        COMP.ID    = R.STE<AC.STE.COMPANY.CODE>
        IF STE.V.DATE = TODAY  THEN
            FT.ID = STE.REF
            CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
            DEBIT.ACCT       = R.FT<FT.DEBIT.ACCT.NO>
            DEBIT.CURR       = R.FT<FT.DEBIT.CURRENCY>
            FT.CODE          = R.FT<FT.CO.CODE>
            IF COMP EQ FT.CODE THEN
                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,DEBIT.CURR,CRR)
                DEBIT.AMT        = R.FT<FT.AMOUNT.DEBITED>
                DEBIT.THEIR.REF  = R.FT<FT.DEBIT.THEIR.REF>
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACCT,CUSS)
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
                CUST.NAME        = LOC.REF<1,CULR.ARABIC.NAME>
                AMT.TOTAL        += DEBIT.AMT

                XX1 = SPACE(132)
                XX1<1,1>[1,16]   = DEBIT.ACCT
                XX1<1,1>[20,40]  = CUST.NAME
                XX1<1,1>[60,10]  = DEBIT.AMT
                XX1<1,1>[80,10]  = CRR
                XX1<1,1>[100,16]  = FT.ID
                PRINT XX1<1,1>
                PRINT STR(' ',132)

                DEBIT.ACCT = ''
                CUST.NAME  = ''
                DEBIT.AMT  = ''
                CRR        = ''
                FT.ID      = ''

            END
        END
    REPEAT

    XX2 = SPACE(132)
    XX2<1,1>[25,20] = '������ ������'
    XX2<1,1>[60,10] = AMT.TOTAL
    XX2<1,1>[80,10] = '���� ����'

    PRINT STR('=',132)
    PRINT XX2<1,1>
    PRINT STR('=',132)



    RETURN

*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    COMP = ID.COMPANY
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'FT.CUSTOMS'
    PR.HD :="'L'":" "
    WS.DAT = TODAY
    WS.DAT = FMT(WS.DAT,"####/##/##")
    PR.HD :="'L'":SPACE(45):"������� ������� ������� �� ����� ������ ��   : ":WS.DAT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(15):"��� ������":SPACE(25):"������":SPACE(15):"������": SPACE(15):"��� �������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
END
