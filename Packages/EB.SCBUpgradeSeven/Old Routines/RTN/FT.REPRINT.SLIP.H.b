* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>72</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.REPRINT.SLIP.H
*    PROGRAM FT.REPRINT.SLIP.H
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*=============================================================================*


*Line [ 42 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN

        CALL TXTINP('Enter Transaction Reference', 8, 23, '14', 'ANY')
        IF COMI THEN
            TXN.ID = '' ; TXN.ID = COMI ; ER.MSG = '' ; ETEXT = ''
            BEGIN CASE
            CASE TXN.ID[1,2] = 'FT'
                GOSUB SAVE.PARAMETERS
                GOSUB PROCESS.FT.RECORD

                IF NOT(ER.MSG) THEN
                    GOSUB REPRINT.SLIP
                    TEXT = 'Done' ; CALL REM
                END
                GOSUB RESTORE.PARAMETERS
            CASE OTHERWISE
                ER.MSG = 'Invalid Transaction Reference'
            END CASE

            IF ER.MSG THEN
                TEXT = ER.MSG ; CALL REM
            END

        END
    END

    GOTO PROGRAM.END
*=============================================================================*
SAVE.PARAMETERS:
    SAVE.FUNCTION    = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW      = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS  = '' ; SIZE    = ''
    TXN.NO     = '' ; PROCESS = ''
    NBG.CODE   = '' ; RECEIPT = ''
    PARM.ID    = ''
    RETURN
*=============================================================================*
RESTORE.PARAMETERS:
    MAT R.NEW   = MAT R.SAVE
    V$FUNCTION  = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW      = SAVE.ID.NEW
    RETURN
*===================== PROCESS TELLER RECORD =================================*
PROCESS.FT.RECORD:
    IF TXN.ID[3,5] = R.DATES(EB.DAT.JULIAN.DATE)[5] THEN
        FN.FT = "FBNK.FUNDS.TRANSFER" ; F.FT = ""
        CALL OPF(FN.FT,F.FT)
        VF.FUNDS.TRANSFER = '' ; CALL OPF('FBNK.FUNDS.TRANSFER', VF.FUNDS.TRANSFER)
        ETEXT = ''
        DIM APP.REC(FT.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(FT.AUDIT.DATE.TIME)  ; MAT R.SAVE  = ''
        MAT R.SAVE = MAT R.NEW
        SIZE       = FT.AUDIT.DATE.TIME

        CALL F.MATREAD('FBNK.FUNDS.TRANSFER',TXN.ID,MAT APP.REC,SIZE,VF.FUNDS.TRANSFER,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END ELSE
            APPLICATION = 'FUNDS.TRANSFER' ; ID.NEW = COMI ; V$FUNCTION = 'I'
            VER.NAME = "FUNDS.TRANSFER":R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>

            GOSUB GET.DSF.NAMES.ARRAY
        END
    END ELSE
        VF.FUNDS.TRANSFER$HIS = ''
        CALL OPF('FBNK.FUNDS.TRANSFER$HIS', VF.FUNDS.TRANSFER$HIS)

        ETEXT = '' ; DIM APP.REC(FT.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(FT.AUDIT.DATE.TIME) ; MAT R.SAVE = ''
        SIZE = FT.AUDIT.DATE.TIME
        CALL F.MATREAD('FBNK.FUNDS.TRANSFER$HIS',TXN.ID,MAT APP.REC,SIZE,VF.FUNDS.TRANSFER$HIS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END ELSE
            IF R.NEW(FT.RECORD.STATUS) # 'REVE' THEN
                APPLICATION = 'FUNDS.TRANSFER' ; ID.NEW = COMI ; V$FUNCTION = 'I'
                VER.NAME = 'FUNDS.TRANSFER':R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>
*                VER.NAME = "FUNDS.TRANSFER,SCB.STMT.CHRG.PAY"

                PARM.ID  = 'FUNDS.TRANSFER'
                PARM.ID := R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>
                UPDATE.REC = 'F':R.COMPANY(EB.COM.MNEMONIC):'.FUNDS.TRANSFER$HIS'
                UPDATE.TS = 'YES'
                GOSUB GET.DSF.NAMES.ARRAY
            END ELSE
                ER.MSG = 'INVALID RECORD STATUS FOR REPRINT'
            END
        END
    END
    RETURN
*=============================================================================*
REPRINT.SLIP:
    IF DSF.CNT >= 1 THEN
        FOR DSF.LP = 1 TO DSF.CNT
            FOR J=1 TO 10
*            CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP>))
                CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP,J>))
            NEXT J
        NEXT DSF.LP
    END ELSE
        ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
    END

    RETURN
*=============================================================================*
GET.DSF.NAMES.ARRAY:
    IF VER.NAME THEN
        R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 160 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
        IF NOT(ETEXT) THEN
            DSF.CNT = DCOUNT(R.DSF,@VM)
        END ELSE
            ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
        END
    END

    RETURN
*=================== SUBROUTINE END ... YES !!! ==============================*
PROGRAM.END:
    RETURN
END
