* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.AR.CATEG.PL.CR1(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    CATEG=R.NEW(FT.CREDIT.ACCT.NO)
    IF CATEG = R.NEW(FT.CREDIT.ACCT.NO) THEN
        CATT = CATEG [11,4]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
        ARG  = CATTT
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,2] EQ 'PL'  THEN
        VV=LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 2
        XX=R.NEW(FT.CREDIT.ACCT.NO)[3,VV]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT5)
        ARG= CATTT5
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'EGP' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG,CATTTT)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATTTT,CATNAME)
        ARG= CATNAME
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'EUR' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG,CATTTT11)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATTTT11,CATNAME1)
        ARG= CATNAME1
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'USD' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG,CATTTT12)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATTTT12,CATNAME2)
        ARG= CATNAME2
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'GBP' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG,CATTTT13)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATTTT13,CATNAME3)
        ARG= CATNAME3
    END

    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'SAR' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG,CATTTT14)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATTTT14,CATNAME4)
        ARG= CATNAME4
    END

    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'JPY' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG,CATTTT15)
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATTTT15,CATNAME5)
        ARG= CATNAME5
    END
    RETURN
END
