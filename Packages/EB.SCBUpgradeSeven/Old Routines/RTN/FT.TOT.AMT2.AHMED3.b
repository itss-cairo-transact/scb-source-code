* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------AHMED ELNAHRAWY (FOR EVER)------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-23</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TOT.AMT2.AHMED3(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*    FT.AMT = R.NEW(FT.DEBIT.AMOUNT,1)
*    FT.COMM = R.NEW(FT.COMMISION.AMT)<1,1>
*    FT.POS1 = R.NEW(FT.CHARGE.AMT)<1,1>
*    FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
*    FT.FEES =FT.AMT+ FT.COMM + FT.POS1 +FT.POS2

    AMT=R.NEW(FT.AMOUNT.DEBITED)[4,20]
    COM.TYPE = R.NEW(FT.COMMISSION.CODE)
    CH.TYPE  = R.NEW(FT.CHARGE.CODE)

    IF R.NEW(FT.DEBIT.ACCT.NO) NE '9943330010508001'  THEN
        XX= R.NEW(FT.AMOUNT.DEBITED)
        VV= LEN(R.NEW(FT.AMOUNT.DEBITED)) - 3
        BB = R.NEW(FT.AMOUNT.DEBITED)[4,VV]
*    TEXT = " BB = " : BB ; CALL REM
*FT.AMT = R.NEW(FT.AMOUNT.DEBITED,1)
        FT.AMT  = BB

        XX1 = R.NEW(FT.COMMISSION.AMT)<1,1>
        VV1 = LEN (R.NEW(FT.COMMISSION.AMT)<1,1>) - 3
        BB1 = R.NEW(FT.COMMISSION.AMT)<1,1>[4,VV1]
*   TEXT = " BB1 = " : BB1 ; CALL REM
        FT.COMM = BB1

        XX2  = R.NEW(FT.CHARGE.AMT)<1,1>
        VV2  = LEN (R.NEW(FT.CHARGE.AMT)<1,1>) - 3
        BB2  = R.NEW(FT.CHARGE.AMT)<1,1>[4,VV2]
*  TEXT = " BB2 = " : BB2 ; CALL REM
        FT.POS1 = BB2

        XX3  = R.NEW(FT.CHARGE.AMT)<1,2>
        VV3  = LEN (R.NEW(FT.CHARGE.AMT)<1,2>) - 3
        BB3  = R.NEW(FT.CHARGE.AMT)<1,2>[4,VV3]
*   TEXT = " BB3 = " : BB3 ; CALL REM
        FT.POS2 = BB3

* FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
        FT.FEES =FT.AMT+ FT.COMM + FT.POS1 +FT.POS2
 *   FT.FEES =FT.AMT
*****UPDATE NI7OOOO (20110725)***********
        IF COM.TYPE EQ 'CREDIT LESS CHARGES' THEN
            ARG = FT.AMT
        END
        IF COM.TYPE EQ 'DEBIT PLUS CHARGES' THEN
            ARG = FT.FEES
        END
        IF COM.TYPE EQ 'WAIVE' THEN
            ARG = FT.AMT
        END
        IF COM.TYPE EQ 'WAIVE' AND CH.TYPE EQ 'DEBIT PLUS CHARGES' THEN
            ARG = FT.FEES
        END

***************END UPDATE*********************

    END ELSE
        ARG = AMT
    END

    CUR = R.NEW(FT.DEBIT.CURRENCY)
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)

    IN.AMOUNT=ARG
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"
*    TEXT = 'OUT = ' :OUT.AMOUNT ; CALL REM

    RETURN
END
