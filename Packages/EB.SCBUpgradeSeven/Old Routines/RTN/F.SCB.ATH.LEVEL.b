* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE F.SCB.ATH.LEVEL(WS.APP.ID,WS.LCY.AMT,WS.MSG.LEVEL)
************************************************************************
***********by Mohamed Sabry ** 19/9/2011 *****************************
************************************************************************
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TRANS.AUTH.LEVEL

************************************************************************
* this routine check Authoriser level Depending on the Administrative Instructions 9/2008
************************************************************************

******* UPDATE BY MOHAMED SABRY 2012/05/15  **********************

    FN.TRL = 'F.SCB.TRANS.AUTH.LEVEL' ; F.TRL = '' ; R.TRL = '' ; ER.TRL = ''
    CALL OPF(FN.TRL,F.TRL)

    *TEXT = WS.APP.ID ; CALL REM

    WS.LCY.AMT = ABS(WS.LCY.AMT)

    CALL F.READ(FN.TRL,WS.APP.ID,R.TRL,F.TRL,E.TRL1)
    IF E.TRL1 THEN
        CALL F.READ(FN.TRL,'SYSTEM',R.TRL,F.TRL,E.TRL2)
    END

*Line [ 45 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.LEVEL.COUNT = DCOUNT(R.TRL<TRL.OVERRIDE.MSG>,@VM)

    FOR I.LVL = 1 TO WS.LEVEL.COUNT
        WS.FROM.AMT       = R.TRL<TRL.LEVEL.FROM,I.LVL>
        WS.TO.AMT         = R.TRL<TRL.LEVEL.TO,I.LVL>
        WS.OVERRIDE.MSG   = R.TRL<TRL.OVERRIDE.MSG,I.LVL>

        IF WS.LCY.AMT GT WS.FROM.AMT AND WS.LCY.AMT LE WS.TO.AMT THEN
            WS.MSG.LEVEL = WS.OVERRIDE.MSG
            WS.LCY.AMT = 0
            RETURN
        END
    NEXT I.LVL
******* END OF UPDATE BY MOHAMED SABRY 2012/05/15  **********************

    RETURN

******* STOPED BY MOHAMED SABRY 2012/05/15         **********************
*    IF ( WS.LCY.AMT GT 0 AND WS.LCY.AMT LE 150000 ) THEN
*        WS.MSG.LEVEL = "You need authorise from the first level"
*        RETURN
*    END
*    IF ( WS.LCY.AMT GT 150000 AND WS.LCY.AMT LE 500000 ) THEN
*        WS.MSG.LEVEL = "You need authorise from the second level"
*        RETURN
*    END
*    IF ( WS.LCY.AMT GT 500000 AND WS.LCY.AMT LE 1000000 ) THEN
*        WS.MSG.LEVEL = "You need authorise from the third level"
*        RETURN
*    END
*    IF ( WS.LCY.AMT GT 1000000 ) THEN
*        WS.MSG.LEVEL = "You need authorise from the fourth level"
*        RETURN
*    END
*    WS.LCY.AMT = 0
******* END OF STOPED BY MOHAMED SABRY 2012/05/15 ********************
