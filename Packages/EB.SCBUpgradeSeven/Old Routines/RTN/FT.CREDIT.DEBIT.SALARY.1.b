* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>36</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.CREDIT.DEBIT.SALARY.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

*---------------------------------------------------------------------

    GOSUB INITIATE
    IF DATA.FLAG EQ 1 THEN
        RETURN
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT.CREATED "   ; CALL REM
    RETURN

*-----------------------------INITIALIZATIONS------------------------
INITIATE:

    REPORT.ID = 'FT.CREDIT.DEBIT.SALARY.1'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.ST = 'FBNK.STMT.ENTRY' ; F.ST = '' ; R.ST = ''
    CALL OPF( FN.ST,F.ST)

    ETEXT     = ''
    ETEXT1    = ''
    ETEXTM    = ''
    ETEXTM1   = ''
    I         = 1
    II        = 1
    K         = 1
    J         = 1
    FLAG.PR = 0
    DATA.FLAG = 0
    YTEXT = "Enter Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    STR.DATE = COMI
    IF  STR.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter Start date"; CALL REM
        RETURN
    END
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    END.DATE = COMI
    IF END.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END

    GOSUB PRINT.HEAD
    T.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (1407 1408 1413 1445 1455) BY @ID"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ETEXT)

*****************************
    IF SELECTED THEN
        GOSUB PROCESS
    END
    ELSE
        FLAG.PR = 1
    END

    IF FLAG.PR = 1 THEN
        TEXT = "�� ���� �����";CALL REM
        XX4 = SPACE(132)
        XX4 = ''
        PRINT XX4<1,1>
        XX4<1,1>[50,50]  = "�� ���� �����"
        PRINT XX4<1,1>
    END


    RETURN

*------------------------SELECT ALL CUSTOMERS ----------------------
PROCESS:
    OLD.1    = ''
    OLD.2    = ''
    OLD.3    = ''
    OLD.ID   = ''
    OLD.BIRTH = ''
    OLD.BR    = ''
    OLD.NAME  = ''
    NO.PRINT  = 0
    ERR       = ''
    SEL.LIST  = ''
    I         = 0
    OPENING.BAL = ''

    FOR I = 1 TO SELECTED

        CALL F.READ( FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT1)
        Y.CUS.ACC.ID   = KEY.LIST<I>
        CUS.ID         = R.AC<AC.CUSTOMER>

        CALL EB.ACCT.ENTRY.LIST(Y.CUS.ACC.ID<1>,STR.DATE,END.DATE,SEL.LIST,OPENING.BAL,ER)
        LOOP
            REMOVE REC.ID FROM SEL.LIST SETTING SUBVAL
        WHILE REC.ID:SUBVAL
**TEXT ="REC= ":REC.ID; CALL REM
            CALL F.READ(FN.ST,REC.ID,R.ST,F.ST,ERR.STMT)
****************
            IF ERR.STMT EQ '' THEN
                ST.CAT   = R.ST<AC.STE.PRODUCT.CATEGORY>
                ST.AMT   = R.ST<AC.STE.AMOUNT.LCY>
                ST.ID    = R.ST<AC.STE.OUR.REFERENCE>
** TEXT = "CAT= ":ST.CAT;CALL REM
** TEXT = "AMT= ":ST.AMT;CALL REM
                IF ST.AMT LT 0 THEN
                    ST.TYPE = '���'
                END
                ELSE
                    ST.TYPE = '�����'
                END
                IF CUS.ID[1,1] EQ 0 THEN
                    CUS.ID = CUS.ID[2,7]
                END

                CALL F.READ( FN.CUS,CUS.ID, R.CUS, F.CUS, ETEXT)
                NAME1      = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                DEPT.CODE  = R.CUS<EB.CUS.DEPT.CODE>
                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.CODE,BR.NAME)
                BR.NAME.N  = FIELD(BR.NAME,'.',2)
                GOSUB WRITE
            END
        REPEAT

    NEXT I


    RETURN

*******************************************************

WRITE:

    XX3 = SPACE(132)
    XX3 = ''
    PRINT XX3<1,1>
    XX3<1,1>[1,15]   = BR.NAME.N
    XX3<1,1>[15,10]  = CUS.ID
    XX3<1,1>[25,35]  = NAME1
    XX3<1,1>[65,5]   = ST.CAT
    XX3<1,1>[75,12]  = ST.ID
    XX3<1,1>[92,10]  = ST.AMT
    XX3<1,1>[110,10] = ST.TYPE


    PRINT XX3<1,1>

    RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"FT.CREDIT.DEBIT.SALARY.1"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"����� ��� ���� ��� �� ����� ����� ��������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"�� :":SPACE(2):STR.DATE:SPACE(2):"��� :":END.DATE
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�����":SPACE(10):"��� ������":SPACE(10):"��� ������":SPACE(18):"�������":SPACE(5):"��� ������":SPACE(5):"������":SPACE(10):"��� ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD

    RETURN

************************************

END
