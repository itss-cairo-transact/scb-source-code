* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*----------------------------------NI7OOOOOOOOOOOOOOO-------------------------------------------
    SUBROUTINE FUNDS.WARED.GR(ENQ)
***  PROGRAM FUNDS.WARED
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
    TEXT = COMP ; CALL REM
    FN.FT='FBNK.FUNDS.TRANSFER$HIS'
    F.FT=''
    CALL OPF(FN.FT,F.FT)
    TD = TODAY
* CALL CDT('',TD,"-1W")
* TD1= TD[1,6]
* TD2= TD1:"..."
    SELECTED = ""
    KEY.LIST = ""
*TEXT = TD2 ; CALL REM

    LOCATE "PROCESSING.DATE" IN ENQ<2,1> SETTING FT.POS THEN
        DEBIT.DATE = ENQ<4,FT.POS>
    END
    XXX = FIELD(DEBIT.DATE,' ',1)
    YYY = FIELD(DEBIT.DATE,' ',2)
    TEXT = XXX ; CALL REM
    TEXT = YYY ; CALL REM

    T.SEL="SELECT FBNK.FUNDS.TRANSFER$HIS WITH PROCESSING.DATE GE " :XXX: " AND PROCESSING.DATE LE " :YYY: " AND TRANSACTION.TYPE LIKE ...IT... AND CO.CODE EQ " : COMP : " AND RECORD.STATUS NE 'REVE' AND (DEBIT.CURRENCY EQ 'EGP' OR CREDIT.CURRENCY EQ 'EGP') BY PROCESSING.DATE "

*****  T.SEL="SELECT FBNK.FUNDS.TRANSFER$HIS WITH PROCESSING.DATE LIKE 200909... AND TRANSACTION.TYPE LIKE ...IT... AND CO.CODE EQ EG0010005 AND (DEBIT.CURRENCY NE 'EGP' OR CREDIT.CURRENCY NE 'EGP') BY PROCESSING.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ZZ = KEY.LIST<ENQ.LP>[1,12]:';2'
            CALL F.READ( FN.FT,ZZ, R.FT, F.FT, ETEXT.R)
            IF ETEXT.R THEN

                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
            END
        NEXT ENQ.LP
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
**        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
