* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TOT.CHRG(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
    ****************
 TOT.COMM = ""
     COMM.AMT = R.NEW(FT.COMMISSION.AMT)
*Line [ 30 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
     CC = DCOUNT(COMM.AMT,@VM)
     FOR J = 1 TO CC
         TOT.COMM = TOT.COMM + COMM.AMT<1,J>[4,13]
         TEXT = TOT.COMM ; CALL REM
     NEXT J
     AR = AR + TOT.COMM

    ****************
    TOT.CHRG = ""
    CHRG.AMT = R.NEW(FT.CHARGE.AMT)
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CG= DCOUNT(CHRG.AMT,@VM)
    FOR I = 1 TO CG
        TOT.CHRG = TOT.CHRG + CHRG.AMT<1,I>[4,13]
    NEXT I
    AG = AG + TOT.CHRG
    ARG = AR + AG

    RETURN
END
