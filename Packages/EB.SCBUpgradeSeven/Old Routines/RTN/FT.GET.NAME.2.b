* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.GET.NAME.2(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    BEGIN CASE

    CASE ARG[1,2] EQ 'PL'
        VV=LEN(ARG) - 2
        XX=ARG[3,VV]
        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
        ARG  = CATTT1
    CASE ARG[1,3] EQ 'EGP'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC)
        ARG=ACC
    CASE ARG[1,3] EQ 'EUR'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC11)
        ARG=ACC11
    CASE ARG[1,3] EQ 'USD'
        CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC12)
        ARG=ACC12
*************REKO********************
**  CASE ARG[1,2] EQ '99'
***  CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG.ACC13)
***   ARG=ACC13
    CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP'
        CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS)
        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
        NAME = LOCAL<1,CULR.ARABIC.NAME.2>
        ARG  = NAME
    END CASE
    RETURN
END
