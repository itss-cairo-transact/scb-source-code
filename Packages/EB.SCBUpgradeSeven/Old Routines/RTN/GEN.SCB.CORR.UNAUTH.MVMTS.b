* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE GEN.SCB.CORR.UNAUTH.MVMTS
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY


    F.ACCT = 'F.ACCOUNT'; FN.ACCT = ''
    CALL OPF(F.ACCT, FN.ACCT)

    F.ECB = 'F.EB.CONTRACT.BALANCES'; FN.ECB = ''
    CALL OPF(F.ECB, FN.ECB)
    MNE.ID = F.ACCT[2,3]

    F.HOLD = 'F.ENTRY.HOLD'
    FN.HOLD = ''
    CALL OPF(F.HOLD, FN.HOLD)

    F.ACCT = 'F.ACCOUNT'
    FN.ACC = ''
    CALL OPF(F.ACCT,FN.ACC)

    F.FWD.HOLD = 'F.FWD.ENTRY.HOLD'
    FN.FWD.HOLD = ''
    CALL OPF(F.FWD.HOLD, FN.FWD.HOLD)
    F.SE = ''

    OPEN "&SAVEDLISTS&" TO F.SE ELSE PRINT 'CANNOT OPEN SAVEDLISTS'


    SEL1.CMD = ''
    SEL1.CMD = 'GET.LIST GEN.SCB.WBAL.LIST'
    SEL.LIST = ''
    NO.OF.PROBS = ''
    ERR1 = ''
    CALL EB.READLIST(SEL1.CMD, SEL.LIST, '', NO.OF.PROBS, ERR1)


    PRINT 'ECB CORR STARTED FOR THE ACCOUNTS':SEL.LIST

    LOOP
        REMOVE TEMP.ID FROM SEL.LIST SETTING POS
        ACC.ID = TEMP.ID
    WHILE ACC.ID:POS
        R.ECB = ''; R.ACCT = ''
        READU R.ACCT FROM FN.ACCT, ACC.ID ELSE R.ACCT = ''
        READU R.ECB FROM FN.ECB, ACC.ID ELSE R.ECB = ''
        CR.AMT = 0; DR.AMT = 0; FWD.CR.AMT = 0; FWD.DR.AMT = 0; WRITE.FLAG = 0
        IF R.ACCT AND R.ECB THEN
            GOSUB CALCULATE.UANU.AMTS
            GOSUB COMPARE.AND.CORRECT
        END
        RELEASE FN.ACCT, ACC.ID
        RELEASE FN.ECB, ACC.ID
    REPEAT


    RETURN

********************
COMPARE.AND.CORRECT:
********************

    IF R.ECB<ECB.TOT.UNAUTH.CR> + 0 NE CR.AMT THEN
        WRITE.FLAG = 1
        CRT 'For account ':ACC.ID:' TOT.UNAUTH.CR not correct. Old value = ':R.ECB<ECB.TOT.UNAUTH.CR>:' After correction = ':CR.AMT
        R.ECB<ECB.TOT.UNAUTH.CR> = CR.AMT
    END
    IF R.ECB<ECB.TOT.UNAUTH.DB> + 0 NE DR.AMT THEN
        WRITE.FLAG = 1
        CRT 'For account ':ACC.ID:' TOT.UNAUTH.DB not correct. Old value = ':R.ECB<ECB.TOT.UNAUTH.DB>:' After correction = ':DR.AMT
        R.ECB<ECB.TOT.UNAUTH.DB> = DR.AMT
    END
    IF R.ECB<ECB.TOT.FWD.UNAU.CR> + 0 NE FWD.CR.AMT THEN
        WRITE.FLAG = 1
        CRT 'For account ':ACC.ID:' TOT.FWD.UNAU.CR not correct. Old value = ':R.ECB<ECB.TOT.FWD.UNAU.CR>:' After correction = ':FWD.CR.AMT
        R.ECB<ECB.TOT.FWD.UNAU.CR> = FWD.CR.AMT
    END
    IF R.ECB<ECB.TOT.FWD.UNAU.DB> + 0 NE FWD.DR.AMT THEN
        WRITE.FLAG = 1
        CRT 'For account ':ACC.ID:' TOT.FWD.UNAU.DB not correct. Old value = ':R.ECB<ECB.TOT.FWD.UNAU.DB>:' After correction = ':FWD.DR.AMT
        R.ECB<ECB.TOT.FWD.UNAU.DB> = FWD.DR.AMT
    END
    IF WRITE.FLAG EQ 1 THEN
        WRITE R.ECB TO FN.ECB, ACC.ID
    END

    RETURN

********************
CALCULATE.UANU.AMTS:
********************

    HOLD.KEYS = R.ECB<ECB.UNAUTH.KEY>
    PRINT 'UNAUTH KEYS IN ECB ':HOLD.KEYS
    R.ECB<ECB.UNAUTH.KEY> = ''
    FWD.HOLD.KEYS = R.ECB<ECB.FWD.UNAUTH.KEY>
    PRINT 'FWD UNAUTH KEYS IN ECB ':FWD.HOLD.KEYS
    R.ECB<ECB.FWD.UNAUTH.KEY> = ''
    CNT = DCOUNT(HOLD.KEYS, @VM)
    F.CNT = DCOUNT(FWD.HOLD.KEYS, @VM)
    FOR I = 1 TO CNT
        HOLD.ID = HOLD.KEYS<1, I>
        R.HOLD = ''
        READ R.HOLD FROM FN.HOLD, HOLD.ID THEN
            R.ECB<ECB.UNAUTH.KEY, -1> = HOLD.ID
        END ELSE
            CRT 'Missing record ':HOLD.ID:' in ENTRY.HOLD'
            R.HOLD = ''
            WRITE.FLAG = 1
        END

        FMC = DCOUNT(R.HOLD, @FM)
        FOR K = 1 TO FMC-1
            ACC.NUM = R.HOLD<K, AC.STE.ACCOUNT.NUMBER>
            IF ACC.NUM EQ ACC.ID THEN
                ENT.AMT = R.HOLD<K, AC.STE.AMOUNT.LCY>
                IF ENT.AMT GT 0 THEN
                    CR.AMT += ENT.AMT
                END ELSE
                    DR.AMT += ENT.AMT
                END
            END
        NEXT K
    NEXT I

    FOR I = 1 TO F.CNT
        HOLD.ID = FWD.HOLD.KEYS<1, I>
        R.HOLD = ''
        READ R.HOLD FROM FN.FWD.HOLD, HOLD.ID THEN
            R.ECB<ECB.FWD.UNAUTH.KEY, -1> = HOLD.ID
        END ELSE
            CRT 'Missing record ':HOLD.ID:' in FWD.ENTRY.HOLD'
            R.HOLD = ''
            WRITE.FLAG = 1
        END

        FMC = DCOUNT(R.HOLD, @FM)
        FOR K = 1 TO FMC-1
            ACC.NUM = R.HOLD<K, AC.STE.ACCOUNT.NUMBER>
            IF ACC.NUM EQ ACC.ID THEN
                ENT.AMT = R.HOLD<K, AC.STE.AMOUNT.LCY>
                IF ENT.AMT GT 0 THEN
                    FWD.CR.AMT += ENT.AMT
                END ELSE
                    FWD.DR.AMT += ENT.AMT
                END
            END
        NEXT K
    NEXT I

    RETURN

END




