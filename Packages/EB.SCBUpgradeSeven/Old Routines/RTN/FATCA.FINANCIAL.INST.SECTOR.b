* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>636</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FATCA.FINANCIAL.INST.SECTOR
*    PROGRAM FATCA.FINANCIAL.INST.SECTOR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='FATCA.FINANCIAL.INST.SECTOR'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LMM = 'FBNK.LMM.CUSTOMER' ; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUS.POS = 'F.SCB.CUS.POS' ; F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)


    KEY.LIST  = ""  ; SELECTED = ""  ;  ER.MSG=""
    R.CU      = ""  ; R.CU1    = ""
    TOT.AMT   = 0
    COUNT.NO  = 0
    COUNT.BRN = 0
    BRN.BAL   = 0
    TOTAL.BAL = 0

    DAT = TODAY
    CALL CDT("",DAT,'-1C')
    TD = TODAY
    CALL CDT ('',TD,'-1W')

    YTEXT = "Enter Date. : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DATE.NO = COMI

    RETURN

*========================================================================
PROCESS:

*------------------ACCOUT SELECTION-------------------------
******* SELECTION WITH SPECIFIC SECTORS AND CATEGORIES *************

    T.SEL = "SELECT F.SCB.CUS.POS WITH (SECTOR IN (561 1665 1666 1667 1774 3000 3100 3200 3210 3211 3212"
    T.SEL := " 3213 3215 3220 3221 3222 3223 4511 4542 4543 4559 ))"
    T.SEL := " AND SYS.DATE EQ ":DATE.NO
*   T.SEL := " AND SYS.DATE EQ '20140227'"
    T.SEL := " BY COMPANY.BOOK BY CUSTOMER"

*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,E1)
            CUS.ID     = R.CUS.POS<CUPOS.CUSTOMER>
            AMT.LCY    = R.CUS.POS<CUPOS.LCY.AMOUNT>
            CALL F.READ(FN.CU,CUS.ID,R.CU1,F.CU,ER.CU1)
            CO.CODE    = R.CU1<EB.CUS.COMPANY.BOOK>

            IF I EQ 1 THEN
                CUS.ID1  = CUS.ID
                CO.CODE1 = CO.CODE
            END

            IF CUS.ID NE CUS.ID1 THEN
                GOSUB REPORT.WRITE
                TOT.AMT = 0
            END

            TOT.AMT  += AMT.LCY
            CUS.ID1   = CUS.ID
            CO.CODE1  = CO.CODE

        NEXT I

        IF I EQ SELECTED THEN
            CO.CODE1 = ''
            GOSUB REPORT.WRITE
        END

        YY             = ''
        PRINT YY
        YY<1,1>[1,20]  = STR('=',20)
        YY<1,1>[65,20] = STR('=',20)
        PRINT YY<1,1>
        YY             = ''
        YY<1,1>[1,40]  = "��������   ":COUNT.NO
        YY<1,1>[65,40] = "������ ������   "
        YY<1,1>[85,15] = TOTAL.BAL
        PRINT YY<1,1>
        YY             = ''
        YY<1,1>[1,20]  = STR('=',20)
        YY<1,1>[65,20] = STR('=',20)
        PRINT YY<1,1>

        YY             = ''
        PRINT YY
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END

    RETURN

*===============================================================
REPORT.WRITE:
    XX     = SPACE(132)
    XX.BRN =  SPACE(132)
    TOTAL.BAL = TOTAL.BAL + TOT.AMT
    BRN.BAL   = BRN.BAL + TOT.AMT
    COUNT.BRN = COUNT.BRN + 1

    CALL F.READ(FN.CU,CUS.ID1,R.CU,F.CU,ER.CU)
    CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

    IF CUS.NAME EQ '' THEN
        CUS.NAME = R.CU<EB.CUS.SHORT.NAME>
    END

    CO.CODE.NEW  = R.CU<EB.CUS.COMPANY.BOOK>
    CUS.SECTOR   = R.CU<EB.CUS.SECTOR>
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE.NEW,CUS.BRN)
    CALL DBR('SECTOR':@FM:EB.SEC.DESCRIPTION,CUS.SECTOR,CUS.SEC.NAME)

    COUNT.NO = COUNT.NO + 1

    XX<1,I>[1,7]    = COUNT.NO
    XX<1,I>[12,8]   = CUS.ID1
    XX<1,I>[27,35]  = CUS.NAME
    XX<1,I>[65,25]  = CUS.BRN
    XX<1,I>[85,20]  = TOT.AMT
    XX<1,I>[100,30] = CUS.SEC.NAME
    PRINT XX<1,I>

    IF CO.CODE1 NE CO.CODE THEN
        CALL EB.ROUND.AMOUNT ('USD',BRN.BAL,'',"2")
        XX.BRN            = ''
        XX.BRN<1,1>[1,10] = STR('=',20)
        XX.BRN<1,1>[65,10]= STR('=',20)
        PRINT XX.BRN
        XX.BRN            = ''
        XX.BRN<1,1>[1,40] = "������ �����   ":COUNT.BRN
        XX.BRN<1,1>[65,40]= "������ ���� �����   "
        XX.BRN<1,1>[85,15]= BRN.BAL
        PRINT XX.BRN
        XX.BRN            = ''
        XX.BRN<1,1>[1,10] = STR('=',20)
        XX.BRN<1,1>[65,10]= STR('=',20)
        PRINT XX.BRN
        COUNT.BRN = 0
        BRN.BAL   = 0
    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    SYS.DATE = DATE.NO[7,2]:'/':DATE.NO[5,2]:"/":DATE.NO[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(20):"���� �������� ������� ���� ������� ������� ������� ������� �������� ���� �������� ��������"
    PR.HD :="'L'":SPACE(45):"������ ":SYS.DATE
    PR.HD :="'L'":SPACE(30):STR('_',70)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������":SPACE(4):"��� ������":SPACE(5):"��� ������":SPACE(28):"�����":SPACE(15):"������":SPACE(8):"�������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
END
