* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    PROGRAM FIX.CUST.CATEG.DAILY

***Mahmoud Elhawary******5/12/2011*****************************
*   To fix CATEG.ENTRY record with no CUSTOMER.ID            *
**************************************************************

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS

    COMP = ID.COMPANY
*DEBUG
    GOSUB INITIATE
*Line [ 64 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
********************************************
INITIATE:
*--------
    TD1  = TODAY
    TTF1 = '20111201'
    KK = 0
    T.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ERR.MSG = ''
    CUST.ID  = ''
    CUST.ACC = ''


    FOLDER   = "&SAVEDLISTS&"
    FILENAME = "FIXED.CATEG.REC.":TD1
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    RETURN
********************************************
CALLDB:
*--------
    FN.CTE = 'FBNK.CATEG.ENTRY'    ; F.CTE = '' ; R.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.ENT.LW = 'FBNK.CATEG.ENT.LWORK.DAY'    ; F.ENT.LW = '' ; R.ENT.LW = ''
    CALL OPF(FN.ENT.LW,F.ENT.LW)
    FN.BR  = 'FBNK.BILL.REGISTER'  ; F.BR  = '' ; R.BR  = ''
    CALL OPF(FN.BR,F.BR)
    FN.FT  = 'FBNK.FUNDS.TRANSFER' ; F.FT  = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)
    FN.TT  = 'FBNK.TELLER'         ; F.TT  = '' ; R.TT  = ''
    CALL OPF(FN.TT,F.TT)
    FN.IN  = 'F.INF.MULTI.TXN'     ; F.IN  = '' ; R.IN  = ''
    CALL OPF(FN.IN,F.IN)
    FN.DOC  = 'F.SCB.DOCUMENT.PROCURE' ; F.DOC  = '' ; R.DOC  = ''
    CALL OPF(FN.DOC,F.DOC)
    FN.LD  = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD  = '' ; R.LD  = ''
    CALL OPF(FN.LD,F.LD)
    FN.LC  = 'FBNK.LETTER.OF.CREDIT' ; F.LC  = '' ; R.LC  = ''
    CALL OPF(FN.LC,F.LC)
    FN.DR  = 'F.DRAWINGS'        ; F.DR  = '' ; R.DR  = ''
    CALL OPF(FN.DR,F.DR)
    FN.BR.H  = 'FBNK.BILL.REGISTER$HIS'  ; F.BR.H  = '' ; R.BR.H  = ''
    CALL OPF(FN.BR.H,F.BR.H)
    FN.BS  = 'F.SCB.BR.SLIPS'  ; F.BS  = '' ; R.BS  = ''
    CALL OPF(FN.BS,F.BS)
    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H  = '' ; R.FT.H  = ''
    CALL OPF(FN.FT.H,F.FT.H)
    FN.TT.H  = 'FBNK.TELLER$HIS'         ; F.TT.H  = '' ; R.TT.H  = ''
    CALL OPF(FN.TT.H,F.TT.H)
    FN.IN.H  = 'F.INF.MULTI.TXN$HIS'     ; F.IN.H  = '' ; R.IN.H  = ''
    CALL OPF(FN.IN.H,F.IN.H)
    FN.LD.H  = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H  = '' ; R.LD.H  = ''
    CALL OPF(FN.LD.H,F.LD.H)
    FN.LC.H  = 'FBNK.LETTER.OF.CREDIT$HIS' ; F.LC.H  = '' ; R.LC.H  = ''
    CALL OPF(FN.LC.H,F.LC.H)
    FN.DOC.H  = 'F.SCB.DOCUMENT.PROCURE$HIS' ; F.DOC.H  = '' ; R.DOC.H  = ''
    CALL OPF(FN.DOC.H,F.DOC.H)
    FN.DR.H  = 'F.DRAWINGS$HIS'        ; F.DR.H  = '' ; R.DR.H  = ''
    CALL OPF(FN.DR.H,F.DR.H)
    RETURN
********************************************
PROCESS:
*--------
    T.SEL = "SELECT ":FN.ENT.LW
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ERR.MSG)
    CRT SELECTED
    LOOP
        REMOVE ENT.ID FROM K.LIST SETTING POS.ENT
    WHILE ENT.ID:POS.ENT
        CALL F.READ(FN.ENT.LW,ENT.ID,R.ENT.LW,F.ENT.LW,ERR1)
        LOOP
            REMOVE CTE.ID FROM R.ENT.LW SETTING POS.CTE
        WHILE CTE.ID:POS.CTE
            CALL F.READ(FN.CTE,CTE.ID,R.CTE,F.CTE,ERR2)
            CTE.CUST = R.CTE<AC.CAT.CUSTOMER.ID>
            IF CTE.CUST EQ '' THEN
                CTE.REF = R.CTE<AC.CAT.OUR.REFERENCE>
                IF CTE.REF[1,2] EQ 'BR' THEN
                    CALL F.READ(FN.BR,CTE.REF,R.BR,F.BR,ER.BR)
                    CUST.ID = R.BR<EB.BILL.REG.DRAWER>
                    IF ER.BR THEN
                        CALL F.READ.HISTORY(FN.BR.H,CTE.REF,R.BR.H,F.BR.H,ER.BR.H)
                        CUST.ID = R.BR.H<EB.BILL.REG.DRAWER>
                    END
                END
                IF CTE.REF[1,2] EQ 'FT' THEN
                    CALL F.READ(FN.FT,CTE.REF,R.FT,F.FT,ER.FT)
                    CUST.ACC = R.FT<FT.CHARGES.ACCT.NO>
                    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CUST.ACC,CUST.ID)
                    IF CUST.ID EQ '' THEN
                        CUST.ID = R.FT<FT.CHARGED.CUSTOMER>
                        IF CUST.ID EQ '' THEN
                            CUST.ID = R.FT<FT.PROFIT.CENTRE.CUST>
                        END
                    END
                    IF ER.FT THEN
                        CALL F.READ.HISTORY(FN.FT.H,CTE.REF,R.FT.H,F.FT.H,ER.FT.H)
                        CUST.ACC = R.FT.H<FT.CHARGES.ACCT.NO>
                        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CUST.ACC,CUST.ID)
                        IF CUST.ID EQ '' THEN
                            CUST.ID = R.FT.H<FT.CHARGED.CUSTOMER>
                            IF CUST.ID EQ '' THEN
                                CUST.ID = R.FT.H<FT.PROFIT.CENTRE.CUST>
                            END
                        END
                    END
                END
                IF CTE.REF[1,2] EQ 'TT' THEN
                    CALL F.READ(FN.TT,CTE.REF,R.TT,F.TT,ER.TT)
                    CUST.ID = R.TT<TT.TE.CHARGE.CUSTOMER>
                    IF ER.TT THEN
                        CALL F.READ.HISTORY(FN.TT.H,CTE.REF,R.TT.H,F.TT.H,ER.TT.H)
                        CUST.ID = R.TT.H<TT.TE.CHARGE.CUSTOMER>
                    END
                END
                IF CTE.REF[1,3] EQ 'DOC' THEN
                    CALL F.READ(FN.DOC,CTE.REF,R.DOC,F.DOC,ER.DOC)
                    CUST.ID = R.DOC<DOC.PRO.CUSTOMER.ID>
                    IF ER.DOC THEN
                        CALL F.READ.HISTORY(FN.DOC.H,CTE.REF,R.DOC.H,F.DOC.H,ER.DOC.H)
                        CUST.ID = R.DOC.H<DOC.PRO.CUSTOMER.ID>
                    END
                END
                IF CTE.REF[1,2] EQ 'BS' THEN
                    CALL F.READ(FN.BS,CTE.REF,R.BS,F.BS,RD.ERR)
                    CUST.ACC = R.BS<SCB.BS.CUST.ACCT>
                    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CUST.ACC,CUST.ID)
                END
                IF CUST.ID NE '' THEN
                    KK++
                    R.CTE<AC.CAT.CUSTOMER.ID> = CUST.ID

                    CALL F.WRITE (FN.CTE, CTE.ID, R.CTE)
                    CALL JOURNAL.UPDATE(CTE.ID)


***                    WRITE  R.CTE TO F.CTE , CTE.ID ON ERROR
***                        PRINT "CAN NOT WRITE RECORD ":CTE.ID:" TO ":FN.CTE
***                    END
*                    CRT KK:" - ":CTE.ID:" * ":CTE.REF:" * ":CUST.ID
                    CUST.ID  = ''
                    CUST.ACC = ''
                    BB.DATA = CTE.ID:",":CTE.REF:",":CUST.ID
                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            END
        REPEAT
    REPEAT
    RETURN
********************************************
END
