* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgyODE2ODY6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
    SUBROUTINE FT.CHQ.NO1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

*****


    XX     = O.DATA[1,2]
    TEXT = O.DATA ; CALL REM
    TEXT = XX     ; CALL REM

    FN.FT   = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''

    FN.BR   = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    FN.BR.H = 'FBNK.BILL.REGISTER$HIS' ; F.BR.H = '' ; R.BR.H = ''

    FN.IN   = 'F.INF.MULTI.TXN' ; F.IN= '' ; R.IN = ''

    FN.TT   = 'FBNK.TELLER'   ; F.TT = '' ; R.TT = ''

    FN.SCB  = 'F.SCB.TRANS.TODAY' ; F.SCB= '' ; R.SCB = ''

    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.FT.H,F.FT.H)
    CALL OPF( FN.BR,F.BR)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.SCB,F.SCB)

    BEGIN CASE
    CASE O.DATA[1,2] EQ 'FT'
       TEXT = O.DATA ; CALL REM
***        CALL DBR('FUNDS.TRANSFER':@FM:FT.CHEQUE.NUMBER,O.DATA,CHQ.NO)
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.CHEQUE.NUMBER,O.DATA,CHQ.NO)
F.ITSS.FUNDS.TRANSFER$HIS = 'F.FUNDS.TRANSFER$HIS'
FN.F.ITSS.FUNDS.TRANSFER$HIS = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS)
CALL F.READ(F.ITSS.FUNDS.TRANSFER$HIS,O.DATA,R.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS,ERROR.FUNDS.TRANSFER$HIS)
CHQ.NO=R.ITSS.FUNDS.TRANSFER$HIS<FT.CHEQUE.NUMBER>
        TEXT = CHQ.NO ; CALL REM
        O.DATA = CHQ.NO
    CASE XX EQ 'TT'
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('TELLER':@FM:TT.TE.CHEQUE.NUMBER,O.DATA,CHQ.NO1)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,O.DATA,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
CHQ.NO1=R.ITSS.TELLER<TT.TE.CHEQUE.NUMBER>
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('TELLER':@FM:TT.TE.SERIAL.NO,O.DATA,CHQ.NO7)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,O.DATA,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
CHQ.NO7=R.ITSS.TELLER<TT.TE.SERIAL.NO>
        IF  CHQ.NO1 NE '' THEN
            O.DATA = CHQ.NO1
        END
        IF CHQ.NO1 = '' THEN
            O.DATA = CHQ.NO7
        END
    CASE XX EQ 'IN'
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.CHEQUE.NUMBER,O.DATA,CHQ.NO2)
F.ITSS.INF.MULTI.TXN = 'F.INF.MULTI.TXN'
FN.F.ITSS.INF.MULTI.TXN = ''
CALL OPF(F.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN)
CALL F.READ(F.ITSS.INF.MULTI.TXN,O.DATA,R.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN,ERROR.INF.MULTI.TXN)
CHQ.NO2=R.ITSS.INF.MULTI.TXN<INF.MLT.CHEQUE.NUMBER>
        O.DATA = CHQ.NO2

    END CASE
END
