* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTkwMDc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-------------------------------------AHMED NAHRAWY (FOR EVER)----------------------------------------
    SUBROUTINE FT.MARKZY.DRAWING

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS

* IF R.NEW(FT.DEBIT.CURRENCY) EQ 'EGP' OR R.NEW(FT.CREDIT.CURRENCY) EQ 'EGP'  THEN
    DR.ACCOUNT= R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
    CR.ACCOUNT= R.NEW(TF.DR.PAYMENT.ACCOUNT)
    DR.CURR   = DR.ACCOUNT[9,2]
    CR.CURR   = CR.ACCOUNT[9,2]
    TEXT = "DR.CURR" : DR.CURR ; CALL REM
    TEXT = "CR.CURR" : CR.CURR ; CALL REM
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,DR.CURR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DR.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CR.CURR,CURR1)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CR.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR1=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    IF CURR NE CURR1 THEN
        IF CURR EQ 'EGP' OR CURR1 EQ 'EGP' THEN
            CALL PRODUCE.DEAL.SLIP("FT.DEBIT.TANZEL")
            CALL PRODUCE.DEAL.SLIP("FT.DEBIT.TNAZEL")
        END ELSE
            IF CURR NE 'EGP' AND CURR1 NE 'EGP' THEN
                CALL PRODUCE.DEAL.SLIP("FT.DEBIT.MOMLAT")
                CALL PRODUCE.DEAL.SLIP("FT.DEBIT.MOMLT2")
            END
        END
        RETURN
    END
