* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgzMTI3NTk6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
************REHAM YOUSSIF 20150510*****************
    SUBROUTINE FT.TAXES2.AGEND
***************************************************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 51 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
**********************************************
INITIATE:
*-------
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    ACCT.ID = '9949990010321001'
    TD = TODAY
    CALL CDT("",TD,'-1W')

    FROM.DATE = TODAY
    END.DATE  = TODAY
    AMT.TOTAL = ''
    ACC.1     = ''
    ACC.2     = ''
    ACC.3     = ''
    CUST.NAME1= ''
    CUST.NAME2= ''
    CUST.NAME3= ''
    CUR       = ''
    AMT1      = ''
    AMT2      = ''
    AMT3      = ''
    CRR       = ''
    FT.ID     = ''
    SIGN1     = ''
    SIGN2     = ''
    FT.ID     = ''
    AC.ACCOUNT.TITTLE.1 = ''
    XX1  = ''
    XX2  = ''
    XX3  = ''
    XX4  = ''
    RETURN
**********************************************
CALLDB:
*--------
    FN.ACC = "FBNK.ACCOUNT"           ; F.ACC  = '' ; R.ACC  = '' ; CALL OPF(FN.ACC,F.ACC)
    FN.FT  = "F.INF.MULTI.TXN"        ; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.STE = "FBNK.STMT.ENTRY"        ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
    RETURN
**********************************************
PROCESS:
*-------
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.V.DATE    = R.STE<AC.STE.VALUE.DATE>
        STE.B.DATE    = R.STE<AC.STE.BOOKING.DATE>
        STE.REF       = R.STE<AC.STE.OUR.REFERENCE>
        COMP.ID       = R.STE<AC.STE.COMPANY.CODE>
        IF STE.V.DATE EQ TODAY AND COMP.ID EQ 'EG0010099' THEN
            FT.ID            = STE.REF[1,12]
            CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
            ACC.1      = R.FT<INF.MLT.ACCOUNT.NUMBER><1,1>
            ACC.2      = R.FT<INF.MLT.ACCOUNT.NUMBER><1,2>
            ACC.3      = R.FT<INF.MLT.ACCOUNT.NUMBER><1,3>
            CUR        = R.FT<INF.MLT.CURRENCY><1,1>
            AMT1       = R.FT<INF.MLT.AMOUNT.LCY><1,1>
            AMT2       = R.FT<INF.MLT.AMOUNT.LCY><1,2>
            AMT3       = R.FT<INF.MLT.AMOUNT.LCY><1,3>
            SIGN1      = R.FT<INF.MLT.SIGN><1,1>
            SIGN2      = R.FT<INF.MLT.SIGN><1,2>

            FT.CODE          = R.FT<INF.MLT.CO.CODE>
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

*Line [ 132 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC.2,CUSS2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS2=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS2,LOC.REF2)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS2,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF2=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME2         = LOC.REF2<1,CULR.ARABIC.NAME>

*Line [ 148 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITTLE.1,ACC.3,CUSS3)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.3,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS3=R.ITSS.ACCOUNT<AC.ACCOUNT.TITTLE.1>
*Line [ 155 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS3,LOC.REF3)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS3,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF3=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME3         = LOC.REF3<1,CULR.ARABIC.NAME>



            XX1 = SPACE(132)
            XX1<1,1>[1,20]    = FT.ID
            XX1<1,1>[30,20]   = ACC.2
            XX1<1,1>[55,20]   = CUST.NAME2
            XX1<1,1>[80,10]   = AMT2
            XX1<1,1>[100,10]  = CRR
            PRINT XX1<1,1>
            PRINT STR(' ',132)
            XX1 = ''

            XX2 = SPACE(132)
            XX2<1,1>[30,20]   = ACC.3
            XX2<1,1>[55,20]   = FIELD(CUSS3,"-",1)
            XX2<1,1>[80,10]   = AMT3
            XX2<1,1>[100,10]  = CRR
            PRINT XX2<1,1>
            PRINT STR(' ',132)
            XX2 = ''


        END
    REPEAT
    RETURN

*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    COMP = ID.COMPANY
*Line [ 193 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'FT.TAXES.AGEND'
    PR.HD :="'L'":" "
    WS.DAT = TODAY
    WS.DAT = FMT(WS.DAT,"####/##/##")
    PR.HD :="'L'":SPACE(45):"������� ������� ������� �� ������� ������    : ":WS.DAT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(20):"��� ������":SPACE(15):"��� ������": SPACE(15):"����": SPACE(15):"������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
END
