* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTYwNjA6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.DEAL.AC5

*1-TO CALL DAEL SLIP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    CALL PRODUCE.DEAL.SLIP("FT.DB.CRD")
    *CALL PRODUCE.DEAL.SLIP("FT.DB.DBT")

***IF (R.NEW(FT.DEBIT.CURRENCY) = 'EGP') AND (R.NEW(FT.CREDIT.CURRENCY) = 'EGP') THEN
***CALL PRODUCE.DEAL.SLIP("FT.TRANS.AC")
*  CALL PRODUCE.DEAL.SLIP("FT.DB.STMP.AC")
***END ELSE
***CALL PRODUCE.DEAL.SLIP("FT.TRANS.FCY")
* CALL PRODUCE.DEAL.SLIP("FT.DB.STMP.AC")

***END
    RETURN
END
