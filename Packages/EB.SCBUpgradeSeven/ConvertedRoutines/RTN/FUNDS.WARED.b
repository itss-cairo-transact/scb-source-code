* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MjQzMjY6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*----------------------------------NI7OOOOOOOOOOOOOOO-------------------------------------------
    SUBROUTINE FUNDS.WARED(ENQ)
***  PROGRAM FUNDS.WARED
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
    TEXT = COMP ; CALL REM
    FN.FT='FBNK.FUNDS.TRANSFER$HIS'
    F.FT=''
    CALL OPF(FN.FT,F.FT)
    TD = TODAY
    CALL CDT('',TD,"-1W")
    TD1= TD[1,6]
    TD2= TD1:"..."
    SELECTED = ""
    KEY.LIST = ""
    TEXT = TD2 ; CALL REM
    T.SEL="SELECT FBNK.FUNDS.TRANSFER$HIS WITH PROCESSING.DATE LIKE " :TD2: " AND TRANSACTION.TYPE LIKE ...IT... AND CO.CODE EQ " : COMP : " AND (DEBIT.CURRENCY NE 'EGP' OR CREDIT.CURRENCY NE 'EGP') BY PROCESSING.DATE"

*****  T.SEL="SELECT FBNK.FUNDS.TRANSFER$HIS WITH PROCESSING.DATE LIKE 200909... AND TRANSACTION.TYPE LIKE ...IT... AND CO.CODE EQ EG0010005 AND (DEBIT.CURRENCY NE 'EGP' OR CREDIT.CURRENCY NE 'EGP') BY PROCESSING.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT T.SEL
    PRINT KEY.LIST
    PRINT SELECTED
    PRINT ER.MSG
*    ENQ.LP   = '' ;
*    OPER.VAL = ''
    TEXT = SELECTED ; CALL REM
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
