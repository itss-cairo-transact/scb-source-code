* @ValidationCode : Mjo5MjQ4ODY5OTI6Q3AxMjUyOjE2NDA2OTc2MzI4ODg6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
SUBROUTINE FT.AMT.DEBIT.FMT(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 27 ] Hashing I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS
*----------------------------------------------
    XX               = LEN(ARG) - 3
    IN.AMOUNT        = ARG[4,XX]
    CUR              = R.NEW(FT.DEBIT.CURRENCY)
*Line [ 34,35,36 ] Replacing by get loc ref to get the position of the LF - ITSS - R21 Upgrade - 2021-12-27
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.LANGUAGE",FTLR.LANGUAGE.POS)
    LANGUAGE.NUMBER  = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE.POS>
*LANGUAGE.NUMBER  = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE>

    IF LANGUAGE.NUMBER EQ 2 THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        ARG = OUT.AMOUNT :" ":CURR:" ":"�� ���"
    END

    IF LANGUAGE.NUMBER EQ 1 THEN
        ARG = "NO ENGLISH"
    END
*------------------------------------------------------
RETURN
END
