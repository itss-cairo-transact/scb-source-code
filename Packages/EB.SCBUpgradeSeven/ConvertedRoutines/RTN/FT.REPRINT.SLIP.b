* @ValidationCode : MjoxODUwMDMzNzkxOkNwMTI1MjoxNjQwNjk3NjQyNjQ2OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
SUBROUTINE FT.REPRINT.SLIP
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.VERSION
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 27 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*=============================================================================*
*Line [ 32 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        CALL TXTINP('Enter Transaction Reference', 8, 23, '12', 'ANY')
        IF COMI THEN
            TXN.ID = '' ; TXN.ID = COMI ; ER.MSG = '' ; ETEXT = ''
            BEGIN CASE
                CASE TXN.ID[1,2] = 'FT'
                    GOSUB SAVE.PARAMETERS
                    GOSUB PROCESS.FT.RECORD
                    IF NOT(ER.MSG) THEN
                        GOSUB REPRINT.SLIP
                    END
                    GOSUB RESTORE.PARAMETERS
                CASE OTHERWISE
                    ER.MSG = 'Invalid Transaction Reference'
            END CASE
            IF ER.MSG THEN
                TEXT = ER.MSG
                CALL REM

            END
        END
    END

    GOTO PROGRAM.END

*=============================================================================*
SAVE.PARAMETERS:
    SAVE.FUNCTION = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS = '' ; SIZE = ''
    TXN.NO = '' ; PROCESS = ''
    NBG.CODE = '' ; RECEIPT = ''
RETURN
*=============================================================================*
RESTORE.PARAMETERS:
    MAT R.NEW = MAT R.SAVE
    V$FUNCTION = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW = SAVE.ID.NEW
RETURN
*===================== PROCESS FT RECORD =================================*
PROCESS.FT.RECORD:
    IF TXN.ID[3,5] = R.DATES(EB.DAT.JULIAN.DATE)[5] THEN
        VF.FUNDS.TRANSFER = '' ; CALL OPF('F.FUNDS.TRANSFER', VF.FUNDS.TRANSFER)
        ETEXT =''
        DIM APP.REC(FT.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(FT.AUDIT.DATE.TIME)  ; MAT R.SAVE = ''
        MAT R.SAVE = MAT R.NEW
        SIZE =FT.AUDIT.DATE.TIME

        CALL F.MATREAD('F.FUNDS.TRANSFER',TXN.ID,MAT APP.REC,SIZE,VF.FUNDS.TRANSFER,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            APPLICATION = 'FUNDS.TRANSFER' ; ID.NEW = COMI ; V$FUNCTION = 'I'
*Line [ 98 --> 103] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-27
            CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.VERSION.NAME",FTLR.VERSION.NAME.POS)
            IF R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME.POS>[1,1] # ',' THEN
*IF R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>[1,1] # ',' THEN
                VER.NAME = "FUNDS.TRANSFER,":R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME.POS>
            END
            ELSE
                VER.NAME = "FUNDS.TRANSFER":R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME.POS>
            END
            GOSUB GET.DSF.NAMES.ARRAY
        END
    END

    ELSE
        VF.FUNDS.TRANSFER$HIS = '' ; CALL OPF('F.FUNDS.TRANSFER$HIS', VF.FUNDS.TRANSFER$HIS)
        ETEXT = '' ; DIM APP.REC(FT.AUDIT.DATE.TIME) ; MAT APP.REC = ''
        DIM R.SAVE(FT.AUDIT.DATE.TIME) ; MAT R.SAVE = ''
        SIZE = FT.AUDIT.DATE.TIME
        CALL F.MATREAD('F.FUNDS.TRANSFER$HIS',TXN.ID,MAT APP.REC,SIZE,VF.FUNDS.TRANSFER$HIS,ETEXT)
        MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
        IF ETEXT THEN
            ER.MSG = ETEXT ; ETEXT = ''
        END
        ELSE
            IF R.NEW(FT.RECORD.STATUS) # 'REVE' THEN
                APPLICATION = 'FUNDS.TRANSFER' ; ID.NEW = COMI ; V$FUNCTION = 'I'
                PARM.ID = 'FUNDS.TRANSFER,'
                PARM.ID := R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME.POS>
                UPDATE.REC = 'F':R.COMPANY(EB.COM.MNEMONIC):'.FUNDS.TRANSFER$HIS'
                UPDATE.TS = 'YES'
            END
            ELSE
                ER.MSG = 'INVALID RECORD STATUS FOR REPRINT'
            END
        END
    END
RETURN
TEXT = "REPRINT SLIP" ; CALL REM
*=============================================================================*
REPRINT.SLIP:

    IF VER.NAME THEN
        R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 136 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
R.DSF=R.ITSS.VERSION<EB.VER.D.SLIP.FORMAT>
*    IF NOT(ETEXT) THEN
        DSF.CNT = DCOUNT(R.DSF,@VM)
*    END

*        IF DSF.CNT > 1 THEN
*------ EDIT BY NESSMA 2011/07/19 ------
        IF DSF.CNT > 0 THEN
*---------------------------------------
            FOR DSF.LP = 1 TO DSF.CNT
                FOR J=1 TO 10
                    CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP,J>))
*                CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP,2>))
                NEXT J
            NEXT DSF.LP
        END
        ELSE
            ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
        END
        RETURN
*=============================================================================*
GET.DSF.NAMES.ARRAY:
* IF VER.NAME THEN
*     R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*     CALL DBR("VERSION":FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
*     IF NOT(ETEXT) THEN
*         DSF.CNT = DCOUNT(R.DSF,@VM)
*     END
*     ELSE
*         ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
*     END
* END
PROGRAM.END:
        RETURN
    END
