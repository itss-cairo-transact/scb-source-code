* @ValidationCode : MjotNTQxMzUzNjAzOkNwMTI1MjoxNjQwNjk3NjM2OTg1OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------
** CREATE BY NESSMA
*------------------
SUBROUTINE FT.GET.MOSTAFEED(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 33 ] Hashed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS
*----------------------------------------------------------
    BENEFICIARY.CUS.EN  = R.NEW(FT.BEN.CUSTOMER)<1,1>
    BENEFICIARY.CUS.EN := " " : R.NEW(FT.BEN.CUSTOMER)<1,2>
    BENEFICIARY.CUS.EN := " " : R.NEW(FT.BEN.CUSTOMER)<1,3>
*Line [ 39 --> 51 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-27
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.BENEFICIARY.CUS",FTLR.BENEFICIARY.CUS.POS)
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.NOTE.CREDIT",FTLR.NOTE.CREDIT.POS)
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.LANGUAGE",FTLR.LANGUAGE.POS)
    
    BENEFICIARY.CUS.AR  = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS.POS,1>
    BENEFICIARY.CUS.AR := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS.POS,2>
    BENEFICIARY.CUS.AR := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS.POS,3>

    CREDIT.NOTE         = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,1>
    CREDIT.NOTE        := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,2>
    CREDIT.NOTE        := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,3>

    LANGUAGE.NUMBER     = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE.POS>
    
*    BENEFICIARY.CUS.AR  = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1>
*    BENEFICIARY.CUS.AR := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2>
*    BENEFICIARY.CUS.AR := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,3>
*
*    CREDIT.NOTE         = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,1>
*    CREDIT.NOTE        := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,2>
*    CREDIT.NOTE        := " " : R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,3>
*
*    LANGUAGE.NUMBER     = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE>

    IF LANGUAGE.NUMBER EQ '2' THEN
        ARG = BENEFICIARY.CUS.AR
    END

    IF LANGUAGE.NUMBER EQ '1' THEN
        IF BENEFICIARY.CUS.EN THEN
            ARG = BENEFICIARY.CUS.EN
        END ELSE
            IF CREDIT.NOTE THEN
                ARG = CREDIT.NOTE
            END
        END
    END
*-------------------------------------------
RETURN
END
