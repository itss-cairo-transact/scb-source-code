* @ValidationCode : MjotMTAyNDU3MTMxMTpDcDEyNTI6MTY0NTk1OTI0NjIzOTpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:54:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
** ----- NESSREEN AHMED 12/9/2011 -----
*-----------------------------------------------------------------------------
* <Rating>404</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FILL.CUSTOMER.POS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 35 ] Hashed I_TT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.POSITION
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USR.LOCAL.REF

    FN.CUS.POS  = 'F.SCB.CUSTOMER.POSITION' ; F.CUS.POS = '' ; R.CUS.POS = ''
    FN.CUST     = 'F.CUSTOMER'     ; F.CUST = '' ; R.CUST = ''
    FN.USR      = 'F.USER'         ; F.USR  = '' ; R.USR  = ''
    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF( FN.CUS.POS,F.CUS.POS)
    CALL OPF( FN.CUST,F.CUST)
    CALL OPF( FN.USR,F.USR)

    TT.SEL = "SELECT FBNK.CUSTOMER.POSITION"
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            FLAG = 0
            ID.USE = '' ; CUST.NO = '' ; CUST.CO = '' ; REF = '' ; REF.BF = ''
            USER.NO = '' USER.NO.BF = '' ; USER.DEP = '' ; USER.CO = '' ; CUST.SEC = '' ; CUST.EMP = '' ;  CUST.USR = ''
            ERR.POS = '' ; USR.LC.DEP = ''
            ID.USE = KEY.LIST.TT<TTI>
            CUST.NO   = FIELD(ID.USE, "*", 1)
            CALL F.READ( FN.CUST,CUST.NO, R.CUST, F.CUST, ERR.CUS)
            LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>
            CUST.CO  = R.CUST<EB.CUS.COMPANY.BOOK>
            CUST.SEC = R.CUST<EB.CUS.SECTOR>
            IF (CUST.SEC = 1100) OR (CUST.SEC = 1300) THEN
                CUST.EMP = LOCAL.REF<1,CULR.EMPLOEE.NO>
                CUST.USR = "SCB.":CUST.EMP
            END
            REF.BF       = FIELD(ID.USE, "**", 1)
            REF          = FIELD(REF.BF, "*" , 6)
            USER.NO.BF   = FIELD(ID.USE, "**", 2)
            USER.NO      = FIELD(USER.NO.BF, "*", 1)
            CALL F.READ( FN.USR,USER.NO, R.USR, F.USR, ERR.USR)
            USER.DEP  = R.USR<EB.USE.DEPARTMENT.CODE>
            LOCAL.REF.USR = R.USR<EB.USE.LOCAL.REF>
            USR.LC.DEP = LOCAL.REF.USR<1,USER.SCB.DEPT.CODE>
            IF LEN(USER.DEP)= 2 THEN
                USER.CO   = "EG00100":USER.DEP
            END ELSE
                USER.CO   = "EG001000":USER.DEP
            END
            IF (USER.CO # CUST.CO) AND (USER.NO # CUST.USR) THEN
***  IF USER.NO # "SCB.5592" THEN
                IF LOCAL.REF.USR NE 2700 THEN
                    FLAG = 1
                END
            END
            KEY.TO.USE = CUST.NO:"*":USER.NO:"*":TODAY
            CALL F.READ( FN.CUS.POS,KEY.TO.USE, R.CUS.POS, F.CUS.POS, ERR.POS)
            IF ERR.POS THEN
                IF FLAG = 1 THEN
                    R.CUS.POS<CUSP.PROCESS.DATE> = TODAY
                    R.CUS.POS<CUSP.CUSTOMER>     = CUST.NO
                    R.CUS.POS<CUSP.CUST.CO>      = CUST.CO
                    R.CUS.POS<CUSP.CUST.SEC>     = CUST.SEC
                    R.CUS.POS<CUSP.CUST.EMP.NO>  = CUST.EMP
                    R.CUS.POS<CUSP.USER>         = USER.NO
                    R.CUS.POS<CUSP.USER.CO>      = USER.CO
                    R.CUS.POS<CUSP.REF>          = REF
                    CALL F.WRITE(FN.CUS.POS,KEY.TO.USE,R.CUS.POS)
                    CALL JOURNAL.UPDATE(KEY.TO.USE)
                END
            END
        NEXT TTI
    END
RETURN
END
