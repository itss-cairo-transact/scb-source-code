* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MjQxMjY6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*----------------------------------NI7OOOOOOOOOOOOOOO-------------------------------------------
    SUBROUTINE FUNDS.SADER1.YEAR(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    FN.FT='FBNK.FUNDS.TRANSFER$HIS'
    F.FT=''
    CALL OPF(FN.FT,F.FT)
    TD = TODAY
    CALL CDT('',TD,"-1W")
    TD1= TD[1,6]
    TD2= TD1:"..."

    LOCATE "PROCESSING.DATE" IN ENQ<2,1> SETTING FT.POS THEN
        DEBIT.DATE = ENQ<4,FT.POS>
    END
    XXX = FIELD(DEBIT.DATE,' ',1)
    YYY = FIELD(DEBIT.DATE,' ',2)
    TEXT = XXX ; CALL REM
    TEXT = YYY ; CALL REM

    LOCATE "DEBIT.ACCT.NO" IN ENQ<2,1> SETTING FT.POS THEN
        ACCT.NO = ENQ<4,FT.POS>
    END

    TEXT = ACCT.NO ; CALL REM

    T.SEL="SELECT FBNK.FUNDS.TRANSFER$HIS WITH PROCESSING.DATE GE ":XXX:" AND PROCESSING.DATE LE ":YYY:" AND TRANSACTION.TYPE LIKE ...OT... AND CO.CODE EQ " : COMP : " AND (DEBIT.CURRENCY NE 'EGP' OR CREDIT.CURRENCY NE 'EGP') AND (DEBIT.ACCT.NO EQ ":ACCT.NO:" OR CREDIT.ACCT.NO EQ ":ACCT.NO:" ) BY PROCESSING.DATE "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SELECTED : " : SELECTED ; CALL REM

    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
