* @ValidationCode : MjotMjExMjM0MTA4NDpDcDEyNTI6MTY0NTk1ODI3NDQ0MDpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:37:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
** ----- NESSREEN AHMED 07/02/2011 -----
*-----------------------------------------------------------------------------
* <Rating>1217</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FILL.TELLER.D.W

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 35 ] Hashing I_TT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TELLER.D.W
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    FN.TT.D.W = 'F.SCB.TELLER.D.W' ; F.TT.D.W = '' ; R.TT.D.W = ''
    FN.TELLER = 'F.TELLER'     ; F.TELLER = '' ; R.TELLER = ''
    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF( FN.TT.D.W,F.TT.D.W)
    CALL OPF( FN.TELLER,F.TELLER)
    TT.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN(9 62 53 42 66 37 38 7 32 33 71 101 102 901 910 2 54 41 40 86 84 82 8 36 34 25 100 20 63 21 17 11 12 92 14 97 64 105 95 107 98 65 106 108 96 912 81)  "
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
*TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = ''
            CALL F.READ( FN.TELLER,KEY.LIST.TT<TTI>, R.TELLER, F.TELLER, ETEXT.TT)
            TR.CODE   = R.TELLER<TT.TE.TRANSACTION.CODE>
            CURR      = R.TELLER<TT.TE.CURRENCY.1>
            AMT.LCY   = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            AMT.FCY   = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CUST.ID   = R.TELLER<TT.TE.CUSTOMER.1>
            RATE      = R.TELLER<TT.TE.RATE.1>
            TRN.DAT   = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO.1 = R.TELLER<TT.TE.ACCOUNT.1>
            ACCT.NO.2 = R.TELLER<TT.TE.ACCOUNT.2>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>

            BEGIN CASE
                CASE TR.CODE EQ 2 OR TR.CODE EQ 54 OR TR.CODE EQ 41 OR TR.CODE EQ 40 OR TR.CODE EQ 86 OR TR.CODE EQ 84 OR TR.CODE EQ 82 OR TR.CODE EQ 8 OR TR.CODE EQ 36 OR TR.CODE EQ 34 OR TR.CODE EQ 25 OR TR.CODE EQ 100 OR TR.CODE EQ 20 OR TR.CODE EQ 63 OR TR.CODE EQ 21 OR TR.CODE EQ 17 OR TR.CODE EQ 11 OR TR.CODE EQ 12 OR TR.CODE EQ 92 OR TR.CODE EQ 14 OR TR.CODE EQ 97 OR TR.CODE EQ 64 OR TR.CODE EQ 105 OR TR.CODE EQ 95 OR TR.CODE EQ 107 OR TR.CODE EQ 98 OR TR.CODE EQ 65 OR TR.CODE EQ 106 OR TR.CODE EQ 108 OR TR.CODE EQ 96 OR TR.CODE EQ 912 OR TR.CODE EQ 81
                    DW.FLAG = 'W'
                CASE TR.CODE EQ 9 OR TR.CODE EQ 62 OR TR.CODE EQ 53 OR TR.CODE EQ 42 OR TR.CODE EQ 66 OR TR.CODE EQ 37 OR TR.CODE EQ 38 OR TR.CODE EQ 7 OR TR.CODE EQ 32 OR TR.CODE EQ 33 OR TR.CODE EQ 71 OR TR.CODE EQ 101 OR TR.CODE EQ 102 OR TR.CODE EQ 901 OR TR.CODE EQ 910
                    DW.FLAG = 'D'
            END CASE

            IF CUST.ID # '' THEN
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
                IF NEW.SEC = '4650' THEN
                    SC.FLAG = '2'
                END ELSE
                    SC.FLAG = '1'
                END
            END ELSE
                SC.FLAG = '1'
            END
            BR = COMP.CODE[2]
            V.ACC = CURR:'10000':BR:'9900':BR
            KEY.TO.USE = KEY.LIST.TT<TTI>
            CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
            R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
            R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.TT<TTI>
            R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR
            R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
            R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
            R.TT.D.W<TELL.D.W.RATE>       = RATE
            R.TT.D.W<TELL.D.W.NEW.SECTOR> = NEW.SEC
            R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
            R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
            R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
            R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
            R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
            R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

            CALL F.WRITE(FN.TT.D.W,KEY.TO.USE,R.TT.D.W)
** CALL JOURNAL.UPDATE(KEY.TO.USE)
        NEXT TTI
    END
*****FOR BUY AND SELL **********************************
    CC.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN(23 67 58 24 68)  "
**************************************************
    KEY.LIST.CC=""
    SELECTED.CC=""
    ER.MSG.CC=""

    CALL EB.READLIST(CC.SEL,KEY.LIST.CC,"",SELECTED.CC,ER.MSG.CC)
*TEXT = 'SELECTED.CC=':SELECTED.CC ; CALL REM
    IF SELECTED.TT THEN
        FOR SS = 1 TO SELECTED.CC
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = ''
            CALL F.READ( FN.TELLER,KEY.LIST.CC<SS>, R.TELLER, F.TELLER, ETEXT.TT)
            TR.CODE   = R.TELLER<TT.TE.TRANSACTION.CODE>
            CURR.1    = R.TELLER<TT.TE.CURRENCY.1>
            CURR.2    = R.TELLER<TT.TE.CURRENCY.2>
            AMT.LCY   = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            AMT.FCY   = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CUST.ID   = R.TELLER<TT.TE.CUSTOMER.1>
            RATE      = R.TELLER<TT.TE.RATE.1>
            TRN.DAT   = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO.1 = R.TELLER<TT.TE.ACCOUNT.1>
            ACCT.NO.2 = R.TELLER<TT.TE.ACCOUNT.2>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>

            BEGIN CASE
                CASE TR.CODE EQ 23 OR TR.CODE EQ 67 OR TR.CODE EQ 58
                    KEY.TO.USE.C = KEY.LIST.CC<SS> : '-W'
                    DW.FLAG = 'W'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    V.ACC = CURR.2:'10000':BR:'9900':BR
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.CC<SS>
                    R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.2
                    R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                    R.TT.D.W<TELL.D.W.RATE>       = RATE
                    R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                    R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                    R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
**  CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                    KEY.TO.USE.C = ''
                    KEY.TO.USE.C = KEY.LIST.CC<SS> : '-D'
                    DW.FLAG = 'D'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    V.ACC = CURR.1:'10000':BR:'9900':BR
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.CC<SS>
                    R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.1
                    R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                    R.TT.D.W<TELL.D.W.RATE>       = RATE
                    R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                    R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                    R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
**  CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                CASE TR.CODE EQ 24 OR TR.CODE EQ 68
                    KEY.TO.USE.C = KEY.LIST.CC<SS> : '-W'
                    DW.FLAG = 'W'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    V.ACC = CURR.1:'10000':BR:'9900':BR
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.TT<TTI>
                    R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.1
                    R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                    R.TT.D.W<TELL.D.W.RATE>       = RATE
                    R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                    R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                    R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
**  CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                    KEY.TO.USE.C = ''
                    KEY.TO.USE.C = KEY.LIST.CC<SS> : '-D'
                    DW.FLAG = 'D'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    V.ACC = CURR.2:'10000':BR:'9900':BR
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<TELL.D.W.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<TELL.D.W.ID.REF>     = KEY.LIST.TT<TTI>
                    R.TT.D.W<TELL.D.W.CARD.CURRENCY> = CURR.2
                    R.TT.D.W<TELL.D.W.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<TELL.D.W.AMT.FCY>    = AMT.FCY
                    R.TT.D.W<TELL.D.W.RATE>       = RATE
                    R.TT.D.W<TELL.D.W.NEW.SECTOR> = ''
                    R.TT.D.W<TELL.D.W.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<TELL.D.W.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<TELL.D.W.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<TELL.D.W.VAULT.AC.NO> = V.ACC
                    R.TT.D.W<TELL.D.W.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<TELL.D.W.COMP.CO>    = COMP.CODE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
** CALL JOURNAL.UPDATE(KEY.TO.USE.C)
            END CASE
        NEXT SS
    END
RETURN
END
