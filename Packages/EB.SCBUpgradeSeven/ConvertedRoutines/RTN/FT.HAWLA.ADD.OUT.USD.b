* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgyOTE1NTI6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* CREATED BY RIHAM YOUSSEF 4/9/2014
    PROGRAM FT.HAWLA.ADD.OUT.USD
*-----------------------------------------------------------------------------
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.TRANS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    FN.HW   = 'F.SCB.FT.TRANS'    ; F.HW   = '' ; R.HW  = ''
    CALL OPF(FN.HW,F.HW)

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.FT   = 'FBNK.FUNDS.TRANSFER$HIS'    ; F.FT   = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)
    COMP = ID.COMPANY

*    TD1 = '20160901'
    TD1 = TODAY
    CALL CDT('',TD1,'-1W')
    TD2 = TODAY
    CALL CDT('',TD2,'-1W')


    KEY.LIST  = "" ; SELECTED  = "" ; ETEXT.R  = ""
    KEY.LIST2 = "" ; SELECTED2 = "" ; ETEXT.R2 = ""
    T.SEL="SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE IN (OT OT03 OT05 OT40) AND RECORD.STATUS EQ 'MAT' AND CREDIT.CURRENCY NE 'EGP' AND PROCESSING.DATE GE ":TD1:" AND PROCESSING.DATE LE ":TD2:" BY @ID BY DATE.TIME"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<Z>,R.FT,F.FT,ETEXT.R)
            REF           = KEY.LIST<Z>[1,12]
            FT.TYPE       = R.FT<FT.TRANSACTION.TYPE>
            PROC.DATE     = R.FT<FT.PROCESSING.DATE>
******************DEBIT FIELD**************************************
            DEB.ACC       = R.FT<FT.DEBIT.ACCT.NO>
            DEB.CUR       = R.FT<FT.DEBIT.CURRENCY>
            CRE.CUR       = R.FT<FT.CREDIT.CURRENCY>

            AMOUNT.DEBITED   = R.FT<FT.AMOUNT.DEBITED>[4,10]
            AMOUNT.CREDITED  = R.FT<FT.AMOUNT.CREDITED>[4,10]
            TOT.CHR          = R.FT<FT.TOT.REC.CHG.CRCCY>
            DEB.AMT          = AMOUNT.CREDITED - TOT.CHR
            CRE.AMT          = AMOUNT.CREDITED - TOT.CHR

            DEB.CUR          = R.FT<FT.DEBIT.CURRENCY>
            CRE.CUR          = R.FT<FT.CREDIT.CURRENCY>

            DEB.DATE      = R.FT<FT.DEBIT.VALUE.DATE>
            DEB.CUST.NO   = R.FT<FT.DEBIT.ACCT.NO>[1,8]
            DEB.THER.REF  = R.FT<FT.DEBIT.THEIR.REF>
            DEB.NOTE      = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
**************************CREDIT FIELD******************************
            CRE.ACC       = R.FT<FT.CREDIT.ACCT.NO>
            CRE.DATE      = R.FT<FT.CREDIT.VALUE.DATE>
            CRE.CUST.NO   = R.FT<FT.CREDIT.ACCT.NO>[1,8]
            CRE.THER.REF  = R.FT<FT.CREDIT.THEIR.REF>
            CRE.NOTE      = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
            REC.STATUS    = R.FT<FT.RECORD.STATUS>
            CO.CODE       = R.FT<FT.CO.CODE>
            INPT          = R.FT<FT.INPUTTER>
            AUTH          = R.FT<FT.AUTHORISER>
*********************************************************************
            R.HW<TR.OUR.REF>                  = REF
            R.HW<TR.TRANSACTION.TYPE>         = FT.TYPE
            R.HW<TR.PROCESSING.DATE>          = PROC.DATE
            R.HW<TR.DEBIT.ACCT.NO>            = DEB.ACC
            R.HW<TR.DEBIT.CURRENCY>           = DEB.CUR
            R.HW<TR.DEBIT.AMOUNT>             = DEB.AMT
            R.HW<TR.DEBIT.VALUE.DATE>         = DEB.DATE
            R.HW<TR.DEBIT.CUSTOMER>           = DEB.CUST.NO
            R.HW<TR.DEBIT.THEIR.REF>          = DEB.THER.REF
            R.HW<TR.DEBIT.NOTE>               = DEB.NOTE

            R.HW<TR.CREDIT.CUSTOMER>          = CRE.CUST.NO
            R.HW<TR.CREDIT.ACCT.NO>           = CRE.ACC
            R.HW<TR.CREDIT.CURRENCY>          = CRE.CUR
            R.HW<TR.CREDIT.AMOUNT>            = CRE.AMT
            R.HW<TR.CREDIT.VALUE.DATE>        = CRE.DATE
            R.HW<TR.CREDIT.THEIR.REF>         = CRE.THER.REF
            R.HW<TR.CREDIT.NOTE>              = CRE.NOTE

            R.HW<TR.REC.STATUS>               = REC.STATUS
            R.HW<TR.COMPANY>                  = CO.CODE
            R.HW<TR.INPUTTER>                 = INPT
            R.HW<TR.AUTHORISER>               = AUTH
            R.HW<TR.REF.NO>                   = '3'
            IDD     = REF:"-":CRE.CUR:"-":DEB.CUST.NO

            CALL F.WRITE (FN.HW, IDD, R.HW)
            CALL JOURNAL.UPDATE(IDD)

        NEXT Z
    END
    RETURN
END
