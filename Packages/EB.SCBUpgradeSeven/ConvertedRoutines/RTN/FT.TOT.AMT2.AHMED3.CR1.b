* @ValidationCode : Mjo1NzQ2MjIwMzM6Q3AxMjUyOjE2NDA2OTc2NDU0NzA6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------- AHMED ELNAHRAWY (FOR EVER) -------------------------------
SUBROUTINE FT.TOT.AMT2.AHMED3.CR1(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 28 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS
*-------------------------------------------------------------------
    ARG.1 = ""
    AMT   = R.NEW(FT.AMOUNT.CREDITED)[4,20]
    IF R.NEW(FT.CREDIT.ACCT.NO) NE '9943330010508001'  THEN
        XX = R.NEW(FT.AMOUNT.CREDITED)
        VV = LEN(R.NEW(FT.AMOUNT.CREDITED)) - 3
        BB = R.NEW(FT.AMOUNT.CREDITED)[4,VV]
        FT.AMT  = BB

        XX1 = R.NEW(FT.COMMISSION.AMT)<1,1>
        VV1 = LEN (R.NEW(FT.COMMISSION.AMT)<1,1>) - 3
        BB1 = R.NEW(FT.COMMISSION.AMT)<1,1>[4,VV1]
        FT.COMM = BB1

        XX2  = R.NEW(FT.CHARGE.AMT)<1,1>
        VV2  = LEN (R.NEW(FT.CHARGE.AMT)<1,1>) - 3
        BB2  = R.NEW(FT.CHARGE.AMT)<1,1>[4,VV2]
        FT.POS1 = BB2

        XX3  = R.NEW(FT.CHARGE.AMT)<1,2>
        VV3  = LEN (R.NEW(FT.CHARGE.AMT)<1,2>) - 3
        BB3  = R.NEW(FT.CHARGE.AMT)<1,2>[4,VV3]
        FT.POS2 = BB3

        FT.FEES = FT.AMT + FT.COMM + FT.POS1 + FT.POS2
        ARG.1   = FT.FEES
    END ELSE
        ARG.1   = AMT
    END

    CUR = R.NEW(FT.CREDIT.CURRENCY) ; CURR = ""
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    IN.AMOUNT = ARG.1
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ERR.MSG)
    ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"
RETURN
END
