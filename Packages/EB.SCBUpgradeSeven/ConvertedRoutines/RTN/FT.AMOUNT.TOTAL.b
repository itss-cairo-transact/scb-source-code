* @ValidationCode : Mjo3MTA2MzIwMzY6Q3AxMjUyOjE2NDA2OTc2MzI1ODg6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------------------AHMED NAHRAWY (FOR EVER)---------------
SUBROUTINE FT.AMOUNT.TOTAL(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 28 ] Hashing I_FT.LOCAL.REFS layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_AC.LOCAL.REFS
*  DD = DCOUNT(ARG,VM)

    ACCTCH    =R.NEW(FT.CHARGES.ACCT.NO)
    DEBITACCT =R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCTCH,CUR1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCTCH,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR1=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DEBITACCT,CUR2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBITACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR2=R.ITSS.ACCOUNT<AC.CURRENCY>
    TOT    = R.NEW(FT.TOTAL.CHARGE.AMOUNT)[4,10]
    TOT1   = LEN(TOT) -3
    MMM    = LEN(ARG) -3
    IF ACCTCH NE '' THEN
        IF CUR1 EQ CUR2 THEN
            AMOUNT = ARG[4,MMM]+TOT
            ARG = AMOUNT
        END
        IF CUR1 NE CUR2 THEN
            AMOUNT = ARG[4,MMM]
            ARG = AMOUNT
        END
    END
    IF ACCTCH EQ '' THEN
        AMOUNT = ARG[4,MMM]
        ARG = AMOUNT
    END
    TEXT = ARG ; CALL REM
RETURN
END
