* @ValidationCode : MjotNzkxMTcwMTEwOkNwMTI1MjoxNjQ1OTU4Mjk5MDc5OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------------------Created by Riham youssef---------

*    PROGRAM FT.OFS.ATM.FOREX
SUBROUTINE FT.OFS.ATM.FOREX
*----------------------------------------------------
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.FOREX
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.TERMINAL.ID
*---------------------------------------

    GOSUB CHECKLIST
    IF SW = 0  THEN

        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        GOSUB PROGRAM.END

    END
RETURN
*-----------
CHECKLIST:
*---------
    FN.FT = 'FBNK.FUNDS.TRANSFER'
    F.FT  = ''
    R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    DAT.Y = TODAY
    KEY.LIST = "" ; SELECTED = "" ;  ER.FT = "" ; SW = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACEX' AND DEBIT.VALUE.DATE EQ ":TODAY
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
        SW = 1
    END ELSE
        SW = 0
    END
RETURN
*----------------------------------------
INITIALISE:
*----------
*    SCB.OFS.SOURCE  = "TESTOFS"
    SCB.OFS.SOURCE  = "OFS.FOREX"
    SCB.APPL        = "FUNDS.TRANSFER"
    SCB.VERSION     = "POS"

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,,,"

    OPENSEQ "OFS.MNGR.IN" , "FT.OFS.ATFX.IN" TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"FT.OFS.ATFX.IN"
        HUSH OFF
    END

    OPENSEQ "OFS.MNGR.IN" , "FT.OFS.ATFX.IN" TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create FT.OFS.ATFX.IN File IN OFS.MNGR.IN'
        END
    END
    OPENSEQ "OFS.MNGR.OUT" , "FT.OFS.ATFX.OUT" TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"FT.OFS.ATFX.OUT"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "FT.OFS.ATFX.OUT" TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create FT.OFS.ATFX.OUT File IN OFS.MNGR.OUT'
        END
    END


    WS.COUNT = 1
RETURN
*-----------
BUILD.RECORD:
*---------------
    FN.ATFX = 'F.SCB.ATM.FOREX' ; F.ATFX = ''
    CALL OPF(FN.ATFX,F.ATFX)

    FN.ATM.ID = 'F.SCB.ATM.TERMINAL.ID' ; F.ATM.ID = '' ;R.ATM.ID = ''
    CALL OPF(FN.ATM.ID,F.ATM.ID)

    KEY.LIST1 = "" ; SELECTED1 = "" ;  ER.FT1 = "" ; DEB.AMT = 0 ; DEB.ACCT.EGP = 0 ;DEB.AMT.EGP = 0

    T.SEL1 = "SELECT F.SCB.ATM.FOREX WITH FT.FLAG NE 'YES' BY ATM.BANK.ID BY TRANS.SEQ"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.FT1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.ATFX,KEY.LIST1<I>,R.ATFX,F.ATFX,E1)
            ATM.ID   = R.ATFX<ATFX.ATM.BANK.ID>
            TRNS.SEQ = R.ATFX<ATFX.TRANS.SEQ>
            RATE     = R.ATFX<ATFX.RATE>
            CUR      = R.ATFX<ATFX.CURRENCY>
            SET.DATE = R.ATFX<ATFX.SERVER.DATE>
*-------------------------CASE (1)-------------------- *
            DB.ACCT = '' ; CR.ACCT = ''
            DEB.AMT.EGP += R.ATFX<ATFX.COM.EQUI>
            DEB.AMT += R.ATFX<ATFX.SELL.AMT>

            ATM.ID   = R.ATFX<ATFX.ATM.BANK.ID>
            CALL F.READ(FN.ATM.ID,ATM.ID,R.ATM.ID,F.ATM.ID,E6)
            CURR.BASE = R.ATM.ID<SCB.ATM.CURRENCY>
            IF CUR EQ 'EGP' THEN
                DB.ACCT = R.ATM.ID<SCB.ATM.ACCOUNT.NUMBER>
                CR.ACCT = 'PL57049'
            END ELSE
                LOCATE CUR IN CURR.BASE<1,1> SETTING POS THEN
                    DB.ACCT = R.ATM.ID<SCB.ATM.ACCOUNT.NUMBER.FOREX><1,POS>
                END
                CR.ACCT = R.ATM.ID<SCB.ATM.ACCOUNT.NUMBER>
            END
            GOSUB OFS.MSG

            DEB.AMT.EGP   = ''
            DEB.AMT   = ''
            CUR       = ''
            CR.ACCT   = ''
            DB.ACCT   = ''
            WS.COUNT++


        NEXT I
    END
RETURN
********
OFS.MSG:
*-------

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"ACEX"
    OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":CUR
    OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
    OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
    IF CUR = 'EGP' THEN
        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":DEB.AMT.EGP
    END ELSE
        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":DEB.AMT
        OFS.MESSAGE.DATA :=  ",TREASURY.RATE::=":RATE
        OFS.MESSAGE.DATA :=  ",BASE.CURRENCY::=":CUR
    END
    OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
    OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY

    OFS.MESSAGE.DATA :=  ",LOCAL.REF:41:1=":TRNS.SEQ
    OFS.MESSAGE.DATA :=  ",LOCAL.REF:42:1=":ATM.ID
    OFS.MESSAGE.DATA :=  ",LOCAL.REF:45:1=":SET.DATE
    OFS.MESSAGE.DATA :=  ",COMMISSION.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",CHARGE.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",PROFIT.CENTRE.DEPT::=":"99"
    OFS.MESSAGE.DATA :=  ",DEBIT.THEIR.REF::=":WS.COUNT
    OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=SCB"


    SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA

    BB.IN.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END

*    CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE,SCB.OFS.MESSAGE)
    SCB.OFS.SOURCE = "SCBONLINE"
    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)


    BB.OUT.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
    END

    GOSUB UPD.REC

RETURN
************************************************************
UPD.REC:
*-------
    CHKVAL = FIELD (SCB.OFS.MESSAGE,'/', 3)
    IF CHKVAL [1, 1] = 1 THEN
        R.ATFX<ATFX.FT.FLAG> = "YES"

        CALL F.WRITE(FN.ATFX,KEY.LIST1<I>,R.ATFX)
        CALL JOURNAL.UPDATE(KEY.LIST1<I>)

        CALL F.RELEASE(FN.ATFX,KEY.LIST1<I>,F.ATFX)
        CLOSE F.ATFX
    END
RETURN
*-----------
PROGRAM.END:
*-----------

    TEXT = "�� ����� ����� �����";CALL REM
RETURN

*-------------------------------------------------------------
END
