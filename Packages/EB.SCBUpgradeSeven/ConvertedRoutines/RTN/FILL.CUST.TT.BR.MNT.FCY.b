* @ValidationCode : MjoxODQzMzY1MjgyOkNwMTI1MjoxNjQ1OTU5MjQ2MTgzOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:54:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
** ----- NESSREEN AHMED 14/5/2015 -----
*-----------------------------------------------------------------------------
* <Rating>277</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FILL.CUST.TT.BR.MNT.FCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 37 ] Hashed I_TT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUST.TT.BR.FCY.MNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS

    SHIFT ='' ; ROUND =''
*    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)
    FN.TT.BR.MNT = 'F.SCB.CUST.TT.BR.FCY.MNT' ; F.TT.BR.MNT = '' ; R.TT.BR.MNT = ''
    FN.TELLER   = 'F.TELLER$HIS'     ; F.TELLER = '' ; R.TELLER = ''
    FN.BILL.REG = 'F.BILL.REGISTER'  ; F.BILL.REG = '' ; R.BILL.REG = ''
    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''

    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''
    ETEXT.BR = '' ; YYYYMM = '' ; YYYY= '' ; MM = ''

    CALL OPF( FN.TT.BR.MNT, F.TT.BR.MNT)
    CALL OPF( FN.TELLER, F.TELLER)
    CALL OPF( FN.BILL.REG, F.BILL.REG)
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)

    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0
    CUST.NTR = 0

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
    YYYYMM = ST.DATE[1,6]
    YYYY = ST.DATE[1,4]
    MM = ST.DATE[5,2]
    TEXT = 'YYYYMM=':YYYYMM ; CALL REM
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM
    START.DATE = ST.DATE
    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)
******##########CHECK PROGRAM RUN BEFORE OR NOT ###########********************
    CHK.SEL = "SELECT F.SCB.CUST.TT.BR.FCY.MNT WITH YEAR EQ ":YYYY :" AND MONTH EQ " :MM
***********************
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    TEXT = 'SELECTED.CHK=':SELECTED.CHK ; CALL REM
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
******######################################################*******************
        TT.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CUSTOMER.1 BY AUTH.DATE "
***********************
        KEY.LIST.TT=""
        SELECTED.TT=""
        ER.MSG.TT=""

        CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
        TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
        IF SELECTED.TT THEN
            FOR TTI = 1 TO SELECTED.TT
                AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
                KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
                ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
                CUST.COMP = '' ; AMT.EQV.USD = '' ; SELL.RATE = '' ; TT.DATE = '' ; TOT.CUST.EQV = ''

                CALL F.READ( FN.TELLER, KEY.LIST.TT<TTI>, R.TELLER, F.TELLER, ETEXT.TT)
                TT.REF.NO<TTI> = FIELD(KEY.LIST.TT<TTI>,";", 1)
                CUST.ID<TTI>   = R.TELLER<TT.TE.CUSTOMER.1>
                CURR<TTI>      = R.TELLER<TT.TE.CURRENCY.1>
                AMT.LCY<TTI>   = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
                AMT.FCY<TTI>   = R.TELLER<TT.TE.AMOUNT.FCY.1>
                TRN.DAT<TTI>   = R.TELLER<TT.TE.AUTH.DATE>
                COMP.CODE<TTI> = R.TELLER<TT.TE.CO.CODE>
                TT.DATE = TRN.DAT<TTI>
                KEY.ID<TTI> = "USD":"-" :"EGP":"-":TT.DATE
                CALL F.READ(FN.CURR.DALY,KEY.ID<TTI>,R.CURR.DALY,F.CURR.DALY,ERR2)

                IF CURR<TTI> = "USD" THEN
                    AMT.EQV.USD =  AMT.FCY<TTI>
                END ELSE
                    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                        SELL.RATE<TTI> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                    END
                    AMT.EQV.USD = AMT.LCY<TTI> / SELL.RATE<TTI>
                END ;** END OF OTHER FCY**

                KEY.TO.USE  = CUST.ID<TTI>:"-":YYYYMM
                CALL F.READ( FN.TT.BR.MNT, KEY.TO.USE, R.TT.BR.MNT, F.TT.BR.MNT, ERR1)

                R.TT.BR.MNT<CUST.TR.CUST.NO>  = CUST.ID<TTI>
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR( 'CUSTOMER':@FM:EB.CUS.COMPANY.BOOK, CUST.ID<TTI>, CUST.COMP)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID<TTI>,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.COMP=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
                R.TT.BR.MNT<CUST.TR.CUST.CO>  = CUST.COMP
                R.TT.BR.MNT<CUST.TR.YEAR>     = YYYY
                R.TT.BR.MNT<CUST.TR.MONTH>    = MM

                CUST.REF.N = R.TT.BR.MNT<CUST.TR.REF.NO>
*Line [ 138 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(CUST.REF.N,@VM)
                XX = DD+1
                R.TT.BR.MNT<CUST.TR.REF.NO,XX>     = TT.REF.NO<TTI>
                R.TT.BR.MNT<CUST.TR.TRN.CURR,XX>   = CURR<TTI>
                R.TT.BR.MNT<CUST.TR.AMT.FCY,XX>    = AMT.FCY<TTI>

                CALL EB.ROUND.AMOUNT ('USD', AMT.EQV.USD,'',"2")
                R.TT.BR.MNT<CUST.TR.AMT.EQVUSD,XX> =  AMT.EQV.USD

                R.TT.BR.MNT<CUST.TR.TRN.DATE,XX>   = TRN.DAT<TTI>

                TOT.CUST.EQV = R.TT.BR.MNT<CUST.TR.TOT.EQVAMT>
                R.TT.BR.MNT<CUST.TR.TOT.EQVAMT> = TOT.CUST.EQV + AMT.EQV.USD

                CALL F.WRITE(FN.TT.BR.MNT,KEY.TO.USE,R.TT.BR.MNT)
* CALL JOURNAL.UPDATE(KEY.TO.USE)
            NEXT TTI
        END ELSE
            TEXT = 'NO.TELLER.RECORDS' ; CALL REM
        END
***########BILL.REGISTER.SEL###########***
        TEXT = 'BR.SEL' ; CALL REM
        BR.SEL = "SELECT FBNK.BILL.REGISTER WITH CURRENCY NE EGP AND BILL.CHQ.STA EQ 8 AND (COLL.DATE GE ":ST.DATE :" AND COLL.DATE LE " :EN.DATE :" ) BY DRAWER BY COLL.DATE "
***********************
        KEY.LIST.BR=""
        SELECTED.BR=""
        ER.MSG.BR=""

        CALL EB.READLIST(BR.SEL,KEY.LIST.BR,"",SELECTED.BR,ER.MSG.BR)
        TEXT = 'SELECTED.BR=':SELECTED.BR ; CALL REM
        IF SELECTED.BR THEN
            FOR BRI = 1 TO SELECTED.BR
                CALL F.READ( FN.BILL.REG, KEY.LIST.BR<BRI>, R.BILL.REG, F.BILL.REG, ETEXT.BR)
**
                KEY.TO.USE = '' ; BR.REF.NO = '' ; CUST.ID.BR = '' ; LOCAL.REF = '' ; TRN.DAT.BR = '' ; BR.DATE.BR = '' ;  KEY.ID.BR = ''
                MID.RATE = '' ; AMT.FCY.BR = '' ; AMT.LCY.BR = '' ; KEY.IDD = '' ; AMT.EQV.USD.BR = '' ; SELL.RATE.BR = '' ; AMT.EQV.USD.BR = '' ; CUST.COMP = ''
                BR.REF.NO<BRI>  = FIELD(KEY.LIST.BR<BRI>,";", 1)
                CUST.ID.BR<BRI> = R.BILL.REG<EB.BILL.REG.DRAWER>
                CURR.BR<BRI>    = R.BILL.REG<EB.BILL.REG.CURRENCY>

                LOCAL.REF = R.BILL.REG<EB.BILL.REG.LOCAL.REF>
                TRN.DAT.BR<BRI>   = LOCAL.REF<1,BRLR.COLL.DATE>
                BR.DATE.BR = TRN.DAT.BR<BRI>
                KEY.ID.BR<BRI> = CURR.BR<BRI>:"-" :"EGP":"-":BR.DATE.BR
                CALL F.READ(FN.CURR.DALY,KEY.ID.BR<BRI>,R.CURR.DALY,F.CURR.DALY,ERR3)

                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MN THEN
                    MID.RATE<BRI> = R.CURR.DALY<SCCU.MID.RATE,MN>
                END

                AMT.FCY.BR<BRI>   = R.BILL.REG<EB.BILL.REG.AMOUNT>
                AMT.LCY.BR<BRI>   = AMT.FCY.BR<BRI> * MID.RATE<BRI>

                KEY.IDD<BRI> = "USD":"-" :"EGP":"-":BR.DATE.BR
                CALL F.READ(FN.CURR.DALY,KEY.IDD<BRI>,R.CURR.DALY,F.CURR.DALY,ERR4)

                IF CURR.BR<BRI> = "USD" THEN
                    AMT.EQV.USD.BR =  AMT.FCY.BR<BRI>
                END ELSE
                    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NM THEN
                        SELL.RATE.BR<BRI> = R.CURR.DALY<SCCU.SELL.RATE,NM>
                    END
                    AMT.EQV.USD.BR = AMT.LCY.BR<BRI> / SELL.RATE.BR<BRI>
                END ;** END OF OTHER FCY**

                KEY.TO.USE.BR  = CUST.ID.BR<BRI>:"-":YYYYMM
                CALL F.READ( FN.TT.BR.MNT, KEY.TO.USE.BR, R.TT.BR.MNT, F.TT.BR.MNT, ERR1)

                R.TT.BR.MNT<CUST.TR.CUST.NO>  = CUST.ID.BR<BRI>
*Line [ 216 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR( 'CUSTOMER':@FM:EB.CUS.COMPANY.BOOK, CUST.ID.BR<BRI>, CUST.COMP)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID.BR<BRI>,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.COMP=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
                R.TT.BR.MNT<CUST.TR.CUST.CO>  = CUST.COMP
                R.TT.BR.MNT<CUST.TR.YEAR>     = YYYY
                R.TT.BR.MNT<CUST.TR.MONTH>    = MM

                CUST.REF.N = R.TT.BR.MNT<CUST.TR.REF.NO>

*Line [ 215 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                CC = DCOUNT(CUST.REF.N,@VM)
                SS = CC+1
                R.TT.BR.MNT<CUST.TR.REF.NO,SS>     = BR.REF.NO<BRI>
                R.TT.BR.MNT<CUST.TR.TRN.CURR,SS>   = CURR.BR<BRI>
                R.TT.BR.MNT<CUST.TR.AMT.FCY,SS>    = AMT.FCY.BR<BRI>

                CALL EB.ROUND.AMOUNT ('USD', AMT.EQV.USD.BR,'',"2")
                R.TT.BR.MNT<CUST.TR.AMT.EQVUSD,SS> = AMT.EQV.USD.BR
                R.TT.BR.MNT<CUST.TR.TRN.DATE,SS>   = TRN.DAT.BR<BRI>

                TOT.CUST.EQV.BR = R.TT.BR.MNT<CUST.TR.TOT.EQVAMT>
                R.TT.BR.MNT<CUST.TR.TOT.EQVAMT> = TOT.CUST.EQV.BR + AMT.EQV.USD.BR

                CALL F.WRITE(FN.TT.BR.MNT,KEY.TO.USE.BR,R.TT.BR.MNT)
                CALL JOURNAL.UPDATE(KEY.TO.USE.BR)
**
            NEXT BRI
        END         ;*IF SELECTED.BR
***####################################***
        TEXT = '�� �������� �� �������' ; CALL REM
    END
RETURN
END
