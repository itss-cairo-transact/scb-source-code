* @ValidationCode : MjotMTk3NTY0ODA4MzpDcDEyNTI6MTY0MDY5NzYzNzMwMzpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*----------------------------------NI7OOOOOOOOOOOOOOOOOO-------------------------------------------
SUBROUTINE FT.GET.NAME.ENG(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

*Line [ 32 ] Adding F.CATEGORY Layout - ITSS - R21 Upgrade - 2021-12-28
    $INCLUDE I_F.CATEGORY

    BEGIN CASE
        CASE ARG[1,2] EQ 'PL'
            VV=LEN(ARG) - 2
            XX=ARG[3,VV]
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XX,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATTT1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
            ARG = CATTT1

        CASE ARG[1,3] EQ 'EGP'
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            NAME1=ACC
            ARG=NAME1

        CASE ARG[1,3] EQ 'EUR'
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC11)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC11=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            NAME2=ACC11
            ARG=NAME2

        CASE ARG[1,3] EQ 'USD'
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC12)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC12=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            NAME3=ACC12
            ARG=NAME3

        CASE ARG[1,3] EQ 'GBP'
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("ACCOUNT":@FM:AC.ACCOUNT.TITLE.1,ARG,ACC12)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC12=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            NAME4=ACC12
            ARG=NAME4

        CASE ARG[1,2] EQ 99
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS1=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("CUSTOMER":@FM:EB.CUS.NAME.1,CUS1,NAME1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS1,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
NAME1=R.ITSS.CUSTOMER<EB.CUS.NAME.1>
            NAME6 = NAME1
            ARG = NAME6
        CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP' OR ARG[1,3] NE 'EUR' OR ARG[1,3] NE 'GBP'
*XS = ARG[2,7]
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS10)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS10=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR("CUSTOMER":@FM:EB.CUS.NAME.1,CUS10,NAMEE)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS10,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
NAMEE=R.ITSS.CUSTOMER<EB.CUS.NAME.1>
            NAME10 = NAMEE
            ARG  = NAME10
****CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP'
***XS = ARG[2,7]
*CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ARG,CUS10)
*** CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,XS,LOCAL)
***TEXT = XS    ; CALL REM
***NAME10 = LOCAL<1,CULR.ARABIC.NAME>
***TEXT = NAME10    ; CALL REM
***ARG  = NAME10

    END CASE
RETURN
END
