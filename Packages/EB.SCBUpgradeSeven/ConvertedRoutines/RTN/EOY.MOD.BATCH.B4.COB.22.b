* @ValidationCode : MjoxOTI4MTY2NzQ0OkNwMTI1MjoxNjQwNjk3NjMwMDA1OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-101</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/12/23 ***
*******************************************

* SUBROUTINE EOY.MOD.BATCH.B4.COB.22
PROGRAM EOY.MOD.BATCH.B4.COB.22

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 35 ] Hashing the I_OFS.SOURCE.LOCAL.REFS layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

*---------------------------------------------------
*
    FN.DATE = "F.DATES"                 ; F.DATE  = ""
*
    FN.BTCH = "F.BATCH"                ; F.BTCH = ""
*----------------------------------------------------
    CALL OPF (FN.BTCH,F.BTCH)
    CALL OPF (FN.DATE,F.DATE)
*-------------------------------------------------
    R.TEMP = ""
*------------------------------------------------
    GOSUB A.10.DATE
    GOSUB A.50.GET.BTCH.22
RETURN
*----------------------------------------
A.10.DATE:

**    EXECUTE "OFSADMINLOGIN"
    WS.DATE.ID      = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.PER.END = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LPE.DATE     = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.LAST.PER.END = WS.LPE.DATE
    WS.YY.MM        = WS.LAST.PER.END[1,6]
    WS.DD           = WS.LAST.PER.END[2]
    WS.MM           = WS.YY.MM[5,2] + 0
    WS.LOCAT.DATE   = WS.YY.MM:"01"
    WS.ACC.OFFICER  = ID.COMPANY[2]

    WS.TRNS.DATE = TODAY[1,6]:"01"

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "BATCH"
    SCB.VERSION  = "EOY"
    OFS.MESSAGE.DATA = ''

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    OPENSEQ "OFS.MNGR.IN" , "SBY.MOD.BATCH.B4.COB.22" TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"SBY.MOD.BATCH.B4.COB.22"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , "SBY.MOD.BATCH.B4.COB.22" TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create SBY.MOD.BATCH.B4.COB.22 File IN OFS.MNGR.IN'
        END
    END

    OPENSEQ "OFS.MNGR.OUT" , "SBY.MOD.BATCH.B4.COB.22.OUT" TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"SBY.MOD.BATCH.B4.COB.22.OUT"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "SBY.MOD.BATCH.B4.COB.22.OUT" TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create SBY.MOD.BATCH.B4.COB.22.OUT File IN OFS.MNGR.OUT'
        END
    END


RETURN
**=====================================================================================================================
A.50.GET.BTCH.22:
**=====================================================================================================================
    SEL.BTCH.22 = "SELECT ":FN.BTCH:" WITH @ID LIKE .../SCB.EOY.13300.AFT BY @ID"
    CALL EB.READLIST(SEL.BTCH.22,KEY.BTCH.22,"",SELECTED.BTCH.22,ER.MSG.BTCH.22)
*PRINT SELECTED.BTCH.22
    IF SELECTED.BTCH.22 THEN
        FOR I.BTCH.22 = 1 TO SELECTED.BTCH.22
            CALL F.READ(FN.BTCH,KEY.BTCH.22<I.BTCH.22>,R.BTCH.22,F.BTCH.22,ER.BTCH.22)

            WS.BATCH.ID.BTCH.22 = KEY.BTCH.22<I.BTCH.22>
            WS.BATCH.ID         = KEY.BTCH.22<I.BTCH.22>

            IF NOT(ER.BTCH.22) THEN
                WS.BR.CODE = R.BTCH.22<BAT.CO.CODE>[2]
                R.BATCH    = R.BTCH.22
                GOSUB RE.READ.BATCH.22
            END
        NEXT I.BTCH.22
    END
RETURN
**=====================================================================================================================
RE.READ.BATCH.22:
**=====================================================================================================================
    WS.FRQ.D = 'D'
    WS.FRQ.A = 'A'

    OFS.MESSAGE.DATA  :=  ",BATCH.STAGE::=":R.BTCH.22<BAT.BATCH.STAGE>
    OFS.MESSAGE.DATA  :=  ",BATCH.ENVIRONMENT::=":R.BTCH.22<BAT.BATCH.ENVIRONMENT>

*Line [ 136 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT.BATCH.22 = DCOUNT(R.BTCH.22<BAT.JOB.NAME>,@VM)
    IF WS.COUNT.BATCH.22 GE 1 THEN
        FOR I.COUNT.BATCH.22 = 1 TO WS.COUNT.BATCH.22
            WS.OFS.COUNT = I.COUNT.BATCH.22
            IF R.BTCH.22<BAT.JOB.NAME><1,I.COUNT.BATCH.22> EQ 'EOY.ACC.13300' THEN
                OFS.MESSAGE.DATA  :=  ",JOB.NAME:":WS.OFS.COUNT:":1:=":R.BTCH.22<BAT.JOB.NAME><1,I.COUNT.BATCH.22>
                OFS.MESSAGE.DATA  :=  ",VERIFICATION:":WS.OFS.COUNT:":1:=":R.BTCH.22<BAT.VERIFICATION><1,I.COUNT.BATCH.22>
                OFS.MESSAGE.DATA  :=  ",FREQUENCY:":WS.OFS.COUNT:":1:=":WS.FRQ.D
            END ELSE
                OFS.MESSAGE.DATA  :=  ",JOB.NAME:":WS.OFS.COUNT:":1:=":R.BTCH.22<BAT.JOB.NAME><1,I.COUNT.BATCH.22>
                OFS.MESSAGE.DATA  :=  ",VERIFICATION:":WS.OFS.COUNT:":1:=":R.BTCH.22<BAT.VERIFICATION><1,I.COUNT.BATCH.22>
                OFS.MESSAGE.DATA  :=  ",FREQUENCY:":WS.OFS.COUNT:":1:=":R.BTCH.22<BAT.FREQUENCY><1,I.COUNT.BATCH.22>
                OFS.MESSAGE.DATA  :=  ",NEXT.RUN.DATE:":WS.OFS.COUNT:":1:=":R.BTCH.22<BAT.NEXT.RUN.DATE><1,I.COUNT.BATCH.22>
            END
        NEXT I.COUNT.BATCH.22
    END
    GOSUB RUN.OFS.SUB
    WS.OFS.COUNT = 0
RETURN
**=====================================================================================================================

BATCH.FRQ.MOD:
    WS.FRQ.D = 'D'
    WS.FRQ.A = 'A'
    OFS.MESSAGE.DATA  :=  ",FREQUENCY:":WS.OFS.COUNT:":1:=":WS.FRQ.D

    WS.OFS.COUNT = 0
RETURN
**=====================================================================================================================
RUN.OFS.SUB:
**=====================================================================================================================
    IF OFS.MESSAGE.DATA NE '' THEN

        WS.BATCH.ID1 =  FIELD(WS.BATCH.ID,"/",1)
        WS.BATCH.ID2 =  FIELD(WS.BATCH.ID,"/",2)
        WS.BATCH.ID = WS.BATCH.ID1:"^":WS.BATCH.ID2

**        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.BR.CODE:"//EG00100":WS.BR.CODE:",":WS.BATCH.ID:OFS.MESSAGE.DATA
        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,,":WS.BATCH.ID:OFS.MESSAGE.DATA

        BB.IN.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.IN.DATA TO BB.IN ELSE
        END

* SCB R15 UPG 20160717 - S
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E

        BB.OUT.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
        END
    END
    OFS.MESSAGE.DATA = ''
RETURN
**=====================================================================================================================
END
