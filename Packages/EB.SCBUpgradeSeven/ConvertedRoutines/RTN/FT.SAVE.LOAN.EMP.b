* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgzMDY3MzM6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.SAVE.LOAN.EMP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.GOV
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    FN.GOV = 'F.SCB.LOANS.GOV'; F.GOV = ''
    CALL OPF(FN.GOV,F.GOV)

 ***   OPEN FN.GOV TO FVAR.GOV ELSE
 ***       TEXT = "ERROR OPEN FILE" ; CALL REM
 ***       RETURN
 ***   END

    CUST       = R.NEW(FT.DEBIT.CUSTOMER)
    EMP        = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCEPTOR.NAME>
    LOAN.NO    = R.NEW(FT.LOCAL.REF)<1,FTLR.PCODE>

    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='INAU' THEN
        T.SEL ="SELECT F.SCB.LOANS.GOV WITH PAY.CODE EQ 13 AND CUSTOMER.NO LIKE ...":CUST:" AND EMP.NO EQ ":EMP:" AND LOAN.NO EQ ":LOAN.NO
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            R.GOV<GOV.PAY.CODE> = '22'
            CALL F.WRITE (FN.GOV,KEY.LIST<I>, R.GOV)
***            WRITE R.GOV TO FVAR.GOV , KEY.LIST<I>  ON ERROR
***                STOP 'CAN NOT WRITE RECORD ':KEY.LIST<I>:' TO FILE ':FN.GOV
***          END

        NEXT I
    END
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='RNAU' THEN
        T.SEL  ="SELECT F.SCB.LOANS.GOV WITH PAY.CODE EQ 22 AND CUSTOMER.NO "
        T.SEL :="LIKE ...":CUST:" AND EMP.NO EQ ":EMP:" AND LOAN.NO EQ ":LOAN.NO
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            R.GOV<GOV.PAY.CODE> = '13'
            CALL F.WRITE (FN.GOV,KEY.LIST<I>, R.GOV)
***            WRITE R.GOV TO FVAR.GOV , KEY.LIST<I>  ON ERROR
***                STOP 'CAN NOT WRITE RECORD ':KEY.LIST<I>:' TO FILE ':FN.GOV
***            END

        NEXT I

    END
    RETURN
END
