* @ValidationCode : MjoxNjcxNjQ2NzM4OkNwMTI1MjoxNjQwNjk3NjM2MjcxOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
SUBROUTINE FT.DETAIL2(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 26 ] Hashed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS


* ID.NEW = ARG
* CALL DBR('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,ID.NEW,NEW.LOC)
* ARG = NEW.LOC<1,FTLR.NOTE.CREDIT>
*Line [ 33 --> 38 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-27
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.NOTE.CREDIT",FTLR.NOTE.CREDIT.POS)
   
    X = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,1>
    Y = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,2>
    Z = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,3>
    Q = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT.POS,4>
    
*    X = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,1>
*    Y = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,2>
*    Z = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,3>
*    Q = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT,4>
    ARG = Y
RETURN
END
