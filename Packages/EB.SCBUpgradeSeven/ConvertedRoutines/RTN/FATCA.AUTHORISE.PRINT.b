* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU2MTYyMTc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:00:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FATCA.AUTHORISE.PRINT
*    PROGRAM FATCA.AUTHORISE.PRINT

*-------- CREATED BY NOHA HAMED 16/12/2014 ---

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='FATCA.AUTHORISE.PRINT'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*========================================================================
PROCESS:
*TEXT = ID.NEW ; CALL REM
    SAVE.SCREEN.TITLE = 'FATCA.CUSTOMER.SUPPLEMENTARY.INFO'
    ENQ = '%FATCA.CUSTOMER.SUPPLEMENTARY.INFO-DEFAULT'
    ENQ<2,1> = '@ID'
    ENQ<3,1> = 'EQ'
    ENQ<4,1> = ID.NEW
    ENQ<10,1> = 'OUTPUT'

    E = ''
    ETEXT = ''
    TEXT = ''
    ERR = ''

    V.ENV = 'JBASE_ERRMSG_ZERO_USED'
    IF NOT( PUTENV(V.ENV : '=3') ) THEN
        CRT 'PUTENV failed'
        STOP
    END

    CALL ENQUIRY.DISPLAY(ENQ)
    SCREEN.TITLE = SAVE.SCREEN.TITLE
    CALL STANDARD.DISPLAY

    E = ''
    ETEXT = ''
    TEXT = ''
    ERR = ''

    IF NOT( PUTENV(V.ENV : '=3') ) THEN
        CRT 'PUTENV failed'
        STOP
    END

    RETURN

*===============================================================
END
