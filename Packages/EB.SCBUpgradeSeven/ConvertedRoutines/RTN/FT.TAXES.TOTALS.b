* @ValidationCode : MjoyMDYxMjg5MjY5OkNwMTI1MjoxNjQwNjk3NjQ0OTgzOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
************REHAM YOUSSIF 20150510*****************
SUBROUTINE FT.TAXES.TOTALS
*PROGRAM FT.TAXES.TOTALS
***************************************************
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 47 ] Removed UnUsed I_AC.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 50 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 53 ] Removed UnUsed I_TT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_TT.LOCAL.REFS
    GOSUB INITIATE
*Line [ 53 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB SEL.COMPANY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
**********************************************
INITIATE:
*-------
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
**********************************************
CALLDB:
*--------
    FN.ACC    = "FBNK.ACCOUNT"           ; F.ACC  = '' ; R.ACC  = '' ; CALL OPF(FN.ACC,F.ACC)
    FN.FT    = "FBNK.FUNDS.TRANSFER$HIS"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.TT    = "FBNK.TELLER$HIS"; F.TT   = '' ; R.TT   = '' ; CALL OPF(FN.TT,F.TT)
    FN.STE   = "FBNK.STMT.ENTRY" ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
    FN.COMP  = "F.COMPANY" ; F.COMP  = '' ; R.COMP  = '' ; CALL OPF(FN.COMP,F.COMP)
RETURN
**********************************************
SEL.COMPANY:
    AMT.TOT = '0'
    T.SEL = "SELECT ":FN.COMP:" WITH @ID NE 'EG0010088' AND @ID NE 'EG0010099' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",NO.REC,ER.SEL)
    IF NO.REC THEN
        FOR Y = 1 TO NO.REC
            CALL F.READ(FN.COMP,KEY.LIST<Y>,R.COMP,F.COMP,EER.R)
            NAME = R.COMP<EB.COM.COMPANY.NAME,2>
            WS.COMPANY = KEY.LIST<Y>
            AMT.TOTAL = '0'
            GOSUB PROCESS1
            GOSUB PROCESS2
            IF AMT.TOTAL GT '0' THEN
                XX3 = SPACE(132)
                XX3<1,1>[20,20] = '������ ��� ' : NAME : '='
                XX3<1,1>[60,10] = AMT.TOTAL
                XX3<1,1>[80,10] = '���� ����'

                PRINT XX3<1,1>
                PRINT STR('=',120)
                AMT.TOT +=AMT.TOTAL
            END
        NEXT Y
    END
    XX4 = SPACE(132)
    XX4<1,1>[20,20] = '������ '
    XX4<1,1>[60,10] = AMT.TOT
    XX4<1,1>[80,10] = '���� ����'
    PRINT XX4<1,1>

RETURN
**********************************************
PROCESS1:
*-------
    ACCT.ID = '9949990010321101'
*DEBUG
    TD = TODAY
    CALL CDT("",TD,'-2W')
    FROM.DATE = TD
    END.DATE  = TD
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.V.DATE = R.STE<AC.STE.VALUE.DATE>
        STE.REF    = R.STE<AC.STE.TRANS.REFERENCE>[1,12]
        COMP.ID    = R.STE<AC.STE.COMPANY.CODE>
*   IF STE.V.DATE = TODAY  THEN
        FT.ID = STE.REF:';1'
        CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
*Line [ 130 --> 133 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-27
        CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.NOTE.CREDIT",FTLR.NOTE.CREDIT.POS)
        NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT.POS>
*NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
        IF ER.FT THEN
            CALL F.READ(FN.TT,FT.ID,R.TT,F.TT,ER.TT)
*Line [ 136 --> 139 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-27
            CALL GET.LOC.REF("TELLER","TTLR.EFIN.TIME",TTLR.EFIN.TIME.POS)
            NOTE.CREDIT       = R.TT<TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME.POS>
*NOTE.CREDIT       = R.TT<TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME>
        END

        LENN = LEN(NOTE.CREDIT)
        IF LENN EQ '1' THEN
            IF NOTE.CREDIT LE '3' THEN
                IF NOTE.CREDIT NE '.' THEN
                    DEBIT.AMT1         = R.STE<AC.STE.AMOUNT.LCY>


                    IF STE.REF[1,2] EQ 'FT' THEN
                        DEBIT.ACCT        = R.FT<FT.DEBIT.ACCT.NO>
                        DEBIT.CURR        = R.FT<FT.DEBIT.CURRENCY>
                        FT.CODE           = R.FT<FT.CO.CODE>
                        VAL.DATE          = R.FT<FT.DEBIT.VALUE.DATE>
*Line [ 155 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACCT,CUSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                        CUST.NAME         = LOC.REF<1,CULR.ARABIC.NAME>
                    END ELSE

                        DEBIT.CURR        = R.TT<TT.TE.CURRENCY.1>
                        FT.CODE           = R.TT<TT.TE.CO.CODE>
                        VAL.DATE          = R.TT<TT.TE.AUTH.DATE>
                        CUST.NAME    = ''
                        DEBIT.ACCT   = ''
                    END
*Line [ 178 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,FT.CODE,BRAN)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,FT.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRAN=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
                    IF WS.COMPANY NE FT.CODE THEN
                        GOTO NEXT.I.REC1
                    END

*Line [ 189 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,DEBIT.CURR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DEBIT.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                    AMT.TOTAL        += DEBIT.AMT1

                    XX1 = SPACE(132)
                    XX1<1,1>[1,16]    = STE.REF
                    XX1<1,1>[20,10]   = VAL.DATE
                    XX1<1,1>[50,10]   = DEBIT.AMT1
                    XX1<1,1>[70,10]   = CRR
                    XX1<1,1>[90,10]   = NOTE.CREDIT
                    XX1<1,1>[110,16]  = BRAN
                    PRINT XX1<1,1>

                    XX2 = SPACE(132)
                    XX2<1,1>[30,70]    = CUST.NAME
                    XX2<1,1>[1,20]   = DEBIT.ACCT
                    PRINT XX2<1,1>
                    PRINT STR('-',130)
                    DEBIT.ACCT = ''
                    CUST.NAME  = ''
                    DEBIT.AMT  = ''
                    CRR        = ''
                    FT.ID      = ''
                    BRAN       = ''
                END
            END
        END

NEXT.I.REC1:
    REPEAT
RETURN

**********************************************
PROCESS2:
*-------
    ACCT.ID = '9949990010321001'
    TD = TODAY
    CALL CDT("",TD,'-3W')
    FROM.DATE = TD
    END.DATE  = TD
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.V.DATE = R.STE<AC.STE.VALUE.DATE>
        STE.REF    = R.STE<AC.STE.TRANS.REFERENCE>[1,12]
        COMP.ID    = R.STE<AC.STE.COMPANY.CODE>
* IF STE.V.DATE = TODAY  THEN
        FT.ID = STE.REF:';1'
        CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
        NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT.POS>
        IF ER.FT THEN
            CALL F.READ(FN.TT,FT.ID,R.TT,F.TT,ER.TT)
            NOTE.CREDIT       = R.TT< TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME.POS>
        END

        LENN = LEN(NOTE.CREDIT)
        IF LENN EQ '1' THEN
            IF NOTE.CREDIT GT '3' THEN
                IF NOTE.CREDIT NE '.' THEN
                    DEBIT.AMT1         = R.STE<AC.STE.AMOUNT.LCY>
                    IF STE.REF[1,2] EQ 'FT' THEN
                        DEBIT.ACCT        = R.FT<FT.DEBIT.ACCT.NO>
                        DEBIT.CURR        = R.FT<FT.DEBIT.CURRENCY>
                        FT.CODE           = R.FT<FT.CO.CODE>
                        VAL.DATE          = R.FT<FT.DEBIT.VALUE.DATE>
*Line [ 261 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACCT,CUSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 268 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                        CUST.NAME         = LOC.REF<1,CULR.ARABIC.NAME>
                    END ELSE

                        DEBIT.CURR        = R.TT<TT.TE.CURRENCY.1>
                        FT.CODE           = R.TT<TT.TE.CO.CODE>
                        VAL.DATE          = R.TT<TT.TE.AUTH.DATE>
                        CUST.NAME    = ''
                        DEBIT.ACCT   = ''
                    END
*Line [ 284 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,FT.CODE,BRAN)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,FT.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRAN=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
                    IF WS.COMPANY NE FT.CODE THEN
                        GOTO NEXT.I.REC2
                    END

*Line [ 295 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,DEBIT.CURR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DEBIT.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                    AMT.TOTAL        += DEBIT.AMT1

                    XX1 = SPACE(132)
                    XX1<1,1>[1,16]    = STE.REF
                    XX1<1,1>[20,10]   = VAL.DATE
                    XX1<1,1>[50,10]   = DEBIT.AMT1
                    XX1<1,1>[70,10]   = CRR
                    XX1<1,1>[90,10]   = NOTE.CREDIT
                    XX1<1,1>[110,16]  = BRAN
                    PRINT XX1<1,1>

                    XX2 = SPACE(132)
                    XX2<1,1>[30,70]    = CUST.NAME
                    XX2<1,1>[1,20]   = DEBIT.ACCT
                    PRINT XX2<1,1>
                    PRINT STR('-',130)
                    DEBIT.ACCT = ''
                    CUST.NAME  = ''
                    DEBIT.AMT  = ''
                    CRR        = ''
                    FT.ID      = ''
                    BRAN       = ''
                END
            END
        END
NEXT.I.REC2:
    REPEAT
RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    COMP = ID.COMPANY
*Line [ 333 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'FT.TAXES.TOTALS'
    PR.HD :="'L'":" "
    WS.DAT = TODAY
    WS.DAT = FMT(WS.DAT,"####/##/##")
    PR.HD :="'L'":SPACE(45):"������� ������� ������� ������ ��  : ":WS.DAT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(15):"�.�������":SPACE(15):"������":SPACE(15):"������": SPACE(15):"�.������":SPACE(15):"�����"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
RETURN
END
