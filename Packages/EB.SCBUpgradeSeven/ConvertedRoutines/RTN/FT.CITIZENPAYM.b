* @ValidationCode : MjoxMDAyMzA0MjkwOkNwMTI1MjoxNjQwNjk3NjM0OTcwOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
************MAI JAN 2019**
SUBROUTINE FT.CITIZENPAYM
***************************************************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 44 ] Hashed I_AC.LOCAL.REFS layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_AC.LOCAL.REFS

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
**********************************************
INITIATE:
*-------
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    ACCT.ID = '9949990010321801'
    TD = TODAY
    CALL CDT("",TD,'-1W')

    FROM.DATE = TD
    END.DATE  = TODAY

RETURN
**********************************************
CALLDB:
*--------
    FN.ACC = "FBNK.ACCOUNT"           ; F.ACC  = '' ; R.ACC  = '' ; CALL OPF(FN.ACC,F.ACC)
    FN.FT  = "FBNK.FUNDS.TRANSFER$HIS"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
*   FN.FT  = "FBNK.FUNDS.TRANSFER"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
RETURN
**********************************************
PROCESS:
*-------
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    AMT.TOTAL = 0
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.V.DATE = R.STE<AC.STE.VALUE.DATE>
        STE.REF    = R.STE<AC.STE.OUR.REFERENCE>
        COMP.ID    = R.STE<AC.STE.COMPANY.CODE>
        IF STE.V.DATE = TODAY  THEN
            FT.ID = STE.REF
            CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
            DEBIT.ACCT       = R.FT<FT.DEBIT.ACCT.NO>
            DEBIT.CURR       = R.FT<FT.DEBIT.CURRENCY>
            FT.CODE          = R.FT<FT.CO.CODE>
            IF COMP EQ FT.CODE THEN
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,DEBIT.CURR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DEBIT.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                DEBIT.AMT.1        = R.FT<FT.AMOUNT.DEBITED>
                DEBIT.AMT = DEBIT.AMT.1[4,15]
                DEBIT.THEIR.REF  = R.FT<FT.DEBIT.THEIR.REF>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACCT,CUSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME        = LOC.REF<1,CULR.ARABIC.NAME>
                AMT.TOTAL        += DEBIT.AMT

                XX1 = SPACE(132)
                XX1<1,1>[1,16]   = DEBIT.ACCT
                XX1<1,1>[20,40]  = CUST.NAME
                XX1<1,1>[60,10]  = DEBIT.AMT
                XX1<1,1>[80,10]  = CRR
                XX1<1,1>[100,16]  = FT.ID
                PRINT XX1<1,1>
                PRINT STR(' ',132)

                DEBIT.ACCT = ''
                CUST.NAME  = ''
                DEBIT.AMT  = ''
                CRR        = ''
                FT.ID      = ''
            END
        END
    REPEAT

    XX2 = SPACE(132)
    XX2<1,1>[25,20] = '������ ������'
    XX2<1,1>[60,10] = AMT.TOTAL
    XX2<1,1>[80,10] = '���� ����'

    PRINT STR('=',132)
    PRINT XX2<1,1>
    PRINT STR('=',132)
RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    COMP = ID.COMPANY
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'FT.CITIZENPAYM'
    PR.HD :="'L'":" "
    WS.DAT = TODAY
    WS.DAT = FMT(WS.DAT,"####/##/##")
    PR.HD :="'L'":SPACE(45):"������� ������ ������� �� ����� ������ ��:":WS.DAT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(15):"��� ������":SPACE(25):"������":SPACE(15):"������": SPACE(15):"��� �������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
RETURN
END
