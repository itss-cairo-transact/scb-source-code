* @ValidationCode : Mjo1MzIyMzUyNDk6Q3AxMjUyOjE2NDA2OTc2MzQwOTM6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FT.AR.CURRENCY(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 30 ] Hashing I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS
*----------------------------------------
*Line [ 34,35,36 ] Replacing by get loc ref to get the position for the LF - ITSS - R21 Upgrade - 2021-12-27
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.LANGUAGE",FTLR.LANGUAGE.POS)
    LANGUAGE.NUMBER = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE.POS>
*LANGUAGE.NUMBER = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE>
    CUR             = R.NEW(FT.DEBIT.CURRENCY)

    IF LANGUAGE.NUMBER EQ '2' THEN
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    END

    IF LANGUAGE.NUMBER EQ '1' THEN
        FN.CUR = "F.CURRENCY"  ; F.CUR = ""
        CALL OPF(FN.CUR,F.CUR)

        CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ER.CUR)
        CURR = R.CUR<EB.CUR.CCY.NAME><1,1>
    END

    IF LANGUAGE.NUMBER EQ '' THEN
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    END

    ARG = CURR
*------------------------------------------------------
RETURN
END
