* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MjQ3Njc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE GET.CCY.RATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM



    WS.CURR = O.DATA
    NET.RATE = ''
    O.DATA = ''

*----------------------------------------------------------------------**
    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'
    F.CUR  = ''
    R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
*----------------------------------------------------------------------**
    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)

    CUR.COD = R.CUR<RE.BCP.ORIGINAL.CCY>
*Line [ 44 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.CURR = DCOUNT(CUR.COD,@VM)

    FOR POS = 1 TO NO.CURR
        FL.CURR = R.CUR<RE.BCP.ORIGINAL.CCY,POS>

        IF WS.CURR EQ  FL.CURR THEN
            NET.RATE = R.CUR<RE.BCP.RATE,POS>
        END

    NEXT POS


    O.DATA     = NET.RATE

    RETURN
*==============================================================********
END
