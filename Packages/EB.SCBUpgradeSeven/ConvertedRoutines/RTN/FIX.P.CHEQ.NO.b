* @ValidationCode : MjoxNzM2ODgzMjM6Q3AxMjUyOjE2NDYwMzQ5MDkxMDc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Feb 2022 09:55:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
** ----- NESSREEN AHMED 14/05/2009 -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FIX.P.CHEQ.NO
** TO MAKE CHEQUES RECORDS THAT PAIED THROUGH TELLER VERSIONS OVER 20000 LE AND DIDNOT UPDATE THE TABLE WITH CHEQUES RECORDS
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23

    $INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.P.CHEQ

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "PAY_CHEQ"

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
    FN.P.CHQ = 'F.SCB.P.CHEQ' ; F.P.CHQ = '' ; R.P.CHQ = ''
    FN.TELLER = 'F.TELLER$HIS' ; F.TELLER = '' ; R.TELLER = ''
    FN.FT = 'F.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = ''
    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF( FN.P.CHQ,F.P.CHQ)
    CALL OPF( FN.TELLER,F.TELLER)
***UPDATED BY NESSREEN AHMED 21/3/2010******************************
**    TT.SEL = "SELECT FBNK.TELLER$HIS WITH (TRANSACTION.CODE EQ 92 OR TRANSACTION.CODE EQ 95 OR TRANSACTION.CODE EQ 96) AND RECORD.STATUS NE 'REVE' BY AUTH.DATE "
    TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 82 AND RECORD.STATUS NE 'REVE' AND AUTH.DATE GT 20100225 BY AUTH.DATE"
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            CHEQ.VAL = '' ; CHEQ.DAT = '' ; TRN.DAT = '' ; OUR.REF = ''
            CHEQ.NO = '' ; COMP.CODE = '' ; DEP.CO = '' ;  KEY.TO.USE = '' ; CHEQ.ACC = '' ; REF = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = ''
            CALL F.READ( FN.TELLER,KEY.LIST.TT<TTI>, R.TELLER, F.TELLER, ETEXT.TT)
            CHEQ.NO  = R.TELLER<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
            CHEQ.VAL = R.TELLER<TT.TE.NET.AMOUNT>
            CHEQ.DAT = R.TELLER<TT.TE.LOCAL.REF,TTLR.ISSUE.DATE>
            TRN.DAT = R.TELLER<TT.TE.VALUE.DATE.1>
            CHEQ.ACC = R.TELLER<TT.TE.ACCOUNT.1>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>
* CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CHEQ.ACC,DEP.CO)
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
            BRN.ID  = AC.COMP[2]
            DEP.CO = TRIM(BRN.ID,"0","L")


            REF = KEY.LIST.TT<TTI>
            TT.REF  = FIELD(REF, ";" ,1)
            TT.R = TT.REF:";2"

            CALL F.READ( FN.TELLER,TT.R, R.TELLER, F.TELLER, ETEXT.R)
            IF ETEXT.R THEN
                KEY.TO.USE = CHEQ.NO:".":DEP.CO
                CALL F.READ( FN.P.CHQ,KEY.TO.USE, R.P.CHQ, F.P.CHQ, ETEXT)
                IF ETEXT THEN
                    R.P.CHQ<P.CHEQ.CHEQ.VAL> = CHEQ.VAL
                    R.P.CHQ<P.CHEQ.CHEQ.DAT> = CHEQ.DAT
                    R.P.CHQ<P.CHEQ.TRN.DAT> =  TRN.DAT
                    R.P.CHQ<P.CHEQ.ACCOUNT.NO> = CHEQ.ACC
                    R.P.CHQ<P.CHEQ.OUR.REF> = TT.REF
                    R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP.CODE
                    R.P.CHQ<P.CHEQ.OLD.KEY> = "FIX.20100323"
                    CALL F.WRITE(FN.P.CHQ,KEY.TO.USE,R.P.CHQ)
                    CALL JOURNAL.UPDATE(KEY.TO.USE)
                END ELSE
                    CHQ.DATA = TT.REF:"|"
                    CHQ.DATA := CHEQ.NO:"|"
                    CHQ.DATA := DEP.CO:"|"
                    CHQ.DATA := CHEQ.ACC:"|"
                    CHQ.DATA := COMP.CODE:"|"
                    CHQ.DATA := CHEQ.VAL:"|"
                    CHQ.DATA := TRN.DAT
                    XX = XX + 1
                    DIM ZZ(XX)
                    ZZ(XX) = CHQ.DATA

                    WRITESEQ ZZ(XX) TO V.FILE.IN ELSE
                        PRINT  'CAN NOT WRITE LINE ':ZZ(XX)
                    END
                END
            END
        NEXT TTI
    END
RETURN
END
