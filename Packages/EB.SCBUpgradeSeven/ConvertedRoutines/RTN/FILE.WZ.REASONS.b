* @ValidationCode : MjotNjU2Njk3NTQ4OkNwMTI1MjoxNjQ1OTU4MjczMDcyOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:37:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--WAGDY---------------------------------------------------------------------------
* <Rating>-80</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FILE.WZ.REASONS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 26 ] Hashed I_BR.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_BR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BL.BATCH
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.BATCH.FCY

**---------------------------------------------------------------------**
    OPENSEQ "/hq/opce/bclr/user/fcy.clearing" , "WZ.R.REASON": TO DD THEN
        CLOSESEQ DD
        HUSH ON
        EXECUTE 'DELETE ':"/hq/opce/bclr/user/fcy.clearing":' ':"WZ.R.REASON"
        HUSH OFF
    END
    OPENSEQ "/hq/opce/bclr/user/fcy.clearing" , "WZ.R.REASON" TO DD ELSE
        CREATE DD THEN
            PRINT 'FILE CREATED IN /hq/opce/bclr/user/fcy.clearing'
        END
        ELSE
            STOP 'Cannot create File IN /hq/opce/bclr/user/fcy.clearing'
        END
    END
**---------------------------------------------------------------------**

    EOF = ''

*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    MAIN.MULTI = DCOUNT(R.NEW(SCB.BR.BR.CODE),@VM)
    TEXT = "D1= " :  MAIN.MULTI ; CALL REM
    FOR I = 1 TO MAIN.MULTI
*Line [ 71 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        SUB.VAL = DCOUNT(R.NEW(SCB.BR.OUR.REFERENCE)<1,I>,@SM)
        TEXT = "SUB= " : SUB.VAL ; CALL REM
        FOR X = 1 TO SUB.VAL
            BR.CODE       = R.NEW(SCB.BR.BR.CODE)<1,I>
            OUR.REF       = R.NEW(SCB.BR.OUR.REFERENCE)<1,I,X>
            BOOK.DATE     = R.NEW(SCB.BR.BOOK.DATE)
            CURR          = R.NEW(SCB.BR.CURR)<1,I,X>
            AMOUNT        = R.NEW(SCB.BR.AMOUNT)<1,I,X>
            BANK          = R.NEW(SCB.BR.BANK)<1,I,X>
            RETURN.REASON =R.NEW(SCB.BR.RETURN.REASON)<1,I,X>
            DD.DATA = ""
            GOSUB WRITE.ROW
        NEXT X
    NEXT I
RETURN
**-------------------------------------------------------------------**
WRITE.ROW:
    DD.DATA := BR.CODE:"|"
    DD.DATA := OUR.REF:"|"
    DD.DATA := BOOK.DATE:"|"
    DD.DATA := CURR:"|"
    DD.DATA := AMOUNT:"|"
    DD.DATA := BANK:"|"
    DD.DATA := RETURN.REASON

    WRITESEQ DD.DATA TO DD ELSE
        PRINT " ERROR WRITE FILE "
    END

    TEXT = "FINISHED ": DD.DATA ; CALL REM

RETURN
END
