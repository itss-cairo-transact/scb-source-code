* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU2MTA2Njk6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:00:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE EXTRACT.ADV(ARG)


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DE.BIC

*Line [ 30 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*CALL DBR('DE.BIC':@FM:DE.BIC.INSTITUTION,ARG,ADV.NAME)
F.ITSS.DE.BIC = 'F.DE.BIC'
FN.F.ITSS.DE.BIC = ''
CALL OPF(F.ITSS.DE.BIC,FN.F.ITSS.DE.BIC)
CALL F.READ(F.ITSS.DE.BIC,ARG,R.ITSS.DE.BIC,FN.F.ITSS.DE.BIC,ERROR.DE.BIC)
ADV.NAME=R.ITSS.DE.BIC<DE.BIC.INSTITUTION>
IF NOT(ETEXT) THEN
  ARG = ADV.NAME
END

RETURN
END
