* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTQ5OTE6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.CHRG.TYPE(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE

* COMM = R.NEW(FT.COMMISSION.TYPE)
*  CHG = R.NEW(FT.CHARGE.TYPE)
*DD = DCOUNT(COMM,VM)
* DD = DCOUNT(CHG,VM)
* FOR I = 1 TO DD
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR ('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,ARG,DESC)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,ARG,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DESC=R.ITSS.FT.CHARGE.TYPE<FT5.DESCRIPTION>
* ARG = DESC
* NEXT I
*  COMM = ARG[4,13]
    ARG = DESC


    RETURN
END
