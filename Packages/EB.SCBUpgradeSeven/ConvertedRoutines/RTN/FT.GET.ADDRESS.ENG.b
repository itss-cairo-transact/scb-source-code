* @ValidationCode : MjotMjMyNjI0NjU4OkNwMTI1MjoxNjQ2MDM0OTEzNzU3OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Feb 2022 09:55:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOOO--------------------------------------
SUBROUTINE FT.GET.ADDRESS.ENG(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Adding I_F.CATEGORY Layout - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
    $INCLUDE I_F.SCB.CUS.REGION

    BEGIN CASE

        CASE ARG[1,2] EQ 'PL'
            VV=LEN(ARG) - 2
            XX=ARG[3,VV]
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,XX,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATTT1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
            ARG = CATTT1
        CASE ARG[1,3] EQ 'EGP'
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ARG,ACC)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            ACC=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            ARG = ACC
        CASE ARG[1,2] NE 'PL' OR ARG[1,3] NE 'EGP'
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ARG,CUS)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ARG,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,MYLOCAL)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            MYLOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CUSTOMER':@FM:EB.CUS.STREET,CUS,STREET)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            STREET=R.ITSS.CUSTOMER<EB.CUS.STREET>
            ADR = STREET
            ARG = ADR


            CUST.ADDRESS1= MYLOCAL<1,CULR.GOVERNORATE>
            CUST.ADDRESS2= MYLOCAL<1,CULR.REGION>
*Line [ 85 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION<1,1>,CUST.ADDRESS1,CUST.ADD2)
            F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
            FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
            CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
            CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.ADDRESS1,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
            CUST.ADD2=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION,1>
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION<1,1>,CUST.ADDRESS2,CUST.ADD1)
            F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
            FN.F.ITSS.SCB.CUS.REGION = ''
            CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
            CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.ADDRESS2,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
            CUST.ADD1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION,1>

            IF CUST.ADDRESS1 = 98  THEN
                CUST.ADD1 = ''
            END
            IF CUST.ADDRESS2 = 998 THEN
                CUST.ADD2 = ''
            END
            IF CUST.ADDRESS1 = 999 THEN
                CUST.ADD1 = ''
            END
            IF CUST.ADDRESS2 = 999 THEN
                CUST.ADD2 = ''
            END

    END CASE
RETURN
END
