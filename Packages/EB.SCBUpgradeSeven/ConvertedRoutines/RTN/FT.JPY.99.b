* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTg0OTE6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-51</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.JPY.99

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

*---------------------------------------------------------------------

    GOSUB INITIATE
    IF DATA.FLAG EQ 1 THEN
        RETURN
    END
    XX5 = SPACE(120)
    PRINT STR('-',120)
    XX5<1,1>[1,10]  = "��������"
    XX5<1,1>[20,15] = FMT(TOT.FCY,"L2,")
    XX5<1,1>[35,15] = FMT(TOT.LCY,"L2,")
    PRINT XX5<1,1>
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT.CREATED "   ; CALL REM
    RETURN

*-----------------------------INITIALIZATIONS------------------------
INITIATE:

    REPORT.ID = 'FT.JPY.99'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.ST = 'FBNK.STMT.ENTRY' ; F.ST = '' ; R.ST = ''
    CALL OPF( FN.ST,F.ST)

    FN.CO = 'FBNK.TRANSACTION' ; F.CO = '' ; R.CO = ''
    CALL OPF( FN.CO,F.CO)

    ETEXT     = ''
    ETEXT1    = ''
    ETEXTM    = ''
    ETEXTM1   = ''
    I         = 1
    II        = 1
    K         = 1
    J         = 1
    FLAG.PR   = 0
    DATA.FLAG = 0
    TOT.LCY   = 0
    TOT.FCY   = 0
    YTEXT = "Enter Account No. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "ANY")
    ACCT.ID = COMI
    IF  ACCT.ID EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter Account No."; CALL REM
        RETURN
    END

    YTEXT = "Enter Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    STR.DATE = COMI
    IF  STR.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter Start date"; CALL REM
        RETURN
    END
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    END.DATE = COMI
    IF END.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END

    CALL F.READ( FN.AC,ACCT.ID, R.AC, F.AC, ETEXT1)
    Y.CUS.ACC.ID   = ACCT.ID
    AC.CUR         = R.AC<AC.CURRENCY>
    CUS.ID         = R.AC<AC.CUSTOMER>

    CALL F.READ( FN.CUS,CUS.ID, R.CUS, F.CUS, ETEXT)
    NAME1      = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
    DEPT.CODE  = R.CUS<EB.CUS.DEPT.CODE>
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.CODE,BR.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT.CODE,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BR.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    BR.NAME.N  = FIELD(BR.NAME,'.',2)

    GOSUB PRINT.HEAD

*?    T.SEL  = "SELECT FBNK.ACCOUNT WITH @ID EQ ":ACCT.ID:" BY @ID"

*?    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ETEXT)

*****************************
*?    IF SELECTED THEN
    GOSUB PROCESS
*?    END
*?    ELSE
*?        FLAG.PR = 1
*?    END

    IF FLAG.PR = 1 THEN
        TEXT = "�� ���� ����� " :ACCT.ID ; CALL REM
        XX4 = SPACE(132)
        XX4 = ''
        PRINT XX4<1,1>
        XX4<1,1>[50,50]  = "�� ���� ����� ":ACCT.ID
        PRINT XX4<1,1>
    END

    RETURN

*------------------------SELECT ALL CUSTOMERS ----------------------
PROCESS:
    OLD.1    = ''
    OLD.2    = ''
    OLD.3    = ''
    OLD.ID   = ''
    OLD.BIRTH = ''
    OLD.BR    = ''
    OLD.NAME  = ''
    NO.PRINT  = 0
    ERR       = ''
    SEL.LIST  = ''
    I         = 0
    OPENING.BAL = ''

*?    FOR I = 1 TO SELECTED


    CALL EB.ACCT.ENTRY.LIST(Y.CUS.ACC.ID<1>,STR.DATE,END.DATE,SEL.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE REC.ID FROM SEL.LIST SETTING SUBVAL
    WHILE REC.ID:SUBVAL
        CALL F.READ(FN.ST,REC.ID,R.ST,F.ST,ERR.STMT)
****************
        IF ERR.STMT EQ '' THEN
            ST.AUTH      = R.ST<AC.STE.AUTHORISER>
            ST.INP       = R.ST<AC.STE.INPUTTER>
            ST.CAT       = R.ST<AC.STE.PRODUCT.CATEGORY>
            ST.AMT.FCY   = R.ST<AC.STE.AMOUNT.FCY>
            ST.AMT.LCY   = R.ST<AC.STE.AMOUNT.LCY>
            ST.ID        = R.ST<AC.STE.OUR.REFERENCE>
            ST.DATE      = R.ST<AC.STE.VALUE.DATE>
            ST.B.DAT     = R.ST<AC.STE.BOOKING.DATE>
            AUTH.NO      = FIELD(ST.AUTH,'_',2)
            INP.NO       = FIELD(ST.INP,'_',2)
            TRNS.CO      = R.ST<AC.STE.TRANSACTION.CODE>
            CALL F.READ(FN.CO,TRNS.CO,R.CO,F.CO, ETEXT3)
            CO.NAME     = R.CO<AC.TRA.NARRATIVE>
            NAME2       = CO.NAME<1,2>
            IF AUTH.NO EQ 'SCB.1724' OR  AUTH.NO EQ 'SCB.14249' OR  AUTH.NO EQ 'SCB.60135' THEN
                IF ST.AMT.FCY LT 0 THEN
*    ST.TYPE = '���'
* END
* ELSE
*     ST.TYPE = '�����'
* END
                    IF CUS.ID[1,1] EQ 0 THEN
                        CUS.ID = CUS.ID[2,7]
                    END

                    TOT.LCY += ST.AMT.LCY
                    TOT.FCY += ST.AMT.FCY

                    GOSUB PRNT
                END
            END
        END
    REPEAT
*?    NEXT I
    RETURN
*******************************************************

PRNT:
*----
    XX3 = SPACE(132)
    XX3 = ''
*?    PRINT XX3<1,1>
    K++
    XX3<1,K>[1,20]    = ST.ID
    XX3<1,K>[20,10]   = FMT(ST.AMT.FCY,"L2,")
    XX3<1,K>[35,10]   = FMT(ST.AMT.LCY,"L2,")
    XX3<1,K>[50,35]   = NAME2
    ST.BOOK.DATE      = FMT(ST.B.DAT,"####/##/##")
    XX3<1,K>[85,10]   = ST.BOOK.DATE
    XX3<1,K>[100,15]  = INP.NO
    XX3<1,K>[115,15]  = AUTH.NO
    PRINT XX3<1,K>
***************************

    RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
*----------
*Line [ 247 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    STR.DAT1= STR.DATE[7,2]:'/':STR.DATE[5,2]:"/":STR.DATE[1,4]
    END.DAT1= END.DATE[7,2]:'/':END.DATE[5,2]:"/":END.DATE[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"FT.JPY.99"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"����� ����� ����� ����� ":NAME1
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(45):"���� �� ":":":"SCB.14249":SPACE(2):"/":"SCB.1724":SPACE(2):"/":"SCB.60135"
    PR.HD :="'L'":SPACE(45):"�� :":SPACE(2):STR.DAT1:SPACE(2):"��� :":END.DAT1
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":":":ACCT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":":":NAME1
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� ������":":":AC.CUR
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"������":SPACE(10):"����":SPACE(10):"��� ������":SPACE(20):"����� ������":SPACE(5):"��� ������":SPACE(5):"��� ������"

    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD

    RETURN

************************************

END
