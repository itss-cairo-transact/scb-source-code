* @ValidationCode : Mjo3MDU5NTIzNjU6Q3AxMjUyOjE2NDA2OTc2NDAxNjE6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------------------------AHMED NAHRAWY (FOR EVER)-----------------------------------------
SUBROUTINE FT.NAME(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 27 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS

*  CALL DBR
*  NAME = R.NEW(FT.LOCA.REFS)<1,FTLR.BENEFICIARY.CUS>


*  ARG=ID.NEW
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,ID.NEW,NEW.LOC)
F.ITSS.FUNDS.TRANSFER = 'FBNK.FUNDS.TRANSFER'
FN.F.ITSS.FUNDS.TRANSFER = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER)
CALL F.READ(F.ITSS.FUNDS.TRANSFER,ID.NEW,R.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER,ERROR.FUNDS.TRANSFER)
NEW.LOC=R.ITSS.FUNDS.TRANSFER<FT.LOCAL.REF>
*Line [ 34 --> 36 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-28
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.BENEFICIARY.CUS",FTLR.BENEFICIARY.CUS.POS)
    ARG = NEW.LOC<1,FTLR.BENEFICIARY.CUS.POS>
*ARG = NEW.LOC<1,FTLR.BENEFICIARY.CUS>

RETURN
END
