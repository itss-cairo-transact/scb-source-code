* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTM5MTU6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
    SUBROUTINE FT.AR.CATEG.CR.PL1.NEW.1(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    CATEG=R.NEW(FT.DEBIT.ACCT.NO)
    BEGIN CASE
*  CASE CATEG = R.NEW(FT.DEBIT.ACCT.NO)
*      CATT   = CATEG [11,4]
*      CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
*      ARG    = CATTT
    CASE CATEG[1,2] EQ 'PL'
*VV  = LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 2
        XX  = R.NEW(FT.DEBIT.ACCT.NO)[3,10]
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XX,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATTT=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        ARG = CATTT
    CASE CATEG[1,3] EQ 'EGP'
        XX1 =R.NEW(FT.DEBIT.ACCT.NO)[4,5]
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX1,CATNAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XX1,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATNAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        TEXT = "YES " : CATNAME ;  CALL REM
        ARG=CATNAME
    CASE CATEG NE 'PL' OR CATEG NE 'EGP'
        CATT   = CATEG[11,4]
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTTT)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATTTT=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        TEXT = "NAME " : CATTT ; CALL REM
        ARG    = CATTTT
*   TEXT = "YES " ; CALL REM


    END CASE
    RETURN
END
