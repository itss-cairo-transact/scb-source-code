* @ValidationCode : MjoxNDkxNTAzNzQ5OkNwMTI1MjoxNjQwNjk3NjM0MjM4OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------AHMED ELNAHRAWY (FOR EVER)------------------------------------------
SUBROUTINE FT.CHARGE.AHMED (ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 26 ] Hashing I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS
*    FT.AMT = R.NEW(FT.DEBIT.AMOUNT,1)
*    FT.COMM = R.NEW(FT.COMMISION.AMT)<1,1>
*    FT.POS1 = R.NEW(FT.CHARGE.AMT)<1,1>
*    FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
*    FT.FEES =FT.AMT+ FT.COMM + FT.POS1 +FT.POS2

    AMT= R.NEW(FT.AMOUNT.DEBITED)[4,20]
    IF R.NEW(FT.DEBIT.ACCT.NO) NE '9943330010508001'  THEN
        XX= R.NEW(FT.AMOUNT.DEBITED)
        VV= LEN(R.NEW(FT.AMOUNT.DEBITED)) - 3
        BB = R.NEW(FT.AMOUNT.DEBITED)[4,VV]
** TEXT = " BB = " : BB ; CALL REM
        FT.AMT = R.NEW(FT.AMOUNT.DEBITED)<1,1>
        FT.AMT  = BB

        XX1 = R.NEW(FT.COMMISSION.AMT)<1,1>
        VV1 = LEN (R.NEW(FT.COMMISSION.AMT)<1,1>) - 3
        BB1 = R.NEW(FT.COMMISSION.AMT)<1,1>[4,VV1]
*** TEXT = " BB1 = " : BB1 ; CALL REM
        FT.COMM = BB1

        XX2  = R.NEW(FT.CHARGE.AMT)<1,1>
        VV2  = LEN (R.NEW(FT.CHARGE.AMT)<1,1>) - 3
        BB2  = R.NEW(FT.CHARGE.AMT)<1,1>[4,VV2]
*  TEXT = " BB2 = " : BB2 ; CALL REM
        FT.POS1 = BB2

        XX3  = R.NEW(FT.CHARGE.AMT)<1,2>
        VV3  = LEN (R.NEW(FT.CHARGE.AMT)<1,2>) - 3
        BB3  = R.NEW(FT.CHARGE.AMT)<1,2>[4,VV3]
*   TEXT = " BB3 = " : BB3 ; CALL REM
        FT.POS2 = BB3

* FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
        FT.FEES = FT.POS1 +FT.POS2

        ARG = FT.FEES
    END ELSE
        ARG = AMT
    END

RETURN
END
