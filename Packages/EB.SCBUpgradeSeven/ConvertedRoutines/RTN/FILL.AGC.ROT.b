* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgyNzMyMTk6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:37:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
    SUBROUTINE FILL.AGC.ROT(ENQ.DATA)
*    PROGRAM FILL.AGC.ROT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.XX.GEN.CONDITION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.AGC.GROUP.CATEG
*-------------------------------------------------------------------------
    FN.AGC    = 'FBNK.ACCT.GEN.CONDITION' ; F.AGC   = '' ; R.AGC   = ''
    FN.AGC.G  = 'F.SCB.AGC.GROUP.CATEG'   ; F.AGC.G = '' ; R.AGC.G = ''
    FN.CATG   = 'F.CATEGORY'              ; F.CATG  = '' ; R.CATG  = ''

    CALL OPF(FN.AGC,F.AGC)
    CALL OPF(FN.AGC.G,F.AGC.G)
    CALL OPF(FN.CATG,F.CATG)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*---
    T.SEL = "SELECT ":FN.AGC:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AGC,KEY.LIST<I>,R.AGC,F.AGC,E1)
            VALUE.CATEG = R.AGC<EB.XX.VALUE>
*Line [ 57 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            VAL.N = DCOUNT(VALUE.CATEG,@VM)

            FOR NN = 1 TO VAL.N
                CATG.ID = VALUE.CATEG<1,NN>

                NK = 1
                TEMP.ID = KEY.LIST<I> : "-" : CATG.ID<1,1,1>
                R.AGC.G<GRP.CATG.CATEGORY>   =  CATG.ID<1,1,NK>
                CATG.ID2 =  CATG.ID<1,1,NK>
                CALL F.READ(FN.CATG,CATG.ID2 , R.CATG, F.CATG, E22)

                R.AGC.G<GRP.CATG.CATEG.DESC> = R.CATG<EB.CAT.DESCRIPTION,2>
                IF R.AGC.G<GRP.CATG.CATEG.DESC> EQ '' THEN
                    R.AGC.G<GRP.CATG.CATEG.DESC> = R.CATG<EB.CAT.DESCRIPTION,1>
                END
                R.AGC.G<GRP.CATG.GROUP.NO>   =  KEY.LIST<I>

                CALL F.WRITE (FN.AGC.G, TEMP.ID, R.AGC.G)
                CALL JOURNAL.UPDATE(TEMP.ID)

***                WRITE  R.AGC.G TO F.AGC.G , TEMP.ID ON ERROR
***                END
            NEXT NN
        NEXT I
    END
*===BUILD ROUTINE==============================================
    SEL.ENQ = "SELECT F.SCB.AGC.GROUP.CATEG BY @ID"
    CALL EB.READLIST(SEL.ENQ,LIST.ENQ,"",SELECTED.ENQ,ER.MSG.ENQ)
    IF SELECTED.ENQ THEN
        FOR II = 1 TO SELECTED.ENQ
            ENQ.DATA<2,II> = "@ID"
            ENQ.DATA<3,II> = "EQ"
            ENQ.DATA<4,II> = LIST.ENQ<II>
        NEXT II
    END ELSE
        ENQ.DATA<2,1> = "@ID"
        ENQ.DATA<3,1> = "EQ"
        ENQ.DATA<4,1> = "DUMMY"
    END
    RETURN
*==============================================================
END
