* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MjI5MTc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TOT.CHRG.CUR(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

   CUR = R.NEW(FT.DEBIT.CURRENCY)
*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
**********************
  TOT.COMM = ""
      COMM.AMT = R.NEW(FT.COMMISSION.AMT)
*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
      CC = DCOUNT(COMM.AMT,@VM)
      FOR J = 1 TO CC
          TOT.COMM = TOT.COMM + COMM.AMT<1,J>[4,13]
          TEXT = TOT.COMM ; CALL REM
      NEXT J
      AR = AR + TOT.COMM

**********************

    TOT.CHRG = ""
    CHRG.AMT = R.NEW(FT.CHARGE.AMT)
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CG= DCOUNT(CHRG.AMT,@VM)
    FOR I = 1 TO CG
        TOT.CHRG = TOT.CHRG + CHRG.AMT<1,I>[4,13]
    NEXT I
    AG = AG + TOT.CHRG
    ARG = AR + AG
*************************************
    NET.AMOUNT = (ARG)
    IN.AMOUNT = ARG
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    ARG = OUT.AMOUNT:" ":CURR

    RETURN
END
