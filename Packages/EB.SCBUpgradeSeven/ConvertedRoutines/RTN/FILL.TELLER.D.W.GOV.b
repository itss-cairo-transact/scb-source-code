* @ValidationCode : MjotMzY1OTE2MjE0OkNwMTI1MjoxNjQ1OTU4MjczODQ3OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:37:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
** ----- NESSREEN AHMED 14/11/2016 -----
*-----------------------------------------------------------------------------
* <Rating>1217</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FILL.TELLER.D.W.GOV

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 33 ] Hashing I_TT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TELLER.D.W.GOVN
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.GOVERNORATE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY

    FN.TT.D.W = 'F.SCB.TELLER.D.W.GOVN' ; F.TT.D.W = '' ; R.TT.D.W = ''
    FN.TELLER = 'FBNK.TELLER$HIS'     ; F.TELLER = '' ; R.TELLER = ''
    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF( FN.TT.D.W,F.TT.D.W)
    CALL OPF( FN.TELLER,F.TELLER)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)

    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(9 62 53 42 66 37 38 7 32 33 71 101 102 901 910 2 54 41 40 86 84 82 8 36 34 25 100 20 63 21 17 11 12 92 14 97 64 105 95 107 98 65 106 108 96 912 81) AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CO.CODE"
***    TT.SEL = "SELECT FBNK.TELLER$HIS WITH CUSTOMER.1 NE '' AND CUSTOMER.1 NE '99433300' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CO.CODE"
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
**        FOR TTI = 1 TO 10
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = ''

            CALL F.READ( FN.TELLER,KEY.LIST.TT<TTI>, R.TELLER, F.TELLER, ETEXT.TT)
            TR.CODE<TTI>     = R.TELLER<TT.TE.TRANSACTION.CODE>
            CURR<TTI>        = R.TELLER<TT.TE.CURRENCY.1>
            AMT.LCY<TTI>     = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            AMT.FCY<TTI>     = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CUST.ID<TTI>     = R.TELLER<TT.TE.CUSTOMER.1>
            TRN.DAT<TTI>     = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO.1<TTI>   = R.TELLER<TT.TE.ACCOUNT.1>
            ACCT.NO.2<TTI>   = R.TELLER<TT.TE.ACCOUNT.2>
            COMP.CODE<TTI>   = R.TELLER<TT.TE.CO.CODE>
            DR.CR.MARKER<TTI>= R.TELLER<TT.TE.DR.CR.MARKER>
            CONTRACT.GRP<TTI>= R.TELLER<TT.TE.CONTRACT.GRP>
            DATE.TIMEE<TTI>   = R.TELLER<TT.TE.DATE.TIME><1,1>

            BEGIN CASE

***         CASE DR.CR.MARKER<TTI> EQ 'DEBIT'
                CASE TR.CODE<TTI> EQ 2 OR TR.CODE<TTI> EQ 54 OR TR.CODE<TTI> EQ 41 OR TR.CODE<TTI> EQ 40 OR TR.CODE<TTI> EQ 86 OR TR.CODE<TTI> EQ 84 OR TR.CODE<TTI> EQ 82 OR TR.CODE<TTI> EQ 8 OR TR.CODE<TTI> EQ 36 OR TR.CODE<TTI> EQ 34 OR TR.CODE<TTI> EQ 25 OR TR.CODE<TTI> EQ 100 OR TR.CODE<TTI> EQ 20 OR TR.CODE<TTI> EQ 63 OR TR.CODE<TTI> EQ 21 OR TR.CODE<TTI> EQ 17 OR TR.CODE<TTI> EQ 11 OR TR.CODE<TTI> EQ 12 OR TR.CODE<TTI> EQ 92 OR TR.CODE<TTI> EQ 14 OR TR.CODE<TTI> EQ 97 OR TR.CODE<TTI> EQ 64 OR TR.CODE<TTI> EQ 105 OR TR.CODE<TTI> EQ 95 OR TR.CODE<TTI> EQ 107 OR TR.CODE<TTI> EQ 98 OR TR.CODE<TTI> EQ 65 OR TR.CODE<TTI> EQ 106 OR TR.CODE<TTI> EQ 108 OR TR.CODE<TTI> EQ 96 OR TR.CODE<TTI> EQ 912 OR TR.CODE<TTI> EQ 81
                    DW.FLAG<TTI> = 'W'
***         CASE DR.CR.MARKER<TTI> EQ 'CREDIT'
                CASE TR.CODE<TTI> EQ 9 OR TR.CODE<TTI> EQ 62 OR TR.CODE<TTI> EQ 53 OR TR.CODE<TTI> EQ 42 OR TR.CODE<TTI> EQ 66 OR TR.CODE<TTI> EQ 37 OR TR.CODE<TTI> EQ 38 OR TR.CODE<TTI> EQ 7 OR TR.CODE<TTI> EQ 32 OR TR.CODE<TTI> EQ 33 OR TR.CODE<TTI> EQ 71 OR TR.CODE<TTI> EQ 101 OR TR.CODE<TTI> EQ 102 OR TR.CODE<TTI> EQ 901 OR TR.CODE<TTI> EQ 910
                    DW.FLAG<TTI> = 'D'
            END CASE

            AUTH.DATE<TTI> =  R.TELLER<TT.TE.AUTH.DATE>
**      CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
**      NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
            IF CUST.ID # '' THEN
                IF CONTRACT.GRP<TTI> NE '' THEN
                    SC.FLAG<TTI> = '2'
                END ELSE
                    SC.FLAG<TTI> = '1'
                END
            END ELSE
                SC.FLAG = '1'
            END
            BR = COMP.CODE<TTI>[2]
            KEY.TO.USE = KEY.LIST.TT<TTI>[1,12]
            CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
            R.TT.D.W<DWGR.TR.FLAG>    = DW.FLAG<TTI>
            R.TT.D.W<DWGR.ID.REF>     = KEY.LIST.TT<TTI>[1,12]
            R.TT.D.W<DWGR.TR.CURRENCY> = CURR<TTI>
            R.TT.D.W<DWGR.AMT.LCY>    = AMT.LCY<TTI>
            R.TT.D.W<DWGR.AMT.FCY>    = AMT.FCY<TTI>
            IF AMT.FCY<TTI> NE '' THEN
                IF CURR<TTI> EQ 'USD' THEN
                    EQV.USD = AMT.FCY<TTI>
                END ELSE
                    KEY.ID<TTI> = "USD":"-":"EGP":"-":AUTH.DATE<TTI>
                    CALL F.READ(FN.CURR.DALY,KEY.ID<TTI>,R.CURR.DALY,F.CURR.DALY,ERR2)
                    DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
                    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MM THEN
                        SELL.RATE<TTI> = R.CURR.DALY<SCCU.SELL.RATE,MM>
                    END
                    EQV.USD = AMT.LCY<TTI> / SELL.RATE<TTI>
                END ;** END OF OTHER FCY**
                R.TT.D.W<DWGR.EQV.USD>     = EQV.USD
            END
            R.TT.D.W<DWGR.SEC.FLAG>    = SC.FLAG<TTI>
            R.TT.D.W<DWGR.ACCOUNT.1>   = ACCT.NO.1<TTI>
            R.TT.D.W<DWGR.ACCOUNT.2>   = ACCT.NO.2<TTI>
            R.TT.D.W<DWGR.AUTH.DATE>   = AUTH.DATE<TTI>
            R.TT.D.W<DWGR.COMP.CO>     = COMP.CODE<TTI>
            R.TT.D.W<DWGR.BANK.CUS>    = 'YES'

*Line [ 149 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP.CODE<TTI>,BR.GOVRN)
F.ITSS.SCB.BR.GOVERNORATE = 'F.SCB.BR.GOVERNORATE'
FN.F.ITSS.SCB.BR.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.BR.GOVERNORATE,COMP.CODE<TTI>,R.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE,ERROR.SCB.BR.GOVERNORATE)
BR.GOVRN=R.ITSS.SCB.BR.GOVERNORATE<BRGR.BR.GOVERNORATE>
            R.TT.D.W<DWGR.BR.GOVR>     = BR.GOVRN
            R.TT.D.W<DWGR.DD.TIME>     = DATE.TIMEE<TTI>

            CALL F.WRITE(FN.TT.D.W,KEY.TO.USE,R.TT.D.W)
            CALL JOURNAL.UPDATE(KEY.TO.USE)
        NEXT TTI
    END
*****FOR BUY AND SELL **********************************
    CC.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(23 67 58 24 68) AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CO.CODE"
**************************************************
    KEY.LIST.CC=""
    SELECTED.CC=""
    ER.MSG.CC=""

    CALL EB.READLIST(CC.SEL,KEY.LIST.CC,"",SELECTED.CC,ER.MSG.CC)
    TEXT = 'SELECTED.CC=':SELECTED.CC ; CALL REM
    IF SELECTED.CC THEN
        FOR SS = 1 TO SELECTED.CC
**        FOR SS = 1 TO 10
            TR.CODE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; CUST.ID = '' ; RATE = ''
            TRN.DAT = '' ; OUR.REF = '' ; ACCT.NO.1 = '' ; ACCT.NO.2 = ''
            COMP.CODE = '' ;  KEY.TO.USE = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = '' ; DW.FLAG = '' ; SC.FLAG = '' ; V.ACC = ''
            NEW.SEC = '' ; BR = ''
            CALL F.READ( FN.TELLER,KEY.LIST.CC<SS>, R.TELLER, F.TELLER, ETEXT.TT)
            TR.CODE   = R.TELLER<TT.TE.TRANSACTION.CODE>
            CURR.1    = R.TELLER<TT.TE.CURRENCY.1>
            CURR.2    = R.TELLER<TT.TE.CURRENCY.2>
            AMT.LCY   = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            AMT.FCY   = R.TELLER<TT.TE.AMOUNT.FCY.1>
            CUST.ID   = R.TELLER<TT.TE.CUSTOMER.1>
            RATE      = R.TELLER<TT.TE.RATE.1>
            TRN.DAT   = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO.1 = R.TELLER<TT.TE.ACCOUNT.1>
            ACCT.NO.2 = R.TELLER<TT.TE.ACCOUNT.2>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>
*Line [ 192 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP.CODE,BR.GOVRN)
F.ITSS.SCB.BR.GOVERNORATE = 'F.SCB.BR.GOVERNORATE'
FN.F.ITSS.SCB.BR.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.BR.GOVERNORATE,COMP.CODE,R.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE,ERROR.SCB.BR.GOVERNORATE)
BR.GOVRN=R.ITSS.SCB.BR.GOVERNORATE<BRGR.BR.GOVERNORATE>
            DATE.TIMEE =  R.TELLER<TT.TE.DATE.TIME><1,1>

            BEGIN CASE
                CASE TR.CODE EQ 23 OR TR.CODE EQ 67 OR TR.CODE EQ 58
                    KEY.TO.USE.C = KEY.LIST.CC<SS>[1,12]:'-W'
                    DW.FLAG = 'W'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    V.ACC = CURR.2:'10000':BR:'9900':BR
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE.C, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<DWGR.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<DWGR.ID.REF>     = KEY.LIST.CC<SS>[1,12]
                    R.TT.D.W<DWGR.TR.CURRENCY> = CURR.2
                    R.TT.D.W<DWGR.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<DWGR.AMT.FCY>    = AMT.FCY
                    IF CURR.2 NE 'EGP' THEN
                        IF CURR.2 EQ 'USD' THEN
                            EQV.USD = AMT.FCY
                        END ELSE
                            KEY.ID = "USD":"-":"EGP":"-":TRN.DAT
                            CALL F.READ(FN.CURR.DALY,KEY.ID,R.CURR.DALY,F.CURR.DALY,ERR2)
                            DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
                            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MM THEN
                                SELL.RATE = R.CURR.DALY<SCCU.SELL.RATE,MM>
                            END
                            EQV.USD = AMT.LCY / SELL.RATE
                        END       ;** END OF OTHER FCY**
                        R.TT.D.W<DWGR.EQV.USD>     = EQV.USD
                    END
                    R.TT.D.W<DWGR.NEW.SECTOR> = ''
                    R.TT.D.W<DWGR.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<DWGR.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<DWGR.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<DWGR.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<DWGR.COMP.CO>    = COMP.CODE
                    R.TT.D.W<DWGR.BANK.CUS>    = 'YES'
                    R.TT.D.W<DWGR.BR.GOVR>     = BR.GOVRN
                    R.TT.D.W<DWGR.DD.TIME>     = DATE.TIMEE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
                    CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                    KEY.TO.USE.C = ''
                    KEY.TO.USE.C = KEY.LIST.CC<SS>[1,12]: '-D'
                    DW.FLAG = 'D'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE.C, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<DWGR.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<DWGR.ID.REF>     = KEY.LIST.CC<SS>[1,12]
                    R.TT.D.W<DWGR.TR.CURRENCY> = CURR.1
                    R.TT.D.W<DWGR.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<DWGR.AMT.FCY>    = AMT.FCY
                    IF CURR.1 NE 'EGP' THEN
                        IF CURR.1 EQ 'USD' THEN
                            EQV.USD = AMT.FCY
                        END ELSE
                            KEY.ID = "USD":"-":"EGP":"-":TRN.DAT
                            CALL F.READ(FN.CURR.DALY,KEY.ID,R.CURR.DALY,F.CURR.DALY,ERR2)
                            DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
                            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MM THEN
                                SELL.RATE = R.CURR.DALY<SCCU.SELL.RATE,MM>
                            END
                            EQV.USD = AMT.LCY / SELL.RATE
                        END       ;** END OF OTHER FCY**
                        R.TT.D.W<DWGR.EQV.USD>     = EQV.USD
                    END
                    R.TT.D.W<DWGR.NEW.SECTOR> = ''
                    R.TT.D.W<DWGR.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<DWGR.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<DWGR.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<DWGR.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<DWGR.COMP.CO>    = COMP.CODE
                    R.TT.D.W<DWGR.BANK.CUS>    = 'YES'
                    R.TT.D.W<DWGR.BR.GOVR>     = BR.GOVRN
                    R.TT.D.W<DWGR.DD.TIME>     = DATE.TIMEE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
                    CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                CASE TR.CODE EQ 24 OR TR.CODE EQ 68
                    KEY.TO.USE.C = KEY.LIST.CC<SS>[1,12]: '-W'
                    DW.FLAG = 'W'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE.C, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<DWGR.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<DWGR.ID.REF>     = KEY.LIST.TT<TTI>[1,12]
                    R.TT.D.W<DWGR.TR.CURRENCY> = CURR.1
                    R.TT.D.W<DWGR.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<DWGR.AMT.FCY>    = AMT.FCY
                    IF CURR.1 NE 'EGP' THEN
                        IF CURR.1 EQ 'USD' THEN
                            EQV.USD = AMT.FCY
                        END ELSE
                            KEY.ID = "USD":"-":"EGP":"-":TRN.DAT
                            CALL F.READ(FN.CURR.DALY,KEY.ID,R.CURR.DALY,F.CURR.DALY,ERR2)
                            DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
                            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MM THEN
                                SELL.RATE = R.CURR.DALY<SCCU.SELL.RATE,MM>
                            END
                            EQV.USD = AMT.LCY / SELL.RATE
                        END       ;** END OF OTHER FCY**
                        R.TT.D.W<DWGR.EQV.USD>     = EQV.USD
                    END
                    R.TT.D.W<DWGR.NEW.SECTOR> = ''
                    R.TT.D.W<DWGR.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<DWGR.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<DWGR.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<DWGR.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<DWGR.COMP.CO>    = COMP.CODE
                    R.TT.D.W<DWGR.BANK.CUS>    = 'YES'
                    R.TT.D.W<DWGR.BR.GOVR>     = BR.GOVRN
                    R.TT.D.W<DWGR.DD.TIME>     = DATE.TIMEE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
                    CALL JOURNAL.UPDATE(KEY.TO.USE.C)

                    KEY.TO.USE.C = ''
                    KEY.TO.USE.C = KEY.LIST.CC<SS>[1,12]: '-D'
                    DW.FLAG = 'D'
                    SC.FLAG = '2'
                    BR = COMP.CODE[2]
                    CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
                    R.TT.D.W<DWGR.TR.FLAG>    = DW.FLAG
                    R.TT.D.W<DWGR.ID.REF>     = KEY.LIST.TT<TTI>[1,12]
                    R.TT.D.W<DWGR.TR.CURRENCY> = CURR.2
                    R.TT.D.W<DWGR.AMT.LCY>    = AMT.LCY
                    R.TT.D.W<DWGR.AMT.FCY>    = AMT.FCY
                    IF CURR.2 NE 'EGP' THEN
                        IF CURR.2 EQ 'USD' THEN
                            EQV.USD = AMT.FCY
                        END ELSE
                            KEY.ID = "USD":"-":"EGP":"-":TRN.DAT
                            CALL F.READ(FN.CURR.DALY,KEY.ID,R.CURR.DALY,F.CURR.DALY,ERR2)
                            DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
                            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MM THEN
                                SELL.RATE = R.CURR.DALY<SCCU.SELL.RATE,MM>
                            END
                            EQV.USD = AMT.LCY / SELL.RATE
                        END       ;** END OF OTHER FCY**
                        R.TT.D.W<DWGR.EQV.USD>     = EQV.USD
                    END
                    R.TT.D.W<DWGR.NEW.SECTOR> = ''
                    R.TT.D.W<DWGR.SEC.FLAG>   = SC.FLAG
                    R.TT.D.W<DWGR.ACCOUNT.1>  = ACCT.NO.1
                    R.TT.D.W<DWGR.ACCOUNT.2>  = ACCT.NO.2
                    R.TT.D.W<DWGR.AUTH.DATE>   = TRN.DAT
                    R.TT.D.W<DWGR.COMP.CO>    = COMP.CODE
                    R.TT.D.W<DWGR.BANK.CUS>    = 'YES'
                    R.TT.D.W<DWGR.BR.GOVR>     = BR.GOVRN
                    R.TT.D.W<DWGR.DD.TIME>     = DATE.TIMEE

                    CALL F.WRITE(FN.TT.D.W,KEY.TO.USE.C,R.TT.D.W)
                    CALL JOURNAL.UPDATE(KEY.TO.USE.C)
            END CASE
        NEXT SS
    END
RETURN
END
