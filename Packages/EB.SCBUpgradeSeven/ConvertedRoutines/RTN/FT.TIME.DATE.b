* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MjI1MjM6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.TIME.DATE(TIMU)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    CHK.VAL = ""
    CHK.VAL = FIELD(TIMU,";",2)
    IF CHK.VAL THEN
*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DATE.TIME,TIMU,DAT.TIME)
F.ITSS.FUNDS.TRANSFER$HIS = 'F.FUNDS.TRANSFER$HIS'
FN.F.ITSS.FUNDS.TRANSFER$HIS = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS)
CALL F.READ(F.ITSS.FUNDS.TRANSFER$HIS,TIMU,R.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS,ERROR.FUNDS.TRANSFER$HIS)
DAT.TIME=R.ITSS.FUNDS.TRANSFER$HIS<FT.DATE.TIME>
    END ELSE
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('FUNDS.TRANSFER':@FM:FT.DATE.TIME,TIMU,DAT.TIME)
F.ITSS.FUNDS.TRANSFER = 'FBNK.FUNDS.TRANSFER'
FN.F.ITSS.FUNDS.TRANSFER = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER)
CALL F.READ(F.ITSS.FUNDS.TRANSFER,TIMU,R.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER,ERROR.FUNDS.TRANSFER)
DAT.TIME=R.ITSS.FUNDS.TRANSFER<FT.DATE.TIME>
    END
    TIM   = DAT.TIME[7,4]
    TIMHS = FMT(TIM,"R##:##")
    HH    = TIMHS[4,2]
    SS    = TIMHS[1,2]
    TIMU  = SS:":":HH

    RETURN
END
