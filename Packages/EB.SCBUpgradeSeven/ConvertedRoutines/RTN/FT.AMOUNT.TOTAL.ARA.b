* @ValidationCode : MjotODgxMTM1ODA2OkNwMTI1MjoxNjQwNjk3NjMyNTUzOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*------------------------------AHMED NAHRAWY (FOR EVER)---------------
SUBROUTINE FT.AMOUNT.TOTAL.ARA(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Adding Currency Layout - ITSS - R21 Upgrade - 2021-12-27
    $INCLUDE I_F.CURRENCY
*  DD = DCOUNT(ARG,VM)
    ACCTCH=R.NEW(FT.CHARGES.ACCT.NO)
    TEXT = "ACC :" :ACCTCH ; CALL REM
    TOT    = R.NEW(FT.TOTAL.CHARGE.AMOUNT)[4,10]
    TOT1   = LEN(TOT) -3
    MMM    = LEN(ARG) -3
    CUR    = R.NEW(FT.DEBIT.CURRENCY)
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

    IF ACCTCH NE '' THEN
        AMOUNT = ARG[4,MMM]+TOT
        IN.AMOUNT  = AMOUNT
        TEXT = IN.AMOUNT ; CALL REM
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"
***  TEXT = ARG ; CALL REM
    END

    IF ACCTCH EQ '' THEN
        AMOUNT = ARG[4,MMM]
        IN.AMOUNT  = AMOUNT
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"

    END
    TEXT = "END " ; CALL REM
RETURN
END
