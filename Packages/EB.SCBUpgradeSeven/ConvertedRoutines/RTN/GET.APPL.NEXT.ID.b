* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MjQ2MTM6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>1429</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE GET.APPL.NEXT.ID(APPL.NAME, APPL.PREF, NEW.ID, ER.MSG)

* Written by V.Papadopoulos for INFORMER S.A.

* 23/05/00 (N.Betsias) PD.CAPTURE id couldn't processed.

* UPDATED BY P.KARAKITSOS - 27/08/02
* SO AS TO TAKE CARE OF NEW FT ID IF USED BY THE BANK

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LOCKING
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*
* PK - 27/08/02
*
    IF APPL.PREF = "FT" THEN
        NEW.FT.ID = "" ; ER.MSG = ""
        CALL CHECK.FOR.NEW.FT.ID(NEW.FT.ID,ER.MSG)
        TEXT = ''
    END
    IF NOT(NEW.FT.ID) & NOT(ER.MSG) THEN
*
* PK - 27/08/02
*
        I.A = 1 ; TEMP = '' ; ER.MSG = ''
        LOOP WHILE NOT(TEMP) & NOT(ER.MSG)

            NEW.ID = '' ; R.LOCK = '' ; TEMP = ''

            R.LOCK = ''       ;*JM
            READU R.LOCK FROM F.LOCKING,APPL.NAME THEN

                NEW.ID = R.LOCK<EB.LOK.CONTENT, 1>
*            TEXT = 'GET ID = ':NEW.ID ; CALL REM
******* N.B
                FLG = '' ; G= 1 ; START.POS = ''
                LOOP WHILE G <= 25 & NOT(FLG)
                    IF NUM(NEW.ID[G, 1]) THEN
                        START.POS = G
                        FLG = 'TRUE'
                    END
                    G += 1
                REPEAT
                IF NOT(START.POS) THEN START.POS = 3
********* N.B

                JUL.DATE = R.DATES(EB.DAT.JULIAN.DATE)
                IF NEW.ID[START.POS,5] = JUL.DATE[3,5] THEN
                    NXT.SEQ = NEW.ID[5] + 1
                    IF LEN(NXT.SEQ) <= 5 THEN
                        NXT.SEQ = STR('0',5 - LEN(NXT.SEQ)):NXT.SEQ
                        NEW.ID = NEW.ID[1,7]:NXT.SEQ
                    END
                    ELSE
                        ER.MSG = 'CAN NOT GET NEXT ID'
                    END
                END
                ELSE
                    NEW.ID = APPL.PREF:JUL.DATE[3,5]:'00001'
                END

                IF NOT(ER.MSG) THEN
                    TELIA = '' ; TELIA = INDEX(APPL.NAME, '.', 1)
                    AP.NAME = '' ; RET.ID = ''
                    AP.NAME<1> = APPL.NAME[TELIA + 1, LEN(APPL.NAME) - TELIA]
                    AP.NAME<2> = NEW.ID[5]

                    CALL GET.NEXT.SEQ.APPL.ID(AP.NAME, '', APPL.PREF, RET.ID)
                    IF RET.ID THEN
                        NEW.ID = '' ; NEW.ID = RET.ID
                        R.LOCK<EB.LOK.CONTENT, 1> = RET.ID

                        R.TEMP = '' ; V.TEMP = '' ; TEMP = ''
                        CALL OPF(APPL.NAME:'$NAU', V.TEMP)

                        READU R.TEMP FROM V.TEMP,RET.ID LOCKED TEMP = '1' THEN
*Line [ 100 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                            NULL
                        END
                        ELSE
*Line [ 104 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                            NULL
                        END

                        CLOSE V.TEMP

                        IF NOT(TEMP) THEN
                            TEMP = 'YES'
                            CALL F.WRITE (APPL.NAME, APPL.NAME, R.LOCK)
                            CALL JOURNAL.UPDATE(APPL.NAME)

 ***                           WRITE R.LOCK TO F.LOCKING,APPL.NAME ON ERROR
 ***                               ER.MSG = 'ERROR !!! WRITE ':APPL.NAME
 ***                           END
                        END
                        ELSE
                            TEMP = ''
                        END
                    END
                    ELSE
                        NEW.ID = ''
                        ER.MSG = 'ERROR !!! GET NEXT ID'
                    END
                END
            END
            ELSE
                ER.MSG = 'ERROR !!! READ ':APPL.NAME
*            TEXT = "GET ":ER.MSG ; CALL REM
            END

        REPEAT
*
* PK - 27/08/02
*
    END
    ELSE NEW.ID = NEW.FT.ID
*
* PK - 27/08/02
*
    RETURN

END
