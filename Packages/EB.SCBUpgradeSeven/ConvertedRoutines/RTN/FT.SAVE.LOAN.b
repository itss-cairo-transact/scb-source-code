* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgzMDY4NDg6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE FT.SAVE.LOAN
*1-TO CALL DAEL SLIP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.GOV
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    FN.GOV = 'F.SCB.LOANS.GOV'; F.GOV = ''
    CALL OPF(FN.GOV,F.GOV)

***    OPEN FN.GOV TO FVAR.GOV ELSE
***        TEXT = "ERROR OPEN FILE" ; CALL REM
***        RETURN
***    END

    CUST       = R.NEW(FT.DEBIT.CUSTOMER)
    Y.M        = R.NEW(FT.LOCAL.REF)<1,FTLR.CHQ.DATE>
    YEAR.MONTH = Y.M[1,6]

    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='INAU' THEN
        T.SEL ="SELECT F.SCB.LOANS.GOV WITH PAY.CODE EQ 13 AND INSTALLEMENT.DATE LIKE ":YEAR.MONTH:"... AND CUSTOMER.NO LIKE ...":CUST
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            R.GOV<GOV.PAY.CODE> = '20'
            CALL F.WRITE (FN.GOV,KEY.LIST<I>, R.GOV)
***           WRITE R.GOV TO FVAR.GOV , KEY.LIST<I>  ON ERROR
***               STOP 'CAN NOT WRITE RECORD ':KEY.LIST<I>:' TO FILE ':FN.GOV
***           END

        NEXT I
    END
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='RNAU' THEN
        T.SEL ="SELECT F.SCB.LOANS.GOV WITH PAY.CODE EQ 20 AND INSTALLEMENT.DATE LIKE ":YEAR.MONTH:"... AND CUSTOMER.NO LIKE ...":CUST
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            R.GOV<GOV.PAY.CODE> = '13'
            CALL F.WRITE (FN.GOV,KEY.LIST<I>, R.GOV)
***           WRITE R.GOV TO FVAR.GOV , KEY.LIST<I>  ON ERROR
***               STOP 'CAN NOT WRITE RECORD ':KEY.LIST<I>:' TO FILE ':FN.GOV
***           END

        NEXT I

    END
    RETURN
END
