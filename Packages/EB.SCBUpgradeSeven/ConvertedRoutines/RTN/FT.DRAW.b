* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTY1MjU6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------------------------------------NI7OOOOOOOOOOOO---------------------------------------
    SUBROUTINE FT.DRAW(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    COMP = ID.COMPANY

    FN.AC = 'FBNK.FUNDS.TRANSFER$HIS'
    F.AC  = ''
    CALL OPF(FN.AC,F.AC)

   ** T.SEL= "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DEBIT.ACCT.NO EQ 9942310020200102 (DEBIT.THEIR.REF LIKE ...SCE...  OR CREDIT.THEIR.REF LIKE ...SCE...) AND CO.CODE EQ ":COMP
    T.SEL= "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DEBIT.ACCT.NO EQ 9942310020200102 AND (DEBIT.THEIR.REF LIKE ...SCE... OR CREDIT.THEIR.REF LIKE ...SCE...)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL : " : SELECTED ; CALL REM
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ<2,ENQ.LP> = '@ID'
        ENQ<3,ENQ.LP> = 'EQ'
        ENQ<4,ENQ.LP> = 'DUMMY'
    END
**********************
    RETURN
END
