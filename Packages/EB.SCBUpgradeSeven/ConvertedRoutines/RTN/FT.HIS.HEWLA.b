* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgyOTQ2NTA6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-- CREATE BY RIHAM
    SUBROUTINE FT.HIS.HEWLA

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------

    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='FT.HIS.HEWLA'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID = COMI
    FN.DR = 'FBNK.FUNDS.TRANSFER$HIS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    TEXT = "COMI = " : COMI ; CALL REM
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATE.TO = TODAY[3,6]:"..."

*------------------------------------------------------------------------
    XX   = SPACE(132)  ; XX1  = SPACE(132);
    XX2  = SPACE(132)  ; XX3  = SPACE(132);
    XX4  = SPACE(132)  ; XX5  = SPACE(132);
    XX6  = SPACE(132)  ; XX7  = SPACE(132);
    XX8  = SPACE(132)  ; XX9  = SPACE(132);
    XX10  = SPACE(132) ; XX11  = SPACE(132);
    XX12  = SPACE(132) ; XX13  = SPACE(132);
    XX14  = SPACE(132) ; XX15  = SPACE(132);
    XX16  = SPACE(132) ; XX17  = SPACE(132);
    XX18  = SPACE(132) ; XX19 = SPACE(132);
    XX20  = SPACE(132)
*---------------------------------------------------
    DB.ACC = R.DR<FT.DEBIT.ACCT.NO>
    CR.ACC = R.DR<FT.CREDIT.ACCT.NO>
    TEXT = "DB.ACC = " : DB.ACC ; CALL REM
    ACC.NO = DB.ACC
    IF ACC.NO[1,3] NE 'EGP' OR ACC.NO[1,2] NE 'PL' THEN
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
        TEXT = "CUS.ID =" : CUS.ID ; CALL REM
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 121 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BR=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
        CUST.NAME   = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.NAME.2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
*Line [ 138 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<1,2>,CAT.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>

        XX1<1,1>[3,35]  = CUST.NAME

        XX2<1,1>[3,35]  = CUST.ADDRESS

        XX3<1,1>[3,15]  = '��� ������ : '
        XX3<1,1>[45,35] = CATEG

    END
    IF ACC.NO[1,2] EQ 'PL' THEN
        CAT.ID = ACC.NO[3,5]
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<1,2>,CAT.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
        XX1<1,1>[3,75]   = CATEG
        XX2<1,1>[3,35]   = CATEG

        XX3<1,1>[3,15]   = '��� ������ : '
        XX3<1,1>[45,35]   = CATEG

    END

*    DR.AMOUNT = R.DR<FT.AMOUNT.DEBITED>[4,20]
    DR.AMOUNT = R.DR<FT.DEBIT.AMOUNT>
    CR.AMOUNT = R.DR<FT.AMOUNT.CREDITED>[4,20]
    AMOUNT    = DR.AMOUNT
    DAT       = R.DR<FT.DEBIT.VALUE.DATE>

    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    BR.DATA   = R.DR<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,1>
    BR.DATA2  = R.DR<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,2>


    REFER     = R.DR<FT.DEBIT.THEIR.REF>
    BRANCH1   = R.DR<FT.DEPT.CODE>
*********************************
    VV= LEN(R.DR<FT.AMOUNT.DEBITED>) - 3
    BB = R.DR<FT.AMOUNT.DEBITED>[4,VV]
    RR = R.DR<FT.AMOUNT.DEBITED,1>
    FT.AMT  = BB

    VV1 = LEN (R.DR<FT.COMMISSION.AMT><1,1>) - 3
    BB1 = R.DR<FT.COMMISSION.AMT><1,1>[4,VV1]
    FT.COMM = BB1
    IF FT.COMM EQ '' THEN
        FT.COMM = 0
    END

    VV2  = LEN (R.DR<FT.CHARGE.AMT><1,1>) - 3
    BB2  = R.DR<FT.CHARGE.AMT><1,1>[4,VV2]
    FT.POS1= BB2

    VV3  = LEN (R.DR<FT.CHARGE.AMT><1,2>) - 3
    BB3  = R.DR<FT.CHARGE.AMT><1,2>[4,VV3]
    FT.POS2= BB3
********************************
    INPUTT    = R.DR<FT.INPUTTER>
    INP       = FIELD(INPUTT,'_',2)
    AUTH1     = R.DR<FT.AUTHORISER>
    AUTH      = FIELD(AUTH1,'_',2)


    IN.AMOUNT = AMOUNT + FT.COMM + FT.POS1 + FT.POS2
    AMT      = AMOUNT + FT.COMM + FT.POS1 + FT.POS2
    IF IN.AMOUNT = 0 THEN
        OUT.AMOUNT = ''
    END

    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 219 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'


    XX4<1,1>[45,15] = '���� ����� :'
    XX4<1,1>[59,15] = ACC.NO

    XX5<1,1>[45,15]  = '������     : '
    XX5<1,1>[59,15]  = AMT


    XX6<1,1>[45,15] = '������     : '
    XX6<1,1>[59,15] = CUR

    XX7<1,1>[45,15] = '����� ���� : '
    XX7<1,1>[59,15] = MAT.DATE


    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = BR.DATA
    XX10<1,1>[20,15]= BR.DATA2

    XX11<1,1>[3,15]  = '������    : '
    XX11<1,1>[20,15] =  REFER

    XX12<1,1>[3,15]   = ' ������ ������� :'
    XX12<1,1>[45,15]  = AMOUNT

    XX13<1,1>[3,15]  = '��������'
    XX13<1,1>[45,15]  = FT.COMM

    XX14<1,1>[3,15]  = '������ ����� '
    XX14<1,1>[45,15]  = FT.POS1 + FT.POS2
    XX15<1,1>[3,15]  = ''
    XX16<1,1>[3,15]  = ''
    XX17<1,1>[3,15]  = ''

    XX18<1,1>[3,15]  = '������ ������ '
    XX18<1,1>[45,15]  = AMOUNT + FT.COMM + FT.POS1 + FT.POS2

    XX19<1,1>[1,15]  = '������'
    XX19<1,1>[30,15] = '��� �������'
    XX19<1,1>[60,15] = '������'

    XX20<1,1>[1,15]  = INP
    XX20<1,1>[30,15] = COMI
    XX20<1,1>[60,15] = AUTH
************************************
*Line [ 275 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH1,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH1,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    TEXT = "BRANCH = " :BRANCH ; CALL REM
    YYBRN  = FIELD(BRANCH,'.',2)

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  = 'FT.HIS.HEWLA'
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":'����� ���':SPACE(10) :'������� :':T.DAY:SPACE(20)
    PR.HD :="'L'":'-----------------------------------------------------------------------------------'

    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT XX7<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT XX19<1,1>
    PRINT XX20<1,1>

    RETURN
*-------------------------------------------------------------------
END
