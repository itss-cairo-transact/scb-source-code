* @ValidationCode : MjoxMzI0MDM4OTU1OkNwMTI1MjoxNjQ2MDM0OTEyMDk4OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Feb 2022 09:55:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FT.COMM.AMT(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

* COMM = R.NEW(FT.COMMISSION.AMT)
* DD = DCOUNT(COMM,VM)
* FOR I = I TO DD
* COM1 = COMM[4,13]
* ARG = COM1
* NEXT I
*Line [ 36 ] Initialize MM Variable - ITSS - R21 Upgrade - 2021-12-27
    MM = ''
    COMM = ARG[4,13]

**************ADDED BY MAHMOUD 6/12/2009*****************
    COMM.CUR = ARG[1,3]
*Line [ 43 ] Adding brackets - ITSS - R21 Upgrade - 2021-12-27
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,COMM.CUR,CM.CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,COMM.CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CM.CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME><2,2>
*CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,COMM.CUR,CM.CUR)

**    ARG = COMM
*********************************************************

    ARG = COMM:"  ":CM.CUR
    TEXT = MM ; CALL REM
RETURN
END
