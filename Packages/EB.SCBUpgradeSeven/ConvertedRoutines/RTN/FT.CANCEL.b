* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTQ3MDY6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-------------------------------------AHMED NAHRAWY (FOR EVER)----------------------------------------
    SUBROUTINE FT.CANCEL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

* IF R.NEW(FT.DEBIT.CURRENCY) EQ 'EGP' OR R.NEW(FT.CREDIT.CURRENCY) EQ 'EGP'  THEN
    BEGIN CASE
*    CASE  R.NEW(FT.DEBIT.CURRENCY) NE R.NEW(FT.CREDIT.CURRENCY) AND R.NEW(FT.DEBIT.CURRENCY) EQ 'EGP' OR R.NEW(FT.CREDIT.CURRENCY) EQ 'EGP'
*        CALL PRODUCE.DEAL.SLIP("FT.DEBIT.TANZEL")
*        CALL PRODUCE.DEAL.SLIP("FT.DEBIT.TNAZEL")

*    CASE R.NEW(FT.DEBIT.CURRENCY) NE R.NEW(FT.CREDIT.CURRENCY) AND R.NEW(FT.DEBIT.CURRENCY) NE 'EGP' AND R.NEW(FT.CREDIT.CURRENCY) NE 'EGP'
*        CALL PRODUCE.DEAL.SLIP("FT.DEBIT.MOMLAT")
*        CALL PRODUCE.DEAL.SLIP("FT.DEBIT.MOMLT2")

    CASE R.NEW(FT.RECORD.STATUS) = 'RNAU'
        CALL PRODUCE.DEAL.SLIP("FT.DEBIT.REVE")
        CALL PRODUCE.DEAL.SLIP("FT.CR.REVE ")
    END CASE
    RETURN
END
