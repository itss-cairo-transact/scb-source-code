* @ValidationCode : Mjo5NzE1ODU2NzpDcDEyNTI6MTY0NTk1ODMxNTI5NzpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:38:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------RIHAM YOUSSEF 20161010--------------------*
SUBROUTINE FT.TR.IN(ENQ)

*--------------------------------------------------*
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FT.TRANS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    FN.TR ='F.SCB.FT.TRANS' ; F.TR = ''
    CALL OPF(FN.TR,F.TR)


    FN.CU ='FBNK.CUSTOMER'  ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
*--------------------------------------------------*
    SELECTED = ""
    KEY.LIST = ""

    LOCATE "PROCESSING.DATE" IN ENQ<2,1> SETTING FT.POS THEN
        DEBIT.DATE = ENQ<4,FT.POS>
    END

    XXX = FIELD(DEBIT.DATE,' ',1)
    YYY = FIELD(DEBIT.DATE,' ',2)
    TEXT = XXX ; CALL REM
    TEXT = YYY; CALL REM

    T.SEL= "SELECT F.SCB.FT.TRANS WITH PROCESSING.DATE GE ":XXX:" AND PROCESSING.DATE LE ":YYY:" AND REF.NO EQ '2' BY COMPANY BY DEBIT.CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM

    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ZZ = KEY.LIST<ENQ.LP>

            CALL F.READ(FN.TR,ZZ,R.TR,F.TR,ETEXT.R1)
            CUS.ID = R.TR<TR.DEBIT.CUSTOMER>
            CUS.ID = TRIM(CUS.ID,"0","L")
*     TEXT    = CUS.ID ; CALL REM
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,ETEXT.R2)
            NAT     = R.CU<EB.CUS.NATIONALITY>
*   TEXT    = NAT ; CALL REM
            IF NAT EQ 'Eg' THEN
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>

            END ELSE
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = XXX:" ":YYY
            END
        NEXT ENQ.LP
    END
RETURN
END
