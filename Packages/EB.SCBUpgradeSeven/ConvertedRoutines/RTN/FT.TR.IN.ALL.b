* @ValidationCode : MjotOTg2MDE4OTYyOkNwMTI1MjoxNjQwNjk3NjQ2NDI4OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--------RIHAM YOUSSEF 20161010--------------------*
SUBROUTINE FT.TR.IN.ALL(ENQ)

*--------------------------------------------------*
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 32 ] Removed UnUsed I_F.SCB.FT.TRANS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_F.SCB.FT.TRANS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    FN.FT ='FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)


    FN.CU ='FBNK.CUSTOMER'  ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
*--------------------------------------------------*
    SELECTED = ""
    KEY.LIST = ""

*    LOCATE "PROCESSING.DATE" IN ENQ<2,1> SETTING FT.POS THEN
*        DEBIT.DATE = ENQ<4,FT.POS>
*    END

* XXX = FIELD(DEBIT.DATE,' ',1)
* YYY = FIELD(DEBIT.DATE,' ',2)
* TEXT = XXX ; CALL REM
* TEXT = YYY; CALL REM
    XXX = TODAY
    YYY = TODAY
    T.SEL= "SELECT FBNK.FUNDS.TRANSFER WITH DEBIT.CURRENCY NE CREDIT.CURRENCY BY CO.CODE BY DEBIT.CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    TEXT = SELECTED ; CALL REM

    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ZZ = KEY.LIST<ENQ.LP>
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>

        NEXT ENQ.LP
    END ELSE

        ENQ<2,ENQ.LP> = '@ID'
        ENQ<3,ENQ.LP> = 'EQ'
        ENQ<4,ENQ.LP> = XXX:" ":YYY

    END
RETURN
END
