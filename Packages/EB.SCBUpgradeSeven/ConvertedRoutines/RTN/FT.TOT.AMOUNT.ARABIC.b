* @ValidationCode : Mjo3NjQ4ODI4OTg6Q3AxMjUyOjE2NDA2OTc2NDUyOTQ6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*----------------------------------------------AHMED NAHRAWY (FOR EVER)-------------------------------
SUBROUTINE FT.TOT.AMOUNT.ARABIC(ARG)
*-----------------------------------------------------------------------------------------------------
* Modification History
* ====================
*
* 09-Jun-2016     SCBUPG20160609         Inserted I_F.CURRENCY to prevent uninitialised variable message
*
*
* --------------------------------------------------------------------------------------------------------
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
* SCBUPG20160609 - S/E -
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 37 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS

*    FT.AMT = R.NEW(FT.DEBIT.AMOUNT,1)
*    FT.COMM = R.NEW(FT.COMMISION.AMT)<1,1>
*    FT.POS1 = R.NEW(FT.CHARGE.AMT)<1,1>
*    FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
*    FT.FEES =FT.AMT+ FT.COMM + FT.POS1 +FT.POS2

    XX= R.NEW(FT.AMOUNT.CREDITED)
    VV= LEN(R.NEW(FT.AMOUNT.CREDITED)) - 3
    BB = R.NEW(FT.AMOUNT.CREDITED)[4,VV]
*    TEXT = " BB = " : BB ; CALL REM
*FT.AMT = R.NEW(FT.AMOUNT.DEBITED,1)
    FT.AMT  = BB

    XX1 = R.NEW(FT.COMMISSION.AMT)<1,1>
    VV1 = LEN (R.NEW(FT.COMMISSION.AMT)<1,1>) - 3
    BB1 = R.NEW(FT.COMMISSION.AMT)<1,1>[4,VV1]
*   TEXT = " BB1 = " : BB1 ; CALL REM
    FT.COMM = BB1

    XX2  = R.NEW(FT.CHARGE.AMT)<1,1>
    VV2  = LEN (R.NEW(FT.CHARGE.AMT)<1,1>) - 3
    BB2  = R.NEW(FT.CHARGE.AMT)<1,1>[4,VV2]
*   TEXT = " BB2 = " : BB2 ; CALL REM
    FT.POS1 = BB2

    XX3  = R.NEW(FT.CHARGE.AMT)<1,2>
    VV3  = LEN (R.NEW(FT.CHARGE.AMT)<1,2>) - 3
    BB3  = R.NEW(FT.CHARGE.AMT)<1,2>[4,VV3]
***TEXT = " BB3 = " : BB3 ; CALL REM
    FT.POS2 = BB3

* FT.POS2 = R.NEW(FT.CHARGE.AMT)<1,2>
*   FT.FEES = FT.COMM + FT.POS1 +FT.POS2
    FT.FEES = FT.COMM + FT.POS1
* ARG = FT.FEES
    IN.AMOUNT = FT.FEES
    CUR = R.NEW(FT.TOTAL.CHARGE.AMOUNT)
    CUR1= CUR[1,3]
*
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR1,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR1,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    ARG = OUT.AMOUNT:" ":CURR:" ":"�� ���"

RETURN
END
