* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgyNjk2NTA6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:37:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>877</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOY.PRFT.LOSS
**    PROGRAM EOY.PRFT.LOSS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CONSL.PRFT.LOSS


    EXECUTE "CLEAR-FILE F.SCB.CONSL.PRFT.LOSS"

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN
*------------------------------
INITIALISE:

    FN.CONT = 'F.RE.STAT.LINE.CONT'     ; F.CONT = ''
    FN.CONS = 'F.CONSOLIDATE.PRFT.LOSS' ; F.CONS = ''
    FN.LN   = 'F.RE.STAT.REP.LINE'      ; F.LN   = ''
    FN.CNSL = 'F.SCB.CONSL.PRFT.LOSS'   ; F.CNSL = ''
    FN.CUR  = 'FBNK.CURRENCY'           ; F.CUR=''    ;R.CUR = '';E11=''

    CCC.NO = '' ; LEDG.NO = '' ; CURR.NO = ''
    CALL OPF(FN.CONT,F.CONT)
    CALL OPF(FN.CONS,F.CONS)
    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.CNSL,F.CNSL)
    CALL OPF(FN.CUR,F.CUR)

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    NET.AMT     = 0
    NET.AMT.500 = 0
    SER = 0

    RETURN
**----------------------------------------------------------------**
BUILD.RECORD:

    COMMA     = ","
    KEY.LIST1 = "" ; SELECTED1 = "" ;  ER.MSG1 = ""

**-----------GET LINE -------------------------------------------------**
*DEBUG
    T.SEL    = "SELECT ":FN.CONT:" WITH @ID LIKE GENLEDALL.... AND PRFT.CONSOL.KEY NE '' BY @ID"
*    T.SEL    = "SELECT ":FN.CONT:" WITH ( @ID LIKE GENLEDALL.0580...EG0010010 OR @ID LIKE GENLEDALL.1030.EG0010010 ) AND PRFT.CONSOL.KEY NE '' BY @ID"
*    T.SEL    = "SELECT ":FN.CONT:" WITH ( @ID LIKE GENLEDALL.0585...EG0010020 OR @ID LIKE GENLEDALL.1035.EG0010020... ) AND PRFT.CONSOL.KEY NE '' BY @ID"
*    T.SEL    = "SELECT ":FN.CONT:" WITH @ID LIKE GENLEDALL... AND PRFT.CONSOL.KEY EQ 'PL.50010.20.EG.21003.20.1100.3M..1100....EG0010020'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            NET.AMT  = 0
            LINE     = KEY.LIST<HH>
            PL.REF   = FIELD(KEY.LIST<HH>,".",1)[1,2]
            IF PL.REF NE 'RE' THEN
                CALL F.READ(FN.CONT,KEY.LIST<HH>,R.CONT,F.CONT,E1)
                LINE.FLAG    = FIELD(KEY.LIST<HH>,".",2)
                COM.CO       = FIELD(KEY.LIST<HH>,".",3)
                LINE         = "GENLEDALL.":LINE.FLAG
*Line [ 101 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.PRFT.CONSOL.KEY>,@VM)
                FOR I = 1 TO DECOUNT.CONT
                    CURRR.CONT = R.CONT<RE.SLC.PROFIT.CCY,I>
*Line [ 105 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    DECOUNT.CURR.CONT = DCOUNT(R.CONT<RE.SLC.PROFIT.CCY,I>,@SM)
                    GOSUB GET.PL
                NEXT I
            END
        NEXT HH
    END
    RETURN
**************************************************************

GET.PL:
*=======
    PL.ID     = R.CONT<RE.SLC.PRFT.CONSOL.KEY,I>
    CALL F.READ(FN.CONS,PL.ID,R.CONS,F.CONS,E1)
    PL.BRN    = COM.CO
    PL.CATEG  = R.CONS<RE.PTL.VARIABLE.1>

    CATEG     = R.CONS<RE.PTL.VARIABLE.4>
    SECTOR    = R.CONS<RE.PTL.VARIABLE.6>
    SCB.TARGET    = R.CONS<RE.PTL.VARIABLE.10>
    DEPT.OFFICER  = R.CONS<RE.PTL.VARIABLE.2>
*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('SECTOR':@FM:EB.SEC.DESCRIPTION,SECTOR,SEC.NAME)
F.ITSS.SECTOR = 'F.SECTOR'
FN.F.ITSS.SECTOR = ''
CALL OPF(F.ITSS.SECTOR,FN.F.ITSS.SECTOR)
CALL F.READ(F.ITSS.SECTOR,SECTOR,R.ITSS.SECTOR,FN.F.ITSS.SECTOR,ERROR.SECTOR)
SEC.NAME=R.ITSS.SECTOR<EB.SEC.DESCRIPTION>
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CATEG.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,PL.BRN,BRN.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,PL.BRN,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRN.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,PL.CATEG,PL.CATEG.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,PL.CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
PL.CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('RE.STAT.REP.LINE':@FM:RE.SRL.DESC,LINE,LINE.DESC)
F.ITSS.RE.STAT.REP.LINE = 'F.RE.STAT.REP.LINE'
FN.F.ITSS.RE.STAT.REP.LINE = ''
CALL OPF(F.ITSS.RE.STAT.REP.LINE,FN.F.ITSS.RE.STAT.REP.LINE)
CALL F.READ(F.ITSS.RE.STAT.REP.LINE,LINE,R.ITSS.RE.STAT.REP.LINE,FN.F.ITSS.RE.STAT.REP.LINE,ERROR.RE.STAT.REP.LINE)
LINE.DESC=R.ITSS.RE.STAT.REP.LINE<RE.SRL.DESC>

    FOR K = 1 TO  DECOUNT.CURR.CONT
        CONS.CCY = R.CONS<RE.PTL.CURRENCY>
        PRINT CURRR.CONT<1,1,K>
        LOCATE CURRR.CONT<1,1,K> IN CONS.CCY<1,1>  SETTING J THEN
*      CURR.CONT = DCOUNT(R.CONS<RE.PTL.CURRENCY>,VM)

*       FOR J = 1 TO CURR.CONT
            CURR = R.CONS<RE.PTL.CURRENCY,J>

            IF CURR EQ 'EGP' THEN
                MON.BAL.VAL      = R.CONS<RE.PTL.BALANCE,J> + R.CONS<RE.PTL.CREDIT.MOVEMENT,J> + R.CONS<RE.PTL.DEBIT.MOVEMENT,J> + R.CONS<RE.PTL.BALANCE.YTD,J>
                MON.BAL.CCY.VAL      = R.CONS<RE.PTL.BALANCE,J> + R.CONS<RE.PTL.CREDIT.MOVEMENT,J> + R.CONS<RE.PTL.DEBIT.MOVEMENT,J> + R.CONS<RE.PTL.BALANCE.YTD,J>
            END ELSE
                MON.BAL.CCY.VAL = R.CONS<RE.PTL.CCY.BALANCE,J> + R.CONS<RE.PTL.CCY.CREDT.MVE,J> + R.CONS<RE.PTL.CCY.DEBIT.MVE,J> + R.CONS<RE.PTL.CCY.BALANCE.YTD,J>
                MON.BAL.VAL      = R.CONS<RE.PTL.BALANCE,J> + R.CONS<RE.PTL.CREDIT.MOVEMENT,J> + R.CONS<RE.PTL.DEBIT.MOVEMENT,J> + R.CONS<RE.PTL.BALANCE.YTD,J>
            END
*PRINT PL.ID
*PRINT CATEG:'|':PL.CATEG:'|':CURR:'|':MON.BAL.VAL:'|':SECTOR:'|':PL.BRN
            GOSUB WRITE.CNSL
*Line [ 151 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        END ELSE NULL
    NEXT K

    RETURN

**------------------------------------------------------------------**
WRITE.CNSL:
*========
    SER = SER + 1
    CNSL.NO = PL.BRN:'.':FMT(SER,'R%6')

    R.CNSL = ''
    R.CNSL<CNSL.CATEG >      =  CATEG
    R.CNSL<CNSL.PL.CATEG>    =  PL.CATEG
    R.CNSL<CNSL.CURR>        =  CURR
    R.CNSL<CNSL.NATIVE.AMT>  =  FMT(MON.BAL.CCY.VAL,"R2")
    R.CNSL<CNSL.EQU.AMT>     =  FMT(MON.BAL.VAL,"R2")
    R.CNSL<CNSL.SECTOR>      =  SECTOR
    R.CNSL<CNSL.SECTOR.NAME> =  SEC.NAME
    R.CNSL<CNSL.CATEG.NAME>  =  CATEG.NAME
    R.CNSL<CNSL.BRANCH>      =  PL.BRN
    R.CNSL<CNSL.BRANCH.NAME> =  BRN.NAME
    R.CNSL<CNSL.CONSOLE.KEY> =  PL.ID
    R.CNSL<CNSL.LINE>        =  LINE
    R.CNSL<CNSL.LINE.DESC>   =  LINE.DESC
    R.CNSL<CNSL.PL.CATEG.NAME> = PL.CATEG.NAME
    R.CNSL<CNSL.PL.DEPT.OFFICER> = DEPT.OFFICER
    R.CNSL<CNSL.TARGET> = SCB.TARGET

    CALL F.WRITE(FN.CNSL,CNSL.NO,R.CNSL)
    CALL JOURNAL.UPDATE(CNSL.NO)
    RETURN
END
