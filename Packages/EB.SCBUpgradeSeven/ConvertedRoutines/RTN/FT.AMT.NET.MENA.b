* @ValidationCode : MjotMTc2MjM4NDUxNjpDcDEyNTI6MTY0MDY5NzYzMzEyMDpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--- CREATE BY MENA&RIHAM 2017/10/24 ----------------
SUBROUTINE FT.AMT.NET.MENA(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 26 ] Hashing I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------------
    AMT  = 0
    AMT1 = R.NEW(FT.AMOUNT.CREDITED)[4,20]
    AMT2 = R.NEW(FT.DEBIT.AMOUNT)
    RES  = AMT1 - AMT2

*    IF RES EQ '0' THEN
*        TOT.CHG = R.NEW(FT.TOTAL.CHARGE.AMOUNT)[4,20]
*       AMT     = AMT1 - TOT.CHG
*  END ELSE
*     AMT     = AMT1
* END

    ARG = AMT1
*------------------------------------------------
RETURN
END
