* @ValidationCode : Mjo1NzU2MjI2OTpDcDEyNTI6MTY0MDY5NzYzMTQyMzpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-105</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FATCA.TOT.CUS
*  PROGRAM  FATCA.TOT.CUS
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE

    GOSUB PRINT.HEAD
*Line [ 39 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*   TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM

RETURN
*==============================================================
INITIATE:

*   REPORT.ID='FATCA.TOT.CUS'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

RETURN
*=============================================================
CALLDB:

    FN.FT  = 'FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO'              ; F.FT = ''      ; R.FT = ''
    FN.CU  = 'FBNK.CUSTOMER'                                       ; F.CU = ''      ; R.CU = ''
    FATCA.CUS = 0    ; TOT.CUS =0
    FOR I = 1 TO 100
        FATCA.CUS<I> = 0
    NEXT I

    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.CU,F.CU)

    T.SEL  = "SELECT FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO WITH FA.POST.REST EQ ''"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    FOR I = 1 TO SELECTED

*   CRT @(30,3) :KEY.LIST<I>:' ':I:' OF ':SELECTED
        CALL F.READ( FN.FT,KEY.LIST<I>, R.FT, F.FT, ETEXT)
        CALL F.READ( FN.CU,KEY.LIST<I>, R.CU, F.CU, ETEXT)
        COMP.COD = R.CU<EB.CUS.COMPANY.BOOK>[8,2]
        COMP.COD = COMP.COD + 0
*CRT @(60,20) :COMP.COD:" ":KEY.LIST<I>:' ':I:' OF ':SELECTED
        CUS.NO = KEY.LIST<I>
        L = LEN(CUS.NO)
        IF LEN(CUS.NO) EQ 7 THEN
            IF CUS.NO[2,1] <> 5 THEN
                FATCA.CUS<COMP.COD> = FATCA.CUS<COMP.COD> +1
            END
        END
        ELSE
            IF CUS.NO[3,1] <> 5 THEN
                FATCA.CUS<COMP.COD> = FATCA.CUS<COMP.COD> +1
            END
        END
    NEXT I
*CRT @(50,15) :FATCA.CUS<2>:' OF ':SELECTED
*===================================
    PRINT
    FOR I = 1 TO 100
        COMP.ID = "EG00100":FMT(I, "R%2")
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        IF BRANCH NE '' THEN
            XX = SPACE(120)
            NO.OF.CUS = FATCA.CUS<I>
            XX<1,1>[5,20]   = BRANCH
            XX<1,1>[35,30]   =  NO.OF.CUS
            PRINT XX<1,1>
            XX = SPACE(120)
            PRINT
            TOT.CUS = TOT.CUS + NO.OF.CUS
        END
    NEXT I
*   PRINT
    PRINT STR('_',120)
    XX = SPACE(120)
    XX<1,1>[05,20]  = "��������"
    XX<1,1>[35,7]   = TOT.CUS
    PRINT XX<1,1>
    PRINT STR('_',120)
*==============================================

RETURN
*===============================================================
PRINT.HEAD:
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,"EG0010099",BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,"EG0010099",R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    YYBRN = BRANCH
    DATY = TODAY

    T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"FATCA.TOT.CUS"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):"���� ���� ����� ������ ��� ��� ��� ����� ������� "

    PR.HD :="'L'":SPACE(40):STR('_',55)

    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD := "'L'": SPACE (5) : : "����� " : SPACE (25) : "��� ������� "  :

    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
