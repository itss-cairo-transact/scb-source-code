* @ValidationCode : MjozNzgzNTY5OTI6Q3AxMjUyOjE2NDA2OTc2MzQzNDI6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
SUBROUTINE FT.CHECK.CAT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY


*****************UPDATED BY MAHMOUD 4/2/2009*********************
* printing the errors results
*****************************************************************

    GOSUB INITIATE
    GOSUB PRINTHEAD
*Line [ 45 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

INITIATE:
***********
    TDD = TODAY
    REPORT.ID='FT.CHECK.CAT'
    CALL PRINTER.ON(REPORT.ID,'')
    K1 = 0
    XX1 = SPACE(120)
    INPP = ''
    INP.NAME = ''
RETURN
*****************************************************************
CALLDB:
***********
    FN.FT='FBNK.FUNDS.TRANSFER'
    F.FT=''
    CALL OPF(FN.FT,F.FT)
******************************************************************
    FN.ACC='FBNK.ACCOUNT'
    F.ACC=''
    CALL OPF(FN.ACC,F.ACC)
******************************************************************
    FN.CUR='FBNK.CURRENCY'
    F.CUR=''
    CALL OPF(FN.CUR,F.CUR)
******************************************************************
    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD='' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
RETURN
***************************************************
PROCESS:
***********
*DEBUG

    LD.SEL="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT NE 0 AND FIN.MAT.DATE LE ":TDD:" AND ( CATEGORY GE 21001 AND CATEGORY LE 21010 ) AND CO.CODE EQ ":COMP
    CALL EB.READLIST(LD.SEL,KEY.LIST.LD,"",SELECTED.LD,ER.MSG.LD)
    IF SELECTED.LD THEN
        FOR II1 = 1 TO SELECTED.LD
            CALL F.READ(FN.LD,KEY.LIST.LD<II1>,R.LD,F.LD,E11.LD)
            TEXT = " ���� ��� �� ����� ����� ":KEY.LIST.LD<II1> ; CALL REM
            ERR.TXT = " ���� ��� �� ����� ����� "
            FT.NO   = KEY.LIST.LD<II1>
            INPP    = FIELD(R.LD<LD.INPUTTER>,'_',2)
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('USER':@FM:EB.USE.USER.NAME,INPP,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
            GOSUB PRINTERR
*Line [ 96,97 ] Replacing I by II1 opened by for loop - ITSS - R21 Upgrade - 2021-12-27
        NEXT II1
*NEXT I
    END

    T.SEL="SELECT FBNK.FUNDS.TRANSFER WITH @ID EQ ''"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,E11)
            TEXT = " ���� ��� �� ������ ������� ":KEY.LIST<I> ; CALL REM
            ERR.TXT = " ���� ��� �� ������ ������� "
            FT.NO   = KEY.LIST<I>
            INPP    = FIELD(R.FT<FT.INPUTTER>,'_',2)
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('USER':@FM:EB.USE.USER.NAME,INPP,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
            GOSUB PRINTERR
        NEXT I
    END

    T.SEL2="SELECT FBNK.FUNDS.TRANSFER WITH CO.CODE EQ ":COMP:" AND DEBIT.CURRENCY NE CREDIT.CURRENCY AND (CREDIT.ACCT.NO LIKE ...PL... OR DEBIT.ACCT.NO LIKE ...PL...) BY CREDIT.ACCT.NO BY AMOUNT.CREDITED BY DEBIT.ACCT.NO BY AMOUNT.DEBITED"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
    IF SELECTED2 THEN
        FOR K =1 TO SELECTED2
            CALL F.READ(FN.FT,KEY.LIST2<K>,R.FT,F.FT,E22)
            TEXT = "���� ���������� ���� ���� ����� ��� ������� ���� ������ ": KEY.LIST2<K> ; CALL REM
            ERR.TXT = "���� ���������� ���� ���� ����� ��� ������� ���� ������ "
            FT.NO   = KEY.LIST2<K>
            INPP    = FIELD(R.FT<FT.INPUTTER>,'_',2)
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('USER':@FM:EB.USE.USER.NAME,INPP,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
            GOSUB PRINTERR
        NEXT K
    END

    T.SEL3 = "SELECT FBNK.FUNDS.TRANSFER WITH CO.CODE EQ ":COMP:" AND DEBIT.CURRENCY NE 'EGP' AND CREDIT.CURRENCY NE 'EGP' AND (CHARGE.AMT LIKE ...EGP... OR COMMISSION.AMT LIKE ...EGP...) BY DEBIT.CURRENCY BY CREDIT.CURRENCY BY COMMISSION.AMT BY CHARGE.AMT BY CHARGES.ACCT.NO BY INPUTTER"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG)
    IF SELECTED3 THEN
        FOR L = 1 TO SELECTED3
            CALL F.READ(FN.FT,KEY.LIST3<L>,R.FT,F.FT,E1)
            Y.COM = R.FT<FT.CHARGES.ACCT.NO>
            Y.COM1=Y.COM[9,2]
            IF Y.COM1 NE '10' THEN
                TEXT = "���� ��� ������� ���� ��������� ��� ������� ���� ������" : KEY.LIST3<L> ; CALL REM
                ERR.TXT    = "���� ��� ������� ���� ��������� ��� ������� ���� ������"
                FT.NO   = KEY.LIST3<L>
                INPP    = FIELD(R.FT<FT.INPUTTER>,'_',2)
*Line [ 160 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR('USER':@FM:EB.USE.USER.NAME,INPP,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
                GOSUB PRINTERR
            END
        NEXT L
    END

    T.SEL4 = "SELECT FBNK.FUNDS.TRANSFER WITH CO.CODE EQ ":COMP:" AND DEBIT.CURRENCY EQ 'EGP' AND CREDIT.CURRENCY EQ 'EGP' AND (CHARGE.AMT UNLIKE ...EGP... AND COMMISSION.AMT UNLIKE ...EGP... AND CHARGE.AMT NE '' AND COMMISSION.AMT NE '')BY DEBIT.CURRENCY BY CREDIT.CURRENCY BY COMMISSION.AMT BY CHARGE.AMT BY INPUTTER"
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG)
    IF SELECTED4 THEN
        FOR S = 1 TO SELECTED4
            CALL F.READ(FN.FT,KEY.LIST4<S>,S.FT,F.FT,E1)
            S.COM = S.FT<FT.CHARGES.ACCT.NO>
            S.COM1=S.COM[9,2]
            IF S.COM1 EQ '10' THEN
                TEXT = "���� ��� ������� ���� ������ ����� ��� ������� ���� ���� ": KEY.LIST4<S> ; CALL REM
                ERR.TXT = "���� ��� ������� ���� ������ ����� ��� ������� ���� ���� "
                FT.NO   = KEY.LIST4<S>
                INPP    = FIELD(R.FT<FT.INPUTTER>,'_',2)
*Line [ 184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*                CALL DBR('USER':@FM:EB.USE.USER.NAME,INPP,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
                GOSUB PRINTERR
            END
        NEXT S
    END

    OAL2 = 0
    T.SEL5 = "SELECT FBNK.ACCOUNT WITH CO.CODE EQ ":COMP:" AND((CATEGORY GT 9000 AND CATEGORY LT 9999) OR (CATEGORY GT 19000 AND CATEGORY LT 19999)) AND ONLINE.ACTUAL.BAL NE 0 AND ONLINE.ACTUAL.BAL NE '' BY CURRENCY "
    CALL EB.READLIST(T.SEL5,KEY.LIST5,"",SELECTED5,ER.MSG)
    IF SELECTED5 THEN
        FOR A = 1  TO SELECTED5
            CALL F.READ(FN.ACC,KEY.LIST5<A>,A.ACC,F.ACC,E1)
            OAL2 += A.ACC<AC.ONLINE.ACTUAL.BAL>
        NEXT A
        IF OAL2 NE "0" AND OAL2 NE '4073' THEN
            TEXT ="���� ��� ���� ��������� ��� ���� ":OAL2 ; CALL REM
            ERR.TXT ="���� ��� ���� ��������� ��� ���� "
            FT.NO   = OAL2
            INPP = ''
            INP.NAME = ''
            GOSUB PRINTERR
        END
    END



    T.SEL6 = "SELECT FBNK.ACCOUNT WITH CO.CODE EQ ":COMP:" AND @ID EQ ''"
    CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ER.MSG6)
    IF SELECTED6 THEN
        FOR A6 = 1  TO SELECTED6
            CALL F.READ(FN.ACC,KEY.LIST6<A6>,A.ACC,F.ACC,E16)
*Line [ 192,191 ] Replacing A by A6 opened by for loop - ITSS - R21 Upgrade - 2021-12-27
        NEXT A6
*NEXT A
        IF OAL2 NE "0" AND OAL2 NE '4073' THEN
            TEXT = "���� ���� ���� ��� " ; CALL REM
            ERR.TXT =" ���� ���� ���� ��� "
            AC.NO   = KEY.LIST6<A6>
            INPP    = FIELD(A.ACC<AC.INPUTTER>,'_',2)
*Line [ 229 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('USER':@FM:EB.USE.USER.NAME,INPP,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
            GOSUB PRINTERR
        END
    END

    PRINT XX1<1,K1>
RETURN
**************************
PRINTERR:
    K1++
    XX1 = SPACE(120)
    XX1<1,K1>[1,16]   = FT.NO
    XX1<1,K1>[20,40]  = ERR.TXT
    XX1<1,K1>[60,40]  = INP.NAME
    FT.NO      = ''
    ERR.TXT    = ''
    INPP       = ''
    INP.NAME   = ''
RETURN
**************************************************************
PRINTHEAD:
************
*Line [ 257 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YYBRN   = BRANCH
    DATY    = TODAY
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    T.DAY1 = TDD[7,2]:'/':TDD[5,2]:"/":TDD[1,4]
    PR.HD  ="'L'":" "
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(50):"������� �� ����� ��������"
    PR.HD :="'L'":SPACE(50):"�� ����� :":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',35)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(4):"�������/������":SPACE(5):"��� �����": SPACE(40):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
