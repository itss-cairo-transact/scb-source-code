* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU5MTQyMjU6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:05:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
    SUBROUTINE FT.AR.CATEG.PL11.CR(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    CATEG=R.NEW(FT.CREDIT.ACCT.NO)

    IF CATEG = R.NEW(FT.CREDIT.ACCT.NO) THEN
        CATT = CATEG [11,4]
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,CATTT)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATTT=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        ARG  = CATTT
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,2] EQ 'PL'  THEN
        VV=LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 2
        XX=R.NEW(FT.CREDIT.ACCT.NO)[3,5]
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,XX1)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XX,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
XX1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        ARG= XX1
    END
    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'EGP'  THEN
        VV=LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 3
        XX2=R.NEW(FT.CREDIT.ACCT.NO)[4,5]
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX2,XX3)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XX2,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
XX3=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        ARG= XX3
    END

    IF R.NEW(FT.CREDIT.ACCT.NO)[1,3] EQ 'USD'  THEN
        VV=LEN(R.NEW(FT.CREDIT.ACCT.NO)) - 3
        XX2=R.NEW(FT.CREDIT.ACCT.NO)[4,5]
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX2,XX4)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,XX2,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
XX4=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        ARG= XX4
    END

    RETURN
END
