* @ValidationCode : MjozNzM0NDI1NjY6Q3AxMjUyOjE2NDA2OTc2MzEwMTU6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>-102</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FATCA.CUS.NO.CHRG
*    PROGRAM FATCA.CUS.NO.CHRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 39 ] Hashed I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='FATCA.CUS.NO.CHRG'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''; R.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT'  ; F.CU.AC =''  ; R.CU.AC = '' ; ER = ''
    CALL OPF(FN.CU.AC,F.CU.AC)

    FN.STE = 'FBNK.STMT.ENTRY'; F.STE = ''; R.STE = ''
    CALL OPF(FN.STE,F.STE)
    KEY.LIST  = ""  ; SELECTED=""  ;  ER.MSG=""
    Q  = 0
    ENT.LIST = ''

******************
    YTEXT = "Enter Year & Month(YYYYMM) : "
    CALL TXTINP(YTEXT, 8, 22, "6", "A")

    T.DATE    = COMI
    START.DATE = T.DATE:'01'
    CALL GET.LAST.DOM(T.DATE,LAST.DATE,LAST.DAY,MONTH.NAME)
    END.DATE = LAST.DATE
    type.of.date = "PROCESS"
    include.fwd = ""
    COMP.ID = ID.COMPANY

RETURN

*========================================================================
PROCESS:

*------------------CUSTOMER SELECTION-------------------------
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT LT 90)"
    T.SEL := " AND (ACTIVE.CUST EQ 'YES') AND ((NATIONALITY EQ 'US')"
    T.SEL := " OR ( OTHER.NATIONALITY EQ 'US' ))"
    IF COMP.ID NE 'EG0010099' THEN
        T.SEL := " AND (COMPANY.BOOK EQ ":COMP.ID:" )"
    END
    T.SEL := " BY @ID"
*************************************************************
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            FLAG = 'N'
            CUS.ID     = KEY.LIST<I>
            CUS.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            CO.CODE    = R.CU<EB.CUS.COMPANY.BOOK>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,CUS.BRN)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
CUS.BRN=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
            CALL F.READ(FN.CU.AC,CUS.ID ,R.CU.AC,F.CU.AC,ERR2)

            LOOP
                REMOVE ACC.ID FROM R.CU.AC SETTING POS.ACCT
            WHILE ACC.ID:POS.ACCT

*Line [ 114 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                ACC.NO = ACC.ID:@FM:type.of.date:@FM:include.fwd
                CALL EB.ACCT.ENTRY.LIST(ACC.NO,START.DATE,END.DATE,ENT.LIST,OP.BAL,Err)
                LOOP
                    REMOVE STD.ID FROM ENT.LIST SETTING POS
                WHILE STD.ID:POS
                    CALL F.READ(FN.STE,STD.ID,R.STE,F.STE,ER.STE)
                    TRN.CODE  = R.STE<AC.STE.TRANSACTION.CODE>
                    IF TRN.CODE EQ '677' THEN
                        FLAG = 'Y'
                        R.STE = ''
                        R.CU.AC = ''
                    END
                REPEAT
            REPEAT
            IF FLAG EQ 'N' THEN
                Q = Q + 1
                GOSUB REPORT.WRITE
            END
        NEXT I
        IF Q NE 0 THEN
            YY             = ''
            PRINT YY
            YY<1,1>[1,20]  = STR('=',20)
            PRINT YY<1,1>
            YY             = ''
            YY<1,1>[1,40]  = "�����     ":Q
            PRINT YY<1,1>
            YY             = ''
            YY<1,1>[1,20]  = STR('=',20)
            PRINT YY<1,1>
        END
        YY             = ''
        PRINT YY
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END

RETURN

*===============================================================
REPORT.WRITE:

    XX  = SPACE(132)
    XX<1,I>[1,10]    = CUS.ID
    XX<1,I>[15,35]  = CUS.NAME
    XX<1,I>[60,20]  = CUS.BRN
    PRINT XX<1,I>

RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� �������� ����� �� ��� ��� ������ ����� ����"
*   PR.HD :="'L'":SPACE(40):""
    PR.HD :="'L'":SPACE(50):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(5):"��� ������":SPACE(35):"�����"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*----------
END
