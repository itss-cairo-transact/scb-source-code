* @ValidationCode : MjotMTExOTc1ODM1OTpDcDEyNTI6MTY0NjAzNTM1ODA0NTpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Feb 2022 10:02:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FT.RETURNEE.ALL
*1-TO CALL DAEL SLIP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RETURNEE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [32] Initialize Variable
    FVAR.GOV = ""
    FN.GOV = 'F.SCB.RETURNEE'; F.GOV = ''
    CALL OPF(FN.GOV,F.GOV)

***    OPEN FN.GOV TO FVAR.GOV ELSE
***        TEXT = "ERROR OPEN FILE" ; CALL REM
***        RETURN
***    END

    CUST       = R.NEW(FT.DEBIT.CUSTOMER)
    T.DATE     = TODAY
    LOAN.NO    = R.NEW(FT.LOCAL.REF)<1,FTLR.OPERATION.CODE>

    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='INAU' THEN
        T.SEL ="SELECT F.SCB.RETURNEE WITH PAY.CODE2 EQ 1 AND INSTALLEMENT.DATE LE ":T.DATE:" AND CUSTOMER.NO LIKE ...":CUST:" AND LOAN.NO EQ ":LOAN.NO
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            R.GOV<RETURN.PAY.CODE2>  = '2'
            R.GOV<RETURN.RESERVED10> = TODAY

            CALL F.WRITE (FN.GOV, KEY.LIST<I>, R.GOV)

***           WRITE R.GOV TO FVAR.GOV , KEY.LIST<I>  ON ERROR
***               STOP 'CAN NOT WRITE RECORD ':KEY.LIST<I>:' TO FILE ':FN.GOV
***           END

        NEXT I
    END
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='RNAU' THEN
        T.SEL ="SELECT F.SCB.RETURNEE WITH PAY.CODE2 EQ 2 AND INSTALLEMENT.DATE LE ":T.DATE:"... AND CUSTOMER.NO LIKE ...":CUST:" AND RESERVED10 EQ ":T.DATE:" AND LOAN.NO EQ ":LOAN.NO
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            R.GOV<RETURN.PAY.CODE2>  = '1'
            R.GOV<RETURN.RESERVED10> = ''
            WRITE R.GOV TO FVAR.GOV , KEY.LIST<I>  ON ERROR
                STOP 'CAN NOT WRITE RECORD ':KEY.LIST<I>:' TO FILE ':FN.GOV
            END

        NEXT I

    END
RETURN
END
