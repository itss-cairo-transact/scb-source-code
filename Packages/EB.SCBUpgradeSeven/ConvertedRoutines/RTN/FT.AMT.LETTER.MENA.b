* @ValidationCode : MjotNTAzNDMzMDYwOkNwMTI1MjoxNjQwNjk3NjMzMDY1OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*--- CREATE BY MENA 2017/10/24 ----------------
SUBROUTINE FT.AMT.LETTER.MENA(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 29 ] Hashing I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-27
*$INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------------
    AMT  = 0  ; CURR = ""
    AMT1 = R.NEW(FT.AMOUNT.DEBITED)[4,20]
    AMT2 = R.NEW(FT.DEBIT.AMOUNT)
    RES  = AMT1 - AMT2
    CUR  = R.NEW(FT.DEBIT.CURRENCY)
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

    IN.AMOUNT = AMT1
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ERR.MSG)
    ARG       = OUT.AMOUNT:" ":CURR:" ":"�� ���"
*------------------------------------------------
RETURN
END
