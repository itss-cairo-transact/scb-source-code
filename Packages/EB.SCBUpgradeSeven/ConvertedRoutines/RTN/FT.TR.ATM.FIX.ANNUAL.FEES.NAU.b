* @ValidationCode : MjotMjEzNjM5MDIyMTpDcDEyNTI6MTY0MDY5NzY0NjIxMDpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*****NESSREEN AHMED 24/6/2020***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE FT.TR.ATM.FIX.ANNUAL.FEES.NAU

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 44 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS

    TEXT = 'HELLO' ; CALL REM

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.FT = 'F.FUNDS.TRANSFER$NAU' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
    CALL OPF(FN.FT,F.FT)


**********OPEN FILE********************************
    OPENSEQ "&SAVEDLISTS&" , "FT.REP.ATM.FIX.ANNUAL.FEE.NAU" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"FT.REP.ATM.FIX.ANNUAL.FEE.NAU"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "FT.REP.ATM.FIX.ANNUAL.FEE.NAU.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FT.REP.ATM.FIX.ANNUAL.FEE.NAU CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create FT.REP.ATM.FIX.ANNUAL.FEE.NAU File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
    BB.DATA = "FT Ref.NO":",":"Debit Customer ID ":",":"Cust.Arabic Name":",":"Debit Acct No":",":"Credit Acct No":",":"Amount Debited":",":"Processing Date":",":"Card Number":",":"Credit Their Ref":",":"Branch Code" :",":"Branch Name":",":"Override Message"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*YTEXT = "Enter Start.Date : "
*CALL TXTINP(YTEXT, 8, 22, "12", "A")
*ST.D = COMI
*YTEXT = "Enter End.Date : "
*CALL TXTINP(YTEXT, 8, 22, "12", "A")
*ED.D = COMI

    N.SEL = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH TRANSACTION.TYPE EQ 'AC52' AND CREDIT.THEIR.REF LIKE ...Annual... BY CO.CODE"

    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)

    TEXT = 'SELECTED.N=':SELECTED.N ; CALL REM
    IF SELECTED.N THEN
        FOR I = 1 TO SELECTED.N
            AR.NAME = '' ; DB.ACCT = '' ; CR.ACCT = '' ; DB.CUST = ''   ; DB.AMT.N = ''  ; DB.AMT = ''  ;NOTE.DB = ''  ; DB.TH.REF = '' ; CR.TH.REF = ''  ; PR.DATE = '' ; BR.CODE = '' ; BRANCH.NAME = '' ; OVERR.M = ''

            FT.ID = KEY.LIST.N<I>
            CALL F.READ(FN.FT, FT.ID, R.FT, F.FT, E2)
            LOCAL.REF.FT = R.FT<FT.LOCAL.REF>

            DB.CUST<I> = R.FT<FT.DEBIT.CUSTOMER>
            CALL F.READ(FN.CUSTOMER,DB.CUST<I>, R.CUSTOMER, F.CUSTOMER ,E3)
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
            AR.NAME = LOCAL.REF.C<1,CULR.ARABIC.NAME>

            DB.ACCT<I>   = R.FT<FT.DEBIT.ACCT.NO>
            CR.ACCT<I>   = R.FT<FT.CREDIT.ACCT.NO>
            DB.AMT.N<I>  = R.FT<FT.AMOUNT.DEBITED>
            LENN    = LEN(DB.AMT.N<I>)
            AMT.LEN = LENN - 3
            DB.AMT<I>    = DB.AMT.N<I>[4,AMT.LEN]
            PR.DATE<I>   = R.FT<FT.PROCESSING.DATE>
*Line [ 112 --> 115 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-27
            CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.NOTE.DEBITED",FTLR.NOTE.DEBITED.POS)
            NOTE.DB<I>   = LOCAL.REF.FT<1,FTLR.NOTE.DEBITED.POS>
*NOTE.DB<I>   = LOCAL.REF.FT<1,FTLR.NOTE.DEBITED>
            DB.TH.REF<I> = R.FT<FT.DEBIT.THEIR.REF>
            CR.TH.REF<I> = R.FT<FT.CREDIT.THEIR.REF>
            BR.CODE<I>   = R.FT<FT.CO.CODE>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BR.CODE<I>,BRANCH.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,BR.CODE<I>,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
            OVERR.M<I>   = R.FT<FT.OVERRIDE>

            BB.DATA = FT.ID :",":DB.CUST<I>:",":AR.NAME:",": DB.ACCT<I>:",":CR.ACCT<I>:",":DB.AMT<I>:",":PR.DATE<I>:",":DB.TH.REF<I>:",":CR.TH.REF<I>:",":BR.CODE<I> :",":BRANCH.NAME:",":OVERR.M<I>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END   ;***End of main selection of FT records***
    TEXT = '�� �������� �� �������� ' ;CALL REM;
RETURN
END
