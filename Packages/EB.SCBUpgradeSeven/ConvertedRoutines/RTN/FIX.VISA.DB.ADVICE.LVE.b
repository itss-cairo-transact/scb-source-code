* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDU5NTgyNzU0MjA6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Feb 2022 12:37:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
****NESSREEN AHMED 07/11/2010**************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  FIX.VISA.DB.ADVICE.LVE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE

    F.VISA.DB.AD = '' ; FN.VISA.DB.AD = 'F.SCB.READ.DB.ADVICE' ; R.VISA.DB.AD= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.DB.AD,F.VISA.DB.AD)

    YTEXT = "Enter the End Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    ID.KEY = '' ; MBR = '' ; CARD.NO = '' ; X = 0  ; LST.ST.D = ''

    T.SEL = "SELECT F.SCB.READ.DB.ADVICE WITH LST.STAT.DATE EQ " :COMI : " AND CARD.STAT EQ 'LVE' BY CONTRACT.NO BY MBR "

    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.VISA.DB.AD,KEY.LIST<I>, R.VISA.DB.AD, F.VISA.DB.AD ,E1)
            MBR<I>     = R.VISA.DB.AD<SCB.DB.MBR>
            R.VISA.DB.AD<SCB.DB.CARD.STAT>  = ''
            CALL F.WRITE(FN.VISA.DB.AD,KEY.LIST<I>, R.VISA.DB.AD)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)
        NEXT I
    END
    RETURN
END
