* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDA1OTU2MTc4ODc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 11:00:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
*-----------------------------------------------------------------------------
* <Rating>334</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/06/01 ***
*******************************************
    SUBROUTINE FSCB.CU.LCY.LTRNSD(WS.CUSTOMER.ID,WS.LCY.AMT,WS.LST.TRNS.D,WS.LD.FLG)
*    PROGRAM FSCB.CU.LCY.LTRNSD

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
**********************************
    WS.CU.ID      = WS.CUSTOMER.ID
    WS.LCY.AMT    = 0
    WS.LST.TRNS.D = ''
    WS.LD.FLG     = 0
    IF WS.CU.ID EQ '' THEN
        RETURN
    END

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ER.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

* Index files

* Index file for account
    FN.IND.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC = '' ; R.IND.AC = '' ; ER.IND.AC = ''
    CALL OPF(FN.IND.AC,F.IND.AC)
*Index file for LD's , LG's and MM's
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    GOSUB CHK.LD

    IF WS.LD.FLG = 1 THEN
        WS.LST.TRNS.D = 99999999
        RETURN
    END

    GOSUB CHK.AC

*    O.DATA = WS.LCY.AMT:'*':WS.LST.TRNS.D
    RETURN
*------------------------------------------------
****************************************
CHK.LD:
*--------
    WS.IGNR.CUS = ''
    WS.3Y.CUS   = ''

    CALL F.READ(FN.IND.LD,WS.CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            GOSUB LD.REC
        END
    REPEAT
    RETURN
****************************************
LD.REC:
*--------
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
    REC.STA        = R.LD<LD.STATUS>
    IF REC.STA NE 'LIQ' THEN
        WS.GL.ID   = R.LD<LD.CATEGORY>
        WS.CY.ID   = R.LD<LD.CURRENCY>
        WS.LD.AMT  = R.LD<LD.AMOUNT>
        IF WS.LD.AMT GT 0 THEN
            WS.LD.FLG = 1
            RETURN
            IF WS.GL.ID EQ 21096 OR WS.GL.ID EQ 21097 THEN
                GOTO NEXT.LD
            END
            IF WS.GL.ID GE 21050 AND WS.GL.ID LE 21066 THEN
                GOTO NEXT.LD
            END
            IF WS.GL.ID GE 21001 AND WS.GL.ID LE 21013 THEN
                GOSUB GET.RATE
                WS.LCY.AMT += WS.LD.AMT * WS.RATE
            END
            IF WS.GL.ID GE 21017 AND WS.GL.ID LE 21042 THEN
                GOSUB GET.RATE
                WS.LCY.AMT += WS.LD.AMT * WS.RATE
            END
        END
    END
NEXT.LD:
    WS.LD.AMT = 0
    WS.RATE   = 0
    RETURN
****************************************
CHK.AC:
*--------
    CALL F.READ(FN.IND.AC,WS.CU.ID,R.IND.AC,F.IND.AC,ER.IND.AC)
    LOOP
        REMOVE AC.ID FROM R.IND.AC SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
        WS.GL.ID         = R.AC<AC.CATEGORY>
        WS.CY.ID         = R.AC<AC.CURRENCY>
        WS.AC.AMT        = R.AC<AC.WORKING.BALANCE>
        WS.OPN.DATE      = R.AC<AC.OPENING.DATE>
        WS.CU.CR.DATE    = R.AC<AC.DATE.LAST.CR.CUST>
        WS.CU.DR.DATE    = R.AC<AC.DATE.LAST.DR.CUST>

        IF WS.GL.ID[1,1] EQ 9 THEN
            GOTO NEXT.REC
        END
        IF WS.GL.ID GE 1101 AND WS.GL.ID LE 1599 THEN
            GOTO NEXT.REC
        END

        IF WS.LST.TRNS.D EQ '' THEN
            IF ((WS.CU.CR.DATE EQ '') AND (WS.CU.DR.DATE EQ '')) THEN
                WS.LST.TRNS.D = WS.OPN.DATE
            END
        END
        IF WS.CU.CR.DATE GT WS.LST.TRNS.D THEN
            WS.LST.TRNS.D = WS.CU.CR.DATE
        END
        IF WS.CU.DR.DATE GT WS.LST.TRNS.D THEN
            WS.LST.TRNS.D = WS.CU.DR.DATE
        END
        GOSUB GET.RATE
        WS.LCY.AMT += WS.AC.AMT * WS.RATE
NEXT.REC:
    REPEAT
    RETURN
****************************************
GET.RATE:
    WS.RATE = 0
    IF  WS.CY.ID NE 'EGP' THEN
        CALL F.READ(FN.CUR,WS.CY.ID,R.CUR,F.CUR,E1)
        WS.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
        IF WS.CY.ID EQ 'JPY' THEN
            WS.RATE = ( WS.RATE / 100 )
        END
    END ELSE
        WS.RATE = 1
    END
    RETURN
*------------------------------------------------
