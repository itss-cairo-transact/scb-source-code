* @ValidationCode : MjotNDg4NzY0MTg0OkNwMTI1MjoxNjQwNjk3NjM5Mjg5OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
SUBROUTINE FT.KEY.PRT(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 27 ] Removed UnUsed I_FT.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*----------------------------------------------
*** R15 UPG - 20Sep2016
    Y.FT.LOC.VAL = R.NEW(FT.LOCAL.REF)
*Line [ 32 --> 35 ] Replaced by get loc ref to get the LF pos - ITSS - R21 Upgrade - 2021-12-28
    CALL GET.LOC.REF("FUNDS.TRANSFER","FTLR.LANGUAGE",FTLR.LANGUAGE.POS)
    ARG = Y.FT.LOC.VAL<1,FTLR.LANGUAGE.POS>
*ARG = Y.FT.LOC.VAL<1,FTLR.LANGUAGE>
*** R15 UPG - 20Sep2016

    IF ARG EQ 2 THEN
        ARG = "FT.DB.CHQ"
    END

    IF ARG EQ 1 THEN
        ARG  = "FT.DB.ENG"
    END
*------------------------------------------------------
RETURN
END
