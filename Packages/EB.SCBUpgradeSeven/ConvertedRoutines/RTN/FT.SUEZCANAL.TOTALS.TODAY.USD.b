* @ValidationCode : Mjo4NDU5Njk0NDU6Q3AxMjUyOjE2NDA2OTc2NDQxMTI6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:20:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeSeven  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeven
*DONE
************REHAM YOUSSIF 20180910****************
SUBROUTINE FT.SUEZCANAL.TOTALS.TODAY.USD
***************************************************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 44 ] Removed UnUsed I_AC.LOCAL.REFS Layout - ITSS - R21 Upgrade - 2021-12-28
*$INCLUDE I_AC.LOCAL.REFS


    GOSUB INITIATE
*Line [ 48 ] Adding EB.SCBUpgradeSeven. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB SEL.COMPANY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
**********************************************
INITIATE:
*-------
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    ACCT.ID = '9949990020321701'
    TD = TODAY
    CALL CDT("",TD,'-1W')

    FROM.DATE = TODAY
    END.DATE  = TODAY
    AMT.TOTAL = ''

RETURN
**********************************************
CALLDB:
*--------
    FN.ACC    = "FBNK.ACCOUNT"           ; F.ACC  = '' ; R.ACC  = '' ; CALL OPF(FN.ACC,F.ACC)
*    FN.FT    = "FBNK.FUNDS.TRANSFER$HIS"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.FT    = "FBNK.FUNDS.TRANSFER"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.STE   = "FBNK.STMT.ENTRY" ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
    FN.COMP  = "F.COMPANY" ; F.COMP  = '' ; R.COMP  = '' ; CALL OPF(FN.COMP,F.COMP)
RETURN
**********************************************
SEL.COMPANY:

    T.SEL = "SELECT ":FN.COMP:" WITH @ID NE 'EG0010088' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",NO.REC,ER.SEL)
    IF NO.REC THEN
        FOR Y = 1 TO NO.REC
            CALL F.READ(FN.COMP,KEY.LIST<Y>,R.COMP,F.COMP,EER.R)
            NAME = R.COMP<EB.COM.COMPANY.NAME,2>
            WS.COMPANY = KEY.LIST<Y>
            AMT.TOTAL = 0

            GOSUB PROCESS
            IF AMT.TOTAL GT 0 THEN
                XX2 = SPACE(132)
                XX2<1,1>[20,20] = '������ ��� ' : NAME : '='
                XX2<1,1>[60,10] = AMT.TOTAL
                XX2<1,1>[80,10] = '���� ����'

                PRINT STR('=',132)
                PRINT XX2<1,1>
                PRINT STR('=',132)
            END
        NEXT Y
    END
RETURN
**********************************************


PROCESS:
*-------
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.V.DATE = R.STE<AC.STE.BOOKING.DATE>
        STE.REF    = R.STE<AC.STE.OUR.REFERENCE>
        COMP.ID    = R.STE<AC.STE.COMPANY.CODE>
        IF STE.V.DATE = TODAY  THEN
            FT.ID = STE.REF
            CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
            DEBIT.ACCT       = R.FT<FT.DEBIT.ACCT.NO>
            DEBIT.CURR       = R.FT<FT.DEBIT.CURRENCY>
            FT.CODE          = R.FT<FT.CO.CODE>
*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,FT.CODE,BRAN)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,FT.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRAN=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
            IF WS.COMPANY NE FT.CODE THEN
                GOTO NEXT.I.REC
            END
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,DEBIT.CURR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DEBIT.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
            DEBIT.AMT        = R.FT<FT.DEBIT.AMOUNT>
            DEBIT.THEIR.REF  = R.FT<FT.DEBIT.THEIR.REF>
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACCT,CUSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 152 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME        = LOC.REF<1,CULR.ARABIC.NAME>
            AMT.TOTAL        += DEBIT.AMT

            XX1 = SPACE(132)
            XX1<1,1>[1,16]   = DEBIT.ACCT
            XX1<1,1>[20,40]  = CUST.NAME
            XX1<1,1>[60,10]  = DEBIT.AMT
            XX1<1,1>[80,10]  = CRR
            XX1<1,1>[100,16]  = FT.ID
            XX1<1,1>[120,16]  = BRAN
            PRINT XX1<1,1>
            PRINT STR(' ',132)

            DEBIT.ACCT = ''
            CUST.NAME  = ''
            DEBIT.AMT  = ''
            CRR        = ''
            FT.ID      = ''
            BRAN       = ''
        END
NEXT.I.REC:
    REPEAT

RETURN

*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    COMP = ID.COMPANY
*Line [ 187 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-27
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'FT.SUEZCANAL.TOTALS.TODAY.USD'
    PR.HD :="'L'":" "
    WS.DAT = TODAY
    WS.DAT = FMT(WS.DAT,"####/##/##")
    PR.HD :="'L'":SPACE(45):"������� ���� ���� ������ ������� ������� �����    : ":WS.DAT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(15):"��� ������":SPACE(25):"������":SPACE(15):"������": SPACE(15):"��� �������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
RETURN
END
