* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>167</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.CUS.STAF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PARMS

*DEBUG


CUST = R.NEW(LD.CUSTOMER.ID)

F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = ''
     CALL OPF(FN.CUSTOMER,F.CUSTOMER)
     CALL F.READ( FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, ETEXT)

     SEC = R.CUSTOMER<EB.CUS.SECTOR>
     LOCAL = R.CUSTOMER<EB.CUS.LOCAL.REF>
     VER = LOCAL<1,CULR.VERSION.NAME>
  IF VER =',SCB.STAFF' THEN
  CALL DBR ('SCB.VER.IND.SEC.LEG':@FM:VISL.SECTOR,VER,SECTR)
  LOCATE SEC IN SECTR<1,1> SETTING SS THEN SS = SS
  IF SS THEN
  ID = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY)
  T.SEL = "SELECT F.SCB.PARMS WITH @ID LIKE ":ID:"..."
   KEY.LIST=""
     SELECTED=""
     ER.MSG=""
     CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
     IF KEY.LIST THEN
    DIM CH.AMT(SELECTED)  ;                        * Make Dimension Array
    FOR I = 1 TO SELECTED ;                        * To Use It For Amo
      CH.AMT(I) = 0
    NEXT I
 
*         FOR I = 1 TO SELECTED
*           D1 = RIGHT(KEY.LIST<I>,8)
*           D2 = TODAY
*            CALL CDD("C",D1,D2,DAYS)
*           CH.AMT(I) = DAYS
*        NEXT I
        TEMP = CH.AMT(1)
        SORTT = 1
        FOR I = 2 TO SELECTED
       * IF TEMP < CH.AMT(I)  THEN TEMP = CH.AMT(I); SORTT = I
       NEXT I
     ID1 = KEY.LIST<SORTT>
     END
     GOSUB OPEN.FILE
****************************************************************************************************
OPEN.FILE:
  IF ID1 THEN
     F.COUNT = '' ; FN.COUNT = 'F.SCB.PARMS' ; R.COUNT = ''
     CALL OPF(FN.COUNT,F.COUNT)
     CALL F.READ( FN.COUNT, ID1, R.COUNT, F.COUNT, ETEXT)
* R.NEW(LD.INTEREST.SPREAD) ="5"
     R.NEW(LD.INTEREST.SPREAD) = R.COUNT<SCB.PAR.STAFF.SPREAD>
     ER = R.NEW(LD.INTEREST.SPREAD)
     TD = R.COUNT<SCB.PAR.STAFF.SPREAD>
* CALL REBUILD.SCREEN
  END
*----------------------------------------------------------------------------------------
*CALL REBUILD.SCREEN
* ELSE ETEXT = 'THIS.CUSTOMER.IS.NOT.STAFF'
END

 END
* CALL REBUILD.SCREEN
*END
RETURN
END
