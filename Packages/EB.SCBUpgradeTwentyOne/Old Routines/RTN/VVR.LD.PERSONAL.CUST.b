* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
**---------DALIA 18/09/2002---------**

SUBROUTINE VVR.LD.PERSONAL.CUST

* ONLY PRIVATE SECTOR IS ALLOWED RANGE FROM 2000 TO 2999 OTHERWISE ERROR MSG WILL BE DISPLAYED
* THEN DEFAULT FIELD CBE.CODE WITH THE VALUE OF FIELD CBE.NO OF THE CUSTOMER

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS


 SECTOR='' ;ETEXT='';FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''

IF COMI THEN
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL F.READ(FN.CUSTOMER,COMI, R.CUSTOMER, F.CUSTOMER ,E1)
    SECTOR   = R.CUSTOMER<EB.CUS.SECTOR>
    LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
    CBE.NO = LOCAL.REF<1,CULR.CBE.NO>

    IF ( SECTOR < 2000 OR SECTOR > 2999 )  THEN ETEXT = 'ONLY.PRIVATE.SECTOR.ARE.ALLOWED'
    IF CBE.NO # '' THEN R.NEW(LD.CENTRAL.BANK.CODE) = CBE.NO;CALL REBUILD.SCREEN
    ELSE ETEXT = 'This.Customer.Has.No.CBE.Code'
    
END

RETURN
END
