* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
**** INGY-SCB 09/12/2003****
*-----------------------------------------------------------------------------
* <Rating>148</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.LEP.CHRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

   IF COMI # R.NEW(TF.LC.PARTY.CHARGED)  THEN

    IF COMI = 'O' AND R.NEW(TF.LC.WAIVE.CHARGES) = 'NO' THEN
*Line [ 38 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CC = DCOUNT(R.NEW(TF.LC.CHARGE.AMOUNT),@VM)
        AMT = 0
        FOR I = 1 TO CC
            AMT = AMT + R.NEW(TF.LC.CHARGE.AMOUNT)<1,I>
        NEXT I
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.SWIFT.AMT.730> = AMT
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.SWIFT.CURR.730> = R.NEW(TF.LC.LC.CURRENCY)

*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CH = DCOUNT(R.NEW(TF.LC.CHARGE.CODE),@VM)
        CHR = 0
        FOR J = 1 TO CH
            CHR = R.NEW(TF.LC.CHARGE.CODE)<1,J>
            CALL DBR ('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHR,CHRG)
            IF CHRG THEN
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGES.DET.730,J> = CHRG
            END ELSE
                CALL DBR ('FT.COMMISSION.TYPE':@FM:FT4.DESCRIPTION,CHR,COM1)
                IF COM1 THEN
                    R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGES.DET.730,J> = COM1
                 *   TEXT = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGES.DET.730,J> ; CALL REM
                END
            END
        NEXT J
    END ELSE
     R.NEW(TF.LC.LOCAL.REF)<1,LCLR.SWIFT.AMT.730> = ''
     R.NEW(TF.LC.LOCAL.REF)<1,LCLR.SWIFT.CURR.730> = ''
     DEL R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGES.DET.730,1>
*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
     SS =DCOUNT( R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGES.DET.730>,@VM)
*     TEXT = SS ; CALL REM
     FOR S = 1 TO SS
     R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGES.DET.730,S> = ''
     NEXT S
    END

END

    RETURN
END
