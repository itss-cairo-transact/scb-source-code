* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
********* WAEL **************
*-----------------------------------------------------------------------------
* <Rating>130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CANCEL.FFF

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
    MAT.EXP=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
    MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>
    ADV.OPER=MYLOCAL<1,LDLR.ADV.OPERATIVE>

    IF COMI = "EXPIRED" THEN
        IF  TODAY LT MAT.EXP THEN
            ETEXT = 'Not Expired Yet' ; CALL STORE.END.ERROR
        END ELSE
            IF MYCODE EQ "1309" THEN
                TEXT=MYCODE:'MYCODE';CALL REM
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1301"
                R.NEW(LD.FIN.MAT.DATE) = TODAY
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
            END ELSE
                TEXT=MYCODE:'NE ';CALL REM
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1309"
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = MAT.EXP
                R.NEW(LD.FIN.MAT.DATE) = '20991231'
                R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE> = ''

                R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT>  = ''
            END
        END
    END
    IF COMI ="BENF." THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1303"
        R.NEW(LD.FIN.MAT.DATE) = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE> = ''
    END
    IF COMI ="CUSTOMER" THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1302"
        R.NEW(LD.FIN.MAT.DATE) = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE> = ''
    END
    IF COMI EQ 'NO.CHEQ' THEN

        IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'ADVANCE' THEN
            IF MYCODE NE '1111' AND MYCODE NE '1271'  AND MYCODE NE '1281'  AND MYCODE NE '1282' THEN
                ETEXT = 'Not Allowed';CALL STORE.END.ERROR
            END
            IF MYCODE EQ '1111' OR MYCODE EQ '1281' OR MYCODE EQ '1282' THEN
                IF ADV.OPER EQ '' THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1304'
                    R.NEW(LD.FIN.MAT.DATE) = TODAY
                    R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
                END ELSE
                    IF ADV.OPER EQ 'YES' THEN
                        ETEXT='ALREADY OPERATED';CALL STORE.END.ERROR
                    END
                END
            END
            IF MYCODE EQ '1271' OR MYCODE EQ '1283' OR MYCODE EQ '1284' THEN
                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT.INCREASE,ID.NEW,MYINC)
                R.NEW(LD.AMOUNT.INCREASE) = MYINC*-1
                R.NEW(LD.AMT.V.DATE) = TODAY
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1305'
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> =MAT.EXP
                R.NEW(LD.FIN.MAT.DATE) = '20991231'
            END
        END ELSE
            ETEXT='Only Advance IS Allowed';CALL STORE.END.ERROR
        END
    END

    R.NEW(LD.CHRG.CODE)<1,1> = '17'
    CALL REBUILD.SCREEN
    RETURN
END
