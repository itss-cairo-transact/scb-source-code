* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-24</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************

SUBROUTINE VVR.LG.NUMBER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

IF COMI THEN

   *  CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,COMI,DUMMY)
   *  IF NOT(ETEXT) THEN E ='It.Should.Be.An.Expired.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'

     NEW.COMI = COMI ; NEW.COMI[2,1] = 'D'
     T.SEL = "SSELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":NEW.COMI:";..."
      CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
       
         IF KEY.LIST THEN
            FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = '';R.LD=''
            CALL OPF(FN.LD,F.LD)
            MYLD = NEW.COMI:";":SELECTED
            CALL F.READ(FN.LD,MYLD,R.LD,F.LD,E1)
            LAST = R.LD<LD.LOCAL.REF,LDLR.LG.NUMBER,2>
              
                *CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,LAST,DUMMY)
                *IF NOT(ETEXT) THEN ETEXT = 'THIS.RECORD.IS.NOT.EXPIRED' ; RETURN
              IF NEW.COMI = LAST THEN
                  GOSUB GET.RECORDS
              END ELSE
                  MYLD = LAST
                  CALL F.READ(FN.LD,MYLD,R.LD,F.LD,E1)
                  GOSUB GET.RECORDS
              END
CALL REBUILD.SCREEN
       END

END
RETURN
*==================== COPY THE RECORD FROM HISTORY ==============
GET.RECORDS:
*Line [ 66 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
           KK = DCOUNT(R.LD,@FM)
            FOR I = 1 TO KK
               R.NEW(I) = R.LD<I>
*Line [ 70 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR J = 1 TO DCOUNT(R.NEW(I),@VM)
               R.NEW(I)<1,J> = R.LD<I,J>
            NEXT J
       NEXT I
               R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1112'
               R.NEW(LD.LOCAL.REF)<1,LDLR.LG.NUMBER,2> = ID.NEW
RETURN
*=================================================================

END
