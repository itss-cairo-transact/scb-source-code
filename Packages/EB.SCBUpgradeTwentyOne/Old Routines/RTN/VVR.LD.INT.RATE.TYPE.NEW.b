* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.INT.RATE.TYPE.NEW
*TO GET THE DEPOSIT RATE BASED ON AMOUNT,CURRENCY,CATEGORY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL
    COMI = "5"
    IF MESSAGE # 'VAL' THEN
        CURR =  R.NEW(LD.CURRENCY)
        AMT = R.NEW(LD.AMOUNT)
        CATEG = R.NEW(LD.CATEGORY):R.NEW(LD.VALUE.DATE)
        F.LD.LEVEL = '' ; FN.LD.LEVEL = 'F.SCB.LD.TYPE.LEVEL' ; R.LD.LEVEL = ''
        CALL OPF(FN.LD.LEVEL,F.LD.LEVEL)
        CALL F.READ(FN.LD.LEVEL, CATEG, R.LD.LEVEL, F.LD.LEVEL, ETEXT)
        LOCATE CURR IN R.LD.LEVEL<LDTL.CURRENCY,1> SETTING POS THEN
*Line [ 39 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT =  DCOUNT (R.LD.LEVEL<LDTL.RATE,POS>,@SM)
            FOR I = 1 TO TEMP.COUNT
                IF AMT GE R.LD.LEVEL<LDTL.AMT.FROM,POS,I> AND AMT LE R.LD.LEVEL<LDTL.AMT.TO,POS,I> THEN
                    R.NEW(LD.INTEREST.RATE) = R.LD.LEVEL<LDTL.RATE,POS,I>
                    CALL REBUILD.SCREEN
                END
                IF TEMP.COUNT = I AND AMT GE R.LD.LEVEL<LDTL.AMT.FROM,POS,I> THEN
                    R.NEW(LD.INTEREST.RATE) = R.LD.LEVEL<LDTL.RATE,POS,I>
                    CALL REBUILD.SCREEN

                END
            NEXT I
        END
    END

    IF COMI = "1" THEN
        T(LD.INTEREST.RATE)<3> =''
        T(LD.INT.PAYMT.METHOD)<3> ='NOINPUT'
        T(LD.INTEREST.KEY)<3> ='NOINPUT'
        T(LD.INTEREST.SPREAD)<3> ='NOINPUT'
    END ELSE
        IF COMI = "3" THEN
            T(LD.INTEREST.RATE)<3> ='NOINPUT'
            T(LD.INT.PAYMT.METHOD)<3> ='NOINPUT'
            T(LD.INTEREST.KEY)<3> =''
            T(LD.INTEREST.SPREAD)<3> =''
        END
        IF COMI = "5" THEN
            T(LD.INTEREST.RATE)<3> =''
            T(LD.INT.PAYMT.METHOD)<3> ='NOINPUT'
            T(LD.INTEREST.KEY)<3> ='NOINPUT'
            T(LD.INTEREST.SPREAD)<3> ='0'

        END
    END
*Line [ 75 ] Adding EB.SCBUpgradeTwentyOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeTwentyOne.VVR.LD.CUS.STAFF.NEW
*TEXT = "AFT1 ": R.NEW(LD.INTEREST.SPREAD) ; CALL REM

*Line [ 79 ] Adding EB.SCBUpgradeTwentyOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
CALL EB.SCBUpgradeTwentyOne.VVR.LD.EXP.STAFF
*TEXT = "AFT2 ": R.NEW(LD.INTEREST.SPREAD) ; CALL REM
    RETURN
END
