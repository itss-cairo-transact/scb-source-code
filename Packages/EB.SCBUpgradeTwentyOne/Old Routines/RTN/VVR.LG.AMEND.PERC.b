* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>389</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.PERC

* DEFAULT THE OPERATION.CODE WITH THE CORRECT CODE ACCORDING TO THE TYPE OF AMENDEMENTS

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*    IF MESSAGE NE 'VAL' THEN

    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW, MYLOCAL)
    MARG.PERC=MYLOCAL<1,LDLR.MARGIN.PERC>
    MARG.AMT=MYLOCAL<1,LDLR.MARGIN.AMT>
    IF COMI LE 0 THEN  ETEXT='Minus.Not.Allowed'
    IF COMI THEN
        IF COMI > 100 THEN
            ETEXT = 'NOT.GR.THAN.100%'
        END ELSE
            IF COMI = 100 THEN
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>=R.NEW(LD.AMOUNT)
               *** R.NEW(LD.LIMIT.REFERENCE) = '7040.01'
                CALL REBUILD.SCREEN
            END ELSE
***************************************************************************
*************************************************************************
                XX = MARG.PERC
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = (R.NEW(LD.AMOUNT)*COMI)/100
                CUS=R.NEW(LD.CUSTOMER.ID)
               * CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,CUS,LIMREF)
               * LIMPROD = FIELD(LIMREF,'.',2)
               * R.NEW(LD.LIMIT.REFERENCE)=TRIM(LIMPROD,"0","L"):".01"

                IF COMI > XX THEN R.NEW( LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = 1242
                IF COMI < XX THEN R.NEW( LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = 1241

            END
        END

    END
    R.NEW(LD.LOCAL.REF)<1,LDLR.MATUR.DATE>=TODAY
    CALL REBUILD.SCREEN
***************************************************************************
    RETURN
END
