* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.LD.REN.AMOUNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    REN.METH = R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.METHOD>
    REN.IND = R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.IND>
    IF REN.IND = 'YES' AND (REN.METH EQ 1 OR REN.METH EQ 2 ) THEN
        IF COMI THEN
            ETEXT = '������� ������� �� ���� ��� ����� �� ����� ����'
            CALL REBUILD.SCREEN
        END
    END
    IF REN.IND = 'YES' AND (REN.METH EQ 3 OR REN.METH EQ 4 OR REN.METH EQ 5 OR REN.METH EQ 6 ) THEN
        IF NOT(COMI) THEN
            ETEXT = '��� ����� ����'
            CALL REBUILD.SCREEN
        END

    END
    RETURN
END
