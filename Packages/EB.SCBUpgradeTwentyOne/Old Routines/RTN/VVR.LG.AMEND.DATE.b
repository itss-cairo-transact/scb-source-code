* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.DATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    ETEXT = ''

***    IF V$FUNCTION = 'I' THEN
***     IF MESSAGE EQ '' THEN
    IF COMI THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
        OLD.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
****************************************************************************************
***    IF COMI THEN
        IF COMI LT OLD.MAT.DATE  THEN
            ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date'
        END
        IF COMI GT OLD.MAT.DATE  THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1231'
        END
***     END
***CALL REBUILD.SCREEN

***************HHHHHH*************************
        IF R.NEW(LD.CUSTOMER.ID)[1,6] NE '994999' THEN

***        IF COMI THEN
            R.NEW(LD.CHRG.CODE)   = ""
            R.NEW(LD.CHRG.AMOUNT) = ""

            COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
            IF COMM EQ 'YES' THEN

                R.NEW(LD.CHRG.CODE)<1,1> = 13
                R.NEW(LD.CHRG.CODE)<1,2> = 14

                ****CALL REBUILD.SCREEN
            END ELSE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> <  R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
                    R.NEW(LD.CHRG.CODE)<1,1> = 13
                    R.NEW(LD.CHRG.CODE)<1,2> = 14
                END ELSE
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 4
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 7
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 9
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                    END
                END
                ****CALL REBUILD.SCREEN
            END
*** END
***                ****CALL REBUILD.SCREEN
        END

*Line [ 88 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
        FOR  I = 1 TO NO.M
            R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
        NEXT I

        *****CALL REBUILD.SCREEN
    END
***END
***END
    RETURN
END
