* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************
*TO CHECK IF ISSUE.DATE GT TODAY ERROR MESSAGE WILL BE DISPLAYED
*ELSE VALUE DATE WILL BE DEFAULTED WITH ISSUE DATE
    SUBROUTINE VVR.LG.ISSUE.DATE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI  THEN

        IF COMI > TODAY  THEN
            ETEXT = "MUST.BE.LT.TODAY "
        END ELSE
************************
            DAT.TODA = TODAY
            CALL CDT('', DAT.TODA, '-2W')

            IF COMI LT  DAT.TODA THEN
*      ETEXT='The Issue Date Less Than 2 Working Days'
*   END ELSE
                R.NEW(LD.VALUE.DATE)=COMI
                R.NEW(LD.LOCAL.REF)<1,LDLR.MATUR.DATE>=COMI
            END
            CALL REBUILD.SCREEN
**************************
        END
    END
    RETURN
END
