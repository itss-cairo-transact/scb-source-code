* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
** ----- NESSREEN AHMED 18/09/2002,  -----
*-----------------------------------------------------------------------------
* <Rating>297</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.INSURANCE.CHECK

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*A validation routine to validate if the Insurance Company is entered
* the Insurance Type , Amount and Policy.No must be entered too

IF MESSAGE = 'VAL' THEN

 ETEXT = ''
 IF COMI  THEN

  IF NOT(R.NEW( LD.LOCAL.REF )< 1, LDLR.TYPE.INSURANCE>)  THEN E = 'SHOULD.ENTER.INSUR.TYPE';CALL ERR;MESSAGE='REPEAT'
  IF NOT(R.NEW( LD.LOCAL.REF )< 1, LDLR.INSUR.AMT >) THEN E = 'SHOULD.ENTER.INSUR.AMOUNT';CALL ERR;MESSAGE='REPEAT'
  IF NOT(R.NEW( LD.LOCAL.REF )< 1, LDLR.INSUR.POLICY.NO>) THEN E = 'SHOULD.ENTER.INSUR.POLICY.NO';CALL ERR;MESSAGE='REPEAT'

 END

END


RETURN
END

* R.NEW( LD.LOCAL.REF )< 1, LDLR.INSUR.COMPANY>
