* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
****************************NI7OOOOOOOOOOOOOO***************
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.DR2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1

*    IF MESSAGE = 'VAL' THEN
        XX = COMI
        FINDSTR '1' IN XX SETTING FMS,VMS THEN
            T(DEPT.SAMP.MONTH.TF3)<3> = 'NOINPUT'
            T(DEPT.SAMP.AMT.TF3)<3>   = ''
        END
        FINDSTR '2' IN XX SETTING FMS,VMS THEN
            T(DEPT.SAMP.MONTH.TF3)<3> = 'NOINPUT'
            T(DEPT.SAMP.AMT.TF3)<3>   = ''
        END
        FINDSTR '3' IN XX SETTING FMS,VMS THEN
            T(DEPT.SAMP.MONTH.TF3)<3> = 'NOINPUT'
            T(DEPT.SAMP.AMT.TF3)<3>   = ''
        END
        FINDSTR '4' IN XX SETTING FMS,VMS THEN
            T(DEPT.SAMP.MONTH.TF3)<3> = 'NOINPUT'
            T(DEPT.SAMP.AMT.TF3)<3>   = ''
        END

        FINDSTR '5'  IN XX SETTING FMS,VMS THEN
            T(DEPT.SAMP.AMT.TF3)<3>   = 'NOINPUT'
            T(DEPT.SAMP.MONTH.TF3)<3> = ''
        END

 *   END
    RETURN
END
