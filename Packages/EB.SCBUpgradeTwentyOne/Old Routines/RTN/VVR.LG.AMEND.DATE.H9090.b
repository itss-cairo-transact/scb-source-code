* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>5898</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.DATE.H9090

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    ETEXT = ''
    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
    OLD.EXP.DAT = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

    IF COMI EQ OLD.EXP.DAT THEN
        ETEXT='It.Must.Be.CHANGE';CALL STORE.END.ERROR
    END



***    IF V$FUNCTION = 'I' THEN
***     IF MESSAGE EQ '' THEN

    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
    OLD.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    OLD.CODE=LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    PRO.TYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
****************************************************************************************
    IF COMI LT OLD.MAT.DATE  THEN
        ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date';CALL STORE.END.ERROR
    END
    IF COMI GT OLD.MAT.DATE  THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1231'
        IF PRO.TYPE EQ 'BIDBOND' THEN
            IF OLD.CODE EQ '1239' THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.REF.LD> = '1239'
            END
        END

    END
***************HHHHHH*************************
    FN.F.COM= 'F.SCB.LG.FREE.CHARGE' ; F.F.COM = ''
    CALL OPF(FN.F.COM,F.F.COM)
    CALL F.READ(FN.F.COM,R.NEW(LD.CUSTOMER.ID),R.F.COM,F.F.COM,E11)
**********************UP NI7OOO***********************
    FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LMM= 'FBNK.LMM.CHARGE.CONDITIONS'; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)
    FN.CHR= 'FBNK.FT.CHARGE.TYPE' ; F.CHR = ''
    CALL OPF(FN.CHR,F.CHR)
    FN.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.CUR= 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E1111=''
    CALL OPF(FN.CUR,F.CUR)

    AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
    W.CUR =  R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
    CATG = R.ACC<AC.CATEGORY>


***TO SET EGP AS DEFAULT CURRENCY FOR OTHER CURRENCIES
    W.CUR.POST='EGP'
    CALL F.READ(FN.CUR,W.CUR,R.CUR,F.CUR ,E1111)
    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>


**IF MESSAGE NE 'VAL' THEN
    IF CATG EQ '9090' OR CATG EQ '9091' THEN
        IF E11 THEN
            R.NEW(LD.CHRG.CODE) = ''
            R.NEW(LD.CHRG.AMOUNT)  = ''
            R.NEW(LD.CHRG.CLAIM.DATE) = ''
            R.NEW(LD.CHRG.BOOKED.ON)       = ''
*********************
*Line [ 121 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)

            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE > = ''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''

            FOR I = 2 TO COUNT.CODE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
                END
            NEXT I
*********************
            BEGIN CASE
            CASE R.NEW(LD.CUSTOMER.ID) EQ '31300011'
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = 14
                CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,R.LMM,F.LMM,EE2)
                CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "1" ; CALL REM
                LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'

                CH.AMT = R.CHR<FT5.FLAT.AMT,J>
                IF CH.AMT EQ '' THEN
                    CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
* TEXT = "2" ; CALL REM
                    LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                    CH.AMT = R.COM<FT4.FLAT.AMT,K>
                END
********************MODIFIED ON 11-8-2014
                IF W.CUR NE 'EGP' THEN
                    CH.AMT  = CH.AMT / CUR.RATE
                    CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                    IF  W.CUR EQ "JPY" THEN
                        CH.AMT  = CH.AMT * 100
                        CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                    END
                END
********************
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = CH.AMT
            CASE OTHERWISE
                IF (R.NEW(LD.CUSTOMER.ID)[1,6] EQ '994999' ) THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ""
                    R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = ""
                END ELSE
                    COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
                    IF COMM EQ 'YES' THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 13
*********************************************************************
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1>,R.LMM,F.LMM,EE3)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "3" ; CALL REM
                        LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF CH.AMT EQ '' THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
*TEXT = "4" ; CALL REM
                            LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
********************MODIFIED ON 11-8-2014
                        IF W.CUR NE 'EGP' THEN
                            CH.AMT  = CH.AMT / CUR.RATE
                            CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")

                            IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                CH.AMT  = CH.AMT * 100
                                CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                            END
                        END
********************
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,1>  = CH.AMT
**********************************************************************
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,E33)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "5" ; CALL REM
                        LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>
                        IF CH.AMT EQ '' THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
*TEXT = "6" ; CALL REM
                            LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
********************MODIFIED ON 11-8-2014
                        IF W.CUR NE 'EGP' THEN
                            CH.AMT  = CH.AMT / CUR.RATE
                            CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                            IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                CH.AMT  = CH.AMT * 100
                                CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                            END
                        END
********************
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT
                    END ELSE
****END FOR YES

*                       IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
                        IF COMI LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN

*TEXT = "EXP" : R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> ; CALL REM
*TEXT = "COM" : R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>   ; CALL REM
**TO BE MODIFIED
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 19
*********************************************************************
                            CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1>,R.LMM,F.LMM,EE3)
                            CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                            CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "7" ; CALL REM
                            LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                            IF CH.AMT EQ ''  THEN
                                CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
*TEXT = "8" ; CALL REM
                                LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                                CH.AMT = R.COM<FT4.FLAT.AMT,K>
                            END
********************MODIFIED ON 11-8-2014
                            IF W.CUR NE 'EGP' THEN
                                CH.AMT  = CH.AMT / CUR.RATE
                                CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                    CH.AMT  = CH.AMT * 100
                                    CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                END
                            END
********************
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,1>  = CH.AMT
**********************************************************************
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14

                            CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE3)
                            CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                            CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "9" ; CALL REM
                            LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                            IF CH.AMT EQ '' THEN
                                CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
*TEXT = "10" ; CALL REM
                                LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                                CH.AMT = R.COM<FT4.FLAT.AMT,K>
                            END
********************MODIFIED ON 11-8-2014
                            IF W.CUR NE 'EGP' THEN
                                CH.AMT  = CH.AMT / CUR.RATE
                                CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                    CH.AMT  = CH.AMT * 100
                                    CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                END
                            END
********************
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT
                        END ELSE

                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 4
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14
                                CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE3)
                                CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                                CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "11" ; CALL REM
                                LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                                CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                                IF CH.AMT EQ '' THEN
                                    CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
*TEXT = "12" ; CALL REM
                                    LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                                    CH.AMT = R.COM<FT4.FLAT.AMT,K>
                                END
********************MODIFIED ON 11-8-2014
                                IF W.CUR NE 'EGP' THEN
                                    CH.AMT  = CH.AMT / CUR.RATE
                                    CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                    IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                        CH.AMT  = CH.AMT * 100
                                        CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                    END
                                END
********************
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT

                            END

                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
*ETEXT = ''
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 7

                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14

                                CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE2)
                                CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                                CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)

                                LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                                CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                                IF CH.AMT EQ '' THEN
                                    CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
**TEXT = "14" ; CALL REM
                                    LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                                    CH.AMT = R.COM<FT4.FLAT.AMT,K>
                                END
********************MODIFIED ON 11-8-2014
                                IF W.CUR NE 'EGP' THEN
                                    CH.AMT  = CH.AMT / CUR.RATE
                                    CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                    IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                        CH.AMT  = CH.AMT * 100
                                        CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                    END
                                END
********************
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT

                            END
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 9
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14

                                CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE2)
                                CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                                CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
*TEXT = "15" ; CALL REM
                                LOCATE W.CUR.POST IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                                CH.AMT = R.CHR<FT5.FLAT.AMT,J>
                                IF CH.AMT EQ '' THEN
                                    CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
*TEXT = "16" ; CALL REM
                                    LOCATE W.CUR.POST IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                                    CH.AMT = R.COM<FT4.FLAT.AMT,K>
                                END
********************MODIFIED ON 11-8-2014
                                IF W.CUR NE 'EGP' THEN
                                    CH.AMT  = CH.AMT / CUR.RATE
                                    CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                    IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                                        CH.AMT  = CH.AMT * 100
                                        CALL EB.ROUND.AMOUNT ('EGP',CH.AMT,'',"2")
                                    END
                                END
********************
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT

                            END
                        END

***********END OF NO
                    END
**********END OF ELSE OF YES
                END
******************  NOT SUEZ BANK  LG
            END CASE
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''

        END
    END
**END
*************************************
    IF CATG NE '9090' AND CATG NE '9091' THEN
***************************END********************************
        IF E11 THEN
            BEGIN CASE
            CASE R.NEW(LD.CUSTOMER.ID) EQ '31300011'
                R.NEW(LD.CHRG.CODE)<1,1> = 14
                R.NEW(LD.CHRG.CLAIM.DATE) = TODAY

            CASE OTHERWISE
                IF (R.NEW(LD.CUSTOMER.ID)[1,6] EQ '994999') THEN
                    R.NEW(LD.CHRG.CODE)   = ""
                    R.NEW(LD.CHRG.AMOUNT) = ""
                    R.NEW(LD.CHRG.CLAIM.DATE) = ''
                    R.NEW(LD.CHRG.BOOKED.ON) =''
                END ELSE
                    COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
                    IF COMM EQ 'YES' THEN

                        R.NEW(LD.CHRG.CODE)<1,1> = 13
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY



                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY

                    END ELSE
                        IF COMI LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
*******TO BE MODIFIED
                            R.NEW(LD.CHRG.CODE)<1,1> = 19
                            R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY


                            R.NEW(LD.CHRG.CODE)<1,2> = 14
                            R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY

                        END ELSE
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                                R.NEW(LD.CHRG.CODE)<1,1> = 4
                                R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY


                                R.NEW(LD.CHRG.CODE)<1,2> = 14
                                R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY


                            END
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                                R.NEW(LD.CHRG.CODE)<1,1> = 7
                                R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY



                                R.NEW(LD.CHRG.CODE)<1,2> = 14
                                R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY


                            END
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN

                                R.NEW(LD.CHRG.CODE)<1,1> = 9
                                R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY


                                R.NEW(LD.CHRG.CODE)<1,2> = 14
                                R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY

                            END

                        END
                    END
                END
            END CASE

        END ELSE
            R.NEW(LD.CHRG.CODE) = ''
            R.NEW(LD.CHRG.AMOUNT) = ''
            R.NEW(LD.CHRG.CLAIM.DATE) = ''
            R.NEW(LD.CHRG.BOOKED.ON)       = ''
        END

    END

    RETURN
END
