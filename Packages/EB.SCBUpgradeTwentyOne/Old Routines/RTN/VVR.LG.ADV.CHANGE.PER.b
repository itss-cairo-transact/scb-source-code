* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
********* RANIA 11/03/03 **************

*A Routine To Recalculate Margin Amt & commissions if Margin Percenatege is changed .

    SUBROUTINE VVR.LG.ADV.CHANGE.PER

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI > 100 THEN
        ETEXT = 'MUST.BE.LESS.THAN.OR.EQ.100'
    END ELSE

        IF COMI # R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> THEN

            IF COMI = 100 THEN
                R.NEW(LD.LIMIT.REFERENCE) = ' '
*T(LD.LIMIT.REFERENCE)<3>='NOINPUT'
            END ELSE
*T(LD.LIMIT.REFERENCE)<3>=' '
                MYCUS=R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>
***    CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,MYCUS,LIMREF)
**      LIMPROD = FIELD(LIMREF,'.',2)
**    R.NEW(LD.LIMIT.REFERENCE)= TRIM(LIMPROD,"0","L")
            END
            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = (R.NEW( LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>*COMI)/100

**  COUNT.CODE=DCOUNT(R.NEW(LD.CHRG.CODE),VM)
**  COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
            R.NEW(LD.CHRG.AMOUNT)=''

            FOR I = 2 TO COUNT.AMT
                IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
            NEXT I
        END
    END
    CALL REBUILD.SCREEN

    RETURN
END
