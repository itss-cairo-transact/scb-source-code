* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********ABEER**********
*-----------------------------------------------------------------------------
* <Rating>248</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.INWARD.ISSUE.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CONFIRMED

   IF COMI THEN

        DAT=COMI
        FN.SCB.LG.CONFIRMED='F.SCB.LG.CONFIRMED';ID=ID.NEW:".":DAT;R.CHRG='';F.CHRG=''
        CALL F.READ(FN.SCB.LG.CONFIRMED,ID,R.CHRG,F.CHRG, E)
*******************************************************************
        IF NOT(E) THEN
            IF R.CHRG<SCB.CONFIRMED>='YES' THEN
                ETEXT='Aleardy.Confirmed.Before'
            END ELSE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> THEN R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2111'
*Line [ 44 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                COUNT.AMT=DCOUNT( R.CHRG<SCB.AMOUNT>,@VM)
                FOR I = 1 TO COUNT.AMT
                    R.NEW( LD.CHRG.CODE)< 1,I> = R.CHRG<SCB.CODE,I>
                    R.NEW( LD.CHRG.AMOUNT)< 1,I> = R.CHRG<SCB.AMOUNT,I>
                NEXT I
            END
*****************************************************************
        END ELSE
*Line [ 53 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE=DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
*Line [ 55 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
            R.NEW(LD.CHRG.AMOUNT)=''
            R.NEW(LD.CHRG.CODE)=''
            FOR I = 2 TO COUNT.AMT
                IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN
                    DEL R.NEW(LD.CHRG.CODE)<1,I>
                    DEL R.NEW(LD.CHRG.AMOUNT)<1,I>
                END
            NEXT I
            ETEXT = ''
            ETEXT = 'Date.Not.Found' ;RETURN
* IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='5'
        END
    END
**    CALL REBUILD.SCREEN
    RETURN
END
