* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>684</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.REV.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF COMI = 'R' THEN
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,ID.NEW,NEW.AMT)
*            NEW.AMT=R.NEW(LD.AMOUNT)
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            OP.CODE=MYLOCAL<1,LDLR.OPERATION.CODE>
            IF OP.CODE EQ 1111 THEN ETEXT='Reversing.This.Operation.Code.From.Another.Version'
****************************************************************************
            IDHIS=ID.NEW:";":"..."
            T.SEL = "SSELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ": IDHIS
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF KEY.LIST THEN
                OLD.ID= KEY.LIST<SELECTED>
                ID=FIELD(OLD.ID,';',1)

                FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
                CALL OPF(FN.LG.HIS,F.LG.HIS)
                CALL F.READ(FN.LG.HIS,OLD.ID, R.LG.HIS, F.LG.HIS ,E)

                OLD.AMT=R.LG.HIS<LD.AMOUNT>
                OLD.CORR.NO=R.LG.HIS<LD.CUSTOMER.REF>
                OLD.LIMIT=R.LG.HIS<LD.LIMIT.REFERENCE>
                OLD.FIN.DATE=R.LG.HIS<LD.FIN.MAT.DATE>

                LOCAL.REF.HIS=R.LG.HIS<LD.LOCAL.REF>
                OLD.CODE=LOCAL.REF.HIS<1,LDLR.OPERATION.CODE>
                OLD.MARG.PER=LOCAL.REF.HIS<1,LDLR.MARGIN.PERC>
                OLD.MARG.AMT=LOCAL.REF.HIS<1,LDLR.MARGIN.AMT>
                OLD.EXP=LOCAL.REF.HIS<1,LDLR.ACTUAL.EXP.DATE>
                OLD.APPROVE=LOCAL.REF.HIS<1,LDLR.APPROVAL>
                OLD.CORR.DATE=LOCAL.REF.HIS<1,LDLR.APPROVAL.DATE>
                OLD.DRACCT=LOCAL.REF.HIS<1,LDLR.DEBIT.ACCT>
                OLD.END.COMM.DATE=LOCAL.REF.HIS<1,LDLR.END.COMM.DATE>
                OLD.EXTENSION.NO=LOCAL.REF.HIS<1,LDLR.NO.OF.DAYS>
            END
***************** OP.CODE EQ 1232 OR OP.CODE EQ 1233**********************
            IF OP.CODE EQ 1232 OR OP.CODE EQ 1233 OR OP.CODE EQ 1234 OR OP.CODE EQ 1235 THEN
                IF OP.CODE EQ 1232 OR OP.CODE EQ 1234 THEN
                    MYAMT=NEW.AMT-OLD.AMT
                    R.NEW(LD.AMOUNT.INCREASE)= MYAMT*-1
                    IF OP.CODE EQ 1234 THEN
                        R.NEW(LD.FIN.MAT.DATE)=OLD.FIN.DATE
                        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=OLD.EXP
                        R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>=OLD.EXTENSION.NO
                    END
                END ELSE
                    IF OP.CODE EQ 1233 OR OP.CODE EQ 1235 THEN
                        MYAMT=OLD.AMT-NEW.AMT
                        R.NEW(LD.AMOUNT.INCREASE)= MYAMT*1
                        IF OP.CODE EQ 1235 THEN
                            R.NEW(LD.FIN.MAT.DATE)=OLD.FIN.DATE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=OLD.EXP
                            R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>=OLD.EXTENSION.NO
                        END
                    END
                END
            END
            R.NEW(LD.AMT.V.DATE)=TODAY
            R.NEW(LD.CUSTOMER.REF)=OLD.CORR.NO
            R.NEW(LD.LIMIT.REFERENCE)=OLD.LIMIT

            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>=OLD.CODE
            R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>=OLD.APPROVE
            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>=OLD.MARG.AMT
            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=OLD.MARG.PER
            R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>=OLD.CORR.DATE
            R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>=OLD.DRACCT
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>=OLD.END.COMM.DATE
            IF OP.CODE EQ 1231 THEN
                R.NEW(LD.FIN.MAT.DATE)=OLD.FIN.DATE
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=OLD.EXP
                R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>=OLD.EXTENSION.NO
            END

            CALL REBUILD.SCREEN
*******************************************************************************
        END
        TEXT=R.OLD(LD.CHRG.CODE):'CHRGCODE':R.OLD(LD.CHRG.AMOUNT):'CHRGAMT';CALL REM

    END
    RETURN
END
