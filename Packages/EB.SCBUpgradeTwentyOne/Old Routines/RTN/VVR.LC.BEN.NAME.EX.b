* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.BEN.NAME.EX

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    ETEXT = ''
    IF V$FUNCTION='I' THEN
        IF R.NEW(TF.LC.BENEFICIARY.CUSTNO) EQ '' THEN
            IF COMI THEN
                FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
                CALL OPF( FN.CU,F.CU)
                CALL F.READ( FN.CU, COMI, R.CU, F.CU, ETEXT)
                IF NOT(ETEXT) THEN
                    R.NEW(TF.LC.BENEFICIARY)<1,2> = R.CU<EB.CUS.NAME.1>
                    R.NEW(TF.LC.BENEFICIARY)<1,3> = R.CU<EB.CUS.STREET>
                END
                CLOSE F.CU
                CALL REBUILD.SCREEN
            END
        END
    END

    RETURN
END
