* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>298</Rating>
*-----------------------------------------------------------------------------

** ----- 03.12.2002 Pawel TEMENOS EE -----
SUBROUTINE VVR.LD.DEPOSIT.CATEGORY

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

DEFFUN GET.LD.CATEGORY()
 
   IF V$FUNCTION = 'I' AND MESSAGE # 'VAL' AND COMI THEN

      IF R.NEW( LD.VALUE.DATE) AND R.NEW( LD.CURRENCY) THEN

         POSITION = R.NEW( LD.LOCAL.REF)< 1, LDLR.PERSONCORP>
   *      IF POSITION THEN

            CATEGORY = GET.LD.CATEGORY( R.NEW( LD.VALUE.DATE), COMI, R.NEW( LD.CURRENCY), 'DEPOSIT', POSITION)

             IF CATEGORY THEN R.NEW( LD.CATEGORY) = CATEGORY ; CALL REBUILD.SCREEN
            ELSE ETEXT = '��� ������� ��� ����� ��'

    *     END ELSE ETEXT = 'POSITION NEEDED'

      END ELSE ETEXT = '����� ����� ���� � ������'

   END

RETURN
END
