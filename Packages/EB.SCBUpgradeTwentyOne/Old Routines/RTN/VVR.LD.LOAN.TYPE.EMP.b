* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LD.LOAN.TYPE.EMP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.CODE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN
*****************UPDATED BY RIHAM R15********************
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,COMI,COMP.BOOK)
            CUS.BR = COMP.BOOK[8,2]
            AC.OFICER = TRIM(CUS.BR, "0" , "L")
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRN.NAME)

***************************************************************************

            CALL REBUILD.SCREEN
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,SEC)
            ETEXT = '' ; NAME1 = '' ; LOC1 = '' ; LOC2 = ''
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,COMI,LOC1 )
            NAME1 = LOC1< 1,CULR.CBE.NO>

** IF NAME1 = '' THEN
** ETEXT = "No.CBE.Code.For.This.Customer"
** CALL REBUILD.SCREEN
** END
            IF NAME1 NE '' THEN

                R.NEW(LD.CENTRAL.BANK.CODE) = NAME1
            END

**********************UPDATE BY NI7OOOOOOOO (20091018)************
            IF SEC >= 1000 AND SEC < 3000 THEN

                R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = 'PERSON'
            END
            IF SEC >= 4000 AND SEC < 5000 THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = 'CORPORATE'
            END

**************** CATEGORY 1002 **************

            CUS.ID = COMI
            AC.ID  = FMT(CUS.ID,'R%8')

            R.NEW(LD.PRIN.LIQ.ACCT)    = AC.ID:'10100201'

*********************************************
***** TOTAL LOANS *****

            FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)

            WS.CUS.ID = COMI
            T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21055 AND CUSTOMER.ID EQ ":WS.CUS.ID:" AND STATUS NE 'LIQ'"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E4)
                    WS.AMT += R.LD<LD.DRAWDOWN.ISSUE.PRC>

                NEXT I
            END

            TEXT = '������ ���� ������ = ':WS.AMT ; CALL REM


*********************************************
        END
        RETURN
    END
