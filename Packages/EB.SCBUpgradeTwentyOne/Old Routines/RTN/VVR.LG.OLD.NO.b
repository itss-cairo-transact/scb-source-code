* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>750</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.OLD.NO


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    FN.LDD = 'F.LD.LOANS.AND.DEPOSITS.OLD.NO' ; F.LDD = ''
    CALL OPF(FN.LDD,F.LDD)
    POSS = ''

    CALL F.READ(FN.LDD,COMI,R.LDD,F.LDD,E11)

    LOOP
        REMOVE CO.ID FROM R.LDD  SETTING POS1
    WHILE CO.ID:POS1
        COO =  FIELD(CO.ID,'*',1)

        IF COO EQ ID.COMPANY  THEN

            ETEXT = ' ALREADY USED IN THIS COMPANY' ; CALL STORE.END.ERROR
        END

    REPEAT


    CALL REBUILD.SCREEN

    RETURN
END
