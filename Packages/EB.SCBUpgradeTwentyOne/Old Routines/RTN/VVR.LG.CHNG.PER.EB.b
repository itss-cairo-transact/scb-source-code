* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CHNG.PER.EB

*RECALCULATE THE MARG.AMT  IN CASE THERE IS A CHANGE IN THE MARGIN.PERC

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF COMI EQ 0 THEN R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = '0.00'
    IF MESSAGE NE 'VAL' THEN

        IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> LT 0 THEN ETEXT = "MARGIN.AMT.MUST.BE.GT.ZERO"
***       IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> LE 0 THEN ETEXT = "MARGIN.PERCENT.MUST.BE.GT.ZERO"
        CALL REBUILD.SCREEN
        IF COMI THEN
            IF COMI > 100 THEN ETEXT = 'MUST.BE.LESS.OR.EQUAL.100' ; CALL STORE.END.ERROR
            IF COMI = 100 THEN
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)*(COMI/100)
*       R.NEW(LD.LIMIT.REFERENCE)=''
                CALL REBUILD.SCREEN
            END ELSE
           MARG.AMT= R.NEW(LD.AMOUNT)*(COMI/100)
            CALL EB.ROUND.AMOUNT ('EGP',MARG.AMT,'',"2")
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = MARG.AMT
*      R.NEW(LD.LIMIT.REFERENCE)=''
                CALL REBUILD.SCREEN
            END
            CALL REBUILD.SCREEN
        END
    END

    RETURN
END
