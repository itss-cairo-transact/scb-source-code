* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.GRP.COMM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.GROUP.CONDITION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN
        MARG.PERC=R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
        PERIOD.NO=R.NEW(LD.LOCAL.REF)<1,94>
        TEXT=MARG.PERC;CALL REM
        IF MARG.PERC EQ 100 THEN
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> NE '1239' THEN
                OPER.NO= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>:".10"
            END ELSE
                OPER.NO= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
                TEXT=OPER.NO;CALL REM
            END
        END ELSE
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            OLD.PERC=MYLOCAL<1,LDLR.MARGIN.PERC>
            IF OLD.PERC EQ 100 THEN
                OPER.NO= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>:".P1"
                TEXT=OPER.NO:"KK";CALL REM
            END ELSE
                OPER.NO= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
            END
        END
        CALL DBR('LD.GROUP.CONDITION':@FM:LD.GRC.CHARGE.TYPE,OPER.NO,CHRG.TYPE)
*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CHRG.NO =DCOUNT(CHRG.TYPE,@VM)
        FOR I = 1 TO CHRG.NO
            R.NEW(LD.CHRG.CODE)<1,I>=CHRG.TYPE<1,I>
        NEXT I
        CALL REBUILD.SCREEN
    END
    RETURN
END
