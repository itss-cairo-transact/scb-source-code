* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>149</Rating>
*-----------------------------------------------------------------------------
** ----- WAEL  -----

SUBROUTINE VVR.LD.INT.SPREAD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RATES


IF V$FUNCTION = 'A' THEN
IF R.NEW(LD.INTEREST.SPREAD) # ''  THEN



   X = ''
   XX = ''
   XX = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY):R.NEW(LD.VALUE.DATE)
   CALL DBR('SCB.RATES':@FM:SCB.RAT.INTEREST.SPREAD,XX,X)

     IF NOT(ETEXT) THEN
         IF R.NEW(LD.INTEREST.SPREAD) #  X  THEN E ='LESS THAN THE SPREAD IN RATES' ; CALL ERR ; MESSAGE = 'REPEAT'
        * CALL STORE.OVERRIDE
     END

END
END
*====================================================================
RETURN
END
