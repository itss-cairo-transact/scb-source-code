* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>550</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.LOAN.TYPE.AB
***********************************************

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.CODE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

************************************************

    IF MESSAGE NE 'VAL' THEN

        IF COMI THEN
*****************UPDATED BY RIHAM R15********************
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,COMI,COMP.BOOK)
            CUS.BR = COMP.BOOK[8,2]
            AC.OFICER = TRIM(CUS.BR, "0" , "L")
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRN.NAME)
*   CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,COMI,BRN)
*   CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRN,BRN.NAME)
*****************************************************************
*            R.NEW(LD.LOCAL.REF)<1,LDLR.CUSTOMER.BRANCH> = BRN.NAME
            CALL REBUILD.SCREEN
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,SEC)
            ETEXT = '' ; NAME1 = '' ; LOC1 = '' ; LOC2 = ''
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,COMI,LOC1 )
            NAME1 = LOC1< 1, CULR.CBE.NO>
            IF NOT(NAME1) THEN ETEXT = "No.CBE.Code.For.This.Customer"
            ELSE R.NEW(LD.CENTRAL.BANK.CODE) = NAME1
********************************************************************
            IF LEN(COMI) = 7 THEN
                IF COMI[2,1] EQ 4 OR COMI[2,1] EQ 5 OR COMI[2,1] EQ 6 THEN
                    ETEXT = 'This.Customer.is.Not.Allowed' ;R.NEW(LD.CUSTOMER.ID) = '' ; R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = ''
                END
            END ELSE
                IF COMI[3,1] EQ 4 OR COMI[3,1] EQ 5 OR COMI[3,1] EQ 6 THEN
                    ETEXT = 'This.Customer.is.Not.Allowed' ;R.NEW(LD.CUSTOMER.ID) = '' ; R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = ''
                END
            END
*************************************************************************
            IF SEC >= 1000 AND SEC < 3000 THEN R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = 'PERSON'
            ELSE
                IF SEC >= 4000 AND SEC <= 5000 THEN R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = 'CORPORATE'
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
