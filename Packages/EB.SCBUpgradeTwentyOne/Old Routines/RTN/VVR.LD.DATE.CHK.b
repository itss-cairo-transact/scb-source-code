* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
** ----- NESSREEN AHMED 11/09/2002,  -----
*-----------------------------------------------------------------------------
* <Rating>42</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.DATE.CHK

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PARMS


IF COMI THEN

  GOSUB INTIAL
  GOSUB CALCULATE.NO.DAYS

END

GOTO PROGRAM.END
***********************************************************************************************************************
INTIAL:
*-------------TO INTIALIZE ALL VARIABLES -----------------------------------------*

 ETEXT = '' ; TD = TODAY ; DX = '' ; FN.SCB.PARMS = 'F.SCB.PARMS' ; F.SCB.PARMS = '' ; R.SCB.PARMS = ''
 CATEGORY.CURRENCY = '' ; CATEGORY.CURRENCY.DATE = ''

***********************************************************************************************************************
CALCULATE.NO.DAYS:

  CALL CDD(" ",TD,COMI,DX)
  IF DX > 365 THEN
    ETEXT = 'IT.MUST.BE.IN.SAME.YEAR'
    DX = ''
  END ELSE
*  R.NEW(LD.JOINT.NOTES) = DX
  GOSUB CHK.VALUE.DATE
  END

RETURN

***********************************************************************************************************************
CHK.VALUE.DATE:

 ETEXT = ''
 CATEGORY.CURRENCY = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY)
 * R.NEW(LD.JOINT.NOTES) = CATEGORY.CURRENCY
 CALL OPF(FN.SCB.PARMS,F.SCB.PARMS)
 CALL F.READ(FN.SCB.PARMS,CATEGORY.CURRENCY,R.SCB.PARMS,F.SCB.PARMS,E1)
   IF NOT(E1) THEN 
*     R.NEW(LD.JOINT.NOTES) = R.SCB.PARMS<SCB.PARMS.FORW.MAX.DAYS>
     IF DX > R.SCB.PARMS<SCB.PARMS.FORW.MAX.DAYS> OR DX > R.SCB.PARMS<SCB.PARMS.BACK.MAX.DAYS> THEN
      ETEXT = 'SHOULD.NOT.BE.GT.MAX.DAYS'
     END
   END

CLOSE FN.FN.SCB.PARMS
GOSUB CHK.IN.SCB.RATES


RETURN

***********************************************************************************************************************
CHK.IN.SCB.RATES:

 ETEXT = ''
 CATEGORY.CURRENCY.DATE = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY):COMI
 *R.NEW(LD.JOINT.NOTES) = CATEGORY.CURRENCY.DATE
*  CALL DBR( 'SCB.RATES':@FM:1, CATEGORY.CURRENCY.DATE, MY.DUMMY)
*          IF ETEXT THEN E = ETEXT
*     R.NEW(LD.JOINT.NOTES) = MY.DUMMY


RETURN
***********************************************************************************************************************
PROGRAM.END:

 CALL REBUILD.SCREEN

RETURN
END
