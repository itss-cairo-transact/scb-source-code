* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
***********DALIA-SCB 06/04/2003***********

SUBROUTINE VVR.LC.LEP
*A ROUTINE TO CHECK:
                  * IF TF.LC.LC.TYPE DOES NOT THEN DISPLAY ERROR
                  * IF TF.LC.LC.TYPE IS CHANGED THEN EMPTY TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE,
                  * TF.LC.DOC.1ST.COPIES, TF.LC.DOC.2ND.COPIES
                  * IF LCLR.AIDLC & LCLR.TERMS HAVE VALUES THEN DEFAULT TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE
                  * WITH SCB.LCD.TERM:LCLR.AIDLC
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.TYPE.VER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS


IF COMI THEN

IF COMI[2] # 'EP' THEN ETEXT = 'This.Type.is.Not.Allowed'


END

RETURN
END
