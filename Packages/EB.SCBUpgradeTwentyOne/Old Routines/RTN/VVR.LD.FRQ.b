* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LD.FRQ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*--------------------------------------------------
    IF MESSAGE NE 'VAL' THEN
***** WRITE FRQ *****

        V.DATE   = R.NEW(LD.VALUE.DATE)
        MAT.DATE = R.NEW(LD.FIN.MAT.DATE)

        DAYS = "C"
        CALL CDD("",V.DATE,MAT.DATE,DAYS)

        IF DAYS GE 1 AND DAYS LE 7 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '1W'
        END

        IF DAYS GT 7 AND DAYS LE 28 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '2W'
        END

        IF DAYS GE 29 AND DAYS LE 59 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '1M'
        END

        IF DAYS GE 60 AND DAYS LE 89 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '2M'
        END

        IF DAYS GE 90 AND DAYS LE 179 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '3M'
        END

        IF DAYS GE 180 AND DAYS LE 269 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '6M'
        END

        IF DAYS GE 270 AND DAYS LE 364 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '9M'
        END

        IF DAYS GE 365 AND DAYS LE 729 THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '12M'
        END
************************
***** WRITE SPREAD.RATE ****

        WS.AMT = R.NEW(LD.AMOUNT)
        WS.FRQ = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>

        IF WS.FRQ = '12M' THEN
            IF WS.AMT LT 100000 THEN R.NEW(LD.INTEREST.SPREAD) = -4.5
            IF WS.AMT GE 100000 AND WS.AMT LT 500000 THEN R.NEW(LD.INTEREST.SPREAD) = -3.5
            IF WS.AMT GE 500000 AND WS.AMT LT 1000000 THEN R.NEW(LD.INTEREST.SPREAD) = -2.5
            IF WS.AMT GE 1000000 AND WS.AMT LT 5000000 THEN R.NEW(LD.INTEREST.SPREAD) = -2
            IF WS.AMT GE 5000000 AND WS.AMT LT 10000000 THEN R.NEW(LD.INTEREST.SPREAD) = -1.5
            IF WS.AMT GE 10000000 THEN R.NEW(LD.INTEREST.SPREAD) = -1
        END ELSE

            IF WS.FRQ = '6M' OR WS.FRQ = '9M' THEN
                IF WS.AMT LT 100000 THEN R.NEW(LD.INTEREST.SPREAD) = -5
                IF WS.AMT GE 100000 AND WS.AMT LT 500000 THEN R.NEW(LD.INTEREST.SPREAD) = -4
                IF WS.AMT GE 500000 AND WS.AMT LT 1000000 THEN R.NEW(LD.INTEREST.SPREAD) = -3
                IF WS.AMT GE 1000000 AND WS.AMT LT 5000000 THEN R.NEW(LD.INTEREST.SPREAD) = -2.5
                IF WS.AMT GE 5000000 AND WS.AMT LT 10000000 THEN R.NEW(LD.INTEREST.SPREAD) = -2
                IF WS.AMT GE 10000000 THEN R.NEW(LD.INTEREST.SPREAD) = -1.5
            END ELSE
                IF WS.FRQ = '1W' OR WS.FRQ = '2W' THEN
                    R.NEW(LD.INTEREST.SPREAD) = -5.5
                END ELSE
                    IF WS.FRQ = '1M' OR WS.FRQ = '2M' THEN
                        R.NEW(LD.INTEREST.SPREAD) = -3
                    END ELSE
                        IF WS.FRQ = '3M' THEN
                            R.NEW(LD.INTEREST.SPREAD) = -2.5
                        END
                    END
                END
            END
        END
        RETURN
    END
