* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE

*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.MM.CBE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

    MYDATE=''
    MYYEAR=COMI[1,4]
    OLDMONTH=COMI[5,2]
    MYDAY=COMI[7,2]
 
    NEWMONTH=OLDMONTH + 3
     IF OLDMONTH GT '9' THEN
       NEWMONTH= NEWMONTH - 12
       MYYEAR=MYYEAR + 1
    END

      IF NEWMONTH= '2' AND MYDAY GT 28 THEN
       MYDAY='28'
     END ELSE
     IF (NEWMONTH='4' OR NEWMONTH='6' OR NEWMONTH= '9' OR NEWMONTH= '11') AND MYDAY GT 30 THEN
      MYDAY='30'
END
END
    MYDATE=FMT(MYYEAR,'R%4'):FMT(NEWMONTH,'R%2'):FMT(MYDAY,'R%2')
    R.NEW(MM.MATURITY.DATE) = MYDATE
    CALL REBUILD.SCREEN
    RETURN
END
