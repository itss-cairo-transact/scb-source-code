* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.DEPOSIT.CUSTOMER.ACCT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY
    IF MESSAGE EQ 'VAL' THEN
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,COMI,Y.AC.COM)
        TEXT = "C1" : COMP ; CALL REM
        TEXT = "C2" : Y.AC.COM ; CALL REM
*   IF Y.AC.COM NE COMP THEN
*       ETEXT = "INVALID COMPANY FOR ACCOUNT "
*       CALL STORE.END.ERROR
*       CALL REBUILD.SCREEN
*   END
    END
    IF LEN(COMI) = '16' THEN
***   IF (COMI[11,4] NE '1001') AND (COMI[11,4] NE '6501') THEN
***     ETEXT = '�� ���� ���� ����� �� ������ ';CALL STORE.END.ERROR
***   END
    END
*    IF LEN(COMI) NE '16' THEN
*       ETEXT = '�� ���� ���� ����� �� ������';CALL STORE.END.ERROR
*  END
    CALL DBR ('ACCOUNT':@FM:AC.POSTING.RESTRICT,COMI,POST.REST)
    IF POST.REST THEN
        CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST.REST,DESC)
        ETEXT = '���� ������ ':DESC ;CALL STORE.END.ERROR
    END
    CALL REBUILD.SCREEN
*********************************************************
    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    CALL OPF(FN.AC,F.AC)
    IF MESSAGE = 'VAL' THEN
        CALL F.READ(FN.AC,COMI,R.AC,F.AC,Y.ERR)
        Y.LD.CUR = R.NEW(LD.CURRENCY)
        Y.AC.CUR = R.AC<AC.CURRENCY>
        IF Y.LD.CUR NE Y.AC.CUR THEN
            ETEXT = "THE CURRENCY NOT A SAME"
            CALL STORE.END.ERROR
        END
        WS.LD.CUST = R.NEW(LD.CUSTOMER.ID)
        WS.AC.CUST = R.AC<AC.CUSTOMER>
        IF WS.LD.CUST NE WS.AC.CUST THEN
            ETEXT = "THE CUSTOMER NOT A SAME"
            CALL STORE.END.ERROR
        END

    END
*********************************************************
    RETURN
END
