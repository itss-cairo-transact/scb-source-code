* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.CUS.STAFF


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PARMS
    IF MESSAGE # 'VAL' THEN
        CUST = R.NEW(LD.CUSTOMER.ID)
        IF LEN(CUST) = 8 THEN
            CUS.CLAS = CUST[3,1]
        END ELSE
            CUS.CLAS = CUST[2,1]
        END
        IF CUS.CLAS = 1 THEN
            ID.PARMS = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY)
            T.SEL = "SELECT F.SCB.PARMS WITH @ID LIKE ":ID.PARMS:"..."
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF KEY.LIST THEN
                DIM AA(SELECTED)
                FOR I = 1 TO SELECTED
                    D1 = RIGHT(KEY.LIST<I>,8)
                    AA(I) = D1
                NEXT I
                TEMP = AA(1)
                SORT1 = 1
                FOR I = 2 TO SELECTED
                    IF TEMP < AA(I)  THEN TEMP = AA(I); SORT1 =I
                NEXT I
                ID1 = KEY.LIST<SORT1>
                F.PARMS = '' ; FN.PARMS = 'F.SCB.PARMS' ; R.PARMS = ''
                CALL OPF(FN.PARMS,F.PARMS)
                CALL F.READ( FN.PARMS, ID1, R.PARMS, F.PARMS, ETEXT)
                TEMP = R.PARMS<SCB.PAR.STAFF.SPREAD>
                R.NEW(LD.INTEREST.SPREAD) =  TEMP
                CALL REBUILD.SCREEN
            END

        END
    END


    RETURN
END
