* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>446</Rating>
*-----------------------------------------------------------------------------
********* ABEER 01/6/2003**************
*A Routine To Change Operation Code If Maturity.Date Is Changed OR Amount Changed
    SUBROUTINE VVR.LG.INWARD.AMEND.FIN.DATE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN

            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
            MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*            COUNT.CODE = DCOUNT(R.NEW(LD.CHRG.CODE),VM)
*            COUNT.AMT  = DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
*Line [ 39 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT=DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>,@SM)
****************************************************************************************
            IF COMI THEN
*     IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>=''THEN R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>='5'
*    R.NEW(LD.CHRG.AMOUNT)=''
**************************************************************************
                R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>=''
                FOR I = 2 TO COUNT.AMT
*  IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
                    IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>='' THEN
                        DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
                        DEL  R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
                    END
                NEXT I
************************************************************************
                IF COMI LT OLD.MAT.DATE  THEN
                    ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date'
                END ELSE
                    IF COMI # OLD.MAT.DATE  THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=COMI
                        IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2231'
                          *  R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2231'
                        END ELSE
                            IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2234'
                           *     R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2234'
                            END ELSE
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2235'
                            *    R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2235'
                            END
                        END
                    END
                END
            END
***********************************************************************
        END
    END
    RETURN
END
