* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
************ABEER***************
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.INWARD.TYPE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN
        INWARD=COMI[1,2]
        IF INWARD NE 'IN' THEN
            ETEXT='Must.Be.Inward.Type'
        END  ELSE
            T.SEL= "SSELECT F.SCB.LG.PARMS WITH @ID EQ ": COMI
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF NOT(SELECTED) THEN
                ETEXT='It.Must.Be.Existing.Inward.Type'
            END ELSE

                IF KEY.LIST ='INFINAL' AND PGM.VERSION NE ',SCB.LG.INWARD.FINAL' THEN
                     ETEXT='This.Version.Not.For.LG.Inward.Final'
                END
                IF KEY.LIST ='INAD' AND PGM.VERSION NE ',SCB.LG.INWARD.ADVANCE' THEN
                     ETEXT='This.Version.Not.For.LG.Inward.Advance'
                END
                IF KEY.LIST ='INBB' AND PGM.VERSION NE ',SCB.LG.INWARD.BIDBOND' THEN
                    ETEXT='This.Version.Not.For.LG.Inward.BidBond'
                END
******
            END
        END
    END
    RETURN
END
