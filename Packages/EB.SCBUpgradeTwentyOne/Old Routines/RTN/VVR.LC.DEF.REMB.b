* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
**** INGY-SCB 11/12/2003****
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.DEF.REMB

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT


    VER = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.VERSION.NAME>
    CALL OPF('F.SCB.LC.DEFAULT',F.SCB.LC.DEFAULT)
*******************************UPDATED BY RIHAM R15**********************
* READ R.SCB.LC.DEFAULT FROM F.SCB.LC.DEFAULT,VER THEN
    CALL F.READ('F.SCB.LC.DEFAULT',VER,R.SCB.LC.DEFAULT,F.SCB.LC.DEFAULT,ERR)
********************************************************************************
    YY = R.SCB.LC.DEFAULT<SCB.LCV.BNK.T.BNK>
    NN = R.SCB.LC.DEFAULT<SCB.LCV.NARR.PERIOD>
    IF COMI THEN
        IF VER = ",SCB.LIS" OR VER = ",SCB.LIM" THEN
            IF R.NEW(TF.LC.THIRD.PARTY.CUSTNO) #'' THEN
                R.NEW(TF.LC.INSTRUCTIONS) = R.SCB.LC.DEFAULT<SCB.LCV.BNK.T.BNK>
            END ELSE
                R.NEW(TF.LC.INSTRUCTIONS) = R.SCB.LC.DEFAULT<SCB.LCV.NARR.PERIOD>
            END
        END ELSE
            R.NEW(TF.LC.INSTRUCTIONS) = R.SCB.LC.DEFAULT<SCB.LCV.BNK.T.BNK>
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
