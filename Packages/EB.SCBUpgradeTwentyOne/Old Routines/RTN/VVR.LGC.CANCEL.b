* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LGC.CANCEL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS



    CALL DBR ('LETTER.OF.CREDIT':@FM:TF.LC.LOCAL.REF,ID.NEW,MYLOCAL)
    MAT.EXP= R.NEW(TF.LC.ADVICE.EXPIRY.DATE)
    MYCODE=MYLOCAL<1,LCLR.OPERATION.CODE>

    IF COMI = "EXPIRED" THEN
        IF  TODAY LT MAT.EXP THEN
            ETEXT = 'Not Expired Yet' ; CALL STORE.END.ERROR
            ETEXT = ''
        END ELSE
            IF MYCODE EQ "1309" THEN
                TEXT = 'MYCO=':MYCODE ; CALL REM
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = '1301'
                TEXT = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> ; CALL REM
*     R.NEW(TF.LC.REL.PROVIS) = 'Y'
                R.NEW(TF.LC.FULLY.UTILISED) = 'Y'
                R.NEW(TF.LC.EXPIRY.DATE)= TODAY
                R.NEW(TF.LC.ADVICE.EXPIRY.DATE)= TODAY
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ACTUAL.EXP.DATE> = R.NEW(TF.LC.ADVICE.EXPIRY.DATE)

            END ELSE
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1309"
                R.NEW(TF.LC.REL.PROVIS) = ''
                R.NEW(TF.LC.FULLY.UTILISED) = ''

            END
        END

    END
    IF COMI ="BENF." THEN
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1303"
*    R.NEW(TF.LC.REL.PROVIS) = 'Y'
        R.NEW(TF.LC.FULLY.UTILISED) = 'Y'
        R.NEW(TF.LC.EXPIRY.DATE)= TODAY
        R.NEW(TF.LC.ADVICE.EXPIRY.DATE)= TODAY
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ACTUAL.EXP.DATE> = R.NEW(TF.LC.ADVICE.EXPIRY.DATE)
    END
    IF COMI ="CUSTOMER" THEN
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1302"
*   R.NEW(TF.LC.REL.PROVIS) = 'Y'
        R.NEW(TF.LC.FULLY.UTILISED) = 'Y'
        R.NEW(TF.LC.EXPIRY.DATE)= TODAY
        R.NEW(TF.LC.ADVICE.EXPIRY.DATE)= TODAY
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ACTUAL.EXP.DATE> = R.NEW(TF.LC.ADVICE.EXPIRY.DATE)

    END
    IF COMI EQ 'NO.CHEQ' THEN
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1302"
*   R.NEW(TF.LC.REL.PROVIS) = 'Y'
        R.NEW(TF.LC.FULLY.UTILISED) = 'Y'
        R.NEW(TF.LC.EXPIRY.DATE)= TODAY
        R.NEW(TF.LC.ADVICE.EXPIRY.DATE)= TODAY
        R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ACTUAL.EXP.DATE> = R.NEW(TF.LC.ADVICE.EXPIRY.DATE)


*    ETEXT='Only Advance IS Allowed';CALL STORE.END.ERROR
        ETEXT = ''
    END
    CALL REBUILD.SCREEN
    RETURN
END
