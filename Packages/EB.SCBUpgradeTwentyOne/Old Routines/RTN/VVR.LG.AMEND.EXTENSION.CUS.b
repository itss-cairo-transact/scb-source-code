* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>390</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.EXTENSION.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI EQ 'BEN' THEN
        ETEXT='BENF.IS.NOT.ALLOWED' ; CALL STORE.END.ERROR
        CALL REBUILD.SCREEN
    END


    IF V$FUNCTION = 'I' THEN

        IF COMI EQ 'BEN' THEN
            ETEXT='BENF.IS.NOT.ALLOWED'
        END

        CALL REBUILD.SCREEN


        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        OLD.MAT.DATE=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
        PRO.TYPE=MYLOCAL<1,LDLR.PRODUCT.TYPE>
        OLD.CODE=MYLOCAL<1,LDLR.OPERATION.CODE>
        NEW.MAT.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        NEW.CODE= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        IF COMI EQ 'BEN' THEN
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1234' THEN
                ETEXT='BENF.IS.NOT.ALLOWED'
                COMI='CUS'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1232' THEN
                ETEXT='BENF.IS.NOT.ALLOWED'
                COMI='CUS'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1271' THEN
                ETEXT='BENF.IS.NOT.ALLOWED'
                COMI='CUS'
            END
            IF PRO.TYPE EQ 'BIDBOND' THEN
                IF NEW.CODE EQ '1231' THEN
                    IF OLD.CODE NE '1239' THEN
                        ETEXT='BENF.IS.NOT.ALLOWED'
                        COMI='CUS'
                    END
                END
            END
        END ELSE
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1233' THEN
                ETEXT='CUST.IS.NOT.ALLOWED'
                COMI='BEN'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1235' THEN
                ETEXT = 'CUST.IS.NOT.ALLOWED'
                COMI = 'BEN'
            END
        END

**********************************************************
        IF NOT(COMI) THEN
            IF NEW.MAT.DATE GT OLD.MAT.DATE THEN
                CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
                MYOLD.CODE=MYLOCAL<1,LDLR.OPERATION.CODE>

                IF MYOLD.CODE NE '1239' THEN
                    ETEXT = 'You.Should.Enter.Approval.By'
                END

            END
        END
        RETURN
    END
