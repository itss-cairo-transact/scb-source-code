* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>764</Rating>
*-----------------------------------------------------------------------------
********* ABEER 12/3/2003**************
*A Routine To Change Operation Code If Maturity.Date Is Changed OR Amount Changed
    SUBROUTINE VVR.LG.AMEND.AMTDATE.NEW1

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


**********20081114***HHHHH******************
***************HHHHHH*************************
    IF COMI THEN
        R.NEW(LD.CHRG.CODE)   = ""
        R.NEW(LD.CHRG.AMOUNT) = ""

        COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
        IF COMM EQ 'YES' THEN
            R.NEW(LD.CHRG.CODE)<1,1> = 13
            R.NEW(LD.CHRG.CODE)<1,2> = 14
            R.NEW(LD.CHRG.CODE)<1,3> = 18
        END ELSE

           ** IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> <  R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
               ** R.NEW(LD.CHRG.CODE)<1,1> = 13
               ** R.NEW(LD.CHRG.CODE)<1,2> = 14
               ** R.NEW(LD.CHRG.CODE)<1,3> = 18
**IF V$FUNCTION = 'I' THEN
**  CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CATEG)
** IF (CATEG GT 21096 AND CATEG LT 21099) THEN
** IF (CATEG GE 21096 AND CATEG LT 21099) THEN
** END
** END

           ** END ELSE
                IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                    R.NEW(LD.CHRG.CODE)<1,1> = 4
                    R.NEW(LD.CHRG.CODE)<1,2> = 14
** IF V$FUNCTION = 'I' THEN
**  CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CATEG)
**  IF (CATEG GT 21096 AND CATEG LT 21099) THEN
** IF (CATEG GE 21096 AND CATEG LT 21099) THEN
                    R.NEW(LD.CHRG.CODE)<1,3> = 18
** END
** END

                END
                IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                    R.NEW(LD.CHRG.CODE)<1,1> = 7
                    R.NEW(LD.CHRG.CODE)<1,2> = 14
** IF V$FUNCTION = 'I' THEN
**   CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CATEG)
** IF (CATEG GT 21096 AND CATEG LT 21099) THEN
** IF (CATEG GE 21096 AND CATEG LT 21099) THEN
                    R.NEW(LD.CHRG.CODE)<1,3> = 18
** END
** END

                END
                IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                    R.NEW(LD.CHRG.CODE)<1,1> = 9
                    R.NEW(LD.CHRG.CODE)<1,2> = 14
** IF V$FUNCTION = 'I' THEN
**   CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CATEG)
** IF (CATEG GT 21096 AND CATEG LT 21099) THEN
** IF (CATEG GE 21096 AND CATEG LT 21099) THEN
                    R.NEW(LD.CHRG.CODE)<1,3> = 18
** END
** END

              ***  END
            END
        END

    END

********************************************

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN

* CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
            OLD.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

            MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*Line [ 111 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
*Line [ 113 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT  = DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
****************************************************************************************
            IF COMI THEN
                R.NEW(LD.CHRG.AMOUNT)=''
                R.NEW(LD.CHRG.CLAIM.DATE) = ''
                R.NEW(LD.CHRG.BOOKED.ON) = ''
                R.NEW(LD.CHRG.CAPITALISE) = ''
                IF COMI LT OLD.MAT.DATE  THEN
                    ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date'
                END  ELSE
                    IF COMI # OLD.MAT.DATE  THEN
*****      R.NEW(LD.FIN.MAT.DATE)=COMI
                        R.NEW(LD.FIN.MAT.DATE)='20991231'
                        IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
***** R.NEW(LD.FIN.MAT.DATE)=COMI
                            R.NEW(LD.FIN.MAT.DATE)='20991231'
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1234'
                        END  ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1235'
                            R.NEW(LD.FIN.MAT.DATE)='20991231'
                        END

                    END ELSE
**************************************************
                        IF COMI EQ OLD.MAT.DATE THEN
                            IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                                TEXT='YOU.MUST AMEND.DATE.OR.AMOUNT';CALL REM
                            END ELSE
                                IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
                                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
                                END ELSE
                                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1233'
                                    R.NEW(LD.FIN.MAT.DATE)='20991231'

                                END
                            END
                        END
                    END
                END
****CALL REBUILD.SCREEN
            END
        END
    END


*Line [ 159 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I

****CALL REBUILD.SCREEN


    RETURN
END
