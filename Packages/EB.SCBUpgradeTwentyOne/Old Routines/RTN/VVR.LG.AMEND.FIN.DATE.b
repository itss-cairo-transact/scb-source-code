* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1334</Rating>
*-----------------------------------------------------------------------------
********* ABEER 12/3/2003**************
*A Routine To Change Operation Code If Maturity.Date Is Changed OR Amount Changed
    SUBROUTINE VVR.LG.AMEND.FIN.DATE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN

* CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
            OLD.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

            MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
*Line [ 44 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT  = DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
****************************************************************************************
            IF COMI THEN
                R.NEW(LD.CHRG.AMOUNT)=''
                R.NEW(LD.CHRG.CODE) = '4'
                R.NEW(LD.CHRG.CLAIM.DATE) = TODAY
               * R.NEW(LD.CHRG.BOOKED.ON) = TODAY
                R.NEW(LD.CHRG.CAPITALISE) = ''
                CALL REBUILD.SCREEN
                IF COMI LT OLD.MAT.DATE  THEN
                    ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date'
                END  ELSE
                    IF COMI # OLD.MAT.DATE  THEN
                        R.NEW(LD.FIN.MAT.DATE)='20991231'
                        IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1231'
**********************************************
                        END   ELSE
                            IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
                                R.NEW(LD.FIN.MAT.DATE)='20991231'
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1234'
                            END  ELSE
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1235'
                                R.NEW(LD.FIN.MAT.DATE)='20991231'
************************************************************
                            END
                        END
                    END ELSE
**************************************************
                        IF COMI EQ OLD.MAT.DATE THEN
                            IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                                TEXT='YOU.MUST AMEND.DATE.OR.AMOUNT';CALL REM
                            END ELSE
                                IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
                                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
                                END ELSE
                                    IF R.NEW(LD.AMOUNT.INCREASE) < 0 THEN
                                        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1233'
                                    END
                                END
                            END
                        END
                    END
********************************
                END
            END
        END ELSE
********************
            IF MESSAGE EQ 'VAL' THEN
                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
                OLD.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

                IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>  EQ OLD.MAT.DATE THEN
                    IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                        ETEXT='YOU.MUST AMEND.DATE.OR.AMOUNT'
                    END ELSE
                        IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
                        END ELSE
                            IF R.NEW(LD.AMOUNT.INCREASE) < 0 THEN
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1233'
                            END
                        END
                    END
                END
            END
*********************
        END
    END
    RETURN
END
