* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** ABEER 11/05/2003**********
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.INWARD.AMT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS

    IF COMI THEN
        CUST.ID=R.NEW(LD.CUSTOMER.ID)
        CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,CUST.ID,LIMREF)
        CALL DBR('LIMIT':@FM:LI.ONLINE.LIMIT,LIMREF,ON.LIMIT)
        IF NOT(ETEXT) THEN
            IF ON.LIMIT LT COMI THEN
              **  ETEXT='LG.Amount.Must.Be.Less.Than.Online.Limit.For.Counter.Party'
            END
        END
*********************************************************
        IF MESSAGE NE 'VAL' THEN
*Line [ 48 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>,@SM)
*Line [ 50 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE =DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>,@SM)
            R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>=''
            FOR I = 2 TO COUNT.AMT
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I> THEN
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
                END
            NEXT I
            R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>='5'
            CALL REBUILD.SCREEN
        END
********************************************************

    END
    RETURN
END
