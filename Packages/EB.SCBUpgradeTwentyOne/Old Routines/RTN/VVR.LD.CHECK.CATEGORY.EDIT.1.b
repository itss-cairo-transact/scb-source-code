* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LD.CHECK.CATEGORY.EDIT.1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS



    V.DATE   = COMI
    MAT.DATE = R.NEW(LD.FIN.MAT.DATE)
    CATEG = R.NEW(LD.CATEGORY)

    DAYS = "C"
    CALL CDD("",V.DATE,MAT.DATE,DAYS)
    DAYS = DAYS+1
    R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS> = DAYS

    IF CATEG EQ 21001 THEN
        IF DAYS GE 1 AND DAYS LE 29 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� �����'
        END
    END

    IF CATEG EQ 21003 THEN
        IF DAYS GE 30 AND DAYS LE 59 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� ���'
        END
    END
    IF CATEG EQ 21004 THEN
        IF DAYS GE 60 AND DAYS LE 89 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� �����'
        END
    END

    IF CATEG EQ 21005 THEN
        IF DAYS GE 90 AND DAYS LE 179 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� 3 ����'
        END
    END

    IF CATEG EQ 21006 THEN
        IF DAYS GE 180 AND DAYS LE 364 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� 6 ����'
        END
    END

    IF CATEG EQ 21007 THEN
        IF DAYS GE 365 AND DAYS LE 729 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� ���'
        END
    END

    IF CATEG EQ 21008 THEN
        IF DAYS GE 730 AND DAYS LE 1094 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� �����'
        END
    END

    IF CATEG EQ 21009 THEN
        IF DAYS EQ 1095 THEN
        END ELSE
            ETEXT = '��� ������� �� ����� 3 �����'
        END
    END

    IF CATEG EQ 21010 THEN
        IF DAYS GT 1095 THEN
        END ELSE
            ETEXT = '��� ������� ��� �� 3 �����'
        END
    END




    RETURN
END
