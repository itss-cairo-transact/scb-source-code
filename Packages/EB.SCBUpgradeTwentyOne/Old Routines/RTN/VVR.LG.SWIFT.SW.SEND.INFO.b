* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>570</Rating>
*-----------------------------------------------------------------------------
*****RANIA****
*TO CHECK FIELD SWIFT SEND INFO ENTERED WITH SPECIAL FORMAT ACCORDING TO MESSAGE TYPE
    SUBROUTINE VVR.LG.SWIFT.SW.SEND.INFO

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.SWIFT.ACTION

    IF MESSAGE = 'VAL' THEN

*Line [ 36 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        CNT =DCOUNT(R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO>,@SM)
        IF CNT > 6 THEN
            FOR I = CNT TO 7 STEP -1
                DEL R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,I>
            NEXT I
        END
        MSG =  R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD>
        BEGIN CASE

        CASE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,1> = "760" OR R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,1> = "767"
            GOSUB BUILD.760.767

        CASE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,1> = "768"
            GOSUB BUILD.768

        CASE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,1> = "769"
            GOSUB BUILD.769

        END CASE
    END
    RETURN
***************************************************************************
BUILD.760.767:

    FIRSTWORD =FIELD(R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,1>,'/',2)
    IF FIRSTWORD # 'BENCON' AND FIRSTWORD # 'PHONBEN' AND FIRSTWORD # 'TELEBEN' THEN ETEXT = 'ERROR.CODE.11'

    FOR I = 2 TO CNT

        FIRSTWORD1 = R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,I>[1,2]
        TEXT = R.NEW(LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO>
        IF  FIRSTWORD1 # '//' THEN ETEXT = 'ERROR.CODE.2'
    NEXT I


    RETURN
***************************************************************************
BUILD.768:

    FIRSTWORD =FIELD(R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,1>,'/',2)
    IF FIRSTWORD # 'BENACC'  THEN ETEXT = 'ERROR.CODE.1'

    FOR I = 2 TO CNT

        FIRSTWORD1 = R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,I>[1,2]
        IF  FIRSTWORD1 # '//' THEN ETEXT = 'ERROR.CODE.2'
    NEXT I

    RETURN


***************************************************************************
BUILD.769:

    FIRSTWORD = R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,1>[1,2]
    IF  FIRSTWORD # '//' THEN ETEXT = 'ERROR.CODE.1'

    FOR I = 2 TO CNT

        FIRSTWORD1 = R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.SEND.INFO,I>[1,1]
        IF  FIRSTWORD1 = '/'  THEN ETEXT = 'ERROR.CODE.2'
    NEXT I

    RETURN

***************************************************************************
