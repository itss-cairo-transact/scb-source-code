* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
********* ABEER 12/3/2003**************
*A Routine To ensure That If Approval.By Is entered Then Approval.date Is Mandatory
    SUBROUTINE VVR.LG.INW.AMEND.EXTENSION

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
* CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
* NEW.MAT.DATE=R.NEW(LD.FIN.MAT.DATE)
        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        OLD.MAT.DATE=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>

        NEW.MAT.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        IF COMI EQ 'BEN' THEN
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '2234' THEN
                ETEXT='BENF.IS.NOT.ALLOWED'
                COMI='CUS'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '2232' THEN
                ETEXT='BENF.IS.NOT.ALLOWED'
                COMI='CUS'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '2235' THEN
                ETEXT='BENF.IS.NOT.ALLOWED'
                COMI='CUS'
            END

        END
**********************************************************
        IF NOT(COMI) THEN
            IF NEW.MAT.DATE GT OLD.MAT.DATE THEN
                CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
                MYOLD.CODE=MYLOCAL<1,LDLR.OPERATION.CODE>

                IF MYOLD.CODE NE '2239' THEN
                    ETEXT = 'You.Should.Enter.Approval.By'
                END

            END
        END
    END
    RETURN
END
