* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>907</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

    SUBROUTINE VVR.LC.TERMS
*A ROUTINE TO CHECK:
* IF LCLR.TERMS IS CHANGED THEN EMPTY TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE,
* TF.LC.DOC.1ST.COPIES, TF.LC.DOC.2ND.COPIES
* IF LCLR.AIDLC & TF.LC.LC.TYPE HAVE VALUES THEN DEFAULT TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE
* WITH SCB.LCD.TERM:LCLR.AID

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.ADVICE.TEXT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.CLAUSES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT

    IF V$FUNCTION = 'I' AND MESSAGE # "VAL" THEN
*TEXT = "BAKRY" ; CALL REM
*SAVE.COMI = COMI
        FN.SCB.LC.DOCTERMS = 'F.SCB.LC.DOCTERMS' ; F.SCB.LC.DOCTERMS = '' ; R.SCB.LC.DOCTERMS = ''
        DOC.ID = R.NEW(TF.LC.LC.TYPE):'.': R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC>

        IF COMI # R.NEW(TF.LC.LOCAL.REF)<1,LCLR.TERMS> THEN
            R.NEW(TF.LC.CLAUSES.TEXT)   = ''
            R.NEW(TF.LC.DOCUMENT.CODE)  = ''
            R.NEW(TF.LC.DOC.1ST.COPIES) = ''
            R.NEW(TF.LC.DOC.2ND.COPIES) = ''
            R.NEW( TF.LC.DOCUMENT.TXT) = ''
        END

        IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC> AND R.NEW(TF.LC.LC.TYPE) THEN
            CALL OPF( FN.SCB.LC.DOCTERMS,F.SCB.LC.DOCTERMS)
            CALL F.READ(FN.SCB.LC.DOCTERMS,DOC.ID , R.SCB.LC.DOCTERMS, F.SCB.LC.DOCTERMS, ETEXT)
            TRM = R.SCB.LC.DOCTERMS<SCB.LCD.TERM>
            LOCATE COMI IN TRM<1,1> SETTING POS THEN
*IF COMI = R.SCB.LC.DOCTERMS<SCB.LCD.TERM> THEN
*R.NEW(TF.LC.CLAUSES.TEXT) = R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS>
*Line [ 66 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                CO.DOC = DCOUNT(R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS>, @SM)
*Line [ 68 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                CO.ADD = DCOUNT(R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS>, @SM)
                CALL OPF('F.SCB.LC.DEFAULT',F.SCB.LC.DEFAULT)
*******************************UPDATED BY RIHAM R15**********************
**                READ R.SCB.LC.DEFAULT FROM F.SCB.LC.DEFAULT,"DEFAULT" THEN
CALL F.READ ('F.SCB.LC.DEFAULT',"DEFAULT",R.SCB.LC.DEFAULT,F.SCB.LC.DEFAULT,ERR)
******************************************************************************
                    DE.TERM = R.SCB.LC.DEFAULT<SCB.LCV.TERMS>
              *  END
                LOCATE COMI IN DE.TERM<1,1> SETTING POS1 THEN
                    DE.CODE = R.SCB.LC.DEFAULT<SCB.LCV.DOCUMENT.CODE,POS1>
                    DE.TEXT = R.SCB.LC.DEFAULT<SCB.LCV.DOCUMENT.TEXT,POS1>
                END
                FOR I = 1 TO CO.DOC
                    R.NEW(TF.LC.DOCUMENT.CODE)<1,I> =  R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS,I>
                    CALL DBR("LC.ADVICE.TEXT":@FM:TF.AD.NARRATIVE,R.NEW(TF.LC.DOCUMENT.CODE)<1,I>,NARR)
*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    RR = DCOUNT(NARR, @VM)
                    FOR Y = 1 TO RR
                        IF DE.CODE THEN
                            LOCATE R.NEW(TF.LC.DOCUMENT.CODE)<1,I> IN DE.CODE<1,1> SETTING POS2 THEN
                                IF NARR<1,Y> = "XXXXX" THEN
                                    NARR<1,Y> = CHANGE(NARR<1,Y>,"XXXXX",DE.TEXT<1,POS2>)
                                END
                            END
                        END
                        R.NEW(TF.LC.DOCUMENT.TXT)<1,I,Y> = NARR<1,Y>
                        CLOSE  F.SCB.LC.DOCTERMS
                    NEXT Y
                    R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1> = I:"-":R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1>
                NEXT I

                FOR CC = 1 TO CO.ADD
                    CALL DBR("LC.CLAUSES":@FM:LC.CL.DESCR,R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS,CC>,DESC)
*Line [ 102 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DD = DCOUNT (DESC,@VM)
                    TT = ''
                    FOR J = 1 TO DD
                        TT := ' ': DESC<1,J>
                    NEXT J
                    R.NEW(TF.LC.CLAUSES.TEXT)<1,CC> = CC:"-":TRIM(TT)
                NEXT CC
            END ELSE
                ETEXT = 'Term.Does.Not.Match.Type'
            END
        END
*######################################################################
        LOCATE '' IN DE.TERM<1,1> SETTING POS3 THEN
            DD.CODE = R.SCB.LC.DEFAULT<SCB.LCV.DOCUMENT.CODE,POS3>
            DD1.CODE = R.NEW(TF.LC.DOCUMENT.CODE)
            LOCATE DD.CODE IN DD1.CODE<1,1> SETTING POS4 THEN
*Line [ 119 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                CH.NO = DCOUNT(R.NEW(TF.LC.DOCUMENT.TXT)<1,POS4>,@SM)
                FOR TT = 1 TO CH.NO
                    IF INDEX(R.NEW(TF.LC.DOCUMENT.TXT)<1,POS4,TT>,'xxxxxxx',1) AND R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ORIGIN.OF.GOODS> THEN
                        CH.TXT = ''
                        CH.TXT = CHANGE(R.NEW(TF.LC.DOCUMENT.TXT)<1,POS4,TT>,"xxxxxxx",R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ORIGIN.OF.GOODS>)
                        R.NEW(TF.LC.DOCUMENT.TXT)<1,POS4,TT> = CH.TXT
                    END
                NEXT TT
            END
        END
*######################################################################
    END
    RETURN
END
