* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>2756</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.MARGIN.TYPE
*A Routine To CALCULATE THE MARGIN.AMOUNT ACCORDING TO THE MARGIN.PER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN

        IF COMI THEN
            PERC='';DEP.PERC='';SPEC.PERC='' ;LGTYPE=''; MYKIND=''

            IF COMI # R.NEW(LD.AMOUNT) THEN
                R.NEW(LD.CHRG.AMOUNT)=''
            END
            CUSTM = R.NEW(LD.CUSTOMER.ID)
            LG.AMT = "";RATE = "" ; DIF.AMT = "" ; DIF.RATE  = ""
            MARKET = '1'
            CURR = R.NEW(LD.CURRENCY)
            IF CURR NE LCCY  THEN
                FOR.LG.AMT = R.NEW(LD.AMOUNT)
**                CALL MIDDLE.RATE.CONV.CHECK(FOR.LG.AMT,CURR,RATE,MARKET,LG.AMT,DIF.AMT,DIF.RATE)
                LG.AMT=R.NEW(LD.AMOUNT)
            END ELSE
                LG.AMT = R.NEW(LD.AMOUNT)
            END

            IF PGM.VERSION EQ ',SCB.LG.ADVINCROPER' THEN
                LG.AMT=R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>
            END

            MARG.CODE = FIELD(COMI,'-',1)
*****************************
            IF MARG.CODE EQ 'C' THEN
                IF PGM.VERSION=',SCB.LG.ADV.OPER' THEN
***  ETEXT='No Margin Rate Is Allowed LG Will Be Fully Covered';CALL STORE.END.ERROR
***   R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
***  R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                END
            END
*****************************
            IF MARG.CODE EQ 'N' THEN
                LG.FULL.PERC=100
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>= LG.FULL.PERC
            END ELSE

                T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
                KEY.ID = MARG.CODE:'-':CUSTM:'...'
                T.SEL = "SELECT F.SCB.LG.CUS WITH @ID LIKE ": KEY.ID
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
**SELECTED = '1'

                IF NOT(SELECTED) THEN
***************************
***    IF MARG.CODE = 'D' THEN
**       TEXT = 'No Deposit Found LG Will Be Fully Covered';CALL REM
***        R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
***        R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
***    END
***************************
                    IF MARG.CODE = 'S' THEN
                        TEXT='No Special Rate Found LG Will Be Fully Covered';CALL REM
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                    END
                    IF MARG.CODE = 'C' THEN
                        TEXT='No Margin Rate Found LG Will Be Fully Covered';CALL REM
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                    END
                END ELSE
************************
                    CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,KEY.LIST<SELECTED>,LIMREF)
                    LIMPROD = FIELD(LIMREF,'.',2)
                    CALL DBR ('LIMIT':@FM:LI.AVAIL.AMT,LIMREF,ONLIMIT)
*************************

                    FN.LGCUS ="F.SCB.LG.CUS"
                    F.LGCUS = ""

                    CALL OPF(FN.LGCUS,F.LGCUS)
                    CALL F.READ(FN.LGCUS,KEY.LIST<SELECTED>,R.LGCUS,F.LGCUS,'')


                    LGTYPE = R.LGCUS<SCB.LGCS.LG.TYPE>
                    IF ETEXT THEN ETEXT =  ''
                    IF LGTYPE THEN

                        MYKIND =R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND>
                        TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>

                        LOCATE TYPE IN LGTYPE<1,1> SETTING LLL ELSE LLL = 0
                        IF LLL > 0 THEN
                            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.KIND,KEY.LIST<SELECTED>,LGKIND)
                            IF LGKIND THEN
                                LOCATE MYKIND IN LGKIND<1,LLL,1> SETTING PPP  ELSE PPP = 0
                                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,KEY.LIST<SELECTED>,PERC)
                                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.SPEC.PERC,KEY.LIST<SELECTED>,SPEC.PERC)
                                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.DEP.PERC,KEY.LIST<SELECTED>,DEP.PERC)

                                IF PPP > 0 THEN
*******************************************
                                    IF  ONLIMIT > 0 THEN
                                        LGPERC =PERC<1,LLL,PPP>/100
                                        IF MARG.CODE EQ 'C' THEN LGPERC =PERC<1,LLL,PPP>/100
                                        IF MARG.CODE EQ 'D' THEN LGPERC=DEP.PERC<1,LLL,PPP>/100
                                        IF MARG.CODE EQ 'S' THEN LGPERC =SPEC.PERC<1,LLL,PPP>/100
*******************************************
                                        IF LG.AMT LE ONLIMIT THEN
                                            R.NEW(LD.LIMIT.REFERENCE)=TRIM(LIMPROD,"0","L"):".01"
                                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT*LGPERC
                                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/LG.AMT)*100
                                        END ELSE
*R.NEW(LD.LIMIT.REFERENCE)=TRIM(LIMPROD,"0","L"):".01"
*WW =LG.AMT-ONLIMIT
*ZZ = LGPERC * ONLIMIT
*R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = WW+ZZ
*R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/LG.AMT)*100
                                            R.NEW(LD.LIMIT.REFERENCE)=''
                                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>  = LG.AMT
                                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                            TEXT='Limit.Exceeded.Available.Amount';CALL REM

                                        END
                                    END  ELSE
                                        R.NEW(LD.LIMIT.REFERENCE)=''
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>  = LG.AMT
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                        TEXT='Limit.Exceeded.Available.Amount';CALL REM

                                    END

                                END  ELSE
                                    IF MARG.CODE = 'D' THEN
                                        TEXT = 'No Deposit Found LG KIND Will Be Fully Covered';CALL REM
                                        R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                                        R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100

                                    END
                                    IF MARG.CODE = 'S' THEN
                                        TEXT='No Special Rate Found ,LG KIND  Will Be Fully Covered';CALL REM
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                    END

                                    IF MARG.CODE = 'C' THEN
                                        TEXT='No Margin Rate Found LG KIND Will Be Fully Covered';CALL REM
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                    END

                                END
                            END
                        END  ELSE
                            IF MARG.CODE = 'D' THEN
                                TEXT = 'No Deposit Found LG TYPE Will Be Fully Covered';CALL REM
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100

                            END
                            IF MARG.CODE = 'S' THEN
                                TEXT='No Special Rate Found ,LG TYPE  Will Be Fully Covered';CALL REM
                                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                            END

                            IF MARG.CODE = 'C' THEN
                                TEXT='No Margin Rate Found LG TYPE Will Be Fully Covered';CALL REM
                                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = LG.AMT
                                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                            END

                        END

                    END
                END
                CALL REBUILD.SCREEN
            END
        END
        RETURN
    END
