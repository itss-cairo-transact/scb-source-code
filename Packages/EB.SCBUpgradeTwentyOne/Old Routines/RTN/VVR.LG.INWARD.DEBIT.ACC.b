* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
**********ABEER*************************
*-----------------------------------------------------------------------------
* <Rating>139</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.INWARD.DEBIT.ACC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    IF COMI THEN

        MYID=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.CURRENCY,MYID,LGCURR)
        LOCATE COMI IN LGCURR<1,1> SETTING LLL  ELSE ETEXT='Currency.Not.Found' ; RETURN
        R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = ''
        T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": R.NEW(LD.CUSTOMER.ID) :" AND CURRENCY EQ ": COMI :" AND ( CATEGORY EQ '2000' )"
*WAS CATEGORY 2000
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

        IF NOT(SELECTED) THEN
           ** ETEXT='There.Is.No.Account' ;RETURN
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = KEY.LIST<1>
            R.NEW(LD.INT.LIQ.ACCT)=KEY.LIST<1>
        END
        CALL REBUILD.SCREEN
*********************************************************
        IF MESSAGE NE 'VAL' THEN
*Line [ 53 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>,@SM)
*Line [ 55 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE =DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>,@SM)
            R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>=''
            FOR I = 2 TO COUNT.AMT
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I> THEN
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
                END
            NEXT I
            R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>='5'
            CALL REBUILD.SCREEN
        END
********************************************************
    END
    RETURN
END
