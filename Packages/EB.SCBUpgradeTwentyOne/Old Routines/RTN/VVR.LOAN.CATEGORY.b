* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
** ----- 03.12.2002 Pawel TEMENOS EE -----
*-----------------------------------------------------------------------------
* <Rating>577</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LOAN.CATEGORY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.PARMS

*    DEFFUN GET.LD.CATEGORY( START.DATE, END.DATE, CURRENCY, LD.TYPE, LD.POSITION)
    DEFFUN GET.LD.CATEGORY( )
    IF V$FUNCTION = 'I' AND MESSAGE # 'VAL' AND COMI THEN
        IF COMI AND COMI NE R.NEW(LD.FIN.MAT.DATE) THEN ETEXT='Not.Allowed.To.Change.Maturity.Date'
        IF R.NEW( LD.VALUE.DATE) AND R.NEW( LD.CURRENCY) THEN

            POSITION = R.NEW( LD.LOCAL.REF)< 1, LDLR.PERSONCORP>
            IF POSITION THEN

                CATEGORY = GET.LD.CATEGORY( R.NEW( LD.VALUE.DATE), COMI, R.NEW( LD.CURRENCY), 'LOAN', POSITION)
*TEXT = "CATEGORY":CATEGORY;CALL REM
                IF CATEGORY THEN R.NEW( LD.CATEGORY) = CATEGORY ; CALL REBUILD.SCREEN
                ELSE ETEXT = 'NO APPROPRIATE CATEGORY'

            END ELSE ETEXT = 'POSITION NEEDED'

        END ELSE ETEXT = 'VALUE DATE AND CURRENCY NEEDED'
*---------------  MOHAMED MAHMOUD ------------------
*GENERATE.DDACCT:
*---------------
*******************UPDATED BY RIHAM R15*****************************
        CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,R.NEW(LD.CUSTOMER.ID),COMP.BOOK)
        CUS.BR = COMP.BOOK[8,2]
        AC.OFICER = TRIM(CUS.BR, "0" , "L")


        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRN.NAME)


*************************************************************


*        CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,R.NEW(LD.CUSTOMER.ID),BRN)
*        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRN,BRN.NAME)
      *  R.NEW(LD.LOCAL.REF)<1,LDLR.CUSTOMER.BRANCH> = BRN.NAME
*TEXT=BRN:BRN;CALL REM
        BRN = FMT( BRN, 'R%2')
*TEXT=BRN:'BRN2';CALL REM
        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(LD.CURRENCY),ID.CURR)
        CALL DBR('SCB.LOANS.PARMS':@FM:SCB.LO.PAR.CATEGORY,CATEGORY,MYCATEGORY)
        ID.CAT = MYCATEGORY

        ID.CAT = CATEGORY - 20000
        CUST = FMT( R.NEW(LD.CUSTOMER.ID), 'R%8')

        R.NEW(LD.DRAWDOWN.ACCOUNT)= CUST:ID.CURR:ID.CAT:'01'
        CALL REBUILD.SCREEN


        DDACCT = CUST:ID.CURR:ID.CAT:'01'

        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,DDACCT,TITEL)
        IF NOT (ETEXT) THEN
            T.ENRI<12>    = TITEL
            COMI.ENRI<12> = TITEL
        END
    END
    RETURN
END
