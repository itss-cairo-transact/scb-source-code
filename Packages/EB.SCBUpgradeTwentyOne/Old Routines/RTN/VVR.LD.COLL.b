* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.COLL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
**    $INCLUDE           I_F.SCB.CU.CARD
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    IF COMI # "" THEN
        R.NEW(COLL.APPLICATION.ID)= COMI
* IF R.NEW(COLL.APPLICATION.ID) # R.OLD(COLL.APPLICATION.ID) THEN
*  R.NEW(COLL.NOMINAL.VALUE) = ""
* END
        CALL VVR.COL.EXP
        CALL REBUILD.SCREEN
**************************************************************
*Line [ 43 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        COUNTS1 =DCOUNT(R.NEW(COLL.NOTES),@VM)
        MUL.NO = COUNTS1 +1
        R.NEW(COLL.NOTES)<1,MUL.NO> = COMI
        COMI = ""
        R.NEW(COLL.EXECUTION.VALUE) = ""
        CALL REBUILD.SCREEN
*        TEXT = COMI ; CALL REM
***************************************************************
    END
    CALL REBUILD.SCREEN
* ELSE
* COMI = ""
* R.NEW(COLL.APPLICATION.ID)= ""
* END

* IF R.NEW(COLL.COLLATERAL.CODE) # "101" AND R.NEW(COLL.COLLATERAL.CODE) # "103" THEN
* ETEXT = "��� ����� ��������"
*END

    RETURN
END
