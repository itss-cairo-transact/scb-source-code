* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
****************************NI7OOOOOOOOOOOOOO***************
*-----------------------------------------------------------------------------
* <Rating>250</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.DR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME


    INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)

    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)
    IF DEPT.CODE EQ 99 THEN

        IF R.NEW(DEPT.SAMP.AMT.TF2) NE '' THEN
            XX = R.NEW(DEPT.SAMP.TF2.TYPE)
            FINDSTR '1' IN XX SETTING FMS,VMS THEN
                IF COMI EQ '' THEN
                    ETEXT = "���� �� ����� �� LIMI"
                    CALL STORE.END.ERROR
                END
            END
            FINDSTR '2' IN XX SETTING FMS,VMS THEN
                IF COMI EQ '' THEN
                    ETEXT = "���� �� ����� �� LIMI"
                    CALL STORE.END.ERROR

                END
            END
            FINDSTR '3' IN XX SETTING FMS,VMS THEN
                IF COMI EQ '' THEN
                    ETEXT = "���� �� ����� �� LIMI"
                    CALL STORE.END.ERROR

                END
            END
            FINDSTR '4' IN XX SETTING FMS,VMS THEN
                IF COMI EQ '' THEN
                    ETEXT = "���� �� ����� �� LIMI"
                    CALL STORE.END.ERROR

                END
            END
            FINDSTR '5' IN XX SETTING FMS,VMS THEN
                IF COMI NE '' THEN
                    ETEXT = "��� ����� ������ LIMIT"
                    CALL STORE.END.ERROR

                END
            END

        END
    END
    RETURN
END
