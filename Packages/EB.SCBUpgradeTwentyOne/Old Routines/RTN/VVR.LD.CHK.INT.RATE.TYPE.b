* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN AHMED 08/10/2002,  -----

*TO CHECK IF INT.RATE.TYPE = 1  THEN EMPTY LD.INTEREST.KEY & LD.INTEREST.SPREAD
*ANNUITY.PAY.METHOD  SHOULD = END

    SUBROUTINE VVR.LD.CHK.INT.RATE.TYPE

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    IF COMI = '1' THEN

        R.NEW(LD.INTEREST.KEY) = ''
        R.NEW(LD.INTEREST.SPREAD) = ''
*MUST BE ANNUITY PAYMENT METHOD
        R.NEW(LD.INT.PAYMT.METHOD) = 'END'

    END

    IF COMI = '3' THEN R.NEW(LD.INTEREST.RATE) = ''

    CALL REBUILD.SCREEN

    RETURN
END
