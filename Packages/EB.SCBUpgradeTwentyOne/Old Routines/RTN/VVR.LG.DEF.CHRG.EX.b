* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.DEF.CHRG.EX

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    IF COMI THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
        STO1=POSS<1,1>
        STO2=POSS<1,2>
        STO3=POSS<1,3>
        STO4=POSS<1,4>
        IF POSS NE '' THEN
            TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4  ; CALL REM
        END
    END
    IF COMI # R.NEW(LD.CUSTOMER.ID) THEN
        ETEXT = "��� ����� ������� " ;CALL STORE.END.ERROR
    END
*********MODIFIED AT 13-6-2016 UPGRADING PURPOSE ABEER
    R.NEW(LD.CHRG.CODE) = ""
    R.NEW(LD.CHRG.CODE)<1,1> = "13"
    R.NEW(LD.CHRG.CODE)<1,2> = "14"


*Line [ 50 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I

    CALL REBUILD.SCREEN

    IF MESSAGE EQ 'VAL' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        BENF.NAM = MYLOCAL<1,LDLR.IN.FAVOR.OF>
        BENF.ADD = MYLOCAL<1,LDLR.BNF.DETAILS>

        BENF.NAM.NEW = R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.FAVOR.OF>
        BENF.ADD.NEW = R.NEW( LD.LOCAL.REF)< 1,LDLR.BNF.DETAILS>

        IF ( BENF.NAM EQ BENF.NAM.NEW ) AND ( BENF.ADD EQ BENF.ADD.NEW ) THEN
            E= 'MUST CHANGE BENF NAME OR ADDRES'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
**************
    RETURN
END
