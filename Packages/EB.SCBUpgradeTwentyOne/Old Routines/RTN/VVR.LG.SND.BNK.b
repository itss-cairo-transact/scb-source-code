* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.SND.BNK

*IF LG.KIND = "CBB" THEN FIELD SENDING-BNK IS MANDATORY

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER


    IF COMI THEN
        CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,CUS.SEC)
        IF (CUS.SEC < 3000 AND CUS.SEC < 3999) THEN ETEXT = "MUST.BE.BANK"
        IF R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> EQ "BB" OR R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> EQ "FN"  OR R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> EQ "AD" THEN
* OR R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> NE "FNC" OR R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> NE "CAD" THEN
            R.NEW( LD.LOCAL.REF)< 1,LDLR.SENDING.REF>= ''
            ETEXT = 'YOU.MUST.NOT.FILL.THIS.FIELD'

        END
    END ELSE
        IF R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> = "CBB" OR R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> = "CAD" OR R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.KIND> = "FNC" THEN
            ETEXT = 'YOU.MUST.FILL.THIS.FIELD'
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
