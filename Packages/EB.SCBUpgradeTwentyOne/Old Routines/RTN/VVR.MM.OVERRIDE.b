* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
****    RANIA     ****

*A Routine To Generate An Override Msg If the difference between VALUE.DATE & MATURITY.DATE GT one year.

SUBROUTINE VVR.MM.OVERRIDE

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

**************************************************************************************************

     D3 = R.NEW(MM.VALUE.DATE)
     D2 = R.NEW(MM.MATURITY.DATE)
     DAY = "C"
     CALL CDD("",D2,D3,DAY)
     DAY = ABS(DAY)

      IF DAY > 366 THEN

        TEXT = 'MAXIMUM.THAN.1.YEAR'
         AF= MM.MATURITY.DATE ; AV=1
         CALL STORE.OVERRIDE(R.NEW(MM.CURR.NO)+1)
      END

****************************************************************************************************

RETURN
END
