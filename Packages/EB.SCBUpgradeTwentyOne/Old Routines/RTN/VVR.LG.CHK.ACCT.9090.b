* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CHK.ACCT.9090


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    IF COMI # '' THEN
        DEBIT.ACC = COMI
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DEBIT.ACC,DR.CCY)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACC,DR.CUST)
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DEBIT.ACC,DR.CAT)

        IF MESSAGE NE 'VAL' THEN
            IF DR.CAT EQ '9090' OR DR.CAT EQ '9091' THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=''
                TEXT='��� ����� ����� ��������';CALL REM
            END
        END
        IF R.NEW(LD.CUSTOMER.ID) NE '99499900' THEN
            IF R.NEW(LD.CURRENCY) NE DR.CCY THEN
                ETEXT ='Must Be Same Currnecy';CALL STORE.END.ERROR
            END
            IF R.NEW(LD.CUSTOMER.ID) NE DR.CUST THEN
                ETEXT='Customer Account Must Be Same The LG Customer';CALL STORE.END.ERROR
            END

            IF COMI EQ R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> THEN
                ETEXT='DR AND CR ACCT SHOULD BE DIFFRENT';CALL STORE.END.ERROR
            END
        END

    END
    IF MESSAGE NE 'VAL' THEN
        R.NEW(LD.CHRG.LIQ.ACCT)=COMI
        CALL REBUILD.SCREEN
    END
****    END
    RETURN
END
