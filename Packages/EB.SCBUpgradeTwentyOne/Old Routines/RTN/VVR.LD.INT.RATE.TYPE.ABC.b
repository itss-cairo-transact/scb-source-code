* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.INT.RATE.TYPE.ABC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    IF MESSAGE NE 'VAL' THEN
            COMI = "4"
        IF  COMI = "4" THEN
            T(LD.INTEREST.RATE)<3> =''
            T(LD.INT.PAYMT.METHOD)<3> ='NOINPUT'
            T(LD.INTEREST.KEY)<3> ='NOINPUT'
           * T(LD.INTEREST.SPREAD)<3> ='NOINPUT'
        END
    END
*Line [ 37 ] Adding EB.SCBUpgradeTwentyOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeTwentyOne.VVR.LD.CUS.STAFF

    RETURN
END
