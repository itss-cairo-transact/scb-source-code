* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>93</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 28/08/2003***********

    SUBROUTINE VVR.LC.REIMBURSE.BK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG

*  BK = ",SCB.BANK"
    IF COMI THEN

        CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,SEC)
        CALL DBR('SCB.VER.IND.SEC.LEG':@FM:VISL.SECTOR,",SCB.BANK",SS)
       * TEXT = SEC ; CALL REM
       * TEXT = SS ; CALL REM
        CALL DBR('SCB.VER.IND.SEC.LEG':@FM:VISL.INDUSTRY,",SCB.BANK",INDUS)
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CNT = DCOUNT(INDUS,@VM)
       * TEXT = CNT; CALL REM
        FOR I = 1 TO CNT
            LOCATE SEC IN SS<1,I,1> SETTING POS THEN
                YYY = 'YES'
            END
        NEXT I
     IF NOT(YYY) THEN ETEXT = "CUSTOMER.MUST.BANK"
*ELSE ETEXT = "CUSTOMER.MUST.BANK"
* TEXT = "POS= ":SS<1,POS>:" ":POS  ; CALL REM
*  ETEXT = "CUSTOMER.MUST.BANK"
* END
    END
    RETURN
END
