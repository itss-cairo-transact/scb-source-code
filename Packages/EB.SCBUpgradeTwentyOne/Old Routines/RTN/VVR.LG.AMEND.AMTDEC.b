* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
******** ABEER 12/3/2003**************
* A Routine To handle Margin Amount & Percentage Changes If LG Amount is Changed

    SUBROUTINE VVR.LG.AMEND.AMTDEC

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN

            OLD.LG.AMOUNT = R.NEW(LD.AMOUNT)
            CUSTM = R.NEW(LD.CUSTOMER.ID)
            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            OLD.MAT.DATE = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
            LGPERC = MYLOCAL<1,LDLR.MARGIN.PERC>/100
            LGMARG = MYLOCAL<1,LDLR.MARGIN.AMT>
            MYTYPE = MYLOCAL<1,LDLR.PRODUCT.TYPE>
            NEW.MAT.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            AMOUNT.DEC=ABS(COMI)
            NEW.LG.AMOUNT = OLD.LG.AMOUNT - AMOUNT.DEC
            NEW.MARG = OLD.LG.AMOUNT - AMOUNT.DEC
            NEW.LG.MARG = NEW.MARG*LGPERC
****************************************************
            IF COMI THEN
                CALL EB.ROUND.AMOUNT ('EGP',NEW.LG.MARG,'',"2")
**************************************************
                AMOUNT.DEC=ABS(COMI)
                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.MARG
                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = LGPERC*100
                IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1233'
                    R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                END  ELSE
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1235'
                    R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                END
                COMI=AMOUNT.DEC*-1
            END
        END
    END
****    CALL REBUILD.SCREEN
*******************************
    RETURN
END
