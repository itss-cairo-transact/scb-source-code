* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>890</Rating>
*-----------------------------------------------------------------------------
*********** ABEER************
*A Routine To Default Beneficiary Name & Address if it is already existing Customer
    SUBROUTINE VVR.LG.IN.FAVOR.OF

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN

        IF COMI EQ '' THEN
            ETEXT='��� ����� ��� ��������'
        END ELSE
            IF NUM(COMI) THEN
                BEN.ID =COMI
                GOSUB BENF.DEFAULT
            END ELSE
                T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
                T.SEL = "SSELECT FBNK.CUSTOMER WITH MNEMONIC EQ ": COMI
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
                IF KEY.LIST THEN
                    BEN.ID=KEY.LIST
                    GOSUB BENF.DEFAULT
                END
            END
        END

    END

    RETURN
**********************************************************************
BENF.DEFAULT:
    FN.CUSTOMER = 'F.CUSTOMER';F.CUSTOMER='';R.CUSTOMER = '';E=''
    THIRD.NO = R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>
    CUST.ID = R.NEW(LD.CUSTOMER.ID)
*******************************************************************
    IF BEN.ID NE THIRD.NO AND BEN.ID NE CUST.ID THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.LANGUAGE,CUST.ID, MYLANGUAGE)

        CALL OPF(FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ(FN.CUSTOMER,BEN.ID, R.CUSTOMER, F.CUSTOMER ,E)

        MYNAME1  = R.CUSTOMER<EB.CUS.NAME.1>
        MYNAME2  = R.CUSTOMER<EB.CUS.NAME.2>
        MYSTREET = R.CUSTOMER<EB.CUS.STREET>
        MYCOUNTRY = R.CUSTOMER<EB.CUS.TOWN.COUNTRY>
        MYARNAME1= R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
        MYARNAME2= R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>
        MYARSTREET=R.CUSTOMER<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
*Line [ 77 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        COUNTST=DCOUNT(MYSTREET,@VM)
        COUNTRY=COUNTST+1
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        COUNTARST=DCOUNT(MYARSTREET,@VM)
        CALL F.RELEASE(FN.CUSTOMER,BEN.ID,F.CUSTOMER)
****************************************************************************************
        IF MYLANGUAGE = '1' THEN
            R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.FAVOR.OF>='';R.NEW( LD.LOCAL.REF)< 1,LDLR.BNF.DETAILS>=''
            COMI = MYNAME1
            IF MYNAME2 THEN R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.FAVOR.OF,2>=MYNAME2
            FOR I = 1 TO COUNTST
                IF MYSTREET THEN R.NEW( LD.LOCAL.REF)< 1,LDLR.BNF.DETAILS,I> = MYSTREET<1,I>
                IF MYCOUNTRY THEN R.NEW( LD.LOCAL.REF)< 1,LDLR.BNF.DETAILS,COUNTRY> = MYCOUNTRY
            NEXT I
        END ELSE
            IF MYLANGUAGE = '2' THEN
                R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.FAVOR.OF>='';R.NEW( LD.LOCAL.REF)< 1,LDLR.BNF.DETAILS>=''
                COMI = MYARNAME1
                IF MYARNAME2 THEN R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.FAVOR.OF,2>=MYARNAME2
                FOR I = 1 TO COUNTARST
                    IF MYARSTREET THEN R.NEW( LD.LOCAL.REF)< 1,LDLR.BNF.DETAILS,I> = MYARSTREET<1,I>
                NEXT I
            END
        END
        CALL REBUILD.SCREEN
    END ELSE ETEXT="Beneficiary.Should.Differ.From.CustomerID.And.LGCustomer"
************************
END
