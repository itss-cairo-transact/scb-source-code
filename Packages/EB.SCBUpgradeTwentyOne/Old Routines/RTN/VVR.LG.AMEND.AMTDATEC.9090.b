* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>4398</Rating>
*-----------------------------------------------------------------------------
*A Routine To Change Operation Code If Maturity.Date Is Changed OR Amount Changed
    SUBROUTINE VVR.LG.AMEND.AMTDATEC.9090

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE




**********20081114***HHHHH******************
    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
    OLD.EXP.DAT = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

**    IF COMI EQ OLD.EXP.DAT THEN
**        ETEXT='It.Must.Be.CHANGE';CALL STORE.END.ERROR
**    END
    IF COMI LT OLD.EXP.DAT  THEN
        ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date';CALL STORE.END.ERROR
    END
***************HHHHHH*************************

    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
    OLD.MAT.DATE = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>

    FN.F.COM= 'F.SCB.LG.FREE.CHARGE' ; F.F.COM = ''
    CALL OPF(FN.F.COM,F.F.COM)
    CALL F.READ(FN.F.COM,R.NEW(LD.CUSTOMER.ID),R.F.COM,F.F.COM,E11)

    FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.LMM= 'FBNK.LMM.CHARGE.CONDITIONS'; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)
    FN.CHR= 'FBNK.FT.CHARGE.TYPE' ; F.CHR = ''
    CALL OPF(FN.CHR,F.CHR)
    FN.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

**    AC.ID  = R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
    W.CUR =  R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
    CATG = R.ACC<AC.CATEGORY>


    IF CATG EQ '9090' OR CATG EQ '1220' OR CATG EQ '9091' THEN
        IF E11 THEN
            R.NEW(LD.CHRG.CODE) = ''
            R.NEW(LD.CHRG.AMOUNT)  = ''
***********************
*Line [ 87 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)

            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''



            FOR I = 2 TO COUNT.CODE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
                END
            NEXT I

***********************
            BEGIN CASE
            CASE R.NEW(LD.CUSTOMER.ID) EQ '31300011'
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = 14
                CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,R.LMM,F.LMM,EE2)
                CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>

                CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)

                LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                CH.AMT = R.CHR<FT5.FLAT.AMT,J>


                IF EE3 THEN
                    CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                    LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                    CH.AMT = R.COM<FT4.FLAT.AMT,K>
                END
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = CH.AMT

            CASE OTHERWISE
                IF (R.NEW(LD.CUSTOMER.ID)[1,6] EQ '994999' OR R.NEW(LD.CUSTOMER.ID) EQ '1304955' OR R.NEW(LD.CUSTOMER.ID) EQ '40300075' OR R.NEW(LD.CUSTOMER.ID) EQ '7300259' ) THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ""
                    R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = ""
                END ELSE
                    COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
                    IF COMM EQ 'YES' THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 13
*********************************************************************
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1>,R.LMM,F.LMM,EE3)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,1>  = CH.AMT
**********************************************************************

                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,E33)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)

                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT

                    END
                    IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 13
*********************************************************************
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1>,R.LMM,F.LMM,EE3)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,1>  = CH.AMT
**********************************************************************
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14

                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE3)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)

                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT
                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 4
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = 14
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE3)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.CHR<FT5.FLAT.AMT,J>
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT


                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = '7'
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2> = '14'
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE2)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>

                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'

                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>

                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END

                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT
                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 9
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = '14'
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,2>,R.LMM,F.LMM,EE2)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        CH.AMT = R.CHR<FT5.FLAT.AMT,J>
                        IF EE3 THEN
                            CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                            LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                            CH.AMT = R.COM<FT4.FLAT.AMT,K>
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,2>  = CH.AMT

                    END
                END
            END CASE
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
        END
    END

    IF CATG NE '9090' AND CATG NE '1220' AND CATG NE '9091' THEN
***************************END********************************
        IF E11 THEN

            BEGIN CASE
            CASE R.NEW(LD.CUSTOMER.ID) EQ '31300011'
                R.NEW(LD.CHRG.CODE)<1,1> = 14
            CASE OTHERWISE
                IF (R.NEW(LD.CUSTOMER.ID)[1,6] EQ '994999' OR R.NEW(LD.CUSTOMER.ID) EQ '1304955' OR R.NEW(LD.CUSTOMER.ID) EQ '40300075' OR R.NEW(LD.CUSTOMER.ID) EQ '7300259' ) THEN
                    R.NEW(LD.CHRG.CODE)   = ""
                    R.NEW(LD.CHRG.AMOUNT) = ""
                END ELSE
                    COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
                    IF COMM EQ 'YES' THEN
                        R.NEW(LD.CHRG.AMOUNT)<1,1> =''
                        R.NEW(LD.CHRG.CODE)<1,1> = 13

                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                    END ELSE
                        IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
                            R.NEW(LD.CHRG.CODE)<1,1> = 13
                            R.NEW(LD.CHRG.CODE)<1,2> = 14
                        END ELSE
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                                R.NEW(LD.CHRG.CODE)<1,1> = 4
                                R.NEW(LD.CHRG.CODE)<1,2> = 14
                                R.NEW(LD.CHRG.CODE)<1,3> = '13'

                            END
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                                R.NEW(LD.CHRG.CODE)<1,1> = 7
                                R.NEW(LD.CHRG.CODE)<1,2> = 14
                                R.NEW(LD.CHRG.CODE)<1,3> =  '13'

                            END
                            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                                R.NEW(LD.CHRG.CODE)<1,1> = 9
                                R.NEW(LD.CHRG.CODE)<1,2> = 14
                                R.NEW(LD.CHRG.CODE)<1,3> = '13'
                            END
                        END
                    END
                END
            END CASE
        END ELSE
            R.NEW(LD.CHRG.CODE) = ''
        END
*Line [ 299 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
        FOR  I = 1 TO NO.M
            R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
        NEXT I
    END
    RETURN
END
