* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>500</Rating>
*-----------------------------------------------------------------------------


    SUBROUTINE VVR.LD.MOD.AMOUNT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION

    IF COMI THEN

        IF COMI[1,1]= '-' THEN
            NEWAMOUNT = R.NEW (LD.AMOUNT)+COMI

            PARM = '' ; E1 = '' ; NEWAMOUNT = '' ;  AMT = ''

            PARM = R.NEW (LD.CATEGORY)



            CALL DBR('LD.TXN.TYPE.CONDITION':@FM: LTTC.MIN.INIT.AMT,PARM,AMT)

            IF NOT (ETEXT) THEN
                IF NEWAMOUNT LT AMT THEN ETEXT = '��� �� ������'
            END
        END

        IF COMI[1,1]= '+' THEN
            NEWAMOUNT = R.NEW (LD.AMOUNT)+COMI

            PARM = '' ; E1 = '' ; NEWAMOUNT = ''  ; AMT = ''

            PARM = R.NEW (LD.CATEGORY)

            CALL DBR('LD.TXN.TYPE.CONDITION':@FM: LTTC.MAX.INIT.AMT,PARM,AMT)

            IF NOT (ETEXT) THEN
                IF NEWAMOUNT GT AMT THEN ETEXT = '��� �� ������'
            END
        END
        IF MESSAGE # 'VAL' THEN

ZZ = R.NEW(LD.FIN.MAT.DATE)[7,2] - 1
IF LEN(ZZ) = '1' THEN XX='0':ZZ
            IF COMI THEN R.NEW(LD.AMT.V.DATE) = R.NEW(LD.FIN.MAT.DATE)[1,6]:XX
        END
    END
    RETURN
END
