* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*A Routine To Default LG Customer
*-----------------------------------------------------------------------------
* <Rating>464</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CUSTOMER.ID

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE

***---------------------------------------
    FN.F.COM= 'F.SCB.LG.FREE.CHARGE' ; F.F.COM = ''
    CALL OPF(FN.F.COM,F.F.COM)
    CALL F.READ(FN.F.COM,COMI,R.F.COM,F.F.COM,E11)
    IF NOT(E11) THEN
        R.NEW(LD.CHRG.CODE) = ''
        CALL REBUILD.SCREEN
    END
***---------------------------------------

    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    STO5=POSS<1,5>
    STO6=POSS<1,6>
    STO7=POSS<1,7>
    STO8=POSS<1,8>
    STO9=POSS<1,9>
    STO10=POSS<1,10>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
    END

    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN
***************************
            IF COMI NE R.NEW(LD.CUSTOMER.ID) THEN

                R.NEW(LD.AMOUNT)=''
                R.NEW(LD.FIN.MAT.DATE)=''
                R.NEW(LD.LIMIT.REFERENCE)=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>=COMI
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF>=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.IN.FAVOR.OF>=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.BNF.DETAILS>=''

                CALL REBUILD.SCREEN
            END

*************************************
            T.SEL = "SSELECT F.SCB.LG.CUS WITH @ID EQ ":COMI
            KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF NOT(SELECTED) THEN

                R.NEW(LD.LIMIT.REFERENCE)= ''
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                IF  R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> EQ ''  THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>= COMI
                    CALL REBUILD.SCREEN
                END

            END ELSE
                IF  R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> EQ ''  THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>= COMI
                    CALL REBUILD.SCREEN
                END   ELSE
                    IF COMI NE  R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> THEN
                        LIM.REF= R.NEW(LD.LIMIT.REFERENCE)
                        IF  LIM.REF  EQ 17010 OR LIM.REF EQ 7010 THEN
                            ETEXT='Limit.Refernce.Cant.Accept.17010.OR.7010.'
                            R.NEW(LD.LIMIT.REFERENCE)=''
                        END
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=100
** COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
** R.NEW(LD.CHRG.AMOUNT)=''
** FOR I = 2 TO COUNT.AMT
**   IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN
**     DEL R.NEW(LD.CHRG.AMOUNT)<1,I>
**   DEL R.NEW(LD.CHRG.CODE)<1,I>
**END
**NEXT I
***********                        IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
                    END
                END
            END

        END
****
        CALL REBUILD.SCREEN
    END
    RETURN
END
