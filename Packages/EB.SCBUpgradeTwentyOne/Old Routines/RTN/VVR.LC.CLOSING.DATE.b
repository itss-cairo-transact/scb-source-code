* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

    SUBROUTINE VVR.LC.CLOSING.DATE
* A ROUTINE TO CHECK IF TF.LC.EXPIRY.DATE = TF.LC.LATEST.SHIPMENT THEN ADD TF.LC.EXPIRY.DATE & SCB.LCD.CLOSE.PERIOD
* THEN DEFAULT THE RESULT IN TF.LC.CLOSING.DATE

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS


    IF V$FUNCTION='I' THEN
*****NESSREEN AHMED 4/1/2007*****
        IF MESSAGE = '' THEN
*********************************
            IF COMI = R.NEW(TF.LC.LATEST.SHIPMENT) THEN
                DOC.ID = R.NEW(TF.LC.LC.TYPE):'.': R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC>
                CALL DBR("SCB.LC.DOCTERMS":@FM:SCB.LCD.CLOSE.PERIOD,DOC.ID,DAT)
                EXP.DAT = COMI
                IF DAT THEN
                    CALL CDT ('',EXP.DAT,DAT)
                    R.NEW(TF.LC.CLOSING.DATE) = EXP.DAT
                END

            END
        END
    END
    RETURN
END
