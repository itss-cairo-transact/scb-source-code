* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>594</Rating>
*-----------------------------------------------------------------------------
******** ABEER 12/3/2003**************
* A Routine To handle Margin Amount & Percentage Changes If LG Amount is Changed.

    SUBROUTINE VVR.LG.AMEND.AMTINC

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN

            OLD.LG.AMOUNT = R.NEW(LD.AMOUNT)
            CUSTM = R.NEW(LD.CUSTOMER.ID)
*CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            OLD.MAT.DATE = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
            LGPERC = MYLOCAL<1,LDLR.MARGIN.PERC>/100
            LGMARG=MYLOCAL<1,LDLR.MARGIN.AMT>
            THIRD.NO = MYLOCAL<1,LDLR.THIRD.NUMBER>

            MYTYPE=MYLOCAL<1,LDLR.PRODUCT.TYPE>
            MARG.TYPE=MYLOCAL<1,LDLR.MARGIN.TYPE>
            MARG.CODE = FIELD(MARG.TYPE,'-',1)
************* GET ONLINE LIMIT **************************
            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
            KEY.ID = MARG.CODE:'-':CUSTM:'...'

            T.SEL = "SELECT F.SCB.LG.CUS WITH @ID LIKE ": KEY.ID
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
**************************************************
            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,KEY.LIST<SELECTED>,LIMREF)
            LIMPROD = FIELD(LIMREF,'.',2)
            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIMREF,ONLIMIT)

            MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            NEW.MAT.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            NEW.LG.AMOUNT = OLD.LG.AMOUNT + COMI
            NEW.MARG = COMI*LGPERC
            NEW.LG.MARG = LGMARG + NEW.MARG
*******************************************************************************
            IF COMI THEN
*******************************************************************************
                IF COMI > 0 THEN
                    IF COMI < ONLIMIT THEN
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.MARG
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                    END ELSE

                        NEW.MARG = COMI*1

                        NEW.LG.MARG = LGMARG + NEW.MARG

**  WW =NEW.LG.AMOUNT-ONLIMIT
** ZZ = LGPERC * ONLIMIT
** R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = WW+ZZ
** R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.MARG
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                    END
                    IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                        IF MYTYPE EQ 'ADVANCE' THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1271'
                        END ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                    END  ELSE
                        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1234'
                        R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                    END
                END
                CALL REBUILD.SCREEN
            END
        END
*******************************
    END

    RETURN
END
