* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** ABEER 12/3/2003**********
*-----------------------------------------------------------------------------
* <Rating>585</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.CH.PERC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
    IF MESSAGE NE 'VAL' THEN
        IF COMI EQ 0 THEN R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = '0.00'
        IF COMI THEN
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
*  CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
**********************************************************
            OLD.LG.AMOUNT=R.NEW(LD.AMOUNT)
            OLD.MAT.DATE=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
            OLD.MARG.PERC=MYLOCAL<1,LDLR.MARGIN.PERC>
            OLD.MARG.AMT =MYLOCAL<1,LDLR.MARGIN.AMT>
            NEW.MARG.PERC=R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
            NEW.MARG.AMT =R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            AMT.INC.DEC  =R.NEW(LD.AMOUNT.INCREASE)
*  NEW.LG.AMOUNT=OLD.LG.AMOUNT+AMT.INC.DEC
            NEW.LG.MARGIN.AMT=(AMT.INC.DEC*COMI)/100
            NEW.MARGIN=NEW.LG.MARGIN.AMT+OLD.MARG.AMT
            NEW.MAT.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT=DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
*********************************************************
            OPER=R.NEW( LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            CALL EB.ROUND.AMOUNT ('EGP',OLD.MARG.AMT,'',"2")
            CALL EB.ROUND.AMOUNT ('EGP',NEW.MARGIN,'',"2")
            IF COMI GT 100 THEN
                ETEXT ='Must.Be.Less.Than.100'
            END  ELSE
                IF NEW.MAT.DATE GE OLD.MAT.DATE THEN
                    IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> =OLD.MARG.AMT
                        COMI =OLD.MARG.PERC
                    END ELSE
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.MARGIN
                    END
                END  ELSE
                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> =OLD.MARG.AMT

                END
* R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = (NEW.LG.AMOUNT*COMI)/100

            END

****************************************************************
**---------------HYTHAM-----------------20120305-------

*        IF MESSAGE NE 'VAL' THEN
*         COUNT.CODE= DCOUNT(R.NEW(LD.CHRG.CODE),VM)
*         COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
*         R.NEW(LD.CHRG.CODE)=''
*         R.NEW(LD.CHRG.AMOUNT)=''
*         FOR I = 2 TO COUNT.AMT
*             IF R.NEW(LD.CHRG.CODE)<1,I> THEN
*                 DEL R.NEW(LD.CHRG.CODE)<1,I>
*                 DEL R.NEW(LD.CHRG.AMOUNT)<1,I>
*              END
*         NEXT I
*         R.NEW(LD.CHRG.CODE)='4'
*         CALL REBUILD.SCREEN
*       END
**---------------HYTHAM-----------------20120305-------
************************************************************************

*           XX=(NEW.LG.AMOUNT*COMI)/100
*           YY=FIELD(XX,'.',2)
*           TEXT=YY;CALL REM
*           IF YY > 0 THEN YY = '000';TEXT = YY; CALL REM
*           IF MESSAGE NE 'VAL' THEN R.NEW(LD.CHRG.AMOUNT)=''

        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
