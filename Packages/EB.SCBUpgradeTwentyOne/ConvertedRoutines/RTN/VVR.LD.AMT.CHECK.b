* @ValidationCode : MjoxMjU2MDk3MDQ3OkNwMTI1MjoxNjQyMjg5NjY0NDI1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:34:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
** ----- WAEL  -----

SUBROUTINE VVR.LD.AMT.CHECK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.PARMS

    IF COMI THEN
        X = ''
        XX = ''
        XX = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY)
*Line [ 29 ] INITIALIZE "SCB.PAR.MIN.AMT" - ITSS - R21 Upgrade - 2021-12-23
        SCB.PAR.MIN.AMT=''
        CALL REBUILD.SCREEN

*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.PARMS':@FM:SCB.PAR.MIN.AMT,XX,X)
F.ITSS.SCB.PARMS = 'F.SCB.PARMS'
FN.F.ITSS.SCB.PARMS = ''
CALL OPF(F.ITSS.SCB.PARMS,FN.F.ITSS.SCB.PARMS)
CALL F.READ(F.ITSS.SCB.PARMS,XX,R.ITSS.SCB.PARMS,FN.F.ITSS.SCB.PARMS,ERROR.SCB.PARMS)
X=R.ITSS.SCB.PARMS<SCB.PAR.MIN.AMT>

        IF NOT(ETEXT) THEN
            I = ''

*Line [ 43 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(X,@VM)
                IF COMI < X <1,I>  THEN ETEXT ='LESS THAN THE MIN.AMOUNT ALLOWED'
            NEXT I

        END

    END

*====================================================================
RETURN
END
