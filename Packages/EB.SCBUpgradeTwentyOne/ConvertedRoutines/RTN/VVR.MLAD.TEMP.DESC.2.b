* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.MLAD.TEMP.DESC.2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM.2
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.TEMP.2

    FN.ML = 'F.SCB.MLAD.FILE.TEMP.2'; F.ML = ''
    CALL OPF(FN.ML,F.ML)

    IF V$FUNCTION EQ 'I' THEN
        LINE.NO = COMI
        MLAD.LINE.NO = R.NEW(MLPAR.MLAD.TYPE):'-':LINE.NO

*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.MLAD.FILE.TEMP.2':@FM:MLTEMP.DESCRIPTION,MLAD.LINE.NO,TEMP.DESC)
F.ITSS.SCB.MLAD.FILE.TEMP.2 = 'F.SCB.MLAD.FILE.TEMP.2'
FN.F.ITSS.SCB.MLAD.FILE.TEMP.2 = ''
CALL OPF(F.ITSS.SCB.MLAD.FILE.TEMP.2,FN.F.ITSS.SCB.MLAD.FILE.TEMP.2)
CALL F.READ(F.ITSS.SCB.MLAD.FILE.TEMP.2,MLAD.LINE.NO,R.ITSS.SCB.MLAD.FILE.TEMP.2,FN.F.ITSS.SCB.MLAD.FILE.TEMP.2,ERROR.SCB.MLAD.FILE.TEMP.2)
TEMP.DESC=R.ITSS.SCB.MLAD.FILE.TEMP.2<MLTEMP.DESCRIPTION>
        R.NEW(MLPAR.TEMP.DESC)    = TEMP.DESC
    END

    CALL REBUILD.SCREEN
    RETURN
END
