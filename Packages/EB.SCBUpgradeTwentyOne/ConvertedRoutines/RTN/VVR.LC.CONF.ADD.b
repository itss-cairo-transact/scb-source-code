* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 14/07/2003***********

SUBROUTINE VVR.LC.CONF.ADD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
 

IF COMI = 'A' THEN
COMI = 'ADDING OUR CONFIRMATION'
END ELSE
 IF COMI = 'W' THEN
 COMI = 'WITHOUT OUR CONFIRMATION'
 END 
END
IF MESSAGE = 'VAL' THEN
 IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CONF.ADD.IN> NE 'ADDING OUR CONFIRMATION' AND  R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CONF.ADD.IN> NE 'WITHOUT OUR CONFIRMATION' THEN
  ETEXT = 'THIS.VALUE.IS NOT.ALLOWED'
 END
END
     

RETURN
END
