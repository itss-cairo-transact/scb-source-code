* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-25</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.CUS.STAFF.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*    $INCLUDE           I_F.SCB.PARMS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

    IF MESSAGE # 'VAL' THEN
        CUST = R.NEW(LD.CUSTOMER.ID)
        IF LEN(CUST) = 8 THEN
            CUS.CLAS = CUST[3,1]
        END ELSE
            CUS.CLAS = CUST[2,1]
        END
        IF CUS.CLAS = 1 THEN
*            ID.PARMS = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY)
            ID.PARMS = R.NEW(LD.CATEGORY):R.NEW(LD.VALUE.DATE)
            T.SEL = "SELECT F.SCB.LD.TYPE.LEVEL WITH @ID LIKE ":ID.PARMS:"..."
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF KEY.LIST THEN
************* REMOVE * WHEN YOU CAHNGE THE @ID IN TABLE SCB.LD.TYPE.LEVEL TO CATEG.DATE
*                DIM AA(SELECTED)
*               FOR I = 1 TO SELECTED
*                  D1 = RIGHT(KEY.LIST<I>,8)
*                  AA(I) = D1
*               NEXT I
*               TEMP = AA(1)
*               SORT1 = 1
*               FOR I = 2 TO SELECTED
*                   IF TEMP < AA(I)  THEN TEMP = AA(I); SORT1 =I
*               NEXT I
*******************************************************
*                ID1 = KEY.LIST<SORT1>
                ID1 = KEY.LIST<1>

                F.TYPE.LEVEL = '' ; FN.TYPE.LEVEL = 'F.SCB.LD.TYPE.LEVEL' ; R.TYPE.LEVEL = ''
                CALL OPF(FN.TYPE.LEVEL,F.TYPE.LEVEL)
                CALL F.READ( FN.TYPE.LEVEL, ID1, R.TYPE.LEVEL, F.TYPE.LEVEL, ETEXT)
                TEMP = R.TYPE.LEVEL<LDTL.STAFF.SPREDD>
*                R.NEW(LD.INTEREST.SPREAD) = R.NEW(LD.INTEREST.SPREAD)+ TEMP
                R.NEW(LD.INTEREST.SPREAD) = TEMP
                CALL REBUILD.SCREEN
            END

        END
    END


    RETURN
END
