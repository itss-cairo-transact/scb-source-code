* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>28</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 21/05/2003***********

    SUBROUTINE VVR.LC.CU.EXP.DATE
*A ROUTINE TO ADD TF.LC.ADVICE.EXPIRY.DATE & SCB.LCD.EXP.PERIOD & DEFAULT THE RESULT IN TF.LC.EXPIRY.DATE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*   $INCLUDE GLOBUS.BP I_F.FT.COMMISSION.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS


*   ISS.DATE = R.NEW(TF.LC.ISSUE.DATE)
*   AMT = R.NEW(TF.LC.LC.AMOUNT)


    IF V$FUNCTION = 'I' THEN
***NESSREEN AHMED 4/1/2007******
        IF MESSAGE = '' THEN
***********************************
            IF COMI THEN
****************************************************************
*  IF MESSAGE = 'VAL' THEN
* IF R.NEW(TF.LC.WAIVE.CHARGES) = 'NO' THEN
*      R.NEW(TF.LC.CHARGE.ACCT) = R.NEW(TF.LC.APPLICANT.ACC)
*  IF R.NEW(TF.LC.CHARGE.CODE) THEN
*   CALL DBR("FT.COMMISSION.TYPE":@FM:FT4.PERCENTAGE,R.NEW(TF.LC.CHARGE.CODE),PERC)
*    REMN = 0
* CALL CDD("C",ISS.DATE,COMI,PER)
*      *   TEXT= PER ; CALL REM
* NO.PER = (PER / 90)
*INTG = INT(NO.PER)
* REMN = NO.PER - INTG
*        TEXT = INTG ; CALL REM
*        TEXT = REMN ; CALL REM
* IF REMN # '0' THEN NO.PER = INTG + 1
*    TEXT = NO.PER ; CALL REM
*  IF PERC THEN
*   COM.AMT = (PERC * NO.PER * AMT)/100
* TEXT = COM.AMT ; CALL REM
* END
* END
* END
*END
*****************************************************************
                DOC.ID = R.NEW(TF.LC.LC.TYPE):'.': R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC>
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR("SCB.LC.DOCTERMS":@FM:SCB.LCD.EXP.PERIOD,DOC.ID,DAT)
F.ITSS.SCB.LC.DOCTERMS = 'F.SCB.LC.DOCTERMS'
FN.F.ITSS.SCB.LC.DOCTERMS = ''
CALL OPF(F.ITSS.SCB.LC.DOCTERMS,FN.F.ITSS.SCB.LC.DOCTERMS)
CALL F.READ(F.ITSS.SCB.LC.DOCTERMS,DOC.ID,R.ITSS.SCB.LC.DOCTERMS,FN.F.ITSS.SCB.LC.DOCTERMS,ERROR.SCB.LC.DOCTERMS)
DAT=R.ITSS.SCB.LC.DOCTERMS<SCB.LCD.EXP.PERIOD>
                EXP.DAT = COMI
                IF DAT THEN
**         CALL CDT ('',EXP.DAT,DAT)
**         R.NEW(TF.LC.EXPIRY.DATE) = EXP.DAT
                END

            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
