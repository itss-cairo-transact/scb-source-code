* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
********* RANIA 11/03/03 **************

*A Routine To Recalculate Commissions if Margin Amt is changed

    SUBROUTINE VVR.LG.ADV.CHANGE.AMT

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS

    IF MESSAGE NE 'VAL' THEN

        IF COMI # R.OLD( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT > THEN
            IF COMI > R.NEW(LD.AMOUNT) THEN
                ETEXT = 'MUST.BE.LESS.THAN.LG.AMOUNT'
            END ELSE
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>= (COMI/R.NEW( LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>)*100
                IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>= 100 THEN
                    R.NEW(LD.LIMIT.REFERENCE) = ''

                END ELSE
                    T(LD.LIMIT.REFERENCE)<3>=' '
                    MYCUS=R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,MYCUS,LIMREF)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,MYCUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LIMREF=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
                    LIMPROD = FIELD(LIMREF,'.',2)
                    R.NEW(LD.LIMIT.REFERENCE)= TRIM(LIMPROD,"0","L")
                END

*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                COUNT.CODE=DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
                R.NEW(LD.CHRG.AMOUNT)=''

                FOR I = 2 TO COUNT.AMT
                    IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
                NEXT I
            END
        END

        CALL REBUILD.SCREEN
    END
    RETURN
END
