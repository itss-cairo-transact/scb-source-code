* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.MLAD.MAT.DATE.2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM.2

    IF V$FUNCTION EQ 'I' THEN
        IF COMI EQ 'Y' THEN
            R.NEW(MLPAR.NEXT.DAY)              = 0
            R.NEW(MLPAR.LESS.MONTH)            = 0
            R.NEW(MLPAR.MORE.MONTH.TO.3MONTH)  = 0
            R.NEW(MLPAR.MORE.3MONTH.TO.6MONTH) = 0
            R.NEW(MLPAR.MORE.6MONTH.TO.YEAR)   = 0
            R.NEW(MLPAR.MORE.YEAR.TO.2YEAR)    = 0
            R.NEW(MLPAR.MORE.2YEAR.TO.3YEAR)   = 0
            R.NEW(MLPAR.MORE.3YEAR.TO.4YEAR)   = 0
            R.NEW(MLPAR.MORE.4YEAR.TO.5YEAR)   = 0
            R.NEW(MLPAR.MORE.5YEAR.TO.7YEAR)   = 0
            R.NEW(MLPAR.MORE.7YEAR.TO.10YEAR)  = 0
            R.NEW(MLPAR.MORE.10YEAR.TO.15YEAR) = 0
            R.NEW(MLPAR.MORE.15YEAR.TO.20YEAR) = 0
            R.NEW(MLPAR.MORE.20YEAR)           = 0
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
