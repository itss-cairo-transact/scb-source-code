* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.DEFINE.EB

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE



***************UP NI7OOO***************
    FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LMM= 'FBNK.LMM.CHARGE.CONDITIONS'; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)
    FN.CHR= 'FBNK.FT.CHARGE.TYPE' ; F.CHR = ''
    CALL OPF(FN.CHR,F.CHR)
    FN.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    AC.ID.DR  = R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    CALL F.READ(FN.ACC,AC.ID.DR,R.ACC.DR,F.ACC.DR,E.AC)
    CATG.DR = R.ACC.DR<AC.CATEGORY>

    AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
    CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
    CATG = R.ACC<AC.CATEGORY>

    IF CATG EQ '9090' OR CATG.DR EQ '9090' OR CATG EQ '1220' OR CATG.DR EQ '1220' OR CATG EQ '9091' OR CATG.DR EQ '9091' THEN
        ETEXT='CAT NOT ALLOWED';CALL STORE.END.ERROR
        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ""
        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = ""
    END
***********************************************
    SHIFT ='' ; ROUND =''
    DEFFUN SHIFT.DATE(E.COM.DATE, SHIFT, ROUND)
    IF MESSAGE NE 'VAL' THEN
        COMM=FIELD(COMI,'-',1)

        IF COMM EQ 'YES' THEN
            E.COM.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            E.DATE     = SHIFT.DATE( E.COM.DATE , '3M', 'UP')
            EXP.DATE   = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            IF E.DATE GT EXP.DATE  THEN
***      ETEXT='Commission.Schedule.Yes.Not.Allowed'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = '100' THEN
                ETEXT='Commission.Schedule.Yes.Not.Allowed'
                BEGIN.DATE = R.NEW(LD.VALUE.DATE)
                END.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
                DIFF = "C"
                CALL CDD("",BEGIN.DATE,END.DATE,DIFF)
                SAM1 = DIFF / 90
                SAM2 = SAM1 + 0.9
                PERIOD.NO = FIELD(SAM2,".",1)
                R.NEW(LD.LOCAL.REF)<1,94> = PERIOD.NO
            END ELSE
                R.NEW(LD.DEFINE.SCHEDS) = 'YES'
                R.NEW(LD.AUTO.SCHEDS) = 'NO'
                R.NEW(LD.LOCAL.REF)<1,94> = '1'
            END
        END  ELSE
            IF COMM EQ 'NO' THEN
                R.NEW(LD.DEFINE.SCHEDS) = 'NO'
                R.NEW(LD.AUTO.SCHEDS) = 'YES'
************************
                BEGIN.DATE = R.NEW(LD.VALUE.DATE)
                END.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
                DIFF = "C"
                CALL CDD("",BEGIN.DATE,END.DATE,DIFF)
                SAM1 = DIFF / 90
                SAM2 = SAM1 + 0.9
                PERIOD.NO = FIELD(SAM2,".",1)
                R.NEW(LD.LOCAL.REF)<1,94> = PERIOD.NO
************************
            END
        END
    END
    IF R.NEW(LD.CATEGORY) EQ 21096 THEN
*Line [ 117 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    END ELSE
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT((R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>),@VM)
    END
*   FOR  I = 1 TO NO.M
*        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
*   NEXT I
    CALL REBUILD.SCREEN
    RETURN
END
