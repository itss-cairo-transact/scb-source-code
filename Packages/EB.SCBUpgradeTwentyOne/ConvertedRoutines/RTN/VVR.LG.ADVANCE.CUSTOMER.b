* @ValidationCode : MjozOTg4MzQyODY6Q3AxMjUyOjE2NDIyOTAzMjA3MTE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:45:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>43</Rating>
*-----------------------------------------------------------------------------
**** RANIA 23/02/2003 ****

*A Routine To Default Customer Limit Reference AND LG Customer .

SUBROUTINE VVR.LG.ADVANCE.CUSTOMER

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CUS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.EXP.MIN
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POSS=R.ITSS.CUSTOMER<EB.CUS.TEXT>
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    STO5=POSS<1,5>
    STO6=POSS<1,6>
    STO7=POSS<1,7>
    STO8=POSS<1,8>
    STO9=POSS<1,9>
    STO10=POSS<1,10>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
    END

    R.NEW(LD.CHRG.CODE)<1,1> = 11
    R.NEW(LD.CHRG.CODE)<1,2> = 14
    R.NEW(LD.CHRG.CODE)<1,3> = 12
    CALL REBUILD.SCREEN

***-----------------------------
    CUSS = COMI
    FN.CUS.EXP.MIN= 'F.SCB.CUS.EXP.MIN' ; F.CUS.EXP.MIN = ''
    CALL OPF(FN.CUS.EXP.MIN,F.CUS.EXP.MIN)
    CALL F.READ(FN.CUS.EXP.MIN,CUSS,R.CUS.EXP.MIN,F.CUS.EXP.MIN,EE)

    LG.TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND>

    TYPE.EXP.MIN = R.CUS.EXP.MIN<EXP.MIN.TYPE>
    LOCATE LG.TYPE IN TYPE.EXP.MIN<1,1> SETTING POS1 THEN
        MINN = R.CUS.EXP.MIN<EXP.MIN.MIN.AMT,POS1>
    END

    IF MINN NE ''  THEN
        R.NEW(LD.CHRG.AMOUNT)<1,1> = MINN
    END
***-----------------------------
*Line [ 75 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I

    CALL REBUILD.SCREEN


    IF COMI THEN
        IF  R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> EQ ''  THEN
            R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>= COMI
            CALL REBUILD.SCREEN
        END   ELSE
            IF COMI NE  R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> THEN
                LIM.REF= R.NEW(LD.LIMIT.REFERENCE)
                IF  LIM.REF  EQ 17010 OR LIM.REF EQ 7010 THEN
                    ETEXT='Limit.Refernce.Cant.Accept.17010.OR.7010.'
                    R.NEW(LD.LIMIT.REFERENCE)=''
                END
***************************HYTHAM*******20090204
*R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=100
***     COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
***     R.NEW(LD.CHRG.AMOUNT)=''
***     FOR I = 2 TO COUNT.AMT
***        IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN
***             DEL R.NEW(LD.CHRG.AMOUNT)<1,I>
***             DEL R.NEW(LD.CHRG.CODE)<1,I>
***         END
***    NEXT I
**         ****       IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
            END
*************************************20090204************************
        END
****
        IF COMI NE R.NEW(LD.CUSTOMER.ID) THEN
* R.NEW(LD.CURRENCY)=''
            R.NEW(LD.AMOUNT)=''
            R.NEW(LD.FIN.MAT.DATE)=''
            R.NEW(LD.LIMIT.REFERENCE)=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>=COMI
            R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.IN.FAVOR.OF>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.BNF.DETAILS>=''

            CALL REBUILD.SCREEN
        END
****
    END

RETURN
END
