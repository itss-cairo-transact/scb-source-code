* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMT.ADV.MOD

*A Routine To CALCULATE THE MARGIN.AMOUNT ACCORDING TO THE MARGIN.PER

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN

        IF COMI THEN

            IF COMI # R.NEW(LD.AMOUNT) THEN R.NEW(LD.CHRG.AMOUNT)=''

            CUSTM = R.NEW(LD.CUSTOMER.ID)
            IF CUSTM # R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER > THEN R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100

************************** Check The Amount With LG Amount ***************
            LGAMT=R.NEW(LD.AMOUNT)
           ***HASHED ON 22/2/2012 AS REQUEST FROM B10(21/2/2012) LGAMT =FIELD(R.NEW(LD.AMOUNT),'.',1)

            IF COMI GT LGAMT THEN
                ETEXT = "Amount.GT.LG.Amount"; CALL STORE.END.ERROR
            END
            IF COMI GT R.NEW( LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR> THEN  ETEXT="Amount.GT.Increase.Amt"; CALL STORE.END.ERROR
*========================================================
            DIFF=R.NEW( LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>-COMI
            IF COMI LT LGAMT THEN
            *****aDDED ON 22/2/2012
                TEXT='CHQ AMOUNT IS LT LG AMOUNT';CALL REM
            ******
                R.NEW(LD.AMOUNT.INCREASE)= DIFF*-1
                R.NEW(LD.AMT.V.DATE)= TODAY
            END

**********************************************
            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = COMI
            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
            R.NEW(LD.LIMIT.REFERENCE)=''

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
