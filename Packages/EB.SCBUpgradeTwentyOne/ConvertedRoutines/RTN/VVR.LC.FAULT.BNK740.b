* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
**** NESSREEN-SCB 24/7/2006****
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.FAULT.BNK740

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT


    IF COMI THEN
*VER = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.VERSION.NAME>
        CALL OPF('F.SCB.LC.DEFAULT',F.SCB.LC.DEFAULT)
*******************************UPDATED BY RIHAM R15**********************
*        READ R.SCB.LC.DEFAULT FROM F.SCB.LC.DEFAULT,"DEFAULT" THEN

CALL F.READ ('F.SCB.LC.DEFAULT',"DEFAULT",R.SCB.LC.DEFAULT,F.SCB.LC.DEFAULT,ERR)
******************************************************************************
            NN = R.SCB.LC.DEFAULT<SCB.LCV.BNK.T.BNK740>
            R.NEW(TF.LC.BANK.TO.BK740) = NN
      *  END
        CALL REBUILD.SCREEN
    END
    RETURN
END
