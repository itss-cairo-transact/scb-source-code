* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>148</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 14/05/2003***********

    SUBROUTINE VVR.LC.CHARGES

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT

    IF MESSAGE = '' THEN
        TEXT = 'HI' ; CALL REM
** 2/1/2007 IF V$FUNCTION='I' THEN
        IF NUM(COMI) THEN
            R.SCB.LC.DEFAULT = ''
            CALL OPF('F.SCB.LC.DEFAULT',F.SCB.LC.DEFAULT)
*READ R.SCB.LC.DEFAULT FROM F.SCB.LC.DEFAULT,"DEFAULT" THEN
            CALL F.READ ('F.SCB.LC.DEFAULT', "DEFAULT", R.SCB.LC.DEFAULT, F.SCB.LC.DEFAULT, FILE.ERR)
            DE.CHARGE = R.SCB.LC.DEFAULT<SCB.LCV.CHARGE.COD>
            IF COMI THEN
                LOCATE COMI IN DE.CHARGE<1,1> SETTING POS1 THEN
                    DE.TEXT = R.SCB.LC.DEFAULT<SCB.LCV.CHARGE.TEXT,POS1>
                    TN.TXT = ''
*Line [ 45 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    TT = DCOUNT(DE.TEXT, @SM)
*  TEXT = 'TT=':TT ; CALL REM
                    FOR I = 1 TO TT
                        TN.TXT<1,I> = R.SCB.LC.DEFAULT<SCB.LCV.CHARGE.TEXT,POS1,I>
                    NEXT I
                    R.NEW(TF.LC.NARRATIVE.CHRGS) = TN.TXT
                    COMI<1,1> = TN.TXT<1,1>
                END
*END
            END
        END
    END
    CALL REBUILD.SCREEN

    RETURN
END
