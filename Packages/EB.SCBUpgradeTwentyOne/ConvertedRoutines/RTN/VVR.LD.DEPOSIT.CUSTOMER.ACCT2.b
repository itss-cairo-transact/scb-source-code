* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.DEPOSIT.CUSTOMER.ACCT2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY

    IF MESSAGE EQ 'VAL' THEN

*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,COMI,Y.AC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
Y.AC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
        TEXT = "1==  " : COMP ; CALL REM
        TEXT = "2==  " : Y.AC.COM ; CALL REM

**     IF Y.AC.COM NE COMP THEN
**       ETEXT = "INVALID COMPANY FOR ACCOUNT "
**     CALL STORE.END.ERROR
**   CALL REBUILD.SCREEN
**  END
    END

    IF LEN(COMI) = '16' THEN
***   IF (COMI[11,4] NE '1001') AND (COMI[11,4] NE '6501') THEN
***     ETEXT = '�� ���� ���� ����� �� ������ ';CALL STORE.END.ERROR
***   END
    END
*    IF LEN(COMI) NE '16' THEN
*       ETEXT = '�� ���� ���� ����� �� ������';CALL STORE.END.ERROR
*  END
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.POSTING.RESTRICT,COMI,POST.REST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
POST.REST=R.ITSS.ACCOUNT<AC.POSTING.RESTRICT>
    IF POST.REST THEN
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST.REST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST.REST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
        ETEXT = '���� ������ ':DESC ;CALL STORE.END.ERROR
    END
    CALL REBUILD.SCREEN

**----------------
*********************************************************
    RETURN
END
