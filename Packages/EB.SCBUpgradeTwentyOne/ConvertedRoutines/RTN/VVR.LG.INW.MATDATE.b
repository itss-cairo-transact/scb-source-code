* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LG.INW.MATDATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI # R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> THEN
        R.NEW(LD.CHRG.AMOUNT) = ''
        R.NEW(LD.CHRG.CODE) = ''
    END
    IF COMI LE R.NEW(LD.VALUE.DATE) THEN
        ETEXT = 'MUST BE GT VALUE DATE '   ; CALL  STORE.END.ERROR
    END
    IF COMI LE TODAY THEN
        ETEXT = 'MUST BE GT TODAY'  ; CALL  STORE.END.ERROR
    END

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'BIDBOND' THEN
        R.NEW(LD.CHRG.CODE)<1,1> = '4'
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'FINAL' THEN
        TEXT='FINAL';CALL REM
        R.NEW(LD.CHRG.CODE)<1,1> = '7'
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'ADVANCE' THEN
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> EQ '1111' THEN
            IF PGM.VERSION EQ ',SCB.LG.COUNTER.ADV.NON' THEN
                R.NEW(LD.CHRG.CODE)<1,1> = '11'
            END ELSE
                IF PGM.VERSION EQ ',SCB.LG.COUNTER.ADVANCE.1' THEN
                    R.NEW(LD.CHRG.CODE)<1,1> = '9'
                END
            END
        END ELSE
            R.NEW(LD.CHRG.CODE)<1,1> = '9'
        END
    END

    R.NEW(LD.CHRG.CODE)<1,2> = '14'
    R.NEW(LD.CHRG.CODE)<1,3> = '17'
*    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1111'
    R.NEW(LD.FIN.MAT.DATE)='20401231'
*    CALL REBUILD.SCREEN
    RETURN
END
