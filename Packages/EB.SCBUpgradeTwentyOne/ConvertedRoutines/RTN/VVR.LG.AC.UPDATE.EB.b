* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AC.UPDATE.EB

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN
        FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
        CALL OPF(FN.ACC,F.ACC)

        CHG.ACCT= COMI
        CALL F.READ(FN.ACC,CHG.ACCT,R.ACC,F.ACC,E.AC)
        CHG.CATG = R.ACC<AC.CATEGORY>
        CHG.CCY  = R.ACC<AC.CURRENCY>
        CHG.CUST = R.ACC<AC.CUSTOMER>

        IF CHG.CATG EQ '9090' OR CHG.CATG EQ '1220' OR CHG.CATG EQ '9091' THEN
            ETEXT='CAT NOT ALLOWED';CALL STORE.END.ERROR
        END ELSE
            R.NEW(LD.CHRG.LIQ.ACCT) = COMI
            R.NEW(LD.CHRG.CODE)<1,1> = 14
            R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
        END

        IF R.NEW(LD.CUSTOMER.ID) NE '99499900' THEN
            IF R.NEW(LD.CURRENCY) NE CHG.CCY THEN
                ETEXT ='Must Be Same Currnecy';CALL STORE.END.ERROR
            END
            IF R.NEW(LD.CUSTOMER.ID) NE CHG.CUST THEN
                ETEXT='Customer Account Must Be Same The LG Customer';CALL STORE.END.ERROR
            END
            IF COMI EQ R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> THEN
                ETEXT='DR AND CR ACCT SHOULD BE DIFFRENT';CALL STORE.END.ERROR
            END
        END
******************
        AC.DR =R.NEW( LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.DR,DR.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.DR,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF DR.CAT EQ '9090' OR DR.CAT EQ '1220' OR DR.CAT EQ '9091' THEN
            ETEXT='CAT NOT ALLOWED IN DR ACCT';CALL STORE.END.ERROR
        END
******************
        CALL REBUILD.SCREEN
    END

    RETURN
END
