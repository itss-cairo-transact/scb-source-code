* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
********* WAEL **************
*-----------------------------------------------------------------------------
* <Rating>570</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CHRG.AC.9090

* THE DEBIT.ACCOUNT SHOULD BE FOR THE CUSTOMER

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE

    IF COMI THEN
*********************
        FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
        CALL OPF(FN.ACC,F.ACC)
        FN.LMM= 'FBNK.LMM.CHARGE.CONDITIONS'; F.LMM = ''
        CALL OPF(FN.LMM,F.LMM)
        FN.CHR= 'FBNK.FT.CHARGE.TYPE' ; F.CHR = ''
        CALL OPF(FN.CHR,F.CHR)
        FN.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
        CALL OPF(FN.COM,F.COM)

*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
        MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>

        W.CUR =  R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
*******************************
        CHRG.ACC = COMI
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,CHRG.ACC,CHRG.CCY)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CHRG.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CHRG.CCY=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CHRG.ACC,CHRG.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CHRG.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CHRG.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CHRG.ACC,CHRG.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CHRG.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CHRG.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>

        IF MESSAGE NE 'VAL' THEN
        END
        IF R.NEW(LD.CUSTOMER.ID) NE '99499900' THEN
            IF R.NEW(LD.CURRENCY) NE CHRG.CCY THEN
                ETEXT ='Must Be Same Currnecy';CALL STORE.END.ERROR
            END
            IF R.NEW(LD.CUSTOMER.ID) NE CHRG.CUST THEN
                ETEXT='Customer Account Must Be Same The LG Customer';CALL STORE.END.ERROR
            END

            IF COMI EQ R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> THEN
                ETEXT='DR AND CR ACCT SHOULD BE DIFFRENT';CALL STORE.END.ERROR
            END
        END
        CALL REBUILD.SCREEN
***************************
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        IF MYCODE EQ "1301" OR MYCODE EQ '1302' THEN
            IF CHRG.CAT EQ '9090' OR CHRG.CAT EQ '9091' THEN
                R.NEW(LD.CHRG.CODE) = ''
                R.NEW(LD.CHRG.AMOUNT)  = ''
                R.NEW(LD.CHRG.CLAIM.DATE) = ''

*Line [ 85 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE > = ''
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''

                FOR I = 2 TO COUNT.CODE
                    IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                        DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                        DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
                    END
                NEXT I
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = '14'
                CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,R.LMM,F.LMM,EE2)
                CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>

                CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
                CH.AMT = R.CHR<FT5.FLAT.AMT,J>
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = CH.AMT
            END ELSE
                R.NEW(LD.CHRG.CODE)<1,1> = "14"
                R.NEW(LD.CHRG.CLAIM.DATE)<1,1>= TODAY
            END
        END
    END
********************************
    RETURN
END
