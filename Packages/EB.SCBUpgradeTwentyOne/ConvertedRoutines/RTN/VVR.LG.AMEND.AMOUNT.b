* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1776</Rating>
*-----------------------------------------------------------------------------
******** ABEER 12/3/2003**************
* A Routine To handle Margin Amount & Percentage Changes If LG Amount is Changed.

    SUBROUTINE VVR.LG.AMEND.AMOUNT

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
            IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
            OLD.LG.AMOUNT=R.NEW(LD.AMOUNT)
            OLD.CUSTOMER.ID=R.NEW(LD.CUSTOMER.ID)
*CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
            OLD.MAT.DATE=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
            LGPERC=MYLOCAL<1,LDLR.MARGIN.PERC>/100
            THIRD.NO=MYLOCAL<1,LDLR.THIRD.NUMBER>
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE=DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
            MYTYPE=MYLOCAL<1,LDLR.PRODUCT.TYPE>
************* GET ONLINE LIMIT **************************
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,OLD.CUSTOMER.ID,LIMREF)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,OLD.CUSTOMER.ID,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LIMREF=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
            LIMPROD = FIELD(LIMREF,'.',2)
            R.NEW(LD.LIMIT.REFERENCE)= TRIM(LIMPROD,"0","L")
*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LIMIT':@FM:LI.ONLINE.LIMIT,LIMREF,ONLIMIT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIMREF,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
ONLIMIT=R.ITSS.LIMIT<LI.ONLINE.LIMIT>

            MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            NEW.MAT.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            NEW.LG.AMOUNT= OLD.LG.AMOUNT + COMI
*******************************************************************************
            IF COMI THEN
                R.NEW(LD.CHRG.AMOUNT)=''
                FOR I = 2 TO COUNT.AMT
                    IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN
                        DEL R.NEW(LD.CHRG.AMOUNT)<1,I>
                        DEL R.NEW(LD.CHRG.CODE)<1,I>
                    END
                NEXT I
                IF OLD.CUSTOMER.ID EQ THIRD.NO THEN
                    GOSUB CHECK.CODE
**                    RETURN
********************************************************************************
CHECK.CODE:
                    IF COMI > 0 THEN
                        IF NEW.LG.AMOUNT< ONLIMIT THEN
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.AMOUNT*LGPERC
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                        END ELSE
                            WW =NEW.LG.AMOUNT-ONLIMIT
                            ZZ = LGPERC * ONLIMIT
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = WW+ZZ
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                        END
                        IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                            IF MYTYPE EQ 'ADVANCE' THEN
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1271'
                            END ELSE
                                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
                            END

                            R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                            GOSUB TAX.AMOUNT

*   R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='1232'
                        END  ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1234'
                            R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                            GOSUB TAX.AMOUNT
*  R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='1234'
                        END
                    END
                    ELSE
                        IF ABS(COMI) > OLD.LG.AMOUNT OR ABS(COMI) EQ OLD.LG.AMOUNT THEN ETEXT='This.Amount.Must.Be.Less.Than.LGAmount'
                        IF NEW.LG.AMOUNT< ONLIMIT THEN
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.AMOUNT*LGPERC
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                        END ELSE
                            WW =NEW.LG.AMOUNT-ONLIMIT
                            ZZ = LGPERC * ONLIMIT
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = WW+ZZ
                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                        END
                        IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1233'
                            GOSUB TAX.AMOUNT
*  R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='1233'
                        END   ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1235'
                            GOSUB TAX.AMOUNT
* R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='1235'
                        END

                    END
                    RETURN
********************************************************************************************
                END ELSE GOSUB CHECK.CODE
            END ELSE
                IF OLD.MAT.DATE # NEW.MAT.DATE THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1231'
                    GOSUB TAX.AMOUNT
*  R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='1231'
                END ELSE
                    ETEXT='You.Must.Amend.Amount.OR.Date'
                END
            END

*END
*ELSE
*  IF MESSAGE='VAL' THEN
*     IF R.NEW(LD.AMOUNT.INCREASE) EQ '' OR OLD.MAT.DATE EQ  NEW.MAT.DATE THEN
*        ETEXT='You.Must.Amend.Amount.OR.Date'
*   END
* END
* END
*********  To  GET  Equivalent For The Amount  ***********************************
*       LAMT = "" ; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; R.COUNT = ''
*       OLD.LG.AMOUNT=R.NEW(LD.AMOUNT)
*       NEW.LG.AMOUNT= OLD.LG.AMOUNT + COMI
*       CURR   = R.NEW(LD.CURRENCY)
*       MARKET = R.NEW(LD.CURRENCY.MARKET)

*         IF CURR # LCCY THEN
*         CALL MIDDLE.RATE.CONV.CHECK(NEW.LG.AMOUNT,CURR,RATE,MARKET,LAMT,DIF.AMT,DIF.RATE)
*         R.NEW(LD.D.D.MULT.AMT)=LAMT
*         END
*         ELSE R.NEW(LD.D.D.MULT.AMT)=NEW.LG.AMOUNT
*         CALL REBUILD.SCREEN
*************************************************************************************
TAX.AMOUNT:
*--------------- TAX  ----------------
            MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            MYTYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
            MYID = MYCODE:'.':MYTYPE

            FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
            CALL OPF(FN.LG,F.LG)
            CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)
*Line [ 171 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            TAX.CONT = DCOUNT(R.LG<SCB.LG.CH.TAX.AMOUNT>, @VM)
            IF TAX.CONT >= 1 THEN
                FOR I = 1 TO TAX.CONT
                    R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES,I> = R.LG<SCB.LG.CH.TAX.AMOUNT,I>
                NEXT I
            END

            RETURN
*****************************
        END
    END
    RETURN
END
