* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>310</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.OLD.BAL

*    PROGRAM VVR.LOAN.OT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.BALANCE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

**********************************************************
    F.LGG = '' ; FN.LGG = 'F.SCB.LG.BALANCE'; R.LGG = ''
    CALL OPF(FN.LGG,F.LGG)
**********************************
    CAA= "CA..."
    T.SELG = "SELECT F.SCB.LG.BALANCE WITH @ID LIKE " : CAA
**********************************
    KEY.LISTG=""
    SELECTEDG=""
    ER.MSGG=""
    CALL EB.READLIST(T.SELG,KEY.LISTG,"",SELECTEDG,ER.MSGG)
    TEXT = "CCCC= " : SELECTEDG ; CALL REM
    IF KEY.LISTG THEN
        TEXT = "DDDD= "SELECTEDG ; CALL REM
        FOR G = 1 TO SELECTEDG
            CALL F.READ(FN.LGG,KEY.LISTG<G>,R.LGG,F.LGG,READ.ERRG)
*******************************UPDATED BY RIHAM R15**********************

*            DELETE F.LGG , KEY.LISTG<G>
            CALL F.DELETE (FN.LGG,KEY.LISTG<G>)
**********************************************************
        NEXT G
        TEXT = "XXXXX= "SELECTEDG ; CALL REM
***************************************************
        F.COUNT = '' ; FN.COUNT = 'F.SCB.LG.BALANCE'; R.COUNT = ''
        CALL OPF(FN.COUNT,F.COUNT)
**********************************
        FN.ACCOUNT = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

        CALL OPF( FN.ACCOUNT,F.ACCOUNT)
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND STATUS NE LIQ AND AMOUNT NE 0 BY OLD.ACCT.NO"
**********************************
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        PRINT SELECTED
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED

                CALL F.READ(FN.ACCOUNT,KEY.LIST<I>, R.ACCOUNT,F.ACCOUNT, ETEXT)
                CALL F.READ(FN.ACCOUNT,KEY.LIST<I+1>, R.ACCOUNT1,F.ACCOUNT, ETEXT)

                ID.NO = R.ACCOUNT<LD.LOCAL.REF,LDLR.OLD.ACCT.NO>
                PRINT ID.NO
                R.COUNT<LGOLD.CURRENCY> = R.ACCOUNT<LD.CURRENCY>

                CUS = R.ACCOUNT<LD.LOCAL.REF,LDLR.OLD.ACCT.NO>
                CUS1 = R.ACCOUNT1<LD.LOCAL.REF,LDLR.OLD.ACCT.NO>
                IF CUS EQ CUS1 THEN
                    TOT.AMT = TOT.AMT+R.ACCOUNT<LD.AMOUNT>
                END ELSE
                    TOT.AMT = TOT.AMT+R.ACCOUNT<LD.AMOUNT>
                    R.COUNT<LGOLD.AMOUNT> = TOT.AMT
                    R.COUNT<LGOLD.CUS.NO> =  R.ACCOUNT<LD.CUSTOMER.ID>
*******************************UPDATED BY RIHAM R15**********************
*                    WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
*                        PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
*                    END
                    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
                    CALL JOURNAL.UPDATE(ID.NO)
*****************************************************
                    TOT.AMT = "0"
                END
            NEXT I
        END
***************************** ************************
        TEXT = "LCCCCCCCCC"   ; CALL REM
        F.COUNT.LC = '' ; FN.COUNT.LC = 'F.SCB.LG.BALANCE'; R.COUNT.LC = ''
        CALL OPF(FN.COUNT.LC,F.COUNT.LC)

*****************************************************
        FN.ACCOUNT.LC = 'FBNK.LETTER.OF.CREDIT' ; F.ACCOUNT.LC = '' ; R.ACCOUNT.LC = ''

        CALL OPF( FN.ACCOUNT.LC,F.ACCOUNT.LC)
        LGG = "LG..."
        T.SELL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE " :LGG: " : AND FULLY.UTILISED NE 'Y'  BY OLD.ACCT.NO"
**********************************
        KEY.LIST.LC=""
        SELECTED.LC=""
        ER.MSG.LC=""
        CALL EB.READLIST(T.SELL,KEY.LIST.LC,"",SELECTED.LC,R.MSG.LC)
        IF KEY.LIST.LC THEN

            FOR H = 1 TO SELECTED.LC

                CALL F.READ(FN.ACCOUNT.LC,KEY.LIST.LC<H>, R.ACCOUNT.LC,F.ACCOUNT.LC, ETEXT)
                CALL F.READ(FN.ACCOUNT.LC,KEY.LIST.LC<H+1>, R.ACCOUNT1.LC,F.ACCOUNT.LC, ETEXT)

                ID.NO = R.ACCOUNT.LC<TF.LC.LOCAL.REF,LCLR.OLD.ACCT.NO>
                PRINT ID.NO
                R.COUNT.LC<LGOLD.CURRENCY> = R.ACCOUNT.LC<TF.LC.LC.CURRENCY>
                R.COUNT.LC<LGOLD.CUS.NO> =  R.ACCOUNT.LC<TF.LC.APPLICANT.CUSTNO>
                CUS = R.ACCOUNT.LC<TF.LC.LOCAL.REF,LCLR.OLD.ACCT.NO>
                CUS1 = R.ACCOUNT1.LC<TF.LC.LOCAL.REF,LCLR.OLD.ACCT.NO>
                IF CUS EQ CUS1 THEN
                    TOT.AMT1 = TOT.AMT1 + R.ACCOUNT.LC<TF.LC.LC.AMOUNT>
                END ELSE
                    TOT.AMT1 = TOT.AMT1 + R.ACCOUNT.LC<TF.LC.LC.AMOUNT>
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('SCB.LG.BALANCE':@FM:LGOLD.AMOUNT,ID.NO,AMTT)
F.ITSS.SCB.LG.BALANCE = 'F.SCB.LG.BALANCE'
FN.F.ITSS.SCB.LG.BALANCE = ''
CALL OPF(F.ITSS.SCB.LG.BALANCE,FN.F.ITSS.SCB.LG.BALANCE)
CALL F.READ(F.ITSS.SCB.LG.BALANCE,ID.NO,R.ITSS.SCB.LG.BALANCE,FN.F.ITSS.SCB.LG.BALANCE,ERROR.SCB.LG.BALANCE)
AMTT=R.ITSS.SCB.LG.BALANCE<LGOLD.AMOUNT>
                    TOTAL.AMMT = TOT.AMT1 + AMTT
                    R.COUNT.LC<LGOLD.AMOUNT> = TOTAL.AMMT
*******************************UPDATED BY RIHAM R15**********************
*                    WRITE R.COUNT.LC TO F.COUNT.LC , ID.NO ON ERROR
*                        PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT.LC
*                    END
                    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
                    CALL JOURNAL.UPDATE(ID.NO)
*******************************************
                    TOT.AMT1 = "0"
                END
            NEXT H
        END

        RETURN
    END
