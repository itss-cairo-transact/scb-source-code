* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.DEFINE.MOR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    SHIFT ='' ; ROUND =''
    DEFFUN SHIFT.DATE(E.COM.DATE, SHIFT, ROUND)

    IF MESSAGE NE 'VAL' THEN
        COMM=FIELD(COMI,'-',1)
        TEXT=COMM;CALL REM
        IF COMM EQ 'YES' THEN

            E.COM.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            E.DATE     = SHIFT.DATE( E.COM.DATE , '3M', 'UP')
            EXP.DATE   = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            IF E.DATE GT EXP.DATE  THEN
***      ETEXT='Commission.Schedule.Yes.Not.Allowed'
            END

            IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = '100' THEN
                ETEXT='Commission.Schedule.Yes.Not.Allowed'
                BEGIN.DATE = R.NEW(LD.VALUE.DATE)
                END.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
                DIFF = "C"
                CALL CDD("",BEGIN.DATE,END.DATE,DIFF)
                SAM1 = DIFF / 90
                SAM2 = SAM1 + 0.9
                PERIOD.NO = FIELD(SAM2,".",1)
                R.NEW(LD.LOCAL.REF)<1,94> = PERIOD.NO

            END ELSE
               ** R.NEW(LD.DEFINE.SCHEDS) = 'YES'
                R.NEW(LD.DEFINE.SCHEDS) = 'NO'
                R.NEW(LD.AUTO.SCHEDS) = 'NO'
                R.NEW(LD.LOCAL.REF)<1,94> = '1'
            END
        END  ELSE

            IF COMM EQ 'NO' THEN
                R.NEW(LD.DEFINE.SCHEDS) = 'NO'
               ** R.NEW(LD.AUTO.SCHEDS) = 'YES'
                R.NEW(LD.AUTO.SCHEDS) = 'NO'
************************
                BEGIN.DATE = R.NEW(LD.VALUE.DATE)
                END.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
                DIFF = "C"
                CALL CDD("",BEGIN.DATE,END.DATE,DIFF)
                SAM1 = DIFF / 90
                SAM2 = SAM1 + 0.9
                PERIOD.NO = FIELD(SAM2,".",1)
                R.NEW(LD.LOCAL.REF)<1,94> = PERIOD.NO
************************
            END
        END

        CALL REBUILD.SCREEN
    END
    IF R.NEW(LD.CATEGORY) EQ 21096 THEN
*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    END ELSE
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT((R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE>),@VM)
    END
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I

    CALL REBUILD.SCREEN



    RETURN
END
