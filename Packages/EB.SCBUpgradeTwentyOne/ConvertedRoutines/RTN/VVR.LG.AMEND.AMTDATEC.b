* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>250</Rating>
*-----------------------------------------------------------------------------
********* ABEER 12/3/2003**************
*A Routine To Change Operation Code If Maturity.Date Is Changed OR Amount Changed
    SUBROUTINE VVR.LG.AMEND.AMTDATEC

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE

**********20081114***HHHHH******************
***************HHHHHH*************************
    FN.F.COM= 'F.SCB.LG.FREE.CHARGE' ; F.F.COM = ''
    CALL OPF(FN.F.COM,F.F.COM)
    CALL F.READ(FN.F.COM,R.NEW(LD.CUSTOMER.ID),R.F.COM,F.F.COM,E11)

    IF E11 THEN


        IF COMI THEN
            R.NEW(LD.CHRG.CODE)   = ""
            R.NEW(LD.CHRG.AMOUNT) = ""

            COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
            IF COMM EQ 'YES' THEN
*
                R.NEW(LD.CHRG.CODE)<1,1> = 13
                R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
                R.NEW(LD.CHRG.CODE)<1,2> = 14
                R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY
*
            END ELSE
*

                IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>  THEN
                    R.NEW(LD.CHRG.CODE)<1,1> = 13
                    R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
                    R.NEW(LD.CHRG.CODE)<1,2> = 14
                    R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY
                END ELSE
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 4
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY
                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 7
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY
                    END
                    IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 9
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,2> = TODAY
                    END
                END
            END

        END
    END ELSE
        R.NEW(LD.CHRG.CODE) = ''
    END
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I

    CALL REBUILD.SCREEN


    RETURN
END
