* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.MLAD.LINE.DESC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE

* Created by Noha Hamed

    FN.LINE = 'F.RE.STAT.REP.LINE'; F.LINE = ''
    CALL OPF(FN.LINE,F.LINE)

    IF V$FUNCTION EQ 'I' THEN
        IF COMI NE '' THEN
 *           MLAD.LINE.NO = 'GENLED.':COMI
             MLAD.LINE.NO = 'GENALL.':COMI
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('RE.STAT.REP.LINE':@FM:RE.SRL.DESC,MLAD.LINE.NO,LINE.DESC)
F.ITSS.RE.STAT.REP.LINE = 'F.RE.STAT.REP.LINE'
FN.F.ITSS.RE.STAT.REP.LINE = ''
CALL OPF(F.ITSS.RE.STAT.REP.LINE,FN.F.ITSS.RE.STAT.REP.LINE)
CALL F.READ(F.ITSS.RE.STAT.REP.LINE,MLAD.LINE.NO,R.ITSS.RE.STAT.REP.LINE,FN.F.ITSS.RE.STAT.REP.LINE,ERROR.RE.STAT.REP.LINE)
LINE.DESC=R.ITSS.RE.STAT.REP.LINE<RE.SRL.DESC>
            R.NEW(MLPAR.LINE.DESC)    = LINE.DESC
            CALL REBUILD.SCREEN
        END

    END

    RETURN
END
