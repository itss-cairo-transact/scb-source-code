* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>63</Rating>
*-----------------------------------------------------------------------------
**** RANIA 24/02/2003 ****

*A Routine To Default DrawDown Account & Margin Account according to customer,currency & category

    SUBROUTINE VVR.LG.ADVANCE.DCCURR


*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN

            MYID=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
**            CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.CURRENCY,MYID,LGCURR)
**            LOCATE COMI IN LGCURR<1,1> SETTING LLL  ELSE ETEXT='Currency.Not.Found.In.LG.CHARGE' ; RETURN

            R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = ''
            R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> = ''

            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''

            T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": R.NEW(LD.CUSTOMER.ID) :" AND CURRENCY EQ ": COMI :" AND ( CATEGORY GE '1000' AND CATEGORY LE '1599' ) OR ( CATEGORY EQ 6511 OR CATEGORY EQ 6512) "
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

            IF NOT(SELECTED) THEN
                ETEXT='There.Is.No.Account' ;RETURN
            END ELSE
********************MODIFIED ON 26/JAN/2004***************************
                IF COMI NE LCCY THEN
                    T.SEL1= '';CUR.LCCY='EGP';KEY.LIST1 = '';SELECTED1 = '';ERR.MSG1 = ''
                    T.SEL1 = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": R.NEW(LD.CUSTOMER.ID) :" AND CURRENCY EQ ": CUR.LCCY :" AND ( CATEGORY GE '1000' AND CATEGORY LE '1014' )"
                    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ERR.MSG1 )
**  IF NOT(SELECTED1) THEN
**      ETEXT='There.Is.No.Account.By.Local.Currency';RETURN
**  END ELSE
**    R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = KEY.LIST<1>
**  END
                END ELSE
** R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = KEY.LIST<1>
                END
***************************************************************
            END
            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
            T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": R.NEW(LD.CUSTOMER.ID) :" AND CURRENCY EQ ": COMI:" AND CATEGORY EQ '3005'"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
            IF R.NEW(LD.CUSTOMER.ID) EQ '99499900' THEN  R.NEW(LD.CURRENCY) = COMI
            IF R.NEW(LD.CUSTOMER.ID) NE '99499900' THEN
            IF NOT(SELECTED) THEN

                ETEXT='There.Is.No.Margin.Account'

            END ELSE
                R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> = KEY.LIST<1>
            END
       END

            R.NEW(LD.CURRENCY) =COMI
            CALL REBUILD.SCREEN

        END
    END
    RETURN
END
