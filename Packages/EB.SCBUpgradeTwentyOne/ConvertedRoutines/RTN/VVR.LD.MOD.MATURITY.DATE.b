* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE

*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.MOD.MATURITY.DATE


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RATES

*-------------------------------------------------------------------------------------

IF R.NEW(LD.FIN.MAT.DATE) LT TODAY THEN ETEXT = 'Enter.Date.After.Today.Date'
*  IF R.NEW(LD.FIN.MAT.DATE) LT R.OLD(LD.FIN.MAT.DATE) THEN R.NEW()

*-------------------------------------------------------------------------------------

DATE.1 = '' ; DATE.2 = '' ; DAYS = "C" ; DD = ''
CAT.CURR.DATE = '' ; E1 = ''
FN.SCB.RATES = 'F.SCB.RATES' ; F.SCB.RATES = '' ; R.SCB.RATES = ''

*-------------------------------------------------------------------------------------

DATE.1 = R.NEW (LD.VALUE.DATE)
DATE.2 = COMI

CALL CDD("",DATE.1,DATE.2,DAYS)
R.NEW (LD.JOINT.NOTES) = DAYS

CALL CDT("",DATE.1,DAYS)
***R.NEW (LD.CHRG.CLAIM.DATE) = DATE.1

*-------------------------------------------------------------------------------------

 IF R.OLD(LD.FIN.MAT.DATE) NE COMI THEN
    CAT.CURR.DATE = R.NEW (LD.CATEGORY) : R.NEW (LD.CURRENCY) : R.NEW (LD.VALUE.DATE)
     CALL OPF(FN.SCB.RATES,F.SCB.RATES)
      CALL F.READ(FN.SCB.RATES,CAT.CURR.DATE,R.SCB.RATES,F.SCB.RATES,E1)
       R.NEW (LD.NEW.INT.RATE) = R.SCB.RATES<SCB.RAT.NEW.INTEREST.RATE>
        R.NEW (LD.NEW.SPREAD) = R.SCB.RATES<SCB.RAT.NEW.SPREAD>
         CLOSE FN.SCB.RATES
 END
*-------------------------------------------------------------------------------------

CALL REBUILD.SCREEN

RETURN
END
