* @ValidationCode : MjotMTQ3OTU0ODk5MDpDcDEyNTI6MTY0MjI5MDA4ODI1MjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:41:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
****ABEER*****
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.FLAT.LOAN


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN
        T(LD.LIMIT.REFERENCE)<3> ='NOINPUT'
        T(LD.INTEREST.RATE)<3> ='NOINPUT'

*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,COMI,FLAT.ID)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,COMI,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
FLAT.ID=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
        IF NOT(ETEXT) THEN
            ETEXT='Must.Not.Be.Existing.LD'
            T(LD.LIMIT.REFERENCE)<3> ='NOINPUT'
            T(LD.INTEREST.RATE)<3> ='NOINPUT'
            CALL REBUILD.SCREEN
        END ELSE
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.CATEGORY,COMI,FLAT.ID)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,COMI,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
FLAT.ID=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.CATEGORY>
            IF ETEXT THEN
                ETEXT='Must.Be.NAU.LD'
                T(LD.LIMIT.REFERENCE)<3> ='NOINPUT'
                T(LD.INTEREST.RATE)<3> ='NOINPUT'
                CALL REBUILD.SCREEN
            END ELSE
                T(LD.LIMIT.REFERENCE)<3> =''
                T(LD.INTEREST.RATE)<3> =''
*********************************************************************
                FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU';F.LD='';R.LD = '';E=''
                CALL OPF(FN.LD,F.LD)
                CALL F.READ(FN.CUSTOMER,COMI, R.LD, F.LD,E)
                R.NEW(LD.CUSTOMER.ID)=R.LD<LD.CUSTOMER.ID>
                R.NEW(LD.LOCAL.REF)<1,LDLR.CUSBRANCH>=R.LD<LD.LOCAL.REF><1,LDLR.CUSBRANCH>
                R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP>=R.LD<LD.LOCAL.REF><1,LDLR.PERSONCORP>
                R.NEW(LD.CURRENCY)=R.LD<LD.CURRENCY>
                R.NEW(LD.AMOUNT)=R.LD<LD.AMOUNT>
                R.NEW(LD.BUS.DAY.DEFN)=R.LD<LD.BUS.DAY.DEFN>
                R.NEW(LD.VALUE.DATE)=R.LD<LD.VALUE.DATE>
                R.NEW(LD.FIN.MAT.DATE)=R.LD<LD.FIN.MAT.DATE>
                R.NEW(LD.CHRG.CODE)=R.LD<LD.CHRG.CODE>
                R.NEW(LD.CHRG.AMOUNT)=R.LD<LD.CHRG.AMOUNT>
                R.NEW(LD.CHRG.CLAIM.DATE)=R.LD<LD.CHRG.CLAIM.DATE>

                CALL REBUILD.SCREEN
            END
        END

    END ELSE
        T(LD.LIMIT.REFERENCE)<3> ='NOINPUT'
        T(LD.INTEREST.RATE)<3> ='NOINPUT'
        CALL REBUILD.SCREEN
    END
RETURN
END
