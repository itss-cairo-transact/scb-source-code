* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>48</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.EXP.STAFF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.APPL.GEN.CONDITION
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.GROUP.CONDITION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.EXP.STAFF
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.EXP.STAFF


    IF R.NEW(LD.CUSTOMER.ID) THEN
        CUS.ID = R.NEW(LD.CUSTOMER.ID)
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        EMP.NO = LOCAL.REF<1,CULR.EMPLOEE.NO>
 *       TEXT = "EMP" : EMP.NO ; CALL REM

        FN.LD.EXP.STAFF ="F.SCB.LD.EXP.STAFF"

        F.LD.EXP.STAFF = ""
        CALL OPF(FN.LD.EXP.STAFF,F.LD.EXP.STAFF)
        CALL F.READ(FN.LD.EXP.STAFF,EMP.NO,LD.EXP.STAFF.REC,F.LD.EXP.STAFF,E1)
        IF NOT(E1) THEN
*Line [ 53 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            EXP.COUNT = DCOUNT(LD.EXP.STAFF.REC<LDEXP.AMOUNT>,@VM)
            FOR I = 1 TO EXP.COUNT
*                TEXT = LD.EXP.STAFF.REC<LDEXP.AMOUNT,I> ; CALL REM
                IF LD.EXP.STAFF.REC<LDEXP.FINAL.DATE,I> GT TODAY THEN
                    TOT = TOT + LD.EXP.STAFF.REC<LDEXP.AMOUNT,I>

                END
            NEXT I
IF TOT + R.NEW(LD.AMOUNT) GE 100000 THEN
 R.NEW(LD.INTEREST.SPREAD) = 0

TEXT = "THES CUSTOMER IS OVER LIMIT" ; CALL REM

        END
    END
    RETURN
END
