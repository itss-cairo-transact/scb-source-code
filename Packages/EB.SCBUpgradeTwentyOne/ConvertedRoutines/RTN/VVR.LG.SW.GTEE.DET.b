* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*****RANIA*****
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.SW.GTEE.DET
*TO DEFAULT FIELD SW.GTEE.DET WITH TEXT IF FOUND FROM TABLE SCB.LG.TEXT
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.TEXT

    IF MESSAGE NE 'VAL'  THEN

        IF COMI THEN

*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.LG.TEXT':@FM:SCB.LG.TEXT,COMI,MYTEXT)
F.ITSS.SCB.LG.TEXT = 'F.SCB.LG.TEXT'
FN.F.ITSS.SCB.LG.TEXT = ''
CALL OPF(F.ITSS.SCB.LG.TEXT,FN.F.ITSS.SCB.LG.TEXT)
CALL F.READ(F.ITSS.SCB.LG.TEXT,COMI,R.ITSS.SCB.LG.TEXT,FN.F.ITSS.SCB.LG.TEXT,ERROR.SCB.LG.TEXT)
MYTEXT=R.ITSS.SCB.LG.TEXT<SCB.LG.TEXT>
            IF ETEXT THEN ETEXT = ''
            ELSE

*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                MYCOUNT=DCOUNT(MYTEXT,@VM)
                COMI = MYTEXT<1,1>

                FOR I = 2 TO MYCOUNT

                    R.NEW( LD.LOCAL.REF)< 1,LDLR.SW.GTEE.DET,I> =  MYTEXT<1,I>

                NEXT I
                CALL REBUILD.SCREEN
            END

        END
        RETURN
    END
