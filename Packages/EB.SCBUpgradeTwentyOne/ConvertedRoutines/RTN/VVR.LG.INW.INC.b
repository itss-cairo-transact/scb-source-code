* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LG.INW.INC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'BIDBOND' THEN
        R.NEW(LD.CHRG.CODE)<1,1> = '4'
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'FINAL' THEN
        TEXT='FINAL';CALL REM
        R.NEW(LD.CHRG.CODE)<1,1> = '7'
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'ADVANCE' THEN
        R.NEW(LD.CHRG.CODE)<1,1> = '9'
    END
    R.NEW(LD.CHRG.CODE)<1,2> = '14'
    R.NEW(LD.CHRG.CODE)<1,3> = '17'
*   R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1111'
    R.NEW(LD.FIN.MAT.DATE)='20401231'
*************
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
    OLD.MAT.DATE = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
    NEW.MAT.DATE= R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
    IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'ADVANCE' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1271'
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
        END
    END  ELSE
        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1234'
    END
    RETURN
END
