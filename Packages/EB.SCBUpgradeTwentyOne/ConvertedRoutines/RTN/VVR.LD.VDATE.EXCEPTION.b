* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LD.VDATE.EXCEPTION

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.EXCEPTION

    FN.LDE = 'F.SCB.LD.EXCEPTION' ; F.LDE = ''
    CALL OPF(FN.LDE,F.LDE)

    CUS.EXC = R.NEW(LD.CUSTOMER.ID):'.':TODAY

    CALL F.READ(FN.LDE,CUS.EXC,R.LDE,F.LDE,ERR)

*Line [ 34 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE '2' IN R.LDE<LDE.EXCEPTION.CODE,1> SETTING POS ELSE NULL
    EXC.CODE = R.LDE<LDE.EXCEPTION.CODE,POS>

    IF NOT(ERR) AND EXC.CODE EQ '2' THEN

    END ELSE
        IF COMI NE TODAY THEN
            ETEXT = '��� ����� ������ �� ����'
        END

    END
    RETURN
END
