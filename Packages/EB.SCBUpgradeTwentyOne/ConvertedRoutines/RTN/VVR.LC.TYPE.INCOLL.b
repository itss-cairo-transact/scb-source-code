* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>250</Rating>
*-----------------------------------------------------------------------------
***********DALIA&INGY-SCB 22/04/2003***********

    SUBROUTINE VVR.LC.TYPE.INCOLL
*A ROUTINE TO CHECK:
* IF TF.LC.LC.TYPE DOES NOT THEN DISPLAY ERROR
* IF TF.LC.LC.TYPE IS CHANGED THEN EMPTY TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE,
* TF.LC.DOC.1ST.COPIES, TF.LC.DOC.2ND.COPIES
* IF LCLR.AIDLC & LCLR.TERMS HAVE VALUES THEN DEFAULT TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE
* WITH SCB.LCD.TERM:LCLR.AIDLC
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.TYPE.VER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

    IF MESSAGE = 'VAL' THEN
        MSG.TYP = '410'
        MSG = R.NEW(TF.LC.MESSAGE.TYPE)
        IF COMI THEN

            ID = APPLICATION:PGM.VERSION
            CALL OPF('F.SCB.LC.TYPE.VER',F.SCB.LC.TYPE.VER)

*******************************UPDATED BY RIHAM R15**********************
**            READ R.SCB.LC.TYPE.VER FROM F.SCB.LC.TYPE.VER,ID THEN
                CALL F.READ ('F.SCB.LC.TYPE.VER',ID,R.SCB.LC.TYPE.VER,F.SCB.LC.TYPE.VER,ERR)
IF NOT(ERR) THEN
************************************************************
                TYP = R.SCB.LC.TYPE.VER<SCB.SLV.LC.TYPE>
                LOCATE COMI IN TYP<1,1> SETTING YY THEN
                    IF R.SCB.LC.TYPE.VER<SCB.SLV.PRODUCE.MT410,YY> = 'NO' THEN
                        LOCATE MSG.TYP IN MSG<1,1> SETTING XX THEN
                            R.NEW(TF.LC.SEND.MESSAGE)<1,XX> = 'NO'
                        END
                    END
                END
            END ELSE ETEXT = 'Type.Not.Allowed'

        END
        RETURN
    END
