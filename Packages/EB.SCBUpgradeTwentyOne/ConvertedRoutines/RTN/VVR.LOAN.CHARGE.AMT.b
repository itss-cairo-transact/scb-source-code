* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LOAN.CHARGE.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*-----------------------------------------------
    IF MESSAGE NE 'VAL' THEN

        WS.RATE = COMI

        IF PGM.VERSION EQ ',SCB.PEP3' THEN
            WS.AMT = R.NEW(LD.AMOUNT.INCREASE) * WS.RATE / 100
            IF WS.AMT LT 0 THEN WS.AMT = WS.AMT * -1
        END ELSE
            WS.AMT = R.NEW(LD.AMOUNT) * WS.RATE / 100
        END


        WS.CATEG = R.NEW(LD.CATEGORY)

        IF WS.CATEG EQ '21063' THEN
            R.NEW(LD.CHRG.CODE)   = '21'
            R.NEW(LD.CHRG.AMOUNT) = WS.AMT
        END

        IF WS.CATEG EQ '21066' THEN
            R.NEW(LD.CHRG.CODE)   = '22'
            R.NEW(LD.CHRG.AMOUNT) = WS.AMT
        END

        IF WS.CATEG EQ '21065' THEN
            R.NEW(LD.CHRG.CODE)   = '23'
            R.NEW(LD.CHRG.AMOUNT) = WS.AMT
        END

        IF WS.CATEG EQ '21064' THEN
            R.NEW(LD.CHRG.CODE)   = '24'
            R.NEW(LD.CHRG.AMOUNT) = WS.AMT
        END

        CALL REBUILD.SCREEN

    END

    RETURN
END
