* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LD.RATE.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.RATE.PREPAID
*--------------------------------------------------
    IF MESSAGE NE 'VAL' THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.SCB.LD.SPREAD> = ''

        FN.BI = 'F.SCB.LD.RATE.PREPAID' ; F.BI = ''
        CALL OPF(FN.BI,F.BI)

        WS.CCY = R.NEW(LD.CURRENCY)
*--------------------------------------------------
        BI.ID = R.NEW(LD.CATEGORY):"...":"USD"

        IF WS.CCY EQ 'USD' THEN
            T.SEL3 = "SELECT ":FN.BI:" WITH @ID LIKE ":BI.ID:" BY @ID"
        END ELSE
            T.SEL3 = "SELECT ":FN.BI:" WITH @ID UNLIKE ":BI.ID:" BY @ID"
        END

        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
        IF SELECTED3 THEN
            CALL F.READ(FN.BI,KEY.LIST3<SELECTED3>,R.BI,F.BI,E3)
            WS.RATE.1M  = R.BI<LRP.RATE.1M>
            WS.RATE.2M  = R.BI<LRP.RATE.2M>
            WS.RATE.3M  = R.BI<LRP.RATE.3M>
            WS.RATE.6M  = R.BI<LRP.RATE.6M>
            WS.RATE.9M  = R.BI<LRP.RATE.9M>
            WS.RATE.12M = R.BI<LRP.RATE.12M>
        END

        V.DATE   = R.NEW(LD.VALUE.DATE)
        MAT.DATE = R.NEW(LD.FIN.MAT.DATE)

        DAYS = "C"
        CALL CDD("",V.DATE,MAT.DATE,DAYS)
        DAYS = DAYS+1

** 1M **

        IF DAYS GE 29 AND DAYS LE 59 THEN
            WS.RATE = WS.RATE.1M
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '1M'
        END

** 2M **
        IF DAYS GE 60 AND DAYS LE 89 THEN
            WS.RATE = WS.RATE.2M
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '2M'
        END


** 3M **
        IF DAYS GE 90 AND DAYS LE 179 THEN
            WS.RATE = WS.RATE.3M
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '3M'
        END


** 6M **
        IF DAYS GE 180 AND DAYS LE 269 THEN
            WS.RATE = WS.RATE.6M
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '6M'
        END


** 9M **
        IF DAYS GE 270 AND DAYS LE 364 THEN
            WS.RATE = WS.RATE.9M
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '9M'
        END


** 12M **
        IF DAYS GE 365 AND DAYS LE 729 THEN
            WS.RATE = WS.RATE.12M
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = '12M'
        END


        R.NEW(LD.INTEREST.RATE) = WS.RATE
    END
    RETURN
END
