* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CHANGE.AMT
*RECALCULATE THE MARG.PER  IN CASE THERE IS A CHANGE IN THE MARGIN.AMT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT




    IF COMI > R.NEW(LD.AMOUNT) THEN
        ETEXT = 'MUST.BE.LESS.THAN.LG.AMOUNT'
    END ELSE
        IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>= 100 THEN COMI= R.NEW(LD.AMOUNT)
        NEW.MARG.PER= (COMI/R.NEW(LD.AMOUNT))*100
        CALL EB.ROUND.AMOUNT ('EGP',NEW.MARG.PER,'',"2")
        R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> =  NEW.MARG.PER
        PRC.NEW = R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
        PRC.OLD = R.OLD( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
        IF PGM.VERSION = ',SCB.LG.AMMARGIN' THEN
            IF PRC.NEW > PRC.OLD  THEN R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = 1242
            IF PRC.NEW < PRC.OLD THEN R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = 1241
        END
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100 THEN
        END
    END
    IF COMI LT '0' THEN
        ETEXT='Minus.Not.Allowed'
        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=R.OLD( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
    END
    CALL REBUILD.SCREEN
    RETURN
END
