* @ValidationCode : Mjo3MjI5NzU4MzA6Q3AxMjUyOjE2NDUxNTc0NjkwNzg6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 17 Feb 2022 20:11:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>517</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VVR.LC.TYPE.EXIST
*A ROUTINE TO CHECK:
* IF TF.LC.LC.TYPE DOES NOT THEN DISPLAY ERROR
* IF TF.LC.LC.TYPE IS CHANGED THEN EMPTY TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE,
* TF.LC.DOC.1ST.COPIES, TF.LC.DOC.2ND.COPIES
* IF LCLR.AIDLC & LCLR.TERMS HAVE VALUES THEN DEFAULT TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE
* WITH SCB.LCD.TERM:LCLR.AIDLC
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LC.ADVICE.TEXT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LC.CLAUSES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LC.TYPE.VER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LC.LOCAL.REFS



    IF V$FUNCTION='I' AND MESSAGE # "VAL" THEN
        FN.SCB.LC.DOCTERMS = 'F.SCB.LC.DOCTERMS' ; F.SCB.LC.DOCTERMS = '' ; R.SCB.LC.DOCTERMS = ''
        DOC.ID = R.NEW(TF.LC.LC.TYPE):'.': R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC>
        ID = APPLICATION:PGM.VERSION
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
* CALL DBR("SCB.LC.TYPE.VER":@FM:SCB.SLV.LC.TYPE,ID,TYP)
        F.ITSS.SCB.LC.TYPE.VER = 'F.SCB.LC.TYPE.VER'
        FN.F.ITSS.SCB.LC.TYPE.VER = ''
        CALL OPF(F.ITSS.SCB.LC.TYPE.VER,FN.F.ITSS.SCB.LC.TYPE.VER)
        CALL F.READ(F.ITSS.SCB.LC.TYPE.VER,ID,R.ITSS.SCB.LC.TYPE.VER,FN.F.ITSS.SCB.LC.TYPE.VER,ERROR.SCB.LC.TYPE.VER)
        TYP=R.ITSS.SCB.LC.TYPE.VER<SCB.SLV.LC.TYPE>
        IF NOT(ETEXT) THEN
            LOCATE COMI IN TYP<1,1> SETTING YY ELSE ETEXT = 'Type.Not.Allowed'
        END
        IF COMI # R.NEW(TF.LC.LC.TYPE) OR COMI = '' THEN
            R.NEW(TF.LC.CLAUSES.TEXT) = ''
            R.NEW(TF.LC.DOCUMENT.CODE) = ''
            R.NEW(TF.LC.DOC.1ST.COPIES) = ''
            R.NEW(TF.LC.DOC.2ND.COPIES) = ''
            R.NEW( TF.LC.DOCUMENT.TXT) = ''
        END
        IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC> AND R.NEW(TF.LC.LOCAL.REF)<1,LCLR.TERMS> THEN
            CALL OPF( FN.SCB.LC.DOCTERMS,F.SCB.LC.DOCTERMS)
            CALL F.READ( FN.SCB.LC.DOCTERMS, DOC.ID, R.SCB.LC.DOCTERMS, F.SCB.LC.DOCTERMS, ETEXT)
            TRM = R.SCB.LC.DOCTERMS<SCB.LCD.TERM>
            LOCATE R.NEW(TF.LC.LOCAL.REF)<1,LCLR.TERMS> IN TRM<1,1> SETTING POS THEN
* R.NEW(TF.LC.LOCAL.REF)<1,LCLR.TERMS> = R.SCB.LC.DOCTERMS<SCB.LCD.TERMS>
*Line [ 68 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                CO.DOC = DCOUNT(R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS>,@SM)
*Line [ 70 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                CO.ADD = DCOUNT(R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS>,@SM)
                FOR I =1 TO CO.DOC
                    R.NEW(TF.LC.DOCUMENT.CODE)<1,I> = R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS,I>

* R.NEW(TF.LC.CLAUSES.TEXT) = R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS>
* R.NEW(TF.LC.DOCUMENT.CODE) =  R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS>
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*     CALL DBR("LC.ADVICE.TEXT":@FM:TF.AD.NARRATIVE,R.NEW(TF.LC.DOCUMENT.CODE)<1,I>,NARR)
                    F.ITSS.LC.ADVICE.TEXT = 'F.LC.ADVICE.TEXT'
                    FN.F.ITSS.LC.ADVICE.TEXT = ''
                    CALL OPF(F.ITSS.LC.ADVICE.TEXT,FN.F.ITSS.LC.ADVICE.TEXT)
                    CALL F.READ(F.ITSS.LC.ADVICE.TEXT,NARR,R.ITSS.LC.ADVICE.TEXT,FN.F.ITSS.LC.ADVICE.TEXT,ERROR.LC.ADVICE.TEXT)
                    NARR=R.ITSS.LC.ADVICE.TEXT<@FM:TF.AD.NARRATIVE,R.NEW(TF.LC.DOCUMENT.CODE)<1,I>>
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    RR= DCOUNT(NARR,@VM)
                    FOR Y = 1 TO RR
                        R.NEW(TF.LC.DOCUMENT.TXT)<1,I,Y> = NARR<1,Y>
                        CLOSE F.SCB.LC.DOCTERMS
                    NEXT Y
                NEXT I
                FOR CC = 1 TO CO.ADD
*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*     CALL DBR("LC.CLAUSES":@FM:LC.CL.DESCR,R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS,CC>,DESC)
                    F.ITSS.LC.CLAUSES = 'F.LC.CLAUSES'
                    FN.F.ITSS.LC.CLAUSES = ''
                    CALL OPF(F.ITSS.LC.CLAUSES,FN.F.ITSS.LC.CLAUSES)
                    CALL F.READ(F.ITSS.LC.CLAUSES,DESC,R.ITSS.LC.CLAUSES,FN.F.ITSS.LC.CLAUSES,ERROR.LC.CLAUSES)
                    DESC=R.ITSS.LC.CLAUSES<@FM:LC.CL.DESCR,R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS,CC>>
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DD = DCOUNT (DESC,@VM)
                    TT=''
                    FOR J = 1 TO DD
                        TT := ' ': DESC<1,J>
                    NEXT J
                    R.NEW(TF.LC.CLAUSES.TEXT)<1,CC> = TRIM(TT)
                NEXT CC
            END
        END
    END
RETURN
END
