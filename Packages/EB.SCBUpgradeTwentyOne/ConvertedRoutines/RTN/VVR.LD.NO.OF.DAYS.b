* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
** ----- NESSREEN AHMED 12/09/2002,  -----
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.NO.OF.DAYS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DPST.TYPES



V.DATE = '' ; CATEGORY.NAME = '' ; DAYS = ''

IF COMI THEN
*OR R.NEW(LD.JOINT.NOTES)# COMI

   R.NEW(LD.CATEGORY) = ''
   R.NEW(LD.FIN.MAT.DATE) = ''
   
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*   CALL DBR( 'SCB.DPST.TYPES':@FM:SCB.DPST.TYPES.CATEGORY, COMI, CATEGORY.NAME)
F.ITSS.SCB.DPST.TYPES = 'F.SCB.DPST.TYPES'
FN.F.ITSS.SCB.DPST.TYPES = ''
CALL OPF(F.ITSS.SCB.DPST.TYPES,FN.F.ITSS.SCB.DPST.TYPES)
CALL F.READ(F.ITSS.SCB.DPST.TYPES,COMI,R.ITSS.SCB.DPST.TYPES,FN.F.ITSS.SCB.DPST.TYPES,ERROR.SCB.DPST.TYPES)
CATEGORY.NAME=R.ITSS.SCB.DPST.TYPES<SCB.DPST.TYPES.CATEGORY>
     IF NOT(ETEXT) THEN R.NEW(LD.CATEGORY) = CATEGORY.NAME

   DAYS = COMI
   V.DATE = R.NEW(LD.VALUE.DATE)
   CALL CDT("",V.DATE,DAYS)
   R.NEW(LD.FIN.MAT.DATE) = V.DATE

*   R.NEW(LD.JOINT.NOTES)= COMI
     CALL REBUILD.SCREEN


END

RETURN
END
