* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.PERCENT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE

    IF COMI AND COMI LE 100 THEN
        R.NEW(LD.CHRG.CODE)  = ""
        R.NEW(LD.CHRG.AMOUNT)= ""
        MARG.AMT =   ( R.NEW(LD.AMOUNT) *  COMI ) /  100
        CALL EB.ROUND.AMOUNT ('USD',MARG.AMT,'',"2")
        R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = MARG.AMT
    END ELSE
        IF COMI GT 100 THEN
            ETEXT = "Percent shouldn't exceed 100";CALL STORE.END.ERROR
        END
    END
    IF COMI EQ 0 THEN R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = 0

*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
    MAR.PERC =MYLOCAL<1,LDLR.MARGIN.PERC>

    IF COMI LT MAR.PERC THEN
        R.NEW (LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1241'
    END

    IF COMI GT MAR.PERC THEN
        R.NEW (LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1242'
    END
    CALL REBUILD.SCREEN

    FN.F.COM= 'F.SCB.LG.FREE.CHARGE' ; F.F.COM = ''
    CALL OPF(FN.F.COM,F.F.COM)
    CALL F.READ(FN.F.COM,R.NEW(LD.CUSTOMER.ID),R.F.COM,F.F.COM,E11)

    IF E11 THEN
        IF MAR.PERC NE 100 THEN
            R.NEW(LD.CHRG.CODE) = 13
        END ELSE
            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                R.NEW(LD.CHRG.CODE) = 4
            END
            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                R.NEW(LD.CHRG.CODE) = 7
            END
            IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                R.NEW(LD.CHRG.CODE) = 9
            END
        END
    END ELSE
        R.NEW(LD.CHRG.CODE) = ''
    END
    CALL REBUILD.SCREEN

*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I
    CALL REBUILD.SCREEN


    RETURN
END
