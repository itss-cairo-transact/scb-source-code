* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>2078</Rating>
*-----------------------------------------------------------------------------
******* ABEER 17/3/2003**************

*A Routine To handle If margin Percentage Is Changed Then Margin Amount is changed also

    SUBROUTINE VVR.LG.AMEND.MARG.AMT.PERC

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
*   CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
            OLD.LG.AMOUNT=R.NEW(LD.AMOUNT)
            OLD.MAT.DATE=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
            OLD.MARG.PERC=MYLOCAL<1,LDLR.MARGIN.PERC>
            OLD.MARG.AMT =MYLOCAL<1,LDLR.MARGIN.AMT>
            NEW.MARG.PERC=R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
            NEW.MARG.AMT =R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            AMT.INC.DEC  =R.NEW(LD.AMOUNT.INCREASE)
            NEW.LG.AMOUNT=OLD.LG.AMOUNT+AMT.INC.DEC
            NEW.MAT.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT=DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
***********************************************************************************
            IF NOT(ETEXT) THEN
                IF COMI THEN
                    IF COMI # OLD.MARG.AMT OR NEW.MARG.PERC NE OLD.MARG.PERC THEN
                        IF NEW.MAT.DATE GE OLD.MAT.DATE THEN
                            IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                                ETEXT ='Must.Fill.Amt.Inc/Dec.Field.First'
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=OLD.MARG.PERC
                                COMI=OLD.MARG.AMT
**************************************************************************
                                IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
                                R.NEW(LD.CHRG.AMOUNT)=''
                                FOR I = 2 TO COUNT.AMT
                                    IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
                                NEXT I
*******************************************************************************
                            END ELSE
                                IF COMI > NEW.LG.AMOUNT THEN ETEXT = 'Must.Be.Less.Than.New.LGAmount'
                                ELSE
                                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>= (COMI/NEW.LG.AMOUNT)*100
*************************************************************************
                                    IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
                                    R.NEW(LD.CHRG.AMOUNT)=''
                                    FOR I = 2 TO COUNT.AMT
                                        IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
                                    NEXT I
*************************************************************************
                                END
                            END
                        END
                        CALL REBUILD.SCREEN
                    END ELSE
                        IF NEW.MAT.DATE GT OLD.MAT.DATE THEN
                            IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
*   ETEXT ='Must.Fill.Amt.Inc/Dec.Field.First'
                                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=OLD.MARG.PERC
                            END
                            ELSE
                                IF COMI > NEW.LG.AMOUNT THEN ETEXT = 'Must.Be.Less.Than.New.LGAmount'
                                ELSE
                                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>= (COMI/NEW.LG.AMOUNT)*100
                                END
                            END
                        END ELSE
                            IF  NEW.MAT.DATE EQ OLD.MAT.DATE THEN
                                IF R.NEW(LD.AMOUNT.INCREASE)='' OR R.NEW(LD.AMOUNT.INCREASE) = 0 THEN
                                    ETEXT ='Must.Fill.Amt.Inc/Dec.Field.First'
                                END
                            END
                        END
                        CALL REBUILD.SCREEN
                    END
******************************************************************************
                END ELSE
                    COMI=OLD.MARG.AMT
                END
            END
        END
    END
    RETURN
END
