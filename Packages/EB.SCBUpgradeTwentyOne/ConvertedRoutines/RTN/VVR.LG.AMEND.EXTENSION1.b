* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>390</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.EXTENSION1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
        OLD.MAT.DATE=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
        PRO.TYPE=MYLOCAL<1,LDLR.PRODUCT.TYPE>
        OLD.CODE=MYLOCAL<1,LDLR.OPERATION.CODE>
        NEW.MAT.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        NEW.CODE= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        EXP.DAT = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
        IF COMI EQ 'BEN' THEN

            IF EXP.DAT LT TODAY THEN

                TEXT =  '������ ����� ������� & ': EXP.DAT ; CALL REM
                TEXT =  '������ ����� ������� & ': EXP.DAT ; CALL REM
                TEXT =  '������ ����� ������� & ': EXP.DAT ; CALL REM
            END
************HYTHANM 20090218******************
***   IF EXP.DAT LT TODAY AND  NEW.CODE NE "1239" THEN
**   ETEXT='BENF.IS.NOT.ALLOWED'
***   COMI='CUS'
***   END

            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1234' THEN
**-------------20091220---------------------------------------**
**   ETEXT='BENF.IS.NOT.ALLOWED'
                TEXT = 'BENF.IS.NOT.ALLOWED'  ; CALL REM
**   COMI='CUS'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1232' THEN
**  ETEXT='BENF.IS.NOT.ALLOWED'
                TEXT='BENF.IS.NOT.ALLOWED'   ; CALL REM
** COMI='CUS'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1271' THEN
**  ETEXT='BENF.IS.NOT.ALLOWED'
                TEXT='BENF.IS.NOT.ALLOWED' ; CALL REM
** COMI='CUS'
**-------------20091220---------------------------------------**
            END
            IF PRO.TYPE EQ 'BIDBOND' THEN
                IF NEW.CODE EQ '1231' THEN
                    IF OLD.CODE NE '1239' THEN
                        ETEXT='BENF.IS.NOT.ALLOWED'
                        COMI='CUS'
                    END
                END
            END
        END ELSE
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1233' THEN
                ETEXT='CUST.IS.NOT.ALLOWED'
                COMI='BEN'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1235' THEN
                ETEXT = 'CUST.IS.NOT.ALLOWED'
                COMI = 'BEN'
            END
        END

**********************************************************
        IF NOT(COMI) THEN
            IF NEW.MAT.DATE GT OLD.MAT.DATE THEN
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
                MYOLD.CODE=MYLOCAL<1,LDLR.OPERATION.CODE>

                IF MYOLD.CODE NE '1239' THEN
                    ETEXT = 'You.Should.Enter.Approval.By'
                END

            END
        END
        RETURN
    END
