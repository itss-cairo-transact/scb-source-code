* @ValidationCode : MjotMTQ4NjY3MDIwOkNwMTI1MjoxNjQyMjkwNDkyMjMzOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:48:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LG.DEF.CHRG.UNC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
    $INCLUDE I_F.CUSTOMER

    IF COMI THEN
*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POSS=R.ITSS.CUSTOMER<EB.CUS.TEXT>
        STO1=POSS<1,1>
        STO2=POSS<1,2>
        STO3=POSS<1,3>
        STO4=POSS<1,4>
        IF POSS NE '' THEN
            TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4  ; CALL REM
        END

****   NE R.NEW(LD.LOCAL.REF)<1,LDLR.REQ.AMEND.DATE> THEN
        R.NEW(LD.CHRG.CODE) = ""
        R.NEW(LD.CHRG.AMOUNT) = ""
        R.NEW(LD.CHRG.CODE)<1,1> = "11"
        R.NEW(LD.CHRG.CODE)<1,2> = "14"
    END

*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
    FOR  I = 1 TO NO.M
        R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
    NEXT I

    CALL REBUILD.SCREEN


    IF COMI # R.NEW(LD.CUSTOMER.ID) THEN
        ETEXT = "��� ����� ������� " ;CALL STORE.END.ERROR
    END
    CALL REBUILD.SCREEN
RETURN
END
