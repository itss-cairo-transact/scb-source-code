* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>27</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.LOAN.END.DATE

* IF THE FUNC IS INPUT THEN  DEFAULT  LD.INT.END.DATE ,LD.DD.END.DATE WITH THE VALUE OF FIELD LD.FIN.MAT.DATE
* (SEL.CATEG) CHOOSE CATEGORY ACCORDING TO NO. OF DAYS (THE DIFFERENCE BETWEEN FIN.MAT.DATE AND VALUE.DATE)
* (GENERATE.DDACCT) GENERATE THE [NT.LIQ.ACCT] FROM (CURRENCY OF LOAN & CUST.ID & (THE ABOVE SELECTION CATEGORY-17000))
* DISPLAY THE CUSTOMER'S BRANCH IN THE FIELD CUSTOMER.BRANCH

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN
            GOSUB INITIATE
            GOSUB SEL.CATEG
        END
    END
    GOSUB DEFAULT.DATES
    GOSUB GENERATE.DDACCT
    CALL REBUILD.SCREEN
    GOTO PROGRAM.END

*****************************************************************************************
INITIATE:

    DATE.1 = '' ; DATE.2 = '' ; DAYS = "C" ; DD = '' ; E1 = '' ; BRN.NAME = ''
    ETEXT ='' ;MESSAGE='' ;EXIST='' ; CUST =''  ;ID.CAT='' ;ID.CURR=''; BRN = '' ; DDACCT=''

    RETURN
*----------------------------------------------------------------------------------------
SEL.CATEG:

    DATE.1 = R.NEW (LD.VALUE.DATE) ; DATE.2 = COMI
    CALL CDD("",DATE.1,DATE.2,DAYS)
    TEXT=DAYS:'NO.DAYS';CALL REM
*-----------------------------------------------------------------------

    T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
    T.SEL = "SELECT F.SCB.LOANS.PARMS WITH MAX.DURATION >= " : DAYS : " AND  MIN.DURATION <= " : DAYS : " AND PERSON.CORPORATE = " : R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP>
*  T.SEL = "SELECT F.SCB.LOANS.PARMS WITH MAX.DURATION GE " : DAYS : " AND MIN.DURATION LE " : DAYS
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
    IF KEY.LIST THEN
        R.NEW(LD.CATEGORY) = KEY.LIST<SELECTED>
        TEXT=R.NEW(LD.CATEGORY);CALL REM
*   CALL REBUILD.SCREEN
    END    ELSE
        ETEXT ='There.Is.No.Appropriate.Categ'
        TEXT='WRONG';CALL REM
    END
*-----------------------------------------------------------------------

*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,R.NEW(LD.CATEGORY),CATEG.DESC)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,R.NEW(LD.CATEGORY),R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.DESC=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
    IF NOT (ETEXT) THEN
        T.ENRI<11> = CATEG.DESC
        COMI.ENRI<11> = CATEG.DESC
    END
    CALL REBUILD.SCREEN

    RETURN


*---------------------------------------------------------------------------------------
DEFAULT.DATES:

    R.NEW( LD.INT.END.DATE ) = COMI
    R.NEW( LD.D.D.END.DATE ) = COMI
    CALL REBUILD.SCREEN
    RETURN
*-----------------------------------------------------------------------------------------
GENERATE.DDACCT:



*******************UPDATED BY RIHAM R15*****************************
*Line [ 115 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,R.NEW(LD.CUSTOMER.ID),COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,R.NEW(LD.CUSTOMER.ID),R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")


*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

*  CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,R.NEW(LD.CUSTOMER.ID),
*  CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRN,BRN.NAME)

*************************************************************
 *   R.NEW(LD.LOCAL.REF)<1,LDLR.CUSTOMER.BRANCH> = BRN.NAME
    BRN = FMT( BRN, 'R%2')
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(LD.CURRENCY),ID.CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.NEW(LD.CURRENCY),R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
ID.CURR=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
    ID.CAT = R.NEW(LD.CATEGORY) - 17000
    CUST = FMT( R.NEW(LD.CUSTOMER.ID), 'R%8')

*    R.NEW(LD.DRAWDOWN.ACCOUNT)= CUST:ID.CURR:ID.CAT:'01'
    TEXT=R.NEW(LD.DRAWDOWN.ACCOUNT);CALL REM
    DDACCT = CUST:ID.CURR:ID.CAT:'01'

*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,DDACCT,TITEL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DDACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
TITEL=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
    IF NOT (ETEXT) THEN
        T.ENRI<12>    = TITEL
        COMI.ENRI<12> = TITEL
    END


    RETURN
*-----------------------------------------------------------------------------------------

PROGRAM.END:

    RETURN
END
