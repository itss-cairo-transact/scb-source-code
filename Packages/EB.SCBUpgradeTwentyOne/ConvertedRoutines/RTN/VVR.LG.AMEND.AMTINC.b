* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>594</Rating>
*-----------------------------------------------------------------------------
******** ABEER 12/3/2003**************
* A Routine To handle Margin Amount & Percentage Changes If LG Amount is Changed.

    SUBROUTINE VVR.LG.AMEND.AMTINC

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN

            OLD.LG.AMOUNT = R.NEW(LD.AMOUNT)
            CUSTM = R.NEW(LD.CUSTOMER.ID)
*CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
            OLD.MAT.DATE = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
            LGPERC = MYLOCAL<1,LDLR.MARGIN.PERC>/100
            LGMARG=MYLOCAL<1,LDLR.MARGIN.AMT>
            THIRD.NO = MYLOCAL<1,LDLR.THIRD.NUMBER>

            MYTYPE=MYLOCAL<1,LDLR.PRODUCT.TYPE>
            MARG.TYPE=MYLOCAL<1,LDLR.MARGIN.TYPE>
            MARG.CODE = FIELD(MARG.TYPE,'-',1)
************* GET ONLINE LIMIT **************************
            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
            KEY.ID = MARG.CODE:'-':CUSTM:'...'

            T.SEL = "SELECT F.SCB.LG.CUS WITH @ID LIKE ": KEY.ID
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
**************************************************
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,KEY.LIST<SELECTED>,LIMREF)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,KEY.LIST<SELECTED>,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LIMREF=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
            LIMPROD = FIELD(LIMREF,'.',2)
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIMREF,ONLIMIT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIMREF,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
ONLIMIT=R.ITSS.LIMIT<LI.AVAIL.AMT>

            MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            NEW.MAT.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            NEW.LG.AMOUNT = OLD.LG.AMOUNT + COMI
            NEW.MARG = COMI*LGPERC
            NEW.LG.MARG = LGMARG + NEW.MARG
*******************************************************************************
            IF COMI THEN
*******************************************************************************
                IF COMI > 0 THEN
                    IF COMI < ONLIMIT THEN
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.MARG
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                    END ELSE

                        NEW.MARG = COMI*1

                        NEW.LG.MARG = LGMARG + NEW.MARG

**  WW =NEW.LG.AMOUNT-ONLIMIT
** ZZ = LGPERC * ONLIMIT
** R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = WW+ZZ
** R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/NEW.LG.AMOUNT)*100
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = NEW.LG.MARG
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                    END
                    IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                        IF MYTYPE EQ 'ADVANCE' THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1271'
                        END ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1232'
                        END
                        R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                    END  ELSE
                        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1234'
                        R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>='CUS'
                    END
                END
                CALL REBUILD.SCREEN
            END
        END
*******************************
    END

    RETURN
END
