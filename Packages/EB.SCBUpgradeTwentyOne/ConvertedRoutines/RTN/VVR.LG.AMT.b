* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>1575</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMT

*A Routine To CALCULATE THE MARGIN.AMOUNT ACCORDING TO THE MARGIN.PER

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN

        IF COMI THEN
            IF COMI # R.NEW(LD.AMOUNT) THEN R.NEW(LD.CHRG.AMOUNT)=''

            CUSTM = R.NEW(LD.CUSTOMER.ID)
            IF CUSTM # R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER > THEN R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100

*************** NO MULTIVALUE ALLOWED ******************
*Line [ 46 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNTS1 =DCOUNT(R.NEW(LD.AMOUNT),@VM)
            IF COUNTS1 > 1 THEN
                FOR I = COUNTS1 TO 2 STEP -1
                    DEL R.NEW(LD.AMOUNT)<1,I>
                NEXT I
            END
*========================================================
            IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100 THEN
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = COMI
                R.NEW(LD.LIMIT.REFERENCE)=''
            END ELSE
                CUS = R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER >
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,CUS,LIMREF)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LIMREF=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
                LIMPROD = FIELD(LIMREF,'.',2)
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('LIMIT':@FM:LI.AVAIL.AMT,LIMREF,ONLIMIT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIMREF,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
ONLIMIT=R.ITSS.LIMIT<LI.AVAIL.AMT>
******************* GET MARGIN PERCENT *******************
****         CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.TYPE,CUSTM,LGTYPE)
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.TYPE,CUSTM,LGTYPE)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUSTM,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LGTYPE=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.TYPE>
                IF ETEXT THEN ETEXT =  ''
                IF LGTYPE THEN
                    MYKIND =R.NEW( LD.LOCAL.REF)<1,LDLR.LG.KIND>
                    TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
                    LOCATE TYPE IN LGTYPE<1,1> SETTING LLL ELSE LLL = 0
                    IF LLL > 0 THEN
****    CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.KIND,CUSTM,LGKIND)
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.KIND,CUS,LGKIND)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LGKIND=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.KIND>
                        IF LGKIND THEN

                            LOCATE MYKIND IN LGKIND<1,LLL,1> SETTING PPP  ELSE PPP = 0
*****       CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUSTM,PERC)
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUS,PERC)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
PERC=R.ITSS.SCB.LG.CUS<SCB.LGCS.MARGIN.PERC>
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.SPEC.PERC,CUS,SPEC.PERC)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
SPEC.PERC=R.ITSS.SCB.LG.CUS<SCB.LGCS.SPEC.PERC>
                            IF PPP > 0 THEN
                                IF  ONLIMIT > 0 THEN
                                    LGPERC =PERC<1,LLL,PPP>/100
                                    LG.SPEC.PERC=SPEC.PERC<1,LLL,PPP>/100
                                    IF LG.SPEC.PERC THEN
                                        LG.AMT=COMI
                                        YTEXT = "Use Special Rate "
*Line [ 86 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                                        CALL TXTINP(YTEXT, 8, 23, "1.1",  @FM:'Y_N')
                                        IF COMI EQ 'Y' THEN
                                            LGPERC = LG.SPEC.PERC
                                        END ELSE
                                            LGPERC = LGPERC
                                        END
                                       COMI = LG.AMT
                                    END
                                    IF COMI < ONLIMIT THEN
                                        R.NEW(LD.LIMIT.REFERENCE)=TRIM(LIMPROD,"0","L"):".01"
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = COMI*LGPERC
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/COMI)*100
                                    END ELSE
                                        R.NEW(LD.LIMIT.REFERENCE)=TRIM(LIMPROD,"0","L"):".01"
                                        WW =COMI-ONLIMIT
                                        ZZ = LGPERC * ONLIMIT
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = WW+ZZ
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/COMI)*100
                                    END
                                END  ELSE
                                    R.NEW(LD.LIMIT.REFERENCE)=''
                                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>  = COMI
                                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                    TEXT='Limit.Exceeded.Available.Amount';CALL REM
                                END
                            END
                        END
                        CALL REBUILD.SCREEN
                    END
                END
                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
