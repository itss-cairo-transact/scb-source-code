* @ValidationCode : Mjo4NDAyMzQ4NDQ6Q3AxMjUyOjE2NDIyOTgxODM5ODg6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 17:56:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1715</Rating>
*-----------------------------------------------------------------------------
*********** WAEL **************
*TO Default fields Margin Amount & Margin Perc According to Limit Reference Entered
SUBROUTINE VVR.LG.LIMIT.REF

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CUS

    IF MESSAGE NE 'VAL' THEN

*   IF INDEX(COMI,".",2) = "00" THEN
*      COMI =  FIELD(COMI,".",1):".01"
*  END
        IF COMI # R.NEW(LD.LIMIT.REFERENCE) THEN R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = ""

        DD = LEN(COMI)

        IF DD > 7 THEN
            LIM = COMI[2,LEN(COMI)]
            LIMY = LIM[1,4]
            MYPROD = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.LG.PARMS':@FM:SCB.LG.LIMIT.REF,MYPROD,LIM.REF)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,MYPROD,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
LIM.REF=R.ITSS.SCB.LG.PARMS<SCB.LG.LIMIT.REF>
*Line [ 52 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.FIELD = DCOUNT(LIM.REF,@VM)

* LOCATE LIMY IN LIM.REF<1,1> SETTING J ELSE ETEXT = "WRONG.LIMIT.REFERENCE"
        END

        IF COMI ='' THEN
            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> =''
            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = '100'
            CALL REBUILD.SCREEN
            IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
                AMT.INC = R.NEW(LD.AMOUNT.INCREASE)+R.NEW(LD.AMOUNT)
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = AMT.INC
            END ELSE
                IF R.NEW(LD.AMOUNT.INCREASE) < 0 THEN
                    AMT=R.NEW(LD.AMOUNT.INCREASE)
                    DEC=ABS(AMT)
                    AMT.DEC = R.NEW(LD.AMOUNT) - DEC
                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = AMT.DEC
                END
                IF R.NEW(LD.AMOUNT.INCREASE) EQ 0 THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)
                END

            END
        END ELSE
            CUS = R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER >
* CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,CUS,LIMREF)

            LIMPROD = FIELD(LIMREF,'.',2)
* LIMPROD= TRIM(LIMPROD,"0","L"):'.01'

            IF LIMPROD NE COMI THEN
                R.NEW(LD.LIMIT.REFERENCE)=''
*ETEXT='WRONG LIMIT REFERENCE'

                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)
                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> =100

                CALL REBUILD.SCREEN
            END ELSE
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.TYPE,CUS,LGTYPE)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LGTYPE=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.TYPE>
                IF ETEXT THEN ETEXT =  ''
                IF LGTYPE THEN

                    MYKIND =R.NEW( LD.LOCAL.REF)<1,LDLR.LG.KIND >
                    TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
                    LOCATE TYPE IN LGTYPE<1,1> SETTING LLL ELSE LLL = 0
                    IF LLL > 0 THEN
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.KIND,CUS,LGKIND)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LGKIND=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.KIND>
                        IF LGKIND THEN
                            LOCATE MYKIND IN LGKIND<1,LLL,1> SETTING PPP ELSE PPP = 0
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUS,PERC)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUS,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
PERC=R.ITSS.SCB.LG.CUS<SCB.LGCS.MARGIN.PERC>
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR ('LIMIT':@FM:LI.AVAIL.AMT,LIMREF,ONLIMIT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIMREF,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
ONLIMIT=R.ITSS.LIMIT<LI.AVAIL.AMT>
                            IF PPP = 0 THEN
                                TEXT='Kind.Not.Available';CALL REM
                                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)
                                R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                COMI=''
                            END ELSE


                                IF  ONLIMIT > 0 THEN
                                    LGPERC =PERC<1,LLL,PPP>/100
                                    IF R.NEW(LD.AMOUNT)< ONLIMIT THEN

                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)*LGPERC
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = (R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>/R.NEW(LD.AMOUNT))*100
                                    END ELSE
*                                        WW =R.NEW(LD.AMOUNT)-ONLIMIT
*                                        ZZ = LGPERC * ONLIMIT

                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)
                                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                                        TEXT='Limit.Exceeded.Available.Amount';CALL REM

                                    END
                                END ELSE
                                    R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>  =R.NEW(LD.AMOUNT)
                                    R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>  = 100
                                    COMI=''
                                    TEXT='Limit.Exceeded.Available.Amount';CALL REM
                                    CALL REBUILD.SCREEN
                                END
                            END

                        END ELSE

                            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>  =R.NEW(LD.AMOUNT)
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>  = 100
                            COMI=''
                            TEXT='Kind.Not.Available';CALL REM
                        END
                    END ELSE
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>  = R.NEW(LD.AMOUNT)
                        R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100
                        COMI=''
                        TEXT='Type.Not.Available';CALL REM
                    END
                END ELSE
                END
            END
        END
        CALL REBUILD.SCREEN
    END
RETURN
END
