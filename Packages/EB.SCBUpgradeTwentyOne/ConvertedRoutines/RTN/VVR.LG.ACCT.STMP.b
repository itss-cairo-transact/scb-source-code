* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*------------------------------------------------------------
* <Rating>50</Rating>
*------------------------------------------------------------
    SUBROUTINE VVR.LG.ACCT.STMP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> NE '1301' AND R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> NE '1309' AND R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> NE '1304' AND R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> NE '1305' THEN
        AC.ID = COMI
        FN.ACC = 'FBNK.ACCOUNT'; F.ACC = ''
        CALL OPF(FN.ACC,F.ACC)
        CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
        ACCT.CUR      =  R.ACC<AC.CURRENCY>
        ACCT.CUST     =  R.ACC<AC.CUSTOMER>


        LG.CUR  = R.NEW(LD.CURRENCY)
        LG.CUST = R.NEW(LD.CUSTOMER.ID)



        IF COMI THEN
            IF LG.CUR EQ 'EGP' THEN
                ETEXT = 'NOT ALLOWED TO ENTER IN THIS FEILD WITH EGP';CALL STORE.END.ERROR
            END ELSE
                IF ACCT.CUR NE 'EGP' THEN
                    ETEXT='EGP ONLY ALLWOED';CALL STORE.END.ERROR
                END ELSE
                    IF ACCT.CUST NE LG.CUST THEN
                        ETEXT ='CUSTOMER MUST BE EQUAL LG CUSTOMER';CALL STORE.END.ERROR
                    END
                END
            END
        END  ELSE
            IF LG.CUR NE 'EGP' THEN
                ETEXT= 'MUST ENTER ACCOUNT';CALL STORE.END.ERROR
            END
        END
    END
    RETURN
END
