* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************

    SUBROUTINE VVR.LG.INW.CONFIS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN

        R.NEW(LD.AMT.V.DATE)= TODAY
        R.NEW(LD.AMOUNT.INCREASE)= "-":COMI

        IF COMI = R.OLD(LD.AMOUNT) THEN
*  R.NEW(LD.FIN.MAT.DATE) = TODAY
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "2401"
        END

        IF COMI < R.OLD(LD.AMOUNT) THEN
            IF R.NEW(LD.FIN.MAT.DATE) = TODAY THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "2402"
            END ELSE
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "2403"
            END
        END

        IF COMI > R.OLD(LD.AMOUNT) THEN
            ETEXT = "MUST.BE.LT.OR.EQ.LG.AMT"
        END

    END
    CALL REBUILD.SCREEN


    IF MESSAGE EQ 'VAL' THEN
        R.NEW(LD.TRANCHE.AMT) = R.NEW(LD.AMOUNT)
        IF R.NEW(LD.AMOUNT.INCREASE) THEN
            R.NEW(LD.TRANCHE.AMT) =R.NEW(LD.AMOUNT)+R.NEW(LD.AMOUNT.INCREASE)
        END
    END

    RETURN
END
