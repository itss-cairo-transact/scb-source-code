* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
********* RANIA  11/06/2003 **************
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.NON

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI LE 0 THEN ETEXT='Must.Be.Positive.Amt'
**   IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
    R.NEW(LD.CHRG.CODE)<1,1> = 11
    R.NEW(LD.CHRG.CODE)<1,2> = 14
    R.NEW(LD.CHRG.CODE)<1,3> = 18

    IF MESSAGE EQ 'VAL' THEN
        R.NEW(LD.TRANCHE.AMT) = R.NEW(LD.AMOUNT)
        IF R.NEW(LD.AMOUNT.INCREASE) THEN
            R.NEW(LD.TRANCHE.AMT) =R.NEW(LD.AMOUNT)+R.NEW(LD.AMOUNT.INCREASE)
        END
    END

    RETURN
END
