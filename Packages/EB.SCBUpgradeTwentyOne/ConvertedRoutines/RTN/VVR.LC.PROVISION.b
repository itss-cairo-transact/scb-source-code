* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 07/07/2003***********

    SUBROUTINE VVR.LC.PROVISION
* A ROUTINE TO DEFAULT THE FIRST MULTIVALUE WITH THE CUSTOMER NAME

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

*TEXT = 'YARAB2' ; CALL REM
    APPL.ACCT= R.NEW(TF.LC.APPLICANT.ACC)
    ACCT.NO  = R.NEW(TF.LC.APPLICANT.ACC)[1,10]
    SERIAL   = R.NEW(TF.LC.APPLICANT.ACC)[15,2]
*****02/01/2007 NESSREEN AHMED*****
    IF MESSAGE = '' THEN
        TEXT = 'WHY' ; CALL REM
*********************************
        IF PGM.VERSION # ",SCB.LIP" AND COMI = 'Y' THEN
*TEXT = 'DALIA' ; CALL REM
            R.NEW(TF.LC.PROVIS.ACC) = APPL.ACCT
**** 25/12/2006 NESSREEN AHMED****
** R.NEW(TF.LC.CREDIT.PROVIS.ACC) = ACCT.NO:'1076':SERIAL
            R.NEW(TF.LC.CREDIT.PROVIS.ACC) = ACCT.NO:'3010':SERIAL
**********************************
        END ELSE
            R.NEW(TF.LC.PROVIS.ACC) = ''
            R.NEW(TF.LC.CREDIT.PROVIS.ACC) = ''
            R.NEW(TF.LC.PRO.OUT.AMOUNT) = ''
            R.NEW(TF.LC.PROVIS.AMOUNT) = ''
            R.NEW(TF.LC.PROVIS.PERCENT) = ''
            R.NEW(TF.LC.DB.PROV.AMOUNT) = ''
            R.NEW(TF.LC.PROV.EXCH.RATE) = ''
        END
    END
    RETURN
END
