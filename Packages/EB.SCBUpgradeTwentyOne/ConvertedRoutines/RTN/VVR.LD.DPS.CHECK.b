* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LD.DPS.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION


      IF MESSAGE = 'VAL' THEN
*----------------------------------------------------------------------------------------
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*     CALL DBR('LD.TXN.TYPE.CONDITION':@FM:LTTC.MIN.INIT.AMT,R.NEW(LD.CATEGORY),AMT.MN)
F.ITSS.LD.TXN.TYPE.CONDITION = 'FBNK.LD.TXN.TYPE.CONDITION'
FN.F.ITSS.LD.TXN.TYPE.CONDITION = ''
CALL OPF(F.ITSS.LD.TXN.TYPE.CONDITION,FN.F.ITSS.LD.TXN.TYPE.CONDITION)
CALL F.READ(F.ITSS.LD.TXN.TYPE.CONDITION,R.NEW(LD.CATEGORY),R.ITSS.LD.TXN.TYPE.CONDITION,FN.F.ITSS.LD.TXN.TYPE.CONDITION,ERROR.LD.TXN.TYPE.CONDITION)
AMT.MN=R.ITSS.LD.TXN.TYPE.CONDITION<LTTC.MIN.INIT.AMT>
     IF NOT(ETEXT)THEN
        IF R.NEW(LD.AMOUNT) < AMT.MN THEN TEXT = "THE.AMOUNT.IS.LESS.THAN" ; CALL OVE
     END
     *CALL ERR ; MESSAGE = 'REPEAT'
END
 
RETURN
END
