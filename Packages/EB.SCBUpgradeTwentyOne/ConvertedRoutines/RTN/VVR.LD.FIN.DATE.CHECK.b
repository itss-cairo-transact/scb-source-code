* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
** ----- WAEL  -----

SUBROUTINE VVR.LD.FIN.DATE.CHECK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PARMS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DPST.TYPES


IF COMI THEN

     DATE.1 = '' ; DATE.2 = '' ; DAYS = "C"
     DATE.1 = R.NEW (LD.VALUE.DATE)
     DATE.2 = COMI

     CALL CDD("",DATE.1,DATE.2,DAYS)
     CALL REBUILD.SCREEN
    * R.NEW (LD.BK.TO.BK.INFO) = DAYS


AA = ''
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*CALL DBR ('SCB.DPST.TYPES':@FM:SCB.DPST.TYPES.CATEGORY,DAYS,AA)
F.ITSS.SCB.DPST.TYPES = 'F.SCB.DPST.TYPES'
FN.F.ITSS.SCB.DPST.TYPES = ''
CALL OPF(F.ITSS.SCB.DPST.TYPES,FN.F.ITSS.SCB.DPST.TYPES)
CALL F.READ(F.ITSS.SCB.DPST.TYPES,DAYS,R.ITSS.SCB.DPST.TYPES,FN.F.ITSS.SCB.DPST.TYPES,ERROR.SCB.DPST.TYPES)
AA=R.ITSS.SCB.DPST.TYPES<SCB.DPST.TYPES.CATEGORY>

   IF NOT (ETEXT) THEN
     CALL REBUILD.SCREEN
     R.NEW(LD.CATEGORY) = AA
   END



END



RETURN
END
