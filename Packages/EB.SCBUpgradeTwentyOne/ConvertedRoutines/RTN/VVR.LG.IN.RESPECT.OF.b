* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
************ ABEER **************
*-----------------------------------------------------------------------------
* <Rating>43</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.IN.RESPECT.OF

*A Routine To ALLOW ONLY  "6" MULTIVALUES-FIELDS

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


        IF COMI EQ '' THEN
            ETEXT='��� ����� ����� �� ������'
        END ELSE
            IF COMI THEN
*            COUNTS1 =DCOUNT(R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.RESPECT.OF>,SM)
*    IF COUNTS1 > 3 THEN
*        FOR I = COUNTS1 TO 4 STEP -1
*            DEL R.NEW( LD.LOCAL.REF)< 1,LDLR.IN.RESPECT.OF,I>
*        NEXT I
*    END
*    CALL REBUILD.SCREEN
***********************
*--------------- TAX  ----------------
                MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
                MYTYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
                MYID = MYCODE:'.':MYTYPE

                FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
                CALL OPF(FN.LG,F.LG)
                CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)
*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TAX.CONT = DCOUNT(R.LG<SCB.LG.CH.TAX.AMOUNT>, @VM)
                IF TAX.CONT >= 1 THEN
                    FOR I = 1 TO TAX.CONT
                        R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES,I> = R.LG<SCB.LG.CH.TAX.AMOUNT,I>
                    NEXT I
                END

            END
        END
********************************************************

    RETURN
END
