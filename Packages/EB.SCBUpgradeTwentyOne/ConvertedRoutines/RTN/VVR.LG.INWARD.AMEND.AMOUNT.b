* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1035</Rating>
*-----------------------------------------------------------------------------
******** ABEER 12/3/2003**************

* A Routine To handle Margin Amount & Percentage Changes If LG Amount is Changed.

    SUBROUTINE VVR.LG.INWARD.AMEND.AMOUNT

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
*           IF R.NEW(LD.CHRG.CODE)='' THEN R.NEW(LD.CHRG.CODE)='4'
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>='' THEN R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>='5'
*R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
*R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
            OLD.LG.AMOUNT=R.NEW(LD.AMOUNT)
            OLD.CUSTOMER.ID=R.NEW(LD.CUSTOMER.ID)
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,OLD.MAT.DATE)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
OLD.MAT.DATE=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.FIN.MAT.DATE>
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
*            LGPERC=MYLOCAL<1,LDLR.MARGIN.PERC>/100
*            THIRD.NO=MYLOCAL<1,LDLR.THIRD.NUMBER>
*            COUNT.CODE=DCOUNT(R.NEW(LD.CHRG.CODE),VM)
*            COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
*Line [ 52 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE=DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>,@SM)
*Line [ 54 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT=DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>,@SM)
************* GET ONLINE LIMIT **************************
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,OLD.CUSTOMER.ID,LIMREF)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,OLD.CUSTOMER.ID,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LIMREF=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
            LIMPROD = FIELD(LIMREF,'.',2)
       ***     R.NEW(LD.LIMIT.REFERENCE)= TRIM(LIMPROD,"0","L")
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LIMIT':@FM:LI.ONLINE.LIMIT,LIMREF,ONLIMIT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIMREF,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
ONLIMIT=R.ITSS.LIMIT<LI.ONLINE.LIMIT>

            MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            NEW.MAT.DATE=R.NEW(LD.FIN.MAT.DATE)
            NEW.LG.AMOUNT= OLD.LG.AMOUNT + COMI

*******************************************************************************
            IF COMI THEN
*                R.NEW(LD.CHRG.AMOUNT)=''
                R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>=''
                FOR I = 2 TO COUNT.AMT
*IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
                    IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>='' THEN
                        DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
                        DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
                    END
                NEXT I
                IF OLD.CUSTOMER.ID  THEN
                    GOSUB CHECK.CODE
********************************************************************************
CHECK.CODE:
                    IF COMI > 0 THEN
                        IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2232'
                           * R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2232'
                        END ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2234'
                          *  R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2234'
                        END
                    END ELSE
                        IF ABS(COMI) > OLD.LG.AMOUNT OR ABS(COMI) EQ OLD.LG.AMOUNT THEN ETEXT='This.Amount.Must.Be.Less.Than.LGAmount'
                        IF OLD.MAT.DATE EQ NEW.MAT.DATE THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2233'
                           * R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2233'
                        END ELSE
                            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2235'
                           * R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2235'
                        END

                    END
                    RETURN
********************************************************************************************
                END
            END ELSE
                IF OLD.MAT.DATE # NEW.MAT.DATE THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='2231'
                   * R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>='2231'
                END ELSE
                    ETEXT='You.Must.Amend.Amount.OR.Date'
                END
            END

        END
*************************************************************************************

    END
    RETURN
END
