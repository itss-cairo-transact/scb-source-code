* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 10/04/2003***********

SUBROUTINE VVR.LC.LG.REF
* A ROUTINE TO DEFAULT THE APPLICANT WITH THE RELEVANT FIELDS

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

FN.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER.OF.CREDIT = '' ; R.LETTER.OF.CREDIT = ''
IF V$FUNCTION='I' THEN
  IF COMI THEN
     CALL OPF( FN.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT)
     CALL F.READ(FN.LETTER.OF.CREDIT,COMI, R.LETTER.OF.CREDIT, F.LETTER.OF.CREDIT, ETEXT)
     R.NEW(TF.LC.APPLICANT.CUSTNO) = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.CUSTNO>
     R.NEW(TF.LC.APPLICANT)        = R.LETTER.OF.CREDIT<TF.LC.APPLICANT>
     R.NEW(TF.LC.APPLICANT.ACC)    = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.ACC>
     CLOSE  F.LETTER.OF.CREDIT
  END
END

RETURN
END
