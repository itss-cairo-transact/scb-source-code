* @ValidationCode : MjotMTYzNDE4MTIyNjpDcDEyNTI6MTY0NTE1NzA1NTI2MTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 20:04:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>93</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 28/08/2003***********

SUBROUTINE VVR.LC.REIMBURSE.BK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG

*  BK = ",SCB.BANK"
    IF COMI THEN

*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,SEC)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.VER.IND.SEC.LEG':@FM:VISL.SECTOR,",SCB.BANK",SS)
        F.ITSS.SCB.VER.IND.SEC.LEG = 'F.SCB.VER.IND.SEC.LEG'
        FN.F.ITSS.SCB.VER.IND.SEC.LEG = ''
        CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG)
        CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG,SS,R.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG,ERROR.SCB.VER.IND.SEC.LEG)
        SS=R.ITSS.SCB.VER.IND.SEC.LEG<VISL.SECTOR,"SCB.BANK">
* TEXT = SEC ; CALL REM
* TEXT = SS ; CALL REM
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.VER.IND.SEC.LEG':@FM:VISL.INDUSTRY,",SCB.BANK",INDUS)
        F.ITSS.SCB.VER.IND.SEC.LEG = 'F.SCB.VER.IND.SEC.LEG'
        FN.F.ITSS.SCB.VER.IND.SEC.LEG = ''
        CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG)
        CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG,INDUS,R.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG,ERROR.SCB.VER.IND.SEC.LEG)
        INDUS=R.ITSS.SCB.VER.IND.SEC.LEG<VISL.INDUSTRY,"SCB.BANK">
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CNT = DCOUNT(INDUS,@VM)
* TEXT = CNT; CALL REM
        FOR I = 1 TO CNT
            LOCATE SEC IN SS<1,I,1> SETTING POS THEN
                YYY = 'YES'
            END
        NEXT I
        IF NOT(YYY) THEN ETEXT = "CUSTOMER.MUST.BANK"
*ELSE ETEXT = "CUSTOMER.MUST.BANK"
* TEXT = "POS= ":SS<1,POS>:" ":POS  ; CALL REM
*  ETEXT = "CUSTOMER.MUST.BANK"
* END
    END
RETURN
END
