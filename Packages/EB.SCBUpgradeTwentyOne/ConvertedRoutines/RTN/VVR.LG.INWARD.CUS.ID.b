* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** ABEER 11/05/2003**********
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.INWARD.CUS.ID

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS


    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,MYSECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
MYSECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
            IF  MYSECTOR LE 3000  OR MYSECTOR GT 4000 THEN
                ETEXT='Must.Be.Bank'
            END ELSE
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,COMI,LIMREF)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,COMI,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LIMREF=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
                IF NOT(ETEXT) THEN
                    LIMPROD = FIELD(LIMREF,'.',2)
                    R.NEW(LD.LIMIT.REFERENCE)= TRIM(LIMPROD,"0","L")
                    R.NEW(LD.LOCAL.REF)<1,LDLR.SEN.REC.BANK>=COMI
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,COMI,MYNAME)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
MYNAME=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>
                END ELSE
*  ETEXT='This.Bank.Has.No.Limit'
                    ETEXT=''
                END
                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
