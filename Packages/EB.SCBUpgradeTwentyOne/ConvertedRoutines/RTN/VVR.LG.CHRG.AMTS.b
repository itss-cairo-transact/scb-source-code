* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CHRG.AMTS


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

**    IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> NE '1309' THEN
    R.NEW(LD.CHRG.CODE) = '14'
**    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
**  W.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
**W.OPER.NO= R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':MYLOCAL<1,LDLR.PRODUCT.TYPE>
**FN.LG.CHARGE ='F.SCB.LG.CHARGE' ; R.OPER = '' ; F.LG.CHARGE = ''
**CALL OPF(FN.LG.CHARGE,F.LG.CHARGE)
**CALL F.READ(FN.LG.CHARGE,W.OPER.NO,R.OPER,F.LG.CHARGE,E1)

**LOCATE W.CUR IN R.OPER<SCB.LG.CH.CURRENCY,1> SETTING JJ ELSE ETEXT='NOT.FOUND'
**IF  R.OPER<SCB.LG.CH.CHARGE.CODE,JJ,2> EQ '14' THEN
**  COMI = R.OPER<SCB.LG.CH.MIN.AMOUNT,JJ,2>
**TEXT = 'TEST';CALL REM
**END
**END
**    CALL REBUILD.SCREEN
    RETURN
END
