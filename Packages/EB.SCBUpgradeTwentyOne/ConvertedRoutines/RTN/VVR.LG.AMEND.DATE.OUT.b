* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>607</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.AMEND.DATE.OUT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE

    ETEXT = ''
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
LOCAL.REF=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
    OLD.EXP.DAT = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    LGKIND      = LOCAL.REF<1,LDLR.LG.KIND>

    IF COMI EQ OLD.EXP.DAT THEN
        ETEXT='It.Must.Be.CHANGE';CALL STORE.END.ERROR
    END



***    IF V$FUNCTION = 'I' THEN
***     IF MESSAGE EQ '' THEN
    IF COMI THEN
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,LOCAL.REF)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
LOCAL.REF=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
        OLD.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
****************************************************************************************
***    IF COMI THEN
        IF COMI LT OLD.MAT.DATE  THEN
            ETEXT='It.Must.Be.Greater.Than.Previuos.Maturity.Date'
        END
        IF COMI GT OLD.MAT.DATE  THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> ='1231'
        END
***     END
***CALL REBUILD.SCREEN

***************HHHHHH*************************


        BEGIN CASE
        CASE R.NEW(LD.CUSTOMER.ID) EQ '31300011'
            R.NEW(LD.CHRG.CODE)<1,1> = 14
        CASE OTHERWISE
**********
************************MODIFIED BY ABEER AS OF 21-5-2020
            FN.COM= 'F.SCB.LG.FREE.CHARGE' ; F.COM = ''
            CALL OPF(FN.COM,F.COM)
            CUS.ID=R.NEW(LD.CUSTOMER.ID)
            CALL F.READ(FN.COM,CUS.ID,R.COM,F.COM,E11)
**********
            IF NOT(E11) THEN
                IF R.COM<LG.FR.EXP.DAT> GE TODAY  THEN
                    R.NEW(LD.CHRG.CODE)   = ""
                    R.NEW(LD.CHRG.AMOUNT) = ""
                END ELSE
                    ETEXT='Expiried Date For the Customer Free Charge ';CALL STORE.END.ERROR
                END
***********END OF MODIFICATION 21-2-2020
            END ELSE
                COMM=FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>,'-',1)
                IF COMM EQ 'YES' THEN

                    R.NEW(LD.CHRG.CODE)<1,1> = 13
                    R.NEW(LD.CHRG.CODE)<1,2> = 14
                    R.NEW(LD.CHRG.CODE)<1,3> = 18
****CALL REBUILD.SCREEN
                END ELSE
                    IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> LE R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> THEN
                        R.NEW(LD.CHRG.CODE)<1,1> = 19
                        R.NEW(LD.CHRG.CODE)<1,2> = 14
                        R.NEW(LD.CHRG.CODE)<1,3> = 18
                    END ELSE
                        IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
                            R.NEW(LD.CHRG.CODE)<1,1> = 4
                            R.NEW(LD.CHRG.CODE)<1,2> = 14
                            R.NEW(LD.CHRG.CODE)<1,3> = 18
                        END
                        IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
                            R.NEW(LD.CHRG.CODE)<1,1> = 7
                            R.NEW(LD.CHRG.CODE)<1,2> = 14
                            R.NEW(LD.CHRG.CODE)<1,3> = 18
                        END
                        IF R.NEW (LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
                            R.NEW(LD.CHRG.CODE)<1,1> = 9
                            R.NEW(LD.CHRG.CODE)<1,2> = 14
                            R.NEW(LD.CHRG.CODE)<1,3> = 18
                        END

                    END
                END
            END
        END CASE

*Line [ 118 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
        FOR  I = 1 TO NO.M
            R.NEW(LD.CHRG.CLAIM.DATE)<1,I> = TODAY
        NEXT I

    END
    RETURN
END
