* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
********* WAEL **************
*-----------------------------------------------------------------------------
* <Rating>705</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CANCEL.9090

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE


*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
    MAT.EXP=MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>
    MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>
    ADV.OPER=MYLOCAL<1,LDLR.ADV.OPERATIVE>

*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,LG.FIN.MAT)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
LG.FIN.MAT=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.FIN.MAT.DATE>
***********************
    FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LMM= 'FBNK.LMM.CHARGE.CONDITIONS'; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)
    FN.CHR= 'FBNK.FT.CHARGE.TYPE' ; F.CHR = ''
    CALL OPF(FN.CHR,F.CHR)
    FN.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
    W.CUR =  R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
    CATG = R.ACC<AC.CATEGORY>

**********************
***********************
    IF COMI = "EXPIRED" THEN
        LG.REN =  R.NEW(LD.LOCAL.REF)<1,LDLR.EARLY.LIQ.Y.N>
        IF LG.REN NE 'YES' THEN
            IF  TODAY LT MAT.EXP THEN
                ETEXT = 'Not Expired Yet' ; CALL STORE.END.ERROR
            END ELSE
                IF MYCODE EQ "1309" THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1301"
                    R.NEW(LD.FIN.MAT.DATE) = TODAY
                    R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
***************

                    IF CATG EQ '9090' OR CATG EQ '9091' THEN
                        R.NEW(LD.CHRG.CODE) = ''
                        R.NEW(LD.CHRG.CLAIM.DATE)=''
                        R.NEW(LD.CHRG.AMOUNT)  = ''

*Line [ 80 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''

                        FOR I = 2 TO COUNT.CODE
                            IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                                DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                                DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
                            END
                        NEXT I
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = '14'
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,R.LMM,F.LMM,EE2)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>

                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING JJJ ELSE ETEXT = 'ERROR IN CUR3'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,JJJ>
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = CH.AMT
                    END ELSE
                        R.NEW(LD.CHRG.CODE)<1,1> = "14"
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,1>= TODAY
                    END
**********************
                END ELSE
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>  = "1309"
                    R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = MAT.EXP
                    R.NEW(LD.FIN.MAT.DATE)                      = LG.FIN.MAT

*************
                    IF CATG EQ '9090' OR CATG EQ '9091' THEN
                        R.NEW(LD.CHRG.CODE) = ''
                        R.NEW(LD.CHRG.CLAIM.DATE)=''
                        R.NEW(LD.CHRG.AMOUNT)  = ''

*Line [ 115 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''

                        FOR I = 2 TO COUNT.CODE
                            IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                                DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                                DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
                            END
                        NEXT I
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = '14'
                        CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,R.LMM,F.LMM,EE2)
                        CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>

                        CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
                        LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING JJJ ELSE ETEXT = 'ERROR IN CUR3'
                        CH.AMT = R.CHR<FT5.FLAT.AMT,JJJ>
                        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = CH.AMT
                    END ELSE
                        R.NEW(LD.CHRG.CODE)<1,1> = "14"
                        R.NEW(LD.CHRG.CLAIM.DATE)<1,1>= TODAY
                    END
*********************
                END
            END
******MODIFICATION FOR NEW FEILED
        END ELSE
            IF  LG.REN  EQ 'YES' THEN
                ETEXT = 'Not Allowed TO Cancel';CALL STORE.END.ERROR
            END
        END
    END
    IF COMI ="BENF." THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1303"
        R.NEW(LD.FIN.MAT.DATE) = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
        R.NEW(LD.CHRG.CODE) = ''
        R.NEW(LD.CHRG.CLAIM.DATE) = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''
    END
    IF COMI ="CUSTOMER" THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1302"
        R.NEW(LD.FIN.MAT.DATE) = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
***
        IF CATG EQ '9090' OR CATG EQ '9091' THEN
            R.NEW(LD.CHRG.CODE) = ''
            R.NEW(LD.CHRG.CLAIM.DATE)=''
            R.NEW(LD.CHRG.AMOUNT)  = ''

*Line [ 167 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''

            FOR I = 2 TO COUNT.CODE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                    DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
                END
            NEXT I
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = '14'
            CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,R.LMM,F.LMM,EE2)
            CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>

            CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
            LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING JJJ ELSE ETEXT = 'ERROR IN CUR3'
            CH.AMT = R.CHR<FT5.FLAT.AMT,JJJ>
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>  = CH.AMT
        END ELSE
            R.NEW(LD.CHRG.CODE)<1,1> = "14"
            R.NEW(LD.CHRG.CLAIM.DATE)<1,1>= TODAY
        END
***
    END
    IF COMI EQ 'NO.CHEQ' THEN

        IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'ADVANCE' THEN
            IF MYCODE NE '1111' AND MYCODE NE '1271'  AND MYCODE NE '1281'  AND MYCODE NE '1282' THEN
                ETEXT = 'Not Allowed';CALL STORE.END.ERROR
            END
            IF MYCODE EQ '1111' OR MYCODE EQ '1281' OR MYCODE EQ '1282' THEN
                IF ADV.OPER EQ '' THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1304'
                    R.NEW(LD.FIN.MAT.DATE) = TODAY
                    R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
                END ELSE
                    IF ADV.OPER EQ 'YES' THEN
                        ETEXT='ALREADY OPERATED';CALL STORE.END.ERROR
                    END
                END
            END
            IF MYCODE EQ '1271' OR MYCODE EQ '1283' OR MYCODE EQ '1284' THEN
*Line [ 222 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT.INCREASE,ID.NEW,MYINC)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYINC=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.AMOUNT.INCREASE>
                R.NEW(LD.AMOUNT.INCREASE) = MYINC*-1
                R.NEW(LD.AMT.V.DATE) = TODAY
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1305'
                R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> =MAT.EXP
                R.NEW(LD.FIN.MAT.DATE) = LG.FIN.MAT
            END
        END ELSE
            ETEXT='Only Advance IS Allowed';CALL STORE.END.ERROR
        END
    END

    CALL REBUILD.SCREEN
    RETURN
END
