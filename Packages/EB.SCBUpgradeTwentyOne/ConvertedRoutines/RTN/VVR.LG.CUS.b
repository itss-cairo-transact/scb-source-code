* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>750</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.CUS

*A Routine To GET THE MARGIN.PER FROM TABLE "SCB.LG.CUS" ACCORGING TO THE THE CUST.NO & THE LG.KIND

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS

*================================
    IF COMI EQ 'FG' THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,1> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,2> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,3> = ''
        R.NEW(LD.CHRG.CODE) = ''
    END

    IF MESSAGE NE 'VAL' THEN

        IF COMI THEN

*---------------------------
            MYID=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            MYTYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.LG.PARMS':@FM:SCB.LGP.LG.KIND,MYTYPE,MYKIND)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,MYTYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
MYKIND=R.ITSS.SCB.LG.PARMS<SCB.LGP.LG.KIND>
            LOCATE COMI IN MYKIND<1,1> SETTING J  ELSE ETEXT='This.LG.Kind.Not.For.' : MYTYPE ; CALL STORE.END.ERROR
*----------------------------

            CUST = R.NEW(LD.CUSTOMER.ID)
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.TYPE,CUST,LGTYPE)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUST,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LGTYPE=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.TYPE>
            IF ETEXT THEN ETEXT =  ''
            IF LGTYPE THEN
                TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
                LOCATE TYPE IN LGTYPE<1,1> SETTING LLL ELSE LLL = 0
                IF LLL > 0 THEN
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.LG.KIND,CUST,LGKIND)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUST,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
LGKIND=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.KIND>
                    IF LGKIND THEN
                        LOCATE COMI IN LGKIND<1,LLL,1> SETTING PPP ELSE PPP = 0
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUST,PERC)
F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
FN.F.ITSS.SCB.LG.CUS = ''
CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
CALL F.READ(F.ITSS.SCB.LG.CUS,CUST,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
PERC=R.ITSS.SCB.LG.CUS<SCB.LGCS.MARGIN.PERC>
                        IF PPP = 0 THEN
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=100
                            TEXT='LG KIND NOT AVAILABLE';CALL REM
                        END ELSE
                            LGPERC =PERC<1,LLL,PPP>
                            R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>=LGPERC
                        END
                    END
                END ELSE
                    TEXT='LG TYPE NOT AVAILABLE';CALL REM
                    R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>  =100
                END
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
