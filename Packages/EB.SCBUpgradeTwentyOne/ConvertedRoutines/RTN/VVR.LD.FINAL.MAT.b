* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LD.FINAL.MAT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE

*Line [ 30 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.SCHEDULE.DEFINE':@FM:LD.SD.DATE,ID.NEW,DAT.SCD)
F.ITSS.LD.SCHEDULE.DEFINE = 'FBNK.LD.SCHEDULE.DEFINE'
FN.F.ITSS.LD.SCHEDULE.DEFINE = ''
CALL OPF(F.ITSS.LD.SCHEDULE.DEFINE,FN.F.ITSS.LD.SCHEDULE.DEFINE)
CALL F.READ(F.ITSS.LD.SCHEDULE.DEFINE,ID.NEW,R.ITSS.LD.SCHEDULE.DEFINE,FN.F.ITSS.LD.SCHEDULE.DEFINE,ERROR.LD.SCHEDULE.DEFINE)
DAT.SCD=R.ITSS.LD.SCHEDULE.DEFINE<LD.SD.DATE>

    IF COMI LE DAT.SCD THEN

        R.NEW(LD.AUTO.SCHEDS)   = "YES"
        R.NEW(LD.DEFINE.SCHEDS) = "NO"

        CALL REBUILD.SCREEN

    END ELSE

        R.NEW(LD.AUTO.SCHEDS)   = "NO"
        R.NEW(LD.DEFINE.SCHEDS) = "YES"

        CALL REBUILD.SCREEN
    END


    RETURN
END
