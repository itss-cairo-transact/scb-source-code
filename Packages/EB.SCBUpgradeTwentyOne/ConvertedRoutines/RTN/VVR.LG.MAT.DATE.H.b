* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>489</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************
*TO CHECK IF ACTUAL EXP DATE LT TODAY ERROR MESSAGE WILL BE DISPLAYED

    SUBROUTINE VVR.LG.MAT.DATE.H

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

******************HHH*****20081113****


****  R.NEW(LD.CHRG.AMOUNT) = ""

*******************HHHHHHHH************

    DEFFUN SHIFT.DATE( )
    IF COMI LE TODAY THEN  ETEXT = 'MUST.BE.GR.OR.EQ.TODAY'  ; CALL  STORE.END.ERROR
    IF COMI # R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> THEN
***   IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> LE TODAY THEN  ETEXT = 'MUST.BE.GR.OR.EQ.TODAY'  ; CALL  STORE.END.ERROR
    END
    IF COMI # R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> THEN
        IF MESSAGE NE 'VAL' THEN
            CUST.ID = R.NEW(LD.CUSTOMER.ID)
            LIM = FMT(R.NEW(LD.LIMIT.REFERENCE),"R%10")
            ID.LIMIT = CUST.ID:'.':LIM

**************************
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = COMI
****************************

*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('LIMIT':@FM:LI.EXPIRY.DATE,ID.LIMIT,LIM.DATE)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,ID.LIMIT,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
LIM.DATE=R.ITSS.LIMIT<LI.EXPIRY.DATE>

            IF COMI < TODAY THEN ETEXT = 'MUST.BE.GR.OR.EQ.TODAY'  ; CALL  STORE.END.ERROR
            IF COMI LE R.NEW(LD.VALUE.DATE) THEN ETEXT = 'MUST BE GR VALUE DATE '   ; CALL  STORE.END.ERROR
            IF COMI LE TODAY THEN
                ETEXT = 'U MUST ENTER DATE GREATER THEN TODAY'  ; CALL  STORE.END.ERROR
            END
            IF LIM.DATE LT TODAY THEN TEXT = 'LIMIT DATE IS EXPIRED' ;CALL REM
            VAL.DATE=R.NEW(LD.VALUE.DATE)
*         MAX.DATE = SHIFT.DATE(VAL.DATE, '12M', 'UP')
*        IF COMI GT MAX.DATE THEN ETEXT = 'SHOULD NOT BE LESS THAN YEAR'

*Line [ 69 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.CODE=DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
*Line [ 71 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.AMT =DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1111' THEN
****     R.NEW(LD.CHRG.AMOUNT)=''
***                R.NEW(LD.FIN.MAT.DATE) = COMI
*****               R.NEW(LD.FIN.MAT.DATE)=SHIFT.DATE(COMI,"15D",UP)
                R.NEW(LD.FIN.MAT.DATE)='20991231'
**FOR I = 2 TO COUNT.AMT
**  IF R.NEW(LD.CHRG.AMOUNT)<1,I> ='' THEN DEL R.NEW(LD.CHRG.AMOUNT)<1,I>;DEL R.NEW(LD.CHRG.CODE)<1,I>
**NEXT I
**********************HYTH****20090204*************
                IF R.NEW(LD.CHRG.CODE)='' THEN
*****                 ***   R.NEW(LD.CHRG.CODE)='4'
***                    R.NEW(LD.FIN.MAT.DATE) = COMI
*****                 R.NEW(LD.FIN.MAT.DATE) =SHIFT.DATE(COMI,"15D",UP)
                    R.NEW(LD.FIN.MAT.DATE)='20991231'
                END
            END
        END ELSE
*********************************************************
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '2111' THEN
                IF MESSAGE NE 'VAL' THEN
*Line [ 93 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    COUNT.AMT= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>,@SM)
*Line [ 95 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    COUNT.CODE =DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>,@SM)
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>=''
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>=''
                    FOR I = 2 TO COUNT.AMT
                        IF R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I> THEN
                            DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
                            DEL R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
                        END
                    NEXT I
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>='5'
****           R.NEW(LD.FIN.MAT.DATE) = SHIFT.DATE(COMI,"15D",UP)
***                    R.NEW(LD.FIN.MAT.DATE) = COMI
                    R.NEW(LD.FIN.MAT.DATE)='20991231'
                    CALL REBUILD.SCREEN
                END
            END
********************************************************
        END
    END ELSE
****       R.NEW(LD.FIN.MAT.DATE) = SHIFT.DATE(COMI,"15D",UP)
***        R.NEW(LD.FIN.MAT.DATE) = COMI
        R.NEW(LD.FIN.MAT.DATE)='20991231'
        CALL REBUILD.SCREEN
    END
    RETURN
END
