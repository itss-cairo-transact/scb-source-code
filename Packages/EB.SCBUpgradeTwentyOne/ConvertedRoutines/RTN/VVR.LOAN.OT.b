* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LOAN.OT

*    PROGRAM VVR.LOAN.OT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.BALANCE.OT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

    F.COUNT = '' ; FN.COUNT = 'F.SCB.LOAN.BALANCE.OT'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
**********************************
    FN.ACCOUNT = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

    CALL OPF( FN.ACCOUNT,F.ACCOUNT)
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21053 BY OLD.ACCT"
**********************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.ACCOUNT,KEY.LIST<I>, R.ACCOUNT,F.ACCOUNT, ETEXT)
            CALL F.READ(FN.ACCOUNT,KEY.LIST<I+1>, R.ACCOUNT1,F.ACCOUNT, ETEXT)

            ID.NO = R.ACCOUNT<LD.LOCAL.REF,LDLR.OLD.ACCT>
            PRINT ID.NO
            R.COUNT<SCB.BAL.CURRENCY> = R.ACCOUNT<LD.CURRENCY>

            CUS = R.ACCOUNT<LD.LOCAL.REF,LDLR.OLD.ACCT>
            CUS1 = R.ACCOUNT1<LD.LOCAL.REF,LDLR.OLD.ACCT>
            IF CUS EQ CUS1 THEN
                TOT.AMT = TOT.AMT+R.ACCOUNT<LD.AMOUNT>
            END ELSE
                TOT.AMT = TOT.AMT+R.ACCOUNT<LD.AMOUNT>
                R.COUNT<SCB.BAL.AMOUNT> = TOT.AMT
*******************************UPDATED BY RIHAM R15**********************
*                WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
*                    PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
*                END
                CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
                CALL JOURNAL.UPDATE(ID.NO)
*****************************************************************
                TOT.AMT = "0"
            END
        NEXT I
    END
    RETURN
END
