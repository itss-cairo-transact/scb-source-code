* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************

    SUBROUTINE VVR.LG.CONFIS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN
        IF COMI = R.OLD(LD.AMOUNT) THEN
            R.NEW(LD.FIN.MAT.DATE) = TODAY
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1401"
        END
        IF COMI < R.OLD(LD.AMOUNT) THEN
            IF R.NEW(LD.FIN.MAT.DATE) = TODAY THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1402"
            END ELSE
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1403"
                R.NEW(LD.AMT.V.DATE) = TODAY
                R.NEW(LD.AMOUNT.INCREASE) = "-":COMI
                PER        = R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>/100
*******MODIFIED ON 1-3-2015
                DB.AMT     = COMI*PER
***************
                CALL EB.ROUND.AMOUNT ('EGP',DB.AMT,'',"2")
*******************MODIFIED ON 1-3-2015
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
                MARG.AMT=MYLOCAL<1,LDLR.MARGIN.AMT>
****************
                R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = MARG.AMT - DB.AMT
                CALL REBUILD.SCREEN
            END
        END
        IF COMI > R.OLD(LD.AMOUNT) THEN
            ETEXT = "MUST.BE.LT.OR.EQ.LG.AMT"
        END
    END
    CALL REBUILD.SCREEN
    IF MESSAGE EQ 'VAL' THEN
        R.NEW(LD.TRANCHE.AMT) = R.NEW(LD.AMOUNT)
        IF R.NEW(LD.AMOUNT.INCREASE) THEN
            R.NEW(LD.TRANCHE.AMT) =R.NEW(LD.AMOUNT)+R.NEW(LD.AMOUNT.INCREASE)
        END
    END

    RETURN
END
