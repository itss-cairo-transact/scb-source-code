* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*TO GET THE EQUIVALENT FOR THE LG AMOUNT
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LG.EQUI

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    LAMT = "" ; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; R.COUNT = ''
    IF R.NEW(LD.AMOUNT.INCREASE) THEN
        NEW.LG.AMOUNT=ABS(R.NEW(LD.AMOUNT.INCREASE))
    END ELSE
        NEW.LG.AMOUNT=R.NEW(LD.AMOUNT)
    END
    CURR   = R.NEW(LD.CURRENCY)
    MARKET = R.NEW(LD.CURRENCY.MARKET)

    IF CURR # LCCY THEN
        CALL MIDDLE.RATE.CONV.CHECK(NEW.LG.AMOUNT,CURR,RATE,MARKET,LAMT,DIF.AMT,DIF.RATE)
        IF R.NEW(LD.AMOUNT.INCREASE) EQ '' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=LAMT
        END ELSE
            IF R.NEW(LD.AMOUNT.INCREASE) GT 0 THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>+LAMT
            END ELSE
                IF R.NEW(LD.AMOUNT.INCREASE) LT 0 THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>-LAMT
                END
            END
        END
    END ELSE
        IF CURR EQ LCCY THEN
            IF R.NEW(LD.AMOUNT.INCREASE) EQ  '' THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=R.NEW(LD.AMOUNT)
            END ELSE
                IF R.NEW(LD.AMOUNT.INCREASE) GT 0 THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>= R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>+R.NEW(LD.AMOUNT.INCREASE)
                END ELSE
                    IF R.NEW(LD.AMOUNT.INCREASE) LT 0 THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>+R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>-R.NEW(LD.AMOUNT.INCREASE)
                    END
                END
            END
        END
        CALL REBUILD.SCREEN
        RETURN
    END
