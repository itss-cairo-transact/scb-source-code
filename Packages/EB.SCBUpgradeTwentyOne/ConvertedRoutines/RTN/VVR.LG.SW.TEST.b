* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------
**** RANIA 24/02/2003 ****

*A Routine To Default DrawDown Account & Margin Account according to customer,currency & category

    SUBROUTINE VVR.LG.SW.TEST


*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    IF COMI THEN

        MYID=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.CURRENCY,MYID,LGCURR)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
LGCURR=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.CURRENCY>
        LOCATE COMI IN R.NEW(SCB.LG.CH.CURRENCY)<1,1> SETTING LLL  ELSE ETEXT='Currency.Not.Found' ; RETURN

        R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> = ''


        T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> :" AND CURRENCY EQ ": COMI :" AND ( CATEGORY EQ '5000' )"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

        IF NOT(SELECTED) THEN
            ETEXT='There.Is.No.Account' ;RETURN
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = KEY.LIST<1>
        END

        T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": R.NEW( LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER> :" AND CURRENCY EQ ": COMI:" AND CATEGORY EQ '1056'"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

        IF NOT(SELECTED) THEN
            ETEXT='There.Is.No.Margin.Account'
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> = KEY.LIST<1>
        END



        CALL REBUILD.SCREEN

    END

    RETURN
END
