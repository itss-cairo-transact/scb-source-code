* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-------------------------NI7OOOOOOOOOOOOOOOOO----------------------------------------------------
    SUBROUTINE VVR.LG.CHECK.ACCOUNT.MOR


*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
****    IF MESSAGE NE 'VAL' THEN
****   TEXT = "MESSAGE" : MESSAGE ; CALL REM
    IF COMI # '' THEN
        DEBIT.ACC = COMI
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DEBIT.ACC,DR.CCY)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CCY=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.ACC,DR.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>

        IF R.NEW(LD.CURRENCY) NE DR.CCY THEN
            ETEXT ='Must Be Same Currnecy';CALL STORE.END.ERROR
        END
       *** IF R.NEW(LD.CUSTOMER.ID) NE DR.CUST THEN
     TEXT = R.NEW(LD.LOCAL.REF)<1,LDLR.SEN.REC.BANK> ; CALL REM
     TEXT = DR.CUST ; CALL REM
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.SEN.REC.BANK> NE DR.CUST THEN
            ETEXT='Customer Account Must Be Same The LG Customer';CALL STORE.END.ERROR
        END
        IF COMI EQ R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> THEN ETEXT='DR AND CR ACCT SHOULD BE DIFFRENT';CALL STORE.END.ERROR
        R.NEW(LD.CHRG.LIQ.ACCT)=COMI
        CALL REBUILD.SCREEN
    END
    XX = COMI
*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,XX,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,XX,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF  CATEG EQ 3005 THEN
        ETEXT='WRONG CATEGORY'
        CALL REBUILD.SCREEN
    END

****    END
    RETURN
END
