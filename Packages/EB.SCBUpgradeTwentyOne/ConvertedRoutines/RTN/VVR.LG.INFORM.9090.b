* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>485</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************

    SUBROUTINE VVR.LG.INFORM.9090

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE

    IF COMI THEN
*****************
*Line [ 42 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        COUNT.CODE= DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)
*Line [ 44 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        COUNT.AMT =DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>,@SM)
        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = ''



        FOR I = 2 TO COUNT.CODE
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I> THEN
                DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,I>
                DEL R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,I>
            END
        NEXT I

*****************
        FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
        CALL OPF(FN.ACC,F.ACC)
        FN.LMM= 'FBNK.LMM.CHARGE.CONDITIONS'; F.LMM = ''
        CALL OPF(FN.LMM,F.LMM)
        FN.CHR= 'FBNK.FT.CHARGE.TYPE' ; F.CHR = ''
        CALL OPF(FN.CHR,F.CHR)
        FN.COM= 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
        CALL OPF(FN.COM,F.COM)


        AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
        CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
        CATG = R.ACC<AC.CATEGORY>
        W.CUR =  R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>

        IF CATG EQ '9090' OR CATG EQ '1220' OR CATG EQ '9091' THEN
            R.NEW(LD.CHRG.CODE) = ''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1> = 14

            CALL F.READ(FN.LMM,R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,1>,R.LMM,F.LMM,EE2)
            CH.CO = R.LMM<LD21.CHARGE.CODE.KEY>
            CALL F.READ(FN.CHR,CH.CO,R.CHR,F.CHR,EE3)
            LOCATE W.CUR IN R.CHR<FT5.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR'
            CH.AMT = R.CHR<FT5.FLAT.AMT,J>
            IF EE3 THEN
                CALL F.READ(FN.COM,CH.CO,R.COM,F.COM,EE4)
                LOCATE W.CUR IN R.COM<FT4.CURRENCY,1> SETTING K ELSE ETEXT = 'ERROR IN CUR'
                CH.AMT = R.COM<FT4.FLAT.AMT,K>
            END
            R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,1>  = CH.AMT
        END ELSE
            IF CATG NE '9090' AND CATG NE '1220' AND CATG NE '9091' THEN
                R.NEW(LD.CHRG.CODE)<1,1> = '14'
                R.NEW(LD.CHRG.CLAIM.DATE)<1,1> = TODAY
            END
        END
        IF COMI > R.OLD(LD.AMOUNT) THEN
            ETEXT = "MUST.BE.LT.OR.EQ.LG.AMT"
        END

    END
    IF MESSAGE EQ 'VAL' THEN
        R.NEW(LD.TRANCHE.AMT) = R.NEW(LD.AMOUNT)
        IF R.NEW(LD.AMOUNT.INCREASE) THEN
            R.NEW(LD.TRANCHE.AMT) = R.NEW(LD.AMOUNT)+R.NEW(LD.AMOUNT.INCREASE)
        END
    END


    RETURN
END
