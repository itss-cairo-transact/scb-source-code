* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyOne
*DONE
    SUBROUTINE VVR.LG.COM.DFLT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.COM

******************************
    IF COMI[1,2] EQ 'LD' THEN
        ID.RC = FIELD(COMI,'-',1)
*LD.LOANS.AND.DEPOSITS.OLD.NO
**OR SELECT WITH OLD.NO AND CO.CODE
        FN.LD='F.LD.LOANS.AND.DEPOSITS';F.LD='';R.LD=''
        CALL F.READ(FN.LD,ID.RC,R.LD,F.LD, ETEXT)
        LG.TYPS = R.LD<LD.LOCAL.REF,LDLR.PRODUCT.TYPE>
        IF LG.TYPS NE '' THEN
*PRINT LG.TYP
            LG.CUR =  R.LD<LD.CURRENCY>
            LG.CO.ALL=R.LD<LD.CO.CODE>
            LG.CO =LG.CO.ALL[8,2]
            LOCAL.REF = R.LD<LD.LOCAL.REF>
            OLD.LG.NO = LOCAL.REF<1,LDLR.OLD.NO>

            IF LG.CO.ALL EQ ID.COMPANY THEN
                IF LG.TYPS EQ 'BIDBOND' THEN
                    DR.ACCT='PL52050'
                END
                IF LG.TYPS EQ 'FINAL' THEN
                    DR.ACCT='PL52052'
                END
                IF LG.TYPS EQ 'ADVANCE' THEN
                    DR.ACCT='PL52054'
                END
                LG.AMT=R.LD<LD.AMOUNT>
                R.NEW(FT.DEBIT.VALUE.DATE)   = TODAY
                R.NEW(FT.DEBIT.ACCT.NO)  = DR.ACCT
                R.NEW(FT.DEBIT.CURRENCY) =  LG.CUR
                R.NEW(FT.CREDIT.ACCT.NO)  = LG.CUR:'16110000100':LG.CO
                CALL REBUILD.SCREEN
*****************
                COMI = ID.RC :'-': OLD.LG.NO
            END ELSE
                ETEXT='NOT SAME BRNACH';CALL STORE.END.ERROR
            END
        END
    END ELSE
        IF COMI NE '0' THEN
            ID.RC = FIELD(COMI,'-',2)
            NO.DAT= LEN(ID.RC)

            IF NO.DAT NE '8' THEN
                ETEXT='WRONG FORMAT';CALL STORE.END.ERROR
            END ELSE
                COM.YEAR  =ID.RC[1,4]
                TODAY.YEAR =TODAY[1,4]

                IF COM.YEAR LE TODAY.YEAR THEN
                    ETEXT='SHOULD BE GREATER THAN ': COM.YEAR;CALL STORE.END.ERROR
                END
            END
        END
    END
    RETURN
END
