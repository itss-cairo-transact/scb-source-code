* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.PL.PRD.TXN(Y.RET.DATA)
****Mahmoud Elhawary******18/3/2014***************************
*   nofile routine to get TXN on PL for PRODUCT category     *
**************************************************************

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 43 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    CUS.ID    = ''
    CTE.CAT   = ''
    CTE.CUR   = ''
    CTE.BAL   = 0
    MNTH.NUM  = ''
    NO.OF.MONTHS = ''
    TOTAL.INT = 0
    TOTAL.COM = 0
    TOT.INT.ALL = 0
    TOT.COM.ALL = 0
    NO.PRINT    = ''
    XX = ''
    KK = 0
    CURR.ID = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.COM = 'F.COMPANY' ; F.COM = '' ; R.COM = '' ; ER.COM = ''
    CALL OPF(FN.COM,F.COM)
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.CAT = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = '' ; ER.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CTE = 'FBNK.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "PL.CATEGORY"      IN D.FIELDS<1> SETTING YCAT.POS THEN CAT.ID    = D.RANGE.AND.VALUE<YCAT.POS> ELSE RETURN
    LOCATE "START.DATE"       IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"         IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
    LOCATE "PRODUCT.CATEGORY" IN D.FIELDS<1> SETTING YPRD.POS THEN PRD.CAT   = D.RANGE.AND.VALUE<YPRD.POS> ELSE RETURN
    LOCATE "CURRENCY"         IN D.FIELDS<1> SETTING YCUR.POS THEN CURR.ID   = D.RANGE.AND.VALUE<YCUR.POS> ELSE RETURN


    COM.SEL = "SSELECT ":FN.COM
    CALL EB.READLIST(COM.SEL,K.COM,'',SELECTED.COM,ERRR1)
    LOOP
        REMOVE COMP.ID FROM K.COM SETTING POS.COM
    WHILE COMP.ID:POS.COM
        ID.COMPANY = COMP.ID
        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,END.DATE,CAT.ID,CATEG.LIST)
        LOOP
            REMOVE CTE.ID FROM CATEG.LIST SETTING POS.CTE.ID
        WHILE CTE.ID:POS.CTE.ID
            CALL F.READ(FN.CTE,CTE.ID,R.CTE,F.CTE,ER.CTE)
            CTE.CUR = R.CTE<AC.CAT.CURRENCY>
            CTE.AMT = R.CTE<AC.CAT.AMOUNT.LCY>
            CTE.DAT = R.CTE<AC.CAT.BOOKING.DATE>
            CTE.CUS = R.CTE<AC.CAT.CUSTOMER.ID>
            CTE.TXN = R.CTE<AC.CAT.TRANSACTION.CODE>
            CTE.PRD = R.CTE<AC.CAT.PRODUCT.CATEGORY>
            CTE.OUR = R.CTE<AC.CAT.OUR.REFERENCE>
            IF CTE.CUR NE LCCY THEN
                CTE.AMT = R.CTE<AC.CAT.AMOUNT.FCY>
            END
            IF CTE.PRD EQ PRD.CAT THEN
                IF NOT(CURR.ID) THEN
                    GOSUB RET.DATA
                END ELSE
                    IF CTE.CUR EQ CURR.ID THEN
                        GOSUB RET.DATA
                    END
                END
            END
        REPEAT
    REPEAT
    RETURN
*******************************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = COMP.ID:"*":CAT.ID:"*":FROM.DATE:"*":END.DATE:"*":CTE.CUR:"*":CTE.DAT:"*":CTE.ID:"*":CTE.OUR:"*":CTE.CUS:"*":CTE.AMT:"*":CTE.PRD
    RETURN
**************************************
END
