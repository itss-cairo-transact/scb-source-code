* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 5/9/2018*********************************
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.V.MNTLY.TRN.RP.COMP(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*-----------------------------------------------------------------------*
    TD1 = ''  ;  USR.COMP = ''
    USR.COMP = ID.COMPANY
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.TRNS  = 'FBNK.ACCT.ACTIVITY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    AMT.CR   = 0
    AMT.DR   = 0

    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]

    MDATE = YYYY:MM:'01'
    CALL CDT("",MDATE,'-1W')

    **YTEXT = "Enter Start Date : "
    **CALL TXTINP(YTEXT, 8, 22, "12", "A")
    **FROM.DATE = COMI
    **YTEXT = "Enter End Date : "
    **CALL TXTINP(YTEXT, 8, 22, "12", "A")
    **END.DATE.1 = COMI
    **TEXT = 'TO.DATE=':END.DATE.1 ; CALL REM
    **END.DATE = COMI

****
    LOCATE "START.DATE"   IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"     IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
    END.DATE.1 = END.DATE
****
    FOR I = 1 TO 80
        IF (END.DATE) LE (END.DATE.1) THEN
****N.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 10000 AND VERSION.NAME NE CLOSED AND CO.CODE EQ EG0010099 AND CURRENCY IN (EGP USD EUR GBP SAR )BY CO.CODE  "
    N.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 10000 AND VERSION.NAME NE CLOSED AND CO.CODE EQ ":USR.COMP :" AND CURRENCY IN (EGP USD EUR GBP SAR ) BY CURRENCY  "
            CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
         **   TEXT = 'SEL.BR=':SELECTED.N ; CALL REM

            ENT.NO = ''
            END.DATE.2 = FROM.DATE[1,8]
            END.DATE = END.DATE.2

            LOOP
                REMOVE ACCT.ID  FROM KEY.LIST.N SETTING POS
            WHILE ACCT.ID:POS
                CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
                CLR.AMT = 0
                LOOP
                    REMOVE TRNS.NO FROM ID.LIST  SETTING POS1
                WHILE TRNS.NO:POS1
                    CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)
                    REC.STATUS = R.STMT<AC.STE.RECORD.STATUS>
                    BK.DATE    = R.STMT<AC.STE.BOOKING.DATE>
                    IF REC.STATUS NE 'REVE' THEN
                        CURR = R.STMT<AC.STE.CURRENCY>
                        IF CURR = 'EGP' THEN
                            ST.AMT = R.STMT<AC.STE.AMOUNT.LCY>
                        END ELSE
                            ST.AMT = R.STMT<AC.STE.AMOUNT.FCY>
                        END
                        IF ST.AMT GT 0 THEN
                            AMT.CR = ST.AMT + AMT.CR
                        END
                        IF ST.AMT LT 0 THEN
                            AMT.DR = ST.AMT + AMT.DR
                        END
                    END
                REPEAT
                OP.BAL = OPENING.BAL
                CL.BAL = OP.BAL + AMT.CR + AMT.DR
                Y.RET.DATA<-1> = ACCT.ID:"*":CURR:"*":OP.BAL:"*":AMT.CR:"*":AMT.DR:"*":CL.BAL:"*":FROM.DATE:"*":END.DATE:"*":BK.DATE
                AMT.CR = 0
                AMT.DR = 0
                CL.BAL = 0
                OP.BAL = 0
            REPEAT
            CALL CDT('',FROM.DATE , '1W')
            END.DATE = FROM.DATE
        END
    NEXT I

    RETURN
END
