* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*************************MAHMOUD 19/1/2016*****************************
*-----------------------------------------------------------------------------
* <Rating>427</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.LIM.COLL.CD(XXX)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS

    COLL.ID.ARR = ''
    DD = '*'

    LOCATE "FROM.COMPANY" IN D.FIELDS<1> SETTING YCOM1.POS THEN COMP1 = D.RANGE.AND.VALUE<YCOM1.POS> ELSE RETURN
    LOCATE "TO.COMPANY" IN D.FIELDS<1> SETTING YCOM2.POS THEN COMP2 = D.RANGE.AND.VALUE<YCOM2.POS> ELSE RETURN

    FN.LI = "FBNK.LIMIT" ; F.LI = "" ; R.LI = ""
    CALL OPF(FN.LI,F.LI)
    FN.RI.COLL = "FBNK.RIGHT.COLLATERAL" ; F.RI.COL = "" ; R.RI.COL = ""
    CALL OPF(FN.RI.COLL,F.RI.COLL)

    L.SEL  = " SELECT FBNK.LIMIT WITH MAXIMUM.TOTAL NE '' AND PRODUCT.ALLOWED EQ '' AND LIMIT.PRODUCT IN( 103 108 139 406 602 603 ) "
    L.SEL := " AND COMPANY.BOOK GE ":COMP1
    L.SEL := " AND COMPANY.BOOK LE ":COMP2
    L.SEL := " BY COMPANY.BOOK "

    CALL EB.READLIST(L.SEL,LI.LIST,'',SELECTED.LI,ERR.1)
    LOOP
        REMOVE LI.ID FROM LI.LIST SETTING POS.LI
    WHILE LI.ID:POS.LI
        CALL F.READ(FN.LI,LI.ID,R.LI,F.LI,ER.LI)
*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        RI.COLL.@VM = ''
*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        COLL.@VM = ''
        MM1 = DCOUNT(R.LI<LI.COLLAT.RIGHT>,@VM)
        FOR MMV = 1 TO MM1
        RR1 = DCOUNT(R.LI<LI.COLLAT.RIGHT,MMV>,@SM)
        LI.LCL = R.LI<LI.LOCAL.REF>
        LI.CUS = LI.LCL<1,LILR.LIM.CUST>
        FOR XV = 1 TO RR1
            RI.COLL.ID = R.LI<LI.COLLAT.RIGHT,MMV,XV>
            COLL.CUS  = FIELD(RI.COLL.ID,'.',1)
**?        IF LI.CUS EQ COLL.CUS THEN GOTO NEXT.REC ELSE NULL
*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            RI.COLL.@VM := RI.COLL.ID:@@VM
            CALL F.READ(FN.RI.COLL,RI.COLL.ID,R.RI.COLL,F.RI.COLL,ERR111)
            LOOP
                REMOVE COLL.ID FROM R.RI.COLL SETTING POS11
            WHILE COLL.ID:POS11
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                COLL.@VM := COLL.ID:@SM
            REPEAT
NEXT.REC:
        NEXT XV
        NEXT MMV
*Line [ 79 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF COLL.@VM NE '' THEN
            GOSUB RET.DATA
        END
    REPEAT
    RETURN
*-------------------------------------------------------------------
RET.DATA:
*--------
*Line [ 88 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    XXX<-1> = LI.ID:DD:RI.COLL.@VM:DD:COLL.@VM:DD
    RETURN
*-------------------------------------------------------------------

END
