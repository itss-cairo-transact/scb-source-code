* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>970</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.SCB.SAVING.TOTALS.6501(Y.SAV.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*-------------------------------------------------------------------------

    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-------------------------------------------------------------------------
INITIATE:

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    HEAD.DESC  = "Layers / Average Balance in EGP":","
    HEAD.DESC := "Count / Number of Customers":","
    HEAD.DESC := "%":","
    HEAD.DESC := "Balance in EGP (000)":","
    HEAD.DESC := "%":","

    RETURN

*========================================================================
PROCESS:

    AMT.500    = 0   ; AMT.5000    = 0 ; AMT.30000   = 0
    AMT.100000 = 0   ; AMT.1000000 = 0 ; AMT.5000000 = 0
    AMT.GT.5000000 = 0

    CUS.500    = 0   ; CUS.5000    = 0 ; CUS.30000   = 0
    CUS.100000 = 0   ; CUS.1000000 = 0 ; CUS.5000000 = 0
    CUS.GT.5000000 = 0

    AMT.500.PER    = 0   ; AMT.5000.PER    = 0 ; AMT.30000.PER   = 0
    AMT.100000.PER = 0   ; AMT.1000000.PER = 0 ; AMT.5000000.PER = 0
    AMT.GT.5000000.PER = 0

    CUS.500.PER    = 0   ; CUS.5000.PER    = 0 ; CUS.30000.PER   = 0
    CUS.100000.PER = 0   ; CUS.1000000.PER = 0 ; CUS.5000000.PER = 0
    CUS.GT.5000000.PER = 0

    AMT.TOTAL = 0
    CUS.TOTAL = 0
    CUS.ID      = ''
    CUS.ID.NEXT = ''

*-------------------------------------------
    T.SEL  = "SELECT ":FN.AC:" WITH CATEGORY EQ 6501 AND OPEN.ACTUAL.BAL GE 0 AND CURRENCY EQ 'EGP' BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CALL F.READ(FN.AC,KEY.LIST<I+1>,R.AC1,F.AC,E2)

            AMT = R.AC<AC.OPEN.ACTUAL.BAL>
            CUS.ID      = R.AC<AC.CUSTOMER>
            CUS.ID.NEXT = R.AC1<AC.CUSTOMER>

            IF AMT GE 0 AND AMT LE 500 THEN
                AMT.500 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.500++
            END

            IF AMT GT 500 AND AMT LE 5000 THEN
                AMT.5000 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.5000++
            END

            IF AMT GT 5000 AND AMT LE 30000 THEN
                AMT.30000 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.30000++
            END

            IF AMT GT 30000 AND AMT LE 100000 THEN
                AMT.100000 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.100000++
            END

            IF AMT GT 100000 AND AMT LE 1000000 THEN
                AMT.1000000 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.1000000++
            END

            IF AMT GT 1000000 AND AMT LE 5000000 THEN
                AMT.5000000 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.5000000++
            END

            IF AMT GT 5000000 THEN
                AMT.GT.5000000 += AMT
                IF CUS.ID.NEXT NE CUS.ID THEN CUS.GT.5000000++
            END

            AMT.TOTAL += AMT
            CUS.TOTAL = CUS.500 + CUS.5000 + CUS.30000 + CUS.100000 + CUS.1000000 + CUS.5000000 + CUS.GT.5000000

        NEXT I
    END

    AMT.TOTAL = AMT.TOTAL / 1000

    AMT.500 = AMT.500 / 1000
    AMT.5000 = AMT.5000 / 1000
    AMT.30000 = AMT.30000 / 1000
    AMT.100000 = AMT.100000 / 1000
    AMT.1000000 = AMT.1000000 / 1000
    AMT.5000000  = AMT.5000000 / 1000
    AMT.GT.5000000 = AMT.GT.5000000 / 1000

    AMT.TOTAL = FMT(AMT.TOTAL,"L0")

    AMT.500 = FMT(AMT.500,"L0")
    AMT.5000 = FMT(AMT.5000,"L0")
    AMT.30000 = FMT(AMT.30000,"L0")
    AMT.100000 = FMT(AMT.100000,"L0")
    AMT.1000000 = FMT(AMT.1000000,"L0")
    AMT.5000000  = FMT(AMT.5000000,"L0")
    AMT.GT.5000000 = FMT(AMT.GT.5000000,"L0")

    AMT.500.PER = (AMT.500 / AMT.TOTAL) * 100
    AMT.5000.PER = (AMT.5000 / AMT.TOTAL) * 100
    AMT.30000.PER = (AMT.30000 / AMT.TOTAL) * 100
    AMT.100000.PER = (AMT.100000 / AMT.TOTAL) * 100
    AMT.1000000.PER = (AMT.1000000 / AMT.TOTAL) * 100
    AMT.5000000.PER  = (AMT.5000000 / AMT.TOTAL) * 100
    AMT.GT.5000000.PER = (AMT.GT.5000000 / AMT.TOTAL) * 100

    CUS.500.PER = (CUS.500 / CUS.TOTAL) * 100
    CUS.5000.PER = (CUS.5000 / CUS.TOTAL) * 100
    CUS.30000.PER = (CUS.30000 / CUS.TOTAL) * 100
    CUS.100000.PER = (CUS.100000 / CUS.TOTAL) * 100
    CUS.1000000.PER = (CUS.1000000 / CUS.TOTAL) * 100
    CUS.5000000.PER  = (CUS.5000000 / CUS.TOTAL) * 100
    CUS.GT.5000000.PER = (CUS.GT.5000000 / CUS.TOTAL) * 100

    AMT.500.PER = FMT(AMT.500.PER,"L2")
    AMT.5000.PER = FMT(AMT.5000.PER,"L2")
    AMT.30000.PER = FMT(AMT.30000.PER,"L2")
    AMT.100000.PER = FMT(AMT.100000.PER,"L2")
    AMT.1000000.PER = FMT(AMT.1000000.PER,"L2")
    AMT.5000000.PER  = FMT(AMT.5000000.PER,"L2")
    AMT.GT.5000000.PER = FMT(AMT.GT.5000000.PER,"L2")

    CUS.500.PER = FMT(CUS.500.PER,"L2")
    CUS.5000.PER = FMT(CUS.5000.PER,"L2")
    CUS.30000.PER = FMT(CUS.30000.PER,"L2")
    CUS.100000.PER = FMT(CUS.100000.PER,"L2")
    CUS.1000000.PER = FMT(CUS.1000000.PER,"L2")
    CUS.5000000.PER  = FMT(CUS.5000000.PER,"L2")
    CUS.GT.5000000.PER = FMT(CUS.GT.5000000.PER,"L2")

    TXT.500 = 'Amount < 500'
    Y.SAV.DATA<-1> = TXT.500:"*":CUS.500:"*":CUS.500.PER:"*":AMT.500:"*":AMT.500.PER
*******
    TXT.5000  = '501 < Amount < 5000'
    Y.SAV.DATA<-1> = TXT.5000:"*":CUS.5000:"*":CUS.5000.PER:"*":AMT.5000:"*":AMT.5000.PER
*********
    TXT.30000  = '5001 < Amount < 30000'
    Y.SAV.DATA<-1> = TXT.30000:"*":CUS.30000:"*":CUS.30000.PER:"*":AMT.30000:"*":AMT.30000.PER
*************
    TXT.100000  = '30001 < Amount < 100000'
    Y.SAV.DATA<-1> = TXT.100000:"*":CUS.100000:"*":CUS.100000.PER:"*":AMT.100000:"*":AMT.100000.PER
**************
    TXT.1000000  = '100001 < Amount < 1000000'
    Y.SAV.DATA<-1> = TXT.1000000:"*":CUS.1000000:"*":CUS.1000000.PER:"*":AMT.1000000:"*":AMT.1000000.PER
***********
    TXT.5000000 = '1000001 < Amount < 5000000'
    Y.SAV.DATA<-1> = TXT.5000000:"*":CUS.5000000:"*":CUS.5000000.PER:"*":AMT.5000000:"*":AMT.5000000.PER
**************
    TXT.GT.5000000  = 'Amount > 5000001'
    Y.SAV.DATA<-1> = TXT.GT.5000000:"*":CUS.GT.5000000:"*":CUS.GT.5000000.PER:"*":AMT.GT.5000000:"*":AMT.GT.5000000.PER
**************

    TXT.TOTAL  = 'Totals'
    Y.SAV.DATA<-1> = TXT.TOTAL:"*":CUS.TOTAL:"*":"*":AMT.TOTAL

*********************************
    RETURN
END
