* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
**************************************MAHMOUD 8/11/2015*******************************************
    SUBROUTINE E.NOF.DEPO.TXN.TODAY.NEW(REC.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 41 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    K  = 0
    K2 = 0
    DDY.NO = 0
    TDD1 = TODAY
    FROM.DATE = TDD1
    END.DATE  = TDD1
    KEY.LIST  = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2 = '' ; SELECTED2 = '' ; ERR.2 = ''
    PERS.DR = ''
    PERS.CR = ''
    COMP.DR = ''
    COMP.CR = ''
    CURR.CODE = 'EGP'
    REC.TITLE = ''
    REC.TITLE<1> = '����'
    REC.TITLE<2> = '�����'
    REC.TITLE<3> = '�����'
    REC.TITLE<4> = '������'
    YLL = DCOUNT(REC.TITLE,@FM)
    FOR XXY = 1 TO YLL
        PERS.DR<XXY>= 0
        PERS.CR<XXY>= 0
        COMP.DR<XXY>= 0
        COMP.CR<XXY>= 0
    NEXT XXY

*========================================
CALLDB:
*-------
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC = '' ; R.CU.AC = ''
    CALL OPF(FN.CU.AC,F.CU.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ENT = 'FBNK.ACCT.ENT.TODAY' ; F.ENT = '' ; R.ENT = ''
    CALL OPF(FN.ENT,F.ENT)

    RETURN
*========================================
PROCESS:
*-------
*----------------------------------AC REC---------------------------------------------
    SEL.ACC = " SELECT FBNK.ACCT.ENT.TODAY "
    CALL EB.READLIST(SEL.ACC,K.LIST,'',SELECTED,ERR.MSG)
    CRT SELECTED
    LOOP
        REMOVE ACC.ID FROM K.LIST SETTING POSS.ACC
    WHILE ACC.ID:POSS.ACC
        K++
        CALL F.READ(FN.AC,ACC.ID,R.AC,F.AC,ERR.1)
        REC.CAT = R.AC<AC.CATEGORY>
        REC.CUR = R.AC<AC.CURRENCY>
        IF REC.CUR EQ CURR.CODE THEN
            IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' OR REC.CAT[1,2] EQ '65' THEN
                CU.ID = R.AC<AC.CUSTOMER>
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CU.ID,CU.LCL)
                REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
                CALL F.READ(FN.ENT,ACC.ID,ID.LIST,F.ENT,ER.ENT)
                LOOP
                    REMOVE STE.ID FROM ID.LIST SETTING POS.STE
                WHILE STE.ID:POS.STE
                    IF STE.ID THEN
                        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                        STE.AMT.LCY = R.STE<AC.STE.AMOUNT.LCY>
                        IF STE.AMT.LCY LT 0 THEN
                            IF REC.SEC EQ '4650' THEN
                                IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' THEN
                                    PERS.DR<1> += STE.AMT.LCY
                                END ELSE
                                    IF REC.CAT EQ '6512' THEN
                                        PERS.DR<3> += STE.AMT.LCY
                                    END ELSE
                                        PERS.DR<2> += STE.AMT.LCY
                                    END
                                END
                            END ELSE
                                IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' THEN
                                    COMP.DR<1> += STE.AMT.LCY
                                END ELSE
                                    IF REC.CAT EQ '6512' THEN
                                        COMP.DR<3> += STE.AMT.LCY
                                    END ELSE
                                        COMP.DR<2> += STE.AMT.LCY
                                    END
                                END
                            END
                        END ELSE
                            IF REC.SEC EQ '4650' THEN
                                IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' THEN
                                    PERS.CR<1> += STE.AMT.LCY
                                END ELSE
                                    IF REC.CAT EQ '6512' THEN
                                        PERS.CR<3> += STE.AMT.LCY
                                    END ELSE
                                        PERS.CR<2> += STE.AMT.LCY
                                    END
                                END
                            END ELSE
                                IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' THEN
                                    COMP.CR<1> += STE.AMT.LCY
                                END ELSE
                                    IF REC.CAT EQ '6512' THEN
                                        COMP.CR<3> += STE.AMT.LCY
                                    END ELSE
                                        COMP.CR<2> += STE.AMT.LCY
                                    END
                                END
                            END
                        END
                    END
                REPEAT
            END
        END
    REPEAT
*----------------------------------LD REC---------------------------------------------
    SEL.CMD2  = "SELECT ":FN.LD:" WITH CUSTOMER.ID NE ''"
    SEL.CMD2 := " AND CURRENCY EQ ":CURR.CODE
    SEL.CMD2 := " AND (( VALUE.DATE EQ ":TDD1:" AND AMOUNT NE 0 AND OLD.NO EQ '' )"
    SEL.CMD2 := "  OR  ( FIN.MAT.DATE EQ ":TDD1:" AND AMOUNT EQ 0 ))"
    SEL.CMD2 := " AND (( CATEGORY GE 21001 AND CATEGORY LE 21010 )"
    SEL.CMD2 := "  OR  ( CATEGORY GE 21017 AND CATEGORY LE 21029 )"
    SEL.CMD2 := "  OR  ( CATEGORY EQ 21041 OR CATEGORY EQ 21042 OR CATEGORY EQ 21013 ))"
    CALL EB.READLIST(SEL.CMD2,KEY.LIST2,'',SELECTED2,ERR.2)
    LOOP
        REMOVE LD.ID FROM KEY.LIST2 SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        LD.COM  = R.LD<LD.CO.CODE>
        LD.CAT  = R.LD<LD.CATEGORY>
        LD.VAL  = R.LD<LD.VALUE.DATE>
        LD.CUR  = R.LD<LD.CURRENCY>
        LD.STA  = R.LD<LD.STATUS>
        LD.FIN  = R.LD<LD.FIN.MAT.DATE>
        LD.LCL  = R.LD<LD.LOCAL.REF>
        LD.REN  = LD.LCL<1,LDLR.RENEW.IND>
        LD.OLD  = LD.LCL<1,LDLR.OLD.NO>
        LD.BAL  = R.LD<LD.AMOUNT>
        IF LD.BAL EQ 0 THEN
            LD.BAL  = R.LD<LD.REIMBURSE.AMOUNT>
        END
        LD.CUS = R.LD<LD.CUSTOMER.ID>
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LD.CUS,CU.LCL)
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
*************************************
        IF LD.VAL EQ TDD1 AND LD.OLD EQ '' AND LD.STA EQ 'CUR' THEN
            IF REC.SEC EQ '4650' THEN
                IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                    PERS.CR<3> += LD.BAL
                END ELSE
                    PERS.CR<4> += LD.BAL
                END
            END ELSE
                IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                    COMP.CR<3> += LD.BAL
                END ELSE
                    COMP.CR<4> += LD.BAL
                END
            END
        END ELSE
            CALL DBR('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.FIN.MAT.DATE,LD.ID:';1',LD.FIRST.FIN)
            IF (LD.REN NE 'YES') OR ( LD.FIRST.FIN NE LD.FIN ) THEN
                CRT LD.FIRST.FIN:"-":LD.FIN:"-":LD.ID
                IF REC.SEC EQ '4650' THEN
                    IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                        PERS.DR<3> += LD.BAL *-1
                    END ELSE
                        PERS.DR<4> += LD.BAL *-1
                    END
                END ELSE
                    IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                        COMP.DR<3> += LD.BAL *-1
                    END ELSE
                        COMP.DR<4> += LD.BAL *-1
                    END
                END
            END
        END
    REPEAT
    GOSUB PRINT.REC
    RETURN
*========================================
PRINT.REC:
*----------
    LL = DCOUNT(REC.TITLE,@FM)
    FOR XX1 = 1 TO LL
        REC.DATA<-1>=REC.TITLE<XX1>:"*":PERS.DR<XX1>:"*":PERS.CR<XX1>:"*":COMP.DR<XX1>:"*":COMP.CR<XX1>
    NEXT XX1
    RETURN
*========================================
END
