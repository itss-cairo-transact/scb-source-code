* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.HQ.ACCT(Y.HQ.DATA)

****Mahmoud Elhawary******27/5/2009****************************
*    NOFILE ENQUIRY for interbranch transactions              *
***************************************************************

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*    $INCLUDE GLOBUS.BP I_F.ACCT.ENT.LWORK.DAY
    $INCLUDE T24.BP I_F.FUNDS.TRANSFER  ;*FT.
    $INCLUDE           I_FT.LOCAL.REFS  ;*FTLR.
    $INCLUDE T24.BP I_F.BILL.REGISTER   ;*EB.BILL.REG.
    $INCLUDE           I_BR.LOCAL.REFS  ;*BRLR.
    $INCLUDE T24.BP I_F.TELLER          ;*TT.TE.
    $INCLUDE T24.BP I_F.DRAWINGS        ;*TF.DR.
    $INCLUDE T24.BP I_F.LETTER.OF.CREDIT          ;*TF.LC.
    $INCLUDE           I_F.INF.MULTI.TXN          ;*INF.MLT.
    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM         ;*RE.BCP.
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*----------------------------------
    LLLY    = ''
    CUR.TXT = ''
    OPERX   = ''
    OPERX2  = ''
    LOCATE "CURRENCY" IN D.FIELDS<1> SETTING YCURR.POS THEN
        CUR  = D.RANGE.AND.VALUE<YCURR.POS>
        OPER = D.LOGICAL.OPERANDS<YCURR.POS>
    END ELSE
        RETURN
    END
    LOCATE "BRANCH" IN D.FIELDS<1> SETTING YBRAN.POS THEN
        CCSEL = 'BRR'
        CCMP  = D.RANGE.AND.VALUE<YBRAN.POS>
        OPER2 = D.LOGICAL.OPERANDS<YBRAN.POS>
    END ELSE
        CCSEL = ''
    END
    LOCATE "BOOKING.DATE" IN D.FIELDS<1> SETTING YDATE.POS THEN
        TD1 = D.RANGE.AND.VALUE<YDATE.POS>
        TTTT = TD1
    END ELSE
        RETURN
    END
    IF TTTT[5,2] EQ '12' THEN
        LLLY = TTTT
        CALL LAST.WDAY(LLLY)
    END
    LCCMP = LEN(CCMP)
    IF LCCMP EQ 1 THEN
        CCMP = '0':CCMP
    END ELSE
        ZZZ = 0
    END
    IF CCMP EQ '99' AND OPER2 EQ 1 THEN
        CC.TXT = '����� �'
    END ELSE
        CC.TXT = ''
    END
    IF CCMP NE '' THEN
        CCMP = 'EG00100':CCMP
    END
    IF OPER EQ 1 THEN
        OPERX = 'EQ'
        IF CUR EQ 'EGP' THEN
            CUR.TXT = '���� �����'
        END ELSE
            CUR.TXT = '���� ������' :'  ':CUR
        END
    END
    IF OPER EQ 5 THEN
        OPERX = 'NE'
        IF CUR EQ 'EGP' THEN
            CUR.TXT = '���� ������'
        END ELSE
            CUR.TXT = '���� �����'
        END
    END
    IF OPER2 EQ 1 THEN
        OPERX2 = 'EQ'
    END
    IF OPER2 EQ 5 THEN
        OPERX2 = 'NE'
    END

*----------------------------------
    TTDY = TODAY
    LWDY = TODAY
    CALL CDT("",LWDY,'-1W')
*----------------------------------------------------------------

    GOSUB INITIATE
*Line [ 123 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
INITIATE:
*--------
    REPORT.NAME='E.NOF.HQ.ACCT'

    K = 0
    K.LIST.COM ="" ; SELECTED.COM ="" ; ER.MSG.COM =""
    K.LIST.ACC ="" ; SELECTED.ACC ="" ; ER.MSG.ACC =""
    K.LIST.STE ="" ; SELECTED.STE ="" ; ER.MSG.STE =""
    K.LIST.ENT ="" ; SELECTED.ENT ="" ; ER.MSG.ENT =""
    K.LIST.AC =""  ; SELECTED.AC =""  ; ER.MSG.AC =""
    K.LIST    =""  ; SELECTED    =""  ; ER.MSG    =""
    AMT.LCY = ''
    AMT.IN.DR  = 0
    AMT.IN.CR  = 0
    AMT.OUT.DR = 0
    AMT.OUT.CR = 0
    TOT.IN.DR  = 0
    TOT.IN.CR  = 0
    TOT.OUT.DR = 0
    TOT.OUT.CR = 0
    TOTAL.IN.DR = 0
    TOTAL.IN.CR = 0
    TOTAL.OUT.DR = 0
    TOTAL.OUT.CR = 0
    STE.CNF.DR = ''
    STE.CNF.CR = ''
    STE.CR.COMP = ''
    STE.DR.COMP = ''
    MMBR = ''
    STE.VER = ''
    CN.FLAG = ''
    SSTE = ''
    SSTE1= ''
    YYY = SPACE(120)
    XX  = SPACE(120)
    XX1 = SPACE(120)
    XX2 = SPACE(120)
    XX3 = SPACE(120)
    RETURN
*===============================================
CALLDB:
*--------
    FN.COM = 'F.COMPANY'
    F.COM = ''
    R.COM = ''
    CALL OPF(FN.COM,F.COM)
    FN.STE = 'FBNK.STMT.ENTRY'
    F.STE = ''
    R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ENT = 'FBNK.ACCT.ENT.TODAY'
    F.ENT = ''
    R.ENT = ''
    CALL OPF(FN.ENT,F.ENT)
    FN.LWD = 'FBNK.ACCT.ENT.LWORK.DAY'
    F.LWD = ''
    R.LWD = ''
    CALL OPF(FN.LWD,F.LWD)
    FN.STP = 'FBNK.STMT.PRINTED'
    F.STP = ''
    R.STP = ''
    CALL OPF(FN.STP,F.STP)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.FT = 'FBNK.FUNDS.TRANSFER'
    F.FT = ''
    R.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS'
    F.FT.H = ''
    R.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)
    FN.TT = 'FBNK.TELLER'
    F.TT = ''
    R.TT = ''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H = 'FBNK.TELLER$HIS'
    F.TT.H = ''
    R.TT.H = ''
    CALL OPF(FN.TT.H,F.TT.H)
    FN.MLT = 'F.INF.MULTI.TXN'
    F.MLT = ''
    R.MLT = ''
    CALL OPF(FN.MLT,F.MLT)
    FN.LCD = 'FBNK.DRAWINGS'
    F.LCD = ''
    R.LCD = ''
    CALL OPF(FN.LCD,F.LCD)
    FN.LCM = 'FBNK.LETTER.OF.CREDIT'
    F.LCM = ''
    R.LCM = ''
    CALL OPF(FN.LCM,F.LCM)
    FN.CUR = 'F.RE.BASE.CCY.PARAM'
    F.CUR = ''
    R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    RETURN
*===============================================
PROCESS:
*--------
    IF CCSEL NE '' THEN
        C.SEL  ="SELECT ":FN.COM
***/////////////////////********
        IF TD1 NE LLLY AND TD1 NE '20100718' THEN
            C.SEL :=" WITH @ID NE ":COMP
            C.SEL :=" AND @ID ":OPERX2:" ":CCMP
        END ELSE
            C.SEL :=" WITH @ID ":OPERX2:" ":CCMP
        END
***/////////////////////********
        C.SEL :=" BY @ID"
    END ELSE
        C.SEL  ="SELECT ":FN.COM
        IF TD1 NE LLLY AND TD1 NE '20100718' THEN
            C.SEL :=" WITH @ID NE ":COMP
        END
        C.SEL :=" BY @ID"
    END
    CALL EB.READLIST(C.SEL,K.LIST.COM,'',SELECTED.COM,ER.MSG.COM)
    IF SELECTED.COM THEN
        TOTAL.IN.DR = 0
        TOTAL.IN.CR  = 0
        TOTAL.OUT.DR = 0
        TOTAL.OUT.CR = 0
        FOR CMM = 1 TO SELECTED.COM
            TOT.IN.DR  = 0
            TOT.IN.CR  = 0
            TOT.OUT.DR = 0
            TOT.OUT.CR = 0
            COM.ID = K.LIST.COM<CMM>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COM.ID,COM.NAME)
*>>>>>>>>>>>>>>>>>>>>>>>>
            A.SEL  ="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 11500 11505 )"
            A.SEL :=" AND CO.CODE EQ ":COMP
            A.SEL :=" AND CURRENCY ":OPERX:" ":CUR
            A.SEL :=" BY @ID"
            KK1 = 0
            CALL EB.READLIST(A.SEL,K.LIST.AC,'',SELECTED.AC,ER.MSG.AC)
            LOOP
                REMOVE AC.ID FROM K.LIST.AC SETTING POS.AC
            WHILE AC.ID:POS.AC
                CALL EB.ACCT.ENTRY.LIST(AC.ID<1>,TD1,TD1,ID.LIST,OPENING.BAL,ER)
*----------------------------------------------------------------------------
                LOOP
                    REMOVE STE.ID FROM ID.LIST SETTING POS.STE
                WHILE STE.ID:POS.STE
                    AMT.IN.DR  = 0
                    AMT.IN.CR  = 0
                    AMT.OUT.DR = 0
                    AMT.OUT.CR = 0
                    STE.TTYP   = ''
                    STE.CNF.DR = ''
                    STE.CNF.CR = ''
                    ORD.BNK    = ''
                    ARAB.INT   = ''
                    STE.VER = ''
                    STE.TCOD= ''
                    STE.CR.COMP = ''
                    STE.DR.COMP = ''
                    SSTE.MLT = ''
                    SSTE.LC  = ''
                    MMBR = ''
                    CN.FLAG = ''
                    FT.REC.STAT = ''
                    XC1D = ''
                    XC1C = ''
                    FT.DR.CAT1 = ''

                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                    STE.NRV2 = R.STE<AC.STE.NARRATIVE><1,2>
                    STE.ACC  = R.STE<AC.STE.ACCOUNT.NUMBER>
                    STE.CUR  = R.STE<AC.STE.CURRENCY>
                    STE.COM  = R.STE<AC.STE.COMPANY.CODE>
                    STE.REF  = R.STE<AC.STE.TRANS.REFERENCE>
                    STE.OUR.REF   = R.STE<AC.STE.OUR.REFERENCE>
                    STE.REVE.MARK= R.STE<AC.STE.REVERSAL.MARKER>
                    STE.BOOK.DATE= R.STE<AC.STE.BOOKING.DATE>
                    STE.LCY  = R.STE<AC.STE.AMOUNT.LCY>
                    STE.FCY  = R.STE<AC.STE.AMOUNT.FCY>
                    IF STE.CUR NE 'EGP' THEN
                        CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,EC2)
*Line [ 309 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                        LOCATE STE.CUR IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS2 ELSE NULL
                        CUR.RATE  = R.CUR<RE.BCP.RATE,POS2>
                        RATE.TYPE = R.CUR<RE.BCP.RATE.TYPE,POS2>
                        IF RATE.TYPE EQ 'MULTIPLY' THEN
                            STE.LCY = STE.FCY * CUR.RATE
                        END
                        IF RATE.TYPE EQ 'DIVIDE' THEN
                            IF CUR.RATE NE 0 THEN
                                STE.LCY = STE.FCY / CUR.RATE
                            END ELSE
                                STE.LCY = 0
                            END
                        END
                    END
                    STE.VLD  = R.STE<AC.STE.VALUE.DATE>
                    STE.STA  = R.STE<AC.STE.RECORD.STATUS>
                    STE.SID  = R.STE<AC.STE.SYSTEM.ID>
                    STE.DPT  = R.STE<AC.STE.DEPARTMENT.CODE>
                    STE.CRF  = R.STE<AC.STE.CRF.TYPE>
                    STE.STN  = R.STE<AC.STE.STMT.NO>
                    IF STE.NRV2[1,6] NE '994999' THEN
                        IF STE.NRV2[1,3] EQ '994' THEN
                            IF STE.NRV2 EQ '99400500' THEN
                                ARAB.INT = STE.NRV2
                            END ELSE
                                ARAB.INT = ''
                            END
                            STE.NRV2 = ''
                        END ELSE
                            STE.NRV2 = ''
                        END
                    END
                    IF STE.REF[4] EQ '\BNK' THEN
                        STE.REF = FIELD(STE.REF,'\',1):'\B01'
                    END
                    FINDSTR '\B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'\',1) ; CN.FLAG = 'CONF' ELSE CN.FLAG = ''
*Line [ 346 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DXX = DCOUNT(STE.STN,@VM)
                    FOR DX1 = 1 TO DXX
                        SSTE = R.STE<AC.STE.STMT.NO,DX1>
                        IF SSTE[1,2] EQ 'EG' THEN
                            IF STE.NRV2 EQ '' THEN
                                DX1 = DXX
                            END ELSE
                                IF STE.NRV2[2] EQ SSTE[2] OR STE.NRV2[2] EQ COMP[2] THEN
                                    DX1 = DXX
                                END
                                IF STE.NRV2 EQ '99499900' AND SSTE[2] EQ '99' THEN
                                    DX1 = DXX
                                END
                            END
                        END ELSE
                            SSTE = COMP
                        END
                    NEXT DX1
*******************************************************
                    IF STE.REF[1,2] EQ 'FT' THEN
                        CALL F.READ(FN.FT,STE.REF,R.FT,F.FT,ERR.FT.1)
                        STE.CNF.DR = R.FT<FT.DEBIT.THEIR.REF>
                        STE.CNF.CR = R.FT<FT.CREDIT.THEIR.REF>
                        FT.DR.ACC  = R.FT<FT.DEBIT.ACCT.NO>
                        FT.CR.ACC  = R.FT<FT.CREDIT.ACCT.NO>
                        STE.TTYP   = R.FT<FT.TRANSACTION.TYPE>
                        STE.FTLR   = R.FT<FT.LOCAL.REF>
                        STE.CR.COMP= R.FT<FT.CREDIT.COMP.CODE>
                        STE.DR.COMP= R.FT<FT.DEBIT.COMP.CODE>
                        ORD.BNK    = R.FT<FT.ORDERING.BANK>
                        FT.REC.STAT= R.FT<FT.RECORD.STATUS>
                        STE.VER = STE.FTLR<1,FTLR.VERSION.NAME>
                        IF STE.TTYP EQ '' THEN
                            CALL F.READ.HISTORY(FN.FT.H,STE.REF,R.FT.H,F.FT.H,ERR.FT.1H)
                            STE.CNF.DR = R.FT.H<FT.DEBIT.THEIR.REF>
                            STE.CNF.CR = R.FT.H<FT.CREDIT.THEIR.REF>
                            FT.DR.ACC  = R.FT.H<FT.DEBIT.ACCT.NO>
                            FT.CR.ACC  = R.FT.H<FT.CREDIT.ACCT.NO>
                            STE.TTYP   = R.FT.H<FT.TRANSACTION.TYPE>
                            STE.FTLR   = R.FT.H<FT.LOCAL.REF>
                            STE.CR.COMP= R.FT.H<FT.CREDIT.COMP.CODE>
                            STE.DR.COMP= R.FT.H<FT.DEBIT.COMP.CODE>
                            ORD.BNK    = R.FT.H<FT.ORDERING.BANK>
                            FT.REC.STAT= R.FT.H<FT.RECORD.STATUS>
                            STE.VER = STE.FTLR<1,FTLR.VERSION.NAME>
                        END
************8/4/2010*****************************
                        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,FT.DR.ACC,FT.DR.CAT)
                        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,FT.CR.ACC,FT.CR.CAT)
                        XC1D = STE.CNF.DR
                        XC1C = STE.CNF.CR
                        FT.DR.CAT1 = FT.DR.CAT
                        IF STE.REVE.MARK EQ 'R' THEN
                            STE.CNF.DR = XC1C
                            STE.CNF.CR = XC1D
                            FT.DR.CAT1 = FT.CR.CAT
                        END ELSE
                            XC1D = ''
                            XC1C = ''
                            FT.DR.CAT1 = FT.DR.CAT
                        END
                        IF CN.FLAG EQ '' THEN
                            IF STE.DR.COMP NE COMP AND STE.CR.COMP NE COMP THEN
                                MMBR = 'INTERBRANCH'
                            END ELSE
                                MMBR = ''
                            END
                            IF STE.NRV2 EQ '' THEN
                                IF MMBR NE '' THEN
                                    IF STE.REVE.MARK NE 'R' THEN
                                        IF STE.LCY LT 0 THEN
                                            SSTE = STE.DR.COMP
                                        END ELSE
                                            SSTE = STE.CR.COMP
                                        END
                                    END ELSE
                                        IF STE.LCY LT 0 THEN
                                            SSTE = STE.CR.COMP
                                        END ELSE
                                            SSTE = STE.DR.COMP
                                        END
                                    END
                                END
                            END
                        END
                    END
                    IF STE.REF[1,2] EQ 'TT' THEN
                        CALL F.READ(FN.TT,STE.REF,R.TT,F.TT,ERR.TTH)
                        STE.CNF.DR = R.TT<TT.TE.NARRATIVE.1>
                        STE.CNF.CR = R.TT<TT.TE.NARRATIVE.1>
                        STE.TCOD   = R.TT<TT.TE.TRANSACTION.CODE>
                        IF STE.TCOD EQ '' THEN
                            CALL F.READ.HISTORY(FN.TT.H,STE.REF,R.TT.H,F.TT.H,ERR.TT.1H)
                            STE.CNF.DR = R.TT.H<TT.TE.NARRATIVE.1>
                            STE.CNF.CR = R.TT.H<TT.TE.NARRATIVE.1>
                            STE.TCOD   = R.TT.H<TT.TE.TRANSACTION.CODE>
                        END
                    END
                    IF STE.REF[1,2] EQ 'IN' THEN
                        MLT.NO = FIELD(STE.OUR.REF,'-',2)
                        CALL F.READ(FN.MLT,STE.REF,R.MLT,F.MLT,ER.MLT)
                        MLT.AC     = R.MLT<INF.MLT.ACCOUNT.NUMBER ,MLT.NO>
                        MLT.MK     = R.MLT<INF.MLT.BILL.NO>
                        MLT.DEPT   = R.MLT<INF.MLT.DEPT.CODE>
                        IF CN.FLAG EQ '' THEN
                            IF STE.LCY LT 0 THEN
                                STE.CNF.DR = R.MLT<INF.MLT.THEIR.REFERENCE,MLT.NO>
                                STE.CNF.CR = ''
                            END ELSE
                                STE.CNF.CR = R.MLT<INF.MLT.THEIR.REFERENCE,MLT.NO>
                                STE.CNF.DR = ''
                            END
                            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,MLT.AC,SSTE.MLT)
                        END ELSE
                            SSTE.MLT = ''
                        END
                    END
                    IF SSTE.MLT NE '' AND STE.NRV2 EQ '' THEN
                        SSTE = SSTE.MLT
                    END

                    IF STE.REF[1,2] EQ 'TF' AND CN.FLAG EQ '' THEN
                        IF STE.SID EQ 'LCD' THEN
                            CALL F.READ(FN.LCD,STE.REF,R.LCD,F.LCD,ER.LCD)
                            IF STE.LCY LE 0 THEN
                                LC.AC = R.LCD<TF.DR.DRAWDOWN.ACCOUNT>
                            END ELSE
                                LC.AC = R.LCD<TF.DR.PAYMENT.ACCOUNT>
                            END
                        END
                        IF STE.SID EQ 'LCM' THEN
                            CALL F.READ(FN.LCM,STE.REF,R.LCM,F.LCM,ER.LCM)
                            IF STE.LCY LE 0 THEN
                                LC.AC = R.LCM<TF.LC.PROVIS.ACC>
                            END ELSE
                                LC.AC = R.LCM<TF.LC.CREDIT.PROVIS.ACC>
                            END
                        END
                        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,LC.AC,SSTE.LC)
                    END ELSE
                        SSTE.LC = ''
                    END
                    IF SSTE.LC NE '' THEN
                        SSTE = SSTE.LC
                    END
                    IF STE.REF[1,2] EQ 'BR' THEN
                        CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,STE.REF,STE.BR.LCL)
                        STE.BR.BANK = STE.BR.LCL<1,BRLR.BANK>
                        STE.CHQ.TYPE = STE.BR.LCL<1,BRLR.BIL.CHQ.TYPE>
                        STE.CHQ.STAT = STE.BR.LCL<1,BRLR.BR.STATUS>
                    END ELSE
                        STE.BR.BANK  = ''
                        STE.CHQ.TYPE = ''
                        STE.CHQ.STAT = ''
                    END
*                    IF STE.TTYP EQ 'AC08' OR STE.TTYP EQ 'AC09' OR STE.TTYP EQ 'AC11' OR STE.TTYP EQ 'IC02' THEN
*                        IF STE.NRV2[2] EQ '00' OR STE.NRV2[2] EQ COMP[2] OR STE.NRV2[2] EQ '' THEN
*                            IF MMBR NE '' THEN
*                                IF SSTE EQ 'EG0010099' THEN
*                                    IF FT.DR.CAT1 NE '16151' THEN
*                                        SSTE = 'EG0010088'
*                                    END
*                                END
*                            END ELSE
*                                IF CN.FLAG EQ '' THEN
*                                    IF SSTE EQ 'EG0010099' THEN
*                                        IF FT.DR.CAT1 NE '16151' THEN
*                                            SSTE = 'EG0010088'
*                                        END
*                                    END
*                                END
*                            END
*                        END
*                    END
*                    IF ORD.BNK[1,2] EQ 'BR' AND ARAB.INT EQ '99400500' THEN
*                        IF STE.CHQ.STAT EQ 'IN' THEN
*                            IF MMBR NE '' THEN
*                                IF SSTE EQ 'EG0010099' THEN
*                                    SSTE = 'EG0010088'
*                                END
*                            END ELSE
*                                IF CN.FLAG EQ '' THEN
*                                    SSTE = 'EG0010088'
*                                END
*                            END
*                        END
*                    END
*                    IF ARAB.INT EQ '99400500' AND STE.CHQ.STAT EQ 'IN' THEN
*                        IF SSTE EQ 'EG0010099' THEN
*                            SSTE = 'EG0010088'
*                        END
*                    END
*                    IF STE.REF[1,3] EQ 'CBE' THEN
*                        IF SSTE EQ 'EG0010099' THEN
*                            SSTE = 'EG0010088'
*                        END
*                    END
*                    IF STE.REF[1,2] EQ 'IN' AND ( MLT.MK EQ 'CBE' ) THEN
* OR MLT.DEPT EQ '88') THEN
*                        IF SSTE EQ 'EG0010099' THEN
*                            SSTE = 'EG0010088'
*                        END
*                    END
*                    IF STE.REF[1,2] EQ 'BR' AND STE.LCY LT 0 AND STE.BR.BANK NE '9902' AND STE.BR.BANK NE '9901' THEN
*                        IF STE.NRV2[2] EQ '00' OR STE.NRV2[2] EQ COMP[2] OR STE.NRV2[2] EQ '' THEN
*                            IF CN.FLAG EQ '' THEN
*                                IF STE.CHQ.TYPE NE 10 THEN
*                                    IF STE.CHQ.STAT EQ 'IN' THEN
*                                        IF SSTE EQ 'EG0010099' THEN
*                                            SSTE = 'EG0010088'
*                                        END
*                                    END
*                                END
*                            END
*                        END
*                    END
*                    IF STE.REF[1,3] EQ 'SCB' AND STE.SID EQ 'CQ' THEN
*                        SSTE = 'EG0010088'
*                    END
*                    IF SSTE EQ COMP THEN
*                        IF LLLY EQ TD1 THEN
*                            SSTE = 'EG0010099'
*                        END
*                    END
                    IF CN.FLAG EQ 'CONF' THEN
*??AND SSTE[2] NE '99' THEN
                        STE.CNF.DR = 'CONFIRMATION'
                        STE.CNF.CR = 'CONFIRMATION'
                    END
*??                    IF STE.VER NE ',SCB.CONFIRM' AND STE.VER NE ',SCB.CONFIRM2' AND STE.VER NE ',SCB.CHQ.1' AND STE.VER NE ',SCB.CRT1' AND STE.VER NE ',SCB.CHQ.CUS.AMT.OLDSYS.BR' AND COM.ID NE 'EG0010099' AND STE.REF[1,2] NE 'IN' THEN
* AND STE.DPT NE '88'
*                        IF STE.SID EQ 'LC' OR STE.SID EQ 'LCD' OR STE.SID EQ 'LCM' THEN
*                            STE.CNF.DR = 'CONFIRMATION'
*                            STE.CNF.CR = 'CONFIRMATION'
*                        END

*??                        IF STE.REF[1,3] EQ 'SCB' AND STE.SID EQ 'CQ' THEN
*??                            STE.CNF.DR = 'CONFIRMATION'
*??                            STE.CNF.CR = 'CONFIRMATION'
*??                        END

*??                        IF STE.REF[1,2] EQ 'BR' AND STE.SID EQ 'CQ' AND STE.CRF EQ 'CREDIT' THEN
*??                            IF SSTE[2] NE '99' THEN
*??                                STE.CNF.DR = 'CONFIRMATION'
*??                                STE.CNF.CR = 'CONFIRMATION'
*??                            END
*??                        END

                    IF STE.CHQ.STAT EQ 'IN' AND SSTE[2] EQ '88' THEN
                        STE.CNF.DR = 'CONFIRMATION'
                        STE.CNF.CR = 'CONFIRMATION'
                    END
                    IF SSTE[2] EQ '88' AND STE.LCY LE 0 THEN
                        IF FT.REC.STAT NE 'REVE' THEN
                            STE.CNF.DR = 'CONFIRMATION'
                            STE.CNF.CR = 'CONFIRMATION'
                        END
                    END
*??                    END
*****************************************************
                    IF SSTE EQ COM.ID AND STE.BOOK.DATE EQ TD1 THEN
                        IF STE.STA NE 'REVE' THEN
*****************************************************
                            K++
                            IF STE.LCY LE 0 THEN
                                IF STE.CNF.DR EQ 'CONFIRMATION' THEN
                                    AMT.IN.DR   = STE.LCY
                                    TOT.IN.DR  += AMT.IN.DR
                                    CCF = 'CONF'
                                END ELSE
                                    AMT.OUT.DR  = STE.LCY
                                    TOT.OUT.DR += AMT.OUT.DR
                                    CCF = ''
                                END
                            END ELSE
                                IF STE.CNF.CR EQ 'CONFIRMATION' THEN
                                    AMT.IN.CR   = STE.LCY
                                    TOT.IN.CR  += AMT.IN.CR
                                    CCF = 'CONF'
                                END ELSE
                                    AMT.OUT.CR  = STE.LCY
                                    TOT.OUT.CR += AMT.OUT.CR
                                    CCF = ''
                                END
                            END
*****************************************************
*>>>>>>>>>>>>>>>>>>>>>>>>
                            GOSUB RETT.DATA
*>>>>>>>>>>>>>>>>>>>>>>>>
                        END
                    END
                REPEAT
            REPEAT
        NEXT CMM
    END
    RETURN
*===============================================
RETT.DATA:
*--------
    Y.HQ.DATA<-1> = COM.ID:"*":STE.VLD:"*":STE.REF:"*":STE.CUR:"*":STE.LCY:"*":STE.FCY:"*":CCF:"*":TTTT:"*":STE.ID:"*":STE.SID:"*":CUR.TXT:"*":CC.TXT
    RETURN
END
