* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>190</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOD.ATM.LOCK.REV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS

    FN.OFS.SOURCE    ="F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "AC.LOCKED.EVENTS"
    OFS.OPTION       = "ATM.123/R"
    OFS.USER.INFO    = "INPUTT99//EG0010099"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    COMMA            = ","
    KEY.LIST         = ""
    SELECTED         = ""
    ER.MSG           = ""

**---------------------------------------------------------------------**

    REV.DATE = TODAY
    HH       = "-30W"
    CALL CDT('',REV.DATE,HH)

    T.SEL = "SELECT FBNK.AC.LOCKED.EVENTS WITH FROM.DATE LE ": REV.DATE :" AND CO.CODE EQ EG0010099 AND INPUTTER LIKE ...ATM123... "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED

        OFS.MESSAGE.DATA = KEY.LIST<I>

        F.PATH = FN.OFS.IN

        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTION:COMMA:OFS.USER.INFO:COMMA:OFS.MESSAGE.DATA
        OFS.ID = "LOCK.REV.":KEY.LIST<I>:"-":REV.DATE

        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM


    NEXT I

    RETURN
END
