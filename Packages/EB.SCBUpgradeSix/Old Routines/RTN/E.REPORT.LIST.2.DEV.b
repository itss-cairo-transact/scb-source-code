* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.REPORT.LIST.2.DEV

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*-----------------------------------------------------------------------
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID
*-----------------------------------------------------------------------
***    PRINTER.IDX = R.USER<EB.USE.PRINTER.FOR.RPTS>
    PRINTER.IDX = R.USER<EB.USE.PRINTER.FOR.P.FUNC>

    PRINTER.ID = DOWNCASE(PRINTER.IDX)
    RPT.ID = UPCASE(PRINTER.IDX)
    REPORT.ID = 'BATCH.REPORT.':RPT.ID
    DEVICE.ID = ''

*-----------------------------------------------------------------------
    GOSUB DEVICE.NAME



    RETURN
*-----------------------------------------------------------------------
DEVICE.NAME:
    BEGIN CASE
    CASE PRINTER.ID = "fot1"
        DEVICE.ID = "flp2"

    CASE PRINTER.ID = "gwlp3"
        DEVICE.ID = "wlp3"

    CASE PRINTER.ID = "alt1"
        DEVICE.ID = "llp3"

    CASE PRINTER.ID = "gnlp1"
        DEVICE.ID = "nlp3"

    CASE PRINTER.ID = "gilp"
        DEVICE.ID = "ilp3"

    CASE PRINTER.ID = "gxlp3"
        DEVICE.ID = "xlp3"

    CASE PRINTER.ID = "ghlp2"
        DEVICE.ID = "hlp2"

    CASE PRINTER.ID = "het1"
        DEVICE.ID = "qlp3"

    CASE PRINTER.ID = "git1"
        DEVICE.ID = "jlp3"

    CASE PRINTER.ID = "mot1"
        DEVICE.ID = "elp3"

    CASE PRINTER.ID = "mat1"
        DEVICE.ID = "mlp4"

    CASE PRINTER.ID = "pot1"
        DEVICE.ID = "plp3"

    CASE PRINTER.ID = "tat1"
        DEVICE.ID = "ylp3"

*    CASE PRINTER.ID = "sat1"
*        DEVICE.ID = "clp2"

    CASE PRINTER.ID = "pho01mm"
        DEVICE.ID = "pho01mm"

*��� ����� ����� �����
    CASE PRINTER.ID = "pdm01"
        DEVICE.ID = ""

*    CASE PRINTER.ID = "letter"
*        DEVICE.ID = "letter"



*****             DEVICE.ID = ""
* IT
    CASE PRINTER.ID EQ "dupshadow"
        DEVICE.ID = "dup_shadow"
* IT
    CASE PRINTER.ID EQ "infotest"
        DEVICE.ID = ""

*    CASE PRINTER.ID = "ph011tre"
*        DEVICE.ID = "remote1"

* IT
    CASE PRINTER.ID EQ "t24t14"
        DEVICE.ID = "tmp2"

* IT
    CASE PRINTER.ID = "ibmhptest"
        DEVICE.ID = ""

* IT
    CASE PRINTER.ID EQ "p6400"
        DEVICE.ID = "p6400"

* IT
    CASE PRINTER.ID EQ "t24ts"
        DEVICE.ID = "t24tst"

* IT
    CASE PRINTER.ID EQ "t24it"
        DEVICE.ID = "t24it"

* 05                                   �������
    CASE PRINTER.ID EQ "pma01"
        DEVICE.ID = "mlp4"

* 06 ABOUR       ������ - ���� ���� ����
    CASE PRINTER.ID EQ "pab01"
        DEVICE.ID = "rlp4"
    CASE PRINTER.ID EQ "pab06cusp"
        DEVICE.ID = ""

    CASE PRINTER.ID EQ "ami2"
        DEVICE.ID = ""

* 07 OROBA       �������
    CASE PRINTER.ID EQ "por01"
        DEVICE.ID = "por01"

* 21 BURG    ��� ����� - ���� ����
    CASE PRINTER.ID EQ "pbu01"
        DEVICE.ID = "prg05"

* 23 AMRYA       ��������
    CASE PRINTER.ID EQ "pam01"
        DEVICE.ID = ""

* 32 MANSURA      �������� - �����
    CASE PRINTER.ID EQ "ptmn01"
        DEVICE.ID = "ptmn01"

* 40 ISMALIA                 ���������
    CASE PRINTER.ID EQ "pis01"
        DEVICE.ID = "pis01"

* 50 SUEZ               ������
    CASE PRINTER.ID EQ "psu01"
        DEVICE.ID = "wlp3"

* 70 RAMADAN     ������ �� ����� - ���� �����
    CASE PRINTER.ID EQ "pra01"
        DEVICE.ID = "vlp2"

    END  CASE
    RETURN

*--------------------------------------------------------------------
