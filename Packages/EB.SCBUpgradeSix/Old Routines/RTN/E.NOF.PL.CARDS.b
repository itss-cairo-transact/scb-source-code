* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***************MAHMOUD 14/9/2011****************
*-----------------------------------------------------------------------------
* <Rating>2177</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.PL.CARDS(Y.ALL.REC)
***    PROGRAM    E.NOF.PL.CARDS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 52 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
*#    TDAT = TODAY
*#    CALL ADD.MONTHS(TDAT,'-1')
*#    LDAY = TDAT
*#    CALL LAST.DAY(LDAY)
*#    PDATE = LDAY[1,6]:'01'

    Y.ALL.REC = ''
    ACCT.ID   = ''
*#    FROM.DATE = PDATE
*#    END.DATE  = LDAY
*#    Y.DATE    = FROM.DATE
*#CRT FROM.DATE:" * ":END.DATE
    KK = 0
    XX = SPACE(120)
    CTE.BAL  = ''
    CAT.GRP  = ''
    ACCT.ID  = ''
    CAT.GRP1  = ' 56001 56002 56019 56020 57001 57002 56001 56023 57003 57004 57005 57006 57007 57008 57009 57010 57011 57012 57013 57014 57015 57047 57051 '
    CAT.GRP2  = ' 56003 57016 57017 56003 56005 56018 56021 57018 56017 57019 57020 57021 57022 57023 57024 57025 57026 57027 57028 57029 57030 57031 57032 52649 62005 57048 57050 '
    CAT.GRP3  = ' 56004 56006 56007 56008 57033 57034 57035 57036 '
    CAT.GRP4  = ' 52023 52024 56009 56010 57037 56009 56011 56012 56013 57038 56014 56009 57038 57039 57040 57041 57042 52648 57043 56015 56016 57045 '
    CAT.GRP5  = ' 57044 '
*-----------------------------------
    EXP.CAT   = ' 56001 56002 56003 56005 56006 56007 56008 56009 56010 56011 56012 56013 56015 56016 56017 62005 56023 '
    INC.CAT   = ' 52023 52024 52648 52649 57001 57002 57003 57004 57005 57006 57007 57008 57009 57010 57011 57012 57013 57014 57015 57016 57017 57018 57019 57020 57021 57022 57023 57024 57025 57026 57027 57028 57029 57031 57032 57033 57034 57037 57038 57039 57040 57041 57042 57043 57044 57045 '
*-----------------------------------
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CAT = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = '' ; ER.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'F.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CTE = 'F.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.CTP = 'F.CATEG.ENTRY' ; F.CTP = '' ; R.CTP = '' ; ER.CTP = ''
    CALL OPF(FN.CTP,F.CTP)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CATEGORY.GROUP" IN D.FIELDS<1> SETTING YGRP.POS THEN CATEG.GROUP  = D.RANGE.AND.VALUE<YGRP.POS> ELSE RETURN
    LOCATE "START.DATE"     IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE    = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"       IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE     = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
***    PROMPT "CATEG GROUP :" ; INPUT CATEG.GROUP
    Y.CRNT.YEAR = END.DATE[1,4]
    Y.LAST.YEAR = Y.CRNT.YEAR - 1
    BEGIN CASE
    CASE CATEG.GROUP EQ 1
        CAT.GRP = CAT.GRP1
    CASE CATEG.GROUP EQ 2
        CAT.GRP = CAT.GRP2
        ACCT.ID = 'EGP1518600010099'
    CASE CATEG.GROUP EQ 3
        CAT.GRP = CAT.GRP3
    CASE CATEG.GROUP EQ 4
        CAT.GRP = CAT.GRP4
    CASE CATEG.GROUP EQ 5
        CAT.GRP = CAT.GRP5
    CASE OTHERWISE
        RETURN
    END CASE

    CUR.SEL  = "SELECT ":FN.CUR
    CALL EB.READLIST(CUR.SEL,K.LIST.CUR,'',SELECTED.CUR,CUR.ER.MSG)
    LOOP
        REMOVE CUR.ID FROM K.LIST.CUR SETTING POS.CUR
    WHILE CUR.ID:POS.CUR
        CC.SEL  = "SELECT ":FN.CAT:" WITH @ID IN (":CAT.GRP:")"
        CC.SEL := " BY @ID "
        CALL EB.READLIST(CC.SEL,K.LIST,'',SELECTED,ER.MSG)
        LOOP
            REMOVE CAT.ID FROM K.LIST SETTING POS.CAT
        WHILE CAT.ID:POS.CAT
            CALL F.READ(FN.CAT,CAT.ID,R.CAT,F.CAT,ER.CAT)
            CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,END.DATE,CAT.ID,CAT.LIST)
            GOSUB GET.OPEN.BAL
            CTE.BAL = OPENING.BAL
            TOT.DR.MVMT = 0
            TOT.CR.MVMT = 0
            TOT.EXP     = 0
            TOT.INC     = 0
            LOOP
                REMOVE CTE.ID FROM CAT.LIST SETTING POS
            WHILE CTE.ID:POS
                CALL F.READ(FN.CTE,CTE.ID,R.CTE,F.CTE,ER.CTE)
                CTE.CUR = R.CTE<AC.CAT.CURRENCY>
                IF CTE.CUR EQ CUR.ID THEN
                    CTE.NRR = R.CTE<AC.CAT.NARRATIVE>
*Line [ 157 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    IF CTE.NRR EQ 'YEAR END PL ENTRY ':Y.LAST.YEAR:'1231C' OR CTE.NRR EQ 'YEAR END PL ENTRY ':Y.LAST.YEAR:'1231CL' THEN GOTO NXT.REC ELSE NULL
*                    IF NOT(INDEX('YEAR END PL ENTRY 20141231',CTE.NRR,1)) THEN  ;*GOTO NXT.REC ELSE NULL
*                    IF CTE.CUR EQ LCCY THEN
                    CTE.AMT = R.CTE<AC.CAT.AMOUNT.LCY>
*                    END ELSE
*                        CTE.AMT = R.CTE<AC.CAT.AMOUNT.FCY>
*                    END
                    CTE.DAT = R.CTE<AC.CAT.BOOKING.DATE>
                    CTE.VAL = R.CTE<AC.CAT.VALUE.DATE>
                    CTE.TXN = R.CTE<AC.CAT.TRANSACTION.CODE>
                    CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,CTE.TXN,CTE.TRN)
                    CTE.OUR = R.CTE<AC.CAT.OUR.REFERENCE>
                    IF CTE.OUR EQ '' THEN
                        CTE.OUR = R.CTE<AC.CAT.TRANS.REFERENCE>
                    END
                    CTE.INP1 = R.CTE<AC.CAT.INPUTTER>
                    CTE.AUT1 = R.CTE<AC.CAT.AUTHORISER>
                    CTE.INP  = FIELD(CTE.INP1,'_',2)
                    CTE.AUT  = FIELD(CTE.AUT1,'_',2)
                    CTE.BAL += CTE.AMT
                    FINDSTR CAT.ID:' ' IN EXP.CAT SETTING POS.EXP THEN TOT.EXP += CTE.AMT ELSE TOT.INC += CTE.AMT
                    IF CTE.AMT LT 0 THEN
                        TOT.DR.MVMT += CTE.AMT
                    END ELSE
                        TOT.CR.MVMT += CTE.AMT
                    END
                    IF CTE.ID THEN
                    END
                END
*                END
NXT.REC:
            REPEAT
            TOT.ALL = ABS(OPENING.BAL) + ABS(TOT.DR.MVMT) + ABS(TOT.CR.MVMT) + ABS(CTE.BAL)
            IF TOT.ALL NE 0 THEN
                GOSUB RET.DATA
            END
        REPEAT
    REPEAT
    GOSUB ACCT.TRANS
    RETURN
**************************************
GET.OPEN.BAL:
*-----------
    OPENING.BAL = 0
    SS.YEAR = FROM.DATE[1,4]
    SS.DATE = SS.YEAR:'0101'
    EE.DATE = FROM.DATE
    CALL CDT("",EE.DATE,'-1C')

    CALL GET.CATEG.MONTH.ENTRIES(SS.DATE,EE.DATE,CAT.ID,CAT.PRV.LIST)
    LOOP
        REMOVE CTP.ID FROM CAT.PRV.LIST SETTING POS.CTP
    WHILE CTP.ID:POS.CTP
        CALL F.READ(FN.CTP,CTP.ID,R.CTP,F.CTP,ER.CTP)
        CTP.NRR = R.CTP<AC.CAT.NARRATIVE>
*Line [ 213 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        IF CTP.NRR EQ 'YEAR END PL ENTRY ':Y.LAST.YEAR:'1231C' OR CTP.NRR EQ 'YEAR END PL ENTRY ':Y.LAST.YEAR:'1231CL' THEN GOTO NXT.REC2 ELSE NULL
        CTP.CUR = R.CTP<AC.CAT.CURRENCY>
        IF CTP.CUR EQ CUR.ID THEN
*            IF CTP.CUR EQ LCCY THEN
            OPENING.BAL += R.CTP<AC.CAT.AMOUNT.LCY>
*            END ELSE
*                OPENING.BAL += R.CTP<AC.CAT.AMOUNT.FCY>
*            END
        END

NXT.REC2:

    REPEAT
    RETURN
**************************************
ACCT.TRANS:
*----------
    IF ACCT.ID THEN
        CAT.ID = ACCT.ID
        CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
        CTE.BAL = OPENING.BAL
        TOT.DR.MVMT = 0
        TOT.CR.MVMT = 0
        TOT.EXP     = 0
        TOT.INC     = 0
        LOOP
            REMOVE STE.ID FROM ID.LIST SETTING POS
        WHILE STE.ID:POS
            CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
            CTE.CUR = R.STE<AC.STE.CURRENCY>
            CTE.AMT = R.STE<AC.STE.AMOUNT.LCY>
            CTE.DAT = R.STE<AC.STE.BOOKING.DATE>
            CTE.VAL = R.STE<AC.STE.VALUE.DATE>
            CTE.OUR = R.STE<AC.STE.OUR.REFERENCE>
            CTE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
*            IF CTE.CUR NE LCCY THEN
*                CTE.AMT = R.STE<AC.STE.AMOUNT.FCY>
*            END
            FINDSTR CAT.ID:' ' IN EXP.CAT SETTING POS.EXP THEN TOT.EXP += CTE.AMT ELSE TOT.INC += CTE.AMT
            CTE.BAL += CTE.AMT
            IF CTE.AMT LT 0 THEN
                TOT.DR.MVMT += CTE.AMT
            END ELSE
                TOT.CR.MVMT += CTE.AMT
            END
        REPEAT
        TOT.ALL = ABS(OPENING.BAL) + ABS(TOT.DR.MVMT) + ABS(TOT.CR.MVMT) + ABS(CTE.BAL)
        IF TOT.ALL NE 0 THEN
            GOSUB RET.DATA
        END
    END
    RETURN
**************************************
RET.DATA:
*--------
    TOT.CR.DR = TOT.CR.MVMT + TOT.DR.MVMT
    Y.ALL.REC<-1> = COMP:"*":CATEG.GROUP:"*":CUR.ID:"*":CAT.ID:"*":FROM.DATE:"*":END.DATE:"*":OPENING.BAL:"*":TOT.DR.MVMT:"*":TOT.CR.MVMT:"*":TOT.CR.DR:"*":CTE.BAL:"*":TOT.EXP:"*":TOT.INC
    RETURN
**************************************
END
