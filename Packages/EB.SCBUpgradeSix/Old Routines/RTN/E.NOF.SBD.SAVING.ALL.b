* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>970</Rating>
*------------WRITTEN BY MOHAMED MOSTAFA  IN 2017/10/15-----------------------------------------------------------------
    SUBROUTINE E.NOF.SBD.SAVING.ALL(Y.SAV.DATA)
**    PROGRAM E.NOF.SBD.SAVING.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------

    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-------------------------------------------------------------------------
INITIATE:

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    RETURN

*========================================================================
PROCESS:
    AMT.25.6502  = 0 ;      AMT.25.6501   = 0   ;   AMT.25.6503   = 0  ;  AMT.25.6504   = 0
    AMT.500.6502 = 0 ;      AMT.500.6501  = 0   ;   AMT.500.6503  = 0  ;  AMT.500.6504  = 0
    AMT.2.6502   = 0 ;      AMT.2.6501    = 0   ;   AMT.2.6503    = 0  ;  AMT.2.6504    = 0
    AMT.5.6502   = 0 ;      AMT.5.6501    = 0   ;   AMT.5.6503    = 0  ;  AMT.5.6504    = 0
    AMT.5M.6502  = 0 ;      AMT.5M.6501   = 0   ;   AMT.5M.6503    = 0  ; AMT.5M.6504    = 0

** DEBUG
***6501 6502 6503 6504         �����
***6505 6506 6507 6508         ����� ������
***6514 6517 6516 6517         ����� �������
*-------------------------------------------
    T.SEL  = "SELECT ":FN.AC:" WITH CATEGORY IN (6502 6501 6503 6504 6505 6506 6507 6508 6514 6515 6516 6517) AND OPEN.ACTUAL.BAL GT 0 AND CURRENCY EQ 'EGP'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)

            AMT = R.AC<AC.OPEN.ACTUAL.BAL>
            IF R.AC<AC.CATEGORY> EQ 6502 OR R.AC<AC.CATEGORY> EQ 6505 OR  R.AC<AC.CATEGORY> EQ 6514 THEN

                IF AMT LT 250000 THEN
                    AMT.25.6502 += AMT
                END
                IF AMT GE 250000 AND AMT LE 500000 THEN
                    AMT.500.6502 += AMT
                END

                IF AMT GT 500000 AND AMT LE 2000000 THEN
                    AMT.2.6502 += AMT
                END

                IF AMT GT 2000000 AND AMT LE 5000000 THEN
                    AMT.5.6502 += AMT
                END

                IF AMT GT 5000000 THEN
                    AMT.5M.6502 += AMT

                END
            END
*------------------------------------------------------------------------------------
            IF R.AC<AC.CATEGORY> EQ 6501 OR R.AC<AC.CATEGORY> EQ 6506 OR R.AC<AC.CATEGORY> EQ 6515 THEN

                IF AMT LT 250000 THEN
                    AMT.25.6501 += AMT
                END

                IF AMT GE 250000 AND AMT LT 500000 THEN
                    AMT.500.6501 += AMT
                END

                IF AMT GE 500000 AND AMT LT 2000000 THEN
                    AMT.2.6501 += AMT
                END

                IF AMT GE 2000000 AND AMT LT 5000000 THEN
                    AMT.5.6501 += AMT
                END

                IF AMT GE 5000000 THEN
                    AMT.5M.6501 += AMT

                END
            END
*----------------------------------------------------------------------

            IF R.AC<AC.CATEGORY> EQ 6503 OR R.AC<AC.CATEGORY> EQ 6507 OR R.AC<AC.CATEGORY> EQ 6516 THEN

                IF AMT LT 250000 THEN
                    AMT.25.6503 += AMT
                END

                IF AMT GE 250000 AND AMT LE 500000 THEN
                    AMT.500.6503 += AMT
                END

                IF AMT GT 500000 AND AMT LE 2000000 THEN
                    AMT.2.6503 += AMT
                END

                IF AMT GT 2000000 AND AMT LE 5000000 THEN
                    AMT.5.6503 += AMT
                END

                IF AMT GT 5000000 THEN
                    AMT.5M.6503 += AMT

                END
            END
*--------------------------------------------------------------------

            IF R.AC<AC.CATEGORY> EQ 6504 OR R.AC<AC.CATEGORY> EQ 6508 OR R.AC<AC.CATEGORY> EQ 6517 THEN

                IF AMT LT 250000 THEN
                    AMT.25.6504 += AMT
                END

                IF AMT GE 250000 AND AMT LE 500000 THEN
                    AMT.500.6504 += AMT
                END

                IF AMT GT 500000 AND AMT LE 2000000 THEN
                    AMT.2.6504 += AMT
                END

                IF AMT GT 2000000 AND AMT LE 5000000 THEN
                    AMT.5.6504 += AMT
                END

                IF AMT GT 5000000 THEN
                    AMT.5M.6504 += AMT

                END
            END
*--------------------------------------------------------------------

        NEXT I

        Y.SAV.DATA<-1> =  "DESCRIPTION*SAVING FOR 1 MONTH*SAVING FOR 3 MONTH*SAVING FOR 6 MONTH*SAVING FOR 1 YEAR"
        Y.SAV.DATA<-1> := "LESS THAN       250000*":AMT.25.6502:"*":AMT.25.6501:"*":AMT.25.6503:"*": AMT.25.6504:"*"
        Y.SAV.DATA<-1> := "FROM 250000  TO 500000*":AMT.500.6502:"*":AMT.500.6501:"*":AMT.500.6503:"*":AMT.500.6504:"*"
        Y.SAV.DATA<-1> := "FROM 500000  TO 2000000*":AMT.2.6502:"*":AMT.2.6501:"*":AMT.2.6503:"*":AMT.2.6504:"*"
        Y.SAV.DATA<-1> := "FROM 2000000 TO 5000000*":AMT.5.6502:"*":AMT.5.6501:"*":AMT.5.6503:"*":AMT.5.6504:"*"
        Y.SAV.DATA<-1> := "GREATER THAN    5000000*":AMT.5M.6502:"*":AMT.5M.6501:"*":AMT.5M.6503:"*":AMT.5M.6504:"*"
    END
    RETURN
END
