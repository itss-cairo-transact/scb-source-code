* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOY.CONS.DELETE

**    PROGRAM EOY.CONS.DELETE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE

**------------------------------------------------------------------**

    FN.CONT = 'F.RE.STAT.LINE.CONT'     ; F.CONT = ''
    FN.CONS = 'F.CONSOLIDATE.PRFT.LOSS' ; F.CONS = ''

    CCC.NO = '' ; LEDG.NO = '' ; CURR.NO = ''
    CALL OPF(FN.CONT,F.CONT)
    CALL OPF(FN.CONS,F.CONS)

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""


    T.SEL    = "SELECT ":FN.CONT:" WITH ( @ID LIKE GENLED.9993.EG... OR @ID LIKE GENLED.9994.EG... OR @ID LIKE GENLED.9995.EG... ) AND @ID UNLIKE ...EG0010099 BY @ID "

**  T.SEL    = "SELECT ":FN.CONT:" WITH @ID EQ GENLED.9993.EG0010001 "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN

        FOR HH = 1 TO SELECTED
            CALL F.READ(FN.CONT,KEY.LIST<HH>,R.CONT,F.CONT,E1)
*Line [ 64 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.PRFT.CONSOL.KEY>,@VM)

            FOR I = 1 TO DECOUNT.CONT
                CONS.ID  = R.CONT<RE.SLC.PRFT.CONSOL.KEY,I>
                CALL F.READ(FN.CONS,CONS.ID,R.CONS,F.CONS,E2)
***                DELETE F.CONS , CONS.ID
                CALL F.DELETE(FN.CONS , CONS.ID)
                CALL JOURNAL.UPDATE('')
            NEXT I
        NEXT HH

    END

    RETURN
**---------------------------------------------------------------------**

END
