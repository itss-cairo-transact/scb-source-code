* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-9</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOD.HOLD.DEL
**    PROGRAM EOD.HOLD.DEL
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL

    DAT.DEL = TODAY
    DAT.DEL.2 = TODAY
    CALL CDT('EG', DAT.DEL, '-2W')
    CALL CDT('EG', DAT.DEL.2, '-5W')
    FN.HOLD.C  = 'F.HOLD.CONTROL'  ; F.HOLD.C = '' ; R.HOLD.C = ''
    CALL OPF(FN.HOLD.C,F.HOLD.C)

    FN.HOLD    = '&HOLD&'  ; F.HOLD = '' ; R.HOLD = ''
    CALL OPF(FN.HOLD,F.HOLD)
    KEY.LIST = "" ; SELECTED = ""; ER.MSG = ""

    T.SEL = "SELECT F.HOLD.CONTROL WITH (( REPORT.NAME UNLIKE ...GENLED... AND REPORT.NAME UNLIKE ...SCBPLFIN... ) AND RUN.IN.BATCH EQ N AND DATE.CREATED LT ": DAT.DEL :" ) OR DATE.CREATED LT " : DAT.DEL.2

**    T.SEL = "SELECT F.HOLD.CONTROL WITH RUN.IN.BATCH EQ N AND DATE.CREATED LT ":DAT.DEL
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
        PRINT "I = ":I

        CALL F.READ(FN.HOLD,KEY.LIST<I>,R.HOLD,F.HOLD,READ.ERR)

****        DELETE F.HOLD , KEY.LIST<I> ON ERROR
****            PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.HOLD
****        END
        CALL F.DELETE(FN.HOLD , KEY.LIST<I>)

        CALL F.READ(FN.HOLD.C,KEY.LIST<I>,R.HOLD.C,F.HOLD.C,READ.ERR.C)
****        DELETE F.HOLD.C , KEY.LIST<I> ON ERROR
****            PRINT "CANNOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.HOLD.C
****        END
        CALL F.DELETE(FN.HOLD.C , KEY.LIST<I>)
        CALL JOURNAL.UPDATE('')
    NEXT I

*-----------------------------------------------------------------------*

    RETURN
END
