* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>450</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.TXT(Y.RET.DATA)
**  PROGRAM  E.NOF.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH

*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
**  COM.CODE = 40
**    DAL.DAYZ = TODAY
**    CALL CDT('',DAL.DAYZ,"+1W")

**   DAL.YEAR  = DAL.DAYZ[1,4]
**   DAL.MONTH = DAL.DAYZ[5,2]
**   DAL.DAY   = DAL.DAYZ[7,2]

    LOCATE "BRANCH" IN D.FIELDS<1> SETTING BRN.POS THEN
        COM.CODEE  = D.RANGE.AND.VALUE<BRN.POS>
    END

**------------
    USR.DEP    = "1700":COM.CODEE
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
*
    DAL.TODAY = TODAY
    IF LOCT EQ '1'  THEN
        CALL CDT('',DAL.TODAY,"+1W")
    END ELSE
        CALL CDT('',DAL.TODAY,"+2W")
    END


    DAL.DAYZ = DAL.TODAY
*   CALL CDT('',DAL.DAYZ,"+1W")

    DAL.YEAR  = DAL.DAYZ[1,4]
    DAL.MONTH = DAL.DAYZ[5,2]
    DAL.DAY   = DAL.DAYZ[7,2]
    COM.CODE  = TRIM(COM.CODEE, "0", "L")
**--------------
    IDD =  DAL.YEAR:"-":DAL.MONTH:"-":DAL.DAY:"-":"Sess2OUT":COM.CODE:".txt"

**  TEXT= "IDD=  "  : IDD ; CALL REM

    Path = "/hq/opce/bclr/user/dltx/":IDD
*    Path = "../bnk.data/OFS/OFS.BK/":IDD

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 0
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            AMTT              = FIELD(Line,",",10)
            IF AMTT THEN
                I = I + 1
                AMTTT = AMTT + AMTTT
            END
        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    Y.RET.DATA<-1> = I:'*':AMTTT:'*':COM.CODE:'*':DAL.DAYZ

    RETURN
END
