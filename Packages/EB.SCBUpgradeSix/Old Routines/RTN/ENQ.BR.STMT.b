* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>46</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.BR.STMT(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*********************************************




    KEY.LIST = '' ; SELECTED = '' ; S.VR = ''
    T.SEL = 'SELECT F.SCB.BR.TRANSACTION '
*T.SEL = 'SELECT F.SCB.BR.TRANSACTION WITH DEBIT.VALUE.DATE LIKE 200509... AND CHQ.STATUS EQ "8"'

    CRITERIA.CNT = DCOUNT(ENQ<2>, @VM)
    IF CRITERIA.CNT THEN
        T.SEL := " WITH "
        FOR CRITERIA.LP = 1 TO CRITERIA.CNT
            IF ENQ<3,CRITERIA.LP> = "LK" THEN
                ENQ<3,CRITERIA.LP> = "LIKE"
            END
            IF ENQ<3,CRITERIA.LP> = "UL" THEN
                ENQ<3,CRITERIA.LP> = "UNLIKE"
            END
            IF ENQ<2,CRITERIA.LP> = "CHQ.STATUS" AND ENQ<4,CRITERIA.LP> = '20' THEN
                ENQ<4,CRITERIA.LP> = ''
            END
            IF ENQ<3,CRITERIA.LP> # "RG" THEN
                IF CRITERIA.LP > 1 THEN
                    T.SEL := " AND "
                END
                T.SEL := ENQ<2,CRITERIA.LP>:' ':ENQ<3,CRITERIA.LP>:' ':ENQ<4,CRITERIA.LP>
            END
        NEXT CRITERIA.LP

    END

    T.SEL := " BY CHQ.STATUS BY ID.BR"
*TEXT = T.SEL ; CALL REM

    CALL EB.READLIST(T.SEL, KEY.LIST, '', SELECTED, S.VR)
*T.SEL := ' AND ':ENQ<2,J>:' ':ENQ<3,J>:' ':ENQ<4,J>
*TEXT = SELECTED: " RECORD SELECTED " ; CALL REM
    I ="" ; BR.LP = ''
    FOR BR.LP = 1 TO SELECTED
        IF KEY.LIST<BR.LP>[1,12] # KEY.LIST<BR.LP-1>[1,12] THEN
            I += 1
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<BR.LP>
*TEXT =  ENQ<2,I>:' ':ENQ<3,I>:' ':ENQ<4,I> ; CALL REM
        END
    NEXT BR.LP


    RETURN
END
