* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 26/02/2012*********************************
*-----------------------------------------------------------------------------
* <Rating>-44</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.VISA.CHK.BR(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.FLAG

*-----------------------------------------------------------------------*
    COUNT.FLG = 0
    COMP.CO = ''
    T.DATE = TODAY
    YYYYMM = TODAY[1,6]
    TEXT = 'YEAR.MM=':YYYYMM ; CALL REM
    T.SEL = "SELECT F.COMPANY WITH (@ID NE EG0010099 AND @ID NE EG0010088) BY @ID "

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
   ** TEXT = 'SEL=':SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
        COMP.CO = KEY.LIST<I>
       ** TEXT = 'COMP=':COMP.CO ; CALL REM
        CHK.SEL = "SELECT F.SCB.VISA.PAY.FLAG WITH PAYMENT.FLAG EQ 'YES' AND YEAR.MM EQ ":YYYYMM : " AND COMPANY.CO EQ ": COMP.CO
        KEY.LIST.CHK=""
        SELECTED.CHK=""
        ER.MSG.CHK=""

        CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
       ** TEXT = 'COMPNOT=':KEY.LIST<I> ; CALL REM
        IF NOT(SELECTED.CHK) THEN
            COUNT.FLG = COUNT.FLG + 1
            Y.RET.DATA<-1> = COUNT.FLG:"*":COMP.CO
           COMP.CO = ''
        END
    NEXT I
*-----------------------------------------------------------------------*
    RETURN
END
