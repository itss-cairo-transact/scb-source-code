* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-9</Rating>
*-----------------------------------------------------------------------------
***   PROGRAM EOD.BASE.CCY
    SUBROUTINE EOD.BASE.CCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    DD1 = TODAY[1,6]:"15"
    DD2 = TODAY[1,6]:"25"

    IF TODAY GE DD1 AND  TODAY LT DD2 THEN

        FN.CURR = 'FBNK.RE.BASE.CCY.PARAM' ;F.CURR = '' ; R.CURR = ''
        CALL OPF(FN.CURR,F.CURR)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""
        T.SEL = "SELECT FBNK.RE.BASE.CCY.PARAM WITH @ID EQ NZD "

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        FOR I = 1 TO SELECTED
            PRINT "I = ":I
            CALL F.READ(FN.CURR,KEY.LIST<I>,R.CURR,F.CURR,READ.ERR)

            ID.NEW = 'EOM'
            CALL F.WRITE (FN.CURR, ID.NEW , R.CURR )
****            WRITE  R.CURR TO F.CURR , ID.NEW ON ERROR
****                PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CURR
****            END
        NEXT I

    END
************************************************************

    RETURN
END
