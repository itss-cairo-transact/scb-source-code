* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.SCB.BR.SLIPS(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""


    VERSION.NAME = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.VERSION.NAME>

    IF VERSION.NAME = ",SCB.CHQ.LCY.REG1" OR VERSION.NAME = ",SCB.CHQ.LCY.REG1.COLL" OR VERSION.NAME = ",SCB.CHQ.LCY.REG2" OR  VERSION.NAME = ",SCB.CHQ.LCY.REG.H" OR VERSION.NAME = ",SCB.CHQ.LCY.REG.HH" THEN
        T.SEL = "SELECT FBNK.SCB.BR.SLIPS WITH RECEIVE.DATE EQ ": TODAY :" AND CO.CODE EQ ":COMP:" AND CURRENCY EQ 'EGP' AND( OPERATION.TYPE EQ '����� �����' OR OPERATION.TYPE EQ '� ���� �����')"
    END
    IF VERSION.NAME = ",SCB.BILLS.H"  THEN
        T.SEL = "SELECT FBNK.SCB.BR.SLIPS WITH RECEIVE.DATE EQ ": TODAY :" AND CO.CODE EQ ":COMP:" AND OPERATION.TYPE EQ '�������� �����' OR OPERATION.TYPE EQ '�������� �����'"
    END
    IF VERSION.NAME = ",SCB.CHQ.OUT.OF.CLEARING" OR VERSION.NAME = ",SCB.CHQ.OUT.OF.CLEARING.NOT" THEN
        T.SEL = "SELECT FBNK.SCB.BR.SLIPS WITH RECEIVE.DATE EQ ": TODAY :" AND CO.CODE EQ ":COMP:" AND CURRENCY NE 'EGP' AND OPERATION.TYPE EQ '����� �����'"
    END

    IF VERSION.NAME = "" THEN
        T.SEL = "SELECT FBNK.SCB.BR.SLIPS WITH CO.CODE EQ ":COMP
    END

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "�� ���� ����� �����"
    END

    RETURN
END
