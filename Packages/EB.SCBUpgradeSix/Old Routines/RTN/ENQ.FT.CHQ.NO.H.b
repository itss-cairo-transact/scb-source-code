* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-37</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.FT.CHQ.NO.H

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

*****

    ATMM   = FIELD(O.DATA,'*',1)
    FT.ID  = FIELD(O.DATA,'*',2)
    REF.NO = FIELD(O.DATA,'*',3)
    ST.ID  = FIELD(O.DATA,'*',4)
    O.DATA = FT.ID


    TR.ID   = O.DATA
    TR.ID.H = O.DATA:";1"
    FN.FT   = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''

    FN.BR   = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    FN.BR.H = 'FBNK.BILL.REGISTER$HIS' ; F.BR.H = '' ; R.BR.H = ''

    FN.IN   = 'F.INF.MULTI.TXN' ; F.IN= '' ; R.IN = ''

    FN.ST   = 'FBNK.STMT.ENTRY' ; F.ST= '' ; R.ST = ''
    FN.DR   = 'FBNK.DRAWINGS' ; F.DR= '' ; R.DR = ''
    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.FT.H,F.FT.H)
    CALL OPF( FN.BR,F.BR)
    CALL OPF( FN.BR.H,F.BR.H)
    CALL OPF( FN.IN,F.IN)
    CALL OPF( FN.ST,F.ST)
    CALL OPF( FN.DR,F.DR)
    IF ATMM [1,3] NE 'ATM' AND ATMM [1,3] NE 'POS' THEN
        CALL F.READ( FN.FT,TR.ID, R.FT, F.FT, ETEXT)
        IF NOT(ETEXT) THEN
            OR.BNK = R.FT< FT.LOCAL.REF,FTLR.CHEQUE.NO>
            O.DATA = OR.BNK
        END ELSE
            CALL F.READ( FN.FT.H,TR.ID.H, R.FT.H, F.FT.H, ETEXT2)
            OR.BNK = R.FT.H<FT.LOCAL.REF,FTLR.CHEQUE.NO>
            O.DATA = OR.BNK
        END
    END

    IF REF.NO [1,2] EQ 'FT' THEN
        TR.ID.ATM  = FT.ID[1,12]
        CALL F.READ(FN.FT,TR.ID.ATM, R.FT, F.FT, ETEXT)
        IF NOT(ETEXT) THEN
            IF R.FT<FT.ORDERING.BANK> EQ 'ATM' THEN
****************UPDATED BY MAHMOUD 15/1/2013*********************
*#                ATM.REF = R.FT< FT.LOCAL.REF,FTLR.RETRIVAL.REF.NO>
                ATM.REF = R.FT< FT.LOCAL.REF,FTLR.TERMINAL.LOC>
                ATM.REF = CHANGE(ATM.REF,'-',' ')
                ATM.REF = TRIM(ATM.REF)
                ATM.REF = CHANGE(ATM.REF,' ','-')
**************** END OF UPDATE **********************************
                O.DATA = ATM.REF
            END
        END ELSE
            TR.ID.ATM.H =  FT.ID[1,12] : ';1'
            CALL F.READ( FN.FT.H,TR.ID.ATM.H, R.FT.H, F.FT.H, ETEXT2)
            IF R.FT.H<FT.ORDERING.BANK> EQ 'ATM' THEN
****************UPDATED BY MAHMOUD 15/1/2013*********************
*#                ATM.REF = R.FT.H<FT.LOCAL.REF,FTLR.RETRIVAL.REF.NO>
                ATM.REF = R.FT.H< FT.LOCAL.REF,FTLR.TERMINAL.LOC>
                ATM.REF = CHANGE(ATM.REF,'-',' ')
                ATM.REF = TRIM(ATM.REF)
                ATM.REF = CHANGE(ATM.REF,' ','-')
**************** END OF UPDATE **********************************
                O.DATA = ATM.REF
            END
        END
    END
    IF REF.NO [1,2] EQ 'BR' THEN
        CALL F.READ( FN.BR,REF.NO, R.BR, F.BR, ETEXT)
        IF NOT(ETEXT) THEN
            OR.BNK = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.NO>
            O.DATA = OR.BNK
        END ELSE
            CALL F.READ( FN.BR.H,REF.NO:";1", R.BR.H, F.BR.H, ETEXT21)
            OR.BNK = R.BR.H<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.NO>
            O.DATA = OR.BNK
        END
    END

    IF REF.NO [1,2] EQ 'IN' THEN
        CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT111)
        NOO = FIELD(R.ST<AC.STE.OUR.REFERENCE>,"-",2)
        O.DATA = R.ST<AC.STE.THEIR.REFERENCE>
        CALL F.READ( FN.IN,REF.NO, R.IN, F.IN, ETEXT1)
        IF NOT(ETEXT1) THEN
            OR.BNK = R.IN<INF.MLT.CHEQUE.NUMBER><1,NOO>
            IF R.IN<INF.MLT.CHEQUE.NUMBER><1,NOO> NE '' THEN
                O.DATA = OR.BNK
            END
        END
    END
**-------------------------20100316-----------------------------**

    IF REF.NO [1,2] EQ 'BT' THEN
        CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT111)
        ACC = R.ST<AC.STE.ACCOUNT.NUMBER>

        IF ACC EQ '9943330010508001' THEN
            OR.REF = R.ST<AC.STE.THEIR.REFERENCE>
            O.DATA = OR.REF
        END
    END

**-------------------------20100316-----------------------------**
************ADDED BY MAHMOUD 14/12/2009******************
    IF REF.NO [1,2] EQ 'TT' THEN
        CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT111)
        OR.BNK = R.ST<AC.STE.THEIR.REFERENCE>
        O.DATA = OR.BNK
    END
**********************************
    IF REF.NO [1,2] EQ 'TF' THEN
        REF.NO1 = REF.NO [1,14]
        CALL F.READ( FN.DR,REF.NO1, R.DR, F.DR, ETEXT1111)
        OUR.REF =  R.DR<TF.DR.LOCAL.REF,DRLR.OUR.REFERENCE>
        REF = R.DR<TF.DR.LOCAL.REF,DRLR.REFRENCE>
        IF OUR.REF EQ '' THEN
            OUR.REF = REF
        END

        O.DATA = OUR.REF
    END

*********************************************************
    IF REF.NO [1,2] EQ 'FT' THEN

        CALL F.READ( FN.FT,FT.ID[1,12], R.FT, F.FT, ETEXT311)
        FT.REF = R.FT<FT.DEBIT.THEIR.REF>
        DB.ACC = R.FT<FT.DEBIT.ACCT.NO>
        CR.ACC = R.FT<FT.CREDIT.ACCT.NO>
        IF (DB.ACC EQ '9942310020200102' OR CR.ACC EQ '9942310020200102') THEN
            O.DATA = FT.REF
        END
        IF ETEXT311 THEN
            FT.ID.HIS = FT.ID[1,12]:';1'
            CALL F.READ( FN.FT.H,FT.ID.HIS, R.FT.H, F.FT.H, ETEXT211)
            FT.REF.H = R.FT.H<FT.DEBIT.THEIR.REF>
            DB.ACC.H = R.FT.H<FT.DEBIT.ACCT.NO>
            CR.ACC.H = R.FT.H<FT.CREDIT.ACCT.NO>
            IF (DB.ACC.H EQ '9942310020200102' OR CR.ACC.H EQ '9942310020200102') THEN
                O.DATA = FT.REF.H
            END
        END
    END
    O.DATA = O.DATA[1,20]

**-----------------------------------------------------------
    IF REF.NO [1,2] EQ 'IN' THEN
        IF O.DATA EQ '' THEN
            CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT111)
            ST.REF = R.ST<AC.STE.THEIR.REFERENCE>
            O.DATA = ST.REF
        END
    END
**-----------------------------------------------------------

    RETURN
END
