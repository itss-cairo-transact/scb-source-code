* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 08/01/2013*********************************
*-----------------------------------------------------------------------------
* <Rating>280</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.VISA.DUPLICATE(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CARD.TYPE

*-----------------------------------------------------------------------*
    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    T.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE VI... AND CANCELLATION.DATE EQ '' AND CARD.CODE IN (101 102 104 105 201 202 204 205 ) BY CO.CODE BY ACCOUNT "

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SEL=':SELECTED ; CALL REM


    CALL F.READ(FN.CARD.ISSUE, KEY.LIST<1>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
    CARD.ACC<1> =  R.CARD.ISSUE<CARD.IS.ACCOUNT>

    FOR I = 2 TO SELECTED
        CALL F.READ(FN.CARD.ISSUE, KEY.LIST<I>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
        CARD.ACC<I> = R.CARD.ISSUE<CARD.IS.ACCOUNT>
        IF CARD.ACC<I> = CARD.ACC<I-1> THEN
            CALL F.READ(FN.CARD.ISSUE, KEY.LIST<I-1>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
            LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            CARD.NO<I-1> = LOCAL.REF<1,LRCI.VISA.NO>
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CARD.ACC<I-1>,CUST)
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,LOC.REF)
            CUST.NAME<I-1> = LOC.REF<1,CULR.ARABIC.NAME>
            CARD.CODE<I-1> = LOCAL.REF<1,LRCI.CARD.CODE>
            CALL DBR('SCB.VISA.CARD.TYPE':@FM:VICA.CODE.DESC,CARD.CODE<I-1>,DESC)
            COMP.CO<I-1> = R.CARD.ISSUE<CARD.IS.CO.CODE>
            Y.RET.DATA<-1> = CARD.NO<I-1> :"*":CARD.ACC<I-1>:"*":CUST.NAME<I-1> :"*":DESC:"*":COMP.CO<I-1>
            CARD.NO = '' ; CARD.ACC.S = '' ;CUST.NAME = '' ; CARD.CODE = '' ; COMP.CO = '' ; DESC = ''
            FLAG = 1
        END ELSE
            IF FLAG = 1 THEN
                CALL F.READ(FN.CARD.ISSUE, KEY.LIST<I-1>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                CARD.NO<I-1> = LOCAL.REF<1,LRCI.VISA.NO>
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CARD.ACC<I-1>,CUST)
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,LOC.REF)
                CUST.NAME<I-1> = LOC.REF<1,CULR.ARABIC.NAME>
                CARD.CODE<I-1> = LOCAL.REF<1,LRCI.CARD.CODE>
                CALL DBR('SCB.VISA.CARD.TYPE':@FM:VICA.CODE.DESC,CARD.CODE<I-1>,DESC)
                COMP.CO<I-1> = R.CARD.ISSUE<CARD.IS.CO.CODE>
                Y.RET.DATA<-1> = CARD.NO<I-1> :"*":CARD.ACC<I-1>:"*":CUST.NAME<I-1> :"*":DESC:"*":COMP.CO<I-1>
                CARD.NO = '' ; CARD.ACC.S = '' ;CUST.NAME = '' ; CARD.CODE = '' ; COMP.CO = '' ; DESC = ''
                FLAG = 0
            END
        END
        IF I = SELECTED  THEN
            IF FLAG = 1 THEN
                CALL F.READ(FN.CARD.ISSUE, KEY.LIST<I>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                CARD.NO<I> = LOCAL.REF<1,LRCI.VISA.NO>
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CARD.ACC<I>,CUST)
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,LOC.REF)
                CUST.NAME<I> = LOC.REF<1,CULR.ARABIC.NAME>
                CARD.CODE<I> = LOCAL.REF<1,LRCI.CARD.CODE>
                CALL DBR('SCB.VISA.CARD.TYPE':@FM:VICA.CODE.DESC,CARD.CODE<I>,DESC)
                COMP.CO<I> = R.CARD.ISSUE<CARD.IS.CO.CODE>
                Y.RET.DATA<-1> = CARD.NO<I> :"*":CARD.ACC<I>:"*":CUST.NAME<I> :"*":DESC:"*":COMP.CO<I>
            END
        END
    NEXT I
*-----------------------------------------------------------------------*
    RETURN
END
