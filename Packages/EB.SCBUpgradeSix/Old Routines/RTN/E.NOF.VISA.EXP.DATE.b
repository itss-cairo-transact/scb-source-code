* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 29/07/2015*********************************
*-----------------------------------------------------------------------------
* <Rating>280</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.VISA.EXP.DATE(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CARD.TYPE

*-----------------------------------------------------------------------*
    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)


    Path = "&SAVEDLISTS&/VISAEXPD"
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    EOF = ''

    LOOP WHILE NOT(EOF)
        CARD.NO = '' ; EXP.DATE = ''; BR.NO = '' ; CI.EXP.DATE = '' ; BRANCH.NO = '' ; CO.EXP.DATE = '' ; MM= '' ; YY= '' ; DD= ''
        READSEQ Line FROM MyPath THEN
            CARD.NO           = FIELD(Line,"|",1)
            ACCOUNT.NO        = FIELD(Line,"|",2)
            EXP.DATE          = FIELD(Line,"|",6)
            CARD.NAME         = FIELD(Line,"|",8)
*TEXT = 'CARD.NO':CARD.NO ; CALL REM
            DD = EXP.DATE[1,2]
            MM = EXP.DATE[4,2]
            YY = EXP.DATE[7,2]
            YYYY= "20":YY
            CO.EXP.DATE = YYYY:MM:DD

            T.SEL = "SELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO EQ " :CARD.NO

            KEY.LIST=""
            SELECTED=""
            ER.MSG=""

            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

            CALL F.READ(FN.CARD.ISSUE, KEY.LIST<1>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
            CI.EXP.DATE =  R.CARD.ISSUE<CARD.IS.EXPIRY.DATE>
            LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            BRANCH.NO = LOCAL.REF<1,LRCI.BRANCH.NO>

            IF CO.EXP.DATE # CI.EXP.DATE THEN
                Y.RET.DATA<-1> = CARD.NO :"*":ACCOUNT.NO:"*":CO.EXP.DATE:"*":CI.EXP.DATE:"*":BRANCH.NO:"*":CARD.NAME
                CARD.NO = '' ; ACCOUNT.NO = '' ;EXP.DATE = '' ; CI.EXP.DATE = '' ; BRANCH.NO = '' ; CARD.NAME = '' ; CO.EXP.DATE = ''
            END

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath
*-----------------------------------------------------------------------*
    RETURN
END
