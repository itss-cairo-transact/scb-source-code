* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*****NESSREEN AHMED 12/1/2011**********************************************
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.VAULT.INSR(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VOLT.INSURANCE

*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FLAG = 0

    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

*    AMT.CR   = 0
*    AMT.DR   = 0
    INS.AMT  = 0   ; INS.AMT.TOT = 0  ; CL.BAL = 0   ; TOT.EQV.BL = 0 ; EQV.BL = 0
    N.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 10000 AND VERSION.NAME NE CLOSED BY CO.CODE  "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
    ENT.NO = ''
    ACCT.ID<1> = KEY.LIST.N<1>
    CALL F.READ( FN.ACC,ACCT.ID<1>, R.ACC,F.ACC, ETEXT1)
    CL.BAL<1>  =   R.ACC<AC.OPEN.ACTUAL.BAL>
    CURR<1>    =   R.ACC<AC.CURRENCY>
    COMP.CO<1> =   R.ACC<AC.CO.CODE>
    IF CURR<1> NE 'EGP' THEN
        CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 70 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE CURR<1> IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
        CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
        RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
        IF RATE.TYPE EQ 'MULTIPLY' THEN
            EQV.BL  = CL.BAL<1> * CUR.RATE
        END
        IF RATE.TYPE EQ 'DIVIDE' THEN
            EQV.BL = CL.BAL<1> / CUR.RATE
        END

    END ELSE
        EQV.BL = CL.BAL<1>
    END
    TOT.EQV.BL = TOT.EQV.BL + EQV.BL
    FOR I = 2 TO SELECTED.N
        ACCT.ID<I> = KEY.LIST.N<I>
        CALL F.READ( FN.ACC,ACCT.ID<I>, R.ACC,F.ACC, ETEXT1)
        CL.BAL<I>  =   R.ACC<AC.OPEN.ACTUAL.BAL>
        CURR<I>    =   R.ACC<AC.CURRENCY>
        COMP.CO<I> =   R.ACC<AC.CO.CODE>
        IF COMP.CO<I> # COMP.CO<I-1> THEN
            CALL DBR( 'SCB.VOLT.INSURANCE':@FM:VLT.INS.EQU.AMOUNT, COMP.CO<I-1>, INS.AMT)
            Y.RET.DATA<-1> = COMP.CO<I-1>:"*":INS.AMT:"*":TOT.EQV.BL
            INS.AMT = 0   ;  TOT.EQV.BL = 0
            IF CURR<I> NE 'EGP' THEN
                CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 97 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CURR<I> IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
                RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
                IF RATE.TYPE EQ 'MULTIPLY' THEN
                    EQV.BL  = CL.BAL<I> * CUR.RATE
                END
                IF RATE.TYPE EQ 'DIVIDE' THEN
                    EQV.BL = CL.BAL<I> / CUR.RATE
                END

            END ELSE
                EQV.BL = CL.BAL<I>
            END
            TOT.EQV.BL = TOT.EQV.BL + EQV.BL
        END ELSE
            IF CURR<I> NE 'EGP' THEN
                CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 115 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CURR<I> IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
                RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
                IF RATE.TYPE EQ 'MULTIPLY' THEN
                    EQV.BL  = CL.BAL<I> * CUR.RATE
                END
                IF RATE.TYPE EQ 'DIVIDE' THEN
                    EQV.BL = CL.BAL<I> / CUR.RATE
                END

            END ELSE
                EQV.BL = CL.BAL<I>
            END
            TOT.EQV.BL = TOT.EQV.BL + EQV.BL
        END
        IF I = SELECTED.N THEN
            CALL DBR( 'SCB.VOLT.INSURANCE':@FM:VLT.INS.EQU.AMOUNT, COMP.CO<I>, INS.AMT)
            Y.RET.DATA<-1> = COMP.CO<I>:"*":INS.AMT:"*":TOT.EQV.BL
        END
    NEXT I

    RETURN
END
