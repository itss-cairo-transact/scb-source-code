* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.CUSTOMER.SEL(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""

    IF APPLICATION = 'TELLER' THEN

        BEGIN CASE
        CASE R.NEW(TT.TE.TRANSACTION.CODE) = 45

            T.SEL = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.STAFF' "
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CASE R.NEW(TT.TE.TRANSACTION.CODE) = 10

            T.SEL = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.PRIVATE'"
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
*
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CASE R.NEW(TT.TE.TRANSACTION.CODE) = 44

            T.SEL = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.CORPORATE' "
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
*
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CASE R.NEW(TT.TE.TRANSACTION.CODE) = 46

            T.SEL = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.PRIVATE'"
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
*
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CASE R.NEW(TT.TE.TRANSACTION.CODE) = 47

            T.SEL = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.PRIVATE'"
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
*
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        CASE OTHERWISE
            T.SEL = "SELECT FBNK.CUSTOMER WITH SECTOR # 5000"
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
*
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        END CASE
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO REGIONS FOUND"
        END
    END
*************************************************************************
*************************************************************************

    RETURN
END
