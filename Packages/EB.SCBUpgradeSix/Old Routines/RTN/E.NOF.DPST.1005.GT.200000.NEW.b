* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
****NESSREEN AHMED 11/9/2014******************
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.DPST.1005.GT.200000.NEW(Y.RET.DATA)
**    PROGRAM E.NOF.DPST.1005.GT.200000

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TELLER.DPST.1005
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.TT.DPST = 'F.SCB.TELLER.DPST.1005' ; F.TT.DPST = ''
    CALL OPF(FN.TT.DPST,F.TT.DPST)

    TOT.TT = 0 ; TOT.AMT = 0 ; SS= 0 ; NN = 0


*    N.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 73 WITH AUTH.DATE GE '20140904' BY CUSTOMER.1 BY AUTH.DATE "
    N.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 73 WITH AUTH.DATE GE '20140904' AND RECORD.STATUS EQ 'MAT' BY CUSTOMER.1 BY AUTH.DATE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)
    TEXT = "SEL=":SELECTED.N ; CALL REM
    CUST<1>    = R.TT<TT.TE.CUSTOMER.1>
    AMT<1>     = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.D<1>  = R.TT<TT.TE.AUTH.DATE>
    CO.CODE<1> = R.TT<TT.TE.CO.CODE>
    TOT.AMT    = TOT.AMT + AMT<1>
    FOR I = 2 TO SELECTED.N
        CONTACT.DATE = ''
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I> = R.TT<TT.TE.CUSTOMER.1>
        AMT<I>  = R.TT<TT.TE.AMOUNT.LOCAL.1>
        IF CUST<I> # CUST<I-1> THEN
            IF TOT.AMT GT 200000 THEN
                KEY.TO.USE = CUST<I-1>
                CALL F.READ( FN.TT.DPST, KEY.TO.USE, R.TT.DPST, F.TT.DPST, ERR1)
                CALL DBR( 'CUSTOMER':@FM:EB.CUS.CONTACT.DATE,KEY.TO.USE,CONTACT.DATE)
                R.TT.DPST<TT.DPST.CUST.NO>     = KEY.TO.USE
                R.TT.DPST<TT.DPST.AMT.LCY>     = TOT.AMT
                R.TT.DPST<TT.DPST.RUN.DATE>   = TODAY
                R.TT.DPST<TT.DPST.CUST.DATE>  = CONTACT.DATE
                CALL F.WRITE(FN.TT.DPST, KEY.TO.USE, R.TT.DPST)
                CALL JOURNAL.UPDATE(KEY.TO.USE)

                TOT.AMT = ''
            END ELSE
                TOT.AMT = ''
                TOT.AMT    = TOT.AMT + AMT<I>
            END
        END ELSE
            TOT.AMT = TOT.AMT + AMT<I>
        END
        IF I = SELECTED.N THEN
*  IF TOT.AMT GT 200000 THEN
*      Y.RET.DATA<-1> = CUST<I>:"*":TOT.TT:"*":TOT.AMT
*  END
        END

    NEXT I
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    TT.SEL = "SELECT F.SCB.TELLER.DPST.1005 WITH CUST.DATE GE '20140904' BY CUST.NO"

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            CUST.OVR = ''
            CUST.OVR = KEY.LIST.TT<TTI>
* TEXT = 'CUST1=':CUST<TTI> ; CALL REM
            KEY.LIST.DD = ""
            SELECTED.DD = ""
            DD.N = ""
            DD.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 73 WITH AUTH.DATE GE '20140904' AND CUSTOMER.1 EQ " :CUST.OVR:" BY AUTH.DATE "
            CALL EB.READLIST(DD.SEL, KEY.LIST.DD, "", SELECTED.DD, DD.N)

            FOR SS = 1 TO SELECTED.DD
                CALL F.READ( FN.TT,KEY.LIST.DD<SS>, R.TT,F.TT, ERR.TT)
                CUST    = R.TT<TT.TE.CUSTOMER.1>
                AMT     = R.TT<TT.TE.AMOUNT.LOCAL.1>
                AUTH.D  = R.TT<TT.TE.AUTH.DATE>
                CO.CODE = R.TT<TT.TE.CO.CODE>
                Y.RET.DATA<-1> = CUST:"*" : AMT :"*": AUTH.D : "*": CO.CODE: "*":KEY.LIST.DD<SS>
                CUST = '' ; AMT = '' ; AUTH.D = '' ; CO.CODE = ''
            NEXT SS
        NEXT TTI
    END

    RETURN
END
