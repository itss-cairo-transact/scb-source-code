* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-------------------------------------------------------------------------------------
* <Rating>769</Rating>
*-------------------------------------------------------------------------------------
    SUBROUTINE EB.CORR.ENTRY.HOLD
*-------------------------------------------------------------------------------------
*
* CREATED  : JAICHANDRAN.T.P/TEMENOS
* DATE     : 15 NOV 2008
* SITE     : KECB (R08.000 - ORACLE)
*
* PURPOSE  : Routine to cross check and correct the ENTRY.HOLD entries against the
*            contract to which it belongs to.
*
* NOTE     : This routine serves to check/correct Standard T24 Applications,
*            any Local Development may require seperate attention during correction.
*
*-------------------------------------------------------------------------------------
    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE
    INCLUDE T24.BP I_F.STMT.ENTRY
    INCLUDE T24.BP I_SCREEN.VARIABLES
*-------------------------------------------------------------------------------------

    GOSUB GET.INPUT
    GOSUB OPEN.FILES
    GOSUB MAIN.PROCESS

    RETURN
*-------------------------------------------------------------------------------------
GET.INPUT:
*-------------------------------------------------------------------------------------
*
    CORRECT    = 0
    LAST.APP   = ''
    DELETE.APP = ''
*
    LOCATE C.U IN T.CONTROLWORD<1> SETTING X ELSE
        T.CONTROLWORD<X> = C.U
    END
*
    CRT @(4,5):'    Choose from the Below Options'
    CRT @(4,7):' 1. Check   ENTRY.HOLD'
    CRT @(4,8):' 2. Correct ENTRY.HOLD'
    CRT @(4,9):' 3. Exit'
    CALL TXTINP('INPUT OPTION',8,22,1.1,'')
*
    BEGIN CASE
    CASE COMI = C.U OR COMI GE 3
        GOSUB PROGRAM.RETURN
    CASE COMI = 2
        CORRECT = 1
        FOR I = 4 TO 18
            CRT @(4,I) : S.CLEAR.EOL
        NEXT I
*Line [ 70 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL TXTINP('INPUT APPLICATION',8,22,32.1,'PG':@FM:'HUD')
        IF COMI = C.U THEN
            GOSUB PROGRAM.RETURN
        END
        DELETE.APP = COMI
    END CASE

    RETURN
*-------------------------------------------------------------------------------------
MAIN.PROCESS:
*-------------------------------------------------------------------------------------

    EXECUTE 'COMO ON EB.CORR.ENTRY.HOLD_':DATE() 'DG':'_':TIME() 'MTS'

    CNT = 0
    SSELECT F.ENTRY.HOLD      ;* Best at large volume site
    LOOP
        READNEXT ENT.ID ELSE
            ENT.ID = ''
        END
    WHILE ENT.ID <> '' DO
        CNT += 1
        GOSUB GET.SYS.ID
        BEGIN CASE
        CASE CORRECT AND SYS.ID         ;* If said to correct
            IF DELETE.APP = APP THEN
                GOSUB CHECK.OR.CORR.REC
            END
        CASE SYS.ID AND CORRECT = 0     ;* Only check
            GOSUB CHECK.OR.CORR.REC
        END CASE
        IF MOD(CNT,100) = 0 THEN
            CRT 'Processed ':CNT
        END
    REPEAT

    EXECUTE 'COMO OFF'

    RETURN
*-------------------------------------------------------------------------------------
CHECK.OR.CORR.REC:
*-------------------------------------------------------------------------------------

    IF LAST.APP NE APP AND APP NE 'LOCAL' THEN    ;* This will enable me to delete 'LOCAL' entries too
        APPLICATION = APP
        V$FUNCTION = 'GET.DEF'
        CALL EB.EXECUTE.APPLICATION(APPLICATION)  ;* Initialized 'V' variable
        LAST.APP = APPLICATION
        FN.FILE$NAU = 'F.':APPLICATION:'$NAU'
        F.FILE$NAU  = ''
        CALL OPF(FN.FILE$NAU,F.FILE$NAU)
    END
*
    IF APP = 'LOCAL' THEN
        MAT R.NEW = ''
        V         = 11        ;* Minimal Req
    END ELSE
        MATREADU R.NEW FROM F.FILE$NAU,CONT.ID ELSE         ;* lock it
            MAT R.NEW = ''
        END
    END
*
    BEGIN CASE
    CASE R.NEW(1) = '' AND CORRECT      ;* If said to correct
        CRT 'Correcting Entry ': ENT.ID
        GOSUB RESTORE.ACCT.BAL
    CASE R.NEW(1) = ''
        CRT 'Missing Transaction ':CONT.ID:' (':ENT.ID:') Application ':APPLICATION
    CASE R.NEW(V-8) = 'IHLD' AND CORRECT
        CRT 'Correcting Entry ': ENT.ID
        GOSUB RESTORE.ACCT.BAL
    CASE R.NEW(V-8) = 'IHLD'
        CRT 'Record in IHLD Status ':ENT.ID
    END CASE
*
    RELEASE F.FILE$NAU,CONT.ID          ;* Release the lock

    RETURN
*-------------------------------------------------------------------------------------
OPEN.FILES:
*-------------------------------------------------------------------------------------

    CALL OPF('F.ENTRY.HOLD',F.ENTRY.HOLD)

    RETURN

*-------------------------------------------------------------------------------------
GET.SYS.ID:
*-------------------------------------------------------------------------------------

    SYS.ID = ''
    BEGIN CASE
    CASE NUM(ENT.ID)          ;* Must be local,
        SYS.ID  = ''
        CONT.ID = ENT.ID
        APP     = 'LOCAL'
        CRT 'Unknown Application ': ENT.ID
    CASE ENT.ID['.',1,1] = 'ACCOUNT'
        SYS.ID  = 'ACCOUNT.CLOSURE'
        CONT.ID = ENT.ID['.',2,1][8,99]
        APP     = 'ACCOUNT.CLOSURE'
    CASE ENT.ID[1,2] = 'AC'   ;* Commanly used in Local Development, so cross check Manually
        SYS.ID  = 'AC'
        CONT.ID = ENT.ID[3,99]
        APP     = 'AZ.ACCOUNT'
    CASE ENT.ID[1,2] = 'BL'
        SYS.ID  = 'BL'
        CONT.ID = ENT.ID[3,99]
        APP     = 'BL.BILLS'
    CASE ENT.ID[1,2] = 'CC'
        SYS.ID  = 'CC'
        CONT.ID = ENT.ID[3,99]
        IF INDEX(CONT.ID,'.',1) THEN
            APP = 'CHEQUE.ISSUE'
        END ELSE
            APP = 'CHEQUE.COLLECTION'
        END
    CASE ENT.ID[1,2] = 'DG'
        SYS.ID  = 'DG'
        CONT.ID = ENT.ID[3,99]
        APP     = 'DISAGIO'
    CASE ENT.ID[1,2] = 'DX'
        SYS.ID  = ''          ;* As of now, do not check or correct
        CONT.ID = ENT.ID[3,99]['.',1,1]
        BEGIN CASE
        CASE CONT.ID[1,5] = 'DXTRA'
            APP = 'DX.TRADE'
        CASE 1
            SYS.ID = ''
            CRT 'Unknown Application ': CONT.ID
        END CASE
    CASE ENT.ID[1,2] = 'EB'   ;* Must be local
        SYS.ID  = 'EB'
        CONT.ID = ENT.ID
        APP     = 'LOCAL'
        CRT 'Unknown Application ': ENT.ID
    CASE ENT.ID[1,2] = 'FT' AND NUM(ENT.ID[3,99]) ;* STO
        SYS.ID  = 'FT'
        CONT.ID = ENT.ID[3,99]
        APP     = 'STANDING.ORDER'
    CASE ENT.ID[1,2] = 'FT'   ;* Commanly used in Local Development, so cross check Manually
        SYS.ID  = 'FT'
        CONT.ID = ENT.ID[3,99]
        APP     = 'FUNDS.TRANSFER'
    CASE ENT.ID[1,2] = 'FX'
        SYS.ID  = 'FX'
        CONT.ID = ENT.ID[3,99]
        APP     = 'FOREX'
    CASE ENT.ID[1,2] = 'IC'
        SYS.ID  = 'IC'
        CONT.ID = ENT.ID[3,99]
        APP     = 'AC.PENDING'
    CASE ENT.ID[1,2] = 'LC'
        SYS.ID  = 'LC'
        CONT.ID = ENT.ID[3,99]
        IF LEN(CONT.ID) = 12 THEN
            APP = 'LETTER.OF.CREDIT'
        END ELSE
            APP = 'DRAWINGS'
        END
    CASE ENT.ID[1,2] = 'LD'
        SYS.ID  = 'LD'
        CONT.ID = ENT.ID[3,99]
        APP     = 'LD.LOANS.AND.DEPOSITS'
    CASE ENT.ID[1,2] = 'MG'
        SYS.ID  = 'MG'
        CONT.ID = ENT.ID[3,99]
        IF INDEX(CONT.ID,'.',1) THEN
            APP = 'MG.PAYMENT'
        END ELSE
            APP = 'MG.MORTGAGE'
        END
    CASE ENT.ID[1,2] = 'MD'
        SYS.ID  = 'MD'
        CONT.ID = ENT.ID[3,99]
        APP     = 'MD.DEAL'
    CASE ENT.ID[1,2] = 'PD'
        SYS.ID  = 'PD'
        CONT.ID = ENT.ID[3,99]
*Line [ 250 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF CONT.ID[1,4] MATCHES 'PDCA':@VM:'PDPD' THEN
            APP = 'PD.CAPTURE'
        END ELSE
            APP = 'PD.PAYMENT.DUE'
        END
    CASE ENT.ID[1,2] = 'MM'
        SYS.ID  = 'MM'
        CONT.ID = ENT.ID[3,99]
        APP     = 'MM.MONEY.MARKET'
    CASE ENT.ID[1,2] = 'PS'
        SYS.ID  = 'PS'
        CONT.ID = ENT.ID[3,99]
        APP     = 'PAYMENT.STOP'
    CASE ENT.ID[1,2] = 'SW'
        SYS.ID  = 'SW'
        CONT.ID = ENT.ID[3,99]
        APP     = 'SWAP'
    CASE ENT.ID[1,2] = 'SC'
        SYS.ID = 'SC'
        CONT.ID = ENT.ID[3,99]
        BEGIN CASE
        CASE CONT.ID[1,6] = 'SCTRSC'
            APP    = 'SEC.TRADE'
        CASE 1
            SYS.ID = ''
            CRT 'Unknown Application ': CONT.ID
        END CASE
    CASE ENT.ID[1,2] = 'SL'
        SYS.ID  = 'SL'
        CONT.ID = ENT.ID[3,99]
        BEGIN CASE
        CASE LEN(CONT.ID) = 26
            APP = 'SL.CHARGE'
        CASE LEN(CONT.ID) = 19
            APP = 'SL.LOANS'
        CASE LEN(CONT.ID) = 18
            APP = 'SL.BUY.SELL'
        CASE LEN(CONT.ID) = 14
            APP = 'FACILITY'
        CASE LEN(CONT.ID) = 12
            APP = 'PRE.SYNDICATION.FILE'
        CASE 1
            SYS.ID = ''
            CRT 'Unknown Application ':CONT.ID
        END CASE
    CASE ENT.ID[1,2] = 'TT'
        SYS.ID  = ENT.ID[1,2]
        CONT.ID = ENT.ID[3,99]
        APP     = 'TELLER'
    CASE 1
        CRT 'Invalid Application ': ENT.ID
    END CASE

    RETURN
*-------------------------------------------------------------------------------------
RESTORE.ACCT.BAL:
*-------------------------------------------------------------------------------------

    IF SYS.ID = 'DX' THEN
        ID.NEW = ENT.ID[3,99]
    END ELSE
        ID.NEW = CONT.ID
    END
    CALL EB.ACCOUNTING(SYS.ID,'DEL','','')        ;* This will release the balance as well as currency position
    CALL JOURNAL.UPDATE(ID.NEW)         ;* Update the same

    RETURN
*-------------------------------------------------------------------------------------
PROGRAM.RETURN:
*-------------------------------------------------------------------------------------

    RETURN TO PROGRAM.RETURN

    RETURN
*-------------------------------------------------------------------------------------
END
