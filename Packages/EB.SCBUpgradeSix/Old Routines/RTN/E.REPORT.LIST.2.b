* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-15</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.REPORT.LIST.2

    $INSERT  TEMENOS.BP I_COMMON
    $INSERT  TEMENOS.BP I_EQUATE
    $INSERT  TEMENOS.BP I_ENQUIRY.COMMON


*-----------------------------------------------------------------------

    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL, C$YYYYMM


*-----------------------------------------------------------------------

*    IF ENQHARES001 NE '' THEN
*        RETURN
*    END

*-----------------------------------------------------------------------
    SEQ.FILE.NAME = 'TEMENOS.BP'
*    REP.NAME = 'REPORTS.NAMES'
    REP.NAME = 'reports.names'

    C$REP.NAME = ''
    C$REP.TITLE = ''
    C$D1 = ''
    C$D2 = ''
    C$D3 = ''
    C$D4 = ''
    C$DALL = ''

    WRK.YY = TODAY[1,4]
    WRK.MM = TODAY[5,2]
    WRK.MM = WRK.MM - 1
    IF WRK.MM LT 1 THEN
        WRK.MM = 12
        WRK.YY = WRK.YY -1
    END
    C$YYYYMM = WRK.YY:WRK.MM
*-----------------------------------------------------------------------
    OPENSEQ SEQ.FILE.NAME,REP.NAME TO SEQ.FILE.POINTER ELSE
        CREATE SEQ.FILE.POINTER ELSE
*           CRT  "Unable to create file  ":SEQ.FILE.NAME
            STOP
            RETURN
        END
    END
*   CRT "Openseq was successful ":SEQ.FILE.NAME
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
*            CRT "Message Extracted :":Y.MSG

            PGM.NAME = FIELD(Y.MSG,'|',3,1)
            PGM.NAME = TRIM(PGM.NAME,' ',"A")
            PGM.NAME = TRIM(PGM.NAME,'',"D")
            WS.STRING = PGM.NAME
            GOSUB HANDL.ARRAY
            PGM.NAME = WS.STRING

            C$REP.NAME<-1> = PGM.NAME

            REP.TITLE = FIELD(Y.MSG,'|',4,1)
            DEPT1 = FIELD(Y.MSG,'|',5,1)
            DEPT2 = FIELD(Y.MSG,'|',6,1)
            DEPT3 = FIELD(Y.MSG,'|',7,1)
            DEPT4 = FIELD(Y.MSG,'|',8,1)

            WS.STRING = REP.TITLE
            GOSUB HANDL.ARRAY
            REP.TITLE = WS.STRING

            WS.STRING = DEPT1
            GOSUB HANDL.ARRAY
            DEPT1 = WS.STRING

            WS.STRING = DEPT2
            GOSUB HANDL.ARRAY
            DEPT2 = WS.STRING

            WS.STRING = DEPT3
            GOSUB HANDL.ARRAY
            DEPT3 = WS.STRING

            WS.STRING = DEPT4
            GOSUB HANDL.ARRAY
            DEPT4 = WS.STRING

            C$REP.TITLE<-1> = REP.TITLE
            C$D1<-1>  = DEPT1
            C$D2<-1>  = DEPT2
            C$D3<-1>  = DEPT3
            C$D4<-1>  = DEPT4

            C$DALL<-1> = DEPT1:'/':DEPT2:'/':DEPT3:'/':DEPT4
        END ELSE
            EOF = 1
*           CRT "Unable to read from file "
        END

    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

    RETURN
*--------------------------------------------------------------------
HANDL.ARRAY:

    X = LEN(WS.STRING)
    J = 0
    OUT.STRING = ''
    FOR I = 1 TO X
        IF WS.STRING[I,1] GE CHAR(30) AND WS.STRING[I,1] LE CHAR(255)  THEN
            J = J + 1
            OUT.STRING[J,1] = WS.STRING[I,1]

        END
    NEXT I
    IF  OUT.STRING EQ '' THEN
        OUT.STRING = ' '
    END
    WS.STRING = OUT.STRING

    RETURN
*--------------------------------------------------------------------
