* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM EOD.CHANGE.LIM.DATE
    SUBROUTINE EOD.CHANGE.LIM.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BRANCH.ACCT.OLD
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*****
*****

    EOF = ''

    FN.LIMIT = 'FBNK.LIMIT' ; F.LIMIT = '' ; R.LIMIT = ''
    CALL OPF(FN.LIMIT,F.LIMIT)


    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',L.WK.DAY)

    T.SEL  = "SELECT FBNK.LIMIT WITH EXPIRY.DATE LE L.WK.DAY AND ADM.EXTENSION.DATE NE '20491231' AND NOTES UNLIKE ...SYS... "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    LOOP
        REMOVE LIM.ID  FROM KEY.LIST SETTING POS
    WHILE LIM.ID:POS
        CALL F.READ( FN.LIMIT,LIM.ID, R.LIMIT,F.LIMIT, ETEXT)

        R.LIMIT<LI.ADM.EXTENSION.DATE> = '20491231'
        CALL F.WRITE (FN.LIMIT, LIM.ID , R.LIMIT )
        CALL JOURNAL.UPDATE(LIM.ID)
****        WRITE  R.LIMIT TO F.LIMIT , LIM.ID ON ERROR
****            PRINT "CAN NOT WRITE RECORD":LIM.ID:"TO" :FN.LIMIT
****        END
    REPEAT

    RETURN
END
