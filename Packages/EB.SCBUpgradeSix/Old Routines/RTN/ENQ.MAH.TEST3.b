* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE ENQ.MAH.TEST3(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ENT.TODAY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.ENT = 'FBNK.ACCT.ENT.TODAY'
    F.ENT = ''
    R.STMT.TODAY = ''
    CALL OPF(FN.ENT,F.ENT)
    T.SEL = "SELECT ":FN.ENT
    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ENT,K.LIST<I>,R.STMT.TODAY,F.ENT,ERRR)
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            KK=DCOUNT(R.STMT.TODAY,@VM)
            FOR K = 1 TO KK
                KK1 += 1
                ENQ<2,KK1> = "@ID"
                ENQ<3,KK1> = "EQ"
                ENQ<4,KK1> = R.STMT.TODAY<AC.ENT.ENTRY.NO,K>
            NEXT K
        NEXT I
    END ELSE
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
    RETURN
END
