* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*************MOHAMED SABRY & MAHMOUD ELHAWARY 28/12/2011*********************
*-----------------------------------------------------------------------------
* <Rating>3763</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.TT.WITHDRAWAL(Y.ALL.REC)
*#     PROGRAM E.NOF.TT.WITHDRAWAL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 45 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
***************************************************
INITIATE:
*--------
    KK = 0
    II = 0
    XX = ''
    KK1= 0
    CUS.MNTH  = ''
    NCUS.MNTH = ''
    NN.MNTH   = ''
    TOT.CUS.AMT = 0
    MIN.CUS.AMT = 0
    MAX.CUS.AMT = 0
    AVR.CUS.AMT = 0
    WS.AVAL.CODE = " 16 18 35 43 85 93 "
    NEW.SEC.GRP = " 4650 1250 2250 3250 4250 "
    RETURN
***************************************************
CALLDB:
*--------
    FN.TT.HIS  = "FBNK.TELLER$HIS" ; F.TT.HIS = '' ; R.TT.HIS =''
    CALL OPF(FN.TT.HIS,F.TT.HIS)
    FN.TT.TRNS = "FBNK.TELLER.TRANSACTION" ; F.TT.TRNS = ''; R.TT.TRNS =''
    RETURN
***************************************************
PROCESS:
*--------
    LOCATE "CURRENCY"    IN D.FIELDS<1> SETTING YCUR.POS THEN CUR.ID    = D.RANGE.AND.VALUE<YCUR.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
    LOCATE "GROUP.NO"    IN D.FIELDS<1> SETTING YGRP.POS THEN GRP.NO    = D.RANGE.AND.VALUE<YGRP.POS> ELSE RETURN
*Line [ 80 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE "NEW.SECTOR"  IN D.FIELDS<1> SETTING YSEC.POS THEN NEW.SEC   = D.RANGE.AND.VALUE<YSEC.POS> ELSE NULL
*Line [ 82 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE "CREDIT.CODE" IN D.FIELDS<1> SETTING YCRD.POS THEN CRD.CODE  = D.RANGE.AND.VALUE<YCRD.POS> ELSE NULL

*#    PROMPT "CUR.ID   " ; INPUT CUR.ID
*#    PROMPT "FROM     " ; INPUT FROM.DATE
*#    PROMPT "TO       " ; INPUT END.DATE
*#    PROMPT "GRP.NO   " ; INPUT GRP.NO
*#    PROMPT "NEW.SEC  " ; INPUT NEW.SEC
*#    PROMPT "CRD.CODE " ; INPUT CRD.CODE

    SEL.CMD ="SELECT ":FN.TT.HIS:" WITH CUSTOMER.1 NE '' AND VALUE.DATE.1 GE ":FROM.DATE:" AND VALUE.DATE.1 LE ":END.DATE:" AND CURRENCY.1 EQ ":CUR.ID:" BY CUSTOMER.1 BY VALUE.DATE.1 "
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    CRT NOREC
    IF NOREC THEN
        FOR I = 1 TO NOREC
            TT.ID = SELLIST<I>
            CALL F.READ(FN.TT.HIS,SELLIST<I>, R.TT.HIS, F.TT.HIS, E.TT.HIS )
            IF R.TT.HIS<TT.TE.CURRENCY.1> NE CUR.ID THEN GOTO NEXT.I
            WS.CUS.ID = R.TT.HIS<TT.TE.CUSTOMER.1>
            IF WS.CUS.ID EQ '' THEN GOTO NEXT.I
            IF WS.CUS.ID[1,3] EQ '994' THEN GOTO NEXT.I
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,WS.CUS.ID,CUS.CO)
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,LOCAL.REF)
            WS.NEW.SECTOR  = LOCAL.REF<1,CULR.NEW.SECTOR>
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF WS.NEW.SECTOR EQ '' THEN GOTO NEXT.I
            IF GRP.NO EQ 1 THEN
                FINDSTR WS.NEW.SECTOR:" " IN NEW.SEC.GRP SETTING POS.SEC ELSE GOTO NEXT.I
            END
            IF GRP.NO EQ 2 THEN
*Line [ 112 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR WS.NEW.SECTOR:" " IN NEW.SEC.GRP SETTING POS.SEC THEN GOTO NEXT.I ELSE NULL
            END
            IF WS.CREDIT.CODE NE '' THEN GOTO NEXT.I
            WS.TRNS.CODE = R.TT.HIS<TT.TE.TRANSACTION.CODE>
            CALL DBR ('TELLER.TRANSACTION':@FM:TT.TR.TRANSACTION.CODE.1,WS.TRNS.CODE,WS.TRANS)
            FINDSTR WS.TRANS IN WS.AVAL.CODE SETTING POS ELSE GOTO NEXT.I
*Line [ 119 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            IF R.TT.HIS<TT.TE.VALUE.DATE.1> GE FROM.DATE AND R.TT.HIS<TT.TE.VALUE.DATE.1> LE END.DATE THEN '' ELSE GOTO NEXT.I
            IF R.TT.HIS<TT.TE.RECORD.STATUS> NE 'MAT' THEN GOTO NEXT.I
            IF CUR.ID EQ LCCY THEN
                TT.AMT = R.TT.HIS<TT.TE.AMOUNT.LOCAL.1,1>
            END ELSE
                TT.AMT = R.TT.HIS<TT.TE.AMOUNT.FCY.1,1>
            END
            TT.V.DATE  = R.TT.HIS<TT.TE.VALUE.DATE.1>
            CUS.MNTH = WS.CUS.ID:"-":TT.V.DATE[1,6]
            NN.MNTH = TT.V.DATE[1,6]
            IF NCUS.MNTH EQ '' THEN
                NCUS.MNTH   = CUS.MNTH
            END
            IF CUS.MNTH NE NCUS.MNTH THEN
                DD.V.DATE  = FIELD(NCUS.MNTH,'-',2):'01'
                CALL LAST.DAY(DD.V.DATE)
                MNTH.DAYS = DD.V.DATE[2]
                AVR.CUS.AMT =  TOT.CUS.AMT / MNTH.DAYS
                GOSUB RET.DATA
                NCUS.MNTH   = CUS.MNTH
                II = 0
                TOT.CUS.AMT = 0
                MIN.CUS.AMT = 0
                MAX.CUS.AMT = 0
                AVR.CUS.AMT = 0
            END
            II++
            TOT.CUS.AMT += TT.AMT
            GOSUB FILL.ARRAY
            IF MIN.CUS.AMT EQ 0 THEN
                MIN.CUS.AMT = TT.AMT
            END
            IF TT.AMT GT MAX.CUS.AMT THEN
                MAX.CUS.AMT = TT.AMT
            END
            IF TT.AMT LT MIN.CUS.AMT THEN
                MIN.CUS.AMT = TT.AMT
            END
NEXT.I:
        NEXT I
        DD.V.DATE  = FIELD(NCUS.MNTH,'-',2):'01'
        CALL LAST.DAY(DD.V.DATE)
        MNTH.DAYS = DD.V.DATE[2]
        AVR.CUS.AMT =  TOT.CUS.AMT / MNTH.DAYS
        GOSUB RET.DATA
**        MNTH.COUNT = DCOUNT(ARR.MNTH,VM)
        CALL MONTHS.BETWEEN(FROM.DATE,END.DATE,MNTH.COUNT)
        FOR AAA = 0 TO MNTH.COUNT
            NN.MNTH = FROM.DATE
            CALL ADD.MONTHS(NN.MNTH,AAA)
            NN.MNTH = NN.MNTH[1,6]
            CUS.CO  = 999999999
            NCUS.ID = 99999999
            NCUS.MNTH   = NCUS.ID:"-":ARR.MNTH<NN.MNTH,1>
            TOT.CUS.AMT = ARR.MNTH<NN.MNTH,2>
            II          = ARR.MNTH<NN.MNTH,3>
            MIN.CUS.AMT = ''
            MAX.CUS.AMT = ''
            AVR.CUS.AMT = ''
            IF ARR.MNTH<NN.MNTH,1> THEN
**                GOSUB RET.DATA
            END
        NEXT AAA
    END
    RETURN
****************************************
FILL.ARRAY:
*--------
*Line [ 188 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE NN.MNTH IN ARR.MNTH<1> SETTING POS.MN THEN '' ELSE
        ARR.MNTH<NN.MNTH,1> = NN.MNTH
    END
    ARR.MNTH<NN.MNTH,2> += TT.AMT
    ARR.MNTH<NN.MNTH,3> += 1
    RETURN
***************************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = CUS.CO:"*":CUR.ID:"*":FROM.DATE:"*":END.DATE:"*":NCUS.MNTH:"*":TOT.CUS.AMT:"*":MIN.CUS.AMT:"*":MAX.CUS.AMT:"*":AVR.CUS.AMT:"*":II
*#    KK++
*#    XX<1,KK>[1, 20] = NCUS.MNTH
*#    XX<1,KK>[20,19] = FMT(TOT.CUS.AMT,"L2,")
*#    XX<1,KK>[39,19] = FMT(MAX.CUS.AMT,"L2,")
*#    XX<1,KK>[58,19] = FMT(MIN.CUS.AMT,"L2,")
*#    XX<1,KK>[77,3]  = II
*#    PRINT XX<1,KK>
    RETURN
***************************************************
END
