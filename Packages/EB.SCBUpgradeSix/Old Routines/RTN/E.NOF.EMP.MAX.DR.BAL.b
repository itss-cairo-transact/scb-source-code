* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-95</Rating>
*-----------------------------------------------------------------------------
*****************MAHMOUD 25/6/2013 *************************
* TO GET THE HIGHST DEBIT BALANCE THROUGH THE LAST QUARTER *
************************************************************
    SUBROUTINE E.NOF.EMP.MAX.DR.BAL(Y.RET.DATA)

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    $INCLUDE T24.BP I_F.ACCT.ACTIVITY   ;*IC.ACT.
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 46 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*==============================================================
INITIATE:
*--------
*?????????????????????????????????????
    TD1  = TODAY
*####### For Test Only ###### CALL CDT('',TD1,'+1W')
*?????????????????????????????????????

    IF TD1[5,2] = '01' OR TD1[5,2] = '02' OR TD1[5,2] = '03' THEN
        TDLY = TD1[1,4] - 1
        FRDT = TDLY:'1001'
        TODT = TDLY:'1231'
    END
    IF TD1[5,2] = '04' OR TD1[5,2] = '05' OR TD1[5,2] = '06' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0101'
        TODT = TDCY:'0331'
    END
    IF TD1[5,2] = '07' OR TD1[5,2] = '08' OR TD1[5,2] = '09' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0401'
        TODT = TDCY:'0630'
    END
    IF TD1[5,2] = '10' OR TD1[5,2] = '11' OR TD1[5,2] = '12' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0701'
        TODT = TDCY:'0930'
    END
    CRT FRDT:" - ":TODT
    CUST.NAME    = ''
    Y.OPEN.BAL   = 0
    Y.MIN.BAL    = ''
    XX           = SPACE(130)
    K = 0
*##    CAT.GRP  = " 3201 1102 1404 1414 1206 1208 1401 1402 1405 1406 1415 11240 11232 11231 1480 1481 1202 1203 "
*##    CAT.GRP := " 1212 1502 1503 1214 1416 1201 1211 1477 11242 1501 1588 1599 1001 1516 1002 1523 1217 1524 1205 "
*##    CAT.GRP := " 1207 1525 1216 1215 1504 1507 1508 1512 1514 1518 1519 1582 1558 1598 1595 1596 1597 1577 "
*##    CAT.GRP := " 1499 1483 1493 1301 1302 1303 1377 1390 1399 1566 1591 "
*##    CAT.GRP := " 1003 1059 1011 1509 1510 1511 1513 1579 1534 1544 1559 "
    CAT.GRP = " 1407 1413 1408 1445 1455 "
    RETURN
*===============================================================
CALLDB:
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.ACC     = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ACC.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACC.ACT = ''
    CALL OPF(FN.ACC.ACT,F.ACC.ACT)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    RETURN
*************************************************************************
PROCESS:
*-------
    S.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( ":CAT.GRP:" ) BY CUSTOMER "
    CALL EB.READLIST(S.SEL,KEY.LIST,'',SELECTED,ER.MSG)
*?    LOOP
*?        REMOVE CUS.ID FROM KEY.LIST SETTING POS.CUS
*?    WHILE CUS.ID:POS.CUS
*?        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,CUS.LCL)
*?        CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
*?        CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
*?        IF CRD.STA NE '' OR CRD.COD GE 110 THEN NULL ELSE
*?            CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,CUS.ACC.ERR)
********************************************************************
    LOOP
        REMOVE Y.ACC.ID FROM KEY.LIST SETTING POSS1
    WHILE Y.ACC.ID:POSS1
        CALL F.READ(FN.ACC,Y.ACC.ID,R.ACC,F.ACC,Y.ACC.ERR)
        CUS.ID    = R.ACC<AC.CUSTOMER>
        Y.CURR    = R.ACC<AC.CURRENCY>
        Y.CAT     = R.ACC<AC.CATEGORY>
        Y.ACC.COM = R.ACC<AC.CO.CODE>
*************************************
*##                FINDSTR Y.CAT:' ' IN CAT.GRP SETTING POS.GRP THEN
*************************************
        KK1 = 0
        CALL GET.ENQ.BALANCE(Y.ACC.ID,FRDT,Y.OPEN.BAL)
        KK1++
        Y.MIN.BAL<KK1> = Y.OPEN.BAL
        ACT.DATE = FRDT
        LOOP
        WHILE ACT.DATE LE TODT
            ACT.MNTH = ACT.DATE[1,6]
            ACC.ACT.ID = Y.ACC.ID:"-":ACT.MNTH
****************************************************
            CALL F.READ(FN.ACC.ACT,ACC.ACT.ID,R.ACC.ACT,F.ACC.ACT,Y.ERR1)
            IF NOT(Y.ERR1) THEN
                MIN.BAL.AC = MINIMUM(R.ACC.ACT<IC.ACT.BALANCE>)
                IF MIN.BAL.AC NE '' AND MIN.BAL.AC LT 0 THEN
                    KK1++
                    Y.MIN.BAL<KK1> = MIN.BAL.AC
                END
            END
****************************************************
            CALL ADD.MONTHS(ACT.DATE,'1')
        REPEAT
        MIN.DR.BAL = MINIMUM(Y.MIN.BAL)
        IF MIN.DR.BAL NE '' AND MIN.DR.BAL LT 0 THEN
            DR.GHRG = MIN.DR.BAL * (0.05 / 100)
            GOSUB RET.DATA
            Y.MIN.BAL = ''
        END
*##                END
    REPEAT
*?        END
*?    REPEAT
    RETURN
************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = COMP:"*":FRDT:"*":TODT:"*":CUS.ID:"*":Y.ACC.ID:"*":MIN.DR.BAL:"*":DR.GHRG
    RETURN
*==============================================================
END
