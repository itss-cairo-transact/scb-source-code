* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* WAGDY MOUNIR <Rating>450</Rating>
* NOFILE ROUTINE FOR EXCLE FILE
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.SCB.ACT.LIST(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


    DT = ''
    YTEXT = "Enter Date In Format 'YYYYMM' : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    DT = COMI

    Path = "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&/SCB.ACT.LIST.":DT
*    TEXT = Path ; CALL REM

    OPENSEQ Path TO MY.PATH THEN

*        TEXT = Path ; CALL REM
        EOF = ''

        LOOP WHILE NOT(EOF)
            READSEQ Line FROM MY.PATH THEN
                FLD1              = FIELD(Line,"|",1)
                FLD2              = FIELD(Line,"|",2)
                FLD3              = FIELD(Line,"|",3)
                FLD4              = FIELD(Line,"|",4)
                FLD5              = FIELD(Line,"|",5)
                FLD6              = FIELD(Line,"|",6)
                FLD7              = FIELD(Line,"|",7)

                Y.RET.DATA<-1> = FLD1:'*':FLD2:'*':FLD3:'*':FLD4:'*':FLD5 :'*':FLD6:'*':FLD7

            END ELSE
                EOF = 1
            END
        REPEAT
        CLOSESEQ MY.PATH

    END
