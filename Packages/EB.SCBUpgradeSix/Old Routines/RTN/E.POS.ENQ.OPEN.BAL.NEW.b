* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
* Version 5 02/06/00  GLOBUS Release No. R06.005 16/04/07
    SUBROUTINE E.POS.ENQ.OPEN.BAL.NEW
*
* 14/25/96 - GB9601487
*            Created
*
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POS.MVMT.TODAY
*
* 26/01/98 - GB9800050
*            Read from the enquiry data file selected and not
*            POS.MVMT.TODAY
*
* 04/03/98 - GB9800195
*            Show OUR.REF as NO.ITEMS
*
    MESSAGE = 'CALCULATING OPENING BALANCE'
    CALL DISPLAY.MESSAGE(MESSAGE,3)
*
*      LOCATE "ONLINE.ONLY" IN ENQ.SELECTION<2,1> SETTING OL.POS THEN
    ONLINE.ONLY = 'Y'
*     END ELSE
*       ONLINE.ONLY = ''
*    END
*
    LOCATE "AMOUNT.RANGE" IN ENQ.SELECTION<2,1> SETTING AMT.POS THEN
        AMT.START = ENQ.SELECTION<4,AMT.POS>[' ',1,1]
        AMT.END = ENQ.SELECTION<4,AMT.POS>[' ',2,1]
        IF AMT.END = '' THEN AMT.END = AMT.START
    END ELSE
        AMT.START = '' ; AMT.END = ''
    END

MAIN.PARA:
*=========
*
* Select file to be read
*
*Line [ 60 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    KEY.LIST = ID:@FM:ENQ.KEYS
    NKEY = KEY.LIST ;* Build list of keys for enquiry
    NEW.KEYS = ''
    YPOS.OPEN.BAL = 0
*
* Read the correct record and store in R.RECORD
*
    LOOP
        REMOVE YKEY FROM NKEY SETTING YD
        AST.POS = INDEX(YKEY,'*',1)
        IF AST.POS THEN
            AST.POS += 1
            SEQ.NO = YKEY['*',2,1]
*           SEQ.NO += 0

            IF SEQ.NO = '' THEN
                SEQ.NO = '0'
            END
        END
*
    UNTIL YKEY = ""
*
        IF SEQ.NO = '0' OR AMT.START NE '' OR ONLINE.ONLY = 'Y' THEN
            GOSUB READ.FILE
            BEGIN CASE
            CASE SEQ.NO = '0'
                YPOS.OPEN.BAL += R.RECORD<PSE.AMOUNT.FCY>
            CASE ONLINE.ONLY = 'Y' AND INDEX('BC', R.RECORD<PSE.SYSTEM.STATUS>, 1) > 0    ;* Ignore EOD stuff
            CASE AMT.START NE ''        ;* Check is amount is in range
                IF ABS(R.RECORD<PSE.AMOUNT.FCY>) LT AMT.START OR ABS(R.RECORD<PSE.AMOUNT.FCY>) GT AMT.END THEN
                END ELSE
                    IF NEW.KEYS THEN
*Line [ 93 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                        NEW.KEYS := @FM:YKEY
                    END ELSE
                        NEW.KEYS = YKEY
                    END
                END
            CASE 1
                IF NEW.KEYS THEN
*Line [ 101 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                    NEW.KEYS := @FM:YKEY
                END ELSE
                    NEW.KEYS = YKEY
                END
            END CASE
        END ELSE
            IF NEW.KEYS THEN
*Line [ 109 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                NEW.KEYS := @FM:YKEY
            END ELSE
                NEW.KEYS = YKEY
            END
        END
*
    REPEAT
    O.DATA = YPOS.OPEN.BAL
*
    IF NEW.KEYS<1> THEN
        YKEY = NEW.KEYS<1>
        GOSUB READ.FILE
    END ELSE
        R.RECORD = ''
        R.RECORD<PSE.OUR.REFERENCE> = 'NO.ITEMS'
    END
    DEL NEW.KEYS<1>
    ENQ.KEYS = NEW.KEYS
    ID = NEW.KEYS<1>
*
* Now clear out the message
*
    MESSAGE = ' '
    CALL DISPLAY.MESSAGE(MESSAGE, '3')
*
    RETURN
*
READ.FILE:
*=========
*

**    READ R.RECORD FROM F.DATA.FILE, YKEY ELSE NULL
CALL F.READ(F.DATA.FILE, YKEY,R.RECORD,F.DATA.FILE,ERR.A1)
*Line [ 143 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    IF R.RECORD<PSE.CURRENCY> MATCHES "":@VM:LCCY THEN
        R.RECORD<PSE.AMOUNT.FCY> = R.RECORD<PSE.AMOUNT.LCY> ;* makes calculation easier
    END
    RETURN
*
END
