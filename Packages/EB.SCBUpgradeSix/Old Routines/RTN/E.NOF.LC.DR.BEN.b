* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.LC.DR.BEN(Y.RET.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

**------------------------------------
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
*-------------------------------------------------------------------------
    RETURN
*==============================================================
INITIATE:
    CUST.NAME        = ''
    CUST.NAME1       = ''
    LC.NU            = ''
    OLD.LC.NU        = ''
    ISSU.BANK.NU     = ''
    ISSU.BANK.NAME   = ''
    BEN.CUS.NU       = ''
    CONF.INST        = ''
    LC.CUR           = ''
    LC.AMT           = ''
    LIAB.AMT         = ''
    DEV.EXP.DAT      = ''
    TD1 = TODAY

    RETURN
*===============================================================
CALLDB:
    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    FN.DRW = 'FBNK.DRAWINGS' ; F.DRW = '' ; R.DRW = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF (FN.CU,F.CU)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DRW,F.DRW)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*========================================================================

    CLR.AMT = 0

    LOCATE "BENEFICIARY" IN D.FIELDS<1> SETTING ACC.POS THEN
        BENF = D.RANGE.AND.VALUE<ACC.POS>
    END

    TEXT=BENF;CALL REM

*    TEXT =  ACCTT ; CALL REM


    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND BENEFICIARY LIKE ":BENF:" AND EXPIRY.DATE GE ": TODAY :" AND CO.CODE EQ EG0010099 BY OLD.LC.NUMBER "

    CALL EB.READLIST(T.SEL,KEY.LIST,"", SELECTED, ASD)

    TEXT = SELECTED ; CALL REM
    ENT.NO = ''
    LOOP

        REMOVE LC.ID  FROM KEY.LIST SETTING POS
    WHILE LC.ID:POS
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)
        ISSU.BNK.NO = R.LC<TF.LC.ISSUING.BANK.NO>
        CALL F.READ(FN.CU,ISSU.BNK.NO,R.CU,F.CU,ERR)
        CUST.NAME        = R.CU<EB.CUS.SHORT.NAME>
        LC.NU            = LC.ID[1,12]
        OLD.LC.NU        = R.LC<TF.LC.OLD.LC.NUMBER>
        ISSU.BANK.NO     = R.LC<TF.LC.ISSUING.BANK>
        ISSU.BANK.NAME   = CUST.NAME
        BEN.CU           = R.LC<TF.LC.BENEFICIARY><1,1>
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,BEN.CU,LOCAL.REF)
        CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME>
        CONF.INST        = R.LC<TF.LC.CONFIRM.INST>
        LC.CUR           = R.LC<TF.LC.LC.CURRENCY>
        LC.AMT           = R.LC<TF.LC.LC.AMOUNT>
        LIAB.AMT         = R.LC<TF.LC.LIABILITY.AMT>
        EXP.DAT          = R.LC<TF.LC.EXPIRY.DATE>
        ADV.DAT          = R.LC<TF.LC.ADVICE.EXPIRY.DATE>


        Y.RET.DATA <-1>  = CUST.NAME :"*": LC.NU :"*": BEN.CU :"*": LC.CUR :"*": LC.AMT :"*": LIAB.AMT :"*": EXP.DAT :"*": OLD.LC.NU :"*": CUST.NAME1 :"*": CONF.INST :"*": ISSU.BNK.NO :"*": ADV.DAT
    REPEAT

***-------------------------------------------------------
    R.LC = ''
    CLR.AMT = 0
    T.SEL = "SELECT FBNK.DRAWINGS WITH LC.CREDIT.TYPE LIKE LE... AND MATURITY.REVIEW GE ": TODAY :" AND CO.CODE EQ EG0010099 BY REFRENCE "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    ENT.NO = ''
    LOOP
        REMOVE DRW.ID FROM KEY.LIST SETTING POS
    WHILE DRW.ID:POS
        LC.ID = DRW.ID[1,12]
*******************************
        K.SEL ="SELECT FBNK.LETTER.OF.CREDIT WITH ID EQ ":LC.ID:" AND BENEFICIARY LIKE ":BENF
        CALL EB.READLIST(K.SEL,KEY.LIST1,"", SELECTED1, ASD1)

        ENT.NO = ''
        LOOP

            REMOVE LC.ID1  FROM KEY.LIST1 SETTING POS1
        WHILE LC.ID1:POS1

            CALL F.READ(FN.LC,LC.ID1,R.LC,F.LC,ERR)

            EXP.DAT      = R.LC<TF.LC.EXPIRY.DATE>

            IF EXP.DAT LT TODAY THEN
                R.LC.ISSU.BNK.1 = R.LC<TF.LC.ISSUING.BANK.NO>
                CALL F.READ(FN.CU,R.LC.ISSU.BNK.1,R.CU,F.CU,ERR)
                CUST.NAME.1        = R.CU<EB.CUS.SHORT.NAME>
                LC.NU.1            = LC.ID[1,12]
                OLD.LC.NU.1        = R.LC<TF.LC.OLD.LC.NUMBER>
                ISSU.BANK.NU.1     = R.LC<TF.LC.ISSUING.BANK>
                ISSU.BANK.NAME.1   = CUST.NAME
                BEN.CU.1           = R.LC<TF.LC.BENEFICIARY><1,1>

                CALL F.READ(FN.CU,BEN.CU.1,R.CU1,F.CU,ERR)

                CUST.NAME.2        = R.CU1<EB.CUS.SHORT.NAME>

                CONF.INST.1        = R.LC<TF.LC.CONFIRM.INST>
                LC.CUR.1           = R.LC<TF.LC.LC.CURRENCY>
                LC.AMT.1           = R.LC<TF.LC.LC.AMOUNT>
                LIAB.AMT.1         = R.LC<TF.LC.LIABILITY.AMT>
                EXP.DAT.1          = R.LC<TF.LC.EXPIRY.DATE>
                ADV.DAT.1          = R.LC<TF.LC.ADVICE.EXPIRY.DATE>


                Y.RET.DATA <-1>  = CUST.NAME.1 :"*": LC.NU.1 :"*": BEN.CU.1 :"*": LC.CUR.1 :"*": LC.AMT.1 :"*": LIAB.AMT.1 :"*": EXP.DAT.1 :"*": OLD.LC.NU.1 :"*": CUST.NAME.2 :"*": CONF.INST.1 :"*": R.LC.ISSU.BNK.1 :"*": ADV.DAT.1


            END
        REPEAT
    REPEAT
***-------------------------------------------------------
    RETURN
END
