* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-46</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.LIMIT.FX.1(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''

    FN.LIM = 'FBNK.LIMIT'
    F.LIM  = ''
    R.LIM  = ''
    CALL OPF(FN.LIM,F.LIM)

    T.SEL = "SELECT FBNK.LIMIT WITH @ID LIKE 994... AND @ID UNLIKE 994999... AND NOTES UNLIKE ...CREATE...DEFAULT... BY @ID  "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LIM,KEY.LIST<I>,R.LIM,F.LIM,EER)

            CUS.ID = R.LIM<LI.LIABILITY.NUMBER>
            PROD   = R.LIM<LI.LIMIT.PRODUCT>
            CURR   = R.LIM<LI.LIMIT.CURRENCY>
            AMTR   = R.LIM<LI.MAXIMUM.TOTAL>

    VAL.1='' ; VAL.2='' ; VAL.3=''; VAL.4=''; VAL.5=''; VAL.6=''
****************************************************************
            IF PROD EQ 5700 THEN
                VAL.1=AMTR / 1000000
            END
            IF PROD EQ 3000 AND CURR EQ 'EGP' THEN
                VAL.2=AMTR / 1000000
            END
            IF PROD EQ 3000 AND CURR EQ 'USD' THEN
                VAL.3=AMTR / 1000000
            END
            IF PROD EQ 1010  THEN
                VAL.4=AMTR / 1000000
            END
            IF PROD EQ 1020  THEN
                VAL.5=AMTR / 1000000
            END
            IF PROD EQ 1030  THEN
                VAL.6=AMTR / 1000000
            END
Y.RET.DATA<-1> = CUS.ID:'*':VAL.1:'*':VAL.2:'*':VAL.3:'*':VAL.4:'*':VAL.5:'*':VAL.6 
        NEXT I
    END
    RETURN
END
