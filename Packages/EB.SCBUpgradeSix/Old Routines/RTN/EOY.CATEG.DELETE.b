* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOY.CATEG.DELETE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY

    FN.CATEG = 'FBNK.CATEG.ENTRY' ; F.CATEG = '' ; R.CATEG = ''
    CALL OPF(FN.CATEG,F.CATEG)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
*    T.SELL = "SELECT FBNK.CATEG.ENTRY WITH PL.CATEGORY EQ 54000 OR PL.CATEGORY EQ 53000 OR PL.CATEGORY EQ 54151 OR PL.CATEGORY EQ 53004  AND COMPANY.CODE NE EG0010099"
    T.SELL = "SELECT FBNK.CATEG.ENTRY WITH PL.CATEGORY EQ 54000 OR PL.CATEGORY EQ 54151 AND COMPANY.CODE NE EG0010099"

    CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CATEG,KEY.LIST<I>,R.CATEG,F.CATEG,READ.ERR)
***        DELETE F.CATEG , KEY.LIST<I>
        CALL F.DELETE(FN.CATEG , KEY.LIST<I>)
        CALL JOURNAL.UPDATE('')
    NEXT I
    RETURN
END
