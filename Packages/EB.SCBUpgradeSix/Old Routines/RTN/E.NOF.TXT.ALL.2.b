* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>450</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.TXT.ALL.2(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


**------------
    COMP = ''
    COMP<1,1> ='01'
    COMP<1,2> ="02"
    COMP<1,3> ="03"
    COMP<1,4> ="04"
    COMP<1,5> ="05"
    COMP<1,6> ="06"
    COMP<1,7> ="07"
    COMP<1,8> ="09"
    COMP<1,9> ="10"
    COMP<1,10> ="11"
    COMP<1,11> ="12"
    COMP<1,12> ="13"
    COMP<1,13> ="14"
    COMP<1,14> ="15"
    COMP<1,15> ="20"
    COMP<1,16> ="21"
    COMP<1,17> ="22"
    COMP<1,18> ="23"
    COMP<1,19> ="30"
    COMP<1,20> ="31"
    COMP<1,21> ="32"
    COMP<1,22> ="35"
    COMP<1,23> ="40"
    COMP<1,24> ="50"
    COMP<1,25> ="60"
    COMP<1,26> ="70"
    COMP<1,27> ="80"
    COMP<1,28> ="81"
    COMP<1,29> ="88"
    COMP<1,30> ="90"
    COMP<1,31> ="99"

    FOR II = 1 TO 31


        COMP<1,II>  = TRIM(COMP<1,II>, "0", "L")
**--------------

        IDD = "aib.out." :COMP<1,II>

        MY.PATH=''
        Path = "/hq/opce/bclr/user/aibz/":IDD

        TEXT = "PATH = ":Path ; CALL REM

        OPENSEQ Path , IDD TO MY.PATH THEN

*           CALL F.READ( FN.IN,TEXT.ID, R.IN, F.IN, ETEXT.TXT)

            EOF = ''
            I = 0
            LOOP WHILE NOT(EOF)
                READSEQ Line FROM MY.PATH THEN
                    TEXT = "IN" ; CALL REM
                    AMTT              = FIELD(Line,"|",10)
                    TEXT = "AMTT = ":AMTT ; CALL REM
                    IF AMTT THEN
                        I = I + 1
                        AMTTT = AMTT + AMTTT
                    END
                END ELSE
                    EOF = 1
                END
            REPEAT
            CLOSESEQ MY.PATH

            Y.RET.DATA<-1> = I:'*':AMTTT:'*':COMP<1,II>
        END
        AMTTT=0
    NEXT II
    RETURN
END
