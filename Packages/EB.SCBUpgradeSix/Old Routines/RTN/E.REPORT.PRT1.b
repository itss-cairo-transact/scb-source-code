* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>46</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.REPORT.PRT1

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_ENQUIRY.COMMON
    $INSERT T24.BP I_F.USER

    COMMON /ENQHOLD/ C$F.HOLD, C$USER.LIST, C$REPORT.LIST
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID, HOLD.ID, REP.TITLE

*--------------------------------------------------------------------

    HOLD.ID = O.DATA

*Line [ 33 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2.DEV


    TEXT = 'THE REPORT IS PRINTING BY ':REPORT.ID ;CALL REM
*    TEXT = 'THE REPORT IS PRINTING BY ':DEVICE.ID ;CALL REM
*    TEXT = 'THE REPORT IS PRINTING BY ':PRINTER.ID ;CALL REM
*    TEXT = 'THE REPORT IS PRINTING BY ':HOLD.ID ;CALL REM
**---------------

    BEGIN CASE

    CASE DEVICE.ID EQ "lines"
        GOSUB PRINT.LINE.BY.LINE

    CASE DEVICE.ID NE ''
        GOSUB PRINT.NON.INFOTEST


    CASE DEVICE.ID EQ ''
        GOSUB PRINT.INFOTEST

    END CASE



    RETURN
*----------------------------------------------------------------------
PRINT.NON.INFOTEST:

    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

*   --------------------

    EXE.CMD = "lp -d":DEVICE.ID:" ../bnk.data/eb/&HOLD&/":HOLD.ID


    EXECUTE EXE.CMD



    RETURN
*--------------------------------------------------------------------
PRINT.HEAD:
    PR.HD ="'L'":SPACE(1)
    HEADING PR.HD
    RETURN
*--------------------------------------------------------------------
PRINT.INFOTEST:


    SET.ARABIC = "LANG=Ar_AA"
    SET.US = "LANG=en_US"
*Line [ 89 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF PUTENV(SET.ARABIC) THEN NULL



    BEGIN CASE
    CASE PRINTER.ID EQ "infotest"
        EXE.CMD = "qprt -P ":PRINTER.ID:" -p 13 ../bnk.data/eb/&HOLD&/":HOLD.ID
    CASE PRINTER.ID NE "infotest"
        EXE.CMD = "qprt -P ":PRINTER.ID:" -p 15 ../bnk.data/eb/&HOLD&/":HOLD.ID
    END CASE



    EXE.CMD = "qprt -P ":PRINTER.ID:" -p 13 ../bnk.data/eb/&HOLD&/":HOLD.ID


    EXECUTE EXE.CMD

    RETURN


*      15333001407375100

*--------------------------------------------------------------------
PRINT.LINE.BY.LINE:

    CALL PRINTER.ON(REPORT.ID,'')

    GOSUB PRINT.HEAD
    GOSUB PRINT.REPORT

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*
    RETURN
*--------------------------------------------------------------------
PRINT.REPORT:

    SEQ.FILE.NAME = " ../bnk.data/eb/&HOLD&/"
    REP.NAME = HOLD.ID

    OPENSEQ SEQ.FILE.NAME,REP.NAME TO SEQ.FILE.POINTER ELSE
        CREATE SEQ.FILE.POINTER ELSE
            STOP
            RETURN
        END
    END
*--------------------------------------------------------------------
    TEXT = 'THE REPORT IS PRINTING ON ':REPORT.ID ;CALL REM
    EOF = ''

* ----------------------------
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN

*            IF Y.MSG NE '' THEN
            PRINT Y.MSG
*           END

        END ELSE
            EOF = 1
        END
    REPEAT
* ----------------------------

    CLOSESEQ SEQ.FILE.POINTER

    RETURN
