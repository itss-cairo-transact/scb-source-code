* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.LC.AC1 (ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""

*TEXT = "HHHH" ; CALL REM

*    IF APPLICATION = 'COLLATERAL' THEN
*    CUST = R.NEW(TF.LC.APPLICANT.CUSTNO)
*    CUST = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.LC.CU>
    CUST = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGE.CU>
*TEXT = "CU" : CUST ; CALL REM
*    CUR  = R.NEW(FT.DEBIT.CURRENCY)
*TEXT = CUSS ; CALL REM
*        T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUSS:" AND CATEGORY EQ 9030"
*    T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUSS:" AND (CATEGORY EQ 1111 OR CATEGORY EQ 9030)"

*    T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND (CATEGORY EQ 1001 OR CATEGORY EQ 6501 OR CATEGORY EQ 1201 OR CATEGORY EQ 1202) "
*    T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND (CATEGORY GE 1101 AND  CATEGORY LE 1202  OR CATEGORY GE 1290 AND  CATEGORY LE 1599 ) OR CATEGORY EQ 1001 OR CATEGORY EQ 6501"
*MSABRY 27/1/2009
    T.SEL ="SELECT FBNK.ACCOUNT WITH CO.CODE EQ ":COMP:" AND CUSTOMER EQ ":CUST :" AND (CATEGORY GE 1101 AND  CATEGORY LE 1202  OR CATEGORY GE 1290 AND  CATEGORY LE 1599 ) OR CATEGORY EQ 1001 OR CATEGORY EQ 6501"
*    T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ": R.NEW(FT.DEBIT.CURRENCY)

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    TEXT = T.SEL ; CALL REM

*TEXT = SELECTED ; CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"

*   END

    END
*******************************************************************************************
    RETURN
END
