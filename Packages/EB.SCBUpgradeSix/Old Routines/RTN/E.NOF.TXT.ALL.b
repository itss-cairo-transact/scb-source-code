* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>140</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.TXT.ALL(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
**------------
    COMAND = " chmod -R 777 /hq/opce/bclr/user/dltx"
    EXECUTE COMAND

    COMP  = ''
    FN.CO = "F.COMPANY" ;  F.CO = ""
    CALL OPF(FN.CO, F.CO)

    T.SEL = "SELECT F.COMPANY BY @ID "
    CALL EB.READLIST(T.SEL,SEL.LIST,"",NO.OF.REC,RET.CODE)

    FOR II = 1 TO NO.OF.REC
        COMP = SEL.LIST<II>[8,2]
        DAL.TOD = ''
        USR.DEP = "1700":COMP
        CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)

        DAL.TOD = TODAY
        IF LOCT EQ '1'  THEN
            CALL CDT('',DAL.TOD,"+1W")
*            CALL CDT('',DAL.TOD,"+2W")
        END ELSE
            CALL CDT('',DAL.TOD,"+2W")
*            CALL CDT('',DAL.TOD,"+3W")
        END

        DAL.YEAR  = DAL.TOD[1,4]
        DAL.MONTH = DAL.TOD[5,2]
        DAL.DAY   = DAL.TOD[7,2]
        COMP = COMP - 1 + 1
**------------------------------------------------
        IDD  =  DAL.YEAR:"-":DAL.MONTH:"-":DAL.DAY:"-":"Sess2OUT":COMP:".txt"
        Path = "/hq/opce/bclr/user/dltx/"

        OPENSEQ Path , IDD TO MY.PATH THEN
            EOF = ''
            I   = 0
            LOOP WHILE NOT(EOF)
                READSEQ Line FROM MY.PATH THEN
                    AMTT = FIELD(Line,",",10)
                    IF AMTT THEN
                        I = I + 1
                        AMTTT = AMTT + AMTTT
                    END
                END ELSE
                    EOF = 1
                END
            REPEAT
            CLOSESEQ MY.PATH
            Y.RET.DATA<-1> = I:'*':AMTTT:'*':COMP:'*':DAL.TOD
        END
        AMTTT=0
    NEXT II
    RETURN
END
