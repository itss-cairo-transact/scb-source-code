* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.REPORT.LIST.3J
*    PROGRAM E.REPORT.LIST.3J

    $INSERT  TEMENOS.BP I_COMMON
    $INSERT  TEMENOS.BP I_EQUATE
    $INSERT  TEMENOS.BP I_ENQUIRY.COMMON
    $INSERT  T24.BP I_F.HOLD.CONTROL
    $INSERT  T24.BP I_F.USER
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP

*-----------------------------------------------------------------------
*    DEPT = O.DATA

*    TEXT = DEPT;CALL REM

*    STR.ONE  = "������ ������"
*    STR.ONE  = "����� ��������"
*    STR.ONE  = "���� �����"
*    STR.ONE  = "����� �������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
    STR.ONE  = "������� ��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "����������"
*    STR.ONE  = "������ �������"
*    STR.ONE  = "�������� ������"
*    STR.ONE  = "���� �������"

*-------------------
    STR.TWO = ''
    COMP.ALL = ''
    WS.COMP = ID.COMPANY
    STR.ONE = WS.COMP:"DEP012..."
*Line [ 58 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.PRT2.ALL
*------------------------------------------



*    CALL E.REPORT.PRT2
*    COMP.ALL = ''
*    STR.TWO = ''
*    WS.COMP = ''

    RETURN
*-----------------------------------------------------------------------
