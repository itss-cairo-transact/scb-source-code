* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.PAYMENT.STOP.TYPE.SEL(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP.TYPE

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""

  IF APPLICATION = 'SCB.CHQ.RETURN' THEN
        T.SEL = "SELECT F.PAYMENT.STOP.TYPE WITH @ID GE ":20
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO REGIONS FOUND"
        END
     END
   IF APPLICATION = 'PAYMENT.STOP' THEN
         T.SEL = "SELECT F.PAYMENT.STOP.TYPE WITH @ID LT ":20
         KEY.LIST=""
         SELECTED=""
         ER.MSG=""
         CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

         IF KEY.LIST THEN
             FOR I = 1 TO SELECTED
                 ENQ<2,I> = "@ID"
                 ENQ<3,I> = "EQ"
                 ENQ<4,I> = KEY.LIST<I>
             NEXT I
         END ELSE
             ENQ.ERROR = "NO REGIONS FOUND"
         END
    END

    RETURN
END
