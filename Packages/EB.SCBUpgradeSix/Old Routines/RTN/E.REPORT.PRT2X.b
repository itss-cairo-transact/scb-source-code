* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>33</Rating>
*-----------------------------------------------------------------------------
    PROGRAM  E.REPORT.PRT2X
*******    SUBROUTINE E.REPORT.PRT2X
    $INSERT  TEMENOS.BP I_COMMON
    $INSERT  TEMENOS.BP I_EQUATE
    $INSERT  T24.BP I_RC.COMMON
    $INSERT  TEMENOS.BP I_ENQUIRY.COMMON
    $INSERT  T24.BP I_F.HOLD.CONTROL
    $INSERT  T24.BP I_F.USER
    $INSERT  T24.BP I_USER.ENV.COMMON
    $INSERT  T24.BP I_F.COMPANY
*-----------------------------------------------------------------------

    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID, HOLD.ID, REP.TITLE
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP
*-----------------------------------------------------------------------
**    TEXT = 'THE REPORT IS PRINTING ON ':PRINTER.ID ;CALL REM
**    TEXT = PRINTER.ID ;CALL REM
**    TEXT = DEVICE.ID ;CALL REM
**    TEXT = REPORT.ID ;CALL REM
**    TEXT = HOLD.ID ;CALL REM
**    TEXT = REP.TITLE ;CALL REM
**    TEXT = REP.NAME; CALL REM
**    TEXT = WS.COMP; CALL REM
**    TEXT = DEVICE.ID; CALL REM
**    TEXT = REPORT.ID; CALL REM

    BEGIN CASE
*------------------------
    CASE DEVICE.ID EQ "lines"
        GOSUB PRINT.LINE.BY.LINE
*------------------------
    CASE DEVICE.ID NE ''
        GOSUB PRINT.WITH.DEVICE
*------------------------
    CASE  DEVICE.ID EQ ''
        GOSUB PRINT.WITHOUT.DEVICE
*------------------------
    END CASE


    RETURN

***    STOP

*--------------------------------------------------------------------
PRINT.WITH.DEVICE:

    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD
*   ----------------------------
*


    EXE.CMD = "lpr -h -P":DEVICE.ID:" ../bnk.data/eb/&HOLD&/":HOLD.ID


    EXECUTE EXE.CMD

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN

*--------------------------------------------------------------------
PRINT.WITHOUT.DEVICE:


    SET.ARABIC = "LANG=Ar_AA"
    SET.US = "LANG=en_US"
*Line [ 88 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF PUTENV(SET.ARABIC) THEN NULL

**  ������  **

    BEGIN CASE
    CASE PRINTER.ID EQ "infotest"
        EXE.CMD = "qprt -P ":PRINTER.ID:" -p 13 ../bnk.data/eb/&HOLD&/":HOLD.ID

    CASE PRINTER.ID NE "infotest"
**        EXE.CMD = "qprt -P ":PRINTER.ID:" -p 15 ../bnk.data/eb/&HOLD&/":HOLD.ID
        EXE.CMD = "qprt -P ":PRINTER.ID:" -p 13 ../bnk.data/eb/&HOLD&/":HOLD.ID
    END CASE

    EXECUTE EXE.CMD

***    TEXT = EXE.CMD ; CALL REM

    RETURN

*-------------------------------------------------------------------------
PRINT.LINE.BY.LINE:

    CALL PRINTER.ON(REPORT.ID,'')

    GOSUB PRINT.HEAD
    GOSUB PRINT.REPORT

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*
    RETURN
*--------------------------------------------------------------------
PRINT.REPORT:

    SEQ.FILE.NAME = " ../bnk.data/eb/&HOLD&/"
    REP.NAME = HOLD.ID

    OPENSEQ SEQ.FILE.NAME,REP.NAME TO SEQ.FILE.POINTER ELSE
        CREATE SEQ.FILE.POINTER ELSE
            STOP
            RETURN
        END
    END
*--------------------------------------------------------------------
*    TEXT = 'THE REPORT IS PRINTING ON ':REPORT.ID ;CALL REM
    EOF = ''

* ----------------------------
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN

***   1330036010100201

            PRINT.FLAG = 'Y'

            XX.COMP = Y.MSG[1,2]
            XX.CATEG = Y.MSG[9,8]

            YY.COMP = WS.COMP[8,2]

***     IF XX.COMP EQ YY.COMP THEN

            IF XX.CATEG EQ '10100201' THEN
                TEXT = XX.CATEG; CALL REM
            END




            IF XX.CATEG EQ '10100201' THEN

                PRINT.FLAG = 'N'
            END
***     END


**            IF Y.MSG EQ '' THEN
**                PRINT.FLAG = 'N'
**            END


            IF PRINT.FLAG EQ 'Y' THEN
                PRINT Y.MSG
            END

        END ELSE
            EOF = 1
        END
    REPEAT
* ----------------------------

    CLOSESEQ SEQ.FILE.POINTER

    RETURN


*-------------------------------------------------------------------------
PRINT.HEAD:
    PR.HD = "'L'":SPACE(1)
*    PRINT
    HEADING PR.HD
    RETURN
