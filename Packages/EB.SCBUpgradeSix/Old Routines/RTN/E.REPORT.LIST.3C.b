* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-32</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.REPORT.LIST.3C
**    PROGRAM E.REPORT.LIST.3C

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP
*-----------------------------------------------------------------------
*    DEPT = O.DATA

*    TEXT = DEPT;CALL REM

*    STR.ONE  = "������ ������"
*    STR.ONE  = "����� ��������"
*    STR.ONE  = "���� �����"
    STR.ONE  = 'MNGR'
*    STR.ONE  = "����� �������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
*    STR.ONE  = "������� ��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "����������"
*    STR.ONE  = "������ �������"
*    STR.ONE  = "�������� ������"
*    STR.ONE  = "���� �������"


*-------------------
    STR.TWO = ''
    COMP.ALL = ''
    WS.COMP = ID.COMPANY 
*    WS.COMP = 'EG0010013'

    STR.ONE = WS.COMP:"DEP001..."

*   PRINT STR.ONE
*   STOP

*Line [ 70 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.PRT2.ALL


*-------------------
*    COMP.ALL = ''
*    WS.COMP = ''
*    CALL E.REPORT.PRT2
*    STR.TWO = ''
*-------------------


    RETURN

*-----------------------------------------------------------------------
