* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***************************MAHMOUD 22/10/2014******************************
    SUBROUTINE E.NOF.SAVING.ACC(Y.RET.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 39 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    CU.CNT = 0
    CU.FLG = 0
    KK = 0
    K2 = 0
***********************
    DYNO = 0
    TDD = TODAY
    TD1 = TODAY
    CRT TD1
***********************
    COM.AMT = 0
    ARR.REC  = ''
    ARR.DATA = ''
    REC.GEO  = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''


    RETURN
******************************************************
CLEAR.VAR:
*----------
    RETURN
*=======================================
CALLDB:
*-------
    FN.COM = 'F.COMPANY' ; F.COM = '' ; R.COM = ''
    CALL OPF(FN.COM,F.COM)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    FN.AC = "FBNK.ACCOUNT" ; F.AC = "" ; R.AC = "" ; ER.AC = ""
    CALL OPF(FN.AC,F.AC)
    FN.CU.AC = "FBNK.CUSTOMER.ACCOUNT" ; F.CU.AC = "" ; R.CU.AC = "" ; ER.CU.AC = ""
    CALL OPF(FN.CU.AC,F.CU.AC)
    RETURN
*========================================
PROCESS:
*-------
    C.SEL  ="SSELECT ":FN.COM
    CALL EB.READLIST(C.SEL,K.LIST.COM,'',SELECTED.COM,ER.MSG.COM)
    LOOP
        REMOVE COMP FROM K.LIST.COM SETTING POS.COM
    WHILE COMP:POS.COM
        KK = 0
        COM.AMT = 0
        CU.CNT = 0
        CU.FLG = 0
        GOSUB CU.REC
        IF COM.AMT NE 0 THEN
            GOSUB RET.REC
        END
    REPEAT
    RETURN
**************************************************************************
CU.REC:
*-------
****************************//CU//****************************************
    SEL.CU.AC  = "SELECT ":FN.CU.AC:" WITH COMPANY.CO EQ ":COMP
    CALL EB.READLIST(SEL.CU.AC,KEY.CU,'',SELECTED.CU,ERR.CU)
    CRT SELECTED.CU
    LOOP
        REMOVE CU.ID FROM KEY.CU SETTING POSS.CU
    WHILE CU.ID:POSS.CU
        CALL F.READ(FN.CU.AC,CU.ID,R.CU.AC,F.CU.AC,CU.AC.ERR)
        CU.FLG = 0
        GOSUB AC.REC
        IF CU.FLG EQ 1 THEN
            CU.CNT++
            CU.FLG = 0
        END
    REPEAT
    RETURN
***************************//LD//*****************************************
AC.REC:
*----------
    LOOP
        REMOVE AC.ID FROM R.CU.AC SETTING POS.AC1
    WHILE AC.ID:POS.AC1
        CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ERR.AC1)
        IF NOT(ERR.AC1) THEN
            REC.CAT  = R.AC<AC.CATEGORY>
            IF REC.CAT[1,2] EQ '65' THEN CU.FLG = 1 ELSE GOTO NXT.REC
            REC.CUR  = R.AC<AC.CURRENCY>
            REC.COM  = R.AC<AC.CO.CODE>
            REC.AMT  = R.AC<AC.ONLINE.ACTUAL.BAL>
            IF REC.CUR EQ LCCY THEN
                REC.AMT.LCY = REC.AMT
            END ELSE
                CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,REC.CUR,CUR.RATE)
                IF REC.CUR EQ 'JPY' THEN
                    REC.AMT.LCY = REC.AMT / CUR.RATE<1,1>
                END ELSE
                    REC.AMT.LCY = REC.AMT * CUR.RATE<1,1>
                END
            END
            COM.AMT += REC.AMT.LCY
        END
NXT.REC:
    REPEAT
    RETURN
*****************************************************************
RET.REC:
********
    Y.LINE.REC = COMP:"*":CU.CNT:"*":COM.AMT
    Y.RET.DATA<-1>= Y.LINE.REC
**    CRT Y.LINE.REC
    RETURN
*************************************************
END
