* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>3611</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.ACCOUNT.SEL1(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""
*****************************************************************
    IF APPLICATION = 'TELLER' THEN
        IF AF = TT.TE.CURRENCY.1 OR R.NEW(TT.TE.CURRENCY.1)  THEN
            IF R.NEW(TT.TE.CUSTOMER.1) THEN CUST = R.NEW(TT.TE.CUSTOMER.1)
            IF R.NEW(TT.TE.CUSTOMER.2) THEN CUST = R.NEW(TT.TE.CUSTOMER.2)
            CURR = R.NEW(TT.TE.CURRENCY.1)
        END
        IF AF = TT.TE.CUSTOMER.2 OR R.NEW(TT.TE.CUSTOMER.2) THEN
            IF R.NEW(TT.TE.CURRENCY.1) THEN CURR =  R.NEW(TT.TE.CURRENCY.1)
            IF  R.NEW(TT.TE.CURRENCY.2) THEN CURR =  R.NEW(TT.TE.CURRENCY.2)
            CUST = R.NEW(TT.TE.CUSTOMER.2)
        END
*********NESSREEN******************
        TRAN = R.NEW(TT.TE.TRANSACTION.CODE)
        IF R.NEW(TT.TE.CURRENCY.1) THEN CURR =  R.NEW(TT.TE.CURRENCY.1)
        IF R.NEW(TT.TE.CUSTOMER.1) THEN CUST = R.NEW(TT.TE.CUSTOMER.1)
        IF  R.NEW(TT.TE.CURRENCY.2) THEN CURR =  R.NEW(TT.TE.CURRENCY.2)
        IF R.NEW(TT.TE.CUSTOMER.2) THEN CUST = R.NEW(TT.TE.CUSTOMER.2)

        IF TRAN = 13 THEN
            IF AF = TT.TE.CURRENCY.2 OR R.NEW(TT.TE.CURRENCY.2)  THEN
                IF R.NEW(TT.TE.CUSTOMER.1) THEN CUST = R.NEW(TT.TE.CUSTOMER.1)
                IF R.NEW(TT.TE.CUSTOMER.2) THEN CUST = R.NEW(TT.TE.CUSTOMER.2)
                CURR = R.NEW(TT.TE.CURRENCY.2)
            END
            IF AF = TT.TE.CUSTOMER.2 OR R.NEW(TT.TE.CUSTOMER.2) THEN
                IF R.NEW(TT.TE.CURRENCY.1) THEN CURR =  R.NEW(TT.TE.CURRENCY.1)
                IF  R.NEW(TT.TE.CURRENCY.2) THEN CURR =  R.NEW(TT.TE.CURRENCY.2)
                CUST = R.NEW(TT.TE.CUSTOMER.2)
            END
            IF AF = TT.TE.CURRENCY.1 OR R.NEW(TT.TE.CURRENCY.1)  THEN
                IF R.NEW(TT.TE.CUSTOMER.1) THEN CUST = R.NEW(TT.TE.CUSTOMER.1)
                IF R.NEW(TT.TE.CUSTOMER.2) THEN CUST = R.NEW(TT.TE.CUSTOMER.2)
                CURR = R.NEW(TT.TE.CURRENCY.1)
            END
            IF AF = TT.TE.CUSTOMER.1 OR R.NEW(TT.TE.CUSTOMER.1) THEN
                IF R.NEW(TT.TE.CURRENCY.1) THEN CURR =  R.NEW(TT.TE.CURRENCY.1)
                IF  R.NEW(TT.TE.CURRENCY.2) THEN CURR =  R.NEW(TT.TE.CURRENCY.2)
                CUST = R.NEW(TT.TE.CUSTOMER.1)
            END

        END

        BEGIN CASE
        CASE TRAN = 37
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY EQ " :1205 :" OR CATEGORY EQ " :1206:")"
        CASE TRAN = 38
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY EQ " :1207 :" OR CATEGORY EQ " :1208:")"
        CASE TRAN = 39 OR TRAN = 43
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ":CURR :" AND CATEGORY EQ " :1005
****UPDATED BY NESSREEN AHMED 05/02/2009****************************
**      CASE TRAN = 41 OR TRAN = 42
        CASE TRAN = 41 OR TRAN = 42  OR TRAN = 21 OR TRAN = 105 OR TRAN = 106
**NESSREEN AHMED 10/01/2007****************************
*T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ":CURR :" AND CATEGORY EQ " :6501
* T.SEL = "SELCT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY EQ " :6501 :" OR CATEGORY EQ " :6502 :" OR CATEGORY EQ " :6503 :" OR CATEGORY EQ " :6504:")"
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY EQ 6501 OR CATEGORY EQ 6502 OR CATEGORY EQ 6503 OR CATEGORY EQ 6504 OR CATEGORY EQ 6511 OR CATEGORY EQ 6512)"
*******************************************************
        CASE TRAN = 9
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
****UPDATED BY NESSREEN AHMED 05/02/2009**********************
            T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY GE '1101' AND  CATEGORY LE '1202'  OR CATEGORY GE '1290' AND  CATEGORY LE '1599' ) OR CATEGORY EQ '1001'"
**     CASE TRAN = 2
        CASE TRAN = 2  OR TRAN = 20 OR TRAN = 97 OR TRAN = 98
*     T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY EQ " :1001 :" OR CATEGORY EQ " :1201 :" OR CATEGORY EQ " :1202 :")"
            T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY GE '1101' AND  CATEGORY LE '1202'  OR CATEGORY GE '1290' AND  CATEGORY LE '1599' ) OR CATEGORY EQ '1001'"
****UPDATED BY NESSREEN AHMED 05/02/2009**********************
**     CASE TRAN = 82
        CASE TRAN = 82 OR TRAN = 92 OR TRAN = 95 OR TRAN = 96
*    T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ":CURR :" AND (CATEGORY EQ " :1001 :" OR CATEGORY EQ " :1201 :" OR CATEGORY EQ " :1202 :")"
*****UPDATED BY NESSREEN AHMED 21/01/2010*******************************
*****  T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :"  AND (CATEGORY GE '1101' AND  CATEGORY LE '1202'  OR CATEGORY GE '1290' AND  CATEGORY LE '1599' ) OR CATEGORY EQ '1001'"
       T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :"  AND (CATEGORY GE '1101' AND  CATEGORY LE '1202'  OR CATEGORY GE '1290' AND  CATEGORY LE '1599' ) OR CATEGORY EQ '1001' OR CATEGORY EQ '6511' OR CATEGORY EQ '6512'"
*****END OF UPDATE 21/01/2010*******************************************
****UPDATED BY NESSREEN AHMED 11/03/2009**********************
        CASE TRAN = 62
            T.SEL ="SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST :" AND CURRENCY EQ ":CURR :"  AND (CATEGORY GE '1100' AND  CATEGORY LE '1599') "
****END OF UPDATED 11/03/2009*********************************
        CASE OTHERWISE
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ " :CURR
        END CASE

        IF NOT(CUST)  THEN
            BEGIN CASE
            CASE TRAN = 7 OR TRAN = 8
                T.SEL = "SELECT FBNK.ACCOUNT WITH CURRENCY EQ ":CURR:" AND (CATEGORY EQ " :11043 :" OR CATEGORY EQ " :11044 :" OR CATEGORY EQ " :11035 :" OR CATEGORY EQ " :16026:")"
            CASE TRAN = 32 OR TRAN = 36
                T.SEL = "SELECT FBNK.ACCOUNT WITH CURRENCY EQ ":CURR:" AND (CATEGORY EQ " :11039 :" OR CATEGORY EQ " :11040:")"
            CASE TRAN = 33 OR TRAN = 34
                T.SEL = "SELECT F.CATEGORY WITH @ID GT 60000"
            CASE OTHERWISE
                T.SEL = "SELECT FBNK.ACCOUNT WITH @ID LIKE ":CURR:'...'
            END CASE
        END
************************************************************************
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO ACCOUNTS FOUND"
        END
    END

* **************************************************************************************************
    IF APPLICATION = 'FUNDS.TRANSFER' THEN
* USER.BRANCH = R.USER< EB.USE.DEPARTMENT.CODE>
        IF R.NEW(FT.DEBIT.THEIR.REF) = '1' THEN
            CUST = R.NEW(FT.DEBIT.CUSTOMER)
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ 'EGP'"
        END
        IF R.NEW(FT.DEBIT.THEIR.REF) = '2' THEN
            CUST = R.NEW(FT.DEBIT.CUSTOMER)
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ": R.NEW(FT.DEBIT.CURRENCY)
        END
        IF R.NEW(FT.DEBIT.THEIR.REF) = '3' THEN
            CUST = R.NEW(FT.DEBIT.CUSTOMER)
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY NE 'EGP' AND CURRENCY NE ": R.NEW(FT.DEBIT.CURRENCY)
        END
        IF R.NEW(FT.DEBIT.CURRENCY) THEN CURR = R.NEW(FT.DEBIT.CURRENCY)
* *IF R.NEW(FT.CREDIT.CUSTOMER) THEN CUST = R.NEW(FT.CREDIT.CUSTOMER)
* *IF R.NEW(FT.CREDIT.CURRENCY) THEN CURR = R.NEW(FT.CREDIT.CURRENCY)
*  * IF R.NEW(FT.DEBIT.ACCT.NO) AND R.NEW(FT.CREDIT.ACCT.NO) THEN
* *  CUST = R.NEW(FT.DEBIT.CUSTOMER)
* *  CURR = R.NEW(FT.DEBIT.CURRENCY)
* *  END
*
*   *IF R.NEW(FT.DEBIT.ACCT.NO) AND NOT(R.NEW(FT.CREDIT.ACCT.NO)) THEN
*   IF AF = 4 THEN
*
*    IF R.NEW(FT.DEBIT.CUSTOMER) THEN CUST = R.NEW(FT.DEBIT.CUSTOMER)
*    IF R.NEW(FT.DEBIT.CURRENCY) THEN CURR = R.NEW(FT.DEBIT.CURRENCY)
*  END
*  IF AF = 13 THEN
*       IF R.NEW(FT.CREDIT.CUSTOMER) THEN CUST = R.NEW(FT.CREDIT.CUSTOMER)
*       IF R.NEW(FT.CREDIT.CURRENCY) THEN CURR = R.NEW(FT.CREDIT.CURRENCY)
*  END
*   *IF NOT(R.NEW(FT.CREDIT.CUSTOMER)) THEN CUST = ''
*   *END
*  T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ 'EGP'"
***************************NESSREEN 7/3/2005******************************
*
        IF AF = FT.DEBIT.CUSTOMER OR R.NEW(FT.DEBIT.CUSTOMER) THEN
            IF R.NEW(FT.DEBIT.CURRENCY) THEN CURR =  R.NEW(FT.DEBIT.CURRENCY)
            CUST = R.NEW(FT.DEBIT.CUSTOMER)
        END
        IF AF = FT.CREDIT.CUSTOMER OR R.NEW(FT.CREDIT.CUSTOMER) THEN
            IF R.NEW(FT.CREDIT.CURRENCY) THEN CURR = R.NEW(FT.CREDIT.CURRENCY)
            CUST = R.NEW(FT.CREDIT.CUSTOMER)
        END
        IF AF = FT.CREDIT.CURRENCY THEN
            IF NOT(R.NEW(FT.CREDIT.CUSTOMER)) THEN CUST = ''
        END
        IF R.NEW(FT.TRANSACTION.TYPE) = 'AC' OR 'AC05' OR 'OC' OR 'OD' OR 'IT' OR 'IM' OR 'OC01' THEN
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ": CURR
        END ELSE
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ": CURR :" AND CATEGORY # ":6501
        END
****************************************************
        IF R.NEW(FT.TRANSACTION.TYPE) = 'AC05' OR 'OD' OR 'OT' THEN
*TEXT = "HI" ; CALL REM
            IF R.NEW(FT.LOCAL.REF)<1,FTLR.DRAWER> THEN
*TEXT = "ENTER" ; CALL REM
                IF R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY> THEN CURR = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
                CUST = R.NEW(FT.LOCAL.REF)<1,FTLR.DRAWER>
                T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ": CURR
            END
        END
*****************************************************
        IF NOT(CUST)  THEN
            T.SEL = "SELECT FBNK.ACCOUNT WITH @ID LIKE ":CURR:'...'
        END
*************************************************************************
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO ACCOUNTS FOUND"
        END
************************************************************************
    END
************************************************************************************
    IF APPLICATION = 'LETTER.OF.CREDIT' THEN
        IF AF = TF.LC.APPLICANT.CUSTNO THEN CUST = R.NEW(TF.LC.APPLICANT.CUSTNO)
        IF AF = TF.LC.BENEFICIARY.CUSTNO THEN CUST = R.NEW(TF.LC.BENEFICIARY.CUSTNO)
        IF AF = TF.LC.ADVISING.BK.CUSTNO THEN CUST = R.NEW(TF.LC.ADVISING.BK.CUSTNO)
        IF AF = TF.LC.ADVISE.THRU.CUSTNO THEN CUST = R.NEW(TF.LC.ADVISE.THRU.CUSTNO)
        IF AF = TF.LC.ISSUING.BANK.NO  THEN CUST = R.NEW(TF.LC.ISSUING.BANK.NO)
        IF CUST THEN
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF KEY.LIST THEN
                FOR I = 1 TO SELECTED
                    ENQ<2,I> = "@ID"
                    ENQ<3,I> = "EQ"
                    ENQ<4,I> = KEY.LIST<I>
                NEXT I
            END ELSE
                ENQ.ERROR = "NO ACCOUNTS FOUND"
            END

        END
    END
******************************************************************************************
    IF APPLICATION = 'ACCOUNT.CLOSURE' THEN
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ID.NEW,CUSS)
        CALL DBR('CUSTOMER':@FM:EB.CUS.CUSTOMER.STATUS,CUSS,STAT)
        IF STAT EQ '18' THEN
            T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUSS
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

            IF KEY.LIST THEN
                FOR I = 1 TO SELECTED
                    ENQ<2,I> = "@ID"
                    ENQ<3,I> = "EQ"
                    ENQ<4,I> = KEY.LIST<I>
                NEXT I
            END
        END
    END
*******************************************************************************************
******************************************************************************************
    IF APPLICATION = 'BILL.REGISTER' THEN
        CUSS = R.NEW(EB.BILL.REG.DRAWER)

        T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUSS
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END

    END
*******************************************************************************************
****************NESSREEN AHMED 2/9/2006****************************************************
    IF APPLICATION = 'SCB.VISA.APP' THEN
        CUST = '' ; I = '' ; SELECTED = '' T.SEL = '' ; SELECTED = ''
*CUST = ID.NEW
        CUST = R.NEW(SCB.VISA.MAIN.CUSTOMER)
        T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END

    END
*******************************************************************************
    IF APPLICATION = 'SCB.WH.TRANS' THEN
        CUST = '' ; I = '' ; SELECTED = '' T.SEL = '' ; SELECTED = ''
        CUST = R.NEW(SCB.WH.TRANS.CUSTOMER)
        T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END

    END
********************************************************************************
*******************************************************************************
    IF APPLICATION = 'SCB.SF.TRANS' THEN
        CUST = '' ; I = '' ; SELECTED = '' T.SEL = '' ; SELECTED = ''
        CUST = R.NEW(SCB.SF.TR.CUSTOMER.NO)
        T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END

    END
********************************************************************************
    IF APPLICATION = 'SCB.BR.SLIPS' THEN
        CUST = '' ; I = '' ; SELECTED = '' T.SEL = '' ; SELECTED = ''
        CUST = R.NEW(SCB.BS.CUST.ACCT)
        T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": CUST
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END

    END
*******************************************************************************************

    RETURN
END
