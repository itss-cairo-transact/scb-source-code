* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>348</Rating>
*-----------------------------------------------------------------------------
***    PROGRAM EOD.PL.CCY.CLS.N
    SUBROUTINE EOD.PL.CCY.CLS.N

*   TO CREATE ONTHER FT FOR CURRENCY MARKET

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CATEG.NO.M
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*----------------------------------------------------
*-----------
*    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''

*    CALL OPF(FN.BASE,F.BASE)

*    CALL F.READ(FN.BASE,'EOM',R.BASE,F.BASE,E3)
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    FN.CAT.C = 'FBNK.CATEG.ENTRY' ;F.CAT.C = '' ; R.CAT.C = ''
    CALL OPF(FN.CAT.C,F.CAT.C)
    KEY.LIST.C="" ; SELECTED.C="" ; ER.MSG.C=""

*    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''
*    CALL OPF(FN.BASE,F.BASE)

    IDDD="EG0010001-COB"
    CALL DBR('DATES':@FM:EB.DAT.PERIOD.END,IDDD,P.END)

    START.DAY = P.END[1,6]:"01"
    END.DAY   = P.END
    TOT.AMTT  = 0
    TOT.LOC.AMTT  = 0
    T.SEL.C   = "SELECT F.SCB.CATEG.NO.M WITH (BOOKING.DATE GE ":START.DAY :" AND BOOKING.DATE LE ":END.DAY:" ) AND CURRENCY NE EGP AND CURRENCY NE '' AND COMPANY.BOOK EQ ": ID.COMPANY : " BY CURRENCY  "
    CALL EB.READLIST(T.SEL.C,KEY.LIST.C,"",SELECTED.C,ER.MSG.C)
    COMP  = ID.COMPANY[8,2]
    IF KEY.LIST.C THEN
        FOR I = 1 TO SELECTED.C
            CALL F.READ(FN.CAT.C,KEY.LIST.C<I>,R.CAT.C,F.CAT.C,READ.ERR1C)
            CALL F.READ(FN.CAT.C,KEY.LIST.C<I+1>,R.CAT.C1,F.CAT.C,READ.ERR11C)

            CURR   = R.CAT.C<AC.CAT.CURRENCY>
            CURR1  = R.CAT.C1<AC.CAT.CURRENCY>

            IF CURR EQ CURR1 THEN
                TOT.AMTT += R.CAT.C<AC.CAT.AMOUNT.FCY>
                TOT.LOC.AMTT += R.CAT.C<AC.CAT.AMOUNT.LCY>
            END ELSE
                TOT.AMTT += R.CAT.C<AC.CAT.AMOUNT.FCY>
                TOT.LOC.AMTT += R.CAT.C<AC.CAT.AMOUNT.LCY>

                CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,M.RATE)
                RATE = M.RATE<1,1>
                IF CURR = 'JPY' THEN
                    RATE = RATE / 100
                END
                RATE1 = RATE
                IF TOT.AMTT GT 0 THEN
                    Y.ACCT     = CURR:"1112000010099"
                    CUR.CODE   = CURR
                    FCY.AMTT   = TOT.AMTT
                    LCY.AMTT   = TOT.AMTT * RATE
                    REFF       ="CLOSS-":CUR.CODE
                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
                    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
                    GOSUB AC.STMT.ENTRY

                    TOT.LCY.AMTT = LCY.AMTT

                    LOC.AMTT   = TOT.LOC.AMTT
                    CUR.CODE   = 'EGP'
                    Y.ACCT     = "EGP1112100010099"
                    LCY.AMTT   = LOC.AMTT * -1
                    FCY.AMTT   = ""
                    REFF       ="CLOSS-":CUR.CODE
                    RATE = ''
*                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
                    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
                    GOSUB AC.STMT.ENTRY

                END

*********************************************
                IF TOT.AMTT LT 0 THEN
                    Y.ACCT     = CURR:"1112000010099"
                    CUR.CODE   = CURR
                    FCY.AMTT   = TOT.AMTT
                    LCY.AMTT   = TOT.AMTT * RATE
                    REFF       ="CLOSS-":CUR.CODE
                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
                    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
                    GOSUB AC.STMT.ENTRY

                    TOT.LCY.AMTT = LCY.AMTT

                    CUR.CODE   = 'EGP'
                    Y.ACCT     = "EGP1112100010099"
                    LOC.AMT    = TOT.LOC.AMTT
                    LCY.AMTT   = LOC.AMT * -1
                    FCY.AMTT   = ""
                    REFF       = "CLOSS-":CUR.CODE
                    RATE = ''
*                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
                    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
                    GOSUB AC.STMT.ENTRY

***************************************************
                END
******** TO CHECK IF THERE DIFF IN CURRENCY TOTAL **********************
                TOT.DIF.CUR = 0 ; TOT.DIF.CUR.EGP = 0
                TOT.DIF.EGP = TOT.LOC.AMTT * -1
                TOT.DIF.CUR = ( TOT.DIF.EGP + TOT.LCY.AMTT ) / RATE1
                TOT.DIF.CUR.EGP = TOT.DIF.CUR * RATE1
                IF CURR = "JPY" THEN
                    TOT.DIF.CUR = DROUND(TOT.DIF.CUR,0)
                END ELSE
                    CALL EB.ROUND.AMOUNT ('EGP',TOT.DIF.CUR,'',"2")
                END
                CALL EB.ROUND.AMOUNT ('EGP',TOT.DIF.CUR.EGP,'',"2")
                IF TOT.DIF.CUR GT 0 OR TOT.DIF.CUR LT 0 THEN
                    TOT.DIF.CUR = TOT.DIF.CUR * -1
                    TOT.DIF.CUR.EGP = TOT.DIF.CUR.EGP * -1
                    GOSUB CA.STMT.ENTRY
                    RATE1 = ''
                END
******** TO CHECK IF THERE DIFF IN CURRENCY TOTAL **********************
                TOT.AMTT = "0"
                TOT.LOC.AMTT = "0"
                TOT.LCY.AMTT = "0"
            END
        NEXT I

    END

    RETURN

*-------------------------------------------------------------------
AC.STMT.ENTRY:
**********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

******************************************
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '777'
    ENTRY<AC.STE.THEIR.REFERENCE>  = "POS-CLS-":CUR.CODE
    ENTRY<AC.STE.TRANS.REFERENCE>  = "POS-CLS-":CUR.CODE
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMTT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CUR.CODE
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMTT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = "POS-CLS-":P.END[1,6]

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN
*-------------------------------------------------------------------
CA.STMT.ENTRY:
**********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
******************************************
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ""
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = "777"
    ENTRY<AC.STE.THEIR.REFERENCE>  = "POS-CLS-":CUR.CODE
    ENTRY<AC.STE.TRANS.REFERENCE>  = "POS-CLS-":CUR.CODE
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = "53000"
    ENTRY<AC.STE.AMOUNT.LCY>       = TOT.DIF.CUR.EGP
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = ""
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = TOT.DIF.CUR
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATE1
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = "POS-CLS-":P.END[1,6]

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN

END
************************************************************
