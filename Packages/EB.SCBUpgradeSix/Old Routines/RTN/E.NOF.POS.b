* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>450</Rating>
*[WAGDY MOUNIR]
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.POS(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


    LOCATE "FILE.DATE" IN D.FIELDS<1> SETTING F.DATE THEN
        X.DATE  = D.RANGE.AND.VALUE<F.DATE>
    END

 *   DAL.TODAY = TODAY
 *   DATX  = DAL.TODAY[3,6]
    IDD =  "cl":X.DATE

    Path = "../bnk.data/OFS/OFS.BK/":IDD

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 0
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.IDD            = FIELD(Line,"EBC.AUTH.NO=",2)

            ACC.NOO            = FIELD(Line,"DEBIT.ACCT.NO=",2)
            PANN               = FIELD(Line,"PAN=",2)
            ARNN               = FIELD(Line,"ARN=",2)
            AUTH.NOO           = FIELD(Line,"EBC.AUTH.NO=",2)
            DATEE              = FIELD(Line,"DEBIT.VALUE.DATE=",2)
            TIMEE              = FIELD(Line,"LOCAL.DATE.TIME=",2)
            T.AMTT             = FIELD(Line,"TRANSACTION.AMT=",2)
            D.AMTT             = FIELD(Line,"DEBIT.AMOUNT=",2)
            TERM.LOCC          = FIELD(Line,"TERMINAL.LOC=",2)
            MSG.DESCC          = FIELD(Line,"MESSAGE.DESC=",2)
            P.CODE             = FIELD(Line,"PCODE=",2)


            F1  = FIELD(D.AMTT,",",1)
            F2  = FIELD(PANN,",",1)
            F3  = FIELD(ARNN,",",1)
            F4  = FIELD(AUTH.NOO,",",1)
            F5  = FIELD(ACC.NOO,",",1)
            F6  = FIELD(DATEE,",",1)
            F7  = FIELD(TIMEE,",",1)
            F8  = FIELD(T.AMTT,",",1)
            F9  = FIELD(TERM.LOCC,",",1)
            F10 = FIELD(MSG.DESCC,",",1)
            F11 = FIELD(P.CODE,",",1)
            OUR.ID = FIELD(OUR.IDD,",",1)

            IF OUR.ID EQ '' THEN
                Y.RET.DATA<-1> =F1:"*":F2:"*":F3:"*":F4:"*":F5:"*":F6:"*":F7:"*":F8:"*":F9:"*":F10:"*":F11
            END
        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    RETURN
END
