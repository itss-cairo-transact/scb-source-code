* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.STMT.KSHF.ENG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.COLL
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

**    TEXT = O.DATA ; CALL REM
    TRNS  = FIELD(O.DATA,'*',1)
    AMTT  = FIELD(O.DATA,'*',2)
    ST.ID = FIELD(O.DATA,'*',3)

**-----------------------------------------------
    IF ( TRNS EQ '391' OR TRNS EQ '390' ) THEN

        FN.ST = 'FBNK.STMT.ENTRY' ; F.ST = '' ; R.ST = ''
        CALL OPF( FN.ST,F.ST)
        CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT)
        ACC.ID = R.ST<AC.STE.ACCOUNT.NUMBER>

        FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
        CALL OPF( FN.ACC,F.ACC)
        CALL F.READ( FN.ACC,ACC.ID, R.ACC, F.ACC, ETEXT11)
        COMP   = R.ACC<AC.CO.CODE>
        IF COMP EQ 'EG0010011' THEN
            O.DATA = '������ ������'
        END ELSE
            O.DATA = '����� �����'
        END
    END ELSE
**-------------------------------------------------------

        IF TRNS EQ '401' OR TRNS EQ '420' OR TRNS EQ '430' THEN
            FN.ST = 'FBNK.STMT.ENTRY' ; F.ST = '' ; R.ST = ''
            CALL OPF( FN.ST,F.ST)

            FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = '' ; R.LD.H = ''
            CALL OPF( FN.LD.H,F.LD.H)

            CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT)
            CATT = R.ST<AC.STE.CRF.PROD.CAT>

            IF CATT GE 21020 AND CATT LE 21025 THEN
                IF TRNS EQ '401'  THEN
                    DESCC = "NEW DEPOSIT"
                END
                LD.ID = R.ST<AC.STE.TRANS.REFERENCE>:";1"

                IF TRNS EQ '420'  THEN
                    CALL F.READ( FN.LD.H,LD.ID, R.LD.H, F.LD.H, ETEXT1)
                    FIN.DAT.1 = R.LD.H<LD.FIN.MAT.DATE>
                    BOK.DAT   = R.ST<AC.STE.BOOKING.DATE>

                    IF FIN.DAT.1 EQ BOK.DAT  THEN
                        DESCC = "���� ��� �������"
                    END

                    IF FIN.DAT.1 NE BOK.DAT  THEN
** DESCC = "����� �����"
                        DESCC = "LIQUEFACTION CERTIFICATE"
                    END
                END
            END

            IF CATT GE 21001 AND CATT LE 21010 THEN
                IF TRNS EQ '401'  THEN
                    LD.ID = R.ST<AC.STE.TRANS.REFERENCE>:";1"
                    CALL F.READ( FN.LD.H,LD.ID, R.LD.H, F.LD.H, ETEXT1)

                    OLD.NO = R.LD.H<LD.LOCAL.REF,LDLR.OLD.NO>
                    IF OLD.NO EQ '' THEN
                        DESCC = "NEW DEPOSIT"
                    END
                    IF OLD.NO NE '' THEN
                        IF AMTT LT 0 THEN
**  DESCC = "����� �����"
                            DESCC = "RENEW DEPOSIT"
                        END ELSE
**DESCC = "����� �����"
                            DESCC = "LIQUEFACTION DEPOSIT"
                        END
                    END

                END
                IF TRNS EQ '420'  THEN
                    LD.ID = R.ST<AC.STE.TRANS.REFERENCE>:";1"
                    CALL F.READ( FN.LD.H,LD.ID, R.LD.H, F.LD.H, ETEXT1)
                    FIN.DAT.1 = R.LD.H<LD.FIN.MAT.DATE>
                    BOK.DAT   = R.ST<AC.STE.BOOKING.DATE>
                    INP.1 = R.ST<AC.STE.INPUTTER><1,1>
                    INP = FIELD(INP.1,"_" ,2)


                    IF FIN.DAT.1 EQ BOK.DAT OR INP EQ 'INPUTTER'  THEN
** DESCC = "������� �����"
                        DESCC = "MATURITY DEPOSIT"
                    END ELSE
**  DESC ="����� �����"
                        DESCC = "LIQUEFACTION DEPOSIT"
                    END
                END
            END

            IF CATT EQ '21096'  AND TRNS EQ '430' THEN
** DESCC = '����� ���� ����'
                DESCC = 'CHARGE FOR LG'
            END
            O.DATA = DESCC

        END ELSE
** CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,TRNS,DESC.TRNS)
            FN.TRAN = 'FBNK.TRANSACTION' ; F.TRAN = '' ; R.TRAN = ''
            CALL OPF( FN.TRAN,F.TRAN)
            CALL F.READ( FN.TRAN,TRNS, R.TRAN, F.TRAN, ETEXT)

            O.DATA = R.TRAN<AC.TRA.NARRATIVE><1,1>
        END
    END
    RETURN
END
