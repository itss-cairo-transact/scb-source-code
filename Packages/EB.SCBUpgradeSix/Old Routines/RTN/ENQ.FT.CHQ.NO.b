* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.FT.CHQ.NO

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*****
    TR.ID = O.DATA
    TR.ID.H = O.DATA:";1"
    FN.FT  = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''

    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.FT.H,F.FT.H)
    CALL F.READ( FN.FT,TR.ID, R.FT, F.FT, ETEXT)
    IF NOT(ETEXT) THEN
        OR.BNK = R.FT< FT.LOCAL.REF,FTLR.CHEQUE.NO>
        O.DATA = OR.BNK
    END ELSE
        CALL F.READ( FN.FT.H,TR.ID.H, R.FT.H, F.FT.H, ETEXT2)
        OR.BNK = R.FT.H<FT.LOCAL.REF,FTLR.CHEQUE.NO>
        O.DATA = OR.BNK
    END
    TR.ID.ATM = O.DATA[1,12]
    CALL F.READ( FN.FT,TR.ID.ATM, R.FT, F.FT, ETEXT)
**   TEXT = TR.ID.ATM ; CALL REM
    IF NOT(ETEXT) THEN
        IF R.FT<FT.ORDERING.BANK> EQ 'ATM' THEN
            ATM.REF = R.FT< FT.LOCAL.REF,FTLR.RETRIVAL.REF.NO>
            O.DATA = ATM.REF
        END
    END ELSE
        TR.ID.ATM.H =  O.DATA[1,12] : ';1'
***     TEXT = TR.ID.ATM.H ; CALL REM
        CALL F.READ( FN.FT.H,TR.ID.ATM.H, R.FT.H, F.FT.H, ETEXT2)
        IF R.FT.H<FT.ORDERING.BANK> EQ 'ATM' THEN
            ATM.REF = R.FT.H<FT.LOCAL.REF,FTLR.RETRIVAL.REF.NO>
            O.DATA = ATM.REF
        END
    END
    RETURN
END
