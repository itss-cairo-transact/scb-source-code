* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>425</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.CHQ.NO

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------------------
    TR.ID     = O.DATA
    TR.ID.FMT = FIELD(TR.ID, "/", 1)
    TR.ID.H   = TR.ID.FMT:";1"

    FN.FT     = 'FBNK.FUNDS.TRANSFER'     ; F.FT   = '' ; R.FT   = ''
    FN.FT.H   = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''

    FN.TT     = 'FBNK.TELLER'             ; F.TT   = '' ; R.TT   = ''
    FN.TT.H   = 'FBNK.TELLER$HIS'         ; F.TT.H = '' ; R.TT.H = ''

    FN.IN     = 'F.INF.MULTI.TXN'         ; F.IN   = '' ; R.IN   = ''
    FN.IN.H   = 'F.INF.MULTI.TXN$HIS'     ; F.IN.H = '' ; R.IN.H = ''

    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.FT.H,F.FT.H)

    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.TT.H,F.TT.H)

    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.IN.H,F.IN.H)

    BEGIN CASE
    CASE TR.ID[1,2] EQ 'TT'
        CALL F.READ( FN.TT, TR.ID, R.TT, F.TT, TT.ERR)
        IF TT.ERR THEN
            CALL F.READ( FN.TT.H, TR.ID.H, R.TT.H, F.TT.H, TT.ERR.H)
            TR.NO    = R.TT.H<TT.TE.TRANSACTION.CODE>
            CHQ.NO   = R.TT.H<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
            SER.NO   = R.TT.H<TT.TE.LOCAL.REF,TTLR.SER.NO>
            CHQ.NO.S = R.TT.H<TT.TE.CHEQUE.NUMBER>

            IF CHQ.NO.S EQ '' THEN
                O.DATA = SER.NO
            END ELSE
                O.DATA = CHQ.NO.S
            END
        END ELSE
            TR.NO    = R.TT<TT.TE.TRANSACTION.CODE>
            CHQ.NO   = R.TT<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
            SER.NO   = R.TT<TT.TE.LOCAL.REF,TTLR.SER.NO>
            CHQ.NO.S = R.TT<TT.TE.CHEQUE.NUMBER>
            IF CHQ.NO.S EQ '' THEN
                O.DATA = SER.NO
            END ELSE
                O.DATA = CHQ.NO.S
            END
        END

    CASE TR.ID[1,2] EQ 'FT'
        CALL F.READ( FN.FT,TR.ID, R.FT, F.FT, FT.ERR)
        IF NOT(FT.ERR)  THEN
            CHQ.NO.FT = R.FT<FT.LOCAL.REF,FTLR.CHEQUE.NO>
            CHQ.FT.N  = R.FT<FT.CHEQUE.NUMBER>
            IF CHQ.NO.FT NE '' THEN
                O.DATA = CHQ.NO.FT
            END ELSE
                O.DATA = CHQ.FT.N
            END
        END ELSE
            CALL F.READ( FN.FT.H, TR.ID.H, R.FT.H, F.FT.H, FT.ERR.H)
            CHQ.NO.FT = R.FT.H<FT.LOCAL.REF,FTLR.CHEQUE.NO>
            CHQ.FT.N  = R.FT.H<FT.CHEQUE.NUMBER>
            IF CHQ.NO.FT NE '' THEN
                O.DATA = CHQ.NO.FT
            END ELSE
                O.DATA = CHQ.FT.N
            END
        END
    CASE TR.ID[1,2] EQ 'IN'
        CALL F.READ(FN.IN,TR.ID, R.IN, F.IN, IN.ERR)
        IF NOT(IN.ERR) THEN
*Line [ 118 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CHQ.COUNT = DCOUNT(R.IN<INF.MLT.ACCOUNT.NUMBER>,@VM)
            FOR II = 1 TO CHQ.COUNT
                CHEQ.IN.NO = R.IN<INF.MLT.CHEQUE.NUMBER><1,II>
                IF CHEQ.IN.NO THEN
                    II     = CHQ.COUNT + 1
                    O.DATA = CHEQ.IN.NO
                END
            NEXT II
        END ELSE
            CALL F.READ(FN.IN.H, TR.ID.H, R.IN.H, F.IN.H, IN.ERR.H)
*Line [ 129 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CHQ.COUNT  = DCOUNT(R.IN.H<INF.MLT.ACCOUNT.NUMBER>,@VM)
            FOR II = 1 TO CHQ.COUNT
                CHEQ.IN.NO = R.IN.H<INF.MLT.CHEQUE.NUMBER><1,II>
                IF CHEQ.IN.NO THEN
                    II     = CHQ.COUNT + 1
                    O.DATA = CHEQ.IN.NO
                END
            NEXT II
        END
    END CASE

    RETURN
END
