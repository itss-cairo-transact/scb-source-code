* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*** ���� �������� ������� ������  ***
*** ���� ��� ������ - ������� - ������� - ������� - ���� ��� ������ ***
*** �������� ������� *** *** ���� ������� ���� ������ ***
*** CREATED BY KHALED ***
***=================================
    SUBROUTINE E.SBR.LG.004.NEW(Y.RET.DATA)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB RETURN.DATA
*------------------------------------------------------------------------
    RETURN
*========================================================================
INITIATE:

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CAT = 'FBNK.CATEG.ENTRY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

*************GET FROM ENQUIRY

    LOCATE "CUSTOMER.ID" IN D.FIELDS<1> SETTING ID.X THEN
        ID    = D.RANGE.AND.VALUE<ID.X>
    END
    LOCATE "CURRENCY"    IN D.FIELDS<1> SETTING CU.X THEN
        CUR    = D.RANGE.AND.VALUE<CU.X>
    END
    LOCATE "BOOK.DATE"   IN D.FIELDS<1> SETTING D.X THEN
        BOOK.DATE  = D.RANGE.AND.VALUE<D.X>
    END
    DATF = BOOK.DATE[1,8]
    DATT = BOOK.DATE[10,8]
*****************************
    AMT1.TOT1 = 0 ; AMT1.TOT2 = 0 ; AMT1.TOT3 = 0
    AMT11.TOT1 = 0 ; AMT11.TOT2 = 0 ; AMT11.TOT3 = 0
    AMT2.TOT1 = 0 ; AMT2.TOT2 = 0 ; AMT2.TOT3 = 0
    AMT3.TOT1 = 0 ; AMT3.TOT2 = 0 ; AMT3.TOT3 = 0
    AMT4.TOT1 = 0 ; AMT4.TOT2 = 0 ; AMT4.TOT3 = 0
    AMT5.TOT1 = 0 ; AMT5.TOT2 = 0 ; AMT5.TOT3 = 0
    COM.AMT = 0
    COMP = ID.COMPANY
    RETURN
*========================================================================
PROCESS:
*** ���� ��� ������ ***
*-----------------------
    T.SEL = "SELECT ":FN.LD: " WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND ISSUE.DATE LT ":DATF:" AND CURRENCY EQ ":CUR
    T.SEL := " BY LIMIT.REFERENCE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,E1)

            REF1 = R.LD<LD.LIMIT.REFERENCE>
            REF = REF1[1,4]

            IF REF EQ '2505' THEN
                AMT11.TOT1 += R.LD<LD.AMOUNT>
            END ELSE
                IF REF EQ '2510' THEN
                    AMT11.TOT2 += R.LD<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2520' THEN
                        AMT11.TOT3 += R.LD<LD.AMOUNT>
                    END
                END
            END
        NEXT Z
    END
*--------------------------------------------------
*** ������� ***
*--------------
    T.SEL1 = "SELECT ":FN.LD: " WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND ISSUE.DATE GE ":DATF:" AND ISSUE.DATE LE ":DATT:" AND CURRENCY EQ ":CUR:" AND OPERATION.CODE IN (1111 1271) BY AMOUNT"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.LD,KEY.LIST1<I>,R.LD,F.LD,E2)
            REF1 = R.LD<LD.LIMIT.REFERENCE>
            REF = REF1[1,4]

            IF REF EQ '2505' THEN
                AMT2.TOT1 += R.LD<LD.AMOUNT>
            END ELSE
                IF REF EQ '2510' THEN
                    AMT2.TOT2 += R.LD<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2520' THEN
                        AMT2.TOT3 += R.LD<LD.AMOUNT>
                    END
                END
            END
        NEXT I
        DATS = R.LD<LD.LOCAL.REF><1,LDLR.ISSUE.DATE>
        ISU.DAY  = FMT(DATS,"####/##/##")

    END

*--------------------------------------------------
*** ������� ***
*---------------
    T.SEL3 =  "SELECT ":FN.LD.H:" WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND FIN.MAT.DATE GE ":DATF:" AND FIN.MAT.DATE LE ":DATT:" AND CURRENCY EQ ":CUR:" AND STATUS EQ 'LIQ' AND OPERATION.CODE IN (1401 1402 1403 1404 1405 1406 1407 1408)"
    T.SEL3 := " BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    IF SELECTED3 THEN
        FOR K = 1 TO SELECTED3
            CALL F.READ(FN.LD.H,KEY.LIST3<K>,R.LD.H,F.LD.H,E4)
            CALL F.READ(FN.LD.H,KEY.LIST3<K+1>,R.LD.H1,F.LD.H,E2)
            IF FIELD(KEY.LIST3<K>,';',1) NE FIELD(KEY.LIST3<K+1>,';',1) THEN
                REF1 = R.LD.H<LD.LIMIT.REFERENCE>
                REF = REF1[1,4]

                IF REF EQ '2505' THEN
                    AMT4.TOT1 += R.LD.H<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2510' THEN
                        AMT4.TOT2 += R.LD.H<LD.AMOUNT>
                    END ELSE
                        IF REF EQ '2520' THEN
                            AMT4.TOT3 += R.LD.H<LD.AMOUNT>
                        END
                    END
                END
            END
        NEXT K
    END
*--------------------------------------------------
*** ���������� ***
*-----------------
    T.SEL4 = "SELECT ":FN.LD.H: " WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND FIN.MAT.DATE GE ":DATF:" AND FIN.MAT.DATE LE ":DATT:" AND CURRENCY EQ ":CUR:" AND STATUS EQ 'LIQ' AND OPERATION.CODE IN (1233 1301 1302 1303 1304 1305 1306 1307 1308)"
    T.SEL4 := "BY @ID"
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
    IF SELECTED4 THEN
        FOR D = 1 TO SELECTED4
            CALL F.READ(FN.LD.H,KEY.LIST4<D>,R.LD.H,F.LD.H,E3)
            CALL F.READ(FN.LD.H,KEY.LIST4<D+1>,R.LD.H1,F.LD.H,E2)
            IF FIELD(KEY.LIST4<D>,';',1) NE FIELD(KEY.LIST4<D+1>,';',1) THEN
                REF1 = R.LD.H<LD.LIMIT.REFERENCE>
                REF = REF1[1,4]

                IF REF EQ '2505' THEN
                    AMT5.TOT1 += R.LD.H<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2510' THEN
                        AMT5.TOT2 += R.LD.H<LD.AMOUNT>
                    END ELSE
                        IF REF EQ '2520' THEN
                            AMT5.TOT3 += R.LD.H<LD.AMOUNT>
                        END
                    END
                END
            END
        NEXT D
    END
*-------------------------------------------------
***  ��� ������ + ������� + �������  ***
*-------------------------------------
    AMT1.TOT1 = AMT11.TOT1 + AMT5.TOT1 + AMT4.TOT1
    AMT1.TOT2 = AMT11.TOT2 + AMT5.TOT2 + AMT4.TOT2
    AMT1.TOT3 = AMT11.TOT3 + AMT5.TOT3 + AMT4.TOT3

*-------------------------------------------------
*** ���� ��� ������ ***
*----------------------
    AMT3.TOT1 = (AMT1.TOT1 + AMT2.TOT1) - (AMT4.TOT1 + AMT5.TOT1)
    AMT3.TOT2 = (AMT1.TOT2 + AMT2.TOT2) - (AMT4.TOT2 + AMT5.TOT2)
    AMT3.TOT3 = (AMT1.TOT3 + AMT2.TOT3) - (AMT4.TOT3 + AMT5.TOT3)


*-------------------------------------------------
*** �������� ������� ***
*----------------------
    T.SEL2 = "SELECT ":FN.CAT: " WITH CUSTOMER.ID EQ ":ID:" AND PRODUCT.CATEGORY EQ 21096 AND BOOKING.DATE GE ":DATF:" AND BOOKING.DATE LE ":DATT:" AND CURRENCY EQ ":CUR
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR C = 1 TO SELECTED2
            CALL F.READ(FN.CAT,KEY.LIST2<C>,R.CAT,F.CAT,E6)
            IF CUR EQ 'EGP' THEN
                COM.AMT += R.CAT<AC.CAT.AMOUNT.LCY>
            END ELSE
                IF CUR NE 'EGP' THEN
                    COM.AMT += R.CAT<AC.CAT.AMOUNT.FCY>
                END
            END
        NEXT C
    END
*-------------------------------------------------
*** ���� ������� ***
*---------------
    FIRST.AMT = AMT1.TOT1 + AMT1.TOT2 + AMT1.TOT3
    ISU.AMT   = AMT2.TOT1 + AMT2.TOT2 + AMT2.TOT3
    LAST.AMT  = AMT3.TOT1 + AMT3.TOT2 + AMT3.TOT3
    H.AMT     = FIRST.AMT + ISU.AMT
    IF H.AMT GE LAST.AMT THEN
        X.AMT = H.AMT
    END ELSE
        X.AMT = LAST.AMT
    END
*-------------------------------------------------
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY = FMT(DATY,"####/##/##")
    DATF1 = FMT(DATF,"####/##/##")
    DATT1 = FMT(DATT,"####/##/##")
    CUST.ID = ID
    CUST.ID = TRIM(CUST.ID, "0", "L")
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR,CUR.DESC)

    RETURN
*==============================================================
RETURN.DATA:

    Y.RET.DATA<-1> = CUST.ID:'*':CUR:'*':BOOK.DATE:'*':AMT1.TOT1:'*':AMT1.TOT2:'*':AMT1.TOT3:'*':AMT2.TOT1:'*':AMT2.TOT2:'*':AMT2.TOT3:'*':AMT4.TOT1:'*':AMT4.TOT2:'*':AMT4.TOT3:'*':AMT5.TOT1:'*':AMT5.TOT2:'*':AMT5.TOT3:'*':AMT3.TOT1:'*':AMT3.TOT2:'*':AMT3.TOT3:'*':COM.AMT:'*':X.AMT:'*':DATT1:'*':DATF1:'*':YYBRN:'*':CUST.NAME:'*':CUR.DESC


    RETURN
END
