* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
** ----- NESSREEN AHMED 07/02/2011 -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.TELLER.D.W(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TELLER.D.W
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.TT.D.W = 'F.SCB.TELLER.D.W' ; F.TT.D.W = '' ; R.TT.D.W = ''
    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC = ''   ; R.ACC = ''
    FN.CUR  = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''


    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF(FN.TT.D.W,F.TT.D.W)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.CUR,F.CUR)

    M.DATEE = TODAY
    TT.SEL = "SELECT F.SCB.TELLER.D.W WITH AUTH.DATE EQ " : M.DATEE : " BY CARD.CURRENCY BY COMP.CO"
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM

    AMT.LCY.D = '' ; AMT.LCY.W = ''
    IF SELECTED.TT THEN
        KEY.TO.USE = KEY.LIST.TT<1>
        CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
        CURR<1>       =  R.TT.D.W<TELL.D.W.CARD.CURRENCY>
        COMP.CODE<1>  =  R.TT.D.W<TELL.D.W.COMP.CO>
        AMT.LCY<1>    =  R.TT.D.W<TELL.D.W.AMT.LCY>
        V.ACC<1>      =  R.TT.D.W<TELL.D.W.VAULT.AC.NO>
        DW.FLAG<1>    =  R.TT.D.W<TELL.D.W.TR.FLAG>
        BEGIN CASE
        CASE DW.FLAG<1> = 'D'
            AMT.LCY.D = AMT.LCY.D + AMT.LCY<1>
        CASE DW.FLAG<1> = 'W'
            AMT.LCY.W = AMT.LCY.W + AMT.LCY<1>
        END CASE
        FOR I = 2 TO 5
            KEY.TO.USE = KEY.LIST.TT<I>
            CALL F.READ( FN.TT.D.W,KEY.TO.USE, R.TT.D.W, F.TT.D.W, ERR1)
            CURR<I>       =  R.TT.D.W<TELL.D.W.CARD.CURRENCY>
            COMP.CODE<I>  =  R.TT.D.W<TELL.D.W.COMP.CO>
            V.ACC<I>      =  R.TT.D.W<TELL.D.W.VAULT.AC.NO>
            AMT.LCY<I>    =  R.TT.D.W<TELL.D.W.AMT.LCY>
            DW.FLAG<I>    =  R.TT.D.W<TELL.D.W.TR.FLAG>

            IF CURR<I> # CURR<I-1> THEN
                C.SEL = "SELECT FBNK.ACCOUNT WITH (CATEGORY EQ '10001' OR CATEGORY EQ '10000') AND VERSION.NAME NE CLOSED AND CURRENCY EQ " : CURR<I-1> " BY CO.CODE BY CATEGORY "
                CALL EB.READLIST(C.SEL, KEY.LIST, "", SELECTED, ASD)
                FOR CC = 1 TO SELECTED
                    ACCT.ID = ''
                    ACCT.ID = KEY.LIST<CC>
                    CALL F.READ(FN.ACC, ACCT.ID ,R.ACC,ERR.ACC)
                    OPEN.CURR = R.ACC<AC.OPEN.ACTUAL.BAL>
                    CLOS.CURR = R.ACC<AC.ONLINE.ACTUAL.BAL>
                    OPEN.TOT = OPEN.TOT + OPEN.CURR
                    CLOS.TOT = CLOS.TOT + CLOS.CURR
                NEXT CC
                IF CURR<I-1> NE 'EGP' THEN
                    KEY.CUR = ''
                    KEY.CUR = CURR<I-1>
                    CALL F.READ(FN.CUR,KEY.CUR,R.CUR,F.CUR,ECAA)
                    CASH.C = '10'
*Line [ 104 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
                    CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
                    RATE.C      = CUR.RATE
                    D.RATE      = RATE.C

                    OPEN.C.EQV = OPEN.TOT * RATE.C
                    CLOS.C.EQV = CLOS.TOT * RATE.C
                END ELSE
                    RATE.C    = '1'
                    D.RATE    = ''
                    OPEN.C.EQV = OPEN.TOT
                    CLOS.C.EQV = CLOS.TOT
                END

                Y.RET.DATA<-1> = CURR<I-1>:"*":OPEN.C.EQV:"*":AMT.LCY.D:"*":AMT.LCY.W :"*":CLOS.C.EQV:"*": D.RATE
                OPEN.C.EQV = 0 ; AMT.LCY.D = 0 ; AMT.LCY.W = 0 ; D.RATE = '' ; CLOS.C.EQV = 0  ; CLOS.TOT = 0
                OPEN.TOT = 0 ; CLOS.TOT = 0

                BEGIN CASE
                CASE DW.FLAG<I> = 'D'
                    AMT.LCY.D = AMT.LCY.D + AMT.LCY<I>
                CASE DW.FLAG<I> = 'W'
                    AMT.LCY.W = AMT.LCY.W + AMT.LCY<I>
                END CASE

            END ELSE          ;* IF CURR NOTCHANGED
                BEGIN CASE
                CASE DW.FLAG<I> = 'D'
                    AMT.LCY.D = AMT.LCY.D + AMT.LCY<I>
                CASE DW.FLAG<I> = 'W'
                    AMT.LCY.W = AMT.LCY.W + AMT.LCY<I>
                END CASE
            END     ;* END OF CURR NOT CHANGED
            C.SEL = '' ; KEY.LIST = '' ; SELECTED = '' ; ASD = ''
            IF I = SELECTED.TT THEN
                C.SEL = "SELECT FBNK.ACCOUNT WITH (CATEGORY EQ '10001' OR CATEGORY EQ '10000' ) AND VERSION.NAME NE CLOSED AND CURRENCY EQ " : CURR<I> " BY CO.CODE BY CATEGORY "
                CALL EB.READLIST(C.SEL, KEY.LIST, "", SELECTED, ASD)
                ACCT.ID = ''
                ACCT.ID = KEY.LIST<CC>
                FOR CC = 1 TO SELECTED
                    CALL F.READ(FN.ACC, ACCT.ID,R.ACC,ERR.ACC)
                    OPEN.CURR = R.ACC<AC.OPEN.ACTUAL.BAL>
                    CLOS.CURR = R.ACC<AC.ONLINE.ACTUAL.BAL>
                    OPEN.TOT = OPEN.TOT + OPEN.CURR
                    CLOS.TOT = CLOS.TOT + CLOS.CURR
                NEXT CC
                IF CURR<I> NE 'EGP' THEN
                    KEY.CUR = ''
                    KEY.CUR = CURR<I>
                    CALL F.READ(FN.CUR,KEY.CUR,R.CUR,F.CUR,ECAA)
                    CASH.C = '10'
*Line [ 156 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
                    CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
                    RATE.C      = CUR.RATE
                    D.RATE      = RATE.C

                    OPEN.C.EQV = OPEN.TOT * RATE.C
                    CLOS.C.EQV = CLOS.TOT * RATE.C
                END ELSE
                    RATE.C    = '1'
                    D.RATE    = ''
                    OPEN.C.EQV = OPEN.TOT
                    CLOS.C.EQV = CLOS.TOT
                END

                Y.RET.DATA<-1> = CURR<I>:"*":OPEN.C.EQV:"*":AMT.LCY.D:"*":AMT.LCY.W :"*":CLOS.C.EQV:"*": D.RATE
            END
        NEXT I
    END
    RETURN
END
