* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>559</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOD.ACC.9090

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

***************************
    IDDD   ="EG0010001-COB"
    IDDD.N ="EG0010001"
    CALL DBR('DATES':@FM:EB.DAT.PERIOD.END,IDDD,P.END)
    CALL DBR('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,IDDD.N,N.W.DAY)
    START.D = N.W.DAY[1,6]:"01"
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.ST = 'FBNK.STMT.ACCT.DR' ; F.ST = ''
    CALL OPF(FN.ST,F.ST)
*    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...9091...-":P.END:" AND INT.NO.BOOKING EQ 'Y'"
    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...-":P.END
    T.SEL := " AND INT.NO.BOOKING EQ 'Y' AND LIQUIDITY.ACCOUNT LIKE ...9091..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.ST,KEY.LIST<I>,R.ST,F.ST,E1)

            AC.9091 = R.ST<IC.STMDR.LIQUIDITY.ACCOUNT>

            CALL F.READ(FN.ACC,AC.9091,R.ACC,F.ACC,READ.ERRACC)
            IF R.ACC<AC.CATEGORY> EQ 9091 THEN
                COMP.CO = R.ACC<AC.CO.CODE>
                COMP    = R.ACC<AC.CO.CODE>[8,2]

                IF R.ST<IC.STMDR.GRAND.TOTAL> NE '' THEN
                    IF COMP.CO EQ ID.COMPANY  THEN
                        DR.ACCT = AC.9091
                        AMT     = R.ST<IC.STMDR.GRAND.TOTAL>
                        CURR    = R.ST<IC.STMDR.LIQUIDITY.CCY>
                        DUMY    = "19091000100":COMP
                        CR.ACCT = CURR:DUMY
                        DATEE   = START.D
                        CR.ACCT = CURR:DUMY
                        DATEE   = START.D


                        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,KEY.LIST<I>[1,16],ACC.OFFICER)

***************************

                        Y.ACCT = CR.ACCT
                        Y.AMT  = AMT
                        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
                        GOSUB AC.STMT.ENTRY

                        Y.ACCT = DR.ACCT
                        Y.AMT  = Y.AMT * -1
                        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
                        GOSUB AC.STMT.ENTRY
                    END
                END
            END
        NEXT I
    END
    RETURN

AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '391'
    ENTRY<AC.STE.THEIR.REFERENCE>  = KEY.LIST<I>
    ENTRY<AC.STE.TRANS.REFERENCE>  = KEY.LIST<I>
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = DATEE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = Y.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "IC4"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = KEY.LIST<I>

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("IC4",TYPE,MULTI.ENTRIES,"")

    RETURN
END
