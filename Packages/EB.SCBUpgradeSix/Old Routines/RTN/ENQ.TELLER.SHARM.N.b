* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.TELLER.SHARM.N(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.TT ='FBNK.TELLER$HIS' ;R.TT = '';F.TT =''
    CALL OPF(FN.TT,F.TT)

    COMP = C$ID.COMPANY
    LOCATE "TELLER.ID.1" IN ENQ<2,1> SETTING TT.POS THEN
        TT.ID = ENQ<4,TT.POS>
    END
    LOCATE "AUTH.DATE" IN ENQ<2,1> SETTING DD.POS THEN
        DD.ID = ENQ<4,DD.POS>
    END

    T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN ( 23 67 58 24 68 ) AND CO.CODE EQ ":COMP :" AND TELLER.ID.1 EQ ":TT.ID :" AND AUTH.DATE GE ":DD.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        CALL F.READ( FN.TT,KEY.LIST<I>, R.TT, F.TT,ETEXT)
        D.T    = R.TT<TT.TE.DATE.TIME>[1,6]
        AUTH.D = R.TT<TT.TE.AUTH.DATE>[3,6]

        IF D.T NE AUTH.D THEN
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        END ELSE
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = "DUM"
        END

    NEXT I
    RETURN
END
