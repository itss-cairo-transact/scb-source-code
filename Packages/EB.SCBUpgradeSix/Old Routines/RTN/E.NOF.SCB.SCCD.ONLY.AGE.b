* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***************************MAHMOUD 30/9/2014******************************
*-----------------------------------------------------------------------------
* <Rating>3836</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.SCB.SCCD.ONLY.AGE(Y.RET.DATA)
****    PROGRAM E.NOF.SCB.SCCD.ONLY.AGE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 43 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    KK = 0
    K2 = 0
***********************
    DYNO = 0
    TD1 = TODAY
    TDD = TODAY
    BIRTH15 = TODAY
    BIRTH21 = TODAY
    BIRTH40 = TODAY
    BIRTH60 = TODAY
    BIRTH120 = TODAY
    CALL ADD.MONTHS(BIRTH15,'-180')
    CALL ADD.MONTHS(BIRTH21,'-252')
    CALL ADD.MONTHS(BIRTH40,'-480')
    CALL ADD.MONTHS(BIRTH60,'-720')
    CALL ADD.MONTHS(BIRTH120,'-1440')
    CRT BIRTH15:" --- ":BIRTH21:" --- ":BIRTH40:" --- ":BIRTH60:" --- ":BIRTH120
***********************
    ARR.REC  = ''
    ARR.DATA = ''
    REC.GEO  = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

    GRAND.CAIRO.GRP = " EG0010001 EG0010002 EG0010003 EG0010004 EG0010005 EG0010006 EG0010007 EG0010009 EG0010010 EG0010011 EG0010012 EG0010013 EG0010014 EG0010015 EG0010016 "
    LOWER.EGYPT.GRP = " EG0010020 EG0010021 EG0010022 EG0010023 EG0010031 EG0010032 EG0010060 EG0010070 EG0010090 "
    UPPER.EGYPT.GRP = " EG0010080 EG0010081 "
    SUEZ.CANAL.GRP  = " EG0010030 EG0010040 EG0010050 EG0010051 "
    OTHER.GRP       = " EG0010035 "
    RETURN
******************************************************
CLEAR.VAR:
*----------

    RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)
    RETURN
*========================================
PROCESS:
*-------
***************************//LD//*****************************************
    SEL.CMD  = "SELECT ":FN.CU:" WITH SCCD.CUSTOMER EQ YES AND ( BANK EQ '0017' OR BANK EQ '')"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.CU.LIST)
    CRT SELECTED
    LOOP
        REMOVE CU.ID FROM KEY.LIST SETTING POSS.CU
    WHILE CU.ID:POSS.CU
        CALL F.READ(FN.CU,CU.ID,R.CU,F.CU,ER.CU1)
        REC.BRD = R.CU<EB.CUS.BIRTH.INCORP.DATE>
        CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
*Line [ 113 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        IF REC.SEC NE '4650' THEN GOTO NXT.CUST ELSE NULL
        IF REC.BRD LE TDD AND REC.BRD GT BIRTH15 THEN
            REC.BIRTH = 1
            ARR.REC<REC.BIRTH,REC.GEO,6> = REC.BIRTH
            REC.FLG = 1
            ARR.REC<REC.BIRTH,REC.GEO,3> += 1
        END
        IF REC.BRD LE BIRTH15 AND REC.BRD GT BIRTH21 THEN
            REC.BIRTH = 1
            ARR.REC<REC.BIRTH,REC.GEO,6> = REC.BIRTH
            REC.FLG = 2
            ARR.REC<REC.BIRTH,REC.GEO,5> += 1
        END
        IF REC.BRD LE BIRTH21 AND REC.BRD GT BIRTH40 THEN
            REC.BIRTH = 2
            ARR.REC<REC.BIRTH,REC.GEO,6> = REC.BIRTH
            REC.FLG = 1
            ARR.REC<REC.BIRTH,REC.GEO,3> += 1
        END
        IF REC.BRD LE BIRTH40 AND REC.BRD GT BIRTH60 THEN
            REC.BIRTH = 2
            ARR.REC<REC.BIRTH,REC.GEO,6> = REC.BIRTH
            REC.FLG = 2
            ARR.REC<REC.BIRTH,REC.GEO,5> += 1
        END
        IF REC.BRD LE BIRTH60 THEN
            REC.BIRTH = 3
            ARR.REC<REC.BIRTH,REC.GEO,6> = REC.BIRTH
            REC.FLG = 1
            ARR.REC<REC.BIRTH,REC.GEO,3> += 1
        END
        KK++
        CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
        LOOP
            REMOVE LD.ID FROM R.IND.LD SETTING POSS2
        WHILE LD.ID:POSS2
            K2++
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
            REC.CAT  = R.LD<LD.CATEGORY>
*Line [ 153 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR REC.CAT:" " IN SCCD.GRP SETTING POS.SCCD THEN '' ELSE GOTO NXT.REC
            REC.COM  = R.LD<LD.CO.CODE>
*Line [ 156 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR REC.COM IN GRAND.CAIRO.GRP SETTING POS.GEO1 THEN REC.GEO = 1 ELSE NULL
*Line [ 158 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR REC.COM IN LOWER.EGYPT.GRP SETTING POS.GEO2 THEN REC.GEO = 2 ELSE NULL
*Line [ 160 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR REC.COM IN UPPER.EGYPT.GRP SETTING POS.GEO3 THEN REC.GEO = 3 ELSE NULL
*Line [ 162 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR REC.COM IN SUEZ.CANAL.GRP  SETTING POS.GEO4 THEN REC.GEO = 4 ELSE NULL
*Line [ 164 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR REC.COM IN OTHER.GRP       SETTING POS.GEO5 THEN REC.GEO = 5 ELSE NULL
            REC.CUS  = R.LD<LD.CUSTOMER.ID>
            REC.BNK = CU.LCL<1,CULR.CU.BANK>
            REC.CUR  = R.LD<LD.CURRENCY>
            REC.AMT  = R.LD<LD.AMOUNT>
            REC.VAL  = R.LD<LD.VALUE.DATE>
            REC.LCL  = R.LD<LD.LOCAL.REF>
            REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
*Line [ 173 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            IF REC.QTY EQ '' THEN REC.QTY = 1 ELSE NULL
            REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
*Line [ 176 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
            IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
                REC.TYP = "EGP-1000-3M-60M-SUEZ"
            END
*===========================================
            GOSUB FILL.ARR
*===========================================
NXT.REC:
        REPEAT
NXT.CUST:
    REPEAT
***    ARR.REC = SORT(ARR.REC)
    FOR XXT1 = 1 TO 3
***    MXX1 = DCOUNT(ARR.REC,1,@VM)
    FOR AAA1 = 1 TO 5
        REC.BIRTH = XXT1
        REC.GEO = AAA1
        GOSUB RET.REC
    NEXT AAA1
    NEXT XXT1
    RETURN
*****************************************************************
FILL.ARR:
*---------
    ARR.REC<REC.BIRTH,REC.GEO,1> = REC.GEO
    IF REC.FLG = 1 THEN
        ARR.REC<REC.BIRTH,REC.GEO,2> += REC.AMT
    END ELSE
        ARR.REC<REC.BIRTH,REC.GEO,4> += REC.AMT
    END
    RETURN
*****************************************************************
RET.REC:
********
*Line [ 211 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,6> = 1 THEN REP.HEAD = '��� �� 15 ���.�� 15 ��� 21 ���' ELSE NULL
*Line [ 213 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,6> = 2 THEN REP.HEAD = '���� �� 21 ��� 40 ���.���� �� 40 ��� 60 ���' ELSE NULL
*Line [ 215 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,6> = 3 THEN REP.HEAD = '���� �� 60���.' ELSE NULL

*Line [ 218 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,1> = 1 THEN GEO.NAME = '������� ������' ELSE NULL
*Line [ 220 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,1> = 2 THEN GEO.NAME = '��� ����' ELSE NULL
*Line [ 222 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,1> = 3 THEN GEO.NAME = '��� ����' ELSE NULL
*Line [ 224 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,1> = 4 THEN GEO.NAME = '��� ������' ELSE NULL
*Line [ 226 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.BIRTH,REC.GEO,1> = 5 THEN GEO.NAME = '����' ELSE NULL

    Y.RET.DATA<-1> = ARR.REC<REC.BIRTH,REC.GEO,6>:"*":ARR.REC<REC.BIRTH,REC.GEO,1>:"*":GEO.NAME:"*":ARR.REC<REC.BIRTH,REC.GEO,2>:"*":ARR.REC<REC.BIRTH,REC.GEO,3>:"*":ARR.REC<REC.BIRTH,REC.GEO,4>:"*":ARR.REC<REC.BIRTH,REC.GEO,5>:"*":REP.HEAD
    RETURN
*************************************************
END
