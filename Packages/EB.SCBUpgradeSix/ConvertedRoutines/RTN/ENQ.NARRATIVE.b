* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
******NESSREEN AHMED 4/10/2011***********************
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.NARRATIVE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATA.CAPTURE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

*****
    TR.ID = O.DATA
    TR.ID.FMT = FIELD(TR.ID, "/", 1)
    TR.ID.H = TR.ID.FMT:";1"

    FN.FT  = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''
    FN.TT  = 'FBNK.TELLER' ; F.TT = '' ; R.TT = ''
    FN.TT.H = 'FBNK.TELLER$HIS' ; F.TT.H = '' ; R.TT.H = ''
    FN.DC  = 'FBNK.DATA.CAPTURE' ; F.DC = '' ; R.DC = ''
    FN.DC.H = 'FBNK.DATA.CAPTURE$HIS' ; F.DC.H = '' ; R.DC.H = ''
    FN.INF  = 'F.INF.MULTI.TXN' ; F.INF = '' ; R.INF = ''
    FN.INF.H = 'F.INF.MULTI.TXN$HIS' ; F.INF.H = '' ; R.INF.H = ''

    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.FT.H,F.FT.H)
    CALL OPF( FN.TT,F.TT)
    CALL OPF( FN.TT.H,F.TT.H)
    CALL OPF( FN.DC,F.DC)
    CALL OPF( FN.DC.H,F.DC.H)
    CALL OPF( FN.INF,F.INF)
    CALL OPF( FN.INF.H,F.INF.H)

    NARR = '' ; NOT.DB.FT = ''
  **  TEXT = 'TR.ID=':TR.ID.FMT ; CALL REM

    BEGIN CASE
    CASE TR.ID[1,2] EQ 'TT'
        CALL F.READ( FN.TT, TR.ID.FMT, R.TT, F.TT, TT.ERR)
        IF TT.ERR THEN
            CALL F.READ( FN.TT.H, TR.ID.H, R.TT.H, F.TT.H, TT.ERR.H)
            TR.NO  = R.TT.H<TT.TE.TRANSACTION.CODE>
            NARR   =   R.TT.H<TT.TE.NARRATIVE.2>
            O.DATA = NARR
        END ELSE
            TR.NO = R.TT<TT.TE.TRANSACTION.CODE>
            NARR  = R.TT<TT.TE.NARRATIVE.2>
            O.DATA = NARR
        END
    CASE TR.ID[1,2] EQ 'FT'
        CALL F.READ( FN.FT,TR.ID.FMT, R.FT, F.FT, FT.ERR)
        IF NOT(FT.ERR)  THEN
            NOT.DB.FT = R.FT<FT.LOCAL.REF,FTLR.NOTE.DEBITED>
            O.DATA = NOT.DB.FT
        END ELSE
            CALL F.READ( FN.FT.H,TR.ID.H, R.FT.H, F.FT.H, FT.ERR.H)
            NOT.DB.FT = R.FT.H<FT.LOCAL.REF,FTLR.NOTE.DEBITED>
            O.DATA = NOT.DB.FT
        END
    CASE TR.ID[1,2] EQ 'IN'
        CALL F.READ( FN.INF,TR.ID.FMT, R.INF, F.INF, INF.ERR)
        IF NOT(INF.ERR)  THEN
            NOT.DB.INF = R.INF<INF.MLT.THEIR.REFERENCE,1>
            O.DATA = NOT.DB.INF
        END ELSE
            CALL F.READ( FN.INF.H,TR.ID.H, R.INF.H, F.INF.H, IN.ERR.H)
            NOT.DB.INF = R.INF.H<INF.MLT.THEIR.REFERENCE,1>
            O.DATA = NOT.DB.INF
        END
    CASE OTHERWISE
         O.DATA = ''
    END CASE
**    TEXT = 'O.DATA=':O.DATA ; CALL REM
    RETURN
END
