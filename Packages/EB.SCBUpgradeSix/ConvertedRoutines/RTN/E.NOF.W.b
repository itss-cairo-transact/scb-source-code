* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>450</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.W(Y.RET.DATA)
**  PROGRAM  E.NOF.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*    LOCATE "BRANCH" IN D.FIELDS<1> SETTING BRN.POS THEN
*       COM.CODEE  = D.RANGE.AND.VALUE<BRN.POS>
*   END

**------------
*  COM.CODE  = TRIM(COM.CODEE, "0", "L")
**--------------
    IDD = "aib_out.W"
    WAGDY = ""
    Path = "/hq/opce/bclr/user/aibz/":IDD
    TEXT = "path = ":Path ; CALL REM
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 0
    TEXT = "fileZZ opend" ; CALL REM

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN

*   WAGDY = FIELD(Line,"|",21)
        END
    REPEAT

    CLOSESEQ MyPath
    TEXT = WAGDY ; CALL REM
    Y.RET.DATA<-1> = WAGDY:'*':'DONE'

    RETURN
END
