* @ValidationCode : MjotMjg0ODQyMTEyOkNwMTI1MjoxNjQwODU1MDU4NjQ4OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:04:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*------------ELSAYED-4/11/2013---------*
*-----------------------------------------------------------------------------
* <Rating>-146</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.DRMNT(Y.RET.DATA)
* PROGRAM E.NOF.DRMNT
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CU.DRMNT.INDEX
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
**-----------------------------------*
* COMP = ID.COMPANY
*------------------------------------*
    GOSUB INITIATE
    GOSUB OPEN.FILES
    GOSUB DATA.LOCATE
*------------------------------------*
RETURN
*==============================================================
INITIATE:
    WS.ACCOUNT.NO  = ''
    WS.FROM.DATE   = ''
    WS.END.DATE    = ''
    WS.TOTAL.AMT   = 0
    WS.AC.FLG      = 0
    SEL.STATUS     = ''
    GOSUB CLEAR.DATA
RETURN
*===============================================================
OPEN.FILES:
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.ACH  = 'FBNK.ACCOUNT$HIS' ; F.ACH = '' ; R.ACH = ''
    CALL OPF(FN.ACH,F.ACH)

* Index file for account
    FN.IND.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC = '' ; R.IND.AC = '' ; ER.IND.AC = ''
    CALL OPF(FN.IND.AC,F.IND.AC)

    FN.AC.C = 'FBNK.ACCOUNT.CLOSED' ; F.AC.C = '' ; R.AC.C = '' ; ER.AC.C = ''
    CALL OPF(FN.AC.C,F.AC.C)

    FN.SCDI = "F.SCB.CU.DRMNT.INDEX"  ; F.SCDI = ""
    CALL OPF(FN.SCDI,F.SCDI)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.USR  = "F.USER" ; F.USR   = "" ; R.USR  = ""
    CALL OPF(FN.USR,F.USR)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

RETURN
*===============================================================
DATA.LOCATE:

    LOCATE "CUSTOMER.ID" IN D.FIELDS<1> SETTING CU.POS THEN
        WS.CU.ID  = D.RANGE.AND.VALUE<CU.POS>
    END
    LOCATE "ACCOUNT.NO" IN D.FIELDS<1> SETTING AC.POS THEN
        WS.ACCOUNT.NO  = D.RANGE.AND.VALUE<AC.POS>
    END
    LOCATE "BRANCH" IN D.FIELDS<1> SETTING BR.POS THEN
        WS.BRANCH.ID  = D.RANGE.AND.VALUE<BR.POS>
    END
    LOCATE "FROM.DATE" IN D.FIELDS<1> SETTING DAT.POS THEN
        WS.START.DATE  = D.RANGE.AND.VALUE<DAT.POS>
    END
    LOCATE "TO.DATE" IN D.FIELDS<1> SETTING EDAT.POS THEN
        WS.END.DATE  = D.RANGE.AND.VALUE<EDAT.POS>
    END
    LOCATE "STATUS" IN D.FIELDS<1> SETTING STAT.POS THEN
        WS.STATUS  = D.RANGE.AND.VALUE<STAT.POS>
    END

    IF WS.ACCOUNT.NO THEN
        WS.ACCOUNT.ID = WS.ACCOUNT.NO
        CALL F.READ(FN.AC,WS.ACCOUNT.ID,R.AC,F.AC,ER.AC)
        IF ER.AC THEN
            WS.CU.ID.P = R.AC<AC.CUSTOMER>
        END ELSE
            CALL F.READ.HISTORY(FN.ACH,WS.ACCOUNT.ID,R.ACH,F.ACH,ER.ACH)
            WS.CU.ID.P = R.ACH<AC.CUSTOMER>
        END
        CALL F.READ(FN.SCDI,WS.CU.ID.P,R.SCDI,F.SCDI,ER.SCDI)
        IF ER.SCDI THEN
            TEXT = "��� ������ ��� ����� ����� ������" ; CALL REM
            RETURN
        END

        GOSUB GET.STMT.ENRY
        RETURN
    END
    IF WS.CU.ID THEN
        GOSUB CUSTOMER.SEL
        RETURN
    END
    IF WS.BRANCH.ID THEN
        GOSUB BRANCH.SEL
        RETURN
    END

RETURN
*===============================================================
BRANCH.SEL:
    IF WS.BRANCH.ID NE ID.COMPANY THEN
        TEXT = "��� ������ ��� ����� �������" ; CALL REM
        RETURN
    END
    IF WS.STATUS NE 'A' AND WS.STATUS NE 'D' THEN
        TEXT = "Please select Status ( A or D ) " ; CALL REM
        RETURN
    END
    IF WS.STATUS EQ 'A' THEN
        SEL.STATUS = " AND DRMNT.CODE.DATE EQ '' "
    END

    IF WS.STATUS EQ 'D' THEN
        SEL.STATUS = " AND DRMNT.CODE.DATE NE '' "
    END

    SEL.BRANCH = "SELECT ":FN.SCDI:" WITH COMPANY.BOOK EQ ":WS.BRANCH.ID:SEL.STATUS:" BY @ID "
    CALL EB.READLIST(SEL.BRANCH,KEY.BRANCH,"",SELECTED.BRANCH,ER.MSG.BRANCH)
    IF SELECTED.BRANCH THEN
        FOR IBRANCH = 1 TO SELECTED.BRANCH
            WS.CU.ID = KEY.BRANCH<IBRANCH>
            GOSUB CUSTOMER.SEL
        NEXT IBRANCH
    END
RETURN
*===============================================================
CUSTOMER.SEL:
    CALL F.READ(FN.SCDI,WS.CU.ID,R.SCDI,F.SCDI,ER.SCDI)
    IF ER.SCDI THEN
        TEXT = "��� ������ ��� ����� ����� ������" ; CALL REM
        RETURN
    END

    CALL F.READ(FN.IND.AC,WS.CU.ID,R.IND.AC,F.IND.AC,ER.IND.AC)
    LOOP
        REMOVE WS.ACCOUNT.NO FROM R.IND.AC SETTING POS.AC
    WHILE WS.ACCOUNT.NO:POS.AC
        CALL F.READ(FN.AC,WS.ACCOUNT.NO,R.AC,F.AC,ER.AC)

        WS.CU.ID.P = WS.CU.ID
        GOSUB GET.STMT.ENRY

    REPEAT

    WS.CU.ID.C = WS.CU.ID +100000000
    WS.CU.ID.C = WS.CU.ID.C[2,8]
    SEL.CLOSED = "SELECT ":FN.AC.C:" WITH @ID EQ ":WS.CU.ID.C:"] BY @ID "
    CALL EB.READLIST(SEL.CLOSED,KEY.CLOSED,"",SELECTED.CLOSED,ER.MSG.TEMP)
    IF SELECTED.CLOSED THEN
        FOR ICLOSED = 1 TO SELECTED.CLOSED
            WS.ACCOUNT.NO = KEY.CLOSED<ICLOSED>
            CALL F.READ(FN.AC,WS.ACCOUNT.NO,R.AC,F.AC,ER.AC)
            IF ER.AC THEN
                WS.CU.ID.P    = WS.CU.ID
                GOSUB GET.STMT.ENRY
            END
        NEXT ICLOSED
    END
RETURN
*---------------------------------------------------------------------------------------------------------
GET.STMT.ENRY:
    CALL EB.ACCT.ENTRY.LIST(WS.ACCOUNT.NO<1>,WS.START.DATE,WS.END.DATE,ID.LIST,WS.OPENING.BAL,ER)
    IF ID.LIST EQ '' THEN
        RETURN
    END
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS
    WHILE STE.ID:POS
        IF STE.ID THEN
            CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)

            WS.STE.OUR.REF.ID       =  R.STE<AC.STE.OUR.REFERENCE>
            WS.STE.THEIR.REF.ID     =  R.STE<AC.STE.THEIR.REFERENCE>
            WS.STE.TRANS.REF        =  FIELD(R.STE<AC.STE.TRANS.REFERENCE>,'\',1)

            IF WS.STE.TRANS.REF EQ '' THEN
                WS.STE.TRANS.REF    = R.STE<AC.STE.TRANS.REFERENCE>
            END

            IF WS.STE.OUR.REF.ID EQ '' AND WS.STE.TRANS.REF NE '' THEN
                WS.STE.OUR.REF.ID = WS.STE.TRANS.REF
            END

            WS.STE.CUR              =  R.STE<AC.STE.CURRENCY>
            WS.STE.COMP             =  R.STE<AC.STE.COMPANY.CODE>
            IF WS.STE.CUR EQ 'EGP' THEN
                WS.STE.AMT          =  R.STE<AC.STE.AMOUNT.LCY>
            END ELSE
                WS.STE.AMT          =  R.STE<AC.STE.AMOUNT.FCY>
            END
            WS.NET.AMT             +=  WS.STE.AMT
            WS.STE.BOOK.DATE        =  R.STE<AC.STE.BOOKING.DATE>
            WS.STE.VALE.DATE        =  R.STE<AC.STE.VALUE.DATE>
            WS.STE.TRNS.CODE        =  R.STE<AC.STE.TRANSACTION.CODE>
            WS.STE.ID               =  STE.ID

            IF WS.AC.FLG EQ 0 THEN
                WS.NET.AMT +=  WS.OPENING.BAL
                WS.AC.FLG  = 1
            END

            GOSUB GET.STMT.INP.ATH
            GOSUB SEND.DATA
        END
    REPEAT
*    WS.NET.AMT = WS.TOTAL.AMT + WS.OPENING.BAL
    GOSUB SEND.CLOSD.AMT
    WS.AC.FLG  = 0
RETURN
*-----------------------------------------------------------
GET.STMT.INP.ATH:
*----------------------------------------------------------*
    WS.INP  = FIELD(R.STE<AC.STE.INPUTTER><1,1>,'_',2)
    GOSUB GET.INP.NAME
    WS.AUTH = FIELD(R.STE<AC.STE.AUTHORISER>,'_',2)
    GOSUB GET.AUTH.NAME
RETURN
*----------------------------------------------------------*
GET.INP.NAME:
    CALL F.READ(FN.USR,WS.INP,R.USR,F.USR,ER.USRI)
    IF NOT(ER.USRI) THEN
        WS.INP = R.USR<EB.USE.USER.NAME>
    END
RETURN
*----------------------------------------------------------*
GET.AUTH.NAME:
    CALL F.READ(FN.USR,WS.AUTH,R.USR,F.USR,ER.USRA)
    IF NOT(ER.USRA) THEN
        WS.AUTH = R.USR<EB.USE.USER.NAME>
    END
RETURN
*----------------------------------------------------------*
SEND.DATA:
    IF WS.STE.OUR.REF.ID EQ '' THEN
        WS.STE.OUR.REF.ID = 'XXXXXXXXXX'
    END

    WS.DATA  =        WS.STE.OUR.REF.ID
    WS.DATA :=   "*": WS.ACCOUNT.NO
    WS.DATA :=   "*": WS.FROM.DATE
    WS.DATA :=   "*": WS.END.DATE
    WS.DATA :=   "*": WS.STE.CUR
    WS.DATA :=   "*": WS.STE.TRNS.CODE
    WS.DATA :=   "*": WS.STE.AMT
    WS.DATA :=   "*": WS.STE.BOOK.DATE
    WS.DATA :=   "*": WS.INP
    WS.DATA :=   "*": WS.AUTH
    WS.DATA :=   "*": WS.STE.VALE.DATE
    WS.DATA :=   "*": WS.STE.ID
    WS.DATA :=   "*": WS.CU.ID.P
    WS.DATA :=   "*": WS.OPENING.BAL

    Y.RET.DATA <-1> = WS.DATA
    GOSUB CLEAR.DATA
RETURN
*----------------------------------------------------------*
SEND.CLOSD.AMT:
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*"
    WS.DATA :=   "*": WS.NET.AMT

    Y.RET.DATA <-1> = WS.DATA

    WS.NET.AMT   = ''
    WS.TOTAL.AMT = 0
RETURN
*----------------------------------------------------------*

CLEAR.DATA:
    WS.STE.CUR          = ''
    WS.STE.COMP         = ''
    WS.STE.AMT          = 0
    WS.STE.BOOK.DATE    = ''
    WS.INP              = ''
    WS.AUTH             = ''
    WS.STE.TRNS.CODE    = ''
    WS.STE.VALE.DATE    = ''
    WS.STE.TRANS.REF    = ''
    WS.OPENING.BAL      = ''
*    WS.STE.ID          = ''
    WS.ACCOUNT.NO       = ''
    WS.DATA             = ''
    WS.CU.ID.P          = ''
RETURN
*----------------------------------------------------------*
END
