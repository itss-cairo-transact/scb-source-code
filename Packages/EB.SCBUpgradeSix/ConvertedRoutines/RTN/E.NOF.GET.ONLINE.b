* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>144</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.GET.ONLINE(Y.RET.DATA)

* PROGRAM E.NOF.GET.ONLINE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY

    LOCATE "ACCOUNT.NUMBER" IN D.FIELDS<1> SETTING YACC.POS THEN ACCOUNT.NO    = D.RANGE.AND.VALUE<YACC.POS> ELSE RETURN

* ACCOUNT.NO = ' '
    GOSUB INITIATE
*Line [ 38 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*===========================================
INITIATE:
*--------
    MM  = 0
    ZZ  = 0
    CRN = 0
    DBN = 0
    DB.ARR   = ''
    CR.ARR   = ''
    RET.REC  = ''
    DATA.ARR = ''
    FROM.DATE = TODAY
    END.DATE  = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-12')
*  CRT FROM.DATE:"-":END.DATE
    RETURN
*===========================================
CALLDB:
*------
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    RETURN
*===========================================
PROCESS:
*---------
    CALL EB.ACCT.ENTRY.LIST(ACCOUNT.NO<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    TXN.COUNT  = DCOUNT(ID.LIST,@FM)
***    TXN.COUNT1 = TXN.COUNT - 9

    FOR  I = TXN.COUNT TO 1 STEP -1
        STD.ID = ID.LIST<I>
        IF STD.ID THEN
            CALL F.READ(FN.AC,ACCOUNT.NO,R.AC,F.AC,ER.AC)
            AC.ONLINE.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
            AC.WORKING.BAL = R.AC<AC.WORKING.BALANCE>

            CALL F.READ(FN.STE,STD.ID,R.STE,F.STE,ER.STE)
            STE.OUR.REF = R.STE<AC.STE.OUR.REFERENCE>
            STE.TXN.CODE = R.STE<AC.STE.TRANSACTION.CODE>
            STE.AC.NO   = R.STE<AC.STE.ACCOUNT.NUMBER>
            STE.CUR     = R.STE<AC.STE.CURRENCY>
            IF STE.CUR EQ LCCY THEN
                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
            END ELSE
                STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
            END
            STE.DAT = R.STE<AC.STE.BOOKING.DATE>
*-----------------------------------
            RET.REC = STE.OUR.REF:"*":STE.AC.NO:"*":STE.CUR:"*":AC.ONLINE.BAL:"*":AC.WORKING.BAL:"*":STE.AMT:"*":STE.DAT:"*":STE.TXN.CODE
*-----------------------------------
            IF STE.AMT GT 0 AND CRN LT 10 THEN
                CRN++
                CR.ARR<CRN> = RET.REC
            END
            IF STE.AMT LT 0 AND DBN LT 10 THEN
                DBN++
                DB.ARR<DBN> = RET.REC
            END
            IF CRN EQ 10 AND DBN EQ 10 THEN
                I = 1
            END
        END
    NEXT I

    GOSUB FIX.ARR
    RETURN
*=============================================
FIX.ARR:
*---------
*    CR.ARR = SORT(CR.ARR)
*   DB.ARR = SORT(DB.ARR)

    DB.COUNT = DCOUNT(DB.ARR,@FM)
    CR.COUNT = DCOUNT(CR.ARR,@FM)
    FOR DD = DB.COUNT TO 1 STEP -1
        MM++
        DATA.ARR = DB.ARR<DD>
        GOSUB SEND.ID
    NEXT DD
    FOR CC= CR.COUNT TO 1 STEP -1
        MM++
        DATA.ARR = CR.ARR<CC>
        GOSUB SEND.ID
    NEXT CC
    RETURN
*=============================================
SEND.ID:
*---------
*  CRT MM:"*":DATA.ARR

    Y.RET.DATA<-1> = DATA.ARR

    RETURN
END
