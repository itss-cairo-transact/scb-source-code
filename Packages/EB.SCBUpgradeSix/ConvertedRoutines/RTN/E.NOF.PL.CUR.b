* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>177</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.PL.CUR(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL


    LOCATE "CURR" IN D.FIELDS<1> SETTING BRN.POS THEN
        CURRR  = D.RANGE.AND.VALUE<BRN.POS>
    END

    LOCATE "DATE" IN D.FIELDS<1> SETTING BRN.POS THEN
        DATEE  = D.RANGE.AND.VALUE<BRN.POS>
    END
 *   TEXT =  CURRR ; CALL REM
 *   TEXT =  DATEE ; CALL REM

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN
*------------------------------
INITIALISE:

    FN.BAL   = 'F.RE.STAT.LINE.BAL'     ; F.BAL   = ''
    FN.COMP  = 'F.COMPANY'              ; F.COMP  = ''

    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.COMP,F.COMP)

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""

    RETURN
**----------------------------------------------------------------**
BUILD.RECORD:

    T.SEL    = "SELECT ":FN.COMP:" WITH @ID NE EG0010088 BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO  SELECTED
            XX = 1
            YY = 25
            CALL F.READ(FN.COMP,KEY.LIST<HH>,R.COMP,F.COMP,READ.ERR)
            LOSS.AMT = 0
            PRFT.AMT = 0
            NET.BAL  = 0
            FOR MM = 1 TO 4
                YY = 25

                IDD= 'GENLED-0120-':CURRR:'-':DATEE:'*GENLED-0320-':CURRR:'-':DATEE:'*GENLED-0630-':CURRR:'-':DATEE:'*GENLED-0840-':CURRR:'-':DATEE:'*'

                CONT.ID  = IDD[XX,YY]:KEY.LIST<HH>

                CALL F.READ(FN.BAL,CONT.ID,R.BAL,F.BAL,READ.ERR1)
                CLOS.BAL = R.BAL<RE.SLB.CLOSING.BAL>
                LIN.REF  = CONT.ID[8,4]

                IF  LIN.REF EQ '0120' OR LIN.REF EQ '0320' THEN
                    LOSS.AMT +=  CLOS.BAL
                END
                IF  LIN.REF EQ '0630' OR LIN.REF EQ '0840' THEN
                    PRFT.AMT +=  CLOS.BAL
                END

                XX = XX + 25
            NEXT MM
            NET.BAL  = LOSS.AMT + PRFT.AMT
            Y.RET.DATA<-1> =  KEY.LIST<HH>:"*":LOSS.AMT:"*":PRFT.AMT:"*":NET.BAL:"*":CURRR:"*":DATEE

        NEXT HH

    END
    RETURN

**---------------------------------------------------------------------**
END
