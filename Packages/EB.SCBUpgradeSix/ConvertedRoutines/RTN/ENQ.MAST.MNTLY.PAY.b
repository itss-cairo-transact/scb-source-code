* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*****NESSREEN AHMED 10/6/2019***************************
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ENQ.MAST.MNTLY.PAY(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.PAY.FLAG
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.MNTLY.PAY.AMT

    COMP = C$ID.COMPANY

    YYYYMM = TODAY[1,6]

    CHK.SEL = "SELECT F.SCB.MAST.PAY.FLAG WITH COMPANY.CO EQ 'EG0010099' AND PAYMENT.FLAG EQ 'YES' AND YEAR.MM EQ ":YYYYMM
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF NOT(SELECTED.CHK) THEN
        ENQ.ERROR = "�� ��� ����� ����� ���"
    END ELSE
**********************************
        T.SEL = "SELECT F.SCB.MAST.MNTLY.PAY.AMT WITH COMPANY.CO EQ " : COMP :" AND YEAR.MONTH EQ ":YYYYMM :" BY CR.ACCT.NO"
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
      **  TEXT = 'SELECTED=':SELECTED ; CALL REM
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = "DUMM"
        END
*************************************************************
    END
    RETURN
END
