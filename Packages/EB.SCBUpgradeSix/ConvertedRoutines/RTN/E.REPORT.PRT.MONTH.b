* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.REPORT.PRT.MONTH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID, HOLD.ID, REP.TITLE
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP
*-----------------------------------------------------------------------
*    DEPT = O.DATA
     WS.COMP = ID.COMPANY

**-------------------------
    WS.USER.DSP = 'N'
*-----------------------------------------------------------------------
    FN.HOLD = "F.HOLD.CONTROL"
    F.HOLD  = ""
    R.HOLD = ""
    Y.HOLD.ID   = ""
    CALL OPF(FN.HOLD,F.HOLD)

*   ----------------------------
    SYS.DATE = TODAY
    WS.DATE = SYS.DATE
**  ������ �����
**������� ����� ������
*    WRK.DATE = TODAY
*    P.MON.YY   = WRK.DATE[1,4]
*    P.MON.MM   = WRK.DATE[5,2] - 1
*    P.MON.DD   = WRK.DATE[7,2]
*    IF P.MON.MM LT 01 THEN
*        P.MON.YY = P.MON.YY - 1
*        P.MON.MM = 12
*    END
*    P.MON.MM = FMT(P.MON.MM,"R%2")
*    P.MON.DATE = P.MON.YY:P.MON.MM:'...'

*------------------------------------------------------------------------
**  ������ ������
**  ������� ����� ����� ������
**  STR.DATE =  ��� ��� ��� �� ����� ������
**  END.DATE =  ��� ��� ��� �� ����� ������


    WS.DATE = TODAY

    WS.YY   = WS.DATE[1,4]
    WS.MM   = WS.DATE[5,2]
    WS.DD   = WS.DATE[7,2]
    WS.DATE  = WS.YY:WS.MM:"01"


    CALL CDT('',WS.DATE,'-1W')
    STR.DATE = WS.DATE

    CALL CDT('',WS.DATE,'+1W')
    END.DATE = WS.DATE


*    STR.DATE = 20091112
*    END.DATE = 20091115
*    TEXT = 'SEARCH.DATE ':STR.DATE:' ':END.DATE; CALL REM
*    P.MON.DATE = '200911...'
*    CALL CDT('',WS.DATE,'-1W')
*    WS.DATE = 20081231
*-----------------------------------------------------------------------
*Line [ 94 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2.DEV

*Line [ 97 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2

    GOSUB READ.TEXT.FILE


    RETURN
*-----------------------------------------------------------------------
READ.TEXT.FILE:

    POS = 0
    REP.NAME = ''
    REP.TITLE = ''
    X = DCOUNT(C$REP.NAME,@FM)


    FOR I = 1 TO X

        FINDSTR STR.ONE IN C$DALL<I> SETTING POS ELSE POS = 0
*---------------------------------
        REP.NAME = C$REP.NAME<I>
*---------------------------------
        IF REP.NAME[1,2] NE 'SB' THEN
            POS = 0
        END
*---------------------------------
        IF POS GT 0 THEN
            REP.TITLE = C$REP.TITLE<I>
            GOSUB READ.HOLD.FILE
        END
*---------------------------------
    NEXT I

*---------------
***    COMP.ALL = ''
*---------------
    RETURN
*--------------------------------------------------------------------
READ.HOLD.FILE:

    HOLD.ID = ''

*------------------------------------------------------------------------------
    BEGIN CASE
    CASE     COMP.ALL EQ ''
        SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
****        SEL.HOLD := " AND BANK.DATE LIKE ":P.MON.DATE
        SEL.HOLD := " AND BANK.DATE GE ":STR.DATE
        SEL.HOLD := " AND BANK.DATE LE ":END.DATE
        SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"
        SEL.HOLD := " AND COMPANY.ID EQ ":WS.COMP
*---------------
    CASE    COMP.ALL EQ 'ALL'
        SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
****        SEL.HOLD := " AND BANK.DATE LIKE ":P.MON.DATE
        SEL.HOLD := " AND BANK.DATE GE ":STR.DATE
        SEL.HOLD := " AND BANK.DATE LE ":END.DATE
        SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"
******        SEL.HOLD := " AND COMPANY.ID EQ ":WS.COMP
    END  CASE
*------------------------------------------------------------------------------


    CALL EB.READLIST(SEL.HOLD,SEL.LIST.HOLD,'',NO.OF.REP,ERR.HOLD)

    LOOP
        REMOVE Y.HOLD.ID FROM SEL.LIST.HOLD SETTING POS.HOLD
    WHILE Y.HOLD.ID:POS.HOLD
        CALL F.READ(FN.HOLD,Y.HOLD.ID,R.HOLD,F.HOLD,ERR.HOLD)

        IF Y.HOLD.ID   THEN
            HOLD.ID = Y.HOLD.ID

            IF WS.USER.DSP = 'Y' THEN
                TEXT = REP.TITLE;CALL REM
            END

            EXECUTE "E.REPORT.PRT2X"

        END

    REPEAT


    RETURN
*--------------------------------------------------------------------
