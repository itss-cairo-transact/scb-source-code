* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 19/02/2017*********************************
*-----------------------------------------------------------------------------
* <Rating>-44</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.USR.ENQ.EMP(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ENQ.SAVE.DATA
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    T.DATE = TODAY
    YYYYMM = TODAY[1,6]
*----
    FN.ENQ.SAV = 'F.SCB.ENQ.SAVE.DATA'     ; F.ENQ.SAV = '' ; R.ENQ.SAV = ''
    CALL OPF(FN.ENQ.SAV,F.ENQ.SAV)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    YTEXT = "Enter User.Number : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    USR.NO = COMI
    YTEXT = "Enter Start.Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.D = COMI
    YTEXT = "Enter End.Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ED.D = COMI

    T.SEL = "SELECT F.SCB.ENQ.SAVE.DATA WITH ENQ.USERID EQ ":USR.NO: " AND (ENQ.DATE GE " : ST.D : " AND ENQ.DATE LE " :ED.D : " ) BY ENQ.DATE BY ENQ.TIME "

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SEL=':SELECTED ; CALL REM
    REQ.USR = FIELD(USR.NO, "SCB.", 2)
    FOR I = 1 TO SELECTED
        CALL F.READ( FN.ENQ.SAV,KEY.LIST<I>, R.ENQ.SAV, F.ENQ.SAV, ERR.ENQ)

        USR.NAME<I>  = R.ENQ.SAV<ENQ.SAVE.ENQ.USERNAME>
        ENQ.DAT<I>   = R.ENQ.SAV<ENQ.SAVE.ENQ.DATE>
        ENQ.TIM<I>   = R.ENQ.SAV<ENQ.SAVE.ENQ.TIME>
        ENQ.COMP<I>  = R.ENQ.SAV<ENQ.SAVE.ENQ.COMPANY>
        ENQ.NAM<I>   = R.ENQ.SAV<ENQ.SAVE.ENQ.NAME>
        ENQ.FIELD<I> = R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.NAME>
        ENQ.OPR<I>   = R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.OPER>
        ENQ.DATA<I>  = R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.DATA>

        FLAG.P<I> = 0
*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD = DCOUNT(ENQ.FIELD<I>,@VM)
        FOR NN = 1 TO DD
            ENQ.NAM.SER<NN>  = R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.NAME,NN>
            ENQ.DATA.SER<NN> = R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.DATA,NN>
            IF ENQ.NAM.SER<NN> EQ 'ACCOUNT' THEN
                CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ENQ.DATA.SER<NN> , CUST.NO)
                IF CUST.NO NE '' THEN
                    CALL F.READ(FN.CUSTOMER, CUST.NO, R.CUSTOMER, F.CUSTOMER, E1)
                    LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                    CUS.NAME<I>  = LOCAL.REF<1,CULR.ARABIC.NAME>
                    CUS.SEC = R.CUSTOMER<EB.CUS.SECTOR>
                    EMP.NO  = LOCAL.REF<1,CULR.EMPLOEE.NO>
                    IF (CUS.SEC EQ 1100) OR (CUS.SEC EQ 1200) OR (CUS.SEC EQ 1300) OR (CUS.SEC EQ 1400) THEN
                        IF EMP.NO # REQ.USR THEN
                            FLAG.P<I> = 1
                        END
                    END
                END
            END ELSE
                IF ENQ.NAM.SER<NN> EQ 'CUSTOMER.ID' THEN
                    CALL F.READ(FN.CUSTOMER, ENQ.DATA.SER<NN> , R.CUSTOMER, F.CUSTOMER, E1)
                    LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                    CUS.NAME<I>  = LOCAL.REF<1,CULR.ARABIC.NAME>
                    CUS.SEC = R.CUSTOMER<EB.CUS.SECTOR>
                    EMP.NO  = LOCAL.REF<1,CULR.EMPLOEE.NO>
                    IF (CUS.SEC EQ 1100) OR (CUS.SEC EQ 1200) OR (CUS.SEC EQ 1300) OR (CUS.SEC EQ 1400) THEN
                        IF EMP.NO # REQ.USR THEN
                            FLAG.P<I> = 1
                        END
                    END
                END
            END
        NEXT NN
        IF FLAG.P<I> = 1 THEN
            Y.RET.DATA<-1> = USR.NAME<I>:"*":ST.D :"*": ED.D:"*":ENQ.DAT<I>:"*":ENQ.TIM<I>:"*":ENQ.COMP<I>:"*":ENQ.NAM<I>:"*":ENQ.FIELD<I>:"*":ENQ.OPR<I>:"*":ENQ.DATA<I>:"*":CUS.NAME<I>
        END
    NEXT I
*-----------------------------------------------------------------------*
    RETURN
END
