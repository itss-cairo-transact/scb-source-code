* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.SCB.USD.TRANS.CORP(Y.FT.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------

    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-------------------------------------------------------------------------
INITIATE:

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.CCY = 'F.SCB.CURRENCY.DAILY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    WS.AMT.LT = 0 ; WS.AMT.GE = 0 ; AMT.TOTAL = 0

    YTEXT = "Enter Date from : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DAT.FROM = COMI

    YTEXT = "Enter Date to : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DAT.TO = COMI

    RETURN

*========================================================================
PROCESS:

    T.SEL  = "SELECT ":FN.FT:" WITH DEBIT.CURRENCY NE 'EGP' AND CREDIT.CURRENCY EQ 'EGP' AND DEBIT.CUSTOMER EQ CREDIT.CUSTOMER AND DEBIT.CUSTOMER NE '' AND CREDIT.CUSTOMER NE ''"
    T.SEL := " AND PROCESSING.DATE GE ":DAT.FROM:" AND PROCESSING.DATE LE ":DAT.TO
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,E1)

            WS.CUS.CODE = R.FT<FT.DEBIT.CUSTOMER>
            WS.CCY      = R.FT<FT.DEBIT.CURRENCY>

            CALL F.READ(FN.CU,WS.CUS.CODE,R.CU,F.CU,E2)
            WS.CUS.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF WS.CUS.SECTOR NE '4650' THEN
                WS.AMT.FCY = R.FT<FT.AMOUNT.DEBITED>[4,15]
                WS.AMT.LCY = R.FT<FT.AMOUNT.CREDITED>[4,15]

                IF WS.CCY NE 'USD' THEN
                    WS.CCY.ID  = 'USD-EGP-':R.FT<FT.PROCESSING.DATE>
                    CALL F.READ(FN.CCY,WS.CCY.ID,R.CCY,F.CCY,E3)
                    WS.RATE    = R.CCY<SCCU.SELL.RATE,1>
                    WS.AMT.USD = WS.AMT.LCY / WS.RATE
                END ELSE
                    WS.AMT.USD = WS.AMT.FCY
                END

                IF WS.AMT.USD GE 100000 THEN
                    WS.CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                    WS.CUS.ID   = R.CU<EB.CUS.LOCAL.REF><1,CULR.COM.REG.NO>
                    WS.CUS.CBE  = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                    WS.AMT.GE  += WS.AMT.USD
                    Y.FT.DATA<-1> = WS.CUS.NAME:"*":WS.CUS.CBE:"*":WS.CUS.ID:"*":WS.AMT.USD
                END
                IF WS.AMT.USD LT 100000 THEN
                    WS.AMT.LT += WS.AMT.USD
                END
                AMT.TOTAL += WS.AMT.USD
            END
        NEXT I
    END

    TOTAL.TXT.LT = '������� ������� ����� ����� ��� �� 100 ��� �����'
    Y.FT.DATA<-1> = TOTAL.TXT.LT:"*":" ":"*":" ":"*":WS.AMT.LT

    TXT.TOTAL  = '��������'
    Y.FT.DATA<-1> = TXT.TOTAL:"*":" ":"*":" ":"*":AMT.TOTAL

*********************************
    RETURN
END
