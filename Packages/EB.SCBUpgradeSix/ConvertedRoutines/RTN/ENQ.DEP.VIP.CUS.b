* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>777</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.DEP.VIP.CUS(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY


    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    AMT.MAX = '3000000'
    FN.LD.LOANS.AND.DEPOSITS = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD.LOANS.AND.DEPOSITS = '' ; R.LD.LOANS.AND.DEPOSITS = ''
    CALL OPF( FN.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS)
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21001 AND CATEGORY LE 21010 AND CO.CODE EQ ":COMP:" BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**************************************************
    IF SELECTED THEN
        ENQ.LP = 0
        Z = 0
        FOR ENQ.LP = 1 TO SELECTED
            CALL F.READ(FN.LD.LOANS.AND.DEPOSITS,KEY.LIST<ENQ.LP>, R.LD.LOANS.AND.DEPOSITS, F.LD.LOANS.AND.DEPOSITS,E)

            CURR = R.LD.LOANS.AND.DEPOSITS<LD.CURRENCY>
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.AMOUNT>
            TMP.FLG = 0
            BEGIN CASE
            CASE CURR = "EGP"
                IF AMT GE AMT.MAX THEN
                    TMP.FLG = 1
                END
            CASE CURR = "USD"
                LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ;UNEQU.COMM = "" ; CURR = ""
                MARKET = ""
                UNEQU.COMM = AMT
                CURR = "USD"
                CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                IF LCOMM GE AMT.MAX THEN
                    TMP.FLG = 1
                END
            CASE CURR = "SAR"
                LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ;UNEQU.COMM = "" ; CURR = ""
                MARKET = ""
                UNEQU.COMM = AMT
                CURR = "SAR"
                CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                IF LCOMM GE AMT.MAX THEN
                    TMP.FLG = 1
                END
            CASE CURR = "GBP"
                LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ;UNEQU.COMM = "" ; CURR = ""
                MARKET = ""
                UNEQU.COMM = AMT
                CURR = "GBP"
                CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                IF LCOMM GE AMT.MAX THEN
                    TMP.FLG = 1
                END
            CASE CURR = "EUR"
                LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ;UNEQU.COMM = "" ; CURR = ""
                MARKET = ""
                UNEQU.COMM = AMT
                CURR = "EUR"
                CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                IF LCOMM GE AMT.MAX THEN
                    TMP.FLG = 1
                END
            CASE CURR = "CHF"
                LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ;UNEQU.COMM = "" ; CURR = ""
                MARKET = ""
                UNEQU.COMM = AMT
                CURR = "CHF"
                CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                IF LCOMM GE AMT.MAX THEN
                    TMP.FLG = 1
                END
            END CASE
            IF TMP.FLG = 1 THEN
                Z = Z+1
                ENQ<2,Z> = '@ID'
                ENQ<3,Z> = 'EQ'
                ENQ<4,Z> = KEY.LIST<ENQ.LP>
            END
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO TRANSACTION ISSUED"
*        ENQ<2> = '@ID'
*       ENQ<3> = 'EQ'
*     ENQ<4> = 'DUUMY'
    END
    RETURN
END
