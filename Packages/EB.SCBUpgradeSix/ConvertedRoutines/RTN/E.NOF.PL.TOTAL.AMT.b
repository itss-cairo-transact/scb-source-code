* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
************* SAIZO 2011/12/20****************
    SUBROUTINE E.NOF.PL.TOTAL.AMT(Y.ALL.REC)
***    PROGRAM    E.NOF.PL.TOTAL.AMT

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    KK = 0
    XX = SPACE(120)
    CTE.BAL  = ''
    CAT.GRP  = ''
    ACCT.ID  = ''
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CAT = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = '' ; ER.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'F.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CTE = 'F.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.CTP = 'F.CATEG.ENTRY' ; F.CTP = '' ; R.CTP = '' ; ER.CTP = ''
    CALL OPF(FN.CTP,F.CTP)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "PL.CATEGORY" IN D.FIELDS<1> SETTING YCAT.POS THEN CAT.ID    = D.RANGE.AND.VALUE<YCAT.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
    LOCATE "AMOUNT.LCY" IN D.FIELDS<1> SETTING YPCAT.POS THEN AMOUNT.LCY = D.RANGE.AND.VALUE<YPCAT.POS> ELSE RETURN

    CUR.SEL  = "SELECT ":FN.CUR
    CALL EB.READLIST(CUR.SEL,K.LIST.CUR,'',SELECTED.CUR,CUR.ER.MSG)
    LOOP

        REMOVE CUR.ID FROM K.LIST.CUR SETTING POS.CUR
    WHILE CUR.ID:POS.CUR
        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,END.DATE,CAT.ID,CAT.LIST)
*�        GOSUB GET.OPEN.BAL
        CTE.BAL = OPENING.BAL
        TOT.DR.MVMT = 0
        TOT.CR.MVMT = 0
        K = 0
        LOOP
            REMOVE CTE.ID FROM CAT.LIST SETTING POS
        WHILE CTE.ID:POS
            CALL F.READ(FN.CTE,CTE.ID,R.CTE,F.CTE,ER.CTE)
            CTE.CUR = R.CTE<AC.CAT.CURRENCY>
            CTE.ALCY = R.CTE<AC.CAT.AMOUNT.LCY>
            IF ABS(CTE.ALCY) EQ AMOUNT.LCY  THEN
                IF CTE.CUR EQ CUR.ID THEN
                    IF CTE.CUR EQ LCCY THEN
                        CTE.AMT = R.CTE<AC.CAT.AMOUNT.LCY>
                    END ELSE
                        CTE.AMT = R.CTE<AC.CAT.AMOUNT.FCY>
                    END
                    CTE.DAT = R.CTE<AC.CAT.BOOKING.DATE>
                    CTE.VAL = R.CTE<AC.CAT.VALUE.DATE>
                    CTE.TXN = R.CTE<AC.CAT.TRANSACTION.CODE>
                    CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,CTE.TXN,CTE.TRN)
                    CTE.OUR = R.CTE<AC.CAT.OUR.REFERENCE>
                    IF CTE.OUR EQ '' THEN
                        CTE.OUR = R.CTE<AC.CAT.TRANS.REFERENCE>
                    END
                    CTE.INP1 = R.CTE<AC.CAT.INPUTTER>
                    CTE.AUT1 = R.CTE<AC.CAT.AUTHORISER>
                    CTE.INP  = FIELD(CTE.INP1,'_',2)
                    CTE.AUT  = FIELD(CTE.AUT1,'_',2)
                    CTE.BAL += CTE.AMT
                    K++
                    IF CTE.AMT LT 0 THEN
                        TOT.DR.MVMT += CTE.AMT
                    END ELSE
                        TOT.CR.MVMT += CTE.AMT
                    END
                END
            END
        REPEAT
        TOT.ALL = ABS(OPENING.BAL) + ABS(TOT.DR.MVMT) + ABS(TOT.CR.MVMT) + ABS(CTE.BAL)
        IF TOT.ALL NE 0 THEN
            GOSUB RET.DATA
        END
    REPEAT
    RETURN
**************************************
GET.OPEN.BAL:
*-----------
    OPENING.BAL = 0
    SS.YEAR = FROM.DATE[1,4]
    SS.DATE = SS.YEAR:'0101'
    EE.DATE = FROM.DATE
    CALL CDT("",EE.DATE,'-1C')

    CALL GET.CATEG.MONTH.ENTRIES(SS.DATE,EE.DATE,CAT.ID,CAT.PRV.LIST)
    LOOP
        REMOVE CTP.ID FROM CAT.PRV.LIST SETTING POS.CTP
    WHILE CTP.ID:POS.CTP
        CALL F.READ(FN.CTP,CTP.ID,R.CTP,F.CTP,ER.CTP)
        CTP.CUR = R.CTP<AC.CAT.CURRENCY>
        IF CTP.CUR EQ CUR.ID THEN
            IF CTP.CUR EQ LCCY THEN
                OPENING.BAL += R.CTP<AC.CAT.AMOUNT.LCY>
            END ELSE
                OPENING.BAL += R.CTP<AC.CAT.AMOUNT.FCY>
            END
        END
    REPEAT
    RETURN
**************************************
RET.DATA:
*--------
    TOT.CR.DR = TOT.CR.MVMT + TOT.DR.MVMT
    Y.ALL.REC<-1> = COMP:"*":CAT.ID:"*":CUR.ID:"*":CAT.ID:"*":FROM.DATE:"*":END.DATE:"*":OPENING.BAL:"*":TOT.DR.MVMT:"*":TOT.CR.MVMT:"*":TOT.CR.DR:"*":CTE.BAL:"*":AMOUNT.LCY:"*":K
***    CRT COMP:"*":CUR.ID:"*":CAT.ID:"*":OPENING.BAL:"*":TOT.DR.MVMT:"*":TOT.CR.MVMT:"*":TOT.CR.DR:"*":CTE.BAL
    RETURN
**************************************
END
