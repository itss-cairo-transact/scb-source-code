* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.GET.FT.ARC(NOF.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

    FT.ARR = ''
    FLAG = "Y"
    LOOP WHILE FLAG EQ "Y" OR FLAG EQ "y" DO
        YTEXT = "Enter FT id"
        CALL TXTINP(YTEXT, 8, 22, "30", "A")
        APPL.ID = COMI
        FT.ARR<-1> = APPL.ID

        YTEXT = "If you want to continue y or exit (n)"
        CALL TXTINP(YTEXT, 8, 22, "30", "A")
        FLAG = COMI
    REPEAT
    GOSUB INIT_FT_FILE
    GOSUB PROC_FT
    RETURN

    INIT_FT_FILE:
    FN.FT = 'FBNK.FUNDS.TRANSFER$ARC' ; F.FT = ''
    CALL OPF(FN.FT , F.FT)
    RETURN

    PROC_FT:
*Line [ 46 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    COUNT.FT = DCOUNT(FT.ARR,@FM)
    FT.ID = ''
    IF COUNT.FT THEN
        FOR I=1 TO COUNT.FT
            FT.ID = FT.ARR<I>:";1"
            CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ETEXT.FT)
            IF R.FT THEN
                GG.DATA = FT.ARR<I>:"*"
                GG.DATA := R.FT<FT.DEBIT.ACCT.NO>:"*"
                GG.DATA := R.FT<FT.DEBIT.CURRENCY>:"*"
                GG.DATA := R.FT<FT.DEBIT.AMOUNT>:"*"
                GG.DATA := R.FT<FT.CREDIT.ACCT.NO>:"*"
                GG.DATA := R.FT<FT.CREDIT.CURRENCY>:"*"
                GG.DATA := R.FT<FT.CREDIT.AMOUNT>
                NOF.DATA<-1> = GG.DATA
            END
        NEXT I

    END
    RETURN
END
