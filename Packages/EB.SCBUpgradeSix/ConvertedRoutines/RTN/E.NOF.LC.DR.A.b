* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-61</Rating>
*-----------------------------------------------------------------------------
**************************MUHAMMAD-ELSAYED-25/7/2010********************************************

****************************************************************************
    SUBROUTINE E.NOF.LC.DR.A(Y.RET.DATA)


* PROGRAM E.NOF.LC.DR

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

**------------------------------------
    EG0010099 = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 58 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
*-------------------------------------------------------------------------
    RETURN
*==============================================================
INITIATE:
    CUST.NAME        = ''
    CUST.NAME1       = ''
    LC.NU            = ''
    OLD.LC.NU        = ''
    ISSU.BANK.NU     = ''
    ISSU.BANK.NAME   = ''
    BEN.CUS.NU       = ''
    CONF.INST        = ''
    LC.CUR           = ''
    LC.AMT           = ''
    LIAB.AMT         = ''
    DEV.EXP.DAT      = ''
    TD1 = TODAY

    RETURN
*===============================================================
CALLDB:
    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    FN.DRW = 'FBNK.DRAWINGS' ; F.DRW = '' ; R.DRW = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF (FN.CU,F.CU)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DRW,F.DRW)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*========================================================================

    CLR.AMT = 0

*    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND EXPIRY.DATE GT ": TODAY :" AND CO.CODE EQ ":COMP :" BY OLD.LC.NUMBER"
*    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND EXPIRY.DATE GT ": TODAY :" BY @ID "

    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND EXPIRY.DATE GE ": TODAY :" BY OLD.LC.NUMBER "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    ENT.NO = ''
    LOOP

        REMOVE LC.ID  FROM KEY.LIST SETTING POS
    WHILE LC.ID:POS
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)

*      CUST.NAME = ''
        ISSU.BNK.NO = R.LC<TF.LC.ISSUING.BANK.NO>

*  CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,ISSU.BNK.NO,LOCAL.REF)
*  CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
**TEXT = "CU= " : ISSU.BNK.NO ; CALL REM
        CALL F.READ(FN.CU,ISSU.BNK.NO,R.CU,F.CU,ERR)
*  TEXT = CUST.NAME ; CALL REM

        CUST.NAME        = R.CU<EB.CUS.SHORT.NAME>
**TEXT = "NAME= " : CUST.NAME ; CALL REM
        LC.NU            = LC.ID[1,12]
        OLD.LC.NU        = R.LC<TF.LC.OLD.LC.NUMBER>
        ISSU.BANK.NO     = R.LC<TF.LC.ISSUING.BANK>
        ISSU.BANK.NAME   = CUST.NAME
*****************UPDATED BY RIHAM 16/08/2012*****************
        BEN.CUS1       = R.LC<TF.LC.BENEFICIARY.CUSTNO>
        CALL F.READ(FN.CU,BEN.CUS1,R.CU1,F.CU,ERR)
        CUST.NAME11 = R.CU1<EB.CUS.SHORT.NAME>

        BEN.CUS2       = R.LC<TF.LC.BENEFICIARY,1>

        CALL F.READ(FN.CU,BEN.CUS2,R.CU2,F.CU,ERR)
        CUST.NAME2 = R.CU2<EB.CUS.SHORT.NAME>

        IF BEN.CUS1 EQ '' THEN
            BEN.CUS.NU = BEN.CUS2
            CUST.NAME1 = CUST.NAME2
        END ELSE
            BEN.CUS.NU = BEN.CUS1
            CUST.NAME1 = CUST.NAME11
        END


**TEXT = CUST.NAME1 ; CALL REM
        CONF.INST        = R.LC<TF.LC.CONFIRM.INST>
        LC.CUR           = R.LC<TF.LC.LC.CURRENCY>
        LC.AMT           = R.LC<TF.LC.LC.AMOUNT>
        LIAB.AMT         = R.LC<TF.LC.LIABILITY.AMT>
        EXP.DAT          = R.LC<TF.LC.EXPIRY.DATE>
        ADV.DAT          = R.LC<TF.LC.ADVICE.EXPIRY.DATE>



*    PRINT   :"*": CONF.INST :"*": OLD.LC.NU :"*": ISSU.BANK.NU :"*": LC.CUR :"*": LC.AMT :"*": LIAB.AMT

        Y.RET.DATA <-1>  = CUST.NAME :"*": LC.NU :"*": BEN.CUS.NU :"*": LC.CUR :"*": LC.AMT :"*": LIAB.AMT :"*": EXP.DAT :"*": OLD.LC.NU :"*": CUST.NAME1 :"*": CONF.INST :"*": ISSU.BNK.NO :"*": ADV.DAT
    REPEAT

***-------------------------------------------------------
    R.LC = ''
    CLR.AMT = 0
*    T.SEL = "SELECT FBNK.DRAWINGS WITH LC.CREDIT.TYPE LIKE LE... AND MATURITY.REVIEW GT ": TODAY :" AND CO.CODE EQ ":COMP
    T.SEL = "SELECT FBNK.DRAWINGS WITH LC.CREDIT.TYPE LIKE LE... AND MATURITY.REVIEW GE ": TODAY :" BY REFRENCE "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    ENT.NO = ''
    LOOP
*    TEXT = "SAIZO"; CALL REM
        REMOVE DRW.ID FROM KEY.LIST SETTING POS
    WHILE DRW.ID:POS
        LC.ID = DRW.ID[1,12]
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)

        EXP.DAT      = R.LC<TF.LC.EXPIRY.DATE>

        IF EXP.DAT LT TODAY THEN

* CALL F.READ(FN.DRW,DRW.ID,R.DRW,F.DRW,ERR)
* TEXT = "SAIZO1"; CALL REM
            R.LC.ISSU.BNK.1 = R.LC<TF.LC.ISSUING.BANK.NO>
            CALL F.READ(FN.CU,R.LC.ISSU.BNK.1,R.CU,F.CU,ERR)
*    TEXT = CUST.NAME ; CALL REM
            CUST.NAME.1        = R.CU<EB.CUS.SHORT.NAME>
*TEXT = "CU.NO=  ": R.LC.ISSU.BNK.1 ; CALL REM
*TEXT = CUST.NAME.1 ; CALL REM
            LC.NU.1            = LC.ID[1,12]
            OLD.LC.NU.1        = R.LC<TF.LC.OLD.LC.NUMBER>
            ISSU.BANK.NU.1     = R.LC<TF.LC.ISSUING.BANK>
            ISSU.BANK.NAME.1   = CUST.NAME
*********************UPDATED BY RIHAM 16/8/2012******************
            BEN.CUS3       = R.LC<TF.LC.BENEFICIARY.CUSTNO>

            CALL F.READ(FN.CU,BEN.CUS3,R.CU3,F.CU,ERR)
            CUST.NAME3 = R.CU3<EB.CUS.SHORT.NAME>
            BEN.CUS4       = R.LC<TF.LC.BENEFICIARY,1>
            CALL F.READ(FN.CU,BEN.CUS4,R.CU4,F.CU,ERR)
            CUST.NAME4 = R.CU4<EB.CUS.SHORT.NAME>

            IF BEN.CUS3 EQ '' THEN
                BEN.CUS.NU.1 = BEN.CUS4
                CUST.NAME.2 =  CUST.NAME4
            END ELSE
                BEN.CUS.NU.1 = BEN.CUS3
                CUST.NAME.2 = CUST.NAME3
            END



            CONF.INST.1        = R.LC<TF.LC.CONFIRM.INST>
            LC.CUR.1           = R.LC<TF.LC.LC.CURRENCY>
            LC.AMT.1           = R.LC<TF.LC.LC.AMOUNT>
            LIAB.AMT.1         = R.LC<TF.LC.LIABILITY.AMT>
            EXP.DAT.1          = R.LC<TF.LC.EXPIRY.DATE>
            ADV.DAT.1          = R.LC<TF.LC.ADVICE.EXPIRY.DATE>




*          TEXT = "SAIZO"; CALL REM
*   Y.RET.DATA <-1>  = CUST.NAME.1 :"*": LC.NU.1 :"*": BEN.CUS.NU.1 :"*": LC.CUR.1 :"*": LC.AMT.1 :"*": LIAB.AMT.1 :"*": EXP.DAT.1 :"*": OLD.LC.NU.1 :"*":CUST.NAME.1 :"*": CONF.INST.1 :"*": ISSU.BANK.NU.1 :"*": ADV.DAT.1
            Y.RET.DATA <-1>  = CUST.NAME.1 :"*": LC.NU.1 :"*": BEN.CUS.NU.1 :"*": LC.CUR.1 :"*": LC.AMT.1 :"*": LIAB.AMT.1 :"*": EXP.DAT.1 :"*": OLD.LC.NU.1 :"*": CUST.NAME.2 :"*": CONF.INST.1 :"*": R.LC.ISSU.BNK.1 :"*": ADV.DAT.1


        END

    REPEAT
***-------------------------------------------------------
    RETURN
END
