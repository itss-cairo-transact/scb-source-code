* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.BR.TRNS.CCY(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    FN.STMT='FBNK.STMT.ENTRY' ;R.STMT='';F.STMT=''
    CALL OPF(FN.STMT,F.STMT)


    T.SEL = "SELECT FBNK.STMT.ENTRY WITH (TRANSACTION.CODE EQ 94 OR TRANSACTION.CODE EQ 44 OR TRANSACTION.CODE EQ 236 OR TRANSACTION.CODE EQ 552) AND TRANS.REFERENCE LIKE FT... AND CURRENCY NE EGP AND BOOKING.DATE EQ ":TODAY:" AND COMPANY.CODE EQ ":COMP:" BY TRANS.REFERENCE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**************************************************
    IF SELECTED THEN
        ENQ.LP = 0
        Z = 0
        FOR ENQ.LP = 1 TO SELECTED
            CALL F.READ(FN.STMT,KEY.LIST<ENQ.LP>, R.STMT, F.STMT,E)
*Line [ 63 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            CALL DBR('FUNDS.TRANSFER':@FM:FT.ORDERING.BANK,R.STMT<AC.STE.TRANS.REFERENCE>,BR.NO)
*Line [ 65 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,BR.NO,LOCAL)
            TYPE = LOCAL<1,BRLR.BILL.CHQ.STA>

            IF TYPE EQ 1 OR TYPE EQ 5 OR TYPE EQ 7 OR TYPE EQ 8 THEN
                Z = Z+1
                ENQ<2,Z> = '@ID'
                ENQ<3,Z> = 'EQ'
                ENQ<4,Z> = KEY.LIST<Z>
            END
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO TRANSACTION ISSUED"
*        ENQ<2> = '@ID'
*       ENQ<3> = 'EQ'
*     ENQ<4> = 'DUUMY'
    END
    RETURN
END
