* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 8/9/2011*********************************
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.STMT.ENT.SEL(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

*-----------------------------------------------------------------------*
    TD1 = ''

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = "" ; USR = ""

    FN.STMT.ENT   = 'FBNK.STMT.ENTRY' ; F.STMT.ENT = '' ; ERR.SEL = ''
    CALL OPF(FN.STMT.ENT,F.STMT.ENT)

    YTEXT = "Enter the USER : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    USR = "...":COMI:"..."
   T.SEL = "SELECT FBNK.STMT.ENTRY WITH INPUTTER LIKE " :USR :" OR AUTHORISER LIKE " : USR :"  BY BOOKING.DATE BY TRANS.REFERENCE "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ER.MSG)

    TEXT = 'SELECTED=':SELECTED ; CALL REM
   FOR I = 1 TO SELECTED
**      FOR I = 1 TO 100
        ACCT.NO = '' ; BOOKING.DATE = '' ; AMT.LCY = '' ; AMT.FCY = '' ; OUR.REFERENCE = ''
        CURRENCY = '' ; NARRATIVE  = '' ; INP = '' ; AUTH = '' ; INP.NO = '' ; AUTH.NO = ''
        AUTH.NO = '' ; TRANS.REF = ''
        CALL F.READ( FN.STMT.ENT,KEY.LIST<I>, R.STMT.ENT, F.STMT.ENT, ERR.SEL)
        ACCT.NO       = R.STMT.ENT<AC.STE.ACCOUNT.NUMBER>
        BOOKING.DATE  = R.STMT.ENT<AC.STE.BOOKING.DATE>
        AMT.LCY       = R.STMT.ENT<AC.STE.AMOUNT.LCY>
        AMT.FCY       = R.STMT.ENT<AC.STE.AMOUNT.FCY>
        OUR.REFERENCE = R.STMT.ENT<AC.STE.OUR.REFERENCE>
        CURRENCY      = R.STMT.ENT<AC.STE.CURRENCY>
        NARRATIVE     = R.STMT.ENT<AC.STE.NARRATIVE>
        INP           = R.STMT.ENT<AC.STE.INPUTTER><1,1>
        INP.NO        = FIELD(INP, "_", 2)
        AUTH          = R.STMT.ENT<AC.STE.AUTHORISER>
        AUTH.NO       = FIELD(AUTH, "_", 2)
        TRANS.REF     = R.STMT.ENT<AC.STE.TRANS.REFERENCE>
        Y.RET.DATA<-1> = ACCT.NO:"*":BOOKING.DATE:"*":AMT.LCY:"*":AMT.FCY:"*":OUR.REFERENCE:"*":CURRENCY:"*":NARRATIVE:"*":INP.NO:"*":AUTH.NO:"*":TRANS.REF
    NEXT I
    RETURN
END
