* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>201</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE E.NOF.LC.BR(Y.RET.DATA)

*    PROGRAM E.NOF.LC.DR.A

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

**------------------------------------
 *   EG0010099 = ID.COMPANY
     COMP = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 56 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
*-------------------------------------------------------------------------
    RETURN
*==============================================================

INITIATE:
    CUST.NAME        = ''
    CUST.NAME1       = ''
    LC.NU            = ''
    OLD.LC.NU        = ''
    ISSU.BANK.NU     = ''
    ISSU.BANK.NAME   = ''
    BEN.CUS.NU       = ''
    CONF.INST        = ''
    LC.CUR           = ''
    LC.AMT           = ''
    LIAB.AMT         = ''
    DEV.EXP.DAT      = ''
    TD1 = TODAY

    RETURN

*===============================================================
CALLDB:

    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    FN.DRW = 'FBNK.DRAWINGS' ; F.DRW = '' ; R.DRW = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF (FN.CU,F.CU)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DRW,F.DRW)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

*========================================================================

    CLR.AMT = 0

    X.USD   = 0
    X.EGP   = 0
    X.EUR   = 0
    X.SAR   = 0
    TOT.USD = 0
    TOT.EGP = 0
    TOT.EUR = 0
    TOT.SAR = 0

    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND EXPIRY.DATE GT ": TODAY :" AND CO.CODE EQ ": COMP :" BY OLD.LC.NUMBER "

    HHH = 0
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    ENT.NO = ''
    LOOP

        REMOVE LC.ID  FROM KEY.LIST SETTING POS
    WHILE LC.ID:POS
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)

        ISSU.BNK.NO = R.LC<TF.LC.ISSUING.BANK.NO>

        CALL F.READ(FN.CU,ISSU.BNK.NO,R.CU,F.CU,ERR)

        CUST.NAME        = R.CU<EB.CUS.SHORT.NAME>
        LC.NU            = LC.ID[1,12]
        OLD.LC.NU        = R.LC<TF.LC.OLD.LC.NUMBER>
        ISSU.BANK.NO     = R.LC<TF.LC.ISSUING.BANK>
        ISSU.BANK.NAME   = CUST.NAME

        BEN.CUS1       = R.LC<TF.LC.BENEFICIARY.CUSTNO>
        CALL F.READ(FN.CU,BEN.CUS1,R.CU1,F.CU,ERR)
        CUST.NAME11 = R.CU1<EB.CUS.SHORT.NAME>

        BEN.CUS2       = R.LC<TF.LC.BENEFICIARY,1>

        CALL F.READ(FN.CU,BEN.CUS2,R.CU2,F.CU,ERR)
        CUST.NAME2 = R.CU2<EB.CUS.SHORT.NAME>

        IF BEN.CUS1 EQ '' THEN
            BEN.CUS.NU = BEN.CUS2
            CUST.NAME1 = CUST.NAME2
        END ELSE
            BEN.CUS.NU = BEN.CUS1
            CUST.NAME1 = CUST.NAME11
        END


**TEXT = CUST.NAME1 ; CALL REM
        CONF.INST        = R.LC<TF.LC.CONFIRM.INST>
        LC.CUR           = R.LC<TF.LC.LC.CURRENCY>
        LC.AMT           = R.LC<TF.LC.LC.AMOUNT>
        LIAB.AMT         = R.LC<TF.LC.LIABILITY.AMT>
        EXP.DAT          = R.LC<TF.LC.EXPIRY.DATE>
        ADV.DAT          = R.LC<TF.LC.ADVICE.EXPIRY.DATE>



*    PRINT   :"*": CONF.INST :"*": OLD.LC.NU :"*": ISSU.BANK.NU :"*": LC.CUR :"*": LC.AMT :"*": LIAB.AMT

        IF LC.CUR EQ 'USD' THEN
            TOT.USD  = TOT.USD + LC.AMT
            X.USD    = X.USD + 1

        END
        IF LC.CUR EQ 'EGP' THEN
            TOT.EGP  = TOT.EGP + LC.AMT
            X.EGP = X.EGP + 1
        END
        IF LC.CUR EQ 'EUR' THEN
            TOT.EUR  = TOT.EUR + LC.AMT
            X.EUR = X.EUR + 1
        END

        IF LC.CUR EQ 'SAR' THEN
            TOT.SAR  = TOT.SAR + LC.AMT
            X.SAR = X.SAR + 1
        END


        Y.RET.DATA <-1>  = CUST.NAME :"*": LC.NU :"*": BEN.CUS.NU :"*": LC.CUR :"*": LC.AMT :"*": LIAB.AMT :"*": EXP.DAT :"*": OLD.LC.NU :"*": CUST.NAME1 :"*": CONF.INST :"*": ISSU.BNK.NO :"*": ADV.DAT :"*": X.USD :"*": X.EGP :"*": X.EUR :"*": X.SAR :"*": TOT.USD :"*": TOT.EGP :"*": TOT.EUR :"*": TOT.SAR

    REPEAT

***-------------------------------------------------------
    RETURN
END
