* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.REV.ASS.BRANCH(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.REVAL.REPORT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    LOCATE "BRANCH" IN D.FIELDS<1> SETTING BRN.POS THEN
        COM.CODEE  = D.RANGE.AND.VALUE<BRN.POS>
    END

    FN.CUR   = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.REV  = 'FBNK.REVAL.REPORT' ; F.REV = '' ; R.REV = ''
    CALL OPF(FN.REV,F.REV)

    T.SEL  = " SELECT FBNK.CURRENCY WITH @ID NE EGP AND @ID NE NZD AND @ID NE ITL AND @ID NE INR BY @ID  "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    LOOP
        REMOVE CUR.ID FROM KEY.LIST SETTING POS
    WHILE CUR.ID:POS

        CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR1)

        CCY.AMT    = ""
        LAST.LCY   = ""
        TODAY.LCY  = ""
        DIFF       = ""
        RATEE      = ""

        T.SEL.1  = "SELECT FBNK.REVAL.REPORT WITH @ID LIKE ":COM.CODEE:"...":CUR.ID:"...AL"

        CALL EB.READLIST(T.SEL.1, KEY.LIST.1, "", SELECTED.1, ASD)

        LOOP
            REMOVE REV.ID FROM KEY.LIST.1  SETTING POS11
        WHILE REV.ID:POS11

            CALL F.READ(FN.REV,REV.ID,R.REV,F.REV,ERR11)

            CCY.AMT   += R.REV<1>
            LAST.LCY  += R.REV<2>
            TODAY.LCY += R.REV<3>
            DIFF      += R.REV<4>
            RATEE      = R.REV<6>

        REPEAT

        IF  CCY.AMT > 0 THEN
            DESC  = "SHORT"
        END
        IF  CCY.AMT < 0 THEN
            DESC = 'LONG '

        END

        IF  DIFF > 0 THEN
            CR.DR  = "CR"
        END
        IF  DIFF < 0 THEN
            CR.DR = 'DR'
        END

        Y.RET.DATA<-1> =  CUR.ID:"*":DESC:"*":CCY.AMT:"*":LAST.LCY:"*":TODAY.LCY:"*":DIFF:"*":CR.DR:"*":RATEE:"*":COM.CODEE

    REPEAT

    RETURN

END
