* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE EOD.LG.CHARG.UPDATE.1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

**********MODIFIED BY ABEER  ON 29-9-2016
  **  EXECUTE 'COMO ON LG.CHARG'
*************
    IDDD="EG0010001"

    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,IDDD,L.W.DAY)
    TODAY = L.W.DAY
**    FN.TRNS  = 'FBNK.ACCT.ENT.TODAY' ; F.TRNS = '' ; R.TRNS = ''
    FN.TRNS  = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    SHIFT ='' ; ROUND =''
    DEFFUN SHIFT.DATE(NEW.DATE, SHIFT, ROUND)


    EOF = ''

    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LDD = '' ; R.LDD = ''
    CALL OPF(FN.LDD,F.LDD)


    FN.LDD.CHD = 'FBNK.LD.SCHEDULE.DEFINE' ; F.LDD.CHD = '' ; R.LDD.CHD = ''
    CALL OPF(FN.LDD.CHD,F.LDD.CHD)

    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)
*---------------
**    T.SEL  = "SELECT FBNK.ACCT.ENT.TODAY "
    T.SEL  = "SELECT FBNK.ACCT.ENT.LWORK.DAY "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    ENT.NO = ''

    LOOP
        REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
    WHILE ACCT.ID:POS
        CALL F.READ( FN.TRNS,ACCT.ID, R.TRNS,F.TRNS, ETEXT)
        LOOP

            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)
            CATT  = R.STMT<AC.STE.CRF.PROD.CAT>
            NATT  = R.STMT< AC.STE.NARRATIVE>
            TRNS  = R.STMT<AC.STE.TRANSACTION.CODE>
            INPTT = FIELD(R.STMT<AC.STE.INPUTTER>,'_',2)[1,3]
            BOK.D = R.STMT<AC.STE.BOOKING.DATE>
*--------------
**---------------------------HYTHAM  &  '' ---- 20120315
**IF ( CATT EQ 21096 AND NATT EQ 'CH' AND TRNS EQ '875' AND INPTT EQ 'INP' AND BOK.D EQ TODAY ) THEN
**---------------------------HYTHAM  & ABEER  ---- 20120315
************MODIFIED UPGRADE R15 ABEER
            IF ( CATT EQ 21096 AND TRNS EQ '875' AND INPTT EQ 'INP' AND BOK.D EQ TODAY ) THEN
************END OF MODIFICATION
                LD.ID = R.STMT<AC.STE.OUR.REFERENCE>
                CALL F.READ( FN.LDD,LD.ID, R.LDD,F.LDD, ETEXT11)
                NEW.DATE = R.LDD<LD.LOCAL.REF,LDLR.END.COMM.DATE>
*********MODIFIED UPGRADE R15 ABEER
                NEW.DATE.UP = SHIFT.DATE(NEW.DATE,'3M','UP')
                R.LDD<LD.LOCAL.REF,LDLR.END.COMM.DATE> = NEW.DATE.UP
***********END OF MODIFICATION
******************
                CALL F.WRITE(FN.LDD,LD.ID,R.LDD)

**    WRITE  R.LDD TO F.LDD , LD.ID ON ERROR
**       PRINT "CAN NOT WRITE RECORD":LD.ID:"TO" :FN.LDD
**  END
*******START OF MODIFICATION BY ABEER 29-9-2016
                PRINT NEW.DATE:"-DATE BEFORE-":LD.ID:"DATE AFTER-":R.LDD<LD.LOCAL.REF,LDLR.END.COMM.DATE>
***********END OF MODIFICATION
            END
        REPEAT
    REPEAT
**********MODIFIED BY ABEER  ON 29-9-2016
   ** EXECUTE 'COMO OFF LG.CHARG'
*************
    RETURN
END
