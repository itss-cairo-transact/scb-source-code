* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.INV.CUS.POS(RET.DATA)
*    PROGRAM E.NOF.INV.CUS.POS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    EXECUTE 'COMO ON E.NOF.INV.CUS.POS'
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS , F.CUS)
    CUS.ID = '' ; FUND.CODE = ''
    className = 't24java1.T24Java1'
    methodName = '$CallCusPosition'
    IC.UNIT = '' ; IC.LAST.PRICE = '' ; IC.AMT = '' ; CUS.EXIST = ''
    LOCATE "CUS.ID" IN D.FIELDS<1> SETTING YSDT.POS THEN
        CUS.ID = D.RANGE.AND.VALUE<YSDT.POS>
    END
    LOCATE "FUND.CODE" IN D.FIELDS<1> SETTING YSDT.POS THEN
        FUND.CODE = D.RANGE.AND.VALUE<YSDT.POS>
    END 
    CRT 'D.FIELDS : ':D.FIELDS
    CRT 'CUS.ID : ':CUS.ID
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ERR.CUS)
    IF NOT(ERR.CUS) AND R.CUS<EB.CUS.POSTING.RESTRICT> LT 89 THEN CUS.EXIST = 'A00' ELSE CUS.EXIST = ',D012'
    IF CUS.EXIST = 'A00' THEN
        CRT 'CUS EXIST : ':CUS.EXIST
        PARAM = CUS.ID:'::':FUND.CODE
*        PARAM = "13100983::1"
        CRT 'PARAM : ':PARAM
        CALLJ className, methodName, PARAM SETTING RET ON ERROR GOTO errHandler
*        PARAM = '{"UNIT":"12","LAST.PRICE":"15.556","AMT":"1560"}'
        CHANGE '"' TO '' IN RET
        CRT 'RETURN RET : ':RET
        RET = RET[2,LEN(RET)-2]
        IC.UNIT = FIELD(RET,',',2)
        IC.UNIT = FIELD(IC.UNIT,':',2)
        CRT 'IC UNIT : ':IC.UNIT

        IC.LAST.PRICE = FIELD(RET,',',1)
        IC.LAST.PRICE = FIELD(IC.LAST.PRICE,':',2)
        CRT 'IC LAST PRICE : ':IC.LAST.PRICE

        IC.AMT = FIELD(RET,',',3)
        IC.AMT = FIELD(IC.AMT,':',2)
        CRT 'IC AMT : ':IC.AMT

        RET.DATA = CUS.EXIST:'*':CUS.ID:'*':IC.UNIT:'*':IC.LAST.PRICE:'*':IC.AMT:'*':FUND.CODE
    END ELSE
        RET.DATA = CUS.EXIST
    END
    CRT 'RET DATA : ':RET.DATA
    EXECUTE 'COMO OFF E.NOF.INV.CUS.POS'
    RETURN

errHandler:
    err = SYSTEM(0)
    CRT 'ERROR : ':err
    IF err = 2 THEN

        CRT "Cannot find the JVM.dll !"

        RETURN

    END

    IF err = 3 THEN

        CRT "Class " : className : "doesn't exist !"

        RETURN

    END

    IF err = 5 THEN

        CRT "Method " : methodName : "doesn't exist !"

        RETURN

    END
    RETURN

END
