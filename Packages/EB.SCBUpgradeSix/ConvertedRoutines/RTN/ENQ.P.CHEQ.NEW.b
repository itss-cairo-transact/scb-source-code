* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
****NESSREEN AHMED 8/4/2012*********************************
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.P.CHEQ.NEW(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ

    LOCATE "CHQ.NO" IN ENQ<2,1> SETTING CHQ.POS THEN
        CHQ.ID   = ENQ<4,CHQ.POS>
    END

    LOCATE "ACCOUNT.NO" IN ENQ<2,1> SETTING AC.NO THEN
        ACCT.NO   = ENQ<4,AC.NO>
    END
    XX= '' ; SS = ''

    FN.CHQ ='F.SCB.P.CHEQ' ; R.CHQ = '' ;  F.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    FOR NN = 1 TO 100
        PAY.CHQ.ID= CHQ.ID:'.': NN
        CALL F.READ( FN.CHQ,PAY.CHQ.ID, R.CHQ, F.CHQ,ETEXT)
        IF NOT(ETEXT) THEN
            ACCOUNT.NO = R.CHQ<P.CHEQ.ACCOUNT.NO>
            IF ACCOUNT.NO = ACCT.NO THEN
                ENQ<2,1> = '@ID'
                ENQ<3,1> = 'EQ'
                ENQ<4,1> = PAY.CHQ.ID
                NN = 100
                XX = 1
            END
        END
    NEXT NN
    IF XX = '' THEN
        CHQ.ID = TRIM(CHQ.ID, "0", "L")
        FOR SS = 1 TO 100
            PAY.CHQ.ID= CHQ.ID:'.':SS
            CALL F.READ( FN.CHQ,PAY.CHQ.ID, R.CHQ, F.CHQ,ETEXT)
            IF NOT(ETEXT) THEN
                ACCOUNT.NO = R.CHQ<P.CHEQ.ACCOUNT.NO>
                IF ACCOUNT.NO = ACCT.NO THEN
                    ENQ<2,1> = '@ID'
                    ENQ<3,1> = 'EQ'
                    ENQ<4,1> = PAY.CHQ.ID
                    SS = 100
                    YY = 1
                END
            END
        NEXT SS
* END ELSE
    END
    IF (  YY EQ '' AND XX EQ '' ) THEN
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = "DUMM"

    END

    RETURN
END
