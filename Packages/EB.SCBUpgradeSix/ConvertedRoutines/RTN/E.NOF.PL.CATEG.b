* @ValidationCode : MjotMTA4NjM4Mjk2OkNwMTI1MjoxNjQ1MDQyNDc4MDkyOkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Feb 2022 22:14:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
********************************************NI7OOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>1365</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.PL.CATEG(Y.RET.DATA)

*    PROGRAM  E.NOF.PL.CATEG

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*------------------------------------------------------------------------

    FN.LD="FBNK.STMT.ENTRY"
    F.LD = ""
    CALL OPF(FN.LD,F.LD)

    FN.CC="F.COMPANY"
    F.CC = ""
    CALL OPF(FN.CC,F.CC)

    YF.CATEG.MONTH = "F.CATEG.MONTH"
    F.CATEG.MONTH = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)

    FN.CATEG.ENTRY = "FBNK.CATEG.ENTRY"
    F.CATEG.ENTRY= ""
    CALL OPF(FN.CATEG.ENTRY,F.CATEG.ENTRY)

    YF.CATEGORY = "F.CATEGORY"
    F.CATEGORY = ""
    CALL OPF(YF.CATEGORY,F.CATEGORY)

    F.CATEG.ENT.TODAY = ""
    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)

    F.CATEG.ENT.FWD = ''
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD'
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    F.LDD  = ''
    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    CALL OPF(FN.LDD,F.LDD)

    F.CUR  = ''
    FN.CUR = 'FBNK.CURRENCY'
    CALL OPF(FN.CUR,F.CUR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    F.ITSS.DATES = 'F.DATES'
    FN.F.ITSS.DATES = ''
    CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
    CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
    DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD = TODAY


    AMOUNT  = ''
    AMT.LCY = ''
    AMT.FCY = ''
    CUR.ID  = ''
    TOT.EGP = ''
    TOT.USD = ''
    TOT.GBP = ''
    TOT.AUD = ''
    TOT.AED = ''
    TOT.CHF = ''
    TOT.JPY = ''
    TOT.DKK = ''
    TOT.SEK = ''
    TOT.NOK = ''
    TOT.KED = ''
    TOT.SAR = ''
*------------------------------------------------------------------------
    T.SEL4 = "SELECT F.COMPANY WITH @ID NE 'EG0010088' AND @ID NE 'EG0010099'"
** T.SEL4 = "SELECT F.COMPANY "
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTEDC,ER.MSG4)
    T.SEL2 = "SELECT F.CATEGORY WITH SYSTEM.IND EQ 'PL' AND (@ID UNLIKE 53... OR @ID UNLIKE 54...)"
** T.SEL2 = "SELECT F.CATEGORY WITH @ID EQ 50010 "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    FOR J = 1 TO SELECTED2
        YY    = 1
        FOR C = 1 TO SELECTEDC
            ID.COMPANY      = KEY.LIST4<C>
*       C$ID.COMPANY    = KEY.LIST4<C>
            PL.NO = KEY.LIST2<J>
            TDF = TODAY

            FROM.DATE = TD[1,6]:'01'
            TDT = TODAY
            CALL CDT("",TDT,'-1C')
            TO.DATE   = TDT
            Y.CAT.NO  = PL.NO
            CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
            LOOP
                REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
            WHILE YENTRY.KEY:YTYPE
**                READ YR.CATEG.ENTRY FROM F.CATEG.ENTRY, YENTRY.KEY ELSE YR.CATEG.ENTRY = ''
                CALL F.READ(FN.CATEG.ENTRY, YENTRY.KEY , YR.CATEG.ENTRY , F.CATEG.ENTRY ,EE1)
                CALL F.READ(FN.CATEG.ENTRY,YENTRY.KEY,R.CAT,F.CATEG.ENTRY,E2)
                CUR.ID    = R.CAT<AC.CAT.CURRENCY>
                AMT.LCY   = R.CAT<AC.CAT.AMOUNT.LCY>
                AMT.FCY   = R.CAT<AC.CAT.AMOUNT.FCY>
                CO.CODE   = R.CAT<AC.CAT.COMPANY.CODE>

                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'EGP' THEN
                    TOT.EGP +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'USD' THEN
                    TOT.USD +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'GBP' THEN
                    TOT.GBP +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'EUR' THEN
                    TOT.EUR +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'CHF' THEN
                    TOT.CHF +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'SAR' THEN
                    TOT.SAR +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'AUD' THEN
                    TOT.AUD +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'SEK' THEN
                    TOT.SEK +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'NOK' THEN
                    TOT.NOK +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'DKK' THEN
                    TOT.DKK +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'JPY' THEN
                    TOT.JPY +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'KWD' THEN
                    TOT.KWD +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'CAD' THEN
                    TOT.CAD +=AMT.LCY
                END
                IF YR.CATEG.ENTRY<AC.CAT.CURRENCY> EQ 'AED' THEN
                    TOT.AED +=AMT.LCY
                END
*Line [ 201 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
                F.ITSS.CURRENCY = 'F.CURRENCY'
                FN.F.ITSS.CURRENCY = ''
                CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                CUR=R.ITSS.CURRENCY<@FM:EB.CUR.CCY.NAME>

            REPEAT
        NEXT C
    NEXT J


    T.SEL3 = "SELECT FBNK.CURRENCY"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    FOR K = 1 TO SELECTED3
**    Y.RET.DATA<-1> = KEY.LIST3<K>:"*":KEY.LIST3<K>:"*":'TOT.':KEY.LIST3<K>:"*":KEY.LIST3<K>:"*":TOT.EGP
        NNN = KEY.LIST3<K>
        HHH = 'TOT.':NNN
        IF KEY.LIST3<K> EQ 'EGP' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.EGP
        END
        IF KEY.LIST3<K> EQ 'USD' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.USD
        END
        IF KEY.LIST3<K> EQ 'GBP' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.GBP
        END
        IF KEY.LIST3<K> EQ 'CHF' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.CHF
        END
        IF KEY.LIST3<K> EQ 'SAR' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.SAR
        END
        IF KEY.LIST3<K> EQ 'AED' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.AED
        END
        IF KEY.LIST3<K> EQ 'AUD' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.AUD
        END
        IF KEY.LIST3<K> EQ 'JPY' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.JPY
        END
        IF KEY.LIST3<K> EQ 'KWD' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.KWD
        END
        IF KEY.LIST3<K> EQ 'CAD' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.CAD
        END
        IF KEY.LIST3<K> EQ 'DKK' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.DKK
        END
        IF KEY.LIST3<K> EQ 'SEK' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.SEK
        END
        IF KEY.LIST3<K> EQ 'EUR' THEN
            Y.RET.DATA<-1> = KEY.LIST3<K>:"*":TOT.EUR
        END


    NEXT K
*===== =========================================================
RETURN
END
