* @ValidationCode : MjoxNjY1MjAxMjc4OkNwMTI1MjoxNjQwODU3MTEzMjQ0OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:38:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***************************MAHMOUD 2/9/2014******************************
*-----------------------------------------------------------------------------
* <Rating>432</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.SCCD.DY.REP(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
 
    GOSUB CALLDB
    GOSUB PROCESS
RETURN
*========================================
INITIATE:
*-------
    KK = 0
***********************
**    TTMM = R.DATES(EB.DAT.LAST.PERIOD.END)
    DYNO = 0
    TDD = TODAY
    NXT.I.DATE = TDD
    CALL ADD.MONTHS(NXT.I.DATE,'3')
    CALL CDT('',NXT.I.DATE,'+1C')
***********************
    ARR.DATA = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

RETURN
******************************************************
CLEAR.VAR:
*----------

RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
RETURN
*========================================
PROCESS:
*-------
***************************//LD//*****************************************
    SEL.CMD  = "SELECT ":FN.LD:" WITH CATEGORY IN(":SCCD.GRP:") AND STATUS EQ 'CUR' "
    SEL.CMD := " AND VALUE.DATE EQ ":TDD
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.2)
    CRT SELECTED
*Line [ 88 ] initialised K2 - ITSS - R21 Upgrade - 2021-12-23
    K2 = 0
    LOOP
        REMOVE LD.ID FROM KEY.LIST SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        REC.COM  = R.LD<LD.CO.CODE>
        REC.CUS  = R.LD<LD.CUSTOMER.ID>
        REC.CAT  = R.LD<LD.CATEGORY>
        REC.CUR  = R.LD<LD.CURRENCY>
        REC.AMT  = R.LD<LD.AMOUNT>
        REC.VAL  = R.LD<LD.VALUE.DATE>
        REC.FIN  = R.LD<LD.FIN.MAT.DATE>
        REC.LCL  = R.LD<LD.LOCAL.REF>
        REC.FLG  = REC.LCL<1,LDLR.CHK.FLG>
        REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
        IF REC.QTY EQ '' THEN REC.QTY = 1
        REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
 
        FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
        IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
            REC.TYP = "EGP-1000-3M-60M-SUEZ"
        END
        IF REC.TYP EQ "EGP-1000-3M-60M-SUEZ" THEN
            REC.INT  = REC.AMT * 12 / 100 / 4
        END ELSE
            REC.INT  = 0
        END
*    IF REC.TYP EQ "EGP-10-60M-SUEZ.BNK" THEN
*        REC.TYP = "EGP-10-60M-SUEZ"
*    END
*    IF REC.TYP EQ "EGP-100-60M-SUEZ.BNK" THEN
*        REC.TYP = "EGP-100-60M-SUEZ"
*    END
*    IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
*        REC.TYP = "EGP-1000-3M-60M-SUEZ"
*    END
        CALL F.READ(FN.CU,REC.CUS,R.CU,F.CU,ERR.CU)
        CU.LCL = R.CU<EB.CUS.LOCAL.REF>
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
        IF REC.FLG EQ '' OR REC.FLG NE '�����' THEN
            IF REC.SEC EQ '4650' THEN
                REC.FLG = '�����'
            END ELSE
                REC.FLG = '���� ��������'
            END
        END
        REC.BNK = CU.LCL<1,CULR.CU.BANK>
        IF REC.BNK NE '0017' AND REC.BNK NE '' THEN
            REC.BNK = '9000'
        END ELSE
            REC.BNK = '0017'
        END
*===========================================
        GOSUB FILL.ARR
*===========================================
    REPEAT
    ARR.REC = SORT(ARR.REC)
    MXX1 = DCOUNT(ARR.REC,@FM)
    FOR AAA1 = 1 TO MXX1
        GOSUB RET.REC
    NEXT AAA1
RETURN
*****************************************************************
FILL.ARR:
*---------
    KK++
    ARR.REC<KK> = REC.FLG:"*":REC.BNK:"*":REC.TYP:"*":REC.VAL:"*":REC.AMT:"*":REC.QTY:"*":REC.CAT:"*":REC.CUS:"*":REC.COM:"*":REC.INT:"*":NXT.I.DATE:"*":REC.FIN
RETURN
*****************************************************************
RET.REC:
********
    Y.RET.DATA<-1>= ARR.REC<AAA1>
RETURN
*************************************************
END
