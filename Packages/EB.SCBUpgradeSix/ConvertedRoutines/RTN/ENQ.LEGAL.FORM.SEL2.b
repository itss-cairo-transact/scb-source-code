* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.LEGAL.FORM.SEL2(ENQ)

*TO MAKE A DROP DOWN LIST FOR THE SECTOR FIELD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    IF APPLICATION = 'CUSTOMER' THEN

        CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.INDUSTRY,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,IND1)
        LOCATE R.NEW(EB.CUS.INDUSTRY) IN IND1<1,1> SETTING YY THEN Y = "YYYY"
        CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.LEGAL.FORM,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,LEG1)
        IF LEG1<1,YY> THEN
*Line [ 47 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(LEG1<1,YY>, @SM)
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = LEG1<1,YY,I>

            NEXT I
        END ELSE
            ENQ.ERROR = "Legal.Form Not Found"
        END

    END
    RETURN
END
