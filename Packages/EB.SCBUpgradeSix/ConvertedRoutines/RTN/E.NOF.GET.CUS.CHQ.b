* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>708</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  E.NOF.GET.CUS.CHQ(CHQ.ISSUE.RR)

***Mahmoud Elhawary******13/5/2010****************************
**************************************************************

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    Y.OPEN.BAL= ''
    KK = 0
    XX = SPACE(80)
    STE.BAL = ''
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ER.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = '' ; ER.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)
    FN.CHQ = 'F.SCB.FT.DR.CHQ' ; F.CHQ = '' ; R.CHQ = '' ; ER.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO" IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,CUS.COMP)
    IF CUS.COMP EQ COMP THEN
        CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
        LOOP
            REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
        WHILE ACCT.ID:POS.CUS.ACC
            CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
            AC.CAT = R.ACCT<AC.CATEGORY>
*****************************
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS
            WHILE STE.ID:POS
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.CUR = R.STE<AC.STE.CURRENCY>
                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                STE.REF = R.STE<AC.STE.OUR.REFERENCE>
                IF STE.CUR NE 'EGP' THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                CALL DBR('TRANSACTION':@FM:1,STE.TXN,STE.TRN)
*******************
                CALL F.READ(FN.FT,STE.REF,R.FT,F.FT,ER.FT)
                IF NOT(R.FT) THEN
                    CALL F.READ(FN.FT.H,STE.REF:';1',R.FT,F.FT.H,ER.FT.H)
                END
*******************
                CHQ.NO = R.FT<FT.CHEQUE.NUMBER>
                TR.TYP = R.FT<FT.TRANSACTION.TYPE>
                CR.ACCT= R.FT<FT.CREDIT.ACCT.NO>
                DR.CHQ.ID = CR.ACCT:".":CHQ.NO
                CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CR.ACCT,CR.CAT)
                CALL F.READ(FN.CHQ,DR.CHQ.ID,R.CHQ,F.CHQ,ER.CHQ)
                CHQ.STAT = R.CHQ<DR.CHQ.CHEQ.STATUS>
                CHQ.CUR  = R.CHQ<DR.CHQ.CURRENCY>
                CHQ.AMT  = R.CHQ<DR.CHQ.AMOUNT>
                PAY.DATE = R.CHQ<DR.CHQ.PAY.DATE>
                STE.BAL += STE.AMT
                IF CR.CAT EQ 16151 AND STE.TXN EQ 201 THEN
                    GOSUB RET.DATA
                END
            REPEAT
        REPEAT
    END
    RETURN
**************************************
RET.DATA:
*--------
    CHQ.ISSUE.RR<-1> = COMP:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":STE.DAT:"*":STE.TXN:"*":STE.REF:"*":CHQ.AMT:"*":CHQ.NO:"*":CHQ.CUR:"*":CHQ.STAT:"*":PAY.DATE
    RETURN
**************************************
END
