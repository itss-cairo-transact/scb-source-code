* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
****NESSREEN AHMED 28/03/20187******************
*-----------------------------------------------------------------------------
* <Rating>775</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.EQV.USD.100000.C(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.TOT.TRANS.FCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
    KEY.LIST.M = "" ; SELECTED.M = "" ;  ER.MSG.M = ""

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CBE.TRANS = 'F.SCB.CBE.TOT.TRANS.FCY' ; F.CBE.TRANS = '' ; R.CBE.TRANS = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.CBE.TRANS,F.CBE.TRANS)

    TOT.AMT.DB = 0 ; TOT.AMT.CR = 0 ; TOT.AMT.SELL = 0 ; TOT.AMT.BUY = 0  ; TOT.AMT.N = 0
    TOT.LT.5T.B = 0 ; TOT.LT.5T.C = 0 ; TOT.LT.5T.S= 0 ; TOT.LT.5T.D = 0
    TOT.AMT.LT.5T = 0
    TOT.CN.CR = 0 ; TOT.CN.BUY =  0 ; TOT.CN.DB = 0 ; TOT.CN.SELL = 0

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    N.SEL = "SELECT F.SCB.CBE.TOT.TRANS.FCY WITH (TRANS.DATE GE ":ST.DATE :" AND TRANS.DATE LE " :EN.DATE :" ) AND CUST.NO NE '' AND CUST.TYPE EQ 'C' BY CUST.NO BY TRANS.TYPE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.CBE.TRANS,KEY.LIST.N<1>, R.CBE.TRANS,F.TT, ERR.CBE.TRANS)

    CUST<1>         = R.CBE.TRANS<CBET.CUST.NO>
    CURR<1>         = R.CBE.TRANS<CBET.TRANS.CURR>
    AMT.FCY<1>      = R.CBE.TRANS<CBET.AMT.FCY>
    AMT.LCY<1>      = R.CBE.TRANS<CBET.AMT.LCY>
    AUTH.DAT<1>     = R.CBE.TRANS<CBET.TRANS.DATE>
    TT.DATE = AUTH.DAT<1>
    TR.TYPE<1>      = R.CBE.TRANS<CBET.TRANS.TYPE>
    AMT.EQV.USD<1>  = R.CBE.TRANS<CBET.EQV.USD>

    BEGIN CASE
    CASE TR.TYPE<1> EQ 'CREDIT'
        TOT.AMT.CR = TOT.AMT.CR + AMT.EQV.USD<1>
        TOT.CN.CR = TOT.CN.CR + 1
     CASE TR.TYPE<1> EQ 'BUY'
        TOT.AMT.BUY = TOT.AMT.BUY + AMT.EQV.USD<1>
        TOT.CN.BUY = TOT.CN.BUY + 1
    CASE TR.TYPE<1> EQ 'DEBIT'
        TOT.AMT.DB = TOT.AMT.DB + AMT.EQV.USD<1>
        TOT.CN.DB = TOT.CN.DB + 1
    CASE TR.TYPE<1> EQ 'SELL'
        TOT.AMT.SELL = TOT.AMT.SELL + AMT.EQV.USD<1>
        TOT.CN.SELL = TOT.CN.SELL +1
    END CASE
    TOT.AMT.N = TOT.AMT.CR + TOT.AMT.BUY + TOT.AMT.DB + TOT.AMT.SELL
*   CUST.NTR =  CUST.NTR + 1
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.CBE.TRANS,KEY.LIST.N<I>, R.CBE.TRANS,F.TT, ERR.CBE.TRANS)
        CUST<I>         = R.CBE.TRANS<CBET.CUST.NO>
        CURR<I>         = R.CBE.TRANS<CBET.TRANS.CURR>
        AMT.FCY<I>      = R.CBE.TRANS<CBET.AMT.FCY>
        AMT.LCY<I>      = R.CBE.TRANS<CBET.AMT.LCY>
        AUTH.DAT<I>     = R.CBE.TRANS<CBET.TRANS.DATE>
        TT.DATE = AUTH.DAT<I>
        TR.TYPE<I>      = R.CBE.TRANS<CBET.TRANS.TYPE>
        AMT.EQV.USD<I>  = R.CBE.TRANS<CBET.EQV.USD>

        IF CUST<I> # CUST<I-1> THEN
            CALL F.READ(FN.CUSTOMER, CUST<I-1>, R.CUSTOMER, F.CUSTOMER, E1)
            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
            REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
            CBE.NO = LOCAL.REF<1,CULR.CBE.NO>
            NSN    = LOCAL.REF<1,CULR.NSN.NO>
            IF  TOT.AMT.N GE 100000 THEN
              **CALL EB.ROUND.AMOUNT ('EGP',COMISS.AMT,'',"2")
                TOT.AMT.CR.F = TOT.AMT.CR / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.CR.F,'',"2")
                TOT.AMT.BUY.F =TOT.AMT.BUY / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.BUY.F,'',"2")
                TOT.AMT.DB.F = TOT.AMT.DB / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.DB.F,'',"2")
                TOT.AMT.N.F = TOT.AMT.N.F / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.N.F,'',"2")
                Y.RET.DATA<-1> = "/":"*":ST.DATE
                Y.RET.DATA<-1> = CUST<I-1> :"*":CBE.NO:"*":NSN:"*":TOT.AMT.CR.F:"*":TOT.AMT.BUY.F:"*":TOT.AMT.DB.F:"*":TOT.AMT.SELL.F:"*":TOT.AMT.N.F
                Y.RET.DATA<-1> = "/":"*":EN.DATE
                TOT.AMT.N = 0  ; TOT.AMT.CR = 0 ; TOT.AMT.DB = 0 ; TOT.AMT.BUY = 0 ; TOT.AMT.SELL = 0
                TOT.AMT.N.F = 0  ; TOT.AMT.CR.F = 0 ; TOT.AMT.DB.F = 0 ; TOT.AMT.BUY.F = 0 ; TOT.AMT.SELL.F = 0
                TOT.CN.CR = 0 ; TOT.CN.BUY =  0 ; TOT.CN.DB = 0 ; TOT.CN.SELL = 0
            END ELSE
                TOT.LT.5T.B = TOT.LT.5T.B + TOT.AMT.BUY
                TOT.LT.5T.C = TOT.LT.5T.C + TOT.AMT.CR
                TOT.LT.5T.S = TOT.LT.5T.S + TOT.AMT.SELL
                TOT.LT.5T.D = TOT.LT.5T.D + TOT.AMT.DB
                CUST.NTR =  CUST.NTR + 1
                TOT.AMT.LT.5T = TOT.AMT.LT.5T + TOT.AMT.N

                TOT.CUST.LT.B = TOT.CUST.LT.B + TOT.CN.BUY
                TOT.CUST.LT.C = TOT.CUST.LT.C + TOT.CN.CR
                TOT.CUST.LT.S = TOT.CUST.LT.S + TOT.CN.SELL
                TOT.CUST.LT.D = TOT.CUST.LT.D + TOT.CN.DB

                TOT.AMT.N = 0  ; TOT.AMT.CR = 0 ; TOT.AMT.DB = 0 ; TOT.AMT.BUY = 0 ; TOT.AMT.SELL = 0
                TOT.CN.CR = 0 ; TOT.CN.BUY =  0 ; TOT.CN.DB = 0 ; TOT.CN.SELL = 0
            END
            BEGIN CASE
            CASE TR.TYPE<I> EQ 'CREDIT'
                TOT.AMT.CR = TOT.AMT.CR + AMT.EQV.USD<I>
                TOT.CN.CR = TOT.CN.CR + 1
            CASE TR.TYPE<I> EQ 'BUY'
                TOT.AMT.BUY = TOT.AMT.BUY + AMT.EQV.USD<I>
                TOT.CN.BUY = TOT.CN.BUY + 1
            CASE TR.TYPE<I> EQ 'DEBIT'
                TOT.AMT.DB = TOT.AMT.DB + AMT.EQV.USD<I>
                TOT.CN.DB = TOT.CN.DB + 1
            CASE TR.TYPE<I> EQ 'SELL'
                TOT.AMT.SELL = TOT.AMT.SELL + AMT.EQV.USD<I>
                TOT.CN.SELL = TOT.CN.SELL +1
            END CASE
            TOT.AMT.N = TOT.AMT.CR + TOT.AMT.BUY + TOT.AMT.DB + TOT.AMT.SELL
*****SAME CUSTOMER*************
        END ELSE
            BEGIN CASE
            CASE TR.TYPE<I> EQ 'CREDIT'
                TOT.AMT.CR = TOT.AMT.CR + AMT.EQV.USD<I>
                TOT.CN.CR = TOT.CN.CR + 1
            CASE TR.TYPE<I> EQ 'BUY'
                TOT.AMT.BUY = TOT.AMT.BUY + AMT.EQV.USD<I>
                TOT.CN.BUY = TOT.CN.BUY + 1
            CASE TR.TYPE<I> EQ 'DEBIT'
                TOT.AMT.DB = TOT.AMT.DB + AMT.EQV.USD<I>
                 TOT.CN.DB = TOT.CN.DB + 1
            CASE TR.TYPE<I> EQ 'SELL'
                TOT.AMT.SELL = TOT.AMT.SELL + AMT.EQV.USD<I>
                TOT.CN.SELL = TOT.CN.SELL +1
            END CASE
            TOT.AMT.N = TOT.AMT.CR + TOT.AMT.BUY + TOT.AMT.DB + TOT.AMT.SELL
*****END OF SAME CUSTOMER******
        END         ;**END OF SAME CUSTOMER**
        IF I = SELECTED.N THEN
            IF TOT.AMT.N GE 100000 THEN
                TEXT = 'LAST RECORD' ; CALL REM
                CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NSN = LOCAL.REF<1,CULR.NSN.NO>
                CBE.NO    = LOCAL.REF<1,CULR.CBE.NO>

                TOT.AMT.CR.F = TOT.AMT.CR / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.CR.F,'',"2")
                TOT.AMT.BUY.F =TOT.AMT.BUY / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.BUY.F,'',"2")
                TOT.AMT.DB.F = TOT.AMT.DB / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.DB.F,'',"2")
                TOT.AMT.N.F = TOT.AMT.N.F / 1000
                CALL EB.ROUND.AMOUNT ('EGP',TOT.AMT.N.F,'',"2")
    **          Y.RET.DATA<-1> = CUST<I> :"*":CBE.NO:"*":NSN:"*":TOT.AMT.CR:"*":TOT.AMT.BUY:"*":TOT.AMT.DB:"*":TOT.AMT.SELL:"*":TOT.AMT.N
                Y.RET.DATA<-1> = CUST<I> :"*":CBE.NO:"*":NSN:"*":TOT.AMT.CR.F:"*":TOT.AMT.BUY.F:"*":TOT.AMT.DB.F:"*":TOT.AMT.SELL.F:"*":TOT.AMT.N.F
                TOT.AMT.N = 0  ; TOT.AMT.CR = 0 ; TOT.AMT.DB = 0 ; TOT.AMT.BUY = 0 ; TOT.AMT.SELL = 0
                TOT.AMT.N.F = 0  ; TOT.AMT.CR.F = 0 ; TOT.AMT.DB.F = 0 ; TOT.AMT.BUY.F = 0 ; TOT.AMT.SELL.F = 0
                TOT.AMT.N = 0 ; CUST = '' ; CURR = '' ; NSN = '' ; NW.SECTOR = ''
            END ELSE
                TOT.LT.5T.B = TOT.LT.5T.B + TOT.AMT.BUY
                TOT.LT.5T.C = TOT.LT.5T.C + TOT.AMT.CR
                TOT.LT.5T.S = TOT.LT.5T.S + TOT.AMT.SELL
                TOT.LT.5T.D = TOT.LT.5T.D + TOT.AMT.DB
                TOT.AMT.LT.5T = TOT.AMT.LT.5T + TOT.AMT.N
               CUST.NTR =  CUST.NTR + 1
               TOT.CUST.LT.B = TOT.CUST.LT.B + TOT.CN.BUY
               TOT.CUST.LT.C = TOT.CUST.LT.C + TOT.CN.CR
               TOT.CUST.LT.S = TOT.CUST.LT.S + TOT.CN.SELL
               TOT.CUST.LT.D = TOT.CUST.LT.D + TOT.CN.DB

            END
            TOT.LT.5T.B.F = TOT.LT.5T.B / 1000
            CALL EB.ROUND.AMOUNT ('EGP', TOT.LT.5T.B.F,'',"2")
            TOT.LT.5T.C.F = TOT.LT.5T.C / 1000
            CALL EB.ROUND.AMOUNT ('EGP', TOT.LT.5T.C.F,'',"2")
            TOT.LT.5T.S.F = TOT.LT.5T.S / 1000
            CALL EB.ROUND.AMOUNT ('EGP', TOT.LT.5T.S.F,'',"2")
            TOT.LT.5T.D.F = TOT.LT.5T.D / 1000
            CALL EB.ROUND.AMOUNT ('EGP', TOT.LT.5T.D.F,'',"2")
            TOT.AMT.LT.5T.F = TOT.AMT.LT.5T / 1000
            CALL EB.ROUND.AMOUNT ('EGP', TOT.AMT.LT.5T.F ,'',"2")

       **   Y.RET.DATA<-1> = "����� ��� �� 50 ��� �����":"*":"-":"*":"-":"*":TOT.LT.5T.C:"*":TOT.LT.5T.B:"*":TOT.LT.5T.D:"*":TOT.LT.5T.S:"*":TOT.AMT.LT.5T
            Y.RET.DATA<-1> = "����� ���100��� �����":"*":"-":"*":"-":"*":TOT.LT.5T.C.F:"*":TOT.LT.5T.B.F:"*":TOT.LT.5T.D.F:"*":TOT.LT.5T.S.F:"*":TOT.AMT.LT.5T.F
       **   Y.RET.DATA<-1> = "��� ������� ����� �� 50 ��� ���":"*":"-":"*":"-":"*":TOT.LT.5T.C:"*":TOT.LT.5T.B:"*":TOT.LT.5T.D:"*":TOT.LT.5T.S:"*":TOT.AMT.LT.5T
            Y.RET.DATA<-1> = " ��� ������� ��� �� 100 ��� �����":"*":"-":"*":"-":"*":TOT.CUST.LT.C:"*":TOT.CUST.LT.B:"*":TOT.CUST.LT.D:"*":TOT.CUST.LT.S:"*":CUST.NTR
       **   Y.RET.DATA<-1> = "��� ������� ����� �� 50 ��� ����� =":"*":"-":"*":"-":
       **   Y.RET.DATA<-1> = "     ����� ����":TOT.CUST.LT.S:"����� ����":TOT.CUST.LT.C
       **   Y.RET.DATA<-1> = "     ����� ������" :TOT.CUST.LT.B:"��� ����":TOT.CUST.LT.D CUST.NTR
       **   Y.RET.DATA<-1> = "������":CUST.NTR
        END
    NEXT I
    TEXT = 'END OF REPORT' ; CALL REM
    RETURN
END
