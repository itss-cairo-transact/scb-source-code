* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
****NESSREEN AHMED 30/4/2012*********************************
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.CURR.HIS(Y.RET.DATA)
*************************************************************
*************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    FN.CURR.H   = 'F.CURRENCY$HIS' ; F.CURR.H = ''
    CALL OPF(FN.CURR.H,F.CURR.H)

    FND = ''

    N.SEL = "SELECT FBNK.CURRENCY WITH (@ID NE 'NZD' AND @ID NE 'EGP' AND @ID NE 'ITL') BY NUMERIC.CCY.CODE "
    CALL EB.READLIST(N.SEL , KEY.LIST.N,'',SELECTED.N,ER.MSG.N)
    FOR NN = 1 TO SELECTED.N
        CURR = KEY.LIST.N<NN>
        CUR.ID = CURR:'...'
        LOCATE "DATEE.TIME" IN D.FIELDS<1> SETTING DATE.POS THEN
            DATTIM  = D.RANGE.AND.VALUE<DATE.POS>
        END
        T.SEL = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE ": CUR.ID : " AND DATE.TIME LIKE  ": DATTIM
        KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
        CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.CURR.H,KEY.LIST<I>,R.CURR.H,F.CURR.H,ECAA)
****############### ��������� ################********
                CRMARK.C = '1'
*Line [ 61 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CRMARK.C IN R.CURR.H<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
                BUY.RAT.1  = R.CURR.H<EB.CUR.BUY.RATE,POS>
                SELL.RAT.1 = R.CURR.H<EB.CUR.SELL.RATE,POS>
****############### �������� #################********
                CASH.C = '10'
*Line [ 67 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CASH.C IN R.CURR.H<EB.CUR.CURRENCY.MARKET,1> SETTING POS.C ELSE NULL
                BUY.RAT.10  = R.CURR.H<EB.CUR.BUY.RATE,POS.C>
                SELL.RAT.10 = R.CURR.H<EB.CUR.SELL.RATE,POS.C>
****
                DATETIME = R.CURR.H<EB.CUR.DATE.TIME>
                Y.RET.DATA<-1> = CURR:"*":BUY.RAT.1:"*":SELL.RAT.1:"*":BUY.RAT.10:"*":SELL.RAT.10:"*":DATETIME
            NEXT I
        END ELSE    ;***ELSE OF IF SELECTED
            DATTIM1 = DATTIM[1,6]
            MDATE = "20":DATTIM1
            LOOP WHILE NOT(FND)
                CC = 1
                CW = '-':CC:'W'
**CALL CDT("",MDATE,'-1W')
                CALL CDT("",MDATE, CW)
                DATTIM2 = MDATE[3,6]:"..."
                SS.SEL = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE ": CUR.ID : " AND DATE.TIME LIKE  ":DATTIM2
                KEY.LIST.S = '' ; SELECTED.S = '' ; ER.MSG.S = ''
                CALL EB.READLIST(SS.SEL , KEY.LIST.S,'',SELECTED.S,ER.MSG.S)
                IF SELECTED.S THEN
                    FND = 1
                END ELSE
                    CC = CC + 1
                END
            REPEAT
            FOR SS = 1 TO SELECTED.S
                CALL F.READ(FN.CURR.H,KEY.LIST.S<SS>,R.CURR.H,F.CURR.H,ECAA)
****############### ��������� ################********
                CRMARK.C = '1'
*Line [ 97 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CRMARK.C IN R.CURR.H<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
                BUY.RAT.1  = R.CURR.H<EB.CUR.BUY.RATE,POS>
                SELL.RAT.1 = R.CURR.H<EB.CUR.SELL.RATE,POS>
****############### �������� #################********
                CASH.C = '10'
*Line [ 103 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CASH.C IN R.CURR.H<EB.CUR.CURRENCY.MARKET,1> SETTING POS.C ELSE NULL
                BUY.RAT.10  = R.CURR.H<EB.CUR.BUY.RATE,POS.C>
                SELL.RAT.10 = R.CURR.H<EB.CUR.SELL.RATE,POS.C>
****
                DATETIME = R.CURR.H<EB.CUR.DATE.TIME>
                Y.RET.DATA<-1> = CURR:"*":BUY.RAT.1:"*":SELL.RAT.1:"*":BUY.RAT.10:"*":SELL.RAT.10:"*":DATETIME
****
            NEXT SS
***END     ;*END OF IF SELECTED.S
        END         ;*END OF IF SELECTED
    NEXT NN
*************************************************************
    RETURN
END
