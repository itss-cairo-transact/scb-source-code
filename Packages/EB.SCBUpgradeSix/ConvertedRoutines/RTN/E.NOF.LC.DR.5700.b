* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-28</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE E.NOF.LC.DR.5700(Y.RET.DATA)

*    PROGRAM E.NOF.LC.DR.5700

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FILE.CONTROL
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PGM.FILE


*-------------------------------------------------------------------------

    GOSUB INITIATE
*Line [ 59 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

*-------------------------------------------------------------------------

    RETURN

*=========================================================================

INITIATE:

    CUST.NAME        = ''
    LC.NU            = ''
    OLD.LC.NU        = ''
    ISSU.BANK.NAME   = ''
    BEN.CUS.NU       = ''
    LC.CUR           = ''
    LIAB.AMT         = ''
    EXP.DATE         = ''


    FN.LC      = 'FBNK.LETTER.OF.CREDIT' ; F.LC       = '' ; R.LC       = ''
    FN.DRW     = 'FBNK.DRAWINGS'         ; F.DRW      = '' ; R.DRW      = ''
    FN.CU      = 'FBNK.CUSTOMER'         ; F.CU       = '' ; R.CU       = ''
    FN.LIM.TXN = "FBNK.LIMIT.TXNS"       ; F.LIM.TXN  = '' ; R.LIM.TXN  = ''

    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DRW,F.DRW)
    CALL OPF(FN.LIM.TXN,F.LIM.TXN)

    KEY.LIST     ="" ; SELECTED      = "" ;  ER.MSG  = ""
    KEY.LIST.TXN ="" ; SELECTED.TXN  = "" ;  ER.MSG1 = ""
    RETURN

*===============================================================
CALLDB:


*========================================================================

    T.SEL.TXN = "SELECT FBNK.LIMIT.TXNS WITH @ID LIKE 994...0005740... "
    CALL EB.READLIST(T.SEL.TXN, KEY.LIST.TXN, "", SELECTED.TXN, E.TXN)
*DEBUG
    ENT.NO = ''
    LOOP

        REMOVE TXN.ID FROM KEY.LIST.TXN SETTING POS
    WHILE TXN.ID:POS
        CALL F.READ(FN.LIM.TXN,TXN.ID,R.LIM.TXN,F.LIM.TXN,ERR)

        LOOP
            REMOVE TXN.REC FROM R.LIM.TXN SETTING POS.REC
        WHILE TXN.REC:POS.REC
*      CALL F.READ(FN.LIM.TXN,TXN.ID,R.LIM.TXN,F.LIM.TXN,ERR)

            TF.ID   = FIELD(TXN.REC ,'\',1)
            AMT     = FIELD(TXN.REC,'\',3)
            EXP.DAT = FIELD(TXN.REC,'\',4)


**-----------------LC ---------------

            IF LEN(TF.ID) EQ 12 THEN

                CALL F.READ(FN.LC,TF.ID,R.LC,F.LC,ERR)

                ISSU.BNK.NO = R.LC<TF.LC.ISSUING.BANK.NO>

                CALL F.READ(FN.CU,ISSU.BNK.NO,R.CU,F.CU,ERR)

                CUST.NAME        = R.CU<EB.CUS.SHORT.NAME>
                LC.NU            = TF.ID
                OLD.LC.NU        = R.LC<TF.LC.OLD.LC.NUMBER>
                ISSU.BANK.NAME   = CUST.NAME
                LC.CUR           = R.LC<TF.LC.LC.CURRENCY>
                LIAB.AMT         = AMT
                ISSU.DATE        = R.LC<TF.LC.ISSUE.DATE>
                EXP.DATE         = EXP.DAT
                AMT.USD          = ""
                REMB.BNK.NO       = R.LC<TF.LC.RISK.PARTY>

                Y.RET.DATA <-1>  = ISSU.BNK.NO :"*": CUST.NAME :"*": LC.NU :"*":OLD.LC.NU:"*": LC.CUR :"*": LIAB.AMT :"*": ISSU.DATE :"*": EXP.DATE:"*":AMT.USD:"*":REMB.BNK.NO

*               PRINT ISSU.BNK.NO :"*": CUST.NAME :"*": LC.NU :"*":OLD.LC.NU:"*": LC.CUR :"*": LIAB.AMT :"*": ISSU.DATE :"*": EXP.DATE:"*":AMT.USD

            END

**---------------  LC ---------------

**---------------  DR ---------------

            IF LEN(TF.ID) EQ 14 THEN
                LC.ID = TF.ID[1,12]

                CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)
                ISSU.BNK.NO = R.LC<TF.LC.ISSUING.BANK.NO>

                CALL F.READ(FN.CU,ISSU.BNK.NO,R.CU,F.CU,ERR)

                CUST.NAME        = R.CU<EB.CUS.SHORT.NAME>
                LC.NU            = TF.ID
                OLD.LC.NU        = R.LC<TF.LC.OLD.LC.NUMBER>
                ISSU.BANK.NAME   = CUST.NAME
                LC.CUR           = R.LC<TF.LC.LC.CURRENCY>
                LIAB.AMT         = AMT
                ISSU.DATE        = R.LC<TF.LC.ISSUE.DATE>
                EXP.DATE         = EXP.DAT

                Y.RET.DATA <-1>  = ISSU.BNK.NO :"*": CUST.NAME :"*": LC.NU :"*":OLD.LC.NU:"*": LC.CUR :"*": LIAB.AMT :"*": ISSU.DATE :"*": EXP.DATE:"*":AMT.USD

**              PRINT ISSU.BNK.NO :"*": CUST.NAME :"*": LC.NU :"*":OLD.LC.NU:"*": LC.CUR :"*": LIAB.AMT :"*": ISSU.DATE :"*": EXP.DATE:"*":AMT.USD

            END

**--------------- DR --------------------------
        REPEAT
    REPEAT

***-------------------------------------------------------
    RETURN
END
