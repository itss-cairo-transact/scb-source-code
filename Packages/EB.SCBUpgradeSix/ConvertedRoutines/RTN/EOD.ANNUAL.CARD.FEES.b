* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*---- CREATE BY NESSMA
    SUBROUTINE EOD.ANNUAL.CARD.FEES(DB.ACC,DB.AMT,PAN)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CARD.FEES.ALL
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*----------------------------------------------
    CALL DBR('CARD.ISSUE':@FM:CARD.IS.CO.CODE,"ATMC.":PAN,PAN.COMPANY)
    GOSUB INIT
    GOSUB PROCESS
    RETURN
*----------------------------------------------
INIT:
*----
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "AC.LOCKED.EVENTS"
    OFS.OPTIONS   = "CARD.FEES2"
    COMP          = PAN.COMPANY
    COMP.CODE     = COMP[8,2]

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    COMMA         = ","
    COMP          = ""
    OFS.USER.INFO = ""
    ACCT.NO       = ""
    LOCK.AMT      = ""
    RETURN
*----------------------------------------------
OFS.RECORD:
*----------
    OFS.MESSAGE.DATA  = ""
    OFS.MESSAGE.DATA  = "ACCOUNT.NUMBER=":ACCT.NO:COMMA
    OFS.MESSAGE.DATA := "DESCRIPTION=":APP.ID:COMMA
    OFS.MESSAGE.DATA := "FROM.DATE=":TODAY:COMMA
    OFS.MESSAGE.DATA := "LOCKED.AMOUNT=":LOCK.AMT

    OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA
    OFS.REC := COMMA:OFS.MESSAGE.DATA

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,NEW.FILE ON ERROR  TEXT = " ERROR ";CALL REM

    RETURN
*----------------------------------------------
PROCESS:
*--------
    NEW.FILE      = "ACLK.ANNUAL.FEES.":PAN
    APP.ID        = PAN
    ACCT.NO       = DB.ACC
    LOCK.AMT      = DB.AMT
    COMP          = PAN.COMPANY
    OFS.USER.INFO = "INPUTT":COMP[8,2]:"//":COMP

    IF LOCK.AMT GT 0 THEN
        GOSUB OFS.RECORD
    END
    RETURN
*------------------------------------------------------------------
END
