* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.SCB.BL.BATCH(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY
**************************************************
*UNMATURED1 = TODAY[7,2] + 7
*UNMATURED  = TODAY[1,6]:UNMATURED1

*SAM = TODAY
*CALL CDT('', SAM, '7W')
*UNMATURED = SAM
    FN.NAME = R.ENQ<2>
    SAM1    = TODAY

    COMP       = C$ID.COMPANY
    USR.DEP    = "1700":COMP[8,2]
*** TEXT = USR.DEP ; CALL REM
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
*** TEXT = "LOC : " : LOCT  ; CALL REM
    DAL.TODAY = TODAY
    IF LOCT EQ '1'  THEN
        CALL CDT('',DAL.TODAY,"+1W")
 *        CALL CDT('',DAL.TODAY,"+2W")

    END
    IF LOCT NE '1'  THEN
       CALL CDT('',DAL.TODAY,"+2W")
*        CALL CDT('',DAL.TODAY,"+3W")
    END

***  TEXT = "DAL.TODAY : " : DAL.TODAY  ; CALL REM

*  SAM1 = "20080109"
*    CALL CDT('',SAM1,"+1W")
*    T.SEL  = "SELECT FBNK.":FN.NAME:" WITH MATURITY.EXT EQ ": SAM1 :" AND BILL.CHQ.STA EQ ": 1 :" AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 )"
* T.SEL = "SSELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ ":SAM1:" AND BILL.CHQ.STA EQ 1 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND CURRENCY EQ EGP AND BANK UNLIKE 99... AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>
*MSABRY 27/1/2009
**** T.SEL = "SSELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ ":SAM1:" AND BILL.CHQ.STA EQ 1 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND CURRENCY EQ EGP AND BANK UNLIKE 99... AND CO.CODE EQ ":COMP
    TEXT = DAL.TODAY ; CALL REM

*    T.SEL = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT LE ":DAL.TODAY:" AND ( BILL.CHQ.STA EQ 1 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 )) AND CURRENCY EQ EGP AND BANK UNLIKE 99... AND CO.CODE EQ ":COMP
    T.SEL = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT LE ":DAL.TODAY:" AND ( BILL.CHQ.STA EQ 1 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 )) AND CURRENCY EQ EGP AND IN.OUT.BILL EQ 'YES' AND CO.CODE EQ ":COMP

*********T.SEL = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ '20090712' AND ( BILL.CHQ.STA EQ 1 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 )) AND CURRENCY EQ EGP AND BANK UNLIKE 99... AND CO.CODE EQ ":COMP
***    T.SEL = "SSELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ ":SAM1:" AND BILL.CHQ.STA EQ 1 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BANK UNLIKE 99... AND CO.CODE EQ ":COMP

    ENQ.LP  = '' ; OPER.VAL = ''

*Line [ 88 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR ENQ.LP = 1 TO DCOUNT(ENQ<2>,@VM)
        IF ENQ<3,ENQ.LP> = 'LK' THEN
            OPER.VAL = 'LIKE'
        END
        IF ENQ<3,ENQ.LP> = 'UL' THEN
            OPER.VAL = 'UNLIKE'
        END
        IF ENQ<3,ENQ.LP> # 'LK' AND  ENQ<3,ENQ.LP> # 'UL' THEN
            OPER.VAL = ENQ<3,ENQ.LP>
        END
        T.SEL := ' AND ':ENQ<2,ENQ.LP>:' ':OPER.VAL:' ':ENQ<4,ENQ.LP>

    NEXT ENQ.LP
*    T.SEL := ' AND VERSION.NAME EQ ':VER.NAME
    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
    IF SELECTED >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ<2> = '@ID'
        ENQ<3> = 'EQ'
        ENQ<4> = 'DUUMY'
    END

    RETURN
END
