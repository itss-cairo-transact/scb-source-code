* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*---- COPY BY NESSMA FROM "E.NOF.MKSA.AGNBY.CHQ.DET" ---
    SUBROUTINE E.NOF.MKSA.AGNBY.CHQ.DET.2(Y.RET.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*-------------------------------------------------
    CID.NO = R.USER<EB.USE.DEPARTMENT.CODE>

    Path = "/life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING/FCY.CLEARING.":TODAY

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
*---------------------
    EOF = ''
    I = 1

    FN.DR = 'FBNK.BILL.REGISTER' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,"|",2)
            RETURN.REASON     = FIELD(Line,"|",7)
            DEPT.CODE         = FIELD(Line,"|",1)
            AMOUNT            = FIELD(Line,",",5)

            CALL F.READ(FN.DR,OUR.ID,R.DR,F.DR,E1)
            BANK.BR = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
            BANK    = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            CHQ.NO  = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>

            IF LEN(DEPT.CODE) EQ 1 THEN
                DEPT.CODE = "0" :  DEPT.CODE
            END

            IF OUR.ID AND CID.NO EQ DEPT.CODE THEN
                IF RETURN.REASON THEN
*---- ��������������------------------------------
                    Y.RET.DATA<-1> = DEPT.CODE : '*' : BANK : '*' : BANK.BR : '*' : CHQ.NO : '*' : AMOUNT
                END
            END

        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath
*---------------------------------------------------------
END
