* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>849</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  E.NOF.INT.MANUAL.TXN(TXN.ID)

***Mahmoud Elhawary******13/5/2010****************************
* Internal account manual transaction                        *
**************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 42 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    Y.OPEN.BAL= ''
    KK = 0
    XX = SPACE(80)
    STE.BAL = ''
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    RETURN
****************************************
CALLDB:
*-------
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    RETURN
****************************************
PROCESS:
*-------
    LOCATE "ACCOUNT.NO"  IN D.FIELDS<1> SETTING YACC.POS THEN ACCT.ID   = D.RANGE.AND.VALUE<YACC.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.ID,ACC.CUS)
    IF ACC.CUS EQ '' THEN
        CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
        AC.CAT = R.ACCT<AC.CATEGORY>
*****************************
        CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
        LOOP
            REMOVE STE.ID FROM ID.LIST SETTING POS
        WHILE STE.ID:POS
            CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
            STE.INP = R.STE<AC.STE.INPUTTER>
            STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
            IF STE.REF[1,2] EQ 'DC' THEN
                FINDSTR 'OFS' IN STE.INP SETTING POS.INP THEN GOTO NXT.REC ELSE
                    GOSUB RET.DATA
                END
NXT.REC:
            END
        REPEAT
    END
    RETURN
**************************************
RET.DATA:
*--------
    TXN.ID<-1> = STE.ID:"*":OPENING.BAL
    RETURN
**************************************
END
