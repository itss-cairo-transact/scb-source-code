* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
* PROGRAM EOD.CD.ISL.NEW.RATE
    SUBROUTINE EOD.CD.ISL.NEW.RATE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST

    FN.BI.DATE = 'FBNK.BASIC.INTEREST.DATE' ; F.BI.DATE = '';R.BI.DATE=''
    CALL OPF(FN.BI.DATE, F.BI.DATE)
***Updated by Nessreen Ahmed 6/2/2019***************
***BI.DATE.ID = '87EGP'
   BI.DATE.ID = '90EGP'
***End of Update 6/2/2019***************************

    CALL F.READ(FN.BI.DATE,BI.DATE.ID,R.BI.DATE,F.BI.DATE,DATE.ERR)

    IF R.BI.DATE NE '' THEN
        BI.DATE = R.BI.DATE<EB.BID.EFFECTIVE.DATE,1>
*        TEXT = 'BI.DATE = ':  BI.DATE ; CALL REM;
    END
***Updated by Nessreen Ahmed 6/2/2019***************
*** BI.ID = '87EGP':BI.DATE
    BI.ID = '90EGP':BI.DATE
***End of Update 6/2/2019***************************
*    TEXT = 'BI.ID = ':  BI.ID ; CALL REM;

    FN.BI.INT = 'FBNK.BASIC.INTEREST' ; F.BI.INT = '';R.BI.INT=''
    CALL OPF( FN.BI.INT, F.BI.INT)
    CALL F.READ(FN.BI.INT,BI.ID,R.BI.INT,F.BI.INT,BI.ERR)
    NEW.INT = R.BI.INT<EB.BIN.INTEREST.RATE> 
*    TEXT = 'NEW INT = ':  NEW.INT ; CALL REM;

    FN.ACC.BAL = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.ACC.BAL = '';R.ACC.BAL=''
    CALL OPF(FN.ACC.BAL,F.ACC.BAL)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21036 AND STATUS NE LIQ"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    DIM ZZZ(SELECTED)
    FOR I = 1 TO SELECTED
        LD.ACC.BAL = KEY.LIST<I>:"00"
       * TEXT = 'LD.ACC.BAL =':LD.ACC.BAL ; CALL REM;
        CALL F.READ(FN.ACC.BAL,LD.ACC.BAL,R.ACC.BAL,F.ACC.BAL,E.ACC)
        NEW.INT.V.DATE = R.ACC.BAL<LD27.START.PERIOD.INT>
        *TEXT = 'NEW.INT.V.DATE =':NEW.INT.V.DATE ; CALL REM;
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E2)
        LD.COCODE = R.LD<LD.CO.CODE>
****************************************************
        FN.OFS.SOURCE ="F.OFS.SOURCE"; F.OFS.SOURCE = ""
        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')

**      FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        F.OFS.IN          = 0
        OFS.REC           = ""
        OFS.OPERATION     = "LD.LOANS.AND.DEPOSITS"
        OFS.OPTIONS       = "CD.ISL"
        COMP              = R.LD<LD.CO.CODE>
        COM.CODE          = COMP[8,2]
        OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
        OFS.TRANS.ID      = KEY.LIST<I>
        OFS.MESSAGE.DATA  = ""

        DIR.NAME          = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        DAT               = TODAY
        NEW.FILE          = 'CD.ISL.PER':".":KEY.LIST<I>

        OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
            CLOSESEQ V.FILE.IN
            HUSH ON
            EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
            HUSH OFF
            PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
        END
        OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
            CREATE V.FILE.IN THEN
                PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
            END
            ELSE
                STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
            END
        END

        COMMA        =  ","
        OFS.MESSAGE.DATA  =  "NEW.INT.RATE=":NEW.INT:COMMA
        OFS.MESSAGE.DATA :=  "INT.RATE.V.DATE=":NEW.INT.V.DATE:COMMA

        ZZZ(I) = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
        PRINT KEY.LIST<I>:'*':NEW.INT:"*":NEW.INT.V.DATE
        WRITESEQ ZZZ(I) TO V.FILE.IN ELSE
            PRINT  'CAN NOT WRITE LINE ':ZZZ(I)
        END

    NEXT I

    RETURN
END
