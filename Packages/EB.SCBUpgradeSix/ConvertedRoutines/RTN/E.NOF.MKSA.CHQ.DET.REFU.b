* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE  E.NOF.MKSA.CHQ.DET.REFU(Y.RET.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

    CID = ID.COMPANY
    CID.NO = CID[8,2]

    TXT.ID = TODAY[1,4]:"-":TODAY[5,2]:"-":TODAY[7,2]:'-credit.new.txt'
    Path = "/hq/opce/bclr/user/dltx/":TXT.ID

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 1

    FN.DR = 'FBNK.BILL.REGISTER' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,",",3)
            RETURN.REASON     = FIELD(Line,",",13)
            RESULT.CODE       = FIELD(Line,",",1)
            DEPT.CODE         = FIELD(Line,",",8)
            AMOUNT            = FIELD(Line,",",11)

            CALL F.READ(FN.DR,OUR.ID,R.DR,F.DR,E1)

            BANK.BR = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
            BANK    = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            CHQ.NO  = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
            LIQ.ACC = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>


            IF LEN(DEPT.CODE) EQ 1 THEN
                DEPT.CODE = "0" :  DEPT.CODE
            END

            IF CID.NO EQ DEPT.CODE AND RESULT.CODE EQ "520" THEN
                Y.RET.DATA<-1> = DEPT.CODE : '*' : BANK : '*' : BANK.BR : '*' : CHQ.NO : '*' : AMOUNT : '*' :  LIQ.ACC : '*' :  RETURN.REASON
            END


        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath

END
