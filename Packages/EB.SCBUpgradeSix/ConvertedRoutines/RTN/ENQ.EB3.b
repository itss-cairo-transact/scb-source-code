* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***********************CREATED BY MAHMOUD MAGDY 2018/06/05*********
    SUBROUTINE ENQ.EB3(ENQ)
*    PROGRAM ENQ.EB3
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    FN.CUS = 'FBNK.CUSTOMER'
    F.CUS = ''
    R.CUS = ''
    UPDATE.DATE = ''
    QLTY.RATE = ''
    YEAR.DAYS = 365
    QLTY.DAYS = ''
    CALL OPF(FN.CUS,F.CUS)
    COMP = ID.COMPANY

    KK1 = 0

    YTEXT = "���� ����� ������ :"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE.1 = COMI
*    ST.DATE.1 = ''
    TEXT = "ST.DATE.1=":ST.DATE.1 ; CALL REM

    YTEXT = "���� ����� ������ :"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE.2 = COMI
*    ST.DATE.2 = ''
    TEXT = "ST.DATE.2=":ST.DATE.2 ; CALL REM


    EXECUTE "COMO ON ENQ.EB3"
    T.SEL = "SELECT ":FN.CUS
    T.SEL := " WITH (QLTY.RATE NE '' OR RISK.RATE NE '') AND  "
    T.SEL := " COMPANY.BOOK EQ ":COMP:" AND "
    T.SEL := " POSTING.RESTRICT LT 89 AND"
    T.SEL := " SCB.POSTING EQ 29 AND"
    T.SEL := " INTER.BNK.DATE GE '20140326' AND"
    T.SEL := " SECTOR NE 1100 AND SECTOR NE 1200 AND"
    T.SEL := " SECTOR NE 1300 AND SECTOR NE 1400"
*    T.SEL := " BY UPDATE.DATE BY @ID BY QLTY.RATE"
    T.SEL := " BY @ID"

    CRT T.SEL
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    CRT SELECTED : ' IS SELECTED'
*     CRT K.LIST
    IF SELECTED AND ST.DATE.1 NE '' AND ST.DATE.2 NE '' THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CUS,K.LIST<I>,R.CUS,F.CUS,E1)
            UPDATE.DATE = R.CUS<EB.CUS.LOCAL.REF,CULR.UPDATE.DATE>
            QLTY.RATE = R.CUS<EB.CUS.LOCAL.REF,CULR.QLTY.RATE>
            CRT ' UPDATE DATE : ': UPDATE.DATE
            CRT 'QLTY RATE ':QLTY.RATE

            IF QLTY.RATE NE '' AND UPDATE.DATE NE '' THEN

                IF QLTY.RATE EQ 1  OR QLTY.RATE EQ 2 THEN
                    QLTY.DAYS = 4 * YEAR.DAYS 
                END

                ELSE IF QLTY.RATE EQ 3 THEN
                    QLTY.DAYS = 2 * YEAR.DAYS 
                END

                QLTY.FMT = "+":QLTY.DAYS:"C"
                CRT 'QLTY FORMAT ':QLTY.FMT
                CALL CDT("",UPDATE.DATE,QLTY.FMT)

                CRT 'QLTY FORMAT ':QLTY.FMT
                CRT 'QLTY DAYS ':QLTY.DAYS

                CRT 'NEXT UPDATE DATE ':UPDATE.DATE

                IF UPDATE.DATE GE ST.DATE.1 AND UPDATE.DATE LE ST.DATE.2 THEN
                    KK1 += 1
                    ENQ<2,KK1> = "@ID"
                    ENQ<3,KK1> = "EQ"
                    ENQ<4,KK1> = K.LIST<I>
                    CRT K.LIST<I>
                END

            END

        NEXT I
    END
    IF KK1 EQ 0 THEN
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "DUMMY"
    END

    EXECUTE "COMO OFF ENQ.EB3"
    RETURN
END
