* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>450</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.TXT.ALL.NEXT(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.BR = "FBNK.BILL.REGISTER"
    F.BR = ""
    CALL OPF(FN.BR,F.BR)
    R.AMTTT=0
    A.AMTTT=0
    COMP1 = ''
**------------

    DAL.TODAY = TODAY
    DATX  = DAL.TODAY[1,8]
    DATY  = DAL.TODAY[1,8]
    DATZ  = DAL.TODAY[1,8]
    CALL CDT('EG',DATX,'+1W')
    CALL CDT('EG',DATY,'+2W')
    CALL CDT('EG',DATZ,'+3W')

    XAMT =0
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    YAMT = 0
    ZAMT = 0
    X=0
    Y=0
    Z=0

    T.SEL = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT GE ":DATX:" AND MATURITY.EXT LE ":DATZ:" BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BR,KEY.LIST<I>,R.BR,F.BR,ERR)
            COMP = R.BR<EB.BILL.REG.CO.CODE>

            IF I = 1 THEN COMP1 = COMP
            IF COMP1 NE COMP THEN
                Y.RET.DATA<-1> = COMP1:"*":XAMT:"*":X:"*":YAMT:"*":Y:"*":ZAMT:"*":Z
                XAMT = 0
                YAMT = 0
                ZAMT = 0
                X=0
                Y=0
                Z=0
            END

            EXT.DATE = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.MATURITY.EXT>
            AMT = R.BR<EB.BILL.REG.AMOUNT>

            IF EXT.DATE EQ DATX THEN
                XAMT = XAMT + AMT
                X= X+1
            END
            IF EXT.DATE EQ DATY THEN
                YAMT = YAMT + AMT
                Y= Y+1
            END
            IF EXT.DATE EQ DATZ THEN
                ZAMT = ZAMT + AMT
                Z= Z+1
            END

            COMP1 = COMP
        NEXT I
        IF I EQ SELECTED THEN
            Y.RET.DATA<-1> = COMP1:"*":XAMT:"*":X:"*":YAMT:"*":Y:"*":ZAMT:"*":Z
        END
    END

    RETURN
END
