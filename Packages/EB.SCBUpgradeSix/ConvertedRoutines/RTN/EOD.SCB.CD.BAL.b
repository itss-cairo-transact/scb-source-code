* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE

*-----------------------------------------------------------------------------
* <Rating>-15</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOD.SCB.CD.BAL

*   PROGRAM EOD.SCB.CD.BAL

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

*-----------------------------------------------------------------------*

    COMP   = ID.COMPANY
    BRN.NO = COMP[8,2]
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

*   FN.TRNS  = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.TRNS = '' ; R.TRNS = ''
    FN.TRNS  = 'FBNK.ACCT.ENT.TODAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)
    AMT.LCY = 0
*DEBUG
*   T.SEL  = "SELECT FBNK.ACCT.ENT.LWORK.DAY WITH @ID LIKE EGP16102000100...  AND CO.CODE EQ ":COMP
    T.SEL  = "SELECT FBNK.ACCT.ENT.TODAY WITH @ID EQ EGP16102000100":BRN.NO

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    ENT.NO = ''
    LOOP
        REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
    WHILE ACCT.ID:POS
        CALL F.READ(FN.TRNS,ACCT.ID,R.TRNS,F.TRNS,ERR)
        CLR.AMT = 0
        LOOP

            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)
            STMT.GL = R.STMT<AC.STE.CRF.PROD.CAT>
            PRINT R.STMT<AC.STE.AMOUNT.LCY> : "--" : R.STMT<AC.STE.OUR.REFERENCE> : "--" :  R.STMT<AC.STE.CRF.PROD.CAT>
            IF ( STMT.GL GE 21101 AND STMT.GL LE 21103 )  THEN
                AMT.LCY    = R.STMT<AC.STE.AMOUNT.LCY> + AMT.LCY
            END

        REPEAT
    REPEAT

**-----------------------------------------------------------------
*DEBUG

    ACT.ID   = "EGP16102000100":BRN.NO
    CALL F.READ(FN.ACC,ACCT.ID,R.ACC,F.ACC,E1)

    TRNS.CODE = '78'
    Y.AMT     = AMT.LCY * -1
    CURR      = R.ACC<AC.CURRENCY>
    DATEE     = TODAY
    Y.ACCT    = ACT.ID

    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)

    GOSUB AC.STMT.ENTRY

    TRNS.CODE = '79'
    CURR      = R.ACC<AC.CURRENCY>
    DATEE     = TODAY
    Y.ACCT    = "EGP1610200010099"
    Y.AMT     = AMT.LCY

    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)


    GOSUB AC.STMT.ENTRY

*    ------------------------------------------------------------*****

    RETURN

AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""

    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRNS.CODE
    ENTRY<AC.STE.THEIR.REFERENCE>  = 'SCB.CD-':ID.COMPANY
    ENTRY<AC.STE.TRANS.REFERENCE>  = 'SCB.CD-':ID.COMPANY
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = DATEE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = Y.ACCT

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    V = 21
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("IC4",TYPE,MULTI.ENTRIES,"")

    RETURN
END
