* @ValidationCode : MjotMTgwOTAwNTY2NDpDcDEyNTI6MTY0MTk3OTUyNjE5NjpLYXJlZW0gTW9ydGFkYTotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 12 Jan 2022 11:25:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.REPORT.LIST.3J
*    PROGRAM E.REPORT.LIST.3J

*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_COMMON
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_EQUATE
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_F.HOLD.CONTROL
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_F.USER
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP

*-----------------------------------------------------------------------
*    DEPT = O.DATA

*    TEXT = DEPT;CALL REM

*    STR.ONE  = "������ ������"
*    STR.ONE  = "����� ��������"
*    STR.ONE  = "���� �����"
*    STR.ONE  = "����� �������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
    STR.ONE  = "������� ��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "����������"
*    STR.ONE  = "������ �������"
*    STR.ONE  = "�������� ������"
*    STR.ONE  = "���� �������"

*-------------------
    STR.TWO = ''
    COMP.ALL = ''
    WS.COMP = ID.COMPANY
    STR.ONE = WS.COMP:"DEP012..."
*Line [ 58 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.PRT2.ALL
*------------------------------------------



*    CALL E.REPORT.PRT2
*    COMP.ALL = ''
*    STR.TWO = ''
*    WS.COMP = ''

RETURN
*-----------------------------------------------------------------------
