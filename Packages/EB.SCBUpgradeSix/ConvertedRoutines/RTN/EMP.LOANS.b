* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*========================================================
    PROGRAM EMP.LOANS
*========================================================
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*========================================================
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    R.ACCOUNT = ''
    Y.ACC.ERR = ''
    ACCT.ID = ''
    SEQ.FILE.NAME = 'EMP.LOAN'
    RECORD.NAME = 'emp.loans.txt'
*========================================================
    CALL OPF(FN.ACC,F.ACC)
* DEBUG
    OPENSEQ "&SAVEDLISTS&" , "emp.loans" TO SEQ.FILE.POINTER ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    Y.MSG = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            CUS.ID          = FIELD(Y.MSG,"|",2)
            APP.DATE        = FIELD(Y.MSG,"|",9)
            APP.AMT         = FIELD(Y.MSG,"|",8)

            IF LEN(CUS.ID) EQ 8 THEN
                ACCT.ID     = CUS.ID:'10140801'
            END
            ELSE
                ACCT.ID     = '0':CUS.ID:'10140801'
            END
            *DEBUG
            CALL F.READ(FN.ACC,ACCT.ID,R.ACCOUNT,F.ACC,E1)
            NEW.APP.DATE = APP.DATE[7,4]:APP.DATE[4,2]:APP.DATE[1,2]
            R.ACCOUNT<AC.LOCAL.REF,ACLR.APPROVAL.DATE> = NEW.APP.DATE
            R.ACCOUNT<AC.LOCAL.REF,ACLR.AC.AMT.LOC>    = APP.AMT
            IF NOT(E1) THEN
                CALL F.WRITE(FN.ACC,ACCT.ID,R.ACCOUNT)
                CALL JOURNAL.UPDATE(ACCT.ID)
            END
        END ELSE
            EOF = 1
*           CRT "Unable to read from file "
        END
*  END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER
END
