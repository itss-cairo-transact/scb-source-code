* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.REPORT.PRT2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID, HOLD.ID, REP.TITLE
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP
*-----------------------------------------------------------------------
*    DEPT = O.DATA
    WS.COMP = ID.COMPANY

**-------------------------
*** VERY IMPORTANT
*** THIS CASE ONLY IN CAIRO BRANCH

    IF WS.COMP  NE 'EG0010099' THEN
        COMP.ALL = ''
    END
* --------------------------------
*** VERY IMPORTANT
*** THIS CASE ONLY TO DISPLAY REPORT NAME TO USER

***    WS.USER.DSP = 'Y'
    WS.USER.DSP = 'N'

***    BEGIN CASE
***    CASE   WS.COMP EQ 'EG0010001'
***        WS.USER.DSP = 'N'
***    CASE   WS.COMP EQ 'EG0010013'
***        WS.USER.DSP = 'N'
***    END CASE
*-----------------------------------------------------------------------
    FN.HOLD = "F.HOLD.CONTROL"
    F.HOLD  = ""
    R.HOLD = ""
    Y.HOLD.ID   = ""
    CALL OPF(FN.HOLD,F.HOLD)

*   ----------------------------
    SYS.DATE = TODAY
    WS.DATE = SYS.DATE
    CALL CDT('',WS.DATE,'-1W')
*    WS.DATE = 20081231
*-----------------------------------------------------------------------
*Line [ 73 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2.DEV

*Line [ 76 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2

    GOSUB READ.TEXT.FILE


    RETURN
*-----------------------------------------------------------------------
READ.TEXT.FILE:

    POS = 0
    REP.NAME = ''
    REP.TITLE = ''
    X = DCOUNT(C$REP.NAME,@FM)

    FOR I = 1 TO X

        FINDSTR STR.ONE IN C$DALL<I> SETTING POS ELSE POS = 0
*---------------------------------
        REP.NAME = C$REP.NAME<I>
*---------------------------------
        IF REP.NAME[1,2] NE 'SB' THEN
            POS = 0
        END
*---------------------------------
        IF POS GT 0 THEN
            REP.TITLE = C$REP.TITLE<I>
            GOSUB READ.HOLD.FILE
        END
*---------------------------------
    NEXT I

*---------------
    COMP.ALL = ''
*---------------
    RETURN
*--------------------------------------------------------------------
READ.HOLD.FILE:

    HOLD.ID = ''

*------------------------------------------------------------------------------
    BEGIN CASE
    CASE     COMP.ALL EQ ''
        SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
        SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE
        SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"
        SEL.HOLD := " AND COMPANY.ID EQ ":WS.COMP
*---------------
    CASE    COMP.ALL EQ 'ALL'
        SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
        SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE
        SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"
******        SEL.HOLD := " AND COMPANY.ID EQ ":WS.COMP
    END  CASE
*------------------------------------------------------------------------------
    CALL EB.READLIST(SEL.HOLD,SEL.LIST.HOLD,'',NO.OF.REP,ERR.HOLD)
    LOOP
        REMOVE Y.HOLD.ID FROM SEL.LIST.HOLD SETTING POS.HOLD
    WHILE Y.HOLD.ID:POS.HOLD
        CALL F.READ(FN.HOLD,Y.HOLD.ID,R.HOLD,F.HOLD,ERR.HOLD)

        IF Y.HOLD.ID   THEN
            HOLD.ID = Y.HOLD.ID

            IF WS.USER.DSP = 'Y' THEN
                TEXT = REP.TITLE ;CALL REM
            END

******            GOSUB PRINT.ONE.REPORT
******            CALL E.REPORT.PRT2X

*****                TEXT = REP.TITLE ;CALL REM

            EXECUTE "E.REPORT.PRT2X"

        END

    REPEAT


    RETURN
*--------------------------------------------------------------------
PRINT.ONE.REPORT:



    BEGIN CASE
*------------------------
    CASE DEVICE.ID NE ''
        GOSUB PRINT.WITH.DEVICE
****        GOSUB TEST.PRINT.REP
*------------------------
    CASE  DEVICE.ID EQ ''
        GOSUB PRINT.WITHOUT.DEVICE
*------------------------
    END CASE

    RETURN
*--------------------------------------------------------------------
PRINT.WITH.DEVICE:

    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

*   ----------------------------
* EXE.CMD = CP " ../bnk.data/eb/&HOLD&/":HOLD.ID

    EXE.CMD = "lp -d":DEVICE.ID:" ../bnk.data/eb/&HOLD&/":HOLD.ID

* EXE.CMD = "lpe -c -d":DEVICE.ID:" ../bnk.data/eb/&HOLD&/":HOLD.ID

    EXECUTE EXE.CMD


    RETURN

*--------------------------------------------------------------------
PRINT.HEAD:
    PR.HD = "'L'":SPACE(1)
    PRINT
    HEADING PR.HD
    RETURN

*--------------------------------------------------------------------
PRINT.WITHOUT.DEVICE:



    SET.ARABIC = "LANG=Ar_AA"
    SET.US = "LANG=en_US"
*Line [ 209 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF PUTENV(SET.ARABIC) THEN NULL

**  ������  **
    EXE.CMD = "qprt -P ":PRINTER.ID:" -p 13 ../bnk.data/eb/&HOLD&/":HOLD.ID
***    EXE.CMD = "qprt -P ":PRINTER.ID:" -p 11 ../bnk.data/eb/&HOLD&/":HOLD.ID
***    EXE.CMD = "qprt -P ":PRINTER.ID:" -p 9 ../bnk.data/eb/&HOLD&/":HOLD.ID
***    EXE.CMD = "qprt -P ":PRINTER.ID:" -W + ../bnk.data/eb/&HOLD&/":HOLD.ID

    EXECUTE EXE.CMD



    RETURN

***  ���� ����� ���� ***
***  ���� ����� ���� ***
***  ���� ����� ���� ***
***  ���� ����� ���� ***



*****************************************
*****************************************
*****************************************
*****************************************
*--------------------------------------------------------------------
TEST.PRINT.REP:

    CALL PRINTER.ON(REPORT.ID,'')

    GOSUB PRINT.LINE.BY.LINE

    CALL PRINTER.OFF

    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN

*--------------------------------------------------------------------
PRINT.LINE.BY.LINE:

    SEQ.FILE.NAME = " ../bnk.data/eb/&HOLD&/"
    REP.NAME = HOLD.ID

    OPENSEQ SEQ.FILE.NAME,REP.NAME TO SEQ.FILE.POINTER ELSE
        CREATE SEQ.FILE.POINTER ELSE
            STOP
            RETURN
        END
    END
*----------------------------------------------------------------------
    EOF = ''
    LN.NO = 0
    PG.NO = 0
    LOOP WHILE NOT(EOF)
        READSEQ ONE.LINE FROM SEQ.FILE.POINTER THEN
* ------------------

            PRINT ONE.LINE

            LN.NO = LN.NO + 1

*            IF LN.NO GT 350 THEN
*                EOF = 1
*            END

        END ELSE
            EOF = 1
* ------------------
        END

    REPEAT


    CLOSESEQ SEQ.FILE.POINTER

    RETURN
*-------------------------------------------------------------------------
