* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.DEPT.RTN(Y.RET.DEPT)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
*-----------------------------------------------------------------------

*Line [ 28 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2
*-----------------------------------------------------------------------
    DEPT.NAME = '������ ������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '����� ��������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '���� �����'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '����� �������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '�������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '��������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '��������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '�������'
    NO.OF.REP = 20
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '��������'
    NO.OF.REP = 30
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    DEPT.NAME = '���� �������'
    NO.OF.REP = 7
    Y.RET.DEPT<-1> = DEPT.NAME:'*':NO.OF.REP
*-----------------------------------------------------------------------
    RETURN
