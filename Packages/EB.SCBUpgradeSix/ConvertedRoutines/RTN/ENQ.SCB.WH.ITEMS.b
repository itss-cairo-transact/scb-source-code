* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.SCB.WH.ITEMS(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*TEXT = "ENQ.SCB.WH.ITEMS" ; CALL REM
    IF R.NEW(SCB.WH.TRANS.VERSION.NAME) EQ ",SCB.WH.DRAWING" OR R.NEW(SCB.WH.TRANS.VERSION.NAME) EQ ",SCB.WH.ADD" OR R.NEW(SCB.WH.TRANS.VERSION.NAME) EQ ",SCB.WH.OPENING" THEN
*TEXT = R.NEW(SCB.WH.TRANS.VERSION.NAME) ; CALL REM
        CUST    = R.NEW(SCB.WH.TRANS.CUSTOMER)
        CURR    = R.NEW(SCB.WH.TRANS.CURRENCY)
        CATE    = R.NEW(SCB.WH.TRANS.CATEGORY)
        WARE    = R.NEW(SCB.WH.TRANS.WHAREHOUSE.NO)

        T.SEL = "SELECT F.SCB.WH.ITEMS WITH CUSTOMER EQ ": CUST :" AND CURRENCY EQ ": CURR :" AND CATEGORY EQ ": CATE :" AND WHAREHOUSE.NO EQ ": WARE

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO REGIONS FOUND"
        END
    END
    RETURN
END
