* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>142</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.REPORT.CREATE.TODAY
*    PROGRAM   E.REPORT.CREATE.TODAY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HOLD.CTRL.TODAY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY


*-----------------------------------------------------------------------

    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL, C$YYYYMM
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID, HOLD.ID, REP.TITLE
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP

*-----------------------------------------------------------------------

*-----------------------------------------------------------------------

    GOSUB INIT

*----------------

    FOR J = 1 TO 15

        STR.ONE = AR.ARB(J)

        GOSUB RET.HOLD.ID

    NEXT J

*----------------

    RETURN

*-----------------------------------------------------------------------

*-----------------------------------------------------------------------
RET.HOLD.ID:


    GOSUB READ.TEXT.FILE





    RETURN
**-------------------------
*-----------------------------------------------------------------------
INIT:




    DIM AR.ARB(50)
    DIM AR.ENG(50)


**    AR.ARB(1)   = "���� �����"
    AR.ARB(1)   = "MNGR"
    AR.ARB(2)   = "��������"
    AR.ARB(3)   = "��������"
    AR.ARB(4)   = "����������"
    AR.ARB(5)   = "���� �������"
    AR.ARB(6)   = "����� ��������"
    AR.ARB(7)   = "����� �������"
    AR.ARB(8)   = "��������"
    AR.ARB(9)   = "������ �������"
    AR.ARB(10)  = "��������"
    AR.ARB(11)  = "�������"
    AR.ARB(12)  = "������� ��������"
    AR.ARB(13)  = "�������� ������"
    AR.ARB(14)  = "������ ������"
    AR.ARB(15)  = "�������"

*----------------
    STR.DEP = "DEP"

    FOR I = 1 TO 15

        DEP.NO = FMT(I,"R%3")
        WS.DEP = STR.DEP:DEP.NO
        AR.ENG(I) = WS.DEP

    NEXT I

    FN.HOLD = "F.HOLD.CONTROL"
    F.HOLD  = ""
    R.HOLD = ""
    Y.HOLD.ID   = ""
    CALL OPF(FN.HOLD,F.HOLD)
*   --------------------------------------------------------------------

    FN.HCTODAY = "F.SCB.HOLD.CTRL.TODAY"
    F.HCTODAY  = ""
    R.HCTODAY = ""
    Y.HCTODAY.ID   = ""
    CALL OPF(FN.HCTODAY,F.HCTODAY)

*   ----------
***    OPEN FN.HCTODAY TO FILEVAR ELSE ABORT 201, FN.HCTODAY
    FILEVAR = ''
    CALL OPF(FN.HCTODAY,FILEVAR)
    CLEARFILE FILEVAR

*-----------------------------------------------------------------------
    SYS.DATE = TODAY
    WS.DATE = SYS.DATE
    CALL CDT('',WS.DATE,'-1W')
*    WS.DATE = 20081231


*-----------------------------------------------------------------------

    COMP.ALL = 'ALL'
    WS.COMP = ''
    STR.TWO = ''
    WS.USER.DSP = 'N'


**    CALL E.REPORT.LIST.2.DEV


*Line [ 155 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2



    RETURN
*-----------------------------------------------------------------------
READ.TEXT.FILE:

    POS = 0
    REP.NAME = ''
    REP.TITLE = ''
    X = DCOUNT(C$REP.NAME,@FM)

    FOR I = 1 TO X

        FINDSTR STR.ONE IN C$DALL<I> SETTING POS ELSE POS = 0
*---------------------------------
        REP.NAME = C$REP.NAME<I>
*---------------------------------
        IF REP.NAME[1,2] NE 'SB' THEN
            POS = 0
        END
*---------------------------------
        IF POS GT 0 THEN
            REP.TITLE = C$REP.TITLE<I>
            GOSUB READ.HOLD.FILE
        END
*---------------------------------
    NEXT I

*---------------
    RETURN
*--------------------------------------------------------------------
READ.HOLD.FILE:

    HOLD.ID = ''

*------------------------------------------------------------------------------
    BEGIN CASE
    CASE     COMP.ALL EQ ''
        SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
        SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE
        SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"
        SEL.HOLD := " AND COMPANY.ID EQ ":WS.COMP
*---------------
    CASE    COMP.ALL EQ 'ALL'
        SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
        SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE
        SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"
******        SEL.HOLD := " AND COMPANY.ID EQ ":WS.COMP
    END  CASE
*------------------------------------------------------------------------------
    CALL EB.READLIST(SEL.HOLD,SEL.LIST.HOLD,'',NO.OF.REP,ERR.HOLD)
    LOOP
        REMOVE Y.HOLD.ID FROM SEL.LIST.HOLD SETTING POS.HOLD
    WHILE Y.HOLD.ID:POS.HOLD
        CALL F.READ(FN.HOLD,Y.HOLD.ID,R.HOLD,F.HOLD,ERR.HOLD)

        IF Y.HOLD.ID   THEN
            HOLD.ID = Y.HOLD.ID


*            IF WS.USER.DSP = 'Y' THEN
*                TEXT = REP.TITLE ;CALL REM
*            END
*****                TEXT = REP.TITLE ;CALL REM


            GOSUB WRITE.HCTODAY.FILE


        END

    REPEAT


    RETURN
*--------------------------------------------------------------------
*KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK

WRITE.HCTODAY.FILE:

    WS.COMP = R.HOLD<HCF.COMPANY.ID>


    WS.DEP      = AR.ENG(J)
    WS.REP.NAME = R.HOLD<HCF.REPORT.NAME>
    WS.FR       = WS.REP.NAME[3,1]
    WS.COMP     = R.HOLD<HCF.COMPANY.ID>

    ID.NO       = WS.COMP:WS.DEP:WS.REP.NAME

*-------------------------


    R.HCTODAY<HCT.H.DEPT.CODE>        = WS.DEP
    R.HCTODAY<HCT.DEPT.ARABIC.CODE>   = STR.ONE
    R.HCTODAY<HCT.H.CO.CODE>          = WS.COMP
    R.HCTODAY<HCT.REPORT.NAME>        = WS.REP.NAME
    R.HCTODAY<HCT.REPORT.TITLE>       = REP.TITLE
    R.HCTODAY<HCT.FREQ>               = WS.FR
    R.HCTODAY<HCT.BANK.DATE>          = R.HOLD<HCF.BANK.DATE>
    R.HCTODAY<HCT.HOLD.ID>            = HOLD.ID



*-------------------------------------------
    CALL F.WRITE(FN.HCTODAY,ID.NO,R.HCTODAY)
    CALL JOURNAL.UPDATE(ID.NO)


    RETURN

*--------------------------------------------------------------------
