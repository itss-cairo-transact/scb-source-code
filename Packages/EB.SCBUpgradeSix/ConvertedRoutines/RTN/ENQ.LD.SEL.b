* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>77</Rating><M.ELSAYED><27/1/2009>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.LD.SEL(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""
************************************************************************************
**** IF APPLICATION = 'LD.LOANS.AND.DEPOSITS' AND (R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> EQ ',SCB.OPEN.TERM.1' OR R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> EQ ',SCB.STAFF') THEN
    IF APPLICATION = 'LD.LOANS.AND.DEPOSITS' THEN
**** IF R.NEW(LD.CURRENCY) EQ 'EGP' OR R.NEW(LD.CURRENCY) EQ 'SAR' THEN
**** T.SEL = "SELECT F.CATEGORY WITH @ID GE '21001' AND @ID LE '21010'":" AND CO.CODE EQ ":COMP
        T.SEL = "SELECT F.CATEGORY WITH @ID GE '21001' AND @ID LE '21010'"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO CATEGORY"
        END
**** END
        IF (R.NEW(LD.CURRENCY) # 'EGP' AND R.NEW(LD.CURRENCY) # 'SAR')  THEN
            T.SEL = "SELECT F.CATEGORY WITH @ID GE '21001' AND @ID LE '21019'"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF KEY.LIST THEN
                FOR I = 1 TO SELECTED
                    ENQ<2,I> = "@ID"
                    ENQ<3,I> = "EQ"
                    ENQ<4,I> = KEY.LIST<I>
                NEXT I
            END ELSE
                ENQ.ERROR = "NO CATEGORY"
            END
        END
    END

*******************************************************************************************

**********************************************************************************
    IF APPLICATION = 'COLLATERAL' THEN

        IF  R.NEW(COLL.COLLATERAL.CODE) THEN
            CUST=  FIELD(ID.NEW,".",1)
***TEXT = "ID" : CUST ; CALL REM
            COLL.CODE = R.NEW(COLL.COLLATERAL.CODE)
        END
        IF COLL.CODE = "101" THEN
            T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CUSTOMER.ID EQ ": CUST :" AND (CATEGORY GE 21001 AND CATEGORY LE 21008)"
        END
        IF COLL.CODE = "103"  THEN
            T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CUSTOMER.ID EQ " :  CUST  :"  AND  (CATEGORY GE 21020 AND CATEGORY LE 21025)"
        END
**TEXT = T.SEL ; CALL REM
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            OD = ''
            ETEXT = ''
**TEXT = KEY.LIST ; CALL REM
            FOR I = 1 TO SELECTED
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            NEXT I
        END ELSE
            ENQ.ERROR = "NO DEPSOITS FOUND"
        END
    END
*******************************************************************************************

    RETURN
END
