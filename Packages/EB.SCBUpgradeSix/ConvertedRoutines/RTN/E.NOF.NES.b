* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* MADE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.NES(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

    COMP = ID.COMPANY
    EEE  = 0
    TOD  = TODAY
**--------------
    FN.COO = 'F.COMPANY' ;F.COO = '' ; R.COO = ''
    CALL OPF(FN.COO,F.COO)
    KEY.LIST.COO="" ; SELECTED.COO="" ; ER.MSG.COO=""
    T.SEL.COO = "SELECT F.COMPANY BY @ID "
    CALL EB.READLIST(T.SEL.COO,KEY.LIST.COO,"",SELECTED.COO,ER.MSG.COO)
*---------------
    FOR NN = 1 TO SELECTED.COO
        COM.CODE  = KEY.LIST.COO<NN>[2]
*       COM.CODE  = COM.CODE[2]
        COM.CODE  = TRIM(COM.CODE, "0", "L")

        FN.IN    = '/hq/opce/bclr/user/aibz'
        F.IN     = 0
        CALL OPF(FN.IN,F.IN)

        IDD =  "aib_out.":COM.CODE
        CALL F.READ(FN.IN,IDD,R.IN,F.IN,E111)
        IF NOT(E111) THEN
            Path = "/hq/opce/bclr/user/aibz/":IDD
            OPENSEQ Path TO MyPath ELSE
            END
            IF EEE = 0 THEN

                EOF = ''
                LOOP WHILE NOT(EOF)
                    READSEQ Line FROM MyPath THEN
                        F.1  = FIELD(Line,"|",2)
                        FF   = F.1[7,4]:F.1[4,2]:F.1[1,2]
                        IF FF GT TOD THEN 
                            F.2  = FIELD(Line,"|",3)
                            F.3  = F.2[2]:FIELD(Line,"|",4)
                            F.4  = FIELD(Line,"|",6)
                            F.5  = FIELD(Line,"|",8)
                            F.6  = FIELD(Line,"|",10)
                            F.7  = FIELD(Line,"|",21)
                            F.7  = F.7[2]
                            F.7  = TRIM(F.7, "0", "L")
                            F.8  = FIELD(Line,"|",24)
                            Y.RET.DATA<-1> = F.1:"*":F.2:"*":F.3:"*":F.4:"*":F.5:"*":F.6:"*":F.7:"*":F.8
                        END
                    END ELSE
                        EOF = 1
                    END
                REPEAT
            END
            CLOSESEQ MyPath
        END
    NEXT NN
*--------------------
    RETURN
END
