* @ValidationCode : MjotODM0MTYxMDYwOkNwMTI1MjoxNjQwODU3NDEwOTA3OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:43:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
SUBROUTINE E.REPORT.LIST.3A
*    PROGRAM E.REPORT.LIST.3A

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.HOLD.CONTROL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP
*-----------------------------------------------------------------------

    STR.ONE  = "������ ������"
*    STR.ONE  = "����� ��������"
*    STR.ONE  = "���� �����"
*    STR.ONE  = "����� �������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "�������"
*    STR.ONE  = "��������"
*    STR.ONE  = "������� ��������"
*    STR.ONE  = "��������"
*    STR.ONE  = "����������"
*    STR.ONE  = "������ �������"
*    STR.ONE  = "�������� ������"
*    STR.ONE  = "���� �������"
*4         *
*-------------------
    STR.TWO = ''
    COMP.ALL = 'ALL'
    WS.COMP = ID.COMPANY
    STR.ONE = WS.COMP:"DEP014..."
*Line [ 56 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.PRT2.ALL

*-------------------
*    COMP.ALL = 'ALL'
*    WS.COMP = ''
*    CALL E.REPORT.PRT2
*    STR.TWO = ''
RETURN
*-----------------------------------------------------------------------
