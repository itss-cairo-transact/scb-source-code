* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
**-----------------------------------------------------------------------------
* <Rating>-44</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOD.SCCD.INT.ACH.CBE
**    PROGRAM EOD.SCCD.INT.ACH.CBE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SCCD.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SCCD.LOG

**************
    FN.CD.LOG = "F.SCB.SCCD.LOG" ; F.CD.LOG = ""; R.CD.LOG = "";RUN.DAT  =''
    CALL OPF(FN.CD.LOG, F.CD.LOG)

    FIL.NAM = "EOD.SCCD.INT.ACH.CBE":"-":TODAY
    CALL F.READ(FN.CD.LOG,FIL.NAM,R.CD.LOG,F.CD.LOG,E15)
    RUN.DAT  =  R.CD.LOG<SCCD.LOG.LAST.RUN.DATE>
    TEXT=RUN.DAT;CALL REM
    IF RUN.DAT EQ ''  THEN
**************
        COMP.USER = ID.COMPANY
        GOSUB INITIALISE
        GOSUB BUILD.RECORD

***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E

***************
        R.CD.LOG<SCCD.LOG.LAST.RUN.DATE> = TODAY
        R.CD.LOG<SCCD.LOG.PROGRAM.NAME>  = 'EOD.SCCD.INT.ACH.CBE'

**    WRITE R.CD.LOG TO F.CD.LOG , FIL.NAM ON ERROR
**      PRINT 'CAN NOT WRITE RECORD ':FN.CD.LOG
**END

        CALL F.WRITE(FN.CD.LOG,FIL.NAM,R.CD.LOG)
        CALL JOURNAL.UPDATE(FIL.NAM)

    END
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"; F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN          = 0
    OFS.REC           = ""
    OFS.OPERATION     = "FUNDS.TRANSFER"
    OFS.OPTIONS       = "SCCD"

    COMP              = ID.COMPANY
    COM.CODE          = COMP[8,2]
    OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.TRANS.ID      = ""
    OFS.MESSAGE.DATA  = ""


    DIR.NAME          = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    DAT               = TODAY
    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST.CD = "" ; SELECTED.CD = "" ;  ER.MSG.CD = ""

    FN.CD.INT = 'F.SCB.SCCD.INT' ; F.CD.INT = '' ; R.CD.INT = ''
    CALL OPF(FN.CD.INT,F.CD.INT)

    FN.DATE = "F.DATES"; F.DATE  = "" ; R.DATE = "" ; Y.DATE.ID = ""
    CALL OPF (FN.DATE,F.DATE)

    Y.DATE.ID     = "EG0010001"
    CALL F.READ(FN.DATE,Y.DATE.ID,R.DATE,F.DATE,ERR.DATE)

    L.WS.DATE     = R.DATE<EB.DAT.LAST.WORKING.DAY>
    CD.INT.DATE   = L.WS.DATE
    CD.INT.DATE.1 = L.WS.DATE

    CALL CDT('', CD.INT.DATE.1, '1')

    T.SEL = "SELECT F.SCB.SCCD.INT WITH ACH.FLG EQ 'YES-99' AND @ID LIKE 994... AND  ( VALUE.DATE GT " :CD.INT.DATE: " AND VALUE.DATE LE " :CD.INT.DATE.1: " ) "
** T.SEL ="SELECT F.SCB.SCCD.INT WITH @ID LIKE 99... AND VALUE.DATE EQ 20141217 "
    CALL EB.READLIST(T.SEL,KEY.LIST.CD,"",SELECTED.CD,ER.MSG.CD)
    TEXT=SELECTED.CD:'NO.OF.RECORDS';CALL REM
    FOR JJ = 1 TO SELECTED.CD
        CALL F.READ(FN.CD.INT,KEY.LIST.CD<JJ>,R.CD.INT,F.CD.INT,E1)
*********MODIFICATION
        DB.ACCT           =  R.CD.INT<SCCD.INT.LIQ.ACCT>
        NEW.FILE          = 'ACH.CBE.':DB.ACCT:".":DAT:'.':RND(10000)
        OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
            CLOSESEQ V.FILE.IN
            HUSH ON
            EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
            HUSH OFF
            PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
        END
        OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
            CREATE V.FILE.IN THEN
                PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
            END
            ELSE
                STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
            END
        END
************************
        COMMA        =  ","
        TRANS.TYPE   =  'AC'
        DB.CUR       =  'EGP'
        DB.AMT       =  R.CD.INT<SCCD.INT.AMOUNT.TOT>
        DB.DATE      =  R.CD.INT<SCCD.VALUE.DATE>
        DB.ACCT      =  R.CD.INT<SCCD.INT.LIQ.ACCT>
        CR.DATE      =  R.CD.INT<SCCD.VALUE.DATE>
        CR.ACCT      =  '9943330010508001'

        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
        OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
        OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
        OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
        OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATE:COMMA
        OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
        OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": DB.CUR:COMMA
        OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
        OFS.MESSAGE.DATA := "ORDERING.BANK=":"SCB":COMMA
        OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":'SCCD.INT.ACH':COMMA
        OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":DB.ACCT:COMMA
        OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE'

        ZZZ = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

        WRITESEQ ZZZ TO V.FILE.IN ELSE
            PRINT  'CAN NOT WRITE LINE ':ZZZ
        END

    NEXT JJ
    RETURN
**********************
END
