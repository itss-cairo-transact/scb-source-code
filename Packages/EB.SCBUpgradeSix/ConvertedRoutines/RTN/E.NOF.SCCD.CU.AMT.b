* @ValidationCode : MjoxNzMxNDk5OTMyOkNwMTI1MjoxNjQwODU3MDA2ODE0OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:36:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***************************MAHMOUD 26/11/2014******************************
*-----------------------------------------------------------------------------
* <Rating>297</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.SCCD.CU.AMT(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
 
    GOSUB CALLDB
    GOSUB PROCESS
RETURN
*========================================
INITIATE:
*-------
    KK = 0
    K2 = 0
***********************
    DYNO = 0
    TDD = TODAY
    TD1 = TODAY
    CRT TD1
***********************
    COM.AMT = 0
    ARR.REC  = ''
    ARR.DATA = ''
    REC.GEO  = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '
    MIN.AMT    = 100000
RETURN
******************************************************
CLEAR.VAR:
*----------
RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)
RETURN
*========================================
PROCESS:
*-------
*DEBUG
    SEL.CU  = "SELECT ":FN.CU:" WITH SCCD.CUSTOMER EQ YES "
    SEL.CU := " AND CONTACT.DATE GE 20140904 "
    CALL EB.READLIST(SEL.CU,KEY.CU,'',SELECTED.CU,ERR.CU)
    CRT SELECTED.CU
    LOOP
        REMOVE CU.ID FROM KEY.CU SETTING POSS.CU
    WHILE CU.ID:POSS.CU
        CUS.AMT = 0
        GOSUB LD.REC
        IF CUS.AMT NE 0 AND CUS.AMT NE '' THEN
            IF CUS.AMT GE MIN.AMT THEN
                KK++
                GOSUB FILL.ARR
            END
        END
    REPEAT
    GOSUB SORT.ARR
    GOSUB RET.REC
RETURN
***************************//LD//*****************************************
LD.REC:
*----------
    CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ "LD" THEN
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
            REC.CAT  = R.LD<LD.CATEGORY>
 
            FINDSTR REC.CAT:" " IN SCCD.GRP SETTING POS.SCCD THEN NULL ELSE GOTO NXT.REC
            REC.COM  = R.LD<LD.CO.CODE>
            REC.AMT  = R.LD<LD.AMOUNT>
            CUS.AMT += REC.AMT
        END ELSE
            GOTO NXT.REC
        END
NXT.REC:
    REPEAT
RETURN
*****************************************************************
FILL.ARR:
*********
    ARR.REC<KK,1> = CUS.AMT
    ARR.REC<KK,2> = CU.ID
    ARR.REC<KK,3> = REC.COM
RETURN
*****************************************************************
RET.REC:
********
    ARR.DATA.CNT = DCOUNT(ARR.DATA,@FM)
    FOR LL = ARR.DATA.CNT TO 1 STEP -1
        Y.LINE.REC = (FIELD(ARR.DATA<LL>,"-",1) * 1):"*":FIELD(ARR.DATA<LL>,"-",2):"*":FIELD(ARR.DATA<LL>,"-",3)
***        CRT Y.LINE.REC
        Y.RET.DATA<-1>= Y.LINE.REC
    NEXT LL
RETURN
*************************************************
SORT.ARR:
********
    CNT.REC = DCOUNT(ARR.REC,@FM)
    CRT CNT.REC
    FOR L1 = 1 TO CNT.REC
        ARR.DATA<L1> = FMT(ARR.REC<L1,1>,'R%20'):"-":ARR.REC<L1,2>:"-":ARR.REC<L1,3>
    NEXT L1
    ARR.DATA = SORT(ARR.DATA)
RETURN
*****************************************************************
END
