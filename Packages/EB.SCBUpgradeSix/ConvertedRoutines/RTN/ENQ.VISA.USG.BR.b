* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
****NESSREEN AHMED 27/10/2011*********************************
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ENQ.VISA.USG.BR(ENQ)
*************************************************************
*************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS.TOT

    COMP = C$ID.COMPANY
    BR = COMP[2]
**    TEXT = 'BR=':BR ; CALL REM

    YTEXT = "Enter the start Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
****UPDATED BY NESSREEN AHMED 8/12/2011*********************
    YTEXT = "Enter the End Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ED.DATE = COMI
    T.SEL = "SELECT F.SCB.VISA.TRANS.TOT WITH POS.DATE GE ": ST.DATE : " AND POS.DATE LE  ": ED.DATE : " AND BRANCH.NUMBER EQ ": BR
*****END OF UPDATE 8/12/2011**********************************
    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
  **  TEXT = 'SELECTED=':SELECTED ; CALL REM
  **  TEXT = "ac = " : KEY.LIST<1> ; CALL REM
    IF SELECTED >= 1 THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ<2> = '@ID'
        ENQ<3> = 'EQ'
        ENQ<4> = 'DUUMY'
    END

*************************************************************
    RETURN
END
