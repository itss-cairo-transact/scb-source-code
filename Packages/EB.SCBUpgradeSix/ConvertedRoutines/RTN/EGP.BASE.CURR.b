* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE EGP.BASE.CURR(VAL,CURR)
****Mahmoud Elhawary******7/6/2009*****************************
*   Function for converting to EGP by base rate               *
***************************************************************
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

    AMT = VAL
    CUR.COD = CURR
    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)

*Line [ 37 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE CUR.COD IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
    RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
    IF RATE.TYPE EQ 'MULTIPLY' THEN
        TTT = AMT * CUR.RATE
    END
    IF RATE.TYPE EQ 'DIVIDE' THEN
        TTT = AMT / CUR.RATE
    END
    VAL = TTT
    RETURN 
