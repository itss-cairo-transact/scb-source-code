* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***NESSREEN AHMED 24/10/2011*********************************
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.FT.SAME.DB.CR(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

*-----------------------------------------------------------------------*
    TD1 = ''

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = "" ; USR = ""

    FN.FT.H   = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; ERR.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)

    YTEXT = "Enter the start Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    ST.DATE = COMI
    ED.DATE   = TODAY
    CALL CDT("",ED.DATE,'-1W')
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DEBIT.ACCT.NO EQ CREDIT.ACCT.NO AND (TRANSACTION.TYPE EQ 'ACVC' OR TRANSACTION.TYPE EQ 'ACVM' OR TRANSACTION.TYPE EQ 'ACVS') AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE GE ": ST.DATE : " BY AUTH.DATE "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ER.MSG)

    TEXT = 'SELECTED=':SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
        FT.ID = '' ; TR.TYPE = '' ; DB.ACCT.NO = '' ; CR.ACCT.NO = '' ; DB.AMOUNT = '' ; AUTH.DATE = ''
        INP = '' ; AUTH = '' ; INP.NO = '' ; AUTH.NO = '' ; CO.CODE = ''
        CALL F.READ( FN.FT.H,KEY.LIST<I>, R.FT.H, F.FT.H, ERR.FT.H)
        FT.ID         = KEY.LIST<I>
        TR.TYPE       = R.FT.H<FT.TRANSACTION.TYPE>
        DB.ACCT.NO    = R.FT.H<FT.DEBIT.ACCT.NO>
        CR.ACCT.NO    = R.FT.H<FT.CREDIT.ACCT.NO>
        DB.AMOUNT     = R.FT.H<FT.DEBIT.AMOUNT>
        AUTH.DATE     = R.FT.H<FT.AUTH.DATE>
        INP           = R.FT.H<FT.INPUTTER><1,1>
        INP.NO        = FIELD(INP, "_", 2)
        AUTH          = R.FT.H<FT.AUTHORISER>
        AUTH.NO       = FIELD(AUTH, "_", 2)
        CO.CODE       = R.FT.H<FT.CO.CODE>
        Y.RET.DATA<-1> = FT.ID[1,12]:"*":AUTH.DATE:"*":TR.TYPE:"*":DB.ACCT.NO:"*":CR.ACCT.NO:"*":DB.AMOUNT:"*":INP.NO:"*":AUTH.NO:"*":CO.CODE:"*":ST.DATE:"*":ED.DATE
    NEXT I
    RETURN
END
