* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.SAVE.HOLD(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""
    FN.ACC = 'FBNK.AC.LOCKED.EVENTS' ;F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    T.SEL = "SELECT FBNK.AC.LOCKED.EVENTS "
*** T.SEL = "SELECT FBNK.AC.LOCKED.EVENTS WITH @ID EQ ACLK0700300026 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,READ.ERR)
            CATEG = R.ACC<AC.LCK.ACCOUNT.NUMBER>[11,4]
            IF (CATEG GE 6501 AND CATEG LE 6504) THEN
***    TEXT="ID= ":KEY.LIST<I>; CALL REM
***    TEXT= "CAT" : CATEG ; CALL REM
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "EQ"
                ENQ<4,I> = KEY.LIST<I>
            END ELSE
                ENQ<2,I> = "@ID"
                ENQ<3,I> = "NE"
                ENQ<4,I> = "HH"
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS"

    END

*******************************************************************************************
    RETURN
END
