* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.DISTRESSED.DEBTS(M.RET.DATA)

***Mahmoud Elhawary******19/6/2010****************************

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 44 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    ACCT.GRP  = ''
    Y.OPEN.BAL= ''
    KK = 0
    XX = SPACE(120)
    STE.BAL = ''
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    RETURN
****************************************
PROCESS:
*-------
*    LOCATE "BRANCH.NO"  IN D.FIELDS<1> SETTING YCOM.POS THEN COM.ID    = D.RANGE.AND.VALUE<YCOM.POS> ELSE RETURN
    LOCATE "START.DATE" IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"   IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

*    CCMP = COM.ID
*    LCCMP = LEN(COM.ID)
*    IF LCCMP EQ 1 THEN
*        CCMP = '0':CCMP
*    END
*    COM.ID = 'EG00100':CCMP

    S.SEL  = "SELECT ":FN.CUS:" WITH CREDIT.CODE EQ 110"
    S.SEL := " AND COMPANY.BOOK EQ ":COMP
    S.SEL := " BY COMPANY.BOOK"
    CALL EB.READLIST(S.SEL,K.LIST,'',SELECTED,ER.CU)
    LOOP
        REMOVE CUS.ID FROM K.LIST SETTING POS.CUS
    WHILE CUS.ID:POS.CUS
        CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
        CU.COMP = R.CUS<EB.CUS.COMPANY.BOOK>
        CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
        LOOP
            REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
        WHILE ACCT.ID:POS.CUS.ACC
            AC.CAT = ''
            AC.CUR = ''
            CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
            AC.CAT = R.ACCT<AC.CATEGORY>
            AC.CUR = R.ACCT<AC.CURRENCY>
            IF (AC.CAT GE 1220 AND AC.CAT LE 1227) OR (AC.CAT EQ 9090) THEN
                IF (AC.CAT GE 1220 AND AC.CAT LE 1227) THEN
                    ACCT.GRP = 1
                END ELSE
                    ACCT.GRP = 2
                END
*****************************
                CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
                STE.BAL = OPENING.BAL
                TOT.DR.MVMT = 0
                TOT.CR.MVMT = 0
                LOOP
                    REMOVE STE.ID FROM ID.LIST SETTING POS
                WHILE STE.ID:POS
                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                    STE.CUR = R.STE<AC.STE.CURRENCY>
                    STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                    STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                    STE.VAL = R.STE<AC.STE.VALUE.DATE>
                    STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                    STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                    IF STE.CUR NE 'EGP' THEN
                        STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                    END
                    STE.BAL += STE.AMT
                    IF STE.AMT LT 0 THEN
                        TOT.DR.MVMT += STE.AMT
                    END ELSE
                        TOT.CR.MVMT += STE.AMT
                    END
                REPEAT
                GOSUB RET.DATA
            END
        REPEAT
    REPEAT
    RETURN
**************************************
RET.DATA:
*--------
    M.RET.DATA<-1> = CU.COMP:"*":CUS.ID:"*":ACCT.GRP:"*":AC.CUR:"*":FROM.DATE:"*":END.DATE:"*":ACCT.ID:"*":OPENING.BAL:"*":TOT.DR.MVMT:"*":TOT.CR.MVMT:"*":STE.BAL
    RETURN
**************************************
END
