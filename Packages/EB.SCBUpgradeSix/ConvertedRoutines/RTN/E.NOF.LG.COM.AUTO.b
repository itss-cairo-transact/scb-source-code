* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  E.NOF.LG.COM.AUTO(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    YF.CATEG.MONTH = "F.CATEG.MONTH"
    F.CATEG.MONTH = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)
*
    YF.CATEG.ENTRY = "F.CATEG.ENTRY"
    F.CATEG.ENTRY = ""
    CALL OPF(YF.CATEG.ENTRY,F.CATEG.ENTRY)
*
    YF.CATEGORY = "F.CATEGORY"
    F.CATEGORY = ""
    CALL OPF(YF.CATEGORY,F.CATEGORY)
*
    F.CATEG.ENT.TODAY = ""
    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)
*
    F.CATEG.ENT.FWD = ''
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD'
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    F.LD  = ''
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    CALL OPF(FN.LD,F.LD)

**--------------------------------------------------------------------**
*    FROM.DATE = "20100501"
*    TO.DATE   = '20100506'

    LOCATE "FROM.DATE." IN D.FIELDS<1> SETTING BRN.POS THEN
        FROM.DATE  = D.RANGE.AND.VALUE<BRN.POS>
    END

    LOCATE "TO.DATE." IN D.FIELDS<1> SETTING BRN.POS THEN
        TO.DATE  = D.RANGE.AND.VALUE<BRN.POS>
    END

*TEXT  =  FROM.DATE ; CALL REM
*TEXT  =  TO.DATE ; CALL REM
    XX    = 1
    PL.NO = '520505205252054520585245252057'

    FOR I = 1 TO 6

        Y.CAT.NO  = PL.NO[XX,5]
*TEXT =  Y.CAT.NO ; CALL REM
        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
*TEXT = "A=  ": YR.ENTRY.FILE ; CALL REM

        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE
   ***         READ YR.CATEG.ENTRY FROM F.CATEG.ENTRY, YENTRY.KEY ELSE YR.CATEG.ENTRY = ''
            CALL F.READ (YF.CATEG.ENTRY, YENTRY.KEY , YR.CATEG.ENTRY, F.CATEG.ENTRY, CAT.ERR)
            IF YR.CATEG.ENTRY<AC.CAT.COMPANY.CODE> = ID.COMPANY THEN
**HASHED ON 22/3/2012                IF YR.CATEG.ENTRY<AC.CAT.TRANSACTION.CODE> = '430' THEN
                IF YR.CATEG.ENTRY<AC.CAT.TRANSACTION.CODE> = '875' THEN
                    IF YR.CATEG.ENTRY<AC.CAT.PRODUCT.CATEGORY> = '21096' THEN
*                    TEXT = "1" ; CALL REM
                        INPP = YR.CATEG.ENTRY<AC.CAT.INPUTTER>
                        INPTT = FIELD(INPP,'_',2)
                        IF INPTT = 'INPUTTCOB' THEN
*TEXT = "LD= " : YR.CATEG.ENTRY<AC.CAT.OUR.REFERENCE> ; CALL REM
                            LD.ID = YR.CATEG.ENTRY<AC.CAT.OUR.REFERENCE>
                            CALL F.READ( FN.LD,LD.ID, R.LD,F.LD,ETEXT)
                            CHRG.AC = R.LD<LD.CHRG.LIQ.ACCT>
                            OLD.LG  = R.LD<LD.LOCAL.REF,LDLR.OLD.NO>
                            LCY.AMT = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>
                            FCY.AMT = YR.CATEG.ENTRY<AC.CAT.AMOUNT.FCY>
                            B.DAT   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
                            V.DAT   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
                            LG.KIND = R.LD<LD.LOCAL.REF,LDLR.LG.KIND>
                            Y.RET.DATA<-1> = CHRG.AC:"*":LD.ID:"*":OLD.LG:"*":LCY.AMT:"*":FCY.AMT:"*":B.DAT:"*":V.DAT:"*":LG.KIND:"*":FROM.DATE:"*":TO.DATE:"*":YENTRY.KEY:"*":Y.CAT.NO
* TEXT = V.DAT ; CALL REM
                        END
                    END
                END
            END
        REPEAT
*  RETURN
        XX = XX + 5
    NEXT I

    RETURN
END
