* @ValidationCode : MjoyMDE5MTgwOTkzOkNwMTI1MjoxNjQwODU2ODQzNjIzOkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 11:34:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
***************************MAHMOUD 30/9/2014******************************
SUBROUTINE E.NOF.SCB.SCCD.ONLY.GEO(Y.RET.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
 
    GOSUB CALLDB
    GOSUB PROCESS
RETURN
*========================================
INITIATE:
*-------
    KK = 0
    K2 = 0
***********************
    DYNO = 0
*    TDD = TODAY
    TD1 = TODAY
*    DYNO = FMT(ICONV(TD1,'D'),'DW')
*    TDYNO = '-':DYNO:'C'
*    CALL CDT('',TD1,TDYNO)
    CRT TD1
***********************
    ARR.REC  = ''
    ARR.DATA = ''
    REC.GEO  = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

    GRAND.CAIRO.GRP = " EG0010001 EG0010002 EG0010003 EG0010004 EG0010005 EG0010006 EG0010007 EG0010009 EG0010010 EG0010011 EG0010012 EG0010013 EG0010014 EG0010015 EG0010016 "
    LOWER.EGYPT.GRP = " EG0010020 EG0010021 EG0010022 EG0010023 EG0010031 EG0010032 EG0010060 EG0010070 EG0010090 "
    UPPER.EGYPT.GRP = " EG0010080 EG0010081 "
    SUEZ.CANAL.GRP  = " EG0010030 EG0010040 EG0010050 EG0010051 "
    OTHER.GRP       = " EG0010035 "
RETURN
******************************************************
CLEAR.VAR:
*----------

RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)
RETURN
*========================================
PROCESS:
*-------
***************************//LD//*****************************************
    SEL.CMD  = "SELECT ":FN.CU:" WITH SCCD.CUSTOMER EQ YES AND ( BANK EQ '0017' OR BANK EQ '')"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.CU.LIST)
    CRT SELECTED

    LOOP
        REMOVE CU.ID FROM KEY.LIST SETTING POSS.CU
    WHILE CU.ID:POSS.CU
        CALL F.READ(FN.CU,CU.ID,R.CU,F.CU,ER.CU1)
        CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
        IF REC.SEC EQ '4650' THEN
            REC.FLG = 1
        END ELSE
            REC.FLG = 2
        END
        KK++
        CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
        LOOP
            REMOVE LD.ID FROM R.IND.LD SETTING POSS2
        WHILE LD.ID:POSS2
            K2++
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
            REC.CAT  = R.LD<LD.CATEGORY>
 
            FINDSTR REC.CAT:" " IN SCCD.GRP SETTING POS.SCCD THEN NULL ELSE GOTO NXT.REC
            REC.COM  = R.LD<LD.CO.CODE>
 
            FINDSTR REC.COM IN GRAND.CAIRO.GRP SETTING POS.GEO1 THEN REC.GEO = 1 ELSE NULL
 
            FINDSTR REC.COM IN LOWER.EGYPT.GRP SETTING POS.GEO2 THEN REC.GEO = 2 ELSE NULL
 
            FINDSTR REC.COM IN UPPER.EGYPT.GRP SETTING POS.GEO3 THEN REC.GEO = 3 ELSE NULL
 
            FINDSTR REC.COM IN SUEZ.CANAL.GRP  SETTING POS.GEO4 THEN REC.GEO = 4 ELSE NULL
 
            FINDSTR REC.COM IN OTHER.GRP       SETTING POS.GEO5 THEN REC.GEO = 5 ELSE NULL
            REC.CUS  = R.LD<LD.CUSTOMER.ID>
            REC.BNK  = CU.LCL<1,CULR.CU.BANK>
            REC.CUR  = R.LD<LD.CURRENCY>
            REC.AMT  = R.LD<LD.AMOUNT>
            REC.VAL  = R.LD<LD.VALUE.DATE>
            REC.LCL  = R.LD<LD.LOCAL.REF>
***            REC.FLG  = REC.LCL<1,LDLR.CHK.FLG>
            REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
 
            IF REC.QTY EQ '' THEN REC.QTY = 1 ELSE NULL
            REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
 
            FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
            IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
                REC.TYP = "EGP-1000-3M-60M-SUEZ"
            END
*===========================================
            GOSUB FILL.ARR
*===========================================
NXT.REC:
        REPEAT
        IF REC.FLG EQ 1 THEN
            ARR.REC<REC.GEO,3> += 1
        END ELSE
            ARR.REC<REC.GEO,5> += 1
        END
    REPEAT
    MXX1 = DCOUNT(ARR.REC,@FM)
    FOR AAA1 = 1 TO MXX1
        REC.GEO = AAA1
        GOSUB RET.REC
    NEXT AAA1
RETURN
*****************************************************************
FILL.ARR:
*---------
    ARR.REC<REC.GEO,1> = REC.GEO
    IF REC.FLG = 1 THEN
        ARR.REC<REC.GEO,2> += REC.AMT
    END ELSE
        ARR.REC<REC.GEO,4> += REC.AMT
    END
RETURN
*****************************************************************
RET.REC:
********
 
    IF ARR.REC<REC.GEO,1> = 1 THEN GEO.NAME = '������� ������' ELSE NULL
 
    IF ARR.REC<REC.GEO,1> = 2 THEN GEO.NAME = '��� ����' ELSE NULL
 
    IF ARR.REC<REC.GEO,1> = 3 THEN GEO.NAME = '��� ����' ELSE NULL
 
    IF ARR.REC<REC.GEO,1> = 4 THEN GEO.NAME = '��� ������' ELSE NULL
 
    IF ARR.REC<REC.GEO,1> = 5 THEN GEO.NAME = '����' ELSE NULL

    Y.RET.DATA<-1>= ARR.REC<REC.GEO,1>:"*":GEO.NAME:"*":ARR.REC<REC.GEO,2>:"*":ARR.REC<REC.GEO,3>:"*":ARR.REC<REC.GEO,4>:"*":ARR.REC<REC.GEO,5>

RETURN
*************************************************
END
