* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.REPORT.LIST.3
*    PROGRAM E.REPORT.LIST.3

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*-----------------------------------------------------------------------
    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID
*-----------------------------------------------------------------------
*    DEPT = O.DATA

*    TEXT = DEPT;CALL REM

    STR.ONE  = 'المراجعة'

*-----------------------------------------------------------------------
    FN.HOLD = "F.HOLD.CONTROL"
    F.HOLD  = ""
    R.HOLD = ""
    Y.HOLD.ID   = ""
    CALL OPF(FN.HOLD,F.HOLD)

*   ----------------------------
    SYS.DATE = TODAY
    WS.DATE = SYS.DATE
    CALL CDT('',WS.DATE,'-1W')

*-----------------------------------------------------------------------
*Line [ 52 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2.DEV

*Line [ 55 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2

    GOSUB READ.TEXT.FILE


    RETURN
*-----------------------------------------------------------------------
READ.TEXT.FILE:

    POS = 0
    REP.NAME = ''
    REP.TITLE = ''
    X = DCOUNT(C$REP.NAME,@FM)

    FOR I = 1 TO X

        FINDSTR STR.ONE IN C$DALL<I> SETTING POS ELSE POS = 0
*---------------------------------
        REP.NAME = C$REP.NAME<I>
*---------------------------------
        IF REP.NAME[1,2] NE 'SB' THEN
            POS = 0
        END
*---------------------------------
        IF POS GT 0 THEN
            REP.TITLE = C$REP.TITLE<I>
            GOSUB READ.HOLD.FILE
        END
*---------------------------------
    NEXT I

    RETURN
*--------------------------------------------------------------------
READ.HOLD.FILE:

    HOLD.ID = ''
    SEL.HOLD  = "SELECT ":FN.HOLD:" WITH  REPORT.NAME EQ ":REP.NAME
    SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE
    SEL.HOLD := " AND RUN.IN.BATCH EQ 'Y'"

    CALL EB.READLIST(SEL.HOLD,SEL.LIST.HOLD,'',NO.OF.REP,ERR.HOLD)
    LOOP
        REMOVE Y.HOLD.ID FROM SEL.LIST.HOLD SETTING POS.HOLD
    WHILE Y.HOLD.ID:POS.HOLD
        CALL F.READ(FN.HOLD,Y.HOLD.ID,R.HOLD,F.HOLD,ERR.HOLD)

        IF Y.HOLD.ID   THEN
            HOLD.ID = Y.HOLD.ID
            GOSUB PRINT.ONE.REPORT
        END

    REPEAT
    RETURN
*--------------------------------------------------------------------
PRINT.ONE.REPORT:
    CALL PRINTER.ON(REPORT.ID,'')

    GOSUB PRINT.INFOTEST

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN


*--------------------------------------------------------------------
PRINT.INFOTEST:

    SEQ.FILE.NAME = " ../bnk.data/eb/&HOLD&/"
    REP.NAME = HOLD.ID

    OPENSEQ SEQ.FILE.NAME,REP.NAME TO SEQ.FILE.POINTER ELSE
        CREATE SEQ.FILE.POINTER ELSE
            STOP
            RETURN
        END
    END
*----------------------------------------------------------------------

    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
* ------------------


            PRINT Y.MSG


        END ELSE
            EOF = 1
* ------------------
        END

    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

    RETURN
