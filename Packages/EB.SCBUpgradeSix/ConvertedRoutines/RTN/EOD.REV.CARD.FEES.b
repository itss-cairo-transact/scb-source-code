* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*---- CREATE BY NESSMA
**   PROGRAM EOD.REV.CARD.FEES
    SUBROUTINE EOD.REV.CARD.FEES
 
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CARD.FEES.ALL
*----------------------------------------------
    GOSUB INIT
    GOSUB PROCESS
    RETURN
*----------------------------------------------
INIT:
*----
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "AC.LOCKED.EVENTS"
    OFS.OPTIONS   = "CARD.FEES.REV/R"
    COMP          = C$ID.COMPANY
    COMP.CODE     = COMP[8,2]

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME      = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    COMMA         = ","
    COMP          = ""
* OFS.USER.INFO = ""
    ACCT.NO       = ""
    LOCK.AMT      = ""

    RETURN
*----------------------------------------------
OFS.RECORD.FT:
*-------------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "CARD.FEES2"
    COMP             = C$ID.COMPANY
    COMP.CODE        = COMP[8,2]

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = "" ; OFS.REC = ""

    DIR.NAME  = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE2 = "CARD.FEES.R.":TEMP.ID
    COMMA     = ","

    OFS.USER.INFO2 = "AUTO.CHRGE//":COMP2
    TRNS.TYPE      = "AC52"
    DR.ACC         = ACC.ID
    DR.CUR         = "EGP"
    CR.ACC         = "PL57040"
    AMOUNT         = LCK.AMT

    IF AMOUNT GT 0 THEN
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE="    :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :DR.CUR:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :AMOUNT:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE="   :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "NOTE.DEBITED="        :ID.NEW:COMMA
        OFS.MESSAGE.DATA :=  "VERSION.NAME="        :",CARD.FEES2"

        OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO2:COMMA
        OFS.REC := COMMA:OFS.MESSAGE.DATA

        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN,NEW.FILE2 ON ERROR  TEXT = " ERROR ";CALL REM

        OFS.MESSAGE.DATA  = "" ; OFS.REC = ""
    END
    RETURN
*----------------------------------------------
OFS.RECORD:
*----------
    NEW.FILE = "ACLK.REV.CARD.FEES.":R.CF<FEES.ALL.ACLK.REF.NO>
    OFS.MESSAGE.DATA  = "" ; OFS.REC = ""

    OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA
    OFS.REC := OFS.USER.INFO:COMMA:ACLK.ID

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,NEW.FILE ON ERROR  TEXT = " ERROR ";CALL REM

    OFS.MESSAGE.DATA  = "" ; OFS.REC = ""
    RETURN
*----------------------------------------------
PROCESS:
*--------
    FN.CF = "F.SCB.CARD.FEES.ALL"  ; F.CF = "" ; R.CF = ""
    CALL OPF(FN.CF, F.CF)
    FN.ACLK = "FBNK.AC.LOCKED.EVENTS"  ; F.ACLK = "" ; R.ACLK = ""
    CALL OPF(FN.ACLK, F.ACLK)

    KEY.LIST=""  ; SELECTED=""  ; ER.MSG=""

    T.SEL = "SELECT ":FN.CF:" WITH FLAG EQ 'NO' AND ACLK.REF.NO NE '' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR H = 1 TO SELECTED
            CALL F.READ(FN.CF,KEY.LIST<H>,R.CF,F.CF,E.CF)
            ACLK.ID       = R.CF<FEES.ALL.ACLK.REF.NO>
            COMP2         = R.CF<FEES.ALL.COMPANY.ID>
            CALL F.READ(FN.ACLK,ACLK.ID,R.ACLK,F.ACLK,E.ACLK)
            ACC.ID        = R.ACLK<AC.LCK.ACCOUNT.NUMBER>
            LCK.AMT       = R.ACLK<AC.LCK.LOCKED.AMOUNT>
            TEMP.ID       = R.ACLK<AC.LCK.DESCRIPTION>
            COMP          = R.ACLK<AC.LCK.CO.CODE>
            OFS.USER.INFO = "INPUTT":COMP[8,2]:"//":COMP
            ACC.BAL       = ""
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC.ID,ACC.BAL)
            IF ACC.BAL GE LCK.AMT THEN
                GOSUB INIT
                GOSUB OFS.RECORD
                GOSUB OFS.RECORD.FT
            END
        NEXT H
    END
    RETURN
*------------------------------------------------------------------
END
