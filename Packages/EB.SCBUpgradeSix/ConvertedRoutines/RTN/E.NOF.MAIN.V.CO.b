* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
******NESSREEN AHMED 26/4/2012*****************
*-----------------------------------------------------------------------------
* <Rating>196</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.MAIN.V.CO(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FLAG = 0

    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.TRNS  = 'FBNK.ACCT.ENT.TODAY' ; F.TRNS = '' ; R.TRNS = ''

    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    AMT.CR   = 0
    AMT.DR   = 0

    REQ.COMP = ID.COMPANY
    N.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 10000 AND VERSION.NAME NE CLOSED AND CO.CODE EQ  " : REQ.COMP : " BY CURRENCY "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
    ENT.NO = ''
    LOOP
        REMOVE ACCT.ID  FROM KEY.LIST.N SETTING POS
    WHILE ACCT.ID:POS
        CALL F.READ( FN.TRNS,ACCT.ID, R.TRNS,F.TRNS, ETEXT)
        CLR.AMT = 0
        LOOP
            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)
            REC.STATUS = R.STMT<AC.STE.RECORD.STATUS>

            IF REC.STATUS NE 'REVE' THEN
                CURR = R.STMT<AC.STE.CURRENCY>
                IF CURR = 'EGP' THEN
                    ST.AMT = R.STMT<AC.STE.AMOUNT.LCY>
                END ELSE
                    ST.AMT = R.STMT<AC.STE.AMOUNT.FCY>
                END
                IF ST.AMT GT 0 THEN
                    AMT.CR = ST.AMT + AMT.CR
                END
                IF ST.AMT LT 0 THEN
                    AMT.DR = ST.AMT + AMT.DR
                END
            END

        REPEAT
        CALL F.READ( FN.ACC,ACCT.ID, R.ACC,F.ACC, ETEXT1)

        OP.BAL =   R.ACC<AC.OPEN.ACTUAL.BAL>
        CL.BAL =   R.ACC<AC.ONLINE.ACTUAL.BAL>
        CURR   =   R.ACC<AC.CURRENCY>

        IF (OP.BAL#'0' AND OP.BAL#'') OR (CL.BAL#'0' AND CL.BAL#'') THEN
            Y.RET.DATA<-1> = ACCT.ID:"*":CURR:"*":OP.BAL:"*":AMT.CR:"*":AMT.DR:"*":CL.BAL
            AMT.CR = 0
            AMT.DR = 0
        END
    REPEAT

    RETURN
END
