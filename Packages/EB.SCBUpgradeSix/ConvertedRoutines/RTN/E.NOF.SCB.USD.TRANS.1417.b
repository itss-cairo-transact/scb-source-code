* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
    SUBROUTINE E.NOF.SCB.USD.TRANS.1417(Y.FT.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------

    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-------------------------------------------------------------------------
INITIATE:

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.CCY = 'F.SCB.CURRENCY.DAILY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    WS.AMT.LT = 0 ; WS.AMT.GE = 0 ; AMT.TOTAL = 0

    YTEXT = "Enter Date from : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DAT.FROM = COMI

    YTEXT = "Enter Date to : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    DAT.TO = COMI

    RETURN

*========================================================================
PROCESS:

    T.SEL = "SELECT ":FN.AC:" WITH CATEGORY IN (1417 1419 1512) AND CURRENCY NE 'EGP'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            AC.ID = KEY.LIST<I>

            CALL EB.ACCT.ENTRY.LIST(AC.ID<1>,DAT.TO,DAT.TO,STMT.ID.LIST,OPENING.BAL,ER)
            WS.AMT.FCY = OPENING.BAL
            IF WS.AMT.FCY LT 0 THEN
                WS.CCY      = R.AC<AC.CURRENCY>
                WS.CUS.CODE = R.AC<AC.CUSTOMER>

                IF WS.CCY NE 'USD' THEN
                    WS.CCY.ID1 = WS.CCY:'-EGP-':DAT.TO
                    CALL F.READ(FN.CCY,WS.CCY.ID1,R.CCY1,F.CCY,E3)
                    WS.RATE1   = R.CCY<SCCU.SELL.RATE,1>
                    WS.AMT.LCY = WS.AMT.FCY * WS.RATE1

                    WS.CCY.ID  = 'USD-EGP-':DAT.TO
                    CALL F.READ(FN.CCY,WS.CCY.ID,R.CCY,F.CCY,E3)
                    WS.RATE    = R.CCY<SCCU.SELL.RATE,1>
                    WS.AMT.USD = WS.AMT.LCY / WS.RATE
                END ELSE
                    WS.AMT.USD = WS.AMT.FCY
                END
                WS.AMT.USD = WS.AMT.USD * -1

                IF WS.AMT.USD GE 100000 THEN
                    CALL F.READ(FN.CU,WS.CUS.CODE,R.CU,F.CU,E2)
                    WS.CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                    WS.CUS.ID   = R.CU<EB.CUS.LOCAL.REF><1,CULR.COM.REG.NO>
                    WS.CUS.CBE  = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                    WS.AMT.GE  += WS.AMT.USD
                    Y.FT.DATA<-1> = WS.CUS.NAME:"*":WS.CUS.CBE:"*":WS.CUS.ID:"*":WS.AMT.USD
                END
                IF WS.AMT.USD LT 100000 THEN
                    WS.AMT.LT += WS.AMT.USD
                END
                AMT.TOTAL += WS.AMT.USD

            END
        NEXT I
    END

    TOTAL.TXT.LT = '������� ����� �� 100 ��� �����'
    Y.FT.DATA<-1> = TOTAL.TXT.LT:"*":" ":"*":" ":"*":WS.AMT.LT

    TXT.TOTAL  = '��������'
    Y.FT.DATA<-1> = TXT.TOTAL:"*":" ":"*":" ":"*":AMT.TOTAL

*********************************
    RETURN
END
