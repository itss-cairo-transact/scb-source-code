* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
 PROGRAM EOD.CD.ISL.NEW.RATE.D
*    SUBROUTINE EOD.CD.ISL.NEW.RATE.D

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

    T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY EQ 21036 AND STATUS NE LIQ AND INPUTTER LIKE ...INPUTT... "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
PRINT SELECTED
    DIM ZZZ(SELECTED)
    FOR I = 1 TO SELECTED 
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E2)
        LD.COCODE = R.LD<LD.CO.CODE>
****************************************************
        FN.OFS.SOURCE ="F.OFS.SOURCE"; F.OFS.SOURCE = ""
        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')

*       FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        F.OFS.IN          = 0
        OFS.REC           = ""
        OFS.OPERATION     = "LD.LOANS.AND.DEPOSITS"
        OFS.OPTIONS       = "CD.ISL/D"
        COMP              = R.LD<LD.CO.CODE>
        COM.CODE          = COMP[8,2]
        OFS.USER.INFO     = "AUTHOR":COM.CODE:"/":"/" :COMP
        OFS.TRANS.ID      = KEY.LIST<I>
        OFS.MESSAGE.DATA  = ""

        DIR.NAME          = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        DAT               = TODAY
        NEW.FILE          = 'CD.ISL.PER.D':".":KEY.LIST<I>

        OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
            CLOSESEQ V.FILE.IN
            HUSH ON
            EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
            HUSH OFF
            PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
        END
        OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
            CREATE V.FILE.IN THEN
                PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
            END
            ELSE
                STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
            END
        END

        COMMA        =  "," 
        ZZZ(I) = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
*       PRINT KEY.LIST<I>:'*':NEW.INT:"*":NEW.INT.V.DATE
        WRITESEQ ZZZ(I) TO V.FILE.IN ELSE
            PRINT  'CAN NOT WRITE LINE ':ZZZ(I)
        END

    NEXT I

    RETURN
END
