* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>-49</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.REPORT.PRT2.ALL

*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_COMMON
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_EQUATE
*Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_F.HOLD.CONTROL
*Line [ 29 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_F.USER
*Line [ 31 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_F.COMPANY
*Line [ 35 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-13
$INSERT I_F.SCB.HOLD.CTRL.TODAY
*-----------------------------------------------------------------------

    COMMON /ENQHARES001/ C$REP.NAME, C$REP.TITLE, C$D1, C$D2, C$D3, C$D4, C$DALL
    COMMON /ENQHARES002/ PRINTER.ID, DEVICE.ID, REPORT.ID, HOLD.ID, REP.TITLE
    COMMON /ENQHARES003/ STR.ONE, STR.TWO, COMP.ALL, WS.COMP
*-----------------------------------------------------------------------
    WS.COMP = ID.COMPANY

**-------------------------
    IF WS.COMP  NE 'EG0010099' THEN
        COMP.ALL = ''
    END
    IF WS.COMP  EQ 'EG0010099' THEN
        IF COMP.ALL EQ 'ALL'   THEN
            DEP.CODEX = STR.ONE[10,6]
            STR.ONE = "...":DEP.CODEX:"..."
        END
    END
* --------------------------------
    WS.USER.DSP = 'N'
* --------------------------------
    FN.HCTODAY = "F.SCB.HOLD.CTRL.TODAY"
    F.HCTODAY  = ""
    R.HCTODAY = ""
    Y.HCTODAY.ID   = ""
    CALL OPF(FN.HCTODAY,F.HCTODAY)

*-----------------------------------------------------------------------
    FN.HOLD = "F.HOLD.CONTROL"
    F.HOLD  = ""
    R.HOLD = ""
    Y.HOLD.ID   = ""
    CALL OPF(FN.HOLD,F.HOLD)

*   ----------------------------
    SYS.DATE = TODAY
    WS.DATE = SYS.DATE
    CALL CDT('',WS.DATE,'-1W')
*    WS.DATE = 20081231
*-----------------------------------------------------------------------
*Line [ 69 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2.DEV

*Line [ 72 ] Adding EB.SCBUpgradeSix. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeSix.E.REPORT.LIST.2

    GOSUB READ.HCTODAY.FILE


    RETURN
*--------------------------------------------------------------------
READ.HCTODAY.FILE:


    HOLD.ID = ''

*    HOLD.ID = 'doc.DEP001.doc'
*    EXECUTE "E.REPORT.PRT2X"
*    STR.ONE = "EG0010006DEP005..."
*-----------------

    SEL.HOLD  = "SELECT ":FN.HCTODAY:" WITH @ID LIKE ":STR.ONE
    SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE:" BY @ID"




*------------------------------------------
**                                               ��� ����� ����� �������� ��� ���� �����
**                                          ���� �������� ������ ��� ��� ������� ����
    IF WS.COMP  EQ 'EG0010099' THEN
        IF COMP.ALL EQ 'ALL'   THEN
            IF DEP.CODEX EQ 'DEP006' THEN
                SEL.HOLD  = "SELECT ":FN.HCTODAY:" WITH @ID LIKE ":STR.ONE
                SEL.HOLD := " AND BANK.DATE EQ ":WS.DATE:" BY REPORT.NAME"
            END
        END
    END
*------------------------------------------



    CALL EB.READLIST(SEL.HOLD,SEL.LIST.HOLD,'',NO.OF.REP,ERR.HOLD)
    LOOP
        REMOVE Y.HCTODAY.ID FROM SEL.LIST.HOLD SETTING POS.HOLD
    WHILE Y.HCTODAY.ID:POS.HOLD
        CALL F.READ(FN.HCTODAY,Y.HCTODAY.ID,R.HCTODAY,F.HCTODAY,ERR.HOLD)




        HOLD.ID = R.HCTODAY<HCT.HOLD.ID>

*        IF WS.USER.DSP = 'Y' THEN
*            TEXT = REP.TITLE ;CALL REM
*        END


***        TEXT = REP.TITLE ;CALL REM

        EXECUTE "E.REPORT.PRT2X"


    REPEAT


    RETURN
*--------------------------------------------------------------------
