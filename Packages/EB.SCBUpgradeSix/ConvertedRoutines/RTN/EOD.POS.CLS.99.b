* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSix  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSix
*DONE
*-----------------------------------------------------------------------------
* <Rating>232</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE EOD.POS.CLS.99

*    PROGRAM EOD.POS.CLS.99

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

*----------------------------------------------------

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'EOM',R.BASE,F.BASE,E3)

    KEY.LIST.AC = "" ; SELECTED.AC = "" ;  ER.MSG = ""

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    T.SEL.AC = " SELECT FBNK.ACCOUNT WITH CATEGORY EQ 11120  AND ( ONLINE.ACTUAL.BAL NE '' AND ONLINE.ACTUAL.BAL NE 0 )  AND CO.CODE EQ 'EG0010099' BY CURRENCY "

    CALL EB.READLIST(T.SEL.AC,KEY.LIST.AC,"",SELECTED.AC,ER.MSG.C)

    COMP  = ID.COMPANY[8,2]

    IF KEY.LIST.AC THEN

        FOR I = 1 TO SELECTED.AC

            CALL F.READ(FN.ACC,KEY.LIST.AC<I>,R.ACC,F.ACC,READ.ERRC)

            CURR   = R.ACC<AC.CURRENCY>
            AMTT   = R.ACC<AC.ONLINE.ACTUAL.BAL>

            CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END


            Y.ACCT     = KEY.LIST.AC<I>
            CUR.CODE   = CURR
            FCY.AMTT   = AMTT * -1
            LCY.AMTT   = (AMTT * RATE ) * -1
            REFF       = "CLOSS-":'EGP'
            RATEE      = RATE

            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)
            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)


            GOSUB AC.STMT.ENTRY
            PRINT CUR.CODE:"*":LCY.AMTT

            LOC.AMTT   = AMTT * RATE
            CUR.CODE   = 'EGP'
            Y.ACCT     = "EGP1112100010099"
            LCY.AMTT   = LOC.AMTT
            FCY.AMTT   = ""
            REFF       ="CLOSS-":CURR
            RATEE      = ''

            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)

            IF CURR EQ 'USD' THEN

                FIN.AMT = LCY.AMTT

                CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,Y.ACCT,AC.FIN.BAL)
                DIF.AMT  = AC.FIN.BAL + FIN.AMT

                IF ( DIF.AMT GE -5  AND DIF.AMT LE 5 ) THEN

                    LCY.AMTT = LOC.AMTT - DIF.AMT
                END
            END

            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")

            GOSUB AC.STMT.ENTRY

            PRINT CUR.CODE:"*":LCY.AMTT

            AMMM = AMMM + LCY.AMTT

        NEXT I

        PRINT "AMMM = " : AMMM
    END

*=================================================================================================
*    KEY.LIST.AC1 = "" ; SELECTED.AC1 = "" ;  ER.MSG1 = ""

    FN.ACC1 = 'FBNK.ACCOUNT' ; F.ACC1 = ''
    CALL OPF(FN.ACC1,F.ACC1)

    Y.ACCT = "EGP1112100010099"
    CALL F.READ(FN.ACC1,Y.ACCT,R.ACC1,F.ACC1,READ.ERRC1)

    IF NOT(READ.ERRC1) THEN
        CURR   = R.ACC1<AC.CURRENCY>
        AMTT   = R.ACC1<AC.ONLINE.ACTUAL.BAL>

        LOC.AMTT   = AMTT
        CUR.CODE   = 'EGP'
        LCY.AMTT   = LOC.AMTT
        FCY.AMTT   = ""
        REFF       ="CLOSS.DIFF-EGP"
        RATEE      = ''

        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")
        ACC.OFFICER = R.ACC1<AC.ACCOUNT.OFFICER>
        CATEG = R.ACC1<AC.CATEGORY>
        LCY.AMTT = DROUND(LCY.AMTT,2)
        IF LCY.AMTT NE 0 AND LCY.AMTT NE '' THEN
            GOSUB CA.STMT.ENTRY

            LOC.AMTT   = AMTT * -1
            LCY.AMTT   = LOC.AMTT
            FCY.AMTT   = ""
            LCY.AMTT = DROUND(LCY.AMTT,2)
            GOSUB AC.STMT.ENTRY
        END
    END
    RETURN

*-------------------------------------------------------------------
AC.STMT.ENTRY:
**********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '777'
    ENTRY<AC.STE.THEIR.REFERENCE>  = "POS-CLS-":CUR.CODE
    ENTRY<AC.STE.TRANS.REFERENCE>  = "POS-CLS-":CUR.CODE
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMTT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CUR.CODE
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMTT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATEE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = REFF

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN
************************************************************
*-------------------------------------------------------------------
CA.STMT.ENTRY:
**********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
******************************************
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ""
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = "777"
    ENTRY<AC.STE.THEIR.REFERENCE>  = "CLOSE-EGP"
    ENTRY<AC.STE.TRANS.REFERENCE>  = "CLOSE-EGP"
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = "53088"
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMTT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = ""
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ''
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = "CLOSE-EGP":TODAY[1,6]

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
   CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN
END
