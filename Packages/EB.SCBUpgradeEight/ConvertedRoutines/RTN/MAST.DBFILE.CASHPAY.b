* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>206</Rating>
*-----------------------------------------------------------------------------
  SUBROUTINE MAST.DBFILE.CASHPAY
**WRITTEN BY NESSREEN AHMED 10/06/2019

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.MNTLY.PAY.AMT
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.PAYMENT

*A REPORT PRINT VISA.ACCOUNT IN "SCB.MAST.MNTLY.PAY.AMT" AND MAKE A CASH PAIED IN SCB.MAST.DAILY.PAYMENT A DAY BEFORE

    REQ.COMP = ''  ; YYYYMM = ''
    REQ.COMP = ID.COMPANY
** TEXT = 'COMPANY=':REQ.COMP ; CALL REM

    YYYYMM = TODAY[1,6]
**TEXT = 'YEAR.MM=':YYYYMM ; CALL REM

    CHK.SEL = "SELECT F.SCB.MAST.PAY.FLAG WITH COMPANY.CO EQ 'EG0010099' AND PAYMENT.FLAG EQ 'YES' AND YEAR.MM EQ ":YYYYMM
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF NOT(SELECTED.CHK) THEN
        E = '��� ����� �������� ����' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        GOSUB INITIATE
        GOSUB PRINT.HEAD
*Line [ 75 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 76 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
        GOSUB CALLDB
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        RETURN
    END
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    YYYYMM = ''

    YYYYMM = TODAY[1,6]
**    TEXT = 'YEAR.MM=':YYYYMM ; CALL REM
    MDATE = TODAY
    CALL CDT("",MDATE,'-1W')
    PYDATE = MDATE
**   TEXT = 'PYDATE=':PYDATE ; CALL REM

    RETURN
*===============================================================
CALLDB:

    FN.MNT.PAY = 'F.SCB.MAST.MNTLY.PAY.AMT' ; F.MNT.PAY = '' ; R.MNT.PAY = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.MNT.PAY,F.MNT.PAY)

    FN.DLY.PAY = 'F.SCB.MAST.DAILY.PAYMENT' ; F.DLY.PAY = '' ; R.DLY.PAY = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.DLY.PAY,F.DLY.PAY)

    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY3 = '' ; E3 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.TELLER = 'F.TELLER' ; F.TELELR = '' ; R.TELLER = '' ; RETRY1 = '' ; E.TT = ''
    CALL OPF(FN.TELLER,F.TELLER)

    FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E.FT = ''
    CALL OPF(FN.FT,F.FT)

    TEXT = 'YYYYMM=':YYYYMM ; CALL REM
    TEXT = 'REQ.COMP=':REQ.COMP ; CALL REM
    T.SEL = "SELECT F.SCB.MAST.MNTLY.PAY.AMT WITH YEAR.MONTH EQ ":YYYYMM :" AND COMPANY.CO EQ ": REQ.COMP : " BY VISA.NO "
***************************************************************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    TEXT = '��� �� ��� ������=':SELECTED ; CALL REM
    XX= '' ; ZZ = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            VISA.NO = '' ; DB.ACCT = '' ; CR.ACCT = '' ; DB.AMT = '' ; CUST = '' ; CUST.NAM = ''
            REF.NO  = '' ; DB.ACCT.N = '' ; DB.AMT.N = '' ; CC = 0

            KEY.TO.USE = KEY.LIST<I>

            CALL OPF(FN.MNT.PAY,F.MNT.PAY)
            CALL F.READ(FN.MNT.PAY, KEY.TO.USE, R.MNT.PAY, F.MNT.PAY, E1)

            VISA.NO  = R.MNT.PAY<MMP.VISA.NO>
            DB.ACCT  = R.MNT.PAY<MMP.DB.ACCT.NO>
            CR.ACCT  = R.MNT.PAY<MMP.CR.ACCT.NO>
            DB.AMT   = R.MNT.PAY<MMP.DEBIT.AMT>

*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER,DB.ACCT, CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)
            CUST.NAM = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>

            N.SEL = "SELECT F.SCB.MAST.DAILY.PAYMENT WITH TRANS.DATE EQ " : PYDATE : " AND TRANS.TYPE NE SETT AND CARD.NO EQ ": VISA.NO
            KEY.LIST.N=""
            SELECTED.N=""
            ER.MSG.N=""

            KEY.TO.USE = ''
            CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
            IF SELECTED.N THEN
                FOR NN = 1 TO SELECTED.N
                    KEY.TO.USE = KEY.LIST.N<NN>
                    CALL OPF(FN.DLY.PAY,F.DLY.PAY)
                    CALL F.READ(FN.DLY.PAY, KEY.TO.USE, R.DLY.PAY, F.DLY.PAY, E2)

                    REF.NO      = R.DLY.PAY<MST.PYM.REFER.NO>
                    CR.ACCT.N   = R.DLY.PAY<MST.PYM.CUST.ACCT>
                    DB.AMT.N    = R.DLY.PAY<MST.PYM.AMOUNT>

                    N1 = VISA.NO[1,6]
                    N2 = VISA.NO[13,4]
                    VISA.NO.MSK = N1:"XXXXXX":N2

                    XX<1,ZZ>[1,16] = VISA.NO.MSK
                    XX<1,ZZ>[20,35]= CUST.NAM
                    XX<1,ZZ>[60,16]= CR.ACCT.N
                    XX<1,ZZ>[85,16]= REF.NO
                    XX<1,ZZ>[110,8]= DB.AMT.N
                    PRINT XX<1,ZZ>
                    ZZ=ZZ+1
                    XX = '' ; VISA.NO.MSK = '' ; CUST.NAM = '' ; CR.ACCT.N = '' ; REF.NO = '' ; DB.AMT.N = ''
                NEXT NN
                PRINT STR('-',132)
                CC = CC + 1
            END
*******TELLER LIVE*****************************************************************************************
            TT.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE EQ 38 AND VISA.NUMBER EQ " : VISA.NO
            KEY.LIST.TT = ""
            SELECTED.TT = ""
            ER.MSG.TT = ""

            CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
            IF SELECTED.TT THEN
                FOR TT = 1 TO SELECTED.TT
                    KEY.TO.USE.TT = KEY.LIST.TT<TT>
                    CALL F.READ(FN.TELLER,  KEY.TO.USE.TT, R.TELLER, F.TELLER, E.TT)
                    REF.NO      = KEY.LIST.TT<TT>
                    CR.ACCT.N   = R.TELLER<TT.TE.ACCOUNT.1>
                    DB.AMT.N    = R.TELLER<TT.TE.NET.AMOUNT>
                    N1 = VISA.NO[1,6]
                    N2 = VISA.NO[13,4]
                    VISA.NO.MSK = N1:"XXXXXX":N2

                    XX<1,ZZ>[1,16] = VISA.NO.MSK
                    XX<1,ZZ>[20,35]= CUST.NAM
                    XX<1,ZZ>[60,16]= CR.ACCT.N
                    XX<1,ZZ>[85,16]= REF.NO
                    XX<1,ZZ>[110,8]= DB.AMT.N
                    PRINT XX<1,ZZ>
                    ZZ=ZZ+1
                    XX = '' ; VISA.NO.MSK = '' ; CUST.NAM = '' ; CR.ACCT.N = '' ; REF.NO = '' ; DB.AMT.N = ''
                NEXT TT
                PRINT STR('-',132)
                CC = CC + 1
            END     ;*END OF SELECTED.TT
**********FUNDS.TRANSFER LIVE****************************************************************************************
            FT.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVM' AND VISA.NO EQ " : VISA.NO
            KEY.LIST.FT=""
            SELECTED.FT=""
            ER.MSG.FT=""

            CALL EB.READLIST(FT.SEL,KEY.LIST.FT,"",SELECTED.FT,ER.MSG.FT)
            IF SELECTED.FT THEN
                FOR FF = 1 TO SELECTED.FT
                    FT.ID = KEY.LIST.FT<FF>
                    CALL F.READ(FN.FT,  FT.ID, R.FT, F.FT, E2)
                    REF.NO      = FT.ID
                    CR.ACCT.N   = R.FT<FT.CREDIT.ACCT.NO>
                    DB.AMT.N    = R.FT<FT.DEBIT.AMOUNT>
                    N1 = VISA.NO[1,6]
                    N2 = VISA.NO[13,4]
                    VISA.NO.MSK = N1:"XXXXXX":N2

                    XX<1,ZZ>[1,16] = VISA.NO.MSK
                    XX<1,ZZ>[20,35]= CUST.NAM
                    XX<1,ZZ>[60,16]= CR.ACCT.N
                    XX<1,ZZ>[85,16]= REF.NO
                    XX<1,ZZ>[110,8]= DB.AMT.N
                    PRINT XX<1,ZZ>
                    ZZ=ZZ+1
                    XX = '' ; VISA.NO.MSK = '' ; CUST.NAM = '' ; CR.ACCT.N = '' ; REF.NO = '' ; DB.AMT.N = ''
                NEXT FF
                PRINT STR('-',132)
                CC = CC + 1
            END     ;* END OF SELECTED.FT

        NEXT I
    END
    PRINT
**********************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 252 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

    COMP.NAME = ''
*Line [ 261 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,REQ.COMP,BR.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,REQ.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BR.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    TEXT = 'COMP.NAME=':BR.NAME ; CALL REM
    YYBRN = BR.NAME

    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN

    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):"���� �������� ����� ����� ������� ������� ������� � ����� ������� ������"
    PR.HD :="'L'":SPACE(50):"���� ����� � ��� ��� ����"
    PR.HD :="'L'":SPACE(28):STR('_',80)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ���������"::SPACE(10):"����� ��������":SPACE(22):"���� �������":SPACE(14):"���� �������":SPACE(10):"���� ������":SPACE(1)
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(9):STR('_',14):SPACE(20):STR('_',15):SPACE(11):STR('_',12):SPACE(8):STR('_',10)
    PR.HD := "'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================

END
