* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
***********ABEER***********
*-----------------------------------------------------------------------------
* <Rating>-33</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.AMAMTDATE.1231.CU2
*  PROGRAM LG.AMAMTDATE.1231.CU2


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        MYOLDCODE = R.OLD(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        IF MYCODE='1231' AND MYOLDCODE='1239' THEN
            GOSUB INITIATE
*Line [ 55 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 56 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            GOSUB PRINT.HEAD
            GOSUB BODY
        END
    END ELSE
        IF ID.NEW EQ '' THEN
            GOSUB INITIATE
*Line [ 63 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 65 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            IF MYCODE='1231' AND MYOLDCODE EQ '1239' THEN
                GOSUB PRINT.HEAD
                GOSUB BODY
            END  ELSE
                MYID=MYCODE:'.':MYTYPE
** CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
                OPER.CODE=MYID
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.1231.CU2'
    CALL PRINTER.ON(REPORT.ID,'')
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
    RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN  YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN  CALL TXTINP(YTEXT, 8, 22, "12", "A")

    IF ID.NEW THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>

    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
**THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
*   DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    CURRNO = R.LD<LD.CURR.NO>
    CURRNO1= CURRNO - 1
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
******

    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
*************************************
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
*Line [ 152 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 160 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT************************************
    IF ID.NEW EQ '' THEN
        IDHIS='';ID='';OLD.ID=''
        IDHIS=COMI:";":CURRNO1
        ID=IDHIS
        OLD.ID=IDHIS

        FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
        CALL OPF(FN.LG.HIS,F.LG.HIS)
        CALL F.READ(FN.LG.HIS,IDHIS, R.LG.HIS, F.LG.HIS ,ETEXT)
        LOCAL.REF.HIS=R.LG.HIS<LD.LOCAL.REF>
    END
***************************************************************************


    IF ID.NEW EQ '' THEN

        MYOLDCODE=LOCAL.REF.HIS<1,LDLR.OPERATION.CODE>
        OLD.FIN=LOCAL.REF.HIS<1,LDLR.ACTUAL.EXP.DATE>
        FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
    END ELSE
        IF ID.NEW THEN
            OLD.FIN=R.OLD(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
        END
    END
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT=R.LG.HIS<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT=R.OLD(LD.AMOUNT)
        END
    END
**    CALL F.RELEASE(FN.LG.HIS,OLD.ID,F.LG.HIS)
***************GET AMOUNT INC OR DEC************************************
*        FN.LG='F.LD.LOANS.AND.DEPOSITS';F.LG='';R.LG='';F.LG=''
*        CALL OPF(FN.LG,F.LG)
*        CALL F.READ(FN.LG,ID, R.LG, F.LG ,ETEXT)
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END

*        CALL F.RELEASE(FN.LG,ID,F.LG)
**********************************************************************************
    AMT.DEC=OLD.AMOUNT-NEW.AMOUNT
    AMT.INC=NEW.AMOUNT-OLD.AMOUNT
*    END
**********************************************************************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>
    RETURN
*===============================================================
PRINT.HEAD:
*     MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*     MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

** CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    PR.HD ="'L'":SPACE(1):"��� ���� �����������"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
*Line [ 240 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************

*Line [ 251 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":YY
* PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(28):"������ ���":"*":EXT.NO:"*"
    HEADING PR.HD

    RETURN
*==================================================================
BODY:

    LNTH1 = LEN(THIRD.NAME1)+12 ; LNTH2 = LEN(THIRD.NAME2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(THIRD.ADDR1)+12 ; LNTHD2 = LEN(THIRD.ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2

    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":THIRD.NAME1:SPACE(LNE1):"|"
    PRINT "|         : ":THIRD.NAME2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
* PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ��� � �    : ":LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "������������     : ":" **":NEW.AMOUNT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
    PRINT SPACE(20): "������� � �����   : ":BENF1
    PRINT SPACE(20): "                  : ":BENF2
    PRINT
    PRINT SPACE(20):"���� ������� �����: " : FIN.OLD
*  END
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(4):"������ ������� ����� ������ ": CORR.DATE :"����� �� ���� ���� �� �� ����"
    PRINT
    PRINT SPACE(2):"��� ������ ."
    PRINT
    PRINT SPACE(2):"����� �������� ��� ���� ������ ��������� ��� ���� ���� ������ ������ ������"
    PRINT
    PRINT SPACE(2):"�� �� ��� ������� ���" : FIN.DATE:"."
    PRINT
    PRINT SPACE(2):"������� ����� ������ ���� ��� ������� ����� �������� � ������� ����� ������"
    PRINT
    PRINT SPACE(2):"���� ����� ."
    PRINT
    PRINT SPACE(2):"����� ������ ��������"
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":XX
    PRINT SPACE(5):"LG.AMAMTDATE.1231.CU2"
*Line [ 287 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==============================================================
