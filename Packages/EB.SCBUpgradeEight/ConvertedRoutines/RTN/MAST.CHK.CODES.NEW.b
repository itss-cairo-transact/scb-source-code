* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>564</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  MAST.CHK.CODES.NEW

*WRITTEN BY NESSREEN AHMED / SCB

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.TRANS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.CODES
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.TRN
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.ROUTINE.CHK



**    Path = "/life/MCTEST/NT24/bnk/bnk.run/RPT.HOLD/MASTUSG"
    Path = "NESRO/MASTUSG"

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    F.MAST.TRANS = '' ; FN.MAST.TRANS = 'F.SCB.MAST.TRANS' ; R.MAST.TRANS = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.MAST.TRANS,F.MAST.TRANS)

****************UPDATED 17/2/2008*************************************
**    YTEXT = "Enter the Date : "
**    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    F.RT.CHK = '' ; FN.RT.CHK = 'F.SCB.MAST.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)

    DATEE = '' ; YEAR1 = '' ; MONTH1= ''
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YY = YYYY-1
    END ELSE
        MU = MT-1
        YY = YYYY
    END

    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END

    YYDD = YY:MON:'01'
    YEAR1= YY
    MONTH1 = MON
    BR = R.USER<EB.USE.DEPARTMENT.CODE>


    EOF = ''
    LOOP WHILE NOT(EOF)
        TRN.CARD.NO = '' ; TRN.FLAG = '' ; TRN.FLAG = '' ; POST.DATE = '' ; TRN.DATE = ''
        TRANS.CURR = '' ; TRN.TYPE = '' ; TRANS.AMT = '' ; TRN.DESC = ''  ; CC.TRN.CARD.NO = ''

        READSEQ Line FROM MyPath THEN
            CARD.NO           = Line[1,16]
            T.SEL =  "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""

            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
                    KEY.TO.USE = KEY.LIST<I>
                    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                    CALL F.READU(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1, RETRY1)
                    LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                    CUST.DEPT = LOCAL.REF<1,LRCI.BRANCH.NO>
                    BANK.ACC =  R.CARD.ISSUE<CARD.IS.ACCOUNT>
                    CUST.NAME = R.CARD.ISSUE<CARD.IS.NAME>
                NEXT I
                TRN.CARD.NO         = Line[20,16]
                TRN.FLAG            = Line[39,1]
                POST.DATE.N         = Line[49,6]
                POST.DATE           = '20':POST.DATE.N
                TRN.DATE.N          = Line[57,6]
                TRN.DATE            = '20':TRN.DATE.N
                TRANS.CURR          = Line[76,3]
                TRN.TYPE            = Line[79,2]
                TRANS.AMT           = Line[97,15]
                TRANS.AMT.1 = TRIM(TRANS.AMT, "0", "L")
                TRANS.AMT.FMT = TRANS.AMT.1/100
                TRN.DESC          = Line[113,35]
                CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN TRN.DESC


                N.SEL ="SELECT F.SCB.MAST.CODES WITH TRN.TYPE EQ ": "'":TRN.TYPE:"'"
                KEY.LIST.2=""
                SELECTED.2=""
                ER.MSG.2=""
                CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
                F.MAST.CODE = '' ; FN.MAST.CODE = 'F.SCB.MAST.CODES' ; R.MAST.CODE = '' ; E3 = '' ; RETRY3 = ''
                CALL OPF(FN.MAST.CODE,F.MAST.CODE)
                IF NOT(SELECTED.2) THEN
                    IF TRN.TYPE # '80' THEN TEXT = 'NEW CODE=':TRN.TYPE ; CALL REM
                END
**********************************************************************
            END ELSE          ;**END OF SELECTED***
            END
        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath
    TEXT = 'END OF FILE' ; CALL REM
**********************************************************
    RETURN
END
