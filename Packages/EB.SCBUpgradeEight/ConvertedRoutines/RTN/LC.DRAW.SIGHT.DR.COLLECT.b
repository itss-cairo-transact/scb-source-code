* @ValidationCode : MjotNDI0ODczMDQ6Q3AxMjUyOjE2NDIxMDQ5Nzk1ODQ6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 13 Jan 2022 22:16:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
******************************NI7OOOOOOOOOOOOO******************
*-----------------------------------------------------------------------------
* <Rating>-84</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LC.DRAW.SIGHT.DR.COLLECT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_DR.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='LC.DRAW.SIGHT.DR.COLLECT'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
* BT.ID = ID.NEW
    BT.ID = COMI
    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    FN.DR.HIS = 'FBNK.DRAWINGS$HIS' ; F.DR.HIS = ''
    CALL OPF(FN.DR.HIS,F.DR.HIS)

    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)
    DEBIT.AC = R.DR<TF.DR.DRAWDOWN.ACCOUNT>
    ACC.NO = DEBIT.AC
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

**    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*UPDATED BY HYTHAM ---UPGRADING R15---2016-03-07---
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
    ACC.BR  = AC.COMP[8,2]
**********

    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ " " THEN
        CUST.ADDRESS = "���� ��������� ������ "
    END
    CATEG.ID  = ACC.NO[11,4]
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>

    AMOUNT     = R.DR<TF.DR.REIMBURSE.AMOUNT>
    IN.AMOUNT  = AMOUNT
    IF IN.AMOUNT  = 0 THEN
        OUT.AMOUNT = ''
    END
    IF IN.AMOUNT NE 0 THEN
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT    = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    END

    NO.OF.DAYS = R.DR<TF.DR.NUMBER.OF.DAYS>
*Line [ 150 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DRAWINGS':@FM:TF.DR.LOCAL.REF,COMI,NEW.LOC)
    F.ITSS.DRAWINGS = 'FBNK.DRAWINGS'
    FN.F.ITSS.DRAWINGS = ''
    CALL OPF(F.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS)
    CALL F.READ(F.ITSS.DRAWINGS,COMI,R.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS,ERROR.DRAWINGS)
    NEW.LOC=R.ITSS.DRAWINGS<TF.DR.LOCAL.REF>
    NOTES       = NEW.LOC<1,DRLR.NOTES.DEBIT>
    NOTES1      = NEW.LOC<1,DRLR.NOTES.DEBIT,1>
    NOTES2      = NEW.LOC<1,DRLR.NOTES.DEBIT,2>
    NOTES3      = NEW.LOC<1,DRLR.NOTES.DEBIT,3>
    NOTES4      = NEW.LOC<1,DRLR.NOTES.DEBIT,4>
    REFERENCE   = NEW.LOC<1,DRLR.REFRENCE.DEBIT>
    REF2        = NEW.LOC<1,DRLR.REFRENCE>

    T.SEL = "SELECT FBNK.DRAWINGS WITH @ID EQ " : COMI
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.DR,KEY.LIST,R.DR,F.DR,E1)
    REFERENCE  = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE.DEBIT>
    REF22      = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE>
** DAT        = R.DR<TF.DR.VALUE.DATE>
    DAT        = R.DR<TF.DR.MATURITY.REVIEW>
    V.DATE     = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    YY.ID = COMI:';1'
    CALL F.READ(FN.DR.HIS,YY.ID,R.DR.HIS,F.DR.HIS,E1)
    INPUTTER   = R.DR.HIS<TF.DR.INPUTTER>
    INP        = FIELD(INPUTTER,'_',2)
    AUTH       = R.DR.HIS<TF.DR.AUTHORISER>
    AUTHI      = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)   ;XX12  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)   ;XX13  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)   ;XX14  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)   ;XX15  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)   ;XX16  = SPACE(132)
    XX10 = SPACE(132)  ; XX11  = SPACE(132)  ;XX17  = SPACE(132) ;XX18  = SPACE(132)
*--------------------------------------------------------------
    IF CATEG.ID EQ 1512 THEN
***LOAN.AMT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.MARG.AMOUNT>
        LOAN.AMT = NEW.LOC<1,DRLR.MARG.AMOUNT>
        DAT1     = NEW.LOC<1,DRLR.NO.DAY>
        LOAN.DATE = DAT1[7,2]:'/':DAT1[5,2]:"/":DAT1[1,4]
        XX11<1,1>[3,15]  = '���� �����     : ':' ':LOAN.AMT
        XX11<1,1>[45,35] = '������ ����� �� : ':' ':LOAN.DATE
    END
*--------------------------------------------------------------
    XX1<1,1>[5,35]   = CUST.NAME
    XX1<1,1>[5,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = DEBIT.AC

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ����� ������ ����� : '
    XX4<1,1>[70,15] = V.DATE

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15]  = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15]  = COMI

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = INP
*-------------------------------------------
    XX10<1,1>[3,35] = '��������       : '
    XX14<1,1>[3,35] =  REFERENCE
    XX15<1,1>[3,35] =  NOTES1
    XX16<1,1>[3,35] =  NOTES2
    XX17<1,1>[3,35] =  NOTES3
**    XX18<1,1>[3,35] = "������: "  : REFERENCE
*-------------------------------------------
    XX8<1,1>[3,35]  = '������ ������� : '
    XX9<1,1>[3,35] = OUT.AMT
    XX12<1,1>[3,35] = "����� ����� ����� �� " : NO.OF.DAYS

*-------------------------------------------------------------------
*Line [ 236 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACC.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":"������ ������ ":' - ':"SIGHT PAYMENT"
    PR.HD :="'L'":" "
    PR.HD :="'L'": CUST.NAME
    TEXT = "CUST.NAME = " : CUST.NAME ; CALL REM
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX1<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX11<1,1>
    PRINT XX10<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
