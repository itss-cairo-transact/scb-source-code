* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
********* WAEL ********
*-----------------------------------------------------------------------------
* UPDATED BY RIHAM YOUSSEF 8/4/2015
*-----------------------------------------------------------------------------
    SUBROUTINE LG.CANCEL.MOSTAFED

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*Line [ 45 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 46 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
    GOSUB CALLDB
    IF CANRES EQ 'CUSTOMER' THEN
        GOSUB INITIATE
        GOSUB PRINT.HEAD
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
*==============================================================
INITIATE:
    REPORT.ID      = 'LG.CANCEL.MOSTAFED'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    IF ID.NEW       = '' THEN
        FN.LD       = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT       = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD       = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
    LOCAL.REF       = R.LD<LD.LOCAL.REF>
    BENF1           = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2           = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1           = LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2           = LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO        = LOCAL.REF<1,LDLR.THIRD.NUMBER>
    MYCODE          = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE          = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    PURPOSE         = LOCAL.REF<1,LDLR.IN.RESPECT.OF>
    AC.NUM          = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    CANRES          = LOCAL.REF<1,LDLR.CANCEL.REASON>
*************************************
    FN.AC           = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC         = R.AC<AC.CUSTOMER>
*************************************
    THIRD.NO          = LOCAL.REF<1,LDLR.THIRD.NUMBER>
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    IF THIRD.NO EQ CUST.AC THEN
        THIRD.NAME1   = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2   = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1   = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2   = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    END ELSE
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        THIRD.NAME1   = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2   = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1   = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2   = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        LG.CUST1      = LOC.REF<1,CULR.ARABIC.NAME>
        LG.CUST2      = LOC.REF<1,CULR.ARABIC.NAME.2>
    END
    LG.NO             = LOCAL.REF<1,LDLR.OLD.NO>
    LG.ISSUE.DATE     = LOCAL.REF<1,LDLR.ISSUE.DATE,1>
    LG.MAT.DATE       = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

    LG.APPROVAL.DATE  = LOCAL.REF<1,LDLR.APPROVAL.DATE>
    LG.ACCT           = LOCAL.REF<1,LDLR.DEBIT.ACCT,1>
    IF ID.NEW EQ '' THEN
        LG.AMT        = R.LD<LD.AMOUNT>
        LG.AMT.OLD    = R.LD<LD.AMOUNT>
    END ELSE
        LG.AMT        = R.NEW(LD.AMOUNT)
        LG.AMT.OLD    = R.LD<LD.AMOUNT>
    END
    IF ID.NEW EQ '' THEN
        AMT.INC       = ABS(R.LD<LD.AMOUNT.INCREASE>)
    END ELSE
        AMT.INC       = ABS(R.NEW(LD.AMOUNT.INCREASE))
    END
    CUR               = R.LD<LD.CURRENCY>
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.TYPE           = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 153 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME           = TYPE.NAME
    DATX              = LG.ISSUE.DATE
    XX                = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY              = LG.MAT.DATE
    LG.MAT.DATE       = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    YY                = LG.MAT.DATE[7,2]:'/':LG.MAT.DATE[5,2]:'/':LG.MAT.DATE[1,4]
    DATZ              = TODAY
    CDATE             = LOCAL.REF<1,LDLR.END.COMM.DATE>
    CCDATE            = CDATE[7,2]:'/':CDATE[5,2]:"/":CDATE[1,4]
    ZZ                = DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ              = LG.APPROVAL.DATE
    QQ                = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]
*---------------------------------UPDATED BY NOHA 10/04/2009---
    LG.DATE.NEW       = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    YY.NEW            = LG.DATE.NEW[7,2]:'/':LG.DATE.NEW[5,2]:"/":LG.DATE.NEW[1,4]
*---------------------------------------------------------------
    D.TIME            = LG.APPROVAL.DATE
    WWW               = D.TIME[7,2]:'/':D.TIME[5,2]:'/':D.TIME[1,4]
    MYCODE           = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE           = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>


    RETURN
*===============================================================
PRINT.HEAD:
    MYID             = MYCODE:'.':MYTYPE

*Line [ 194 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
*Line [ 204 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD ="'L'":SPACE(1):"����������":":":MYBRANCH
    PR.HD :="'L'":SPACE(1):STR('_',22)
    PR.HD :="'L'":SPACE(1):"��� ":ZZ
    HEADING PR.HD
    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2
    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� :  � � " :LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(17): "���������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "������� ������     : ":THIRD.NAME1
    IF THIRD.NAME2 THEN
        PRINT SPACE(17): "                  : ":THIRD.NAME2
    END ELSE
        PRINT SPACE(17): ""
    END
    PRINT
    IF THIRD.NO NE CUST.AC THEN
        PRINT SPACE(17): "��������������    : ":LG.CUST1
        IF LG.CUST2 THEN
            PRINT SPACE(17): "                  : ":LG.CUST2
        END ELSE
            PRINT SPACE(17): ""
        END
    END
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT
*=============
    PRINT SPACE(5):"���� ���� ���� ,"
    PRINT
    PRINT SPACE(5):"����� �������� ��� ������� �� ������ ����� ��� ������ ������ ������ ���������� "
    PRINT
    PRINT SPACE(2):"�� ���� ������� �� ������� ���� ������� ����� ������ �� ����."
    PRINT
    PRINT SPACE(2):"��� ��� ���� ������ ����� ������ ���� ������ �� �������"
    PRINT
    PRINT SPACE(2):"���� ������ �������� "
    PRINT
    PRINT SPACE(2):"_____________________"
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    PRINT SPACE(5):"LG.CANCEL.MOSTAFED"
*Line [ 231 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    RETURN
END
