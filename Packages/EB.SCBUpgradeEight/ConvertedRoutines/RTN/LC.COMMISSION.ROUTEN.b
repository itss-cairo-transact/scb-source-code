* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
* --- BAKRY - SCB --- 09/04/2003
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LC.COMMISSION.ROUTEN(CUSTOMER,DEAL.AMOUNT,DEAL.CURRENCY,CURRENCY.MARKET,CROSS.RATE,CROSS.CURRENCY,DRAWDOWN.CCY,T.DATA,CUST.COND,CHARGE.AMOUNT)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
** ----- ----- -----
    CALL OPF('F.FT.COMMISSION.TYPE',F.FT.COMMISSION.TYPE)
**     READ R.LC.COMMISSION FROM F.FT.COMMISSION.TYPE,T.DATA<1,1> THEN
    CALL F.READ('F.FT.COMMISSION.TYPE',T.DATA<1,1>,R.LC.COMMISSION,F.FT.COMMISSION.TYPE,ERR.A1)
    CUR = R.LC.COMMISSION<FT4.CURRENCY>
    LOCATE DEAL.CURRENCY IN CUR<1,1> SETTING POS THEN
*Line [ 34 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        CONT = DCOUNT( R.LC.COMMISSION<FT4.UPTO.AMT,POS>, @SM )
*---------
        I.AMT = INT(DEAL.AMOUNT / R.LC.COMMISSION<FT4.UPTO.AMT,POS,CONT>)
        R.AMT = DEAL.AMOUNT - ( I.AMT * R.LC.COMMISSION<FT4.UPTO.AMT,POS,CONT> )
        CHARGE.AMOUNT = I.AMT * R.LC.COMMISSION<FT4.MINIMUM.AMT,POS,CONT>
        FOR I = 1 TO CONT
            IF R.AMT <= R.LC.COMMISSION<FT4.UPTO.AMT,POS,I> THEN
                CHARGE.AMOUNT += R.LC.COMMISSION<FT4.MINIMUM.AMT,POS,I>
                I = CONT
            END
        NEXT I
**    END
*---------
    END
    TEXT = CHARGE.AMOUNT ; CALL REM
    RETURN
END
