* @ValidationCode : MjotMTEzMDY0NjExNTpDcDEyNTI6MTY0MTM2OTUwMDI2Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 05 Jan 2022 09:58:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LG.DEALSLIP

*1-TO CALL DAEL SLIP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE

    CALL PRODUCE.DEAL.SLIP("ADVICE.CONFIS")

RETURN
END
