* @ValidationCode : MjotMTY0ODkzMTAxOTpDcDEyNTI6MTY0MTcxODY4NzE3Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 10:58:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-72</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LG.AMAMTDATE.2231

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE / commented for duplication - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE


    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        BENF=R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>
        IF MYCODE='2231' THEN
            GOSUB INITIATE
*Line [ 50 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 51 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            GOSUB PRINT.HEAD
            GOSUB BODY
        END
    END ELSE
        IF ID.NEW EQ '' THEN
            GOSUB INITIATE
*Line [ 58 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 60 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            IF MYCODE='2231' THEN
                GOSUB PRINT.HEAD
                GOSUB BODY

            END ELSE
                MYID=MYCODE:'.':MYTYPE
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
OPER.CODE=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.OPERATION.CODE>
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.2231'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN CALL TXTINP(YTEXT, 8, 22, "12", "A")

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    IF ID.NEW THEN
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>

    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    BENF=LOCAL.REF<1,LDLR.APPROVAL>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.ISSUE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE =DATE.ISSUE[7,2]:"/":DATE.ISSUE[5,2]:"/":DATE.ISSUE[1,4]
*  DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
*Line [ 114 ] change LDLR.OLD.LG.NO to LDLR.OLD.NO - ITSS - R21 Upgrade - 2021-12-23
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    SUPPL1 =LOCAL.REF<1,LDLR.GUARRANTOR,1>
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    SEND.REC=LOCAL.REF<1,LDLR.SENDING.REF>
**********************GET OLD AMOUNT AND FIN MAT DATE************************************
    IDHIS=COMI:";":"..."
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ": IDHIS
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* IF KEY.LIST THEN
    OLD.ID= KEY.LIST<SELECTED>
    ID=FIELD(OLD.ID,';',1)
***************************************************************************
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
    CALL F.READ(FN.LG.HIS,OLD.ID, R.LG.HIS, F.LG.HIS ,E)
    IF ID.NEW EQ '' THEN
        LOCAL.REF=R.LG.HIS<LD.LOCAL.REF>
        OLD.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
        FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
    END  ELSE
        IF ID.NEW THEN
            OLD.FIN=R.OLD(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
        END
    END
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT=R.LG.HIS<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT=R.OLD(LD.AMOUNT)
        END
    END
    CALL F.RELEASE(FN.LG.HIS,OLD.ID,F.LG.HIS)
***************GET AMOUNT INC OR DEC************************************
*  FN.LG='F.LD.LOANS.AND.DEPOSITS';F.LG='';R.LG='';F.LG=''
*  CALL OPF(FN.LG,F.LG)
*  CALL F.READ(FN.LG,ID, R.LG, F.LG ,E)
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END
*   CALL F.RELEASE(FN.LG,ID,F.LG)
**********************************************************************************
    AMT.DEC=OLD.AMOUNT-NEW.AMOUNT
    AMT.INC=NEW.AMOUNT-OLD.AMOUNT
*END
**********************************************************************

RETURN
*===============================================================
PRINT.HEAD:
*    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*Line [ 207 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
OPER.CODE=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.OPERATION.CODE>
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":OPER.CODE:"*"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************

*Line [ 228 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"���� �������� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(38):"�":"*":EXT.NO:"*"
    HEADING PR.HD

RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���    : ":LG.NO:"-":SEND.REC:" (":TYPE.NAME:") "
    PRINT
    PRINT SPACE(20): "�������������    : ": " **":NEW.AMOUNT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
    PRINT SPACE(20):"������ ������    : ":ISSUE.DATE
    PRINT
    PRINT SPACE(20): "������� � �����  : ":SUPPL1
    PRINT
    PRINT SPACE(20): "���� ������� ��� : ":FIN.OLD
*  END
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(6):"�������� ��� ������ ��� /"   :CORR.NO:"������  " :CORR.DATE :"����� ��� ��� �������"
    PRINT;PRINT
    PRINT SPACE(2):"��� ������ ��� ������ ���� ������ ������ ������� ���������":FIN.DATE
    PRINT
    PRINT SPACE(2):"�� ���� ���� ������ ������ ���� ����� ."
    PRINT;PRINT
    PRINT SPACE(2):"� ��� ��� ��� ��� ������  �� ��� ����� ��� �� ���� ����� �� ���� �����"
    PRINT
    PRINT SPACE(2):"( ":OUT.DATE:" )"
    PRINT;PRINT
    PRINT SPACE(6):"� ����� ������ ����� �� ����� ���� ����� ������ ���� ����������"
    PRINT
    PRINT SPACE(2):" ��� ������ � ��� ����� �� �������� ���� �������ϡ ��� ����� �����"
    PRINT
    PRINT SPACE(2):" ������� �� ����� ������ ������ ����� ��� ��� ������� ."
    PRINT;PRINT
    PRINT SPACE(2):"����� ��� ������� ���� � �� ���� �� �� ���� ���� ��� �� ��� �� �����"
    PRINT
    PRINT SPACE(2):"�� ���������."
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":XX
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
RETURN
*==========================================================
