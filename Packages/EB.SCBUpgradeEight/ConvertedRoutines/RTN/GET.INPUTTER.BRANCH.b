* @ValidationCode : MjotNDUyNDU4MzcyOkNwMTI1MjoxNjQyMTA0NzkwNjA0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 13 Jan 2022 22:13:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------

** ----- ?.??.2002 Panos TEMENOS -----
** ----- 6.06.2002 Pawel TEMENOS -----
SUBROUTINE GET.INPUTTER.BRANCH( BRANCH, INPUTTER)

* TO GET DEPARTMENT CODE / BRANCH NO. ACCORDING TO THE INPUTTER DEPARTMENT
* TO DISPLAY IN ENQUIRIES

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER

    BRANCH = ''
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*   IF INPUTTER THEN CALL DBR( 'USER':@FM:EB.USE.DEPARTMENT.CODE, FIELD( INPUTTER< 1, 1>, '_', 2), BRANCH)
    IF INPUTTER THEN
        F.ITSS.USER = 'F.USER'
        FN.F.ITSS.USER = ''
        CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
        CALL F.READ(F.ITSS.USER,FIELD( INPUTTER< 1, 1>, '_', 2),R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
        BRANCH=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
    END
RETURN
END
