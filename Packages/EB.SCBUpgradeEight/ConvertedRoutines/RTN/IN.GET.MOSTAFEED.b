* @ValidationCode : Mjo5MzA4OTA4NzM6Q3AxMjUyOjE2NDA3MDk4NzQ2NTU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:44:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*------------------
** CREATE BY NESSMA
*------------------
SUBROUTINE IN.GET.MOSTAFEED(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_INF.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SUPPLIER
*-------------------------------------------
    FN.IN = "F.INF.MULTI.TXN" ; F.IN = ""
    CALL OPF(FN.IN, F.IN)

    IN.ID = ARG
    CALL F.READ(FN.IN,IN.ID,R.IN,F.IN,ER.IN)
    LANGUAGE.NUMBER  = R.IN<INF.MLT.LOCAL.REF><1,INLR.LANGUAGE>

    MOSTAFEEED.ID    = R.IN<INF.MLT.LOCAL.REF><1,INLR.SUPP.CODE>
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('SCB.SUPPLIER':@FM:SUPP.SUPP.NAME,MOSTAFEEED.ID,SUP.NAME)
F.ITSS.SCB.SUPPLIER = 'F.SCB.SUPPLIER'
FN.F.ITSS.SCB.SUPPLIER = ''
CALL OPF(F.ITSS.SCB.SUPPLIER,FN.F.ITSS.SCB.SUPPLIER)
CALL F.READ(F.ITSS.SCB.SUPPLIER,MOSTAFEEED.ID,R.ITSS.SCB.SUPPLIER,FN.F.ITSS.SCB.SUPPLIER,ERROR.SCB.SUPPLIER)
SUP.NAME=R.ITSS.SCB.SUPPLIER<SUPP.SUPP.NAME>

    IF LANGUAGE.NUMBER EQ '2' THEN
        ARG = SUP.NAME
    END

    IF LANGUAGE.NUMBER EQ '1' THEN
        ARG = SUP.NAME
    END
*-------------------------------------------
RETURN
END
