* @ValidationCode : MjotMjEwMjk1MDcxNDpDcDEyNTI6MTY0MTM2ODk2MzE2OTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 05 Jan 2022 09:49:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*------INGY-------*
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LD.TIMEDATE(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE deleted for duplicate - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
    ARG = ID.NEW
*Line [ 29 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.DATE.TIME,ARG,TTIME)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ARG,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
TTIME=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.DATE.TIME>
*Line [ 30 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DT = DCOUNT(TTIME,@VM)
    TT = TTIME<1,DT>
    TIM = TT[7,4]
    TIMHS = FMT(TIM,"R##:##")
*   HH = TIMHS[4,2]
*    SS = TIMHS[1,2]
*     TIMU = HH:":":SS
    ARG = TIMHS
RETURN
END
