* @ValidationCode : MjotNzAzMTg1MjA1OkNwMTI1MjoxNjQxNzE4ODg0MDI3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:01:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
********* WAEL *****
*-----------------------------------------------------------------------------
* <Rating>-201</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LG.PART.CONFIS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE


*Line [ 46 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 47 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
MYVER=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.DESCRIPTION>

    IF MYCODE EQ '2403' THEN
        GOSUB INITIATE
        GOSUB PRINT.HEAD
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "LETTER CREATED SUCCESFULLY" ; CALL REM
    END
RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.PART.CONFIS'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    SEND.REC=LOCAL.REF<1,LDLR.SENDING.REF>
*Line [ 79 ] change LDLR.OLD.LG.NO to LDLR.OLD.NO - ITSS - R21 Upgrade - 2021-12-23
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    PURPOSE = LOCAL.REF<1,LDLR.IN.RESPECT.OF>

    APPROVE=LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL1 =LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL2 =LOCAL.REF<1,LDLR.GUARRANTOR,2>

    ADDR1 =LOCAL.REF<1,LDLR.FREE.TEXT,1>
    ADDR2 =LOCAL.REF<1,LDLR.FREE.TEXT,2>
    ADDR3 =LOCAL.REF<1,LDLR.FREE.TEXT,3>

    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE,1>
    LG.MAT.DATE = R.LD<LD.FIN.MAT.DATE>
    LG.APPROVAL.DATE=LOCAL.REF<1,LDLR.APPROVAL.DATE,1>
    LG.ACCT=LOCAL.REF<1,LDLR.DEBIT.ACCT,1>

    LG.AMT =R.LD<LD.AMOUNT>
    LG.OLD=LG.AMT+CONFIS.AMT

    CUR=R.LD<LD.CURRENCY>
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    DATZ= TODAY
    ZZ= DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ= LG.APPROVAL.DATE
    QQ = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]
    OP.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    CHK.NO = LOCAL.REF<1,LDLR.CHEQUE.NO>
    CONFIS.PART.AMT=LOCAL.REF<1,LDLR.CONFISC.AMT>
    IF CONFIS.PART.AMT THEN
        CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    END ELSE
        CONFIS.AMT = R.LD<LD.AMOUNT>
    END
    LG.OLD=LG.AMT+CONFIS.AMT
    REST.AMT = LG.AMT - CONFIS.AMT
    COR.D = LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORRS.DATE = COR.D[7,2]:'/':COR.D[5,2]:"/":COR.D[1,4]
*--------------------------------------------------------------------------
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":COMI:"..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,KEY.LIST<SELECTED>,R.LD,F.LD,E1)

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    OPR.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>

    WW = R.LD<LD.DATE.TIME>
    D.TIME = WW[1,6]
*  WWW = D.TIME[1,2]:'/':D.TIME[3,2]:"/":"20":D.TIME[5,2]
    D.TIME=LG.APPROVAL.DATE
    WWW = D.TIME[7,2]:'/':D.TIME[5,2]:"/":D.TIME[1,4]

RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*Line [ 188 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
OPER.CODE=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.OPERATION.CODE>
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":"������ �����":"*"
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
*Line [ 197 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
AC.OFICER=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

*Line [ 207 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"���� �������� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD

RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:"-":SEND.REC:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(17): "����������������� : ":" **":LG.OLD:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "����������� ����� : ":SUPPL1
    PRINT SPACE(17): "                  : ":SUPPL2
    PRINT
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT
*=============
    PRINT SPACE(5):" ������ ������� ������ " : WWW
    PRINT
    PRINT SPACE(1):"����� ��� ���� ��� �� ��� ����� ������� ��� " :CHK.NO:" ����� ":CONFIS.AMT:" ":CRR
    PRINT
    IF OP.CODE EQ "2401"  THEN
        PRINT SPACE(1):"������ ����� ����� ��  ���� ���� ������ ������ ������"
    END ELSE
        PRINT SPACE(1):" ������ ����� ����� �� ����� ������� �� ���� ���� ������ ������ ������"
    END
    PRINT
    PRINT SPACE(1):"���� ����� ������ ���� ������ ����� ���� ������� �����"  :LG.AMT :CRR
    PRINT ; PRINT
    PRINT SPACE(1):"���� ��� ��� �������� ����� ���� ������ ����������� �� ����� ����"  :LG.AMT :CRR
    PRINT
    PRINT SPACE(1):"����� ������� ��������� � ����� ������ ��� ����� ��� ������ �����"
    PRINT
    PRINT SPACE(1):"�� ���� ������ ������� ."

    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(5):" ���� ��� ��� ":CHK.NO:SPACE(25):"��� ��� ���� ������"
    PRINT SPACE(5):"�����" :CONFIS.AMT:" ":CRR

RETURN
*================================================================
