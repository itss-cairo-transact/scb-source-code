* @ValidationCode : MjoyMTA2MDQ5NTM4OkNwMTI1MjoxNjQwNzA4NDU2NzkyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:20:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*********************CREATED BY MAHMOUD MAGDY 2020/6/16***************

PROGRAM GET.PROG.FILE.2


*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES

    EXECUTE "COMO ON GET.PROG.FILE.2"


    LW.DAY = ''
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010099',LW.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010099',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LW.DAY=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    CRT 'LAST WORKING.DATE ':LW.DAY
    FOLDER = 'AML'
    EXECUTE 'TIME'
    GOSUB INIT_LD
    GOSUB PROC_LD

    EXECUTE 'TIME'
    GOSUB INIT_LD_L
    GOSUB PROC_LD_L

    EXECUTE 'TIME'
    GOSUB INIT_FT
    GOSUB PROC_FT

    EXECUTE 'TIME'
    GOSUB INIT_FT_L
    GOSUB PROC_FT_L

    EXECUTE 'TIME'
    GOSUB INIT_TT
    GOSUB PROC_TT

    EXECUTE 'TIME'
    GOSUB INIT_TT_L
    GOSUB PROC_TT_L

    EXECUTE 'TIME'
    GOSUB INIT_SE
    GOSUB PROC_SE

    EXECUTE 'TIME'
    GOSUB INIT_ACC
    GOSUB PROC_ACC_HIS

    EXECUTE 'TIME'
    GOSUB INIT_ACC.1
    GOSUB PROC_ACC

    EXECUTE 'TIME'
    GOSUB INIT_AC_CLOSED
    GOSUB PROC_AC.CLOSED
    EXECUTE 'TIME'
    GOSUB PRINT.DATE
    EXECUTE 'chmod -R 777 AML'
    CRT 'AML program is done'
    EXECUTE "COMO OFF GET.PROG.FILE.2"
RETURN


***************************CREATE ACCOUNT TXT(EE) *****************
INIT_ACC:
    OPENSEQ FOLDER , "GET.FILE_ACCOUNT$HIS.txt" TO EE THEN
        CLOSESEQ EE
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_ACCOUNT$HIS.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_ACCOUNT$HIS.txt" TO EE ELSE
        CREATE EE THEN
            PRINT 'FILE GET.FILE_ACCOUNT$HIS.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_ACCOUNT$HIS.txt File IN ':FOLDER
        END
    END

    ACC.HIS.HEADER = '@id|currency|ACCOUNT.TITLE.1|SHORT.TITLE|OPENING.DATE|CATEGORY|CUSTOMER|CO.CODE|CLOSURE.DATE|DATE.TIME'
    WRITESEQ ACC.HIS.HEADER TO EE ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_ACCOUNT$HIS'
    END
    GOSUB INIT_ACC_FILE

RETURN
****************************CREATE ACCOUNT LIVE**************
INIT_ACC.1:

    OPENSEQ FOLDER , "GET.FILE_ACCOUNT.txt" TO KK THEN
        CLOSESEQ KK
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_ACCOUNT.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_ACCOUNT.txt" TO KK ELSE
        CREATE KK THEN
            PRINT 'FILE GET.FILE_ACCOUNT.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_ACCOUNT.txt File IN ':FOLDER
        END
    END

    ACC.HEADER = '@id|currency|ACCOUNT.TITLE.1|SHORT.TITLE|OPENING.DATE|CATEGORY|CUSTOMER|CO.CODE|CLOSURE.DATE|DATE.TIME'
    WRITESEQ ACC.HEADER TO KK ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_ACCOUNT'
    END
    GOSUB INIT_ACC_FILE.1

RETURN
***********************************CREATE CLOSED ACCOUNTS**************
INIT_AC_CLOSED:

    OPENSEQ FOLDER , "GET.FILE_ACCOUNT.CLOSED.txt" TO OO THEN
        CLOSESEQ OO
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_ACCOUNT.CLOSED.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_ACCOUNT.CLOSED.txt" TO OO ELSE
        CREATE OO THEN
            PRINT 'FILE GET.FILE_ACCOUNT.CLOSED.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_ACCOUNT.CLOSED.txt File IN ':FOLDER
        END
    END

    CLOSE.ACC.HEADER = '@id'
    WRITESEQ CLOSE.ACC.HEADER TO OO ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_ACCOUNT.CLOSED.txt'
    END
    GOSUB INIT_AC.CLOSED_FILE

RETURN
***********************************CREATE FUNDS AND TRANSFER TXT *********************
INIT_FT:

    OPENSEQ FOLDER , "GET.FUNDS.TRANSFER$HIS.txt" TO GG THEN
        CLOSESEQ GG
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FUNDS.TRANSFER$HIS.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FUNDS.TRANSFER$HIS.txt" TO GG ELSE
        CREATE GG THEN
            PRINT 'FILE GET.FUNDS.TRANSFER$HIS.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FUNDS.TRANSFER$HIS.txt File IN ':FOLDER
        END
    END
    FT.HIS.HEADER = '@id|transaction.type|cheque.number|debit.currency|credit.currency|inputter|debit.acct.no|credit.acct.no|processing.date|debit.value.date|co.code|date.time'
    FT.HIS.HEADER := '|terminal.loc|loc.amt.debited|amount.debited|remining.bal|credit.value.date|amount.credited|loc.amt.credited|ben.customer|credit.their.ref|authoriser'
    FT.HIS.HEADER := '|debit.customer|credit.customer|nots.credit|ord.cust.acct|send.instit|ordering.bank|acct.with.bank|bank.br.ach|receiver.bank|ben.acct.no|terminal.id'

    WRITESEQ FT.HIS.HEADER TO GG ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_FUNDS.TRANSFER$HIS'
    END
    GOSUB INIT_FT_FILE

RETURN
***********************************CREATE FUNDS AND TRANSFER LIVE TXT *********************
INIT_FT_L:



    OPENSEQ FOLDER , "GET.FUNDS.TRANSFER.txt" TO GG1 THEN
        CLOSESEQ GG1
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FUNDS.TRANSFER.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FUNDS.TRANSFER.txt" TO GG1 ELSE
        CREATE GG1 THEN
            PRINT 'FILE GET.FUNDS.TRANSFER.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FUNDS.TRANSFER.txt File IN ':FOLDER
        END
    END

    FT.HEADER = '@id|transaction.type|cheque.number|debit.currency|credit.currency|inputter|debit.acct.no|credit.acct.no|processing.date|debit.value.date|co.code|date.time'
    FT.HEADER := '|terminal.loc|loc.amt.debited|amount.debited|remining.bal|credit.value.date|amount.credited|loc.amt.credited|ben.customer|credit.their.ref|authoriser'
    FT.HEADER := '|debit.customer|credit.customer|nots.credit|ord.cust.acct|send.instit|ordering.bank|acct.with.bank|bank.br.ach|receiver.bank|ben.acct.no|terminal.id'

    WRITESEQ FT.HEADER TO GG1 ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_FUNDS.TRANSFER'
    END
    GOSUB INIT_FT_FILE_L

RETURN
******************************CREAT LOAN AND DEPOSETS TXT****************************
INIT_LD:

    OPENSEQ FOLDER , "GET.FILE_LOANS.AND.DEPOSITS$HIS.txt" TO HH THEN
        CLOSESEQ HH
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_LOANS.AND.DEPOSITS$HIS.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_LOANS.AND.DEPOSITS$HIS.txt" TO HH ELSE
        CREATE HH THEN
            PRINT 'FILE GET.FILE_LOANS.AND.DEPOSITS$HIS.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_LOANS.AND.DEPOSITS$HIS.txt File IN ':FOLDER
        END
    END
    LD.HIS.HEADER = '@id|fin.mat.date|currency|value.date|status|category|matur.date|amount|customer.id|co.code|collateral.id|date.time'

    WRITESEQ LD.HIS.HEADER TO HH ELSE
        PRINT 'ERROR WIRTE FILE GET.FILE_LOANS.AND.DEPOSITS$HIS'
    END
    GOSUB INIT_LD_FILE

RETURN
******************************CREAT LOAN AND DEPOSETS LIVE TXT****************************
INIT_LD_L:

    OPENSEQ FOLDER , "GET.FILE_LOANS.AND.DEPOSITS.txt" TO HH1 THEN
        CLOSESEQ HH1
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_LOANS.AND.DEPOSITS.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_LOANS.AND.DEPOSITS.txt" TO HH1 ELSE
        CREATE HH1 THEN
            PRINT 'FILE GET.FILE_LOANS.AND.DEPOSITS.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_LOANS.AND.DEPOSITS.txt File IN ':FOLDER
        END
    END

    LD.HEADER = '@id|fin.mat.date|currency|value.date|status|category|matur.date|amount|customer.id|co.code|collateral.id|date.time'

    WRITESEQ LD.HEADER TO HH1 ELSE
        PRINT 'ERROR WIRTE FILE GET.FILE_LOANS.AND.DEPOSITS$HIS'
    END
    GOSUB INIT_LD_L_FILE

RETURN
***************************CREATE TELLER TXT(FF)*****************
INIT_TT:


    OPENSEQ FOLDER , "GET.FILE_TELLER$HIS.txt" TO FF THEN
        CLOSESEQ FF
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_TELLER$HIS.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_TELLER$HIS.txt" TO FF ELSE
        CREATE FF THEN
            PRINT 'FILE GET.FILE_TELLER$HIS.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_TELLER$HIS.txt File IN ':FOLDER
        END
    END


    TT.HIS.HEADER = '@id|transaction.code|dr.cr.marker|inputter|authoriser|bk.cus|dpst.nsn.no|arabic.name|narrative.2|bnk.customer'
    TT.HIS.HEADER := '|value.date.1|value.date.2|date.time|co.code|atm.number|currency.1|amount.local.1|amount.fcy.1|new.cust.bal|cheque.number|ser.no|account.1'

    WRITESEQ TT.HIS.HEADER TO FF ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_TELLER$HIS.txt'
    END

    GOSUB INIT_TT_FILE

RETURN

***************************CREATE TELLER TXT LIVE(FF)*****************
INIT_TT_L:

    OPENSEQ FOLDER , "GET.FILE_TELLER.txt" TO FF1 THEN
        CLOSESEQ FF1
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_TELLER.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_TELLER.txt" TO FF1 ELSE
        CREATE FF1 THEN
            PRINT 'FILE GET.FILE_TELLER.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_TELLER.txt File IN ':FOLDER
        END
    END

    TT.HEADER = '@id|transaction.code|dr.cr.marker|inputter|authoriser|bk.cus|dpst.nsn.no|arabic.name|narrative.2|bnk.customer'
    TT.HEADER := '|value.date.1|value.date.2|date.time|co.code|atm.number|currency.1|amount.local.1|amount.fcy.1|new.cust.bal|cheque.number|ser.no|account.1'

    WRITESEQ TT.HEADER TO FF1 ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_TELLER.txt'
    END

    GOSUB INIT_TT_FILE_L

RETURN
********************************************************************
INIT_SE:
    OPENSEQ FOLDER , "GET.FILE_STMT.ENTRY.txt" TO ZZ THEN
        CLOSESEQ ZZ
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"GET.FILE_STMT.ENTRY.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "GET.FILE_STMT.ENTRY.txt" TO ZZ ELSE
        CREATE ZZ THEN
            PRINT 'FILE GET.FILE_STMT.ENTRY.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create GET.FILE_STMT.ENTRY.txt File IN ':FOLDER
        END
    END
    SE.HEADER = '@ID|TRANSACTION.CODE| OUR.REFERENCE| AMOUNT.LCY|INPUTTER|SYSTEM.ID|ACCOUNT.NUMBER|CUSTOMER.ID|DATE.TIME|BOOKING.DATE|COMPANY.CODE|CURRENCY|AMOUNT.FCY|OUTSTANDING.BAL'
    WRITESEQ SE.HEADER TO ZZ ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_STMT.ENTRY'
    END

    GOSUB INIT_SE_FILE
RETURN
********************************************************************
INIT_CUS_FILE:

    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

RETURN

INIT_ACC_FILE:

    FN.ACC$HIS = 'FBNK.ACCOUNT$HIS' ; F.ACC$HIS = '' ; R.ACC.HIS = ''
    CALL OPF(FN.ACC$HIS,F.ACC$HIS)
RETURN

INIT_ACC_FILE.1:

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
RETURN

INIT_FT_FILE:

    FN.FT$HIS = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT$HIS = ''
    CALL OPF(FN.FT$HIS , F.FT$HIS)

RETURN

INIT_FT_FILE_L:

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT , F.FT)

RETURN

INIT_LD_FILE:

    FN.LD$HIS = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD$HIS = ''
    CALL OPF(FN.LD$HIS , F.LD$HIS)

RETURN

INIT_LD_L_FILE:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD , F.LD)

RETURN



INIT_TT_FILE:

    FN.TT$HIS = 'FBNK.TELLER$HIS' ; F.TT$HIS = ''
    CALL OPF(FN.TT$HIS , F.TT$HIS)

RETURN

INIT_TT_FILE_L:

    FN.TT = 'FBNK.TELLER' ; F.TT = ''
    CALL OPF(FN.TT , F.TT)

RETURN

INIT_AC.CLOSED_FILE:
    FN.AC.CLOSED = 'FBNK.ACCOUNT.CLOSED' ; F.AC.CLOSED = ''
    CALL OPF(FN.AC.CLOSED,F.AC.CLOSED)
RETURN

INIT_SE_FILE:
    FN.SE = 'FBNK.STMT.ENTRY' ; F.SE = ''
    CALL OPF(FN.SE,F.SE)

    FN.LW.ENT = 'FBNK.ACCT.ENT.LWORK.DAY'  ; F.LW.ENT = ''
    CALL OPF(FN.LW.ENT,F.LW.ENT)
RETURN


********************************** SELECT ACCOUNTS ********************
PROC_ACC_HIS:
    T.SEL.ACC.HIS = 'SELECT ':FN.ACC$HIS:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.SEL.ACC.HIS
    CALL EB.READLIST(T.SEL.ACC.HIS,KEY.LIST.ACC.HIS,'',SELECTED.ACC.HIS,ERR.ACC.HIS)
    EE.DATA = ''
    IF SELECTED.ACC.HIS THEN
        FOR I=1 TO SELECTED.ACC.HIS
            ACC.ID = KEY.LIST.ACC.HIS<I>
            CALL F.READ(FN.ACC$HIS,KEY.LIST.ACC.HIS<I>,R.ACC.HIS,F.ACC$HIS,ETEXT.ACC.HIS)
            EE.DATA = ACC.ID:'|'
            EE.DATA := R.ACC.HIS<AC.CURRENCY>:'|'
            EE.DATA := R.ACC.HIS<AC.ACCOUNT.TITLE.1>:'|'
            EE.DATA := R.ACC.HIS<AC.SHORT.TITLE>:'|'
            EE.DATA := R.ACC.HIS<AC.OPENING.DATE>:'|'
            EE.DATA := R.ACC.HIS<AC.CATEGORY>:'|'
            EE.DATA := R.ACC.HIS<AC.CUSTOMER>:'|'
            EE.DATA := R.ACC.HIS<AC.CO.CODE>:'|'
            EE.DATA := R.ACC.HIS<AC.CLOSURE.DATE>:'|'
            EE.DATA := R.ACC.HIS<AC.DATE.TIME>
            CHANGE @VM TO '@VM' IN EE.DATA
            CHANGE @SM TO '@SM' IN EE.DATA

************ WRITE ACCOUNTS
            WRITESEQ EE.DATA TO EE ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_ACCOUNT$HIS'
            END

        NEXT I
    END
    GOSUB CLOSE_ACC_FILE
RETURN

CLOSE_ACC_FILE:

    CLOSESEQ EE
    PRINT 'ACCOUNTS FINISHED'

RETURN
***********************************************************
********************************** SELECT ACCOUNTS LIVE********************
PROC_ACC:
    T.SEL.ACC = 'SELECT ':FN.ACC:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.SEL.ACC
    CALL EB.READLIST(T.SEL.ACC,KEY.LIST.ACC,'',SELECTED.ACC,ERR.ACC)
    KK.DATA = ''
    IF SELECTED.ACC THEN
        FOR I=1 TO SELECTED.ACC
            ACC.ID.L = KEY.LIST.ACC<I>
            CALL F.READ(FN.ACC,KEY.LIST.ACC<I>,R.ACC,F.ACC,ETEXT.ACC)
            KK.DATA = ACC.ID.L:'|'
            KK.DATA := R.ACC<AC.CURRENCY>:'|'
            KK.DATA := R.ACC<AC.ACCOUNT.TITLE.1>:'|'
            KK.DATA := R.ACC<AC.SHORT.TITLE>:'|'
            KK.DATA := R.ACC<AC.OPENING.DATE>:'|'
            KK.DATA := R.ACC<AC.CATEGORY>:'|'
            KK.DATA := R.ACC<AC.CUSTOMER>:'|'
            KK.DATA := R.ACC<AC.CO.CODE>:'|'
            KK.DATA := R.ACC<AC.CLOSURE.DATE>:'|'
            KK.DATA := R.ACC<AC.DATE.TIME>
            CHANGE @VM TO '@VM' IN KK.DATA
            CHANGE @SM TO '@SM' IN KK.DATA

************ WRITE ACCOUNTS
            WRITESEQ KK.DATA TO KK ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_ACCOUNT'
            END

        NEXT I
    END
    GOSUB CLOSE_ACC_FILE.L
RETURN

CLOSE_ACC_FILE.L:

    CLOSESEQ KK
    PRINT 'ACCOUNTS LIVE FINISHED'

RETURN
***********************************************************
PROC_FT:

    GG.DATA = ''

    T.FT.HIS.SEL = 'SELECT ':FN.FT$HIS:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'

    CRT T.FT.HIS.SEL
    CALL EB.READLIST(T.FT.HIS.SEL,KEY.LIST.FT.HIS,"",SELECTED.FT.HIS,ERR.FT.HIS)
    IF SELECTED.FT.HIS THEN
        FOR I = 1 TO SELECTED.FT.HIS
            CALL F.READ(FN.FT$HIS,KEY.LIST.FT.HIS<I>,R.FT.HIS,F.FT$HIS,ETEXT.FT.HIS)
            FT.ID.HIS = KEY.LIST.FT.HIS<I>
            GG.DATA = FT.ID.HIS:'|'
            GG.DATA := R.FT.HIS<FT.TRANSACTION.TYPE>:'|'
            GG.DATA := R.FT.HIS<FT.CHEQUE.NUMBER>:'|'
            GG.DATA := R.FT.HIS<FT.DEBIT.CURRENCY>:'|'
            GG.DATA := R.FT.HIS<FT.CREDIT.CURRENCY>:'|'
            GG.DATA := R.FT.HIS<FT.INPUTTER>:'|'
            GG.DATA := R.FT.HIS<FT.DEBIT.ACCT.NO>:'|'
            GG.DATA := R.FT.HIS<FT.CREDIT.ACCT.NO>:'|'
            GG.DATA := R.FT.HIS<FT.PROCESSING.DATE>:'|'
            GG.DATA := R.FT.HIS<FT.DEBIT.VALUE.DATE>:'|'
            GG.DATA := R.FT.HIS<FT.CO.CODE>:'|'
            GG.DATA := R.FT.HIS<FT.DATE.TIME>:'|'
            GG.DATA := R.FT.HIS<FT.LOCAL.REF,FTLR.TERMINAL.LOC>:'|'
            GG.DATA := R.FT.HIS<FT.LOC.AMT.DEBITED>:'|'
            GG.DATA := R.FT.HIS<FT.AMOUNT.DEBITED>:'|'
            GG.DATA := R.FT.HIS<FT.LOCAL.REF,FTLR.REMINING.BAL.NO>:'|'
            GG.DATA := R.FT.HIS<FT.CREDIT.VALUE.DATE>:'|'
            GG.DATA := R.FT.HIS<FT.AMOUNT.CREDITED>:'|'
            GG.DATA := R.FT.HIS<FT.LOC.AMT.CREDITED>:'|'
            GG.DATA := R.FT.HIS<FT.BEN.CUSTOMER>:'|'
            GG.DATA := R.FT.HIS<FT.CREDIT.THEIR.REF>:'|'
            GG.DATA := R.FT.HIS<FT.AUTHORISER>:'|'
            GG.DATA := R.FT.HIS<FT.DEBIT.CUSTOMER>:'|'
            GG.DATA := R.FT.HIS<FT.CREDIT.CUSTOMER>:'|'
            GG.DATA := R.FT.HIS<FT.LOCAL.REF,FTLR.NOTE.CREDIT>:'|'
            GG.DATA := R.FT.HIS<FT.ORD.CUST.ACCT>:'|'
            GG.DATA := R.FT.HIS<FT.LOCAL.REF,FTLR.SEND.INSTIT>:'|'
            GG.DATA := R.FT.HIS<FT.ORDERING.BANK>:'|'
            GG.DATA := R.FT.HIS<FT.ACCT.WITH.BANK>:'|'
            GG.DATA := R.FT.HIS<FT.LOCAL.REF,FTLR.BANK.BR.ACH>:'|'
            GG.DATA := R.FT.HIS<FT.RECEIVER.BANK>:'|'
            GG.DATA := R.FT.HIS<FT.BEN.ACCT.NO>:'|'
            GG.DATA := R.FT.HIS<FT.LOCAL.REF,FTLR.TERMINAL.ID>
            CHANGE @VM TO '@VM' IN GG.DATA
            CHANGE @SM TO '@SM' IN GG.DATA

***************** WRITE FUNDS.TRANSFERS
            WRITESEQ GG.DATA TO GG ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_FUNDS.TRANSFER$HIS'
            END
        NEXT I
    END

    GOSUB CLOSE_FT_FILE
RETURN

CLOSE_FT_FILE:

    CLOSE GG
    PRINT 'FUNDS.TRANSFER_HIS FINISHED'

RETURN
***********************************************************
PROC_FT_L:

    GG1.DATA = ''

    T.FT.SEL = 'SELECT ':FN.FT:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.FT.SEL
    CALL EB.READLIST(T.FT.SEL,KEY.LIST.FT,"",SELECTED.FT,ERR.FT)
    IF SELECTED.FT THEN
        FOR I = 1 TO SELECTED.FT
            CALL F.READ(FN.FT,KEY.LIST.FT<I>,R.FT,F.FT,ETEXT.FT)
            FT.ID = KEY.LIST.FT<I>
            GG1.DATA = FT.ID:'|'
            GG1.DATA := R.FT<FT.TRANSACTION.TYPE>:'|'
            GG1.DATA := R.FT<FT.CHEQUE.NUMBER>:'|'
            GG1.DATA := R.FT<FT.DEBIT.CURRENCY>:'|'
            GG1.DATA := R.FT<FT.CREDIT.CURRENCY>:'|'
            GG1.DATA := R.FT<FT.INPUTTER>:'|'
            GG1.DATA := R.FT<FT.DEBIT.ACCT.NO>:'|'
            GG1.DATA := R.FT<FT.CREDIT.ACCT.NO>:'|'
            GG1.DATA := R.FT<FT.PROCESSING.DATE>:'|'
            GG1.DATA := R.FT<FT.DEBIT.VALUE.DATE>:'|'
            GG1.DATA := R.FT<FT.CO.CODE>:'|'
            GG1.DATA := R.FT<FT.DATE.TIME>:'|'
            GG1.DATA := R.FT<FT.LOCAL.REF,FTLR.TERMINAL.LOC>:'|'
            GG1.DATA := R.FT<FT.LOC.AMT.DEBITED>:'|'
            GG1.DATA := R.FT<FT.AMOUNT.DEBITED>:'|'
            GG1.DATA := R.FT<FT.LOCAL.REF,FTLR.REMINING.BAL.NO>:'|'
            GG1.DATA := R.FT<FT.CREDIT.VALUE.DATE>:'|'
            GG1.DATA := R.FT<FT.AMOUNT.CREDITED>:'|'
            GG1.DATA := R.FT<FT.LOC.AMT.CREDITED>:'|'
            GG1.DATA := R.FT<FT.BEN.CUSTOMER>:'|'
            GG1.DATA := R.FT<FT.CREDIT.THEIR.REF>:'|'
            GG1.DATA := R.FT<FT.AUTHORISER>:'|'
            GG1.DATA := R.FT<FT.DEBIT.CUSTOMER>:'|'
            GG1.DATA := R.FT<FT.CREDIT.CUSTOMER>:'|'
            GG1.DATA := R.FT<FT.LOCAL.REF,FTLR.NOTE.CREDIT>:'|'
            GG1.DATA := R.FT<FT.ORD.CUST.ACCT>:'|'
            GG1.DATA := R.FT<FT.LOCAL.REF,FTLR.SEND.INSTIT>:'|'
            GG1.DATA := R.FT<FT.ORDERING.BANK>:'|'
            GG1.DATA := R.FT<FT.ACCT.WITH.BANK>:'|'
            GG1.DATA := R.FT<FT.LOCAL.REF,FTLR.BANK.BR.ACH>:'|'
            GG1.DATA := R.FT<FT.RECEIVER.BANK>:'|'
            GG1.DATA := R.FT<FT.BEN.ACCT.NO>:'|'
            GG1.DATA := R.FT<FT.LOCAL.REF,FTLR.TERMINAL.ID>
            CHANGE @VM TO '@VM' IN GG1.DATA
            CHANGE @SM TO '@SM' IN GG1.DATA

***************** WRITE FUNDS.TRANSFERS
            WRITESEQ GG1.DATA TO GG1 ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_FUNDS.TRANSFER'
            END
        NEXT I
    END

    GOSUB CLOSE_FT_FILE_L
RETURN

CLOSE_FT_FILE_L:

    CLOSE GG1
    PRINT 'FUNDS.TRANSFER FINISHED'

RETURN
*********************************SELECT LD************************
PROC_LD:

    HH.DATA = ''

    T.LD.HIS.SEL = 'SELECT ':FN.LD$HIS:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.LD.HIS.SEL
    CALL EB.READLIST(T.LD.HIS.SEL,KEY.LIST.LD.HIS,"",SELECTED.LD.HIS,ERR.LD.HIS)
    IF SELECTED.LD.HIS THEN
        FOR I = 1 TO SELECTED.LD.HIS
            CALL F.READ(FN.LD$HIS,KEY.LIST.LD.HIS<I>,R.LD.HIS,F.LD$HIS,ETEXT.LD.HIS)
            LD.ID.HIS = KEY.LIST.LD.HIS<I>
            HH.DATA = LD.ID.HIS:'|'
            HH.DATA := R.LD.HIS<LD.FIN.MAT.DATE>:'|'
            HH.DATA := R.LD.HIS<LD.CURRENCY>:'|'
            HH.DATA := R.LD.HIS<LD.VALUE.DATE>:'|'
            HH.DATA := R.LD.HIS<LD.STATUS>:'|'
            HH.DATA := R.LD.HIS<LD.CATEGORY>:'|'
            HH.DATA := R.LD.HIS<LD.LOCAL.REF,LDLR.MATUR.DATE>:'|'
            HH.DATA := R.LD.HIS<LD.AMOUNT>:'|'
            HH.DATA := R.LD.HIS<LD.CUSTOMER.ID>:'|'
            HH.DATA := R.LD.HIS<LD.CO.CODE>:'|'
            HH.DATA := R.LD.HIS<LD.LOCAL.REF,LDLR.COLLATERAL.ID>:'|'
            HH.DATA := R.LD.HIS<LD.DATE.TIME>:'|'
            CHANGE @VM TO '@VM' IN HH.DATA
            CHANGE @SM TO '@SM' IN HH.DATA
*************WRITE LD
            WRITESEQ HH.DATA TO HH ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_LOANS.AND.DEPOSITS$HIS'
            END
        NEXT I
    END
    GOSUB CLOSE_LD_FILE
RETURN

CLOSE_LD_FILE:

    CLOSE HH
    PRINT 'GET.FILE_LOANS.AND.DEPOSITS$HIS FINISHED'

RETURN

*********************************SELECT LD************************
PROC_LD_L:

    HH1.DATA = ''

    T.LD.SEL = 'SELECT ':FN.LD:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.LD.SEL
    CALL EB.READLIST(T.LD.SEL,KEY.LIST.LD,"",SELECTED.LD,ERR.LD)
    IF SELECTED.LD THEN
        FOR I = 1 TO SELECTED.LD
            CALL F.READ(FN.LD,KEY.LIST.LD<I>,R.LD,F.LD,ETEXT.LD)
            LD.ID = KEY.LIST.LD<I>
            HH1.DATA = LD.ID:'|'
            HH1.DATA := R.LD<LD.FIN.MAT.DATE>:'|'
            HH1.DATA := R.LD<LD.CURRENCY>:'|'
            HH1.DATA := R.LD<LD.VALUE.DATE>:'|'
            HH1.DATA := R.LD<LD.STATUS>:'|'
            HH1.DATA := R.LD<LD.CATEGORY>:'|'
            HH1.DATA := R.LD<LD.LOCAL.REF,LDLR.MATUR.DATE>:'|'
            HH1.DATA := R.LD<LD.AMOUNT>:'|'
            HH1.DATA := R.LD<LD.CUSTOMER.ID>:'|'
            HH1.DATA := R.LD<LD.CO.CODE>:'|'
            HH1.DATA := R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>:'|'
            HH1.DATA := R.LD<LD.DATE.TIME>:'|'
            CHANGE @VM TO '@VM' IN HH1.DATA
            CHANGE @SM TO '@SM' IN HH1.DATA
*************WRITE LD
            WRITESEQ HH1.DATA TO HH1 ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_LOANS.AND.DEPOSITS'
            END
        NEXT I
    END
    GOSUB CLOSE_LD_L_FILE
RETURN

CLOSE_LD_L_FILE:

    CLOSE HH1
    PRINT 'GET.FILE_LOANS.AND.DEPOSITS FINISHED'

RETURN

********************************SELECT TELLERS *************************
PROC_TT:

    FF.DATA = ''

    T.TT.HIS.SEL = 'SELECT ':FN.TT$HIS:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.TT.HIS.SEL
    CALL EB.READLIST(T.TT.HIS.SEL,KEY.LIST.TT.HIS,"",SELECTED.TT.HIS,ERR.TT.HIS)
    IF SELECTED.TT.HIS THEN
        FOR I = 1 TO SELECTED.TT.HIS
            CALL F.READ(FN.TT$HIS,KEY.LIST.TT.HIS<I>,R.TT.HIS,F.TT$HIS,ETEXT.TT.HIS)
            TT.ID.HIS = KEY.LIST.TT.HIS<I>
            FF.DATA = TT.ID.HIS:'|'
            FF.DATA := R.TT.HIS<TT.TE.TRANSACTION.CODE>:'|'
            FF.DATA := R.TT.HIS<TT.TE.DR.CR.MARKER>:'|'
            FF.DATA := R.TT.HIS<TT.TE.INPUTTER>:'|'
            FF.DATA := R.TT.HIS<TT.TE.AUTHORISER>:'|'
            FF.DATA := R.TT.HIS<TT.TE.LOCAL.REF,TTLR.BK.CUS>:'|'
            FF.DATA := R.TT.HIS<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>:'|'
            FF.DATA := R.TT.HIS<TT.TE.LOCAL.REF,TTLR.ARABIC.NAME>:'|'
            FF.DATA := R.TT.HIS<TT.TE.NARRATIVE.2>:'|'
            FF.DATA := R.TT.HIS<TT.TE.LOCAL.REF,TTLR.BNK.CUSTOMER>:'|'
            FF.DATA := R.TT.HIS<TT.TE.VALUE.DATE.1>:'|'
            FF.DATA := R.TT.HIS<TT.TE.VALUE.DATE.2>:'|'
            FF.DATA := R.TT.HIS<TT.TE.DATE.TIME>:'|'
            FF.DATA := R.TT.HIS<TT.TE.CO.CODE>:'|'
            FF.DATA := R.TT.HIS<TT.TE.LOCAL.REF,TTLR.ATM.NUMBER>:'|'
            FF.DATA := R.TT.HIS<TT.TE.CURRENCY.1>:'|'
            FF.DATA := R.TT.HIS<TT.TE.AMOUNT.LOCAL.1>:'|'
            FF.DATA := R.TT.HIS<TT.TE.AMOUNT.FCY.1>:'|'
            FF.DATA := R.TT.HIS<TT.TE.NEW.CUST.BAL>:'|'
            FF.DATA := R.TT.HIS<TT.TE.CHEQUE.NUMBER>:'|'
            FF.DATA := R.TT.HIS<TT.TE.LOCAL.REF,TTLR.SER.NO>:'|'
            FF.DATA := R.TT.HIS<TT.TE.ACCOUNT.1>:'|'
            CHANGE @VM TO '@VM' IN FF.DATA
            CHANGE @SM TO '@SM' IN FF.DATA
*****************WRITE TELLERS
            WRITESEQ FF.DATA TO FF ELSE
                PRINT 'ERROR WRITE FILE TELLERS'
            END
        NEXT I

    END
    GOSUB CLOSE_TT_FILE

RETURN

CLOSE_TT_FILE:
    CLOSESEQ FF
    PRINT 'GET.FILE_TELLER$HIS FINISHED'

RETURN

********************************SELECT TELLERS LIVE *************************
PROC_TT_L:

    FF1.DATA = ''

    T.TT.SEL = 'SELECT ':FN.TT:' WITH DATE.TIME GE ':LW.DAY[3,6]:'0000'
    CRT T.TT.SEL
    CALL EB.READLIST(T.TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ERR.TT)
    IF SELECTED.TT THEN
        FOR I = 1 TO SELECTED.TT
            CALL F.READ(FN.TT,KEY.LIST.TT<I>,R.TT,F.TT,ETEXT.TT)
            TT.ID = KEY.LIST.TT<I>
            FF1.DATA = TT.ID:'|'
            FF1.DATA := R.TT<TT.TE.TRANSACTION.CODE>:'|'
            FF1.DATA := R.TT<TT.TE.DR.CR.MARKER>:'|'
            FF1.DATA := R.TT<TT.TE.INPUTTER>:'|'
            FF1.DATA := R.TT<TT.TE.AUTHORISER>:'|'
            FF1.DATA := R.TT<TT.TE.LOCAL.REF,TTLR.BK.CUS>:'|'
            FF1.DATA := R.TT<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>:'|'
            FF1.DATA := R.TT<TT.TE.LOCAL.REF,TTLR.ARABIC.NAME>:'|'
            FF1.DATA := R.TT<TT.TE.NARRATIVE.2>:'|'
            FF1.DATA := R.TT<TT.TE.LOCAL.REF,TTLR.BNK.CUSTOMER>:'|'
            FF1.DATA := R.TT<TT.TE.VALUE.DATE.1>:'|'
            FF1.DATA := R.TT<TT.TE.VALUE.DATE.2>:'|'
            FF1.DATA := R.TT<TT.TE.DATE.TIME>:'|'
            FF1.DATA := R.TT<TT.TE.CO.CODE>:'|'
            FF1.DATA := R.TT<TT.TE.LOCAL.REF,TTLR.ATM.NUMBER>:'|'
            FF1.DATA := R.TT<TT.TE.CURRENCY.1>:'|'
            FF1.DATA := R.TT<TT.TE.AMOUNT.LOCAL.1>:'|'
            FF1.DATA := R.TT<TT.TE.AMOUNT.FCY.1>:'|'
            FF1.DATA := R.TT<TT.TE.NEW.CUST.BAL>:'|'
            FF1.DATA := R.TT<TT.TE.CHEQUE.NUMBER>:'|'
            FF1.DATA := R.TT<TT.TE.LOCAL.REF,TTLR.SER.NO>:'|'
            FF1.DATA := R.TT<TT.TE.ACCOUNT.1>:'|'
            CHANGE @VM TO '@VM' IN FF1.DATA
            CHANGE @SM TO '@SM' IN FF1.DATA
*****************WRITE TELLERS
            WRITESEQ FF1.DATA TO FF1 ELSE
                PRINT 'ERROR WRITE FILE TELLERS'
            END
        NEXT I

    END
    GOSUB CLOSE_TT_FILE_L

RETURN

CLOSE_TT_FILE_L:
    CLOSESEQ FF1
    PRINT 'GET.FILE_TELLER FINISHED'

RETURN

****************************SELECT ACCOUNT CLOSED****
PROC_AC.CLOSED:

    T.AC.CLOSE.SEL = 'SELECT ':FN.AC.CLOSED:' WITH ACCT.CLOSE.DATE GE ':LW.DAY
    CRT T.AC.CLOSE.SEL
    OO.DATA = ''

    CALL EB.READLIST(T.AC.CLOSE.SEL,KEY.LIST.AC.CLOSE,"",SELECTED.AC.CLOSE,ERR.AC.CLOSE)
    IF SELECTED.AC.CLOSE THEN
        FOR I = 1 TO SELECTED.AC.CLOSE
*    CALL F.READ(FN.AC.CLOSED,KEY.LIST.AC.CLOSE<I>,R.AC.CLOSE,F.AC.CLOSED,ETEXT.AC.CLOSE)
            AC.CLOSE.ID = KEY.LIST.AC.CLOSE<I>
            OO.DATA = AC.CLOSE.ID

            WRITESEQ OO.DATA TO OO ELSE
                PRINT 'ERROR WRITE FILE GET.FILE_ACCOUNT.CLOSED'
            END
        NEXT I
    END
    GOSUB CLOSE_AC_CLOSE
RETURN

CLOSE_AC_CLOSE:
    CLOSESEQ OO
    PRINT 'GET.FILE_ACCOUNT.CLOSED FINISHED'

RETURN

*****************SELECT STMT.ENTRY************************************
PROC_SE:


    ZZ.DATA = ''

*    T.SE.SEL = 'SELECT ':FN.SE:' WITH PROCESSING.DATE GE ':LW.DAY[3,6]:'0000'
    T.LW.SEL = 'SELECT ':FN.LW.ENT
    CRT T.LW.SEL
    CALL EB.READLIST(T.LW.SEL,KEY.LIST.LW,"",SELECTED.LW,ERR.LW)
    IF SELECTED.LW THEN
        FOR I = 1 TO SELECTED.LW
            CALL F.READ(FN.LW.ENT,KEY.LIST.LW<I>,R.LW.ACCT,F.LW.ENT,ETEXT.ACCT)
*            CRT DCOUNT(R.LW.ACCT,FM)
*Line [ 880 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            FOR J = 1 TO DCOUNT(R.LW.ACCT,@FM)
                CALL F.READ(FN.SE,R.LW.ACCT<J>,R.SE,F.SE,ETEXT.SE)
                SE.ID = R.LW.ACCT<J>
                ZZ.DATA = SE.ID:'|'
                ZZ.DATA := R.SE<AC.STE.TRANSACTION.CODE>:'|'
                ZZ.DATA := R.SE<AC.STE.OUR.REFERENCE>:'|'
                ZZ.DATA := R.SE<AC.STE.AMOUNT.LCY>:'|'
                ZZ.DATA := R.SE<AC.STE.INPUTTER>:'|'
                ZZ.DATA := R.SE<AC.STE.SYSTEM.ID>:'|'
                ZZ.DATA := R.SE<AC.STE.ACCOUNT.NUMBER>:'|'
                ZZ.DATA := R.SE<AC.STE.CUSTOMER.ID>:'|'
                ZZ.DATA := R.SE<AC.STE.DATE.TIME>:'|'
                ZZ.DATA := R.SE<AC.STE.BOOKING.DATE>:'|'
                ZZ.DATA := R.SE<AC.STE.COMPANY.CODE>:'|'
                ZZ.DATA := R.SE<AC.STE.CURRENCY>:'|'
                ZZ.DATA := R.SE<AC.STE.AMOUNT.FCY>:'|'
                ZZ.DATA := R.SE<AC.STE.OUTSTANDING.BAL>:'|'
                CHANGE @VM TO '@VM' IN ZZ.DATA
                CHANGE @SM TO '@SM' IN ZZ.DATA
                WRITESEQ ZZ.DATA TO ZZ ELSE
                    PRINT 'ERROR WRITE FILE GET.FILE_STMT.ENTRY'
                END
            NEXT J
        NEXT I
    END
    GOSUB CLOSE_SE_FILE

RETURN

CLOSE_SE_FILE:
    CLOSESEQ ZZ
    PRINT 'GET.FILE_STMT.ENTRY FINISHED'

RETURN

PRINT.DATE:

    OPENSEQ FOLDER , "AML.DATE.txt" TO QQ THEN
        CLOSESEQ QQ
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':"AML.DATE.txt"
        HUSH OFF
    END
    OPENSEQ FOLDER , "AML.DATE.txt" TO QQ ELSE
        CREATE QQ THEN
            PRINT 'FILE AML.DATE.txt CREATED IN ':FOLDER
        END
        ELSE
            STOP 'Cannot create AML.DATE.txt File IN ':FOLDER
        END
    END

    WRITESEQ LW.DAY TO QQ ELSE
        PRINT 'ERROR WRITE FILE GET.FILE_STMT.ENTRY'
    END


RETURN
**********************************************************************
END
