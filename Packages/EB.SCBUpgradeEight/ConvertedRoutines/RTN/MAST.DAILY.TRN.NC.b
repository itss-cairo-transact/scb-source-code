* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1602</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  MAST.DAILY.TRN.NC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.TRANS.NC
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.CODES
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.TRN.NC
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.ROUTINE.CHK

    Path = "NESRO/MASTUSG"

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    F.MAST.TRANS = '' ; FN.MAST.TRANS = 'F.SCB.MAST.TRANS.NC' ; R.MAST.TRANS = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.MAST.TRANS,F.MAST.TRANS)

**************UPDATED IN 17/2/2008*********************************
**   YTEXT = "Enter the Date : "
**   CALL TXTINP(YTEXT, 8, 22, "12", "A")
    RT.CHK = '' ; FN.RT.CHK = 'F.SCB.MAST.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)
******************************************

    DATEE = '' ; YEAR1 = '' ; MONTH1= '' ; BRAN = ''  ; YEARN = ''
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YEARN = YYYY-1
    END ELSE
        MU = MT-1
        YEARN = YYYY
    END
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END
    YYDD = YEARN:MON:'01'

    YEAR1= YEARN
    MONTH1 = MON
    BR = R.USER<EB.USE.DEPARTMENT.CODE>

    CHK.SEL = "SELECT F.SCB.MAST.ROUTINE.CHK WITH BRANCH EQ ":BR :" AND YEAR EQ ":YEAR1:" AND MONTH EQ ":MONTH1 :" AND DAILY.TRN.NC EQ 'YES' "
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    IF LEN(BR) < 2 THEN
        BRAN = '0':BR
    END ELSE
        BRAN = BR
    END

    KEYID = YEARN:MON:BRAN
    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
******************************************************
        TEXT = 'Start.Of.File' ; CALL REM
**************************************************************
        EOF = ''

        LOOP WHILE NOT(EOF)
            XX= ''
            TRN.CARD.NO = '' ; TRN.FLAG = '' ; TRN.FLAG = '' ; POST.DATE = '' ; TRN.DATE = ''
            TRANS.CURR = '' ; TRN.TYPE = '' ; TRANS.AMT = '' ; TRN.DESC = ''  ; CC.TRN.CARD.NO = ''

            READSEQ Line FROM MyPath THEN
                CARD.NO           = Line[1,16]
                T.SEL =  "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
                KEY.LIST=""
                SELECTED=""
                ER.MSG=""

                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF NOT(SELECTED) THEN
                    TRN.CARD.NO         = Line[20,16]
                    TRN.FLAG            = Line[39,1]
                    POST.DATE.N         = Line[49,6]
                    POST.DATE           = '20':POST.DATE.N
                    TRN.DATE.N          = Line[57,6]
                    TRN.DATE            = '20':TRN.DATE.N
                    TRANS.CURR          = Line[76,3]
                    TRN.TYPE            = Line[79,2]
                    TRANS.AMT           = Line[97,15]
                    TRANS.AMT.1 = TRIM(TRANS.AMT, "0", "L")
                    TRANS.AMT.FMT = TRANS.AMT.1/100
                    TRN.DESC          = Line[113,35]
                    CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN TRN.DESC

                    F.MAST.DAILY.TRN = '' ; FN.MAST.DAILY.TRN = 'F.SCB.MAST.DAILY.TRN.NC' ; R.MAST.DAILY.TRN = '' ; E2 = '' ; RETRY2 = ''
                    CALL OPF(FN.MAST.DAILY.TRN,F.MASY.DAILY.TRN)

                    ID.KEY = CARD.NO:YEARN:MON
                    CALL F.READ(FN.MAST.DAILY.TRN,ID.KEY, R.MAST.DAILY.TRN, F.MAST.DAILY.TRN ,E2)
                    IF NOT(E2) THEN
                        CC.TRN.CARD.NO = R.MAST.DAILY.TRN<MAST.DAILY.TRN.CARD.NO.NC>
*Line [ 145 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DD = DCOUNT(CC.TRN.CARD.NO,@VM)
                        XX = DD+1

                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.CARD.NO.NC,XX> = TRN.CARD.NO
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.FLAG.NC,XX>    = TRN.FLAG
                        R.MAST.DAILY.TRN<MAST.DAILY.POS.DATE.NC,XX>    = POST.DATE
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.DATE.NC,XX>    = TRN.DATE
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.CURR.NC,XX>    = TRANS.CURR
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.TYPE.NC,XX>    = TRN.TYPE
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.AMT.NC,XX>     = TRANS.AMT.FMT
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.DESC.NC,XX>    = TRN.DESC

                        CALL F.WRITE(FN.MAST.DAILY.TRN,ID.KEY, R.MAST.DAILY.TRN)
                        CALL JOURNAL.UPDATE(ID.KEY)
                    END ELSE
                        CC.TRN.CARD.NO = R.MAST.DAILY.TRN<MAST.DAILY.TRN.CARD.NO.NC>
*Line [ 162 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DD = DCOUNT(CC.TRN.CARD.NO,@VM)
                        XX = DD+1

                        R.MAST.DAILY.TRN<MAST.DAILY.CARD.BR.NC>          = ' '
                        R.MAST.DAILY.TRN<MAST.DAILY.CUST.NAME.NC>        = ' '
                        R.MAST.DAILY.TRN<MAST.DAILY.CUST.ACCT.NC>        = ' '
                        R.MAST.DAILY.TRN<MAST.DAILY.CARD.NO.NC>          = CARD.NO

                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.CARD.NO.NC,XX>   = TRN.CARD.NO
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.FLAG.NC,XX>      = TRN.FLAG
                        R.MAST.DAILY.TRN<MAST.DAILY.POS.DATE.NC,XX>      = POST.DATE
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.DATE.NC,XX>      = TRN.DATE
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.CURR.NC,XX>      = TRANS.CURR
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.TYPE.NC,XX>      = TRN.TYPE
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.AMT.NC,XX>       = TRANS.AMT.FMT
                        R.MAST.DAILY.TRN<MAST.DAILY.TRN.DESC.NC,XX>      = TRN.DESC

                        CALL F.WRITE(FN.MAST.DAILY.TRN,ID.KEY, R.MAST.DAILY.TRN)
                        CALL JOURNAL.UPDATE(ID.KEY)
                    END
************TO WRITE TO TABLE SCB.MAST.TRANS**************************

                    CARD.NO.CO = '' ; TRN.TYP.CO = '' ; DESC.CO = '' ; TRN.TYP.CO = '' ; TRN.TYPE.CH = ''

                    CARD.NO.CO = R.MAST.DAILY.TRN<MAST.DAILY.TRN.CARD.NO.NC>
*Line [ 188 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    TT = DCOUNT(CARD.NO.CO,@VM)
                    TRN.TYP.CO    = R.MAST.DAILY.TRN<MAST.DAILY.TRN.TYPE.NC,TT>
*              N.SEL =  "SELECT F.SCB.MAST.CODES WITH TRN.TYPE EQ ":TRN.TYP.CO
                    N.SEL ="SELECT F.SCB.MAST.CODES WITH TRN.TYPE EQ ": "'":TRN.TYP.CO:"'"
                    KEY.LIST.2=""
                    SELECTED.2=""
                    ER.MSG.2=""

                    CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
                    F.MAST.CODE = '' ; FN.MAST.CODE = 'F.SCB.MAST.CODES' ; R.MAST.CODE = '' ; E3 = '' ; RETRY3 = ''
                    CALL OPF(FN.MAST.CODE,F.MAST.CODE)
                    IF SELECTED.2 THEN
                        FOR RR = 1 TO SELECTED.2
                            CALL F.READ(FN.MAST.CODE, KEY.LIST.2<RR>, R.MAST.CODE, F.MAST.CODE, E3)
                            DESC.CO = R.MAST.CODE<MAST.COD.DESCRIPTION>
*Line [ 204 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DCT = DCOUNT(DESC.CO,@VM)
                            FOR WW = 1 TO DCT
                                TRN.TYPE.CH = R.MAST.CODE<MAST.COD.TRN.TYPE,WW>
                                IF TRN.TYP.CO = TRN.TYPE.CH THEN
                                    CODE.TO.USE = KEY.LIST.2<RR>
                                END ELSE
                                END
                            NEXT WW
                        NEXT RR
                        CALL F.READ(FN.MAST.TRANS, ID.KEY, R.MAST.TRANS, F.MAST.TRANS, E2)
                        IF NOT(E2) THEN
                            TRANS.CODE = R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC>
*Line [ 217 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            YY= DTC+1
                            R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC,YY>= CODE.TO.USE
                            R.MAST.TRANS<SCB.MAST.TRANS.CURR.NC,YY>= R.MAST.DAILY.TRN<MAST.DAILY.TRN.CURR.NC,TT>
                            R.MAST.TRANS<SCB.MAST.TRANS.AMT.NC,YY>= R.MAST.DAILY.TRN<MAST.DAILY.TRN.AMT.NC,TT>
                            R.MAST.TRANS<SCB.MAST.POS.DATE.NC,YY>= R.MAST.DAILY.TRN<MAST.DAILY.POS.DATE.NC,TT>
                            CALL F.WRITE(FN.MAST.TRANS,ID.KEY, R.MAST.TRANS)
                            CALL JOURNAL.UPDATE(ID.KEY)
                        END ELSE
                            TRANS.CODE = R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC>
*Line [ 228 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            YY= DTC+1
                            R.MAST.TRANS<SCB.MAST.BRANCH.NUMBER.NC> = ' '
                            R.MAST.TRANS<SCB.MAST.CUST.NAME.NC>= ' '
                            R.MAST.TRANS<SCB.MAST.CUST.ACCT.NC>= ' '
                            R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC,YY>= CODE.TO.USE
                            R.MAST.TRANS<SCB.MAST.TRANS.CURR.NC,YY>= R.MAST.DAILY.TRN<MAST.DAILY.TRN.CURR.NC,TT>
                            R.MAST.TRANS<SCB.MAST.TRANS.AMT.NC,YY>= R.MAST.DAILY.TRN<MAST.DAILY.TRN.AMT.NC,TT>
                            R.MAST.TRANS<SCB.MAST.POS.DATE.NC,YY>= R.MAST.DAILY.TRN<MAST.DAILY.POS.DATE.NC,TT>
                            CALL F.WRITE(FN.MAST.TRANS,ID.KEY, R.MAST.TRANS)
                            CALL JOURNAL.UPDATE(ID.KEY)

                        END
                    END ELSE  ;** END OF SELECTED.2
                        IF TRN.TYP.CO # '80' THEN TEXT = 'NEW CODE=':TRN.TYP.CO ; CALL REM
                    END
**********************************************************************
                END ;**END OF SELECTED***

            END ELSE
                EOF = 1
            END
        REPEAT
        CLOSESEQ MyPath
        TEXT = 'END OF FILE' ; CALL REM
************UPDATED IN 18/2/2008*************************************
*Line [ 261 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
       *** CALL amr.MAST.DEBIT.CUST.TOT
        CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
        R.RT.CHK<RT.CHK.BRANCH> = BR
        R.RT.CHK<RT.CHK.YEAR> = YEAR1
        R.RT.CHK<RT.CHK.MONTH> = MONTH1
        R.RT.CHK<RT.CHK.DAILY.TRN.NC> = 'YES'
        CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
        CALL JOURNAL.UPDATE(KEYID)
**********************************************************************
    END   ;*** END OF SELECTED.CHK****
    RETURN
END
