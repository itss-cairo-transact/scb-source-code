* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
******NESSREEN AHMED***************
*-----------------------------------------------------------------------------
* <Rating>378</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAST.DEBIT.CUST.NEW

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.USAGES.TOT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.ROUTINE.CHK
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* TO DEBIT THE TOTAL USAGES FROM CUSTOMERS VISA ACCOUNT
* AFTER READING THEM FROM SCB.MAST.USAGES.TOT TABLE

    TEXT = 'YARAB' ; CALL REM

    GOSUB INITIALISE
*Line [ 57 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 58 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
    GOSUB CALLDB
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
    RETURN
*==============================================================
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS = "SCB.MAST"
***OFS.USER.INFO = "/"

    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]

**    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
***UPDATED BY NESSREEN AHMED 2/4/2009****************
***   OFS.USER.INFO = "CAIRO22/1234567"
*************HYTHAM********20090318**********
    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*==============================================================
CALLDB:
    FN.ACC= 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    F.MAST.USAGES.TOT = '' ; FN.MAST.USAGES.TOT = 'F.SCB.MAST.USAGES.TOT' ; R.MAST.USAGES.TOT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.MAST.USAGES.TOT,F.MAST.USAGES.TOT)

**********UPDATED IN 21/2/2008***************************
**    YTEXT = "Enter the Start Date Of Month : "
**    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    RT.CHK = '' ; FN.RT.CHK = 'F.SCB.MAST.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)

    BR = R.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YY = YYYY-1
    END ELSE
        MU = MT-1
        YY = YYYY
    END
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END

    YYDD = YY:MON:'01'
    YEAR1= YY
    MONTH1 = MON
    BR = R.USER<EB.USE.DEPARTMENT.CODE>

    CHK.SEL = "SELECT F.SCB.MAST.ROUTINE.CHK WITH BRANCH EQ ":BR :" AND YEAR EQ ":YEAR1:" AND MONTH EQ ":MONTH1 :" AND DEBIT.CUST EQ 'YES' "
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    IF LEN(BR) < 2 THEN
        BRAN = '0':BR
    END ELSE
        BRAN = BR
    END

    KEYID = YY:MON:BRAN
    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
** T.SEL = "SELECT F.SCB.MAST.USAGES.TOT WITH POS.DATE GE ":COMI:" AND TOT.AMT.CR EQ '' BY @ID "
        T.SEL = "SELECT F.SCB.MAST.USAGES.TOT WITH POS.DATE GE ":YYDD:" AND TOT.AMT.DB NE '0' AND TOT.AMT.CR EQ '' BY @ID "
        KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
        IF SELECTED THEN
            TEXT = 'SELECTED=':SELECTED ; CALL REM
            FOR I = 1 TO SELECTED

                VISA.NO = '' ; ACCT.NO = '' ; CUST.NO = '' ; CUST.NAME = '' ; DEP.CODE = '' ; TOT.ALL = ''
                CALL F.READ(FN.MAST.USAGES.TOT,KEY.LIST<I>,R.MAST.USAGES.TOT,F.MAST.USAGES.TOT,ERR2)
                DEP.CODE=R.MAST.USAGES.TOT<USAGES.TOT.M.BRANCH.NUMBER>
                VISA.NO = KEY.LIST<I>[1,16]
                ACCT.NO<I> = R.MAST.USAGES.TOT<USAGES.TOT.M.ACCT.NO>
*Line [ 170 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<I>,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
                TOT.ALL = R.MAST.USAGES.TOT<USAGES.TOT.M.TOT.AMT.DB>
                IF LEN(DEP.CODE) < 2 THEN
                    BR = '0':DEP.CODE
                END ELSE
                    BR = DEP.CODE
                END
*CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO<I>,ACC.CURR)
                ACC.CURR = ACCT.NO<I>[9,2]
*********UPDATED ON 16/10/2008************************
**  BR.ACC = '994999':BR:ACC.CURR:'5010':'01'
*********UPDATED ON 02/05/2010************************
*** BR.ACC = '99499900':ACC.CURR:'5010':'01'
                BR.ACC = 'EGP1124000010099'
******************************************************
                COMMA = ","
                OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":'ACMU':COMMA
                OFS.MESSAGE.DATA := "DEBIT.CUSTOMER=": CUST.NO:COMMA
                OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": ACCT.NO<I>:COMMA
                OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.ALL:COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":BR.ACC:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
                OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE'
************UPDATED BY NESSREEN 03/06/2009***************************
                CALL F.READ(FN.ACC,ACCT.NO<I>,R.ACC,F.ACC,E11)
                COMP = R.ACC<AC.CO.CODE>
                COM.CODE = COMP[8,2]
                OFS.USER.INFO = "VISAMAST//":COMP

**********************************************************************

                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
**                OFS.ID = "T":TNO:".":OPERATOR:"_MAST":RND(10000):".":TODAY
****UPDATED ON ****
                OFS.ID = "T":TNO:"_MAST":RND(10000):".":"DB":I:".":ACCT.NO<I>:".":TODAY:"-":COMP
                WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160628 - S
*                SCB.OFS.SOURCE = "SCBOFFLINE"
*                SCB.OFS.ID = '' ; SCB.OPT = ''
*                CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*                END
*                CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*                OFS.REC = ''
***** SCB R15 UPG 20160628 - E

**************************************************************************
            NEXT I
        END
*Line [ 223 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeEight.MAST.CREDIT.CUST.NEW
        TEXT = 'WRITE' ; CALL REM
        CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
        R.RT.CHK<RT.CHK.BRANCH> = BR
        R.RT.CHK<RT.CHK.YEAR> = YEAR1
        R.RT.CHK<RT.CHK.MONTH> = MONTH1
        R.RT.CHK<RT.CHK.DEBIT.CUST> = 'YES'
        CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
        CALL JOURNAL.UPDATE(KEYID)
**********************************************************************
    END   ;*** END OF SELECTED.CHK****
    RETURN
END
