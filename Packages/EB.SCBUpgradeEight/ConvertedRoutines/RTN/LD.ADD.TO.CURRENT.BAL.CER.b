* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LD.ADD.TO.CURRENT.BAL.CER(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LD = ''
    CALL OPF(FN.LD,F.LD)
    COMP = ID.COMPANY

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT NE 0"
* T.SEL := " AND PRIN.LIQ.ACCT NE 1001 AND PRIN.LIQ.ACCT NE 6501 AND PRIN.LIQ.ACCT NE 6502 AND PRIN.LIQ.ACCT NE 6503 AND PRIN.LIQ.ACCT NE 6504 THEN
    T.SEL := " AND ( CATEGORY GE 21020 AND CATEGORY LE 21032 )"
    T.SEL := " AND COLLATERAL.ID NE '' AND CO.CODE EQ ":COMP

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    TEXT = SELECTED; CALL REM

    ENQ.LP   = '' ;
    OPER.VAL = ''
    I = 0
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            CALL F.READ( FN.LD,KEY.LIST<ENQ.LP>, R.LD, F.LD, ETEXT)
            ACCT = R.LD<LD.PRIN.LIQ.ACCT>
            CATE = ACCT[11,4]
*TEXT = CATE; CALL REM
            IF CATE NE '1001' AND CATE NE '6501' AND CATE NE '6502' AND CATE NE '6503' AND CATE NE '6504' THEN
*TEXT = CATE; CALL REM
                I = I + 1
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<ENQ.LP>
            END ELSE
                I++
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'
            END
        NEXT ENQ.LP
    END ELSE
        I++
        ENQ<2,I> = '@ID'
        ENQ<3,I> = 'EQ'
        ENQ<4,I> = 'DUMMY'


    END

**********************
    RETURN
END
