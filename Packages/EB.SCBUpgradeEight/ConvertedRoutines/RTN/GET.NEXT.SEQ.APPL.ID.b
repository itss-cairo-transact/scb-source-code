* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>425</Rating>
*-----------------------------------------------------------------------------
      SUBROUTINE GET.NEXT.SEQ.APPL.ID(APPL.NAME, ID.LIST, ID.PREF, RET.ID)

* Written by V.Papadopoulos for INFORMER S.A.

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.CAPTURE

      IF APPL.NAME THEN
         IF NOT(RET.ID) THEN
            TD.DATE = TODAY
         END
         ELSE
            TD.DATE = RET.ID
         END

         RET.ID = ''
         FOUND = ''

         IF APPL.NAME<2> THEN
            SEQ.I = APPL.NAME<2>
         END
         ELSE
            SEQ.I = 1
         END

         LOOP WHILE NOT(FOUND) & SEQ.I <= 99999
            FMT.I = STR('0', 5 - LEN(SEQ.I)) : SEQ.I
            JUL.TODAY = ''
            CALL JULDATE(TD.DATE, JUL.TODAY)
            RET.ID = ID.PREF : JUL.TODAY[5] : FMT.I
            DUMMY = ''
            ETEXT = ''
*Line [ 55 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR(APPL.NAME<1>:@FM:1, RET.ID, DUMMY)
F.ITSS.APPL.NAME = 'F.APPL.NAME'
FN.F.ITSS.APPL.NAME = ''
CALL OPF(F.ITSS.APPL.NAME,FN.F.ITSS.APPL.NAME)
CALL F.READ(F.ITSS.APPL.NAME,RET.ID,R.ITSS.APPL.NAME,FN.F.ITSS.APPL.NAME,ERROR.APPL.NAME)
DUMMY=R.ITSS.APPL.NAME<@FM:1>
            IF ETEXT THEN
               ETEXT = ''
*Line [ 59 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*               CALL DBR(APPL.NAME<1>:'$NAU':@FM:1, RET.ID, DUMMY)
F.ITSS.APPL.NAME = 'F.APPL.NAME'
FN.F.ITSS.APPL.NAME = ''
CALL OPF(F.ITSS.APPL.NAME,FN.F.ITSS.APPL.NAME)
CALL F.READ(F.ITSS.APPL.NAME,RET.ID,R.ITSS.APPL.NAME,FN.F.ITSS.APPL.NAME,ERROR.APPL.NAME)
DUMMY=R.ITSS.APPL.NAME<'$NAU':@FM:1>
               IF ETEXT THEN
                  ETEXT = ''
*Line [ 63 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                  CALL DBR(APPL.NAME<1>:'$HIS':@FM:1, RET.ID:';1', DUMMY)
F.ITSS.APPL.NAME = 'F.APPL.NAME'
FN.F.ITSS.APPL.NAME = ''
CALL OPF(F.ITSS.APPL.NAME,FN.F.ITSS.APPL.NAME)
CALL F.READ(F.ITSS.APPL.NAME,RET.ID:';1',R.ITSS.APPL.NAME,FN.F.ITSS.APPL.NAME,ERROR.APPL.NAME)
DUMMY=R.ITSS.APPL.NAME<'$HIS':@FM:1>
                  IF ETEXT THEN
                     LOCATE RET.ID IN ID.LIST<1, 1> SETTING ID.IN ELSE
                        ID.IN = 0
                     END
                     IF NOT(ID.IN) THEN
                        FOUND = RET.ID
                     END
                  END
               END
            END
            SEQ.I += 1
         REPEAT

         IF FOUND & SEQ.I <= 99999 THEN
            RET.ID = FOUND
         END

         ETEXT = ''
      END

      RETURN

   END
