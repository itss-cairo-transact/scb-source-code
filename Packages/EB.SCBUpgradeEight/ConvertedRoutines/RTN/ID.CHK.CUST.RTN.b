* @ValidationCode : MjotMTE1NDQ5NDUwODpDcDEyNTI6MTY0MDcwOTYzMzIwODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:40:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
SUBROUTINE ID.CHK.CUST.RTN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.STMT
*-------------------------------------------
    CUST.ID = COMI
    FN.CUS  = "FBNK.CUSTOMER"  ; F.CUS = ""
    CALL OPF(FN.CUS,F.CUS)

    IF  V$FUNCTION = "I" THEN
        CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,ERR.CUS)
        CUS.EMAIL = R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
        CHK.MAIL = FIELD(CUS.EMAIL,'@',1)

        IF CHK.MAIL EQ '' THEN
            E = "��� ������ ��� �� ���� ��������"
        END
    END
*-------------------------------------------
    CALL REBUILD.SCREEN
*-------------------------------------------
RETURN
END
