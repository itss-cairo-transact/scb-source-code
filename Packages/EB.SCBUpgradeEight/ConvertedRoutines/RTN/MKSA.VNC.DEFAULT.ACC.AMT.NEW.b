* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE MKSA.VNC.DEFAULT.ACC.AMT.NEW
*    PROGRAM    VNC.DEFAULT.ACC.AMT.NEW

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.LCY.REF

    ACC.MAIN = '9943330010508499'

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    FN.ACC = 'F.SCB.BR.LCY.REF' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    T.SEL = "SELECT F.SCB.BR.LCY.REF WITH ACP.REJ EQ 820 AND T.DATE EQ ": TODAY:" BY BRN.NO"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL : " : SELECTED ; CALL REM
    TOT.AMT = 0
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E1)
            CALL F.READ(FN.ACC,KEY.LIST<I+1>,R.ACC.1,F.ACC,E1)
            IF R.ACC<LCY.REF.BRN.NO> EQ R.ACC.1<LCY.REF.BRN.NO> THEN
                AMT       = R.ACC<LCY.REF.AMT>
                TOT.AMT = TOT.AMT + AMT
            END
            ELSE
                TOT.AMT = TOT.AMT +  R.ACC<LCY.REF.AMT>
                BRN       = R.ACC<LCY.REF.BRN.NO>
**         TEXT = BRN :"*":  TOT.AMT ; CALL REM
                GOSUB ADD.BRAN
                TOT.AMT = 0
            END
        NEXT I
        GOSUB LAST.ACC
        RETURN
ADD.BRAN:
        XX = LEN(BRN)
        IF XX LT 2 THEN
            BRN = "0": BRN
        END
        ACC.NO = "99433300105085" : BRN

*Line [ 69 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CUR.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
        NEXT.M = CUR.M + 1
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NEXT.M> = ACC.NO
        R.NEW(INF.MLT.SIGN)<1,NEXT.M> = 'CREDIT'
        R.NEW(INF.MLT.TXN.CODE)<1,NEXT.M> = '79'
        R.NEW(INF.MLT.CURRENCY)<1,NEXT.M> = 'EGP'
        R.NEW(INF.MLT.AMOUNT.LCY)<1,NEXT.M> = TOT.AMT
        RETURN
***********************************************************
LAST.ACC:
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LAST.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
        FOR X = 1 TO LAST.M
            TOTAL.AMT = TOTAL.AMT + R.NEW(INF.MLT.AMOUNT.LCY)<1,X>
        NEXT X
****************************************************************

        LAST.M.NEW = LAST.M + 1
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,LAST.M.NEW> = ACC.MAIN
        R.NEW(INF.MLT.SIGN)<1,LAST.M.NEW> = 'DEBIT'
        R.NEW(INF.MLT.TXN.CODE)<1,LAST.M.NEW> = '78'
        R.NEW(INF.MLT.CURRENCY)<1,LAST.M.NEW> = 'EGP'
        R.NEW(INF.MLT.AMOUNT.LCY)<1,LAST.M.NEW> = TOTAL.AMT
        RETURN
    END
