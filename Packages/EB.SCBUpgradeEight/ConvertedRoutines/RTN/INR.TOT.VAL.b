* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE INR.TOT.VAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

    ETEXT = ""
*---------------------------------------------
    R.NEW(SCB.C.CBE.TOT.US.99)=R.NEW(SCB.C.CBE.TOT.US.1)+R.NEW(SCB.C.CBE.TOT.US.2)+R.NEW(SCB.C.CBE.TOT.US.3)+R.NEW(SCB.C.CBE.TOT.US.4)+R.NEW(SCB.C.CBE.TOT.US.5)+R.NEW(SCB.C.CBE.TOT.US.6)+R.NEW(SCB.C.CBE.TOT.US.7)+R.NEW(SCB.C.CBE.TOT.US.8)+R.NEW(SCB.C.CBE.TOT.US.9)+R.NEW(SCB.C.CBE.TOT.US.10)+R.NEW(SCB.C.CBE.TOT.US.11)+R.NEW(SCB.C.CBE.TOT.US.12)
    R.NEW(SCB.C.CBE.TOT.CR.99)=R.NEW(SCB.C.CBE.TOT.CR.1)+R.NEW(SCB.C.CBE.TOT.CR.2)+R.NEW(SCB.C.CBE.TOT.CR.3)+R.NEW(SCB.C.CBE.TOT.CR.4)+R.NEW(SCB.C.CBE.TOT.CR.5)+R.NEW(SCB.C.CBE.TOT.CR.6)+R.NEW(SCB.C.CBE.TOT.CR.7)+R.NEW(SCB.C.CBE.TOT.CR.8)+R.NEW(SCB.C.CBE.TOT.CR.9)+R.NEW(SCB.C.CBE.TOT.CR.10)+R.NEW(SCB.C.CBE.TOT.CR.11)+R.NEW(SCB.C.CBE.TOT.CR.12)

    RETURN
END
