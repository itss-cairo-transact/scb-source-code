* @ValidationCode : MjotNzA0MTgxNjc2OkNwMTI1MjoxNjQxMzY3MjY0NTc2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 05 Jan 2022 09:21:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*    PROGRAM GOAML.VER2.TEXT
SUBROUTINE GOAML.VER2.TEXT


*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CU.DRMNT.INDEX
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.GOVERNORATE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INDUSTRY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.PROFESSION
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DE.BIC
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER.TRANSACTION
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY

    YTEXT = "Enter Application ID"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    USER.INPUT = COMI


    GOSUB INIT

    YTEXT = "Enter begin date"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    FROM.DATE = COMI

    YTEXT = "Enter end date"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    TO.DATE = COMI


    YTEXT = "Do you want searching in LD HIS.press y or n"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    LD.FLAG = COMI


    USER.INPUT.1 = FMT(USER.INPUT,"R%8")

    IF USER.INPUT[1,2] EQ 'FT' THEN
        APPL.ID = USER.INPUT:';1'
        GOSUB GET.FT
    END
    ELSE IF USER.INPUT[1,2] EQ 'TT' THEN
        APPL.ID = USER.INPUT:';1'
        GOSUB GET.TT
    END
    ELSE IF USER.INPUT[1,2] EQ 'LD' THEN

        APPL.ID = USER.INPUT
        CALL F.READ(FN.LD,APPL.ID,R.LD,F.LD,ETEXT.LD)

        IF NOT(R.LD) THEN
            INX = 1
            TAR.LD = ''
            T.ID = APPL.ID
            LOOP
*    DEBUG
                T.ID= FIELD(T.ID,';',1):';':INX
                CRT '|':T.ID:'|'
                CALL F.READ(FN.LD.HIS,T.ID,TAR.LD,F.LD.HIS,ETEXT.LD)
                INX++
                IF TAR.LD<LD.RECORD.STATUS> EQ 'MAT' THEN
                    R.LD = TAR.LD
                    APPL.ID = T.ID
                END
                IF NOT(TAR.LD)  THEN
                    BREAK
                END
            REPEAT
        END
        IF R.LD THEN
            GOSUB GET.LD
        END
    END
    ELSE IF USER.INPUT MATCH '0N' AND USER.INPUT NE '' AND LEN(USER.INPUT) LE 8 THEN
        T.SEL.CUS = "SELECT FBNK.CUSTOMER WITH @ID EQ ":USER.INPUT
        CALL EB.READLIST(T.SEL.CUS, KEY.LIST.CUS, "", SELECTED.CUS, ERR.CUS)
        CRT T.SEL.CUS
        IF SELECTED.CUS THEN
*            FROM.DATE = TODAY[1,4]-2:TODAY[5,4]
            CRT 'FROM DATE : ':FROM.DATE
            T.SEL.FT = 'SELECT ':FN.FT
            T.SEL.FT := ' WITH PROCESSING.DATE GE ':FROM.DATE:' AND PROCESSING.DATE LE ':TO.DATE
            T.SEL.FT := ' AND RECORD.STATUS EQ "MAT"'
            T.SEL.FT := ' AND ( DEBIT.CUSTOMER EQ ':USER.INPUT:' OR CREDIT.CUSTOMER EQ ':USER.INPUT:' )'
            CRT T.SEL.FT
            CALL EB.READLIST(T.SEL.FT,KEY.LIST.FT,'',SELECTED.FT,ERR.FT)
            IF SELECTED.FT THEN
                FOR K=1 TO SELECTED.FT
                    APPL.ID = KEY.LIST.FT<K>
                    GOSUB GET.FT
                NEXT K
            END

            T.SEL.TT = 'SELECT ':FN.TT:' WITH  CUSTOMER.1 NE "" AND ACCOUNT.1 NE 9943330010508001'
            T.SEL.TT := ' AND AUTH.DATE GE ':FROM.DATE:' AND AUTH.DATE LE ':TO.DATE
            T.SEL.TT := ' AND RECORD.STATUS EQ "MAT"'
            T.SEL.TT := ' AND CUSTOMER.1 EQ ':USER.INPUT
            CRT T.SEL.TT
            CALL EB.READLIST(T.SEL.TT,KEY.LIST.TT,'',SELECTED.TT,ERR.TT)
            IF SELECTED.TT THEN
                FOR K=1 TO SELECTED.TT
                    APPL.ID = KEY.LIST.TT<K>
                    GOSUB GET.TT
                NEXT K
            END


            T.SEL.LD = 'SELECT ':FN.LD
            T.SEL.LD := ' WITH VALUE.DATE GE ':FROM.DATE:' AND VALUE.DATE LE ':TO.DATE
            T.SEL.LD := ' AND ( CUSTOMER.ID EQ ':USER.INPUT:' OR DRAWDOWN.ACCOUNT LIKE ':USER.INPUT.1:'... )'
            CRT T.SEL.LD
            CALL EB.READLIST(T.SEL.LD,KEY.LIST.LD,'',SELECTED.LD,ERR.LD)
            IF SELECTED.LD THEN
                FOR K=1 TO SELECTED.LD
                    CALL F.READ(FN.LD,KEY.LIST.LD<K>,R.LD,F.LD,ETEXT.LD)
                    APPL.ID = KEY.LIST.LD<K>
                    GOSUB GET.LD
                NEXT K

            END

            IF LD.FLAG EQ 'Y' OR LD.FLAG EQ 'y' THEN
                T.SEL.LD = 'SELECT ':FN.LD.HIS
                T.SEL.LD := ' WITH VALUE.DATE GE ':FROM.DATE:' AND VALUE.DATE LE ':TO.DATE
                T.SEL.LD := ' AND RECORD.STATUS EQ "MAT"'
                T.SEL.LD := ' AND ( CUSTOMER.ID EQ ':USER.INPUT:' OR DRAWDOWN.ACCOUNT LIKE ':USER.INPUT.1:'... )'
                CRT T.SEL.LD
                CALL EB.READLIST(T.SEL.LD,KEY.LIST.LD,'',SELECTED.LD,ERR.LD)
                IF SELECTED.LD THEN
                    FOR K=1 TO SELECTED.LD
                        CALL F.READ(FN.LD.HIS,KEY.LIST.LD<K>,R.LD,F.LD.HIS,ETEXT.LD)
                        APPL.ID = KEY.LIST.LD<K>
                        GOSUB GET.LD
                    NEXT K

                END
            END






        END

    END
    ELSE
        TEXT = 'Input (FUNDS TRANSFER) OR (TELLER) OR (LOANS AND DEPOSITS) OR (CUSTOMER) id , try again please' ; CALL REM
    END
    TEXT = 'Program Done' ; CALL REM
RETURN

INIT:
*     DEBUG

    OPENSEQ "&SAVEDLISTS&" , "TRANSACTION.csv" TO GG THEN
        CLOSESEQ GG
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"TRANSACTION.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "TRANSACTION.csv" TO GG ELSE
        CREATE GG THEN
            PRINT 'FILE TRANSACTION.csv CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create TRANSACTION.csv File IN &SAVEDLISTS&'
        END
    END

    OPENSEQ "&SAVEDLISTS&" , "ACCOUNT.csv" TO AA THEN
        CLOSESEQ AA
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ACCOUNT.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ACCOUNT.csv" TO AA ELSE
        CREATE AA THEN
            PRINT 'FILE ACCOUNT.csv CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create ACCOUNT.csv File IN &SAVEDLISTS&'
        END
    END

    OPENSEQ "&SAVEDLISTS&" , "PERSON.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"PERSON.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "PERSON.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE PERSON.csv CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create PERSON.csv File IN &SAVEDLISTS&'
        END
    END

    OPENSEQ "&SAVEDLISTS&" , "ENTITY.csv" TO CC THEN
        CLOSESEQ CC
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ENTITY.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ENTITY.csv" TO CC ELSE
        CREATE CC THEN
            PRINT 'FILE ENTITY.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create ENTITY.csv File IN &SAVEDLISTS&'
        END
    END

    OPENSEQ "&SAVEDLISTS&" , "SIGNATORY.csv" TO DD THEN
        CLOSESEQ DD
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SIGNATORY.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SIGNATORY.csv" TO DD ELSE
        CREATE DD THEN
            PRINT 'FILE SIGNATORY.csv CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create SIGNATORY.csv File IN &SAVEDLISTS&'
        END
    END
    OPENSEQ "&SAVEDLISTS&" , "DIRECTOR.csv" TO QQ THEN
        CLOSESEQ QQ
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DIRECTOR.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DIRECTOR.csv" TO QQ ELSE
        CREATE QQ THEN
            PRINT 'FILE DIRECTOR.csv CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create DIRECTOR.csv File IN &SAVEDLISTS&'
        END
    END


    FROM.DATE = ''
    TO.DATE = ''
    FN.FT = "FBNK.FUNDS.TRANSFER$HIS" ; F.FT = ''
    CALL OPF(FN.FT , F.FT)

    FN.TT = "FBNK.TELLER$HIS" ; F.TT = ''
    CALL OPF(FN.TT , F.TT)

*    FN.CUS = "FBNK.CUSTOMER" ; F.CUS = ''
*    CALL OPF(FN.CUS , F.CUS)

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC = ''
    CALL OPF(FN.ACC , F.ACC)

    FN.CUS = "FBNK.CUSTOMER" ; F.CUS = '' ; T.SEL.CUS = '' ; KEY.LIST.CUS = '' ; ERR.CUS = '' ; ETEXT.CUS = ''
    CALL OPF(FN.CUS , F.CUS)

    FN.LD.HIS = "FBNK.LD.LOANS.AND.DEPOSITS$HIS" ; F.LD.HIS = ''
    CALL OPF(FN.LD.HIS , F.LD.HIS)

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    CALL OPF(FN.LD , F.LD)

    T.SEL.LD.HIS = '' ; KEY.LIST.LD = '' ; ERR.LD = '' ; ETEXT.LD = ''

    TRN.ID = '' ; IND = ''

    FN.DRMNT.INDEX = 'F.SCB.CU.DRMNT.INDEX' ; F.DRMNT.INDEX = ''
    CALL OPF(FN.DRMNT.INDEX , F.DRMNT.INDEX)

    FN.STMT.ENTRY = 'FBNK.STMT.ENTRY' ; FV.STMT.ENTRY = ''
    CALL OPF(FN.STMT.ENTRY,FV.STMT.ENTRY)

*    FROM.DATE = '20100101'
    TRX.PROCESS.DATE = ''
    AGENT.TYPE = ''
    CUS.ID = ''
    PRD.ID = ''
    FROM.CLIENT = '' ; TO.CLIENT = ''
    DRMNT.CODE = ''
    FROM.CUS = '' ; TO.CUS = ''



RETURN

GET.FT:
    PRD.ID = 'FT'
*DEBUG
*    T.SEL.FT = 'SELECT ':FN.FT :' SAMPLE 200 BY-DSND PROCESSING.DATE'

*    T.SEL.FT = 'SELECT ':FN.FT :' WITH RECORD.STATUS EQ "MAT"'
*    T.SEL.FT := ' AND PROCESSING.DATE GE ':FROM.DATE:' AND @ID EQ "FT1903004952;1" SAMPLE 200'


*   CALL EB.READLIST(T.SEL.FT,KEY.LIST.FT,'',SELECTED.FT,ERR.FT)
*   IF SELECTED.FT THEN
*        FOR I=1 TO SELECTED.FT
*DEBUG
*            CALL F.READ(FN.FT,KEY.LIST.FT<I>,R.FT,F.FT,ETEXT.FT)
*    APPL.ID = APPL.ID:';1'
    CALL F.READ(FN.FT,APPL.ID,R.FT,F.FT,ETEXT.FT)
    CHANGE ',' TO '-' IN R.FT
*DEBUG
    IF R.FT THEN

        IF R.FT<FT.DEBIT.ACCT.NO>[1,1] MATCH '1A' AND R.FT<FT.CREDIT.ACCT.NO>[1,1] MATCH '1A' THEN
            TEXT = 'Transactoin ':APPL.ID:' has internal accounts only' ; CALL REM
        END
        TRANSMODE.COMMENT = R.FT<FT.LOCAL.REF,FTLR.FOR.BK.CHR.COM>
*Line [ 362 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        CHANGE @SM TO ' ' IN TRANSMODE.COMMENT
*DEBUG
        GG.DATA = APPL.ID:','
        GG.DATA := ','
        GG.DATA := R.FT<FT.CO.CODE>:','
*Line [ 368 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('FT.TXN.TYPE.CONDITION':@FM:FT.TRANSACTION.TYPE,R.FT<FT.TRANSACTION.TYPE>,TRX.DESC)
F.ITSS.FT.TXN.TYPE.CONDITION = 'F.FT.TXN.TYPE.CONDITION'
FN.F.ITSS.FT.TXN.TYPE.CONDITION = ''
CALL OPF(F.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION)
CALL F.READ(F.ITSS.FT.TXN.TYPE.CONDITION,R.FT<FT.TRANSACTION.TYPE>,R.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION,ERROR.FT.TXN.TYPE.CONDITION)
TRX.DESC=R.ITSS.FT.TXN.TYPE.CONDITION<FT.TRANSACTION.TYPE>
        GG.DATA := TRX.DESC:','
        GG.DATA := R.FT<FT.PROCESSING.DATE>:','
        GG.DATA := ',,,'
        GG.DATA := R.FT<FT.CREDIT.VALUE.DATE>:','
        GG.DATA := R.FT<FT.DEBIT.VALUE.DATE>:','
        GG.DATA := R.FT<FT.TRANSACTION.TYPE>:','
        GG.DATA := TRANSMODE.COMMENT:','
        GG.DATA := R.FT<FT.LOC.AMT.DEBITED>:','
        GG.DATA := R.FT<FT.DEBIT.CURRENCY>:','
        IF R.FT<FT.DEBIT.CURRENCY> NE 'EGP' THEN
            GG.DATA := R.FT<FT.DEBIT.AMOUNT>:','
        END ELSE
            GG.DATA := ','
        END
        IF R.FT<FT.TRANSACTION.TYPE> EQ 'OT03' OR R.FT<FT.TRANSACTION.TYPE> EQ 'OT05' OR R.FT<FT.TRANSACTION.TYPE> EQ 'OT06' THEN
            FROM.CLIENT = 'Y'
            TO.CLIENT = 'N'
        END
        ELSE IF R.FT<FT.TRANSACTION.TYPE> EQ 'IT' OR R.FT<FT.TRANSACTION.TYPE> EQ 'IM' OR R.FT<FT.TRANSACTION.TYPE> EQ 'IT01' THEN
            FROM.CLIENT = 'N'
            TO.CLIENT = 'Y'
        END
        ELSE
            FROM.CLIENT = 'Y'
            TO.CLIENT = 'Y'
        END

        IF R.FT<FT.TRANSACTION.TYPE> EQ 'AC3A' OR R.FT<FT.TRANSACTION.TYPE> EQ 'AC30' OR R.FT<FT.TRANSACTION.TYPE> EQ 'AC31' OR R.FT<FT.TRANSACTION.TYPE> EQ 'AC32' OR R.FT<FT.TRANSACTION.TYPE> EQ 'AC33' THEN
            FROM.TYPE = 'A'
            TO.TYPE = 'P'
*Line [ 405 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,R.FT<FT.DEBIT.ACCT.NO>,FROM.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.FT<FT.DEBIT.ACCT.NO>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
FROM.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
            TO.CUS = FROM.CUS
        END
        ELSE
            IF R.FT<FT.TRANSACTION.TYPE> EQ 'ACVA' THEN
                FROM.TYPE = 'P'
                TO.TYPE = 'A'
*Line [ 418 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,R.FT<FT.CREDIT.ACCT.NO>,TO.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.FT<FT.CREDIT.ACCT.NO>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
TO.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
                FROM.CUS = TO.CUS

            END
            ELSE
                FROM.TYPE = 'A'
                TO.TYPE = 'A'
*Line [ 431 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,R.FT<FT.DEBIT.ACCT.NO>,FROM.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.FT<FT.DEBIT.ACCT.NO>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
FROM.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 438 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,R.FT<FT.CREDIT.ACCT.NO>,TO.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.FT<FT.CREDIT.ACCT.NO>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
TO.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
            END


            GG.DATA := R.FT<FT.TREASURY.RATE>:','
            GG.DATA := FROM.CLIENT:','
*        GG.DATA := 'A':','
*   GG.DATA := FROM.TYPE:','
*Line [ 452 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,FROM.CUS,CUS.LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,FROM.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            IF CUS.LOCAL<1,CULR.NEW.SECTOR> NE '4650' AND CUS.LOCAL THEN
                IF FROM.TYPE EQ 'A' THEN
                    GG.DATA := 'EA':','
                END
                ELSE IF FROM.TYPE EQ 'P' THEN
                    GG.DATA := 'E':','
                END
                ELSE
                    GG.DATA := FROM.TYPE:','
                END

            END
            ELSE
                GG.DATA := FROM.TYPE:','
            END

            GG.DATA := ',,'
            IF R.FT<FT.TRANSACTION.TYPE> EQ 'OT03' OR R.FT<FT.TRANSACTION.TYPE> EQ 'OT05' THEN
*DEBUG
*                GG.DATA := 'EG':','
                FROM.COUNTRY.CODE = 'EG'
                ACT.BAK  = R.FT<FT.ACCT.WITH.BANK>[4,15]
*Line [ 481 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('DE.BIC':@FM:DE.BIC.INSTITUTION,ACT.BAK,INST)
F.ITSS.DE.BIC = 'F.DE.BIC'
FN.F.ITSS.DE.BIC = ''
CALL OPF(F.ITSS.DE.BIC,FN.F.ITSS.DE.BIC)
CALL F.READ(F.ITSS.DE.BIC,ACT.BAK,R.ITSS.DE.BIC,FN.F.ITSS.DE.BIC,ERROR.DE.BIC)
INST=R.ITSS.DE.BIC<DE.BIC.INSTITUTION>
*Line [ 488 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('DE.BIC':@FM:DE.BIC.COUNTRY.CODE,ACT.BAK,COUNTRY.CODE)
F.ITSS.DE.BIC = 'F.DE.BIC'
FN.F.ITSS.DE.BIC = ''
CALL OPF(F.ITSS.DE.BIC,FN.F.ITSS.DE.BIC)
CALL F.READ(F.ITSS.DE.BIC,ACT.BAK,R.ITSS.DE.BIC,FN.F.ITSS.DE.BIC,ERROR.DE.BIC)
COUNTRY.CODE=R.ITSS.DE.BIC<DE.BIC.COUNTRY.CODE>

            END
            ELSE IF R.FT<FT.TRANSACTION.TYPE> EQ 'IT' OR R.FT<FT.TRANSACTION.TYPE> EQ 'IM' THEN
*DEBUG
                ACT.BAK = R.FT<FT.ORDERING.BANK>[4,15]
*Line [ 500 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('DE.BIC':@FM:DE.BIC.INSTITUTION,ACT.BAK,INST)
F.ITSS.DE.BIC = 'F.DE.BIC'
FN.F.ITSS.DE.BIC = ''
CALL OPF(F.ITSS.DE.BIC,FN.F.ITSS.DE.BIC)
CALL F.READ(F.ITSS.DE.BIC,ACT.BAK,R.ITSS.DE.BIC,FN.F.ITSS.DE.BIC,ERROR.DE.BIC)
INST=R.ITSS.DE.BIC<DE.BIC.INSTITUTION>
*Line [ 507 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('DE.BIC':@FM:DE.BIC.COUNTRY.CODE,ACT.BAK,FROM.COUNTRY.CODE)
F.ITSS.DE.BIC = 'F.DE.BIC'
FN.F.ITSS.DE.BIC = ''
CALL OPF(F.ITSS.DE.BIC,FN.F.ITSS.DE.BIC)
CALL F.READ(F.ITSS.DE.BIC,ACT.BAK,R.ITSS.DE.BIC,FN.F.ITSS.DE.BIC,ERROR.DE.BIC)
FROM.COUNTRY.CODE=R.ITSS.DE.BIC<DE.BIC.COUNTRY.CODE>
                COUNTRY.CODE = 'EG'
            END ELSE
                COUNTRY.CODE = ''
                FROM.COUNTRY.CODE = ''
            END
            GG.DATA := FROM.COUNTRY.CODE:','
            GG.DATA := TO.CLIENT:','
*        GG.DATA := 'A':','
*            GG.DATA := TO.TYPE:','

*Line [ 524 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,TO.CUS,CUS.LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,TO.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            IF CUS.LOCAL<1,CULR.NEW.SECTOR> NE '4650' AND CUS.LOCAL THEN
                IF TO.TYPE EQ 'A' THEN
                    GG.DATA := 'EA':','
                END
                ELSE IF TO.TYPE EQ 'P' THEN
                    GG.DATA := 'E':','
                END
                ELSE
                    GG.DATA := TO.TYPE:','
                END

            END
            ELSE
                GG.DATA := TO.TYPE:','
            END
            GG.DATA := ',,'

            GG.DATA := COUNTRY.CODE:','
            GG.DATA := '0,'



***************** WRITE FUNDS.TRANSFERS
            WRITESEQ GG.DATA TO GG ELSE
                PRINT 'ERROR WRITE FILE TRANSACTION'
            END
*        AGENT.TYPE = 'A'
            AGENT.TYPE = FROM.TYPE

            TRX.PROCESS.DATE = R.FT<FT.PROCESSING.DATE>
            DEBIT.ACCOUNT = R.FT<FT.DEBIT.ACCT.NO>
            CREDIT.ACCOUNT = R.FT<FT.CREDIT.ACCT.NO>
            TRN.ID = APPL.ID
            ACC.ID  = R.FT<FT.DEBIT.ACCT.NO>
*        CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.ID,CUS.ID)
            CUS.ID = FROM.CUS
            IND = 'FROM'
            GOSUB GET.ACC
            ACC.ID = R.FT<FT.CREDIT.ACCT.NO>
*        CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.ID,CUS.ID)
            CUS.ID = TO.CUS
*        AGENT.TYPE = 'A'
            AGENT.TYPE = TO.TYPE
            IND = 'TO'
            GOSUB GET.ACC
        END
        FROM.COUNTRY.CODE = ''
        TRX.PROCESS.DATE = ''
        COUNTRY.CODE = ''
        DEBIT.ACCOUNT = ''
        CREDIT.ACCOUNT = ''
        TRN.ID = ''
        ACC.ID = ''
        CUS.ID = ''
        TRANSMODE.COMMENT = ''
        IND = ''
        FROM.CLIENT = ''
        TO.CLIENT = ''
        AGENT.TYPE = ''
        FROM.TYPE = ''
        TO.TYPE = ''
        CUS.LOCAL = ''
        FROM.CUS = '' ; TO.CUS = ''

*        NEXT I


*    END

        RETURN

GET.TT:

        PRD.ID = 'TT'
        DPST.NSN = ''
        FROM.CUS = ''
        TO.CUS = ''
*    T.SEL.TT = 'SELECT ':FN.TT:' WITH  CUSTOMER.1 NE "" AND ACCOUNT.1 NE 9943330010508001 '
*    T.SEL.TT := ' AND AUTH.DATE GE ':FROM.DATE
*    T.SEL.TT := ' AND RECORD.STATUS EQ "MAT" AND @ID EQ "TT1807002861;1" SAMPLE 200'
*        T.SEL.TT := ' AND @ID LIKE "':APPL.ID:';1"'
*    CRT T.SEL.TT
*    CALL EB.READLIST(T.SEL.TT,KEY.LIST.TT,'',SELECTED.TT,ERR.TT)

*    IF SELECTED.TT THEN
*        FOR I=1 TO SELECTED.TT

*            CALL F.READ(FN.TT,KEY.LIST.TT<I>,R.TT,F.TT,ETEXT.TT)
*    APPL.ID = APPL.ID:';1'
        CALL F.READ(FN.TT,APPL.ID,R.TT,F.TT,ETEXT.TT)
        CHANGE ',' TO '-' IN R.TT
        IF R.TT THEN
*        T.SEL.CUS = 'SELECT ':FN.CUS:' WITH NSN.NO EQ ':R.TT<TT.TE.NARRATIVE.2,2>
            DPST.NSN = R.TT<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>
            T.SEL.CUS = 'SELECT ':FN.CUS:' WITH POSTING.RESTRICT LT 89 AND ( NSN.NO EQ ':DPST.NSN:' OR ID.NUMBER EQ ':DPST.NSN:' OR COM.REG.NO EQ ':DPST.NSN:' )'
            CRT T.SEL.CUS
            CALL EB.READLIST(T.SEL.CUS,KEY.LIST.CUS,'',SELECTED.CUS,ERR.CUS)
            TRX.PROCESS.DATE = R.TT<TT.TE.AUTH.DATE>

            IF R.TT<TT.TE.DR.CR.MARKER> EQ 'DEBIT' THEN
                DEBIT.ACCOUNT = R.TT<TT.TE.ACCOUNT.1>
                CREDIT.ACCOUNT = R.TT<TT.TE.ACCOUNT.2>
                TRN.ID = APPL.ID
                ACC.ID  = R.TT<TT.TE.ACCOUNT.1>
*Line [ 635 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.ID,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
                FROM.CUS = CUS.ID
                IND = 'FROM'
                IF R.TT<TT.TE.TRANSACTION.CODE> EQ '23' THEN
                    AGENT.TYPE = 'P'
                    CUS.ID = KEY.LIST.CUS<1>
                    FROM.CUS = KEY.LIST.CUS<1>
                    IF CUS.ID NE '' THEN
                        FROM.CLIENT = 'Y'
                    END
                    ELSE
                        FROM.CLIENT = 'N'
                    END
                END
                ELSE
                    AGENT.TYPE = 'A'
                    FROM.CLIENT = 'Y'
                END

                FROM.TYPE = AGENT.TYPE
                GOSUB GET.ACC
*            ACC.ID = R.TT<TT.TE.ACCOUNT.2>
                ACC.ID = ''
                CUS.ID = KEY.LIST.CUS<1>
                TO.CUS = CUS.ID
                IND = 'TO'
                AGENT.TYPE = 'P'
                IF CUS.ID NE '' THEN
                    TO.CLIENT = 'Y'
                END
                ELSE
                    TO.CLIENT = 'N'
                END
                TO.TYPE = AGENT.TYPE
                GOSUB GET.ACC
            END
            ELSE

                DEBIT.ACCOUNT = R.TT<TT.TE.ACCOUNT.1>
                CREDIT.ACCOUNT = R.TT<TT.TE.ACCOUNT.2>
                TRN.ID = APPL.ID
*            ACC.ID  = R.TT<TT.TE.ACCOUNT.2>
                ACC.ID = ''
                CUS.ID = KEY.LIST.CUS<1>
                FROM.CUS = KEY.LIST.CUS<1>
                IND = 'FROM'
                AGENT.TYPE = 'P'
                IF CUS.ID NE '' THEN
                    FROM.CLIENT = 'Y'
                END
                ELSE
                    FROM.CLIENT = 'N'
                END
                FROM.TYPE = AGENT.TYPE
                GOSUB GET.ACC
                ACC.ID = R.TT<TT.TE.ACCOUNT.1>
*Line [ 697 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.ID,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
                TO.CUS = CUS.ID
                IND = 'TO'
                IF R.TT<TT.TE.TRANSACTION.CODE> EQ '24' THEN
                    AGENT.TYPE = 'P'
                    IF CUS.ID NE '' THEN
                        TO.CLIENT = 'Y'
                    END
                    ELSE
                        TO.CLIENT = 'N'
                    END
                END
                ELSE
                    AGENT.TYPE = 'A'
                    TO.CLIENT = 'Y'
                END
                TO.TYPE = AGENT.TYPE
                GOSUB GET.ACC
            END

*        TT.NAR.2 = R.TT<TT.TE.NARRATIVE.2>
*            TT.NAR.2 = R.TT<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>
            TT.NAR.2 = R.TT<TT.TE.LOCAL.REF,TTLR.ARABIC.NAME>:' ':R.TT<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>
*            CHANGE VM TO ' ' IN TT.NAR.2
            TT.DEPO.PURP = R.TT<TT.TE.LOCAL.REF,TTLR.DEPOSIT.PURPOSE>
*Line [ 650 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CHANGE @SM TO ' ' IN TT.DEPO.PURP
            TT.COMMENT = TT.NAR.2:' ':TT.DEPO.PURP
            TT.ID = APPL.ID
            GG.DATA  = APPL.ID:','
            GG.DATA := ','
            GG.DATA := R.TT<TT.TE.CO.CODE>:','
*Line [ 735 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('TELLER.TRANSACTION':@FM:TT.TR.DESC:@FM:'F',R.TT<TT.TE.TRANSACTION.CODE>,TT.TRX.DESC)
F.ITSS.TELLER.TRANSACTION = 'FBNK.TELLER.TRANSACTION'
FN.F.ITSS.TELLER.TRANSACTION = ''
CALL OPF(F.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION)
CALL F.READ(F.ITSS.TELLER.TRANSACTION,R.TT<TT.TE.TRANSACTION.CODE>,R.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION,ERROR.TELLER.TRANSACTION)
TT.TRX.DESC=R.ITSS.TELLER.TRANSACTION<TT.TR.DESC:@FM:'F'>
            GG.DATA := TT.TRX.DESC<1,1,2>:','
            GG.DATA := R.TT<TT.TE.AUTH.DATE>:','
            GG.DATA := ',,,'
            GG.DATA := R.TT<TT.TE.AUTH.DATE>:','
            GG.DATA := R.TT<TT.TE.VALUE.DATE.1>:','
            GG.DATA := R.TT<TT.TE.TRANSACTION.CODE>:','
            GG.DATA := TT.COMMENT:','
            GG.DATA := R.TT<TT.TE.AMOUNT.LOCAL.1>:','
            GG.DATA := R.TT<TT.TE.CURRENCY.1>:','
            GG.DATA := R.TT<TT.TE.AMOUNT.FCY.1>:','
            GG.DATA := R.TT<TT.TE.RATE.1>:','
            GG.DATA := FROM.CLIENT:','
*            GG.DATA := FROM.TYPE:','
*Line [ 755 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,FROM.CUS,CUS.LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,FROM.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            IF CUS.LOCAL<1,CULR.NEW.SECTOR> NE '4650' AND CUS.LOCAL THEN
                IF FROM.TYPE EQ 'A' THEN
                    GG.DATA := 'EA':','
                END
                ELSE IF FROM.TYPE EQ 'P' THEN
                    GG.DATA := 'E':','
                END
                ELSE
                    GG.DATA := FROM.TYPE:','
                END

            END
            ELSE
                GG.DATA := FROM.TYPE:','
            END
            GG.DATA := ',,'
            GG.DATA := 'EG':','
            GG.DATA := TO.CLIENT:','
*            GG.DATA := TO.TYPE:','
*Line [ 781 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,TO.CUS,CUS.LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,TO.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            IF CUS.LOCAL<1,CULR.NEW.SECTOR> NE '4650' AND CUS.LOCAL THEN
                IF TO.TYPE EQ 'A' THEN
                    GG.DATA := 'EA':','
                END
                ELSE IF TO.TYPE EQ 'P' THEN
                    GG.DATA := 'E':','
                END
                ELSE
                    GG.DATA := TO.TYPE:','
                END

            END
            ELSE
                GG.DATA := TO.TYPE:','
            END
            GG.DATA := ',,'
            GG.DATA := 'EG':','
            GG.DATA := '0,'


*****************WRITE TELLERS
            WRITESEQ GG.DATA TO GG ELSE
                PRINT 'ERROR WRITE FILE TELLERS'
            END

            TRX.PROCESS.DATE = ''
            DEBIT.ACCOUNT = ''
            CREDIT.ACCOUNT = ''
            TRN.ID = ''
            ACC.ID = ''
            IND = ''
            PRD.ID = ''
            CUS.ID = ''
            AGENT.TYPE = ''
            FROM.TYPE = ''
            TO.TYPE = ''
            FROM.CLIENT = ''
            TO.CLIENT = ''
            DPST.NSN = ''
            FROM.CUS = '' ; TO.CUS = '' ; CUS.LOCAL = ''

        END
*        NEXT I

        RETURN

GET.LD:
        PRD.ID = 'LD'
*    APPL.ID = APPL.ID:';1'

        IF R.LD THEN
            CHANGE ',' TO '-' IN R.LD
            GG.DATA = APPL.ID:','
            GG.DATA := ','
            GG.DATA := R.LD<LD.CO.CODE>:','
*Line [ 843 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,R.LD<LD.CATEGORY>,CAT.DESC)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,R.LD<LD.CATEGORY>,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CAT.DESC=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
            GG.DATA := CAT.DESC:','
            GG.DATA := R.LD<LD.VALUE.DATE>:','
            GG.DATA := ',,,'
            GG.DATA := R.LD<LD.VALUE.DATE>:','
            GG.DATA := R.LD<LD.VALUE.DATE>:','
            GG.DATA := R.LD<LD.CATEGORY>:','
            GG.DATA := ','
            LD.RATE = R.LD<LD.EXCG.RATE,1>
            IF R.LD<LD.CURRENCY> EQ 'EGP' THEN
                GG.DATA := R.LD<LD.DRAWDOWN.ISSUE.PRC>:','
            END ELSE
                GG.DATA := R.LD<LD.DRAWDOWN.ISSUE.PRC> * LD.RATE :','
            END
            GG.DATA := R.LD<LD.CURRENCY>:','
            IF R.LD<LD.CURRENCY> NE 'EGP' THEN
                GG.DATA := R.LD<LD.DRAWDOWN.ISSUE.PRC>:','
            END
            ELSE
                GG.DATA := ','
            END
            GG.DATA := LD.RATE:','
            FROM.CLIENT = 'Y'
            TO.CLIENT = 'Y'
            GG.DATA := FROM.CLIENT:','

*Line [ 875 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,R.LD<LD.DRAWDOWN.ACCOUNT>,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.LD<LD.DRAWDOWN.ACCOUNT>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*            GG.DATA := 'A':','
*Line [ 883 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,CUS.LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            IF CUS.LOCAL<1,CULR.NEW.SECTOR> NE '4650' AND CUS.LOCAL THEN
                GG.DATA := 'EA':','
            END
            ELSE
                GG.DATA := 'A':','
            END
            GG.DATA := ',,'
            GG.DATA := 'EG':','
            GG.DATA := TO.CLIENT:','
*            GG.DATA := 'A':','
            IF CUS.LOCAL<1,CULR.NEW.SECTOR> NE '4650' AND CUS.LOCAL THEN
                GG.DATA := 'EA':','
            END
            ELSE
                GG.DATA := 'A':','
            END
            GG.DATA := ',,'
            GG.DATA := 'EG':',0,'

***************** WRITE FUNDS.TRANSFERS
            WRITESEQ GG.DATA TO GG ELSE
                PRINT 'ERROR WRITE FILE TRANSACTION'
            END

            AGENT.TYPE = 'A'
            TRX.PROCESS.DATE = R.LD<LD.VALUE.DATE>
            DEBIT.ACCOUNT = R.LD<LD.DRAWDOWN.ACCOUNT>
            CREDIT.ACCOUNT = R.LD<LD.DRAWDOWN.ACCOUNT>
            TRN.ID = APPL.ID
            ACC.ID  = R.LD<LD.DRAWDOWN.ACCOUNT>
*Line [ 920 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.ID,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
            IND = 'FROM'
            GOSUB GET.ACC
            ACC.ID = R.LD<LD.DRAWDOWN.ACCOUNT>
*Line [ 930 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.ID,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
            AGENT.TYPE = 'A'
            IND = 'TO'
            GOSUB GET.ACC

        END
        TRX.PROCESS.DATE = ''
        DEBIT.ACCOUNT = ''
        CREDIT.ACCOUNT = ''
        TRN.ID = ''
        LD.RATE = ''
        ACC.ID = ''
        CUS.ID = ''
        IND = ''
        CUS.LOCAL = ''

        RETURN

GET.ACC:
*DEBUG
        IF AGENT.TYPE EQ 'A' THEN
            CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ETEXT.ACC)

            CHANGE ',' TO '-' IN R.ACC
            AA.DATA = TRN.ID:IND:','
            AA.DATA := 'Suez Canal Bank':','
            AA.DATA := '001700':','
            AA.DATA := 'SUCAEGCXXXX':','
            AA.DATA := R.ACC<AC.CO.CODE>:','
            AA.DATA := ACC.ID:','
            AA.DATA := R.ACC<AC.CURRENCY>:','
            AA.DATA := R.ACC<AC.ACCOUNT.TITLE.1>:','
            AA.DATA := ','
            AA.DATA := R.ACC<AC.CUSTOMER>:','
            AA.DATA := R.ACC<AC.CATEGORY>:','
            AA.DATA := R.ACC<AC.OPENING.DATE>:','
*Line [ 972 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('ACCOUNT.CLOSED':@FM:1,ACC.ID,CLOSE.DATE)
F.ITSS.ACCOUNT.CLOSED = 'FBNK.ACCOUNT.CLOSED'
FN.F.ITSS.ACCOUNT.CLOSED = ''
CALL OPF(F.ITSS.ACCOUNT.CLOSED,FN.F.ITSS.ACCOUNT.CLOSED)
CALL F.READ(F.ITSS.ACCOUNT.CLOSED,ACC.ID,R.ITSS.ACCOUNT.CLOSED,FN.F.ITSS.ACCOUNT.CLOSED,ERROR.ACCOUNT.CLOSED)
CLOSE.DATE=R.ITSS.ACCOUNT.CLOSED<1>
            IF NOT(R.ACC) AND CLOSE.DATE THEN
*Line [ 980 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('ACCOUNT.CLOSED':@FM:1,ACC.ID,CLOSE.DATE)
F.ITSS.ACCOUNT.CLOSED = 'FBNK.ACCOUNT.CLOSED'
FN.F.ITSS.ACCOUNT.CLOSED = ''
CALL OPF(F.ITSS.ACCOUNT.CLOSED,FN.F.ITSS.ACCOUNT.CLOSED)
CALL F.READ(F.ITSS.ACCOUNT.CLOSED,ACC.ID,R.ITSS.ACCOUNT.CLOSED,FN.F.ITSS.ACCOUNT.CLOSED,ERROR.ACCOUNT.CLOSED)
CLOSE.DATE=R.ITSS.ACCOUNT.CLOSED<1>
                AA.DATA := CLOSE.DATE:','
            END ELSE
                AA.DATA := ','
            END
            ACCT.ENT.ID = ACC.ID
            TARGET.TRANS = TRN.ID
            PROCESS.DATE = TRX.PROCESS.DATE
            IF PRD.ID NE 'LD' THEN
                GOSUB BAL.AFTER.TRANS
                AA.DATA :=  AMT.AFTER.TRANS :','
            END
            ELSE
                AA.DATA :=  '0' :','
            END
            ACCT.ENT.ID = 'D'
            TARGET.TRANS = 'D'
            AA.DATA := TRX.PROCESS.DATE:','
            CALL F.READ(FN.DRMNT.INDEX,R.ACC<AC.CUSTOMER>,R.DRMNT,'',DRMNT.ERR)
            IF R.DRMNT NE '' THEN
*Line [ 868 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DATE.INDEX = DCOUNT(R.DRMNT<SCDI.CODE.DATE>,@VM)
                CODE.DATE =  R.DRMNT<SCDI.CODE.DATE,DATE.INDEX>
                DRMNT.CODE = R.DRMNT<SCDI.CODE,DATE.INDEX>
                IF R.DRMNT<SCDI.CODE,DATE.INDEX> EQ '' THEN
                    DRMNT.CODE = 'Ac'
                END
                ELSE IF R.DRMNT<SCDI.CODE,DATE.INDEX> EQ '1' THEN
                    DRMNT.CODE = 'Do'

                END

            END
            ELSE IF R.ACC THEN
                DRMNT.CODE = 'Ac'
            END
            ELSE IF NOT(R.ACC) AND CLOSE.DATE THEN
                DRMNT.CODE = 'CbC'
            END
            AA.DATA := DRMNT.CODE:','
            AA.DATA := R.ACC<AC.ACCOUNT.TITLE.1>:','
            AA.DATA := ','
            AA.DATA := ','
            AA.DATA := '0'

************ WRITE ACCOUNTS
            WRITESEQ AA.DATA TO AA ELSE
                PRINT 'ERROR WRITE FILE ACCOUNTS'
            END
        END

        GOSUB GET.CUS
        DATE.INDEX = ''
        CODE.DATE = ''
        DRMNT.CODE = ''

        RETURN

GET.CUS:
*DEBUG

        IF PRD.ID EQ 'TT' AND CUS.ID EQ '' AND AGENT.TYPE EQ 'P' THEN
*        BB.DATA = TRN.ID:IND:',,,,,,,,,,,':R.TT<TT.TE.NARRATIVE.2,2>:',,,,,,,,,,,,,,,,,,,,,,,0'
            BB.DATA = TRN.ID:IND:',,,,,,,,,,,':R.TT<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>:',,,,,,,,,,,,,,,,,,,,,,,0'
            WRITESEQ BB.DATA TO BB ELSE
                PRINT 'ERROR WRITE FILE PERSON'
            END

*        DD.DATA = TRN.ID:IND:',,,,,,,,,,,,,':R.TT<TT.TE.NARRATIVE.2,2>:',,,,,,,,,,,,,,,,,,,,,,,0'
            DD.DATA = TRN.ID:IND:',,,,,,,,,,,,,':R.TT<TT.TE.LOCAL.REF,TTLR.DPST.NSN.NO>:',,,,,,,,,,,,,,,,,,,,,,,0'
            WRITESEQ DD.DATA TO DD ELSE
                PRINT 'ERROR WRITE FILE SIGNATORY'
            END
            RETURN
        END
*    CALL F.READ(FN.CUS,R.ACC<AC.CUSTOMER>, R.CUS, F.CUS, ETEXT.CUS)
        CALL F.READ(FN.CUS,CUS.ID, R.CUS, F.CUS, ETEXT.CUS)
        CHANGE ',' TO '-' IN R.CUS
        CUS.LOCAL     = R.CUS<EB.CUS.LOCAL.REF>
*    SEC = R.CUS<EB.CUS.SECTOR>
        IF CUS.LOCAL<1,CULR.GOVERNORATE> EQ '98' OR CUS.LOCAL<1,CULR.GOVERNORATE> EQ '97' OR CUS.LOCAL<1,CULR.GOVERNORATE> EQ '' THEN
            COMP.CODE = R.CUS<EB.CUS.COMPANY.BOOK>
*Line [ 1068 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP.CODE,TWN.COUNTRY)
F.ITSS.SCB.BR.GOVERNORATE = 'F.SCB.BR.GOVERNORATE'
FN.F.ITSS.SCB.BR.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.BR.GOVERNORATE,COMP.CODE,R.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE,ERROR.SCB.BR.GOVERNORATE)
TWN.COUNTRY=R.ITSS.SCB.BR.GOVERNORATE<BRGR.BR.GOVERNORATE>
*Line [ 1075 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,CNTR)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,TWN.COUNTRY,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
CNTR=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
        END
        ELSE
            TWN.COUNTRY = CUS.LOCAL<1,CULR.GOVERNORATE>
*Line [ 1085 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,CNTR)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,TWN.COUNTRY,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
CNTR=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
        END

        CUS.GENDER = FIELD(CUS.LOCAL<1,CULR.GENDER>,'-',1)
        IF CUS.GENDER EQ 'M' THEN
            CUS.GEN = 'M'
        END
        ELSE IF CUS.GENDER EQ 'F' THEN
            CUS.GEN = 'F'
        END
        ELSE
            CUS.GEN = ''
        END
        CUS.SECTOR = R.CUS<EB.CUS.SECTOR>
        CUS.CITY = CUS.LOCAL<1,CULR.REGION>
*Line [ 1106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR("SCB.CUS.REGION":@FM:REG.DESCRIPTION,CUS.CITY,CUS.CITY.DESC)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,CUS.CITY,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
CUS.CITY.DESC=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
**    XX = '1'
**    IF  XX = '0' THEN
        IF CUS.SECTOR EQ '1000' OR CUS.SECTOR EQ '1100' OR CUS.SECTOR EQ '1200' OR CUS.SECTOR EQ '1300' OR CUS.SECTOR EQ '1400' OR CUS.SECTOR EQ '2000' THEN
            BB.DATA = TRN.ID:IND:','

            BB.DATA := CUS.GEN:','
            BB.DATA := ','
            BB.DATA := CUS.LOCAL<1,CULR.FNAME.ARABIC>:','
            BB.DATA := CUS.LOCAL<1,CULR.MNAME.ARABIC>:','
            BB.DATA := CUS.LOCAL<1,CULR.PREFIX.ARABIC>:','
            BB.DATA := CUS.LOCAL<1,CULR.LNAME.ARABIC>:','
            BB.DATA := R.CUS<EB.CUS.BIRTH.INCORP.DATE>:','
            BB.DATA := ',,,'
            BB.DATA := CUS.LOCAL<1,CULR.NSN.NO>:','
            BB.DATA := CUS.LOCAL<1,CULR.ID.ISSUE.DATE>:','
            BB.DATA := CUS.LOCAL<1,CULR.ID.EXPIRY.DATE>:','
            BB.DATA := ','
            IF CUS.LOCAL<1,CULR.ID.TYPE> EQ "3" THEN
                BB.DATA := CUS.LOCAL<1,CULR.ID.NUMBER>:','
            END
            ELSE
                BB.DATA := ','
            END
            BB.DATA := ','
            BB.DATA := R.CUS<EB.CUS.NATIONALITY>:','
            BB.DATA := ','
            BB.DATA :=R.CUS<EB.CUS.RESIDENCE>:','
            BB.DATA := CUS.LOCAL<1,CULR.TELEPHONE,1>:','
            BB.DATA := R.CUS<EB.CUS.SMS.1,1>:','
            IF R.CUS<EB.CUS.EMAIL.1> THEN
                BB.DATA := R.CUS<EB.CUS.EMAIL.1>:','
            END
            ELSE IF CUS.LOCAL<1,CULR.EMAIL.ADDRESS> THEN
                BB.DATA := CUS.LOCAL<1,CULR.EMAIL.ADDRESS,1>:','
            END
            ELSE
                BB.DATA := ','
            END

*Line [ 990 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CHANGE @SM TO ' ' IN CUS.LOCAL<1,CULR.ARABIC.ADDRESS>
            BB.DATA := CUS.LOCAL<1,CULR.ARABIC.ADDRESS>:','
            BB.DATA := CNTR:','
            BB.DATA := CUS.CITY.DESC:','
            BB.DATA :=  R.CUS< EB.CUS.RESIDENCE>:','
            BB.DATA := ','
            JOB = CUS.LOCAL<1,CULR.PROFESSION>
*Line [ 1160 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,JOB,JOB.DESC)
F.ITSS.SCB.CUS.PROFESSION = 'F.SCB.CUS.PROFESSION'
FN.F.ITSS.SCB.CUS.PROFESSION = ''
CALL OPF(F.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION)
CALL F.READ(F.ITSS.SCB.CUS.PROFESSION,JOB,R.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION,ERROR.SCB.CUS.PROFESSION)
JOB.DESC=R.ITSS.SCB.CUS.PROFESSION<SCB.PRF.DESCRIPTION>
            BB.DATA := JOB.DESC :','
            BB.DATA := R.CUS<EB.CUS.EMPLOYERS.NAME>:','
            BB.DATA := ',,,,0'

************ WRITE PERSON
            WRITESEQ BB.DATA TO BB ELSE
                PRINT 'ERROR WRITE FILE ACCOUNTS'
            END

            FOR C=1 TO DCOUNT(R.CUS<EB.CUS.RELATION.CODE>,@VM)
                IF R.CUS<EB.CUS.RELATION.CODE,C> EQ '94' OR R.CUS<EB.CUS.RELATION.CODE,C> EQ '49' THEN
                    R.REL.CUS = '' ; ETEXT.REL.CUS = '' ; REL.CUS.GEN = '' ; REL.CUS.GENDER = ''
                    REL.CUS.LOCAL = ''
                    CALL F.READ(FN.CUS,R.CUS<EB.CUS.REL.CUSTOMER,C>, R.REL.CUS, F.CUS, ETEXT.REL.CUS)
                    REL.CUS.LOCAL     = R.REL.CUS<EB.CUS.LOCAL.REF>
                    REL.CUS.GENDER = FIELD(REL.CUS.LOCAL<1,CULR.GENDER>,'-',1)
                    CUS.CITY = REL.CUS.LOCAL<1,CULR.REGION>
*Line [ 1184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR("SCB.CUS.REGION":@FM:REG.DESCRIPTION,CUS.CITY,REL.CITY.DESC)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,CUS.CITY,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
REL.CITY.DESC=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
                    IF REL.CUS.LOCAL<1,CULR.GOVERNORATE> EQ '98' OR REL.CUS.LOCAL<1,CULR.GOVERNORATE> EQ '97' OR REL.CUS.LOCAL<1,CULR.GOVERNORATE> EQ '' THEN
                        COMP.CODE = R.CUS<EB.CUS.COMPANY.BOOK>
*Line [ 1193 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP.CODE,TWN.COUNTRY)
F.ITSS.SCB.BR.GOVERNORATE = 'F.SCB.BR.GOVERNORATE'
FN.F.ITSS.SCB.BR.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.BR.GOVERNORATE,COMP.CODE,R.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE,ERROR.SCB.BR.GOVERNORATE)
TWN.COUNTRY=R.ITSS.SCB.BR.GOVERNORATE<BRGR.BR.GOVERNORATE>
*Line [ 1200 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,REL.CNTR)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,TWN.COUNTRY,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
REL.CNTR=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
                    END
                    ELSE
                        TWN.COUNTRY = REL.CUS.LOCAL<1,CULR.GOVERNORATE>
*Line [ 1210 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,REL.CNTR)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,TWN.COUNTRY,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
REL.CNTR=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
                    END
                    IF REL.CUS.GENDER EQ 'M' THEN
                        REL.CUS.GEN = 'M'
                    END
                    ELSE IF REL.CUS.GENDER EQ 'F' THEN
                        REL.CUS.GEN = 'F'
                    END
                    ELSE
                        REL.CUS.GEN = ''
                    END
                    IF R.CUS<EB.CUS.REL.CUSTOMER,C> THEN
                        DD.DATA = TRN.ID:IND:','
                        DD.DATA := ',,'
                        DD.DATA := REL.CUS.GEN:','
                        DD.DATA := ','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.FNAME.ARABIC>:','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.MNAME.ARABIC>:','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.PREFIX.ARABIC>:','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.LNAME.ARABIC>:','
                        DD.DATA := R.REL.CUS<EB.CUS.BIRTH.INCORP.DATE>:','
                        DD.DATA := ',,,'
                        DD.DATA := REL.CUS.LOCAL<1,CULR.NSN.NO>:','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.ID.ISSUE.DATE>:','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.ID.EXPIRY.DATE>:','
                        DD.DATA := ','
                        IF REL.CUS.LOCAL<1,CULR.ID.TYPE> EQ "3" THEN
                            DD.DATA := REL.CUS.LOCAL<1,CULR.ID.NUMBER>:','
                        END
                        ELSE
                            DD.DATA := ','
                        END
                        DD.DATA := ','
                        DD.DATA := R.REL.CUS<EB.CUS.NATIONALITY>:','
                        DD.DATA := ','
                        DD.DATA :=R.REL.CUS<EB.CUS.RESIDENCE>:','
                        DD.DATA := REL.CUS.LOCAL<1,CULR.TELEPHONE,1>:','
                        DD.DATA := R.REL.CUS<EB.CUS.SMS.1,1>:','
                        IF R.REL.CUS<EB.CUS.EMAIL.1> THEN
                            DD.DATA := R.REL.CUS<EB.CUS.EMAIL.1>:','
                        END
                        ELSE IF REL.CUS.LOCAL<1,CULR.EMAIL.ADDRESS> THEN
                            DD.DATA := REL.CUS.LOCAL<1,CULR.EMAIL.ADDRESS,1>:','
                        END
                        ELSE
                            DD.DATA := ','
                        END
*Line [ 1071 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        CHANGE @SM TO ' ' IN REL.CUS.LOCAL<1,CULR.ARABIC.ADDRESS>
                        DD.DATA := REL.CUS.LOCAL<1,CULR.ARABIC.ADDRESS>:','
                        DD.DATA := REL.CNTR:','
                        DD.DATA := REL.CITY.DESC:','
                        DD.DATA :=  R.REL.CUS< EB.CUS.RESIDENCE>:','
                        DD.DATA := ','
                        JOB = REL.CUS.LOCAL<1,CULR.PROFESSION>
*Line [ 1271 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,JOB,REL.JOB.DESC)
F.ITSS.SCB.CUS.PROFESSION = 'F.SCB.CUS.PROFESSION'
FN.F.ITSS.SCB.CUS.PROFESSION = ''
CALL OPF(F.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION)
CALL F.READ(F.ITSS.SCB.CUS.PROFESSION,JOB,R.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION,ERROR.SCB.CUS.PROFESSION)
REL.JOB.DESC=R.ITSS.SCB.CUS.PROFESSION<SCB.PRF.DESCRIPTION>
                        DD.DATA := REL.JOB.DESC :','
                        DD.DATA := R.REL.CUS<EB.CUS.EMPLOYERS.NAME>:','
                        DD.DATA := ',,,,0'
                        WRITESEQ DD.DATA TO DD ELSE
                            PRINT 'ERROR WRITE FILE ENTITY'
                        END
                    END

                END
            NEXT


        END ELSE

            CC.DATA = TRN.ID:IND:','
            CC.DATA := R.CUS<EB.CUS.SHORT.NAME,1>:','
            CC.DATA := R.CUS<EB.CUS.NAME.1,1>:','
            CC.DATA := R.CUS<EB.CUS.SECTOR>:','
            CC.DATA := CUS.LOCAL<1,CULR.COM.REG.NO>:','
            INDS = R.CUS<EB.CUS.INDUSTRY>
*Line [ 1298 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('INDUSTRY':@FM:EB.IND.DESCRIPTION:@FM:'F',INDS,INDS.DESC)
F.ITSS.INDUSTRY = 'F.INDUSTRY'
FN.F.ITSS.INDUSTRY = ''
CALL OPF(F.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY)
CALL F.READ(F.ITSS.INDUSTRY,INDS,R.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY,ERROR.INDUSTRY)
INDS.DESC=R.ITSS.INDUSTRY<EB.IND.DESCRIPTION:@FM:'F'>
            CC.DATA := INDS.DESC:','
            CC.DATA := CUS.LOCAL<1,CULR.TELEPHONE,1>:','
            CC.DATA := CUS.LOCAL<1,CULR.ARABIC.ADDRESS>:','
            CC.DATA := CNTR:','
            CC.DATA := CUS.CITY.DESC:','
            CC.DATA := ','
            CC.DATA :=  R.CUS< EB.CUS.RESIDENCE>:','
            IF R.CUS<EB.CUS.EMAIL.1> THEN
                CC.DATA := R.CUS<EB.CUS.EMAIL.1>:','
            END
            ELSE IF CUS.LOCAL<1,CULR.EMAIL.ADDRESS> THEN
                CC.DATA := CUS.LOCAL<1,CULR.EMAIL.ADDRESS,1>:','

            END ELSE
                CC.DATA := ','
            END
*        CC.DATA := R.CUS<EB.CUS.EMAIL.1>:','
            CC.DATA := ','
            CC.DATA := R.CUS<EB.CUS.BIRTH.INCORP.DATE>:','
            CC.DATA := ',,'
            CC.DATA := CUS.LOCAL<1,CULR.TAX.NO>
            CC.DATA := ',0'



************ WRITE PERSON
            WRITESEQ CC.DATA TO CC ELSE
                PRINT 'ERROR WRITE FILE ENTITY'
            END

            FOR C=1 TO DCOUNT(R.CUS<EB.CUS.RELATION.CODE>,@VM)
                IF R.CUS<EB.CUS.RELATION.CODE,C> EQ '94' OR R.CUS<EB.CUS.RELATION.CODE,C> EQ '49' THEN
                    R.REL.CUS = '' ; ETEXT.REL.CUS = '' ; REL.CUS.GEN = '' ; REL.CUS.GENDER = ''
                    REL.CUS.LOCAL = ''
                    CALL F.READ(FN.CUS,R.CUS<EB.CUS.REL.CUSTOMER,C>, R.REL.CUS, F.CUS, ETEXT.REL.CUS)
                    REL.CUS.LOCAL     = R.REL.CUS<EB.CUS.LOCAL.REF>
                    REL.CUS.GENDER = FIELD(REL.CUS.LOCAL<1,CULR.GENDER>,'-',1)
                    CUS.CITY = REL.CUS.LOCAL<1,CULR.REGION>
*Line [ 1343 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR("SCB.CUS.REGION":@FM:REG.DESCRIPTION,CUS.CITY,REL.CITY.DESC)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,CUS.CITY,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
REL.CITY.DESC=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
                    IF REL.CUS.LOCAL<1,CULR.GOVERNORATE> EQ '98' OR REL.CUS.LOCAL<1,CULR.GOVERNORATE> EQ '97' OR REL.CUS.LOCAL<1,CULR.GOVERNORATE> EQ '' THEN
                        COMP.CODE = R.CUS<EB.CUS.COMPANY.BOOK>
*Line [ 1352 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP.CODE,TWN.COUNTRY)
F.ITSS.SCB.BR.GOVERNORATE = 'F.SCB.BR.GOVERNORATE'
FN.F.ITSS.SCB.BR.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.BR.GOVERNORATE,COMP.CODE,R.ITSS.SCB.BR.GOVERNORATE,FN.F.ITSS.SCB.BR.GOVERNORATE,ERROR.SCB.BR.GOVERNORATE)
TWN.COUNTRY=R.ITSS.SCB.BR.GOVERNORATE<BRGR.BR.GOVERNORATE>
*Line [ 1359 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,REL.CNTR)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,TWN.COUNTRY,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
REL.CNTR=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
                    END
                    ELSE
                        TWN.COUNTRY = REL.CUS.LOCAL<1,CULR.GOVERNORATE>
*Line [ 1369 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,REL.CNTR)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,TWN.COUNTRY,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
REL.CNTR=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
                    END
                    IF REL.CUS.GENDER EQ 'M' THEN
                        REL.CUS.GEN = 'M'
                    END
                    ELSE IF REL.CUS.GENDER EQ 'F' THEN
                        REL.CUS.GEN = 'F'
                    END
                    ELSE
                        REL.CUS.GEN = ''
                    END
                    IF R.CUS<EB.CUS.REL.CUSTOMER,C> THEN
                        QQ.DATA = TRN.ID:IND:','

                        QQ.DATA := REL.CUS.GEN:','
                        QQ.DATA := ','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.FNAME.ARABIC>:','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.MNAME.ARABIC>:','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.PREFIX.ARABIC>:','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.LNAME.ARABIC>:','
                        QQ.DATA := R.REL.CUS<EB.CUS.BIRTH.INCORP.DATE>:','
                        QQ.DATA := ',,,'
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.NSN.NO>:','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.ID.ISSUE.DATE>:','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.ID.EXPIRY.DATE>:','
                        QQ.DATA := ','
                        IF REL.CUS.LOCAL<1,CULR.ID.TYPE> EQ "3" THEN
                            QQ.DATA := REL.CUS.LOCAL<1,CULR.ID.NUMBER>:','
                        END
                        ELSE
                            QQ.DATA := ','
                        END
                        QQ.DATA := ','
                        QQ.DATA := R.REL.CUS<EB.CUS.NATIONALITY>:','
                        QQ.DATA := ','
                        QQ.DATA :=R.REL.CUS<EB.CUS.RESIDENCE>:','
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.TELEPHONE,1>:','
                        QQ.DATA := R.REL.CUS<EB.CUS.SMS.1,1>:','
                        IF R.REL.CUS<EB.CUS.EMAIL.1> THEN
                            QQ.DATA := R.REL.CUS<EB.CUS.EMAIL.1>:','
                        END
                        ELSE IF REL.CUS.LOCAL<1,CULR.EMAIL.ADDRESS> THEN
                            QQ.DATA := REL.CUS.LOCAL<1,CULR.EMAIL.ADDRESS,1>:','
                        END
                        ELSE
                            QQ.DATA := ','
                        END

*Line [ 1195 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        CHANGE @SM TO ' ' IN REL.CUS.LOCAL<1,CULR.ARABIC.ADDRESS>
                        QQ.DATA := REL.CUS.LOCAL<1,CULR.ARABIC.ADDRESS>:','
                        QQ.DATA := REL.CNTR:','
                        QQ.DATA := REL.CITY.DESC:','
                        QQ.DATA :=  R.REL.CUS< EB.CUS.RESIDENCE>:','
                        QQ.DATA := ','
                        JOB = REL.CUS.LOCAL<1,CULR.PROFESSION>
*Line [ 1431 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                        CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,JOB,REL.JOB.DESC)
F.ITSS.SCB.CUS.PROFESSION = 'F.SCB.CUS.PROFESSION'
FN.F.ITSS.SCB.CUS.PROFESSION = ''
CALL OPF(F.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION)
CALL F.READ(F.ITSS.SCB.CUS.PROFESSION,JOB,R.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION,ERROR.SCB.CUS.PROFESSION)
REL.JOB.DESC=R.ITSS.SCB.CUS.PROFESSION<SCB.PRF.DESCRIPTION>
                        QQ.DATA := REL.JOB.DESC :','
                        QQ.DATA := R.REL.CUS<EB.CUS.EMPLOYERS.NAME>:','
                        QQ.DATA := ',,,,,0'
                        WRITESEQ QQ.DATA TO QQ ELSE
                            PRINT 'ERROR WRITE FILE ACCOUNTS'
                        END
                    END

                END
            NEXT

        END
**    END

        DD.DATA = TRN.ID:IND:','
        DD.DATA := ',,'
        DD.DATA := CUS.GEN:','
        DD.DATA := ','
        DD.DATA := CUS.LOCAL<1,CULR.FNAME.ARABIC>:','
        DD.DATA := CUS.LOCAL<1,CULR.MNAME.ARABIC>:','
        DD.DATA := CUS.LOCAL<1,CULR.PREFIX.ARABIC>:','
        DD.DATA := CUS.LOCAL<1,CULR.LNAME.ARABIC>:','
        DD.DATA := R.CUS<EB.CUS.BIRTH.INCORP.DATE>:','
        DD.DATA := ',,,'
        DD.DATA := CUS.LOCAL<1,CULR.NSN.NO>:','
        DD.DATA := CUS.LOCAL<1,CULR.ID.ISSUE.DATE>:','
        DD.DATA := CUS.LOCAL<1,CULR.ID.EXPIRY.DATE>:','
        DD.DATA := ','
        IF CUS.LOCAL<1,CULR.ID.TYPE> EQ "3" THEN
            DD.DATA := CUS.LOCAL<1,CULR.ID.NUMBER>:','
        END
        ELSE
            DD.DATA := ','
        END
        DD.DATA := ','
        DD.DATA := R.CUS<EB.CUS.NATIONALITY>:','
        DD.DATA := ','
        DD.DATA :=R.CUS<EB.CUS.RESIDENCE>:','
        DD.DATA := CUS.LOCAL<1,CULR.TELEPHONE,1>:','
        DD.DATA := R.CUS<EB.CUS.SMS.1,1>:','
        IF R.CUS<EB.CUS.EMAIL.1> THEN
            DD.DATA := R.CUS<EB.CUS.EMAIL.1>:','
        END
        ELSE IF CUS.LOCAL<1,CULR.EMAIL.ADDRESS> THEN
            DD.DATA := CUS.LOCAL<1,CULR.EMAIL.ADDRESS,1>:','
        END
        ELSE
            DD.DATA := ','
        END
*    DD.DATA := R.CUS<EB.CUS.EMAIL.1>:','
*Line [ 1254 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        CHANGE @SM TO ' ' IN CUS.LOCAL<1,CULR.ARABIC.ADDRESS>
        DD.DATA := CUS.LOCAL<1,CULR.ARABIC.ADDRESS>:','
        DD.DATA := CNTR:','
        DD.DATA := CUS.CITY.DESC:','
        DD.DATA :=  R.CUS< EB.CUS.RESIDENCE>:','
        DD.DATA := ','
        JOB = CUS.LOCAL<1,CULR.PROFESSION>
*Line [ 1496 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,JOB,JOB.DESC)
F.ITSS.SCB.CUS.PROFESSION = 'F.SCB.CUS.PROFESSION'
FN.F.ITSS.SCB.CUS.PROFESSION = ''
CALL OPF(F.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION)
CALL F.READ(F.ITSS.SCB.CUS.PROFESSION,JOB,R.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION,ERROR.SCB.CUS.PROFESSION)
JOB.DESC=R.ITSS.SCB.CUS.PROFESSION<SCB.PRF.DESCRIPTION>
        DD.DATA := JOB.DESC :','
        DD.DATA := R.CUS<EB.CUS.EMPLOYERS.NAME>:','
        DD.DATA := ',,,,0'


************ WRITE PERSON
        WRITESEQ DD.DATA TO DD ELSE
            PRINT 'ERROR WRITE FILE ENTITY'
        END
        CUS.LOCAL =''
        CUS.GEN = ''
        CUS.CITY.DESC = ''
        CNTR = ''
        RETURN

BAL.AFTER.TRANS:
*DEBUG
        AMT.AFTER.TRANS = 0
        OPEN.ACT.BAL = 0
        STMT.ENT.ID = ''

        CALL EB.ACCT.ENTRY.LIST(ACCT.ENT.ID,PROCESS.DATE,PROCESS.DATE,ID.LIST,OPEN.ACT.BAL,ER)

        IF NOT(ER) THEN AMT.AFTER.TRANS = OPEN.ACT.BAL
        LOOP
            REMOVE STMT.ENT.ID FROM ID.LIST SETTING HOLD.POS
        WHILE STMT.ENT.ID:HOLD.POS
*DEBUG
            CALL F.READ(FN.STMT.ENTRY,STMT.ENT.ID,R.STMT.ENTRY,FV.STMT.ENTRY,ERR1)
            ACCT.NUM = R.STMT.ENTRY<AC.STE.ACCOUNT.NUMBER>
            AMT.FCY =  R.STMT.ENTRY<AC.STE.AMOUNT.FCY>
            AMT.LCY =  R.STMT.ENTRY<AC.STE.AMOUNT.LCY>
            TRANS.REF =  R.STMT.ENTRY<AC.STE.TRANS.REFERENCE>
            CURR = R.STMT.ENTRY<AC.STE.CURRENCY>


            IF CURR EQ 'EGP' THEN
                AMT.AFTER.TRANS += AMT.LCY
            END ELSE
                AMT.AFTER.TRANS += AMT.FCY
            END


            IF TRANS.REF[1,12] EQ TARGET.TRANS THEN
                BREAK
            END

        REPEAT

        RETURN



    END
