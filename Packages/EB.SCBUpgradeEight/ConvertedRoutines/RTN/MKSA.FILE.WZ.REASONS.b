* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-80</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MKSA.FILE.WZ.REASONS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY
**---------------------------------------------------------------------**
    OPENSEQ "FCY.CLEARING", "WZ.R.REASON": TO DD THEN
        CLOSESEQ DD
        HUSH ON
        EXECUTE 'DELETE ':"FCY.CLEARING":' ':"WZ.R.REASON"
        HUSH OFF
    END
    OPENSEQ "FCY.CLEARING", "WZ.R.REASON" TO DD ELSE
        CREATE DD THEN
            PRINT 'FILE CREATED IN FCY.CLEARING'
        END
        ELSE
            STOP 'Cannot create File IN FCY.CLEARING'
        END
    END
**---------------------------------------------------------------------**
    EOF = ''

*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    MAIN.MULTI = DCOUNT(R.NEW(SCB.BR.BR.CODE),@VM)
    FOR I = 1 TO MAIN.MULTI
*Line [ 68 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        SUB.VAL = DCOUNT(R.NEW(SCB.BR.OUR.REFERENCE)<1,I>,@SM)
        FOR X = 1 TO SUB.VAL
            BR.CODE       = R.NEW(SCB.BR.BR.CODE)<1,I>
            OUR.REF       = R.NEW(SCB.BR.OUR.REFERENCE)<1,I,X>
            BOOK.DATE     = R.NEW(SCB.BR.BOOK.DATE)
            CURR          = R.NEW(SCB.BR.CURR)<1,I,X>
            AMOUNT        = R.NEW(SCB.BR.AMOUNT)<1,I,X>
            BANK          = R.NEW(SCB.BR.BANK)<1,I,X>
            RETURN.REASON = R.NEW(SCB.BR.RETURN.REASON)<1,I,X>
            DD.DATA = ""
            GOSUB WRITE.ROW
        NEXT X 
    NEXT I

    CALL !HUSHIT(0)
    COMAND2 = " chmod -R 777 FCY.CLEARING/FCY.CLEARING.WZ.R.REASON"
    EXECUTE COMAND2

    RETURN
**-------------------------------------------------------------------**
WRITE.ROW:
*---------
    DD.DATA := BR.CODE:"|"
    DD.DATA := OUR.REF:"|"
    DD.DATA := BOOK.DATE:"|"
    DD.DATA := CURR:"|"
    DD.DATA := AMOUNT:"|"
    DD.DATA := BANK:"|"
    DD.DATA := RETURN.REASON

    WRITESEQ DD.DATA TO DD ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
END
