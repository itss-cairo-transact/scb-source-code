* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE ID.AC.CL.LIVE(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*    $INCLUDE GLOBUS.BP I_F.ACCOUNT.CLOSED
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
*    DEBUG
    COMP = ID.COMPANY
    FN.AC.CL = 'FBNK.ACCOUNT.CLOSED' ; F.AC.CL = '' ; R.AC.CL = ''
    CALL OPF(FN.AC.CL,F.AC.CL)
    X= 0
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2="" ; E1 = ''

    T.SEL = "SELECT ":FN.AC.CL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
*&            T.SEL2 = "SELECT ":FN.AC:" WITH @ID EQ ":KEY.LIST<I>
*&            CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*=================================================================
*&            IF SELECTED2   THEN
            AC.COM = R.AC<AC.CO.CODE>
            IF AC.COM EQ COMP THEN
                IF R.AC NE '' THEN
                    X= X + 1
                    ENQ<2,X> = '@ID'
                    ENQ<3,X> = 'EQ'
                    ENQ<4,X> = KEY.LIST<I>
                END ELSE
                    X= X + 1
                    ENQ<2,X> = '@ID'
                    ENQ<3,X> = 'EQ'
                    ENQ<4,X> = 'DUMMY'
                END
            END ELSE
                X= X + 1
                ENQ<2,X> = '@ID'
                ENQ<3,X> = 'EQ'
                ENQ<4,X> = 'DUMMY'
            END
        NEXT I
    END
    RETURN
END
