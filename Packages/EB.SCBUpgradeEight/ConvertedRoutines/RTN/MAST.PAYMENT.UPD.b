* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1197</Rating>
*-----------------------------------------------------------------------------
***NESSREEN AHMED 01/12/2013*********

    SUBROUTINE MAST.PAYMENT.UPD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.SETT
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.PAYMENT
******************************************************************
**********************FILLING DATA********************************
******************************************************************
    FN.DP = 'F.SCB.MAST.DAILY.PAYMENT' ; F.DP = '' ; R.DP = '' ; RETRY.DP = '' ; E.DP = ''
    CALL OPF(FN.DP,F.DP)

    FN.CI = 'F.CARD.ISSUE' ; F.CI = '' ; R.CI = ''

    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE EQ 38 "
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CARD.NO = '' ; EMB.NAME = '' ;  CURR = '' ; AMT = '' ; PAY.DATE = '' ; REF = '' ; TT.CO.CODE = '' ; TT.A.DATE = ''
            TT.TR.TYPE = ''  ; VI.CO = '' ; CI.ID = '' ; VISA.CO.CODE = ''
            TT.TR.TYPE = ''
            FN.TELLER = 'F.TELLER' ; F.TELELR = '' ; R.TELLER = '' ; RETRY1 = '' ; E1 = ''
            KEY.TO.USE = KEY.LIST<I>
            CALL OPF(FN.TELLER,F.TELLER)
            CALL F.READ(FN.TELLER,  KEY.TO.USE, R.TELLER, F.TELLER, E1)

            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,TTLR.VISA.NUMBER>
            CALL OPF(FN.CI,F.CI)
            IF VISA.NO[1,8] = '52481500' THEN
                VI.CO = 'MACL'
            END
            IF VISA.NO[1,8] = '52571500' THEN
                VI.CO = 'MAGO'
            END
            CI.ID = VI.CO:'.':VISA.NO
            CALL F.READ(FN.CI, CI.ID, R.CI, F.CI, E1.CI)
            VISA.CO.CODE = R.CI<CARD.IS.CO.CODE>
            ACCT = R.TELLER<TT.TE.ACCOUNT.1>
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACCT , CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
            FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)

            EMB.NA = R.CUSTOMER<EB.CUS.SHORT.NAME>
            REFEREN = 'CREDIT PAYMENT'
            CURR = R.TELLER<TT.TE.CURRENCY.1>
            AMTT = R.TELLER<TT.TE.NET.AMOUNT>
            TR.DATE = R.TELLER<TT.TE.DATE.TIME>
            TT.CO.CODE = R.TELLER<TT.TE.CO.CODE>
            TT.A.DATE = R.TELLER<TT.TE.AUTH.DATE>
            TT.TR.TYPE = R.TELLER<TT.TE.TRANSACTION.CODE>
            PAY.DATE = TODAY

            CALL F.READ(FN.DP,  KEY.TO.USE, R.DP, F.DP, E.DP)

            R.DP<MST.PYM.REFER.NO> = KEY.TO.USE
            R.DP<MST.PYM.CARD.BR> = VISA.CO.CODE
            R.DP<MST.PYM.CARD.NO> = VISA.NO
            R.DP<MST.PYM.CUST.ACCT> = ACCT
            R.DP<MST.PYM.CURR> =  CURR
            R.DP<MST.PYM.AMOUNT> = AMTT
            R.DP<MST.PYM.TRANS.TYPE> = TT.TR.TYPE
            R.DP<MST.PYM.TRANS.DATE> = TT.A.DATE
            R.DP<MST.PYM.TR.DESC> = REFEREN

            CALL F.WRITE(FN.DP,KEY.TO.USE, R.DP)
           ** CALL JOURNAL.UPDATE(KEY.TO.USE)


        NEXT I
    END
***********************************************************************************************
***************FT SELECTION********************************************************************
    IDDD = "EG0010001"
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,IDDD,LAST.W.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LAST.W.DAY=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVM' AND PROCESSING.DATE EQ ": LAST.W.DAY
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    IF SELECTED.N THEN
        FOR X=1 TO SELECTED.N
            FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
            FT.COM.CODE = '' ;  FT.TR.TYPE = ''   ; FT.CUST.ACCT = ''   ; CURR.FT = '' ;  REFEREN = ''
            LOCAL.REF.FT = ''   ; VI.CO = '' ; CI.ID = ''  ; VISA.CO.CODE.FT = ''

            FT.ID = KEY.LIST.N<X>

            CALL OPF(FN.FT,F.FT)
            CALL F.READ(FN.FT, FT.ID, R.FT, F.FT, E2)

            LOCAL.REF.FT = R.FT<FT.LOCAL.REF>
            VISA.NO.FT = LOCAL.REF.FT<1,FTLR.VISA.NO>
*********UPDATED BY NESSREEN 18/5/2010**********************************
            CALL OPF(FN.CI,F.CI)
            IF VISA.NO.FT[1,8] = '52481500' THEN
                VI.CO = 'MACL'
            END
            IF VISA.NO.FT[1,8] = '52571500' THEN
                VI.CO = 'MAGO'
            END
            CI.ID = VI.CO:'.':VISA.NO.FT
            CALL F.READ(FN.CI, CI.ID, R.CI, F.CI, E1.CI)
            VISA.CO.CODE.FT = R.CI<CARD.IS.CO.CODE>
*******************************18/5/2010************************************
            CUST.FT = R.FT<FT.CREDIT.CUSTOMER>

            FN.CUSTOMER.FT = 'F.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
            CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
            CALL F.READ(FN.CUSTOMER.FT, CUST.FT, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)

            EMB.NA.FT = R.CUSTOMER.FT<EB.CUS.SHORT.NAME>
            REFEREN = 'CREDIT PAYMENT'
            REF.FT = REFEREN:STR(' ',30 - LEN(REFEREN))
            CURR.FT = R.FT<FT.CREDIT.CURRENCY>
            AMTT.FT = R.FT<FT.DEBIT.AMOUNT>
            PAY.DATE.FT = R.FT<FT.AUTH.DATE>
            FT.COM.CODE = R.FT<FT.CO.CODE>
            FT.CUST.ACCT = R.FT<FT.CREDIT.ACCT.NO>
            FT.TR.TYPE  = R.FT<FT.TRANSACTION.TYPE>

            CALL F.READ(FN.DP,  FT.ID, R.DP, F.DP, E.DP)

            R.DP<MST.PYM.REFER.NO> = FT.ID
            R.DP<MST.PYM.CARD.BR> = VISA.CO.CODE.FT
            R.DP<MST.PYM.CARD.NO> = VISA.NO.FT
            R.DP<MST.PYM.CUST.ACCT> = FT.CUST.ACCT
            R.DP<MST.PYM.CURR> = CURR.FT
            R.DP<MST.PYM.AMOUNT> = AMTT.FT
            R.DP<MST.PYM.TRANS.TYPE> = FT.TR.TYPE
            R.DP<MST.PYM.TRANS.DATE> = PAY.DATE.FT
            R.DP<MST.PYM.TR.DESC> = REFEREN

            CALL F.WRITE(FN.DP,FT.ID, R.DP)
           ** CALL JOURNAL.UPDATE(FT.ID)

        NEXT X
    END
***********************************************************************************************
***************DEBIT PAYMENT FOR SETTLEMENT****************************************************
***********************************************************************************************
    S.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACMS' AND PROCESSING.DATE EQ ": LAST.W.DAY
    KEY.LIST.S=""
    SELECTED.S=""
    ER.MSG.S=""

    CALL EB.READLIST(S.SEL,KEY.LIST.S,"",SELECTED.S,ER.MSG.S)
    IF SELECTED.S THEN
        FOR SS=1 TO SELECTED.S
            VISA.NO.FT.S = ''   ; VI.CO = '' ;  CI.ID = ''  ; VISA.CO.CODE.FT.S = ''
            FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
            FT.ID.S = KEY.LIST.S<SS>
            CALL OPF(FN.FT,F.FT)
            CALL F.READ(FN.FT,  FT.ID.S, R.FT, F.FT, E2)

            LOCAL.REF.FT.S = R.FT<FT.LOCAL.REF>
            VISA.NO.FT.S = LOCAL.REF.FT.S<1,FTLR.VISA.NO>
            CALL OPF(FN.CI,F.CI)
            IF VISA.NO.FT.S[1,8] = '52481500' THEN
                VI.CO = 'MACL'
            END
            IF VISA.NO.FT.S[1,8] = '52571500' THEN
                VI.CO = 'MAGO'
            END
            CI.ID = VI.CO:'.':VISA.NO.FT.S
            CALL F.READ(FN.CI, CI.ID, R.CI, F.CI, E1.CI)
            VISA.CO.CODE.FT.S = R.CI<CARD.IS.CO.CODE>
            CUST.FT.S = R.FT<FT.CREDIT.CUSTOMER>

            FN.CUSTOMER.FT = 'F.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
            CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
            CALL F.READ(FN.CUSTOMER.FT, CUST.FT.S, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)

            EMB.NA.FT.S = R.CUSTOMER.FT<EB.CUS.SHORT.NAME>
            REFEREN.S = 'DEBIT PAYMENT'
            CURR.FT.S = R.FT<FT.CREDIT.CURRENCY>
            AMTT.FT.S = R.FT<FT.DEBIT.AMOUNT>
            FT.CO.CODE.S = R.FT<FT.CO.CODE>
            PAY.DATE.FT.S = R.FT<FT.AUTH.DATE>
            FT.CUST.ACCT.S = R.FT<FT.CREDIT.ACCT.NO>
            FT.TR.TYPE.S  = R.FT<FT.TRANSACTION.TYPE>

            CALL F.READ(FN.DP, FT.ID.S , R.DP, F.DP, E.DP)

            R.DP<MST.PYM.REFER.NO> = FT.ID.S
            R.DP<MST.PYM.CARD.BR> = VISA.CO.CODE.FT.S
            R.DP<MST.PYM.CARD.NO> = VISA.NO.FT.S
            R.DP<MST.PYM.CUST.ACCT> = FT.CUST.ACCT.S
            R.DP<MST.PYM.CURR> = CURR.FT.S
            R.DP<MST.PYM.AMOUNT> = AMTT.FT.S
            R.DP<MST.PYM.TRANS.TYPE> = FT.TR.TYPE.S
            R.DP<MST.PYM.TRANS.DATE> = PAY.DATE.FT.S
            R.DP<MST.PYM.TR.DESC> = REFEREN.S

            CALL F.WRITE(FN.DP,FT.ID.S, R.DP)
          **  CALL JOURNAL.UPDATE(FT.ID.S)

        NEXT SS
    END
***********************************************************************************************
***********************************************************************************************
***************CREDIT PAYMENT OR DEBIT PAYMENT FOR SETTLEMENT***************************************************
    SET.SEL = "SELECT F.SCB.MAST.SETT WITH TRANS.DATE EQ ": LAST.W.DAY

    KEY.LIST.SET=""
    SELECTED.SET=""
    ER.MSG.SET=""

    CALL EB.READLIST(SET.SEL,KEY.LIST.SET,"",SELECTED.SET,ER.MSG.SET)
    IF SELECTED.SET THEN
        FOR ST=1 TO SELECTED.SET
            VISA.NO.SET = '' ; VI.CO = '' ; CI.ID = '' ;  VISA.CO.CODE.SET = ''
            FN.SET = 'F.SCB.MAST.SETT' ; F.SET = '' ; R.SET = '' ; RETRY3= '' ; E3 = ''
            SET.ID = KEY.LIST.SET<ST>
            CALL OPF(FN.SET,F.SET)
            CALL F.READ(FN.SET,  SET.ID, R.SET, F.SET, E3)
            VISA.NO.SET = R.SET<MSET.CARD.NO>
            CALL OPF(FN.CI,F.CI)
            IF VISA.NO.SET[1,8] = '52481500' THEN
                VI.CO = 'MACL'
            END
            IF VISA.NO.SET[1,8] = '52571500' THEN
                VI.CO = 'MAGO'
            END
            CI.ID = VI.CO:'.':VISA.NO.SET
            CALL F.READ(FN.CI, CI.ID, R.CI, F.CI, E1.CI)
            VISA.CO.CODE.SET = R.CI<CARD.IS.CO.CODE>
            CUST.FT.SET = R.SET<MSET.CUST.NO>
            FLAG.1 = R.SET<MSET.TRN.INDICATOR>
            FN.CUSTOMER.SET = 'F.CUSTOMER' ; F.CUSTOMER.SET = '' ; R.CUSTOMER.SET = '' ; RETRY.SET = '' ; E.SET = ''
            CALL OPF(FN.CUSTOMER.SET,F.CUSTOMER.SET)
            CALL F.READ(FN.CUSTOMER.SET, CUST.FT.SET, R.CUSTOMER.SET, F.CUSTOMER.SET, E.SET)

            EMB.NA.SET = R.CUSTOMER.SET<EB.CUS.SHORT.NAME>
            EMB.NAME.SET = STR(' ',30)

            IF FLAG.1 = 'C' THEN
                REFEREN.SET = 'CREDIT PAYMENT'
            END
            IF FLAG.1 = 'D' THEN
                REFEREN.SET = 'DEBIT PAYMENT'
            END

            REF.SET = REFEREN.SET:STR(' ',30 - LEN(REFEREN.SET))
            AMTT.SET = R.SET<MSET.TRN.AMT>
            PAY.DATE.SET = R.SET<MSET.TRANS.DATE>
            CO.CODE.SET = R.SET<MSET.CO.CODE>
            CALL F.READ(FN.DP,  SET.ID, R.DP, F.DP, E.DP)

            R.DP<MST.PYM.REFER.NO> = SET.ID
            R.DP<MST.PYM.CARD.BR> = VISA.CO.CODE.SET
            R.DP<MST.PYM.CARD.NO> = VISA.NO.SET
            R.DP<MST.PYM.CURR> = 'EGP'
            R.DP<MST.PYM.AMOUNT> =  AMTT.SET
            R.DP<MST.PYM.TRANS.TYPE> = 'SETT'
            R.DP<MST.PYM.TRANS.DATE> = PAY.DATE.SET
            R.DP<MST.PYM.TR.DESC> = REFEREN.SET

            CALL F.WRITE(FN.DP,SET.ID, R.DP)
          **  CALL JOURNAL.UPDATE(SET.ID)
        NEXT ST

    END

*************************************************************
    RETURN

END
