* @ValidationCode : MjotMjAwMDk1OTk3OkNwMTI1MjoxNjQwNzA3NjUyNzg5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:07:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE GET.NAME.3(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

*TEXT = ARG; CALL REM

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    CALL OPF(FN.AC,F.AC)

    IF ARG EQ '0230534010100605' OR ARG EQ '0330117110100103' THEN
        TEXT = ARG ; CALL REM
        CALL F.READ(FN.AC,ARG,R.AC,F.AC,E1)
        NAME = R.AC<AC.SHORT.TITLE>
        ARG  = NAME
    END
    IF (ARG[1,8] EQ '02305340' OR ARG[1,8] EQ '03301171') AND ( ARG NE '0230534010100605' ARG NE '0330117110100103' ) THEN
        CUST.ID = ARG[2,7]
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        NAME = LOCAL<1,CULR.ARABIC.NAME.2>
        ARG  = NAME
    END
RETURN
END
