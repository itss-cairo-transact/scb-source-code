* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1552</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  LC.COLLECT.COM
*    PROGRAM LC.COLLECT.COM

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

***----------------------------------------------------      ***
    YF.CATEG.MONTH = "F.CATEG.MONTH";F.CATEG.MONTH = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)

    FN.CAT.ENT = "F.CATEG.ENTRY";F.CAT.ENT  = "" ;R.CAT.ENT  = ""
    CALL OPF(FN.CAT.ENT,F.CAT.ENT)

    YR.CATEGORY = "F.CATEGORY" ;F.CATEGORY = "" ;R.CATEGORY = ""
    CALL OPF(YR.CATEGORY,F.CATEGORY)

    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"; F.CATEG.ENT.TODAY = ""
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)
******************
    FN.CAT  = 'FBNK.CATEG.ENT.TODAY' ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
******************
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD';F.CATEG.ENT.FWD = ''
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    FN.TF='F.LETTER.OF.CREDIT'; F.TF=''
    CALL OPF(FN.TF,F.TF)

    FN.FT.HIS='F.FUNDS.TRANSFER$HIS'; F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    FN.FT='F.FUNDS.TRANSFER'; F.FT=''
    CALL OPF(FN.FT,F.FT)

    FN.IN = 'F.INF.MULTI.TXN'; F.IN  = ''
    CALL OPF(FN.IN,F.IN)
** ------------------------------------------------    **
    FROM.DATE  = FIELD(O.DATA,'*',1)[1,8]
    TO.DATE    = FIELD(O.DATA,'*',1)[10,8]

    CUS.NO     = FIELD(O.DATA,'*',2)
    TF.ID      = FIELD(O.DATA,'*',3)

    CALL F.READ(FN.TF,TF.ID,R.TF,F.TF,ETEXT)
    TF.ID.OLD = R.TF<TF.LC.OLD.LC.NUMBER>

    FCY.AMT    = 0
    LCY.AMT    = 0
    PL.NUM     = 12
    XX         = 1
    YY         = 5

    PL.CAT.ALL = '523005230152302523035230452305520305203152032520335204023004'
    FOR I = 1 TO PL.NUM
        PL.CAT     = PL.CAT.ALL[XX,YY]
        Y.CAT.NO   = PL.CAT
************************************************************
        T.SEL  = "SELECT FBNK.CATEG.ENT.TODAY WITH @ID LIKE ": Y.CAT.NO:"..."
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        LOOP
            REMOVE CAT.ID  FROM KEY.LIST SETTING POS
        WHILE CAT.ID:POS
            CALL F.READ(CATEG.ENT.TODAY.FILE,CAT.ID, R.CAT,F.CAT, ETEXT)

            LOOP
                REMOVE TRNS.NO.T FROM R.CAT  SETTING POS1
            WHILE TRNS.NO.T:POS1

                CALL F.READ(FN.CAT.ENT,TRNS.NO.T,R.CAT.ENT,F.CAT.ENT,ERR1)
                CUR.CAT    = R.CAT.ENT<AC.CAT.CURRENCY>
                CATEG.PL.T = R.CAT.ENT<AC.CAT.PL.CATEGORY>
                TRNS.ID    = R.CAT.ENT<AC.CAT.OUR.REFERENCE>
                SYS.ID     = R.CAT.ENT<AC.CAT.SYSTEM.ID>
****************************FUNDS TRANSFER TODAY**************
                IF SYS.ID EQ 'FT' THEN
                    FT.ID = R.CAT.ENT<AC.CAT.OUR.REFERENCE>
                    CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ETEXT)
                    CATEG.LC.OLD.NO = R.FT<FT.DEBIT.THEIR.REF>

                    IF CATEG.LC.OLD.NO EQ TF.ID.OLD THEN
                        IF CUR.CAT EQ 'EGP' THEN
                            LCY.AMT = LCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.LCY>
                        END ELSE
                            FCY.AMT =   FCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.FCY>
                        END
                    END
                END
************************INF MULTI TXN TODAY****************
                IF SYS.ID EQ 'MLT' THEN
                    IN.ID = R.CAT.ENT<AC.CAT.OUR.REFERENCE>
                    INF.ID=FIELD(IN.ID,'-',1)
                    MULTI.INF.ID=FIELD(IN.ID,'-',2)

                    CALL F.READ(FN.IN,INF.ID,R.IN,F.IN,ETEXT)
                    CATEG.IN.LC.OLD.NO = R.IN<INF.MLT.THEIR.REFERENCE,MULTI.INF.ID>

                    IF TF.ID.OLD EQ CATEG.IN.LC.OLD.NO THEN
                        IF CUR.CAT EQ 'EGP' THEN
                            LCY.AMT = LCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.LCY>
                        END ELSE
                            FCY.AMT =   FCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.FCY>
                        END
                    END
                END
********************************LETTER OF CREDIT TODAY********************************
                IF SYS.ID EQ 'LCM' OR SYS.ID EQ 'LCD' THEN
                    TRNS.ID.LC = TRNS.ID[1,12]
                    IF TF.ID EQ TRNS.ID.LC THEN
                        IF CUR.CAT EQ 'EGP' THEN
                            LCY.AMT = LCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.LCY>
                        END ELSE
                            FCY.AMT =   FCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.FCY>
                        END
                    END
                END
            REPEAT
        REPEAT
***********************************************************
        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
        LOOP
            REMOVE CAT.ID FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE CAT.ID:YTYPE
            CALL F.READ(FN.CAT.ENT,CAT.ID,R.CAT.ENT,F.CAT.ENT,ETEXT)
            CCUR  = R.CAT.ENT<AC.CAT.CURRENCY>

            SYS.ID = R.CAT.ENT<AC.CAT.SYSTEM.ID>
            COMP = R.CAT.ENT<AC.CAT.COMPANY.CODE>
********************************LETTER OF CREDIT*******************
            IF SYS.ID EQ 'LCM' OR SYS.ID EQ 'LCD' THEN
                TRNS.ID.LIV = R.CAT.ENT<AC.CAT.OUR.REFERENCE>
                TRNS.ID.LC.LIV = TRNS.ID.LIV[1,12]
                IF TF.ID EQ TRNS.ID.LC.LIV THEN
                    IF R.CAT.ENT<AC.CAT.CURRENCY> EQ  'EGP' THEN
                        LCY.AMT = LCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.LCY>
                    END ELSE
                        FCY.AMT = FCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.FCY>
                    END
                END
            END
****************************FUNDS TRANSFER *************************
            IF SYS.ID EQ 'FT' THEN
                FT.ID = R.CAT.ENT<AC.CAT.OUR.REFERENCE>
                FT.ID.HIS = FT.ID:';1'
                CALL F.READ(FN.FT.HIS,FT.ID.HIS,R.FT.HIS,F.FT.HIS,ETEXT)
                IF R.FT.HIS<FT.TRANSACTION.TYPE> NE '' THEN
                    CATEG.LC.OLD.NO = R.FT.HIS<FT.DEBIT.THEIR.REF>
                END
**    CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ETEXT)
**  CATEG.LC.OLD.NO = R.FT<FT.DEBIT.THEIR.REF>
                IF CATEG.LC.OLD.NO EQ TF.ID.OLD THEN
                    IF R.CAT.ENT<AC.CAT.CURRENCY> EQ  'EGP' THEN
                        LCY.AMT = LCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.LCY>
                    END ELSE
                        FCY.AMT = FCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.FCY>
                    END
                END
            END
********************************INF MULTI TXN***********************
            IF SYS.ID EQ 'MLT' THEN
                IN.ID = R.CAT.ENT<AC.CAT.OUR.REFERENCE>
                INF.ID=FIELD(IN.ID,'-',1)
                MULTI.INF.ID=FIELD(IN.ID,'-',2)

                CALL F.READ(FN.IN,INF.ID,R.IN,F.IN,ETEXT)
                CATEG.IN.LC.OLD.NO = R.IN<INF.MLT.THEIR.REFERENCE,MULTI.INF.ID>

                IF  CATEG.IN.LC.OLD.NO EQ TF.ID.OLD THEN
                    IF R.CAT.ENT<AC.CAT.CURRENCY> EQ  'EGP' THEN
                        LCY.AMT = LCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.LCY>
                    END ELSE
                        FCY.AMT = FCY.AMT + R.CAT.ENT<AC.CAT.AMOUNT.FCY>
                    END
                END
            END
*********************************************************
** PRINT CAT.ID :"  ***  " : COMP : "*" : FCY.AMT
        REPEAT
        XX = XX + YY
    NEXT I
    O.DATA = LCY.AMT:'-':FCY.AMT
    RETURN
END
