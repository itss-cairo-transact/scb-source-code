* @ValidationCode : Mjo1NzU1MjE1ODpDcDEyNTI6MTY0MTM2ODExNjM2Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 05 Jan 2022 09:35:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LAST.WDAY(REQ.DATE)
************************************************************************
***********by Mahmoud Elhawary ** 5/1/2009 *****************************
************************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
************************************************************************
*convert the date required to the last working day of the month
************************************************************************
    TDR   = REQ.DATE
    TDRMN = TDR[5,2]
    TDRYY = TDR[1,4]
    IF TDRMN NE '12' THEN
        TDNMN = TDRMN + 1
    END ELSE
        TDNMN = '01'
        TDRYY = TDRYY + 1
    END
    X = LEN(TDNMN)
    IF X = 1 THEN
        TDNMN = '0':TDNMN
    END
    TDREM = TDRYY:TDNMN:'01'
    CALL CDT("",TDREM,'-1W')
    TDD = TDREM
    REQ.DATE = TDD
RETURN
