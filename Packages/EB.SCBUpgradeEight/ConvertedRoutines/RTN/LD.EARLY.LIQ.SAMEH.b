* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LD.EARLY.LIQ.SAMEH(CUSTOMER, DEAL.AMOUNT, DEAL.CURRENCY, CURRENCY.MARKET, CROSS.RATE, CROSS.CURRENCY, DRAWDOWN.CCY, T.DATA, CUST.COND, CHARGE.AMOUNT)

********PREPARED BY SAMEH*************************************************
*CHECK LIB YEAR                                                          *
*CHECK CURRENCY IF CURRENCY EQ EGP LIQ.RATE EQ .02 ELSE LIQ.RATE EQ .01  *
*THERE IS NO INTEREST IF DEPOSIT PERIOD 3M OR MORE ELSE                  *
*CALC LIQ.COMMISSION EQ AMOUNT * LIQ.RATE * PERIOD                       *
**************************************************************************

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.LD.ACC.BAL

    LDYEAR = R.NEW(LD.VALUE.DATE)[1,4]
    IF MOD(LDYEAR, 4) = 0 THEN
        LD.DAYS = 366
    END ELSE
        LD.DAYS = 365
    END

    IF R.NEW(LD.CURRENCY) = 'EGP' THEN
        LIQ.RATE = .02
    END ELSE
        LIQ.RATE = .01
    END

*    IF R.NEW(LD.CATEGORY) GE 21005 AND R.NEW(LD.CATEGORY) LE 21010 OR R.NEW(LD.CATEGORY) GE 21016 AND R.NEW(LD.CATEGORY) LE 21018 THEN
*        CALL DBR ('RE.LD.ACC.BAL':@FM:RE.LAB.OUTS.CUR.ACC.I.PAY,ID.NEW,ACCRUAL)
*        R.NEW(LD.CHRG.AMOUNT,1) = ACCRUAL
*        CHARGE.AMOUNT = R.NEW(LD.CHRG.AMOUNT,1)
*        CALL REBUILD.SCREEN
*    END ELSE
        GOSUB CALC.LIQ.CHARGE.LESS3M
*    END
    RETURN

********************************************************
CALC.LIQ.CHARGE.LESS3M:
    IF R.NEW(LD.CHRG.AMOUNT,1) = '' THEN

        EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
        ISSUE.DATE   = R.NEW(LD.VALUE.DATE)
        AMMOUNT      = R.NEW(LD.AMOUNT)
        DAY          = 'C'
        CALL CDD("",ISSUE.DATE,EXP.DATE , DAY)

        R.NEW(LD.CHRG.AMOUNT)<1,1> = (AMMOUNT * LIQ.RATE ) * (DAY / LD.DAYS )
        CHARGE.AMOUNT = R.NEW(LD.CHRG.AMOUNT,1)
        CALL REBUILD.SCREEN

    END

    ZZ = (AMMOUNT * LIQ.RATE ) * (DAY / LD.DAYS )

    IF ZZ # R.NEW(LD.CHRG.AMOUNT,1) THEN

        TEXT = "�� ���� ���� ������"
        CALL STORE.OVERRIDE(R.NEW(EB.CUS.CURR.NO)+1)

    END
    RETURN

************************************************************
END
