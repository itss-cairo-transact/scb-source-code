* @ValidationCode : Mjo2NTkyODQ0Mjk6Q3AxMjUyOjE2NDIxMDUwOTMzNzc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 13 Jan 2022 22:18:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
SUBROUTINE LC.ISSUE.IMP
**PROGRAM LC.ISSUE.IMP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='LC.ISSUE.IMP'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------

    IF ID.NEW EQ '' THEN
        FN.LC = 'FBNK.LETTER.OF.CREDIT'
    END ELSE
        FN.LC = 'FBNK.LETTER.OF.CREDIT$NAU'
    END
    F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    IF ID.NEW EQ '' THEN
        YTEXT = "Enter the TF No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        LC.ID =COMI[1,12]
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E1)
        LC.REF=LC.ID
    END ELSE
******************************************
        IF ID.NEW NE '' THEN
            LC.ID =ID.NEW
            CALL F.READ(FN.LC,ID.NEW,R.LC,F.LC,E1)
            LC.REF=ID.NEW
        END
    END



    CUS.NO = R.LC<TF.LC.APPLICANT.CUSTNO>
    AC.NO  = R.LC<TF.LC.PROVIS.ACC>

***UPDATED BY HYTHAM ---UPGRADING R15---2016-03-07---
*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,AC.NO,AC.COMP)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,AC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
    BRANCH.ID  = AC.COMP[8,2]
********
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*  **  CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,AC.NO,BRANCH.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,AC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    BRANCH.ID=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL.REF1)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS>
    NN.TITLE = 'LC.ISSUE.IMP'


    LC.NO = R.LC<TF.LC.OLD.LC.NUMBER>
    AMOUNT = R.LC<TF.LC.LC.AMOUNT>
    CUR.ID = R.LC<TF.LC.LC.CURRENCY>

*Line [ 132 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>


    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]    = '��� ������       :  ':CUST.NAME
    XX9<1,1>[3,35]   = '����������       :  ':CUST.ADDRESS

    XX1<1,1>[30,15] = '������ ������ ��� :  ':LC.NO:' - ':LC.REF

    XX2<1,1>[30,15] = '��������          :  ':AMOUNT : ' ' : CUR

    XX3<1,1>[3,35]   = '����� �������� ����� �� ���� ���� �������� �������� ������ ������ �����'
    XX4<1,1>[3,35]   = '- ���� �� ����� ��������'
    XX5<1,1>[3,35]   = '- ����� �������� ���� ������� ���� ����� �� ������ �� ���� ���� ��� ��������'
    XX6<1,1>[3,35]   = '�� ��������� �� ��� ����� ���� ������ ��� ������ ���� ��� ���� �� ����� ��� ��������'
    XX7<1,1>[3,35]   = '������ ������ ���������'
    XX8<1,1>[30,35]  = '������� ����� ���� ��������'
*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):NN.TITLE
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX9<1,1>
    PRINT STR(' ',80)
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT STR(' ',80)
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT STR(' ',80)
    PRINT XX7<1,1>
    PRINT STR(' ',80)
    PRINT XX8<1,1>
*===============================================================
RETURN
END
