* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-212</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.INWARD.ISSUE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONDITIONS> EQ '' THEN
        GOSUB INITIATE
*Line [ 46 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 47 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
        GOSUB CALLDB
        MYID = MYCODE:'.':MYTYPE
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
MYVER=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.DESCRIPTION>

        IF MYCODE = "2111" THEN
****************************************
******************************************
            GOSUB PRINT.HEAD
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
        END ELSE
            E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*==============================================================
INITIATE:
    REPORT.ID='LG.INWARD.ISSUE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
PRINT.HEAD:
    PR.HD ="'L'":SPACE(1):"Suez Canal Bank"
    PR.HD :="'L'":SPACE(1):"Branch:":YYBRN : SPACE(5) : "ON " : ISSUE.DATE
    PR.HD :="'L'":SPACE(1):STR('_',30)
    HEADING PR.HD

    LNTH1 = LEN(CUST.NAME)+2
    LNE1 = 50-LNTH1
    LNTHD1 = LEN(CUST.ADD)+2
    LNED1 = 50-LNTHD1
    PRINT

    PRINT " ________________________________________________"
    PRINT "| ":CUST.NAME:SPACE(LNE1):"|"
    PRINT "|":SPACE(49):"|"
****************************************
    PRINT "| ":CUST.ADD:SPACE(LNED1):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT

    PRINT SPACE(1):"Dear Sirs,"
    PRINT

    PRINT SPACE(20): "L/G NO.SC  : ":LG.NO
    PRINT
    PRINT SPACE(20): "Amount : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(20): "Your ref. ": LG.REF
    PRINT
    PRINT SPACE(20):"________________________________________________"

    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = ''
    CALL OPF(FN.CUST,F.CUST)


    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    ISS.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE = ISS.DATE[7,2]:"/":ISS.DATE[5,2]:"/":ISS.DATE[1,4]
    FIN.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = FIN.DATE[7,2]:"/":FIN.DATE[5,2]:"/":FIN.DATE[1,4]
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.REF = LOCAL.REF<1,LDLR.SENDING.REF>
    MSG.TYPE=R.LD<LD.OUR.REMARKS>
    SWIFT.DATE = LOCAL.REF<1,99>
    SWIFT.DATE = SWIFT.DATE[7,2]:"/":SWIFT.DATE[5,2]:"/":SWIFT.DATE[1,4]

    LC.REF = LOCAL.REF<1,94>
    ISS.COU = LOCAL.REF<1,95>
    FOR.DEL = LOCAL.REF<1,96>
    BEN.PRI = LOCAL.REF<1,97>
    REM.CRE = LOCAL.REF<1,98>

    CHR.CODE=LOCAL.REF<1,LDLR.T.CHRG.CODE>
    CHRG.AMT.COMM=LOCAL.REF<1,LDLR.T.CHRG.AMT,1>
    CHRG.AMT.STMP=LOCAL.REF<1,LDLR.T.CHRG.AMT,2>
    CHRG.AMT.POST=LOCAL.REF<1,LDLR.T.CHRG.AMT,3>
    CHRG.AMT=CHRG.AMT.COMM+CHRG.AMT.STMP+CHRG.AMT.POST

    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CUR.CO=LOCAL.REF<1,LDLR.ACC.CUR>

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 153 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME

    MYCODE =LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
    CUST.ID = R.LD<LD.CUSTOMER.ID>
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
OPER.CODE=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.OPERATION.CODE>
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.ID,AC.OFICER)
*Line [ 174 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.ID,AC.OFICER)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
AC.OFICER=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

*Line [ 184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',1)
**********************

    CALL F.READ(FN.CUST,CUST.ID,R.CUST,F.CUST,E1)
    CUST.NAME=R.CUST<EB.CUS.SHORT.NAME>

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT> THEN
        CUST.ADD = LOCAL.REF<1,LDLR.FREE.TEXT>
    END ELSE
        CUST.ADD=R.CUST<EB.CUS.STREET>
    END
    SEND.BANK=LOCAL.REF<1,LDLR.SEN.REC.BANK>
    CALL F.READ(FN.CUST,SEND.BANK,R.CUST,F.CUST,E1)
    SEND.NAME=R.CUST<EB.CUS.SHORT.NAME>
    SEND.ADD=R.CUST<EB.CUS.STREET>

********************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
    LG.NO='LG/': SAM:"/": SAM1:"/":SAM2:'-':LC.REF
********************
    CUR=R.LD<LD.CURRENCY>
    FN.CUR='F.CURRENCY'
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,E1)
    CRR=R.CUR<EB.CUR.CCY.NAME,1>


    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    RETURN
*===============================================================
BODY:
    PRINT;PRINT

    PRINT SPACE(8):"With reference to your swift dated ": SWIFT.DATE : " MTF " : MSG.TYPE : " we write to inform"
    PRINT

    PRINT SPACE(5):"you that we have ":ISS.COU:" and ":FOR.DEL:" the above mentioned letter of "
    PRINT

    PRINT SPACE(5):"guarantee to the ":BEN.PRI:"  in Cairo."
    PRINT

    IF LC.REF NE '' THEN
        PRINT SPACE(5):"Requesting them to authorize us to amend LC No" : LC.REF : "to be operative."
        PRINT
    END

    PRINT SPACE(5):"we enclose herewith two copies of the letter of gurantee for your records."
    PRINT

    PRINT SPACE(5):"Please ": REM.CRE :" our account with ": SEND.NAME
    PRINT

    PRINT SPACE(5):"The sum of ( " : CUR:" )" : CHRG.AMT : " being our commission and charges made up as follows":":-"
    PRINT

    PRINT SPACE(5): CUR : SPACE(1):CHRG.AMT.COMM :" Our commision for one year ending on " : FIN.DATE
    PRINT

    PRINT SPACE(09): CHRG.AMT.STMP : " Fiscal stamps"
    PRINT

    PRINT SPACE(09): CHRG.AMT.POST : " Postal expenses"
    PRINT

    PRINT SPACE(5):STR('_',11)
    PRINT SPACE(5): CUR : SPACE(1) : CHRG.AMT
    PRINT
    PRINT SPACE(5):STR('_',11)
    PRINT

    PRINT SPACE(5):"Kindly aknowladge receipt and let us have your advice of execution,"
    PRINT

    PRINT SPACE(5):"quoting our Ref.No.Sc " : LG.NO
    PRINT
    IF CUR.CO NE CUR THEN
        PRINT SPACE(5):"It remains understood that in case of implementation reimbursement will be made in ": CUR
        PRINT
    END
    PRINT SPACE(35):"Yours faithfully"
    PRINT SPACE(35):"For The Suez Canal Bank"
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

    RETURN
END
*==================================================================
