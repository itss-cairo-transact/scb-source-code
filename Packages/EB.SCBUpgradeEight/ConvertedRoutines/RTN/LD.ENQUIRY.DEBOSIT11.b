* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LD.ENQUIRY.DEBOSIT11

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE

*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('LD.SCHEDULE.DEFINE':@FM:LD.SD.FREQUENCY,O.DATA,FREQ)
F.ITSS.LD.SCHEDULE.DEFINE = 'FBNK.LD.SCHEDULE.DEFINE'
FN.F.ITSS.LD.SCHEDULE.DEFINE = ''
CALL OPF(F.ITSS.LD.SCHEDULE.DEFINE,FN.F.ITSS.LD.SCHEDULE.DEFINE)
CALL F.READ(F.ITSS.LD.SCHEDULE.DEFINE,O.DATA,R.ITSS.LD.SCHEDULE.DEFINE,FN.F.ITSS.LD.SCHEDULE.DEFINE,ERROR.LD.SCHEDULE.DEFINE)
FREQ=R.ITSS.LD.SCHEDULE.DEFINE<LD.SD.FREQUENCY>

    FRQLEN = LEN(FREQ) - 1


    IF FREQ[FRQLEN,1] EQ 'M' THEN

        O.DATA = FREQ[1,FRQLEN]:"����"

    END ELSE

        O.DATA = FREQ[1,FRQLEN]:"����"

    END
IF NOT(FREQ) THEN O.DATA = ""
    RETURN
END
