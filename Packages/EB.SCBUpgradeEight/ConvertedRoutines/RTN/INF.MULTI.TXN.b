* @ValidationCode : MjoyMTM0MDAwMTAzOkNwMTI1MjoxNjQwNzA5OTkyODIxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:46:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>9157</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE INF.MULTI.TXN

***** MODIFIED *****
******************************************************************************************
* 07/05/2001 -N.Betsias- New application created in order to be able handle multiple     *
* debit/credit transactions.                                                             *
* This application based on data capture concept in order to update Position&Position    *
* management tables.                                                                     *
*                                                                                        *
* Do not modify this program without comments.                                           *
* If you want to modify something then do it using external routines.                    *
*                                                                                        *
* 05/12/2001 -N.Betsias, INFORMER SA-                                                    *
* Fix a bug with on accounting entries sequence.                                         *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 25/03/02                                                     *
* FIXED THE PROBLEM THAT WE ENTER CUSTOMER DIFFERENT THAN THE ACCOUNT'S OWNER            *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 06/09/02                                                     *
* INCREASED THE DIMENSION SIZE BECAUSE AFTER THE CURRENT TRANSACTION IF YOU OPEN TABLES  *
* WITH MORE THAN 200 FIELDS (E.G LD) IT WILL CRASH.                                      *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 03/04/03                                                     *
* OVERRIDES SHOULD APPEAR AT INPUT AND NOT AT AUTHORISATION                              *
* UPDATE ALSO THE WORKING BALANCE OF ACCOUNTS AT INPUT.                                  *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 29/04/03                                                     *
* FIX THE BUG WHEN A RECORD IS RE-INPUTTED SO AS NOT TO UPDATE TWICE                     *
* THE WORKING BALANCE - PGM.FILE RECORD MUST BE CHANGED BY ADDING .NOH TO FIELD          *
* ADDITIONAL.INFO. APART FROM THAT OVERRIDES ARE EMPTIED IF RECORD IS RE-INPUTTED.       *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 04/11/03                                                     *
* A RECORD SHOULD BE VIEWED WITH SEE FUNCTION REGARDLESS THE JULIAN DATE. PRINT          *
* FUNCTION IS ALSO ENABLED.                                                              *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 12/01/04                                                     *
* 1) UNAUTHORISED RECORDS WITH OLD JULIAN DATA CAN BE PROCESSED.                         *
* 2) REFRESH THE UNBALANCED AMOUNT                                                       *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 27/01/04                                                     *
* FIX THE BUG THAT POSITIONS ARE UPDATED EVEN WHEN AN ERROR IS ENCOUNTERED.              *
*                                                                                        *
* UPDATED BY P.KARAKITSOS - 18/05/04                                                     *
* AMOUNT.FCY IS SHOULD HAVE VALUE IN CASE OF LOCAL CURRENCY BECAUSE IT AFFECTS THE       *
* STMT.ENTRY AND CATEG.ENTRY AS WELL.                                                    *
******************************************************************************************

*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TRANSACTION
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEALER.DESK
*Line [ 80 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 82 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 84 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY.MARKET
*Line [ 86 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 88 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 90 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.EU.PARAMETER
*Line [ 92 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATA.CAPTURE
*Line [ 94 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 96 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.INF.MULTI.TXN


    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising


* Main Program Loop

    LOOP
        CALL RECORDID.INPUT
    UNTIL (MESSAGE EQ 'RET')
        V$ERROR = ''
        IF MESSAGE EQ 'NEW FUNCTION' THEN
            GOSUB CHECK.FUNCTION        ;* Special Editing of Function
            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END
        END ELSE
            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT
            CALL RECORD.READ
            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END
            CALL MATRIX.ALTER
REM >       GOSUB CHECK.RECORD              ;* Special Editing of Record
REM >       IF ERROR THEN GOTO MAIN.REPEAT

REM >       GOSUB PROCESS.DISPLAY           ;* For Display applications

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN          ;* From main program
*****************************************************************************************


*****************************************************************************************
PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN
*****************************************************************************************



*****************************************************************************************
PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE          ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL        ;* Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION      ;* Special Cross Validation
        END CASE
*
* PKAR20040127 - START
*
*         IF NOT(V$ERROR) THEN
        IF NOT(V$ERROR) & NOT(END.ERROR) THEN
*
* PKAR20040127 - END
*
            GOSUB UPDATE.POSITION
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END
*
* PK - 03/04/03 START
*
*        STMTS = ''
*        IF NOT(V$ERROR) THEN
*           G.A = 1 ; ERR.MESS = ''
*           G = DCOUNT(R.NEW(INF.MLT.TXN.CODE), VM)
*           LOOP WHILE G => G.A  & NOT(ERR.MESS)
*              GOSUB FILL.STMT.ENTRY
*              G = G - 1
*           REPEAT
*
*            YTYPE = 'VAL'
*            CALL EB.ACCOUNTING("MLT", YTYPE, STMTS, '')
*            IF END.ERROR OR TEXT = "NO" THEN RETURN
*
* PK - 03/04/03 END
*
        CALL EB.ACCOUNTING("MLT", "AUT", "", "")
*
        CALL AUTH.RECORD.WRITE

        IF MESSAGE NE "ERROR" THEN
            GOSUB AFTER.AUTH.WRITE      ;* Special Processing after write
        END
*         END

    END

RETURN
*****************************************************************************************

*****************************************************************************************
FILL.STMT.ENTRY:

    YR.STMT = ''
    YR.STMT<AC.STE.ACCOUNT.NUMBER> = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,G>
    YR.STMT<AC.STE.COMPANY.CODE> = ID.COMPANY
    YR.STMT<AC.STE.AMOUNT.LCY> = R.NEW(INF.MLT.AMOUNT.LCY)<1,G>
    IF YR.STMT<AC.STE.AMOUNT.LCY> NE "" THEN
        IF R.NEW(INF.MLT.SIGN)<1,G>[1,1] = "D" THEN
            YR.STMT<AC.STE.AMOUNT.LCY> = YR.STMT<AC.STE.AMOUNT.LCY> * -1
        END
    END
    YR.STMT<AC.STE.TRANSACTION.CODE> = R.NEW(INF.MLT.TXN.CODE)<1,G>
    YR.STMT<AC.STE.THEIR.REFERENCE> = R.NEW(INF.MLT.THEIR.REFERENCE)<1,G>
    YR.STMT<AC.STE.PL.CATEGORY> = R.NEW(INF.MLT.PL.CATEGORY)<1,G>
    YR.STMT<AC.STE.CUSTOMER.ID> = R.NEW(INF.MLT.CUSTOMER.ID)<1,G>
    YR.STMT<AC.STE.ACCOUNT.OFFICER> = R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,G>
    YR.STMT<AC.STE.PRODUCT.CATEGORY> = R.NEW(INF.MLT.PROD.CATEGORY)<1,G>

    IF YR.STMT<AC.STE.ACCOUNT.NUMBER> NE "" THEN
        ACC = YR.STMT<AC.STE.ACCOUNT.NUMBER>
        GOSUB GET.ACC.CCY
        YR.STMT<AC.STE.ACCOUNT.OFFICER> = R.ACC<AC.ACCOUNT.OFFICER>
        YR.STMT<AC.STE.PRODUCT.CATEGORY> = R.ACC<AC.CATEGORY>
    END

    YR.STMT<AC.STE.VALUE.DATE> = R.NEW(INF.MLT.VALUE.DATE)<1, G>
    YR.STMT<AC.STE.CURRENCY> = R.NEW(INF.MLT.CURRENCY)<1, G>
    YR.STMT<AC.STE.AMOUNT.FCY> = R.NEW(INF.MLT.AMOUNT.FCY)<1, G>
    IF YR.STMT<AC.STE.AMOUNT.FCY> NE "" THEN
        IF R.NEW(INF.MLT.SIGN)<1,G>[1,1] = 'D' THEN
            YR.STMT<AC.STE.AMOUNT.FCY> = YR.STMT<AC.STE.AMOUNT.FCY> * -1
        END
    END

    YR.STMT<AC.STE.EXCHANGE.RATE> = R.NEW(INF.MLT.EXCHANGE.RATE)<1, G>
    YR.STMT<AC.STE.POSITION.TYPE> = R.NEW(INF.MLT.POSITION.TYPE)<1, G>
    IF R.NEW(INF.MLT.OUR.REFERENCE)<1, G> THEN
        YR.STMT<AC.STE.OUR.REFERENCE> = R.NEW(INF.MLT.OUR.REFERENCE)<1, G>
    END
    ELSE
        YR.STMT<AC.STE.OUR.REFERENCE> = ID.NEW:'-':G
    END
    YR.STMT<AC.STE.EXPOSURE.DATE> = R.NEW(INF.MLT.EXPOSURE.DATE)<1, G>
    YR.STMT<AC.STE.CURRENCY.MARKET> = R.NEW(INF.MLT.CURRENCY.MARKET)<1, G>
    YR.STMT<AC.STE.LOCAL.REF> = R.NEW(INF.MLT.LOCAL.REF)
    YR.STMT<AC.STE.DEPARTMENT.CODE> = R.NEW(INF.MLT.DEPART.CODE)<1, G>
    YR.STMT<AC.STE.TRANS.REFERENCE> = ID.NEW
    YR.STMT<AC.STE.SYSTEM.ID> = 'MLT'
    YR.STMT<AC.STE.BOOKING.DATE> = TODAY
    YR.STMT<AC.STE.STMT.NO> = R.NEW(INF.MLT.STMT.NO)
    YR.STMT<AC.STE.OVERRIDE> = R.NEW(INF.MLT.OVERRIDE)
    YR.STMT<AC.STE.DEALER.DESK> = R.NEW(INF.MLT.DEALER.DESK)<1, G>
    YR.STMT<AC.STE.BANK.SORT.CDE> = R.NEW(INF.MLT.BANK.SORT.CODE)<1, G>
    YR.STMT<AC.STE.CHEQUE.NUMBER> = R.NEW(INF.MLT.CHEQUE.NUMBER)<1, G>

    STMTS<-1> = LOWER(YR.STMT)

RETURN
*****************************************************************************************

*****************************************************************************************
PROCESS.DISPLAY:

* Display the record fields.

    IF SCREEN.MODE EQ 'MULTI' THEN
        CALL FIELD.MULTI.DISPLAY
    END ELSE
        CALL FIELD.DISPLAY
    END

RETURN
*****************************************************************************************

*****************************************************************************************
CHECK.ID:
    CALL EB.FORMAT.ID("IN")

***********ADDED BY MAHMOUD 21/4/2010**********************
*Line [ 336 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR("INF.MULTI.TXN$NAU":@FM:INF.MLT.CO.CODE,ID.NEW,COOO)
F.ITSS.INF.MULTI.TXN$NAU = 'F.INF.MULTI.TXN$NAU'
FN.F.ITSS.INF.MULTI.TXN$NAU = ''
CALL OPF(F.ITSS.INF.MULTI.TXN$NAU,FN.F.ITSS.INF.MULTI.TXN$NAU)
CALL F.READ(F.ITSS.INF.MULTI.TXN$NAU,ID.NEW,R.ITSS.INF.MULTI.TXN$NAU,FN.F.ITSS.INF.MULTI.TXN$NAU,ERROR.INF.MULTI.TXN$NAU)
COOO=R.ITSS.INF.MULTI.TXN$NAU<INF.MLT.CO.CODE>
    IF COOO NE '' THEN
        IF V$FUNCTION # "S" & V$FUNCTION # "P" THEN
            IF COOO # ID.COMPANY THEN
                E = 'CANNOT ACCESS RECORD FROM ANOTHER COMPANY'
                CALL ERR ; V$ERROR = 1
            END
        END
    END
***********************************************************

    IF V$FUNCTION = 'I' THEN
        ETEXT = '' ; DUMM.IF.E = ''
*Line [ 348 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 356 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR("INF.MULTI.TXN":@FM:1, ID.NEW, DUMM.IF.E)
F.ITSS.INF.MULTI.TXN = 'F.INF.MULTI.TXN'
FN.F.ITSS.INF.MULTI.TXN = ''
CALL OPF(F.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN)
CALL F.READ(F.ITSS.INF.MULTI.TXN,ID.NEW,R.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN,ERROR.INF.MULTI.TXN)
DUMM.IF.E=R.ITSS.INF.MULTI.TXN<1>
        IF NOT(ETEXT) THEN
            E = 'DEAL ALREADY AUTHORISED'
            CALL ERR ; V$ERROR = 1
        END ELSE ETEXT = ''
        IF ID.NEW[1,2] # 'IN' THEN
            E = 'ENTER FULL ID '
            CALL ERR ; V$ERROR = 1
        END
    END
*
* PK - 04/11/03 START
*
    ELSE
        IF V$FUNCTION # "S" & V$FUNCTION # "P" THEN
*
* PK - 04/11/03 END
*
            FIR.ID.PART = 'IN':R.DATES(EB.DAT.JULIAN.DATE)[5]
            IF LEN(COMI) <= 5 THEN
                COMI = STR('0',5 - LEN(COMI)) : COMI
                ID.NEW = FIR.ID.PART:COMI
            END
            ELSE
                IF FIR.ID.PART NE ID.NEW[1,7] THEN
*
* PKAR20040112 - START
*
                    IF R.NEW(INF.MLT.RECORD.STATUS) # "INAU" & R.NEW(INF.MLT.RECORD.STATUS) # "RNAU" & R.NEW(INF.MLT.RECORD.STATUS) # "IHLD" THEN
*
* PKAR20040112 - END
*
                        E = 'DATE CANNOT BE GREATER THAN TODAY'
                        V$ERROR = 1
                        CALL ERR
*
* PKAR20040112 - START
*
                    END
*
* PKAR20040112 - END
*
                END
            END
*
* PK - 04/11/03 START
*
        END
    END
*
* PK - 04/11/03 END
*

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.


RETURN
*****************************************************************************************


*****************************************************************************************
CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


RETURN

*****************************************************************************************


*****************************************************************************************
CHECK.FIELDS:

    BEGIN CASE
        CASE AF = INF.MLT.ACCOUNT.NUMBER
            IF COMI THEN
                ACC = COMI
                GOSUB GET.ACC.CCY
                R.NEW(INF.MLT.CURRENCY)<1, AV> = AC.CCY
                CALL REBUILD.SCREEN ; P = 0
            END
        CASE AF = INF.MLT.CURRENCY
            IF COMI THEN
                IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1, AV> THEN
                    ACC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1, AV>
                    GOSUB GET.ACC.CCY
                    IF AC.CCY # COMI  THEN
                        E = 'INVALID CURRENCY'
                    END
                END
            END
        CASE AF = INF.MLT.PL.CATEGORY
            IF COMI THEN
* May I have to change the following.
                IF COMI < 50000 AND COMI > 70000 THEN
                    E = 'INVALID PL CATEGORY'
                END
                ELSE
                    SYS.IND = '' ; ETEXT = ''
*Line [ 449 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 463 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR("CATEGORY":@FM:EB.CAT.SYSTEM.IND, COMI, SYS.IND)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,COMI,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
SYS.IND=R.ITSS.CATEGORY<EB.CAT.SYSTEM.IND>
                    IF SYS.IND # 'PL' THEN
                        E = 'INVALID PL CATEGORY'
                    END
                END
            END
        CASE AF = INF.MLT.TXN.CODE
            IF R.NEW(INF.MLT.SIGN)<1, AV> THEN
                IF COMI THEN
                    TXN.CODE = COMI
                    SIGN = R.NEW(INF.MLT.SIGN)<1, AV>
                    GOSUB CHECK.TXN.TABLE
                    IF ERR.MESS THEN E = ERR.MESS
                END
            END
            ELSE E = 'MISSING SIGN'
        CASE OTHERWISE
*Line [ 467 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
    END CASE

    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

RETURN
*****************************************************************************************

*****************************************************************************************
GET.ACC.CCY:

    F.ACCOUNT = ''
    CALL OPF("F.ACCOUNT", F.ACCOUNT)

    CALL F.READ("F.ACCOUNT", ACC, R.ACC, F.ACCOUNT, ETEXT)
    IF NOT(ETEXT) THEN
        AC.CCY = R.ACC<AC.CURRENCY>
    END
* PKAR20040127   ELSE ETEXT = ''

RETURN
*****************************************************************************************

*****************************************************************************************
CHECK.TXN.TABLE:

    FN.NAME = 'F.TRANSACTION' ; F.TRANSACTION = ''
    CALL OPF(FN.NAME, F.TRANSACTION)

    ERR.MESS = ''
    CALL F.READ("F.TRANSACTION", TXN.CODE, R.TXN, F.TRANSACTION, ERR.MESS)
    IF NOT(ERR.MESS) THEN
        IF R.TXN<AC.TRA.DATA.CAPTURE>[1,1] = 'Y' THEN
            IF SIGN[1,1] # R.TXN<AC.TRA.DEBIT.CREDIT.IND>[1,1] THEN
                ERR.MESS = 'INVALID SIGN FOR TRANSACTION CODE'
            END
        END
        ELSE
            ERR.MESS = 'INVALID TRANSACTION CODE'
        END
    END


RETURN
*************************************************************************

*************************************************************************
CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSS.VALIDATION
*
*
* PK - 29/04/03 START
*
    R.NEW(INF.MLT.OVERRIDE) = ""
*
* PK - 29/04/03 END
*
    R.NEW(INF.MLT.NUM.OF.CRS) = ''
    R.NEW(INF.MLT.NUM.OF.DRS) = ''
    R.NEW(INF.MLT.TOT.LCY.DRS.AMT) = ''
    R.NEW(INF.MLT.TOT.LCY.CRS.AMT) = ''

    IF NOT(R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1>) & NOT(R.NEW(INF.MLT.PL.CATEGORY)<1,1>) THEN
        AF = INF.MLT.ACCOUNT.NUMBER ; AV = 1 ; AS = 1
        ETEXT = 'ENTER ACCOUNT OR PL'
        CALL STORE.END.ERROR
    END

*Line [ 545 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    I = 1 ; CNT = DCOUNT(R.NEW(INF.MLT.TXN.CODE), @VM)
    LOOP WHILE I <= CNT
        YDIF.RATE = '' ; YCALC.LAMT = '' ; YCALC.FAMT = '' ; YCALC.RATE = ''
        YFAMT = '' ; YLAMT = '' ; BASE.CCY = '' ; TEST.YRATE = ''
        YDIF.AMT = ''
        GOSUB CHECK.ACC.AND.PL
*
*     PK - 25/03/02
*
*******  CALL INF.MULTI.TXN.UPD(R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I>,
*******                         R.NEW(INF.MLT.PL.CATEGORY)<1,I>,
*******                        R.NEW(INF.MLT.CUSTOMER.ID)<1,I>,
*******                        I)
*
*     PK - 25/03/02
*
* PK - 18/05/04 START
*

*
* MAHMOUD - 16/6/2011
* Changed <1,CNT> to <1, I> & AV = CNT to AV = I ; To validate all multi values
* MAHMOUD - Start
*

        IF R.NEW(INF.MLT.AMOUNT.FCY)<1, I> & R.NEW(INF.MLT.CURRENCY)<1, I> = LCCY THEN
            AF = INF.MLT.AMOUNT.FCY ; AV = I ; AS = 1
            ETEXT = "INPUT NOT ALLOWED FOR LCY"
            CALL STORE.END.ERROR
            CALL REBUILD.SCREEN ; P = 0
        END
*
* MAHMOUD - End
*

*
* PK - 18/05/04 END
*
        GOSUB CHECK.TXN.AND.SIGN
        GOSUB ADD.DATA
        GOSUB CHECK.CCY
        GOSUB CHECK.AMTS
        GOSUB CHECK.VAL.DATE
        GOSUB CHECK.EXCH.RATE
        GOSUB CHECK.EXP.DATE
        GOSUB UPD.EXCH.RATE
        GOSUB UPD.NOINP.AMTS
        GOSUB CHECK.PL.NUMBER
        I += 1
    REPEAT
    GOSUB CHECK.PROD.CAT

    IF R.NEW(INF.MLT.TOT.LCY.DRS.AMT) # R.NEW(INF.MLT.TOT.LCY.CRS.AMT) THEN
        AF = INF.MLT.TOT.LCY.DRS.AMT ; AV = 1 ; AS = 1
        ETEXT = 'UNBALANCED TRANSACTION - CANNOT CONTINUE'
        CALL STORE.END.ERROR
*
* PKAR20040112 - START
*
        CALL REBUILD.SCREEN ; P = 0
*
* PKAR20040112 - END
*
    END

    IF END.ERROR THEN         ;* Cross validation error
        RETURN      ;* Back to field input via UNAUTH.RECORD.WRITE
    END

*
*  Overrides should reside here.
*
REM > CALL XX.OVERRIDE
*

*
    CURR.NO = R.NEW(INF.MLT.CURR.NO) + 1
*Line [ 623 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    I = 1 ; CNT = DCOUNT(R.NEW(INF.MLT.TXN.CODE), @VM)
    LOOP WHILE I <= CNT
        GOSUB DISP.FCY.OVR
        GOSUB DISP.EURO.OVR
        GOSUB DISP.EXH.OVR
        GOSUB DISP.EXP.OVR
*########################### MAHMOUD 20/5/2012 #####################
        GOSUB CHQ.ACC.OVR
        IF I = CNT THEN
            GOSUB USR.LEVEL.OVR
        END
*###########################    END OF ADD     #####################
        I += 1
    REPEAT
*
* PK - 03/04/03 START
*
*      IF TEXT = "NO" THEN                ; * Said NO to override
*         V$ERROR = 1
*         MESSAGE = "ERROR"               ; * Back to field input
*         RETURN
*      END
*
* PK - 03/04/03 END
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc
*
* PK - 29/04/03 START
*
    IF TEXT # "NO" THEN
        IF R.NEW(INF.MLT.RECORD.STATUS) = "INAU" THEN GOSUB CHECK.DELETE
    END

    IF TEXT # "NO" THEN
        IF R.NEW(INF.MLT.RECORD.STATUS) = "INAO" THEN GOSUB CHECK.DELETE
    END

*
* PK - 29/04/03 END
*
* PKAR20040127 - START
*
    IF NOT(ETEXT) & NOT(END.ERROR) THEN
*
* PKAR20040127 - END
*
* PK - 03/04/03 START
*
        IF TEXT # "NO" THEN
            GOSUB INPUT.PROCESS
        END
*
* PK - 03/04/03 END
*
* PKAR20040127 - START
*
    END
*
*      IF TEXT = "NO" ! END.ERROR THEN                ; * Said No to override

    IF TEXT = "NO" ! END.ERROR ! ETEXT THEN
*
* PKAR20040127 - END
*
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END


*
* Additional updates should be performed here
*
REM > CALL XX...

RETURN

*************************************************************************
*
* PK - 03/04/03 START
*
INPUT.PROCESS:
*
    STMTS = ''
    IF NOT(V$ERROR) THEN
        G.A = 1 ; ERR.MESS = ''
*Line [ 713 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        G = DCOUNT(R.NEW(INF.MLT.TXN.CODE), @VM)
        LOOP WHILE G => G.A  & NOT(ERR.MESS)
            GOSUB FILL.STMT.ENTRY
            G = G - 1
        REPEAT
        YTYPE = 'VAL'
        CALL EB.ACCOUNTING("MLT", YTYPE, STMTS, '')
        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END
*
RETURN
*
* PK - 03/04/03 END
*
*************************************************************************
UPDATE.POSITION:

    SAVE.ID.NEW = ID.NEW
* PK - 06/09/02  DIM R.TEMP(200)
* PK - 06/09/02  DIM R.NEW(200)
*
* PK - 06/09/02
*
    DIM R.TEMP(400)
    DIM R.NEW(400)  ;* FOR RUNNING APPLICATIONS AFTER CURRENT TRANSACTION WITH MORE THAN 200 FIELDS
*
* PK - 06/09/02
*
    MAT R.TEMP = MAT R.NEW

*Line [ 744 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR J = 1 TO DCOUNT(R.TEMP(INF.MLT.TXN.CODE), @VM)
        ID.NEW = SAVE.ID.NEW
        ID.NEW := '-':J
        MAT R.NEW = ''
        R.NEW(DC.DC.ACCOUNT.NUMBER) = R.TEMP(INF.MLT.ACCOUNT.NUMBER)<1,J>
        R.NEW(DC.DC.SIGN) = R.TEMP(INF.MLT.SIGN)<1,J>
        R.NEW(DC.DC.AMOUNT.LCY) = R.TEMP(INF.MLT.AMOUNT.LCY)<1,J>
        R.NEW(DC.DC.TRANSACTION.CODE) = R.TEMP(INF.MLT.TXN.CODE)<1,J>
        R.NEW(DC.DC.THEIR.REFERENCE) = R.TEMP(INF.MLT.THEIR.REFERENCE)<1,J>
        R.NEW(DC.DC.PL.CATEGORY) = R.TEMP(INF.MLT.PL.CATEGORY)<1,J>
        R.NEW(DC.DC.CUSTOMER.ID) = R.TEMP(INF.MLT.CUSTOMER.ID)<1,J>
        R.NEW(DC.DC.ACCOUNT.OFFICER) = R.TEMP(INF.MLT.ACCOUNT.OFFICER)<1,J>
        R.NEW(DC.DC.PRODUCT.CATEGORY) = R.TEMP(INF.MLT.PROD.CATEGORY)<1,J>
        R.NEW(DC.DC.VALUE.DATE) = R.TEMP(INF.MLT.VALUE.DATE)<1,J>
        R.NEW(DC.DC.CURRENCY) = R.TEMP(INF.MLT.CURRENCY)<1,J>
        R.NEW(DC.DC.AMOUNT.FCY) = R.TEMP(INF.MLT.AMOUNT.FCY)<1,J>
        R.NEW(DC.DC.EXCHANGE.RATE) = R.TEMP(INF.MLT.EXCHANGE.RATE)<1,J>
        R.NEW(DC.DC.POSITION.TYPE) = R.TEMP(INF.MLT.POSITION.TYPE)<1,J>
        R.NEW(DC.DC.OUR.REFERENCE) = R.TEMP(INF.MLT.OUR.REFERENCE)<1,J>
        R.NEW(DC.DC.EXPOSURE.DATE) = R.TEMP(INF.MLT.EXPOSURE.DATE)<1,J>
        R.NEW(DC.DC.CURRENCY.MARKET) = R.TEMP(INF.MLT.CURRENCY.MARKET)<1,J>
        R.NEW(DC.DC.DEPARTMENT.CODE) = R.TEMP(INF.MLT.DEPART.CODE)<1,J>
        R.NEW(DC.DC.DEALER.DESK) = R.TEMP(INF.MLT.DEALER.DESK)<1,J>
        R.NEW(DC.DC.BANK.SORT.CDE) = R.TEMP(INF.MLT.BANK.SORT.CODE)<1,J>
        R.NEW(DC.DC.CHEQUE.NUMBER) = R.TEMP(INF.MLT.CHEQUE.NUMBER)<1,J>

        R.NEW(DC.DC.STMT.NO) = R.TEMP(INF.MLT.STMT.NO)
        R.NEW(DC.DC.OVERRIDE) = R.TEMP(INF.MLT.OVERRIDE)
        R.NEW(DC.DC.RECORD.STATUS) = R.TEMP(INF.MLT.RECORD.STATUS)
        R.NEW(DC.DC.CURR.NO) = R.TEMP(INF.MLT.CURR.NO)
        R.NEW(DC.DC.INPUTTER) = R.TEMP(INF.MLT.INPUTTER)
        R.NEW(DC.DC.DATE.TIME) = R.TEMP(INF.MLT.DATE.TIME)
        R.NEW(DC.DC.AUTHORISER) = R.TEMP(INF.MLT.AUTHORISER)
        R.NEW(DC.DC.CO.CODE) = R.TEMP(INF.MLT.CO.CODE)
        R.NEW(DC.DC.DEPT.CODE) = R.TEMP(INF.MLT.DEPT.CODE)
        R.NEW(DC.DC.AUDITOR.CODE) = R.TEMP(INF.MLT.AUDITOR.CODE)
        R.NEW(DC.DC.AUDIT.DATE.TIME) = R.TEMP(INF.MLT.AUDIT.DATE.TIME)
********** MUST BE HASHED IN R10 RELEASE *******************
***###        CALL PM.CONTROL.DC
********** END OF REMARK ***********************************
    NEXT J

    ID.NEW = SAVE.ID.NEW

    MAT R.NEW = MAT R.TEMP

RETURN
*************************************************************************

*************************************************************************
UPD.NOINP.AMTS:

    IF R.NEW(INF.MLT.SIGN)<1,I>[1,1] = 'D' THEN
        R.NEW(INF.MLT.NUM.OF.DRS) += 1
        R.NEW(INF.MLT.TOT.LCY.DRS.AMT) += R.NEW(INF.MLT.AMOUNT.LCY)<1,I>
    END
    ELSE
        R.NEW(INF.MLT.NUM.OF.CRS) += 1
        R.NEW(INF.MLT.TOT.LCY.CRS.AMT) += R.NEW(INF.MLT.AMOUNT.LCY)<1,I>
    END

RETURN
*************************************************************************

*************************************************************************
UPD.EXCH.RATE:

    IF YCALC.LAMT NE "" THEN
        R.NEW(INF.MLT.AMOUNT.LCY)<1,I> = YCALC.LAMT
    END

    IF YCALC.FAMT NE "" THEN
        R.NEW(INF.MLT.AMOUNT.FCY)<1,I> = YCALC.FAMT
    END

    IF YCALC.RATE NE "" THEN
        R.NEW(INF.MLT.EXCHANGE.RATE)<1,I> = YCALC.RATE
    END

    IF R.NEW(INF.MLT.AMOUNT.LCY)<1,I> = 0 THEN
        R.NEW(INF.MLT.AMOUNT.LCY)<1,I> = ''
    END

    IF R.NEW(INF.MLT.AMOUNT.FCY)<1,I> = 0 THEN
        R.NEW(INF.MLT.AMOUNT.FCY)<1,I> = ''
    END

RETURN
*************************************************************************

*************************************************************************
DISP.FCY.OVR:

    IF YDIF.AMT NE "" AND YDIF.AMT NE 0 THEN
        IF RETURN.CODE<6> = "O" THEN
*Line [ 840 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            TEXT = "CONVERSION.DIFF":@FM:YDIF.AMT
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END

RETURN
*************************************************************************

*************************************************************************
DISP.EXP.OVR:

    EXP.DATE = R.NEW(INF.MLT.EXPOSURE.DATE)<1,I>
    IF EXP.DATE NE "" THEN
    END

RETURN
*************************************************************************
*########################### MAHMOUD 20/5/2012 #####################
USR.LEVEL.OVR:
    IF R.NEW(INF.MLT.TOT.LCY.DRS.AMT) GE R.NEW(INF.MLT.TOT.LCY.CRS.AMT) THEN
        TOT.AMT.CHK = R.NEW(INF.MLT.TOT.LCY.DRS.AMT)
    END ELSE
        TOT.AMT.CHK = R.NEW(INF.MLT.TOT.LCY.CRS.AMT)
    END
    CALL F.SCB.ATH.LEVEL(APPLICATION,TOT.AMT.CHK,WS.MSG.LEVEL)
    TEXT = WS.MSG.LEVEL
    CALL STORE.OVERRIDE(CURR.NO)

RETURN
*************************************************************************
CHQ.ACC.OVR:

    IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I> THEN
        SCB.AC.ID = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I>
        SCB.CHQ.NO= R.NEW(INF.MLT.CHEQUE.NUMBER)<1,I>
        SCB.MLT.LCY.AMT = R.NEW(INF.MLT.AMOUNT.LCY)<1,I>
*Line [ 896 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,SCB.AC.ID,SCB.AC.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
SCB.AC.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF R.NEW(INF.MLT.SIGN)<1,I>[1,1] = "D" THEN
            CALL F.SCB.CHK.DR.BANK.CHQ(SCB.AC.ID,SCB.CHQ.NO,SCB.MLT.LCY.AMT,WS.MSG)
            IF WS.MSG NE '' THEN
                TEXT = WS.MSG
                CALL STORE.OVERRIDE(CURR.NO)
            END
        END ELSE
            IF SCB.AC.CAT EQ '16151' THEN
                IF R.NEW(INF.MLT.CHEQUE.NUMBER)<1,I> EQ '' THEN
                    TEXT = "MISSING CHEQUE NUMBER FOR CHEQUE ACCOUNT"
                    CALL STORE.OVERRIDE(R.NEW(CURR.NO))
                END
            END
        END
    END
RETURN
*************************************************************************
*###########################    END OF ADD     #####################

*************************************************************************
DISP.EXH.OVR:

    IF YDIF.RATE NE "" THEN
*Line [ 901 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        TEXT = "XRATE.DIFF":@FM:YDIF.RATE
        CALL STORE.OVERRIDE(CURR.NO)
    END

RETURN
*************************************************************************

*************************************************************************
DISP.EURO.OVR:

    ACC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I>
    IF ACC THEN
        ETEXT = '' ; AUTO.PAY.ACCT = ''
*Line [ 915 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 941 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR("ACCOUNT":@FM:AC.AUTO.PAY.ACCT, ACC, AUTO.PAY.ACCT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AUTO.PAY.ACCT=R.ITSS.ACCOUNT<AC.AUTO.PAY.ACCT>
    END

    SUB.TXN = 1
    TXN.CODE = R.NEW(INF.MLT.TXN.CODE)<1,I>
    BEGIN CASE
        CASE R.EU.PARAMETER<EUP.SUBS.FOR.TXN.LIST> = '' OR R.EU.PARAMETER<EUP.SUBS.FOR.TXN.LIST = "YES"
            LOCATE TXN.CODE IN R.EU.PARAMETER<EUP.DC.AUT.PAY.TXNS,1> SETTING SUB.TXN ELSE SUB.TXN = ''
        CASE R.EU.PARAMETER<EUP.SUBS.FOR.TXN.LIST> = "NO"
            LOCATE TXN.CODE IN R.EU.PARAMETER<EUP.DC.AUT.PAY.TXNS,1> SETTING SUB.TXN THEN SUB.TXN = ''
    END CASE

    IF SUB.TXN THEN
        IF AUTO.PAY.ACCT THEN
*Line [ 930 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            TEXT = "ACCT.LINKED":@FM:AUTO.PAY.ACCT
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END

RETURN
*************************************************************************

*************************************************************************
CHECK.EXP.DATE:

    YDATE = R.NEW(INF.MLT.EXPOSURE.DATE)<1,I>
    GOSUB CHECK.FOR.WORKING.DAY
    IF ETEXT NE "" THEN
        AF = INF.MLT.EXPOSURE.DATE ; AV = I ; AS = I
        CALL STORE.END.ERROR
    END

RETURN
*************************************************************************

*************************************************************************
CHECK.FOR.WORKING.DAY:

*  YDAYTYPE = "" ; CALL AWD("", YDATE, YDAYTYPE)
*  IF ETEXT = "" THEN IF YDAYTYPE = "N" THEN
*     ETEXT = "HOLIDAY TABLE MISSING"
*  END

RETURN
*************************************************************************

*************************************************************************
CHECK.EXCH.RATE:

    IF R.NEW(INF.MLT.CURRENCY)<1,I> = LCCY THEN
        IF R.NEW(INF.MLT.EXCHANGE.RATE)<1,I> THEN
            AF = INF.MLT.EXCHANGE.RATE ; AV = I ; AS = 1
            ETEXT = 'NOT A FOREIGN CURRENCY TXN'
            CALL STORE.END.ERROR
        END
    END

RETURN
*************************************************************************

*************************************************************************
CHECK.VAL.DATE:

    IF R.NEW(INF.MLT.VALUE.DATE)<1,I> < R.DATES(EB.DAT.BACK.VALUE.MAXIMUM) THEN
        AF = INF.MLT.VALUE.DATE ; AV = I ; AS = 1
        ETEXT = 'DATE < MAXIMUM BACK VALUE'
        CALL STORE.END.ERROR
    END ELSE
        IF R.NEW(INF.MLT.VALUE.DATE)<1,I> > R.DATES(EB.DAT.FORW.VALUE.MAXIMUM) THEN
            AF = INF.MLT.VALUE.DATE ; AV = I ; AS = 1
            ETEXT = "DATE > MAXIMUM FORWARD VALUE"
            CALL STORE.END.ERROR
        END
    END


RETURN
*************************************************************************

*************************************************************************
CHECK.AMTS:

    IF R.NEW(INF.MLT.CURRENCY)<1, I> = LCCY THEN
        IF NOT(R.NEW(INF.MLT.AMOUNT.LCY)<1,I>) THEN
            AF = INF.MLT.AMOUNT.LCY ; AV = I ; AS = 1
            ETEXT = 'MISSING AMOUNT.LCY'
            CALL STORE.END.ERROR
        END
    END
    ELSE
        IF NOT(R.NEW(INF.MLT.AMOUNT.FCY)<1,I>) THEN
            AF = INF.MLT.AMOUNT.FCY ; AV = I ; AS = 1
            ETEXT = 'MISSING AMOUNT.FCY'
            CALL STORE.END.ERROR
        END
    END

RETURN
*************************************************************************

*************************************************************************
CHECK.CCY:

    CCY = R.NEW(INF.MLT.CURRENCY)<1, I>
    ACC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1, I>
    IF ACC & CCY THEN
        GOSUB GET.ACC.CCY
        IF AC.CCY # CCY THEN
            AF = INF.MLT.CURRENCY ; AV = I ; AS = 1
            ETEXT = 'INVALID CURRENCY'
            CALL STORE.END.ERROR
        END
    END

    GOSUB CHECK.HOLIDAY

RETURN
*************************************************************************
CHECK.PL.NUMBER:
****************
    PL.NO = '' ; IND.CAT = '' ; PROD.CAT = '' ; PL.CUST = ''
    PL.NO = R.NEW(INF.MLT.PL.CATEGORY)<1, I>
    IF PL.NO THEN
*Line [ 1040 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 1072 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR("CATEGORY":@FM:EB.CAT.SYSTEM.IND,PL.NO,IND.CAT)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,PL.NO,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
IND.CAT=R.ITSS.CATEGORY<EB.CAT.SYSTEM.IND>
        IF IND.CAT # 'PL' THEN
            AF = INF.MLT.PL.CATEGORY ; AV = I ; AS = 1
            ETEXT = 'MUST BE PL CATEGORY'
            CALL STORE.END.ERROR
        END
    END
RETURN
*************************************************************************
****MAHMOUD*****
CHECK.PROD.CAT:
****************
*Line [ 1053 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    MM = 1 ; CNTMM = DCOUNT(R.NEW(INF.MLT.TXN.CODE), @VM)
    PL.AAA   = '' ; SIGN.AAA = '' ; PL.PROD.CAT = '' ; CCC = ''
    ACCT.CCC = '' ; AC.PROD.CAT = '' ; ACCT.SIGN = '' ;
    PL.CCC   = '' ; CCC.PROD.CAT = '' ; SIGN.CCC    = '' ; PL.CUST.AAA = ''
    FOR AAA = 1 TO CNTMM
        PL.AAA      = R.NEW(INF.MLT.PL.CATEGORY)<1, AAA>
        SIGN.AAA    = R.NEW(INF.MLT.SIGN)<1, AAA>
        PL.CUST.AAA = R.NEW(INF.MLT.CUSTOMER.ID)<1, AAA>
        PL.PROD.AAA = R.NEW(INF.MLT.PROD.CATEGORY)<1,AAA>
        IF PL.AAA THEN
            PL.PROD.CAT = R.NEW(INF.MLT.PROD.CATEGORY)<1, AAA>
*Line [ 1065 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CCC = DCOUNT(R.NEW(INF.MLT.TXN.CODE), @VM)
            FOR XX1 = 1 TO CCC
                ACCT.CCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1, XX1>
                PL.CCC   = R.NEW(INF.MLT.PL.CATEGORY)<1, XX1>
                IF ACCT.CCC THEN
*Line [ 1108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.CCC,AC.PROD.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.CCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.PROD.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
                    ACCT.SIGN  = R.NEW(INF.MLT.SIGN)<1, XX1>
                    IF ACCT.SIGN NE SIGN.AAA THEN
                        IF (PL.AAA LT 60000 OR PL.AAA GT 69998) THEN
                            IF AC.PROD.CAT NE PL.PROD.CAT THEN
                                AF = INF.MLT.PROD.CATEGORY ; AV = AAA ; AS = 1
                                ETEXT = 'MUST BE SAME AS ACCOUNT CATEGORY'
                                CALL STORE.END.ERROR
                            END
                            IF NOT(NUM(ACCT.CCC)) THEN
                                AF = INF.MLT.ACCOUNT.NUMBER ; AV = XX1 ; AS = 1
                                ETEXT = 'MUST NOT BE INTERNAL ACCOUNT WITH PL'
                                CALL STORE.END.ERROR
                            END
                            IF PL.CUST.AAA EQ '' THEN
                                AF = INF.MLT.CUSTOMER.ID ; AV = AAA ; AS = 1
                                ETEXT = 'INPUT MISSING'
                                CALL STORE.END.ERROR
                            END
                            IF PL.PROD.AAA EQ '' THEN
                                AF = INF.MLT.PROD.CATEGORY ; AV = AAA ; AS = 1
                                ETEXT = 'INPUT MISSING'
                                CALL STORE.END.ERROR
                            END
                        END ELSE
                            IF NUM(ACCT.CCC) THEN
                                IF PL.CUST.AAA EQ '' THEN
                                    AF = INF.MLT.CUSTOMER.ID ; AV = AAA ; AS = 1
                                    ETEXT = 'INPUT MISSING'
                                    CALL STORE.END.ERROR
                                END
                                IF PL.PROD.AAA EQ '' THEN
                                    AF = INF.MLT.PROD.CATEGORY ; AV = AAA ; AS = 1
                                    ETEXT = 'INPUT MISSING'
                                    CALL STORE.END.ERROR
                                END
                            END
                        END
                    END
                END
                IF PL.CCC THEN
                    SIGN.CCC     = R.NEW(INF.MLT.SIGN)<1, XX1>
                    CCC.BROD.CAT = R.NEW(INF.MLT.PROD.CATEGORY)<1, XX1>
                    IF SIGN.CCC NE SIGN.AAA THEN
                        AF = INF.MLT.PL.CATEGORY ; AV = AAA ; AS = 1
                        ETEXT = 'MUST NOT BE DEBIT CREDIT PL CATEGORY'
                        CALL STORE.END.ERROR
                    END
                END
            NEXT XX1
        END
    NEXT AAA
RETURN
*************************************************************************
CHECK.HOLIDAY:

    IF R.NEW(INF.MLT.CURRENCY)<1, I> # LCCY THEN
        IF R.COMPANY(EB.COM.LOCAL.REGION) NE "" THEN
            HOL.REC.REGION = R.COMPANY(EB.COM.LOCAL.REGION)
        END ELSE HOL.REC.REGION = "00"
        CCY.F = R.NEW(INF.MLT.CURRENCY)<1,I>
        HOL.REC.KEY = CCY.F[1,2]:HOL.REC.REGION:TODAY[1,4]
        HOL.REC = ''          ;* PKAR20040127 ETEXT = ''
        F.HOLIDAY = ''
        CALL OPF("F.HOLIDAY", F.HOLIDAY)
        CALL F.READ("F.HOLIDAY", HOL.REC.KEY, HOL.REC, F.HOLIDAY,ETEXT)
        IF ETEXT NE "" THEN
            AF = INF.MLT.CURRENCY ; AV = I ; AS = 1
            ETEXT = "NO HOLIDAY RECORD FOR THIS CCY"
            CALL STORE.END.ERROR
        END

        YFAMT = R.NEW(INF.MLT.AMOUNT.FCY)<1,I>
        YRATE = R.NEW(INF.MLT.EXCHANGE.RATE)<1,I>
        YLAMT = R.NEW(INF.MLT.AMOUNT.LCY)<1,I>
        YMARKET = R.NEW(INF.MLT.CURRENCY.MARKET)<1,I>
        GOSUB CALLING.FCY.AMT.CONVERT
        IF ETEXT NE "" THEN
            AF = INF.MLT.CURRENCY ; AV = I ; AS = I
            CALL STORE.END.ERROR
        END ELSE
            YCALC.FAMT = YFAMT
            IF R.NEW(INF.MLT.EXCHANGE.RATE)<1,I> = "" THEN YCALC.RATE = YRATE
            IF R.NEW(INF.MLT.AMOUNT.LCY)<1,I> = "" THEN YCALC.LAMT = YLAMT
        END
    END


RETURN
*************************************************************************

*************************************************************************
CALLING.FCY.AMT.CONVERT:

    IF YFAMT AND YLAMT AND YRATE THEN
        TOLERANCE.TEST = 1
    END
    ELSE
        TOLERANCE.TEST = ''
    END
    CALL EXCHRATE(YMARKET,R.NEW(INF.MLT.CURRENCY)<1,I>,YFAMT,LCCY,YLAMT,
    BASE.CCY,YRATE,YDIF.AMT,X,RETURN.CODE)
    IF NOT(RETURN.CODE<1>) THEN
        YDIF.RATE = RETURN.CODE<2>
        IF TOLERANCE.TEST AND NOT(YDIF.RATE) THEN
            TEST.YRATE = '' ; BASE.CCY = '' ; RET.CODE = ''
            CALL EXCHRATE(YMARKET, R.NEW(INF.MLT.CURRENCY)<1,I>, YFAMT,LCCY,
            YLAMT,BASE.CCY,TEST.YRATE,YDIF.AMT,X,RET.CODE)
            IF NOT(RETURN.CODE<1>) THEN
                TOLERANCE = ((YRATE - TEST.YRATE)/TEST.YRATE)*100
                RATE.ALLOWANCE = ''
*Line [ 1182 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                CALL DBR("CURRENCY":@FM:EB.CUR.RATE.ALLOWANCE,
                R.NEW(INF.MLT.CURRENCY)<1,I>, RATE.ALLOWANCE)
                IF NOT(RATE.ALLOWANCE) THEN
                    RATE.ALLOWANCE = R.COMPANY(EB.COM.DEFAULT.RATE.ALLOW)
                END
                IF ABS(TOLERANCE) GT RATE.ALLOWANCE THEN
                    YDIF.RATE = OCONV(ICONV(TOLERANCE,'MD2'),'MD2')
                END
            END
        END
    END


RETURN
*************************************************************************

*************************************************************************
ADD.DATA:

    IF NOT(R.NEW(INF.MLT.CURRENCY.MARKET)<1, I>) THEN
        R.NEW(INF.MLT.CURRENCY.MARKET)<1, I> = '1'
    END

    IF NOT(R.NEW(INF.MLT.POSITION.TYPE)<1, I>) THEN
        R.NEW(INF.MLT.POSITION.TYPE)<1, I> = 'TR'
    END

    IF NOT(R.NEW(INF.MLT.VALUE.DATE)<1, I>) THEN
        R.NEW(INF.MLT.VALUE.DATE)<1, I> = TODAY
    END

RETURN
*************************************************************************

*************************************************************************
CHECK.TXN.AND.SIGN:

    TXN.CODE = R.NEW(INF.MLT.TXN.CODE)<1, I>
    SIGN = R.NEW(INF.MLT.SIGN)<1, I>
    GOSUB CHECK.TXN.TABLE
    IF ERR.MESS THEN
        AF = INF.MLT.TXN.CODE ; AV = I ; AS = 1
        ETEXT = ERR.MESS
        CALL STORE.END.ERROR
    END

RETURN
*************************************************************************

*************************************************************************
CHECK.ACC.AND.PL:
    IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1, I> & R.NEW(INF.MLT.PL.CATEGORY)<1,I> THEN
        AF = INF.MLT.ACCOUNT.NUMBER
        AV = I ; AS = 1
        ETEXT = 'EITHER ACCOUNT OR PL'
        CALL STORE.END.ERROR
    END

    IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1, I> & R.NEW(INF.MLT.PROD.CATEGORY)<1,I> THEN
        AF = INF.MLT.PROD.CATEGORY
        AV = I ; AS = 1
        ETEXT = 'NOT VALID WITH ACCOUNT INPUT'
        CALL STORE.END.ERROR
    END

    IF NOT(R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I>) & NOT(R.NEW(INF.MLT.PL.CATEGORY)<1,I>) THEN
        AF = INF.MLT.ACCOUNT.NUMBER ; AV = I ; AS = 1
        ETEXT = 'ENTER ACCOUNT OR PL'
        CALL STORE.END.ERROR
    END


RETURN
*************************************************************************

*************************************************************************
AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************
CHECK.DELETE:
*
* PK - 03/04/03 START
*
    STMTS = ''
    IF NOT(V$ERROR) THEN
        G.A = 1 ; ERR.MESS = ''
*Line [ 1272 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        G = DCOUNT(R.NEW(INF.MLT.TXN.CODE), @VM)
        LOOP WHILE G => G.A
            GOSUB FILL.STMT.ENTRY
            G = G - 1
        REPEAT
        YTYPE = 'DEL'
        CALL EB.ACCOUNTING("MLT", YTYPE, STMTS, '')
        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END
*
* PK - 03/04/03 END
*
RETURN
*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************

BEFORE.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:


RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
        CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE
*
* If there are any OVERRIDES a call to EXCEPTION.LOG should be made
*
* IF R.NEW(V-9) THEN
*    EXCEP.CODE = "110" ; EXCEP.MESSAGE = "OVERRIDE CONDITION"
*    GOSUB EXCEPTION.MESSAGE
* END
*

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('VCHQR',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

RETURN

*************************************************************************
*
EXCEPTION.MESSAGE:
*
    CALL EXCEPTION.LOG("U",
    APP.CODE,
    APPLICATION,
    APPLICATION,
    EXCEP.CODE,
    "",
    FULL.FNAME,
    ID.NEW,
    R.NEW(V-7),
    EXCEP.MESSAGE,
    ACCT.OFFICER)
*
RETURN

*************************************************************************

INITIALISE:

    APP.CODE = ""   ;* Set to product code ; e.g FT, FX
    ACCT.OFFICER = ""         ;* Used in call to EXCEPTION. Should be relevant A/O
    EXCEP.CODE = ""
    ERR.MESS = ''

    GOSUB MY.INIT


RETURN

*************************************************************************

*************************************************************************
MY.INIT:

    EU.INSTALLED = ''
    R.EU.PARAMETER = ''
    LOCATE 'EU' IN R.COMPANY(EB.COM.APPLICATIONS)<1,1> SETTING EU.INSTALLED ELSE
        EU.INSTALLED = ''
    END
    IF EU.INSTALLED THEN
        F.EU.PARAMETER = ''
        CALL OPF("F.EU.PARAMETER", F.EU.PARAMETER)
        CALL F.READ("F.EU.PARAMETER",ID.COMPANY,R.EU.PARAMETER,
        F.EU.PARAMETER, READ.ERR)
    END

    LOCATE 'PM' IN R.COMPANY(EB.COM.APPLICATIONS)<1,1> SETTING POSN ELSE
        POSN = 0
    END

    IF POSN THEN
        F.PM.PARAMETER = ''
        PM.PARAM.REC = ''
        ER = ''
        CALL OPF("F.PM.PARAMETER", F.PM.PARAMETER)
        CALL F.READ("F.PM.PARAMETER","SYSTEM",PM.PARAM.REC,
        F.PM.PARAMETER, ER)
        IF ER THEN
            TEXT = 'MISSING PM.PARAMETER RECORD'
            CALL FATAL.ERROR(APPLICATION)
        END
    END

    IF POSN THEN
        PM.CALL = 1
        CALL PM.SETUP.PARAM
    END ELSE
        PM.CALL = 0
    END



RETURN
*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *


    MAT F = "" ; MAT N = "" ; MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = "" ; ID.CONCATFILE = ""

    ID.F = "ID" ; ID.N = "16" ; ID.T = "A"

* The ID must be IN-JULIAN DATE-SERIAL NO (IN0109800001)
* The AUTO.ID.START and COMPANY applications must be updated
* The EB.SYSTEM.ID must be updated also.
* Future use : The 'R' must be worked.

    Z=0
*
    Z+=1 ; F(Z) = "XX<ACCOUNT.NUMBER" ; N(Z) = "16..C" ; T(Z) = "ANT"
*Line [ 1445 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.SHORT.TITLE
    Z+=1 ; F(Z) = "XX-PL.CATEGORY" ; N(Z) = "6..C" ; T(Z) = "A"
*Line [ 1448 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CATEGORY":@FM:EB.CAT.DESCRIPTION
    Z+=1 ; F(Z) = "XX-GL.NUMBER" ; N(Z) = "20" ; T(Z) = "A"
    Z+=1 ; F(Z) = "XX-DISC.PERCENT" ; N(Z) = "20" ; T(Z) = "AMT"
**?    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX-SIGN" ; N(Z) = "20.1" ; T(Z) = ""
    T(Z)<2> = "DEBIT_CREDIT"
    Z+=1 ; F(Z) = "XX-TXN.CODE" ; N(Z) = "4.1.C" ; T(Z) = ""
*Line [ 1456 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "TRANSACTION":@FM:AC.TRA.NARRATIVE
    Z+=1 ; F(Z) = "XX-CURRENCY.MARKET" ; N(Z) = "2" ; T(Z) = ""
*Line [ 1459 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY.MARKET":@FM:EB.CMA.DESCRIPTION
    Z+=1 ; F(Z) = "XX-CURRENCY" ; N(Z) = "3.1.C" ; T(Z) = "CCY"
*Line [ 1462 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:EB.CUR.CCY.NAME
    Z+=1 ; F(Z) = "XX-AMOUNT.LCY" ; N(Z) = "19" ; T(Z) = "AMT"
    Z+=1 ; F(Z) = "XX-RESERVED.15" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX-EXCHANGE.RATE" ; N(Z) = "14" ; T(Z) = "R"
    Z+=1 ; F(Z) = "XX-AMOUNT.FCY" ; N(Z) = "19" ; T(Z) = "AMT"
    Z+=1 ; F(Z) = "XX-VALUE.DATE" ; N(Z) = "11" ; T(Z) = "D"
    Z+=1 ; F(Z) = "XX-EXPOSURE.DATE" ; N(Z) = "11" ; T(Z) = "D"
    Z+=1 ; F(Z) = "XX-RESERVED.14" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX-THEIR.REFERENCE" ; N(Z) = "35" ; T(Z) = "A"
    Z+=1 ; F(Z) = "XX-CUSTOMER.ID" ; N(Z) = "10" ; T(Z) = "CUS"
*Line [ 1475 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.SHORT.NAME
    Z+=1 ; F(Z) = "XX-ACCOUNT.OFFICER" ; N(Z) = "4" ; T(Z) = ""
*Line [ 1478 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME
    Z+=1 ; F(Z) = "XX-RESERVED.13" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX-PROD.CATEGORY" ; N(Z) = "6" ; T(Z) = ""
*** CHECKFILE(Z) = "CATEGORY":EB.CAT.DESCRIPTION
*Line [ 1484 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CATEGORY":@FM:EB.CAT.DESCRIPTION
    Z+=1 ; F(Z) = "XX-OUR.REFERENCE" ; N(Z) = "25" ; T(Z) = "A"
    Z+=1 ; F(Z) = "XX-POSITION.TYPE" ; N(Z) = "10" ; T(Z) = "A"
    Z+=1 ; F(Z) = "XX-RESERVED.12" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX-DEPART.CODE" ; N(Z) = "4" ; T(Z) = ""
*Line [ 1491 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME
    Z+=1 ; F(Z) = "XX-DEALER.DESK" ; N(Z) = "10" ; T(Z) = ""
*Line [ 1494 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEALER.DESK":@FM:FX.DD.DESCRIPTION
    Z+=1 ; F(Z) = "XX-BANK.SORT.CODE" ; N(Z) = "20" ; T(Z) = "A"
    Z+=1 ; F(Z) = "XX-RESERVED.11" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX>CHEQUE.NUMBER" ; N(Z) = "14" ; T(Z) = "A"
    Z+=1 ; F(Z) = "NUM.OF.CRS" ; N(Z) = "10" ; T(Z) = ""
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "NUM.OF.DRS" ; N(Z) = "10" ; T(Z) = ""
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "TOT.LCY.DRS.AMT" ; N(Z) = "19" ; T(Z) = "AMT"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "TOT.LCY.CRS.AMT" ; N(Z) = "19" ; T(Z) = "AMT"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "BILL.NO"         ; N(Z) = "20" ; T(Z) = "A"
    Z+=1 ; F(Z) = "RESERVED.17" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"

    Z+=1 ; F(Z) = "XX.LOCAL.REF" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.20" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.21" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.10" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.09" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.07" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.06" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.05" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.04" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "RESERVED.03" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX.STMT.NO" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
    Z+=1 ; F(Z) = "XX.OVERRIDE" ; N(Z) = "35" ; T(Z) = "A"
    T(Z)<3> = "NOINPUT"
*
    V = Z + 9

RETURN

*************************************************************************

END
