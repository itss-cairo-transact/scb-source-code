* @ValidationCode : MjozMDc4NTY3MzI6Q3AxMjUyOjE2NDE3MTg3MTczNDI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 10:58:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
**********ABEER**********
*-----------------------------------------------------------------------------
* <Rating>-54</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LG.AMAMTDATE.2232

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY.PARAM
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS



    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        IF MYCODE='2232' THEN
            GOSUB INITIATE
*Line [ 51 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 52 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            GOSUB PRINT.HEAD
            GOSUB BODY
        END
    END ELSE
        IF ID.NEW EQ '' THEN
            GOSUB INITIATE
*Line [ 59 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 61 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            IF MYCODE='2232' THEN
                GOSUB PRINT.HEAD
                GOSUB BODY
            END  ELSE
                MYID=MYCODE:'.':MYTYPE
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
OPER.CODE=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.OPERATION.CODE>
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.2232'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN   YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN   CALL TXTINP(YTEXT, 8, 22, "12", "A")

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    IF ID.NEW THEN
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>


    APPROVE=LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL1 =LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL2 =LOCAL.REF<1,LDLR.GUARRANTOR,2>

    ADDR1 =LOCAL.REF<1,LDLR.FREE.TEXT,1>
    ADDR2 =LOCAL.REF<1,LDLR.FREE.TEXT,2>
    ADDR3 =LOCAL.REF<1,LDLR.FREE.TEXT,3>

    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
*   DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*Line [ 124 ] change LDLR.OLD.LG.NO to LDLR.OLD.NO - ITSS - R21 Upgrade - 2021-12-23
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT =R.LD<LD.AMOUNT>
    IN.AMOUNT=LG.AMT:'.00'

    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
    CUR=R.LD<LD.CURRENCY>
*Line [ 152 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 159 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CUR,CRR.DESC)
F.ITSS.CURRENCY.PARAM = 'F.CURRENCY.PARAM'
FN.F.ITSS.CURRENCY.PARAM = ''
CALL OPF(F.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM)
CALL F.READ(F.ITSS.CURRENCY.PARAM,CUR,R.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM,ERROR.CURRENCY.PARAM)
CRR.DESC=R.ITSS.CURRENCY.PARAM<EB.CUP.CCY.NAME>

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 168 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    SEND.REC=LOCAL.REF<1,LDLR.SENDING.REF>
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT************************************
    IDHIS=COMI:";":"..."
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ": IDHIS
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    IF KEY.LIST THEN
    OLD.ID= KEY.LIST<SELECTED>
    ID=FIELD(OLD.ID,';',1)
***************************************************************************
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
    CALL F.READ(FN.LG.HIS,OLD.ID,R.LG.HIS,F.LG.HIS,E)
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT=R.LG.HIS<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT= R.OLD(LD.AMOUNT)
        END
    END
    CALL F.RELEASE(FN.LG.HIS,OLD.ID,F.LG.HIS)
***************GET AMOUNT INC OR DEC************************************
*   FN.LG='F.LD.LOANS.AND.DEPOSITS';F.LG='';R.LG='';F.LG=''
*   CALL OPF(FN.LG,F.LG)
*   CALL F.READ(FN.LG,ID, R.LG, F.LG ,E)
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END
    CALL F.RELEASE(FN.LG,ID,F.LG)
**********************************************************************************
    AMT.DEC=OLD.AMOUNT-NEW.AMOUNT
    AMT.INC=NEW.AMOUNT-OLD.AMOUNT
*   END
**********************************************************************
RETURN
*===============================================================
PRINT.HEAD:
*  MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*  MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*Line [ 222 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
OPER.CODE=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.OPERATION.CODE>
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":OPER.CODE:"*"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
*Line [ 232 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
*Line [ 242 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"���� �������� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(38):"�":"*":EXT.NO:"*"
    HEADING PR.HD

RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���   : ": LG.NO :"-":SEND.REC:" (":TYPE.NAME:") "
    PRINT
    PRINT SPACE(20): "�������������   : ":" **":OLD.AMOUNT:"**":" ":CRR
    PRINT
    PRINT SPACE(20): "������� � ����� : ": SUPPL1
    PRINT SPACE(36): ":":SUPPL2
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(8):"������ ������� " :"������":" ":  CORR.DATE :"� ���� ��� �������"
    PRINT
    PRINT SPACE(2):"������� �����  ���  ����   ������ ��� ���� ����  ������ ������ ������"
    PRINT
    PRINT SPACE(2):"�����" :  "  *":AMT.DEC:"* " :CRR.DESC :" ����� ���� ������� ����� ":" *":NEW.AMOUNT:"* ":CRR.DESC
    PRINT
    PRINT;PRINT
    PRINT SPACE(2):"���� ��� ��� ������������� ��������� �� ����� ����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT
*Line [ 250 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 252 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR.DESC:" ":"�� ��� ."
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
        PRINT

    NEXT I
    PRINT
    PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
    PRINT;PRINT
    PRINT SPACE(8):"����� ����� ��� ���� ������ ������� �� ���� ������ ��� �����"
    PRINT
    PRINT SPACE(2):"��� ������ �����  ��� ������� ."
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT ;PRINT
    PRINT ; PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":XX
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

RETURN
*==============================================================
