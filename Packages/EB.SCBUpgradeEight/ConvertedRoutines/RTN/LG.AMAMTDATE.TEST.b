* @ValidationCode : MjoxMDM2MzgzODc5OkNwMTI1MjoxNjQxMzY5MjMxMTg2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 05 Jan 2022 09:53:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
SUBROUTINE LG.AMAMTDATE.TEST
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY.PARAM
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SCB.LG.PARMS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SCB.LG.CHARGE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_LD.LOCAL.REFS
*--------------------------------------------------
    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        CUST   = R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>
*        MYCODE = '1232'
*        CUST   = 'CUS'

        IF MYCODE='1232' AND CUST='CUS' THEN
            GOSUB INITIATE
*Line [ 51 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            FOR II = 1 TO 2
                GOSUB PRINT.HEAD
                GOSUB BODY
            NEXT II
        END
    END  ELSE
        IF ID.NEW EQ '' THEN
            GOSUB INITIATE
*Line [ 61 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
*           MYCODE = '1232'
*           CUST   = 'CUS'
            IF MYCODE='1232' AND CUST='CUS' THEN
                FOR II = 1 TO 2
                    GOSUB PRINT.HEAD
                    GOSUB BODY
                NEXT II
            END   ELSE
                MYID=MYCODE:'.':MYTYPE
                OPER.CODE=MYID
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.1232.BENF'
    CALL PRINTER.ON(REPORT.ID,'')
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN  YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN  CALL TXTINP(YTEXT, 8, 22, "12", "A")

    IF ID.NEW THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)

            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>

    MYCODE     = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE     = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1      = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2      = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1      = LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2      = LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO   = LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CUST       = LOCAL.REF<1,LDLR.APPROVAL>
    CORR.NO    = R.LD<LD.CUSTOMER.REF>
    CURRNO     = R.LD<LD.CURR.NO>
    CURRNO1    = CURRNO-1
    AMT.INC    = R.LD<LD.AMOUNT.INCREASE>
    NEW.AMOUNT = R.LD<LD.AMOUNT>
    OLD.AMOUNT = NEW.AMOUNT - AMT.INC
    DATE.CORR  = LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE  = DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.ISSUE = LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE = DATE.ISSUE[7,2]:"/":DATE.ISSUE[5,2]:"/":DATE.ISSUE[1,4]
    DATE.FIN   = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    IN.DATE    = DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]

    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>

    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
**********MODIFIED ON 26/3/2012

    THIRD.NO = LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CUST.ID=R.LD<LD.CUSTOMER.ID>
    IF THIRD.NO NE CUST.ID THEN
        CUST.AC = THIRD.NO
    END ELSE
        CUST.AC =CUST.AC
    END
    CUS.AC =   R.LD<LD.CUSTOMER.ID>
*Line [ 146 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUS.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    CUS.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    CUS.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*************
*Line [ 157 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END

    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>

*Line [ 179 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 186 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CUR,CRR.DESC)
F.ITSS.CURRENCY.PARAM = 'F.CURRENCY.PARAM'
FN.F.ITSS.CURRENCY.PARAM = ''
CALL OPF(F.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM)
CALL F.READ(F.ITSS.CURRENCY.PARAM,CUR,R.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM,ERROR.CURRENCY.PARAM)
CRR.DESC=R.ITSS.CURRENCY.PARAM<EB.CUP.CCY.NAME>
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 194 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT AND FIN MAT DATE************************************
    IDHIS=ID.NEW:";":CURRNO1
    ID=IDHIS
    OLD.ID=IDHIS
***************************************************************************
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
    CALL F.READ(FN.LG.HIS,IDHIS, R.LG.HIS, F.LG.HIS ,ETEXT)
    IF ID.NEW EQ '' THEN
        LOCAL.REF=R.LG.HIS<LD.LOCAL.REF>
        OLD.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
        FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
    END ELSE
        IF ID.NEW THEN
            OLD.FIN=R.OLD(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
        END
    END
** IF ID.NEW EQ '' THEN
** OLD.AMOUNT=R.LG.HIS<LD.AMOUNT>
**END ELSE
** IF ID.NEW THEN
**  OLD.AMOUNT=R.OLD(LD.AMOUNT)
** END
** END

* CALL F.RELEASE(FN.LG.HIS,OLD.ID,F.LG.HIS)
***************GET AMOUNT INC OR DEC************************************
*    FN.LG='F.LD.LOANS.AND.DEPOSITS';F.LG='';R.LG='';F.LG=''
*    CALL OPF(FN.LG,F.LG)
*    CALL F.READ(FN.LG,ID, R.LG, F.LG ,E)
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END
* CALL F.RELEASE(FN.LG,ID,F.LG)
**********************************************************************************
    AMT.DEC=OLD.AMOUNT-NEW.AMOUNT
**** AMT.INC=NEW.AMOUNT-OLD.AMOUNT
    AMT.INC=R.LD<LD.AMOUNT.INCREASE>
    OLD.AMOUNT = NEW.AMOUNT - AMT.INC
*** AMT.INC=30.00
*END
**********************************************************************

RETURN
*===============================================================
PRINT.HEAD:
* MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*  MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

** CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    OPER.CODE=MYID
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":OPER.CODE:"*"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
*Line [ 264 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************

*Line [ 275 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):"�������":":":XX
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD

RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���    : ":LG.NO:" )":TYPE.NAME:"("
    PRINT
    PRINT SPACE(20): "�������������    : ": " **":OLD.AMOUNT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
*    PRINT SPACE(20):"������ ������    : ":ISSUE.DATE
*   PRINT
    PRINT SPACE(20): "������� � �����  : ":THIRD.NAME
*  PRINT
* PRINT SPACE(20): "���� ������� ��� : ":FIN.OLD
*  END
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(6):"�������� ��� ���� ������ ������ ������ ����� ��� ��� �������"
    PRINT
    PRINT SPACE(2):"����� ��� ���� ������ ��� ���� ���� ������ ������ ������ �����":"*":AMT.INC:"*" :CRR.DESC
    PRINT
    PRINT SPACE(2):"����� ����������� �����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT;PRINT
    PRINT SPACE(2):"���� ��� ��� �������� ����� ���� ������ ������� �� �����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT
    IN.AMOUNT=NEW.AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 291 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 293 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR.DESC
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
    PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
    PRINT;PRINT
    PRINT SPACE(6):"����� ����� ��� ���� ������ ������� �� ���� ������ ��� ����� ���"
    PRINT
    PRINT SPACE(2):" ������ ����� ��� ������� . "
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT ;PRINT
    IF II = '1' THEN
************MODIFIED ON 26/3/2012
        PRINT SPACE(5): "���� ��� : ":CUS.NAME
        PRINT SPACE(15):CUS.ADDR1
******************************
        PRINT SPACE(15):"����� ������ ��������"
    END
    PRINT ; PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

RETURN
*==========================================================
