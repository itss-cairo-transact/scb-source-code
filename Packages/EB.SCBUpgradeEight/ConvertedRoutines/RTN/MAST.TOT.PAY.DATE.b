* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>2985</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAST.TOT.PAY.DATE
**WRITTEN BY NESSREEN AHMED 04/10/2008

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

*A REPORT TO PRINT THE TOTAL USAGES TO BE DEBITED FROM CUSTOMERS' VISA ACCOUNT

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 51 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 52 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='MAST.TOT.PAY.DATE'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    RETURN
*===============================================================
CALLDB:

**   YTEXT = "Enter the Date : "
**   CALL TXTINP(YTEXT, 8, 22, "12", "A")

    FN.TELLER = 'F.TELLER$HIS' ; F.TELLER = '' ; R.TELLER = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.TELLER,F.TELLER)

    TOT.TT = '' ; TOT.FT = '' ; TOT.AMT.ALL = ''
*=============TELLER SELECTION==========================================*
*****UPDATED BY NESSREEN AHMED 24/03/2009**********************
**  T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 38 AND VALUE.DATE.1 EQ ":COMI
    T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 38 AND RECORD.STATUS EQ MAT AND AUTH.DATE EQ ":COMI :" BY CO.CODE "
*****************************************************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    TEXT = '��� ��������� �������=':SELECTED ; CALL REM
    XX= '' ; ZZ = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            VISA.NO = '' ; CUST.NAME = '' ; TRAN.DATE = '' ; TRANS.AMT = ''

            KEY.TO.USE = KEY.LIST<I>
            CALL OPF(FN.TELLER,F.TELLER)
            CALL F.READ(FN.TELLER,  KEY.TO.USE, R.TELLER, F.TELLER, E1)

            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,TTLR.VISA.NUMBER>
            ACCT = R.TELLER<TT.TE.ACCOUNT.1>
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACCT , CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>

            FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)
            CUST.NAME = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            TRAN.DATE = R.TELLER<TT.TE.VALUE.DATE.1>
            TRANS.AMT = R.TELLER<TT.TE.NET.AMOUNT>
***********************************************************
*****UPDATED BY NESSREEN AHMED ON 26/10/2008***************
            TOT.TT = TOT.TT + TRANS.AMT
***********************************************************
            COMP.CO   = R.TELLER<TT.TE.CO.CODE>

            XX<1,ZZ>[1,16]= VISA.NO
            XX<1,ZZ>[20,16]= ACCT
            XX<1,ZZ>[40,35]= CUST.NAME
            XX<1,ZZ>[80,8]= TRAN.DATE
            XX<1,ZZ>[95,8]= TRANS.AMT
            XX<1,ZZ>[110,10] = COMP.CO
            XX<1,ZZ>[125,5] = '����'
            PRINT XX<1,ZZ>
            PRINT STR('-',132)
            ZZ=ZZ+1
            XX = ''
***********************************************************
        NEXT I
    END
*========FT SELECTION=======================================
*******UPDATED BY NESSREEN AHMED 24/03/2009***************************************
**   N.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ ACVM AND CREDIT.VALUE.DATE EQ ":COMI
*******UPDATED BY NESSREEN 30/03/2009****************************
**   N.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ ACVM AND AUTH.DATE EQ ":COMI
     N.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ ACVM AND RECORD.STATUS EQ MAT AND AUTH.DATE EQ ":COMI : " BY CO.CODE"
*******************************************************************
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    TEXT = '��� ��������� ��������=':SELECTED.N ; CALL REM

    FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = '' ; RETRY3= '' ; E3 = ''
    CALL OPF(FN.FT,F.FT)
    IF SELECTED.N THEN
        FOR NN = 1 TO SELECTED.N
            VISA.NO.FT = '' ; CUST.NAME.FT = '' ; TRAN.DATE.FT = '' ; TRANS.AMT.FT = ''
            FT.ID = KEY.LIST.N<NN>
            CALL F.READ(FN.FT,FT.ID, R.FT, F.FT , EERE3)
            VISA.NO.FT = R.FT<FT.LOCAL.REF,FTLR.VISA.NO>
            CUST.FT = R.FT<FT.DEBIT.CUSTOMER>

            FN.CUSTOMER.FT = 'FBNK.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
            CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
            CALL F.READ(FN.CUSTOMER.FT, CUST.FT, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)
            CUST.NAME.FT = R.CUSTOMER.FT<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            ACCT.FT = R.FT<FT.DEBIT.ACCT.NO>
            TRAN.DATE.FT = R.FT<FT.DEBIT.VALUE.DATE>
            TRANS.AMT.FT = R.FT<FT.DEBIT.AMOUNT>
*******UPDATED ON 26/10/2008********************************
            TOT.FT = TOT.FT + TRANS.AMT.FT
************************************************************
            COMP.CO.FT = R.FT<FT.CO.CODE>

            XX<1,ZZ>[1,16]= VISA.NO.FT
            XX<1,ZZ>[20,16]= ACCT.FT
            XX<1,ZZ>[40,35]= CUST.NAME.FT
            XX<1,ZZ>[80,8]= TRAN.DATE.FT
            XX<1,ZZ>[95,8]= TRANS.AMT.FT
            XX<1,ZZ>[110,10]= COMP.CO.FT
            XX<1,ZZ>[125,5]= '�����'

            PRINT XX<1,ZZ>
            PRINT STR('-',132)
            ZZ=ZZ+1
            XX = ''

        NEXT NN
    END
*===========================================================
***UPDATED ON 26/10/2008************************************
    TOT.TRN.ALL = SELECTED + SELECTED.N
    TOT.AMT.ALL = TOT.TT + TOT.FT
    PRINT
    PRINT
    XX<1,ZZ>[1,16]= "������ ��� �������"
    XX<1,ZZ>[25,16]= TOT.TRN.ALL
    XX<1,ZZ>[90,8]= "������ ������"
    XX<1,ZZ>[110,8]= TOT.AMT.ALL
    PRINT XX<1,ZZ>
    ZZ=ZZ+1
    XX = ''

************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 203 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TR.DATY = COMI
    T.TR.DAY = TR.DATY[7,2]:'/':TR.DATY[5,2]:"/":TR.DATY[1,4]

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"������� ���������� ������ ������ ������� �� ���" :SPACE(1):T.TR.DAY
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ���������"::SPACE(10):"���� ������":SPACE(10):"����� ��������":SPACE(25):"����� ������":SPACE(10):"���� ������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(9):STR('_',12):SPACE(8):STR('_',15):SPACE(25):STR('_',12):SPACE(8):STR('_',10)
    HEADING PR.HD
    RETURN
*==============================================================

END
