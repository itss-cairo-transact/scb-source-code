* @ValidationCode : MjotNTc1OTA5NDI4OkNwMTI1MjoxNjQwNzA4OTU4NjQ1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:29:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE GET.TITLE(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE  I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.TITLE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    TEXT = ARG; CALL REM
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,ARG,LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ARG,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    TIT = LOCAL<1,CULR.TITLE>
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR("SCB.CUS.TITLE":@FM:SCB.TIT.DESCRIPTION.TITLE,TIT,TITLE)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TIT,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
    ARG = TITLE

RETURN
END
