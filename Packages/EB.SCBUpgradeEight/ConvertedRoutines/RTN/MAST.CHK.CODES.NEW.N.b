* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1287</Rating>
*-----------------------------------------------------------------------------
***NESSREEN AHMED 23/10/2013********

    SUBROUTINE MAST.CHK.CODES.NEW.N

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.NEW
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.TRN.N
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CURR.CODES

    Path = "NESRO/mstr/MASTTR"
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    TEXT = Path ; CALL REM
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
    END ELSE
        MU = MT-1
    END
    YYDD = YYYY:MU
    EOF = ''
    LOOP WHILE NOT(EOF)

        CARD.NO = '' ;REASON.COD = ''; REASON.COD.TR = '' ; BANK.ACC = '' ; BANK.ACC.TR = '' ; ORG.MSGE = '' ; ORG.MSGE.TR = ''
        MSGE.TYPE = '' ; PROC.COD = '' ; BILL.CURR = '' ; BILL.AMT = '' ; BILL.AMT.TR = '' ; BILL.AMT.NN = '' ; BILL.AMT.1 = '' ; BILL.AMT.FMT = ''
        DB.CR = '' ; POST.DATE = '' ; PURCH.DATE = '' ; TRANS.CURR.GL = ''
        MSGE.DESC = '' ; MERCH.CITY = '' ; MERCH.COUNTRY = ''
        TRANS.AMT = '' ; TRANS.AMT.1 = '' ; TRANS.AMT.FMT = ''

        READSEQ Line FROM MyPath THEN
            CARD.NO           = Line[1,16]
*TEXT = 'CARD.NO=':CARD.NO ; CALL REM
            T.SEL =  "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""

            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
                    KEY.TO.USE = KEY.LIST<I>
                    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                    CALL F.READ(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                    LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                    CUST.DEPT = LOCAL.REF<1,LRCI.BRANCH.NO>
                    BANK.ACC =  R.CARD.ISSUE<CARD.IS.ACCOUNT>
                    CUST.NAME = R.CARD.ISSUE<CARD.IS.NAME>
                NEXT I
                REASON.COD        = Line[34,4]
                REASON.COD.TR =  TRIM(REASON.COD, "", "D")
                LENCOD = LEN(REASON.COD.TR)
                IF LENCOD = 0 THEN
                    REASON.COD.TR = "NA"
                END
                ORG.MSGE          = Line[70,5]
                ORG.MSGE.TR =  TRIM(ORG.MSGE, " ", "T")
                MSGE.TYPE         = Line[76,4]
                PROC.COD          = Line[81,6]
                BILL.CURR         = Line[88,3]
*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, BILL.CURR , BILL.CURR.GL)
F.ITSS.SCB.VISA.CURR.CODES = 'F.SCB.VISA.CURR.CODES'
FN.F.ITSS.SCB.VISA.CURR.CODES = ''
CALL OPF(F.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES)
CALL F.READ(F.ITSS.SCB.VISA.CURR.CODES,BILL.CURR,R.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES,ERROR.SCB.VISA.CURR.CODES)
BILL.CURR.GL=R.ITSS.SCB.VISA.CURR.CODES<SCB.CURR.CURRENCY>
                BILL.AMT.NN       = Line[94,16]
                BILL.AMT.1 = TRIM(BILL.AMT.NN, "0", "L")
                BILL.AMT.FMT = BILL.AMT.1/100
                DB.CR             = Line[111,2]
                POST.DATE         = Line[114,8]
                PURCH.DATE        = Line[123,8]
                TRANS.CURR        = Line[132,3]
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, TRANS.CURR , TRANS.CURR.GL)
F.ITSS.SCB.VISA.CURR.CODES = 'F.SCB.VISA.CURR.CODES'
FN.F.ITSS.SCB.VISA.CURR.CODES = ''
CALL OPF(F.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES)
CALL F.READ(F.ITSS.SCB.VISA.CURR.CODES,TRANS.CURR,R.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES,ERROR.SCB.VISA.CURR.CODES)
TRANS.CURR.GL=R.ITSS.SCB.VISA.CURR.CODES<SCB.CURR.CURRENCY>
                TRANS.AMT      = Line[138,16]
                TRANS.AMT.1 = TRIM(TRANS.AMT, "0", "L")
                TRANS.AMT.FMT = TRANS.AMT.1/100
                MSGE.DESC         = Line[188,60]
                CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MSGE.DESC
                MERCH.CITY        = Line[249,60]
                CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MERCH.CITY
                MERCH.COUNTRY     = Line[310,2]

                ID.KEY = CARD.NO:YYDD

                ORG.MSG.CO = '' ; MSG.TYPE.CO = '' ; PROC.CODE.CO = '' ; DAILY.COMB = '' ; CODES.COMB = ''  ; DB.CR.CO = '' ;   REASON.CO.CO = ''
                ORG.MSG.CODE = '' ; MSG.TYPE.CODE = '' ; PROC.CODE.CODE = '' ; DB.CR.CODE = '' ; REASON.CO.CODE = '' ; CODE.TO.USE = ''

                ORG.MSG.CO   = ORG.MSGE.TR
                MSG.TYPE.CO = MSGE.TYPE
                PROC.CODE.CO = PROC.COD
                DB.CR.CO = DB.CR
                REASON.CO.CO=  REASON.COD.TR
                DAILY.COMB = ORG.MSG.CO:MSG.TYPE.CO:PROC.CODE.CO:DB.CR.CO:REASON.CO.CO

                N.SEL =  "SELECT F.SCB.VISA.CODES.NEW WITH ORG.MSG.TYPE EQ ":ORG.MSG.CO:" AND MSG.TYPE EQ ":MSG.TYPE.CO:" AND PROCESS.CODE EQ ":PROC.CODE.CO:" AND FLAG EQ ":DB.CR.CO:" AND REASON.CODE EQ ":REASON.CO.CO
                KEY.LIST.2=""
                SELECTED.2=""
                ER.MSG.2=""

                CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
                F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.CODES.NEW' ; R.VISA.CODE = '' ; E3 = '' ; RETRY3 = ''
                CALL OPF(FN.VISA.CODE,F.VISA.CODE)
                IF NOT(SELECTED.2) THEN
                    IF DAILY.COMB # "PAYMT0220230000CR0001" AND DAILY.COMB # "PAYMT0420230000DB0001" AND DAILY.COMB # "PAYMTPMNT230000CRNA" AND DAILY.COMB # "PAYMTPMNT230000DBNA" THEN TEXT = 'NEW CODE=':DAILY.COMB ; CALL REM
                END ELSE
                    FOR RR = 1 TO SELECTED.2
                        CALL F.READ(FN.VISA.CODE, KEY.LIST.2<RR>, R.VISA.CODE, F.VISA.CODE, E3)
                        ORG.MSG.CODE.T = R.VISA.CODE<SCB.VICD.ORG.MSG.TYPE>
*Line [ 148 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DCT = DCOUNT(ORG.MSG.CODE.T,@VM)
                        FOR WW = 1 TO DCT
                            ORG.MSG.CODE   = R.VISA.CODE<SCB.VICD.ORG.MSG.TYPE,WW>
                            MSG.TYPE.CODE  = R.VISA.CODE<SCB.VICD.MSG.TYPE,WW>
                            PROC.CODE.CODE = R.VISA.CODE<SCB.VICD.PROCESS.CODE,WW>
***************************NEW********************************************
                            DB.CR.CODE = R.VISA.CODE<SCB.VICD.FLAG,WW>
                            REASON.CO.CODE = R.VISA.CODE<SCB.VICD.REASON.CODE,WW>
**************************************************************************
                            CODES.COMB = ORG.MSG.CODE:MSG.TYPE.CODE:PROC.CODE.CODE:DB.CR.CODE:REASON.CO.CODE
                            IF DAILY.COMB = CODES.COMB THEN
                                CODE.TO.USE = KEY.LIST.2<RR>
                            END
                        NEXT WW
                        IF CODE.TO.USE = '' THEN
                            TEXT = 'NO.CODE.WITH' ; CALL REM
                            TEXT = 'ORG.MSG=':ORG.MSG.CODE ; CALL REM
                            TEXT = 'MSG.TYPE=':MSG.TYPE.CODE ; CALL REM
                            TEXT = 'PROC.C=':PROC.CODE.CODE ; CALL REM
                            TEXT = 'FLAQ=':DB.CR.CODE ; CALL REM
                            TEXT = 'REASON=':REASON.CO.CODE ; CALL REM
                        END
                    NEXT RR
                END ;** END OF SELECTED.2
**********************************************************************
            END ELSE          ;**END OF SELECTED***
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath
    TEXT = 'END OF FILE' ; CALL REM
    RETURN
END
