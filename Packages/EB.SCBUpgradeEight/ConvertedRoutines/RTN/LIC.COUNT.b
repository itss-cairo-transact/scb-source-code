* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE LIC.COUNT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LICENSE.COUNT
*=========================================
    FN.LIC = "F.SCB.LICENSE.COUNT"  ; F.LIC  = ""
    CALL OPF (FN.LIC,F.LIC)

    CMD = "jInstallKey -v"
    EXECUTE CMD CAPTURING Y.OUTPUT

    TOT.LIC  = OCONV(Y.OUTPUT<6>,"MCN")
    LIC.FREE = OCONV(Y.OUTPUT<7>,"MCN")
    LIC.USED = TOT.LIC - LIC.FREE
    TIME.NUM = TIME()
    DATE.NUM = DATE()

    WS.DATE.1 = OCONV(DATE.NUM,'D4/')
    WS.TIME = OCONV(TIME.NUM,'MTS')
    WS.DATE = WS.DATE.1[7,4]:WS.DATE.1[1,2]:WS.DATE.1[4,2]
*=========================================
    LIC.ID = WS.DATE:'-':WS.TIME

    CALL F.READ(FN.LIC,LIC.ID,R.LIC,F.LIC,E4)

    R.LIC<SLC.LICENSE.USED> = LIC.USED
    R.LIC<SLC.LICENSE.FREE> = LIC.FREE
    R.LIC<SLC.L.DATE>       = WS.DATE
    R.LIC<SLC.L.TIME>       = WS.TIME

    CALL F.WRITE(FN.LIC,LIC.ID,R.LIC)
    CALL JOURNAL.UPDATE(LIC.ID)

END
