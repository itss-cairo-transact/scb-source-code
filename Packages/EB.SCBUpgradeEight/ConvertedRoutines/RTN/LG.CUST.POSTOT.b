* @ValidationCode : Mjo5NDk0ODAxMDI6Q3AxMjUyOjE2NDIxMDU3OTMyNDU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 13 Jan 2022 22:29:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>216</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LG.CUST.POSTOT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY.PARAM
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CUS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS


    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 53 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 54 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CUST.POSTOT'
    CALL PRINTER.ON(REPORT.ID,'')
    YTEXT = "Enter The Customer ID &":"NNNNNNNN"
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    CUST.ID=COMI
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.ID,ACCT.BR)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    ACCT.BR=R.ITSS.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
IF ETEXT THEN E='Customer.Not.Found';CALL ERR;MESSAGE='REPEAT';RETURN
RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''; R.LD=''
    CALL OPF(FN.LD,F.LD)
******************************************************************
    CUST.ID=COMI
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CUSTOMER.ID EQ ": CUST.ID :" AND CATEGORY GE ": " 21096" :" AND LE ": " 21097" :" BY CURRENCY"
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**************************************
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
        CURR=R.LD<LD.CURRENCY>
*        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CURR,CRR)
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CURR,CRR.DESC)
        F.ITSS.CURRENCY.PARAM = 'F.CURRENCY.PARAM'
        FN.F.ITSS.CURRENCY.PARAM = ''
        CALL OPF(F.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM)
        CALL F.READ(F.ITSS.CURRENCY.PARAM,CURR,R.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM,ERROR.CURRENCY.PARAM)
        CRR.DESC=R.ITSS.CURRENCY.PARAM<EB.CUP.CCY.NAME>

        LOCAL.REF=R.LD<LD.LOCAL.REF>
        MARG.CURR=LOCAL.REF<1,LDLR.ACC.CUR>
        MARG.AMT=LOCAL.REF<1,LDLR.MARGIN.AMT>
        LG.KIND=LOCAL.REF<1,LDLR.LG.KIND>
        LG.TYPE=LOCAL.REF<1,LDLR.PRODUCT.TYPE>
        OPER.CODE=LOCAL.REF<1,LDLR.OPERATION.CODE>
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
        F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
        FN.F.ITSS.SCB.LG.PARMS = ''
        CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
        CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
        TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
        AMT=R.LD<LD.AMOUNT>
        LG.PERC=LOCAL.REF<1,LDLR.MARGIN.PERC>
        LG.PERC1=FIELD(LG.PERC,'.',1)
        LG.PERC2=FIELD(LG.PERC,'.',2)[1,2]
        IF LG.PERC2 NE '' THEN
            LG.PERC=LG.PERC1:'.':LG.PERC2
        END ELSE
            LG.PERC=LG.PERC
        END
        LG.EXPIRY=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
        LG.ISSUE=LOCAL.REF<1,LDLR.ISSUE.DATE>
**************************************************
        LG.ID= 'LG':KEY.LIST<I>[3,10]
        XX<2,1>[1,15] = LG.ID
        XX<2,1>[16,10]=TYPE.NAME
        XX<2,1>[27,4]=CRR.DESC
        XX<2,1>[31,15]=AMT
        XX<2,1>[46,15]=MARG.AMT
        IF LG.TYPE EQ 'ADVANCE' AND LG.PERC EQ ''  THEN
            XX<2,1>[58,4]=LG.PERC
        END ELSE
            XX<2,1>[58,4]=LG.PERC:"%"
        END
        LG.ISSUE = LG.ISSUE[7,2]:"/":LG.ISSUE[5,2]:"/":LG.ISSUE[1,4]
        XX<2,1>[66,8]=LG.ISSUE
        LG.EXPIRY = LG.EXPIRY[7,2]:"/":LG.EXPIRY[5,2]:"/":LG.EXPIRY[1,4]
        XX<2,1>[80,8]=LG.EXPIRY

        PRINT XX<2,1>
        XX<2,1>=''
********************************
*Line [ 143 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CURR,CURR.CODE)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CURR.CODE=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
*Line [ 150 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,MARG.CURR,CURR.CODE1)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,MARG.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CURR.CODE1=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
        VAR.CURR.NO<CURR.CODE> =VAR.CURR.NO<CURR.CODE> + 1
        VAR.AMT<CURR.CODE> = VAR.AMT<CURR.CODE>+R.LD<LD.AMOUNT>
****************************************************************
        IF CURR.CODE EQ 10 THEN
            VAR.TOT<CURR.CODE>=R.LD<LD.AMOUNT>
        END ELSE
            MARKET='1'
            UNEQU.AMT=R.LD<LD.AMOUNT>
            EQU.AMT=''
            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.AMT,CURR,RATE,MARKET,EQU.AMT,DIF.AMT,DIF.RATE)

            VAR.TOT<CURR.CODE>=EQU.AMT
        END
        TOT.ALL +=VAR.TOT<CURR.CODE>
********************************MARG AMT*************************
        IF CURR.CODE1 EQ 10 THEN
            VAR.TOT.MARG<CURR.CODE1>=MARG.AMT
        END ELSE
            MARKET='1'
            UNEQU.AMT=MARG.AMT
            EQU.AMT=''
            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.AMT,MARG.CURR,RATE,MARKET,EQU.AMT,DIF.AMT,DIF.RATE)

            VAR.TOT.MARG<CURR.CODE1>=EQU.AMT
        END
        TOT.ALL.MARG +=VAR.TOT.MARG<CURR.CODE1>
*****************************************************************
        MARG.AMT=LOCAL.REF<1,LDLR.MARGIN.AMT>
        VAR.MRGAMT<CURR.CODE>= VAR.MRGAMT<CURR.CODE>+MARG.AMT
    NEXT I
****************************************
    XX<3,1>=STR('_',88)
    PRINT XX<3,1>

    XX<4,1>=''
    PRINT XX<4,1>
*****************************************
    XX<5,1>=STR(' ',20):STR('-',55)
    PRINT XX<5,1>
    XX<6,1>[1,2] = "����"
    XX<6,1>[12,4]="���"
    XX<6,1>[20,15] = "���� ��������"
    XX<6,1>[40,15] = "���� ���������"

    PRINT STR(' ',22):XX<6,1>

    XX<7,1>[1,2] = "----"
    XX<7,1>[12,4]="---"
    XX<7,1>[20,15] = "-------------"
    XX<7,1>[40,15] = "--------------"

    PRINT STR(' ',22):XX<7,1>
*****************************************
    FOR J = 10  TO 99
        IF VAR.CURR.NO<J> # '' THEN

            XX<8,1>[1,2] = J
            XX<8,1>[12,4]=VAR.CURR.NO<J>
            XX<8,1>[20,15] = VAR.AMT<J>
            XX<8,1>[40,15] = VAR.MRGAMT<J>

            PRINT STR(' ',22):XX<8,1>
            XX<8,1> = ''
        END
    NEXT J
*********************************************************
    XX<9,1>=STR(' ',20):STR('-',55)
    XX<10,1>[1,8]='������'
    XX<10,1>[12,4]=SELECTED
    XX<10,1>[20,15]=TOT.ALL
    XX<10,1>[40,15]= TOT.ALL.MARG

    PRINT XX<9,1>
    PRINT STR(' ',22):XX<10,1>
*****************************************************************
    XX<11,1>[1,8]="������":":"
*Line [ 233 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.CUS':@FM:SCB.LGCS.LIMIT.NO,CUST.ID,LIMI)
    F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
    FN.F.ITSS.SCB.LG.CUS = ''
    CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
    CALL F.READ(F.ITSS.SCB.LG.CUS,CUST.ID,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
    LIMI=R.ITSS.SCB.LG.CUS<SCB.LGCS.LIMIT.NO>
*Line [ 240 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('LIMIT':@FM:LI.ONLINE.LIMIT,LIMI,AMT.LIM)
    F.ITSS.LIMIT = 'F.LIMIT'
    FN.F.ITSS.LIMIT = ''
    CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
    CALL F.READ(F.ITSS.LIMIT,LIMI,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
    AMT.LIM=R.ITSS.LIMIT<LI.ONLINE.LIMIT>
*Line [ 247 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('LIMIT':@FM:LI.EXPIRY.DATE,LIMI,EXP.LIM)
    F.ITSS.LIMIT = 'F.LIMIT'
    FN.F.ITSS.LIMIT = ''
    CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
    CALL F.READ(F.ITSS.LIMIT,LIMI,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
    EXP.LIM=R.ITSS.LIMIT<LI.EXPIRY.DATE>

    IF  TOT.ALL GT AMT.LIM THEN
        AMT.LIM =")":AMT.LIM:"("
    END ELSE
        AMT.LIM=AMT.LIM
    END
    XX<11,1>[16,15]=AMT.LIM
    XX<11,1>[30,8]="���� ���":" : "
    EXP.LIM = EXP.LIM[7,2]:"/":EXP.LIM[5,2]:"/":EXP.LIM[1,4]
    XX<11,1>[43,8]=EXP.LIM
    PRINT STR(' ',22):XX<11,1>
***********************************
*Line [ 266 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.CUS':@FM:SCB.LGCS.LG.TYPE,CUST.ID,TYPE.LG)
    F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
    FN.F.ITSS.SCB.LG.CUS = ''
    CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
    CALL F.READ(F.ITSS.SCB.LG.CUS,CUST.ID,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
    TYPE.LG=R.ITSS.SCB.LG.CUS<SCB.LGCS.LG.TYPE>
*Line [ 273 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUST.ID,PERC.LG)
    F.ITSS.SCB.LG.CUS = 'F.SCB.LG.CUS'
    FN.F.ITSS.SCB.LG.CUS = ''
    CALL OPF(F.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS)
    CALL F.READ(F.ITSS.SCB.LG.CUS,CUST.ID,R.ITSS.SCB.LG.CUS,FN.F.ITSS.SCB.LG.CUS,ERROR.SCB.LG.CUS)
    PERC.LG=R.ITSS.SCB.LG.CUS<SCB.LGCS.MARGIN.PERC>
*Line [ 219 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.T=DCOUNT(TYPE.LG,@VM)

    FOR G=1 TO NO.T
*Line [ 284 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,TYPE.LG<1,G>,TYPE.NAME)
        F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
        FN.F.ITSS.SCB.LG.PARMS = ''
        CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
        CALL F.READ(F.ITSS.SCB.LG.PARMS,TYPE.LG<1,G>,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
        TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
        XX<12,1>[1,10] = TYPE.NAME
        XX<12,1>[15,4] = PERC.LG<1,G>:"%"
        PRINT STR(' ',22):XX<12,1>
        XX<12,1> = ''
    NEXT G
*    XX<11,1>='-----------------------------------------------------'
    XX<13,1>=STR(' ',20):STR('-',55)
    PRINT XX<13,1>
**************************************
RETURN
*===============================================================
PRINT.HEAD:
*     CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.ID,ACCT.BR)
*     CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 305 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.ID,ACCT.BR)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    ACCT.BR=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    ACCT.BR = ACCT.BR[2]
    ACCT.BR = TRIM(ACCT.BR,"0","L")

*Line [ 315 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACCT.BR,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACCT.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
*Line [ 325 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME =LOC.REF<1,CULR.ARABIC.NAME>

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"���:":YYBRN:SPACE(44):"�������:":T.DAY
    PR.HD :="'L'":" "
*    PR.HD :="'L'":SPACE(1):"�������:":T.DAY
    PR.HD :="'L'":SPACE(1):"�����:":"'T'":SPACE(29):"��� ������:":"'P'"
    PR.HD :="'L'":" "
*    PR.HD :="'L'":SPACE(2):"��� ������:":"'P'"
    PR.HD :="'L'":SPACE(25):"��������� �� ���� ������ ����"
    PR.HD :="'L'":SPACE(25):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"��� ������":" : ":CUST.ID :"  " :CUST.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"��� ������ ":SPACE(3):"����" :SPACE(7):"����" :SPACE(3):"� ����":SPACE(6):"� ����� ":SPACE(5):"����":SPACE(4):"�.�������":SPACE(4):"�.��������"
    PR.HD :="'L'":STR('_',88)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
