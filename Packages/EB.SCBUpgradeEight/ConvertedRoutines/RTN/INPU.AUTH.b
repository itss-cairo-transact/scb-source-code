* @ValidationCode : MjoxOTA3NjkyNDU1OkNwMTI1MjoxNjQxMzY3NDg3MDAwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 05 Jan 2022 09:24:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
******NESSREEN AHMED*******
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE INPU.AUTH(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE and removed for duplication - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*A Routine to select only the inputter name or authorizer name from Inputter or Authorizer field
*from Teller module
*This Routine will be put in the format of the field in Deal.slip.Format

    TEXT = 'ARG=':ARG ; CALL REM
    INPUTTER = (ARG)
    TEXT = 'INP=':INPUTTER ; CALL REM
    ARG = FIELD(INPUTTER,'_',2)
 
RETURN
END
