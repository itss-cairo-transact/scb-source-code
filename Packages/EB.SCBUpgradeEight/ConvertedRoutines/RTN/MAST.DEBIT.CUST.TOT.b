* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1934</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAST.DEBIT.CUST.TOT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.CODES
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.USAGES.TOT

* WRITTEN BY NESSREEN AHMED-SCB
* TO COLLECT THE TOTAL USAGES TO BE DEBITED FROM CUSTOMERS'VISA ACCOUNT AND WRITE IT ON TABLE
* SCB.MAST.USAGES.TOT , THEN PRINTOUT A REPORT CONTANING THE CUSTOMERS AND TOTAL AMOUNT TO BE DEBITED


    GOSUB INITIATE
***    GOSUB PRINT.HEAD
*Line [ 55 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 56 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
    GOSUB CALLDB
***    CALL PRINTER.OFF
***    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
*** REPORT.ID='MAST.DEBIT.CUST.TOT'
*** CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    TEXT = '2' ; CALL REM
    F.MAST.TRANS = '' ; FN.MAST.TRANS = 'F.SCB.MAST.TRANS' ; R.MAST.TRANS = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.MAST.TRANS,F.MAST.TRANS)

    F.MAST.USAGES.TOT = '' ; FN.MAST.USAGES.TOT = 'F.SCB.MAST.USAGES.TOT' ; R.MAST.USAGES.TOT = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.MAST.USAGES.TOT,F.MAST.USAGES.TOT)

*******UPDATED IN 18/2/2008*********************************
**    YTEXT = "Enter the Start Date Of Month : "
**    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    DATEE = '' ; YYYY = '' ; MM = '' ; MT = '' ; MU = '' ; YYDD = '' ; MON = ''
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YY = YYYY-1
    END ELSE
        MU = MT-1
        YY = YYYY
    END
***********************************
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END

***********************************
    YYDD = YY:MON:'01'
    BR = R.USER<EB.USE.DEPARTMENT.CODE>

**    T.SEL = "SELECT F.SCB.MAST.TRANS WITH POS.DATE GE ":COMI :" BY @ID "
    T.SEL = "SELECT F.SCB.MAST.TRANS WITH POS.DATE GE ":YYDD :" BY @ID "

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED.DB=':SELECTED ; CALL REM
*================================================================
    IF SELECTED THEN

****************************************
        CALL F.READ(FN.MAST.TRANS,KEY.LIST<1>,R.MAST.TRANS,F.MAST.TRANS,E2)
*****************************************
        XX=''  ; ZZ=''
        ACCT.NO = ''  ; POS.DATE=''
        TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''  ; TOT.CUS.ALL = ''
        TOT.AMT.CR = '' ; TOT.AMT.DB = ''
        VISA.NO = KEY.LIST<1>[1,16]
**********UPDATED IN 18/2/2008*********************************
        BR.NO = R.MAST.TRANS<SCB.MAST.BRANCH.NUMBER>
***************************************************************
        ACCT.NO<1> = R.MAST.TRANS<SCB.MAST.CUST.ACCT>
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<1>,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO<1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>

        TRNS.CODE=R.MAST.TRANS<SCB.MAST.TRANS.CODE>
*Line [ 127 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
        POS.DATE<1> = R.MAST.TRANS<SCB.MAST.POS.DATE,TRNS.COUNT>
        FOR J= 1 TO TRNS.COUNT
            T.CODE=TRNS.CODE<1,J>
            IF T.CODE EQ '1' THEN
                TOT.USE=TOT.USE+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
            END
            IF T.CODE EQ '2' THEN
                TOT.INT=TOT.INT+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
            END
            IF T.CODE EQ '3' THEN
                TOT.COMM=TOT.COMM+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
            END

            IF T.CODE EQ '4' THEN
                TOT.FEE=TOT.FEE+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
            END

            IF T.CODE EQ '5' THEN
                TOT.ACT=TOT.ACT+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>

            END
            IF T.CODE EQ '6' THEN
                TOT.REV=TOT.REV+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
            END
        NEXT J
        TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV

***********************************
        FOR I = 2 TO SELECTED
            CALL F.READ(FN.MAST.TRANS,KEY.LIST<I>,R.MAST.TRANS,F.MAST.TRANS,E2)
            ACCT.NO<I> = R.MAST.TRANS<SCB.MAST.CUST.ACCT>
            IF ACCT.NO<I> # ACCT.NO<I-1> THEN
                XX<1,ZZ>[1,30]= CUST.NAME
                XX<1,ZZ>[50,16]= VISA.NO
                XX<1,ZZ>[80,16]= ACCT.NO<I-1>
                XX<1,ZZ>[110,15]=TOT.ALL

*************************************************
****************TO WRITE ON SCB.MAST.USAGES.TOT**
*************************************************
                KEY.ID =  VISA.NO:TODAY
                IF TOT.ALL < 0 THEN
                    TOT.AMT.CR = TOT.ALL
                END ELSE
                    TOT.AMT.DB = TOT.ALL
                END
                CALL F.READ(FN.MAST.USAGES.TOT,KEY.ID,R.MAST.USAGES.TOT,F.MAST.USAGES.TOT,E3)
                R.MAST.USAGES.TOT<USAGES.TOT.M.VISA.NO>= VISA.NO
                R.MAST.USAGES.TOT<USAGES.TOT.M.ACCT.NO>= ACCT.NO<I-1>
                R.MAST.USAGES.TOT<USAGES.TOT.M.TOT.AMT.DB>= TOT.AMT.DB
                R.MAST.USAGES.TOT<USAGES.TOT.M.TOT.AMT.CR>= TOT.AMT.CR
                R.MAST.USAGES.TOT<USAGES.TOT.M.POS.DATE>= POS.DATE<I-1>
**************UPDATED IN 18/2/2008*******************
                R.MAST.USAGES.TOT<USAGES.TOT.M.BRANCH.NUMBER>= BR.NO
*****************************************************
                CALL F.WRITE(FN.MAST.USAGES.TOT,KEY.ID, R.MAST.USAGES.TOT)
                CALL JOURNAL.UPDATE(KEY.ID)
*************************************************
                CUST.NAME = '' ; VISA.NO = '' ; TOT.ALL = ''  ; TOT.AMT.CR = '' ;  TOT.AMT.DB = ''
                TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''
                TOT.CUS.ALL = ''  ; BR.NO = ''
                VISA.NO = KEY.LIST<I>[1,16]
**********UPDATED IN 18/2/2008*********************************
                BR.NO = R.MAST.TRANS<SCB.MAST.BRANCH.NUMBER>
***************************************************************
                ACCT.NO<I> = R.MAST.TRANS<SCB.MAST.CUST.ACCT>
*Line [ 214 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<I>,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 221 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
                TRNS.CODE=R.MAST.TRANS<SCB.MAST.TRANS.CODE>
*Line [ 199 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
                POS.DATE<I> = R.MAST.TRANS<SCB.MAST.POS.DATE,TRNS.COUNT>
                FOR J= 1 TO TRNS.COUNT
                    T.CODE=TRNS.CODE<1,J>
                    IF T.CODE EQ '1' THEN
                        TOT.USE=TOT.USE+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '2' THEN
                        TOT.INT=TOT.INT+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '3' THEN
                        TOT.COMM=TOT.COMM+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '4' THEN
                        TOT.FEE=TOT.FEE+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '5' THEN
                        TOT.ACT=TOT.ACT+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>

                    END
                    IF T.CODE EQ '6' THEN
                        TOT.REV=TOT.REV+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END
                NEXT J
                TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV
            END ELSE
                TRNS.CODE=R.MAST.TRANS<SCB.MAST.TRANS.CODE>
*Line [ 229 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
                POS.DATE<I> = R.MAST.TRANS<SCB.MAST.POS.DATE,TRNS.COUNT>
                FOR J= 1 TO TRNS.COUNT
                    T.CODE=TRNS.CODE<1,J>
                    IF T.CODE EQ '1' THEN
                        TOT.USE=TOT.USE+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '2' THEN
                        TOT.INT=TOT.INT+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '3' THEN
                        TOT.COMM=TOT.COMM+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '4' THEN
                        TOT.FEE=TOT.FEE+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '5' THEN
                        TOT.ACT=TOT.ACT+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>

                    END
                    IF T.CODE EQ '6' THEN
                        TOT.REV=TOT.REV+R.MAST.TRANS<SCB.MAST.TRANS.AMT><1,J>
                    END
                NEXT J
                TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV

            END
************************************************************
            IF I = SELECTED   THEN
                XX<1,ZZ>[1,30]= CUST.NAME
                XX<1,ZZ>[50,16]= VISA.NO
                XX<1,ZZ>[80,16]= ACCT.NO<I>
                XX<1,ZZ>[110,15]=TOT.ALL
***       PRINT XX<1,ZZ>
                ZZ=ZZ+1
*************************************************
****************TO WRITE ON SCB.MAST.USAGES.TOT**
*************************************************
                KEY.ID =  VISA.NO:TODAY
                IF TOT.ALL < 0 THEN
                    TOT.AMT.CR = TOT.ALL
                END ELSE
                    TOT.AMT.DB = TOT.ALL
                END
                CALL F.READ(FN.MAST.USAGES.TOT,KEY.ID,R.MAST.USAGES.TOT,F.MAST.USAGES.TOT,E3)
                R.MAST.USAGES.TOT<USAGES.TOT.M.VISA.NO>= VISA.NO
                R.MAST.USAGES.TOT<USAGES.TOT.M.ACCT.NO>= ACCT.NO<I>
                R.MAST.USAGES.TOT<USAGES.TOT.M.TOT.AMT.DB>= TOT.AMT.DB
                R.MAST.USAGES.TOT<USAGES.TOT.M.TOT.AMT.CR>= TOT.AMT.CR
                R.MAST.USAGES.TOT<USAGES.TOT.M.POS.DATE>= POS.DATE<I>
**************UPDATED IN 18/2/2008*******************
                R.MAST.USAGES.TOT<USAGES.TOT.M.BRANCH.NUMBER>= BR.NO
*****************************************************
                CALL F.WRITE(FN.MAST.USAGES.TOT,KEY.ID, R.MAST.USAGES.TOT)
                CALL JOURNAL.UPDATE(KEY.ID)
*************************************************
            END
**************************************************************************
        NEXT I
****UPDATED BY NESSREEN AHMED 2/4/2012*********************
        N.SEL = "SELECT F.SCB.MAST.USAGES.TOT WITH POS.DATE GE ":YYDD
        KEY.LIST.N ="" ; SELECTED.N="" ;  ER.MSG.N=""
        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        TEXT = '��� �������=':SELECTED.N ; CALL REM

****END OF UPDATED 2/4/2012********************************

    END   ;* END OF IF SELECTED***
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 334 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ������ ��� ��������� ������� ��� ���� ������"
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"����� ��������":SPACE(35):"��� ������":SPACE(20):"��� ���� �����":SPACE(12):"����������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(33):STR('_',12):SPACE(18):STR('_',20):SPACE(10):STR('_',8)
    HEADING PR.HD
    RETURN
*==============================================================

END
