* @ValidationCode : MjoxNzQ5MTU1ODEzOkNwMTI1MjoxNjQwNzA5NzIzNDQ5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:42:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
SUBROUTINE IN.AR.CUR(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_INF.LOCAL.REFS
*-----------------------------------------
    FN.IN = "F.INF.MULTI.TXN" ; F.IN = ""
    CALL OPF(FN.IN, F.IN)

    IN.ID = ARG
    CALL F.READ(FN.IN,IN.ID,R.IN,F.IN,ER.IN)
    IN.DCOUNT        = R.IN<INF.MLT.ACCOUNT.NUMBER>
    LANGUAGE.NUMBER  = R.IN<INF.MLT.LOCAL.REF><1,INLR.LANGUAGE>
*Line [ 36 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    LOOP.NN          = DCOUNT(IN.DCOUNT,@VM)

    FOR II  = 1 TO LOOP.NN
        CHQ.NUM = R.IN<INF.MLT.CHEQUE.NUMBER><1,II>
        IF CHQ.NUM NE '' THEN
            CUR  = R.IN<INF.MLT.CURRENCY><1,II>
            II   = LOOP.NN
        END
    NEXT II

    IF LANGUAGE.NUMBER EQ '2' THEN
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    END

    IF LANGUAGE.NUMBER EQ '1' THEN
        FN.CUR = "F.CURRENCY"  ; F.CUR = ""
        CALL OPF(FN.CUR,F.CUR)

        CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ER.CUR)
        CURR = R.CUR<EB.CUR.CCY.NAME><1,1>
    END

    ARG = CURR
*------------------------------------------------------
RETURN
END
