* @ValidationCode : Mjo0MDU3NzM1MTM6Q3AxMjUyOjE2NDE3MTc3NDM5Njg6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 10:42:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE GET.RENEW.METHOD(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.RENEW.METHOD

    ARG = R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.IND>
*Line [ 33 ] change LDLR.RENNEW.METHOD to LDLR.RENEW.METHOD as per in the layout- ITSS - R21 Upgrade - 2021-12-23
    METH = R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.METHOD>
    IF ARG = 'YES' THEN
        ARG = "������� ����"
*    CALL DBR("SCB.LD.RENEW.METHOD":@FM:LD.REN.DESCRIPTION,METH,DESC)
*   ARG = DESC
    END
    IF ARG = '' OR ARG = 'NO' THEN
        ARG = "������� �� ����"
    END
RETURN
END
