* @ValidationCode : MjotMjg4MDgwNTY6Q3AxMjUyOjE2NDA3MDY3MzQzOTI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 17:52:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*--- cREATED BY NESSMA ON 2019/03/10
SUBROUTINE GET.CHEQ.DATE(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_FT.LOCAL.REFS
*----------------------------------------------
***UPDATED BY NESSREEN 11/3/2019******
***    ARG  = R.NEW(FT.LOCAL.REF)<1,FTLR.CHQ.DATE>
    CHQ.DATE = ""
    Y.FT.LOC.VAL = R.NEW(FT.LOCAL.REF)
    CHQ.DATE = Y.FT.LOC.VAL<1,FTLR.CHQ.DATE>
    ARG      = FMT(CHQ.DATE,"####/##/##")
*--------------------------------------------
RETURN
END
