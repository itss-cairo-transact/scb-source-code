* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*--WAGDY---------------------------------------------------------------------------
* <Rating>-36</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MKSA.VAR.BR.CLR.LCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.FCY.REF
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.LCY.REF

    TOD = TODAY
    CALL !HUSHIT(0)
    COMAND2 = " chmod -R 777 /hq/opce/bclr/user/dltx/":TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.txt "
* TEXT =  COMAND2 ; CALL REM
    EXECUTE COMAND2

    ZZXX = " �� ���� ��� ����� ���� �"

**---------------------------------------------------------------------**
*Line [ 61 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP(ZZXX, 8, 23, '1.1',@FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
*        CALL !HUSHIT(0)
*        COMAND2 = " chmod -R 777 /hq/opce/bclr/user/dltx/":TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.txt "
*        EXECUTE COMAND2

        EOF = ''
        FN.REF = 'F.SCB.BR.LCY.REF' ; F.REF = '' ; R.REF = ''
        CALL OPF(FN.REF,F.REF)

*2010-03-14-credit.txt
        TOD = TODAY

        Path = "/hq/opce/bclr/user/dltx/":TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.txt"
        TEXT =  TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.txt" ; CALL REM
        EOF = ''
        OPENSEQ Path TO MY.PATH ELSE
            TEXT = "ERROR OPEN FILE" ; CALL REM
            RETURN
        END
*   I    = 1
        EOD = ''
        LOOP WHILE NOT(EOF)
            READSEQ Line FROM MY.PATH THEN

                OUR.REF      = FIELD(Line,",",3)

                R.REF<1>     = FIELD(Line,",",1)
                R.REF<2>     = FIELD(Line,",",2)
                R.REF<3>     = FIELD(Line,",",3)
                R.REF<4>     = FIELD(Line,",",4)
                R.REF<5>     = FIELD(Line,",",5)
                R.REF<6>     = FIELD(Line,",",6)
                R.REF<7>     = FIELD(Line,",",7)
                R.REF<8>     = FIELD(Line,",",8)
                R.REF<9>     = FIELD(Line,",",9)
                R.REF<10>    = FIELD(Line,",",10)
                R.REF<11>    = FIELD(Line,",",11)
                R.REF<12>    = FIELD(Line,",",12)
                R.REF<13>    = FIELD(Line,",",13)
                R.REF<14>    = TODAY

                WRITE  R.REF TO F.REF , OUR.REF ON ERROR
                    PRINT "CAN NOT WRITE RECORD":OUR.REF:"TO" :FN.REF
                END
* I = I + 1
            END ELSE

                EOF = 1
            END

        REPEAT
        TEXT = "�� ��� ����� �� ������ ��� ������ �����"  ; CALL REM
        CLOSESEQ MY.PATH

*  RETURN
    END
**-------------------------------------------------------------------**
END
