* @ValidationCode : MjoyMDEyMjkzMTY0OkNwMTI1MjoxNjQwNzA4OTA0NTIyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:28:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
SUBROUTINE GET.SAVE.ACCT.UNDERAGE.D

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ACCT.UNDERAGE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*================ CLEAN FILE =====================================-*
*    FN.UNDER = "F.SCB.ACCT.UNDERAGE"
*    OPEN FN.UNDER TO FILEVAR ELSE ABORT 201,FN.UNDER
*    CLEARFILE FILEVAR
*==================== OPEN TABLES ================================
    FN.CU  = 'FBNK.CUSTOMER'  ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC  = 'FBNK.ACCOUNT'   ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = ''
    CALL OPF( FN.CUS.ACC,F.CUS.ACC)
    FN.SA  = 'F.SCB.ACCT.UNDERAGE'  ; F.SA = '' ; R.SA = ''
    CALL OPF(FN.SA,F.SA)


*========================= DEFINE VARIABLES =========================
    COMP=ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    Y.CUS.ACC  = ''

*========================= GET ALL INDIVIDUAL CUSTOMERS ==================
    T.SEL  = "SELECT FBNK.CUSTOMER WITH SECTOR EQ '2000'"
    T.SEL := " AND POSTING.RESTRICT LT 90 AND ACCOUNT.OFFICER EQ 11"
    T.SEL := " AND ACTIVE.CUST EQ 'YES' AND BIRTH.INCORP.DATE GE 19901031 "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*=========================  ===========================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            R.SA = ''
            DAYS = "C"
            CALL F.READ( FN.CU,KEY.LIST<I>, R.CU, F.CU, ETEXT)
            CUST.NO       = KEY.LIST<I>
            CUST.COMP     =  R.CU<EB.CUS.COMPANY.BOOK>
            CU.STATUS     = R.CU<EB.CUS.CUSTOMER.STATUS>
            CU.BIRTH.DATE = R.CU<EB.CUS.BIRTH.INCORP.DATE>
            CALL CDD("", CU.BIRTH.DATE,TODAY,DAYS)
            CU.AGE        = DAYS/365

            IF CU.AGE LE 21 THEN
                CALL F.READ(FN.CUS.ACC,KEY.LIST<I>,R.CUS.ACC,F.CUS.ACC,Y.CUS.ACC.ERR)
                LOOP
                    REMOVE Y.CUS.ACC.ID FROM R.CUS.ACC SETTING SUBVAL
                WHILE Y.CUS.ACC.ID:SUBVAL

*=========READ FROM ACCOUNT THE DEPIT CATEG ================
                    CALL F.READ( FN.AC,Y.CUS.ACC.ID,R.AC, F.AC, ETEXT1)
                    IF R.AC<AC.CURRENCY> EQ 'EGP' THEN
                        IF R.AC<AC.CATEGORY> EQ '6511' OR R.AC<AC.CATEGORY> EQ '6512' THEN
                            BAL = R.AC<AC.OPEN.ACTUAL.BAL>
                            R.SA<ACCT.UNDER.CUSTOMER.NO> = CUST.NO
                            R.SA<ACCT.UNDER.ACCT.FLAG>   = ''
                            R.SA<ACCT.UNDER.CO.CODE>     = CUST.COMP
                            R.SA<ACCT.UNDER.BIRTH.DATE>  = CU.BIRTH.DATE
                            R.SA<ACCT.UNDER.RESERVED9>   = BAL
                            CALL F.WRITE(FN.SA,Y.CUS.ACC.ID,R.SA)
                            CALL JOURNAL.UPDATE(Y.CUS.ACC.ID)
                        END
                    END
                REPEAT
            END

            CU.RND.AGE = DAYS

            IF CU.RND.AGE GT 7665 AND CU.RND.AGE LE 7695.5 THEN
                CALL F.READ(FN.CUS.ACC,KEY.LIST<I>,R.CUS.ACC,F.CUS.ACC,Y.CUS.ACC.ERR)
                LOOP
                    REMOVE Y.CUS.ACC.ID FROM R.CUS.ACC SETTING SUBVAL
                WHILE Y.CUS.ACC.ID:SUBVAL

*=========READ FROM ACCOUNT THE DEPIT CATEG ================
                    CALL F.READ( FN.AC,Y.CUS.ACC.ID,R.AC, F.AC, ETEXT1)
                    IF R.AC<AC.CURRENCY> EQ 'EGP' THEN
                        IF R.AC<AC.CATEGORY> EQ '6511' OR R.AC<AC.CATEGORY> EQ '6512' THEN
                            BAL = R.AC<AC.OPEN.ACTUAL.BAL>
                            R.SA<ACCT.UNDER.RESERVED9>   = BAL
                            R.SA<ACCT.UNDER.CUSTOMER.NO> = CUST.NO
                            R.SA<ACCT.UNDER.ACCT.FLAG>   = '21'
                            R.SA<ACCT.UNDER.CO.CODE>     = CUST.COMP
                            R.SA<ACCT.UNDER.BIRTH.DATE>  = CU.BIRTH.DATE
                            CALL F.WRITE(FN.SA,Y.CUS.ACC.ID,R.SA)
                            CALL JOURNAL.UPDATE(Y.CUS.ACC.ID)
                        END
                    END
                REPEAT
            END
        NEXT I
    END
RETURN
END
