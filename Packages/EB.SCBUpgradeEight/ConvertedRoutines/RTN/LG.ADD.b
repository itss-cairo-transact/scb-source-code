* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE LG.ADD(AUTH,LG.CO)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


    LG.USER    = AUTH
    CO.COD    = LG.CO
TEXT=LG.USER:'-LG.USER':OPERATOR;CALL REM
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,LG.USER,USR.DEP)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,LG.USER,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USR.DEP=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
    FN.DEPT = 'F.DEPT.ACCT.OFFICER' ; F.DEPT = ''
    CALL OPF(FN.DEPT,F.DEPT)
    CALL F.READ(FN.DEPT,USR.DEP,R.DEPT,F.DEPT,E1)
***********OPERATION CENTER************
    IF USR.DEP EQ '88' THEN
        BRN.NAME = R.DEPT<EB.DAO.NAME>
        BRN.AREA = R.DEPT<EB.DAO.AREA>
        BRN.NAME = FIELD(BRN.NAME,'.',2)
*        TEXT = BRN.AREA:'-AREA OP';CALL REM
        TEXT = BRN.NAME:'-NAME OP';CALL REM
    END ELSE
********BRNACHES**************
        TEXT=CO.COD:'-CO';CALL REM
*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('COMPANY':@FM:EB.COM.NAME.ADDRESS,CO.COD,BRN.AREA)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.COD,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRN.AREA=R.ITSS.COMPANY<EB.COM.NAME.ADDRESS>
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.COD,BRN.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.COD,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRN.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        TEXT = BRN.AREA:'-AREA BR';CALL REM
        TEXT = BRN.NAME:'-NAME BR';CALL REM
    END
    BRN.PHON = R.DEPT<EB.DAO.TELEPHONE.NO>
    BRN.FAX  = R.DEPT<EB.DAO.FAX.NO>
****
    BRN.NAM.ADD = BRN.AREA:"-": BRN.NAME
    BRN.PH.FAX  = BRN.PHON:"-":BRN.FAX
****
    PRINT BRN.NAME
*   PRINT BRN.NAM.ADD
*   PRINT BRN.PH.FAX

    RETURN
END
