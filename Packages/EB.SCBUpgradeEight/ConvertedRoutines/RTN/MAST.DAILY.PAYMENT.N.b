* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
***NESSREEN AHMED 24/8/2010*********

    SUBROUTINE MAST.DAILY.PAYMENT.N

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BRSIN.FLAG

* A ROUTINE TO CREATE A TEXT FILE CONTANING ALL THE CASH PAYMENT THAT HAPPENED THROUGH THE DAY

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "scbpym1"
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
**********************FILLING DATA********************************
******************************************************************
    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE EQ 38 "
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CARD.NO = '' ; EMB.NAME = '' ;  CURR = '' ; AMT = '' ; PAY.DATE = '' ; REF = ''
            FN.TELLER = 'F.TELLER' ; F.TELELR = '' ; R.TELLER = '' ; RETRY1 = '' ; E1 = ''
            KEY.TO.USE = KEY.LIST<I>
            CALL OPF(FN.TELLER,F.TELLER)
            CALL F.READ(FN.TELLER,  KEY.TO.USE, R.TELLER, F.TELLER, E1)

            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,TTLR.VISA.NUMBER>
            ACCT = R.TELLER<TT.TE.ACCOUNT.1>

            CURR = R.TELLER<TT.TE.CURRENCY.1>
            AMTT = R.TELLER<TT.TE.NET.AMOUNT>
            FMTAMT = AMTT*100

***********************************************************
            TOTAMT = TOTAMT + AMTT
            TR.DATE = R.TELLER<TT.TE.DATE.TIME>
            PAY.DATE = TODAY
            TRANS.DATE = TODAY
            TYY = TODAY[3,2]
            TMM  = TODAY[5,2]
            TDD = TODAY[7,2]

***************FILLING BRIDGE******************************************************************************

            VISA.DATA = 'SCB'
            VISA.DATA := 'S01'
            VISA.DATA := STR("0",5 - LEN(I)):I
            VISA.DATA := STR(" ",1)
            VISA.DATA := VISA.NO:STR(" ",19 - LEN(VISA.NO))
            VISA.DATA := STR(" ",2)
            VISA.DATA := VISA.NO:STR(" ",19 - LEN(VISA.NO))
            VISA.DATA := '1'
            VISA.DATA := '1':TYY:TMM:TDD
            VISA.DATA := I:STR(" ",5 - LEN(I))
            VISA.DATA := STR(" ",29)
            VISA.DATA := '80'
            VISA.DATA := '04'
            VISA.DATA := STR(" ",5)
            VISA.DATA := 'C'
            VISA.DATA := '818'
            VISA.DATA := STR("0",15 - LEN(FMTAMT)):FMTAMT
            VISA.DATA := STR(" ",1)
            VISA.DATA := STR("0",15)
            VISA.DATA := STR(" ",1)
            VISA.DATA := '818'
            VISA.DATA := STR("0",15 - LEN(FMTAMT)):FMTAMT
            VISA.DATA := STR(" ",1)
            VISA.DATA := STR("0",15)
            VISA.DATA := STR(" ",1)
            VISA.DATA := '818'
            VISA.DATA := STR("0",15)
            VISA.DATA := STR(" ",1)
            VISA.DATA := STR("0",15)
            VISA.DATA := STR(" ",1)
            VISA.DATA := 'BNK'
            VISA.DATA := STR(" ",15)
            VISA.DATA := STR(" ",40)
            VISA.DATA := '1':TYY:TMM:TDD
            VISA.DATA := STR(" ",1)
            VISA.DATA := '1':TYY:TMM:TDD
            VISA.DATA := STR(" ",97)

            DIM ZZ(SELECTED)
            ZZ(SELECTED) = VISA.DATA

            WRITESEQ ZZ(SELECTED) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':ZZ(SELECTED)
            END

        NEXT I

    END
    XX = I+1
***********************************************************************************************
***************FT SELECTION********************************************************************
    N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVM' "
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    TEXT = 'SELECTED.N=':SELECTED.N ; CALL REM
    AA = XX
    IF SELECTED.N THEN
        FOR X=1 TO SELECTED.N
            FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
            FT.ID = KEY.LIST.N<X>
            CALL OPF(FN.FT,F.FT)
            CALL F.READ(FN.FT,  FT.ID, R.FT, F.FT, E2)

            LOCAL.REF.FT = R.FT<FT.LOCAL.REF>
            VISA.NO.FT = LOCAL.REF.FT<1,FTLR.VISA.NO>

            CUST.FT = R.FT<FT.CREDIT.CUSTOMER>
            CURR.FT = R.FT<FT.CREDIT.CURRENCY>
            AMTT.FT = R.FT<FT.DEBIT.AMOUNT>
            FMTAMT.FT = AMTT.FT*100
*******UPDATED BY NESSREN AHMED 21/10/2009*************************
       **   PAY.DATE.FT = R.FT<FT.CREDIT.VALUE.DATE>
            PAY.DATE.FT = R.FT<FT.AUTH.DATE>
************END OF UPDATE *****************************************
            FYY = TODAY[3,2]
            FMM  = TODAY[5,2]
            FDD = TODAY[7,2]
***************FILLING BRIDGE FOR FT REC*********************************************************************
*****UPDATED ACCORDING TO COMPANY REQUIRMENTS TO ACCEPT TWO FILES ******
****IN 14/10/2008*****
            VISA.DATA.FT = 'SCB'
       **   VISA.DATA.FT := 'S99'
            VISA.DATA.FT := 'S01'
            VISA.DATA.FT := STR("0",5 - LEN(AA)):AA
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := VISA.NO.FT:STR(" ",19 - LEN(VISA.NO.FT))
            VISA.DATA.FT := STR(" ",2)
            VISA.DATA.FT := VISA.NO.FT:STR(" ",19 - LEN(VISA.NO.FT))
            VISA.DATA.FT := '1'
            VISA.DATA.FT := '1':FYY:FMM:FDD
            VISA.DATA.FT := AA:STR(" ",5 - LEN(AA))
            VISA.DATA.FT := STR(" ",29)
            VISA.DATA.FT := '80'
            VISA.DATA.FT := '04'
            VISA.DATA.FT := STR(" ",5)
            VISA.DATA.FT := 'C'
            VISA.DATA.FT := '818'
            VISA.DATA.FT := STR("0",15 - LEN(FMTAMT.FT)):FMTAMT.FT
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := STR("0",15)
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := '818'
            VISA.DATA.FT := STR("0",15 - LEN(FMTAMT.FT)):FMTAMT.FT
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := STR("0",15)
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := '818'
            VISA.DATA.FT := STR("0",15)
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := STR("0",15)
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := 'BNK'
            VISA.DATA.FT := STR(" ",15)
            VISA.DATA.FT := STR(" ",40)
            VISA.DATA.FT := '1':FYY:FMM:FDD
            VISA.DATA.FT := STR(" ",1)
            VISA.DATA.FT := '1':FYY:FMM:FDD
            VISA.DATA.FT := STR(" ",97)

            DIM NN(AA)
            NN(AA) = VISA.DATA.FT

            WRITESEQ NN(AA) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':NN(AA)
            END

            AA = AA + 1
        NEXT X
    END
***********************************************************************************************

    RETURN

END
