* @ValidationCode : MjoxOTE4Mzc5ODY0OkNwMTI1MjoxNjQwNzA3NjA2MzM4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:06:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE GET.NAME.2(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS

*TEXT = ARG; CALL REM

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    CALL OPF(FN.AC,F.AC)

    IF ARG NE '2305340' OR ARG NE '3301171' THEN
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,ARG,LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ARG,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        NAME = LOCAL<1,CULR.ARABIC.NAME.2>
        NAME = ARG
    END

*   IF ARG EQ '3301171' THEN
*       AC = '0330117110100102'
*       CALL DBR("ACCOUNT":@FM:AC.SHORT.TITLE,AC,NAME)
*       ARG = NAME
*   END
RETURN
END
