* @ValidationCode : MjoxNDIyNzYwODg5OkNwMTI1MjoxNjQ4NDg3NTU0ODkzOk1vdW5pcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Mar 2022 19:12:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE MAKASA.PRINT.CCY


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BRANCH.ACCT.OLD
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 46 ] Add $INCLUDE I_CU.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT.CREATED "   ; CALL REM
RETURN

*-----------------------------INITIALIZATIONS------------------------
INITIATE:

    REPORT.ID = 'MAKASA.PRINT.CCY'
    CALL PRINTER.ON(REPORT.ID,'')

    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]

    Path = "/hq/opce/bclr/user/aibz/aib_rj.":COM.CODE
*  Path = "/hq/opce/bclr/user/aibz/aib_rj.orouba"
*  Path = "/hq/opce/bclr/user/aibz/aib_rj.cairo"
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF( FN.BR,F.BR)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF( FN.ACC,F.ACC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF( FN.CUR,F.CUR)

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    I         = 1
    K         = 1
    J         = 1
    TOT.AMT   = 0
    TOT.AMT.1 = 0
    TOT.AMT.2 = 0
RETURN

*------------------------RED FORM TEXT FILE----------------------
PROCESS:

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CUST              = ''
            OUR.ID            = FIELD(Line,"|",24)
            RESULT.CODE       = FIELD(Line,"|",16)
            CURR              = FIELD(Line,"|",6)
            AMT.CLR           = FIELD(Line,"|",10)
            IF OUR.ID THEN
                IF RESULT.CODE EQ "1" THEN

                    CALL F.READ( FN.BR, OUR.ID, R.BR, F.BR, ETEXT)
                    LOCAL      = R.BR<EB.BILL.REG.LOCAL.REF>
                    ACC.NO     = LOCAL<1,BRLR.LIQ.ACCT>
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST)
                    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                    FN.F.ITSS.ACCOUNT = ''
                    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                    CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                    CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
                    CALL F.READ( FN.CUS, CUST, R.CUS, F.CUS, ETEXT1)
                    CUST.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
                    CHQ.NO    = LOCAL<1,BRLR.BILL.CHQ.NO>

*=========================GET CURRENCY==================================

                    T.SEL = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ " : CURR
                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                    IF KEY.LIST THEN
                        FIN.CURR = KEY.LIST<1>
                    END ELSE
                        FIN.CURR = 'EGP'
                    END
                    CURR.NAME  = FIN.CURR

*======================================================================

                    MAT.DATE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                    MATURITY  = MAT.DATE[7,2]:"/":MAT.DATE[5,2]:"/":MAT.DATE[1,4]
                    COMPP     = R.BR<EB.BILL.REG.CO.CODE>
                    IF COMPP EQ COMP THEN
                        GOSUB WRITE
                        K= K + 1
                        TOT.AMT.1 = TOT.AMT.1 + AMT.CLR
                    END

                END
                IF RESULT.CODE EQ "2" THEN

                    CALL F.READ( FN.BR, OUR.ID, R.BR, F.BR, ETEXT)
                    LOCAL      = R.BR<EB.BILL.REG.LOCAL.REF>
                    ACC.NO     = LOCAL<1,BRLR.LIQ.ACCT>
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST)
                    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                    FN.F.ITSS.ACCOUNT = ''
                    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                    CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                    CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
                    CALL F.READ( FN.CUS, CUST, R.CUS, F.CUS, ETEXT1)
                    CUST.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
                    CHQ.NO    = LOCAL<1,BRLR.BILL.CHQ.NO>

*=========================GET CURRENCY==================================

                    T.SEL = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ " : CURR
                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                    IF KEY.LIST THEN
                        FIN.CURR = KEY.LIST<1>
                    END ELSE
                        FIN.CURR = 'EGP'
                    END
                    CURR.NAME  = FIN.CURR

*======================================================================

                    MAT.DATE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                    MATURITY  = MAT.DATE[7,2]:"/":MAT.DATE[5,2]:"/":MAT.DATE[1,4]
                    COMPP     = R.BR<EB.BILL.REG.CO.CODE>


                    IF COMPP EQ COMP THEN
                        GOSUB WRITE
                        J= J + 1
                        TOT.AMT.2 = TOT.AMT.2 + AMT.CLR

                    END
                END
            END
            I = I + 1
        END ELSE
            EOF = 1
        END

    REPEAT

    NO.CHQ.1= K - 1
    NO.CHQ.2= J - 1
    TOT.CHQ = NO.CHQ.1 + NO.CHQ.2
    TOT.AMT = TOT.AMT.1 + TOT.AMT.2
    XX2 = SPACE(132)
    XX1 = SPACE(132)
    XX4 = SPACE(132)
    XX5 = SPACE(132)
    XX1<1,1>[1,132]  = ""
    XX2<1,1>[1,12]   = "������ ��� ������� = "
    XX2<1,1>[25,15]  = TOT.CHQ
    XX2<1,1>[41,15]  = "�������� = "
    XX2<1,1>[57,15]  = TOT.AMT
    XX4<1,1>[1,12]   = "��� ������� ������� = "
    XX4<1,1>[25,15]  = NO.CHQ.1
    XX4<1,1>[41,15]  = "�������� = "
    XX4<1,1>[57,15]  = TOT.AMT.1
    XX5<1,1>[1,12]   = "��� ������� ������� = "
    XX5<1,1>[25,15]  = NO.CHQ.2
    XX5<1,1>[41,15]  = "�������� = "
    XX5<1,1>[57,15]  = TOT.AMT.2

    PRINT XX1<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX2<1,1>

RETURN
*******************************************************
WRITE:

    XX3 = SPACE(132)
    XX3<1,1>[1,15]   = CHQ.NO
    XX3<1,1>[20,15]  = MATURITY
    XX3<1,1>[40,15]  = AMT.CLR
    XX3<1,1>[60,15]  = CURR.NAME
    XX3<1,1>[80,30]  = CUST.NAME
    PRINT XX3<1,1>


RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:

*Line [ 239 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"������� ���� ��� ���� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":"��� �����":SPACE(10):"����� ������":SPACE(10):"������":SPACE(10):"������":SPACE(10):"��� ������"

    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
END
