* @ValidationCode : MjotOTc5ODE4NDk4OkNwMTI1MjoxNjQwNzA3NTc0MTIyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:06:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
SUBROUTINE GET.M(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*-----------------------------------
    ARG = R.NEW(LD.CATEGORY)
*Line [ 30 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,ARG,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,ARG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>

    WS.DESC = ""
    WS.D1   = CATEG
    WS.D2   = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    TEXT = WS.D2 ; CALL REM
    IF WS.D2 EQ "1M" THEN
        WS.D3 = " ��� "
    END
    IF WS.D2 EQ "2M" THEN
        WS.D3 = " ����� "
    END
    IF WS.D2 EQ "3M" THEN
        WS.D3 = "3 ���� "
    END
    IF WS.D2 EQ "6M" THEN
        WS.D3 = " 6 ���� "
    END
    IF WS.D2 EQ "9M" THEN
        WS.D3 = " 9 ���� "
    END
    IF WS.D2 EQ "12M" THEN
        WS.D3 = " ��� "
    END

    IF WS.D3 EQ '' THEN
        WS.DESC = WS.D1
    END ELSE
        WS.DESC = WS.D1 :" ���� ": WS.D3
    END
    TEXT = WS.DESC ; CALL REM
    ARG = WS.DESC
RETURN
END
