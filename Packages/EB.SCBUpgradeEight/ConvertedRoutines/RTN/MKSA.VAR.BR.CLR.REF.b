* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
*--WAGDY-MOUNIR-LABIB---------------------------------------------------------
*-----------------------------------------------------------------------------
    SUBROUTINE MKSA.VAR.BR.CLR.REF

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.FCY.REF

**---------------------------------------------------------------------**

    EOF = ''
    FN.REF = 'F.SCB.BR.FCY.REF' ; F.REF = '' ; R.REF = ''
    CALL OPF(FN.REF,F.REF)

**---------------------------------------------------------------------**

*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    MAIN.MULTI = DCOUNT(R.NEW(SCB.BR.BR.CODE),@VM)
    FOR I = 1 TO MAIN.MULTI

*Line [ 64 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        SUB.VAL = DCOUNT(R.NEW(SCB.BR.OUR.REFERENCE)<1,I>,@SM)
        FOR X = 1 TO SUB.VAL

            OUR.REF = R.NEW(SCB.BR.OUR.REFERENCE)<1,I,X>
            CALL F.READ(FN.REF,OUR.REF,R.REF,F.REF,ETEXT1)

            R.REF<BR.REF.BR.BRANCH>     = R.NEW(SCB.BR.BR.CODE)<1,I>
            R.REF<BR.REF.REF.NO>        = R.NEW(SCB.BR.REF.NO)
            R.REF<BR.REF.OUR.REFERENCE> = R.NEW(SCB.BR.OUR.REFERENCE)<1,I,X>
            R.REF<BR.REF.BOOK.DATE>     = R.NEW(SCB.BR.BOOK.DATE)
            R.REF<BR.REF.VALUE.DATE>    = R.NEW(SCB.BR.VALUE.DATE)
            R.REF<BR.REF.CUR>           = R.NEW(SCB.BR.CURR)<1,I,X>
            R.REF<BR.REF.AMT>           = R.NEW(SCB.BR.AMOUNT)<1,I,X>
            R.REF<BR.REF.BANK>          = R.NEW(SCB.BR.BANK)<1,I,X>
            R.REF<BR.REF.R.REASON>      = R.NEW(SCB.BR.RETURN.REASON)<1,I,X>
            R.REF<BR.REF.OLD.NO>        = R.NEW(SCB.BR.OLD.NO)<1,I,X>

            CALL F.WRITE(FN.REF ,OUR.REF, R.REF)

        NEXT X
    NEXT I
    RETURN
**-------------------------------------------------------------------**
END
