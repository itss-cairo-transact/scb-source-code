* @ValidationCode : MjotNDU0OTYwNjA4OkNwMTI1MjoxNjQwNzA5NTQ2Mjg1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:39:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>57</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE HOLD.CD.MATURE


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

*TEXT='HOLD CD MATURE';CALL REM

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "AC.LOCKED.EVENTS"
    OFS.OPTIONS="SCB"

***OFS.USER.INFO = "/"
**   COMP = ID.COMPANY
**   COM.CODE = COMP[8,2]
**OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP


    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
********************
    DAT = TODAY
    FN.LD.LOANS.AND.DEPOSITS = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD.LOANS.AND.DEPOSITS = '' ; R.LD.LOANS.AND.DEPOSITS = ''
    CALL OPF(FN.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS)
    CALL CDT('',DAT,'-1W')
**********************************
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH FIN.MAT.DATE GT ":DAT:" AND FIN.MAT.DATE LE ":TODAY:" AND BLOCK.PURPOSE NE ''":" AND CATEGORY GE 21013  "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
*******************************************************
        CALL F.READ('F.LD.LOANS.AND.DEPOSITS',KEY.LIST<I>,R.TERM,VF.LD.LOANS.AND.DEPOSITS, ER.MSG)
        CD.AMT=R.TERM<LD.DRAWDOWN.ISSUE.PRC>
        CD.ACCT=R.TERM<LD.PRIN.LIQ.ACCT>
        CD.HOLD.DATE=TODAY
        CD.INP=R.TERM<LD.INPUTTER><1,1>
*******************************************************
        COMMA = ","
        OFS.MESSAGE.DATA =  "ACCOUNT.NUMBER=":CD.ACCT:COMMA
        OFS.MESSAGE.DATA := "DESCRIPTION=":KEY.LIST<I>:COMMA
        OFS.MESSAGE.DATA := "LOCKED.AMOUNT=":CD.AMT:COMMA
        OFS.MESSAGE.DATA := "FROM.DATE=":CD.HOLD.DATE
*------------------------------------
        COMP              = R.TERM<LD.CO.CODE>
        COM.CODE          = COMP[8,2]
        OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
*-------------------------------------------------
        GOSUB CREATE.FILE
    NEXT I
RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.TERM<LD.DATE.TIME>
    INPP=R.TERM<LD.INPUTTER>
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    CALL ALLOCATE.UNIQUE.TIME(THE.TIME)
    INPUT.FILE.NAME = "AMTBLK":TODAY:THE.TIME
    WRITE OFS.REC ON F.OFS.IN, INPUT.FILE.NAME ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

RETURN
*--------------------------------------------------------
END
