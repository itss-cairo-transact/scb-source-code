* @ValidationCode : MjotNjE2NzI1ODM5OkNwMTI1MjoxNjQwNzA3ODIxMTUzOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:10:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
SUBROUTINE GET.NAME.TO(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-28
    $INSERT I_FT.LOCAL.REFS
*----------------------------------------------
    LANG      = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE>
    BRANCH    = R.NEW(FT.CO.CODE)
    FT.ID     = ARG
    DEPT.CODE = R.USER<EB.USE.DEPARTMENT.CODE>

    IF DEPT.CODE EQ '99' THEN
        IF LANG EQ '2' THEN
            ARG = "���� ������"
        END
        IF LANG EQ '1' THEN
            ARG = "ALL BRANCHES"
        END
    END ELSE
        FN.CO = "F.COMPANY" ; F.CO = ""
        CALL OPF(FN.CO, F.CO)

        COMP   = R.NEW(FT.CO.CODE)
        CALL F.READ(FN.CO,COMP,R.CO,F.CO,ER.CO)

        NUM.ID = COMP[8,2]
        IF NUM.ID[1,1] EQ 0 THEN
            NUM.ID = COMP[9,2]
        END
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*        CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,NUM.ID,ARG)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,NUM.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
ARG=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

        IF LANG EQ '2' THEN
            ARG = FIELD(ARG,'.',2)
        END
        IF LANG EQ '1' THEN
            ARG = FIELD(ARG,'.',1)
        END
    END
*--------------------------------------------
RETURN
END
