* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.FREE.CHARGE(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.FREE.CHARGE

    COMP = ID.COMPANY

    KEY.LIST="" ;SELECTED="" ;ER.MSG=""
    END.DATE = TODAY
   * CALL CDT('EG',END.DATE,"+1W")

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 "
    T.SEL := " AND AMOUNT NE 0 AND STATUS NE 'LIQ' AND ( LG.KIND LIKE F..."
    T.SEL := " OR LG.KIND LIKE A... ) AND FIN.MAT.DATE GT ":TODAY
    T.SEL := " AND ACTUAL.EXP.DATE GT ": TODAY
    T.SEL := " AND CO.CODE EQ ":COMP
    T.SEL := " AND END.COMM.DATE LE ":END.DATE
    T.SEL := " BY END.COMM.DATE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**************************************************
    IF SELECTED >= 1 THEN
        LP     = 1
        FOR ENQ.LP = 1 TO SELECTED
***********************
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CUSTOMER.ID,KEY.LIST<ENQ.LP>,MYCUS)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,KEY.LIST<ENQ.LP>,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYCUS=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>
            FN.FREE = 'F.SCB.LG.FREE.CHARGE' ;F.FREE = '' ; R.FREE = '';F.FREE=''
            CALL OPF(FN.FREE,F.FREE)
            CALL F.READ(FN.FREE,MYCUS,R.FREE,F.FREE,READ.ERR)
            FREE.DATE=R.FREE<LG.FR.EXP.DAT>
            FREE.CUST=R.FREE<LG.FR.SCB.CUSTOMER>

            IF  FREE.CUST EQ '' THEN
                ENQ<2,LP> = '@ID'
                ENQ<3,LP> = 'EQ'
                ENQ<4,LP> = KEY.LIST<ENQ.LP>
                LP = LP + 1
            END ELSE
                IF ENQ.LP = SELECTED AND LP EQ 1 THEN
                    ENQ<2,LP> = '@ID'
                    ENQ<3,LP> = 'EQ'
                    ENQ<4,LP> = "DUMMY"
*                  ENQ.ERROR='NO RECORDS FOUND'
                END
            END
***********************************
        NEXT ENQ.LP
    END ELSE
        ENQ<2,LP> = '@ID'
        ENQ<3,LP> = 'EQ'
        ENQ<4,LP> = "DUMMY"
*        ENQ.ERROR='NO RECORDS FOUND'
    END
    RETURN
END
