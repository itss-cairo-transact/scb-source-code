* @ValidationCode : MjotMTUwOTg1ODY5MzpDcDEyNTI6MTY0MjEwNjQ3MjI1NTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 13 Jan 2022 22:41:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*----------------------------NI7OOOOOOOOOOOOOOOO-------------------------------------------------
SUBROUTINE LG.CONFIS.LETTER.OUT1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE
*Line [ 41 ] add include I_F.COMPANY - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY


    GOSUB CALLDB

**IF MYCODE # '1401' AND MYCODE # '1402' AND MYCODE # '1403' AND  MYCODE # '1407'  THEN
***  TEXT ="NOT.VALID.VERSION" ; CALL REM
**  E = "NOT.VALID.VERSION":" - ": ; CALL ERR ; MESSAGE = 'REPEAT'
** END ELSE

** IF MYCODE = "1401" OR MYCODE EQ '1402' THEN
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB INFO
    GOSUB BODY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

** END

** END
RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CONFIS.LETTER.OUT'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1  = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2  = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1  = LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2  = LOCAL.REF<1,LDLR.BNF.DETAILS,2>
*** THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    THIRD.NO =R.LD<LD.CUSTOMER.ID>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COCODE)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    COCODE=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('COMPANY ':@FM:EB.COM.COMPANY.NAME,COCODE,NAMECO)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COCODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    NAMECO=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    PURPOSE = LOCAL.REF<1,LDLR.IN.RESPECT.OF>
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    CORRNO      =R.LD<LD.CUSTOMER.REF>
    CORDATE     =LOC.REF<1,LDLR.APPROVAL.DATE>
*LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE,1>
    LG.MAT.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    LG.APPROVAL.DATE=LOCAL.REF<1,LDLR.APPROVAL.DATE,1>
    LG.ACCT=LOCAL.REF<1,LDLR.DEBIT.ACCT,1>
    LG.AMT =R.LD<LD.AMOUNT>
* LG.AMT = R.NEW(LD.AMOUNT)
    CUR=R.LD<LD.CURRENCY>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
    FN.F.ITSS.SCB.LG.PARMS = ''
    CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
    CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
    TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    DATZ= TODAY
    ZZ= DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ= LG.APPROVAL.DATE
    QQ = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    REST.AMT = LG.AMT - CONFIS.AMT

    IN.AMOUNT = LG.AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)

*------------------------------------
*  T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":ID.NEW:"..."
*  CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)
    CUR.NOO  =  R.NEW(LD.CURR.NO)-1
    ID2 = ID.NEW : CUR.NOO

    CALL F.READ(FN.LD.H,ID2,R.LD.H,F.LD.H,E1.H)


*   CALL F.READ(FN.LD,KEY.LIST<SELECTED>,R.LD,F.LD,E1)

    LOCAL.REF = R.LD.H<LD.LOCAL.REF>
    OPR.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>

    WW = R.LD.H<LD.DATE.TIME>
    D.TIME = WW[1,6]
    D.TIME=LG.APPROVAL.DATE
    WWW = D.TIME[7,2]:'/':D.TIME[5,2]:"/":D.TIME[1,4]
*-----------------------------------

RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

**    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":"������":"*"
*CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)

*Line [ 195 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    AC.OFICER=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

*Line [ 205 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":MYBRANCH
    PR.HD :="'L'":SPACE(1):STR('_',22)
    PR.HD :="'L'":SPACE(1):"��� ":ZZ
    HEADING PR.HD

RETURN
*==========================================================
INFO:

    LNTH1 = LEN(THIRD.NAME1)+12 ; LNTH2 = LEN(THIRD.NAME2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(THIRD.ADDR1)+12 ; LNTHD2 = LEN(THIRD.ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":"��� ���� ������":SPACE(LNE1):"|"
**  PRINT "|         : ":THIRD.NAME2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ��� : " :NAMECO:SPACE(LNED1):"|"
* PRINT "|         : ":THIRD.ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:" ":TYPE.NAME:""
    PRINT
    PRINT SPACE(17): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "�����  :          ":BENF1
    PRINT SPACE(17): "                  : ":BENF2
    PRINT
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "����� �������  :":THIRD.NAME1
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT

RETURN
*======================================================================
BODY:
*IF MYCODE EQ 1401 THEN
*   PRINT SPACE(5):" ������ ������� ������ " : WWW : " ����� �������� ���� ���� ����� ���� ."
    PRINT SPACE(5):"���� ���� ���� , "
* END ELSE
** IF MYCODE EQ 1402 THEN
* PRINT SPACE(5):" ������ ������� ������ " : WWW : "����� �������� ���� ���� ����� ���� ": CONFIS.AMT :"��"
    PRINT SPACE(5):"�������� ��� ������ ��� ":CORRNO:"������":CORDATE:"����� �� ����"
    PRINT
    PRINT SPACE(5):" ��� ��� ��� ������ ������ ���������� ���� ����� �����"
    PRINT
    PRINT SPACE(5):"����� ������� ���� ":CONFIS.AMT:"����� �� ��� �� ���� ���� �������"
    PRINT
    PRINT SPACE(5):"������ ������ ���� ����� ������ �� ������� ������ ������� �� ���������� ������    "
    PRINT
** END
** END
    PRINT SPACE(2):" ����� ������� ��������� ."
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
RETURN
*================================================================
