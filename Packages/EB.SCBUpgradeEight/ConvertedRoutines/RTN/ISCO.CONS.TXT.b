* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*    PROGRAM ISCO.CONS.TXT
    SUBROUTINE ISCO.CONS.TXT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.CF
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.CS
*==================== create cf and cs text files ========
    OPENSEQ "&SAVEDLISTS&" , "ISCO.CONS.CF.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ISCO.CONS.CF.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ISCO.CONS.CF.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ISCO.CONS.CF CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create ISCO.CONS.CF.TXT File IN &SAVEDLISTS&'
        END
    END
*****
    OPENSEQ "&SAVEDLISTS&" , "ISCO.CONS.CS.TXT" TO CC THEN
        CLOSESEQ CC
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ISCO.CONS.CS.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ISCO.CONS.CS.TXT" TO CC ELSE
        CREATE CC THEN
            PRINT 'FILE ISCO.CONS.CS CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create ISCO.CONS.CS.TXT File IN &SAVEDLISTS&'
        END
    END
*========================= DEFINE VARIABLES =========================
    EOF = ''
    COMP=ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
*============================ OPEN TABLES =========================

    FN.CF  = 'F.SCB.ISCO.CF' ; F.CF = '' ; R.CF = ''
    CALL OPF(FN.CF,F.CF)
    FN.CS  = 'F.SCB.ISCO.CS' ; F.CS = '' ; R.CS = ''
    CALL OPF(FN.CS,F.CS)

    T.SEL  = "SELECT F.SCB.ISCO.CF WITH (@ID LIKE ...":YEAR:NEW.MONTH:") OR (@ID LIKE ...":YEAR:NEW.MONTH:"LC) OR (@ID LIKE ...":YEAR:NEW.MONTH:"LD) "
    T.SEL := "OR (@ID LIKE ...":YEAR:NEW.MONTH:".A)"
    T.SEL := "OR (@ID LIKE ...":YEAR:NEW.MONTH:"L)"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    CALL EB.READLIST(T.SEL, KEY.LIST2, "", SELECTED2, ASD2)

*======================== BB.DATA ===========================

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CF,KEY.LIST<I>, R.CF, F.CF, ETEXT)
            ID.1 = KEY.LIST<I>
            CF.TYPE  = R.CF<ISCO.CF.CF.TYPE>
            AMT.OVER = R.CF<ISCO.CF.AMT.OVERDUE>
            BAL      = R.CF<ISCO.CF.CF.ACCT.BALANCE> * -1
            APPROVAL = R.CF<ISCO.CF.APPROVAL.DATE>
            AMED     = R.CF<ISCO.CF.CF.AMENDMENT.DATE>

            TEST.APP  = APPROVAL[5,2]
            TEST.DAY  = APPROVAL[7,2]
            TEST.AMED = AMED[5,2]
            TEST.AMED.DAY  = AMED[7,2]

            IF (TEST.APP EQ '04' OR TEST.APP EQ '06' OR TEST.APP EQ '09' OR TEST.APP EQ '11') AND  TEST.DAY GT 30 THEN

                TEST.DAY = '30'
            END
            IF (TEST.AMED EQ '04' OR TEST.AMED EQ '06' OR TEST.AMED EQ '09' OR TEST.AMED EQ '11') AND  TEST.AMED.DAY GT 30 THEN

                TEST.AMED.DAY = '30'
            END

            APP.DATE  = APPROVAL[5,2]:TEST.DAY:APPROVAL[1,4]
            AMED.DATE = AMED[5,2]:TEST.AMED.DAY:AMED[1,4]

            IF AMED.DATE LE APP.DATE THEN
                AMED.DATE = ''
            END

            BB.DATA  = R.CF<ISCO.CF.SEG.IDENT>:'|'
            BB.DATA := R.CF<ISCO.CF.DATA.PROV.ID>:'|'
            BB.DATA := R.CF<ISCO.CF.PRE.DATA.PROV.ID>:'|'

            NEW.BR   =  R.CF<ISCO.CF.DATA.PROV.BRANCH.ID>
            NEW.BR   =  FMT(NEW.BR, "R%4")
            BB.DATA := NEW.BR:'|'

            PREV.BRN =  R.CF<ISCO.CF.PRE.DATA.BRANCH.ID>

*NOHA 20171122*
            IF PREV.BRN NE '' THEN
                PREV.BRN =  FMT(PREV.BRN, "R%4")
                BB.DATA := PREV.BRN:'|'
            END
            ELSE
                BB.DATA := '':'|'
            END
***************

            BB.DATA := R.CF<ISCO.CF.CF.ACC.NO>:'|'
            BB.DATA := R.CF<ISCO.CF.CONSENT.STATUS>:'|'
            BB.DATA := R.CF<ISCO.CF.CONSENT.EXPIRE.DATE>:'|'
            BB.DATA := R.CF<ISCO.CF.PRIMARY.CARD.NO>:'|'
            BB.DATA := '':'|'
            BB.DATA := R.CF<ISCO.CF.PRE.PRIMARY.CARD.NO>:'|'
            BB.DATA := APP.DATE:'|'
            BB.DATA := R.CF<ISCO.CF.APPROVAL.AMOUNT>:'|'
            BB.DATA := R.CF<ISCO.CF.AMT.INSTALL>:'|'
            BB.DATA := '':'|'
            BB.DATA := R.CF<ISCO.CF.CURRENCY>:'|'
            BB.DATA := R.CF<ISCO.CF.LIABILITY.IND>:'|'
            BB.DATA := R.CF<ISCO.CF.CF.TYPE>:'|'
            BB.DATA := R.CF<ISCO.CF.TERM.OF.LOAN>:'|'
            BB.DATA := R.CF<ISCO.CF.REPAYMENT.TYPE>:'|'
            BB.DATA := R.CF<ISCO.CF.FIRST.DISBURSE.DATE>:'|'
            BB.DATA := R.CF<ISCO.CF.CF.ACCT.BALANCE>:'|'
            BB.DATA := R.CF<ISCO.CF.LAST.AMT.PAID>:'|'
            BB.DATA := R.CF<ISCO.CF.AMT.OVERDUE>:'|'
            BB.DATA := R.CF<ISCO.CF.NDPD>:'|'
            BB.DATA := R.CF<ISCO.CF.NO.OF.PAYMENTS>:'|'
            BB.DATA := R.CF<ISCO.CF.ASSET.CLASS>:'|'
            BB.DATA := R.CF<ISCO.CF.ACC.STATUS>:'|'
            BB.DATA := R.CF<ISCO.CF.LAST.PAY.RECEIVE.DATE>:'|'
            BB.DATA := AMED.DATE:'|'
            BB.DATA := R.CF<ISCO.CF.CF.SETTLEMENT.DATE>:'|'
            BB.DATA := R.CF<ISCO.CF.PROTEST.NOTIFY>:'|'
            BB.DATA := R.CF<ISCO.CF.PROTEST.NOTIFY.DATE>:'|'
            BB.DATA := R.CF<ISCO.CF.AMT.WRITTEN.OFF>:'|'
            BB.DATA := R.CF<ISCO.CF.REASON.WRITTEN.OFF>:'|'
            BB.DATA := R.CF<ISCO.CF.AMT.FORGIVEN>:'|'
            BB.DATA := R.CF<ISCO.CF.REASON.AMT.FORGIVEN>:'|'

            XX = R.CF<ISCO.CF.ACC.STATUS>
            IF XX EQ 90 THEN
                CLS.DATE = ''
                CLS.DATE = R.CF<ISCO.CF.ACCT.CLOSING.DATE>[5,2]
                CLS.DATE :=R.CF<ISCO.CF.ACCT.CLOSING.DATE>[7,2]
                CLS.DATE :=R.CF<ISCO.CF.ACCT.CLOSING.DATE>[1,4]
            END ELSE
                CLS.DATE = ''
            END

            BB.DATA := CLS.DATE:'|'
            BB.DATA := R.CF<ISCO.CF.CLOSURE.REASON>:'|'
            BB.DATA := R.CF<ISCO.CF.COMMENT>:'|'
            BB.DATA := R.CF<ISCO.CF.SUIT.FIELD.STATUS>:'|'
            BB.DATA := R.CF<ISCO.CF.SUIT.REF.NO>:'|'
            BB.DATA := R.CF<ISCO.CF.SUIT.AMT>:'|'
            BB.DATA := R.CF<ISCO.CF.COURT>:'|'
            BB.DATA := R.CF<ISCO.CF.COURT.LOCATION>:'|'
            BB.DATA := R.CF<ISCO.CF.COURT.DECREE>:'|'
            BB.DATA := R.CF<ISCO.CF.SUIT.DATE>:'|'
            BB.DATA := R.CF<ISCO.CF.DECREE.DATE>:'|'
            BB.DATA := R.CF<ISCO.CF.LEGAL.ACTION.ORIGIN>:'|'
            BB.DATA := R.CF<ISCO.CF.DISPUTE.NO>:'|'
            BB.DATA := R.CF<ISCO.CF.DISPUTE.STATUS>:'|'
            BB.DATA := R.CF<ISCO.CF.TRANS.TYPE.CODE>:'|'
            BB.DATA := R.CF<ISCO.CF.FILLER>:'|'
            BB.DATA := R.CF<ISCO.CF.NOTIFIED.ACCT>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END

    IF SELECTED2 THEN
        FOR J = 1 TO SELECTED2
            CALL F.READ( FN.CS,KEY.LIST2<J>, R.CS, F.CS, ETEXT2)
*****CREATE NEW TEXT****
            BIRTH.DATE = R.CS<ISCO.CS.BIRTH.DATE>
            B.DATE     = BIRTH.DATE[5,2]:BIRTH.DATE[7,2]:BIRTH.DATE[1,4]

            CC.DATA   = R.CS<ISCO.CS.SEG.IDENT>:'|'
            CC.DATA  := R.CS<ISCO.CS.DATA.PROV.ID>:'|'
            NEW.BRN   = R.CS<ISCO.CS.DATA.PROV.BRANCH.ID>
            NEW.BRN   = FMT(NEW.BRN, "R%4")
            CC.DATA :=NEW.BRN:'|'
            CC.DATA :=R.CS<ISCO.CS.CF.ACC.NO>:'|'
            NEW.CUS  =R.CS<ISCO.CS.DATA.PROV.IDENT.CODE>
            NEW.CUS  =FMT(NEW.CUS, "R%16")
            CC.DATA :=NEW.CUS:'|'
            CC.DATA :=R.CS<ISCO.CS.PRE.DATA.PROV.CODE>:'|'
            CC.DATA :=R.CS<ISCO.CS.BUREAU.SBJ.CODE>:'|'
            CC.DATA :=R.CS<ISCO.CS.NATIONAL.ID>:'|'
            NEW.CCR  = R.CS<ISCO.CS.IDENT.CODE.1>
            IF NEW.CCR NE '' THEN
                NEW.CCR = FMT(NEW.CCR, "R%12")
            END
            CC.DATA  :=NEW.CCR:'|'
            CC.DATA  :=R.CS<ISCO.CS.ISSUE.AUTHORITY.1>:'|'
            CC.DATA  :=R.CS<ISCO.CS.IDENT.1.EXPIRE.DATE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.IDENT.CODE.2>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ISSUE.AUTHORITY.2>:'|'
            CC.DATA  :=R.CS<ISCO.CS.IDENT.2.EXPIRE.DATE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.IDENT.CODE.3>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ISSUE.AUTHORITY.3>:'|'
            CC.DATA  :=R.CS<ISCO.CS.IDENT.3.EXPIRE.DATE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.LANGUAGE.ID>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CS.E.FIRST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CS.E.MIDDLE.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CS.E.LAST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CS.A.FIRST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CS.A.MIDDLE.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CS.A.LAST.NAME><1,1>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PRE.CS.E.FIRST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PRE.CS.E.MIDDLE.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PRE.E.LAST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PRE.CS.A.FIRST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PRE.CS.A.MIDDLE.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PRE.CS.A.LAST.NAME>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.TYPE.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.LINE.1.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.LINE.2.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.LINE.3.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.CITY.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.LINE.1.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.LINE.2.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.LINE.3.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.CITY.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.GOV.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.PO.BOX.NO>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.1.ZIP.CODE>:'|'
            CC.DATA  := 'EG':'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.TYPE.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.LINE.1.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.LINE.2.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.LINE.3.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.CITY.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.LINE.1.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.LINE.2.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.LINE.3.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.CITY.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.GOV.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.PO.BOX.NO>:'|'
            CC.DATA  :=R.CS<ISCO.CS.ADD.2.ZIP.CODE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.COUNTRY.2>:'|'
            CC.DATA  :=R.CS<ISCO.CS.TELE.AREA.CODE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.PHONE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.MOBILE>:'|'
            CC.DATA  :=R.CS<ISCO.CS.EMAIL>:'|'
            CC.DATA  :=B.DATE:'|'
            CC.DATA  :=R.CS<ISCO.CS.GENDER>:'|'
            CC.DATA  :=R.CS<ISCO.CS.CITZENSHIP>:'|'
            CC.DATA  :=R.CS<ISCO.CS.MARITAL.STATUS>:'|'
            CC.DATA  :=R.CS<ISCO.CS.OCCUPATION>:'|'
            CC.DATA  :=R.CS<ISCO.CS.EMPLOYER.NAME.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.EMPLOYER.NAME.A>:'|'
            CC.DATA  :=R.CS<ISCO.CS.EMP.COMM.REG.ID>:'|'
            CC.DATA  :=R.CS<ISCO.CS.BUSS.ENTITY.NAME.E>:'|'
            CC.DATA  :=R.CS<ISCO.CS.BUSS.ENTITY.NAME.A>

            WRITESEQ CC.DATA TO CC ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT J
    END

    CLOSESEQ BB
    CLOSESEQ CC
END
