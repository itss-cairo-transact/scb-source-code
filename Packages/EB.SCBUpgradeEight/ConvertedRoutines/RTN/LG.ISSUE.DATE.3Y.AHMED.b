* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating><UPDATED BY M.ELSAYED><27/1/2009>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.ISSUE.DATE.3Y.AHMED(ENQ)
*    PROGRAM LG.ISSUE.DATE.3Y
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

**************************************************
    ISSU.DATE  = TODAY
*   TEXT = "TOD=   " : TODAY ; CALL REM
*  TEXT = "ISSS=   " : ISSU.DATE ; CALL REM


    CALL CDT('', ISSU.DATE , '-1095C')
* TEXT = "ISSS22222=   " : ISSU.DATE ; CALL REM
*  UNMATURED = ISS.DATE
*  FN.NAME = R.ENQ<2>

*   ENQ<2,1> = "VALUE.DATE"
*   ENQ<3,1> = "LE"
*   ENQ<4,1> =  ISSU.DATE
    TEXT = "DATE1=   " : ISSU.DATE ; CALL REM
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF( FN.LD,F.LD)
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH VALUE.DATE LE ISSU.DATE AND CATEGORY EQ 21096 AND AMOUNT NE 0 AND STATUS NE LIQ AND OPERATION.CODE EQ 1401 AND CO.CODE EQ ": COMP
    TEXT = "DATE2 =   " : ISSU.DATE ; CALL REM
    KEY.LIST='';
    SELECTED='';
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = " SELECTED = " : SELECTED ;CALL REM
    IF SELECTED  THEN
        TEXT = " SELECTED2 = " : SELECTED ;CALL REM
        FOR I = 1 TO SELECTED
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<I>
            I = I+1
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
        TEXT = "LASTDATE=   " : ISSU.DATE ; CALL REM
    END
    RETURN
END
