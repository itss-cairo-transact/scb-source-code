* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE LIST.CUSTOMER.2400(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*----------------------------------------
    FN.CUS = "FBNK.CUSTOMER$NAU"  ; F.CUS =""
    CALL OPF(FN.CUS, F.CUS)

    FN.USR = "F.USER"  ; F.USR =""
    CALL OPF(FN.USR, F.USR)
*----------------------------------------
    ZZ = 1
    T.SEL = "SELECT FBNK.CUSTOMER$NAU BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FLG = 0

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.CUS,KEY.LIST<II>, R.CUS, F.CUS, E.CUS)
            INPP = R.CUS<EB.CUS.INPUTTER>
            INPP = FIELD (INPP, '_' , 2)

            CALL F.READ(FN.USR,INPP, R.USR, F.USR, E.USR)
            DEPT.USR = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>

            IF DEPT.USR EQ '2400' THEN
                ENQ<2,ZZ> = '@ID'
                ENQ<3,ZZ> = 'EQ'
                ENQ<4,ZZ> = KEY.LIST<II>
                ZZ++
                FLG = 1
            END
        NEXT II

        IF FLG EQ 0 THEN
            ENQ<2,2> = '@ID'
            ENQ<3,2> = 'EQ'
            ENQ<4,2> = 'DUMMY'
        END
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
*----------------------------------------
    RETURN
END
