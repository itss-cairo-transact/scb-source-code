* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>920</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAST.DAILY.COM.CO

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.TRANS.COM
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.COM
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.TRN.N
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CURR.CODES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.ROUTINE.CHK

*****WRITTEN BY NESSREEN AHMED 23/10/2013*****
***************************************
    F.MAST.DAILY.TRN = '' ;  FN.MAST.DAILY.TRN = 'F.SCB.MAST.DAILY.TRN.N' ; R.MAST.DAILY.TRN = '' ; E1 = ''
    CALL OPF(FN.MAST.DAILY.TRN,F.MAST.DAILY.TRN)

    F.MAST.TRANS = '' ; FN.MAST.TRANS = 'F.SCB.MAST.TRANS.COM' ; R.MAST.TRANS = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.MAST.TRANS,F.MAST.TRANS)


******3/2/2008****************************
    F.RT.CHK = '' ; FN.RT.CHK = 'F.SCB.MAST.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)
******************************************
    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    DATEE = '' ; YEAR1 = '' ; MONTH1= ''  ; YEARN = '' ; YY = ''
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
**    TEXT = 'DATE=':DATEE ; CALL REM
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YEARN = YYYY-1
    END ELSE
        MU = MT-1
        YEARN = YYYY
    END
**************************************
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END
***************************************
    YYDD = YY:MON:'01'
    YEAR1= YEARN
    MONTH1 = MON
    BR = R.USER<EB.USE.DEPARTMENT.CODE>
************3/2/2008**********************************
**** CHK.SEL = "SELECT F.SCB.VISA.ROUTINE.CHK WITH BRANCH EQ ":BR :" AND YEAR EQ ":YEAR1:" AND MONTH EQ ":MONTH1 :" AND DAILY.TRN EQ 'YES' "
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    IF LEN(BR) < 2 THEN
        BRAN = '0':BR
    END ELSE
        BRAN = BR
    END

    KEYID = COMI
***    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
**** IF SELECTED.CHK THEN
****     E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
**** END ELSE
******************************************************
    T.SEL = "SELECT F.SCB.MAST.DAILY.TRN.N WITH POS.DATE GE ":KEYID
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.MAST.DAILY.TRN,KEY.LIST<I>, R.MAST.DAILY.TRN, F.MAST.DAILY.TRN ,E1)
            IF NOT(E1) THEN
** ORG.MSG = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
** DD = DCOUNT(ORG.MSG,VM)
** XX = DD+1
************TO WRITE TO TABLE SCB.MAST.TRANS**************************
                ORG.MSG.CO = '' ; MSG.TYPE.CO = '' ; PROC.CODE.CO = '' ; DAILY.COMB = '' ; CODES.COMB = ''  ; DB.CR.CO = '' ;   REASON.CO.CO = ''
                ORG.MSG.CODE = '' ; MSG.TYPE.CODE = '' ; PROC.CODE.CODE = '' ; DB.CR.CODE = '' ; REASON.CO.CODE = '' ; CODE.TO.USE = ''

                ORG.MSG.F = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE>
*Line [ 129 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(ORG.MSG.F,@VM)
                FOR TT =1 TO DD
                    ORG.MSG.CO   = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE,TT>
                    MSG.TYPE.CO = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE,TT>
                    PROC.CODE.CO = R.MAST.DAILY.TRN<MST.DAILY.PROCESS.CODE,TT>
                    DB.CR.CO =   R.MAST.DAILY.TRN<MST.DAILY.DB.CR.FLAG,TT>
                    REASON.CO.CO=  R.MAST.DAILY.TRN<MST.DAILY.REASON.CODE,TT>

                    DAILY.COMB = ORG.MSG.CO:MSG.TYPE.CO:PROC.CODE.CO:DB.CR.CO
                    N.SEL =  "SELECT F.SCB.VISA.CODES.COM WITH ORG.MSG.TYPE EQ ":ORG.MSG.CO:" AND MSG.TYPE EQ ":MSG.TYPE.CO:" AND PROCESS.CODE EQ ":PROC.CODE.CO:" AND FLAG EQ ":DB.CR.CO
                    KEY.LIST.2=""
                    SELECTED.2=""
                    ER.MSG.2=""

                    CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)

                    F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.CODES.COM' ; R.VISA.CODE = '' ; E3 = '' ; RETRY3 = ''
                    CALL OPF(FN.VISA.CODE,F.VISA.CODE)
                    IF SELECTED.2 THEN
                        FOR RR = 1 TO SELECTED.2
                            CALL F.READ(FN.VISA.CODE, KEY.LIST.2<RR>, R.VISA.CODE, F.VISA.CODE, E3)
                            ORG.MSG.CODE.T = R.VISA.CODE<VICD.COM.ORG.MSG.TYPE>
** TEXT = 'ORG=':ORG.MSG.CODE.T ; CALL REM
*Line [ 153 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DCT = DCOUNT(ORG.MSG.CODE.T,@VM)
                            FOR WW = 1 TO DCT
                                ORG.MSG.CODE   = R.VISA.CODE<VICD.COM.ORG.MSG.TYPE,WW>
                                MSG.TYPE.CODE  = R.VISA.CODE<VICD.COM.MSG.TYPE,WW>
                                PROC.CODE.CODE = R.VISA.CODE<VICD.COM.PROCESS.CODE,WW>
                                DB.CR.CODE = R.VISA.CODE<VICD.COM.FLAG,WW>
                                CODES.COMB = ORG.MSG.CODE:MSG.TYPE.CODE:PROC.CODE.CODE:DB.CR.CODE

                                IF DAILY.COMB = CODES.COMB THEN
                                    CODE.TO.USE = KEY.LIST.2<RR>
                                END ELSE
                                END
                                IF KEY.LIST<I> = '4042380010006078200912' THEN
                                 **   TEXT = 'CODCOM=':CODES.COMB ; CALL REM
                                 **   TEXT = 'CO=':CODE.TO.USE ; CALL REM
                                END
                            NEXT WW
                        NEXT RR
                        CALL F.READ(FN.MAST.TRANS, KEY.LIST<I>, R.MAST.TRANS, F.MAST.TRANS, E4)
                        IF NOT(E4) THEN
                            TRANS.CODE = R.MAST.TRANS<MAST.COM.TRANS.CODE>
*Line [ 175 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            YY= DTC+1
                            R.MAST.TRANS<MAST.COM.TRANS.CODE,YY>= CODE.TO.USE
                            R.MAST.TRANS<MAST.COM.TRANS.CURR,YY>= R.MAST.DAILY.TRN<MST.DAILY.BILL.CURR,TT>
                            R.MAST.TRANS<MAST.COM.TRANS.AMT,YY>= R.MAST.DAILY.TRN<MST.DAILY.BILL.AMT,TT>
                            R.MAST.TRANS<MAST.COM.POS.DATE,YY>= R.MAST.DAILY.TRN<MST.DAILY.POS.DATE,TT>
                            CALL F.WRITE(FN.MAST.TRANS,KEY.LIST<I>, R.MAST.TRANS)
                            CALL JOURNAL.UPDATE(KEY.LIST<I>)
                        END ELSE
                            TRANS.CODE = R.MAST.TRANS<MAST.COM.TRANS.CODE>
*Line [ 186 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            YY= DTC+1
                            R.MAST.TRANS<MAST.COM.BRANCH.NUMBER> = R.MAST.DAILY.TRN<MST.DAILY.CARD.BR>
                            R.MAST.TRANS<MAST.COM.CUST.NAME>= R.MAST.DAILY.TRN<MST.DAILY.CUST.NAME>
                            R.MAST.TRANS<MAST.COM.CUST.ACCT>= R.MAST.DAILY.TRN<MST.DAILY.CUST.ACCT>
                            R.MAST.TRANS<MAST.COM.TRANS.CODE,YY>= CODE.TO.USE
                            R.MAST.TRANS<MAST.COM.TRANS.CURR,YY>=  R.MAST.DAILY.TRN<MST.DAILY.BILL.CURR,TT>
                            R.MAST.TRANS<MAST.COM.TRANS.AMT,YY>= R.MAST.DAILY.TRN<MST.DAILY.BILL.AMT,TT>
                            R.MAST.TRANS<MAST.COM.POS.DATE,YY>= R.MAST.DAILY.TRN<MST.DAILY.POS.DATE,TT>
                            CALL F.WRITE(FN.MAST.TRANS,KEY.LIST<I>, R.MAST.TRANS)
                            CALL JOURNAL.UPDATE(KEY.LIST<I>)
                        END   ;* END OF IF NOT(E4)
                    END ELSE  ;** END OF SELECTED.2
                    END       ;** END OF SELECTED.2
**********************************************************************
                NEXT TT
            END     ;**IF NOT(E2)
        NEXT I

    END   ;**END OF SELECTED***

**      CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
**      R.RT.CHK<RT.CHK.BRANCH> = BR
**      R.RT.CHK<RT.CHK.YEAR> = YEAR1
**      R.RT.CHK<RT.CHK.MONTH> = MONTH1
**     R.RT.CHK<RT.CHK.DAILY.TRN> = 'YES'
**     CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
**     CALL JOURNAL.UPDATE(KEYID)

    RETURN
END
