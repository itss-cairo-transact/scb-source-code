* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>600</Rating>
*-----------------------------------------------------------------------------

** ----- ?.??.2002 Panos TEMENOS -----
** ----- 6.06.2002 Pawel TEMENOS -----
SUBROUTINE GET.TELLER.TRN.DESC( TRANS.ID, VER.NAME)

* TO GET VERSION NAME ACCOURDING TO TRANS.ID (TRANSACTION.CODE)

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

IF TRANS.ID = 01  THEN VER.NAME = 'BUY'
IF TRANS.ID = 23 THEN VER.NAME = 'SCB.BUYFCY'
IF TRANS.ID = 24  THEN VER.NAME = 'SCB.SELLFCY'
IF TRANS.ID = 2  THEN VER.NAME = 'CASH.WDRAWAL.CBE'
IF TRANS.ID = 901 THEN VER.NAME = 'CASH.DPST.CBE'
IF TRANS.ID = 5  THEN VER.NAME = 'CASH.WITHDRAWAL'
RETURN
END
