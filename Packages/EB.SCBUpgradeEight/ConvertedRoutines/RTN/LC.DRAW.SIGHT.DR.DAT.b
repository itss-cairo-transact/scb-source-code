* @ValidationCode : MjoxMTI1MzUwMDQ0OkNwMTI1MjoxNjQyMTA1MDY5OTE5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 13 Jan 2022 22:17:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-84</Rating>
*-----------------------------------------------------------------------------
*** ����� ��� SIGHT PAYMENT ***
*** IMPORT DRAWINGS (SIGHT PAYMENT) ***
***=================================

SUBROUTINE LC.DRAW.SIGHT.DR.DAT

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_DR.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='LC.DRAW.SIGHT.DR.DAT'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    BT.ID = COMI
    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    FN.DR.HIS = 'FBNK.DRAWINGS$HIS' ; F.DR.HIS = ''
    CALL OPF(FN.DR.HIS,F.DR.HIS)

    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)
    DEBIT.AC = R.DR<TF.DR.DRAWDOWN.ACCOUNT>
    TEXT = "DEB.AC : " : DEBIT.AC ; CALL REM
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,DEBIT.AC,DRAWER.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,DEBIT.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    DRAWER.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>

*UPDATED BY HYTHAM ---UPGRADING R15---2016-03-07---
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,DEBIT.AC,AC.COMP)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,DEBIT.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
    BRANCH.ID  = AC.COMP[8,2]
******
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
* **   CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,DEBIT.AC,BRANCH.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,DEBIT.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    BRANCH.ID=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 103 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF1)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ " " THEN
        CUST.ADDRESS = "���� ��������� ������ "
    END
    CATEG.ID  = DEBIT.AC[11,4]
*Line [ 132 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,DEBIT.AC,CUR.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,DEBIT.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 146 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>

    AMOUNT    = R.DR<TF.DR.REIMBURSE.AMOUNT>
    DR.RATE   = R.DR<TF.DR.RATE.BOOKED>
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT    = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
*   BYAN       = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES.DEBIT>
    BYAN       = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE.DEBIT>
    NO.OF.DAYS = R.DR<TF.DR.NUMBER.OF.DAYS>
* DAT        = R.DR<TF.DR.VALUE.DATE>
*  DAT        = R.DR<TF.DR.MATURITY.REVIEW>
    DAT        = R.DR<TF.DR.LOCAL.REF><1,DRLR.NO.DAY>
    TEXT = DAT ; CALL REM
    V.DATE     = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    ID.HIS = COMI:';1'
    CALL F.READ(FN.DR.HIS,ID.HIS,R.DR.HIS,F.DR.HIS,ERR)
    INPUTTER = R.DR.HIS<TF.DR.INPUTTER>
    INP   = FIELD(INPUTTER,'_',2)
    AUTH  = R.DR.HIS<TF.DR.AUTHORISER>
    AUTHI = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX20 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX21 = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10 = SPACE(132)  ; XX11  = SPACE(132) ; XX12  = SPACE(132)
*--------------------------------------------------------------
    IF CATEG.ID EQ 1512 THEN
        LOAN.AMT = R.DR<TF.DR.LOCAL.REF><1,DRLR.MARG.AMOUNT>
        DAT1= R.DR<TF.DR.LOCAL.REF><1,DRLR.NO.DAY>
        LOAN.DATE = DAT1[7,2]:'/':DAT1[5,2]:"/":DAT1[1,4]
        XX11<1,1>[3,15]  = '���� �����     : ':' ':LOAN.AMT
        XX11<1,1>[45,35] = '������ ����� �� : ':' ':LOAN.DATE
    END
*--------------------------------------------------------------
    XX1<1,1>[3,35]   = CUST.NAME
    XX2<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]   = '������     : '
    XX<1,1>[59,15]   = AMOUNT

    IF DR.RATE EQ '' THEN
        DR.RATE = 0
    END

    XX21<1,1>[45,15]   = '����� :' : DR.RATE

    XX1<1,1>[45,15]  = '��� ������ : '
    XX1<1,1>[59,15]  = DEBIT.AC

    XX2<1,1>[45,15]  = '��� ������ : '
    XX2<1,1>[59,15]  = CATEG

    XX3<1,1>[45,15]  = '������     : '
    XX3<1,1>[59,15]  = CUR

    XX20<1,1>[45,20]  = "����� ����� :": V.DATE

    XX6<1,1>[1,15]   = '������'
    XX7<1,1>[1,15]   = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15]  = BT.ID
    TEXT = INP ; CALL REM
    TEXT = AUTHI ; CALL REM
    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15]  = INP
*-------------------------------------------
    XX10<1,1>[3,35] = '��������       : ' : BYAN
*-------------------------------------------
    XX8<1,1>[3,35]  = '������ ������� : '
    XX9<1,1>[3,35] = OUT.AMT
    XX12<1,1>[3,35] = "����� ����� ����� �� " : V.DATE

*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":"������ ������ ":' - ':"SIGHT.PAYMENT.DATE"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX21<1,1>
    PRINT XX<1,1>
    PRINT XX20<1,1>
* PRINT XX11<1,1>
    PRINT XX10<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX12<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
