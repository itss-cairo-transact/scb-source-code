* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE LG.AMAMTDATE.1234.BENF

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        CUST=R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>
        IF MYCODE='1234' AND CUST='CUS' THEN
*Line [ 47 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 48 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            FOR II = 1 TO 2
                GOSUB INITIATE
                GOSUB PRINT.HEAD
                GOSUB BODY
                CALL PRINTER.OFF
                CALL PRINTER.CLOSE(REPORT.ID,0,'')
            NEXT II
        END
    END  ELSE
        IF ID.NEW EQ '' THEN
*Line [ 59 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 61 ] Adding amr. in order to call another routine - ITSS - R21 Upgrade - 2022-01-13
            GOSUB CALLDB
            IF MYCODE='1234' AND CUST='CUS' THEN
                FOR II = 1 TO 2
                    GOSUB INITIATE
                    GOSUB PRINT.HEAD
                    GOSUB BODY
                    CALL PRINTER.OFF
                    CALL PRINTER.CLOSE(REPORT.ID,0,'')
                NEXT II
            END ELSE
                MYID=MYCODE:'.':MYTYPE
                OPER.CODE=MYID
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.1234.BENF'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN   YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN   CALL TXTINP(YTEXT, 8, 22, "12", "A")

    IF ID.NEW THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
**  THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CUST=LOCAL.REF<1,LDLR.APPROVAL>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    CURRNO = R.LD<LD.CURR.NO>
    CURRNO1= CURRNO-1
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.ISSUE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE =DATE.ISSUE[7,2]:"/":DATE.ISSUE[5,2]:"/":DATE.ISSUE[1,4]
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
*  DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
*********
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
***********
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME.2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CUR,CRR.DESC)
F.ITSS.CURRENCY.PARAM = 'F.CURRENCY.PARAM'
FN.F.ITSS.CURRENCY.PARAM = ''
CALL OPF(F.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM)
CALL F.READ(F.ITSS.CURRENCY.PARAM,CUR,R.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM,ERROR.CURRENCY.PARAM)
CRR.DESC=R.ITSS.CURRENCY.PARAM<EB.CUP.CCY.NAME>

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT AND FIN MAT DATE************************************
***************************************************************************
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT =  R.LD<LD.AMOUNT.INCREASE> - R.LD<LD.AMOUNT>
        TEXT = OLD.AMOUNT :'-OLD.AMOUNT';CALL REM
        AMT.INC = R.LD<LD.AMOUNT.INCREASE>
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT= R.OLD(LD.AMOUNT)
            AMT.INC = R.LD<LD.AMOUNT.INCREASE>
        END
    END
    IF ID.NEW EQ '' THEN
        OLD.FIN = ''
        FIN.OLD = ''
    END ELSE
        IF ID.NEW THEN
            OLD.FIN=R.OLD(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
        END
    END

    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END
**********************************************************************************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>
**********************************************************************

    RETURN
*===============================================================
PRINT.HEAD:
    MYID = MYCODE:'.':MYTYPE
    OPER.CODE=MYID
*Line [ 221 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
*Line [ 231 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YY = FIELD(BRANCH,'.',2)
    PR.HD  ="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):"�������":":":XX
    PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(28):"������ ���":"*":EXT.NO:"*":"*":"�����"
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ��� � �   : ":LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "�������������    : ": " **":OLD.AMOUNT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
*    PRINT SPACE(20):"������ ������    : ":ISSUE.DATE
*   PRINT
    PRINT SPACE(20): "������� � �����  : ":THIRD.NAME
*************************
    IF THIRD.NAME.2 THEN
        PRINT SPACE(5):THIRD.NAME.2
    END ELSE
        PRINT SPACE(5):""
    END
*//////////////////////////////////////////////////////
    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(6):"�������� ��� ���� ������ ������ ������ � ���� ��� ��� �������"
    PRINT
    PRINT SPACE(2):"����� ��� ���� ������ ��� ���� ���� ������ ������ ������ �����":"*":AMT.INC:"*" :CRR.DESC
    PRINT
    PRINT SPACE(2):"����� ����������� �����":"*":NEW.AMOUNT:"*":CRR.DESC:" ":"������������� ���":FIN.DATE
    PRINT
    PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
    PRINT
    PRINT SPACE(2):"���� ��� ��� �������� ����� ���� ������ ������� �� �����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT
    IN.AMOUNT=NEW.AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 266 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 268 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR.DESC
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
    PRINT SPACE(2):"�� ����� �����":" ":OUT.DATE:".":" "
    PRINT
    PRINT SPACE(2):"���� �� ����� ���� ��� ������ ��� ��� ������� ���� ��������"
    PRINT
    PRINT SPACE(2): "�� ����� ���� ����� ��� ������ ����� ���� ������ ."
    PRINT;PRINT
    PRINT SPACE(4):"����� ����� ��� ���� ������ ������� �� ���� ������ ��� ����� ���"
    PRINT
    PRINT SPACE(2):"������ ����� ��� ������� ."
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT ;PRINT
    IF II = '1' THEN
        PRINT SPACE(5): "���� ��� : ":THIRD.NAME
*************************
        IF THIRD.NAME.2 THEN
            PRINT SPACE(5):THIRD.NAME.2
        END ELSE
            PRINT SPACE(5):""
        END
        PRINT SPACE(15):THIRD.ADDR1
        PRINT SPACE(15):"����� ������ ��������"
    END ELSE
        PRINT;PRINT;PRINT;PRINT
    END
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT SPACE(5):"LG.AMAMTDATE.1234.BENF"
*Line [ 303 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==========================================================
