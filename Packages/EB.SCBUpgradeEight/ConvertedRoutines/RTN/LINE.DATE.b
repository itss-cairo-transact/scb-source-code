* @ValidationCode : MjotNzU4MzQwOTI2OkNwMTI1MjoxNjQxMzY5NzUzNjE4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 05 Jan 2022 10:02:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE LINE.DATE(REQ.DATE)
************************************************************************
***********by Mahmoud Elhawary ** 5/1/2009 *****************************
************************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE

************************************************************************
*If the date required is not today
*and the date required is end of month
*and last day of month is holiday then
*make the date processed for LINE (line date) equal last day of month
*otherwise if date required equal day before holiday then
*make the line date equal the day before next working day
************************************************************************

    TDD = ''
    TDX = TODAY
    TDL = REQ.DATE
    IF REQ.DATE NE TDX THEN
        CALL CDT("",TDL,'+1W')
        TDR = TDL
        TDN = TDL
        CALL CDT("",TDR,'-1W')
        CALL CDT("",TDN,'-1C')
        TDLMN = TDL[5,2]
        TDRMN = TDR[5,2]
        IF TDLMN NE TDRMN THEN
            TDREM = TDL[1,4]:TDLMN:'01'
            CALL CDT("",TDREM,'-1C')
            TDD = TDREM
        END ELSE
            IF TDR NE TDN THEN
                TDD = TDN
            END ELSE
                TDD = REQ.DATE
            END
        END
    END ELSE
        TDD = TDX
    END
    REQ.DATE = TDD
RETURN
