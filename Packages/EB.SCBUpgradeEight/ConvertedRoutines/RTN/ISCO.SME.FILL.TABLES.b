* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>3177</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM ISCO.SME.FILL.TABLES
     SUBROUTINE ISCO.SME.FILL.TABLES

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CF
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE

*=========== CREATE AND OPEN THE TEXT FILE CONS.CF.OUT ============

    OPENSEQ "&SAVEDLISTS&" , "SME.CF.OUT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SME.CF.OUT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SME.CF.OUT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SME.CF.OUT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create SME.CF.OUT File IN &SAVEDLISTS&'
        END
    END

*=========== CREATE AND OPEN THE TEXT FILE CONS.CS.OUT =============

    OPENSEQ "&SAVEDLISTS&" , "SME.CS.OUT" TO CC THEN
        CLOSESEQ CC
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SME.CS.OUT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SME.CS.OUT" TO CC ELSE
        CREATE CC THEN
            PRINT 'FILE SME.CS.OUT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create SME.CS.OUT File IN &SAVEDLISTS&'
        END
    END

    EOF = ''

*=========== OPEN THE CS AND CF TABLES==================
    F.CF = "F.SCB.ISCO.SME.CF"; FVAR.CF=''; R.CF = ''; FN.CF = ''
    F.CS = "F.SCB.ISCO.SME.CS"; FVAR.CS=''; R.CS = ''; FN.CS = ''
    R.CFF = '' ; R.CSS = ''
    OPEN F.CF TO FVAR.CF ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    OPEN F.CS TO FVAR.CS ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END


*====================== OPEN THE TABLES ===========================
    FN.CU  = 'FBNK.CUSTOMER'              ; F.CU = ''      ; R.CU = ''
    CALL OPF( FN.CU,F.CU)

    FN.ACC  = 'FBNK.ACCOUNT'              ; F.ACC = ''     ; R.ACC = ''
    CALL OPF( FN.ACC,F.ACC)

    FN.LMT  = 'FBNK.LIMIT'                ; F.LMT = ''     ; R.LMT = ''
    CALL OPF( FN.LMT,F.LMT)

    FN.CUS.ACC  = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = ''
    CALL OPF( FN.CUS.ACC,F.CUS.ACC)

    FN.CBE  = 'F.SCB.CREDIT.CBE'          ; F.CBE = ''     ; R.CBE = ''; R.CBE2 = ''
    CALL OPF( FN.CBE ,F.CBE)

    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS'  ; F.LD    = ''   ; R.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    Y.CUS.ACC = ''; R.FILL.CF    = ''; ID.CF = ''
    R.FILL.CS = ''; R.FILL.Close = ''; R.FILL.CL.S = ''
    ID.CS = '';     TYPE  = ''
    R.CFT = ''
    MONTH = 0;      YEAR = 0;       NEW.MONTH = 0
    NDPD = 0
    ISLAMIC.IND = ''

*========================== SOME GLOBAL VARIABLES ===================
    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    MONTH.B4   = MONTH
    YEAR       = TODAY.DATE[1,4]
    DAY        = TODAY.DATE[7,2]

    ISSUE.YEAR = YEAR - 1
    IF MONTH EQ 1 THEN
        NEW.MONTH     = 12
        NEW.MONTH.B4  = 11
        YEAR          = YEAR - 1
    END
    ELSE
        NEW.MONTH     = MONTH - 1
        NEW.MONTH.B4  = MONTH.B4 - 2
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
    IF LEN(NEW.MONTH.B4) EQ 1 THEN
        NEW.MONTH.B4 = '0':NEW.MONTH.B4
    END
    CLOSED.ACCT.DATE = YEAR:NEW.MONTH.B4
    ISSUE.DATE = ISSUE.YEAR:MONTH:DAY

*=================READ FROM CUTOMER WITH PERSONS CODE ===============
    T.SEL  = "SELECT FBNK.CUSTOMER WITH NEW.SECTOR NE '4650'"
    T.SEL := " AND (POSTING.RESTRICT NE 99 AND POSTING.RESTRICT NE 90)"
    T.SEL := " AND ACTIVE.CUST EQ 'YES'"
    T.SEL := " AND ((TURNOVER GT 0 AND TURNOVER LT 200000000) OR (TURNOVER EQ 0 AND PAID.CAPITAL GT 0 AND PAID.CAPITAL LT 10000000 AND LEGAL.ISS.DATE GE ": ISSUE.DATE:" ))"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*================ DATE FOR  THE HIGHEST CREDIT ===================================
    MONTH.TODAY = TODAY[5,2]
    YEAR.TODAY  = TODAY[1,4]
    DAY.TODAY   = TODAY[7,2]
    H.YEAR      = YEAR.TODAY - 1
    H.DATE      = H.YEAR:MONTH.TODAY:DAY.TODAY

*=================READ FROM SCB.ISCO.CF TABLE FOR CLOSED ACCOUNTS =================

*   Q.SEL  = "SELECT F.SCB.ISCO.SME.CF WITH ACC.STATUS NE 90 AND @ID LIKE ...":CLOSED.ACCT.DATE:"..."
*   CALL EB.READLIST(Q.SEL, KEY.LIST13, "", SELECTED13, ASD13)

*========================= GET DATA OF CREDIT =======================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            TOT.MRG.AMT = 0
            HIGH = 0
            TYPE = 'MAIN'
            OLD.ACCT.NEW = ''
            AMT.OVER = 0
            BAL.AMT  = 0
            NDPD = 0
            CRT @(30,3) KEY.LIST<I>:' ':I:' OF ':SELECTED
            CALL F.READ( FN.CU,KEY.LIST<I>, R.CU, F.CU, ETEXT)
            CUS.ID      =  KEY.LIST<I>
            CREDIT.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
            WOMEN.NO    = R.CU<EB.CUS.LOCAL.REF><1,CULR.OWNERSHIP.PER>
            WOMEN.NO    = WOMEN.NO + 0
            CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,Y.CUS.ACC.ERR)
            LOOP
                REMOVE Y.CUS.ACC.ID FROM R.CUS.ACC SETTING SUBVAL
            WHILE Y.CUS.ACC.ID:SUBVAL
*=========READ FROM ACCOUNT THE DEPIT CATEG ================
                CALL F.READ( FN.ACC,Y.CUS.ACC.ID,R.ACC, F.ACC, ETEXT1)
                TOT.MRG.AMT = 0
                RATE        = 1
                BAL.AMT = R.ACC<AC.OPEN.ACTUAL.BAL>
                CURR = R.ACC<AC.CURRENCY>
****DON'T TAKE  WITH BALANCE EQ 0
                IF (BAL.AMT NE 0 AND CREDIT.CODE EQ 110) OR (CREDIT.CODE NE 110) THEN
                    IF (R.ACC<AC.CATEGORY> GE '1100' AND R.ACC<AC.CATEGORY> LE '1599') AND R.ACC<AC.OPEN.ACTUAL.BAL> NE '' THEN
                        LMT.REF =''
                        LMT.REF1=FIELD(R.ACC<AC.LIMIT.REF>,'.',1,1)
                        IF LEN(LMT.REF1) EQ 3 THEN
                            LMT.REF = R.ACC<AC.CUSTOMER>:'.':'0000':R.ACC<AC.LIMIT.REF>
                        END
                        IF LEN(LMT.REF1) EQ 4 THEN
                            LMT.REF = R.ACC<AC.CUSTOMER>:'.':'000':R.ACC<AC.LIMIT.REF>
                        END
                        IF LEN(LMT.REF1) EQ 5 THEN
                            LMT.REF = R.ACC<AC.CUSTOMER>:'.':'00':R.ACC<AC.LIMIT.REF>
                        END

* ==========================================================
                        CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT2)
                        LMT.CURR = R.LMT<LI.LIMIT.CURRENCY>

                        LOCATE LMT.CURR IN CURR.BASE<1,1> SETTING POS THEN
                            RATE = R.BASE<RE.BCP.RATE,POS>
                        END
                        LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                            CURR.RATE = R.BASE<RE.BCP.RATE,POS>
                        END


                        IF R.LMT<LI.MAXIMUM.TOTAL> EQ '' THEN
                            R.LMT<LI.EXPIRY.DATE> = "20001010"
                        END
*============================================================
                        IF R.LMT<LI.MAXIMUM.TOTAL> EQ 0 THEN
                            R.LMT<LI.MAXIMUM.TOTAL> = 1
                        END
*============================================================
                        AMT_OVER = 0
                        IF (R.ACC<AC.OPEN.ACTUAL.BAL> * -1) GT R.LMT<LI.MAXIMUM.TOTAL> THEN
                            AMT_OVER = (R.ACC<AC.OPEN.ACTUAL.BAL> * -1) - R.LMT<LI.MAXIMUM.TOTAL>
                        END

* =============== ???? ??? ???????   ========================
                        DAYS = 'C'
                        DDD = R.LMT<LI.EXPIRY.DATE>[1,8]
                        IF R.LMT<LI.EXPIRY.DATE>[1,8] LT TODAY THEN

                            CALL CDD("",DDD,TODAY,DAYS)
                            NDPD = DAYS
                            IF NDPD GT 999 THEN NDPD = 999
                        END ELSE
                            NDPD = 0
                        END

*=============== CUSTOMER MOGANAB          ==================
                        IF (R.ACC<AC.CATEGORY> GE '1220' AND R.ACC<AC.CATEGORY> LE '1227') THEN
                            R.LMT<LI.APPROVAL.DATE> =R.ACC<AC.LOCAL.REF><1,ACLR.APPROVAL.DATE>
                            IF R.ACC<AC.OPEN.ACTUAL.BAL> LT 0 THEN
                                R.LMT<LI.MAXIMUM.TOTAL> = R.ACC<AC.OPEN.ACTUAL.BAL> * -1
                                AMT_OVER = (R.ACC<AC.OPEN.ACTUAL.BAL> * -1) - R.LMT<LI.MAXIMUM.TOTAL>
                            END
                            ELSE
                                R.LMT<LI.MAXIMUM.TOTAL> = R.ACC<AC.OPEN.ACTUAL.BAL>
                                AMT_OVER = R.ACC<AC.OPEN.ACTUAL.BAL> - R.LMT<LI.MAXIMUM.TOTAL>
                            END
                            COMP.ACCT = R.ACC<AC.INTEREST.LIQU.ACCT>
                            INT.AMT = 0
                            INT.AMT.OLD = 0
                            BAL.AMT     = 0

                            COMP.ACCT.OLD = COMP.ACCT[1,10]:"9090":COMP.ACCT[15,2]
*Line [ 282 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                            CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,COMP.ACCT,INT.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMP.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
INT.AMT=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
*Line [ 289 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                            CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,COMP.ACCT.OLD,INT.AMT.OLD)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMP.ACCT.OLD,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
INT.AMT.OLD=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
                            BAL.AMT = R.ACC<AC.OPEN.ACTUAL.BAL>
                            TOT.MRG.AMT =  INT.AMT + INT.AMT.OLD

                            BAL.AMT = DROUND(BAL.AMT,0)
                            TOT.MRG.AMT = DROUND(TOT.MRG.AMT,0)

                            IF BAL.AMT LT 0 THEN
                                BAL.AMT = BAL.AMT * -1
                            END

                            IF TOT.MRG.AMT LT 0 THEN
                                TOT.MRG.AMT = TOT.MRG.AMT * -1
                            END

                            NDPD = 999
                            AMT_OVER = BAL.AMT

                        END

* ==========================================================
                        ASSET_CLASS = 0
                        IF NDPD LE 30  THEN ASSET_CLASS = 401
                        IF NDPD GE 31  AND NDPD LE 90 THEN ASSET_CLASS = 402
                        IF NDPD GE 91  AND NDPD LE 120 THEN ASSET_CLASS = 403
                        IF NDPD GE 121 THEN ASSET_CLASS = 404

*=============== REPAYMENT AND TERM OF LOAN VALUES===========
                        IF R.ACC<AC.CATEGORY> EQ '1205' OR R.ACC<AC.CATEGORY> EQ '1206' OR R.ACC<AC.CATEGORY> EQ '1207' OR R.ACC<AC.CATEGORY> EQ '1208' THEN
                            REPAY.TYPE   = '005'
                            TERM.OF.LOAN = ''
****CALC HIGHEST CREDIT

                            X.SEL  = "SELECT F.SCB.ISCO.SME.CF WITH CF.ACC.NO EQ " :Y.CUS.ACC.ID:" AND DATE.TIME GE H.DATE"
                            CALL EB.READLIST(X.SEL, KEY.LIST4, "", SELECTED4, ASD4)
                            FOR O = 1 TO SELECTED4
                                CALL F.READ( F.CF,KEY.LIST4<O>, R.CF, FN.CF, ETEXT)
                                BAL.AMT = R.CF<ISCO.CF.CF.ACCT.BALANCE> * -1
                                IF BAL.AMT GT HIGH THEN
                                    HIGH     = BAL.AMT
                                END
                            NEXT O
                        END
                        ELSE
                            REPAY.TYPE   = ''
                            TERM.OF.LOAN = '12'
                            HIGH = ''
                        END

*********************************************************************
                        CREDIT.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
                        IF CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120 THEN
                            ASSET_CLASS = 404
                            NDPD = 999
                        END
                        IF ASSET_CLASS EQ 404 THEN
                            NDPD = 999
                        END

*==============creat erecord to fil in the CF table==========================
                        DATE.TEST = YEAR:NEW.MONTH:'30'

                        IF NDPD EQ 0 THEN
                            AMT_OVER = 0
                        END
                        IF AMT_OVER EQ 0 THEN     ;*AHM
                            NDPD = 0
                        END
                        IF AMT_OVER LT 0 THEN
                            AMT_OVER = AMT_OVER * -1
                        END

                        AMT_OVER = DROUND(AMT_OVER,0)
                        IF R.ACC<AC.OPEN.ACTUAL.BAL> LT 0 AND (R.ACC<AC.CATEGORY> LT '1220' OR R.ACC<AC.CATEGORY> GT '1227') THEN

                            BAL.AMOUNT = R.ACC<AC.OPEN.ACTUAL.BAL> * -1
                            BAL.AMT    = DROUND(BAL.AMOUNT,0)
                        END
                        AAA = R.ACC<AC.OPEN.ACTUAL.BAL>
                        BBB = R.ACC<AC.CATEGORY>
                        IF R.ACC<AC.OPEN.ACTUAL.BAL> GT 0 AND (R.ACC<AC.CATEGORY> LT '1220' OR R.ACC<AC.CATEGORY> GT '1227') THEN
                            BAL.AMOUNT = R.ACC<AC.OPEN.ACTUAL.BAL>
                            BAL.AMT    = DROUND(BAL.AMOUNT,0)
                        END


                        IF R.ACC<AC.CATEGORY> EQ '1507' OR R.ACC<AC.CATEGORY> EQ '1512' THEN
                            R.LMT = ''
                            LC.CUS.ID   = Y.CUS.ACC.ID[1,8]
                            IF LC.CUS.ID[1,1] EQ 0 THEN
                                LC.CUS.ID = LC.CUS.ID[2,7]
                            END

                            LMT.REF     = LC.CUS.ID:'.0020000.01'
                            LMT.REF2    = LC.CUS.ID:'.0030000.01'

                            CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT7)
                            IF ETEXT7 THEN
                                CALL F.READ( FN.LMT,LMT.REF2, R.LMT, F.LMT, ETEXT7)
                            END

                            CATT = R.ACC<AC.CATEGORY>
                            IF BAL.AMT EQ 0 THEN
                                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT> = 1
                            END
                            ELSE
                                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT> = DROUND(BAL.AMT,0)
                                AMT_OVER = 0
                                NDPD = 0
                            END
                        END
                        ELSE
                            IF CURR NE LMT.CURR AND LMT.CURR NE '' THEN
                                NEW.APP.AMT = DROUND(R.LMT<LI.MAXIMUM.TOTAL>,0) * RATE
                                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT> = DROUND(NEW.APP.AMT / CURR.RATE,0)
                            END
                            ELSE
                                NEW.APP.AMT = DROUND(R.LMT<LI.MAXIMUM.TOTAL>,0)
                                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>  = DROUND(NEW.APP.AMT,0)
                            END

                        END

                        IF (R.ACC<AC.CATEGORY> EQ '1560' OR R.ACC<AC.CATEGORY> EQ '1599') AND R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT> EQ '' THEN
                            R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT> = DROUND(BAL.AMT,0)
                        END

                        APP.DATE = R.LMT<LI.APPROVAL.DATE>
*******************************AHMED 30-3-2015***********************

                        CATEG.LINE  = '(1206 1208 1401 1402 1405 1406 1415 1417 1418 1419 1420 1421 1477 1480 1481 1483 1484 1485)'

                        WS.CATEG  = R.ACC<AC.CATEGORY>
                        FINDSTR WS.CATEG IN CATEG.LINE SETTING POS THEN
                            AMT_OVER = 0
                            NDPD = 0

                        END

*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
                        COMP.CODE = R.CU<EB.CUS.ACCOUNT.OFFICER>
                        IF COMP.CODE EQ '11' THEN
                            ISLAMIC.IND = '001'
                        END
                        ELSE
                            ISLAMIC.IND = '002'
                        END
*************************************************

                        CATT = R.ACC<AC.CATEGORY>
                        IF(CATT EQ 1206 OR CATT EQ 1208 OR CATT EQ 1417 OR CATT EQ 1419 OR CATT EQ 1421 OR CATT EQ 1484 OR CATT EQ 1485 OR CATT EQ 1401 OR CATT EQ 1402 OR CATT EQ 1405 OR CATT EQ 1406 OR CATT EQ 1415 OR CATT EQ 1480 OR CATT EQ 1481 OR CATT EQ 1483 OR CATT EQ 1493 OR CATT EQ 1499 OR CATT EQ 1218 OR CATT EQ 21066) THEN
                            SECURED.IND = '001'
                        END
                        ELSE
                            SECURED.IND = '003'
                        END
**************************************************
                        IF (CATT EQ '1507' OR CATT EQ '1516' OR CATT EQ '1525' OR CATT EQ '1526' OR CATT EQ '1532') THEN
                            R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT> = 1
                        END

                        R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = COMP.CODE
                        R.FILL.CF<ISCO.CF.CF.ACC.NO>           = Y.CUS.ACC.ID
                        R.FILL.CF<ISCO.CF.APPROVAL.DATE>       = APP.DATE
                        R.FILL.CF<ISCO.CF.AMT.INSTALL>         = ''
                        R.FILL.CF<ISCO.CF.CURRENCY>            = Y.CUS.ACC.ID[9,2]
                        R.FILL.CF<ISCO.CF.CF.TYPE>             = CATT
                        R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = TERM.OF.LOAN
                        R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = REPAY.TYPE
                        R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = BAL.AMT
                        R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = AMT_OVER
                        R.FILL.CF<ISCO.CF.NDPD>                = NDPD
                        R.FILL.CF<ISCO.CF.ASSET.CLASS>         = ASSET_CLASS
                        R.FILL.CF<ISCO.CF.REASON.AMT.FORGIVEN> = '002'
                        R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = SECURED.IND
                        R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
                        R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = TOT.MRG.AMT
                        R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
                        R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
                        R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
                        R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
                        R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY

                        IF R.ACC<AC.POSTING.RESTRICT> EQ '' THEN
                            R.FILL.CF<ISCO.CF.ACC.STATUS> = '3'
                        END
                        ELSE
                            R.FILL.CF<ISCO.CF.ACC.STATUS> = 'a'
                        END
                        ID.CF = Y.CUS.ACC.ID:YEAR:NEW.MONTH

                        WRITE R.FILL.CF TO FVAR.CF , ID.CF  ON ERROR
                            STOP 'CAN NOT WRITE RECORD ':ID.CF:' TO FILE ':F.CF
                        END
*===================create record to fil in the CS table==========
                        IF TYPE EQ 'MAIN' THEN
                            R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = COMP.CODE
                            R.FILL.CS<ISCO.CS.CF.ACC.NO>           = Y.CUS.ACC.ID
                            R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= CUS.ID
                            ID.CS = Y.CUS.ACC.ID:YEAR:NEW.MONTH
                        END
                        IDENT.CODE                             = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                        R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = FMT(IDENT.CODE,"R%12")
                        R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
                        R.FILL.CS<ISCO.CS.IDENT.1.EXPIRE.DATE> = WOMEN.NO
                        R.FILL.CS<ISCO.CS.A.SUBJ.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:" ":R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>
                        R.FILL.CS<ISCO.CS.ADD.1.TYPE.E>        = '001'
                        R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
                        R.FILL.CS<ISCO.CS.ADD.1.GOV.E>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                        R.FILL.CS<ISCO.CS.COUNTRY>             = R.CU<EB.CUS.RESIDENCE>
                        LOCAL.REF1                             = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 495 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
                        IF NO_OF_TEL EQ 1 THEN
                            R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                        END
                        IF NO_OF_TEL GT 1 THEN
                            R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                        END
                        R.FILL.CS<ISCO.CS.LEGAL.CONDITION>     = R.CU<EB.CUS.LOCAL.REF><1,CULR.LEGAL.FORM>
                        R.FILL.CS<ISCO.CS.SME.CATEGORY>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.SME.CATEGORY>
                        R.FILL.CS<ISCO.CS.PAID.CAPITAL>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.PAID.CAPITAL>
                        R.FILL.CS<ISCO.CS.TURNOVER>              = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER>
                        R.FILL.CS<ISCO.CS.TOTAL.ASSETS>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TOTAL.ASSETS>
                        R.FILL.CS<ISCO.CS.NO.OF.EMPLOYEES>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.NO.OF.EMP>
                        R.FILL.CS<ISCO.CS.ESTABLISH.DATE>        = R.CU<EB.CUS.BIRTH.INCORP.DATE>
                        R.FILL.CS<ISCO.CS.BUSINESS.START.DATE>   = R.CU<EB.CUS.LEGAL.ISS.DATE>
                        R.FILL.CS<ISCO.CS.FIN.STATEMENT.DATE>    = R.CU<EB.CUS.LOCAL.REF><1,CULR.FIN.STAT.DATE>
                        R.FILL.CS<ISCO.CS.MAX.TURNOVER.AMT.DATE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER.DATE>
                        R.FILL.CS<ISCO.CS.BUSINESS.END.DATE>     = R.CU<EB.CUS.LEGAL.EXP.DATE>
                        R.FILL.CS<ISCO.CS.BAC1>                  = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.ACTIVITY>
                        IF WOMEN.NO GT 0 THEN
                            R.FILL.CS<ISCO.CS.BAC3>              = "001"
                        END
                        ELSE
                            R.FILL.CS<ISCO.CS.BAC3>              = "003"
                        END
                        R.FILL.CS<ISCO.CS.RECORD.STATUS>         = 'INAU'
                        R.FILL.CS<ISCO.CS.CURR.NO>               = '1'
                        R.FILL.CS<ISCO.CS.INPUTTER>              = 'ISCORE'
                        R.FILL.CS<ISCO.CS.CO.CODE>               = COMP


                        WRITE R.FILL.CS TO FVAR.CS , ID.CS  ON ERROR
                            STOP 'CAN NOT WRITE RECORD ':ID.CS:' TO FILE ':F.CS
                        END

                    END
                END
            REPEAT
        NEXT I
    END
**********************PERSONAL LOANS*************
    K.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY IN ('21058' '21062' '21067' '21068' '21069' '21070' '21071' '21073') AND STATUS NE 'LIQ'"
    CALL EB.READLIST(K.SEL, KEY.LIST.LD, "", SELECTED.LD, ERR.LD)

    IF SELECTED.LD THEN
        FOR K =1 TO SELECTED.LD
            DIFF.MONTH = 1

            CALL F.READ( FN.LD,KEY.LIST.LD<K>, R.LD, F.LD, ETX.LD)
            CUS.LD = R.LD<LD.CUSTOMER.ID>
            CALL F.READ( FN.CU,CUS.LD, R.CU, F.CU, ETX.CUS)
            CUS.LD.0 = CUS.LD
            WOMEN.NO    = R.CU<EB.CUS.LOCAL.REF><1,CULR.OWNERSHIP.PER>
            WOMEN.NO    = WOMEN.NO + 0

            IF LEN(CUS.LD) NE 8 THEN
                CUS.LD.0 = '0':CUS.LD
            END
            LD.ACC = KEY.LIST.LD<K>

***************NEW 13-2-2018**************
            L.VALUE.DATE = R.LD<LD.VALUE.DATE>
            LD.MAT.DATE  = R.LD<LD.FIN.MAT.DATE>
            DIFF.MONTH = ''

            CALL EB.NO.OF.MONTHS(L.VALUE.DATE,LD.MAT.DATE,DIFF.MONTH)

*Line [ 575 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('LD.SCHEDULE.DEFINE':@FM:LD.SD.AMOUNT,LD.ACC,LD.INST.AMT)
F.ITSS.LD.SCHEDULE.DEFINE = 'FBNK.LD.SCHEDULE.DEFINE'
FN.F.ITSS.LD.SCHEDULE.DEFINE = ''
CALL OPF(F.ITSS.LD.SCHEDULE.DEFINE,FN.F.ITSS.LD.SCHEDULE.DEFINE)
CALL F.READ(F.ITSS.LD.SCHEDULE.DEFINE,LD.ACC,R.ITSS.LD.SCHEDULE.DEFINE,FN.F.ITSS.LD.SCHEDULE.DEFINE,ERROR.LD.SCHEDULE.DEFINE)
LD.INST.AMT=R.ITSS.LD.SCHEDULE.DEFINE<LD.SD.AMOUNT>
            LD.INST.AMT = LD.INST.AMT<1,1>
            LD.PAY.DUE.ID = 'PD':LD.ACC
*Line [ 584 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,LD.PAY.DUE.ID,LD.AMT.OVER)
F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
FN.F.ITSS.PD.PAYMENT.DUE = ''
CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
CALL F.READ(F.ITSS.PD.PAYMENT.DUE,LD.PAY.DUE.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
LD.AMT.OVER=R.ITSS.PD.PAYMENT.DUE<PD.TOTAL.OVERDUE.AMT>
*Line [ 591 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.PAYMENT.DTE.DUE,LD.PAY.DUE.ID,LD.DUE.DATE)
F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
FN.F.ITSS.PD.PAYMENT.DUE = ''
CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
CALL F.READ(F.ITSS.PD.PAYMENT.DUE,LD.PAY.DUE.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
LD.DUE.DATE=R.ITSS.PD.PAYMENT.DUE<PD.PAYMENT.DTE.DUE>
*Line [ 568 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.DUE   = DCOUNT(LD.DUE.DATE,@VM)
            LD.DUE.DATE = LD.DUE.DATE<1,NO.DUE>

            IF LD.DUE.DATE THEN
                DAYS = 'C'
                CALL CDD("",LD.DUE.DATE,TODAY,DAYS)
                LD.NDPD = DAYS
                IF DAYS GT 999 THEN LD.NDPD = 999
            END
            ELSE
                LD.NDPD = 0
            END

***************GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
            COMP.CODE = R.CU<EB.CUS.ACCOUNT.OFFICER>
            IF COMP.CODE EQ '11' THEN
                ISLAMIC.IND = '001'
            END
            ELSE
                ISLAMIC.IND = '002'
            END
****************************************

            LD.ID  = CUS.LD.0:KEY.LIST.LD<K>[3,10]:YEAR:NEW.MONTH:'L'
            LD.AMT = R.LD<LD.AMOUNT>
            R.FILL.CF<ISCO.CF.APPROVAL.DATE>       = R.LD<LD.VALUE.DATE>
            R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
            R.FILL.CF<ISCO.CF.CF.ACC.NO>           = LD.ACC
            R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(R.LD<LD.AMOUNT>,0)
            R.FILL.CF<ISCO.CF.CURRENCY>            = '10'
            R.FILL.CF<ISCO.CF.CF.TYPE>             = R.LD<LD.CATEGORY>
            R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = DIFF.MONTH
            R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = ''
            R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = DROUND(R.LD<LD.AMOUNT>,0)
            R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = LD.AMT.OVER
            R.FILL.CF<ISCO.CF.NDPD>                = LD.NDPD
            R.FILL.CF<ISCO.CF.AMT.INSTALL>         = DROUND(LD.INST.AMT,0)
            R.FILL.CF<ISCO.CF.ASSET.CLASS>         ='000'
            R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = '003'
            R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
            R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = 0
            R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
            R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
            R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
            R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
            R.FILL.CF<ISCO.CF.ACC.STATUS>          = '3'
            R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY
            CALL F.WRITE (F.CF,LD.ID,R.FILL.CF)
            CALL JOURNAL.UPDATE(LD.ID)

*----------- CS FOR PERSONAL LOAN LD ---------------------------

            R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID>   = R.CU<EB.CUS.ACCOUNT.OFFICER>
            R.FILL.CS<ISCO.CS.CF.ACC.NO>             = LD.ACC
            R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>  = CUS.LD

            IDENT.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
            R.FILL.CS<ISCO.CS.IDENT.CODE.1>          = FMT(IDENT.CODE,"R%12")
            R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>     = ''
            R.FILL.CS<ISCO.CS.IDENT.1.EXPIRE.DATE>   = WOMEN.NO

            R.FILL.CS<ISCO.CS.A.SUBJ.NAME>           = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            R.FILL.CS<ISCO.CS.ADD.1.TYPE.E>          = '001'

            R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
            R.FILL.CS<ISCO.CS.ADD.1.GOV.E>           = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
            R.FILL.CS<ISCO.CS.COUNTRY>               = R.CU<EB.CUS.RESIDENCE>
            LOCAL.REF1                               = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
*Line [ 637 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
            IF NO_OF_TEL EQ 1 THEN
                R.FILL.CS<ISCO.CS.PHONE>             = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
            END
            IF NO_OF_TEL GT 1 THEN
                R.FILL.CS<ISCO.CS.PHONE>             = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
            END
            R.FILL.CS<ISCO.CS.LEGAL.CONDITION>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.LEGAL.FORM>
            R.FILL.CS<ISCO.CS.SME.CATEGORY>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.SME.CATEGORY>
            R.FILL.CS<ISCO.CS.PAID.CAPITAL>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.PAID.CAPITAL>
            R.FILL.CS<ISCO.CS.TURNOVER>              = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER>
            R.FILL.CS<ISCO.CS.TOTAL.ASSETS>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TOTAL.ASSETS>
            R.FILL.CS<ISCO.CS.NO.OF.EMPLOYEES>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.NO.OF.EMP>
            R.FILL.CS<ISCO.CS.ESTABLISH.DATE>        = R.CU<EB.CUS.BIRTH.INCORP.DATE>
            R.FILL.CS<ISCO.CS.BUSINESS.START.DATE>   = R.CU<EB.CUS.LEGAL.ISS.DATE>
            R.FILL.CS<ISCO.CS.FIN.STATEMENT.DATE>    = R.CU<EB.CUS.LOCAL.REF><1,CULR.FIN.STAT.DATE>
            R.FILL.CS<ISCO.CS.MAX.TURNOVER.AMT.DATE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER.DATE>
            R.FILL.CS<ISCO.CS.BUSINESS.END.DATE>     = R.CU<EB.CUS.LEGAL.EXP.DATE>
            R.FILL.CS<ISCO.CS.BAC1>                  = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.ACTIVITY>
            IF WOMEN.NO GT 0 THEN
                R.FILL.CS<ISCO.CS.BAC3>              = "001"
            END
            ELSE
                R.FILL.CS<ISCO.CS.BAC3>              = "003"
            END
*******************************

            R.FILL.CS<ISCO.CS.RECORD.STATUS>       = 'INAU'
            R.FILL.CS<ISCO.CS.CURR.NO>             = '1'
            R.FILL.CS<ISCO.CS.INPUTTER>            = 'ISCORE'
            R.FILL.CS<ISCO.CS.CO.CODE>             = COMP

            CALL F.WRITE (F.CS,LD.ID,R.FILL.CS)
            CALL JOURNAL.UPDATE(LD.ID)
        NEXT K
    END

END
