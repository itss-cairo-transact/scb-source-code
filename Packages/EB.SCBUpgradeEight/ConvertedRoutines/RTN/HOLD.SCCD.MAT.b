* @ValidationCode : MjotOTExOTU1NzY4OkNwMTI1MjoxNjQwNzA5NTkyODk3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:39:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*****ABEER
SUBROUTINE HOLD.SCCD.MAT
*    PROGRAM HOLD.SCCD.MAT

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON



    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
********************
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''   ;R.LD=''
    CALL OPF(FN.LD,F.LD)

    FN.COLL  = 'FBNK.COLLATERAL'       ; F.COLL = '' ; R.COLL = ''
    CALL OPF(FN.COLL,F.COLL)
**********************************
    MAT.DATE = TODAY
    CALL CDT('EG00',MAT.DATE, '1W')
    PRINT MAT.DATE:'AFTER CDT'
    PRINT MAT.DATE:'-MAT.DATE'

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH BLOCK.PURPOSE NE '' AND CATEGORY EQ 21103 AND STATUS NE LIQ AND FIN.MAT.DATE EQ ":MAT.DATE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
*******************************************************
        FIL.NAM = "SCCD.HOLD.":KEY.LIST<I>
        OPENSEQ "SCCD" , FIL.NAM TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"SCCD":' ':FIL.NAM
            HUSH OFF
        END

        OPENSEQ "SCCD" , FIL.NAM TO BB ELSE
            CREATE BB THEN
                PRINT 'FILE SCCD.HOLD.MAT CREATED IN SCCD'
            END ELSE
                STOP 'Cannot create SCCD.HOLD.MAT File IN SCCD'
            END
        END
*********************************************

        LOCAL.REF = R.LD<LD.LOCAL.REF>

        GRN.WILL = LOCAL.REF<1,LDLR.GRANT.WILL>
        COLL.ID  = LOCAL.REF<1,LDLR.COLLATERAL.ID>

        CALL F.READ(FN.COLL, COLL.ID, R.COLL, F.COLL, ETEXT11)
        LOC.REF.COLL  = R.COLL<COLL.LOCAL.REF>
        COLL.AMT    = LOC.REF.COLL<1,COLR.COLL.AMOUNT>

        CD.ACCT=LOCAL.REF<1,LDLR.ACCT.STMP>
        CD.HOLD.DATE=R.LD<LD.FIN.MAT.DATE>
        CD.INP=R.LD<LD.INPUTTER><1,1>
        CD.CODE     = R.LD<LD.CO.CODE>
*******************************************************
        IDD = 'AC.LOCKED.EVENTS,SCB,AUTO.SZ//':CD.CODE
        COMMA = ","
        OFS.MESSAGE.DATA =  "ACCOUNT.NUMBER=":CD.ACCT:COMMA
        OFS.MESSAGE.DATA := "DESCRIPTION=":KEY.LIST<I>:COMMA
        OFS.MESSAGE.DATA := "LOCKED.AMOUNT=":COLL.AMT:COMMA
        OFS.MESSAGE.DATA := "FT.REFRENCE=":"21103":COMMA
        OFS.MESSAGE.DATA := "FROM.DATE=":CD.HOLD.DATE


        MSG.DATA = IDD:",":",":OFS.MESSAGE.DATA
        WRITESEQ MSG.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "

        END
        EXECUTE 'COPY FROM SCCD TO OFS.IN ':FIL.NAM
        EXECUTE 'DELETE ':"SCCD":' ':FIL.NAM

    NEXT I

RETURN
END
