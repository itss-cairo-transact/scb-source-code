* @ValidationCode : MjotNjAwNTA3NzYxOkNwMTI1MjoxNjQwNzA3NDk3MjcyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:04:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*----------CREATED BY NOHA HAMED 11-3-2018------------------------------------------
*-----------------------------------------------------------------------------
SUBROUTINE GET.CL.AUTH.BY.GROUP
*    PROGRAM GET.CL.AUTH.BY.GROUP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CL.GROUPS
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
RETURN
*--------------------------------------------------
INITIATE:
*========

    T.DATE = TODAY
    FN.CL  = 'F.SCB.DEPT.SAMPLE1'  ; F.CL = ''; R.CL = ''
    CALL OPF(FN.CL,F.CL)
    FN.G  = 'F.SCB.CL.GROUPS'          ; F.G = ''; R.G = ''
    CALL OPF(FN.G,F.G)
    FN.CUS  = 'FBNK.CUSTOMER'          ; F.CUS = ''; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
********************************
*T.DATE = '20180408'
    YTEXT  = "Enter Group NO : "
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    GROUP.NO = COMI
RETURN

*========================================================================
PROCESS:
*=========
    T.SEL  = "SELECT F.SCB.DEPT.SAMPLE1 WITH FLG.INP1 EQ 1"
    T.SEL := " AND FLG.INP2 EQ 2"
    T.SEL := " AND REQUEST.DATE.HWALA EQ ":T.DATE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        Q = 0
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CL,KEY.LIST<I>, R.CL, F.CL, ETEXT)
            CL.NO = KEY.LIST<I>
            CUS1 = R.CL<DEPT.SAMP.CUS.HWALA>
            CUS2 = R.CL<DEPT.SAMP.CUS.LG11>
            CUS3 = R.CL<DEPT.SAMP.CUS.LG22>
            CUS4 = R.CL<DEPT.SAMP.CUS.TEL>
            CUS5 = R.CL<DEPT.SAMP.CUS.TF1>
            CUS6 = R.CL<DEPT.SAMP.CUS.TF2>
            CUS7 = R.CL<DEPT.SAMP.CUS.TF3>
            CUS8 = R.CL<DEPT.SAMP.CUS.BR>
            CUS9 = R.CL<DEPT.SAMP.CUS.AC>
            CUS10 = R.CL<DEPT.SAMP.CUS.WH>

            BEGIN CASE
                CASE CUS1
                    CUS.NO = CUS1
                CASE CUS2
                    CUS.NO = CUS2
                CASE CUS3
                    CUS.NO = CUS3
                CASE CUS4
                    CUS.NO = CUS4
                CASE CUS5
                    CUS.NO = CUS5
                CASE CUS6
                    CUS.NO = CUS6
                CASE CUS7
                    CUS.NO = CUS7
                CASE CUS8
                    CUS.NO = CUS8
                CASE CUS9
                    CUS.NO = CUS9
                CASE CUS10
                    CUS.NO = CUS10

            END CASE
            CALL F.READ(FN.G,CUS.NO, R.G, F.G, E.G)
            CUS.GROUP.NO = R.G<CL.GROUP.GROUP.NO>
            IF (CUS.GROUP.NO EQ GROUP.NO) OR (CUS.GROUP.NO EQ '' AND GROUP.NO EQ 4) THEN
                Q = Q + 1
                ENQ<2,Q> = "@ID"
                ENQ<3,Q> = "EQ"
                ENQ<4,Q> = KEY.LIST<I>
            END
        NEXT I

    END ELSE
        ENQ<2,Q> = '@ID'
        ENQ<3,Q> = 'EQ'
        ENQ<4,Q> = 'DUMMY'

    END

RETURN
END
