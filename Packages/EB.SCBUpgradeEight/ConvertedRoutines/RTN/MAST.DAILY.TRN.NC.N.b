* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
****CREATED BY NESSREEN AHMED 23/9/2013******
*-----------------------------------------------------------------------------
* <Rating>1632</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAST.DAILY.TRN.NC.N

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.TRANS.NC
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.NEW
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DAILY.TRN.NC.N
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CURR.CODES
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.ROUTINE.CHK

*****WRITTEN BY NESSREEN AHMED SCB*****
***************************************
    Path = "NESRO/mstr/MASTTR"

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    F.MAST.TRANS = '' ; FN.MAST.TRANS = 'F.SCB.MAST.TRANS.NC' ; R.MAST.TRANS = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.MAST.TRANS,F.MAST.TRANS)

******3/2/2008****************************
    F.RT.CHK = '' ; FN.RT.CHK = 'F.SCB.MAST.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)

    DATEE = '' ; YEAR1 = '' ; MONTH1= ''  ; YEARN = '' ; YY = ''
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    TEXT = 'DATE=':DATEE ; CALL REM
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YEARN = YYYY-1
    END ELSE
        MU = MT-1
        YEARN = YYYY
    END
**************************************
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END
***************************************
    YYDD = YY:MON:'01'
    YEAR1= YEARN
    MONTH1 = MON
    BR = R.USER<EB.USE.DEPARTMENT.CODE>
    TEXT = 'Y=':YEAR1 ; CALL REM
    TEXT = 'M=':MONTH1 ; CALL REM
    TEXT = 'BR=':BR ; CALL REM
************3/2/2008**********************************
    CHK.SEL = "SELECT F.SCB.MAST.ROUTINE.CHK WITH BRANCH EQ ":BR :" AND YEAR EQ ":YEAR1:" AND MONTH EQ ":MONTH1 :" AND DAILY.TRN.NC EQ 'YES' "
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

* KEYID = YEARN:MU:'.':BR
    IF LEN(BR) < 2 THEN
        BRAN = '0':BR
    END ELSE
        BRAN = BR
    END

    KEYID = YEARN:MON:BRAN
** KEYID = YY:MON:'.':BR
    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
******************************************************
        TEXT = 'Start.Of.File' ; CALL REM
        EOF = ''
        LOOP WHILE NOT(EOF)

            CARD.NO = '' ;REASON.COD = ''; REASON.COD.TR = '' ; BANK.ACC = '' ; BANK.ACC.TR = '' ; ORG.MSGE = '' ; ORG.MSGE.TR = ''
            MSGE.TYPE = '' ; PROC.COD = '' ; BILL.CURR = '' ; BILL.AMT = '' ; BILL.AMT.TR = '' ; BILL.AMT.NN = '' ; BILL.AMT.1 = '' ; BILL.AMT.FMT = ''
            DB.CR = '' ; POST.DATE = '' ; PURCH.DATE = '' ; TRANS.CURR.GL = ''
            MSGE.DESC = '' ; MERCH.CITY = '' ; MERCH.COUNTRY = ''
            TRANS.AMT = '' ; TRANS.AMT.1 = '' ; TRANS.AMT.FMT = ''
            READSEQ Line FROM MyPath THEN
                CARD.NO           = Line[1,16]
***TEXT = 'CARD.NO=':CARD.NO ; CALL REM
                T.SEL =  "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
                KEY.LIST=""
                SELECTED=""
                ER.MSG=""

                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF NOT(SELECTED) THEN
*** TEXT = 'NO.CARD=':CARD.NO ; CALL REM
                    REASON.COD        = Line[34,4]
                    REASON.COD.TR =  TRIM(REASON.COD, "", "D")
                    LENCOD = LEN(REASON.COD.TR)
                    IF LENCOD = 0 THEN
                        REASON.COD.TR = "NA"
                    END
                    ORG.MSGE          = Line[70,5]
                    ORG.MSGE.TR =  TRIM(ORG.MSGE, " ", "T")
                    MSGE.TYPE         = Line[76,4]
                    PROC.COD          = Line[81,6]
                    BILL.CURR         = Line[88,3]
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, BILL.CURR , BILL.CURR.GL)
F.ITSS.SCB.VISA.CURR.CODES = 'F.SCB.VISA.CURR.CODES'
FN.F.ITSS.SCB.VISA.CURR.CODES = ''
CALL OPF(F.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES)
CALL F.READ(F.ITSS.SCB.VISA.CURR.CODES,BILL.CURR,R.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES,ERROR.SCB.VISA.CURR.CODES)
BILL.CURR.GL=R.ITSS.SCB.VISA.CURR.CODES<SCB.CURR.CURRENCY>
                    BILL.AMT.NN       = Line[94,16]
                    BILL.AMT.1 = TRIM(BILL.AMT.NN, "0", "L")
                    BILL.AMT.FMT = BILL.AMT.1/100
                    DB.CR             = Line[111,2]
                    POST.DATE         = Line[114,8]
                    PURCH.DATE        = Line[123,8]
                    TRANS.CURR        = Line[132,3]
*Line [ 165 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-13
*                    CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, TRANS.CURR , TRANS.CURR.GL)
F.ITSS.SCB.VISA.CURR.CODES = 'F.SCB.VISA.CURR.CODES'
FN.F.ITSS.SCB.VISA.CURR.CODES = ''
CALL OPF(F.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES)
CALL F.READ(F.ITSS.SCB.VISA.CURR.CODES,TRANS.CURR,R.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES,ERROR.SCB.VISA.CURR.CODES)
TRANS.CURR.GL=R.ITSS.SCB.VISA.CURR.CODES<SCB.CURR.CURRENCY>
                    TRANS.AMT      = Line[138,16]
                    TRANS.AMT.1 = TRIM(TRANS.AMT, "0", "L")
                    TRANS.AMT.FMT = TRANS.AMT.1/100
                    MSGE.DESC         = Line[188,60]
                    CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MSGE.DESC
                    MERCH.CITY        = Line[249,60]
                    CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MERCH.CITY
                    MERCH.COUNTRY     = Line[310,3]

                    F.MAST.DAILY.TRN = '' ; FN.MAST.DAILY.TRN = 'F.SCB.MAST.DAILY.TRN.NC.N' ; R.MAST.DAILY.TRN = '' ; E2 = '' ; RETRY2 = ''
                    CALL OPF(FN.MAST.DAILY.TRN,F.MAST.DAILY.TRN)

                    ID.KEY = CARD.NO:YEAR1:MONTH1
                    CALL F.READ(FN.MAST.DAILY.TRN,ID.KEY, R.MAST.DAILY.TRN, F.MAST.DAILY.TRN ,E2)
                    IF NOT(E2) THEN
                        ORG.MSG = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE.NC>
*Line [ 170 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DD = DCOUNT(ORG.MSG,@VM)
                        XX = DD+1

                        R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE.NC,XX> = ORG.MSGE.TR
                        R.MAST.DAILY.TRN<MST.DAILY.MSG.TYPE.NC,XX>     = MSGE.TYPE
                        R.MAST.DAILY.TRN<MST.DAILY.PROCESS.CODE.NC,XX> = PROC.COD
                        R.MAST.DAILY.TRN<MST.DAILY.DB.CR.FLAG.NC,XX>   = DB.CR
                        R.MAST.DAILY.TRN<MST.DAILY.REASON.CODE.NC,XX>  = REASON.COD.TR
                        R.MAST.DAILY.TRN<MST.DAILY.BILL.CURR.NC,XX>    = BILL.CURR.GL
                        R.MAST.DAILY.TRN<MST.DAILY.BILL.AMT.NC,XX>     = BILL.AMT.FMT
                        R.MAST.DAILY.TRN<MST.DAILY.POS.DATE.NC,XX>     = POST.DATE
                        R.MAST.DAILY.TRN<MST.DAILY.VALUE.DATE.NC,XX>   = PURCH.DATE
                        R.MAST.DAILY.TRN<MST.DAILY.TRANS.CURR.NC,XX>   = TRANS.CURR.GL
                        R.MAST.DAILY.TRN<MST.DAILY.TRANS.AMT.NC,XX>    = TRANS.AMT.FMT
                        R.MAST.DAILY.TRN<MST.DAILY.MSG.DESC.NC,XX>     = MSGE.DESC
                        R.MAST.DAILY.TRN<MST.DAILY.MERCH.CITY.NC,XX>   = MERCH.CITY
                        R.MAST.DAILY.TRN<MST.DAILY.MERCH.COUNTRY.NC,XX>= MERCH.COUNTRY

                        CALL F.WRITE(FN.MAST.DAILY.TRN,ID.KEY, R.MAST.DAILY.TRN)
                        CALL JOURNAL.UPDATE(ID.KEY)

                    END ELSE
                        ORG.MSG = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE.NC>
*Line [ 194 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DD = DCOUNT(ORG.MSG,@VM)
                        XX = DD+1

                        R.MAST.DAILY.TRN<MST.DAILY.CARD.BR.NC>         = ' '
                        R.MAST.DAILY.TRN<MST.DAILY.CUST.NAME.NC>       = ' '
                        R.MAST.DAILY.TRN<MST.DAILY.CUST.ACCT.NC>       = ' '
                        R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE.NC,XX> = ORG.MSGE.TR
                        R.MAST.DAILY.TRN<MST.DAILY.MSG.TYPE.NC,XX>     = MSGE.TYPE
                        R.MAST.DAILY.TRN<MST.DAILY.PROCESS.CODE.NC,XX> = PROC.COD
                        R.MAST.DAILY.TRN<MST.DAILY.DB.CR.FLAG.NC,XX>   = DB.CR
                        R.MAST.DAILY.TRN<MST.DAILY.REASON.CODE.NC,XX>  = REASON.COD.TR
                        R.MAST.DAILY.TRN<MST.DAILY.BILL.CURR.NC,XX>    = BILL.CURR.GL
                        R.MAST.DAILY.TRN<MST.DAILY.BILL.AMT.NC,XX>     = BILL.AMT.FMT
                        R.MAST.DAILY.TRN<MST.DAILY.POS.DATE.NC,XX>     = POST.DATE
                        R.MAST.DAILY.TRN<MST.DAILY.VALUE.DATE.NC,XX>   = PURCH.DATE
                        R.MAST.DAILY.TRN<MST.DAILY.TRANS.CURR.NC,XX>   = TRANS.CURR.GL
                        R.MAST.DAILY.TRN<MST.DAILY.TRANS.AMT.NC,XX>    = TRANS.AMT.FMT
                        R.MAST.DAILY.TRN<MST.DAILY.MSG.DESC.NC,XX>     = MSGE.DESC
                        R.MAST.DAILY.TRN<MST.DAILY.MERCH.CITY.NC,XX>   = MERCH.CITY
                        R.MAST.DAILY.TRN<MST.DAILY.MERCH.COUNTRY.NC,XX>= MERCH.COUNTRY

                        CALL F.WRITE(FN.MAST.DAILY.TRN,ID.KEY, R.MAST.DAILY.TRN)
                        CALL JOURNAL.UPDATE(ID.KEY)
                    END
**####***####
************TO WRITE TO TABLE SCB.VISA.TRANS**************************
                    ORG.MSG.CO = '' ; MSG.TYPE.CO = '' ; PROC.CODE.CO = '' ; DAILY.COMB = '' ; CODES.COMB = ''  ; DB.CR.CO = '' ;   REASON.CO.CO = ''
                    ORG.MSG.CODE = '' ; MSG.TYPE.CODE = '' ; PROC.CODE.CODE = '' ; DB.CR.CODE = '' ; REASON.CO.CODE = '' ; CODE.TO.USE = ''

                    ORG.MSG.F = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE.NC>
*Line [ 225 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    TT = DCOUNT(ORG.MSG.F,@VM)
                    ORG.MSG.CO   = R.MAST.DAILY.TRN<MST.DAILY.ORG.MSG.TYPE.NC,TT>
                    MSG.TYPE.CO = R.MAST.DAILY.TRN<MST.DAILY.MSG.TYPE.NC,TT>
                    PROC.CODE.CO = R.MAST.DAILY.TRN<MST.DAILY.PROCESS.CODE.NC,TT>
                    DB.CR.CO =   R.MAST.DAILY.TRN<MST.DAILY.DB.CR.FLAG.NC,TT>
                    REASON.CO.CO=  R.MAST.DAILY.TRN<MST.DAILY.REASON.CODE.NC,TT>
                    DAILY.COMB = ORG.MSG.CO:MSG.TYPE.CO:PROC.CODE.CO:DB.CR.CO:REASON.CO.CO

                    N.SEL =  "SELECT F.SCB.VISA.CODES.NEW WITH ORG.MSG.TYPE EQ ":ORG.MSG.CO:" AND MSG.TYPE EQ ":MSG.TYPE.CO:" AND PROCESS.CODE EQ ":PROC.CODE.CO:" AND FLAG EQ ":DB.CR.CO:" AND REASON.CODE EQ ":REASON.CO.CO
                    KEY.LIST.2=""
                    SELECTED.2=""
                    ER.MSG.2=""

                    CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)

                    F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.CODES.NEW' ; R.VISA.CODE = '' ; E3 = '' ; RETRY3 = ''
                    CALL OPF(FN.VISA.CODE,F.VISA.CODE)
                    IF SELECTED.2 THEN
                        FOR RR = 1 TO SELECTED.2
                            CALL F.READ(FN.VISA.CODE, KEY.LIST.2<RR>, R.VISA.CODE, F.VISA.CODE, E3)
                            ORG.MSG.CODE.T = R.VISA.CODE<SCB.VICD.ORG.MSG.TYPE>
*Line [ 247 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DCT = DCOUNT(ORG.MSG.CODE.T,@VM)
                            FOR WW = 1 TO DCT
                                ORG.MSG.CODE   = R.VISA.CODE<SCB.VICD.ORG.MSG.TYPE,WW>
                                MSG.TYPE.CODE  = R.VISA.CODE<SCB.VICD.MSG.TYPE,WW>
                                PROC.CODE.CODE = R.VISA.CODE<SCB.VICD.PROCESS.CODE,WW>
***************************NEW********************************************
                                DB.CR.CODE = R.VISA.CODE<SCB.VICD.FLAG,WW>
                                REASON.CO.CODE = R.VISA.CODE<SCB.VICD.REASON.CODE,WW>
**************************************************************************
                                CODES.COMB = ORG.MSG.CODE:MSG.TYPE.CODE:PROC.CODE.CODE:DB.CR.CODE:REASON.CO.CODE
                                IF DAILY.COMB = CODES.COMB THEN
                                    CODE.TO.USE = KEY.LIST.2<RR>
                                END ELSE
                                END
                            NEXT WW
                        NEXT RR
                        CALL F.READ(FN.MAST.TRANS, ID.KEY, R.MAST.TRANS, F.MAST.TRANS, E2)
                        IF NOT(E2) THEN
                            TRANS.CODE = R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC>
*Line [ 267 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            YY= DTC+1
                            R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC,YY>= CODE.TO.USE
                            R.MAST.TRANS<SCB.MAST.TRANS.CURR.NC,YY>= R.MAST.DAILY.TRN<MST.DAILY.BILL.CURR.NC,TT>
                            R.MAST.TRANS<SCB.MAST.TRANS.AMT.NC,YY>= R.MAST.DAILY.TRN<MST.DAILY.BILL.AMT.NC,TT>
                            R.MAST.TRANS<SCB.MAST.POS.DATE.NC,YY>= R.MAST.DAILY.TRN<MST.DAILY.POS.DATE.NC,TT>
                            CALL F.WRITE(FN.MAST.TRANS,ID.KEY, R.MAST.TRANS)
                            CALL JOURNAL.UPDATE(ID.KEY)
                        END ELSE
                            TRANS.CODE = R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC>
*Line [ 278 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            YY= DTC+1
                            R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC> = ''
                            R.MAST.TRANS<SCB.MAST.CUST.NAME.NC>= ' '
                            R.MAST.TRANS<SCB.MAST.CUST.ACCT.NC>= ' '
                            R.MAST.TRANS<SCB.MAST.TRANS.CODE.NC,YY>= CODE.TO.USE
                            R.MAST.TRANS<SCB.MAST.TRANS.CURR.NC,YY>=  R.MAST.DAILY.TRN<MST.DAILY.BILL.CURR.NC,TT>
                            R.MAST.TRANS<SCB.MAST.TRANS.AMT.NC,YY>= R.MAST.DAILY.TRN<MST.DAILY.BILL.AMT.NC,TT>
                            R.MAST.TRANS<SCB.MAST.POS.DATE.NC,YY>= R.MAST.DAILY.TRN<MST.DAILY.POS.DATE.NC,TT>
                            CALL F.WRITE(FN.MAST.TRANS,ID.KEY, R.MAST.TRANS)
                            CALL JOURNAL.UPDATE(ID.KEY)
                        END
                    END ELSE  ;** END OF SELECTED.2
                    END
**********************************************************************
                END ;**END OF SELECTED***

            END ELSE
                EOF = 1
            END

        REPEAT
        CLOSESEQ MyPath
        TEXT = 'END OF FILE' ; CALL REM
*********************************************************************
        CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
        R.RT.CHK<RT.CHK.BRANCH> = BR
        R.RT.CHK<RT.CHK.YEAR> = YEAR1
        R.RT.CHK<RT.CHK.MONTH> = MONTH1
        R.RT.CHK<RT.CHK.DAILY.TRN.NC> = 'YES'
        CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
        CALL JOURNAL.UPDATE(KEYID)
    END
    TEXT = '�� ������ �������� ����� ������'; CALL REM
    RETURN
END
