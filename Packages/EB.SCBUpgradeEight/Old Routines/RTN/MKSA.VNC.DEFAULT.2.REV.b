* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE MKSA.VNC.DEFAULT.2.REV

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN


    COMP = ID.COMPANY


*   Path = "/hq/opce/bclr/user/dltx/creditnew.txt"
*    Path = "/IMPL2/BRTEST/NT24/bnk/bnk.run/FCY.CLEARING/credit.NEW.txt"

        TOD = TODAY
        Path = "/hq/opce/bclr/user/dltx/":TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.txt"
        EOF = ''
        OPENSEQ Path TO MY.PATH ELSE
            TEXT = "ERROR OPEN FILE" ; CALL REM
            RETURN
        END

        GOSUB GET.DATE
        GOSUB GET.DATA
        RETURN

**--
GET.DATE:
        DAY1 = ""
        DAY2 = ""
        I    = 1
        EOD = ''
        LOOP WHILE NOT(EOF)
            READSEQ Line FROM MY.PATH THEN
                DAY.VAL              = FIELD(Line,",",2)
                IF ( DAY.VAL NE DAY1  AND I GT 1 ) THEN
                    DAY2 = DAY.VAL
                    EOF  = 1
                END ELSE
                    DAY1 = DAY.VAL
                END
                I = I + 1
            END ELSE
                EOF = 1
            END

        REPEAT

        CLOSESEQ MY.PATH
*************** GET FIRST AND SECOND DAY ******************
        DAY1.DD   = DAY1[1,2]
        DAY1.MM   = DAY1[4,2]
        DAY1.YYYY = DAY1[7,4]
        NEW.DAT1 = DAY1.YYYY : DAY1.MM : DAY1.DD
        DAY2.DD   = DAY2[1,2]
        DAY2.MM   = DAY2[4,2]
        DAY2.YYYY = DAY2[7,4]
        NEW.DAT2 = DAY2.YYYY : DAY2.MM : DAY2.DD
        IF NEW.DAT1 LT NEW.DAT2 THEN
            FIRST.DAY = DAY1
            SECOND.DAY = DAY2
        END ELSE
            FIRST.DAY = DAY2
            SECOND.DAY = DAY1
        END
        TEXT = "FIRST DAY IS ": FIRST.DAY ; CALL REM
        TEXT = "SECOD DAY IS ": SECOND.DAY ; CALL REM
        RETURN
***------------------------------------------------------------***
GET.DATA:
        EOF         = ''
        REFU        = 0
        ACPT        = 0
        ALL.AMT     = 0
        A.AMTTT     = 0
        R.AMTTT     = 0
        ALL.AMT.DT1 = 0
        A.AMTTT.DT1 = 0
        R.AMTTT.DT1 = 0
        ALL.AMT.DT2 = 0
        A.AMTTT.DT2 = 0
        R.AMTTT.DT2 = 0
*****************
        OPENSEQ Path TO MY.PATH THEN
            LOOP WHILE NOT(EOF)
                READSEQ Line FROM MY.PATH THEN
                    AMTT              = FIELD(Line,",",11)
                    TXT.COMP          = FIELD(Line,",",8)
                    COD               = FIELD(Line,",",1)
                    DT                = FIELD(Line,",",2)
************************************************************
                    IF AMTT NE '' AND  COD EQ 520 AND DT EQ FIRST.DAY  THEN
                        REFU.DT1 = REFU.DT1 + 1
                        R.AMTTT.DT1 = AMTT + R.AMTTT.DT1
                    END
                    IF AMTT NE '' AND COD EQ 820 AND DT EQ FIRST.DAY THEN
                        ACPT.DT1 = ACPT.DT1 + 1
                        A.AMTTT.DT1 = AMTT + A.AMTTT.DT1
                    END
***********************************************************
                    IF AMTT NE '' AND  COD EQ 520 AND DT EQ SECOND.DAY  THEN
                        REFU.DT2 = REFU.DT2 + 1
                        R.AMTTT.DT2 = AMTT + R.AMTTT.DT2
                    END
                    IF AMTT NE '' AND COD EQ 820 AND DT EQ SECOND.DAY  THEN
                        ACPT.DT2 = ACPT.DT2 + 1
                        A.AMTTT.DT2 = AMTT + A.AMTTT.DT2
                    END
*************************************************************
                    IF AMTT NE '' AND DT EQ FIRST.DAY  THEN
                        ALL.AMT.DT1 = ALL.AMT.DT1 + AMTT
                    END
                    IF AMTT NE '' AND DT EQ SECOND.DAY  THEN
                        ALL.AMT.DT2 = ALL.AMT.DT2 + AMTT
                    END


                END ELSE
                    EOF = 1
                END

            REPEAT
            CLOSESEQ MY.PATH
*    TEXT = "YES" ; CALL REM
***************************************************
            ACC.1 = '9943330010508001'
            ACC.2 = '9943330010508001'
            ACC.3 = '9943330010508499'
************************* DAY ONE *************************************
*  IF V$FUNCTION = 'I' OR V$FUNCTION = 'A' THEN
            IF V$FUNCTION = 'I' THEN
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1> = ACC.1
                R.NEW(INF.MLT.SIGN)<1,1> = 'CREDIT'
                R.NEW(INF.MLT.TXN.CODE)<1,1> = '79'
                R.NEW(INF.MLT.CURRENCY)<1,1> = 'EGP'
                R.NEW(INF.MLT.AMOUNT.LCY)<1,1> =  ALL.AMT.DT1
*----------------------------------------
*      R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,2> = ACC.2
*      R.NEW(INF.MLT.SIGN)<1,2> = 'DEBIT'
*      R.NEW(INF.MLT.TXN.CODE)<1,2> = '78'
*      R.NEW(INF.MLT.CURRENCY)<1,2> = 'EGP'
*      R.NEW(INF.MLT.AMOUNT.LCY)<1,2> =  R.AMTTT.DT1
*----------------------------------------
****************** DAY TWO ********************************************
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,2> = ACC.1
                R.NEW(INF.MLT.SIGN)<1,2> = 'CREDIT'
                R.NEW(INF.MLT.TXN.CODE)<1,2> = '79'
                R.NEW(INF.MLT.CURRENCY)<1,2> = 'EGP'
                R.NEW(INF.MLT.AMOUNT.LCY)<1,2> =  ALL.AMT.DT2
*----------------------------------------
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,3> = ACC.2
                R.NEW(INF.MLT.SIGN)<1,3> = 'DEBIT'
                R.NEW(INF.MLT.TXN.CODE)<1,3> = '78'
                R.NEW(INF.MLT.CURRENCY)<1,3> = 'EGP'
                R.NEW(INF.MLT.AMOUNT.LCY)<1,3> =  R.AMTTT.DT2 + R.AMTTT.DT1

***********************
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,4> = ACC.3
                R.NEW(INF.MLT.SIGN)<1,4> = 'DEBIT'
                R.NEW(INF.MLT.TXN.CODE)<1,4> = '78'
                R.NEW(INF.MLT.CURRENCY)<1,4> = 'EGP'
                R.NEW(INF.MLT.AMOUNT.LCY)<1,4> = A.AMTTT.DT1 + A.AMTTT.DT2
            END

            RETURN

    END
