* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-294</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.INW.AMEND

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
    EXT.NO = ""
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONDITIONS> EQ '' THEN
        GOSUB INITIATE
*Line [ 46 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        GOSUB CALLDB
        MYID = MYCODE:'.':MYTYPE
        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)

        IF MYCODE = "2111" THEN
****************************************
******************************************
            GOSUB PRINT.HEAD
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
        END ELSE
            E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*==============================================================
INITIATE:
    REPORT.ID='LG.INW.AMEND'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
PRINT.HEAD:
    PR.HD ="'L'":SPACE(1):"Suez Canal Bank"
    PR.HD :="'L'":SPACE(1):"Branch:":YYBRN : SPACE(5) : "ON " : ISSUE.DATE
    PR.HD :="'L'":SPACE(1):STR('_',30)
    HEADING PR.HD

    LNTH1=LEN(BENF1)+2;LNTH2=LEN(BENF2)+2
    LNE1 = 50-LNTH1;LNE2 = 50-LNTH2
    LNTHD1=LEN(ADDR1)+2;LNTHD2=LEN(ADDR2)+2;LNTHD3=LEN(ADDR3)+2
    LNED1 = 50-LNTHD1;LNED2= 50-LNTHD2;LNED3=50-LNTHD3
    PRINT

    PRINT " ________________________________________________"
    PRINT "| ":BENF1:SPACE(LNE1):"|"
    PRINT "| ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
****************************************
    PRINT "| ":ADDR1:SPACE(LNED1):"|"
    PRINT "| ":ADDR2:SPACE(LNED2):"|"
    PRINT "| ":ADDR3:SPACE(LNED3):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT

    PRINT SPACE(20):"Translation"
    PRINT SPACE(20): STR('_',11)
    PRINT

    PRINT SPACE(15):"Extension No( ":EXT.NO :" )"
    PRINT
    PRINT SPACE(15):"LG.No.Sc.": LG.NO
    PRINT
    PRINT SPACE(15):"For ":CUR: "**":LG.AMT:"**"
    PRINT
    PRINT SPACE(15):ISS.COU :"By Us On ":
    PRINT
    PRINT SPACE(15):"In Your Favour"
    PRINT
    PRINT SPACE(15):"B/O"
    PRINT
    PRINT SPACE(15):"Validity":": ": FIN.DATE

    PRINT SPACE(15):"________________________________________________"

    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = ''
    CALL OPF(FN.CUST,F.CUST)


    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>

    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    ADDR3 =LOCAL.REF<1,LDLR.BNF.DETAILS,3>

    ISS.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE = ISS.DATE[7,2]:"/":ISS.DATE[5,2]:"/":ISS.DATE[1,4]
    FIN.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = FIN.DATE[7,2]:"/":FIN.DATE[5,2]:"/":FIN.DATE[1,4]
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.REF = LOCAL.REF<1,LDLR.SENDING.REF>
    MSG.TYPE=R.LD<LD.OUR.REMARKS>
    SWIFT.DATE = LOCAL.REF<1,99>
    SWIFT.DATE = SWIFT.DATE[7,2]:"/":SWIFT.DATE[5,2]:"/":SWIFT.DATE[1,4]

    LC.REF = LOCAL.REF<1,94>
    ISS.COU = LOCAL.REF<1,95>
    FOR.DEL = LOCAL.REF<1,96>
    BEN.PRI = LOCAL.REF<1,97>
    REM.CRE = LOCAL.REF<1,98>

    CHR.CODE=LOCAL.REF<1,LDLR.T.CHRG.CODE>
    CHRG.AMT.COMM=LOCAL.REF<1,LDLR.T.CHRG.AMT,1>
    CHRG.AMT.STMP=LOCAL.REF<1,LDLR.T.CHRG.AMT,2>
    CHRG.AMT.POST=LOCAL.REF<1,LDLR.T.CHRG.AMT,3>
    CHRG.AMT=CHRG.AMT.COMM+CHRG.AMT.STMP+CHRG.AMT.POST

    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CUR.CO=LOCAL.REF<1,LDLR.ACC.CUR>

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME

    MYCODE =LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
    CUST.ID = R.LD<LD.CUSTOMER.ID>
    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.ID,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YYBRN = FIELD(BRANCH,'.',1)
**********************

    CALL F.READ(FN.CUST,CUST.ID,R.CUST,F.CUST,E1)
    CUST.NAME=R.CUST<EB.CUS.SHORT.NAME>
    CUST.ADD=R.CUST<EB.CUS.STREET>

    SEND.BANK=LOCAL.REF<1,LDLR.SEN.REC.BANK>
    CALL F.READ(FN.CUST,SEND.BANK,R.CUST,F.CUST,E1)
    SEND.NAME=R.CUST<EB.CUS.SHORT.NAME>
    SEND.ADD=R.CUST<EB.CUS.STREET>

********************
    SAM   = LG.NO[4,2]

    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
    LG.NO='LG/': SAM:"/": SAM1:"/":SAM2:LC.REF
********************
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    RETURN
*===============================================================
BODY:
    PRINT;PRINT

    PRINT SPACE(8):"With reference to your letter dated ": SWIFT.DATE : " ref. MTF " : MSG.TYPE : "and at the consent"
    PRINT

    PRINT SPACE(5):"of our correspondent we hereby extend the validity of the above mentioned LG "
    PRINT

    PRINT SPACE(5):"until ":FIN.DATE :"The other terms and conditions ruling this gurantee remain unchanged."
    PRINT

*    IF LC.REF NE '' THEN
*       PRINT SPACE(5):"Requesting them to authorize us to amend LC No" : LC.REF : "to be operative."
*      PRINT
* END

    PRINT SPACE(5):"Consequently any claim in respect there of should be made to us by the "
    PRINT

    PRINT SPACE(5):"()":"at the latest should we receive no claim"
    PRINT

    PRINT SPACE(5):"from you by that date our liability will cease(IPSO FACTO) and the present"
    PRINT

    PRINT SPACE(5):"LG will definitely,become null and void."
    PRINT

    PRINT SPACE(05):"Please return to us the letter of gurantee together with its letter of extension"
    PRINT

    PRINT SPACE(09):"When no longer required for cancellation ."
    PRINT

    PRINT SPACE(35):"Yours faithfully,"
    PRINT SPACE(35):"For The Suez Canal Bank"
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

    RETURN
END
*==================================================================
