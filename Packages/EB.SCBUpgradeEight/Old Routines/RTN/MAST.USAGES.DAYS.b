* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>464</Rating>
*-----------------------------------------------------------------------------
*****WRITTEN BY NESSREEN AHMED 23/10/2013**************

    SUBROUTINE MAST.USAGES.DAYS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.TRANS.TOT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.USG.DAYS

    F.MAST.DAYS = '' ; FN.MAST.DAYS = 'F.SCB.MAST.USG.DAYS' ; R.MAST.DAYS = '' ; E1 = ''
    CALL OPF(FN.MAST.DAYS,F.MAST.DAYS)

    F.MAST.TRANS.TOT = '' ; FN.MAST.TRANS.TOT = 'F.SCB.MAST.TRANS.TOT' ; R.MAST.TRANS.TOT = '' ; E2 = ''
    CALL OPF(FN.MAST.TRANS.TOT,F.MAST.TRANS.TOT)
******************************************
    YTEXT = "Enter the Start Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    YYDD = COMI

    T.SEL = "SELECT F.SCB.MAST.TRANS.TOT WITH POS.DATE GE ":YYDD :" BY BRANCH.NUMBER "

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED.VISA.TRANS=':SELECTED ; CALL REM
    IF SELECTED THEN          ;* SELECTED OF I*
        FOR I = 1 TO SELECTED
            KEY.TO.USE = ''; TRANS.CODE = ''
            TRANS.AMT = '' ; XX = '' ; BR.NO = ''
            CALL F.READ(FN.MAST.TRANS.TOT, KEY.LIST<I>, R.MAST.TRANS.TOT, F.MAST.TRANS.TOT, E2)

            TRANS.CODE = R.MAST.TRANS.TOT<MST.MAST.TRANS.CODE>
*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DTC = DCOUNT(TRANS.CODE,@VM)
            FOR DD = 1 TO DTC
                TRANS.CODE = '' ; TRANS.AMT = '' ; TRANS.POS.DAT = ''
                TRANS.CODE = R.MAST.TRANS.TOT<MST.MAST.TRANS.CODE,DD>
                TRANS.AMT  = R.MAST.TRANS.TOT<MST.MAST.TRANS.AMT,DD>
                TRANS.POS.DAT = R.MAST.TRANS.TOT<MST.MAST.POS.DATE,DD>

                CALL F.READ(FN.MAST.DAYS, TRANS.POS.DAT , R.MAST.DAYS, F.MAST.DAYS, E1)
                IF NOT(E1) THEN
                    BEGIN CASE
                    CASE TRANS.CODE = '1'
                        R.MAST.DAYS<MSMD.AMT1> += TRANS.AMT
                    CASE TRANS.CODE = '2'
                        R.MAST.DAYS<MSMD.AMT2> += TRANS.AMT
                    CASE TRANS.CODE = '3'
                        R.MAST.DAYS<MSMD.AMT3> += TRANS.AMT
                    CASE TRANS.CODE = '4'
                        R.MAST.DAYS<MSMD.AMT4> += TRANS.AMT
                    CASE TRANS.CODE = '5'
                        R.MAST.DAYS<MSMD.AMT5> += TRANS.AMT
                    CASE TRANS.CODE = '6'
                        R.MAST.DAYS<MSMD.AMT6> += TRANS.AMT
                    END CASE
                   CALL F.WRITE(FN.MAST.DAYS, TRANS.POS.DAT, R.MAST.DAYS)
                   CALL JOURNAL.UPDATE(TRANS.POS.DAT)

                END ELSE
                    BEGIN CASE
                    CASE TRANS.CODE = '1'
                        R.MAST.DAYS<MSMD.AMT1> += TRANS.AMT
                    CASE TRANS.CODE = '2'
                        R.MAST.DAYS<MSMD.AMT2> += TRANS.AMT
                    CASE TRANS.CODE = '3'
                        R.MAST.DAYS<MSMD.AMT3> += TRANS.AMT
                    CASE TRANS.CODE = '4'
                        R.MAST.DAYS<MSMD.AMT4> += TRANS.AMT
                    CASE TRANS.CODE = '5'
                        R.MAST.DAYS<MSMD.AMT5> += TRANS.AMT
                    CASE TRANS.CODE = '6'
                        R.MAST.DAYS<MSMD.AMT6> += TRANS.AMT
                    END CASE
                    CALL F.WRITE(FN.MAST.DAYS,TRANS.POS.DAT, R.MAST.DAYS)
                    CALL JOURNAL.UPDATE(TRANS.POS.DAT)
                END
            NEXT DD
        NEXT I
    END

    RETURN
END
