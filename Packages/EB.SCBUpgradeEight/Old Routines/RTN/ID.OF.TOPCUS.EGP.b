* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE ID.OF.TOPCUS.EGP(ENQ.DATA)
*    PROGRAM ID.OF.TOPCUS.EGP
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TOPCUS.CR.TOT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY


    FN.TOP.TOT = "F.SCB.TOPCUS.CR.TOT"
    F.TOP.TOT = ''
    R.SCB.TOPCUS.CR.TOT=''
    Y.TOP.TOT.ID=''
    Y.TOP.TOT.ERR=''
    K = 0

    CALL OPF(FN.TOP.TOT,F.TOP.TOT)
*MSABRY
*   SEL.CMD ="SSELECT " :FN.TOP.TOT:" WITH CCY.CUS EQ LCY BY TOTAL.CUS"
    SEL.CMD ="SSELECT " :FN.TOP.TOT:" WITH CCY.CUS EQ LCY AND CO.CODE EQ ":COMP:" BY TOTAL.CUS"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    IF NOREC THEN
        FOR I = NOREC TO 1 STEP -1
            K ++
            ENQ.DATA<2,K> = "@ID"
            ENQ.DATA<3,K> = "EQ"
            ENQ.DATA<4,K> = SELLIST<I>
            IF K = 20 THEN I = 1
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END

    RETURN
END
