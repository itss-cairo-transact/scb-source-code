* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>78</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE GET.VER.ENQ.LIST(MENU.ID,ENQ.LIST,VER.LIST)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MENU

    GOSUB INITIALISE
    GOSUB PROCESS

    RETURN

*
**
*
INITIALISE:
*---------

    FN.HTM = "F.HELPTEXT.MENU"
    FV.HTM = ""
    CALL OPF(FN.HTM,FV.HTM)

    RETURN
*
**
*
PROCESS:
*------

    CALL F.READ(FN.HTM,MENU.ID,R.HTM,FV.HTM,MENU.ERR)
    IF NOT(MENU.ERR) THEN
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.OF.ITEMS = DCOUNT(R.HTM<EB.MEN.APPLICATION>,@VM)
        FOR ITEM.IND = 1 TO NO.OF.ITEMS
            ITEM.ID = R.HTM<EB.MEN.APPLICATION,ITEM.IND>
            BEGIN CASE
            CASE FIELD(ITEM.ID," ",1,1) = "ENQ"   ;* The item is an enquiry
                ENQ.ID = FIELD(ITEM.ID," ",2,1)
                LOCATE ENQ.ID IN ENQ.LIST<1> SETTING ENQ.POS ELSE
                    ID.TO.ADD = "ENQUIRY>":ENQ.ID
                    INS ID.TO.ADD BEFORE ENQ.LIST<ENQ.POS>
                END
            CASE INDEX(ITEM.ID,",",1)   ;* The item is a version
                LOCATE ITEM.ID IN VER.LIST<1> SETTING VER.POS ELSE
                    ID.TO.ADD = "VERSION>":ITEM.ID
                    INS ID.TO.ADD BEFORE VER.LIST<VER.POS>
                END
            CASE ITEM.ID = "*"
*Line [ 68 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                NULL
            CASE OTHERWISE
*Line [ 71 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                CALL EB.SCBUpgradeEight.GET.VER.ENQ.LIST(ITEM.ID,ENQ.LIST,VER.LIST)
            END CASE
        NEXT ITEM.IND
    END

    RETURN
END
