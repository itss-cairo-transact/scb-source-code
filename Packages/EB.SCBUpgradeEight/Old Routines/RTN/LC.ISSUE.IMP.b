* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE LC.ISSUE.IMP
**PROGRAM LC.ISSUE.IMP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='LC.ISSUE.IMP'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------

    IF ID.NEW EQ '' THEN
        FN.LC = 'FBNK.LETTER.OF.CREDIT'
    END ELSE
        FN.LC = 'FBNK.LETTER.OF.CREDIT$NAU'
    END
    F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    IF ID.NEW EQ '' THEN
        YTEXT = "Enter the TF No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        LC.ID =COMI[1,12]
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,E1)
        LC.REF=LC.ID
    END ELSE
******************************************
        IF ID.NEW NE '' THEN
            LC.ID =ID.NEW
            CALL F.READ(FN.LC,ID.NEW,R.LC,F.LC,E1)
            LC.REF=ID.NEW
        END
    END



    CUS.NO = R.LC<TF.LC.APPLICANT.CUSTNO>
    AC.NO  = R.LC<TF.LC.PROVIS.ACC>

***UPDATED BY HYTHAM ---UPGRADING R15---2016-03-07---
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,AC.NO,AC.COMP)
    BRANCH.ID  = AC.COMP[8,2]
********
  **  CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,AC.NO,BRANCH.ID)
    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL.REF)
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS>
    NN.TITLE = 'LC.ISSUE.IMP'


    LC.NO = R.LC<TF.LC.OLD.LC.NUMBER>
    AMOUNT = R.LC<TF.LC.LC.AMOUNT>
    CUR.ID = R.LC<TF.LC.LC.CURRENCY>

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)


    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]    = '��� ������       :  ':CUST.NAME
    XX9<1,1>[3,35]   = '����������       :  ':CUST.ADDRESS

    XX1<1,1>[30,15] = '������ ������ ��� :  ':LC.NO:' - ':LC.REF

    XX2<1,1>[30,15] = '��������          :  ':AMOUNT : ' ' : CUR

    XX3<1,1>[3,35]   = '����� �������� ����� �� ���� ���� �������� �������� ������ ������ �����'
    XX4<1,1>[3,35]   = '- ���� �� ����� ��������'
    XX5<1,1>[3,35]   = '- ����� �������� ���� ������� ���� ����� �� ������ �� ���� ���� ��� ��������'
    XX6<1,1>[3,35]   = '�� ��������� �� ��� ����� ���� ������ ��� ������ ���� ��� ���� �� ����� ��� ��������'
    XX7<1,1>[3,35]   = '������ ������ ���������'
    XX8<1,1>[30,35]  = '������� ����� ���� ��������'
*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):NN.TITLE
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX9<1,1>
    PRINT STR(' ',80)
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT STR(' ',80)
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT STR(' ',80)
    PRINT XX7<1,1>
    PRINT STR(' ',80)
    PRINT XX8<1,1>
*===============================================================
    RETURN
END
