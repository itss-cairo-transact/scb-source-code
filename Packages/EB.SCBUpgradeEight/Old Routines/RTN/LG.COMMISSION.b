* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>379</Rating>
*-----------------------------------------------------------------------------
** ----- 01.03.2003 (MOHAMED MAHMOUD) -----

    SUBROUTINE LG.COMMISSION(CUSTOMER, DEAL.AMOUNT, DEAL.CURRENCY, CURRENCY.MARKET, CROSS.RATE, CROSS.CURRENCY, DRAWDOWN.CCY, T.DATA, CUST.COND, CHARGE.AMOUNT)


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER



*    IF  R.NEW(LD.DEFINE.SCHEDS)='YES' THEN
*       CALL LG.COMMISSION.PART(CUSTOMER, DEAL.AMOUNT, DEAL.CURRENCY, CURRENCY.MARKET, CROSS.RATE, CROSS.CURRENCY, DRAWDOWN.CCY, T.DATA, CUST.COND, CHARGE.AMOUNT)
*  END ELSE
    SHIFT ='' ; ROUND =''


*DEBUG
    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)

** ----- ----- -----

*TEXT = '1 -CUSTOMER    = ': CUSTOMER                                  ; CALL REM
*TEXT = '2 -AMOUNT      = ': DEAL.AMOUNT                               ; CALL REM
*TEXT = '3 -CURRENCY    = ': DEAL.CURRENCY                             ; CALL REM
*TEXT = '4 -MARKET      = ': CURRENCY.MARKET                           ; CALL REM
*TEXT = '5 -RATE        = ': CROSS.RATE                                ; CALL REM
*TEXT = '6 -CROSS.CUR   = ': CROSS.CURRENCY                            ; CALL REM
*TEXT = '7 -DRAWDOWN.CCY= ': DRAWDOWN.CCY                              ; CALL REM
*TEXT = '8 -T.DATA      = ': T.DATA                                    ; CALL REM
*TEXT = '9 -CUST.COND   = ': CUST.COND                                 ; CALL REM
*TEXT = '10-ISSUE.DATE  = ': R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>    ; CALL REM
*TEXT = '11-MAT.DATE    = ': R.NEW(LD.FIN.MAT.DATE)                    ; CALL REM
*TEXT = '12-MARGIN.AMT  = ': R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>    ; CALL REM
*TEXT = '13-OPER.CODE   = ': R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>; CALL REM
*TEXT = '14-COMM.CURR   = ': R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>       ; CALL REM

    FN.LG.CHARGE= 'F.SCB.LG.CHARGE' ; F.LG.CHARGE = ''
    CALL OPF(FN.LG.CHARGE,F.LG.CHARGE)

    W.CUR          = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
*    W.EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
    W.EXP.DATE     = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
    W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
    W.ISSUE.DATE   = R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
    W.PRODUCT.TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    W.OPER.NO      = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':W.PRODUCT.TYPE
    START.DATE     = W.ISSUE.DATE
    OPER.NO        = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    W.END.COMM.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
    COMM.NO         = 1
    COMM.AMT1       = 0
    FLG1            = ''
    FLG2            = ''
    FLG3            = ''
    FLG4            = ''

*TEXT = W.OPER.NO ; CALL REM

    IF OPER.NO = '1111' THEN
        GOSUB COMM.CALC
* TEXT = " ## 0 ## " ; CALL REM
    END
************2-2-2005*********
    IF OPER.NO = '1239' OR OPER.NO ='1301' OR OPER.NO EQ '1281' OR OPER.NO EQ '1282' THEN
        GOSUB COMM.CALC
    END
*****************************

*                  ................................................
*----------------- Applied In Cases For (OPERATIVE FOR ADVANCE L/Gs --------------------
*                  ................................................

    IF OPER.NO = 1251 THEN
*LEN1 = LEN(R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>)
        W.ISSUE.DATE   = R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.DATE>
        START.DATE     = W.ISSUE.DATE
        DEAL.AMOUNT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>      ;*[4,LEN1-3]
        GOSUB COMM.CALC
*  TEXT = " ## 1 ## " ; CALL REM
    END

*                  ..................................................
*----------------- Applied In Cases For Change Of (EXP.DATE & AMOUNT) --------------------
*                  ..................................................

    IF OPER.NO = 1231 THEN
* TEXT = 'EXP.DATE = ':W.EXP.DATE ; CALL REM
        IF W.END.COMM.DATE >= W.EXP.DATE THEN
            W.MARGIN.AMT = DEAL.AMOUNT
            FLG1 = 'N'
        END ELSE
            START.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
        END
        GOSUB COMM.CALC
* TEXT = " ## 2 ## " ; CALL REM
    END

    IF OPER.NO = 1234 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> < '100' THEN
**        W.EXP.DATE     = R.OLD(LD.FIN.MAT.DATE)
        W.EXP.DATE=R.OLD(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        DEAL.AMOUNT    = R.NEW(LD.AMOUNT.INCREASE)
        W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
        FLG2 = 'N' ; FLG3 = 'N'
        IF W.END.COMM.DATE >= W.EXP.DATE THEN
            FLG4 = 'N'
        END
        GOSUB COMM.CALC
* TEXT = " ## 3 ## " ; CALL REM
        FLG4 = '' ; FLG2 = '' ; FLG3 = ''
        COMM.AMT1 = CHARGE.AMOUNT

        IF W.END.COMM.DATE < W.EXP.DATE THEN
            DEAL.AMOUNT    = R.NEW(LD.AMOUNT.INCREASE) + R.NEW(LD.AMOUNT)
            W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
*            W.EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
            W.EXP.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            START.DATE     = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            GOSUB COMM.CALC
*   TEXT = " ## 4 ## " ; CALL REM
        END

    END

    IF OPER.NO = 1235  THEN
        DEAL.AMOUNT    = R.NEW(LD.AMOUNT) + R.NEW(LD.AMOUNT.INCREASE)

        IF W.END.COMM.DATE >= W.EXP.DATE THEN
            W.MARGIN.AMT = DEAL.AMOUNT
            FLG1 = 'N'
        END ELSE
            START.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
        END
        GOSUB COMM.CALC
*TEXT = " ## 5 ## " ; CALL REM
    END

    IF OPER.NO = 1232 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> < '100' THEN
        DEAL.AMOUNT    = R.NEW(LD.AMOUNT.INCREASE)
        W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
        FLG2 = 'N' ; FLG3 = 'N'
        GOSUB COMM.CALC
*TEXT = " ## 6 ## " ; CALL REM
    END

    IF OPER.NO=1233 OR (OPER.NO=1232 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>='100') THEN
        W.MARGIN.AMT = DEAL.AMOUNT
        COMM.NO      = 1
        FLG1         = 'N'
        GOSUB COMM.CALC
*  TEXT = " ## 7 ## " ; CALL REM
    END

    IF OPER.NO=1234 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>='100' THEN
        W.MARGIN.AMT = DEAL.AMOUNT
        COMM.NO      = 1
        FLG1         = 'N'
        GOSUB COMM.CALC
*  TEXT = " ## 8 ## " ; CALL REM
    END


*                  ..............................................
*----------------- Applied In Cases For Change Of (MARGIN AMOUNT) --------------------
*                  ..............................................

*   IF (OPER.NO=1241 AND R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = '100') THEN FLG3 = 'N'
    IF (OPER.NO=1241 AND R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = '100') THEN
        START.DATE = TODAY
        GOSUB COMM.CALC
*   TEXT = " ## 8 ## " ; CALL REM
    END ELSE
        IF (OPER.NO=1241 AND R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> < '100') OR (OPER.NO='1242') THEN
            W.MARGIN.AMT = DEAL.AMOUNT
            COMM.NO      = 1
            FLG1         = 'N'
            GOSUB COMM.CALC
*   TEXT = " ## 9 ## " ; CALL REM
        END
    END

    RETURN

*   IF OPER.NO = 1241 THEN
*      FLG2 = 'N'
*   END

*                  ........................................
*----------------- To Calculate ( END.COMM.DATE & COMM.NO ) --------------------
*                  ........................................

COMM.CALC:
*-------------
*TEXT = 'MARGIN = ':W.MARGIN.AMT ; CALL REM
*TEXT = 'AMT = ' :DEAL.AMOUNT  ; CALL REM

    IF FLG1 # 'N' THEN
        TMP.NO = 0
* TEXT = 'START = ':START.DATE ; CALL REM
* TEXT = 'TODAY = ' :TODAY ; CALL REM
        FOR I = 1 TO 100
*TEXT = "START.DATE = " : START.DATE ; CALL REM
            START.DATE = SHIFT.DATE( START.DATE, '3M', 'UP')
            TMP.DATE = START.DATE
            IF TMP.NO = 0 AND FLG3 = 'N' AND TMP.DATE > TODAY THEN
                TMP.NO = I - 1
            END
            CALL CDT('', TMP.DATE, '2')
            IF TMP.DATE > W.EXP.DATE THEN
*TEXT = " I = " : I ; CALL REM
*TEXT = " TMP.NO = " : TMP.NO ; CALL REM
                COMM.NO = I - TMP.NO ; I = 100
            END
        NEXT I
*DEBUG
* TEXT = 'CDT = ' : START.DATE ; CALL REM
        CALL CDT('EG', START.DATE, '-1')

        IF W.MARGIN.AMT = DEAL.AMOUNT OR W.OPER.NO = '1111.ADVANCE' OR W.OPER.NO = '1112.ADVANCE' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = W.EXP.DATE ; CALL REBUILD.SCREEN
        END ELSE
            IF FLG2 # 'N' AND W.MARGIN.AMT # DEAL.AMOUNT THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = START.DATE ; CALL REBUILD.SCREEN
            END
        END

* TEXT = 'START = ':START.DATE ; CALL REM
* TEXT = 'TODAY = ' :TODAY ; CALL REM
* TEXT = 'COMM.NO = ' : COMM.NO ; CALL REM
* TEXT = 'TMP.NO = ': TMP.NO ; CALL REM
    END

*--------------------------------------------------------------------------------

    CALL F.READ(FN.LG.CHARGE,W.OPER.NO,R.OPER,F.LG.CHARGE,E1)
    LOCATE W.CUR IN R.OPER<SCB.LG.CH.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
    W.CHARGE.CODE = R.OPER<SCB.LG.CH.CHARGE.CODE,J>

**                                 = ................. =
**                                === ............... ===
**                               =====  ISSUEING L/G =====
**                                === ............... ===
**                                 = ................. =


*TEXT = 'OLD = ' :R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> ; CALL REM
*TEXT = 'CHRG.CODE = ':W.CHARGE.CODE ; CALL REM

*Line [ 280 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    FOR K = 1 TO DCOUNT(W.CHARGE.CODE,@SM)
        IF W.MARGIN.AMT = DEAL.AMOUNT OR R.OPER<SCB.LG.CH.RATE,J,K> = '0' THEN
            IF K = 1 THEN
                CHARGE.AMOUNT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K> + COMM.AMT1
*  TEXT = 'CHRG.AMT = ':R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K> ; CALL REM
            END ELSE
                IF FLG4 # 'N' THEN
                    R.NEW(LD.CHRG.CODE)<1,K> = R.OPER<SCB.LG.CH.CHARGE.CODE,J,K>
                    R.NEW(LD.CHRG.AMOUNT)<1,K> = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                END
            END
            CALL REBUILD.SCREEN
        END ELSE
            IF R.OPER<SCB.LG.CH.RATE,J,K> > '0' THEN
                COMM.AMT = DEAL.AMOUNT * R.OPER<SCB.LG.CH.RATE,J,K>
*  TEXT = 'COMM = ' : COMM.AMT ; CALL REM
                IF COMM.AMT < R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K> THEN
                    COMM.AMT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                END
            END ELSE
                COMM.AMT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
            END
            IF K = 1 THEN
                CHARGE.AMOUNT = (COMM.AMT * COMM.NO) + COMM.AMT1
            END ELSE
                IF FLG4 # 'N' THEN
                    R.NEW(LD.CHRG.CODE)<1,K> = R.OPER<SCB.LG.CH.CHARGE.CODE,J,K>
                    R.NEW(LD.CHRG.AMOUNT)<1,K> = COMM.AMT * COMM.NO
                END
            END
            CALL REBUILD.SCREEN
        END
    NEXT K

*   END
*    END
CALL REBUILD.SCREEN
    RETURN
END
