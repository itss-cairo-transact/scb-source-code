* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-271</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.ADVOPER.1251

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

*Line [ 44 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    FOR II = 1 TO 2
        GOSUB INITIATE
        GOSUB PRINT.HEAD
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    NEXT II
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.ADVOPER.1251'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
************************************************
    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
************************************************
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME.2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    LG.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    LG.AMT =R.LD<LD.AMOUNT>
    LG.CHQ=LOCAL.REF<1,LDLR.CHQ.AMT.CUR>

    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    IN.DATE  = LG.MAT.DATE
    IN.AMOUNT= LG.CHQ

    DATY = TODAY
    XXX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)

    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LD.LOCAL.REF<1,LDLR.PRODUCT.TYPE>
******************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF = COMI
    END
    LG.CO= R.LD<LD.CO.CODE>

    RETURN
*===============================================================
PRINT.HEAD:
    MYID = MYCODE:'.':MYTYPE
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD ="'L'":SPACE(1):"����������":":":MYBRANCH
    HEADING PR.HD
    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT
    PRINT SPACE(20): "���� �������� ��� : ":LG.NO:" )":TYPE.NAME:"("
    PRINT
    PRINT SPACE(20): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(20): "������ ��� ������ : ":XX
    PRINT

*//////////////////////////////////////////////////////
    IF MYCODE[2,3]= "111" THEN
        PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
    END ELSE
        PRINT SPACE(20): "����������� ����� : ":THIRD.NAME
        IF THIRD.NAME.2 THEN
            PRINT SPACE(5):THIRD.NAME.2
        END ELSE
            PRINT SPACE(5):""
        END
    END
*//////////////////////////////////////////////////////
    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    IF LG.CHQ EQ LG.AMT THEN
        PRINT SPACE(5):"�������� ��� ���� ������ ������ ������ � ����� ������� ������ �����"
        PRINT SPACE(5):"������� �������� ��������� ������ ������ ��� ���� ������ ��� ������ "
        PRINT SPACE(5):"����� ������ ������� ��� ": YY : "."
        PRINT SPACE(5):"� ���� ���� ��� ���� ������� ������� ���� �� ����� �� ����� �������"
        PRINT SPACE(5):") ": OUT.DATE :" ( ."
        PRINT;PRINT
    END ELSE
        PRINT SPACE(5):"�������� ��� ���� ������ ������ ������ � ����� ������� ������ ����" : LG.CHQ:CRR :" �� ����"
        PRINT SPACE(5):"������� �������� ��������� ������ ������ ��� ���� ������ ��� ������ "
        PRINT SPACE(5):"����� ������ �������": "����� ����� ����� " : LG.CHQ : CRR :" ���" :YY
        PRINT SPACE(5):"� ��� �� ���� ���� ������ � ������� ��� �� ���� ����� ."
        PRINT SPACE(5):"� ��� ��� ��� �������� ����� ��� ������ �� ����� ���� " : LG.CHQ : CRR
        IF IN.AMOUNT EQ '' THEN
            OUT.AMOUNT = ''
        END
*Line [ 192 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 194 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
                PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR
            END ELSE
                PRINT SPACE(2): OUT.AMOUNT<1,I>
            END
        NEXT I
        PRINT SPACE(5):"�� ����� ����� ) ": OUT.DATE :"( ."
    END
    PRINT SPACE(5):"���� �� ����� ���� ��� ������ ��� ��� ������� ���� �������� ������� "
    PRINT
    PRINT SPACE(5):"� ���� ��� ������ ����� ���� ������ ."

    PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT
    IF II = '1' THEN
        PRINT SPACE(5): "���� ��� : ":THIRD.NAME
        IF THIRD.NAME.2 THEN
            PRINT SPACE(5):THIRD.NAME.2
        END ELSE
            PRINT SPACE(5):""
        END
        PRINT SPACE(15):THIRD.ADDR1
        PRINT SPACE(15):"����� ������ ��������"
    END  ELSE
        PRINT;PRINT;PRINT;PRINT
    END
    PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT;PRINT  "������ �� :":XXX
    PRINT SPACE(5):"LG.ADVOPER.1251"
*Line [ 225 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==================================================================
