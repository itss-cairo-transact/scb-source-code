* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
**********ABEER***********
*-----------------------------------------------------------------------------
* <Rating>-47</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.AMAMTDATE.1231.BENF
* PROGRAM LG.AMAMTDATE.1231.BENF

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        CUST=R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL>
        IF MYCODE='1231' AND CUST='CUS' THEN
*  IF (MYCODE='1231' OR MYCODE='1111') AND (CUST='CUS' OR CUST='BEN') THEN
            GOSUB INITIATE
*Line [ 56 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            GOSUB PRINT.HEAD
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
        END
    END ELSE
        IF ID.NEW EQ '' THEN
*Line [ 65 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            IF MYCODE='1231' AND CUST='CUS'  THEN
                GOSUB INITIATE
                GOSUB PRINT.HEAD
                GOSUB BODY
                CALL PRINTER.OFF
                CALL PRINTER.CLOSE(REPORT.ID,0,'')
            END ELSE
                MYID=MYCODE:'.':MYTYPE
                OPER.CODE=MYID
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.1231.BENF'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN  YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN  CALL TXTINP(YTEXT, 8, 22, "12", "A")

    IF ID.NEW THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    ADDR3 =LOCAL.REF<1,LDLR.BNF.DETAILS,3>
**THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CUST=LOCAL.REF<1,LDLR.APPROVAL>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    CURRNO = R.LD<LD.CURR.NO>
    CURRNO1= CURRNO - 1
    DATE.CORR =LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.ISSUE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE =DATE.ISSUE[7,2]:"/":DATE.ISSUE[5,2]:"/":DATE.ISSUE[1,4]
*   DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]

**********
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
**********MODIFIED ON 29/2/2012

    THIRD.NO = LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CUST.ID=R.LD<LD.CUSTOMER.ID>
    IF THIRD.NO NE CUST.ID THEN
        CUST.AC = THIRD.NO
    END ELSE
        CUST.AC =CUST.AC
    END
*************
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME.2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>

    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END

    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT AND FIN MAT DATE************************************

    IF ID.NEW EQ '' THEN

        IDHIS='';ID='';OLD.ID=''
        IDHIS=COMI:";":CURRNO1
        ID=IDHIS
        OLD.ID=IDHIS

        FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
        CALL OPF(FN.LG.HIS,F.LG.HIS)
        CALL F.READ(FN.LG.HIS,IDHIS, R.LG.HIS, F.LG.HIS ,ETEXT)
    END
***************************************************************************

    IF ID.NEW EQ '' THEN
        LOCAL.REF=R.LG.HIS<LD.LOCAL.REF>
        OLD.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
        FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
    END ELSE
        IF ID.NEW THEN
            OLD.FIN=R.OLD(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
        END
    END
********************
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END
**********************************************************************************
**********************************************************************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
        INPUTTER    = R.NEW(LD.INPUTTER)
        INP         = FIELD(INPUTTER,'_',2)
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF         = COMI
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>

    RETURN
*===============================================================
PRINT.HEAD:
    MYID = MYCODE:'.':MYTYPE
*UPDATED BY HYTHAM ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD  ="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(28):"������ ���":"*":EXT.NO:"*"
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
  PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
 PRINT "|         : ":ADDR3:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���    : ":LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "�������������    : ": " **":NEW.AMOUNT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
    PRINT SPACE(20):"������ ������    : ":ISSUE.DATE
    PRINT
    PRINT SPACE(20): "������� � �����  : ":THIRD.NAME
    PRINT
    IF THIRD.NAME.2 THEN
        PRINT SPACE(5):THIRD.NAME.2
    END ELSE
        PRINT SPACE(5):""
    END
    PRINT SPACE(20): "���� ������� ��� : ":FIN.OLD
*  END
*//////////////////////////////////////////////////////
    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(2):"���� ��� ��� ���������������� ."
    PRINT;PRINT
    PRINT SPACE(2):"��� ������ ��� ����� ���� ������ ������ ������� ���������":FIN.DATE
    PRINT
    PRINT SPACE(2):"�� ���� ���� ������ ������ ���� ����� ."
    PRINT;PRINT
    PRINT SPACE(2):"� ��� ��� ��� ��� ������  �� ��� ����� ��� �� ���� �� ���� �����"
    PRINT
    PRINT SPACE(2):"( ":OUT.DATE:".":" )"
    PRINT;PRINT
    PRINT SPACE(6):"� ����� ������ ����� �� ����� ���� ����� ������ ���� ����������"
    PRINT
    PRINT SPACE(2):" ��� ������ � ��� ����� �� �������� ���� �������ϡ ��� ����� �����"
    PRINT
    PRINT SPACE(2):" ������� �� ����� ������ ������ ����� ��� ��� ������� ."
    PRINT;PRINT
    PRINT SPACE(2):"����� ��� ������� ���� � �� ���� �� �� ���� ���� ��� �� ��� �� �����"
    PRINT
    PRINT SPACE(2):"�� ���������."
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":XX
    PRINT SPACE(5):"LG.AMAMTDATE.1231.BENF"
*Line [ 308 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==========================================================
