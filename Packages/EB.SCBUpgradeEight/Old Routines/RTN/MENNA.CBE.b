* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    PROGRAM MENNA.CBE


*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.GOVERNORATE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.GOVERNORATE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.ID.TYPE


    GOSUB INITIAL
    GOSUB PROCESS
    RETURN

INITIAL:
    FN.SL = "&SAVEDLISTS&" ; F.SL = ''
    CALL OPF(FN.SL,F.SL)


    DIR.NAME = '&SAVEDLISTS&'
    NEW.FILE = "JANUARY.LOAD.XML"
    OPENSEQ DIR.NAME,NEW.FILE TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END

**************************************************************

    SEL.CMD    = "" ; KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""   ; XX= ""
    CALL TXTINP('Enter Starting Date', 22, 23, '8', 'ANY')
    START.DATE1 = COMI
    CALL TXTINP('Enter Ending Date', 22, 23, '8', 'ANY')
    END.DATE1 = COMI
    COMI = ''

****************************************************************

    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ""

    FN.CUSH = "FBNK.CUSTOMER$HIS"
    F.CUSH  = ""

    FN.DATE = "F.DATES"
    F.DATE  = ""


    CALL OPF (FN.CUS,F.CUS)
    CALL OPF (FN.CUSH,F.CUSH)
    CALL OPF (FN.DATE,F.DATE)
*****************************************************************
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)

    WS.LAST.DAT = TODAY
    WS.SYS.DATE = TODAY
    START.DATE = START.DATE1[3,6]:'0000'
    END.DATE   = END.DATE1[3,6]:'2359'

    WS.COMP.DAT = WS.LAST.DAT[3,6]
    NSN.ARR = ''
    RETURN
******************************************************************
PROCESS:

    SEL.CMD = "SELECT FBNK.CUSTOMER WITH DATE.TIME GE ":START.DATE:" AND DATE.TIME LE ":END.DATE
    SEL.CMD := " AND NEW.SECTOR EQ 4650 AND (SECTOR NE 5010 OR SECTOR NE 5020) "
    SEL.CMD := " AND (POSTING.RESTRICT LT 70 OR POSTING.RESTRICT EQ '' ) AND NATIONALITY EQ EG "
    SEL.CMD := " AND NSN.NO NE '' AND NSN.NO NE '00000000000000' "
    SEL.CMD := " BY @ID "
*DEBUG
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",SELECTED,RET.CODE)

****************************************************************
    FOR JJ = 1 TO SELECTED
        CALL F.READ(FN.CUS,SEL.LIST<JJ>,R.CUS,F.CUS,ERR.CUS)
        NAT.ID = R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
*Line [ 135 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        FIND NAT.ID IN NSN.ARR SETTING C.@FM ELSE
            IF LEN(NAT.ID) = 14 THEN
                XX = XX + 1
                NSN.ARR<-1> = NAT.ID
            END
        END
    NEXT JJ
    NSN.ARR = ''

****************************************************************
    IF SELECTED THEN
        DIM HEADER(9)
        HEADER(1) = "<?xml version=":'"':"1.0":'"':" ":"encoding=":'"':"utf-8":'"':" ?>"
        HEADER(2) = "<!-- version: 1.0.1806.10 -->"

        HEADER(3) = '<document>'
        HEADER(4) = '<header>'
        HEADER(5) = '<bankCode>':'1700':'</bankCode>'
        HEADER(6) = '<month>':END.DATE[1,6]:'</month>'
        HEADER(7) = '<noOfCustomers>':XX:'</noOfCustomers>'
        HEADER(8) = '</header>'
        HEADER(9) = '<customers>'
        FOR A = 1 TO 9
            WRITESEQ HEADER(A) TO BB ELSE
                PRINT  'CAN NOT WRITE LINE ':HEADER(A)
            END
        NEXT A
************************************************************
        FOR I = 1 TO SELECTED
            WS.CUS.ID = SEL.LIST<I>
            CALL F.READ(FN.CUS,WS.CUS.ID,R.CUS,F.CUS,MSG.CUS)
            CHANGE '&' TO 'And' IN R.CUS
            WS.DATE.TIME.A = R.CUS<EB.CUS.DATE.TIME,1>
            WS.CUS.DATE    = WS.DATE.TIME.A[1,6]
            WS.POSTING     = R.CUS<EB.CUS.POSTING.RESTRICT>
            WS.GOV = R.CUS<EB.CUS.LOCAL.REF><CULR.GOVERNORATE>

            WS.CO.BOOK   = R.CUS<EB.CUS.COMPANY.BOOK>
            WS.DEPT.CODE = R.CUS<EB.CUS.DEPT.CODE>
            WS.CURR.NO   = R.CUS<EB.CUS.CURR.NO>
            CUS.NAME.A  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,1>
            CUS.NAME.E  = R.CUS<EB.CUS.SHORT.NAME>
            B.DATE = R.CUS<EB.CUS.BIRTH.INCORP.DATE>
            NAT = R.CUS<EB.CUS.NATIONALITY>
            CALL DBR ('COUNTRY':@FM:EB.COU.CENTRAL.BANK.CODE,NAT,NAT.LETTER)
            NAT.ID = R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            SEC.ID = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
***********************************************************************
            SEC.TYPE = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ID.TYPE>
            BEGIN CASE
            CASE SEC.TYPE = '3'
                SEC.TYPE.ID = '1'
            CASE SEC.TYPE = '6'
                SEC.TYPE.ID = '2'
            CASE SEC.TYPE = ''
                SEC.TYPE.ID = ''
            CASE OTHERWISE
                SEC.TYPE.ID = '3'
            END CASE
*********************************************************************
            GEND = R.CUS<EB.CUS.LOCAL.REF><1,CULR.GENDER>
            IF GEND = 'F-����' THEN
                GEND.CODE= '0'
            END ELSE
                GEND.CODE= '1'
            END

*********************************************************************
            GOV = R.CUS<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
            IF GOV EQ '98' THEN
                COMP = R.CUS<EB.CUS.COMPANY.BOOK>
                CALL DBR ('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP,GOVER)
                CALL DBR ('SCB.CBE.GOVERNORATE':@FM:CBE.CBE.CODE,GOVER,RES.GOV)
            END ELSE
                CALL DBR ('SCB.CBE.GOVERNORATE':@FM:CBE.CBE.CODE,GOV,RES.GOV)
            END
**********************************************************************
            GOV1 = NAT.ID[8,2]
            BIR.GOV = TRIM(GOV1, "0" , "L")
**********************************************************************
*Line [ 216 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            FIND NAT.ID IN NSN.ARR SETTING C.@FM ELSE
                IF LEN(NAT.ID) = 14 THEN
                    DIM RECORD(12)
                    NSN.ARR<-1> = NAT.ID
                    SEC.TYPE.ID = ''
                    RECORD(1)   = '<customer>'
                    RECORD(2)   = '<nationalId>':NAT.ID:'</nationalId>'
                    RECORD(3)   = '<secondaryId>':SEC.ID:'</secondaryId>'
                    RECORD(4)   = '<secondaryIdType>':SEC.TYPE.ID:'</secondaryIdType>'
                    RECORD(5)   = '<arabicName>':CUS.NAME.A:'</arabicName>'
                    RECORD(6)   = '<englishName>':CUS.NAME.E:'</englishName>'
                    RECORD(7)   = '<birthDate>':B.DATE:'</birthDate>'
                    RECORD(8)   = '<birthGovCode>':BIR.GOV:'</birthGovCode>'
                    RECORD(9)   = '<gender>':GEND.CODE:'</gender>'
                    RECORD(10)   = '<residenceGovCode>':RES.GOV:'</residenceGovCode>'
                    RECORD(11)  = '<nationality>':NAT.LETTER:'</nationality>'
                    RECORD(12)  = '</customer>'

                    FOR B = 1 TO 12
                        WRITESEQ RECORD(B) TO BB ELSE
                            PRINT  'CAN NOT WRITE LINE ':RECORD(B)
                        END
                    NEXT B
                END
            END
**************************************************************************
            WS.CURR.NO = WS.CURR.NO - 1
            WS.HIS.KEY = WS.CUS.ID:";":WS.CURR.NO
            MSG.CUSH =  ""
            CALL F.READ(FN.CUSH,WS.HIS.KEY,R.CUSH,F.CUSH,MSG.CUSH)
            WS.HIS.DATE.A  = R.CUSH<EB.CUS.DATE.TIME,1>
            WS.POSTING.HIS     = R.CUS<EB.CUS.POSTING.RESTRICT>
            IF R.CUS AND R.CUSH THEN
                WS.CURR.NO -= 1
                WS.HIS.DATE.A = R.CUSH<EB.CUS.DATE.TIME,1>
                WS.POSTING.HIS  = R.CUSH<EB.CUS.POSTING.RESTRICT>
                CUS.NAME.A.HIS  = R.CUSH<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,1>
                CUS.NAME.E.HIS  = R.CUSH<EB.CUS.SHORT.NAME>
                B.DATE.HIS = R.CUSH<EB.CUS.BIRTH.INCORP.DATE>
                NAT.HIS = R.CUSH<EB.CUS.NATIONALITY>
                NAT.ID.HIS = R.CUSH<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
                SEC.ID.HIS = R.CUSH<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
                SEC.TYPE.HIS = R.CUSH<EB.CUS.LOCAL.REF><1,CULR.ID.TYPE>
                GEND.HIS = R.CUSH<EB.CUS.LOCAL.REF><1,CULR.GENDER>
                GOV.HIS = R.CUSH<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>

            END


        NEXT I
        DIM NN(2)
        NN(1) = '</customers>'
        NN(2) = '</document>'
        FOR FF = 1 TO 2
            WRITESEQ NN(FF) TO BB ELSE
                PRINT  'CAN NOT WRITE LINE ':NN(FF)
            END
        NEXT FF

    END
    RETURN
****************************************************************
PRINT.FILE:
    BB.DATA    = ""
    BB.DATA    = WS.CUS.ID:","
    BB.DATA   := WS.HIS.KEY:","
    BB.DATA   := WS.POSTING:","
    BB.DATA   := WS.POSTING.HIS:","
    BB.DATA   := WS.DATE.TIME.A:","
    BB.DATA   := WS.HIS.DATE.A:","
    BB.DATA   := CUS.NAME.A:","
    BB.DATA   := CUS.NAME.A.HIS:","
    BB.DATA   := CUS.NAME.E:","
    BB.DATA   := CUS.NAME.E.HIS:","
    BB.DATA   := B.DATE:","
    BB.DATA   := B.DATE.HIS:","
    BB.DATA   := NAT:","
    BB.DATA   := NAT.HIS:","
    BB.DATA   := NAT.ID:","
    BB.DATA   := NAT.ID.HIS:","
    BB.DATA   := GOV:","
    BB.DATA   := GOV.HIS

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
******************************************************************
*    EXIT
