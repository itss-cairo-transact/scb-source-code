* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*------INGY-------*
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
 SUBROUTINE LD.TIMEDATE(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE T24.BP I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
  ARG = ID.NEW
     CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.DATE.TIME,ARG,TTIME)
*Line [ 30 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
     DT = DCOUNT(TTIME,@VM)
     TT = TTIME<1,DT>
     TIM = TT[7,4]
     TIMHS = FMT(TIM,"R##:##")
  *   HH = TIMHS[4,2]
 *    SS = TIMHS[1,2]
*     TIMU = HH:":":SS
     ARG = TIMHS
 RETURN
 END
