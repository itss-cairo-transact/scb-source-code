* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.AMAMTDATE.1233.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    BRN.NAME = ""
    BRN.AREA = ""

    IF ID.NEW THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        IF MYCODE='1233' THEN
*Line [ 52 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            FOR II = 1 TO 2
                GOSUB INITIATE
                GOSUB PRINT.HEAD
                GOSUB BODY
                CALL PRINTER.OFF
                CALL PRINTER.CLOSE(REPORT.ID,0,'')
            NEXT II
        END
    END ELSE
        IF ID.NEW EQ '' THEN
*Line [ 64 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            IF MYCODE='1233' THEN
                FOR II = 1 TO 2
                    GOSUB INITIATE
                    GOSUB PRINT.HEAD
                    GOSUB BODY
                    CALL PRINTER.OFF
                    CALL PRINTER.CLOSE(REPORT.ID,0,'')
                NEXT II
            END ELSE
                MYID=MYCODE:'.':MYTYPE
                OPER.CODE=MYID
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.1233.1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN CALL TXTINP(YTEXT, 8, 22, "12", "A")

    IF ID.NEW THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>

    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>

    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
*   DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
*************************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    INPUTTER    = R.LD<LD.INPUTTER>
    INP         = FIELD(INPUTTER,'_',2)
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
***************************
    CUSLD    = R.LD<LD.CUSTOMER.ID>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSLD,LOC.REF)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF.THIRD)
    IF THIRD.NO EQ CUSLD THEN
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME.2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    END ELSE
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSLD,LOC.REF.THIRD)
        THIRD.NAME1 = LOC.REF.THIRD<1,CULR.ARABIC.NAME>
        THIRD.NAME.2 = LOC.REF.THIRD<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF.THIRD<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF.THIRD<1,CULR.ARABIC.ADDRESS,2>
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
        LG.CUST1 = LOC.REF<1,CULR.ARABIC.NAME>
        LG.CUST2 = LOC.REF<1,CULR.ARABIC.NAME.2>
    END
*****************************
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT = R.LD<LD.AMOUNT>
    LG.INC.AMT =ABS( R.LD<LD.AMOUNT.INCREASE>)
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CUR,CRR.DESC)

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    IF ID.NEW THEN
        OLD.AMOUNT=R.OLD(LD.AMOUNT)
    END ELSE
        OLD.AMOUNT=''
    END
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT = R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT = R.NEW(LD.AMOUNT)
        END
    END
    IN.AMOUNT = NEW.AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
**********************************************************************************
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT =  NEW.AMOUNT + LG.INC.AMT
        TEXT = OLD.AMOUNT :'-OLD.AMOUNT':'R.LD<LD.AMOUNT.INCREASE>';CALL REM
        AMT.DEC = LG.INC.AMT
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT= R.OLD(LD.AMOUNT)
            AMT.DEC =ABS(R.LD<LD.AMOUNT.INCREASE>)
        END
    END

******************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF=ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF= COMI
    END
    LG.CO= R.LD<LD.CO.CODE>
**********************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
    OPER.CODE=''

*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD ="'L'":SPACE(1):"LG.AMAMTDATE.1233.1"
    PR.HD :="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):"�������":":":XX
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT " ������� : ":BENF1:SPACE(LNE1)
    PRINT "         : ":BENF2:SPACE(LNE2)
    PRINT "|":SPACE(49):"|"
    PRINT " ������� : ":ADDR1:SPACE(LNED1)
    PRINT "         : ":ADDR2:SPACE(LNED2)
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME1:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ��� � �  : ":LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "�������������   : ":" **":OLD.AMOUNT:"**":" ":CRR
    PRINT
* PRINT SPACE(20): "���� ������� ��� :" FIN.DATE
* PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME1
*   END ELSE
    PRINT SPACE(20): "������� � ����� : ":THIRD.NAME1
    IF THIRD.NAME.2 THEN
        PRINT SPACE(5):THIRD.NAME.2
    END ELSE
        PRINT SPACE(5):""
    END
*  END
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(8):"�������� ��� ������ ��� / " : CORR.NO:"������":" ":  CORR.DATE :" " :"����� ��� ����"
    PRINT
    PRINT SPACE(2):"������ ��� ���� ���� ������ ������ ������ �����" : "*":LG.INC.AMT:"*" :CRR.DESC
    PRINT
    PRINT SPACE(2):"����� ���� ������� �����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT;PRINT
    PRINT SPACE(2):"���� ��� ��� ������������� ��������� �� ����� ����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT
*Line [ 285 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 287 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR.DESC
            PRINT ;PRINT
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
    PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
    PRINT;PRINT
    PRINT SPACE(8):"����� ����� ��� ���� ������ ������� �� ���� ������ ��� �����"
    PRINT
    PRINT SPACE(2):"��� ������ �����  ��� ������� ."
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT ;PRINT
    IF II = '1' THEN
        PRINT SPACE(5): "���� ��� : ":THIRD.NAME1
        IF THIRD.NAME.2 THEN
            PRINT SPACE(5):THIRD.NAME.2
        END ELSE
            PRINT SPACE(5):""
        END
        PRINT SPACE(15):THIRD.ADDR1
        PRINT SPACE(15):"����� ������ ��������"
    END ELSE
        PRINT;PRINT;PRINT;PRINT
    END
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
*    PRINT SPACE(5):"LG.AMAMTDATE.1233.1"
*Line [ 318 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

    RETURN
*==============================================================
