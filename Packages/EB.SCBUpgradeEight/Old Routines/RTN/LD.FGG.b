* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-------------------------------NI7OOOOOOOOOOOOO----------------------------------------------
    SUBROUTINE LD.FGG(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.OUR.BANK

    FN.CUS='FBNK.LD.LOANS.AND.DEPOSITS'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
    TDD  = TODAY
    TD   = TODAY
    CALL CDT('', TD, '+30C')
    TEXT = TD ; CALL REM
    TEXT = TDD; CALL REM
   T.SEL="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21097 AND LG.KIND LIKE ...FG... AND AMOUNT NE 0 AND STATUS NE 'LIQ' AND ACTUAL.EXP.DATE GE " : TDD : " AND ACTUAL.EXP.DATE LE " : TD
    ****  T.SEL="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21097 AND LG.KIND LIKE ...FG... AND AMOUNT NE 0 AND STATUS NE 'LIQ' AND ACTUAL.EXP.DATE GE " : TD
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
* CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
*TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
