* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>587</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE INP.SEND.TSK.MAIL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*    $INCLUDE           I_F.SEND.MAIL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROJECT.IMP.STATUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.IMP.CODES

    OPENSEQ "MAIL" , "MAIL.TEST" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE MAIL MAIL.TEST'
        HUSH OFF
    END

    OPENSEQ "MAIL" , "MAIL.TEST" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE MAI.TEST CREATED IN MAIL'
        END
        ELSE
            STOP 'Cannot create MAIL.TEST File IN MAIL'
        END
    END
************************
    TOO = ""; TO.PERSON = ""; XXXX = ""
    TO.PERSON = R.NEW(WN.RESPONSIBILITY)<1,1>
    PRI       = R.NEW(WN.PRIORITY)
    CALL DBR('USER': @FM:EB.USE.LOCAL.REF,TO.PERSON,XXXX)
    CALL DBR('SCB.IMP.CODES': @FM:WN.DESCRIPTIO,PRI,PRIORITY)
    TOO = XXXX<1,2>

    TSK.ID    = R.NEW(WN.TASK.ID)<1,1>
    T.ST.DATE = R.NEW(WN.START.DATE)<1,1>
    T.DU.DATE = R.NEW( WN.TARGET.DATE)<1,1>
    T.sum     = R.NEW(WN.REQUIRED.TASK)<1,1>

    SUBJ = "You Got A New Task"
    X1   = " == You have got a new task task.id = ":TSK.ID:" with ":PRIORITY:" priority"
    X2   = " == Task start date is ":T.ST.DATE
    X3   = " == Task due date is ":T.DU.DATE
    X4   = " == Task Required :" :T.sum
    X5   = " == Best Regards,"
    X6   = " == Scb Development Team."

    BB.DATA = ''

    BB.DATA  = TOO :'|'
    BB.DATA := SUBJ:'|'
    BB.DATA := X1
    BB.DATA := X2
    BB.DATA := X3
    BB.DATA := X4
    BB.DATA := X5
    BB.DATA := X6
    TEXT = "AN EMAIL WILL BE SENT TO ":TOO ; CALL REM
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**********************

    RETURN
END
