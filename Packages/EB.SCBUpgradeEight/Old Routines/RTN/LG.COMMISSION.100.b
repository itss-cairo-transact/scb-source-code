* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.COMMISSION.100

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE

    TEXT='TEST ';CALL REM

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  EQ 'NO-����� �����' THEN
        BEGIN.DATE = R.NEW(LD.VALUE.DATE)
        END.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        DIFF = "C"
        CALL CDD("",BEGIN.DATE,END.DATE,DIFF)
        SAM1 = DIFF / 90
        SAM2 = SAM1 + 0.9
        PERIOD.NO = FIELD(SAM2,".",1)

        CHRG.AMT=DEAL.AMOUNT*.6
R.NEW(LD.CHRG.AMOUNT)=CHRG.AMT
        TEXT=PERIOD.NO:'P';CALL REM
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> NE  100 THEN
            R.NEW(LD.CHRG.AMOUNT)<1,1> = PERIOD.NO * R.NEW(LD.CHRG.AMOUNT)<1,1>
            R.NEW(LD.CHRG.AMOUNT)<1,2> =  1*R.NEW(LD.CHRG.AMOUNT)<1,2>
*R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> =
        END ELSE
            R.NEW(LD.CHRG.AMOUNT)<1,1> =  R.NEW(LD.CHRG.AMOUNT)<1,1>
            R.NEW(LD.CHRG.AMOUNT)<1,2> =  R.NEW(LD.CHRG.AMOUNT)<1,2>
        END
        TEXT="Total Commission Computed Is :":R.NEW(LD.CHRG.AMOUNT)<1,1>:"AMT";CALL REM
        TEXT="Total Post Fees  Computed IS :":R.NEW(LD.CHRG.AMOUNT)<1,2>:"AMT1";CALL REM
    END
    RETURN
END
