* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
***NESSREEN AHMED 20/06/2019*********

SUBROUTINE MAST.DAILY.PAYMENT.H.B3

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BRSIN.FLAG

* A ROUTINE TO CREATE A TEXT FILE CONTANING ALL THE CASH PAYMENT THAT HAPPENED THROUGH THE DAY

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "scbpymh1-B3"
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
**********************FILLING DATA********************************
******************************************************************
    IDDD = "EG0010001"
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,IDDD,LAST.W.DAY)

    TDATEE = TODAY
    RQTIME = '1500'
    SDAT = TDATEE[3,6]:RQTIME
    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE EQ 38 AND DATE.TIME LE ": SDAT
*    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE EQ 38 "
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TOTAMT = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CARD.NO = '' ; CURR = '' ; AMTT = '' ; PAY.DATE = ''
            FN.TELLER = 'F.TELLER' ; F.TELELR = '' ; R.TELLER = '' ; RETRY1 = '' ; E1 = ''
            KEY.TO.USE = KEY.LIST<I>
            CALL OPF(FN.TELLER,F.TELLER)
            CALL F.READ(FN.TELLER,  KEY.TO.USE, R.TELLER, F.TELLER, E1)
            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,TTLR.VISA.NUMBER>
            CURR = R.TELLER<TT.TE.CURRENCY.1>
            AMTT = R.TELLER<TT.TE.NET.AMOUNT>

            TOTAMT = TOTAMT + AMTT
            PAY.DATE = TODAY
            TRANS.DATE = TODAY
**         TYY = TODAY[3,2]
**         TMM  = TODAY[5,2]
**         TDD = TODAY[7,2]
        NEXT I
    END
***************FT SELECTION********************************************************************
**  N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVM' "
    N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVM' AND DATE.TIME LE ": SDAT
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    TOT.AMTT.FT = '' ; TOT.CR.AMT = '' ; FMT.TOT.CR.AMT = ''
    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    IF SELECTED.N THEN
        FOR X=1 TO SELECTED.N
            AMTT.FT = ''
            FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
            FT.ID = KEY.LIST.N<X>
            CALL OPF(FN.FT,F.FT)
            CALL F.READ(FN.FT, FT.ID, R.FT, F.FT, E2)
            LOCAL.REF.FT = R.FT<FT.LOCAL.REF>
            VISA.NO.FT = LOCAL.REF.FT<1,FTLR.VISA.NO>
            CURR.FT = R.FT<FT.CREDIT.CURRENCY>
            AMTT.FT = R.FT<FT.DEBIT.AMOUNT>
            TOT.AMTT.FT = TOT.AMTT.FT + AMTT.FT
        NEXT X
    END
*************************************************************************
    TOT.CR = SELECTED + SELECTED.N
    TOT.CR.AMT = TOTAMT + TOT.AMTT.FT
    FMT.TOT.CR.AMT =  TOT.CR.AMT*100
*************************************************************************
    SEND.DAT = TODAY
**N**CALL CDT('' ,SEND.DAT,+1W)
    TYY = SEND.DAT[3,2]
    TMM = SEND.DAT[5,2]
    TDD = SEND.DAT[7,2]

***************FILLING BRIDGE******************************************************************************

    VISA.DATA = 'SCB'
*****UPDATED ACCORDING TO COMPANY REQUIRMENTS TO ACCEPT TWO FILES ******
****IN 14/10/2008*****
    VISA.DATA := 'S01'
    VISA.DATA := 'E'
    VISA.DATA := 'SCBPYM1':STR(" ",3)

    VISA.DATA := STR(" ",1)
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",5)
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",5 - LEN(TOT.CR)):TOT.CR
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",15)
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",15 - LEN(FMT.TOT.CR.AMT)):FMT.TOT.CR.AMT
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",5)
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",5)
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",15)
    VISA.DATA := STR(" ",1)
    VISA.DATA := STR("0",15)
    VISA.DATA := STR(" ",1)
    VISA.DATA := '1':TYY:TMM:TDD

    DIM ZZ(1)
    ZZ(1) = VISA.DATA

    WRITESEQ ZZ(1) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZ(1)
    END
       TEXT = '�� �������� �� ��������' ; CALL REM
    RETURN

END
