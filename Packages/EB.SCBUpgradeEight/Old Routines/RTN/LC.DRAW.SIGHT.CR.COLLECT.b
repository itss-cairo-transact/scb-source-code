* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
******************************NI7OOOOOOOOOOO***********************
*-----------------------------------------------------------------------------
* <Rating>-76</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LC.DRAW.SIGHT.CR.COLLECT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='LC.DRAW.SIGHT.CR.COLLECT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
* BT.ID = ID.NEW

    BT.ID = COMI
    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    CREDIT.AC = R.DR<TF.DR.PAYMENT.ACCOUNT>

    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,CREDIT.AC,DRAWER.ID)

*UPDATED BY HYTHAM ---UPGRADING R15---2016-03-07---
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,CREDIT.AC,AC.COMP)
    BRANCH.ID  = AC.COMP[8,2]
******
*    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CREDIT.AC,BRANCH.ID)
    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS>

    CATEG.ID  = CREDIT.AC[11,4]
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,CREDIT.AC,CUR.ID)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

    AMOUNT    = R.DR<TF.DR.PAYMENT.AMOUNT>
    IN.AMOUNT = AMOUNT
    OUT.AMOUNT = ''
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    CALL DBR('DRAWINGS':@FM:TF.DR.LOCAL.REF,COMI,NEW.LOC)
*************
    VER.NAME = ''
    VER.NAME = R.DR<TF.DR.LOCAL.REF><1,DRLR.VERSION.NAME>
    IF VER.NAME EQ ',SCB.DP' THEN
        DAT =R.DR<TF.DR.MATURITY.REVIEW>
    END ELSE
        DAT  = R.DR<TF.DR.VALUE.DATE>
    END
*****************
    NOTES  = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES,1>
    NOTES1 = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES,2>
    NOTES2 = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES,3>
    NOTES3 = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES,4>
    TEXT   = "NOT : " : NOTES ; CALL REM
    REFERENCE= R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE>


    V.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    INPUTTER = R.DR<TF.DR.INPUTTER>
* AUTH = R.NEW(TF.DR.AUTHORISER)
    INP = FIELD(INPUTTER,'_',2)
* AUTHI =FIELD(AUTH,'_',2)
    AUTH     = R.DR<TF.DR.AUTHORISER>
    AUTHI    = FIELD(AUTH,'_',2)

* AUTH = R.USER<EB.USE.SIGN.ON.NAME>
* CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)

    XX   = SPACE(132)  ; XX3  = SPACE(132)   ; XX14  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132)  ; XX11  = SPACE(132)
    XX12  = SPACE(132)  ; XX13  = SPACE(132)
    XX1<1,1>[3,35]   = CUST.NAME

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = CREDIT.AC

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = V.DATE

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = COMI

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = INP

    XX8<1,1>[3,35]  = '������ ������� : '
    XX9<1,1>[3,35] = OUT.AMT

    XX10<1,1>[3,35]  = '������ :'
    XX11<1,1>[3,35]  = NOTES
    XX12<1,1>[3,35]  = NOTES1
    XX13<1,1>[3,35]  = NOTES2
    XX14<1,1>[3,35]  = NOTES3
*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":"������ ������� "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
