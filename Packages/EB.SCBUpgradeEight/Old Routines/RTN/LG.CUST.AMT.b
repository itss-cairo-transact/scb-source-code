* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>333</Rating>
*-----------------------------------------------------------------------------
******* WAEL *******

    SUBROUTINE LG.CUST.AMT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 50 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CUST.AMT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ ":21096:" AND AMOUNT GT 0 BY CUSTOMER.ID BY CURRENCY AND WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    CUST.TMP = 0
    CUR.TMP  = 0
    FOR I = 1 TO SELECTED

        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
        LOCAL.REF   = R.LD<LD.LOCAL.REF>
        LG.CUST<I>  = R.LD<LD.CUSTOMER.ID>
        LG.AMT<I>   = R.LD<LD.AMOUNT>
        LG.CUR<I>   = R.LD<LD.CURRENCY> ; J = R.LD<LD.CURRENCY>
        LG.D.CUR<I> = LOCAL.REF<1,LDLR.ACC.CUR> ; J1 = LOCAL.REF<1,LDLR.ACC.CUR>
        LG.MARG<I>  = LOCAL.REF<1,LDLR.MARGIN.AMT>
        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,LG.CUR<I>,J)
        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,LG.D.CUR<I>,J1)

        LG.TOT<J>    += LG.AMT<I>
        MARG.TOT<J1> += LG.MARG<I>

        AA = 0
        IF LG.CUST<I> # CUST.TMP OR CUST.TMP = 0 THEN AA = 1
        IF LG.CUST<I> # CUST.TMP OR LG.CUR<I> # CUR.TMP THEN
            IF CUST.TMP # 0 THEN
*******TEXT=CUST.TMP:"-":CUR.TMP:"-":TOT.LG:"-":COUNT1 ; CALL REM


*******************************************************************
******************************** NEW CUSTOMER *********************
*******************************************************************
                YY = SPACE(80)

                YY<1,1>[1,10] = CUST.TMP
                CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUST.TMP,CUS.NAME)
                CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,CUST.TMP:".":FMT(7010.01,"R2%10"),LIMIT.AVAIL)
                CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUST.TMP,MARG.PER)
                YY<1,1>[15,35] = CUS.NAME
                IF LIMIT.AVAIL # '' THEN
                    YY<1,1>[88,15] = "������ = " :LIMIT.AVAIL
                    YY<1,1>[110,15] = MARG.PER<1,1>:" % ":MARG.PER<1,2>:" % ":MARG.PER<1,3>:" % "
                END
                YY<1,3>[15,4] = COUNT1
                CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.TMP,CUR.NAME)
                YY<1,3>[25,10] = CUR.NAME
                YY<1,3>[45,15] = TOT.LG
                YY<1,3>[65,15] = TOT.MARG

                PRINT YY<1,1>
                PRINT YY<1,3>

                IF AA = 1 THEN
                    PRINT STR('-',121)
                END

                TOT.LG = 0 COUNT1 = 0 TOT.MARG = 0
                TOT.LG = LG.AMT<I> ; COUNT1 = 1
                TOT.MARG = LG.MARG<I>
*******TEXT=LG.AMT<I>:"-":I ; CALL REM
            END ELSE
******TEXT=LG.AMT<I>:"-":I ; CALL REM
                TOT.LG = TOT.LG + LG.AMT<I>
                TOT.MARG = TOT.MARG + LG.MARG<I>
                COUNT1 = COUNT1 + 1
            END
        END ELSE
******TEXT=LG.AMT<I>:"-":I ; CALL REM
            TOT.LG = TOT.LG + LG.AMT<I>
            TOT.MARG = TOT.MARG + LG.MARG<I>
            COUNT1 = COUNT1 + 1
        END
        CUST.TMP = LG.CUST<I>
        CUR.TMP  = LG.CUR<I>

    NEXT I
******TEXT=CUST.TMP:"-":CUR.TMP:"-":TOT.LG:"-":COUNT1 ; CALL REM

********************************************************************
******************************** SAME CUSTOMER *********************
********************************************************************

    NN = SPACE(80)
    NN<1,1>[1,10] = CUST.TMP
    CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUST.TMP,CUS.NAME)
    CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,CUST.TMP:".":FMT(7010.01,"R2%10"),LIMIT.AVAIL)
    CALL DBR('SCB.LG.CUS':@FM:SCB.LGCS.MARGIN.PERC,CUST.TMP,MARG.PER)
    NN<1,1>[15,35] = CUS.NAME
    IF LIMIT.AVAIL # '' THEN
        NN<1,1>[88,15] = "������ = " :LIMIT.AVAIL
        NN<1,1>[110,15] = MARG.PER<1,1>:" % ":MARG.PER<1,2>:" % ":MARG.PER<1,3>:" % "
    END
    NN<1,3>[15,4] = COUNT1
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.TMP,CUR.NAME)
    NN<1,3>[25,10] = CUR.NAME
    NN<1,3>[45,15] = TOT.LG
    NN<1,3>[65,15] = TOT.MARG

    IF AA = 1 THEN
        PRINT NN<1,1>
    END
    PRINT NN<1,3>

    IF AA = 1 THEN
        PRINT STR('-',121)
    END

    IF I = SELECTED THEN
        PRINT STR('=',121)
    END

*/////////////////////////////////////////////////////

    PRINT SPACE(50): "  ��������������������������������"
    PRINT SPACE(40): "_____________________________________________"
    PRINT " "
    PRINT "������" :SPACE(10):" ���� ������":SPACE(10):" ���� ������ ������"
    PRINT "=======":SPACE(10):"============":SPACE(10):"===================="
    FOR K = 10 TO 100
        IF LG.TOT<K> # '' THEN
            SS = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ ":K
            SS.LIST ="" ; SS.SELECTED="" ;  SS.ER.MSG=""
            CALL EB.READLIST(SS,SS.LIST,"",SS.SELECTED,SS.ER.MSG)
            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,SS.LIST,CUR.NM)
            PRINT CUR.NM:SPACE(10):LG.TOT<K>:SPACE(10):MARG.TOT<K>
        END

    NEXT K
*//////////////////////////////////////////////////////

    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ���� ���� ������ ������ "
    PR.HD :="'L'":SPACE(50):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):" ������" :SPACE(3):" ��� ��������" :SPACE(5):" ������" :SPACE(5):" ������ ��������" :SPACE(5):" ������ ���������"
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    RETURN
*==============================================================
END
