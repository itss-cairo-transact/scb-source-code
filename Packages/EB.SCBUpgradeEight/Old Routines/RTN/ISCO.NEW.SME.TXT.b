* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-682</Rating>
*-----------------------------------------------------------------------------
    PROGRAM ISCO.NEW.SME.TXT
*  SUBROUTINE ISCO.NEW.SME.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CF
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.EXT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*==================== create cf and cs text files ========

    OPENSEQ "&SAVEDLISTS&" , "SME.CF.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SME.CF.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SME.CF.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SME.CF.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create SME.CF.TXT File IN &SAVEDLISTS&'
        END
    END
*****
    OPENSEQ "&SAVEDLISTS&" , "SME.CS.TXT" TO CC THEN
        CLOSESEQ CC
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SME.CS.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SME.CS.TXT" TO CC ELSE
        CREATE CC THEN
            PRINT 'FILE SME.CS.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create SME.CS.TXT File IN &SAVEDLISTS&'
        END
    END
    IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP
    IF IOCTL(CC, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP
*========================= DEFINE VARIABLES =========================
    EOF  = ''
    R.CF = ''
    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    MONTH.B4   = MONTH

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
        NEW.MONTH.B4 = 11
    END
    ELSE
        NEW.MONTH = MONTH - 1
        NEW.MONTH.B4 = MONTH.B4 - 2
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
    IF LEN(NEW.MONTH.B4) EQ 1 THEN
        NEW.MONTH.B4 = '0':NEW.MONTH.B4
    END
    LAST.MONTH.DATE = YEAR:NEW.MONTH.B4
    T.DATE = YEAR:NEW.MONTH
*============================ OPEN TABLES =========================
    FN.CF  = 'F.SCB.ISCO.SME.CF' ; F.CF = '' ; R.CF = ''
    CALL OPF(FN.CF,F.CF)
    FN.CS  = 'F.SCB.ISCO.SME.CS' ; F.CS = '' ; R.CS = ''
    CALL OPF(FN.CS,F.CS)
    FN.CUR= 'FBNK.CURRENCY'      ; F.CUR=''  ; R.CUR = '' ; E11=''
    CALL OPF(FN.CUR,F.CUR)
    FN.N.CUR= 'F.NUMERIC.CURRENCY'  ; F.N.CUR=''  ; R.N.CUR = '' ; E10=''
    CALL OPF(FN.N.CUR,F.N.CUR)
    FN.EXT = 'F.SCB.CUSTOMER.EXT'  ; F.EXT = ''  ; R.EXT = '' ; EXT =''
    CALL OPF(FN.EXT,F.EXT)
    FN.CU = 'FBNK.CUSTOMER'; F.CU = ''; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FLAG.SECURED = 'N'
    FLAG.404     = ''
    T.SEL  = "SELECT F.SCB.ISCO.SME.CS WITH"
    T.SEL := " ((@ID LIKE ...":YEAR:NEW.MONTH:")"
    T.SEL := " OR (@ID LIKE ...":YEAR:NEW.MONTH:"LD)"
    T.SEL := " OR (@ID LIKE ...":YEAR:NEW.MONTH:"LC)"
    T.SEL := " OR (@ID LIKE ...":YEAR:NEW.MONTH:"L)"
    T.SEL := " OR (@ID LIKE ...":YEAR:NEW.MONTH:".A)) BY CF.ACC.NO"

*    T.SEL = "SELECT F.SCB.ISCO.SME.CS WITH (@ID LIKE 11300616...201511... "
*    T.SEL := "OR @ID LIKE ...2300722...201511... "
*    T.SEL := "OR @ID LIKE ...4300270...201511... "
*    T.SEL := "OR @ID LIKE ...10300143...201511... "
*    T.SEL := "OR @ID EQ 1130033510122001) BY CF.ACC.NO"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*======================== BB.DATA ===========================

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CF,KEY.LIST<I>,R.CF,F.CF,ETEXT)
            CALL F.READ( FN.CS,KEY.LIST<I>,R.CS,F.CS,ETEXTS)

            ACCT.ID    = KEY.LIST<I>
            ACCT.CS.ID = KEY.LIST<I>
            CF.ACCT.NO = R.CF<ISCO.CF.CF.ACC.NO>
*********************6/12/2017*********
            SME.CATEG  = R.CS<ISCO.CS.SME.CATEGORY>
            TURNOVER   = R.CS<ISCO.CS.TURNOVER>
            CLOSE.DATE = R.CF<ISCO.CF.ACCT.CLOSING.DATE>
            ACCT.NO    = R.CF<ISCO.CF.CF.ACC.NO>
            ACCT.STAT  = R.CF<ISCO.CF.ACC.STATUS>
            CF.BAL     = R.CF<ISCO.CF.CF.ACCT.BALANCE>

            BEGIN CASE
            CASE SME.CATEG EQ '202'
                SME.CATEG = '205'
            CASE SME.CATEG EQ '203'
                SME.CATEG = '205'
            CASE SME.CATEG EQ '204' AND TURNOVER GT 50000000
                SME.CATEG = '206'
            CASE SME.CATEG EQ '204' AND TURNOVER LE 50000000
                SME.CATEG = '205'
            END CASE


*******UPDATED BY NOHA 31/7/2017*******
            CUS.ID = ACCT.ID[1,8]
            IF CUS.ID[1,1] EQ '0' THEN CUS.ID = CUS.ID[2,7]

            CALL F.READ( FN.CU,CUS.ID, R.CU, F.CU, ETEXT)
            CREDIT.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
            NEW.BR      = R.CU<EB.CUS.ACCOUNT.OFFICER>
            NEW.BR      = FMT(NEW.BR, "R%4")
            AMT_OVER    = R.CF<ISCO.CF.AMT.OVERDUE>

            ASSET_CLASS = R.CF<ISCO.CF.ASSET.CLASS>
            NDPD = R.CF<ISCO.CF.NDPD>
            IF CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120 OR CREDIT.CODE EQ 111 THEN
                ASSET_CLASS = 404
                NDPD = 999
                IF AMT_OVER EQ 0 THEN AMT_OVER = 1
                R.CF<ISCO.CF.AMT.OVERDUE>         = AMT_OVER
                CALL F.WRITE(FN.CF,ACCT.ID,R.CF)
                CALL JOURNAL.UPDATE(ACCT.ID)

            END

            GOSUB CREATE.CF

        NEXT I

    END
    CLOSESEQ BB
    CLOSESEQ CC
    RETURN

*===============CREATE CF TXT =====================
CREATE.CF:
    APPROVAL = R.CF<ISCO.CF.APPROVAL.DATE>

    IF APPROVAL EQ '20100229' THEN
        APP.DATE = '02282010'
    END
    ELSE
        APP.DATE = APPROVAL[5,2]:APPROVAL[7,2]:APPROVAL[1,4]
    END
    CAT.TYPE  = R.CF<ISCO.CF.CF.TYPE>

*CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUS.ID,NEW.BR)

    BB.DATA   = R.CF<ISCO.CF.SEG.IDENT>:'|'
    BB.DATA  := R.CF<ISCO.CF.DATA.PROV.ID>:'|'
    BB.DATA  := R.CF<ISCO.CF.PRE.DATA.PROV.ID>:'|'
*NEW.BR    = FMT(NEW.BR, "R%4")
    BB.DATA  := NEW.BR:'|'
    BB.DATA  := R.CF<ISCO.CF.PRE.DATA.PROV.BRAN.ID>:'|'
    BB.DATA  := R.CF<ISCO.CF.CF.ACC.NO>:'|'
    BB.DATA  := R.CF<ISCO.CF.CONSENT.STATUS>:'|'
    BB.DATA  := R.CF<ISCO.CF.CONSENT.EXPIRE.DATE>:'|'
    BB.DATA  := R.CF<ISCO.CF.PRE.CF.ACC.NO>:'|'
    BB.DATA  := APP.DATE:'|'

    IF (CAT.TYPE GE 1220 AND CAT.TYPE LT 1227) THEN
        BB.DATA  := '1':'|'
    END
    ELSE
        BB.DATA  := R.CF<ISCO.CF.APPROVAL.AMOUNT>:'|'
    END

*CLOSE.DATE = R.CF<ISCO.CF.ACCT.CLOSING.DATE>

    C.DATE     = CLOSE.DATE[5,2]:CLOSE.DATE[7,2]:CLOSE.DATE[1,4]

***************************
    ISLAMIC = R.CF<ISCO.CF.PROTEST.NOTIFY.DATE>
****************************

    BB.DATA  := R.CF<ISCO.CF.AMT.INSTALL>:'|'
    BB.DATA  := R.CF<ISCO.CF.CURRENCY>:'|'
    BB.DATA  := R.CF<ISCO.CF.CF.TYPE>:'|'
    BB.DATA  := R.CF<ISCO.CF.TERM.OF.LOAN>:'|'
    BB.DATA  := R.CF<ISCO.CF.REPAYMENT.TYPE>:'|'
    BB.DATA  := R.CF<ISCO.CF.FIRST.DISBURSE.DATE>:'|'
    BB.DATA  := CF.BAL:'|'
    BB.DATA  := R.CF<ISCO.CF.LAST.AMT.PAID>:'|'
    BB.DATA  := R.CF<ISCO.CF.AMT.OVERDUE>:'|'
*BB.DATA  := R.CF<ISCO.CF.NDPD>:'|'
    BB.DATA  := NDPD:'|'
    BB.DATA  := R.CF<ISCO.CF.NO.OF.PAYMENTS>:'|'
*BB.DATA  := R.CF<ISCO.CF.ASSET.CLASS>:'|'
    BB.DATA  := ASSET_CLASS:'|'
    BB.DATA  := ACCT.STAT:'|'
    BB.DATA  := R.CF<ISCO.CF.LAST.PAY.RECEIVE.DATE>:'|'
    BB.DATA  := R.CF<ISCO.CF.CF.AMENDMENT.DATE>:'|'
    BB.DATA  := R.CF<ISCO.CF.CF.SETTLEMENT.DATE>:'|'
    BB.DATA  := R.CF<ISCO.CF.PROTEST.NOTIFY>:'|'

**********TO REMOVE 10 FIELDS BECAUSE IT IS FULLY SECURED
    IF R.CF<ISCO.CF.PROTEST.NOTIFY> EQ '001' THEN
        FLAG.SECURED = 'Y'
    END
    ELSE
        FLAG.SECURED = 'N'
    END

  *  CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,CUS.LOCAL)
     CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID ,CUS.LOCAL)
    FLAG.404 = CUS.LOCAL<1,CULR.CREDIT.CODE>
***************************

*BB.DATA  := R.CF<ISCO.CF.PROTEST.NOTIFY.DATE>:'|'
    BB.DATA  := ISLAMIC:'|'
    BB.DATA  := R.CF<ISCO.CF.AMT.WRITTEN.OFF>:'|'
    BB.DATA  := R.CF<ISCO.CF.REASON.WRITTEN.OFF>:'|'
    BB.DATA  := R.CF<ISCO.CF.AMT.FORGIVEN>:'|'
    BB.DATA  := R.CF<ISCO.CF.REASON.AMT.FORGIVEN>:'|'
*BB.DATA  := R.CF<ISCO.CF.ACCT.CLOSING.DATE>:'|'
    BB.DATA  := C.DATE:'|'
    BB.DATA  := R.CF<ISCO.CF.CLOSURE.REASON>:'|'
    BB.DATA  := R.CF<ISCO.CF.COMMENT>:'|'
    BB.DATA  := R.CF<ISCO.CF.SUIT.FIELD.STATUS>:'|'
    BB.DATA  := R.CF<ISCO.CF.SUIT.REF.NO>:'|'
    BB.DATA  := R.CF<ISCO.CF.SUIT.AMT>:'|'
    BB.DATA  := R.CF<ISCO.CF.COURT>:'|'
*    BB.DATA  := R.CF<ISCO.CF.COURT.LOCATION>:'|'
     BB.DATA  := "001":'|'
    BB.DATA  := R.CF<ISCO.CF.COURT.DECREE>:'|'
    BB.DATA  := R.CF<ISCO.CF.SUIT.DATE>:'|'
    BB.DATA  := R.CF<ISCO.CF.DECREE.DATE>:'|'
    BB.DATA  := R.CF<ISCO.CF.LEGAL.ACTION.ORIGIN>:'|'
    BB.DATA  := R.CF<ISCO.CF.DISPUTE.NO>:'|'
    BB.DATA  := R.CF<ISCO.CF.DISPUTE.STATUS>:'|'
    BB.DATA  := R.CF<ISCO.CF.TRANS.TYPE.CODE>:'|'
    BB.DATA  := R.CF<ISCO.CF.NOTIFIED.ACCT>

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    R.CF<ISCO.CF.COMMENT> = 'Y'
    CALL F.WRITE(FN.CF,ACCT.ID,R.CF)
    CALL JOURNAL.UPDATE(ACCT.ID)



    GOSUB CREATE.CS
    RETURN

*============== CREATE CS TXT =================

CREATE.CS:

  *  CALL F.READ( FN.EXT,CUST.NO, R.EXT, F.EXT, EXT)
     CALL F.READ( FN.EXT,CUS.ID , R.EXT, F.EXT, EXT)
    ACT.CODE = R.EXT<CUS.EXT.CBE.ACTIVITY>

    CC.DATA   = R.CS<ISCO.CS.SEG.IDENT>:'|'
    CC.DATA  := R.CS<ISCO.CS.DATA.PROV.ID>:'|'
*CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.NO,NEW.BRN)
*NEW.BRN   = FMT(NEW.BRN, "R%4")
    CC.DATA  := NEW.BR:'|'
    CC.DATA  := R.CS<ISCO.CS.CF.ACC.NO>:'|'
    NEW.CUS   = R.CS<ISCO.CS.DATA.PROV.IDENT.CODE>
    NEW.CUS   = FMT(NEW.CUS, "R%16")
    CC.DATA  := NEW.CUS:'|'
    CC.DATA  := R.CS<ISCO.CS.PRE.DATA.PROV.CODE>:'|'
    CC.DATA  := R.CS<ISCO.CS.BUREAU.SBJ.CODE>:'|'
    NEW.CCR   = R.CS<ISCO.CS.IDENT.CODE.1>
    IF NEW.CCR NE '' THEN
        NEW.CCR = FMT(NEW.CCR, "R%12")
    END
    CC.DATA  := NEW.CCR:'|'
    CC.DATA  := R.CS<ISCO.CS.ISSUE.AUTHORITY.1>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.1.EXPIRE.DATE>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.CODE.2>:'|'
    CC.DATA  := R.CS<ISCO.CS.ISSUE.AUTHORITY.2>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.2.EXPIRE.DATE>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.CODE.3>:'|'
    CC.DATA  := R.CS<ISCO.CS.ISSUE.AUTHORITY.3>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.3.EXPIRE.DATE>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.CODE.4>:'|'
    CC.DATA  := R.CS<ISCO.CS.ISSUE.AUTHORITY.4>:'|'
    CC.DATA  := R.CS<ISCO.CS.IDENT.4.EXPIRE.DATE>:'|'
    CC.DATA  := R.CS<ISCO.CS.LANGUAGE.ID>:'|'
    CC.DATA  := R.CS<ISCO.CS.E.SUBJ.NAME>:'|'
    CC.DATA  := R.CS<ISCO.CS.PRE.E.SUBJ.NAME>:'|'
    CC.DATA  := R.CS<ISCO.CS.E.SHORT.NAME>:'|'
    CC.DATA  := R.CS<ISCO.CS.A.SUBJ.NAME>:'|'
    CC.DATA  := R.CS<ISCO.CS.PRE.A.SUBJ.NAME>:'|'
    CC.DATA  := R.CS<ISCO.CS.A.SHORT.NAME>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.TYPE.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.LINE.1.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.LINE.2.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.LINE.3.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.CITY.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.LINE.1.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.LINE.2.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.LINE.3.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.CITY.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.GOV.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.PO.BOX.NO>:'|'
    CC.DATA  := R.CS<ISCO.CS.ADD.1.ZIP.CODE>:'|'
    CC.DATA  := R.CS<ISCO.CS.COUNTRY>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.LINE.1.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.LINE.2.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.LINE.3.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.CITY.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.LINE.1.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.LINE.2.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.LINE.3.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.CITY.A>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.GOV.E>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.PO.BOX.NO>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.ADD.2.ZIP.CODE>:'|'
    CC.DATA  := R.CS<ISCO.CS.OFFICE.COUNTRY>:'|'
    CC.DATA  := R.CS<ISCO.CS.TELE.AREA.CODE>:'|'
    CC.DATA  := R.CS<ISCO.CS.PHONE>:'|'
    CC.DATA  := R.CS<ISCO.CS.FAX.NO>:'|'
    CC.DATA  := R.CS<ISCO.CS.URL>:'|'
    CC.DATA  := R.CS<ISCO.CS.BAC1>:'|'
*    CC.DATA  := ACT.CODE:'|'
    CC.DATA  := R.CS<ISCO.CS.BAC2>:'|'
    CC.DATA  := R.CS<ISCO.CS.BAC3>:'|'
    CC.DATA  := R.CS<ISCO.CS.LEGAL.CONDITION>:'|'
    CC.DATA  := R.CS<ISCO.CS.START.YEAR>:'|'

*******UPDATED BY NOHA HAMED
*CC.DATA  := R.CS<ISCO.CS.SME.CATEGORY>:'|'
    IF (CAT.TYPE GE 1220 AND CAT.TYPE LT 1227) OR (FLAG.SECURED EQ 'Y') OR (FLAG.404 EQ '110') THEN
        CC.DATA  := '':'|'
        CC.DATA  := '0':'|'
    END
    ELSE
*CC.DATA  := R.CS<ISCO.CS.SME.CATEGORY>:'|'
        CC.DATA  := SME.CATEG:'|'
        CC.DATA  := FIELD(R.CS<ISCO.CS.PAID.CAPITAL>,'.',1):'|'
    END

**----------

    IF (CAT.TYPE GE 1220 AND CAT.TYPE LT 1227) OR (FLAG.SECURED EQ 'Y') OR (FLAG.404 EQ '110') THEN
        CC.DATA  := '0':'|'
        CC.DATA  := '0':'|'
        CC.DATA  := '0':'|'
        CC.DATA  := '':'|'
        CC.DATA  := '':'|'
    END
    ELSE
        CC.DATA  := FIELD(R.CS<ISCO.CS.TURNOVER>,'.',1):'|'
        CC.DATA  := FIELD(R.CS<ISCO.CS.TOTAL.ASSETS>,'.',1):'|'
        CC.DATA  := R.CS<ISCO.CS.NO.OF.EMPLOYEES>:'|'
        CC.DATA  := R.CS<ISCO.CS.ESTABLISH.DATE>:'|'
        CC.DATA  := R.CS<ISCO.CS.BUSINESS.START.DATE>:'|'

    END
**---------
    IF (CAT.TYPE GE 1220 AND CAT.TYPE LT 1227) OR (FLAG.SECURED EQ 'Y') OR (FLAG.404 EQ '110') THEN
        CC.DATA  := '':'|'
        CC.DATA  := '':'|'
        CC.DATA  := ''
    END
    ELSE
        CC.DATA  := R.CS<ISCO.CS.FIN.STATEMENT.DATE>:'|'
        CC.DATA  := R.CS<ISCO.CS.MAX.TURNOVER.AMT.DATE>:'|'
        CC.DATA  := R.CS<ISCO.CS.BUSINESS.END.DATE>
    END
**************************

    WRITESEQ CC.DATA TO CC ELSE
        PRINT " ERROR WRITE FILE "
    END
    *R.CS<ISCO.CS.BAC3> = 'Y'
    *CALL F.WRITE(FN.CS,ACCT.CS.ID,R.CS)
    *CALL JOURNAL.UPDATE(ACCT.CS.ID)

    RETURN
*=======================

END
