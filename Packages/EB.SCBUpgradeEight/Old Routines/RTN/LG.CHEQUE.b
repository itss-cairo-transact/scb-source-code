* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
********* WAEL *****
*-----------------------------------------------------------------------------
* <Rating>-126</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.CHEQUE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*Line [ 47 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    IF MYCODE # '1401' AND MYCODE # '1402' AND MYCODE # '1403' AND MYCODE # '1407'  THEN
        E = "NOT.VALID.VERSION":" - " ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        GOSUB INITIATE
        GOSUB PRINT.HEAD
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "LETTER CREATED SUCCESFULLY" ; CALL REM
    END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CHEQUE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    PURPOSE = LOCAL.REF<1,LDLR.IN.RESPECT.OF>
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE,1>
    LG.MAT.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE,1>
    LG.APPROVAL.DATE=LOCAL.REF<1,LDLR.APPROVAL.DATE,1>
    LG.ACCT=LOCAL.REF<1,LDLR.DEBIT.ACCT,1>
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    DATZ= TODAY
    ZZ= DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ= LG.APPROVAL.DATE
    QQ = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]
    OP.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>

    CHK.NO = LOCAL.REF<1,LDLR.CHEQUE.NO>
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    REST.AMT = LG.AMT - CONFIS.AMT
    COR.D = LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORRS.DATE = COR.D[7,2]:'/':COR.D[5,2]:"/":COR.D[1,4]
    IN.AMOUNT=REST.AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,E1)
*--------------------------------------------------------------------------
    INPUTTER    = R.LD<LD.INPUTTER>
    INP         = FIELD(INPUTTER,'_',2)

    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF         = COMI
    END
    LG.CO= R.LD<LD.CO.CODE>
********************
    CUR.NOO  =  R.LD<LD.CURR.NO> - 1
    ID2 = REF : CUR.NOO
    D.TIME=LG.APPROVAL.DATE
    WWW = D.TIME[7,2]:'/':D.TIME[5,2]:"/":D.TIME[1,4]
*-----------------------------------

    RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LD.LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD ="'L'":SPACE(1):"����������":":":MYBRANCH
    PR.HD :="'L'":SPACE(1):STR('_',22)
    PR.HD :="'L'":SPACE(1):"��� ":ZZ
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:" )":TYPE.NAME:"("
    PRINT
    PRINT SPACE(17): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "����������� ����� : ":THIRD.NAME1
    IF THIRD.NAME2 THEN
        PRINT SPACE(17): "                  : ":THIRD.NAME2
    END ELSE
        PRINT
    END
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT

*=============
    PRINT SPACE(5):" ������ ������� ������ " : WWW : " � �������� ������� ������ " : CORRS.DATE

    PRINT SPACE(1):"����� ��� ���� ��� �� ��� ����� ������� ��� " :CHK.NO:" ����� ":CONFIS.AMT:" ":CRR
    IF CONFIS.AMT NE LG.AMT  THEN
        PRINT SPACE(1):" ������ ����� ����� �� ����� ������� �� ���� ���� ������ ������ ������"
    END ELSE
        IF CONFIS.AMT EQ LG.AMT  THEN
            PRINT SPACE(1):"������ ����� ����� ��   ���� ���� ������ ������ ������"
        END
    END

    IF OP.CODE EQ 1402 OR OP.CODE EQ 1401 THEN
        PRINT SPACE(1):"  ���� ����� ������ �� ������� ������ ��������� ���������� ������ �����."
        PRINT SPACE(10):" ������ ������� ��������� � ����� ��� ���� ������ � �������� ����� ������ �������� ."
        PRINT;PRINT;PRINT
    END ELSE
        IF OP.CODE EQ 1403 THEN
            PRINT SPACE(1):"���� ����� ����� ������ ���� ������ ����� ���� ������� �����" :REST.AMT :CRR
            PRINT SPACE(1):"� ��� ��� ��� �������� ����� ���� ������ ������� �� ����� ����" :REST.AMT :CRR
*Line [ 219 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 221 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
                    PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR
                END ELSE
                    PRINT SPACE(2): OUT.AMOUNT<1,I>
                END
            NEXT I
            PRINT SPACE(1):"� ��� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
            PRINT SPACE(10):"������ ������� ��������� � ����� ��� ���� ������ � �������� ����� ��� ������ ����� ��� ������� ."
        END
    END
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(5):" ���� ��� ��� ":CHK.NO
    PRINT SPACE(6):"  �����  " :CONFIS.AMT:" ":CRR
    PRINT SPACE(5):"LG.CHEQUE"
*Line [ 238 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    RETURN
*================================================================
END
