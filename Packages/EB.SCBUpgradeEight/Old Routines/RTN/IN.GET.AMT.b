* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE IN.GET.AMT(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_INF.LOCAL.REFS
*-----------------------------------------
    FN.IN = "F.INF.MULTI.TXN" ; F.IN = ""
    CALL OPF(FN.IN, F.IN)

    IN.ID = ARG
    CALL F.READ(FN.IN,IN.ID,R.IN,F.IN,ER.IN)
    FT.DB.CURR       = R.IN<INF.MLT.ACCOUNT.NUMBER>
    LANGUAGE.NUMBER  = R.IN<INF.MLT.LOCAL.REF><1,INLR.LANGUAGE>
*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    LOOP.NN          = DCOUNT(FT.DB.CURR,@VM)

    FOR II  = 1 TO LOOP.NN
        CHQ.NUM = R.IN<INF.MLT.CHEQUE.NUMBER><1,II>
        IF CHQ.NUM NE '' THEN
            AMT  = R.IN<INF.MLT.AMOUNT.LCY><1,II>
            AMT  = FMT(AMT,"L2")
            II   = LOOP.NN
        END
    NEXT II

    ARG = AMT
*------------------------------------------------------
    RETURN
END
