* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LC.CUS.IMP(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""

    LOCATE 'APPLICANT.CUSTNO' IN ENQ<2,1> SETTING RB.POS THEN
        APP = ENQ<4,RB.POS>
    END

    T.SEL  = "SELECT FBNK.LETTER.OF.CREDIT WITH LIABILITY.AMT GT 0"
    T.SEL := " AND (CATEGORY.CODE EQ 23100 OR CATEGORY.CODE EQ 23102 OR CATEGORY.CODE EQ 23152 OR CATEGORY.CODE EQ 23155 OR CATEGORY.CODE EQ 23150 OR CATEGORY.CODE EQ 23103 )"
    T.SEL := " AND APPLICANT.CUSTNO EQ ":APP:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    TEXT = 'SELECTED=':SELECTED ; CALL REM
***    IF KEY.LIST THEN
**       TEXT = "HHHH"  ; CALL REM
***       FOR I = 1 TO SELECTED
**          TEXT = "HHHH"  ; CALL REM
**          ENQ<2,I> = "@ID"
**          ENQ<3,I> = "EQ"
**          ENQ<4,I> = KEY.LIST<I>
**      NEXT I
**   END
**  IF NOT(KEY.LIST) THEN
**       ENQ.ERROR = "NO RECORDS"
**  END

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS"
*
    END
*

*******************************************************************************************
    RETURN
END
