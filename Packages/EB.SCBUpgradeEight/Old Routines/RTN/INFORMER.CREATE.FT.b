* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>2318</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE INFORMER.CREATE.FT(MAT R.FT,
    FORCE.POSTING,
    JRNL,
    VER.NAME,
    WRITE.IN.NAU,
    ID,
    ER.MSG,
    W.STATUS)

*---------------------------------------------------------------------------*
*                                                                           *
* Subroutine written by J.Mastorakis & V.Papadopoulos for INFORMER S.A.     *
*                                                                           *
*---------------------------------------------------------------------------*

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SPF
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BC.PARAMETER
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.LOCAL.CLEARING
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.COMMON
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLASS
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PM.PARAMETER
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.APPL.DEFAULT
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.GROUP.CONDITION
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AGENCY
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FTCOM

    SAVE.JOURNAL.BYPASS = JOURNAL.BYPASS
    JOURNAL.BYPASS = JRNL

    SAVE.APPLICATION = APPLICATION
    SAVE.FUNCTION = V$FUNCTION
    SAVE.ID.NEW = ID.NEW
    SAVE.V = V
    SAVE.A = A
    SAVE.AF = AF
    SAVE.AV = AV
    SAVE.AS = AS

    DIM R.SAVE(V) ; MAT R.SAVE = '' ; MAT R.SAVE = MAT R.NEW

    ER.MSG = '' ; W.STATUS = ''
    CALL INITIALIZE.FTCOM
    DIM R.ACCT(AC.AUDIT.DATE.TIME)
    DIM APP.REC(FT1.AUDIT.DATE.TIME)
    APPL.DEF.SIZE = FT1.AUDIT.DATE.TIME
    R.ACCT.SIZE = AC.AUDIT.DATE.TIME
    FT.REC.SIZE = FT.AUDIT.DATE.TIME
    MAT R.NEW = ''
    MAT R.ACCT = ''
    MAT APP.REC = ''
    V = FT.AUDIT.DATE.TIME
    IF NOT(V$FUNCTION) THEN
        V$FUNCTION = 'INFORMER.CREATE.FT'
    END

    GOSUB CHECK.IF.PM.ACTIVE

*************************************************************************
* Assign variables
*************************************************************************

    EQU FALSE TO 0 ; EQU TRUE TO 1
    AUTO.PROCESSING.IND = 'Y'
    RECORD.WRITTEN = FALSE
    ETEXT = ''
    NEXT.DUE.DATE = ''
    ERROR.FLAG = ''
    FIRST.FAIL.FLAG = ''

    YDATE = R.DATES(EB.DAT.LAST.WORKING.DAY)
    YDAYS.ORIG = "+1C"
    TODAY = R.DATES(EB.DAT.TODAY)
    Y = ""
    LOCKING.COUNT = ''
    PYMNT.NO = ''

*************************************************************************
SET.UP.CCY.MKT:
*************************************************************************

    APPLICATION.ID = 'FT'
    CALL.TYPE = 1
    MVMT.ID = ''
    FND.CCY.MKT = ''
    TXN.CCY.MKT = ''
    RETURN.CODE = ''
    ERROR.MESSAGE = ''
    CALL FIND.CCY.MKT(APPLICATION.ID,CALL.TYPE,MVMT.ID,FND.CCY.MKT,TXN.CCY.MKT,RETURN.CODE,ERROR.MESSAGE)
    IF RETURN.CODE > 0 THEN
        TEXT = ERROR.MESSAGE:' IN FILE F.MU.TXN.GRP.CCY.MKT'
        CALL FATAL.ERROR("INFORMER.CREATE.FT")
    END

*************************************************************************
* Open files
*************************************************************************

    F.CUSTOMER = ''
    CUST.FILE.NAME.TMP = 'F.CUSTOMER'
    CALL OPF(CUST.FILE.NAME.TMP,F.CUSTOMER)

    F.ACCOUNT = ''
    ACCT.FILE.NAME.TMP = 'F.ACCOUNT'
    CALL OPF(ACCT.FILE.NAME.TMP,F.ACCOUNT)

    F.FUNDS.TRANSFER = ''
    FT.FILE.NAME.TMP = 'F.FUNDS.TRANSFER'
    CALL OPF(FT.FILE.NAME.TMP,F.FUNDS.TRANSFER)

    F.FUNDS.TRANSFER.UNAU = ''
    FT.NAU.FILE.NAME.TMP = 'F.FUNDS.TRANSFER$NAU'
    CALL OPF(FT.NAU.FILE.NAME.TMP,F.FUNDS.TRANSFER.UNAU)

    F.FT.APPL.DEFAULT = ''
    APPL.DEF.FILE.NAME.TMP = 'F.FT.APPL.DEFAULT'
    CALL OPF(APPL.DEF.FILE.NAME.TMP,F.FT.APPL.DEFAULT)

    F.ACCT.ENT.TODAY = ''
    APPL.ACCT.ENT.TODAY = 'F.ACCT.ENT.TODAY'
    CALL OPF(APPL.ACCT.ENT.TODAY, F.ACCT.ENT.TODAY)

*
* Read ft.application.default.rec
*
    ETEXT = ""
    CALL F.MATREAD(APPL.DEF.FILE.NAME.TMP,ID.COMPANY,MAT APP.REC,APPL.DEF.SIZE,F.FT.APPL.DEFAULT,ETEXT)
    IF ETEXT THEN
        ETEXT = "NO APPLICATION DEFAULT FOR COMPANY"
        E = ETEXT ; GOTO FATAL.ERROR
    END

    FIRST.FLAG = 1
    ERROR.FLAG = ''
    GOSUB GET.STANDING.ORDER

*
* Return to calling program
*
    GOTO PROGRAM.END

*************************************************************************
GET.STANDING.ORDER:
*************************************************************************

    PYMNT.NO = ''
    ETEXT = ''
*
* Clear override and process error fields
*
    I.A = FT.STMT.NOS
    LOOP WHILE I.A <= FT.AUDIT.DATE.TIME
        R.FT(I.A) = ''
        I.A += 1
    REPEAT
*
* Process standing orders
*

    FIRST.FLAG = ''
    AUTO.PROCESSING.IND = 'Y'
    CURR.RECORD = ''
    DEBIT.ACCTS = ''
    DEBIT.AMTS = ''
    TOT.CHG = 0
    PYMNT.NO += 1
    GOSUB BUILD.FT.RECORD
    GOSUB WRITE.RECORD

    IF NOT(RUNNING.UNDER.BATCH) THEN
        IF NOT(JOURNAL.BYPASS) THEN
            CALL JOURNAL.UPDATE(ID)
**            RELEASE F.ACCOUNT
            CALL F.RELEASE(F.ACCOUNT,ID,F.ACCOUNT)

            CALL F.RELEASE(ACCT.FILE.NAME.TMP, '', F.ACCOUNT)
            CALL F.RELEASE(FT.NAU.FILE.NAME.TMP, ID, F.FUNDS.TRANSFER.UNAU)
            CALL F.RELEASE(FT.FILE.NAME.TMP, ID, F.FUNDS.TRANSFER)
            CALL F.RELEASE(APPL.ACCT.ENT.TODAY, '', F.ACCT.ENT.TODAY)
        END
    END

    RETURN

*************************************************************************
BUILD.FT.RECORD:
*************************************************************************

    STORE.CR.OVERRIDE = ''
    STORE.DR.OVERRIDE = ''
    STORE.CHG.OVERRIDE = ''
    MAT R.NEW = ''
    MAT R.NEW = MAT R.FT
    MAT R.ACCT = ''
*
* Get the next sequential funds transfer id
*
    APP.IND = 'SAV' ; ID = ''
    ETEXT = ''
    CALL GET.APPL.NEXT.ID('FBNK.FUNDS.TRANSFER', 'FT', ID, ETEXT)
    TEXT = 'ID CREATE = ': ID ; CALL REM
*     CALL FT.GENERATE.ID(APP.IND,ID)
    IF ETEXT NE '' THEN E = ETEXT ; GOTO FATAL.ERROR
    ID.NEW=ID

*
* Set ccy market to 1
*
    R.NEW(FT.CURRENCY.MKT.CR) = '1'
    R.NEW(FT.CURRENCY.MKT.DR) = '1'

    END.ERROR = "" ; ETEXT = "" ; TEXT = ""
    CHARGES.INPUT.IND = "" ; COMM.INPUT.IND = ""
    CURR.NO = 0 ; R.COMM.DEFAULT = "" ; R.CHARGE.DEFAULT = ""

    ER.VER = ''
    IF VER.NAME<1> THEN
        KOMMA = '' ; KOMMA = INDEX(VER.NAME<1>, ',', 1)

        TEMP.APPLICATION = APPLICATION
        APPLICATION = VER.NAME<1>[1, KOMMA - 1]

        TEMP.VERSION = PGM.VERSION
        PGM.VERSION = ',':VER.NAME<1>[KOMMA + 1 , LEN(VER.NAME<1>) - KOMMA]

        CALL CHECK.VERSION.VLD.RTNS(VER.NAME<1>, ER.VER)

        APPLICATION = TEMP.APPLICATION
        PGM.VERSION = TEMP.VERSION

        IF NOT(ER.VER) THEN
            BEGIN CASE
            CASE ETEXT
                ER.VER = ETEXT ; ETEXT = ''
            CASE TEXT
                ER.VER = TEXT ; TEXT = ''
            CASE END.ERROR
                ER.VER = END.ERROR ; END.ERROR = ''
            CASE R.NEW(FT.OVERRIDE)
                ER.VER = R.NEW(FT.OVERRIDE) ; R.NEW(FT.OVERRIDE) = ''
            CASE E
                ER.VER = E ; E = ''
            CASE OTHERWISE
*Line [ 295 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                NULL
            END CASE
        END
    END

    GOSUB COMPLETE.XVAL

    RETURN

*************************************************************************
COMPLETE.XVAL:
*************************************************************************
*
* Call Funds transfer cross-validation
*
    CALL FT.COMPLETE.XVALIDATION(CURR.NO,R.COMM.DEFAULT,R.CHARGE.DEFAULT)

    IF ER.VER THEN
        TEXT = ER.VER
        ER.VER = ''
    END

    IF RUNNING.UNDER.BATCH THEN
*Line [ 319 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        NULL
    END ELSE
        IF PYMNT.NO GT LOCKING.COUNT THEN
*Line [ 323 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
            RETURN
        END
    END

    ERROR.MESSAGE = ""
    EXCEP.LOG.DONE = 0

    IF TEXT <> "" THEN
        GOSUB UPDATE.OVERRIDE
        GOSUB UPDATE.OVERRIDE.EXCEPTION
        R.NEW(FT.RECORD.STATUS) = "IHLD" ; RETURN
    END
    IF END.ERROR <> "" OR ETEXT <> "" THEN
        GOSUB UPDATE.EXCEPTION.LOG
        EXCEP.LOG.DONE = 1
        R.NEW(FT.RECORD.STATUS) = "IHLD" ; RETURN
    END
*
* Update auto processed fields
*
    GOSUB PARA.FORMATTER:
*
    IF R.NEW(FT.CREDIT.COMP.CODE) = "" THEN R.NEW(FT.CREDIT.COMP.CODE) = ID.COMPANY
    IF R.NEW(FT.DEBIT.COMP.CODE) = "" THEN R.NEW(FT.DEBIT.COMP.CODE) = ID.COMPANY
    R.NEW(FT.DEPT.CODE) = R.USER<EB.USE.DEPARTMENT.CODE>
    R.NEW(FT.CO.CODE)= ID.COMPANY
    IF R.NEW(FT.POSITION.TYPE) = "" THEN
        R.NEW(FT.POSITION.TYPE) = "TR"
    END
*
    IF ETEXT <> "" THEN
        R.NEW(FT.RECORD.STATUS) = "IHLD" ; RETURN
    END

    IF ETEXT = "" AND END.ERROR = "" AND TEXT = "" THEN
        GOSUB UPDATE.ACCT.POS
    END ELSE
        RETURN
    END
*
    IF NOT(RUNNING.UNDER.BATCH) THEN    ;* Online we will need authorise
        CALL EB.ACCOUNTING("FT","AUT","","")
    END
*************************************************************************
*****                  D E L I V E R Y                               ****
*************************************************************************
    IF ERROR.FLAG = '' THEN
        IF FIRST.FLAG = '' THEN

            DELIVERY.OK=TRUE
********
            IF R.NEW(FT.RECORD.STATUS)="" THEN
********
                IF R.NEW(FT.STATUS) = "INORATE" THEN
                    IF R.NEW(FT.DELIVERY.MKR) = "D" THEN
                        CALL FT.DELIVERY
                        IF ETEXT THEN
                            DELIVERY.OK=FALSE
                        END ELSE
                            R.NEW(FT.DELIVERY.MKR)="S"
                        END
                    END
                    IF DELIVERY.OK THEN
                        R.NEW(FT.STATUS)="ANORATE"
                        CALL FT.BUILD.NORATES("INORATE","DELETE",ID.NEW)
                        CALL FT.BUILD.NORATES("ANORATE","INSERT",ID.NEW)
                    END
                END ELSE
                    CALL FT.DELIVERY
                    IF ETEXT THEN
                        DELIVERY.OK=FALSE
                    END ELSE
                        IF R.NEW(FT.STATUS) = "IFWD" THEN
                            R.NEW(FT.STATUS) = "AFWD"
                        END
                    END
                END
********
            END
********
            IF DELIVERY.OK = FALSE THEN
                R.NEW(FT.RECORD.STATUS) = "IHLD"
            END
*
        END
    END

    RETURN

*************************************************************************
UPDATE.OVERRIDE:
*************************************************************************

*Line [ 419 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    OVERRIDE.CNT = DCOUNT(R.NEW(FT.OVERRIDE), @VM)
*Line [ 421 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    YGEN.DATA = FIELD(TEXT,@FM,2)
    IF YGEN.DATA <> "" THEN
*Line [ 424 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        YGEN.TEXT = FIELD(TEXT,@FM,1)
        DA.CO = 1
        LOOP
            DATA.MARK.POS = INDEX(YGEN.TEXT,"&",1)
        UNTIL DATA.MARK.POS = 0
            YGEN.TEXT = YGEN.TEXT[1,DATA.MARK.POS-1]:YGEN.DATA<1,DA.CO>:YGEN.TEXT[DATA.MARK.POS+1,99]
            DA.CO += 1
        REPEAT
        TEXT = YGEN.TEXT
    END
*
* Above caters for any errors
*
    CONVERT FM TO VM IN TEXT  ;* In case more than 1 override
    IF OVERRIDE.CNT = 0 THEN
        R.NEW(FT.OVERRIDE)<1,1> = TEXT
    END ELSE
*Line [ 442 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        R.NEW(FT.OVERRIDE) = R.NEW(FT.OVERRIDE):@VM:TEXT
    END
    TEXT = ""

    RETURN

*************************************************************************
WRITE.RECORD:
*************************************************************************
*
* Call FT to set up params
*
    FIRST.FAIL.FLAG = ''
    R.NEW(FT.INPUTTER) = TNO:'_':OPERATOR
    TIME.STAMP = TIMEDATE()
    X = OCONV(DATE(),"D-")
    X = X[9,2]:X[1,2]:X[4,2]:TIME.STAMP[1,2]:TIME.STAMP[4,2]
    R.NEW(FT.DATE.TIME) = X

    RECORD.WRITTEN = FALSE
    IF ETEXT = "" AND END.ERROR = "" AND ERROR.FLAG = "" AND R.NEW(FT.RECORD.STATUS) = "" THEN
        R.NEW(FT.OVERRIDE) = ""
        R.NEW(FT.CURR.NO) = 1
        R.NEW(FT.AUTHORISER) = TNO:'_':OPERATOR
        GOSUB CALL.PM

        ER.RTN = ''
        GOSUB CALL.RTNS.IF.OK
        IF NOT(ER.RTN) THEN

            CALL F.MATWRITE(FT.FILE.NAME.TMP,ID,MAT R.NEW,FT.REC.SIZE)

            W.STATUS = 'L'

            IF NOT(RUNNING.UNDER.BATCH) THEN
                LOCKING.COUNT = PYMNT.NO * 1
                IF NOT(JOURNAL.BYPASS) THEN
                    CALL JOURNAL.UPDATE(ID)
**                    RELEASE F.ACCOUNT
CALL F.RELEASE(F.ACCOUNT,ID,F.ACCOUNT)
                    CALL F.RELEASE(ACCT.FILE.NAME.TMP, '', F.ACCOUNT)
                    CALL F.RELEASE(FT.NAU.FILE.NAME.TMP, ID, F.FUNDS.TRANSFER.UNAU)
                    CALL F.RELEASE(FT.FILE.NAME.TMP, ID, F.FUNDS.TRANSFER)
                    CALL F.RELEASE(APPL.ACCT.ENT.TODAY, '', F.ACCT.ENT.TODAY)
                END
            END

            RECORD.WRITTEN = TRUE
        END
        ELSE
            R.NEW(FT.OVERRIDE)<1, 1> = ER.RTN
        END
    END
    IF RECORD.WRITTEN = FALSE THEN
        IF WRITE.IN.NAU THEN

            W.STATUS = 'E'
            ER.MSG = R.NEW(FT.OVERRIDE)<1, 1>

            IF END.ERROR NE "" THEN
                R.NEW(FT.RECORD.STATUS) = "IHLD"
            END
            IF ETEXT NE "" THEN
                R.NEW(FT.RECORD.STATUS) = "IHLD"
            END
            IF R.NEW(FT.OVERRIDE) NE "" THEN
                IF FIRST.FLAG = 1 THEN
                    ERROR.FLAG = 1
                    FIRST.FAIL.FLAG = 1
                END ELSE
                    R.NEW(FT.RECORD.STATUS)= 'IHLD'
                END
            END
            IF FIRST.FLAG = '' THEN
                CALL F.MATWRITE(FT.NAU.FILE.NAME.TMP,ID,MAT R.NEW,FT.REC.SIZE)

*
                IF NOT(RUNNING.UNDER.BATCH) THEN
                    LOCKING.COUNT = PYMNT.NO * 1
                    IF NOT(JOURNAL.BYPASS) THEN
                        CALL JOURNAL.UPDATE(ID)
**                        RELEASE F.ACCOUNT
CALL F.RELEASE(F.ACCOUNT,ID,F.ACCOUNT)
                        CALL F.RELEASE(ACCT.FILE.NAME.TMP, '', F.ACCOUNT)
                        CALL F.RELEASE(FT.NAU.FILE.NAME.TMP, ID, F.FUNDS.TRANSFER.UNAU)
                        CALL F.RELEASE(FT.FILE.NAME.TMP, ID, F.FUNDS.TRANSFER)
                        CALL F.RELEASE(APPL.ACCT.ENT.TODAY, '', F.ACCT.ENT.TODAY)
                    END
                END
*
            END
*
* Set up an error message for exception log file
* Determine which of END.ERROR, ETEXT, TEXT, T.ETEXT is not null
*
*********************
UPDATE.EXCEPTION.LOG:
*********************
*
            ERROR.NOTE=""
            ERROR.MESSAGE=""
            IF ETEXT NE "" THEN
                ERROR.MESSAGE=ETEXT
            END ELSE
                IF TEXT NE "" THEN
                    ERROR.MESSAGE=TEXT
                END ELSE
                    IF T.ETEXT NE "" THEN
                        ERROR.MESSAGE=T.ETEXT
                    END ELSE
                        IF END.ERROR NE "" THEN
                            LOC.MSG=INDEX(END.ERROR,":",1)
                            IF LOC.MSG NE 0 THEN
                                ERROR.MESSAGE=END.ERROR[LOC.MSG+2,99]
                            END ELSE
                                ERROR.MESSAGE=END.ERROR
                            END
                        END
                    END
                END
            END
*
            IF ERROR.MESSAGE <> "" THEN
                TEXT = ERROR.MESSAGE
                GOSUB UPDATE.OVERRIDE
            END
            IF NOT(EXCEP.LOG.DONE) THEN
*
**************************
UPDATE.OVERRIDE.EXCEPTION:
**************************
*
                IF ERROR.MESSAGE="" THEN
                    IF R.NEW(FT.OVERRIDE) NE "" THEN
                        ERROR.NOTE="OVERRIDE CONDITION"
                        ERROR.MESSAGE=R.NEW(FT.OVERRIDE)
                    END
                END
*
                IF ERROR.MESSAGE <> "" THEN
                    YTEMP.ERROR.MESSAGE = ERROR.MESSAGE
*Line [ 584 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                    YCOUNT.@FM = COUNT(YTEMP.ERROR.MESSAGE,@FM)
*Line [ 586 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                    IF YCOUNT.@FM > 1 THEN
*Line [ 588 ] Add @VM Instead Of VM & Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                        CONVERT @FM TO @VM IN YTEMP.ERROR.MESSAGE
                    END
                    CALL EXCEPTION.LOG("U", "FT", "INFORMER.CREATE.FT",
                    "FUNDS.TRANSFER", "112", ERROR.NOTE,
                    FT.NAU.FILE.NAME.TMP, ID, '1',
                    YTEMP.ERROR.MESSAGE,
                    R.USER<EB.USE.DEPARTMENT.CODE>)
                END
            END
        END
        ELSE
            W.STATUS = 'N'
            ER.MSG = R.NEW(FT.OVERRIDE)<1, 1>
        END
    END
*
    RETURN

*************************************************************************
UPDATE.ACCT.POS:
*************************************************************************
*
* Call input accounting
*
    SAVE.STATUS = R.NEW(FT.STATUS)
    IF FORCE.POSTING THEN
        IF R.NEW(FT.STATUS)[2,6] # 'NORATE' THEN
            R.NEW(FT.STATUS) = 'INORATE'
        END
    END
    CALL FT.INPUT.ACCOUNTING (STORE.CR.OVERRIDE,STORE.DR.OVERRIDE,STORE.CHG.OVERRIDE)
    IF FORCE.POSTING THEN
        TEXT = ''
        ETEXT = ''
        END.ERROR = ''
    END
    R.NEW(FT.STATUS) = SAVE.STATUS
    IF TEXT NE "" THEN
        GOSUB UPDATE.OVERRIDE
        R.NEW(FT.RECORD.STATUS) = "IHLD"
        RETURN
    END
    IF ETEXT = "" AND END.ERROR = "" AND TEXT = "" THEN
*
* Update positions
*
        IF R.NEW(FT.CREDIT.CURRENCY) <> R.COMPANY(EB.COM.LOCAL.CURRENCY) OR R.NEW(FT.DEBIT.CURRENCY) <> R.COMPANY(EB.COM.LOCAL.CURRENCY) OR (R.NEW(FT.CHARGES.ACCT.NO) NE "" AND CHARGE.CURRENCY <> R.COMPANY(EB.COM.LOCAL.CURRENCY)) THEN
            POS.COMP.CODE = ""
            NARR = ""
*
* Call build norates
*
            IF R.NEW(FT.STATUS) = 'INORATE' OR R.NEW(FT.STATUS) = 'ANORATE' THEN
                FUNCTION.IND = 'INSERT' ; V$KEY = R.NEW(FT.STATUS)
                CALL FT.BUILD.NORATES(V$KEY,FUNCTION.IND,ID.NEW)
            END ELSE
                IF R.NEW(FT.STATUS)[2,3] = "FWD" THEN
                    FUNCTION.IND = 'INSERT' ; V$KEY = 'DATE'
                    CALL FT.BUILD.NORATES(V$KEY,FUNCTION.IND,ID.NEW)
                END
            END
        END
    END

    RETURN

*************************************************************************
PARA.FORMATTER:
*************************************************************************

    ETEXT = ''

*TOTAL DR
    IF R.NEW(FT.AMOUNT.DEBITED) <> "" THEN
        TMP.CCY = R.NEW(FT.AMOUNT.DEBITED)[1,3]
        TMP.AMT = R.NEW(FT.AMOUNT.DEBITED)[4,22]
        CALL SC.FORMAT.CCY.AMT(TMP.CCY,TMP.AMT)
        IF ETEXT = "" THEN
            R.NEW(FT.AMOUNT.DEBITED) = TMP.CCY:TMP.AMT
        END
    END
* TOTAL CR
    IF R.NEW(FT.AMOUNT.CREDITED) <> "" THEN
        TMP.CCY = R.NEW(FT.AMOUNT.CREDITED)[1,3]
        TMP.AMT = R.NEW(FT.AMOUNT.CREDITED)[4,22]
        CALL SC.FORMAT.CCY.AMT(TMP.CCY,TMP.AMT)
        IF ETEXT = "" THEN
            R.NEW(FT.AMOUNT.CREDITED) = TMP.CCY:TMP.AMT
        END
    END
* LOCAL TOTAL DR
    IF R.NEW(FT.LOC.AMT.DEBITED) <> "" THEN
        TMP.CCY = R.COMPANY(EB.COM.LOCAL.CURRENCY)
        TMP.AMT = R.NEW(FT.LOC.AMT.DEBITED)
        CALL SC.FORMAT.CCY.AMT(TMP.CCY,TMP.AMT)
        IF ETEXT = "" THEN
            R.NEW(FT.LOC.AMT.DEBITED) = TMP.AMT
        END
    END
* LOCAL TOTAL CR
    IF R.NEW(FT.LOC.AMT.CREDITED) <> "" THEN
        TMP.CCY = R.COMPANY(EB.COM.LOCAL.CURRENCY)
        TMP.AMT = R.NEW(FT.LOC.AMT.CREDITED)
        CALL SC.FORMAT.CCY.AMT(TMP.CCY,TMP.AMT)
        IF ETEXT = "" THEN
            R.NEW(FT.LOC.AMT.CREDITED) = TMP.AMT
        END
    END
* LOCAL MKTG.EXCH PROFIT
    IF R.NEW(FT.MKTG.EXCH.PROFIT) <> "" THEN
        TMP.CCY = R.COMPANY(EB.COM.LOCAL.CURRENCY)
        TMP.AMT = R.NEW(FT.MKTG.EXCH.PROFIT)
        CALL SC.FORMAT.CCY.AMT(TMP.CCY,TMP.AMT)
        IF ETEXT = "" THEN
            R.NEW(FT.MKTG.EXCH.PROFIT) = TMP.AMT
        END
    END

    ETEXT = ''

    RETURN

*************************************************************************
CHECK.IF.PM.ACTIVE:
*************************************************************************
*

    LOCATE 'PM' IN R.COMPANY(EB.COM.APPLICATIONS)<1,1> SETTING POSN ELSE
        POSN = 0
    END
    IF POSN THEN
        F.PM.PARAMETER = ''
        PM.PARAM.REC = ''
        ER = ''
        CALL OPF("F.PM.PARAMETER",F.PM.PARAMETER)
        CALL F.READ("F.PM.PARAMETER","SYSTEM",PM.PARAM.REC,F.PM.PARAMETER,ER)

        IF ER THEN
            TEXT = 'MISSING PM PARAMETER RECORD'
            CALL FATAL.ERROR("FUNDS.TRANSFER")
        END ELSE
            LOCATE 'FT' IN PM.PARAM.REC<PM.PP.APPLN.INC,1> SETTING POSN ELSE
                POSN = 0
            END
        END
    END
    IF POSN THEN PM.ACTIVE = 1 ELSE PM.ACTIVE = 0
    RETURN
*
*******************************************************************************
CALL.PM:
*******************************************************************************

    IF PM.ACTIVE THEN
        SAVE.FUNCTION = V$FUNCTION
        V$FUNCTION = 'I'
        CALL PM.SETUP.PARAM
        CALL PM.CONTROL.FT
        V$FUNCTION = SAVE.FUNCTION
    END

    RETURN
*************************************************************************
FATAL.ERROR:
*************************************************************************
*
* Come here if an error exists - Flush the deferred write cache
*
    IF V$FUNCTION = 'INFORMER.CREATE.FT' THEN
        CALL TRANSACTION.ABORT
        TEXT = E
        ETEXT = ''
        CALL FATAL.ERROR('INFORMER.CREATE.FT')
    END
    ELSE
        ER.MSG = E
        ETEXT = ''
    END

    RETURN

*************************************************************************
CALL.RTNS.IF.OK:

    IF VER.NAME<2> THEN
        ETEXT = ''
        K.I = 2
*Line [ 776 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        LOOP WHILE K.I <= DCOUNT(VER.NAME, @FM)
            TEMP.APPLICATION = APPLICATION
            APPLICATION = VER.NAME<1>[1, KOMMA - 1]

            CALL.ROUTINE = '' ; CALL.ROUTINE = VER.NAME<K.I>
            CALL @CALL.ROUTINE

            APPLICATION = TEMP.APPLICATION
            K.I += 1
        REPEAT

        IF ETEXT THEN
            ER.RTN = ETEXT ; ETEXT = ''
        END
    END

    RETURN
*************************************************************************

*************************************************************************
PROGRAM.END:
*************************************************************************

    JOURNAL.BYPASS = SAVE.JOURNAL.BYPASS

    APPLICATION = SAVE.APPLICATION
    V$FUNCTION = SAVE.FUNCTION
    ID.NEW = SAVE.ID.NEW
    V = SAVE.V
    A = SAVE.A
    AF = SAVE.AF
    AV = SAVE.AV
    AS = SAVE.AS

    LINK.DATA<FT.AMOUNT.DEBITED> = R.NEW(FT.AMOUNT.DEBITED)
    LINK.DATA<FT.AMOUNT.CREDITED> = R.NEW(FT.AMOUNT.CREDITED)
    LINK.DATA<FT.TREASURY.RATE> = R.NEW(FT.TREASURY.RATE)

    MAT R.NEW = MAT R.SAVE

    RETURN

END
