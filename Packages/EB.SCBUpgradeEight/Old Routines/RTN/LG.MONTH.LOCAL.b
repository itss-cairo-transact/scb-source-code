* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>149</Rating>
*-----------------------------------------------------------------------------

******* WAEL *******

    SUBROUTINE LG.MONTH.LOCAL

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 45 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.MONTH.LOCAL'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    CUR.SEL = "SELECT FBNK.CURRENCY"
    CUR.KEY.LIST ="" ; CUR.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUR.SEL,CUR.KEY.LIST,"",CUR.SELECTED,ER.MSG)

 FOR K = 1 TO CUR.SELECTED
    T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ ":21096:" AND WITH CURRENCY EQ ":CUR.KEY.LIST<K>
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED > 0 THEN GOSUB GET.RECORDS
 NEXT K

PRINT XX<50,4>
RETURN
*-----------------------------------
GET.RECORDS:
TOT.LG.LOCAL = ' '
TOT.LG.FOREIGN = ' '

 IF KEY.LIST THEN
      FOR I = 1 TO SELECTED
             CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1) 
             LG.CATEG<I> =R.LD<LD.CATEGORY>
             LG.AMT<I> =R.LD<LD.AMOUNT>
             CUR<I>=R.LD<LD.CURRENCY>
             CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR<I>,CUR.NAME) 
             IF CUR<I> # LCCY THEN
                CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR<I>,MYRATE)
             END ELSE
                MYRATE = 1
             END

             IF CUR<1> = CUR<I> THEN
                IF LG.CATEG<I> = "21096" THEN TOT.LG.LOCAL = TOT.LG.LOCAL + LG.AMT<I>
               * IF LG.CATEG<I> = "21097" THEN TOT.LG.FOREIGN = TOT.LG.FOREIGN + LG.AMT<I>
             END

      NEXT I 
           QQ = TOT.LG.LOCAL + TOT.LG.FOREIGN
           HH = QQ * MYRATE
           LOCAL.AMT = LOCAL.AMT + HH

           XX = SPACE(80) 
           XX<1,1>[1,10]    = CUR.NAME
           XX<1,1>[15,10]   = TOT.LG.LOCAL
           XX<1,1>[30,10]   = QQ
           XX<1,1>[45,5]    = MYRATE
           XX<1,1>[55,15]   = HH
           XX<50,4>[1,20]   = " ������� ���� ���� = ":LOCAL.AMT

           PRINT XX<1,1> 
           PRINT

   END ELSE
       ENQ.ERROR = "NO RECORDS FOUND"
   END

RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30): " ������ ������ ������ �������� �������"
    PR.HD :="'L'":SPACE(28):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):" ������" :SPACE(3):" �������� �������" :SPACE(5):" ��������" :SPACE(5): "�����  " :SPACE(5):"������� "
    PR.HD :="'L'":SPACE(1):STR('_',90)

    HEADING PR.HD
 RETURN
*==============================================================
END
