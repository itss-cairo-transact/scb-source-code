* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
**********ABEER*******************
*-----------------------------------------------------------------------------
* <Rating>-241</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.ADVINCREASE.1271

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*Line [ 47 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    IF MYCODE EQ '1271'  THEN
        FOR J = 1 TO 2
            GOSUB INITIATE
            GOSUB PRINT.HEAD
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
        NEXT J
    END

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.ADVINCREASE.1271'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    IF ID.NEW NE '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            YTEXT = "Enter the L/G. No. : "
            CALL TXTINP(YTEXT,8,22,"12","A")

            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.ISSUE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE =DATE.ISSUE[7,2]:"/":DATE.ISSUE[5,2]:"/":DATE.ISSUE[1,4]
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
**************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
***************

    IF THIRD.NO EQ CUST.AC THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    END ELSE
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
        LG.CUST1 = LOC.REF<1,CULR.ARABIC.NAME>
        LG.CUST2 = LOC.REF<1,CULR.ARABIC.NAME.2>
    END
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CURRNO = R.LD<LD.CURR.NO>
    CURRNO1= CURRNO-1
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CUR,CRR.DESC)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    AMT.INC=R.LD<LD.AMOUNT.INCREASE>
******    NEW.AMOUNT = AMT.INC+R.NEW(LD.AMOUNT)
    NEW.AMOUNT = R.NEW(LD.AMOUNT)
************* **GET AMOUNT INC OR DEC************************************
    NEW.AMOUNT= LG.AMT+AMT.INC
    NEW.AMOUNT=FMT(NEW.AMOUNT,"R2")
**********************************************************************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>
    RETURN
*===============================================================
PRINT.HEAD:
    MYID = MYCODE:'.':MYTYPE

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD ="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):"�������":":":XX
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME1:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���  � �  : ":LG.NO: TYPE.NAME
    PRINT
    PRINT SPACE(20): "�������������    : ": " **":LG.AMT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
    PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME1
    IF THIRD.NAME2 THEN
        PRINT SPACE(5):THIRD.NAME2
    END ELSE
        PRINT SPACE(5):""
    END
    IF THIRD.NO NE CUST.AC THEN
        PRINT SPACE(20): "�����������   : ":LG.CUST1
        PRINT SPACE(20): "              : ":LG.CUST2
        PRINT
    END ELSE
        PRINT SPACE(20): "������  ": THIRD.NAME1
        PRINT SPACE(20): "              : ":THIRD.NAME2
        PRINT
    END
*//////////////////////////////////////////////////////
    PRINT SPACE(20): "________________________________________________"
    PRINT SPACE(6):"�������� ��� ���� ������ ������ ������ ����� ��� ��� �������"
    PRINT SPACE(2):"����� ��� ���� ������ ��� ���� ���� ������ ������ ������ �����": "*":AMT.INC:"*" :CRR.DESC
    PRINT SPACE(2):"����� ����������� �����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT SPACE(2):"���� ��� ��� ������������� ��������� �� ����� ����":"*":NEW.AMOUNT:"*":CRR.DESC
    IN.AMOUNT=NEW.AMOUNT
*****************ZEAD TEST
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 221 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 223 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR.DESC
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
    PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
    PRINT SPACE(6):"���� ������� ��� �� ���� ���� ������� ������� �� ������ �� ���"
    PRINT SPACE(2):"����� ��� ��� ������ ����� ��� �����":"*":AMT.INC:"*" :CRR.DESC:" ": "������� ����� ������� �����"
    PRINT SPACE(2):"��� ���� ��� ���� ������ ��� " :YY :"  ������ ��� �� ������ ."
    PRINT SPACE(6):"���� ��� ���� ������� ������ ���� ������� �� ���� ��� ���� �����"
    PRINT SPACE(2):"�� ���� ��� �� ������ ."
    PRINT SPACE(6):"����� ����� ��� ���� ������ ������� �� ���� ������ ��� ����� ���"
    PRINT SPACE(2):" ������ ����� ��� ������� . "
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ; PRINT
    IF J = '1' THEN
        PRINT SPACE(5): "���� ��� : ":THIRD.NAME1
        IF THIRD.NAME2 THEN
            PRINT SPACE(5):THIRD.NAME2
        END ELSE
            PRINT SPACE(5):""
        END
        PRINT SPACE(15):THIRD.ADDR1
        PRINT SPACE(15):"����� ������ ��������"
    END ELSE
          PRINT;PRINT;PRINT;PRINT
    END
    PRINT SPACE(55):"�� ��� ���� ������ ."
    PRINT  "������ �� :":XX
    PRINT SPACE(5):"LG.ADVINCREASE.1271"
*Line [ 256 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==========================================================
