* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
****NESSREEN AHMED 27/05/2019**************
*-----------------------------------------------------------------------------
* <Rating>229</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAST.DEBIT.ADVICE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.DB.ADVICE

    TEXT = 'HI' ; CALL REM

    Path = "&SAVEDLISTS&/Debit_Advice_File_MC"
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    F.VISA.DB.AD = '' ; FN.VISA.DB.AD = 'F.SCB.MAST.DB.ADVICE' ; R.VISA.DB.AD= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.DB.AD,F.VISA.DB.AD)

    EOF = ''
    X = 0
*    TEXT = 'X=':X ; CALL REM
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CARD.NO = '' ; EMB.NAME = '' ; BR.CODE = '' ; BNK.ACCT = '' ; DB.CR = ''
            CURR    = '' ; AMT = '' ; NO.DEC = '' ; POS.DATE = '' ; ACCT.NO = '' ; INV.NO = ''

            CARD.NO               = Line[1,25]
            EMB.NAME              = Line[26,30]
            BR.CODE               = Line[56,20]
            BNK.ACCT              = Line[76,20]
            DB.CR                 = Line[96,2]
            CURR                  = Line[98,3]
            AMT                   = Line[101,12]
            NO.DEC                = Line[113,1]
            POS.DATE              = Line[114,8]
            ACCT.NO               = Line[122,25]
            INV.NO                = Line[147,10]

            X = X + 1

            CARD.ID = TRIM(CARD.NO, " " , "T")
            ID.KEY = CARD.ID:".":POS.DATE
            CALL F.READ(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD, F.VISA.DB.AD ,E1)
            IF CARD.NO[1,1] EQ '5' THEN
                R.VISA.DB.AD<MDA.CARD.NO>     = TRIM(CARD.NO, " " , "T")
                R.VISA.DB.AD<MDA.EMB.NAME>    = TRIM(EMB.NAME, " " , "T")
                R.VISA.DB.AD<MDA.BANK.BR>     = TRIM(BR.CODE, " " , "T")
                R.VISA.DB.AD<MDA.BANK.ACCT>   = TRIM(BNK.ACCT, " " , "T")
                R.VISA.DB.AD<MDA.DB.CR>       = DB.CR
                R.VISA.DB.AD<MDA.BILL.CURR>   = CURR
                R.VISA.DB.AD<MDA.AMOUNT>      = TRIM(AMT, "0" , "L")/100
                R.VISA.DB.AD<MDA.NO.OF.DEC>   = NO.DEC
                R.VISA.DB.AD<MDA.POST.DATE>   = POS.DATE
                R.VISA.DB.AD<MDA.ACCT.NO>     = ACCT.NO
                R.VISA.DB.AD<MDA.INVOICE.NO>  = INV.NO

                CALL F.WRITE(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD)
                CALL JOURNAL.UPDATE(ID.KEY)
                POST.DATE = POS.DATE
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    TEXT = '�� ����� ��� ��������� ����' ; CALL REM
    TEXT = POST.DATE ; CALL REM

    DIR.NAME = '&SAVEDLISTS&'
    NEW.FILE = "Debit_Advice_File_MC"

    EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
    CLOSESEQ MyPath
    TEXT = 'END' ; CALL REM
    X = ''
    RETURN
END
