* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
********* WAEL ********
*-----------------------------------------------------------------------------
* <Rating>-515</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.CANCEL.LETTER.1
**    PROGRAM LG.CANCEL.LETTER.1

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

*Line [ 48 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
*    IF MYCODE # '1309' AND MYCODE # '1301' THEN E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
    IF MYCODE EQ "1309" THEN
        GOSUB INITIATE

        GOSUB PRINT.HEAD
        GOSUB BODY

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
    IF MYCODE EQ "1301" THEN
        GOSUB INITIATE
*******FOR II = 1 TO 2
        FOR II = 1 TO 1
            GOSUB PRINT.HEAD
            GOSUB BODY2
        NEXT II
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
    IF MYCODE EQ '1304' OR MYCODE EQ '1305'  THEN
        GOSUB INITIATE
        FOR JJ = 1 TO 2
            GOSUB PRINT.HEAD
            GOSUB BODY3
        NEXT JJ
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CANCEL.LETTER.1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    PURPOSE = LOCAL.REF<1,LDLR.IN.RESPECT.OF>
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
*************************************
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
    IF THIRD.NO EQ CUST.AC THEN
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    END ELSE
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
        LG.CUST1 = LOC.REF<1,CULR.ARABIC.NAME>
        LG.CUST2 = LOC.REF<1,CULR.ARABIC.NAME.2>
    END
*LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE,1>
*    LG.MAT.DATE = R.LD<LD.FIN.MAT.DATE>
    LG.MAT.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

    LG.APPROVAL.DATE=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    LG.ACCT=LOCAL.REF<1,LDLR.DEBIT.ACCT,1>
    IF ID.NEW EQ '' THEN
        LG.AMT =R.LD<LD.AMOUNT>
        LG.AMT.OLD =R.LD<LD.AMOUNT>
    END ELSE
        LG.AMT= R.NEW(LD.AMOUNT)
        LG.AMT.OLD =R.LD<LD.AMOUNT>
    END
    IF ID.NEW EQ '' THEN
        AMT.INC=ABS(R.LD<LD.AMOUNT.INCREASE>)
    END ELSE
        AMT.INC=ABS(R.NEW(LD.AMOUNT.INCREASE))
    END
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]

    DATY = LG.MAT.DATE
    LG.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>

*YY = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    YY = LG.MAT.DATE[7,2]:'/':LG.MAT.DATE[5,2]:'/':LG.MAT.DATE[1,4]
*YY = R.LD<LD.LOCAL.REF><1,LDLR.END.COMM.DATE>
    DATZ= TODAY
    CDATE =LOCAL.REF<1,LDLR.END.COMM.DATE>
    CCDATE= CDATE[7,2]:'/':CDATE[5,2]:"/":CDATE[1,4]
    ZZ= DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ= LG.APPROVAL.DATE
    QQ = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]

*--------------------------UPDATED BY NOHA 10/04/2009--------
** T.SEL.NEW = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":ID.NEW: "... AND OPERATION.CODE EQ 1309"
** CALL EB.READLIST(T.SEL.NEW,KEY.LIST.NEW,"",SELECTED.NEW,ERR.MSG.NEW)
** CALL F.READ(FN.LD,KEY.LIST.NEW<SELECTED.NEW>,R.LD.NEW,F.LD,E1.NEW)
** LG.DATE.NEW = R.LD.NEW<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
** YY.NEW = LG.DATE.NEW[7,2]:'/':LG.DATE.NEW[5,2]:'/':LG.DATE.NEW[1,4]

    CALL F.READ(FN.LD,ID.NEW,R.LD.NEW,F.LD,E1.NEW)
    OP.CODE     = R.LD.NEW<LD.LOCAL.REF><1,LDLR.OPERATION.CODE>
    IF OP.CODE EQ 1309 THEN
        LG.DATE.NEW = R.LD.NEW<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
        YY.NEW = LG.DATE.NEW[7,2]:'/':LG.DATE.NEW[5,2]:"/":LG.DATE.NEW[1,4]
    END ELSE
        LG.DATE.NEW = ''
        YY.NEW = ''
    END

*-------------------------------------------------------------
** T.SEL.H = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":ID.NEW:"..."
** CALL EB.READLIST(T.SEL.H,KEY.LIST.H,"",SELECTED.H,ERR.MSG.H)
    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)
*** CALL F.READ(FN.LD.H,KEY.LIST.H<SELECTED>,R.LD.H,F.LD.H,E1.H)
    CURRNO =R.LD<LD.CURR.NO>
    CURRNO1=CURRNO-1

    IF ID.NEW EQ '' THEN
    LG.ID = COMI
    END ELSE
    LG.ID= ID.NEW
    END
    IDHIS=LG.ID:";":CURRNO1

    ID=IDHIS
    OLD.ID = IDHIS
    CALL F.READ(FN.LD.H,IDHIS,R.LD.H,F.LD.H,E1.H)
    LOCAL.REF = R.LD.H<LD.LOCAL.REF>
    OPR.CODE  = LOCAL.REF<1,LDLR.OPERATION.CODE>
    WW = R.LD.H<LD.DATE.TIME>
    LG.MAT.DAT12=R.LD.H<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
    TEXT = "LG.MAT.DAT12 : " : LG.MAT.DAT12 ; CALL REM
    YY.HIS = LG.MAT.DAT12[7,2]:'/':LG.MAT.DAT12[5,2]:'/':LG.MAT.DAT12[1,4]

* D.TIME = WW[1,6]
* WWW = D.TIME[5,2]:'/':D.TIME[3,2]:"/":"20":D.TIME[1,2]
    D.TIME=LG.APPROVAL.DATE
* WWW = D.TIME[7,2]:'/':D.TIME[5,2]:'/':D.TIME[1,4]
    WWW = D.TIME[7,2]:'/':D.TIME[5,2]:'/':D.TIME[1,4]
*-----------------------------------

    RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":"�����":"*"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
 * CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
     CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
 *************

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":MYBRANCH
    PR.HD :="'L'":SPACE(1):STR('_',22)
    PR.HD :="'L'":SPACE(1):"��� ":ZZ
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
*    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:" ":TYPE.NAME:""
    PRINT SPACE(17): "���� �������� ��� :  � � " :LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(17): "���������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "������� ������     : ":THIRD.NAME1
    PRINT SPACE(17): "                  : ":THIRD.NAME2
    PRINT
    IF THIRD.NO NE CUST.AC THEN
        PRINT SPACE(17): "��������������    : ":LG.CUST1
        PRINT SPACE(17): "                  : ":LG.CUST2
    END
    PRINT
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY.HIS
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT

*=============
    PRINT SPACE(5):" ������ ������� �����  ������ ": WWW :" ����� �������� �� ��� ������ "
    PRINT
    PRINT SPACE(2):" ���� ������ ������ ������ �� ����� ��" :" ": YY.HIS :" � ���� �� ���� "
    PRINT
    PRINT SPACE(2):" ���� ��� ������ ���� ����� ����� �� ���� ��� ������."
    PRINT
    PRINT SPACE(2):" � ���� ��� �� ���� ��� ���� ����� ������ ���� ������ ������� "
    PRINT
    PRINT SPACE(2):"�� ������� ":"� ������ ������� �� ����������  ������ ����� ."
    IF (OPR.CODE = '1231' OR OPR.CODE = '1234' OR OPR.CODE = '1235') THEN
        TEXT='CANCEL';CALL REM
        PRINT;PRINT
        TXTY =" �� ������ �������� ������ �� "
        PRINT " ������ ������ ����� �� ���� ��� ���� " :TXTY
        PRINT
        PRINT " ����� �������� ."
    END ELSE
        PRINT;PRINT
        TXTY =" ����� �������� ."
        PRINT " ������ ������ ����� �� ���� ��� ���� " :TXTY
    END
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"


    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*================================================================
BODY2:

    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT
    PRINT
    PRINT SPACE(17): "���� �������� ��� � � : ":LG.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(17): "���������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
***************************
    PRINT SPACE(17): "���� ��� ���      : ":THIRD.NAME1
    PRINT SPACE(17): "                  : ":THIRD.NAME2
    PRINT
    IF THIRD.NO NE CUST.AC THEN
        PRINT SPACE(17): "������� ������    : ":LG.CUST1
        PRINT SPACE(17): "                  : ":LG.CUST2
    END
***************************
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY.HIS
    PRINT
*--------------------------------------------------------------------
    PRINT SPACE(17): "________________________________________________"
    PRINT
    PRINT SPACE(5):" ����� ��� ������ ������� ���� ����� ������" :WWW:" ��� �������� "
*  PRINT  SPACE(5):" ����� ��� ������ ������� ���� ����� ������"      " ��� �������� "
    PRINT SPACE(5):"������ ���� ���� ������ ���� ������ ������ ������ �� ������� � ������ "
    PRINT
    PRINT SPACE(5):"������� �� ����������  ������ ����� � ��� ������� ������� ��� ."

    PRINT
    PRINT SPACE(5):YY.HIS:" ��� �� ����� ���� ��� ������ ���� ��� ������ ."
    PRINT
    IF (OPR.CODE = '1231' OR OPR.CODE = '1234' OR OPR.CODE = '1235') THEN
        PRINT SPACE(5):" ������ ������ ����� ����� ������ ������� � ������ ��������� ����� "
        PRINT
        PRINT SPACE(5):" ���� �������� �������� ."
    END ELSE
        PRINT SPACE(5):" ������ ������ ����� ����� ������ ������� ��������� �������� ��������. "
    END
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT;PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT
    IF II = '1' THEN
        PRINT SPACE(5):"���� ���     :":LG.ACCT
        PRINT SPACE(5):"������       :":THIRD.NAME1
        PRINT SPACE(5):"             :":THIRD.NAME2
        PRINT SPACE(18):":": THIRD.ADDR1
        PRINT SPACE(18):":": THIRD.ADDR2
        PRINT
        PRINT SPACE(5):"����� ������ �������� : ���� ���� �� ���� ������"
        PRINT SPACE(5):" ���� ������� ������ ��� ������ ������ ��������"
        PRINT SPACE(5):"����� ����� ������� ������ ����� ."
    END
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==================================================================
*==================================================================
BODY3:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:" ":TYPE.NAME:""
    PRINT
    PRINT SPACE(17): "��������������� : ":" **":LG.AMT.OLD:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "���� ��� ���      : ":THIRD.NAME1
    PRINT SPACE(17): "                  : ":THIRD.NAME2
    PRINT
    IF THIRD.NO NE CUST.AC THEN
        PRINT SPACE(17): "��������������    : ":LG.CUST1
        PRINT SPACE(17): "                  : ":LG.CUST2
    END
    PRINT
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY.HIS
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT

*=============
    IF MYCODE EQ '1304' THEN
        PRINT SPACE(5):"������ ������� ����� ������ ": WWW :"� ���� ���� �������� ���� ������ ������� "
        PRINT
        PRINT SPACE(2):"�������� ����� ���� ������ ������ ������ ��� ������"
        PRINT
        PRINT SPACE(2):"����� �������� ���� ���� ������ ������ ������� �� ������� � ������ ������� "
        PRINT
        PRINT SPACE(2):"�� ���������� ������ ����� ."
        PRINT
        PRINT SPACE(2):"���� ������ ����� ����� ��� ���� ������ ����� �������� ������� ��������� ��� ������ �� "
        PRINT
        PRINT SPACE(2):"�������� �� ����� ."
        PRINT
        PRINT SPACE(5):"����� ������ �������� "
        PRINT ; PRINT ; PRINT
        PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
        PRINT ; PRINT
        PRINT SPACE(50):"��� ��� ���� ������"
        IF JJ = '1' THEN
            PRINT SPACE(5):"���� ���     :":LG.ACCT
            PRINT SPACE(5):"������       :":THIRD.NAME1
            PRINT SPACE(5):"             :":THIRD.NAME2
            PRINT SPACE(18):":": THIRD.ADDR1
            PRINT SPACE(18):":": THIRD.ADDR2
            PRINT
        END
    END ELSE
        IF MYCODE EQ '1305' THEN
*=============
            PRINT SPACE(5):"������ ������� ����� ������ ": WWW :"� ���� ���� �������� ���� ������� ������ ����� " : AMT.INC
            PRINT
            PRINT SPACE(2):" ��� ���� ������ ������� ":"�������� ����� ���� ������ ������ ������ ��� ������"
            PRINT
            PRINT SPACE(2): "����� �������� ���� ���� ������ �������  ��������  �� ������� � ������ �������"
            PRINT
            PRINT SPACE(2):"�� ���������� ������ ����� ."
            PRINT
            PRINT SPACE(2):"� ��� ��� ��� �������� ����� ������ ������� �� ����� ����"  : LG.AMT

            IN.AMOUNT = LG.AMT
            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 472 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 474 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
                    PRINT SPACE(2):OUT.AMOUNT<1,I> :" ":CRR
                END ELSE
                    PRINT SPACE(2):OUT.AMOUNT<1,I>
                END
            NEXT I

            PRINT
            PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
            PRINT
            PRINT SPACE(2):"����� ������ ��������"
            PRINT ; PRINT ; PRINT
            PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
            PRINT ; PRINT
            PRINT SPACE(50):"��� ��� ���� ������"
            IF JJ = '1' THEN
                PRINT SPACE(5):"���� ���     :":LG.ACCT
                PRINT SPACE(5):"������       :":THIRD.NAME1
                PRINT SPACE(5):"             :":THIRD.NAME2
                PRINT SPACE(18):":": THIRD.ADDR1
                PRINT SPACE(18):":": THIRD.ADDR2
                PRINT
            END
        END
    END
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN

********************************************************************
