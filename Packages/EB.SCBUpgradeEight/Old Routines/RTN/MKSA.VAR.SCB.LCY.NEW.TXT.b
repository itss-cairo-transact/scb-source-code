* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-173</Rating>
*-----------------------------------------------------------------------------
*-- CREATE BY WAGDY
*------------------
    SUBROUTINE MKSA.VAR.SCB.LCY.NEW.TXT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.LCY.REF
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*----------------------------------------------
    XXZZ = " �� ���� ���� ����� ������ ���� �"
*Line [ 53 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP(XXZZ, 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        TOD = TODAY
        FILE.NM = TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.new.txt"
**---------------------------------------------------------------------**
        OPENSEQ "/hq/opce/bclr/user/dltx/", FILE.NM : TO DD THEN
            CLOSESEQ DD
            HUSH ON
            EXECUTE 'DELETE ':"/hq/opce/bclr/user/dltx/":' ':"FILE.NM"
            HUSH OFF
        END
**---------------------------------------------------------------------**
        FN.TRNS = 'F.SCB.BR.LCY.REF' ; F.TRNS = '' ; R.TRNS = ''
        CALL OPF(FN.TRNS,F.TRNS)

        T.SEL   = "SELECT F.SCB.BR.LCY.REF WITH T.DATE EQ ": TODAY : " BY-DSND ACP.REJ "
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
        ENT.NO  = ''

        LOOP
            REMOVE OUR.ID FROM KEY.LIST SETTING POS
        WHILE OUR.ID:POS
            CALL F.READ( FN.TRNS,OUR.ID, R.TRNS,F.TRNS, ETEXT)

            F.1  = R.TRNS<1>
            F.2  = R.TRNS<2>
            F.3  = R.TRNS<3>
            F.4  = R.TRNS<4>
            F.5  = R.TRNS<5>
            F.6  = R.TRNS<6>
            F.7  = R.TRNS<7>
            F.8  = R.TRNS<8>
            F.9  = R.TRNS<9>
            F.10 = R.TRNS<10>
            F.11 = R.TRNS<11>
            F.12 = R.TRNS<12>
            F.13 = R.TRNS<13>

            GOSUB WRITE.ROW
        REPEAT

        TEXT = "��� �� ����� ����� ��� ������ �����"; CALL REM
    END

    CALL !HUSHIT(0)
*-- EDIT BY NESSMA 2014/05/12
    COMAND2 = " chmod -R 666 /hq/opce/bclr/user/dltx/":TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:"-credit.new.txt "
*-- END EDIT --
    EXECUTE COMAND2
    RETURN
**-------------------------------------------------------------------**
WRITE.ROW:
*---------
    DD.DATA  = F.1:','
    DD.DATA := F.2:','
    DD.DATA := F.3:','
    DD.DATA := F.4:','
    DD.DATA := F.5:','
    DD.DATA := F.6:','
    DD.DATA := F.7:','
    DD.DATA := F.8:','
    DD.DATA := F.9:','
    DD.DATA := F.10:','
    DD.DATA := F.11:','
    DD.DATA := F.12:','
    DD.DATA := F.13

    WRITESEQ DD.DATA TO DD ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
END
