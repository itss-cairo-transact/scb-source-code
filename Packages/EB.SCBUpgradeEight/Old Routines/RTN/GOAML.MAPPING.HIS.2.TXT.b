* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE GOAML.MAPPING.HIS.2.TXT 

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.STAFF.RANK
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.PROFESSION
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DE.BIC
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.GOVERNORATE
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    EXECUTE "COMO ON GOAML.MAPPING.HIS.2.TXT"
    TD = ''
    CALL DBR("DATES":@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',TD)
    CRT 'LAST WORKING DATE : ':TD
    TD = TD[3,6]
    GOSUB INIT_ACC
    GOSUB INIT_CUS
    GOSUB PROC_ACC
    GOSUB CLOSE_ACC_FILE

    T.SEL.CUS = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT LT 90 AND ACTIVE.CUST EQ 'YES' AND DATE.TIME GE ":TD:"0000" 
    CALL EB.READLIST(T.SEL.CUS, KEY.LIST.CUS, "", SELECTED.CUS, ERR.CUS)
    CRT T.SEL.CUS
    IF SELECTED.CUS THEN
        FOR K = 1 TO SELECTED.CUS
            CALL F.READ(FN.CUS,KEY.LIST.CUS<K>, R.CUS, F.CUS, ETEXT.CUS)
            IF R.CUS THEN
                GOSUB CREATE.CUST
            END
        NEXT K
        GOSUB CLOSE_CUS_FILE
    END
    ELSE
        CRT 'Customer file is empty'
        GOSUB CLOSE_CUS_FILE
    END


    EXECUTE "COMO OFF GOAML.MAPPING.HIS.2.TXT"
    RETURN


***************************CREATE ACCOUNT TXT(EE) *****************
    INIT_ACC:
    OPENSEQ "&SAVEDLISTS&" , "Acc.GOAML.txt" TO EE THEN
        CLOSESEQ EE
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"Acc.GOAML.txt"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "Acc.GOAML.txt" TO EE ELSE
        CREATE EE THEN
            PRINT 'FILE Acc.GOAML.txt CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create Acc.GOAML.txt File IN &SAVEDLISTS&'
        END
    END

    GOSUB INIT_ACC_FILE

    RETURN


**********************CREATE CUSTOMER TXT(DD)*************
    INIT_CUS:
    OPENSEQ "&SAVEDLISTS&" , "Cus.GOAML.txt" TO DD THEN
        CLOSESEQ DD
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"Cus.GOAML.txt"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "Cus.GOAML.txt" TO DD ELSE
        CREATE DD THEN
            PRINT 'FILE Cus.GOAML.txt CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create Cus.GOAML.txt File IN &SAVEDLISTS&'
        END
    END

    GOSUB INIT_CUS_FILE
    RETURN

********************************** SELECT ACCOUNTS ********************
    PROC_ACC:
    T.SEL.ACC = 'SELECT ':FN.ACC:' WITH  DATE.TIME GE ':TD:'0000 '
    CRT T.SEL.ACC
    CALL EB.READLIST(T.SEL.ACC,KEY.LIST.ACC,'',SELECTED.ACC,ERR.ACC)
    EE.DATA = ''
    IF SELECTED.ACC THEN
        FOR I=1 TO SELECTED.ACC
            ACC.ID = KEY.LIST.ACC<I>
            CALL F.READ(FN.ACC,KEY.LIST.ACC<I>,R.ACC,F.ACC,ETEXT.ACC)
            EE.DATA = ACC.ID:';'
            EE.DATA := R.ACC<AC.CUSTOMER>:';'
            EE.DATA := R.ACC<AC.SHORT.TITLE>:';'
            EE.DATA := R.ACC<AC.CURRENCY>:';'
            EE.DATA := R.ACC<AC.WORKING.BALANCE>:';'
            EE.DATA := R.ACC<AC.TRAN.LAST.DR.CUST>:';'
            EE.DATA := R.ACC<AC.TRAN.LAST.CR.CUST>:';'
            EE.DATA := R.ACC<AC.DATE.LAST.DR.CUST>:';'
            EE.DATA := R.ACC<AC.DATE.LAST.CR.CUST>:';'
            EE.DATA := R.ACC<AC.CATEGORY>:';'
            EE.DATA := 'UNKNOWN':';'
            EE.DATA := 'UNKNOWN':';'
            EE.DATA := R.ACC<AC.OPENING.DATE>:';'
            EE.DATA := R.ACC<AC.CO.CODE>:';'
            EE.DATA := R.ACC<AC.ONLINE.ACTUAL.BAL>:';'
            IF R.ACC<AC.ACCOUNT.TITLE.2> EQ '' THEN
                R.ACC<AC.ACCOUNT.TITLE.2> = 'UNKNOWN'
            END
            EE.DATA := R.ACC<AC.ACCOUNT.TITLE.2>:';'

            IF R.ACC<AC.ACCOUNT.TITLE.1> EQ '' THEN
                R.ACC<AC.ACCOUNT.TITLE.1> = 'UNKNOWN'
            END
            EE.DATA := R.ACC<AC.ACCOUNT.TITLE.1>

************ WRITE ACCOUNTS
            WRITESEQ EE.DATA TO EE ELSE
                PRINT 'ERROR WRITE FILE ACCOUNTS'
            END

        NEXT I
    END
    RETURN

    CLOSE_ACC_FILE:

    CLOSESEQ EE
    PRINT 'ACCOUNTS FINISHED'

    RETURN



***************SELECT CUSTOMERS***********************
CREATE.CUST:




    CUS.ID        = KEY.LIST.CUS<K>
    CUS.LOCAL     = R.CUS<EB.CUS.LOCAL.REF>
    NEW.SEC       = CUS.LOCAL<1,CULR.NEW.SECTOR>
    CUS.RELATIONS = ""
    DD.DATA       = CUS.ID:';'

*****GENDER***
    CUS.GENDER = FIELD(CUS.LOCAL<1,CULR.GENDER>,'-',1)
    IF CUS.GENDER EQ 'M' THEN
        CUS.GEN = 'MALE'
    END
    ELSE
        CUS.GEN = 'FEMALE'
    END
    DD.DATA := CUS.GEN:';'

*****TITLE*****
    CUS.TITLE = CUS.LOCAL<1,CULR.TITLE>
    CALL DBR('SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,CUS.TITLE,CUS.TIT)

    IF CUS.TIT EQ '' THEN
        CUS.TIT = 'UNKNOWN'
    END

    DD.DATA := CUS.TIT:';'

*****NAME******
    IF CUS.LOCAL<1,CULR.ARABIC.NAME> EQ '' THEN
        CUS.LOCAL<1,CULR.ARABIC.NAME> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.ARABIC.NAME>:';'
    DD.DATA := R.CUS<EB.CUS.NAME.1,1>:';'
****LEG ID*****
*DD.DATA := CUS.LOCAL<1,CULR.NSN.NO>
    NSN.NO = CUS.LOCAL<1,CULR.NSN.NO>
    REG.NO = CUS.LOCAL<1,CULR.COM.REG.NO>
    ID.NUM = CUS.LOCAL<1,CULR.ID.NUMBER>

    IF NEW.SEC EQ '4650' THEN
        IF NSN.NO NE '' THEN
            DD.DATA := NSN.NO:';'
        END ELSE
            IF ID.NUM NE '' THEN
                DD.DATA := ID.NUM:';'
            END ELSE
                DD.DATA := '0':';'
            END
        END
    END
    ELSE
        IF REG.NO NE '' AND NEW.SEC NE '4650' THEN
            DD.DATA := REG.NO:';'
        END ELSE
            DD.DATA := '0':';'
        END
    END
*    IF REG.NO EQ '' AND NSN.NO EQ '' AND ID.NUM EQ '' THEN
*        DD.DATA := '0':';'
*    END
****LEG ISSUE**
    ISSUE.DATE = CUS.LOCAL<1,CULR.ID.ISSUE.DATE>
    BIRTH.DATE = R.CUS<EB.CUS.BIRTH.INCORP.DATE>

    IF NEW.SEC EQ '4650' THEN
        IF BIRTH.DATE EQ '' THEN
*DD.DATA := 'UNKNOWN':';'
            DD.DATA := '':';'
        END
        ELSE
            DD.DATA := BIRTH.DATE:';'
        END
        DOC.NAME = '��� ����'
    END ELSE
        IF REG.NO NE '' AND NEW.SEC NE '4650' THEN
            IF ISSUE.DATE EQ '' THEN
*DD.DATA := 'UNKNOWN':';'
                DD.DATA := '':';'
            END
            ELSE

                DD.DATA := ISSUE.DATE:';'
            END
            DOC.NAME = '��� �����'
        END ELSE
            DD.DATA := '' :';'
            DOC.NAME = 'UNKNOWN'
        END
    END
*    IF ID.NUM EQ '' AND NSN.NO EQ '' AND REG.NO EQ '' THEN
*DD.DATA := 'UNKNOWN':';'
*       DD.DATA := '':';'
*        DOC.NAME = 'UNKNOWN'
*    END

****LEG EXPIRE*
    EXP.DATE = CUS.LOCAL<1,CULR.ID.EXPIRY.DATE>
    COM.EXP.DATE = CUS.LOCAL<1,CULR.COM.REG.EXP.D>
    IF NEW.SEC NE '4650' THEN
        IF COM.EXP.DATE NE '' THEN
            DD.DATA := COM.EXP.DATE:';'
        END ELSE
            DD.DATA := '':';'
        END
    END ELSE
        IF NEW.SEC EQ '4650' THEN
            IF EXP.DATE NE '' THEN
                DD.DATA := EXP.DATE:';'
            END ELSE
                DD.DATA := '':';'
            END
        END
    END

*    IF EXP.DATE EQ '' AND COM.EXP.DATE EQ '' THEN
*DD.DATA := 'UNKNOWN':';'
*        DD.DATA := '':';'
*    END

****************
    IF CUS.LOCAL<1,CULR.ID.TYPE> EQ '' THEN
        CUS.LOCAL<1,CULR.ID.TYPE> = 'UNKNOWN'
    END

*DD.DATA := CUS.LOCAL<1,CULR.ID.TYPE>:';'
    DD.DATA := 'NID':';'

    IF DOC.NAME EQ '' THEN
        DOC.NAME = 'UNKNOWN'
    END
*    DD.DATA := DOC.NAME:';'
     DD.DATA := 'NID':';'

    IF R.CUS<EB.CUS.NATIONALITY> EQ '' THEN
        R.CUS<EB.CUS.NATIONALITY> = 'UNKNOWN'
    END

    DD.DATA := R.CUS<EB.CUS.NATIONALITY>:';'

    IF R.CUS<EB.CUS.BIRTH.INCORP.DATE> EQ '' THEN
*R.CUS<EB.CUS.BIRTH.INCORP.DATE> = 'UNKNOWN'
        R.CUS<EB.CUS.BIRTH.INCORP.DATE> = ''
    END
    DD.DATA := R.CUS<EB.CUS.BIRTH.INCORP.DATE>:';'
    AR.ADDR = CUS.LOCAL<1,CULR.ARABIC.ADDRESS>
    CHANGE ';' TO '-' IN AR.ADDR
    IF AR.ADDR EQ '' THEN
        AR.ADDR = 'UNKNOWN'
    END
    DD.DATA := AR.ADDR:';'
*******TOWN COUNTRY*************

*IF R.CUS<EB.CUS.TOWN.COUNTRY> EQ '' THEN
*   R.CUS<EB.CUS.TOWN.COUNTRY> = 'UNKNOWN'
*END
*DD.DATA := R.CUS<EB.CUS.TOWN.COUNTRY>:';'
    IF CUS.LOCAL<1,CULR.GOVERNORATE> EQ '98' OR CUS.LOCAL<1,CULR.GOVERNORATE> EQ '97' OR CUS.LOCAL<1,CULR.GOVERNORATE> EQ '' THEN
        COMP.CODE = R.CUS<EB.CUS.COMPANY.BOOK>
        CALL DBR('SCB.BR.GOVERNORATE':@FM:BRGR.BR.GOVERNORATE,COMP.CODE,TWN.COUNTRY)
        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,CNTR)
    END
    ELSE
        TWN.COUNTRY = CUS.LOCAL<1,CULR.GOVERNORATE>
        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,TWN.COUNTRY,CNTR)
    END
    IF CNTR EQ '' THEN
        CNTR = 'UNKNOWN'
    END
    DD.DATA:= CNTR:';'

    IF R.CUS<EB.CUS.RESIDENCE> EQ '' THEN
        R.CUS<EB.CUS.RESIDENCE> = 'UNKNOWN'
    END
    DD.DATA := R.CUS<EB.CUS.RESIDENCE>:';'

    AR.ADDR = CUS.LOCAL<1,CULR.ARABIC.ADDRESS>
    CHANGE ';' TO '-' IN AR.ADDR
    IF AR.ADDR EQ '' THEN
        AR.ADDR = 'UNKNOWN'
    END
    DD.DATA := AR.ADDR:';'


**********JOB***
*JOB = CUS.LOCAL<1,CULR.STAFF.RANK>
*CALL DBR('SCB.CUS.STAFF.RANK':@FM:STAFF.DESCRIPTION,JOB,JOB.DESC)

*IF NEW.SEC EQ '4650' AND JOB.DESC NE '' THEN
*   JOB = CUS.LOCAL<1,CULR.STAFF.RANK>
*   CALL DBR('SCB.CUS.STAFF.RANK':@FM:STAFF.DESCRIPTION,JOB,JOB.DESC)
*   DD.DATA := JOB.DESC:';'
*END
*ELSE
* IF CUS.LOCAL<1,CULR.JOB.DESCRIPTION> NE '' THEN
*    DD.DATA := CUS.LOCAL<1,CULR.JOB.DESCRIPTION>:';'
* END
*END
    JOB = CUS.LOCAL<1,CULR.PROFESSION>
    CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,JOB,JOB.DESC)

    IF JOB.DESC EQ '' THEN
        DD.DATA := 'UNKNOWN':';'
    END
    ELSE
        DD.DATA := JOB.DESC:';'
    END

*******EMP*****
    EMP.SEC = R.CUS<EB.CUS.SECTOR>
    IF EMP.SEC EQ '1100' THEN
        DD.DATA := 'Y':';'
    END
    ELSE
        DD.DATA := 'N':';'
    END
******EDUCATION LEVEL*********
    EDU = CUS.LOCAL<1,CULR.EDUCATION>
    IF EDU EQ '' THEN
        DD.DATA := 'UNKNOWN':';'
    END
    ELSE

        DD.DATA := EDU:';'
    END
******NEW SEC**
    IF NEW.SEC EQ '' THEN
        DD.DATA := EMP.SEC:';'
    END
    ELSE
        DD.DATA := NEW.SEC:';'
    END
    IF EMP.SEC EQ '' AND NEW.SEC EQ '' THEN
        DD.DATA :='UNKNOWN':';'
    END
**************
*GET RELATIONS
    REL.CODE = R.CUS<EB.CUS.RELATION.CODE>
    IF REL.CODE AND NEW.SEC NE '4650' THEN
        REL.CUS = ''
*Line [ 444 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.OF.REL = DCOUNT(REL.CODE,@VM)
        FOR Q = 1 TO NO.OF.REL
            REL.CODE.1 = REL.CODE<1,Q>
            IF REL.CODE.1 EQ '94' OR REL.CODE.1 EQ '59' THEN
                REL.CUS = R.CUS<EB.CUS.REL.CUSTOMER><1,Q>
                CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,REL.CUS,CUS.S.NAME)

                IF CUS.RELATIONS EQ '' THEN
                    CUS.RELATIONS = CUS.S.NAME<1,1>
                END
                ELSE
                    CUS.RELATIONS = CUS.RELATIONS:';':CUS.S.NAME<1,1>
                END
            END
        NEXT Q
    END
    DD.DATA := R.CUS<EB.CUS.SHORT.NAME>:';'
**************

*DD.DATA := "JOB.TITLE.1":';'
*DD.DATA := "SMS":';'
    DD.DATA := 'UNKNOWN':';'
    IF CUS.LOCAL<1,CULR.TELEPHONE> EQ '' THEN
        CUS.LOCAL<1,CULR.TELEPHONE> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.TELEPHONE,1>:';'

**************
    IF R.CUS<EB.CUS.RESIDENCE> EQ '' THEN
        R.CUS<EB.CUS.RESIDENCE> = 'UNKNOWN'
    END
    DD.DATA := R.CUS<EB.CUS.RESIDENCE>:';'

    IF CUS.LOCAL<1,CULR.OPENING.DATE> EQ '' THEN
*CUS.LOCAL<1,CULR.OPENING.DATE> = 'UNKNOWN'
        CUS.LOCAL<1,CULR.OPENING.DATE> = ''
    END
    DD.DATA := CUS.LOCAL<1,CULR.OPENING.DATE>:';'

    DD.DATA := R.CUS<EB.CUS.COMPANY.BOOK>:';'
    IF CUS.LOCAL<1,CULR.PLACE.ID.ISSUE> EQ '' THEN
        CUS.LOCAL<1,CULR.PLACE.ID.ISSUE> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.PLACE.ID.ISSUE>:';'

    IF R.CUS<EB.CUS.LAST.KYC.REVIEW.DATE> EQ '' THEN
*R.CUS<EB.CUS.LAST.KYC.REVIEW.DATE> = 'UNKNOWN'
        R.CUS<EB.CUS.LAST.KYC.REVIEW.DATE> = ''
    END
    DD.DATA := R.CUS<EB.CUS.LAST.KYC.REVIEW.DATE>:';'

****EMPLOYER NAME FIRST NAME, SECOUND NAME, lAST NAME AND PREFIX************
    IF CUS.LOCAL<1,CULR.FNAME.ARABIC> EQ '' THEN
        CUS.LOCAL<1,CULR.FNAME.ARABIC> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.FNAME.ARABIC>:';'

    IF CUS.LOCAL<1,CULR.MNAME.ARABIC> EQ '' THEN
        CUS.LOCAL<1,CULR.MNAME.ARABIC> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.MNAME.ARABIC>:';'

    IF CUS.LOCAL<1,CULR.LNAME.ARABIC> EQ '' THEN
        CUS.LOCAL<1,CULR.LNAME.ARABIC> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.LNAME.ARABIC>:';'

    IF CUS.LOCAL<1,CULR.PREFIX.ARABIC> EQ '' THEN
        CUS.LOCAL<1,CULR.PREFIX.ARABIC> = 'UNKNOWN'
    END
    DD.DATA := CUS.LOCAL<1,CULR.PREFIX.ARABIC>:';'

    IF R.CUS<EB.CUS.EMPLOYERS.NAME> EQ '' THEN
        R.CUS<EB.CUS.EMPLOYERS.NAME> = 'UNKNOWN'
    END
    DD.DATA := R.CUS<EB.CUS.EMPLOYERS.NAME>
****WRITE CUSTOMER******
    WRITESEQ DD.DATA TO DD ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN

    CLOSE_CUS_FILE:

    CLOSESEQ DD

    PRINT "CUSTOMERS FINISHED"
    RETURN

*************************************************
    INIT_CUS_FILE:

    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    RETURN

    INIT_ACC_FILE:

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC , F.ACC)

    RETURN

END
