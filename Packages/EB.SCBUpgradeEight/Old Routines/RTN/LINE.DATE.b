* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LINE.DATE(REQ.DATE)
************************************************************************
***********by Mahmoud Elhawary ** 5/1/2009 *****************************
************************************************************************

    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE

************************************************************************
*If the date required is not today
*and the date required is end of month
*and last day of month is holiday then
*make the date processed for LINE (line date) equal last day of month
*otherwise if date required equal day before holiday then
*make the line date equal the day before next working day
************************************************************************

    TDD = ''
    TDX = TODAY
    TDL = REQ.DATE
    IF REQ.DATE NE TDX THEN
        CALL CDT("",TDL,'+1W')
        TDR = TDL
        TDN = TDL
        CALL CDT("",TDR,'-1W')
        CALL CDT("",TDN,'-1C')
        TDLMN = TDL[5,2]
        TDRMN = TDR[5,2]
        IF TDLMN NE TDRMN THEN
            TDREM = TDL[1,4]:TDLMN:'01'
            CALL CDT("",TDREM,'-1C')
            TDD = TDREM
        END ELSE
            IF TDR NE TDN THEN
                TDD = TDN
            END ELSE
                TDD = REQ.DATE
            END
        END
    END ELSE
        TDD = TDX
    END
    REQ.DATE = TDD
    RETURN
