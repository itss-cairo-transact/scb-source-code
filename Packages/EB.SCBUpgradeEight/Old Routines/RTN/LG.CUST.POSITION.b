* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1611</Rating>
*-----------------------------------------------------------------------------
*****ABEER    01/07/2003********
*****UPDATED  22/07/2003********
    SUBROUTINE LG.CUST.POSITION

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUST.POSITION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 43 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB BODY
    TEXT = "Report.Generated" ; CALL REM

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
********************************************************
INITIATE:
    REPORT.ID='LG.CUST.POSITION'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter Customer.ID-Start.date-End.date &":"NNNNNNN-YYYYMMDD-YYYYMMDD-CUR "
    CALL TXTINP(YTEXT, 8, 30, "30", "A")
    IF COMI THEN
        IF COUNT(COMI,'-') NE  3 THEN E='Must.Enter.Separator.-.Between.Dates ';CALL ERR ; MESSAGE = 'REPEAT';RETURN
        IF LEN(COMI) LT 29 THEN E='Wrong.Length';CALL ERR ; MESSAGE = 'REPEAT';RETURN
        CUST.ID = FIELD(COMI,"-",1)
        *TEXT=CUST.ID:"CUST.ID";CALL REM

        START.DATE= FIELD(COMI,"-",2)
        ST.D=START.DATE[1,4]:START.DATE[5,2]
        START.ID=CUST.ID:'.':ST.D
        TEMP.ID=CUST.ID:'.197801'

        END.DATE=FIELD(COMI,"-",3)
        EN.D=END.DATE[1,4]:END.DATE[5,2]
        END.ID=CUST.ID:'.':EN.D
        STR.D  =START.DATE[7,2]:"/":START.DATE[5,2]:"/":START.DATE[1,4]
        END.D =END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
        IF END.DATE LE START.DATE THEN E='End.Date.Must.Be.Greater.Than.Start.Date';CALL ERR ; MESSAGE = 'REPEAT';RETURN
        CURR=FIELD(COMI,"-",4)
        IF NOT(CURR) THEN E='Must.Enter.Currency';CALL ERR ; MESSAGE = 'REPEAT';RETURN
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CURR,CURR.N)
    END
    RETURN
******************************************************************
CALLDB:
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
    NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    T.SEL = "SELECT F.SCB.LG.CUST.POSITION WITH @ID GE ":START.ID:" AND @ID LE ":END.ID
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN
        FLG = 'Y'
        MAX.BAL = '' ; START.BAL = ''
        FOR I = 1 TO SELECTED
            FN.LG.CUS.POS = 'F.SCB.LG.CUST.POSITION' ;R.CUS='';F.CUS=''
            CALL F.READ(FN.LG.CUS.POS,KEY.LIST<I>,R.CUS,F.CUS,E1)
            COUNT.DATE=R.CUS<SCB.TXN.DATE>
*Line [ 95 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.D=DCOUNT(COUNT.DATE,@VM)
******************************************************
            IF I = 1 AND COUNT.D = 1 AND START.DATE GT R.CUS<SCB.TXN.DATE><1,1> THEN
                LOCATE CURR IN  R.CUS<SCB.TXN.CURRENCY,1,1> SETTING POSS THEN
                    START.BAL=R.CUS<SCB.TXN.TOT.BALANCE><1,1,POSS>
                END
            END
******************************************************
            FOR J = 1 TO COUNT.D
                IF  START.DATE LE R.CUS<SCB.TXN.DATE><1,J> AND END.DATE GE R.CUS<SCB.TXN.DATE><1,J> THEN
*******************************************************************
                    CURR.C=R.CUS<SCB.TXN.CURRENCY><1,J>
                    *TEXT=CURR.C:'CURR.C';CALL REM
*Line [ 109 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    CURR.COUNT=DCOUNT(CURR.C,@SM)
                    *TEXT=CURR.COUNT:'NO.OF.CURR';CALL REM
********************************************************************
                    FOR  G = 1 TO CURR.COUNT
*  GOSUB START.BALANCE
*************************************************************
                        *TEXT=CURR:'CURR.USER';CALL REM
                        IF CURR EQ R.CUS<SCB.TXN.CURRENCY,J,G> THEN
******************************************************************
*  GOSUB START.BALANCE
                            IF FLG = 'Y' AND START.BAL = '' THEN
                                FLG = 'N'
                                IF J = 1 THEN
                                    GOSUB START.BALANCE
                                END ELSE
                                    IF J GT 1 THEN
                                        LOCATE CURR IN R.CUS<SCB.TXN.CURRENCY,J-1,1> SETTING Z  THEN
                                            START.BAL = R.CUS<SCB.TXN.TOT.BALANCE><1,J-1,Z>
                                        END
                                    END
                                END

                            END
**********************************************************************
                            INC=INC+R.CUS<SCB.TXN.INCREASE><1,J,G>
                            *TEXT=INC;CALL REM
                            DEC=DEC+R.CUS<SCB.TXN.DECREASE><1,J,G>
                            COMM=COMM+R.CUS<SCB.TXN.COMMISSION><1,J,G>
                            EQU.COMM=EQU.COMM+R.CUS<SCB.TOT.EQU.COMM><1,J,G>
                            CONFIS=CONFIS+R.CUS<SCB.TXN.CONFISCATION><1,J,G>

                            TOT.END=R.CUS<SCB.TXN.TOT.BALANCE><1,J,G>
                            TOT.END.BAL=R.CUS<SCB.TXN.TOT.BALANCE><1,J>
                            TMP.MAX.BAL = MAXIMUM(TOT.END.BAL)
                        END
                    NEXT G
                    IF MAX.BAL < TMP.MAX.BAL THEN MAX.BAL = TMP.MAX.BAL
                    LOCATE MAX.BAL  IN TOT.END.BAL<1,1,1> SETTING POS THEN
                        *TEXT=POS:'POS';CALL REM
                        MAX.DATE=R.CUS<SCB.TXN.DATE><1,J>
                        MAX.DATE= MAX.DATE[7,2]:"/":MAX.DATE[5,2]:"/":MAX.DATE[1,4]
                    END

                END
            NEXT J
        NEXT I
    END ELSE E='No.Records.Found';CALL ERR ; MESSAGE = 'REPEAT'
    RETURN
*************************************************************************
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY= DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
*    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� :" :YYBRN
*    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":SPACE(28):"������� �� ���� ���� ���� ������� ������"
    PR.HD :="'L'":SPACE(27): " �� ������ " :STR.D :" ��� " : END.D
    PR.HD :="'L'":SPACE(28):STR('_',35)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    HEADING PR.HD
    RETURN
*******************************************************************
BODY:
    PRINT SPACE(2):"��� ������":": ":  NAME
    PRINT SPACE(2):STR('_',11)
    PRINT
    PRINT SPACE(2):"��� ������":": ":  CUST.ID
    PRINT SPACE(2):STR('_',11)
    PRINT
    PRINT SPACE(2):"����������":": ":  CURR.N
    PRINT SPACE(2):STR('_',11)
    PRINT;PRINT;PRINT
    PRINT SPACE(7):STR('*',56)
    PRINT
    PRINT SPACE(7):"���� ������ �� ��� ������" :STR.D  :"**":START.BAL:"**"
    PRINT;PRINT
    PRINT SPACE(7):"������ �������� ������� ���� ������" :"**":INC:"**"
    PRINT;PRINT
    PRINT SPACE(7):"������ �������� ������� ���� ������":"**":DEC:"**"
    PRINT;PRINT
    PRINT SPACE(7):"������ �������� �������� ���� ������":"**":CONFIS:"**"
    PRINT;PRINT
    PRINT SPACE(7):"���� ������ �� ����� ������":END.D   :"**": TOT.END :"**"
    PRINT;PRINT
    PRINT SPACE(7):"�������� ���� ���� �� ��� ������":"**":COMM:"**"
    PRINT;PRINT
    PRINT SPACE(7):"����� �������� ���� ���� �� ��� ������":"**":EQU.COMM:"**"
    PRINT;PRINT
    PRINT SPACE(7):"���� ������� ���� ���� ��� ������": "**" : MAX.BAL:"**":"��":" ":MAX.DATE
    PRINT;PRINT
    PRINT SPACE(7):STR('*',56)
    PRINT
    RETURN
******************************************************************
START.BALANCE:
    T.SEL1 = "SELECT F.SCB.LG.CUST.POSITION WITH @ID LT ":START.ID :" AND @ID GE ":TEMP.ID
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF KEY.LIST1 THEN
        FOR L = 1 TO SELECTED1
            FN.LG.CUS.POS1 = 'F.SCB.LG.CUST.POSITION' ;R.CUS1='';F.CUS1=''
            CALL F.READ(FN.LG.CUS.POS1,KEY.LIST1<L>,R.CUS1,F.CUS1,E1)
            COUNT.DATE.2=R.CUS1<SCB.TXN.DATE>
*Line [ 216 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.D.C=DCOUNT(COUNT.DATE.2,@VM)
            FOR K = 1 TO COUNT.D.C
                LOCATE CURR IN R.CUS1<SCB.TXN.CURRENCY,K,1>  SETTING Y THEN
                    *TEXT=R.CUS1<SCB.TXN.TOT.BALANCE><1,K,Y>:'RICHTBAL.FOR.CURRENCY';CALL REM
                    START.BAL=R.CUS1<SCB.TXN.TOT.BALANCE><1,K,Y>
                END ELSE
                    IF K= COUNT.D.C THEN START.BAL=0
                END

            NEXT K
        NEXT L
    END ELSE START.BAL=0
    RETURN
**************************************************************************
END
