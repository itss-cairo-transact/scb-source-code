* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE GET.NAME.TO(ARG)

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.USER
    $INSERT           I_FT.LOCAL.REFS
*----------------------------------------------
    LANG      = R.NEW(FT.LOCAL.REF)<1,FTLR.LANGUAGE>
    BRANCH    = R.NEW(FT.CO.CODE)
    FT.ID     = ARG
    DEPT.CODE = R.USER<EB.USE.DEPARTMENT.CODE>

    IF DEPT.CODE EQ '99' THEN
        IF LANG EQ '2' THEN
            ARG = "���� ������"
        END
        IF LANG EQ '1' THEN
            ARG = "ALL BRANCHES"
        END
    END ELSE
        FN.CO = "F.COMPANY" ; F.CO = ""
        CALL OPF(FN.CO, F.CO)

        COMP   = R.NEW(FT.CO.CODE)
        CALL F.READ(FN.CO,COMP,R.CO,F.CO,ER.CO)

        NUM.ID = COMP[8,2]
        IF NUM.ID[1,1] EQ 0 THEN
            NUM.ID = COMP[9,2]
        END
        CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,NUM.ID,ARG)

        IF LANG EQ '2' THEN
            ARG = FIELD(ARG,'.',2)
        END
        IF LANG EQ '1' THEN
            ARG = FIELD(ARG,'.',1)
        END
    END
*--------------------------------------------
    RETURN
END
