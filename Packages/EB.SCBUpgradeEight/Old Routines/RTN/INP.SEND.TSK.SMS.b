* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>587</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE INP.SEND.TSK.SMS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROJECT.IMP.STATUS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.IMP.CODES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.USER.CONTACTS

*    MAIL.ID = ID.NEW
    OPENSEQ "SMS" , "SMS.TEST" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE SMS SMS.TEST'
        HUSH OFF
    END

    OPENSEQ "SMS" , "SMS.TEST" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SMS.TEST CREATED IN SMS'
        END
        ELSE
            STOP 'Cannot create SMS.TEST File IN SMS'
        END
    END
************************
    TOO = ""; TO.PERSON = ""; XXXX = ""
    TO.PERSON = R.NEW(WN.RESPONSIBILITY)<1,1>
    PRI       = R.NEW(WN.PRIORITY)
    CALL DBR('SCB.USER.CONTACTS': @FM:CONTACT.MOBILE,TO.PERSON,TOO)
    CALL DBR('SCB.IMP.CODES': @FM:WN.DESCRIPTION,PRI,PIRORITY)
    TSK.ID    = R.NEW(WN.TASK.ID)<1,1>

    X1   = "You have got a new task task.id = ":TSK.ID:" with ":PIRORITY:" priority... Regards, SCB Team"

    BB.DATA = ''

    BB.DATA  = TOO :'|': X1
    TEXT = "SMS WILL BE SENT TO ":TOO ;CALL REM
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**********************

    RETURN
END
