* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE LMM.LOANS.TXT
*    PROGRAM LMM.LOANS.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*****
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "LMM.LOANS.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/":' ':"LMM.LOANS.TXT"
        HUSH OFF
    END
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "LMM.LOANS.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LMM.LOANS.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create LMM.LOANS.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END
*****
    FN.LM  = 'FBNK.MM.MONEY.MARKET' ; F.LM = '' ; R.LM = ''
    CALL OPF( FN.LM,F.LM)
    FN.LMM  = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.LMM = '' ; R.LMM = ''
    CALL OPF( FN.LMM,F.LMM)

    T.SEL = "SELECT FBNK.MM.MONEY.MARKET WITH MATURITY.DATE GT 20100930 AND (CATEGORY EQ 21075 OR CATEGORY EQ 21076 OR CATEGORY EQ 21078 OR CATEGORY EQ 21079) BY MIS.ACCT.OFFICER BY CUSTOMER.ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*DEBUG
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LM,KEY.LIST<I>, R.LM, F.LM, ETEXT)
            BRANCH = R.LM<MM.MIS.ACCT.OFFICER>
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH,BRAN.NAME)
            BRAN.NAME = FIELD(BRAN.NAME,'.',2)

            LM.ID  = KEY.LIST<I>
            CUS.ID = R.LM<MM.CUSTOMER.ID>
            CURR   = R.LM<MM.CURRENCY>
            AMT    = R.LM<MM.PRINCIPAL>

            RATE   = R.LM<MM.INTEREST.RATE>
            SPREAD = R.LM<MM.INTEREST.SPREAD.1>
            TOT.INT.AMT   = R.LM<MM.TOT.INTEREST.AMT>
            CAT.ID        = R.LM<MM.CATEGORY>
            VALUE.DATE    = R.LM<MM.VALUE.DATE>
            FIN.MAT.DATE  = R.LM<MM.MATURITY.DATE>

            LM.NEW.ID     = LM.ID:'00'
            CALL F.READ(FN.LMM,LM.NEW.ID, R.LMM, F.LMM, ETEXT)
            COMM.INT = R.LMM<LD27.OUTS.CUR.ACC.I.PAY>

            BB.DATA  = BRAN.NAME:'|'
            BB.DATA := LM.ID:'|'
            BB.DATA := CUS.ID:'|'
            BB.DATA := CURR:'|'
            BB.DATA := AMT:'|'
            BB.DATA := RATE:'|'
            BB.DATA := SPREAD:'|'
            BB.DATA := TOT.INT.AMT:'|'
            BB.DATA := CAT.ID:'|'
            BB.DATA := VALUE.DATE:'|'
            BB.DATA := FIN.MAT.DATE:'|'
            BB.DATA := COMM.INT

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
