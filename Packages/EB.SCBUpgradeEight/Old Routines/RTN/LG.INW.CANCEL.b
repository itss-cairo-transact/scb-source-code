* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-252</Rating>
*-----------------------------------------------------------------------------
********* WAEL ********

    SUBROUTINE LG.INW.CANCEL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

*Line [ 46 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)

    IF MYCODE # '2309' AND MYCODE # '2301' THEN E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'

    IF MYCODE = "2309" THEN
        GOSUB INITIATE

        GOSUB PRINT.HEAD
        GOSUB BODY

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
    IF MYCODE = "2301" THEN
        GOSUB INITIATE
        GOSUB PRINT.HEAD
        GOSUB BODY2
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.INW.CANCEL'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    PURPOSE = LOCAL.REF<1,LDLR.IN.RESPECT.OF>
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE,1>
*    LG.MAT.DATE = R.LD<LD.FIN.MAT.DATE>
    LG.MAT.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    LG.APPROVAL.DATE=LOCAL.REF<1,LDLR.APPROVAL.DATE,1>
    LG.ACCT=LOCAL.REF<1,LDLR.DEBIT.ACCT,1>
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    DATZ= TODAY
    ZZ= DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ= LG.APPROVAL.DATE
    QQ = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]
    APPROVE=LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL1 =LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL2 =LOCAL.REF<1,LDLR.GUARRANTOR,2>

    ADDR1 =LOCAL.REF<1,LDLR.FREE.TEXT,1>
    ADDR2 =LOCAL.REF<1,LDLR.FREE.TEXT,2>
    ADDR3 =LOCAL.REF<1,LDLR.FREE.TEXT,3>

    SEND.REC=LOCAL.REF<1,LDLR.SENDING.REF>


*------------------------------------
    T.SEL.H = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":ID.NEW:"..."
    CALL EB.READLIST(T.SEL.H,KEY.LIST.H,"",SELECTED.H,ERR.MSG.H )

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)
    CALL F.READ(FN.LD.H,KEY.LIST.H<SELECTED>,R.LD.H,F.LD.H,E1.H)

    LOCAL.REF = R.LD.H<LD.LOCAL.REF>
    OPR.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>

    WW = R.LD.H<LD.DATE.TIME>
    D.TIME = WW[1,6]
* WWW = D.TIME[5,2]:'/':D.TIME[3,2]:"/":"20":D.TIME[1,2]
    D.TIME=LG.APPROVAL.DATE
    WWW = D.TIME[7,2]:'/':D.TIME[5,2]:'/':D.TIME[1,4]

*-----------------------------------

    RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":"�����":"*"
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"���� �������� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',22)

    HEADING PR.HD


    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:"-":SEND.REC:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(17): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "������� � �����   : ":SUPPL1
    PRINT SPACE(17): "                  : ":SUPPL2
    PRINT
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT

*=============
    PRINT SPACE(5):" ������ ������� ������ ": WWW :" ����� �������� �� ��� ������ "
    PRINT
    PRINT SPACE(2):" ���� ������ ������ ������ �� ����� ��" :" ": YY :" � ���� �� ����� "
    PRINT
    PRINT SPACE(2):" ���� ��� ������ ���� ����� ����� �� ���� ��� ������."
    PRINT
    PRINT SPACE(2):" � ���� ��� �� ���� ��� ���� ����� ������ ���� ������ ������� "
    PRINT
    PRINT SPACE(2):"� ������ �������  �� �������� ������ ."
    PRINT;PRINT
    TXTY =" ����� �������� ."
    PRINT " ������ ������ ����� �� ���� ��� ���� " :TXTY
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":ZZ

    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*================================================================
BODY2:
    LG.AMT = R.OLD(LD.AMOUNT)
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT
    PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:"-":SEND.REC:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(17): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): "������� � �����   : ":SUPPL1
    PRINT SPACE(17): "                  : ":SUPPL2
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY
    PRINT
*--------------------------------------------------------------------
    PRINT SPACE(17): "________________________________________________"
    PRINT
    PRINT SPACE(5):" ����� ��� ������ ������� ���� ����� ������" :WWW:" ��� �������� "
    PRINT
    PRINT SPACE(5):"������ ���� ���� ������ ���� ������ ������ ������ �� ������� � ������ "
    PRINT
    PRINT SPACE(5):"��������  �� ���������� ������ � ��� ������� ������� ���" :" ":YY
    PRINT
    PRINT SPACE(5):" ��� �� ����� ���� ��� ������ ���� ��� ������ ."
    PRINT
    PRINT SPACE(5):" ������ ������ ����� ����� ������ ������� ��������� �������� ��������. "
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT;PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":ZZ
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==================================================================
