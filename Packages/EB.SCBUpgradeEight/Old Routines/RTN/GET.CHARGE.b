* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
****************************NI7OOOOOOOOOOOOOOO*****************
    SUBROUTINE GET.CHARGE(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE

    FN.CH = 'FBNK.FT.CHARGE.TYPE'
    F.CH  = ''
    CALL OPF (FN.CH,F.CH)
    FN.COM = 'FBNK.FT.COMMISSION.TYPE'
    F.COM  = ''
    CALL OPF (FN.COM,F.COM)

* COMM.AC = R.NEW(FT.COMMISSION.TYPE)

    COMM.AC = ARG
    IF COMM.AC EQ R.NEW(FT.COMMISSION.TYPE) THEN
        CALL F.READ(FN.COM,ARG,R.COM,F.COM,E2)
        COMM.AC.NO = R.COM<FT4.CATEGORY.ACCOUNT>
        COMM.NAME  = R.COM<FT4.DESCRIPTION><1,2>

*CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,COMM.AC,COMM.AC.NO)
*CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,COMM.AC.NO,COMM.NAME)
    END
*   TEXT = "NAME1 =" : COMM.AC.NO
    IF COMM.AC EQ R.NEW(FT.CHARGE.TYPE)<1,1> THEN
         **TEXT = "<1,1>" : R.NEW(FT.CHARGE.TYPE)<1,1> ; CALL REM
        CALL F.READ(FN.CH,ARG,R.CH,F.CH,E2)
        COMM.AC.NO = R.CH<FT5.CATEGORY.ACCOUNT>
        COMM.NAME  = R.CH<FT5.DESCRIPTION><1,2>
* CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,COMM.AC,COMM.AC.NO)
* CALL DBR('FT.CHARGE.TYPE':@FM:FT5.SHORT.DESCR,COMM.AC.NO,COMM.NAME)
    END
*** ARG = COMM.AC.NO
    ARG = COMM.NAME
    RETURN
END
