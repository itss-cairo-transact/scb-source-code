* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-271</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.CHEQUEREQUEST.MAD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    FOR II = 1 TO 2
        GOSUB INITIATE
*Line [ 46 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        GOSUB CALLDB
        GOSUB PRINT.HEAD
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    NEXT II
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CHEQUEREQUEST.MAD'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

****************************************************************
    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END


    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    OLD.NO   =LOCAL.REF<1,LDLR.OLD.NO>
    CHEQ.DAY=LOCAL.REF<1,LDLR.LG.ADV.CHEQ.DAY>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME.2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    LG.MATURITY= R.LD<LD.FIN.MAT.DATE>
    LG.MATURITY22 = LOCAL.REF<1,LDLR.NEW.CHQ.DATE>
    LG.MATURITY2  = LG.MATURITY22[7,2]:'/':LG.MATURITY22[5,2]:"/":LG.MATURITY22[1,4]
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    MYDATE=XX
    DAT.TO=TODAY
    DAT.TO = DAT.TO[7,2]:'/':DAT.TO[5,2]:"/":DAT.TO[1,4]
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
*************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>

    RETURN
*===============================================================
PRINT.HEAD:

** CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD  ="'L'":SPACE(1):"����������":":":MYBRANCH
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT SPACE(20): "���� �������� ��� � �: ":OLD.NO:" (":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(20): "������ ��� ������ : ":XX
    PRINT
*//////////////////////////////////////////////////////
* IF MYCODE[2,3]= "111" THEN
*    PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
* END ELSE
    PRINT SPACE(20): "����������� ����� : ":THIRD.NAME
    IF THIRD.NAME.2 THEN
        PRINT SPACE(5):THIRD.NAME.2
    END ELSE
        PRINT SPACE(5):""
    END
* END
*//////////////////////////////////////////////////////
    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT
*  IF MYCODE EQ '1281' OR MYCODE EQ '1283' THEN
*      PRINT SPACE(5):"�������� ��� ���� ������ ������ ������ ������ �������� ��� ��� ������"
*      PRINT
*      PRINT SPACE(5):"�� ���� ���� ������ ������� �������� ������ ."
*      PRINT
*      PRINT SPACE(5):"����� ������ �������� ������ ������� ��� ����� ��� ����� ����� ������"
*      PRINT
*      PRINT SPACE(5):"������ ���� ���� ."
* END ELSE
*    IF MYCODE EQ '1271' OR MYCODE EQ '1281' OR MYCODE EQ '1284' OR MYCODE EQ '1283' OR MYCODE EQ '1288' OR MAYCODE EQ '1111' THEN
    IF MYCODE EQ '1111' OR MYCODE EQ '1281' OR MYCODE EQ '1282' THEN
        PRINT SPACE(5):"�������� ��� ���� ������ ������ ������ ����� ��� ��� �������  ,����� ��� "
        PRINT
        PRINT SPACE(5):"��� ������ ��� ������ �������� ��� ��������� ���� ������ ������� �������� �����"
        PRINT
        PRINT SPACE(5):"���� ������ ������� ���"  : LG.MATURITY2
        PRINT
        IN.AMOUNT = LG.MATURITY2
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
**PRINT SPACE(5):OUT.AMOUNT
        PRINT
        PRINT SPACE(5):"���� �� ���� ���� ������ �������� ������ ��� �� ���� �����"
        PRINT
        PRINT SPACE(5):"����� ������ ���� �������� ������ ������� �� ����� ������� ���� ����"
        PRINT
        PRINT SPACE(5):" �� �� ��� ����� ����� ������� �� ���� ����� ������� ���� ������ ������ ���� ������ ��� ������"
        PRINT
        PRINT SPACE(5):" ��� ����� ���� �� �� ����� ���"
*  END
*  END
        PRINT ; PRINT
        PRINT SPACE(25):"� ������ ����� ���� ��������  ���"  ;PRINT ;PRINT
        IF II = '1' THEN
            PRINT SPACE(5): "���� ��� : ":THIRD.NAME
            IF THIRD.NAME.2 THEN
                PRINT SPACE(5):THIRD.NAME.2
            END ELSE
                PRINT SPACE(5):""
            END
            PRINT SPACE(15):THIRD.ADDR1
            PRINT SPACE(15):"����� ������ ��������"
        END  ELSE
            PRINT;PRINT;PRINT;PRINT
        END
        PRINT SPACE(50):"��� ��� ���� ������"
        OPER.CODE='LG.CHEQUEREQUEST.MAD'
        PRINT SPACE(1):OPER.CODE
*Line [ 226 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeEight.LG.ADD(AUTH,LG.CO)
        TEXT=DAT.TO:'-DAT.TO';CALL REM
        PRINT SPACE(5):DAT.TO
        TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    END
    RETURN
END
*==================================================================
