* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*========================================================
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM ISCO.FIX.CLOSE.ACCT
    SUBROUTINE ISCO.FIX.CLOSE.ACCT
*========================================================
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT TEMENOS.BP I_AC.LOCAL.REFS
    $INSERT TEMENOS.BP I_F.SCB.ISCO.CF
*========================================================
    FN.CF   = 'F.SCB.ISCO.CF'
    F.CF    = ''
    CALL OPF(FN.CF,F.CF)

    SELECTED = ''; ASD = ''

    T.DATE   = TODAY
    TT.DATE  = T.DATE[1,6]
    MONTH    = T.DATE[5,2]
    YEAR     = T.DATE[1,4]

    IF MONTH EQ 1 THEN
        NEW.MONTH     = 12
        YEAR          = YEAR - 1
    END
    ELSE
        NEW.MONTH     = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END

    FINAL.DATE = YEAR: NEW.MONTH

    T.SEL  = "SELECT F.SCB.ISCO.CF WITH CF.ACCT.BALANCE NE 0 AND APPROVAL.DATE NE '' AND APPROVAL.AMOUNT EQ 0 AND (@ID LIKE ...":FINAL.DATE:" OR @ID LIKE ...":FINAL.DATE:"LD OR @ID LIKE ...":FINAL.DATE:"LC OR @ID LIKE ...":FINAL.DATE:".A)"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    FOR I = 1 TO SELECTED

        R.UN = ''; R.CF1 = ''; R.CF2 = ''; R.CF3 = ''; R.CF4 = '';
        ETEXT = ''; ER1 = ''; ER2 = ''; ER3 = ''; ER4 = ''

        CALL F.READ( FN.CF,KEY.LIST<I>, R.CF, F.CF, ETEXT)

        R.CF<ISCO.CF.ASSET.CLASS> = R.CF<ISCO.CF.ASSET.CLASS>[1,2]:'1'

        R.CF<ISCO.CF.APPROVAL.AMOUNT>  = R.CF<ISCO.CF.CF.ACCT.BALANCE>
        R.CF<ISCO.CF.NDPD> = 0
        R.CF<ISCO.CF.AMT.OVERDUE> = 0
        ACCT.ID = KEY.LIST<I>

        CALL F.WRITE(FN.CF,ACCT.ID,R.CF)
        CALL JOURNAL.UPDATE(ACCT.ID)



    NEXT I

*========================================================

END
