* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*    PROGRAM ISCO.FILL.TABLES
    SUBROUTINE ISCO.FILL.TABLES

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.CF
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.CS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.UNKNOWN.CASES

    EOF = ''
*=========== OPEN THE CS AND CF TABLES==================
    F.CF = "F.SCB.ISCO.CF"; FVAR.CF=''; R.CF = ''; FN.CF = ''
    F.CS = "F.SCB.ISCO.CS"; FVAR.CS=''; R.CS = ''; FN.CS = ''
    R.CFF = '' ; R.CSS = ''

    CALL OPF(F.CF,FVAR.CF)
    CALL OPF(F.CS,FVAR.CS)
*====================== OPEN THE TABLES ===========================
    FN.CU  = 'FBNK.CUSTOMER'              ; F.CU = ''      ; R.CU = ''
    CALL OPF( FN.CU,F.CU)

    FN.ACC  = 'FBNK.ACCOUNT'              ; F.ACC = ''     ; R.ACC = ''
    CALL OPF( FN.ACC,F.ACC)

    FN.LMT  = 'FBNK.LIMIT'                ; F.LMT = ''     ; R.LMT = ''
    CALL OPF( FN.LMT,F.LMT)

    FN.CUS.ACC  = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = ''
    CALL OPF( FN.CUS.ACC,F.CUS.ACC)

    FN.CBE  = 'F.SCB.CREDIT.CBE'          ; F.CBE = ''     ; R.CBE = ''; R.CBE2 = ''
    CALL OPF( FN.CBE ,F.CBE)

    FN.UN   = 'F.SCB.ISCO.UNKNOWN.CASES'  ; F.UN    = ''   ; R.UN = ''
    CALL OPF(FN.UN,F.UN)

    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS'  ; F.LD    = ''   ; R.LD = ''
    CALL OPF(FN.LD,F.LD)

    R.FOLD = ''

    Y.CUS.ACC = ''; R.FILL.CF    = ''; ID.CF = ''
    R.FILL.CS = ''; R.FILL.Close = ''; R.FILL.CL.S = ''
    ID.CS = '';     TYPE  = ''
    R.CFT = ''
    MONTH = 0;      YEAR = 0;       NEW.MONTH = 0
    NDPD  = 0

*================= LAST WORKING DATE ===================================
    LST.WRK.D = ''
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,"EG0010099",LST.WRK.D)
*=================READ FROM CUTOMER WITH PERSONS CODE ==================
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (SECTOR EQ '2000' OR SECTOR EQ '1100'"
    T.SEL := " OR SECTOR EQ '1200' OR SECTOR EQ '1300' OR SECTOR EQ '1400')"
    T.SEL := " AND (POSTING.RESTRICT LT 90)"
    T.SEL := " AND (ACTIVE.CUST EQ 'YES') BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT SELECTED
*=================READ FROM SCB.CREDIT.SBE TABLE FOR LD =====================

    Y.SEL  = "SELECT F.SCB.CREDIT.CBE WITH (TOT.US.12 NE 0 OR TOT.CR.12 NE 0)"
    Y.SEL := " AND (TOT.US.12 NE '' AND TOT.CR.12 NE '')"
    CALL EB.READLIST(Y.SEL, KEY.LIST2, "", SELECTED2, ASD2)

*=================READ FROM SCB.CREDIT.SBE TABLE FOR LC =====================

    Z.SEL  = "SELECT F.SCB.CREDIT.CBE WITH (TOT.US.11 NE 0 OR TOT.CR.11 NE 0)"
    Z.SEL := " AND (TOT.US.11 NE '' AND TOT.CR.11 NE '')"
    CALL EB.READLIST(Z.SEL, KEY.LIST3, "", SELECTED3, ASD3)

*================ DATE FOR  THE HIGHEST CREDIT ===================================
    MONTH.TODAY = TODAY[5,2]
    YEAR.TODAY  = TODAY[1,4]
    DAY.TODAY   = TODAY[7,2]
    H.YEAR      = YEAR.TODAY - 1
    H.DATE      = H.YEAR:MONTH.TODAY:DAY.TODAY

*========================== SOME GLOBAL VARIABLES ===================

    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    MONTH.B4   = MONTH
    YEAR       = TODAY.DATE[1,4]
    YEAR.B4 = YEAR
    IF MONTH EQ 1 THEN

        NEW.MONTH     = 12
        NEW.MONTH.B4  = 11
        YEAR          = YEAR - 1
        YEAR.B4 = YEAR
    END
    ELSE
        NEW.MONTH     = MONTH - 1
        NEW.MONTH.B4  = MONTH.B4 - 2
        IF NEW.MONTH.B4 EQ 0 THEN
            NEW.MONTH.B4 = 12
            YEAR.B4 = YEAR - 1
        END
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
    IF LEN(NEW.MONTH.B4) EQ 1 THEN
        NEW.MONTH.B4 = '0':NEW.MONTH.B4
    END

    CLOSED.ACCT.DATE = YEAR.B4:NEW.MONTH.B4
    FINAL.DATE = YEAR:NEW.MONTH

*=================READ FROM SCB.ISCO.CF TABLE FOR CLOSED ACCOUNTS =================

    Q.SEL  = "SELECT F.SCB.ISCO.CF WITH ACC.STATUS NE 90 AND (@ID LIKE ...":CLOSED.ACCT.DATE:" OR @ID LIKE ...":CLOSED.ACCT.DATE:"LC OR @ID LIKE ...":CLOSED.ACCT.DATE:"LD OR @ID LIKE ...":CLOSED.ACCT.DATE:"L) AND APPROVAL.DATE NE ''"

    CALL EB.READLIST(Q.SEL, KEY.LIST13, "", SELECTED13, ASD13)

*========================= GET DATA OF CREDIT =======================

    FOR I = 1 TO SELECTED
        TOT.MRG.AMT = 0
        HIGH = ''
        TYPE = 'MAIN'
        OLD.ACCT.NEW = ''
        OLD.APP.ID   = ''
        AMT_OVER = 0
        BAL.AMT  = 0
        NDPD = 0
        R.FOLD = ''
        R.FILL.CF = ''
        CRT @(30,3) KEY.LIST<I>:' ':I:' OF ':SELECTED
        CALL F.READ( FN.CU,KEY.LIST<I>, R.CU, F.CU, ETEXT)
        CUS.ID =  KEY.LIST<I>
        CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,Y.CUS.ACC.ERR)
        LOOP
            REMOVE Y.CUS.ACC.ID FROM R.CUS.ACC SETTING SUBVAL
        WHILE Y.CUS.ACC.ID:SUBVAL

*=========READ FROM ACCOUNT THE DEPIT CATEG ================
            CALL F.READ( FN.ACC,Y.CUS.ACC.ID,R.ACC, F.ACC, ETEXT1)
            IF (R.ACC<AC.CATEGORY> GE '1100' AND R.ACC<AC.CATEGORY> LE '1599' AND R.ACC<AC.CATEGORY> NE '1407' AND  R.ACC<AC.CATEGORY> NE '1445' AND  R.ACC<AC.CATEGORY> NE '1455' AND R.ACC<AC.CATEGORY> NE '1413' AND  R.ACC<AC.CATEGORY> NE '1408' AND R.ACC<AC.OPEN.ACTUAL.BAL> NE '') OR ( R.ACC<AC.CATEGORY> EQ '1408' AND R.ACC<AC.OPEN.ACTUAL.BAL> NE 0 AND R.ACC<AC.OPEN.ACTUAL.BAL> NE '') THEN
                LMT.REF =''
                LMT.REF1=FIELD(R.ACC<AC.LIMIT.REF>,'.',1,1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    LMT.REF = R.ACC<AC.CUSTOMER>:'.':'0000':R.ACC<AC.LIMIT.REF>
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    LMT.REF = R.ACC<AC.CUSTOMER>:'.':'000':R.ACC<AC.LIMIT.REF>
                END
* ======================================== ==================
                CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT2)
                IF R.LMT<LI.MAXIMUM.TOTAL> EQ '' THEN
                    R.LMT<LI.EXPIRY.DATE> = "20001010"
                END
*============================================================
                IF R.LMT<LI.MAXIMUM.TOTAL> EQ 0 THEN
                    R.LMT<LI.MAXIMUM.TOTAL> = 1
                END
*============================================================
                BAL.AMOUNT = R.ACC<AC.OPEN.ACTUAL.BAL>
                IF R.ACC<AC.OPEN.ACTUAL.BAL> LT 0 THEN
                    BAL.AMOUNT = R.ACC<AC.OPEN.ACTUAL.BAL> * -1
                END
                BAL.AMT    = DROUND(BAL.AMOUNT,0)
                AMT_OVER = 0
                IF (R.ACC<AC.OPEN.ACTUAL.BAL> * -1) GT R.LMT<LI.MAXIMUM.TOTAL> THEN
                    AMT_OVER = (R.ACC<AC.OPEN.ACTUAL.BAL> * -1) - R.LMT<LI.MAXIMUM.TOTAL>
                END
* =========================================================
                DAYS = 'C'
                DDD = R.LMT<LI.EXPIRY.DATE>[1,8]
                IF R.LMT<LI.EXPIRY.DATE>[1,8] LT TODAY THEN
                    DAYS = 'C'
                    CALL CDD("",DDD,TODAY,DAYS)
                    NDPD = DAYS
                    IF NDPD GT 999 THEN NDPD = 999
                END ELSE
                    NDPD = 0
                END
                NEW.APP.DATE =   R.LMT<LI.APPROVAL.DATE>
*=============== CUSTOMER MOGANAB          ==================
                IF R.ACC<AC.CATEGORY> GE '1220' AND R.ACC<AC.CATEGORY> LE '1227' THEN
                    NEW.APP.DATE = R.ACC<AC.LOCAL.REF><1,ACLR.APPROVAL.DATE>
                    R.LMT<LI.MAXIMUM.TOTAL> = R.ACC<AC.OPEN.ACTUAL.BAL> * -1
                    AMT_OVER = (R.ACC<AC.OPEN.ACTUAL.BAL> * -1)
                    NDPD = 999
                    COMP.ACCT = R.ACC<AC.INTEREST.LIQU.ACCT>
                    INT.AMT = 0
                    INT.AMT.OLD = 0
                    BAL.AMT     = 0

                    COMP.ACCT.OLD = COMP.ACCT[1,10]:"9090":COMP.ACCT[15,2]
                    CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,COMP.ACCT,INT.AMT)
                    CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,COMP.ACCT.OLD,INT.AMT.OLD)
                    BAL.AMT = R.ACC<AC.OPEN.ACTUAL.BAL>
                    TOT.MRG.AMT =  INT.AMT + INT.AMT.OLD

                    BAL.AMT = DROUND(BAL.AMT,0)
                    TOT.MRG.AMT = DROUND(TOT.MRG.AMT,0)

                    IF BAL.AMT LT 0 THEN
                        BAL.AMT = DROUND(BAL.AMT,0) * -1
                    END
                    IF TOT.MRG.AMT LT 0 THEN
                        TOT.MRG.AMT = DROUND(TOT.MRG.AMT,0) * -1
                    END

                END

                TERM.OF.LOAN = '12'
*=============== BORROW CUSTONERS ===========================
                IF R.ACC<AC.CATEGORY> EQ '1408' THEN

                    ACCT.OFF = R.ACC<AC.CO.CODE>
                    ACCT.OFFICER = ACCT.OFF[8,2]
                    NEW.APP.DATE = R.ACC<AC.LOCAL.REF><1,ACLR.APPROVAL.DATE>
                    AMT_OVER     = 0
                    NDPD         = 0
                    TERM.OF.LOAN = '48'
                    IF LEN(CUS.ID) NE 8 THEN
                        CUS.BORROW = '0':CUS.ID
                    END ELSE
                        CUS.BORROW = CUS.ID
                    END

                    BORROW.1407  = CUS.BORROW:'10140701'
                    BORROW.1413  = CUS.BORROW:'10141301'
                    BORROW.1445  = CUS.BORROW:'10144501'
                    BORROW.1455  = CUS.BORROW:'10145501'

                    CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,BORROW.1407,AMT.1407)
                    CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,BORROW.1413,AMT.1413)

                    CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,BORROW.1445,AMT.1445)
                    CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,BORROW.1455,AMT.1455)

                    IF AMT.1407 LT 0 THEN
                        AMT.1407 =  DROUND(AMT.1407,0) * -1
                    END
                    IF AMT.1413 LT 0 THEN
                        AMT.1413 =  DROUND(AMT.1413,0) * -1
                    END
                    IF AMT.1445 LT 0 THEN
                        AMT.1445 =  DROUND(AMT.1445,0) * -1
                    END
                    IF AMT.1455 LT 0 THEN
                        AMT.1455 =  DROUND(AMT.1455,0) * -1
                    END
                    BAL.AMT = BAL.AMT + AMT.1407 + AMT.1413 + AMT.1445 + AMT.1455
                    R.LMT<LI.MAXIMUM.TOTAL>  = BAL.AMT
                END
*=============== REPAYMENT AND TERM OF LOAN VALUES===========
                HIGH = ''
                REPAY.TYPE   = ''
                IF R.ACC<AC.CATEGORY> EQ '1501' OR R.ACC<AC.CATEGORY> EQ '1508' OR R.ACC<AC.CATEGORY> EQ '1588' OR R.ACC<AC.CATEGORY> EQ '1518' OR R.ACC<AC.CATEGORY> EQ '1503' OR R.ACC<AC.CATEGORY> EQ '1222' OR R.ACC<AC.CATEGORY> EQ '1599' OR R.ACC<AC.CATEGORY> EQ '1205' OR R.ACC<AC.CATEGORY> EQ '1206' OR R.ACC<AC.CATEGORY> EQ '1207' OR R.ACC<AC.CATEGORY> EQ '1208' THEN
                    TERM.OF.LOAN = ''
                END
* ELSE
*     TERM.OF.LOAN = '12'
* END
*==============creat erecord to fil in the CF table==========================

                IF NDPD EQ 0 THEN
                    AMT_OVER = 0
                END
                IF AMT_OVER EQ 0 THEN
                    NDPD = 0
                END
                IF AMT_OVER LT 0 THEN
                    AMT_OVER = AMT_OVER * -1
                END
                AMT_OVER = DROUND(AMT_OVER,0)
                IF R.LMT<LI.MAXIMUM.TOTAL> LT 0 THEN
                    R.LMT<LI.MAXIMUM.TOTAL> = R.LMT<LI.MAXIMUM.TOTAL> * -1
                END
                APP.DATE = NEW.APP.DATE
*=============== IGNORE NDPD/AMT.OVER FOR SOME CATEG. ==

                CATEG.LINE  = '(1206 1208 1401 1402 1405 1406 1415 1417 1418 1419 1420 1421 1477 1480 1481 1483 1484 1485)'

                WS.CATEG  = R.ACC<AC.CATEGORY>
                FINDSTR WS.CATEG IN CATEG.LINE SETTING POS THEN
                    AMT_OVER = 0
                    NDPD = 0
                END
*=============== CREATE AMENDMENT DATE=================================

                OLD.APP.ID = Y.CUS.ACC.ID:YEAR:NEW.MONTH.B4
                R.FOLD= ''
                CALL F.READ(F.CF,OLD.APP.ID, R.FOLD, FN.CF,OLD.ERR)

                R.FILL.CF<ISCO.CF.APPROVAL.DATE> = APP.DATE
                R.FILL.CF<ISCO.CF.CF.AMENDMENT.DATE> = ''

                IF R.FOLD NE '' AND R.FOLD<ISCO.CF.APPROVAL.DATE> NE '' THEN
                    AA= R.FOLD<ISCO.CF.APPROVAL.AMOUNT>
                    BB= R.LMT<LI.MAXIMUM.TOTAL>
                    IF R.FOLD<ISCO.CF.APPROVAL.AMOUNT> NE R.LMT<LI.MAXIMUM.TOTAL> THEN
                        AMED = '20':R.LMT<LI.DATE.TIME,1>[1,6]
                        IF AMED EQ '20' THEN AMED = ''
                        AMED.DATE = AMED
                        R.FILL.CF<ISCO.CF.CF.AMENDMENT.DATE> = AMED.DATE
                    END
                    OLD.APP.DATE.N = R.FOLD<ISCO.CF.APPROVAL.DATE>
                END

*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
                COMP.CODE = R.CU<EB.CUS.ACCOUNT.OFFICER>
                IF COMP.CODE EQ '11' THEN
                    ISLAMIC.IND = '001'
                END
                ELSE
                    ISLAMIC.IND = '002'
                END
****************************************
                CATT = R.ACC<AC.CATEGORY>
                IF(CATT EQ 1206 OR CATT EQ 1208 OR CATT EQ 1417 OR CATT EQ 1419 OR CATT EQ 1421 OR CATT EQ 1484 OR CATT EQ 1485 OR CATT EQ 1401 OR CATT EQ 1402 OR CATT EQ 1405 OR CATT EQ 1406 OR CATT EQ 1415 OR CATT EQ 1480 OR CATT EQ 1481 OR CATT EQ 1483 OR CATT EQ 1493 OR CATT EQ 1499 OR CATT EQ 1218 OR CATT EQ 21065) THEN
                    SECURED.IND = '001'
                END
                ELSE
                    SECURED.IND = '003'
                END
**************************************************

                R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
                R.FILL.CF<ISCO.CF.CF.ACC.NO>           = Y.CUS.ACC.ID
                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(R.LMT<LI.MAXIMUM.TOTAL>,0)
                R.FILL.CF<ISCO.CF.HIGHEST.CREDIT>      = HIGH
                R.FILL.CF<ISCO.CF.CURRENCY>            = Y.CUS.ACC.ID[9,2]
                R.FILL.CF<ISCO.CF.LIABILITY.IND>       = '001'
                R.FILL.CF<ISCO.CF.CF.TYPE>             = R.ACC<AC.CATEGORY>
                R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = TERM.OF.LOAN
                R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = REPAY.TYPE
                R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = BAL.AMT
                R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = AMT_OVER
                R.FILL.CF<ISCO.CF.NDPD>                = NDPD
                R.FILL.CF<ISCO.CF.ASSET.CLASS>         = '000'
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = SECURED.IND
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
                R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = TOT.MRG.AMT
                R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
                R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
                R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
                R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
                R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY

                IF R.ACC<AC.POSTING.RESTRICT> EQ '' THEN
                    R.FILL.CF<ISCO.CF.ACC.STATUS> = '3'
                END ELSE
                    R.FILL.CF<ISCO.CF.ACC.STATUS> = 'a'
                END
                ID.CF = Y.CUS.ACC.ID:YEAR:NEW.MONTH
                CRT @(40,5) ID.CF
                CALL F.WRITE (F.CF,ID.CF,R.FILL.CF)
                CALL JOURNAL.UPDATE(ID.CF)
*===================create record to fil in the CS table==========
                IF TYPE EQ 'MAIN' THEN
                    R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
                    R.FILL.CS<ISCO.CS.CF.ACC.NO>           = Y.CUS.ACC.ID
                    R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= CUS.ID
                    ID.CS = Y.CUS.ACC.ID:YEAR:NEW.MONTH
                END

                R.FILL.CS<ISCO.CS.NATIONAL.ID>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
                R.FILL.CS<ISCO.CS.CITZENSHIP>          = R.CU<EB.CUS.NATIONALITY>
                CITZENSHIP                             = R.FILL.CS<ISCO.CS.CITZENSHIP>

                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO> THEN
                    IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                    ISSUE.AUTHORITY   = '031'
                END
                ELSE
                    IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
                    ISSUE.AUTHORITY   = R.CU<EB.CUS.LOCAL.REF><1,CULR.PLACE.ID.ISSUE>
                END
                IF CITZENSHIP EQ 'EG' AND ISSUE.AUTHORITY NE '031' THEN
                    R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = ''
                    R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
                END
                ELSE
                    R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = IDENT.CODE
                    R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ISSUE.AUTHORITY
                END
                R.FILL.CS<ISCO.CS.CS.A.LAST.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> EQ 98 THEN
                    R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '002'
                END
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> NE 98 THEN
                    R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '001'
                END
                R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
                R.FILL.CS<ISCO.CS.ADD.1.GOV.E>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                R.FILL.CS<ISCO.CS.COUNTRY.1>        = R.CU<EB.CUS.RESIDENCE>
                LOCAL.REF1                          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 440 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
                IF NO_OF_TEL EQ 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                IF NO_OF_TEL GT 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                    R.FILL.CS<ISCO.CS.MOBILE>= R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>
                END
                R.FILL.CS<ISCO.CS.BIRTH.DATE>  = R.CU<EB.CUS.BIRTH.INCORP.DATE>
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'M-���'  THEN
                    R.FILL.CS<ISCO.CS.GENDER>  = '001':
                END
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'F-����' THEN
                    R.FILL.CS<ISCO.CS.GENDER>  = '002'
                END
                R.FILL.CS<ISCO.CS.CITZENSHIP>= R.CU<EB.CUS.NATIONALITY>
                R.FILL.CS<ISCO.CS.RECORD.STATUS>       = 'INAU'
                R.FILL.CS<ISCO.CS.CURR.NO>             = '1'
                R.FILL.CS<ISCO.CS.INPUTTER>            = 'ISCORE'
                R.FILL.CS<ISCO.CS.CO.CODE>             = COMP

                IF R.CU<EB.CUS.SECTOR> EQ '1100' OR R.CU<EB.CUS.SECTOR> EQ '1300' THEN
                    R.FILL.CS<ISCO.CS.OCCUPATION> = '8'
                END ELSE
                    R.FILL.CS<ISCO.CS.OCCUPATION>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.PROFESSION>
                END

                CALL F.WRITE (F.CS,ID.CS,R.FILL.CS)
                CALL JOURNAL.UPDATE(ID.CS)
            END

        REPEAT
    NEXT I
*=======================GET DATA OF LD ===========================
    IF SELECTED2 THEN
        FOR J = 1 TO SELECTED2
            HIGH = 0
            TYPE = 'LD'
            R.CU = ''
            R.FILL.CF = ''
            OLD.ACCT.NEW = ''
            OLD.APP.ID   = ''
            R.FOLD = ''
            NDPD = 0
            NEW.APP.DATE = ''
*====================== NEW
            LD.CUS.ID   = KEY.LIST2<J>
            CALL F.READ( FN.CU,LD.CUS.ID, R.CU, F.CU, ETEXT)
            SECTOR   = R.CU<EB.CUS.SECTOR>
            POS.REST = R.CU<EB.CUS.POSTING.RESTRICT>
            ACTIVE   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ACTIVE.CUST>

*==================================================================
            IF (SECTOR EQ '1100' OR SECTOR EQ '1200' OR SECTOR EQ '1300' OR SECTOR EQ '1400' OR SECTOR EQ '2000') AND (POS.REST NE 90 AND POS.REST NE 99) AND ACTIVE EQ 'YES' THEN
                CRT @(30,3) KEY.LIST2<J>:' ':J:' OF ':SELECTED2
                CALL F.READ(FN.CBE,KEY.LIST2<J>, R.CBE, F.CBE, ETEXT3)
                LD.CUS.ID   = KEY.LIST2<J>
                MAX.TOTAL   = R.CBE<SCB.C.CBE.TOT.CR.12,1>
                OPN.BALANCE = R.CBE<SCB.C.CBE.TOT.US.12,1>
                CBE.NO      = R.CBE<SCB.C.CBE.CBE.NO,1>
                LMT.REF     = LD.CUS.ID:'.0002500.':'01'
                LD.ACCT     = LD.CUS.ID:'102109601'
* =================================================================
                FOR LM.R = 1 TO 9

                    LMT.REF     = LD.CUS.ID:'.0002500.0':LM.R
                    CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT4)
                    IF R.LMT<LI.APPROVAL.DATE> NE '' THEN
                        LM.R = 10
                    END
                NEXT LM.R


                IF MAX.TOTAL EQ '' THEN
                    R.LMT<LI.EXPIRY.DATE> = "20001010"
                END
*==================================================================
                IF OPN.BALANCE EQ '' THEN
                    OPN.BALANCE = 0
                END

*==================================================================
                AMT_OVER       = 0
                IF OPN.BALANCE GT MAX.TOTAL THEN
                    AMT_OVER   = OPN.BALANCE - MAX.TOTAL
                END
                CALL F.READ( FN.CU,LD.CUS.ID, R.CU, F.CU, ETEXT5)

* =================================================================
                DAYS = 'C'
                DDD = R.LMT<LI.EXPIRY.DATE>[1,8]
                IF DDD EQ '' THEN
                    DDD = "20100101"
                END

                IF R.LMT<LI.EXPIRY.DATE>[1,8] LT TODAY THEN

                    CALL CDD("",DDD,TODAY,DAYS)
                    NDPD = DAYS
                    IF NDPD GT 999 THEN NDPD = 999
                END ELSE
                    NDPD = 0
                END
                NEW.APP.DATE =   R.LMT<LI.APPROVAL.DATE>
*=============== REPAYMENT AND TERM OF LOAN VALUES===========
                LD.ACC.CAT = LD.ACCT[10,4]
                REPAY.TYPE   = '007'
                TERM.OF.LOAN = '12'
                HIGH         = ''
*====================creat erecord to fil in the CF table==========

                IF NDPD EQ 0 THEN
                    AMT_OVER = 0
                END
                IF AMT_OVER EQ 0 THEN   ;*AHM
                    NDPD = 0
                END
                IF AMT_OVER LT 0 THEN
                    AMT_OVER = AMT_OVER * -1
                END
                AMT_OVER = DROUND(AMT_OVER,0)

                IF OPN.BALANCE LT 0 THEN
                    BAL.AMOUNT = OPN.BALANCE * -1
                    BAL.AMT   = DROUND(BAL.AMOUNT,0)
                END
                ELSE
                    BAL.AMOUNT = OPN.BALANCE
                    BAL.AMT    = DROUND(BAL.AMOUNT,0)
                END
                APP.DATE = NEW.APP.DATE
*-----------------------------------------
                OLD.APP.ID = LD.ACCT:YEAR:NEW.MONTH.B4:"LD"
                R.FOLD = ''
                CALL F.READ(F.CF,OLD.APP.ID, R.FOLD, FN.CF,OLD.ERR)

                R.FILL.CF<ISCO.CF.APPROVAL.DATE> = APP.DATE
                R.FILL.CF<ISCO.CF.CF.AMENDMENT.DATE> = ''
                IF R.FOLD NE '' THEN
                    IF R.FOLD<ISCO.CF.APPROVAL.AMOUNT> NE MAX.TOTAL THEN
                        AMED = '20':R.LMT<LI.DATE.TIME,1>[1,6]
                        IF AMED EQ '20' THEN AMED = ''
                        AMED.DATE = AMED
                        R.FILL.CF<ISCO.CF.CF.AMENDMENT.DATE> = AMED.DATE
                    END
                    OLD.APP.DATE.N = R.FOLD<ISCO.CF.APPROVAL.DATE>
                END

*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
                COMP.CODE = R.CU<EB.CUS.ACCOUNT.OFFICER>
                IF COMP.CODE EQ '11' THEN
                    ISLAMIC.IND = '001'
                END
                ELSE
                    ISLAMIC.IND = '002'
                END
**************************************************

                R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
                R.FILL.CF<ISCO.CF.CF.ACC.NO>           = LD.ACCT
                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(MAX.TOTAL * 1000,0)
                R.FILL.CF<ISCO.CF.HIGHEST.CREDIT>      = HIGH
                R.FILL.CF<ISCO.CF.CURRENCY>            = '10'
                R.FILL.CF<ISCO.CF.LIABILITY.IND>       = '001'
                R.FILL.CF<ISCO.CF.CF.TYPE>             = '21096'
                R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = TERM.OF.LOAN
                R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = REPAY.TYPE
                R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = BAL.AMT * 1000
                R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = 0
                R.FILL.CF<ISCO.CF.NDPD>                = 0
                R.FILL.CF<ISCO.CF.ASSET.CLASS>         ='000'
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = '002'
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
                R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = 0
                R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
                R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
                R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
                R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
                R.FILL.CF<ISCO.CF.ACC.STATUS>          = '3'
                R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY

                ID.CF = LD.ACCT:YEAR:NEW.MONTH:"LD"
                CALL F.WRITE (F.CF,ID.CF,R.FILL.CF)
                CALL JOURNAL.UPDATE(ID.CF)
*=============== creat erecord to fil in the CS table================

                IF TYPE EQ 'LD'  THEN
                    R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
                    R.FILL.CS<ISCO.CS.CF.ACC.NO>           = LD.ACCT
                    R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= LD.CUS.ID
                    ID.CS = LD.ACCT:YEAR:NEW.MONTH:"LD"
                END

                R.FILL.CS<ISCO.CS.NATIONAL.ID>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
                R.FILL.CS<ISCO.CS.CITZENSHIP>          = R.CU<EB.CUS.NATIONALITY>
                CITZENSHIP                             = R.FILL.CS<ISCO.CS.CITZENSHIP>

                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO> THEN

                    IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                    ISSUE.AUTHORITY   = '031'
                END
                ELSE
                    IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
                    ISSUE.AUTHORITY   = R.CU<EB.CUS.LOCAL.REF><1,CULR.PLACE.ID.ISSUE>
                END

                IF CITZENSHIP EQ 'EG' AND ISSUE.AUTHORITY NE '031' THEN
                    R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = ''
                    R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
                END
                ELSE
                    R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = IDENT.CODE
                    R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ISSUE.AUTHORITY

                END
                R.FILL.CS<ISCO.CS.CS.A.LAST.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> EQ 98 THEN
                    R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '002'
                END
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> NE 98 THEN
                    R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '001'
                END


                R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
                R.FILL.CS<ISCO.CS.ADD.1.GOV.E>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                R.FILL.CS<ISCO.CS.COUNTRY.1>        = R.CU<EB.CUS.RESIDENCE>
                LOCAL.REF1                          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 672 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
                IF NO_OF_TEL EQ 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                IF NO_OF_TEL GT 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                    R.FILL.CS<ISCO.CS.MOBILE>= R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>
                END
                R.FILL.CS<ISCO.CS.BIRTH.DATE>  = R.CU<EB.CUS.BIRTH.INCORP.DATE>
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'M-���'  THEN
                    R.FILL.CS<ISCO.CS.GENDER>  = '001':
                END
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'F-����' THEN
                    R.FILL.CS<ISCO.CS.GENDER>  = '002'
                END
                R.FILL.CS<ISCO.CS.CITZENSHIP>= R.CU<EB.CUS.NATIONALITY>
                R.FILL.CS<ISCO.CS.RECORD.STATUS>       = 'INAU'
                R.FILL.CS<ISCO.CS.CURR.NO>             = '1'
                R.FILL.CS<ISCO.CS.INPUTTER>            = 'ISCORE'
                R.FILL.CS<ISCO.CS.CO.CODE>             = COMP

                IF R.CU<EB.CUS.SECTOR> EQ '1100' OR R.CU<EB.CUS.SECTOR> EQ '1300' THEN
                    R.FILL.CS<ISCO.CS.OCCUPATION> = '8'
                END ELSE
                    R.FILL.CS<ISCO.CS.OCCUPATION> = R.CU<EB.CUS.LOCAL.REF><1,CULR.PROFESSION>
                END
                CALL F.WRITE (F.CS,ID.CS,R.FILL.CS)
                CALL JOURNAL.UPDATE(ID.CS)
            END
        NEXT J
    END
*================== GET DATA OF LC =================================
    IF SELECTED3 THEN
        FOR Z = 1 TO SELECTED3
            HIGH = 0
            TYPE = 'LC'
            R.CU = ''
            R.FILL.CF = ''
            LC.ACCT = ''
            NDPD = 0
            OLD.APP.ID   = ''
            R.FOLD = ''
            NEW.APP.DATE = ''
*==================================================================
            LC.CUS.ID   = KEY.LIST3<Z>
            CALL F.READ( FN.CU,LC.CUS.ID, R.CU, F.CU, ETEXT)
            SECTOR   = R.CU<EB.CUS.SECTOR>
            POS.REST = R.CU<EB.CUS.POSTING.RESTRICT>
            ACTIVE   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ACTIVE.CUST>

*==================================================================

            IF (SECTOR EQ '1100' OR SECTOR EQ '1200' OR SECTOR EQ '1300' OR SECTOR EQ '1400' OR SECTOR EQ '2000') AND (POS.REST NE 90 AND POS.REST NE 99) AND ACTIVE EQ 'YES' THEN
                CRT @(30,3) KEY.LIST3<Z>:' ':Z:' OF ':SELECTED3
                CALL F.READ( FN.CBE,KEY.LIST3<Z>, R.CBE2, F.CBE, ETEXT6)
                LC.CUS.ID   = KEY.LIST3<Z>
                MAX.TOTAL   = R.CBE2<SCB.C.CBE.TOT.CR.11,1>
                OPN.BALANCE = R.CBE2<SCB.C.CBE.TOT.US.11,1>
                CBE.NO      = R.CBE2<SCB.C.CBE.CBE.NO,1>
                LMT.REF     = LC.CUS.ID:'.0020000.01'
                LMT.REF2    = LC.CUS.ID:'.0030000.01'
                LC.ACCT     = LC.CUS.ID:'102310101'
* =============== ????? ?? ??? ???? ?????  ==================
                CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT7)
                IF MAX.TOTAL EQ '' THEN
                    R.LMT<LI.EXPIRY.DATE> = "20001010"
                END
*============================================================
                IF OPN.BALANCE EQ '' THEN
                    OPN.BALANCE = 0
                END

*============================================================
                AMT_OVER       = 0
                IF OPN.BALANCE GT MAX.TOTAL THEN
                    AMT_OVER   = OPN.BALANCE - MAX.TOTAL
                END
                CALL F.READ( FN.CU,LC.CUS.ID, R.CU, F.CU, ETEXT5)

* =============== ???? ??? ???????  ========================

                DAYS = 'C'
                DDD = R.LMT<LI.EXPIRY.DATE>[1,8]
                IF DDD EQ '' THEN
                    DDD = "20100101"
                END

                IF R.LMT<LI.EXPIRY.DATE>[1,8] LT TODAY THEN

                    CALL CDD("",DDD,TODAY,DAYS)
                    NDPD = DAYS
                    IF NDPD GT 999 THEN NDPD = 999
                END ELSE
                    NDPD = 0
                END
                NEW.APP.DATE =   R.LMT<LI.APPROVAL.DATE>

                IF NDPD EQ 0 THEN
                    AMT_OVER = 0
                END
*===================== ????? ??? ??? ???????  =====================

*=============== REPAYMENT AND TERM OF LOAN VALUES===========
                LC.ACC.CAT = LC.ACCT[10,4]
                REPAY.TYPE   = '007'
                TERM.OF.LOAN = '12'
                HIGH = ''

*====================creat erecord to fil in the CF table==========

                IF NDPD EQ 0 THEN
                    AMT_OVER = 0
                END
                IF AMT_OVER EQ 0 THEN   ;*AHM
                    NDPD = 0
                END
                IF AMT_OVER LT 0 THEN
                    AMT_OVER = AMT_OVER * -1
                END

                AMT_OVER = DROUND(AMT_OVER,0)

                IF OPN.BALANCE LT 0 THEN
                    BAL.AMOUNT = OPN.BALANCE * -1
                    BAL.AMT   = DROUND(BAL.AMOUNT,0)
                END
                ELSE
                    BAL.AMOUNT = OPN.BALANCE
                    BAL.AMT    = DROUND(BAL.AMOUNT,0)
                END

                APP.DATE = NEW.APP.DATE
                OLD.APP.ID = LC.ACCT:YEAR:NEW.MONTH.B4:"LC"
                R.FOLD = ''
                CALL F.READ(F.CF,OLD.APP.ID, R.FOLD, FN.CF,OLD.ERR)
                R.FILL.CF<ISCO.CF.APPROVAL.DATE> = APP.DATE
                R.FILL.CF<ISCO.CF.CF.AMENDMENT.DATE> = ''

                IF R.FOLD NE '' THEN
                    OLD.APP.DATE.N = R.FOLD<ISCO.CF.APPROVAL.AMOUNT>

                    IF R.FOLD<ISCO.CF.APPROVAL.AMOUNT> NE MAX.TOTAL AND R.FOLD<ISCO.CF.APPROVAL.AMOUNT> NE '' THEN
                        AMED = '20':R.LMT<LI.DATE.TIME,1>[1,6]
                        IF AMED EQ '20' THEN AMED = ''
                        AMED.DATE = AMED
                        R.FILL.CF<ISCO.CF.CF.AMENDMENT.DATE> = AMED.DATE
                    END

                END
*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
                COMP.CODE = R.CU<EB.CUS.ACCOUNT.OFFICER>
                IF COMP.CODE EQ '11' THEN
                    ISLAMIC.IND = '001'
                END
                ELSE
                    ISLAMIC.IND = '002'
                END
****************************************

                R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
                R.FILL.CF<ISCO.CF.CF.ACC.NO>           = LC.ACCT
                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(MAX.TOTAL * 1000,0)
                R.FILL.CF<ISCO.CF.HIGHEST.CREDIT>      = HIGH
                R.FILL.CF<ISCO.CF.CURRENCY>            = '10'
                R.FILL.CF<ISCO.CF.LIABILITY.IND>       = '001'
                R.FILL.CF<ISCO.CF.CF.TYPE>             = '23101'
                R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = TERM.OF.LOAN
                R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = REPAY.TYPE
                R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = BAL.AMT * 1000
                R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = 0
                R.FILL.CF<ISCO.CF.NDPD>                = 0
                R.FILL.CF<ISCO.CF.ASSET.CLASS>         = '000'
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = '002'
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
                R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
                R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
                R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
                R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
                R.FILL.CF<ISCO.CF.ACC.STATUS>          = '3'
                R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY

                ID.CF = LC.ACCT:YEAR:NEW.MONTH:"LC"
                CALL F.WRITE (F.CF,ID.CF,R.FILL.CF)
                CALL JOURNAL.UPDATE(ID.CF)
*=============== creat erecord to fil in the CS table================

                IF TYPE EQ 'LC' THEN
                    R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
                    R.FILL.CS<ISCO.CS.CF.ACC.NO>           = LC.ACCT
                    R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= LC.CUS.ID
                    ID.CS = LC.ACCT:YEAR:NEW.MONTH:"LC"
                END

                R.FILL.CS<ISCO.CS.NATIONAL.ID>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
                R.FILL.CS<ISCO.CS.CITZENSHIP>          = R.CU<EB.CUS.NATIONALITY>
                CITZENSHIP                             = R.FILL.CS<ISCO.CS.CITZENSHIP>

                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO> THEN

                    IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                    ISSUE.AUTHORITY   = '031'
                END
                ELSE
                    IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
                    ISSUE.AUTHORITY   = R.CU<EB.CUS.LOCAL.REF><1,CULR.PLACE.ID.ISSUE>
                END

                IF CITZENSHIP EQ 'EG' AND ISSUE.AUTHORITY NE '031' THEN
                    R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = ''
                    R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
                END
                ELSE
                    R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = IDENT.CODE
                    R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ISSUE.AUTHORITY

                END
                R.FILL.CS<ISCO.CS.CS.A.LAST.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> EQ 98 THEN
                    R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '002'
                END
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> NE 98 THEN
                    R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '001'
                END


                R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
                R.FILL.CS<ISCO.CS.ADD.1.GOV.E>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                R.FILL.CS<ISCO.CS.COUNTRY.1>        = R.CU<EB.CUS.RESIDENCE>
                LOCAL.REF1                          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 904 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
                IF NO_OF_TEL EQ 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                IF NO_OF_TEL GT 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                    R.FILL.CS<ISCO.CS.MOBILE>= R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>
                END
                R.FILL.CS<ISCO.CS.BIRTH.DATE>  = R.CU<EB.CUS.BIRTH.INCORP.DATE>
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'M-���'  THEN
                    R.FILL.CS<ISCO.CS.GENDER>  = '001':
                END
                IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'F-����' THEN
                    R.FILL.CS<ISCO.CS.GENDER>  = '002'
                END
                R.FILL.CS<ISCO.CS.CITZENSHIP>= R.CU<EB.CUS.NATIONALITY>
                R.FILL.CS<ISCO.CS.RECORD.STATUS>       = 'INAU'
                R.FILL.CS<ISCO.CS.CURR.NO>             = '1'
                R.FILL.CS<ISCO.CS.INPUTTER>            = 'ISCORE'
                R.FILL.CS<ISCO.CS.CO.CODE>             = COMP
                IF R.CU<EB.CUS.SECTOR> EQ '1100' OR R.CU<EB.CUS.SECTOR> EQ '1300' THEN
                    R.FILL.CS<ISCO.CS.OCCUPATION> = '8'
                END ELSE
                    R.FILL.CS<ISCO.CS.OCCUPATION> = R.CU<EB.CUS.LOCAL.REF><1,CULR.PROFESSION>
                END
                CALL F.WRITE (F.CS,ID.CS,R.FILL.CS)
                CALL JOURNAL.UPDATE(ID.CS)
            END
        NEXT Z
    END
**********************PERSONAL LOANS*************
    K.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY IN ('21055' '21063' '21064' '21065' '21066') AND STATUS NE 'LIQ'"
    CALL EB.READLIST(K.SEL, KEY.LIST.LD, "", SELECTED.LD, ERR.LD)
    IF SELECTED.LD THEN
        FOR K =1 TO SELECTED.LD
            DIFF.MONTH = 1

            CALL F.READ( FN.LD,KEY.LIST.LD<K>, R.LD, F.LD, ETX.LD)
            CUS.LD = R.LD<LD.CUSTOMER.ID>
            CALL F.READ( FN.CU,CUS.LD, R.CU, F.CU, ETX.CUS)
            CUS.LD.0 = CUS.LD
            IF LEN(CUS.LD) NE 8 THEN
                CUS.LD.0 = '0':CUS.LD
            END
            LD.ACC = KEY.LIST.LD<K>

***************NEW 13-2-2018**************
            L.VALUE.DATE = R.LD<LD.VALUE.DATE>
            LD.MAT.DATE  = R.LD<LD.FIN.MAT.DATE>
            DIFF.MONTH   = ''
            IN.RATE.TYPE = R.LD<LD.INT.RATE.TYPE>
            TOT.INT.AMT  = R.LD<LD.TOT.INTEREST.AMT>

            CALL EB.NO.OF.MONTHS(L.VALUE.DATE,LD.MAT.DATE,DIFF.MONTH)
            CALL DBR('LD.SCHEDULE.DEFINE':@FM:LD.SD.AMOUNT,LD.ACC,LD.INST.AMT)
            LD.INST.AMT = LD.INST.AMT<1,1>
            IN.RATE.TYPE = R.LD<LD.INT.RATE.TYPE>
            IF IN.RATE.TYPE EQ 3 THEN
                LD.INST.AMT = LD.INST.AMT + TOT.INT.AMT
            END

            LD.PAY.DUE.ID = 'PD':LD.ACC
            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,LD.PAY.DUE.ID,LD.AMT.OVER)
            CALL DBR('PD.PAYMENT.DUE':@FM:PD.PAYMENT.DTE.DUE,LD.PAY.DUE.ID,LD.DUE.DATE)
*Line [ 969 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.DUE   = DCOUNT(LD.DUE.DATE,@VM)
            LD.DUE.DATE = LD.DUE.DATE<1,NO.DUE>

            IF LD.DUE.DATE THEN
                DAYS = 'C'
                CALL CDD("",LD.DUE.DATE,TODAY,DAYS)
                LD.NDPD = DAYS
                IF DAYS GT 999 THEN LD.NDPD = 999
            END
            ELSE
                LD.NDPD = 0
            END
********************************************
***************GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
            COMP.CODE = R.CU<EB.CUS.ACCOUNT.OFFICER>
            IF COMP.CODE EQ '11' THEN
                ISLAMIC.IND = '001'
            END
            ELSE
                ISLAMIC.IND = '002'
            END
****************************************

            LD.ID  = CUS.LD.0:KEY.LIST.LD<K>[3,10]:YEAR:NEW.MONTH:'L'
            LD.AMT = R.LD<LD.AMOUNT>
            R.FILL.CF<ISCO.CF.APPROVAL.DATE>       = R.LD<LD.VALUE.DATE>
            R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
            R.FILL.CF<ISCO.CF.CF.ACC.NO>           = LD.ACC
            *R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(R.LD<LD.AMOUNT>,0)
             R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(R.LD<LD.DRAWDOWN.ISSUE.PRC>,0)
            R.FILL.CF<ISCO.CF.HIGHEST.CREDIT>      = ''
            R.FILL.CF<ISCO.CF.CURRENCY>            = '10'
            R.FILL.CF<ISCO.CF.LIABILITY.IND>       = '001'
* R.FILL.CF<ISCO.CF.CF.TYPE>             = '21055'
            R.FILL.CF<ISCO.CF.CF.TYPE>             = R.LD<LD.CATEGORY>
            R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = DIFF.MONTH
            R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = ''
            R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = DROUND(R.LD<LD.AMOUNT>,0)
            R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = LD.AMT.OVER
            R.FILL.CF<ISCO.CF.NDPD>                = LD.NDPD
            R.FILL.CF<ISCO.CF.AMT.INSTALL>         = DROUND(LD.INST.AMT,0)
            R.FILL.CF<ISCO.CF.ASSET.CLASS>         ='000'
            R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = '003'
            R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
            R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = 0
            R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
            R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
            R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
            R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
            R.FILL.CF<ISCO.CF.ACC.STATUS>          = '3'
            R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY
            CALL F.WRITE (F.CF,LD.ID,R.FILL.CF)
            CALL JOURNAL.UPDATE(LD.ID)

*----------- CS FOR PERSONAL LOAN LD ---------------------------

            R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = R.CU<EB.CUS.ACCOUNT.OFFICER>
            R.FILL.CS<ISCO.CS.CF.ACC.NO>           = LD.ACC
            R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= CUS.ID
            R.FILL.CS<ISCO.CS.NATIONAL.ID>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            R.FILL.CS<ISCO.CS.CITZENSHIP>          = R.CU<EB.CUS.NATIONALITY>
            CITZENSHIP                             = R.FILL.CS<ISCO.CS.CITZENSHIP>

            IF R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO> THEN
                IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                ISSUE.AUTHORITY   = '031'
            END
            ELSE
                IDENT.CODE        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.NUMBER>
                ISSUE.AUTHORITY   = R.CU<EB.CUS.LOCAL.REF><1,CULR.PLACE.ID.ISSUE>
            END
            IF CITZENSHIP EQ 'EG' AND ISSUE.AUTHORITY NE '031' THEN
                R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = ''
                R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
            END
            ELSE
                R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = IDENT.CODE
                R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ISSUE.AUTHORITY
            END
            R.FILL.CS<ISCO.CS.CS.A.LAST.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> EQ 98 THEN
                R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '002'
            END
            IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE> NE 98 THEN
                R.FILL.CS<ISCO.CS.ADD.1.TYPE.E> = '001'
            END
            R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
            R.FILL.CS<ISCO.CS.ADD.1.GOV.E>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
            R.FILL.CS<ISCO.CS.COUNTRY.1>        = R.CU<EB.CUS.RESIDENCE>
            LOCAL.REF1                          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 1062 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
            IF NO_OF_TEL EQ 1 THEN
                R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
            END
            IF NO_OF_TEL GT 1 THEN
                R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                R.FILL.CS<ISCO.CS.MOBILE>= R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>
            END
            R.FILL.CS<ISCO.CS.BIRTH.DATE>  = R.CU<EB.CUS.BIRTH.INCORP.DATE>
            IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'M-���'  THEN
                R.FILL.CS<ISCO.CS.GENDER>  = '001':
            END
            IF R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER> EQ 'F-����' THEN
                R.FILL.CS<ISCO.CS.GENDER>  = '002'
            END
            R.FILL.CS<ISCO.CS.CITZENSHIP>= R.CU<EB.CUS.NATIONALITY>
            IF R.CU<EB.CUS.SECTOR> EQ '1100' OR R.CU<EB.CUS.SECTOR> EQ '1300' THEN
                R.FILL.CS<ISCO.CS.OCCUPATION> = '8'
            END ELSE
                R.FILL.CS<ISCO.CS.OCCUPATION> = R.CU<EB.CUS.LOCAL.REF><1,CULR.PROFESSION>
            END
            R.FILL.CS<ISCO.CS.RECORD.STATUS>       = 'INAU'
            R.FILL.CS<ISCO.CS.CURR.NO>             = '1'
            R.FILL.CS<ISCO.CS.INPUTTER>            = 'ISCORE'
            R.FILL.CS<ISCO.CS.CO.CODE>             = COMP

            CALL F.WRITE (F.CS,LD.ID,R.FILL.CS)
            CALL JOURNAL.UPDATE(LD.ID)
        NEXT K
    END


*==========================GET CLOSED ACCOUNTS===========================
    IF SELECTED13 THEN
        FOR Q = 1 TO SELECTED13

            CALL F.READ( F.CF,KEY.LIST13<Q>, R.CFF, FN.CF, ETEXT)
            DATE.B4      = YEAR:NEW.MONTH
            NEW.DATE     = YEAR.B4:NEW.MONTH.B4
            KEY.ID       = KEY.LIST13<Q>
            LAST.CH      = KEY.ID[1]
            LEN.CH       = LEN(KEY.ID)
            LAST.3.CH    = KEY.ID[3]
*    IF LAST.CH EQ 'L' THEN DEBUG
            IF LAST.CH EQ 'B' THEN

                IF LEN.CH EQ 23 THEN
                    ACCT.B4    = KEY.ID[1,16]:DATE.B4
                    ACCT.B4.CS = KEY.ID[1,16]:DATE.B4

                    ACCT.T     = KEY.ID[1,16]:NEW.DATE:'B'
                    ACCT.T.CS  = KEY.ID[1,16]:NEW.DATE

                    CS.ACCT.NEW= KEY.ID[1,16]:DATE.B4:'.A'

                END ELSE
                    ACCT.B4     = KEY.ID[2,16]:DATE.B4
                    ACCT.B4.CS  = KEY.ID[2,16]:DATE.B4
                    ACCT.T      = KEY.ID[1,17]:NEW.DATE:'B'
                    ACCT.T.CS   = KEY.ID[1,17]:NEW.DATE

                    CS.ACCT.NEW = KEY.ID[1,17]:DATE.B4:'.A'
                END
            END
            IF LAST.CH EQ 'D'  AND LAST.3.CH NE 'OLD' THEN
                IF LEN.CH EQ 24 THEN
                    ACCT.B4     = KEY.ID[1,16]:DATE.B4:'LD'
                    ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4:'LD'
                    ACCT.T      = KEY.ID[1,16]:NEW.DATE:'LD'
                    ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE:'LD'

                    CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'
                END ELSE
                    ACCT.B4     = KEY.ID[1,17]:DATE.B4:'LD'
                    ACCT.B4.CS  = KEY.ID[1,17]:DATE.B4:'LD'
                    ACCT.T      = KEY.ID[1,17]:NEW.DATE:'LD'
                    ACCT.T.CS   = KEY.ID[1,17]:NEW.DATE:'LD'

                    CS.ACCT.NEW = KEY.ID[1,17]:DATE.B4:'.A'
                END
            END
            IF LAST.CH EQ 'C'  THEN
                IF LEN.CH EQ 24 THEN
                    ACCT.B4     = KEY.ID[1,16]:DATE.B4:'LC'
                    ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4:'LC'
                    ACCT.T      = KEY.ID[1,16]:NEW.DATE:'LC'
                    ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE:'LC'

                    CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'

                END ELSE
                    ACCT.B4    = KEY.ID[1,17]:DATE.B4:'LC'
                    ACCT.B4.CS = KEY.ID[1,17]:DATE.B4:'LC'
                    ACCT.T     = KEY.ID[1,17]:NEW.DATE:'LC'
                    ACCT.T.CS  = KEY.ID[1,17]:NEW.DATE:'LC'

                    CS.ACCT.NEW = KEY.ID[1,17]:DATE.B4:'.A'
                END
            END
*-----------------------------------------------------------------

            IF LAST.CH EQ 'L'  THEN

                IF LEN.CH EQ 24 THEN
                    ACCT.B4     = KEY.ID[1,16]:DATE.B4:'L'
                    ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4:'L'
                    ACCT.T      = KEY.ID[1,16]:NEW.DATE:'L'
                    ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE:'L'

                    CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'

                END ELSE
                    ACCT.B4    = KEY.ID[1,18]:DATE.B4:'L'
                    ACCT.B4.CS = KEY.ID[1,18]:DATE.B4:'L'
                    ACCT.T     = KEY.ID[1,18]:NEW.DATE:'L'
                    ACCT.T.CS  = KEY.ID[1,18]:NEW.DATE:'L'

                    CS.ACCT.NEW = KEY.ID[1,18]:DATE.B4:'.A'
                END
            END

*-----------------------------------------------------------------
            IF LAST.CH NE 'C' AND LAST.CH NE 'B' AND LAST.CH NE 'D' AND LAST.CH NE 'L' THEN
                ACCT.B4     = KEY.ID[1,16]:DATE.B4
                ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4
                ACCT.T      = KEY.ID[1,16]:NEW.DATE
                ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE

                CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'
            END
            CALL F.READ(F.CF,ACCT.B4, R.CFT, FN.CF,CF.ERR)
            CALL F.READ(F.CS,ACCT.B4.CS,R.CSS,FN.CS,CS.ERR)

            R.FILL.CLOSE = ''
            R.FILL.CL.S  = ''

            IF R.CFT EQ '' THEN
*Line [ 1200 ] Comment DEBUG - ITSS - R21 Upgrade - 2021-12-26
                IF ACCT.B4 EQ 201003021727413801201803L THEN *DEBUG

                CALL F.READ(F.CF,ACCT.T, R.CFT, FN.CF,CF.ERR)
                CALL F.READ(F.CS,ACCT.T.CS,R.CSS,FN.CS,CS.ERR)

                R.FILL.CLOSE<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CFF<ISCO.CF.DATA.PROV.BRANCH.ID>
                R.FILL.CLOSE<ISCO.CF.CF.ACC.NO>           = R.CFF<ISCO.CF.CF.ACC.NO>
*DEBUG
                ACC.NOOO = R.CFF<ISCO.CF.CF.ACC.NO>
*NN.SEL   = "SELECT F.SCB.ISCO.CF WITH @ID LIKE ":ACC.NOOO:"..."
                NN.SEL   = "SELECT F.SCB.ISCO.CF WITH CF.ACC.NO EQ ":ACC.NOOO
                NN.SEL  := " AND APPROVAL.DATE NE '' BY @ID"
                CALL EB.READLIST(NN.SEL, NN.KEY.LIST, "", NN.SELECTED, NN.ASD)

                IF NN.SELECTED THEN
                    CALL F.READ(F.CF,NN.KEY.LIST<NN.SELECTED>, R.CFT.NN, FN.CF,CF.ERR)
                    R.FILL.CLOSE<ISCO.CF.APPROVAL.DATE>  = R.CFT.NN<ISCO.CF.APPROVAL.DATE>

                    IF R.CFT.NN<ISCO.CF.APPROVAL.AMOUNT> LE 0 THEN
                        R.FILL.CLOSE<ISCO.CF.APPROVAL.AMOUNT>  = "1"
                    END ELSE
                        R.FILL.CLOSE<ISCO.CF.APPROVAL.AMOUNT>  = R.CFT.NN<ISCO.CF.APPROVAL.AMOUNT>
                    END
                END

                IF R.CFF<ISCO.CF.CF.TYPE> EQ '1501' OR R.CFF<ISCO.CF.CF.TYPE> EQ '1508' OR R.CFF<ISCO.CF.CF.TYPE> EQ '1599' OR R.CFF<ISCO.CF.CF.TYPE> EQ '1205' OR R.CFF<ISCO.CF.CF.TYPE> EQ '1206' OR R.CFF<ISCO.CF.CF.TYPE> EQ '1207' OR R.CFF<ISCO.CF.CF.TYPE> EQ '1208' THEN
                    R.FILL.CLOSE<ISCO.CF.HIGHEST.CREDIT> = ''
                END ELSE
                    R.FILL.CLOSE<ISCO.CF.HIGHEST.CREDIT>      = R.CFF<ISCO.CF.HIGHEST.CREDIT>
                END

                R.FILL.CLOSE<ISCO.CF.CURRENCY>            = R.CFF<ISCO.CF.CURRENCY>
                R.FILL.CLOSE<ISCO.CF.LIABILITY.IND>       = R.CFF<ISCO.CF.LIABILITY.IND>
                R.FILL.CLOSE<ISCO.CF.CF.TYPE>             = R.CFF<ISCO.CF.CF.TYPE>
                R.FILL.CLOSE<ISCO.CF.TERM.OF.LOAN>        = R.CFF<ISCO.CF.TERM.OF.LOAN>
                R.FILL.CLOSE<ISCO.CF.REPAYMENT.TYPE>      = R.CFF<ISCO.CF.REPAYMENT.TYPE>
                R.FILL.CLOSE<ISCO.CF.CF.ACCT.BALANCE>     = 0
                R.FILL.CLOSE<ISCO.CF.AMT.OVERDUE>         = 0
                R.FILL.CLOSE<ISCO.CF.NDPD>                = 0
                R.FILL.CLOSE<ISCO.CF.ASSET.CLASS>         = R.CFF<ISCO.CF.ASSET.CLASS>
                R.FILL.CLOSE<ISCO.CF.ACCT.CLOSING.DATE>   = LST.WRK.D
                R.FILL.CLOSE<ISCO.CF.RECORD.STATUS>       = 'INAU'
                R.FILL.CLOSE<ISCO.CF.PROTEST.NOTIFY>      = R.CFF<ISCO.CF.PROTEST.NOTIFY>
                R.FILL.CLOSE<ISCO.CF.PROTEST.NOTIFY.DATE> = R.CFF<ISCO.CF.PROTEST.NOTIFY.DATE>
                R.FILL.CLOSE<ISCO.CF.CLOSURE.REASON>      = R.CFF<ISCO.CF.CLOSURE.REASON>
                R.FILL.CLOSE<ISCO.CF.CURR.NO>             = '1'
                R.FILL.CLOSE<ISCO.CF.INPUTTER>            = 'ISCORE'
                R.FILL.CLOSE<ISCO.CF.CO.CODE>             = COMP
                R.FILL.CLOSE<ISCO.CF.ACC.STATUS>          = '90'
                R.FILL.CLOSE<ISCO.CF.DATE.TIME>           = TODAY

                CALL F.WRITE (F.CF,CS.ACCT.NEW,R.FILL.CLOSE)
                CALL JOURNAL.UPDATE(CS.ACCT.NEW)
*===================== WRITE CS ==============

                R.FILL.CL.S<ISCO.CS.DATA.PROV.BRANCH.ID> = R.CSS<ISCO.CS.DATA.PROV.BRANCH.ID>
                R.FILL.CL.S<ISCO.CS.CF.ACC.NO>           = R.CSS<ISCO.CS.CF.ACC.NO>
                R.FILL.CL.S<ISCO.CS.DATA.PROV.IDENT.CODE>= R.CSS<ISCO.CS.DATA.PROV.IDENT.CODE>
                R.FILL.CL.S<ISCO.CS.NATIONAL.ID>         = R.CSS<ISCO.CS.NATIONAL.ID>
                R.FILL.CL.S<ISCO.CS.CITZENSHIP>          = R.CSS<ISCO.CS.CITZENSHIP>
                R.FILL.CL.S<ISCO.CS.IDENT.CODE.1>        = R.CSS<ISCO.CS.IDENT.CODE.1>
                R.FILL.CL.S<ISCO.CS.ISSUE.AUTHORITY.1>   = R.CSS<ISCO.CS.ISSUE.AUTHORITY.1>
                R.FILL.CL.S<ISCO.CS.CS.A.LAST.NAME>      = R.CSS<ISCO.CS.CS.A.LAST.NAME>
                R.FILL.CL.S<ISCO.CS.ADD.1.TYPE.E>        = R.CSS<ISCO.CS.ADD.1.TYPE.E>
                R.FILL.CL.S<ISCO.CS.ADD.1.LINE.1.A>      = R.CSS<ISCO.CS.ADD.1.LINE.1.A>
                R.FILL.CL.S<ISCO.CS.ADD.1.GOV.E>         = R.CSS<ISCO.CS.ADD.1.GOV.E>
                R.FILL.CL.S<ISCO.CS.COUNTRY.1>           = R.CSS<ISCO.CS.COUNTRY.1>
                R.FILL.CL.S<ISCO.CS.PHONE>               = R.CSS<ISCO.CS.PHONE>
                R.FILL.CL.S<ISCO.CS.MOBILE>              = R.CSS<ISCO.CS.MOBILE>
                R.FILL.CL.S<ISCO.CS.GENDER>              = R.CSS<ISCO.CS.GENDER>
                R.FILL.CL.S<ISCO.CS.CITZENSHIP>          = R.CSS<ISCO.CS.CITZENSHIP>
                R.FILL.CL.S<ISCO.CS.BIRTH.DATE>          = R.CSS<ISCO.CS.BIRTH.DATE>
                R.FILL.CL.S<ISCO.CS.RECORD.STATUS>       = 'INAU'
                R.FILL.CL.S<ISCO.CS.CURR.NO>             = '1'
                R.FILL.CL.S<ISCO.CS.INPUTTER>            = 'ISCORE'
                R.FILL.CL.S<ISCO.CS.CO.CODE>             = COMP

                FN.CUSS  = 'FBNK.CUSTOMER'    ; F.CUSS = ''
                CALL OPF(FN.CUSS,F.CUSS)

                CUSS.ID = R.FILL.CL.S<ISCO.CS.DATA.PROV.IDENT.CODE>
                CALL F.READ(FN.CUSS,CUSS.ID,R.CUSS,F.CUSS,CUSS.ERR)
                SECTOR.CUS = R.CUSS<EB.CUS.SECTOR>
                IF R.CUSS<EB.CUS.SECTOR> EQ '1100' OR R.CUSS<EB.CUS.SECTOR> EQ '1300' THEN
                    R.FILL.CL.S<ISCO.CS.OCCUPATION> = '8'
                END ELSE
                    R.FILL.CL.S<ISCO.CS.OCCUPATION> = R.CUSS<EB.CUS.LOCAL.REF><1,CULR.PROFESSION>
                END
                CALL F.WRITE (F.CS,CS.ACCT.NEW,R.FILL.CL.S)
                CALL JOURNAL.UPDATE(CS.ACCT.NEW)
            END
            ELSE

                PREV.BRN = R.CFF<ISCO.CF.DATA.PROV.BRANCH.ID>
                NEW.BRN  = R.CFT<ISCO.CF.DATA.PROV.BRANCH.ID>
                IF PREV.BRN NE NEW.BRN THEN
                    R.CFT<ISCO.CF.PRE.DATA.BRANCH.ID> = PREV.BRN
                    CALL F.WRITE (F.CF,ACCT.B4,R.CFT)
                    CALL JOURNAL.UPDATE(ACCT.B4)

                END
            END
        NEXT Q
    END
***************** CALL UNKNOWN CASES PROGRAM **************
    T.SEL      = ''
    KEY.LIST   = ''
    SELECTED33 = 0

    T.SEL = "SELECT F.SCB.ISCO.UNKNOWN.CASES WITH STATUS EQ ''"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED33, ASD)

    FOR I = 1 TO SELECTED33

        R.UN  = '';  R.CF1 = ''; R.CF2 = ''; R.CF3 = ''; R.CF4 = '';
        ETEXT = '';  ER1   = ''; ER2 = ''; ER3 = ''; ER4 = ''
        NDPD  = 999; DAYS  = 'C'; FLAG = 'N'

        CALL F.READ( FN.UN,KEY.LIST<I>, R.UN, F.UN, ETEXT)

        ACCT.ID  = KEY.LIST<I>:FINAL.DATE
        ACCT.ID1 = ACCT.ID
        ACCT.ID2 = ACCT.ID:'LD'
        ACCT.ID3 = ACCT.ID:'LC'

        APP.DATE = R.UN<ISCO.UN.APPROVAL.DATE>
        APP.AMT  = R.UN<ISCO.UN.APPROVAL.AMT>

        CALL CDD("",APP.DATE,TODAY,DAYS)

        IF DAYS LE 365 THEN
            NDPD = 120
        END
        CALL F.READ( F.CF ,ACCT.ID1, R.CF1, FN.CF, ER1)
        CALL F.READ( F.CF ,ACCT.ID2, R.CF2, FN.CF, ER2)
        CALL F.READ( F.CF ,ACCT.ID3, R.CF3, FN.CF, ER3)

        IF ER1 EQ '' THEN
            BAL = R.CF1<ISCO.CF.CF.ACCT.BALANCE> - APP.AMT

            IF R.CF1<ISCO.CF.APPROVAL.DATE> EQ '' THEN
                R.CF1<ISCO.CF.APPROVAL.DATE>   = APP.DATE
                FLAG = 'Y'
            END
            IF R.CF1<ISCO.CF.APPROVAL.AMOUNT> EQ '' OR R.CF1<ISCO.CF.APPROVAL.AMOUNT> EQ 0 THEN
                R.CF1<ISCO.CF.APPROVAL.AMOUNT> = APP.AMT
                FLAG = 'Y'
            END
            IF FLAG EQ 'Y' THEN
                IF BAL GT 1 THEN
                    R.CF1<ISCO.CF.AMT.OVERDUE> = BAL
                    R.CF1<ISCO.CF.NDPD> = NDPD

                END
                ELSE
                    R.CF1<ISCO.CF.AMT.OVERDUE> = 0
                    R.CF1<ISCO.CF.NDPD> = 0
                    R.CF1<ISCO.CF.ASSET.CLASS> = R.CF1<ISCO.CF.ASSET.CLASS>[1,2]:'1'
                END
            END
            CALL F.WRITE(F.CF,ACCT.ID1,R.CF1)
            CALL JOURNAL.UPDATE(ACCT.ID1)
        END

        IF ER2 EQ '' THEN
            BAL = R.CF2<ISCO.CF.CF.ACCT.BALANCE> - APP.AMT
            IF R.CF2<ISCO.CF.APPROVAL.DATE> EQ '' THEN
                R.CF2<ISCO.CF.APPROVAL.DATE>   = APP.DATE
                FLAG = 'Y'
            END
            IF R.CF2<ISCO.CF.APPROVAL.AMOUNT> EQ '' OR R.CF1<ISCO.CF.APPROVAL.AMOUNT> EQ 0 THEN
                R.CF2<ISCO.CF.APPROVAL.AMOUNT> = APP.AMT
                FLAG = 'Y'
            END
            IF FLAG EQ 'Y' THEN
                IF BAL GT 1 THEN
                    R.CF2<ISCO.CF.AMT.OVERDUE> = BAL
                    R.CF2<ISCO.CF.NDPD> = NDPD
                END
                ELSE
                    R.CF2<ISCO.CF.AMT.OVERDUE> = 0
                    R.CF2<ISCO.CF.NDPD> = 0
                    R.CF2<ISCO.CF.ASSET.CLASS> = R.CF2<ISCO.CF.ASSET.CLASS>[1,2]:'1'
                END
            END
            CALL F.WRITE(F.CF,ACCT.ID2,R.CF2)
            CALL JOURNAL.UPDATE(ACCT.ID2)
        END
        IF ER3 EQ '' THEN
            BAL = R.CF3<ISCO.CF.CF.ACCT.BALANCE> - APP.AMT
            IF R.CF3<ISCO.CF.APPROVAL.DATE> EQ '' THEN
                R.CF3<ISCO.CF.APPROVAL.DATE>   = APP.DATE
                FLAG = 'Y'
            END
            IF R.CF3<ISCO.CF.APPROVAL.AMOUNT> EQ '' OR R.CF1<ISCO.CF.APPROVAL.AMOUNT> EQ 0 THEN
                R.CF3<ISCO.CF.APPROVAL.AMOUNT> = APP.AMT
                FLAG = 'Y'
            END
            IF FLAG EQ 'Y' THEN
                IF BAL GT 1 THEN
                    R.CF3<ISCO.CF.AMT.OVERDUE> = BAL
                    R.CF3<ISCO.CF.NDPD> = NDPD
                END
                ELSE
                    R.CF3<ISCO.CF.AMT.OVERDUE> = 0
                    R.CF3<ISCO.CF.NDPD> = 0
                    R.CF3<ISCO.CF.ASSET.CLASS> = R.CF3<ISCO.CF.ASSET.CLASS>[1,2]:'1'
                END
            END
            CALL F.WRITE(F.CF,ACCT.ID3,R.CF3)
            CALL JOURNAL.UPDATE(ACCT.ID3)
        END
    NEXT I


END
