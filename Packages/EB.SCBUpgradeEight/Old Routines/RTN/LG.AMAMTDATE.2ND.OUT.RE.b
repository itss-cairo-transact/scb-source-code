* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
************ABEER**************

    SUBROUTINE LG.AMAMTDATE.2ND.OUT.RE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


    GOSUB INITIATE
*Line [ 50 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.2ND.OUT.RE'
    CALL PRINTER.ON(REPORT.ID,'')
    YTEXT = "Enter LG.NO : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    LG.ID = COMI
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF (FN.LD,F.LD)
    FN.LG.HIS = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LG.HIS = ''
    CALL OPF (FN.LG.HIS,F.LG.HIS)

    RETURN
*===============================================================
CALLDB:

    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    TEXT = COMI ; CALL REM
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    SUP   =LOCAL.REF<1,LDLR.GUARRANTOR>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    CURRNO=R.LD<LD.CURR.NO>
    CURR1 = CURRNO-1
***THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    BENF=LOCAL.REF<1,LDLR.APPROVAL>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.ISSUE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE =DATE.ISSUE[7,2]:"/":DATE.ISSUE[5,2]:"/":DATE.ISSUE[1,4]
*  DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
*****************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
******************
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)

***    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    TEXT = BENF1 ; CALL REM
    TEXT = SUP ; CALL REM
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT AND FIN MAT DATE************************************
    TEXT = CURR1 ;  CALL REM
    IDHIS=COMI:";":CURR1
** T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ": IDHIS
** CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* IF KEY.LIST THEN
**OLD.ID= KEY.LIST<SELECTED>
***ID=FIELD(OLD.ID,';',1)
    ID=IDHIS
    OLD.ID=IDHIS
    TEXT = "IDHIS : " : IDHIS ; CALL REM
***************************************************************************
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
    CALL F.READ(FN.LG.HIS,IDHIS, R.LG.HIS, F.LG.HIS ,ETEXT)
    LOCAL.REF=R.LG.HIS<LD.LOCAL.REF>
    OLD.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    TEXT = "OLDFIN l: " : OLD.FIN ; CALL REM
    FIN.OLD =OLD.FIN[7,2]:"/":OLD.FIN[5,2]:"/":OLD.FIN[1,4]
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT=R.LG.HIS<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT=R.OLD(LD.AMOUNT)
        END
    END
    CALL F.RELEASE(FN.LG.HIS,OLD.ID,F.LG.HIS)
***************GET AMOUNT INC OR DEC************************************
*  FN.LG='F.LD.LOANS.AND.DEPOSITS';F.LG='';R.LG='';F.LG=''
*  CALL OPF(FN.LG,F.LG)
*  CALL F.READ(FN.LG,ID, R.LG, F.LG ,ETEXT)
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT=R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT=R.NEW(LD.AMOUNT)
        END
    END
*   CALL F.RELEASE(FN.LG,ID,F.LG)
**********************************************************************************
******************************

** RETURN
*===============================================================
**PRINT.HEAD:
*    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

** CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    OPER.CODE='�'
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":OPER.CODE:"*"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
* CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*************
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(28):"������ ���":"*":EXT.NO:"*"
    HEADING PR.HD

*** RETURN
*==================================================================
**BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRLNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���    : ":LG.NO:" ":TYPE.NAME:""
    PRINT
    PRINT SPACE(20): "�������������    : ": " **":NEW.AMOUNT:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
    PRINT SPACE(20):"������ ������    : ":ISSUE.DATE
    PRINT
    PRINT SPACE(20): "������� � �����  : ":SUP
    PRINT
    PRINT SPACE(20): "���� ������� ��� : ":FIN.OLD
********** PRINT SPACE(20):"���� ������� �����: " : "2009/09/23"
*  END
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(6):"�������� ��� ������ ��� /"   :CORR.NO:"������  " :CORR.DATE
    PRINT;PRINT
    PRINT SPACE(2):"��� ������ ��� ����� ���� ������ ������ ������� ���������":FIN.DATE
    PRINT
    PRINT SPACE(2):"�� ���� ���� ������ ������ ���� ����� ."
    PRINT;PRINT
    PRINT SPACE(2):"� ��� ��� ��� ��� ������  �� ��� ����� ��� �� ���� �� ���� �����"
    PRINT
    PRINT SPACE(2):"( ":OUT.DATE:".":" )"
    PRINT;PRINT
    PRINT SPACE(6):"� ����� ������ ����� �� ����� ���� ����� ������ ���� ����������"
    PRINT
    PRINT SPACE(2):" ��� ������ � ��� ����� �� �������� ���� �������ϡ ��� ����� �����"
    PRINT
    PRINT SPACE(2):" ������� �� ����� ������ ������ ����� ��� ��� ������� ."
    PRINT;PRINT
    PRINT SPACE(2):"����� ��� ������� ���� � �� ���� �� �� ���� ���� ��� �� ��� �� �����"
    PRINT
    PRINT SPACE(2):"�� ���������."
    PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":XX
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==========================================================
