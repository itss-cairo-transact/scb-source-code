* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*--- cREATED BY NESSMA ON 2019/03/10
    SUBROUTINE GET.CHEQ.DATE(ARG)

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT           I_FT.LOCAL.REFS
*----------------------------------------------
***UPDATED BY NESSREEN 11/3/2019******
***    ARG  = R.NEW(FT.LOCAL.REF)<1,FTLR.CHQ.DATE>
    CHQ.DATE = ""
    Y.FT.LOC.VAL = R.NEW(FT.LOCAL.REF)
    CHQ.DATE = Y.FT.LOC.VAL<1,FTLR.CHQ.DATE>
    ARG      = FMT(CHQ.DATE,"####/##/##")
*--------------------------------------------
    RETURN
END
