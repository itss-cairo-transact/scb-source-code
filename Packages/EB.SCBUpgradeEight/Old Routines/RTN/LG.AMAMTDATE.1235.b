* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
**********ABEER**********

    SUBROUTINE LG.AMAMTDATE.1235

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    TEXT = "3" ; CALL REM

***IF ID.NEW THEN
    IF COMI THEN
        MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        IF MYCODE='1235' THEN
            GOSUB INITIATE
*Line [ 53 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            FOR II = 1 TO 2
                GOSUB PRINT.HEAD
                GOSUB BODY
            NEXT II
        END
    END ELSE
        IF ID.NEW EQ '' THEN
            GOSUB INITIATE
*Line [ 63 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB
            IF MYCODE='1235' THEN
                FOR II = 1 TO 2
                    GOSUB PRINT.HEAD
                    GOSUB BODY
                NEXT II
            END  ELSE
                MYID=MYCODE:'.':MYTYPE
**  CALL DBR ('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
                OPER.CODE=MYID
                E='This.Contract.Is.For.&':OPER.CODE; CALL ERR ; MESSAGE = 'REPEAT';RETURN
            END
        END
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.AMAMTDATE.1235'
    CALL PRINTER.ON(REPORT.ID,'')
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)

    RETURN
*===============================================================
CALLDB:
    IF ID.NEW EQ '' THEN YTEXT = "Enter the L/G. No. : "
    IF ID.NEW EQ '' THEN CALL TXTINP(YTEXT, 8, 22, "12", "A")

    IF ID.NEW THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END ELSE
        IF ID.NEW EQ '' THEN
            FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
            CALL OPF(FN.LD,F.LD)
            CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
        END
    END
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    EXT.NO=LOCAL.REF<1,LDLR.NO.OF.DAYS>
    CORR.NO=R.LD<LD.CUSTOMER.REF>
    CURRNO = R.LD<LD.CURR.NO>
    CURRNO1= CURRNO-1
    DATE.CORR=LOCAL.REF<1,LDLR.APPROVAL.DATE>
    CORR.DATE =DATE.CORR[7,2]:"/":DATE.CORR[5,2]:"/":DATE.CORR[1,4]
    DATE.FIN=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
*   DATE.FIN=R.LD<LD.FIN.MAT.DATE>
    IN.DATE=DATE.FIN
    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
    FIN.DATE=DATE.FIN[7,2]:"/":DATE.FIN[5,2]:"/":DATE.FIN[1,4]
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    THIRD.NAME =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    OLD.LG.NO=LOCAL.REF<1,LDLR.OLD.NO>
    IF OLD.LG.NO THEN
        LG.NO=OLD.LG.NO
    END ELSE
        IF OLD.LG.NO EQ '' THEN
            LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
        END
    END
    LG.AMT =R.LD<LD.AMOUNT>
    LG.INC.AMT = R.LD<LD.AMOUNT.INCREASE>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    CALL DBR ('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CUR,CRR.DESC)

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
**********************GET OLD AMOUNT************************************
    IDHIS=ID.NEW:";":CURRNO1
    OLD.ID= IDHIS
    ID=IDHIS
***************************************************************************
    FN.LG.HIS='F.LD.LOANS.AND.DEPOSITS$HIS';F.LG.HIS='';R.LG.HIS='';F.LG.HIS=''
    CALL OPF(FN.LG.HIS,F.LG.HIS)
    CALL F.READ(FN.LG.HIS,OLD.ID,R.LG.HIS,F.LG.HIS,E)
    IF ID.NEW EQ '' THEN
        OLD.AMOUNT=R.LG.HIS<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            OLD.AMOUNT= R.OLD(LD.AMOUNT)
        END
    END
    CALL F.RELEASE(FN.LG.HIS,OLD.ID,F.LG.HIS)
***************GET AMOUNT INC OR DEC************************************
*   FN.LG='F.LD.LOANS.AND.DEPOSITS';F.LG='';R.LG='';F.LG=''
*   CALL OPF(FN.LG,F.LG)
*   CALL F.READ(FN.LG,ID, R.LG, F.LG ,E)
    IF ID.NEW EQ '' THEN
        NEW.AMOUNT = R.LD<LD.AMOUNT>
    END ELSE
        IF ID.NEW THEN
            NEW.AMOUNT = R.NEW(LD.AMOUNT)
        END
    END
    IN.AMOUNT = NEW.AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)

**********************************************************************************
    AMT.DEC  =  NEW.AMOUNT - LG.INC.AMT
**AMT.DEC  = AMT.DEC1[2,20]
    TEXT = "AMT : " : AMT.DEC ; CALL REM
***AMT.DEC = OLD.AMOUNT-NEW.AMOUNT
    AMT.INC = NEW.AMOUNT-OLD.AMOUNT
*    END

**********************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*  MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*  MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

**CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    OPER.CODE=MYID
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":OPER.CODE:"*"
*UPDATED BY ABEER ---UPGRADING R15---2016-03-07---
 * CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
     CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
 *************

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":YY
    PR.HD :="'L'":SPACE(1):STR('_',22):SPACE(38):"�":"*":EXT.NO:"*"
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ���   : ":LG.NO:" )":TYPE.NAME:"("
    PRINT
    PRINT SPACE(20): "�������������   : ":" **":AMT.DEC:"**":" ":CRR
    PRINT
*//////////////////////////////////////////////////////
*    IF MYCODE[2,3]= "111" THEN
*      PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME
*   END ELSE
    PRINT SPACE(20): "������� � ����� : ":THIRD.NAME
*  END
*//////////////////////////////////////////////////////

    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(8):"�������� ��� ������ ��� / " : CORR.NO:"������":" ":  CORR.DATE :"����� ��� ����"
    PRINT
    PRINT SPACE(2):"������ ��� ���� ���� ������ ������ ������ �����" : "*":LG.INC.AMT:"*" :CRR.DESC
    PRINT
    PRINT SPACE(2):"����� ����������� �����":"*":NEW.AMOUNT:"*":CRR.DESC:" ":"������������� ���":FIN.DATE
    PRINT
    PRINT SPACE(2):"���� �� ���� ���� ������ � ������� ������ ��� �� ���� ����� ."
    PRINT
    PRINT SPACE(2):"���� ��� ��� ������������� ��������� �� ����� ����":"*":NEW.AMOUNT:"*":CRR.DESC
    PRINT
    IN.AMOUNT=NEW.AMOUNT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 263 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)

*Line [ 266 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR.DESC
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
    PRINT SPACE(2):"�� ����� �����":") ":OUT.DATE:".":" ("
    PRINT
    PRINT SPACE(2):"���� �� ����� ���� ��� ������ ��� ��� ������� ���� ��������"
    PRINT
    PRINT SPACE(2): "�� ����� ���� ����� ��� ������ ����� ���� ������ ."
    PRINT;PRINT
    PRINT SPACE(8):"����� ����� ��� ���� ������ ������� �� ���� ������ ��� �����"
    PRINT
    PRINT SPACE(2):"��� ������ �����  ��� ������� ."
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT ;PRINT
    IF II = '1' THEN
        PRINT SPACE(5): "���� ��� : ":THIRD.NAME
        PRINT SPACE(15):THIRD.ADDR1
        PRINT SPACE(15):"����� ������ ��������"
    END
    PRINT ; PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    TEXT = "REPORT CREATED SUCCESSFULLY1235 " ; CALL REM
    TEXT = "4" ; CALL REM
    RETURN
*==============================================================
