* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE INT.ACCT(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*    COMP = ID.COMPANY

***************************************
***********
    FN.ACC= 'FBNK.ACCOUNT';F.ACC='';R.ACC= '';E1=''
    CALL OPF(FN.ACC,F.ACC)

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

*T.SEL  = "SELECT FBNK.ACCOUNT WITH (CUSTOMER.NO EQ '' OR CATEGORY GT 9999) AND (CO.CODE EQ EG0010030 OR CO.CODE EQ EG0010031 OR CO.CODE EQ EG0010032 OR CO.CODE EQ EG0010035 OR CO.CODE EQ EG0010040 OR CO.CODE EQ EG0010050 OR CO.CODE EQ EG0010060 OR CO.CODE EQ EG0010070 OR CO.CODE EQ EG0010080 OR CO.CODE EQ EG0010081 OR CO.CODE EQ EG0010088 OR CO.CODE EQ EG0010090 OR CO.CODE EQ EG0010099)"
    T.SEL  = "SELECT FBNK.ACCOUNT WITH (CUSTOMER.NO EQ '' OR CATEGORY GT 9999)"
*T.SEL := " AND (ONLINE.ACTUAL.BAL EQ 0 OR ONLINE.ACTUAL.BAL EQ '') AND DATE.LAST.CR.CUST LT 20100419"
*T.SEL := " AND DATE.LAST.CR.AUTO LT 20100419 AND DATE.LAST.CR.BANK LT 20100419"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    Z = 1
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E1)
            IF NOT(E1) THEN
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = KEY.LIST<I>
                Z = Z+1

            END
        NEXT I
    END ELSE
        ENQ<2,Z> = "@ID"
        ENQ<3,Z> = "EQ"
        ENQ<4,Z> = "NOLIST"
    END
    RETURN
END
