* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>289</Rating>
*-----------------------------------------------------------------------------

******* WAEL *******

    SUBROUTINE LG.REPORT.TODAY

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    GOSUB INITIATE  
    GOSUB PRINT.HEAD
*Line [ 47 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.REPORT.TODAY'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
   T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CURRENCY EQ USD"
*    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND OPERATION.CODE EQ 1111":" AND ISSUE.DATE EQ ":TODAY
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED > 0 THEN GOSUB GET.RECORDS
RETURN
*-----------------------------------
GET.RECORDS:  
   IF KEY.LIST THEN
      C = 0
      FOR I = 1 TO SELECTED

             CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
             LOCAL.REF = R.LD<LD.LOCAL.REF>

             LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
             LG.CUS = R.LD<LD.CUSTOMER.ID>
             LG.AMT = R.LD<LD.AMOUNT>
             CUR<I> = R.LD<LD.CURRENCY>
          CALL DBR("CURRENCY":@FM:EB.CUR.CCY.NAME,CUR,CUR.NAME)
             EXP.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
             LG.THIRD = LOCAL.REF<1,LDLR.THIRD.NUMBER>
          CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,LG.THIRD,LOC.REF)
             CUST.NAME = LOC.REF<1,CULR.ARABIC.NAME>
             BENF.NAME1 = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
             BENF.NAME2 = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
             MARG  = LOCAL.REF<1,LDLR.MARGIN.AMT>

             IF CUR<1> # CUR<I> THEN
                 C = C + 1
                 CURREN = CUR<I>
                 TOT.LG = TOT.LG + LG.AMT
             END 

         ZZ= ''
         ZZ<1,1> = SPACE(132)
         ZZ<1,1>[1,20] = LG.NO
         ZZ<1,1>[22,9] = LG.CUS
         ZZ<1,1>[35,10] = LG.AMT
         ZZ<1,1>[50,5] = CUR.NAME
         ZZ<1,1>[60,10] = EXP.DATE[7,2]:'/':EXP.DATE[5,2]:"/":EXP.DATE[1,4]
         ZZ<1,1>[73,35] = CUST.NAME
         ZZ<1,1>[110,22] = BENF.NAME1

    IF BENF.NAME2 THEN
    ZZ<1,2>[110,22] = BENF.NAME2
    ZZ<1,3> = " "
  END ELSE
    ZZ<1,2> = " "
  END
         PRINT ZZ<1,1>
         PRINT ZZ<1,2>
    IF BENF.NAME2 THEN PRINT ZZ<1,3>
    IF I = SELECTED THEN
       PRINT STR('=',100)
       PRINT CURREN:"-":TOT.LG:"-":C:"-":"XXXXX"
    END
   NEXT I 
   END ELSE
       ENQ.ERROR = "NO RECORDS FOUND"
   END

RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):" ���� ���� �������� "
    PR.HD :="'L'":SPACE(30):" ���� �� ������� �����"
    PR.HD :="'L'":SPACE(28):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ������":SPACE(3):"��� ������":SPACE(5):"���� ������":SPACE(10):"���� ������" :SPACE(5):" ����� ��������"
    PR.HD := SPACE(5):" ���� ������" :SPACE(10):" ��� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',120)
    HEADING PR.HD
 RETURN
*==============================================================
END
