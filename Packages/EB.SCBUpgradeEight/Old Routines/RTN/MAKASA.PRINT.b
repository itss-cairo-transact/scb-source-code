* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MAKASA.PRINT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BRANCH.ACCT.OLD
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT.CRESTED "   ; CALL REM
    RETURN

*-----------------------------INITIALIZATIONS------------------------
INITIATE:

*****    REPORT.ID = 'MAKASA.PRINT'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    TXT.ID = TODAY[1,4]:"-":TODAY[5,2]:"-":TODAY[7,2]:'-credit.new.txt'
    Path = "/hq/opce/bclr/user/dltx/":TXT.ID


*    Path = "/hq/opce/bclr/user/dltx/credit.txt"

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF( FN.BR,F.BR)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF( FN.ACC,F.ACC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)


    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    I         = 1
    K         = 1
    J         = 1
    TOT.AMT   = 0
    TOT.AMT.1 = 0
    TOT.AMT.2 = 0
    RETURN

*------------------------RED FORM TEXT FILE----------------------
PROCESS:

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CUST              = ''
            OUR.ID            = FIELD(Line,",",3)
            RESULT.CODE       = FIELD(Line,",",1)
            AMT.CLR           = FIELD(Line,",",11)
            CHQ.NO            = FIELD(Line,",",10)
            ACC.NO            = FIELD(Line,",",9)

            IF OUR.ID THEN
                IF RESULT.CODE EQ "820" THEN

                    CALL F.READ( FN.BR, OUR.ID, R.BR, F.BR, ETEXT)

                    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST)

                    IF CUST EQ '' THEN
                        ACC.NO = "0":ACC.NO
                        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST)
                    END

                    CALL F.READ( FN.CUS, CUST, R.CUS, F.CUS, ETEXT1)
                    CUST.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
                    MAT.DATE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                    MATURITY  = MAT.DATE[7,2]:"/":MAT.DATE[5,2]:"/":MAT.DATE[1,4]
                    COMPP     = R.BR<EB.BILL.REG.CO.CODE>
                    COMP      = C$ID.COMPANY

                    IF COMPP EQ COMP THEN
                        GOSUB WRITE
                        K= K + 1
                        TOT.AMT.1 = TOT.AMT.1 + AMT.CLR
                    END

                END
                IF RESULT.CODE EQ "520" THEN
                    CALL F.READ( FN.BR, OUR.ID, R.BR, F.BR, ETEXT)

                    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST)

                    IF CUST EQ '' THEN
                        ACC.NO = "0":ACC.NO
                        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,ACC.NO,CUST)
                    END

                    CALL F.READ( FN.CUS, CUST, R.CUS, F.CUS, ETEXT1)
                    CUST.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
                    MAT.DATE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                    MATURITY  = MAT.DATE[7,2]:"/":MAT.DATE[5,2]:"/":MAT.DATE[1,4]
                    COMPP     = R.BR<EB.BILL.REG.CO.CODE>
                    COMP      = C$ID.COMPANY

                    IF COMPP EQ COMP THEN
                        GOSUB WRITE
                        J= J + 1
                        TOT.AMT.2 = TOT.AMT.2 + AMT.CLR

                    END
                END
            END
            I = I + 1
        END ELSE
            EOF = 1
        END

    REPEAT

    NO.CHQ.1= K - 1
    NO.CHQ.2= J - 1
    TOT.CHQ = NO.CHQ.1 + NO.CHQ.2
    TOT.AMT = TOT.AMT.1 + TOT.AMT.2
    XX2 = SPACE(132)
    XX1 = SPACE(132)
    XX4 = SPACE(132)
    XX5 = SPACE(132)
    XX1<1,1>[1,132]  = ""
    XX2<1,1>[1,12]   = "������ ��� ������� = "
    XX2<1,1>[14,15]  = TOT.CHQ
    XX2<1,1>[41,15]  = "�������� = "
    XX2<1,1>[57,15]  = TOT.AMT
    XX4<1,1>[1,12]   = "��� ������� ������� = "
    XX4<1,1>[14,15]  = NO.CHQ.1
    XX4<1,1>[41,15]  = "�������� = "
    XX4<1,1>[57,15]  = TOT.AMT.1
    XX5<1,1>[1,12]   = "��� ������� ������� = "
    XX5<1,1>[14,15]  = NO.CHQ.2
    XX5<1,1>[41,15]  = "�������� = "
    XX5<1,1>[57,15]  = TOT.AMT.2

    PRINT XX1<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX2<1,1>

    RETURN
*******************************************************
WRITE:

    XX3 = SPACE(132)
    XX3<1,1>[1,15]   = CHQ.NO
    XX3<1,1>[20,15]  = MATURITY
    XX3<1,1>[40,15]  = AMT.CLR
    XX3<1,1>[60,30]  = CUST.NAME
    PRINT XX3<1,1>


    RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"����� ����� ���� �������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":"��� �����":SPACE(10):"����� ������":SPACE(10):"������":SPACE(10):"��� ������"

    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
END
