* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-251</Rating>
*-----------------------------------------------------------------------------

************ WAEL ***********

    SUBROUTINE LG.CONFIS

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

*Line [ 47 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)

    IF MYCODE # '1401' AND MYCODE # '1402' AND MYCODE # '1403' THEN
        E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE

        IF MYCODE = "1401" THEN
            GOSUB INITIATE
            GOSUB PRINT.HEAD
            GOSUB INFO
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
            TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
        END ELSE
            GOSUB INITIATE
            GOSUB PRINT.HEAD
            GOSUB INFO
            GOSUB BODY2
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
            TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
        END
    END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.CONFIS'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    PURPOSE = LOCAL.REF<1,LDLR.IN.RESPECT.OF>
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE,1>
    LG.MAT.DATE = R.LD<LD.FIN.MAT.DATE>
    LG.APPROVAL.DATE=LOCAL.REF<1,LDLR.APPROVAL.DATE,1>
    LG.ACCT=LOCAL.REF<1,LDLR.DEBIT.ACCT,1>
    LG.AMT =R.LD<LD.AMOUNT>
* LG.AMT = R.NEW(LD.AMOUNT)
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    DATZ= TODAY
    ZZ= DATZ[7,2]:'/':DATZ[5,2]:"/":DATZ[1,4]
    DATQ= LG.APPROVAL.DATE
    QQ = DATQ[7,2]:'/':DATQ[5,2]:"/":DATQ[1,4]
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    REST.AMT = LG.AMT - CONFIS.AMT

    IN.AMOUNT = LG.AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)

*------------------------------------
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":COMI:"..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,KEY.LIST<SELECTED>,R.LD,F.LD,E1)

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    OPR.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>

    WW = R.LD<LD.DATE.TIME>
    D.TIME = WW[1,6]
    WWW = D.TIME[5,2]:'/':D.TIME[3,2]:"/":"20":D.TIME[1,2]
*-----------------------------------

    RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":"������":"*"
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L") 

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"����������":":":MYBRANCH
    PR.HD :="'L'":SPACE(1):STR('_',22)
    PR.HD :="'L'":SPACE(1):"��� ":ZZ
    HEADING PR.HD

    RETURN
*==========================================================
INFO:

    LNTH1 = LEN(THIRD.NAME1)+12 ; LNTH2 = LEN(THIRD.NAME2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(THIRD.ADDR1)+12 ; LNTHD2 = LEN(THIRD.ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
    PRINT "| ������� : ":THIRD.NAME1:SPACE(LNE1):"|"
    PRINT "|         : ":THIRD.NAME2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":THIRD.ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PRINT ;PRINT
    PRINT SPACE(17): "���� �������� ��� : ":LG.NO:" )":TYPE.NAME:"("
    PRINT
    PRINT SPACE(17): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(17): "������ ��� ������ : ":XX
    PRINT
    PRINT SPACE(17): " ������� � �����  : ":BENF1
    PRINT SPACE(17): "                  : ":BENF2
    PRINT
    PRINT SPACE(17): "���������������   : ":PURPOSE
    PRINT
    PRINT SPACE(17): "���� ������� ���  :":YY
    PRINT
    PRINT SPACE(17): "________________________________________________"
    PRINT ; PRINT

    RETURN
*======================================================================
BODY:
    PRINT SPACE(5):" ������ ������� ������ " : WWW : " ����� �������� ���� ���� ����� ���� ."
    PRINT
    PRINT SPACE(2):" ���� ������ ������ ������ ��� ���������� . � ��� ����� ������ �� �������"
    PRINT
    PRINT SPACE(2):" � ������� �� ���������� ������ ����� ."
    PRINT ; PRINT ; PRINT
    PRINT SPACE(2):" ����� ������ ������ � ������� ."

    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    RETURN
*================================================================
BODY2:

    PRINT SPACE(5):" ������ ������� ������ "  : WWW : " ����� �������� ���� ���� ����� ���� " :CONFIS.AMT
    PRINT
    PRINT SPACE(2):" �� ���� ���� ������ ������ ������ ��� ���������� � ��� ����� ����� ����� ����� ���� "
    PRINT
    PRINT SPACE(2):" ������� ����� " : LG.AMT :
*Line [ 231 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 233 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PRINT SPACE(2): OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PRINT SPACE(2): OUT.AMOUNT<1,I>
        END
    NEXT I
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"
    PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"

    RETURN
*==================================================================
