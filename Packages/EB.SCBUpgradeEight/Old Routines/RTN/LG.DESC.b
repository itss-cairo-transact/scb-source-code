* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.DESC(OP.CODE,DESC)

    IF OP.CODE EQ '1111' THEN
        DESC="�����"
    END
    IF OP.CODE EQ '1231' THEN
        DESC='��'
    END
    IF OP.CODE EQ '1232' THEN
        DESC = '�����'
    END
    IF OP.CODE EQ '1233' THEN
        DESC = '�����'
    END
    IF OP.CODE EQ '1234' THEN
        DESC = '����� �� ��'
    END
    IF OP.CODE EQ '1235' THEN
        DESC = '����� �� ��'
    END
    IF OP.CODE EQ '1239' THEN
        DESC = '������ �����'
    END
    IF OP.CODE EQ '1241' THEN
        DESC = '����� �����'
    END
    IF OP.CODE EQ '1242' THEN
        DESC = '����� �����'
    END
    IF OP.CODE EQ '1251' THEN
        DESC = '����� �����'
    END
    IF OP.CODE EQ '1301'OR OP.CODE EQ '1302' OR OP.CODE EQ '1303' THEN
        DESC = '�����'
    END
    IF OP.CODE EQ '1309' THEN
        DESC= '����� ����'
    END
    IF OP.CODE EQ '1401' OR OP.CODE EQ '1402' OR OP.CODE EQ '1403' THEN
        DESC = '������'
    END
