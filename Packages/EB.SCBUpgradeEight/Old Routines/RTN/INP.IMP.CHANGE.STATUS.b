* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE INP.IMP.CHANGE.STATUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROJECT.IMP.STATUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET


    FN.IMP = 'F.SCB.PROJECT.IMP.STATUS'; F.IMP = ''
    CALL OPF(FN.IMP,F.IMP)
    R.IMP = ''

**    OPEN FN.IMP TO FVAR.IMP ELSE
    CALL OPF ('FN.IMP', FVAR.IMP)
    TEXT = "ERROR OPEN FILE" ; CALL REM
    RETURN
**END

    TSK.ID     = ID.NEW
    COMP       = 30004

    IF V$FUNCTION = 'I'  THEN

        CALL F.READ( FN.IMP,TSK.ID, R.IMP, F.IMP, ETEXT)

        R.IMP<WN.PROGRESS.STATUS>      = COMP
        R.IMP<WN.OVER.ALL.COMLETATION> = 100

        CALL F.WRITE (FN.IMP, TSK.ID, R.IMP)

 ***       WRITE R.IMP TO FVAR.IMP , TSK.ID  ON ERROR
 ***           STOP 'CAN NOT WRITE RECORD ':TSK.ID:' TO FILE ':F.IMP
 ***       END

    END
    RETURN
END
