* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LC.TYPE.IMP.ALL(ENQUIRY.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""

    LOCATE 'APPLICANT.ACC' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        APPL.ACC = ENQUIRY.DATA<4,RB.POS>
    END

    LOCATE 'ISSUE.DATE' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        ISS.DATE = ENQUIRY.DATA<4,RB.POS>
    END

    GE.DATE = FIELD(ISS.DATE,' ',1)
    LE.DATE = FIELD(ISS.DATE,' ',2)

*    TEXT = "GE.DATE : " : GE.DATE  ; CALL REM
*    TEXT = "LE.DATE : " : LE.DATE  ; CALL REM

    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH (LC.TYPE EQ DIA OR LC.TYPE EQ DIC OR LC.TYPE EQ DID OR LC.TYPE EQ DIF OR LC.TYPE EQ DIM OR LC.TYPE EQ DIS)  AND APPLICANT.ACC EQ ":APPL.ACC:" AND (ISSUE.DATE GE ":GE.DATE:" AND  ISSUE.DATE LE ":LE.DATE:") AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

 *   TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
* END ELSE
    END ELSE
        ENQ<2,I> = '@ID'
        ENQ<3,I> = 'EQ'
        ENQ<4,I> = 'DUMMY'
    END


* END

*******************************************************************************************
    RETURN
END
