* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
**** this routine To Default Customer Name by close if posting restriction eq 99 for customer banks ******
**** and not close the customer if have any application id
**** all check dependent for POSTING.RESTRICT FLD *** regenerate by Nessma 2010/11/28
**********************************************************************************************************
    SUBROUTINE GET.USED.CUS(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------------------

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"   ; F.LD = ""

    FN.LMM = "FBNK.LMM.CUSTOMER"   ; F.LMM = ""

    FN.LI = "FBNK.LIMIT.LIABILITY"   ; F.LI = ""

    FN.LC = "FBNK.LC.APPLICANT"   ; F.LC = ""
    FN.LCC = "FBNK.LETTER.OF.CREDIT"   ; F.LCC = ""
*------------------------------------------------------
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'    ;  F.ACC = ''; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
*-----------------------------------------*
    DATA.FLAG = 0

    YTEXT = "Enter Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    START.DATE = COMI
    IF START.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END
*--------
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    END.DATE = COMI
    IF END.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END
*-------------------------------------------*
    T.SEL  = "SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":START.DATE:" AND CONTACT.DATE LE ":END.DATE
    T.SEL := " AND COMPANY.BOOK EQ ":COMP.ID:" AND TEXT UNLIKE BR... BY CONTACT.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*-------------------------------------------*
    IF SELECTED THEN
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "NE"
        ENQ<4,1> = START.DATE :"*": END.DATE
        II = 2
        FOR H = 1 TO SELECTED
            FLAG = 0
            CUST.NO  = KEY.LIST<H>

            CALL DBR('CUSTOMER.ACCOUNT':@FM:EB.CAC.ACCOUNT.NUMBER,CUST.NO,CUS.ACC)
            IF CUS.ACC NE '' THEN
                FLAG = 1
            END
            IF FLAG EQ 0 THEN
                CALL OPF(FN.LD, F.LD)
                CALL OPF(FN.LMM,F.LMM)
                GOSUB CHK.LD
            END
            IF FLAG EQ 0 THEN
                CALL OPF(FN.LI,F.LI)
                GOSUB CHK.LIMIT
            END
            IF FLAG EQ 0 THEN
                CALL OPF(FN.LC,F.LC)
                CALL OPF(FN.LCC,F.LCC)
                GOSUB CHK.LC
            END
            IF FLAG EQ 0 THEN
                ENQ<2,II> = "@ID"
                ENQ<3,II> = "EQ"
                ENQ<4,II> = KEY.LIST<H>
                II = II + 1
            END

        NEXT H
    END
    ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
    RETURN
*-------------------------------------------------------
CHK.LD:
*------
    CALL F.READ(FN.LMM,CUST.NO,R.LMM,F.LMM,ER.LMM)
    LOOP
        REMOVE CONTRACT.ID FROM R.LMM SETTING POS.LMM
    WHILE CONTRACT.ID:POS.LMM
        IF CONTRACT.ID[1,2] EQ 'LD' THEN
            LD.ID = CONTRACT.ID
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ETEXT.LD)
            AMT.LD = R.LD<LD.AMOUNT>
            IF AMT.LD GT 0 THEN
                FLAG   = 1
            END
        END
    REPEAT
    RETURN
*-----------------------------------------------------
CHK.LIMIT:

    CALL F.READ(FN.LI,CUST.NO,R.LI,F.LI,ER.LI )
    LOOP
        REMOVE LI.ID FROM R.LI SETTING POS.LI
    WHILE LI.ID:POS.LI
        VAR.Y = LI.ID
        VAR.Y = FIELD(VAR.Y,'.',2)

        IF VAR.Y NE '0009900' AND VAR.Y NE '0009700' THEN
            FLAG = 1
        END
    REPEAT
    RETURN
*-----------------------------------------------------
CHK.LC:
    CALL F.READ(FN.LC,CUST.NO,R.LC,F.LC,ER.LC )
    LOOP
        REMOVE LC.ID FROM R.LC SETTING POS.LC
    WHILE LC.ID:POS.LC
        CALL F.READ(FN.LCC,LC.ID,R.LCC,F.LCC,ETEXT.LD)
        WS.LCC.CHK =          R.LCC<TF.LC.OPERATION>
        IF WS.LCC.CHK NE "" THEN
            FLAG = 1
        END
    REPEAT
    RETURN
*--------------------- END OF ROUTINE ----------------
END
