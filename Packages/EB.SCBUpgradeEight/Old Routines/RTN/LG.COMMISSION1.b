* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>1371</Rating>
*-----------------------------------------------------------------------------
** ----- 09.06.2003 (MOHAMED MAHMOUD) -----

    SUBROUTINE LG.COMMISSION1


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    DEFFUN SHIFT.DATE( )

    IF MESSAGE # 'VAL' THEN

        FN.LG.CHARGE= 'F.SCB.LG.CHARGE' ; F.LG.CHARGE = ''
        CALL OPF(FN.LG.CHARGE,F.LG.CHARGE)

        W.CUR           = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
        W.EXP.DATE      = R.NEW(LD.FIN.MAT.DATE)
        W.MARGIN.AMT    = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
        W.ISSUE.DATE    = R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
        W.PRODUCT.TYPE  = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
        W.OPER.NO       = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:'.':W.PRODUCT.TYPE
        START.DATE      = W.ISSUE.DATE
        OPER.NO         = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        W.END.COMM.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
        COMM.NO         = 1
        COMM.AMT1       = 0
        FLG1            = ''
        FLG2            = ''
        FLG3            = ''
        FLG4            = ''
        DEAL.AMOUNT = R.NEW(LD.AMOUNT)
        DEAL.CURRENCY = R.NEW(LD.CURRENCY)
**************************************************
        W.EXP.DATE      = R.NEW(LD.FIN.MAT.DATE)
        W.ISSUE.DATE    = R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>

        W.EXP = W.EXP.DATE[7,2]
        W.ISSUE = W.ISSUE.DATE[7,2]
        DIFF.DAY = W.EXP - W.ISSUE
        W.EXP.REST =  W.EXP.DATE[1,6]

        IF DIFF.DAY LE '2' THEN
            IF DIFF.DAY GT 0 THEN
                W.EXP.DATE= W.EXP.REST:W.ISSUE

            END
        END ELSE
            W.EXP.DATE      = R.NEW(LD.FIN.MAT.DATE)

        END
**************************************************

        IF OPER.NO = '2111' THEN
            GOSUB COMM.CALC
        END

*                  ................................................
*----------------- Applied In Cases For (OPERATIVE FOR ADVANCE L/Gs --------------------
*                  ................................................

        IF OPER.NO = 2251 THEN
            W.ISSUE.DATE   = R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.DATE>
            START.DATE     = W.ISSUE.DATE
*DEAL.AMOUNT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>  ;*[4,LEN1-3]
            DEAL.AMOUNT    = R.NEW(LD.AMOUNT)
            GOSUB COMM.CALC
        END

*                  ..................................................
*----------------- Applied In Cases For Change Of (EXP.DATE & AMOUNT) --------------------
*                  ..................................................

        IF OPER.NO = 2231 THEN

            IF W.END.COMM.DATE >= W.EXP.DATE THEN
                W.MARGIN.AMT = DEAL.AMOUNT
                FLG1 = 'N'
            END ELSE
                START.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            END
            GOSUB COMM.CALC

        END

        IF OPER.NO = 2234 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> < '100' THEN
            W.EXP.DATE     = R.OLD(LD.FIN.MAT.DATE)
            DEAL.AMOUNT    = R.NEW(LD.AMOUNT.INCREASE)
            W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            FLG2 = 'N'
            FLG3 = 'N'
            IF W.END.COMM.DATE >= W.EXP.DATE THEN
                FLG4 = 'N'
            END
            GOSUB COMM.CALC

            FLG4 = '' ; FLG2 = '' ; FLG3 = ''
            COMM.AMT1 = CHARGE.AMOUNT
            W.EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
            IF W.END.COMM.DATE < W.EXP.DATE THEN
                DEAL.AMOUNT    = R.NEW(LD.AMOUNT.INCREASE) + R.NEW(LD.AMOUNT)
                W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
                START.DATE     = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
                START.DATE = SHIFT.DATE( START.DATE, '1D', 'UP')

                GOSUB COMM.CALC
            END

        END

        IF OPER.NO = 2235  THEN
            DEAL.AMOUNT    = R.NEW(LD.AMOUNT) + R.NEW(LD.AMOUNT.INCREASE)

            IF W.END.COMM.DATE >= W.EXP.DATE THEN
                W.MARGIN.AMT = DEAL.AMOUNT
                FLG1 = 'N'
            END ELSE
                START.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            END

            GOSUB COMM.CALC
        END

        IF OPER.NO = 2232 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> < '100' THEN
            DEAL.AMOUNT    = R.NEW(LD.AMOUNT.INCREASE)
            W.MARGIN.AMT   = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            FLG2 = 'N' ; FLG3 = 'N'

            GOSUB COMM.CALC
        END

        IF OPER.NO=2233 OR (OPER.NO=2232 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>='100') THEN
            W.MARGIN.AMT = DEAL.AMOUNT
            COMM.NO      = 1
            FLG1         = 'N'

            GOSUB COMM.CALC
        END

        IF OPER.NO=2234 AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>='100' THEN
            W.MARGIN.AMT = DEAL.AMOUNT
            COMM.NO      = 1
            FLG1         = 'N'

            GOSUB COMM.CALC
        END
    END

    RETURN

*                  ........................................
*----------------- To Calculate ( END.COMM.DATE & COMM.NO ) --------------------
*                  ........................................

COMM.CALC:
*-------------
    IF FLG1 # 'N' THEN
        TMP.NO = 0

        FOR I = 1 TO 100
            START.DATE = SHIFT.DATE( START.DATE, '3M', 'UP')
            TMP.DATE = START.DATE
            IF TMP.NO = 0 AND FLG3 = 'N' AND TMP.DATE > TODAY THEN
                TMP.NO = I - 1
            END
            CALL CDT('', TMP.DATE, '2')
            IF TMP.DATE > W.EXP.DATE THEN
                COMM.NO = I - TMP.NO ; I = 100
            END
        NEXT I

        CALL CDT('', START.DATE, '-1')

        IF  R.NEW(LD.LOCAL.REF)<1,LDLR.ADV.OPERATIVE> ="N" AND W.OPER.NO = '2111.INAD' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = W.EXP.DATE ; CALL REBUILD.SCREEN
        END ELSE
            IF FLG2 # 'N' AND W.MARGIN.AMT # DEAL.AMOUNT THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = START.DATE ; CALL REBUILD.SCREEN
            END
        END
    END
*--------------------------------------------------------------------------------

    CALL F.READ(FN.LG.CHARGE,W.OPER.NO,R.OPER,F.LG.CHARGE,E1)
    LOCATE W.CUR IN R.OPER<SCB.LG.CH.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
    W.CHARGE.CODE = R.OPER<SCB.LG.CH.CHARGE.CODE,J>

**                                 = ................. =
**                                === ............... ===
**                               =====  ISSUEING L/G =====
**                                === ............... ===
**                                = ................. =

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.UNLIMITED> EQ "YES" THEN
        COMM.NO = 1
    END

    IF COMM.NO GT 4 THEN
        COMM.NO = 4
    END
*Line [ 232 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    FOR K = 1 TO DCOUNT(W.CHARGE.CODE,@SM)

        IF R.OPER<SCB.LG.CH.RATE,J,K> = '0' THEN
            IF K = 1 THEN
                IF R.NEW(LD.VALUE.DATE) GT W.ISSUE.DATE THEN
                    CHARGE.AMOUNT = (R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K> + COMM.AMT1) * 2
                END
                IF R.NEW(LD.VALUE.DATE) LE W.ISSUE.DATE THEN
                    CHARGE.AMOUNT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K> + COMM.AMT1
                END
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.ADV.OPERATIVE> EQ "NO" THEN
                    CHARGE.AMOUNT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                END
            END ELSE
                IF FLG4 # 'N' THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,K> = R.OPER<SCB.LG.CH.CHARGE.CODE,J,K>
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,K> = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                END
            END
            CALL REBUILD.SCREEN
        END ELSE
            IF R.OPER<SCB.LG.CH.RATE,J,K> > '0' THEN
                COMM.AMT = DEAL.AMOUNT * R.OPER<SCB.LG.CH.RATE,J,K>
                IF COMM.AMT < R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K> THEN
                    COMM.AMT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                END
            END ELSE
                COMM.AMT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
            END
            IF K = 1 THEN
                IF R.NEW(LD.VALUE.DATE) GT W.ISSUE.DATE THEN
                    CHARGE.AMOUNT = (COMM.AMT * COMM.NO) + COMM.AMT1 + R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,K> = CHARGE.AMOUNT
                END
                IF R.NEW(LD.VALUE.DATE) LE W.ISSUE.DATE THEN
                    CHARGE.AMOUNT = (COMM.AMT * COMM.NO) + COMM.AMT1
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,K> = CHARGE.AMOUNT
                END
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.ADV.OPERATIVE> EQ "NO" THEN
                    CHARGE.AMOUNT = R.OPER<SCB.LG.CH.MIN.AMOUNT,J,K>
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,K> = CHARGE.AMOUNT
                END
            END ELSE
                IF FLG4 # 'N' THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,K> = R.OPER<SCB.LG.CH.CHARGE.CODE,J,K>
                    R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,K> = COMM.AMT * COMM.NO
                END
            END
            CALL REBUILD.SCREEN
        END
    NEXT K

    RETURN
END
