* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LAST.WDAY(REQ.DATE)
************************************************************************
***********by Mahmoud Elhawary ** 5/1/2009 *****************************
************************************************************************
    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE
************************************************************************
*convert the date required to the last working day of the month
************************************************************************
    TDR   = REQ.DATE
    TDRMN = TDR[5,2]
    TDRYY = TDR[1,4]
    IF TDRMN NE '12' THEN
        TDNMN = TDRMN + 1
    END ELSE
        TDNMN = '01'
        TDRYY = TDRYY + 1
    END
    X = LEN(TDNMN)
    IF X = 1 THEN
        TDNMN = '0':TDNMN
    END
    TDREM = TDRYY:TDNMN:'01'
    CALL CDT("",TDREM,'-1W')
    TDD = TDREM
    REQ.DATE = TDD
    RETURN
