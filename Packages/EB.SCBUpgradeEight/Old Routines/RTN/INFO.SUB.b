* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>386</Rating>
*-----------------------------------------------------------------------------

** ----- 00.01.2004 Pawel TEMENOS -----
SUBROUTINE INFO.SUB( R.DL.DEFINE, REPORT)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DL.DEFINE

** ----- ----- -----
MAIN:
** ----- ----- -----

      DL.ITEMS = ''

      ITEM.NO = DCOUNT( R.DL.DEFINE< DL.DEF.FILE.NAME>, @VM)
      FOR DL.IDX = 1 TO ITEM.NO

         FILE.NAME = R.DL.DEFINE< DL.DEF.FILE.NAME, DL.IDX>

         FILE.NO = DCOUNT( DL.ITEMS, @FM) ; FILE.IDX = FILE.NO + 1
         FOR IDX = 1 TO FILE.NO

            IF DL.ITEMS< IDX, 1> >= FILE.NAME THEN FILE.IDX = IDX ; EXIT

         NEXT IDX
         IF DL.ITEMS< FILE.IDX, 1> # FILE.NAME THEN INS FILE.NAME BEFORE DL.ITEMS< FILE.IDX>

         RECORD.NAME = R.DL.DEFINE< DL.DEF.RECORD.NAME, DL.IDX>

         RECORD.NO = DCOUNT( DL.ITEMS< FILE.IDX, 2>, @SM) ; RECORD.IDX = RECORD.NO + 1
         FOR IDX = 1 TO RECORD.NO

            IF DL.ITEMS< FILE.IDX, 2, IDX> >= RECORD.NAME THEN RECORD.IDX = IDX ; EXIT

         NEXT IDX
         IF DL.ITEMS< FILE.IDX, 2, RECORD.IDX> # RECORD.NAME THEN INS RECORD.NAME BEFORE DL.ITEMS< FILE.IDX, 2, RECORD.IDX>

      NEXT DL.IDX

      ** ----- ----- -----

      FILE.NAME.NO = DCOUNT( DL.ITEMS, @FM)
      FOR FILE.NAME.IDX = 1 TO FILE.NAME.NO

         CALL WRITE.REPORT.LINE( 1, REPORT, '')
         CALL WRITE.REPORT.LINE( 1, REPORT, ':: ' : DL.ITEMS< FILE.NAME.IDX, 1> : ' ::')
         CALL WRITE.REPORT.LINE( 1, REPORT, '')

         RECORD.NAME.NO = DCOUNT( DL.ITEMS< FILE.NAME.IDX, 2>, @SM)
         FOR RECORD.NAME.IDX = 1 TO RECORD.NAME.NO

            CALL WRITE.REPORT.LINE( 2, REPORT, FMT( RECORD.NAME.IDX, 'R%4') : ' : ' : DL.ITEMS< FILE.NAME.IDX, 2, RECORD.NAME.IDX>)

         NEXT RECORD.NAME.IDX

      NEXT FILE.NAME.IDX

      RETURN

** ----- ----- -----

END
