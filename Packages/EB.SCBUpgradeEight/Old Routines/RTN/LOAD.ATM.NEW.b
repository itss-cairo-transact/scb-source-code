* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    PROGRAM LOAD.ATM.NEW

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS

    EXECUTE "OFLIN"
*================ Read User From Voc =======================
    FN.TM = "VOC" ; F.TM = ''
    CALL OPF(FN.TM,F.TM)
    CALL F.READ(FN.TM,'CHK.LIN',R.TM,F.TM,ER10)
    IF R.TM EQ 'CAIRO' THEN
*================ End Read User From Voc ========================
        EXECUTE "clear"
        EXECUTE "sh ATM.OF.LINE/OBJ/get.log.to.live.sh"
*==================================================================
        PROMPT "DO YOU WANT TO CONTINUE Y / N ===> " ; INPUT  AA
        IF AA = "Y" OR AA = "y" THEN
*=============== Check If Program Run Before ====================
            KEY.LIST.CHK="" ; SELECTED.CHK="" ;  ER.FT.CHK=""
            T.SEL.CHK = "SELECT FBNK.FUNDS.TRANSFER WITH MESSAGE.DESC = ATM.OF.LINE"

            CALL EB.READLIST(T.SEL.CHK,KEY.LIST.CHK,"",SELECTED.CHK,ER.FT.CHK)
            IF SELECTED.CHK THEN
                PRINT "*** ERROR *** THIS PROGRAM IS DONE BEFORE PLEASE CHECK FIRST"
            END ELSE
*=============== End Check If Program Run Before ====================


                FLG = 0 ; KK = 3
                SCB.OFS.SOURCE = "TESTOFS"
                OPENSEQ "ATM.ARG" , "ATM.ARG.LOAD.OUT" TO BB THEN
                    CLOSESEQ BB
                    HUSH ON
                    EXECUTE 'DELETE ':"ATM.ARG":' ':"ATM.ARG.LOAD.OUT"
                    HUSH OFF

                END
                OPENSEQ "ATM.ARG" , "ATM.ARG.LOAD.OUT" TO BB ELSE
                    CREATE BB THEN
                    END ELSE
                        STOP 'Cannot create ATM.ARG.OUT File IN ATM.ARG'
                    END
                END
*===========================
                OPENSEQ "ATM.ARG" , "ATM.ARG.LOAD.IN" TO BB.IN THEN
                    CLOSESEQ BB.IN
                    HUSH ON
                    EXECUTE 'DELETE ':"ATM.ARG":' ':"ATM.ARG.LOAD.IN"
                    HUSH OFF

                END
                OPENSEQ "ATM.ARG" , "ATM.ARG.LOAD.IN" TO BB.IN ELSE
                    CREATE BB.IN THEN
                    END ELSE
                        STOP 'Cannot create ATM.ARG.LOAD.IN File IN ATM.ARG.LOAD.IN'
                    END
                END
*===========================
                OFS.MESSAGE.DATA = ''

                EOF = ''
******* READ SOURCE FILE **********************
                SEQ.FILE.NAME = 'ATM.OF.LINE'
                RECORD.NAME = 'ATM.ARG.OUT'
                OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
                    TEXT = 'Unable to Locate ':SEQ.FILE.POINTER  ; CALL REM
                    STOP
                    RETURN
                END
                EOF1 = ''
                LOOP WHILE NOT(EOF1)
                    READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN

                        CHK.OPER = FIELD(Y.MSG,",",1)

                        IF CHK.OPER = "FUNDS.TRANSFER" OR CHK.OPER = "AC.LOCKED.EVENTS" OR CHK.OPER = "ENQUIRY.SELECT" THEN
                            IF CHK.OPER = "FUNDS.TRANSFER"   THEN Y.MSG := ",MESSAGE.DESC::=ATM.OF.LINE"
                            IF CHK.OPER = "AC.LOCKED.EVENTS" THEN Y.MSG := ",DESCRIPTION::=ATM.OF.LINE"
                            SCB.OFS.MESSAGE = Y.MSG
**********************
                        END ELSE
                            IF CHK.OPER[1,2] = "FT" OR CHK.OPER[1,4] = "ACLK" THEN
                                CHK.RESPONS = FIELD(Y.MSG,"/",3)
                                IF CHK.RESPONS[1,1] = 1 THEN
                                    BB.DATA  = SCB.OFS.MESSAGE
                                    WRITESEQ BB.DATA TO BB.IN ELSE
                                    END
* SCB R15 UPG 20160717 - S
*                                    CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                                    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E

                                    BB.DATA  = SCB.OFS.MESSAGE
                                    DISPLAY BB.DATA
                                    WRITESEQ BB.DATA TO BB ELSE
                                    END
                                END
                            END
                        END
                    END ELSE
                        EOF1 = 1
                    END

                REPEAT
***** UPDATED BY KHALED 2012/09/25 ****
                WS.AMT.FT = 0 ; COUNT.FT  = 0

                FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
                CALL OPF(FN.FT,F.FT)

                TRN.TYPE = 'AC3':'...'

                T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH MESSAGE.DESC = ATM.OF.LINE"
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
                IF SELECTED THEN
                    FOR I = 1 TO SELECTED
                        CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,E1)
                        WS.AMT.FT += R.FT<FT.LOC.AMT.DEBITED>
                        COUNT.FT   = I
                    NEXT I
                END
*====================== START 2014/05/13 ===============================================
                FN.LK = 'FBNK.AC.LOCKED.EVENTS' ; F.LK = ''
                CALL OPF(FN.LK,F.LK)
                WS.AMT.LK = 0 ; COUNT.LK = 0 ; CHK.DAT.LK = ''
                CHK.DAT.LK = TODAY
                CALL CDT("",CHK.DAT.LK,'-1W')

                T.SEL.LK = '' ; T.SEL.LK = '' ; KEY.LIST.LK = '' ; SELECTED.LK = '' ; ER.FT.LK = ''
                T.SEL.LK = "SELECT FBNK.AC.LOCKED.EVENTS WITH FROM.DATE GE ":CHK.DAT.LK:" AND INPUTTER LIKE ...ATM123..."
                CALL EB.READLIST(T.SEL.LK,KEY.LIST.LK,"",SELECTED.LK,ER.FT.LK)
                IF SELECTED.LK THEN
                    FOR I = 1 TO SELECTED.LK
                        CALL F.READ(FN.LK,KEY.LIST.LK<I>,R.LK,F.LK,E1)
                        WS.AMT.LK += R.LK<AC.LCK.LOCKED.AMOUNT>
                        COUNT.LK   = I
                    NEXT I
                END
*====================== END 2014/05/13 ==================================================

                SEQ.FILE.NAME = 'ATM.OF.LINE'
                RECORD.NAME = 'ATM.ARG.BAL.FT'
                OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
                    TEXT = 'Unable to Locate ':SEQ.FILE.POINTER  ; CALL REM
                    STOP
                    RETURN
                END
                EOF1 = ''

                LOOP WHILE NOT(EOF1)

                    READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN

                        WS.AMT.FILE      = FIELD(Y.MSG,",",1)
                        WS.COUNT.FILE    = FIELD(Y.MSG,",",2)
                        WS.AMT.FILE.LK   = FIELD(Y.MSG,",",3)
                        WS.COUNT.FILE.LK = FIELD(Y.MSG,",",4)


                    END ELSE
                        EOF1 = 1
                    END

                REPEAT
*========================================================================================
                FLAG.FT = 0 ; FLAG.LK = 0

                IF (WS.AMT.FT EQ WS.AMT.FILE) AND (COUNT.FT EQ WS.COUNT.FILE) THEN
                    PRINT 'FT CHECK IS OK'
                    FLAG.FT = 1
                END ELSE
                    PRINT 'ERROR IN FT PLEASE REVIEW FIRST'
                END
                IF (WS.AMT.FILE.LK EQ WS.AMT.LK) AND (WS.COUNT.FILE.LK EQ COUNT.LK ) THEN
                    PRINT 'POS CHECK IS OK'
                    FLAG.LK = 1
                END ELSE
                    PRINT 'ERROR IN POS PLEASE REVIEW FIRST'
                END
***************************************
                IF FLAG.FT = 1 AND FLAG.LK = 1 THEN
                    EXECUTE "sh ATM.OF.LINE/OBJ/strt.switch.7.sh"
                    PRINT 'DONE'
                END
            END
        END
    END
    RETURN
END
