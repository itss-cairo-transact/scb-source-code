* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-101</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.CONFIS.CHEQ

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB INITIATE
*Line [ 45 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)

    GOSUB PRINT.HEAD
    GOSUB BODY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*==============================================================
INITIATE:
    REPORT.ID='LG.CONFIS.CHEQ'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
PRINT.HEAD:
    PR.HD ="'L'":SPACE(1):"������ : ����� ����� �������"
    PR.HD :="'L'":SPACE(1):"���� �������� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',28)
    HEADING PR.HD
    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = ''
    CALL OPF(FN.CUST,F.CUST)


    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    ISS.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    ISSUE.DATE = ISS.DATE[7,2]:"/":ISS.DATE[5,2]:"/":ISS.DATE[1,4]
    FIN.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = FIN.DATE[7,2]:"/":FIN.DATE[5,2]:"/":FIN.DATE[1,4]
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.REF = LOCAL.REF<1,LDLR.SENDING.REF>

    LG.CONFIS.PART=LOCAL.REF<1,LDLR.CONFISC.AMT>
    IF LG.CONFIS.PART THEN
        LG.CONFIS =LOCAL.REF<1,LDLR.CONFISC.AMT>
    END ELSE
        LG.CONFIS = R.LD<LD.AMOUNT>
    END
    CUR=R.LD<LD.CURRENCY>
    CUR.CO=LOCAL.REF<1,LDLR.ACC.CUR>

    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME

    MYCODE =LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
    CUST.ID = R.LD<LD.CUSTOMER.ID>
    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
*   CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.ID,AC.OFICER)

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YYBRN = FIELD(BRANCH,'.',1)
**********************

    CALL F.READ(FN.CUST,CUST.ID,R.CUST,F.CUST,E1)
    CUST.NAME=R.CUST<EB.CUS.SHORT.NAME>

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT> THEN
        CUST.ADD = LOCAL.REF<1,LDLR.FREE.TEXT>
    END ELSE
        CUST.ADD=R.CUST<EB.CUS.STREET>
    END
    SEND.BANK=LOCAL.REF<1,LDLR.SEN.REC.BANK>
    CALL F.READ(FN.CUST,SEND.BANK,R.CUST,F.CUST,E1)
    SEND.NAME=R.CUST<EB.CUS.SHORT.NAME>
    CORR=SEND.NAME
    SEND.ADD=R.CUST<EB.CUS.STREET>
    LC.REF=LOCAL.REF<1,94>
********************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
    LG.NO='LG/': SAM:"/": SAM1:"/":SAM2:'-':LC.REF
********************
    CUR=R.LD<LD.CURRENCY>
    FN.CUR='F.CURRENCY'
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,E1)
    CRR=R.CUR<EB.CUR.CCY.NAME,2>


    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    RETURN
*===============================================================
BODY:
    PRINT;PRINT

    PRINT SPACE(5):"����� ������ ������ ��� ����� �����":" ": LG.CONFIS:" ":CRR:" " :"���� ��� ������"
    PRINT

    PRINT SPACE(3):"��� �������":"   ": CORR:"   " : " ���  ����   ����  ���" :" ":LG.NO
    PRINT

    PRINT SPACE(3):"���� ��� ���� �� ������ �� ��� �����" :" ."
    PRINT;PRINT;PRINT;PRINT

    PRINT SPACE(50):"����� ������ ������"
    PRINT SPACE(50):"���� �������� ��������"
    PRINT;PRINT
    PRINT  "������ �� :":XX
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

    RETURN
END
*==================================================================
