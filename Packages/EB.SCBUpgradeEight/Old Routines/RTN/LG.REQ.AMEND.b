* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>-269</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE LG.REQ.AMEND

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB INITIATE
*Line [ 45 ] Adding EB.SCBUpgradeEight. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB BODY

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='LG.REQ.AMEND'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
* YTEXT = "Enter the L/G. No. : "
*CALL TXTINP(YTEXT, 8, 22, "12", "A")

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>

    APPROVE=LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL1 =LOCAL.REF<1,LDLR.GUARRANTOR,1>
    SUPPL2 =LOCAL.REF<1,LDLR.GUARRANTOR,2>

    ADDR1 =LOCAL.REF<1,LDLR.FREE.TEXT,1>
    ADDR2 =LOCAL.REF<1,LDLR.FREE.TEXT,2>
    ADDR3 =LOCAL.REF<1,LDLR.FREE.TEXT,3>

    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.ISSUE.DATE=LOCAL.REF<1,LDLR.ISSUE.DATE>
    LG.MAT.DATE=LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    SEND.REC=LOCAL.REF<1,LDLR.SENDING.REF>
    LC.REF=LOCAL.REF<1,94>
    DATX = LG.ISSUE.DATE
    XX = DATX[7,2]:'/':DATX[5,2]:"/":DATX[1,4]
    DATY = LG.MAT.DATE
    YY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    IN.DATE = LG.MAT.DATE

    CALL DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)

    RETURN
*===============================================================
PRINT.HEAD:
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
    PR.HD ="'L'":SPACE(1):"��� ���� �����������":SPACE(40):"*":OPER.CODE:"*"
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,AC.OFICER)
    AC.OFICER = AC.OFICER[2]
    AC.OFICER = TRIM(AC.OFICER,"0","L")

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    MYBRANCH = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(1):"���� �������� ��������"
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD

    RETURN
*==================================================================
BODY:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"
* IF MYCODE[2,3]= "111" THEN
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"
* END ELSE
*   PRINT "| ������� : ":THIRD.NAME:SPACE(LNE1):"|"
*   PRINT "|         : ":SPACE(LNE2):"|"
*   PRINT "|":SPACE(49):"|"
*   PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
*   PRINT "|         : ":THIRD.ADDR1:SPACE(LNED2):"|"
*   PRINT "|_________________________________________________|"
* END
    PRINT
    PRINT
    PRINT SPACE(20): "���� �������� ��� ": LG.NO :"-":SEND.REC:" (":TYPE.NAME:") "
    PRINT
    PRINT SPACE(20): "����������������� : ":" **":LG.AMT:"**":" ":CRR
    PRINT
    PRINT SPACE(20): "������ ��� ������ : ":XX
    PRINT

    PRINT SPACE(20): "����������� � �����: ": SUPPL1
    PRINT SPACE(36): ":":SUPPL2
*//////////////////////////////////////////////////////
    PRINT
    PRINT SPACE(20): "________________________________________________"
    PRINT ; PRINT
    PRINT SPACE(7):"�������� ��� ���� ������ ������":" ":CORR.DATE:"������ ����� �� ���� ���� ������ ���� ����� "
    PRINT
    PRINT SPACE(5):"����� �� ������ ���� ������ ������ ������ ���� ���� ���� ����"
    PRINT
    PRINT SPACE(5):"����� �������� ���� ��� ����� �������� �� ��� ����� � �������� ��� ��� �� ���� ."
    PRINT
    PRINT SPACE(5):"��� ����� ����� ��� ���� ���� ������ ������� �� ���� ��� ������ ������� ��� ����"
    PRINT
    PRINT SPACE(5):"������� �� ��� ����� ��� �� ��� ��� ���� ������ ��� ����� �� ���� �������� ���� "
    PRINT
    PRINT SPACE(5):"����� �������� ����� � ���� ����� �� ��� ������ ������ ������ � ������ ������ "
    PRINT
    PRINT SPACE(7):"����� �� ��� � ���� ����� ����� ���������� ���� ��� ��� �� ."
    PRINT
    PRINT SPACE(5):"����� ������ �������� ���� ��� ��� ������ �� ������ ����� ����� ���� ������"
    PRINT
    PRINT SPACE(5):"���� ������ � ��� ������ �������� ������� ������� � ��� ����� �������� �����"
    PRINT
    PRINT SPACE(5):"����� ����� ������ �� ���� ���� ������ ������ ����� ����� ��� �������"
    PRINT
    PRINT SPACE(5):"�������� ��� �����":"��� �� �������� ������� ."
    PRINT;PRINT;PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���"   ;PRINT ;PRINT
    PRINT ; PRINT ; PRINT
    PRINT SPACE(50):"��� ��� ���� ������"
    PRINT  "������ �� :":XX
    TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*==================================================================
