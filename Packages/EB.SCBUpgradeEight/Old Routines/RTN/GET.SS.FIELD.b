* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
*-----------------------------------------------------------------------------
* <Rating>333</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE GET.SS.FIELD(APPL, Y.FLD, Y.FLD.NAME, NUM.NAME, YERROR)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDARD.SELECTION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LOCAL.REF.TABLE

    GOSUB INITIALISATION
    IF NOT(YERROR) THEN
        GOSUB GET.FLD.VAL.SVAL
        GOSUB CHECK.FLD
    END

    RETURN
********************************************************************************
GET.FLD.NAME:

    FLD.NAME = ''
    LOCATE FLD IN R.SS<SSL.SYS.FIELD.NO, 1> SETTING J THEN
        FLD.NAME = R.SS<SSL.SYS.FIELD.NAME, J>
    END

    RETURN
********************************************************************************
CHECK.FLD:

    IF NUM(FLD) THEN
        GOSUB GET.FLD.NAME
    END
    ELSE
        FLD.NAME = FLD
    END
*
    IF FLD.NAME = 'LOCAL.REF' THEN
        IF NOT(VAL) THEN
            YERROR = 'VALUE FIELD MISSING'
        END
        ELSE
            IF VAL > LR.COUNT THEN
                YERROR = 'INVALID FIELD'
            END
        END
    END
    IF NOT(YERROR) THEN
        LOCATE FLD.NAME IN R.SS<SSL.SYS.FIELD.NAME, 1> SETTING J THEN
            IF R.SS<SSL.SYS.TYPE, J> # 'D' THEN
                YERROR = 'INVALID FIELD'
            END
            ELSE
                IF R.SS<SSL.SYS.FIELD.NAME, J> = 'LOCAL.REF' THEN
                    IF NUM.NAME = 'NUM' THEN
                        Y.FLD.NAME = R.SS<SSL.SYS.FIELD.NO, J>:'.':VAL
                    END
                    ELSE
                        Y.FLD.NAME = 'LOCAL.REF-':VAL
                    END
                END
                ELSE
                    IF NUM.NAME = 'NUM' THEN
                        Y.FLD.NAME = R.SS<SSL.SYS.FIELD.NO, J>
                    END
                    ELSE
                        Y.FLD.NAME = R.SS<SSL.SYS.FIELD.NAME, J>
                    END
                END
            END
        END
        ELSE
            YERROR = 'NOT ON STANDARD.SELECTION'
        END
    END

    RETURN
********************************************************************************
GET.FLD.VAL.SVAL:

    FLD = '' ; VAL = '' ; SVAL = ''
    DASH.POS = INDEX(Y.FLD, '-', 1)
    IF DASH.POS THEN          ;* Field name entered
        FLD = FIELD(Y.FLD, '-', 1, 1)
        REMAING.FLD = Y.FLD[LEN(Y.FLD) - DASH.POS]
        DOT.POS = INDEX(REMAING.FLD, '.', 1)
        IF DOT.POS THEN
            VAL = REMAING.FLD[1, DOT.POS -1]
            SVAL = FIELD(REMAING.FLD, '.', 2)
        END
        ELSE
            VAL = REMAING.FLD
        END
    END
    ELSE  ;* Check if number or single value field name entered
        DOT.POS = INDEX(Y.FLD, '.', 1)
        IF DOT.POS THEN       ;* Check if multi value number or single field name
            TMP.FLD = FIELD(Y.FLD, '.', 1, 1)
            IF NUM(TMP.FLD) THEN        ;* Multi value number entered
                FLD = TMP.FLD
                VAL = FIELD(Y.FLD, '.', 2, 1)
                SVAL = FIELD(Y.FLD, '.', 3, 1)
            END
            ELSE    ;* Single value
                FLD = Y.FLD
            END
        END
        ELSE        ;* Single value
            FLD = Y.FLD
        END
    END

    RETURN
********************************************************************************
INITIALISATION:

    YERROR = ''
    Y.FLD.NAME = ''
**    OPEN 'F.STANDARD.SELECTION' TO VF.SS THEN
    CALL OPF ('F.STANDARD.SELECTION', VF.SS)
    R.SS = ''
**    READ R.SS FROM VF.SS, APPL THEN
    CALL F.READ('F.STANDARD.SELECTION',APPL, R.SS, VF.SS,ERR1)
    IF NOT(ERR1) THEN
        *OPEN 'F.LOCAL.REF.TABLE' TO VF.LRT THEN
            CALL OPF ('F.LOCAL.REF.TABLE' , VF.LRT)
            R.LRT = ''
**            READ R.LRT FROM VF.LRT, APPL THEN
            CALL F.READ('F.LOCAL.REF.TABLE' ,APPL, R.LRT, VF.LRT, ERR2)
            IF NOT(ERR2) THEN
*Line [ 149 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LR.COUNT = DCOUNT(R.LRT<EB.LRT.LOCAL.TABLE.NO>, @VM)
            END ELSE
                LR.COUNT = '0'
            END
            CLOSE VF.LRT
        *END
        *ELSE
        *    YERROR = 'UNABLE TO OPEN &':FM:'LOCAL.REF.TABLE'
        *END
    END
    ELSE
*Line [ 161 ] Add @VM Instead Of VM & Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        YERROR = 'MISSING & - RECORD. ID=&':@FM:'STANDARD.SELECTION':@VM:APPL
    END
    CLOSE VF.SS
**    END
**    ELSE
**        YERROR = 'UNABLE TO OPEN &':FM:'STANDARD.SELECTION'
**    END

    RETURN

END
