* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEight  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEight
*DONE
    SUBROUTINE IND.CUS(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    FN.CUS = 'FBNK.CUSTOMER'
    F.CUS = ''
    FN.IND = 'FBNK.INDUSTRY'
    F.IND = ''
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    KEY.LIST1=""
    SELECTED1=""
    ER.MSG1=""
    KK1=0

    SEL.CMD = "SELECT ":FN.IND:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(SEL.CMD,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR X = 1 TO SELECTED
            SEL.CMD1 = "SELECT ":FN.CUS:" WITH INDUSTRY EQ ":KEY.LIST<X>:" AND CO.CODE EQ ":COMP
            CALL EB.READLIST(SEL.CMD1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 LT 1 THEN
                KK1 ++
                ENQ<2,KK1> = '@ID'
                ENQ<3,KK1> = 'EQ'
                ENQ<4,KK1> = KEY.LIST<X>
            END
        NEXT X
    END
    RETURN
END
