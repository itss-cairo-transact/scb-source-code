* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
****NESSREEN AHMED 08/08/2010****
*-----------------------------------------------------------------------------
* <Rating>648</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.PREPARE.ACCT.DB

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.DB.ADVICE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.ROUTINE.CHK
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.MNTLY.PAY.AMT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

    TEXT = 'YARAB' ; CALL REM
    YYYY.MM = ''

    F.READ.DB.AD = '' ; FN.READ.DB.AD = 'F.SCB.VISA.DB.ADVICE'
    CALL OPF(FN.READ.DB.AD,F.READ.DB.AD)

    F.VISA.APP = '' ; FN.VISA.APP = 'F.SCB.VISA.APP'
    CALL OPF(FN.VISA.APP,F.VISA.APP)

    F.ACCT = '' ; FN.ACCT = 'F.ACCOUNT'
    CALL OPF(FN.ACCT,F.ACCT)

    F.RT.CHK = '' ; FN.RT.CHK = 'F.SCB.VISA.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)

    F.MNT.P = '' ; FN.MNT.P = 'F.SCB.VISA.MNTLY.PAY.AMT' ; R.MNT.P = '' ; E.MNT.P = ''
    CALL OPF(FN.MNT.P,F.MNT.P)

    F.CARD.ISS = '' ; FN.CARD.ISS = 'F.CARD.ISSUE'  ; R.CARD.ISS = '' ; E.CARD.ISS = '' ; ERR.CI = ''
    CALL OPF(FN.CARD.ISS,F.CARD.ISS)
**************************************************************
    DIR.NAME = '&SAVEDLISTS&'

    TEXT = 'TODAY=':TODAY ; CALL REM
    NEW.FILE = "VISA.MP.ERR.":TODAY
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
*************************************************************
***##########################################################
    YTEXT = "Enter the Required Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    YY = COMI[1,4]
    MON = COMI[5,2]
    YYYY.MM = COMI[1,6]
************************************************************
    CHK.SEL = "SELECT F.SCB.VISA.MNTLY.PAY.AMT WITH YEAR.MONTH EQ ": YYYY.MM
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        SER.NO = '' ; KEY.UPD = ''
*************************************************************
        CARD.NO = '' ; ACCT.NO = '' ; CUST.NO = '' ; CRD.COMP = '' ; DB.AMT = '' ; CARD.T = '' ; PAY.ACCT.NO = '' ; PAY.WAY = ''
        N.SEL = "SELECT F.SCB.VISA.DB.ADVICE WITH POST.DATE EQ ":COMI
        KEY.LIST.N ="" ; SELECTED.N="" ;  ER.MSG.N=""

        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        TEXT ='������ ����� ������ �� �����=':SELECTED.N ; CALL REM
        IF SELECTED.N THEN
            FOR NN = 1 TO SELECTED.N
                CALL F.READ(FN.READ.DB.AD,KEY.LIST.N<NN>,R.READ.DB.AD,F.READ.DB.AD,ERR.DB.AD)
                CARD.NO = R.READ.DB.AD<VDA.CARD.NO>
                DB.AMT  = R.READ.DB.AD<VDA.AMTOUNT>
                T.SEL = "SELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO EQ ":CARD.NO :" BY ACCOUNT "
                KEY.LIST.T ="" ; SELECTED.T="" ;  ER.MSG.T=""

                CALL EB.READLIST(T.SEL,KEY.LIST.T,"",SELECTED.T,ER.MSG.T)
                IF SELECTED.T THEN
                    CALL F.READ(FN.CARD.ISS, KEY.LIST.T<1>, R.CARD.ISS, F.CARD.ISS, E1)
                    ACCT.NO  = R.CARD.ISS<CARD.IS.ACCOUNT>
                    CARD.T = R.CARD.ISS<CARD.IS.LOCAL.REF,LRCI.CARD.CODE>
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
                    CRD.COMP = R.CARD.ISS<CARD.IS.CO.CODE>

                    A.SEL = "SELECT F.SCB.VISA.APP WITH MAIN.CUSTOMER EQ ": CUST.NO :" AND CARD.TYPE EQ " :CARD.T : " BY VISA.APP.DATE "
                    KEY.LIST.A = "" ; KEY.LIST.A = ""   ; ER.MSG.A = ""
                    CALL EB.READLIST(A.SEL ,KEY.LIST.A,"",SELECTED.A,ER.MSG.A)
                    XX = SELECTED.A
                    IF SELECTED.A THEN
                        ID.USE = KEY.LIST.A<XX>
                        CALL F.READ(FN.VISA.APP,ID.USE,R.VISA.APP,F.VISA.APP,ERR.APP)
                        PAY.WAY = R.VISA.APP<SCB.VISA.PAYMENT.WAY>
                        IF PAY.WAY EQ 'VISA.ACCOUNT' THEN
                            SER.NO = SER.NO + 1
                            KEY.UPD = SER.NO:".":TODAY
                            PAY.ACCT.NO = R.VISA.APP<SCB.VISA.PAY.ACCT.NO>

                            CALL F.READ(FN.MNT.P,KEY.UPD,R.MNT.P,F.MNT.P,E.MNT.P)
                            R.MNT.P <MPA.VISA.NO>     = CARD.NO
                            R.MNT.P <MPA.DB.ACCT.NO>  = PAY.ACCT.NO
                            R.MNT.P <MPA.CR.ACCT.NO>  = ACCT.NO
                            R.MNT.P <MPA.DEBIT.AMT>   = DB.AMT
                            R.MNT.P <MPA.YEAR.MONTH>  = YY:MON
                            R.MNT.P <MPA.COMPANY.CO>  = CRD.COMP

                            CALL F.WRITE(FN.MNT.P,KEY.UPD, R.MNT.P)
                            CALL JOURNAL.UPDATE(KEY.UPD)
                        END ELSE        ;**ELSE OF PAY.WAY
                            VISA.DATA = CARD.NO
                            VISA.DATA := "|Cust.no="
                            VISA.DATA := CUST.NO
                            VISA.DATA := "|Card.Type="
                            VISA.DATA := CARD.T
                            VISA.DATA := "|Pay.Way="
                            VISA.DATA := PAY.WAY
                            VISA.DATA := "|Pay.way.in application.is cash"
                            WRITESEQ VISA.DATA TO V.FILE.IN ELSE
                                PRINT  'CAN NOT WRITE LINE ':VISA.DATA
                            END
                        END   ;** END OF IF PAY.WAY EQ 'VISA.ACCOUNT' ***
                    END ELSE  ;**ELSE OF SELECTED.A
                        VISA.DATA = CARD.NO
                        VISA.DATA := "|Cust.no="
                        VISA.DATA := CUST.NO
                        VISA.DATA := "|Card.Type="
                        VISA.DATA := CARD.T
                        VISA.DATA := "|Card.Type.in.application.is.different"
                        WRITESEQ VISA.DATA TO V.FILE.IN ELSE
                            PRINT  'CAN NOT WRITE LINE ':VISA.DATA
                        END
                    END       ;** END OF SELECTED.A
                END ELSE      ;** ELSE OF SELECTED.T
                    VISA.DATA = CARD.NO
                    VISA.DATA := "|NotInCARDISSUE"
                    WRITESEQ VISA.DATA TO V.FILE.IN ELSE
                        PRINT  'CAN NOT WRITE LINE ':VISA.DATA
                    END
                END ;** END OF SELECTED.T
            NEXT NN
        END         ;** END OF IF SELECTED.N
*******************
**KEYID = YY:MON
**      CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
***      R.RT.CHK<RT.CHK.MNT.PRE.PAY> = 'YES'
**      CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
**      CALL JOURNAL.UPDATE(KEYID)
*********************************************************************
        TEXT = '�� ������� '; CALL REM
        END.SEL = "SELECT F.SCB.VISA.MNTLY.PAY.AMT WITH YEAR.MONTH EQ ": YYYY.MM
        KEY.LIST.END=""
        SELECTED.END=""
        ER.MSG.END=""

        CALL EB.READLIST(END.SEL,KEY.LIST.END,"",SELECTED.END,ER.MSG.END)
        TEXT = '������ ��������� ���� �� �������' : SELECTED.END ; CALL REM
    END

    RETURN
END
