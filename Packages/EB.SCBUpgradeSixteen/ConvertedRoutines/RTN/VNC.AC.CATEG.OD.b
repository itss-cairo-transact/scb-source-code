* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
** ----- 21.12.2004 INGY-SCB -----
*-----------------------------------------------------------------------------
* <Rating>90</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.AC.CATEG.OD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    IF V$FUNCTION EQ 'I' THEN

*** IF CUSTOMER.LIABILITY FOR THIS CUSTOMER HAVE LIMIT.PRODUCT = 100 THEN FORCE
*** LIMIT.REF =  '10100.01' AND LINK .TO.LIMIT = YES

        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CUST = ID.NEW[1,8]
        IF  LEN(CUST) = 8 AND CUST[1,1] = 0 THEN
            CUST1 = CUST[2,8]
        END ELSE
            CUST1 = CUST
        END


*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.CUSTOMER.LIABILITY,CUST1,CUST.LIAB)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST1,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.LIAB=R.ITSS.CUSTOMER<EB.CUS.CUSTOMER.LIABILITY>
        T.SEL = 'SELECT FBNK.LIMIT WITH LIABILITY.NUMBER EQ ': CUST.LIAB
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,E)
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
*CALL DBR('LIMIT':@FM:LI.RECORD.PARENT,KEY.LIST<I>,LIM.PARE)
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('LIMIT':@FM:LI.LIMIT.PRODUCT,KEY.LIST<I>,LIM.PRO)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,KEY.LIST<I>,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
LIM.PRO=R.ITSS.LIMIT<LI.LIMIT.PRODUCT>
                IF NOT(ETEXT) THEN
                    IF LIM.PRO = '100'  THEN
* LL = LIM.PARE[11,8]
                        R.NEW(AC.LIMIT.REF) = '10100.01'
                        R.NEW(AC.LINK.TO.LIMIT) = 'YES'
                    END
                END

            NEXT I
        END ELSE
            E = 'CUSTOMER HAS NO LIMIT' ; CALL ERR  ; MESSAGE = 'REPEAT'
        END


********MODIFIED ON 2/5/2006******************
*    ID.CAT = ID.NEW[11,4]
*    IF PGM.VERSION EQ ',SCB.CUSTAC.NEW.OD' THEN
*        IF ID.CAT NE '1201' THEN
*            E='Category Allowed Is 1201';CALL ERR;MESSAGE='REPEAT'
*        END
*    END ELSE
*        IF PGM.VERSION EQ ',SCB.CUSTAC.NEW.WOD' THEN
*            IF ID.CAT NE '1202' THEN
*                E='Category Allowed Is 1202 ';CALL ERR;MESSAGE='REPEAT'
*            END
*
*        END
*    END
**************************
    END
    RETURN
END
