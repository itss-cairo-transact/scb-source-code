* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHQ.BATCH

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.BATCH = 'FBNK.BILL.REGISTER' ; F.BATCH = '' ; R.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

    KEY.LIST=""; SELECTED="" ; ER.MSG=""

    IDDD="EG0010001"
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,IDDD,N.W.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
N.W.DAY=R.ITSS.DATES<EB.DAT.NEXT.WORKING.DAY>
    COMP       = C$ID.COMPANY

***    IF COMP EQ 'EG0010013 ' THEN
***        HH = TODAY
***    END ELSE
    HH = N.W.DAY
***    END

***    BNK.BR = "1700" : R.USER<EB.USE.DEPARTMENT.CODE>

**  T.SEL  = "SSELECT FBNK.BILL.REGISTER WITH BILL.CHQ.STA EQ '1' AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND BIL.CHQ.TYPE EQ '6' AND BANK.BR EQ '990201' AND MATURITY.EXT LE " : HH
    T.SEL  = "SSELECT FBNK.BILL.REGISTER WITH BILL.CHQ.STA EQ '1' AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND BIL.CHQ.TYPE EQ '6' AND IN.OUT.BILL EQ 'NO' AND MATURITY.EXT LE " : HH

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL---  "  : SELECTED ; CALL REM
    CALL F.READ(FN.BATCH, KEY.LIST, R.BATCH, F.BATCH, ETEXT)

    IF SELECTED THEN
        R.NEW(SCB.BLB.DESCRIPTION)   = "BATCH TO SEND TO COLLECTION"

        FOR I = 1 TO SELECTED
            R.NEW(SCB.BLB.CHQ.REG.ID)<1,I>    = KEY.LIST<I>
        NEXT I

        R.NEW(SCB.BLB.NO.OF.CHQS)    = SELECTED

        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BATCH, KEY.LIST<I>, R.BATCH, F.BATCH, ETEXT)
            R.NEW(SCB.BLB.AMOUNT)   += R.BATCH<EB.BILL.REG.AMOUNT>
        NEXT I

        R.NEW(SCB.BLB.USR.STATUS) = 5
        R.NEW(SCB.BLB.CURRENCY)   = "EGP"
        R.NEW(SCB.BLB.BATCH.DATE) = TODAY
    END ELSE
        E = " �� ���� ����� ���� � "  ; CALL ERR; MESSAGE = 'REPEAT'
    END
    RETURN
END
