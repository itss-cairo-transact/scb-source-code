* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LC.WRITE.SERIAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TF.REF.NO
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.TYPES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

*

    IF NOT(R.NEW(TF.LC.DATE.TIME)) THEN
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('LC.TYPES':@FM:LC.TYP.IMPORT.EXPORT,R.NEW(TF.LC.LC.TYPE),IE.TYPE)
F.ITSS.LC.TYPES = 'F.LC.TYPES'
FN.F.ITSS.LC.TYPES = ''
CALL OPF(F.ITSS.LC.TYPES,FN.F.ITSS.LC.TYPES)
CALL F.READ(F.ITSS.LC.TYPES,R.NEW(TF.LC.LC.TYPE),R.ITSS.LC.TYPES,FN.F.ITSS.LC.TYPES,ERROR.LC.TYPES)
IE.TYPE=R.ITSS.LC.TYPES<LC.TYP.IMPORT.EXPORT>
        IF IE.TYPE THEN
            IF IE.TYPE = "I" THEN
                TEMP.TYPE = "IMPORT"
            END ELSE
                TEMP.TYPE = "EXPORT"
            END

        END
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('SCB.TF.REF.NO':@FM:SCBTF.REF.NEXT.NO,TEMP.TYPE,TF.SERIAL)
F.ITSS.SCB.TF.REF.NO = 'F.SCB.TF.REF.NO'
FN.F.ITSS.SCB.TF.REF.NO = ''
CALL OPF(F.ITSS.SCB.TF.REF.NO,FN.F.ITSS.SCB.TF.REF.NO)
CALL F.READ(F.ITSS.SCB.TF.REF.NO,TEMP.TYPE,R.ITSS.SCB.TF.REF.NO,FN.F.ITSS.SCB.TF.REF.NO,ERROR.SCB.TF.REF.NO)
TF.SERIAL=R.ITSS.SCB.TF.REF.NO<SCBTF.REF.NEXT.NO>
IF LEN(R.NEW(TF.LC.APPLICANT.CUSTNO)) = 8 THEN
    BRN.CU = R.NEW(TF.LC.APPLICANT.CUSTNO)[1,2]
END ELSE
BRN.CU = R.NEW(TF.LC.APPLICANT.CUSTNO)[1,1]
END
        R.NEW(TF.LC.OLD.LC.NUMBER) =  ID.NEW[3,2]:R.NEW(TF.LC.LC.TYPE)[2,LEN(R.NEW(TF.LC.LC.TYPE))]:BRN.CU:R.NEW(TF.LC.LC.CURRENCY):TF.SERIAL
        FN.SCB.TF.REF.NO='F.SCB.TF.REF.NO';R.SCB.TF.REF.NO='';F.SCB.TF.REF.NO=''
        CALL OPF(FN.SCB.TF.REF.NO,F.SCB.TF.REF.NO)
        CALL F.READ(FN.SCB.TF.REF.NO,TEMP.TYPE,R.SCB.TF.REF.NO,F.SCB.TF.REF.NO,ETEXT)

        R.SCB.TF.REF.NO<SCBTF.REF.NEXT.NO> = R.SCB.TF.REF.NO<SCBTF.REF.NEXT.NO> + 1

        CALL F.WRITE(FN.SCB.TF.REF.NO,TEMP.TYPE,R.SCB.TF.REF.NO)
    END
    RETURN
END
