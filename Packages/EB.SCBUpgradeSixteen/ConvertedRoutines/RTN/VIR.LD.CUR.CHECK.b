* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LD.CUR.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

**- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -**

    FN.AC = 'FBNK.ACCOUNT'
    F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    COMP = ID.COMPANY

    Y.LD.CUR = R.NEW(LD.CURRENCY)

    ACC1 = R.NEW(LD.DRAWDOWN.ACCOUNT)
    CALL F.READ(FN.AC,ACC1,R.AC1,F.AC,Y.ERR)

    ACC2 = R.NEW(LD.PRIN.LIQ.ACCT)
    CALL F.READ(FN.AC,ACC2,R.AC2,F.AC,Y.ERR)

    ACC3 = R.NEW(LD.INT.LIQ.ACCT)
    CALL F.READ(FN.AC,ACC3,R.AC3,F.AC,Y.ERR)

    Y.AC.CUR1 = R.AC1<AC.CURRENCY>
    Y.AC.CUR2 = R.AC2<AC.CURRENCY>
    Y.AC.CUR3 = R.AC3<AC.CURRENCY>

    CO.CO1    = R.AC1<AC.CO.CODE>
    CO.CO2    = R.AC2<AC.CO.CODE>
    CO.CO3    = R.AC3<AC.CO.CODE>

    IF Y.LD.CUR NE Y.AC.CUR1 THEN
        ETEXT = "THE CURRENCY NOT A SAME" ; CALL STORE.END.ERROR
    END

    IF Y.LD.CUR NE Y.AC.CUR2 THEN
        ETEXT = "THE CURRENCY NOT A SAME" ; CALL STORE.END.ERROR
    END

    IF Y.LD.CUR NE Y.AC.CUR3 THEN
        ETEXT = "THE CURRENCY NOT A SAME"  ; CALL STORE.END.ERROR
    END

**- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -**
**--------------------------------------------------------------------**
*
*
*    IF CO.CO1  NE COMP THEN
*        ETEXT = "INVALID COMPANY FOR ACCOUNT " ; CALL STORE.END.ERROR
*    END

*    IF CO.CO2  NE COMP THEN
*        ETEXT = "INVALID COMPANY FOR ACCOUNT " ; CALL STORE.END.ERROR
*    END

*    IF CO.CO3  NE COMP THEN
*        ETEXT = "INVALID COMPANY FOR ACCOUNT " ; CALL STORE.END.ERROR
*    END

**--------------------------------------------------------------------**
    RETURN
END
