* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-43</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/10 ***
*******************************************

    SUBROUTINE VIR.MUA.EXCEPTION.CHK
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY

    GOSUB INITIATE
    GOSUB GET.DATA
    GOSUB CHANGE.COMPANY
*    IF R.NEW(MUA.RECORD.STATUS) NE '' THEN
    GOSUB CHK.EXCEPTION
*    END
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY
*    FN.SCS = "F.SCB.CUSTOMER.SECURITY"  ; F.SCS  = "" ; R.SCS = ""
*    CALL OPF (FN.SCS,F.SCS)
    WS.430 = 0
    WS.420 = 0
    WS.421 = 0
    WS.433 = 0
    RETURN
*-----------------------------------------------------
GET.DATA:
    WS.HMM.ID           = R.NEW(MUA.HMM.ID)
*Line [ 53 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.HMM.CNT          = DCOUNT(WS.HMM.ID,@VM)

    WS.USR.CO           = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.USR.ID           = R.NEW(MUA.USER.ID)
    WS.USR.POS          = R.NEW(MUA.USER.BNK.POSITION)
    WS.USR.COMP         = 'EG00100':WS.USR.CO[2]
    WS.USR.SCB.DEPT     = R.NEW(MUA.DEP.ACCT.CODE)
    WS.USR.TO.DATE      = R.NEW(MUA.END.DATE.PROFILE)


    FOR IHMM = 1 TO WS.HMM.CNT
        IF WS.HMM.ID<1,IHMM> EQ '430' THEN
            WS.430 = 1
        END
        IF WS.HMM.ID<1,IHMM> EQ '420' THEN
            WS.420 = 1
        END
        IF WS.HMM.ID<1,IHMM> EQ '421' THEN
            WS.421 = 1
        END
        IF WS.HMM.ID<1,IHMM> EQ '433' THEN
            WS.433 = 1
        END
    NEXT IHMM

*Line [ 79 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.MSG.CNT = DCOUNT(R.NEW(MUA.OVERRIDE),@VM)
    FOR IOVR.CNT = 1 TO WS.OVR.MSG.CNT
        DEL R.NEW(MUA.OVERRIDE)<1,IOVR.CNT>
        IOVR.CNT --
        WS.OVR.MSG.CNT --
    NEXT IOVR.CNT

    RETURN
*-----------------------------------------------------
CHK.EXCEPTION:
    WS.USR.SCB.DEPT.NEW     = R.NEW(MUA.DEP.ACCT.CODE)
    WS.USR.SCB.DEPT.OLD     = R.OLD(MUA.DEP.ACCT.CODE)
    WS.TLR.NEW              = R.NEW(MUA.TELLER.ID)
    WS.TLR.OLD              = R.OLD(MUA.TELLER.ID)

    IF WS.USR.SCB.DEPT.NEW NE '5100' AND WS.USR.SCB.DEPT.NEW NE '5200' THEN
        IF WS.430 = 1 AND WS.421 = 1 THEN
            TEXT = "Need EXCEPTION from Administrative Instructions 3/2011"
            CALL STORE.OVERRIDE(CURR.NO)
        END
        IF WS.420 = 1 AND WS.421 = 1 THEN
            TEXT = "Need EXCEPTION from Administrative Instructions 7/2011"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END

    IF WS.USR.SCB.DEPT.NEW EQ '5100' THEN
        IF WS.USR.SCB.DEPT.OLD NE '5100' THEN
            TEXT = "We Need To EXCEPTION from Head Office"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END

    RETURN
*-----------------------------------------------------
CHANGE.COMPANY:

    IF WS.433 EQ 0 THEN
        FINDSTR 'EG0010099' IN R.NEW(MUA.COMPANY.CODE) SETTING FPOS, VPOS THEN
            DEL R.NEW(MUA.COMPANY.CODE)<1,VPOS>
            CALL REBUILD.SCREEN
        END
    END

    IF WS.433 EQ 1 THEN
        FINDSTR 'EG0010099' IN R.NEW(MUA.COMPANY.CODE) SETTING FPOS, VPOS THEN
*Line [ 126 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
*Line [ 129 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.CO.CNT = DCOUNT(R.NEW(MUA.COMPANY.CODE),@VM)
            WS.CO.CNT ++
            R.NEW(MUA.COMPANY.CODE)<1,WS.CO.CNT> = 'EG0010099'
            CALL REBUILD.SCREEN
        END
    END
    RETURN
*-----------------------------------------------------
