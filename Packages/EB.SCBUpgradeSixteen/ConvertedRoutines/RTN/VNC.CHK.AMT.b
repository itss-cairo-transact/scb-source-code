* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHK.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-15
$INSERT I_F.SCB.LOANS.INST
*--------------------------------------
    FN.TMP = 'F.SCB.LOANS.INST'  ; F.TMP = ''
    CALL OPF(FN.TMP,F.TMP)
    AMT     = 0
*--------------------------------------
    TOT.AMT = R.NEW(LINST.TOTAL.AMOUNT)

*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.AMT  = DCOUNT(R.NEW(LINST.INST.AMOUNT),@VM)

    FOR II  = 1 TO NO.AMT
        AMT = AMT + R.NEW(LINST.INST.AMOUNT)<1,II>
    NEXT II
    IF TOT.AMT NE AMT THEN
        E = "����� ����� ������� �� ����� ������� �����"
        CALL ERR
        CALL STORE.END.ERROR
    END
*--------------------------------------
    RETURN
END
