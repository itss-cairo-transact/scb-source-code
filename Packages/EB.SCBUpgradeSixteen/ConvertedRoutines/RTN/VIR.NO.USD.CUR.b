* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>187</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.NO.USD.CUR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

********************************************************************
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

*****************TO CHECK DEBIT AND CREDIT ACCOUNT ********************
    ACCC1 = R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC1,CCURR1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCC1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CCURR1=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF CCURR1 EQ '' THEN

        ETEXT = ',RESPONS.CODE=D75,' ;  CALL STORE.END.ERROR
    END

    ACCC2 = R.NEW(FT.CREDIT.ACCT.NO)
*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC2,CCURR2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCC2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CCURR2=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF CCURR2 EQ '' THEN

        ETEXT = ',RESPONS.CODE=D76,' ;  CALL STORE.END.ERROR

    END

    IF ACCC1 EQ ACCC2 THEN
        ETEXT = ',RESPONS.CODE=D77,' ;  CALL STORE.END.ERROR
    END
*****************TO STOP A DIFFERENT CUSTOMER************
    IF R.NEW(FT.CREDIT.CUSTOMER) # R.NEW(FT.DEBIT.CUSTOMER) THEN

        ETEXT = ',RESPONS.CODE=D77,' ; CALL STORE.END.ERROR

    END

*****************TO STOP MISSING AMOUNT************
    IF R.NEW(FT.DEBIT.AMOUNT) = '' THEN

        ETEXT = ',RESPONS.CODE = D06,' ; CALL STORE.END.ERROR

    END

*****************TO CHECK TRANSACTION************
    IF R.NEW(FT.TRANSACTION.TYPE)[1,2] # 'AC' THEN

        ETEXT = ',RESPONS.CODE=D01,' ; CALL STORE.END.ERROR

    END

*****************TO CHECK DEBIT AND CREDIT ACCOUNT ********************
    ACCC3 = R.NEW(FT.DEBIT.ACCT.NO)
    CALL F.READ(FN.AC,ACCC3,R.AC,F.AC,E2)
    CUS.ID  = R.AC<AC.CUSTOMER>
    AC.POST = R.AC<AC.POSTING.RESTRICT>
*******
    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E3)
    CUS.POST = R.CU<EB.CUS.POSTING.RESTRICT>

    IF AC.POST NE '' THEN
        ETEXT = ',RESPONS.CODE=D77,' ;  CALL STORE.END.ERROR
    END
    IF  CUS.POST NE '' THEN
        ETEXT = ',RESPONS.CODE=D77,' ;  CALL STORE.END.ERROR
    END

****************TO CHECK THE CURRENCY********************

    IF R.NEW(FT.DEBIT.CURRENCY) = 'EGP' AND R.NEW(FT.CREDIT.CURRENCY) # 'EGP' THEN

        ETEXT = ',RESPONS.CODE=D76,' ; CALL STORE.END.ERROR
    END

*    IF R.NEW(FT.DEBIT.CURRENCY) # 'USD' AND R.NEW(FT.CREDIT.CURRENCY) = 'EGP' THEN
*        ETEXT = ',RESPONS.CODE=D76,' ; CALL STORE.END.ERROR
*    END

*****************TO CHECK DEBIT ACCOUNT *********************
    DEB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)

    IF NUM(DEB.ACCT[1,3]) THEN
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,DEB.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEB.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF (CATEG >= 5000 AND CATEG <= 5999) THEN
            ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
        END
    END ELSE
        ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
    END

*****************TO CHECK CREDIT ACCOUNT *********************
    CRD.ACCT = R.NEW(FT.CREDIT.ACCT.NO)

    IF NUM(CRD.ACCT[1,3]) THEN
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,CRD.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CRD.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF (CATEG >= 5000 AND CATEG <= 5999) THEN
            ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
        END
    END ELSE
        ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
    END
*****************TO GET THE NET VALUE*********************
    SCB.ACT.CB = R.NEW(FT.DEBIT.ACCT.NO)
    SCB.CHK.AMT = FIELD(R.NEW(FT.AMOUNT.DEBITED),R.NEW(FT.DEBIT.CURRENCY),2)
*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,SCB.ACT.CB,SCB.W.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
SCB.W.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
    SCB.W.BAL += SCB.CHK.AMT ;*R.NEW(FT.DEBIT.AMOUNT)
    ACT.CATEG = SCB.ACT.CB[11,4]
*    IF (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR ACT.CATEG EQ 6511 OR ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1002 OR ACT.CATEG EQ 1003 OR ACT.CATEG EQ 1005 OR ACT.CATEG EQ 1006 OR (ACT.CATEG GE  1101 AND ACT.CATEG LE 1202) OR (ACT.CATEG GE  1290 AND ACT.CATEG LE 1599) THEN
*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,SCB.ACT.CB,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
        IF LIM.REF THEN
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.ACT.CB,ACT.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACT.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
            LIM.ID = ACT.CUS:".":"0000":LIM.REF
*Line [ 193 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM.ID,AVIL.BAL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVIL.BAL=R.ITSS.LIMIT<LI.AVAIL.AMT>
*Line [ 200 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('LIMIT':@FM:LI.EXPIRY.DATE,LIM.ID,SCB.EXP.DAT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
SCB.EXP.DAT=R.ITSS.LIMIT<LI.EXPIRY.DATE>
        END ELSE
            AVIL.BAL = 0
        END
        END.BAL.L22 = SCB.W.BAL + AVIL.BAL
*        TEXT = 'SCB.W.BAL=' : SCB.W.BAL ;CALL REM
*        TEXT = 'AVIL.BAL=' : AVIL.BAL ;CALL REM
*        TEXT = 'END.BAL.L22=' : END.BAL.L22 ;CALL REM

*Line [ 215 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,SCB.ACT.CB,LOC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOC.BAL=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 162 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        BALC = DCOUNT(LOC.BAL,@VM)
        BALCC = LOC.BAL<1,BALC>

*        TEXT = 'END.BAL.LOC=' : BALCC ;CALL REM
        END.BAL.L = END.BAL.L22 - BALCC

*       TEXT = 'END.BAL.L=' : END.BAL.L ;CALL REM

*        IF END.BAL.L LT R.NEW(FT.DEBIT.AMOUNT) THEN
        IF END.BAL.L LT SCB.CHK.AMT THEN

            ETEXT = ',RESPONS.CODE=D51,' ; CALL STORE.END.ERROR

        END

        IF SCB.EXP.DAT LE TODAY AND SCB.EXP.DAT THEN

            ETEXT = ',RESPONS.CODE=D51,' ; CALL STORE.END.ERROR

        END

*********************************
        CR.AC = R.NEW(FT.CREDIT.ACCT.NO)
        TR.TYPE = R.NEW(FT.TRANSACTION.TYPE)
*Line [ 247 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,CR.AC,CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF CR.CATEG EQ '1002' THEN
            IF (TR.TYPE NE 'AC12' AND TR.TYPE NE 'AC13' AND TR.TYPE NE 'AC55' AND TR.TYPE NE 'AC56') THEN
                ETEXT = ',RESPONS.CODE=D52,' ;  CALL STORE.END.ERROR
            END
        END
***************APPROVED**********
*        CALL F.READ(FN.FT,ID.NEW,R.FT,F.FT,E6)

        IF ID.NEW NE  '' THEN

            R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT> = ',RESPONS.CODE=A00,'

        END
        RETURN
    END
