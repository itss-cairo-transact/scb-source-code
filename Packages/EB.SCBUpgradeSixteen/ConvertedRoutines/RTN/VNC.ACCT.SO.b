* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>274</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.ACCT.SO


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.GEN.CONDITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCOUNT.TYPES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.SERIAL
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDING.ORDER
*TEXT = "SCB.ACCOUNT.AM" ; CALL REM


    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN

*TEXT= "ID.NEW" : ID.NEW ;CALL REM
    FN.OS = 'FBNK.STANDING.ORDER' ; F.OS = ''
    CALL OPF(FN.OS,F.OS)
    IDD = FIELD(ID.NEW,".",1)
    T.SEL = "SELECT FBNK.STANDING.ORDER WITH @ID LIKE ":IDD:".9... BY @ID"
*TEXT = "T.SEL": T.SEL ; CALL REM
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF NOT(KEY.LIST) THEN
        ID.NEW =  IDD:".900"
    END ELSE
*TEXT =  "SEL ": SELECTED  ; CALL REM

*    ID.NEW = KEY.LIST<SELECTED> +1
        DD = FIELD(KEY.LIST<SELECTED>,".",2)
        DDD=DD+1
        ID.NEW =FIELD(KEY.LIST<SELECTED>,".",1):".":DDD
*TEXT = " ID" :ID.NEW ; CALL REM
*        CALL F.READ(FN.OS,KEY.LIST<I>,R.OS,F.OS,E1)


    END
    RETURN
****************************************************************************************************
END
