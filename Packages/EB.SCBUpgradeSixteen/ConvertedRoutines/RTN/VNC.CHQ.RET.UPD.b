* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*******NESSREEN AHMED 30/3/2011*******
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHQ.RET.UPD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW

    CUST.ID = ''
    KEY.ID= ID.NEW
    TEXT = 'ID.NEW=':ID.NEW ; CALL REM
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'SCB.CHQ.RETURN.NEW':@FM:CHQ.RET.CUSTOMER, KEY.ID , CUST.ID)
F.ITSS.SCB.CHQ.RETURN.NEW = 'F.SCB.CHQ.RETURN.NEW'
FN.F.ITSS.SCB.CHQ.RETURN.NEW = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW,KEY.ID,R.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW,ERROR.SCB.CHQ.RETURN.NEW)
CUST.ID=R.ITSS.SCB.CHQ.RETURN.NEW<CHQ.RET.CUSTOMER>
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*   ** CALL DBR( 'SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.CUSTOMER, KEY.ID , CARD.TYP.N)
F.ITSS.SCB.CHQ.RETURN.NEW$NAU = 'F.SCB.CHQ.RETURN.NEW$NAU'
FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW$NAU,FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW$NAU,KEY.ID,R.ITSS.SCB.CHQ.RETURN.NEW$NAU,FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU,ERROR.SCB.CHQ.RETURN.NEW$NAU)
CARD.TYP.N=R.ITSS.SCB.CHQ.RETURN.NEW$NAU<CHQ.RET.CUSTOMER>
    IF NOT(CUST.ID) THEN
        E = '��� �� ���� ��� ���� �� ���'
        CALL ERR ; MESSAGE = 'REPEAT'
    END

    RETURN
END
