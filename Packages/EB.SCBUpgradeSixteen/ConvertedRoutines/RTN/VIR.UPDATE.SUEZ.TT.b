* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.UPDATE.SUEZ.TT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.TT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*---------------------------------------------------
    FLG   = 0
    FLG.H = 0
    GOSUB INITIAL
    RETURN
*---------------------------------------------------
INITIAL:
*-------
    FN.TT  = "FBNK.TELLER"      ; F.TT  = ""
    CALL OPF(FN.TT, F.TT)

    FN.TTH  = "FBNK.TELLER$HIS"      ; F.TTH  = ""
    CALL OPF(FN.TTH, F.TTH)

    FN.TMP = "F.SCB.SUEZ.TT"    ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
            TT.NO = R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
            CALL F.READ(FN.TT,TT.NO,R.TT,F.TT,E.TT)
            CALL F.READ(FN.TTH,TT.NO:";1",R.TTH,F.TTH,E.TTH)
            CALL F.READ(FN.TTH,TT.NO:";2",R.TTH,F.TTH,E.TTH2)

            IF E.TTH2 EQ '' THEN
                TEXT = 'INVALID TT REFRENCE' ; CALL REM
                CALL STORE.END.ERROR
                RETURN
            END

            IF E.TT THEN
                IF E.TTH THEN
                    TEXT = 'INVALID TT REFRENCE' ; CALL REM
                    CALL STORE.END.ERROR
                END ELSE
                    FLG.H = 1
                    GOSUB CHECK.RECORD
                END
            END ELSE
                FLG.H = 0
                GOSUB CHECK.RECORD
            END
        END
    END
*---------------------------------------------------------
    IF V$FUNCTION = 'D' OR V$FUNCTION = 'R' THEN
*-- FOR TT
        TT.NO = R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
        CALL F.READ(FN.TMP,TT.NO,R.TMP,F.TMP,E.TMP)
        AMT1  = R.TMP<SUEZ.TT.OUTSTANDING>
        AMT2  = FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>,"-",2)
        AMT3  = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
        AMT4  = AMT2 * AMT3

        REC.STAT = R.NEW(LD.RECORD.STATUS)[1,4]
        TEXT = "REC= ":REC.STAT ; CALL REM

        IF V$FUNCTION EQ 'R' THEN
            R.TMP<SUEZ.TT.OUTSTANDING> = AMT1 + AMT4
            CALL F.WRITE(FN.TMP,TT.NO,R.TMP)
        END

        IF V$FUNCTION EQ 'D' AND REC.STAT EQ 'RNAU' THEN
            R.TMP<SUEZ.TT.OUTSTANDING> = AMT1 - AMT4
            CALL F.WRITE(FN.TMP,TT.NO,R.TMP)
        END

        IF V$FUNCTION EQ 'D' AND REC.STAT EQ 'INAU' THEN
            R.TMP<SUEZ.TT.OUTSTANDING> = AMT1 + AMT4
            CALL F.WRITE(FN.TMP,TT.NO,R.TMP)
        END

    END

    RETURN
*------------------------------------------------------------
CHECK.RECORD:
*------------
    GOSUB CHECK.NATIONAL.ID

    IF FLG EQ 0 THEN
        CALL F.READ(FN.TMP,TT.NO,R.TMP,F.TMP,E.TMP)
        AMT1    = R.TMP<SUEZ.TT.OUTSTANDING>
        AMT2    = FIELD(R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>,"-",2)
        AMT3    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
        AMT4    = AMT2 * AMT3
        NEW.AMT = AMT1 - AMT4

        IF AMT1 LT AMT4 THEN
            TEXT = 'AMOUNT LESS THAN TT' ; CALL REM
            CALL STORE.END.ERROR
        END ELSE
            CALL F.READ(FN.TMP,TT.NO,R.TMP,F.TMP,E.TMP)
            R.TMP<SUEZ.TT.OUTSTANDING> = NEW.AMT
            CALL F.WRITE(FN.TMP,TT.NO,R.TMP)
        END
    END
    RETURN
*----------------------------------------------------------
CHECK.NATIONAL.ID:
*-----------------
    IF FLG.H EQ 0 THEN
        TT.TEST = R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
        CALL F.READ(FN.TT,TT.TEST,R.TT,F.TT,E.TMP)
    END

    IF FLG.H EQ 1 THEN
        TT.TEST = R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>:";1"
        CALL F.READ(FN.TTH,TT.TEST,R.TT,F.TTH,E.TMPH)
    END

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CHK.FLG>  EQ '�����'  THEN
        TT.ID   = R.TT<TT.TE.LOCAL.REF><1,TTLR.NSN.NO>
        ID.TEST = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>
    END ELSE
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.CHK.FLG> EQ '�����' THEN
            TT.ID   = R.TT<TT.TE.LOCAL.REF><1,TTLR.NSN.NO>:'.':R.TT<TT.TE.LOCAL.REF><1,TTLR.PLACE.ID.ISSUE>
            ID.TEST = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PLACE.ID.ISSUE>
        END ELSE
            TT.ID   = R.TT<TT.TE.LOCAL.REF><1,TTLR.NSN.NO>
            ID.TEST = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>
        END
    END

    IF TT.ID NE ID.TEST THEN
        FLG = 1     ;* ERROR
        TEXT = 'INVALID NATIONAL ID' ; CALL REM
        CALL STORE.END.ERROR
    END ELSE
        FLG = 0
    END
*------------------------------------------------------------
    RETURN
END
