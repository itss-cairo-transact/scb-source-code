* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BT.TT.DEF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.TT

*Line [ 28 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('TELLER.USER':@FM:1,OPERATOR,USE.ID)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
USE.ID=R.ITSS.TELLER.USER<1>

    ACCTT  = 'EGP10001':USE.ID:'0001'

    R.NEW(BT.TT.DEBIT.ACCOUNT) = ACCTT

    IF V$FUNCTION = 'I' THEN

        FN.BR = "F.SCB.BT.BATCH.TT"
        F.BR  = ""
        ETEXT = ""

        CALL OPF(FN.BR,F.BR)
        CALL F.READ(FN.BR,ID.NEW,R.BR,F.BR,ETEXT)
        IF NOT(ETEXT) THEN
            E = "�� ���� �������" ;CALL ERR ; MESSAGE = 'REPEAT'
        END
    END

    RETURN
END
