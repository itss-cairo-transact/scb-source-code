* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VNC.CD.LMM.INT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES

    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS' ;F.LDD = '' ; R.LDD = ''
    CALL OPF(FN.LDD,F.LDD)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ( CATEGORY GE 21026 AND CATEGORY LE 21028 ) AND VALUE.DATE GE " : TODAY
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.LDD,KEY.LIST<I>,R.LDD,F.LDD,READ.ERRD)

        FN.CORD = 'FBNK.LMM.ACCOUNT.BALANCES' ;F.CORD = '' ; R.CORD = ''
        CALL OPF(FN.CORD,F.CORD)
        LMM.ID       = KEY.LIST<I> :"00"
        CALL F.READ(FN.CORD,LMM.ID,R.CORD,F.CORD,READ.ERR)

        V.DATE   = R.LDD<LD.VALUE.DATE>
        CALL CDT('EG',V.DATE,'1W')

        R.CORD<LD27.START.PERIOD.INT> = V.DATE

*WRITE R.CORD TO F.CORD , LMM.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":LMM.ID:"TO" :FN.CORD
*END
        CALL F.WRITE (FN.CORD,LMM.ID,R.CORD)
    NEXT I

    RETURN
END
