* @ValidationCode : MjoxMzM4NTczMjcwOkNwMTI1MjoxNjQ0NDIyMTIxMjk3OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Feb 2022 17:55:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1622</Rating>
*-----------------------------------------------------------------------------
** ----- 27.06.2002 WAEL & NESSREEN SCB -----
* 02122002 PS. IF UNAUTHORIZED RECORD EXISTED NO ERROR WAS REPORTED
SUBROUTINE VNC.AC.INTERNAL.MASK.TT
* The purpose of this routine is the validation of an internal account.
* The user's input will be :
* CCC   : Currency
* GGGGG : Category code
* BB    : Branch code
* e.g EGP1648001NN
* The NN will be the serial number, SCB.ACCT.SERIAL table will be used in order to
* calculate the serial number. The key for this table will CCCXXXXXBB.

*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ACCT.SERIAL
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 47 ] $INCLUDE I_F.CURRENCY - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
    IF LEVEL.STATUS THEN GOTO PROGRAM.END
    IF MESSAGE = 'REPEAT' THEN GOTO PROGRAM.END

    IF V$FUNCTION = 'I' THEN

        IF  LEN(ID.NEW) = 12 THEN
            GOSUB INTIAL
            GOSUB CHECK.IF.EXIST
            IF NOT(EXIST) THEN
*         GOSUB CHECK.INT.NEW.ID
                GOSUB ACCT.TITLE
            END
            GOSUB CHECK.IF.INT.EXIST
        END ELSE
            E = 'Mask.Must.BE.(CCCGGGGGBBNN)'

        END

        IF E THEN
            CALL ERR
            MESSAGE = 'REPEAT'
        END
        GOTO PROGRAM.END

***********************************************************************************************************************

INTIAL:

*-------------TO INTIALIZE ALL VARIABLES -----------------------------------------*

        ETEXT= ''; SER.NO= '0'; EXIST= '' ; E ='' ;MYYIIDD= '' ; MY.DUMMY11= '' ; KEY.TO.USE = ''
        SYSTEM.RETURN.CODE= '' ; ID.DEPT ='' ; SELECT.STATEMENT = ''
        FN.SCB.ACCT.SERIAL = 'F.SCB.ACCT.SERIAL' ; F.SCB.ACCT.SERIAL = '' ; R.SCB.ACCT.SERIAL = ''
        RETRY = '' ; E1 = ''
        CATEGORY.NAME = '' ; DEPT.NAME = ''

        RETURN
***********************************************************************************************************************

CHECK.IF.EXIST:

*-------------TO CHECK IF IT EXISTS OR NOT IN LIVE FILE---------------------------*
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.MNEMONIC, ID.NEW , MYYIIDD)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ID.NEW,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        MYYIIDD=R.ITSS.ACCOUNT<AC.MNEMONIC>
        IF NOT(ETEXT) THEN EXIST = 'Y'
*----------- TO CHECK IF IT EXISTS OR NOT IN UNAUTHORIZED------------------------*
        ELSE  ETEXT= ''
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT$NAU':@FM:AC.MNEMONIC, ID.NEW , MYYIIDD)
        F.ITSS.ACCOUNT$NAU = 'F.ACCOUNT$NAU'
        FN.F.ITSS.ACCOUNT$NAU = ''
        CALL OPF(F.ITSS.ACCOUNT$NAU,FN.F.ITSS.ACCOUNT$NAU)
        CALL F.READ(F.ITSS.ACCOUNT$NAU,ID.NEW,R.ITSS.ACCOUNT$NAU,FN.F.ITSS.ACCOUNT$NAU,ERROR.ACCOUNT$NAU)
        MYYIIDD=R.ITSS.ACCOUNT$NAU<AC.MNEMONIC>
        IF NOT(ETEXT) THEN EXIST = 'Y'  ;*E = 'THIS.INTERNAL.ACC.NOT.AUTHORIZED.&' : ID.NEW; CALL ERR ; MESSAGE = 'REPEAT'
    END

RETURN
***********************************************************************************************************************

CHECK.INT.NEW.ID:

* ------------If the inserts EGP164805532 then this is invalid because the user is
* is also inserts the serial (55).

    IF ID.NEW[9,2] # '00' THEN  E = 'Invalid.Mask'
*   IF ID.NEW[11,2] # '01' THEN  E = 'Invalid.Mask' **--EXIST IN DEV--**


    ELSE


* -------------TO VERIFY THE INTERNAL ACCOUNT MASK -------------------------------*



*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'CURRENCY':@FM:1, ID.NEW[ 1, 3], MY.DUMMY11)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,ID.NEW[ 1, 3],R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        MY.DUMMY11=R.ITSS.CURRENCY<1>
        IF ETEXT THEN E = ETEXT


*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'CATEGORY':@FM:1, ID.NEW[ 4, 5], MY.DUMMY11)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,ID.NEW[ 4, 5],R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        MY.DUMMY11=R.ITSS.CATEGORY<1>
        IF ETEXT THEN E = ETEXT



        ID.DEPT = TRIM( ID.NEW[11,2], '0', 'L')      ;* remove leading zeros
*Line [ 152 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'DEPT.ACCT.OFFICER':@FM:1,ID.DEPT, MY.DUMMY11)
        F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
        FN.F.ITSS.DEPT.ACCT.OFFICER = ''
        CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
        CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ID.DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
        MY.DUMMY11=R.ITSS.DEPT.ACCT.OFFICER<1>

        IF ETEXT THEN E = ETEXT

    END


    IF NOT(E) THEN
        KEY.TO.USE = ID.NEW[1,8]:ID.NEW[2]
        R.NEW( AC.ACCOUNT.OFFICER ) = ID.DEPT


        GOSUB GET.SERIAL.NO

    END


RETURN
***********************************************************************************************************************

GET.SERIAL.NO:



    CALL OPF(FN.SCB.ACCT.SERIAL,F.SCB.ACCT.SERIAL)
    CALL F.READU(FN.SCB.ACCT.SERIAL,KEY.TO.USE, R.SCB.ACCT.SERIAL, F.SCB.ACCT.SERIAL ,E1,RETRY)

    IF E1 THEN
        SER.NO = 1

    END ELSE

        SER.NO = R.SCB.ACCT.SERIAL<SCB.AC.SER.SERIAL.NO> +1
    END


    SER.NO = STR('0', 2- LEN(SER.NO) ):SER.NO
    ID.NEW = KEY.TO.USE :SER.NO
    CALL F.RELEASE(FN.SCB.ACCT.SERIAL,KEY.TO.USE,F.SCB.ACCT.SERIAL)


    ETEXT= '' ; E = ''
*Line [ 200 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'ACCOUNT$NAU':@FM:AC.MNEMONIC, ID.NEW , MYYIIDD)
    F.ITSS.ACCOUNT$NAU = 'F.ACCOUNT$NAU'
    FN.F.ITSS.ACCOUNT$NAU = ''
    CALL OPF(F.ITSS.ACCOUNT$NAU,FN.F.ITSS.ACCOUNT$NAU)
    CALL F.READ(F.ITSS.ACCOUNT$NAU,ID.NEW,R.ITSS.ACCOUNT$NAU,FN.F.ITSS.ACCOUNT$NAU,ERROR.ACCOUNT$NAU)
    MYYIIDD=R.ITSS.ACCOUNT$NAU<AC.MNEMONIC>
    IF NOT(ETEXT) THEN E = 'Unauthorized.Record.Exist'
RETURN
***********************************************************************************************************************

CHECK.IF.INT.EXIST:

    IF NUM(ID.NEW[1,3]) THEN
        E = 'Not.Allowed.For.Int'
        CALL ERR ; MESSAGE = 'REPEAT'

    END

RETURN

**************************************************************************************************************************
ACCT.TITLE:

*Line [ 224 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'CATEGORY':@FM:EB.CAT.SHORT.NAME, ID.NEW[ 4, 5], CATEGORY.NAME)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,ID.NEW[ 4, 5],R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEGORY.NAME=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
    IF ETEXT THEN E = ETEXT
********************************
    F.COUNT = '' ; FN.COUNT = 'F.CATEGORY';R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
    CALL F.READ(FN.COUNT,ID.NEW[ 4, 5],R.COUNT,F.COUNT,ERRORR)

    CATTITLE1 = R.COUNT<EB.CAT.SHORT.NAME,2>

********************************

* 02122002 E = ''
    ID.DEPT = TRIM( ID.NEW[ 11, 2], '0', 'L')      ;* remove leading zeros
*Line [ 243 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ID.DEPT, DEPT.NAME)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ID.DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    IF ETEXT THEN E = ETEXT
    IND1 = INDEX(DEPT.NAME,'.',1)
    LEN1 = LEN(DEPT.NAME)
    LEN1 = LEN1 - IND1
    DEPT.NAME1 = DEPT.NAME[LEN1]
*Line [ 255 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
* CALL DBR( 'CURRENCY':@FM:EB.CUR.CCY.NAME,R.NEW(AC.CURRENCY), SAM)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,R.NEW(AC.CURRENCY),R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    SAM=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    R.NEW(AC.ACCOUNT.TITLE.1) = R.NEW(AC.CURRENCY):'-':CATEGORY.NAME: '-' :DEPT.NAME
    R.NEW(AC.SHORT.TITLE) = R.NEW(AC.CURRENCY):'-':CATEGORY.NAME: '-' :DEPT.NAME
*R.NEW( AC.ACCOUNT.OFFICER ) = DEPT.NAME
    R.NEW( AC.ACCOUNT.OFFICER ) = ID.DEPT
    R.NEW(AC.LOCAL.REF)<1,ACLR.ARABIC.TITLE>=R.NEW(AC.CURRENCY):CATTITLE1:'-':DEPT.NAME1


RETURN
***************************************************************************************************
PROGRAM.END:
RETURN
END
***************************************************************************************************
