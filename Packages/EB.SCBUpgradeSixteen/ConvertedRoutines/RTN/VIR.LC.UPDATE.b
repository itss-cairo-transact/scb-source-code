* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VIR.LC.UPDATE
*A ROUTINE TO CHECK
                 * IF THE TF.LC.LATEST.SHIPMENT HAS VALUE THE ADD THIS VALUE TO SCB.LCD.LAST.SHIP.TO.ADV
                 * IF THE TF.LC.EXPIRY.DATE # TF.LC.LATEST.SHIPMENT + SCB.LCD.LAST.SHIP.TO.ADV THEN BRING
                 * OVERRIDE & EMPTY LINK.DATA<TNO>
                 * IF LCLR.ANNEX11 HAS VALUE THEN UPDATE THE TABLE WITH THE NEW ANNEX11

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.ANNEX.INDEX
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS

IF V$FUNCTION = 'I' THEN
FN.SCB.LC.ANNEX.INDEX = 'F.SCB.LC.ANNEX.INDEX' ; F.SCB.LC.ANNEX.INDEX = '' ; R.SCB.LC.ANNEX.INDEX = ''
   IF R.NEW(TF.DR.LOCAL.REF)<1,DRLR.ANNEX11> THEN
   BASE.ID = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.ANNEX11>
     R.SCB.LC.ANNEX.INDEX<SCB.LC.ANDX.LC.ID> = ID.NEW
        CALL OPF( FN.SCB.LC.ANNEX.INDEX,F.SCB.LC.ANNEX.INDEX)
                 CALL F.WRITE(FN.SCB.LC.ANNEX.INDEX,BASE.ID,R.SCB.LC.ANNEX.INDEX)
                  CALL JOURNAL.UPDATE(ID.NEW)

  END

END

RETURN
END
