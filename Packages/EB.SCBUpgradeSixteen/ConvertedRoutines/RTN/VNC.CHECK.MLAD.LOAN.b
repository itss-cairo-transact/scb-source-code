* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHECK.MLAD.LOAN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.LOANS.PARM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.LOANS.PARM.CONT
*--------------------------------------------------
    IF V$FUNCTION = 'I' THEN
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('SCB.MLAD.LOANS.PARM.CONT':@FM:MLAD.LOANS.CONT.AR.DESCRIPTION,ID.NEW,AR.DESC)
F.ITSS.SCB.MLAD.LOANS.PARM.CONT = 'F.SCB.MLAD.LOANS.PARM.CONT'
FN.F.ITSS.SCB.MLAD.LOANS.PARM.CONT = ''
CALL OPF(F.ITSS.SCB.MLAD.LOANS.PARM.CONT,FN.F.ITSS.SCB.MLAD.LOANS.PARM.CONT)
CALL F.READ(F.ITSS.SCB.MLAD.LOANS.PARM.CONT,ID.NEW,R.ITSS.SCB.MLAD.LOANS.PARM.CONT,FN.F.ITSS.SCB.MLAD.LOANS.PARM.CONT,ERROR.SCB.MLAD.LOANS.PARM.CONT)
AR.DESC=R.ITSS.SCB.MLAD.LOANS.PARM.CONT<MLAD.LOANS.CONT.AR.DESCRIPTION>
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('SCB.MLAD.LOANS.PARM.CONT':@FM:MLAD.LOANS.CONT.EN.DESCRIPTION,ID.NEW,EN.DESC)
F.ITSS.SCB.MLAD.LOANS.PARM.CONT = 'F.SCB.MLAD.LOANS.PARM.CONT'
FN.F.ITSS.SCB.MLAD.LOANS.PARM.CONT = ''
CALL OPF(F.ITSS.SCB.MLAD.LOANS.PARM.CONT,FN.F.ITSS.SCB.MLAD.LOANS.PARM.CONT)
CALL F.READ(F.ITSS.SCB.MLAD.LOANS.PARM.CONT,ID.NEW,R.ITSS.SCB.MLAD.LOANS.PARM.CONT,FN.F.ITSS.SCB.MLAD.LOANS.PARM.CONT,ERROR.SCB.MLAD.LOANS.PARM.CONT)
EN.DESC=R.ITSS.SCB.MLAD.LOANS.PARM.CONT<MLAD.LOANS.CONT.EN.DESCRIPTION>
        IF AR.DESC THEN
            R.NEW(MLAD.LOANS.P.AR.DESCRIPTION) = AR.DESC
            R.NEW(MLAD.LOANS.P.EN.DESCRIPTION) = EN.DESC

            CALL REBUILD.SCREEN
        END ELSE
            E = 'Invalid.Input'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
            RETURN
        END
    END
*------------------------------------------------
    RETURN
END
