* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*********NESSREEN AHMED 21/3/2007
*-----------------------------------------------------------------------------
* <Rating>-93</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.CARD.EXIST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CARD.TYPE

*TO PRINT A REPORT CONTAING ALL EXISTING CARD

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 49 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 50 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.CARD.EXIST'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)


*****************************************************************************************************
*  T.SEL = "SELECT FBNK.CARD.ISSUE WITH (CARD.CODE EQ 101 OR CARD.CODE EQ 102 OR CARD.CODE EQ 104 OR CARD.CODE EQ 105 OR CARD.CODE EQ 201 OR CARD.CODE EQ 202 OR CARD.CODE EQ 204 OR CARD.CODE EQ 205) BY ACCOUNT "
    T.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE VI... AND CANCELLATION.DATE EQ ' ' BY ACCOUNT"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*===============================================================================================
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
        XX= '' ; ZZ= ''

        FOR I = 1 TO SELECTED
            CUST.NAME = '' ; VISA.NO = '' ; ACCT = '' ; DESC = '' ; CUST = ''
            CALL F.READU(FN.CARD.ISSUE,KEY.LIST<I>,R.CARD.ISSUE,F.CARD.ISSUE,E1,RETRY1)
            ACCT = R.CARD.ISSUE<CARD.IS.ACCOUNT>
            LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,LRCI.VISA.NO>
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT,CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
         **   CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
            CUST.NAME = R.CARD.ISSUE<CARD.IS.NAME>
            CARD.CODE = LOCAL.REF<1,LRCI.CARD.CODE>
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('SCB.VISA.CARD.TYPE':@FM:VICA.CODE.DESC,CARD.CODE,DESC)
F.ITSS.SCB.VISA.CARD.TYPE = 'F.SCB.VISA.CARD.TYPE'
FN.F.ITSS.SCB.VISA.CARD.TYPE = ''
CALL OPF(F.ITSS.SCB.VISA.CARD.TYPE,FN.F.ITSS.SCB.VISA.CARD.TYPE)
CALL F.READ(F.ITSS.SCB.VISA.CARD.TYPE,CARD.CODE,R.ITSS.SCB.VISA.CARD.TYPE,FN.F.ITSS.SCB.VISA.CARD.TYPE,ERROR.SCB.VISA.CARD.TYPE)
DESC=R.ITSS.SCB.VISA.CARD.TYPE<VICA.CODE.DESC>

**************************************************
            XX<1,ZZ>[1,30] = CUST.NAME
            XX<1,ZZ>[50,16] = VISA.NO
            XX<1,ZZ>[80,16] = ACCT
            XX<1,ZZ>[110,35] = DESC

            PRINT XX<1,ZZ>
            XX = ''
            ZZ = ZZ+1

*****************************************************************************************
        NEXT I
    END   ;* OF SELECTED
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"������ ��������� ������� ������ �������"
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"����� ��������":SPACE(35):"��� ������":SPACE(20):"��� ������":SPACE(20):"��� �������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(33):STR('_',18):SPACE(13):STR('_',20):SPACE(10):STR('_',17)
    HEADING PR.HD
    RETURN
*==============================================================
    RETURN
END
