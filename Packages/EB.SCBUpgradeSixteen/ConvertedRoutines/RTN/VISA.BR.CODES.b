* @ValidationCode : MjotNTc4OTAyMDYzOkNwMTI1MjoxNjQ0NDIxNzk0MzI2OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Feb 2022 17:49:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1812</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VISA.BR.CODES
    
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CI.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VISA.BR.CODES
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VISA.DAILY.TRN
*Line [ 43 ] $INCLUDE I_F.DEPT.ACCT.OFFICER - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*****WRITTEN BY NESSREEN AHMED 5/10/2008*****
***************************************************
    YTEXT = "Enter the Start Date Of Required Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
***************************************************
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.BR.CODES'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
**TEXT = 'DB' ; CALL REM
    ORG.MSG = '' ; ORG.MSGE.TR = '' ; MSGE.TYPE = '' ; PROC.COD = '' ; DB.CR = '' ; BILL.AMT = ''
    TOT.OMLT = '' ; BR = '' ; YY = '' ; TOT.ISSU = '' ; TOT.RNEW = '' ; TOT.RISS = '' ; TOT.RPRN = '' ; TOT.LPAY = '' ; TOT.CRAD = '' ; TOT.REFN = '' ; TOT.DBET = '' ; ZZ = ''

    F.VISA.DAILY.TRN = '' ; FN.VISA.DAILY.TRN = 'F.SCB.VISA.DAILY.TRN' ; R.VISA.DAILY.TRN = '' ; E1 = ''
    CALL OPF(FN.VISA.DAILY.TRN,F.VISA.DAILY.TRN)

    T.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":COMI :" BY CARD.BR "
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        TEXT = "SELECTED=":SELECTED ; CALL REM
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST<1>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E1)
        BR<1> =   R.VISA.DAILY.TRN<SCB.DAILY.CARD.BR>
        ORG.MSG = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD = DCOUNT(ORG.MSG,@VM)
        FOR XX = 1 TO DD
            ORG.MSGE.TR<XX> = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,XX>
            MSGE.TYPE<XX> = R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,XX>
            PROC.COD<XX> =  R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,XX>
            DB.CR<XX> = R.VISA.DAILY.TRN<SCB.DAILY.DB.CR.FLAG,XX>
            BILL.AMT<XX> = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,XX>
            N.SEL =  "SELECT F.SCB.VISA.BR.CODES WITH ORG.MSG.TYPE EQ ":ORG.MSGE.TR<XX>:" AND MSG.TYPE EQ ":MSGE.TYPE<XX>:" AND PROCESS.CODE EQ ":PROC.COD<XX>:" AND FLAG EQ ":DB.CR<XX>
            CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
            F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.BR.CODES' ; R.VISA.CODE = '' ; E3 = ''
            CALL OPF(FN.VISA.CODE,F.VISA.CODE)
**TEXT = 'SELECTED.2=':SELECTED.2 ; CALL REM
            IF SELECTED.2 THEN
                CALL F.READ(FN.VISA.CODE, KEY.LIST.2<1>, R.VISA.CODE, F.VISA.CODE, E3)
                IF KEY.LIST.2<1> = '1' THEN
                    TOT.ISSU = TOT.ISSU+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '2' THEN
                    TOT.RNEW = TOT.RNEW+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '3' THEN
                    TOT.RISS= TOT.RISS+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '4' THEN
                    TOT.RPRN = TOT.RPRN+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '5' THEN
                    TOT.OMLT = TOT.OMLT+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '6' THEN
                    TOT.LPAY = TOT.LPAY+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '7' THEN
                    TOT.CRAD = TOT.CRAD+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '8' THEN
                    TOT.REFN = TOT.REFN+BILL.AMT<XX>
                END
                IF KEY.LIST.2<1> = '9' THEN
                    TOT.DBET = TOT.DBET+BILL.AMT<XX>
                END
            END     ;*END OF IF SELCTED.2

        NEXT XX
*====START FROM SEC REC =====
        XX= '' ; DD= ''
        FOR I = 2 TO SELECTED
            CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST<I>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E1)
            BR<I> =   R.VISA.DAILY.TRN<SCB.DAILY.CARD.BR>
            IF BR<I> # BR<I-1> THEN
                YY<1,ZZ>[1,18]= "�������"
                YY<1,ZZ>[20,10]= TOT.ISSU
                YY<1,ZZ>[35,10]= TOT.RNEW
                YY<1,ZZ>[50,10]= TOT.RISS
                YY<1,ZZ>[65,10]= TOT.RPRN
                YY<1,ZZ>[80,10]= TOT.OMLT
                YY<1,ZZ>[95,10]= TOT.LPAY
                YY<1,ZZ>[110,10]= TOT.CRAD
                YY<1,ZZ>[125,10]= TOT.REFN
                YY<1,ZZ>[140,10]= TOT.DBET
                PRINT YY<1,ZZ>
                ZZ=ZZ+1
                YY = ''
                TOT.ISSU = '' ;  TOT.RNEW = '' ; TOT.RISS = '' ; TOT.RPRN = '' ;  TOT.OMLT = '' ; TOT.LPAY = '' ; TOT.CRAD = '' ; TOT.REFN = '' ; TOT.DBET = ''
            END ELSE
                ORG.MSG = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 145 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(ORG.MSG,@VM)
                FOR XX = 1 TO DD
                    ORG.MSGE.TR<XX> = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,XX>
                    MSGE.TYPE<XX> = R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,XX>
                    PROC.COD<XX> =  R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,XX>
                    DB.CR<XX> = R.VISA.DAILY.TRN<SCB.DAILY.DB.CR.FLAG,XX>
                    BILL.AMT<XX> = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,XX>
                    N.SEL =  "SELECT F.SCB.VISA.BR.CODES WITH ORG.MSG.TYPE EQ ":ORG.MSGE.TR<XX>:" AND MSG.TYPE EQ ":MSGE.TYPE<XX>:" AND PROCESS.CODE EQ ":PROC.COD<XX>:" AND FLAG EQ ":DB.CR<XX>
                    CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
                    F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.BR.CODES' ; R.VISA.CODE = '' ; E3 = ''
                    CALL OPF(FN.VISA.CODE,F.VISA.CODE)
**TEXT = 'SELECTED.2LOOP=':SELECTED.2 ; CALL REM
                    IF SELECTED.2 THEN
                        CALL F.READ(FN.VISA.CODE, KEY.LIST.2<1>, R.VISA.CODE, F.VISA.CODE, E3)
                        IF KEY.LIST.2<1> = '1' THEN
                            TOT.ISSU = TOT.ISSU+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '2' THEN
                            TOT.RNEW = TOT.RNEW+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '3' THEN
                            TOT.RISS= TOT.RISS+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '4' THEN
                            TOT.RPRN = TOT.RPRN+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '5' THEN
                            TOT.OMLT = TOT.OMLT+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '6' THEN
                            TOT.LPAY = TOT.LPAY+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '7' THEN
                            TOT.CRAD = TOT.CRAD+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '8' THEN
                            TOT.REFN = TOT.REFN+BILL.AMT<XX>
                        END
                        IF KEY.LIST.2<1> = '9' THEN
                            TOT.DBET = TOT.DBET+BILL.AMT<XX>
                        END
                    END       ;*END OF IF SELCTED.2

                NEXT XX
            END
*============================
            IF I = SELECTED THEN
                YY<1,ZZ>[1,18]= "�������"
                YY<1,ZZ>[20,10]= TOT.ISSU
                YY<1,ZZ>[35,10]= TOT.RNEW
                YY<1,ZZ>[50,10]= TOT.RISS
                YY<1,ZZ>[65,10]= TOT.RPRN
                YY<1,ZZ>[80,10]= TOT.OMLT
                YY<1,ZZ>[95,10]= TOT.LPAY
                YY<1,ZZ>[110,10]= TOT.CRAD
                YY<1,ZZ>[125,10]= TOT.REFN
                YY<1,ZZ>[140,10]= TOT.DBET
                PRINT YY<1,ZZ>
                ZZ=ZZ+1
                YY = ''
            END
        NEXT I
*==========================
    END   ;*END OF IF SELECTED
RETURN
*===============================================================
PRINT.HEAD:
*Line [ 214 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<@FM:EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ������ �������� �������� ������"
    PR.HD :="'L'":SPACE(43): "���� "
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):STR('_',120)
    PR.HD :="'L'":SPACE(1):"�����":SPACE(13):"����":SPACE(8):"����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�������"
    PR.HD :="'L'":SPACE(19):"�������":SPACE(5):"�������":SPACE(8):"����":SPACE(8):"����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�����":SPACE(8):"�.�����":SPACE(8):"�.�����"
    PR.HD :="'L'":SPACE(1):STR('_',120)
    HEADING PR.HD

*==============================================================
RETURN
END
