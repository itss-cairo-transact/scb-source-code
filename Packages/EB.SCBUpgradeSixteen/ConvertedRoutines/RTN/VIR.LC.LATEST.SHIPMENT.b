* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>144</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VIR.LC.LATEST.SHIPMENT
*A ROUTINE TO CHECK
                 * IF THE TF.LC.LATEST.SHIPMENT HAS VALUE THE ADD THIS VALUE TO SCB.LCD.LAST.SHIP.TO.ADV
                 * IF THE TF.LC.EXPIRY.DATE # TF.LC.LATEST.SHIPMENT + SCB.LCD.LAST.SHIP.TO.ADV THEN BRING
                 * OVERRIDE & EMPTY LINK.DATA<TNO>
                 * IF LCLR.ANNEX11 HAS VALUE THEN UPDATE THE TABLE WITH THE NEW ANNEX11

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*$INCLUDE           I_F.SCB.LC.ANNEX.INDEX
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

IF V$FUNCTION = 'I' THEN
*FN.SCB.LC.ANNEX.INDEX = 'F.SCB.LC.ANNEX.INDEX' ; F.SCB.LC.ANNEX.INDEX = '' ; R.SCB.LC.ANNEX.INDEX = ''
DOC.ID = R.NEW(TF.LC.LC.TYPE):'.': R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC>
IF  R.NEW(TF.LC.LATEST.SHIPMENT) THEN
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*CALL DBR("SCB.LC.DOCTERMS":@FM:SCB.LCD.LAST.SHIP.TO.ADV,DOC.ID,DAT)
F.ITSS.SCB.LC.DOCTERMS = 'F.SCB.LC.DOCTERMS'
FN.F.ITSS.SCB.LC.DOCTERMS = ''
CALL OPF(F.ITSS.SCB.LC.DOCTERMS,FN.F.ITSS.SCB.LC.DOCTERMS)
CALL F.READ(F.ITSS.SCB.LC.DOCTERMS,DOC.ID,R.ITSS.SCB.LC.DOCTERMS,FN.F.ITSS.SCB.LC.DOCTERMS,ERROR.SCB.LC.DOCTERMS)
DAT=R.ITSS.SCB.LC.DOCTERMS<SCB.LCD.LAST.SHIP.TO.ADV>
EXP.DAT = R.NEW(TF.LC.LATEST.SHIPMENT)
  IF DAT THEN
    CALL CDT ('',EXP.DAT,DAT)
   IF EXP.DAT # R.NEW(TF.LC.ADVICE.EXPIRY.DATE) THEN
     TEXT = 'LATESTSHIPMENT."&".EXPIRY.DATE.ARE.EQUAL'
      AF= TF.LC.EXPIRY.DATE ; AV=1
      CALL STORE.OVERRIDE(R.NEW(TF.LC.CURR.NO)+1)
      
   END
    END
 *  IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ANNEX11> THEN
  * BASE.ID = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ANNEX11>
   *  R.SCB.LC.ANNEX.INDEX<SCB.LC.ANDX.LC.ID> = ID.NEW
    *    CALL OPF( FN.SCB.LC.ANNEX.INDEX,F.SCB.LC.ANNEX.INDEX)
     *            CALL F.WRITE(FN.SCB.LC.ANNEX.INDEX,BASE.ID,R.SCB.LC.ANNEX.INDEX)
      *            CALL JOURNAL.UPDATE(ID.NEW)

 * END
END 
 LINK.DATA<TNO> = ''
END
  IF R.NEW(TF.LC.BANK.TO.BANK) = R.OLD(TF.LC.BANK.TO.BANK) THEN R.NEW(TF.LC.BANK.TO.BANK) =''

RETURN
END
