* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>93</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/03/18 ***
*******************************************

    SUBROUTINE VIR.CU.SECURITY.CHK
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.SECURITY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB GET.DATA
    GOSUB CHK.SECURITY.KEY
    GOSUB CHANGE.COMPANY
    GOSUB DRMT.CHANGE
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY
    FN.CU   = "FBNK.CUSTOMER"            ; F.CU   = ""
    FN.SCS  = "F.SCB.CUSTOMER.SECURITY"  ; F.SCS  = "" ; R.SCS = ""
    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.SCS,F.SCS)
    RETURN
*-----------------------------------------------------
GET.DATA:
    WS.NATION           = R.NEW(EB.CUS.NATIONALITY)
    WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
    WS.CUSTOMER.STATUS  = R.NEW(EB.CUS.CUSTOMER.STATUS)
    WS.PASSPORT.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER>
    WS.NATIONAL.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO>
    WS.PLACE.ISSUE      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE>
    WS.REG.NO           = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.COM.REG.NO>
    WS.ID.TYPE          = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE>

    WS.SECURITY.KEY.OLD = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.SECURITY.KEY>
    WS.SECURITY.KEY.NEW = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.SECURITY.KEY>

    WS.NEW.SECTOR.NEW   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
    WS.NEW.SECTOR.OLD   = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>

*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('SCB.NEW.SECTOR':@FM:C.PROVE.CODE,WS.NEW.SECTOR.NEW,WS.PROVE.CODE.NEW)
F.ITSS.SCB.NEW.SECTOR = 'F.SCB.NEW.SECTOR'
FN.F.ITSS.SCB.NEW.SECTOR = ''
CALL OPF(F.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR)
CALL F.READ(F.ITSS.SCB.NEW.SECTOR,WS.NEW.SECTOR.NEW,R.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR,ERROR.SCB.NEW.SECTOR)
WS.PROVE.CODE.NEW=R.ITSS.SCB.NEW.SECTOR<C.PROVE.CODE>
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('SCB.NEW.SECTOR':@FM:C.PROVE.CODE,WS.NEW.SECTOR.OLD,WS.PROVE.CODE.OLD)
F.ITSS.SCB.NEW.SECTOR = 'F.SCB.NEW.SECTOR'
FN.F.ITSS.SCB.NEW.SECTOR = ''
CALL OPF(F.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR)
CALL F.READ(F.ITSS.SCB.NEW.SECTOR,WS.NEW.SECTOR.OLD,R.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR,ERROR.SCB.NEW.SECTOR)
WS.PROVE.CODE.OLD=R.ITSS.SCB.NEW.SECTOR<C.PROVE.CODE>

    WS.DRMNT.CO.NEW   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>
    WS.DRMNT.CO.OLD   = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>

    WS.PROVE.CODE = WS.PROVE.CODE.NEW
    WS.NEW.SECTOR = WS.NEW.SECTOR.NEW

    WS.ACCT.OFFCR.NEW = R.NEW(EB.CUS.ACCOUNT.OFFICER)
***    WS.NEW = R.NEW(EB.CUS.COMPANY.BOOK)
***    WS.ACCT.OFFCR.NEW = WS.NEW[8,2]
    WS.ACCT.OFFCR.OLD = R.OLD(EB.CUS.ACCOUNT.OFFICER)
***    WS.OLD = R.OLD(EB.CUS.COMPANY.BOOK)
***    WS.ACCT.OFFCR.OLD = WS.OLD[8,2]

    WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
    WS.POST.REST.OLD    = R.OLD(EB.CUS.POSTING.RESTRICT)

    RETURN
*-----------------------------------------------------
CHK.SECURITY.KEY:
**** --------------------------------- ������ �� ������� ������ ������� �������� �������� --------------------------*
**** ---------------------------------          ��� ������� �������� ��������           --------------------------*
    IF WS.PROVE.CODE EQ 1 THEN
        IF WS.CUSTOMER.STATUS NE 3 AND WS.ID.TYPE NE 5 THEN
            IF WS.NATION EQ 'EG' THEN
                IF  WS.NATIONAL.ID EQ '00000000000000' THEN
                    TEXT = "National ID missing"
                    CALL STORE.OVERRIDE(CURR.NO)
                END
                IF  WS.NATIONAL.ID EQ '' THEN
                    TEXT = "National ID missing"
                    CALL STORE.OVERRIDE(CURR.NO)
                END
            END ELSE
                IF  WS.PASSPORT.ID EQ '' THEN
                    TEXT = "Passport ID missing"
                    CALL STORE.OVERRIDE(CURR.NO)
                END
            END
        END
    END
**** --------------------------------- ������ �� ������� ������ ������� ��� ����� ������� -----------------------*

    IF WS.PROVE.CODE EQ 2 THEN
        IF  WS.PLACE.ISSUE EQ '' AND WS.NEW.SECTOR NE '4250' AND WS.POST.REST NE 78 THEN
            TEXT = "No. Place of Issue missing"
            CALL STORE.OVERRIDE(CURR.NO)
        END
        IF  WS.REG.NO EQ '' AND WS.NEW.SECTOR.NEW NE '4250' AND WS.POST.REST NE 78 THEN
            TEXT = "Commercial register number missing"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END
**** --------------------------------- �� ���� ���� ����� ������� �� ��� ������� �� ��� ������ -------------------*
    IF WS.SECURITY.KEY.OLD NE '' THEN
        IF WS.PROVE.CODE.NEW NE WS.PROVE.CODE.OLD THEN
            TEXT = "The identification document type has changed"
            CALL STORE.OVERRIDE(CURR.NO)
        END

        IF WS.SECURITY.KEY.OLD NE WS.SECURITY.KEY.NEW THEN
            TEXT = "Customer security key has been changed"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END

    RETURN
*-----------------------------------------------------
*** ADD BY MOHAMED SABRY 2012/10/29
CHANGE.COMPANY:
    IF WS.ACCT.OFFCR.OLD NE '' THEN
        IF WS.ACCT.OFFCR.NEW NE WS.ACCT.OFFCR.OLD THEN
            WS.NEW.COMPANY = WS.ACCT.OFFCR.NEW + 100
            WS.NEW.COMPANY = 'EG00100':WS.NEW.COMPANY[2]

            R.NEW(EB.CUS.COMPANY.BOOK) =  WS.NEW.COMPANY

            TEXT = "Company of customer has been changed"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END
    RETURN
*-----------------------------------------------------
*** ADD BY MOHAMED SABRY 2012/12/17
DRMT.CHANGE:
    IF WS.DRMNT.CO.OLD NE WS.DRMNT.CO.NEW THEN
        IF WS.DRMNT.CO.OLD EQ '1' THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.DATE>   = ""
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>   = ''
            TEXT = "Dormant Code Removed"
            CALL STORE.OVERRIDE(CURR.NO)
        END

        IF WS.DRMNT.CO.NEW NE '' THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.DATE>   = TODAY
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>   = '1'
            TEXT = "Dormant Code Added"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END
*   IF WS.DRMNT.CO.OLD EQ WS.DRMNT.CO.NEW THEN
    IF (WS.DRMNT.CO.OLD EQ WS.DRMNT.CO.NEW) AND (WS.DRMNT.CO.NEW EQ '1') THEN
        TEXT = "Dormant Customer"
        CALL STORE.OVERRIDE(CURR.NO)
    END
    RETURN
*-----------------------------------------------------
