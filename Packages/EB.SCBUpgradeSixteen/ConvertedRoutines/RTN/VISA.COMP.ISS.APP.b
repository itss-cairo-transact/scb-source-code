* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>577</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.COMP.ISS.APP
 *   PROGRAM VISA.COMP.ISS.APP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
    COMP = ID.COMPANY
    PROCESS.CODE =''
    GOSUB INITIATE

    GOSUB PRINT.HEAD
*Line [ 47 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 48 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-15
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
* TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM
    RETURN
*==============================================================
INITIATE:
*    REPORT.ID='VISA.COMP.ISS.APP'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
REP:
    YTEXT = "���� 1- ������  2-����� ��� ����� "
    CALL TXTINP(YTEXT, 8, 22, "8", "")
    PROCESS.CODE = COMI
    E = ''
    IF PROCESS.CODE LT '1' OR PROCESS.CODE GT '2' THEN
        E = "�� ���� ���� 1 �� 2"
        CALL ERR;MESSAGE = 'REPEAT'

    END
    IF E THEN GOTO REP

    RETURN
*=============================================================
CALLDB:
    FN.APP  = 'F.SCB.VISA.APP'             ; F.APP = ''    ;R.APP = ''
    FN.CARD = 'FBNK.CARD.ISSUE'           ; F.CARD = ''    ;R.CARD = ''

    CALL OPF(FN.APP,F.APP)
    CALL OPF(FN.CARD,F.CARD)

    TOT.CARD = 0
    BRANCH.1 = COMP[8,2]
* DEBUG
    T.SEL = "SELECT FBNK.CARD.ISSUE WITH CAN.REASON EQ '' AND (@ID LIKE VICL... OR @ID LIKE VIGO...) AND BRANCH.NO EQ ":BRANCH.1
    T.SEL := " AND (CARD.CODE EQ 101 OR CARD.CODE EQ 102 OR CARD.CODE EQ 104 OR CARD.CODE EQ 105"
    T.SEL := " OR CARD.CODE EQ 201 OR CARD.CODE EQ 202 OR CARD.CODE EQ 204 OR CARD.CODE EQ 205)"
* T.SEL := " AND @ID LIKE ...3435"

    KEY.LIST=""  ; SELECTED=""  ;  E1=""
    KEY.LIST1=""  ; SELECTED1=""  ;  E2=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*TEXT =SELECTED ;CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CARD,KEY.LIST<I>,R.CARD,F.CARD,E1)
            T.SEL1 = "SELECT F.SCB.VISA.APP WITH MAIN.CUSTOMER EQ ":R.CARD<CARD.IS.LOCAL.REF,LRCI.VISA.CUST.NO>
            T.SEL1 :=" AND CARD.TYPE EQ ":R.CARD<CARD.IS.LOCAL.REF,LRCI.CARD.CODE>:" BY VISA.APP.DATE"
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG)
            R.APP = ''
            CALL F.READ(FN.APP,KEY.LIST1<SELECTED1>,R.APP,F.APP,E2)
*     TEXT = "COMI ":PROCESS.CODE ; CALL REM
*   IF (E2) THEN
*=================================================
            IF PROCESS.CODE EQ '1' THEN
                PRINT
                TOT.CARD = TOT.CARD + 1

                XX = SPACE(120)
                XX<1,1>[1,16]     = KEY.LIST<I>[6,16]
                XX<1,1>[20,50]    = R.CARD<CARD.IS.NAME>
                XX<1,1>[53,50]    = R.CARD<CARD.IS.LOCAL.REF,LRCI.CARD.CODE>
                XX<1,1>[59,8]     = R.CARD<CARD.IS.LOCAL.REF><1,LRCI.VISA.CUST.NO>
                IF (E2) THEN
                    XX<1,1>[85,10] = "����� ��� ���� "
                END
                ELSE
                    IF R.APP<SCB.VISA.PAYMENT.WAY> EQ 'VISA.ACCOUNT' THEN
                        XX<1,1>[75,10]    = R.APP<SCB.VISA.PERCENT>
                        XX<1,1>[105,10]   = R.APP<SCB.VISA.PAY.ACCT.NO>
                    END
                    ELSE
                        XX<1,1>[85,10] = "������ ������� "
                    END
                END
                PRINT XX<1,1>

*        END
            END
            ELSE
              *  TEXT = "COMI ":E2:" - ":R.APP ; CALL REM
                IF (E2) OR R.APP EQ '' THEN
                    TOT.CARD = TOT.CARD + 1
                    XX = SPACE(120)
                    XX<1,1>[1,16]     = KEY.LIST<I>[6,16]
                    XX<1,1>[20,50]    = R.CARD<CARD.IS.NAME>
                    XX<1,1>[53,50]    = R.CARD<CARD.IS.LOCAL.REF,LRCI.CARD.CODE>
                    XX<1,1>[59,8]     = R.CARD<CARD.IS.LOCAL.REF><1,LRCI.VISA.CUST.NO>
                    XX<1,1>[85,10] = "����� ��� ���� "
                    PRINT XX<1,1>
                END

            END
        NEXT I

        PRINT STR('=',125)
        PRINT ; PRINT SPACE(5):" ��� ��������  ":TOT.CARD
        PRINT STR('=',125)
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    YYBRN = BRANCH
    DATY = TODAY
*   T.DAY = DATY[3,2]:'/':DATY[5,2]:"/":DATY[0,4]
    T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"VISA.COMP.ISS.APP"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):" ���� ������� ������ ����� ����� ����� ������  "

*   PR.HD :="'L'":SPACE(50): " �� ������ " :ST.DATE :" ��� " : EN.DATE
    PR.HD :="'L'":SPACE(40):STR('_',55)


    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):" ��� �������" :SPACE(5):" ��� ������":SPACE(27):"�.������":SPACE(4):" ���� ������":SPACE(25):"���� ������"
    PR.HD :="'L'":STR('_',128)
**    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
