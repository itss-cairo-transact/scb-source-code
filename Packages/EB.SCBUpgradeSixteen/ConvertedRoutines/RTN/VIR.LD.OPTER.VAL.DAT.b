* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>160</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LD.OPTER.VAL.DAT


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PARMS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*TO CHECK INTEREST SPREAD RATE FOR STAFF
**************************************************************************************************
    ID = R.NEW(LD.CATEGORY):R.NEW(LD.CURRENCY)
    T.SEL = "SELECT F.SCB.PARMS WITH @ID LIKE ":ID:"..."
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        DIM AA(SELECTED)
        FOR I = 1 TO SELECTED
            D1 = RIGHT(KEY.LIST<I>,8)
            AA(I) = D1
        NEXT I
        TEMP = AA(1)
        SORT1 = 1
        FOR I = 2 TO SELECTED
            IF TEMP < AA(I)  THEN TEMP = AA(I); SORT1 =I
        NEXT I
        ID1 = KEY.LIST<SORT1>
        GOSUB OPEN.FILE
    END ELSE
*        TEXT = '������ ����� ���� �������'
 *       AF= LD.VALUE.DATE ; AV=1
  *      CALL STORE.OVERRIDE(R.NEW(LD.CURR.NO)+1)
    END
    GOTO END.RTN
****************************************************************************************************
OPEN.FILE:
    F.COUNT = '' ; FN.COUNT = 'F.SCB.PARMS' ; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
    CALL F.READ( FN.COUNT, ID1, R.COUNT, F.COUNT, ETEXT)
    D3 = R.NEW(LD.VALUE.DATE)
    D2 = TODAY
    CALL CDD("C",D2,D3,DAY)
    DAY = ABS(DAY)
    IF COMI < TODAY THEN
        IF R.COUNT<SCB.PAR.BACK.MAX.DAYS> LT DAY  THEN
*            TEXT = '��� �� �������'
 *           AF= LD.VALUE.DATE ; AV=1
  *          CALL STORE.OVERRIDE(R.NEW(LD.CURR.NO)+1)
        END
    END ELSE
        IF COMI > TODAY THEN
            IF R.COUNT<SCB.PAR.FORW.MAX.DAYS> LT DAY THEN
   *             TEXT = '��� �� �������'
    *            AF= LD.VALUE.DATE ; AV=1
     *           CALL STORE.OVERRIDE(R.NEW(LD.CURR.NO)+1)
            END
        END
    END
    RETURN
TEXT = R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.IND> ; CALL REM
*****************************************************************************************************
END.RTN:
    RETURN
END
