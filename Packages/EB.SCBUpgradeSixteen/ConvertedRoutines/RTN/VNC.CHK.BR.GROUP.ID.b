* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2011/06/25 ***
*******************************************
    SUBROUTINE VNC.CHK.BR.GROUP.ID

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP

    WS.ID.COMPANY = ID.COMPANY[2]
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C' OR V$FUNCTION = 'A' THEN
        WS.ID       = ID.NEW
        WS.SRL      = WS.ID[3]
        WS.SRL.DSCR = ''
        WS.BR.DSCR = '������� ������ ������ ��� '

        IF WS.SRL = '001' THEN
            WS.SRL.DSCR = '������'
            R.NEW(CG.LIMIT.CHK)  = 'Y'
        END ELSE
            WS.SRL.DSCR = '�����'
            R.NEW(CG.LIMIT.CHK)  = 'Y'
            IF WS.SRL = '003'   THEN
                WS.SRL.DSCR = '���� � �����'
                R.NEW(CG.LIMIT.CHK)  = 'N'
            END
        END

        IF WS.ID LE 99901000 THEN
            E = '��� ������ �� ��� ������ �����'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END ELSE
            WS.CHK.ID =  WS.ID[4,2]
            IF WS.CHK.ID EQ 99 THEN
                E = '��� ����� �������� ������ �������'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END
            IF WS.CHK.ID # WS.ID.COMPANY THEN
                E = '��� ����� ������� ��� ��� ���'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END ELSE
*IF R.OLD(CG.GROUP.NAME) EQ '' THEN
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,ID.COMPANY,WS.COMPANY.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,ID.COMPANY,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
WS.COMPANY.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
                R.NEW(CG.GROUP.NAME) = WS.BR.DSCR:' ':WS.COMPANY.NAME :' ':WS.SRL.DSCR
                R.NEW(CG.GROUP.ID)   = ID.NEW
                CALL REBUILD.SCREEN
*END
            END
        END
    END
    RETURN
END
