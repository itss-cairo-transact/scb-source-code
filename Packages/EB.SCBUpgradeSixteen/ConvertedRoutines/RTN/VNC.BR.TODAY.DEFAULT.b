* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VNC.BR.TODAY.DEFAULT

*MAKE ISSUE.DATE EQ TODAY & RECEIVE.DATE EQ TODAY & STATUS.DATE EQ TODAY & START.DATE EQ TODAY

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*-----------------------------------------------
    COMP.ID = "" ; USER.CO = ""
    COMP.ID = ID.COMPANY[2] + 0
    USER.CO = R.USER<EB.USE.DEPARTMENT.CODE>

    IF V$FUNCTION = 'I' THEN
        IF COMP.ID NE USER.CO THEN
            ETEXT = "INVALID COMPANY"
        END
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.ISSUE.DATE>   = TODAY
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE> = TODAY
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.STATUS.DATE>  = TODAY
        R.NEW(EB.BILL.REG.START.DATE)                     = TODAY

        IF PGM.VERSION EQ ',SCB.CHQ.LCY.REG.0017' THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MAT.DATE> = TODAY
        END

    END
    RETURN
END
