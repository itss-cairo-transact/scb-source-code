* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-32</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LD.EARLY.LIQ
*(CUSTOMER, DEAL.AMOUNT, DEAL.CURRENCY,
*   CURRENCY.MARKET, CROSS.RATE, CROSS.CURRENCY, DRAWDOWN.CCY, T.DATA,
*  CUST.COND, CHARGE.AMOUNT)

********PREPARED BY
****SAMEH*************************************************
*CHECK LIB YEAR
*
*CHECK CURRENCY IF CURRENCY EQ EGP LIQ.RATE EQ .02 ELSE LIQ.RATE EQ .01
*
*THERE IS NO INTEREST IF DEPOSIT PERIOD 3M OR MORE ELSE
*
*CALC LIQ.COMMISSION EQ AMOUNT * LIQ.RATE * PERIOD
*
**************************************************************************

*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.LD.ACC.BAL
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

    R.NEW(LD.INTEREST.BASIS)= "E"
    LDYEAR = R.NEW(LD.VALUE.DATE)[1,4]
    IF MOD(LDYEAR, 4) = 0 THEN
        LD.DAYS = 36600
    END ELSE
        LD.DAYS = 36500
    END


    TEMP.EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
    TEMP.ISSUE.DATE   = R.NEW(LD.VALUE.DATE)
    TEMP.DAY = 'C'
    CALL CDD("",TEMP.ISSUE.DATE,TEMP.EXP.DATE , TEMP.DAY)
    AMT = R.NEW(LD.AMOUNT)
    CURR = R.NEW(LD.CURRENCY)
    *TEXT = "DAY1 =   " : TEMP.DAY ; CALL REM
    *TEXT = "CURR" : CURR ; CALL REM
    *TEXT = " AMT " : R.NEW(LD.AMOUNT) ; CALL REM
    BEGIN CASE
***  CASE TEMP.DAY LE 30
********************************HYTHAM 20080813*****************
    CASE TEMP.DAY LT 7

        R.NEW(LD.NEW.INT.RATE)= "0"
        R.NEW(LD.INT.RATE.V.DATE) = R.NEW(LD.VALUE.DATE)
**********HYTH20080806***********

    CASE TEMP.DAY LT 30 AND CURR NE  "EGP"
        R.NEW(LD.NEW.INT.RATE)= "0"
        R.NEW(LD.INT.RATE.V.DATE) = R.NEW(LD.VALUE.DATE)
****************************HYTH*************************
    CASE TEMP.DAY GE 7 AND TEMP.DAY LT 30
        *TEXT = "HHHHHHHHHHH" ; CALL REM
        IF  CURR EQ "EGP" THEN
            *TEXT = "CURRRRRRR= " : CURR ; CALL REM
            IF R.NEW(LD.AMOUNT) GE '100000' THEN
                *TEXT = " DAYY=  " : TEMP.DAY ; CALL REM
                CATEG = "21001":TEMP.ISSUE.DATE
                GOSUB GET.RATE.DEPOSIT
            END
        END
*  END ELSE

*       R.NEW(LD.CHRG.AMOUNT)<1,1> = R.NEW( LD.TOT.INTEREST.AMT)
*     CALL REBUILD.SCREEN
*        END


    CASE TEMP.DAY GE 30 AND TEMP.DAY LT 60
        CATEG = "21003":TEMP.ISSUE.DATE
**   TEXT = "CATEG=":CATEG ; CALL REM
        GOSUB GET.RATE.DEPOSIT
**   TEXT = "RATE=":INT.RATE ; CALL REM

    CASE TEMP.DAY GE 60 AND TEMP.DAY LT 90
        IF R.NEW(LD.CURRENCY) EQ "EGP" THEN
            CATEG = "21003":TEMP.ISSUE.DATE
            GOSUB GET.RATE.DEPOSIT
        END ELSE
            CATEG = "21004":TEMP.ISSUE.DATE
            GOSUB GET.RATE.DEPOSIT
        END
    CASE TEMP.DAY GE 90 AND TEMP.DAY LT 180
        CATEG = "21005":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
    CASE TEMP.DAY GE 180 AND TEMP.DAY LT 360
        CATEG = "21006":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
    CASE TEMP.DAY GE 360 AND TEMP.DAY LT 720
        CATEG = "21007":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
    CASE TEMP.DAY GE 720 AND TEMP.DAY LT 1080
        CATEG = "21008":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
    CASE TEMP.DAY GE 1080
        CATEG = "21009":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
    CASE OTHERWISE

        RETURN

    END CASE

*TEXT = "DD" ; CALL REM
    LIQ.RATE = INT.RATE - 2
    TEXT = 'LIQ.RATE=':LIQ.RATE ; CALL REM
    R.NEW(LD.NEW.INT.RATE) = LIQ.RATE
    R.NEW(LD.INT.RATE.V.DATE) = R.NEW(LD.VALUE.DATE)
    CUSS = R.NEW(LD.CUSTOMER.ID)
    CUS = LEN(R.NEW(LD.CUSTOMER.ID))
    IF CUS EQ 7 THEN
        CLASS = CUSS[2,1]
    END ELSE
        CLASS = CUSS[3,1]
    END
*    TEXT = "CLASS":CLASS ; CALL REM
******************HYTHAM-20080807********************
*    IF CLASS NE 1 THEN
    IF CLASS NE 1 AND R.NEW(LD.CUSTOMER.ID) NE "1304955" THEN
*      TEXT = "INTTTT"  ; CALL REM
        R.NEW(LD.NEW.SPREAD) = 0
        R.NEW(LD.SPREAD.V.DATE) = R.NEW(LD.VALUE.DATE)
    END
* IF INT.RATE THEN
*    GOSUB CALC.LIQ.CHARGE.LESS3M
*END
    RETURN

*************************************************************
GET.RATE.DEPOSIT:
    F.LD.LEVEL = '' ; FN.LD.LEVEL = 'F.SCB.LD.TYPE.LEVEL' ; R.LD.LEVEL = ''

    CALL OPF(FN.LD.LEVEL,F.LD.LEVEL)

    CALL F.READ(FN.LD.LEVEL, CATEG, R.LD.LEVEL, F.LD.LEVEL, ETEXT)
    LOCATE CURR IN R.LD.LEVEL<LDTL.CURRENCY,1> SETTING POS THEN
        VV = R.LD.LEVEL<LDTL.CURRENCY,1>
        EE = R.LD.LEVEL<LDTL.RATE,POS>
*Line [ 167 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        TEMP.COUNT =  DCOUNT (R.LD.LEVEL<LDTL.AMT.FROM,POS>,@SM)
        FOR I = 1 TO TEMP.COUNT
            AMT.FROM = R.LD.LEVEL<LDTL.AMT.FROM,POS,I>
            AMT.TO = R.LD.LEVEL<LDTL.AMT.TO,POS,I>
            IF AMT GE AMT.FROM AND AMT LE AMT.TO THEN
                INT.RATE = R.LD.LEVEL<LDTL.RATE,POS,I>
                A = R.NEW(LD.INTEREST.RATE)
                B = R.LD.LEVEL<LDTL.RATE,POS,I>
            END

            IF TEMP.COUNT EQ I AND AMT GE R.LD.LEVEL<LDTL.AMT.FROM,POS,I> THEN
                INT.RATE = R.LD.LEVEL<LDTL.RATE,POS,I>

            END
        NEXT I
    END
*TEXT = "SESE == " : INT.RATE ; CALL REM

    RETURN



********************************************************
*CALC.LIQ.CHARGE.LESS3M:
*TEXT = R.NEW(LD.CHRG.AMOUNT,1) :"===SSS" ; CALL REM
*   IF R.NEW(LD.CHRG.AMOUNT,1) = '' THEN

*      EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
*     ISSUE.DATE   = R.NEW(LD.VALUE.DATE)
*    AMMOUNT      = R.NEW(LD.AMOUNT)
*   DAY          = 'C'
*  CALL CDD("",ISSUE.DATE,EXP.DATE , DAY)
* R.NEW(LD.CHRG.AMOUNT)<1,1> = (AMMOUNT * LIQ.RATE ) * (DAY / LD.DAYS )
* CHARGE.AMOUNT = R.NEW(LD.CHRG.AMOUNT,1)
* CALL REBUILD.SCREEN

*END

*ZZ = (AMMOUNT * LIQ.RATE ) * (DAY / LD.DAYS )
*IF R.NEW(LD.CHRG.AMOUNT,1) AND ZZ # R.NEW(LD.CHRG.AMOUNT,1) THEN

*   TEXT = "�� ���� ���� ������" ; CALL REM
*  CALL STORE.OVERRIDE(R.NEW(EB.CUS.CURR.NO)+1)

*END
    RETURN

************************************************************
END
