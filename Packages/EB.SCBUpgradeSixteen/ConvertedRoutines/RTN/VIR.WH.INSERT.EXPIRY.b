* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.WH.INSERT.EXPIRY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.EXPIRY
*-------------------------------------------------
    FN.WH.ITEMS = 'F.SCB.WH.ITEMS'    ; F.WH.ITEMS = ''
    CALL OPF( FN.WH.ITEMS,F.WH.ITEMS)

    FN.EXP = 'F.SCB.WH.EXPIRY'        ; F.EXP      = ''
    CALL OPF(FN.EXP,F.EXP)
*******************************UPDATED BY RIHAM R15**********************

*    OPEN FN.EXP TO FVAR.EXP ELSE
*        TEXT = "ERROR OPEN FILE" ; CALL REM
*        RETURN
*    END
*-------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='INA2' THEN
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DECOUNT.ITEM = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)
        FOR I = 1 TO DECOUNT.ITEM

            R.WH      = ''
            WH.ID     = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I> :'-': R.NEW(SCB.WH.TRANS.VALUE.DATE)<1,I>
            CALL F.READ(FN.EXP, WH.ID , R.WH , F.EXP, ETEXT)
            BRANCH.NO = R.NEW(SCB.WH.TRANS.BRANCH)

            IF LEN(BRANCH.NO) EQ 1 THEN
                BRANCH.NO = '0':BRANCH.NO
            END
            R.WH<EXP.CO.CODE> = 'EG00100':BRANCH.NO

            REG.ID   = FIELD(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),'.',1)
            CATEG.NO = REG.ID[13,4]

            R.WH<EXP.CREDIT.ACCT>   = R.NEW(SCB.WH.TRANS.CURRENCY) :'1':CATEG.NO:'000100':BRANCH.NO
            R.WH<EXP.VALUE.DATE>    = R.NEW(SCB.WH.TRANS.VALUE.DATE)<1,I>
            R.WH<EXP.EXPIRY.DATE>   = R.NEW(SCB.WH.TRANS.EXPIRY.DATE)<1,I>
            ACCT.ITEM               = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>
            R.WH<EXP.DEBIT.ACCT>    = ACCT.ITEM[3,16]
            R.WH<EXP.REF.WH.ID>     = ID.NEW
            NEW.AMT                 = R.WH<EXP.VALUE.BALANCE> + R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,I>
            R.WH<EXP.VALUE.BALANCE> = NEW.AMT

*WRITE R.WH TO F.EXP , WH.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':WH.ID:' TO FILE ':FN.EXP
*END
            CALL F.WRITE (FN.EXP,WH.ID,R.WH)
            ITEM.NUM = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>
            CALL F.READ(FN.WH.ITEMS,ITEM.NUM, R.WH.ITEMS, F.WH.ITEMS, ETEXT)
            R.WH.ITEMS<SCB.WH.IT.EXPIRING.DATE> = R.NEW(SCB.WH.TRANS.EXPIRY.DATE)<1,I>
            CALL F.WRITE(FN.WH.ITEMS,ITEM.NUM,R.WH.ITEMS)
        NEXT I
    END
*-----------------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='RNAU' THEN
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DECOUNT.ITEM = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)
        FOR I = 1 TO DECOUNT.ITEM
            R.WH<EXP.VALUE.DATE>  = R.NEW(SCB.WH.TRANS.VALUE.DATE)<1,I>
            ACCT.ITEM   = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>
            WH.ID       = ACCT.ITEM:'-':R.WH<EXP.VALUE.DATE>
*******************************UPDATED BY RIHAM R15**********************
*            DELETE FVAR.EXP, WH.ID
            CALL F.DELETE (FN.EXP,WH.ID)
*************************************************************************
        NEXT I
    END
*----------------------------------------------------------
    RETURN
END
