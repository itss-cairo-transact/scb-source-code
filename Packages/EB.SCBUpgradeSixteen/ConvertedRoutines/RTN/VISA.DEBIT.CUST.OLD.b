* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2295</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.DEBIT.CUST.OLD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS.TOT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* TO DEBIT THE TOTAL USAGES FROM CUSTOMERS VISA ACCOUNT AND PRINTOUT A REPORT CONTANING
* THE CUSTOMERS AND TOTAL AMOUNT TO BE DEBITED

    TEXT = 'YARAB' ; CALL REM

    GOSUB INITIALISE
*Line [ 54 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 55 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-15
    GOSUB CALLDB
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
    RETURN
*==============================================================
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS = "SCB.VISA"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*==============================================================
CALLDB:
    F.VISA.TRANS.TOT = '' ; FN.VISA.TRANS.TOT = 'F.SCB.VISA.TRANS.TOT' ; R.VISA.TRANS.TOT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.TRANS.TOT,F.VISA.TRANS.TOT)

    YTEXT = "Enter the Start Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    T.SEL = "SELECT F.SCB.VISA.TRANS.TOT WITH POS.DATE GE ":COMI :" BY @ID "
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
****************************************
        CALL F.READU(FN.VISA.TRANS.TOT,KEY.LIST<1>,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E2,RETRY2)
        DEP.CODE=R.VISA.TRANS.TOT<VISA.TRANS.BRANCH.NUMBER>
*****************************************
* XX=''  ; ZZ=''
        ACCT.NO = ''
        TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''  ; TOT.CUS.ALL = ''
        VISA.NO = KEY.LIST<1>[1,16]
        TEXT = 'VISA.NO=':KEY.LIST<1>[1,16] ; CALL REM
        ACCT.NO<1> = R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<1>,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO<1>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>

        TRNS.CODE=R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 121 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
        FOR J= 1 TO TRNS.COUNT
            T.CODE=TRNS.CODE<1,J>
            IF T.CODE EQ '1' THEN
                TOT.USE=TOT.USE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END
            IF T.CODE EQ '2' THEN
                TOT.INT=TOT.INT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END
            IF T.CODE EQ '3' THEN
                TOT.COMM=TOT.COMM+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END

            IF T.CODE EQ '4' THEN
                TOT.FEE=TOT.FEE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END

            IF T.CODE EQ '5' THEN
                TOT.ACT=TOT.ACT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>

            END
            IF T.CODE EQ '6' THEN
                TOT.REV=TOT.REV+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END
        NEXT J
        TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV
*******************************************************************************
***********************************
        FOR I = 2 TO SELECTED
            DEP.CODE = ''
            CALL F.READU(FN.VISA.TRANS.TOT,KEY.LIST<I>,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E2,RETRY2)
            DEP.CODE=R.VISA.TRANS.TOT<VISA.TRANS.BRANCH.NUMBER>
            ACCT.NO<I> = R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>
            IF ACCT.NO<I> # ACCT.NO<I-1> THEN
*************************************************************
*============CREATE FT FOR RECORDS============================*
                IF LEN(DEP.CODE) < 2 THEN
                    BR = '0':DEP.CODE
                END ELSE
                    BR = DEP.CODE
                END
*CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO<I>,ACC.CURR)
                ACC.CURR = ACCT.NO<I>[9,2]
                BR.ACC = '994999':'01':ACC.CURR:'5010':'01'

                COMMA = ","
                OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":'ACVU':COMMA
                OFS.MESSAGE.DATA := "DEBIT.CUSTOMER=": CUST.NO:COMMA
                OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": ACCT.NO<I-1>:COMMA
                OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.ALL:COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": 'EGP':COMMA

                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":BR.ACC:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
                OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE'

                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                OFS.ID = "T":TNO:".":OPERATOR:"_VISA":RND(10000):".":TODAY
                WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160703 - S
*                SCB.OFS.SOURCE = "SCBOFFLINE"
*                SCB.OFS.ID = '' ; SCB.OPT = ''
*                CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*                END
*                CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
********************************************************

                CUST.NAME = '' ; VISA.NO = '' ; TOT.ALL = ''
                TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''
                TOT.CUS.ALL = ''

                VISA.NO = KEY.LIST<I>[1,16]
                ACCT.NO<I> = R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>
*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<I>,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 224 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
                TRNS.CODE=R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 208 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
                FOR J= 1 TO TRNS.COUNT
                    T.CODE=TRNS.CODE<1,J>
                    IF T.CODE EQ '1' THEN
                        TOT.USE=TOT.USE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '2' THEN
                        TOT.INT=TOT.INT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '3' THEN
                        TOT.COMM=TOT.COMM+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '4' THEN
                        TOT.FEE=TOT.FEE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '5' THEN
                        TOT.ACT=TOT.ACT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>

                    END
                    IF T.CODE EQ '6' THEN
                        TOT.REV=TOT.REV+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                NEXT J
                TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV

            END ELSE

                TRNS.CODE=R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 239 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
                FOR J= 1 TO TRNS.COUNT
                    T.CODE=TRNS.CODE<1,J>
                    IF T.CODE EQ '1' THEN
                        TOT.USE=TOT.USE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '2' THEN
                        TOT.INT=TOT.INT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '3' THEN
                        TOT.COMM=TOT.COMM+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '4' THEN
                        TOT.FEE=TOT.FEE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '5' THEN
                        TOT.ACT=TOT.ACT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>

                    END
                    IF T.CODE EQ '6' THEN
                        TOT.REV=TOT.REV+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                NEXT J
                TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV

            END

**************************************************************************
            IF I = SELECTED   THEN
                TEXT = 'SELECTED END=':SELECTED ; CALL REM
*============CREATE FT FOR LAST RECORD============================*
*CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.NO<1>,CATEG)
                IF LEN(DEP.CODE) < 2 THEN
                    BR = '0':DEP.CODE
                END ELSE
                    BR = DEP.CODE
                END
*  CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO<1>,ACC.CURR)
                BR.ACC = '994999':'01':ACC.CURR:'5010':'01'
                COMMA = ","
                OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":'ACVU':COMMA
                OFS.MESSAGE.DATA := "DEBIT.CUSTOMER=": CUST.NO:COMMA
                OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": ACCT.NO<I-1>:COMMA
                OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.ALL:COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": 'EGP':COMMA

                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":BR.ACC:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
                OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE'

                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                OFS.ID = "T":TNO:".":OPERATOR:"_VISA":RND(10000):".":TODAY
                WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160703 - S
*                SCB.OFS.SOURCE = "SCBOFFLINE"
*                SCB.OFS.ID = '' ; SCB.OPT = ''
*                CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*                END
*                 CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
********************************************************
            END
**************************************************************************
        NEXT I
    END
    RETURN
END
