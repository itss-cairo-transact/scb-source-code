* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>740</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.CHK.CODES.NEW.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.NEW
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.DAILY.TRN
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CURR.CODES


    TEXT = 'HI' ; CALL REM
    Path = "NESRO/DAILYTR"
    TEXT = Path ; CALL REM
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    TEXT = 'WELCOME' ; CALL REM
    F.VISA.TRANS = '' ; FN.VISA.TRANS = 'F.SCB.VISA.TRANS' ; R.VISA.TRANS = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.VISA.TRANS,F.VISA.TRANS)

    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    EOF = ''
    LOOP WHILE NOT(EOF)

        CARD.NO = '' ;REASON.COD = ''; REASON.COD.TR = '' ; BANK.ACC = '' ; BANK.ACC.TR = '' ; ORG.MSGE = '' ; ORG.MSGE.TR = ''
        MSGE.TYPE = '' ; PROC.COD = '' ; BILL.CURR = '' ; BILL.AMT = '' ; BILL.AMT.TR = '' ; BILL.AMT.NN = '' ; BILL.AMT.1 = '' ; BILL.AMT.FMT = ''
        DB.CR = '' ; POST.DATE = '' ; PURCH.DATE = '' ; TRANS.CURR.GL = ''
        MSGE.DESC = '' ; MERCH.CITY = '' ; MERCH.COUNTRY = ''
        TRANS.AMT = '' ; TRANS.AMT.1 = '' ; TRANS.AMT.FMT = ''

        READSEQ Line FROM MyPath THEN
            CARD.NO           = Line[1,16]
            T.SEL =  "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""

            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
                    KEY.TO.USE = KEY.LIST<I>
                    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                    CALL F.READU(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1, RETRY1)
                    LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                    CUST.DEPT = LOCAL.REF<1,LRCI.BRANCH.NO>
                    BANK.ACC =  R.CARD.ISSUE<CARD.IS.ACCOUNT>
                    CUST.NAME = R.CARD.ISSUE<CARD.IS.NAME>
                NEXT I
                REASON.COD        = Line[34,4]
                REASON.COD.TR =  TRIM(REASON.COD, "", "D")
                LENCOD = LEN(REASON.COD.TR)
                IF LENCOD = 0 THEN
                    TEXT = 'ZERO' ; CALL REM
                    REASON.COD.TR = "NA"
                END
                ORG.MSGE          = Line[60,5]
                ORG.MSGE.TR =  TRIM(ORG.MSGE, " ", "T")
                MSGE.TYPE         = Line[66,4]
                PROC.COD          = Line[71,6]
                BILL.CURR         = Line[78,3]
*Line [ 103 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, BILL.CURR , BILL.CURR.GL)
F.ITSS.SCB.VISA.CURR.CODES = 'F.SCB.VISA.CURR.CODES'
FN.F.ITSS.SCB.VISA.CURR.CODES = ''
CALL OPF(F.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES)
CALL F.READ(F.ITSS.SCB.VISA.CURR.CODES,BILL.CURR,R.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES,ERROR.SCB.VISA.CURR.CODES)
BILL.CURR.GL=R.ITSS.SCB.VISA.CURR.CODES<SCB.CURR.CURRENCY>
                BILL.AMT.NN       = Line[84,16]
                BILL.AMT.1 = TRIM(BILL.AMT.NN, "0", "L")
                BILL.AMT.FMT = BILL.AMT.1/100

                DB.CR             = Line[101,2]
                POST.DATE         = Line[104,8]
                PURCH.DATE        = Line[113,8]

                TRANS.CURR        = Line[122,3]
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, TRANS.CURR , TRANS.CURR.GL)
F.ITSS.SCB.VISA.CURR.CODES = 'F.SCB.VISA.CURR.CODES'
FN.F.ITSS.SCB.VISA.CURR.CODES = ''
CALL OPF(F.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES)
CALL F.READ(F.ITSS.SCB.VISA.CURR.CODES,TRANS.CURR,R.ITSS.SCB.VISA.CURR.CODES,FN.F.ITSS.SCB.VISA.CURR.CODES,ERROR.SCB.VISA.CURR.CODES)
TRANS.CURR.GL=R.ITSS.SCB.VISA.CURR.CODES<SCB.CURR.CURRENCY>
                TRANS.AMT      = Line[126,16]
                TRANS.AMT.1 = TRIM(TRANS.AMT, "0", "L")
                TRANS.AMT.FMT = TRANS.AMT.1/100

                MSGE.DESC         = Line[174,24]
                CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MSGE.DESC

                MERCH.CITY        = Line[200,13]
                CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MERCH.CITY

                MERCH.COUNTRY     = Line[214,2]

* ID.KEY = CARD.NO:COMI
***********TO WRITE TO TABLE SCB.VISA.TRANS**************************
                ORG.MSG.CO = '' ; MSG.TYPE.CO = '' ; PROC.CODE.CO = '' ; DAILY.COMB = '' ; CODES.COMB = ''  ; DB.CR.CO = '' ;   REASON.CO.CO = ''
                ORG.MSG.CODE = '' ; MSG.TYPE.CODE = '' ; PROC.CODE.CODE = '' ; DB.CR.CODE = '' ; REASON.CO.CODE = '' ; CODE.TO.USE = ''

                ORG.MSG.CO   = ORG.MSGE.TR
                MSG.TYPE.CO = MSGE.TYPE
                PROC.CODE.CO = PROC.COD
                DB.CR.CO = DB.CR
                REASON.CO.CO=  REASON.COD.TR
                DAILY.COMB = ORG.MSG.CO:MSG.TYPE.CO:PROC.CODE.CO:DB.CR.CO:REASON.CO.CO
               * TEXT = 'DAILYCO=':DAILY.COMB ; CALL REM
                N.SEL =  "SELECT F.SCB.VISA.CODES.NEW WITH ORG.MSG.TYPE EQ ":ORG.MSG.CO:" AND MSG.TYPE EQ ":MSG.TYPE.CO:" AND PROCESS.CODE EQ ":PROC.CODE.CO:" AND FLAG EQ ":DB.CR.CO:" AND REASON.CODE EQ ":REASON.CO.CO
                KEY.LIST.2=""
                SELECTED.2=""
                ER.MSG.2=""

                CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
                F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.CODES.NEW' ; R.VISA.CODE = '' ; E3 = '' ; RETRY3 = ''
                CALL OPF(FN.VISA.CODE,F.VISA.CODE)
                IF NOT(SELECTED.2) THEN
                    TEXT = 'NEW CODE=':DAILY.COMB ; CALL REM
                END
**********************************************************************
            END ELSE          ;**END OF SELECTED***
* TEXT = 'NEW CARD=':CARD.NO ; CALL REM
            END
        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath
    TEXT = 'END OF FILE' ; CALL REM
    RETURN
END
