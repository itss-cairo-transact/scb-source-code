* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SF.INSERT.EXPIRY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.EXPIRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*-------------------------------------------------
    FN.SF = 'F.SCB.SF.TRANS'       ; F.SF = ''
    CALL OPF( FN.SF,F.SF)

    FN.EXP = 'F.SCB.SF.EXPIRY'     ; F.EXP = ''
    CALL OPF( FN.EXP,F.EXP)

*******************************UPDATED BY RIHAM R15**********************
    FN.EXP = 'F.SCB.SF.EXPIRY'     ; FVAR.EXP = ''
    CALL OPF( FN.EXP,FVAR.EXP)
*    OPEN FN.EXP TO FVAR.EXP ELSE
*        TEXT = "ERROR OPEN FILE" ; CALL REM
*        RETURN
*    END
*----------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='INAU' THEN
        SAFE.ID = R.NEW(SCB.SF.TR.BB.SAFF.KEEP.NO)
        CUST.ID = R.NEW(SCB.SF.TR.CUSTOMER.NO)
        RENT.ID = CUST.ID : "." : SAFE.ID

        FN.SF.R = "F.SCB.SAFEKEEP.RIGHT"   ; F.SF.R = ""
        CALL OPF(FN.SF.R,F.SF.R)
        CALL F.READ(FN.SF.R,RENT.ID,R.SF.R,F.SF.R,ER.SF.R)
        RENT.DATE  = R.SF.R<SCB.SAF.RIG.VALUE.DATE>
*        EXP.ID  = R.NEW(SCB.SF.TR.ACCT.CHRG.COMM) : "-" : RENT.DATE

        RENEW.DATE = RENT.DATE
        CALL ADD.MONTHS(RENEW.DATE,'12')

        EXP.ID  = R.NEW(SCB.SF.TR.ACCT.CHRG.COMM) : "-" : RENEW.DATE

        CALL F.READ(FN.EXP,EXP.ID,R.EXP,F.EXP,ER.EXP)
        R.EXP<SF.EXP.ACCOUNT.NO>  = R.NEW(SCB.SF.TR.ACCT.CHRG.COMM)
        R.EXP<SF.EXP.RENEW.DATE>  = RENEW.DATE
        R.EXP<SF.EXP.RENT.DATE>   = RENT.DATE
        R.EXP<SF.EXP.RENT.AMOUNT> = R.NEW(SCB.SF.TR.COMMISSION.AMT)
        R.EXP<SF.EXP.COMP.AMOUNT> = R.NEW(SCB.SF.TR.COMMISSION.AMT2)
        R.EXP<SF.EXP.CO.CODE>     = ID.COMPANY
        R.EXP<SF.EXP.SAFF.USED>   = 'NO'

*WRITE R.EXP TO FVAR.EXP , EXP.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':EXP.ID:' TO FILE ':FN.EXP
*END
        CALL F.WRITE (FN.EXP,EXP.ID,R.EXP)
    END
*----------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='RNAU' THEN
        SAFE.ID = R.NEW(SCB.SF.TR.BB.SAFF.KEEP.NO)
        CUST.ID = R.NEW(SCB.SF.TR.CUSTOMER.NO)
        RENT.ID = CUST.ID : "." : SAFE.ID

        FN.SF.R = "F.SCB.SAFEKEEP.RIGHT"   ; F.SF.R = ""
        CALL OPF(FN.SF.R,F.SF.R)
        CALL F.READ(FN.SF.R,RENT.ID,R.SF.R,F.SF.R,ER.SF.R)

        RENT.DATE = R.SF.R<SCB.SAF.RIG.VALUE.DATE>
        EXP.ID    = R.NEW(SCB.SF.TR.ACCT.CHRG.COMM) :"-": RENT.DATE
*******************************UPDATED BY RIHAM R15**********************

*        DELETE FVAR.EXP, EXP.ID
        CALL F.DELETE (FN.EXP,EXP.ID)
************************************************************************
    END
    RETURN
END
