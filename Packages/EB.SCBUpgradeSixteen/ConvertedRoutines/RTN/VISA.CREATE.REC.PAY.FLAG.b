* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
******NESSREEN AHMED 17/1/2012*********************
*-----------------------------------------------------------------------------
* <Rating>1809</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.CREATE.REC.PAY.FLAG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.FLAG

*****WRITTEN BY NESSREEN AHMED SCB*****
***************************************
    F.VISA.PAY.FL = '' ; FN.VISA.PAY.FL = 'F.SCB.VISA.PAY.FLAG' ; R.VISA.PAY.FL = '' ; E1 = ''
    CALL OPF(FN.VISA.PAY.FL,F.VISA.PAY.FL)

    DATEE = '' ; YEAR1 = '' ; MONTH1= ''  ; YEARN = '' ; YY = ''
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    TEXT = 'DATE=':DATEE ; CALL REM
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    YYYYMM = DATEE[1,6]
    TEXT = 'YEAR.MM=':YYYYMM ; CALL REM
***************************************
    BR = R.USER<EB.USE.DEPARTMENT.CODE>
    IF LEN(BR) < 2 THEN
        BR = '0':BR
    END ELSE
        BR = BR
    END
    COMP = "EG00100":BR
    TEXT = 'COMPANY=':COMP ; CALL REM
***    CHK.SEL = "SELECT F.SCB.VISA.PAY.FLAG WITH COMPANY.CO EQ " :COMP :" AND YEAR.MM EQ ":YYYYMM
***    KEY.LIST.CHK=""
***    SELECTED.CHK=""
***    ER.MSG.CHK=""
***    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
***    IF SELECTED.CHK THEN
***        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
***    END ELSE
    KEY.TO.USE = "EG0010099.":YYYYMM
    CALL F.READ(FN.VISA.PAY.FL,  KEY.TO.USE, R.VISA.PAY.FL, F.VISA.PAY.FL, E1)
    IF NOT(E1) THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        R.VISA.PAY.FL<PAFL.COMPANY.CO> = COMP
        R.VISA.PAY.FL<PAFL.TRANS.DATE> = TODAY
        R.VISA.PAY.FL<PAFL.YEAR.MM> = YYYYMM
        R.VISA.PAY.FL<PAFL.PAYMENT.FLAG> = 'YES'
        CALL F.WRITE(FN.VISA.PAY.FL, KEY.TO.USE, R.VISA.PAY.FL)
        CALL JOURNAL.UPDATE(KEY.TO.USE)
        TEXT = '�� ����� �������� �����' ; CALL REM
    END

    RETURN
END
