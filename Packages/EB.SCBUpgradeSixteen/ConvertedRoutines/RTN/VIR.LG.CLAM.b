* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*----------------------------NI7OOOOOOOOOOOOOOOOO-------------------------------------------------
    SUBROUTINE VIR.LG.CLAM

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

**    IF MESSAGE EQ 'VAL' THEN
    IF V$FUNCTION = 'A' THEN
*Line [ 36 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DECOUNT.CHARGE = DCOUNT(R.NEW(LD.CHRG.CODE),@VM)
        FOR I = 1 TO DECOUNT.CHARGE
            CHRG.CO   = R.NEW(LD.CHRG.CODE)<1,I>
            CHRG.AMT  = R.NEW(LD.CHRG.AMOUNT)<1,I>
            BOOK.DATE = R.NEW(LD.CHRG.BOOKED.ON)<1,I>
            CLAM.DATE = R.NEW(LD.CHRG.CLAIM.DATE)<1,I>

            TEXT = "CLAM.DATE : " :CLAM.DATE ; CALL REM

            IF (CHRG.CO NE '' OR CHRG.AMT NE '' OR BOOK.DATE NE '') AND CLAM.DATE EQ '' THEN
*Line [ 47 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                E = "CLAM DATE IS NULL"

*** SCB UPG 20160619 - S
* CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160619 - E

            END
* IF (CHRG.CO NE '' OR CHRG.AMT NE '' OR CLAM.DATE NE '') AND BOOK.DATE EQ '' THEN
* E = "BOOK DATE IS NULL " ; CALL ERR ; MESSAGE = 'REPEAT'
* END
            IF (CHRG.CO NE '' OR CLAM.DATE NE '' OR BOOK.DATE NE '') AND CHRG.AMT EQ '' THEN
*Line [ 60 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                E = "CHRG.AMT IS '' "
*** SCB UPG 20160619 - S
* CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160619 - E
            END

            IF (CLAM.DATE NE '' OR CHRG.AMT NE '' OR BOOK.DATE NE '') AND CHRG.CO EQ '' THEN
*Line [ 69 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                E = "CHRG.CODE IS '' "
*** SCB UPG 20160619 - S
* CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160619 - E
            END
        NEXT I
    END
    RETURN
END
