* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.AC.CU.TITLE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*TO DEFAULT FEILD ACCOUNT.TITLE,SHORT.TITLE,ARABIC.TITLE WITH ARABIC CUSTOMER NAME AND CATEGORY NAME

    IF V$FUNCTION = 'I' THEN
****UPDATED BY NESSREEN AHMED 10/10/2011******************
*----------- 2011/06/07 BY NESSMA
****        IF ID.NEW[1,1] EQ 0 THEN
****            ID.CUST = ID.NEW[2,7]
****        END ELSE
****            ID.CUST = ID.NEW[1,8]
****        END
****        CUST.NO = ID.CUST
*        R.NEW(AC.CUSTOMER) = ID.CUST
        ID.CUST= TRIM( ID.NEW[ 1, 8], '0', 'L')
        CUST.NO = ID.CUST
****END OF UPDATE 10/10/2011*******************************
        TEXT = ID.CUST ; CALL REM
*--------------------------
        ID.CURR= ID.NEW[9,2]
        ID.CAT = ID.NEW[11,4]
*        ID.CUST= TRIM( ID.NEW[ 1, 8], '0', 'L')

*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,ID.CAT,CATTITLE)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,ID.CAT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATTITLE=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,ID.CURR,CURRTITLE)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,ID.CURR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CURRTITLE=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CURRTITLE,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURRTITLE,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,ID.CUST,MYLOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ID.CUST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
MYLOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        ARNAME = MYLOCAL<1,CULR.ARABIC.NAME>

        TEXT = ARNAME ; CALL REM
        R.NEW(AC.CUSTOMER) = CUST.NO
        R.NEW(AC.ACCOUNT.TITLE.1) = ARNAME
        R.NEW(AC.SHORT.TITLE)     = ARNAME
        R.NEW(AC.LOCAL.REF)<1,ACLR.ARABIC.TITLE> = ARNAME
        CALL REBUILD.SCREEN

    END
    RETURN
END
