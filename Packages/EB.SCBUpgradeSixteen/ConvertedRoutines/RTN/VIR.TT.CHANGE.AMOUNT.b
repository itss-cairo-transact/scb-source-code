* @ValidationCode : MjotMTQ0MjA3MTc4OkNwMTI1MjoxNjQyMjcxMzU4MDI0OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 20:29:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-53</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VIR.TT.CHANGE.AMOUNT


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS

*************      FBNK.ENTRY.HOLD       *********************
*5520000210100101]EG0010001]-100.00]35]]]]55200002]1 ]1001 ]20060625]EGP]]]]TR]]]]1]]1]TT0617300004]TT]20060622]]]]]]]]]]]]]]]]]]01]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
*EGP100005501    ]EG0010001]100.00 ]52]]]]        ]55]10000]20060622]EGP]]]]TR]]]]1]]1]TT0617300004]TT]20060622]]]]]]]]]]]]]]]]]]01]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
*1]1
**************************************************************

    FN.ENTRY.HOLD = 'FBNK.ENTRY.HOLD' ; F.ENTRY.HOLD = '' ; R.ENTRY.HOLD = ''
    CALL OPF( FN.ENTRY.HOLD,F.ENTRY.HOLD)
    CALL F.READ( FN.ENTRY.HOLD,"TT":ID.NEW, R.ENTRY.HOLD, F.ENTRY.HOLD, ETEXT)
    R.ENTRY.HOLD<1,1>   = "ACCOUNT NO":"]"
    R.ENTRY.HOLD<1,2>  := "EG0010001":"]"
    R.ENTRY.HOLD<1,3>  := "AMOUNT":"]"
    R.ENTRY.HOLD<1,4>  := "TRANSACTION CODE":"]"
    R.ENTRY.HOLD<1,9>  := "DEPT.CODE":"]"
*Line [ 45 ] Comment unused variables - ITSS - R21 Upgrade - 2021-12-23
*R.ENTRY.HOLD<1,6>  :=
*R.ENTRY.HOLD<1,7>  :=
*R.ENTRY.HOLD<1,8>  :=
*R.ENTRY.HOLD<1,9>  :=
*R.ENTRY.HOLD<1,10> :=
*R.ENTRY.HOLD<1,11> :=
    CALL F.WRITE(FN.ENTRY.HOLD,"TT":ID.NEW,R.ENTRY.HOLD)

RETURN
END
