* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VNC.BOAT.ID.CHECK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER

    IF V$FUNCTION = 'I' THEN
        ETEXT = ''
*Line [ 27 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('SCB.BOAT.CUSTOMER':@FM:BO.STATUS,ID.NEW,STT)
F.ITSS.SCB.BOAT.CUSTOMER = 'F.SCB.BOAT.CUSTOMER'
FN.F.ITSS.SCB.BOAT.CUSTOMER = ''
CALL OPF(F.ITSS.SCB.BOAT.CUSTOMER,FN.F.ITSS.SCB.BOAT.CUSTOMER)
CALL F.READ(F.ITSS.SCB.BOAT.CUSTOMER,ID.NEW,R.ITSS.SCB.BOAT.CUSTOMER,FN.F.ITSS.SCB.BOAT.CUSTOMER,ERROR.SCB.BOAT.CUSTOMER)
STT=R.ITSS.SCB.BOAT.CUSTOMER<BO.STATUS>

        IF NOT(ETEXT) THEN
            E = 'THIS RECORD ALREADY AUTHORISED'
            CALL ERR ; MESSAGE = 'REPEAT'
        END


        ETEXT = ''
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('SCB.BOAT.CUSTOMER$NAU':@FM:BO.STATUS,ID.NEW,STT)
F.ITSS.SCB.BOAT.CUSTOMER$NAU = 'F.SCB.BOAT.CUSTOMER$NAU'
FN.F.ITSS.SCB.BOAT.CUSTOMER$NAU = ''
CALL OPF(F.ITSS.SCB.BOAT.CUSTOMER$NAU,FN.F.ITSS.SCB.BOAT.CUSTOMER$NAU)
CALL F.READ(F.ITSS.SCB.BOAT.CUSTOMER$NAU,ID.NEW,R.ITSS.SCB.BOAT.CUSTOMER$NAU,FN.F.ITSS.SCB.BOAT.CUSTOMER$NAU,ERROR.SCB.BOAT.CUSTOMER$NAU)
STT=R.ITSS.SCB.BOAT.CUSTOMER$NAU<BO.STATUS>
        IF NOT(ETEXT) THEN
            E = 'THIS RECORD ALREADY STORED'
            CALL ERR ; MESSAGE = 'REPEAT'
        END

    END

    RETURN
END
