* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BR.CHECK.EXIST.CHQ

* PREVENT USER FORM INPUTING A NEW RECORD
* PREVENT USER FROM MODIFYING A RECORD

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN

        FN.BR = "FBNK.BILL.REGISTER"
        F.BR  = ""
        ETEXT = ""

        CALL OPF(FN.BR,F.BR)
        CALL F.READ(FN.BR,ID.NEW,R.BR,F.BR,ETEXT)
        IF ETEXT THEN
            E = "��� �����" ;CALL ERR ; MESSAGE = 'REPEAT'
        END
**********
        CHQ.CHECK = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.OPERATION.TYPE>
        IF CHQ.CHECK NE "����� �����" THEN
            E = "�� ���� ����� ��������� �� ���� �������";CALL ERR ;MESSAGE = 'REPEAT'
        END
*********
        BR.STATUS = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
        IF BR.STATUS EQ 7 OR BR.STATUS EQ 8 THEN
            E = "������ �������";CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
    RETURN
END
