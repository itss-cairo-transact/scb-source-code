* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.ISCO.SETT.DATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CF

    FN.ISC = 'F.SCB.ISCO.SME.CF'; F.ISC = ''
    CALL OPF(FN.ISC,F.ISC)
    FN.CU = 'FBNK.CUSTOMER'; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC = 'FBNK.ACCOUNT'; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    ETEXT      = ''
    T.TODAY    = TODAY
    MONTH      = T.TODAY[5,2]
    YEAR       = T.TODAY[1,4]

    IF MONTH EQ 1 THEN
        NEW.YEAR  = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.YEAR  = YEAR
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END

    OLD.DATE  = NEW.YEAR:NEW.MONTH:'01'

    NEW.DATE  = YEAR:MONTH:'01'

    SETT.DATE = R.NEW(ISCO.CF.CF.SETTLEMENT.DATE)
    CHK.MONTH = NEW.MONTH * 1

    IF V$FUNCTION = "I" THEN
        IF SETT.DATE GE NEW.DATE OR SETT.DATE LT OLD.DATE THEN
            E =  '��� �� ���� ������� ���� ��� ':CHK.MONTH
            CALL ERR  ; MESSAGE = 'REPEAT'
        END

    END
    RETURN
END
