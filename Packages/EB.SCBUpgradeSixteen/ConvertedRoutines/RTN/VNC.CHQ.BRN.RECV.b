* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VNC.CHQ.BRN.RECV

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CA = 'F.SCB.CHEQ.APPL' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    CALL F.READ(FN.CA,ID.NEW,R.CA,F.CA,E1)

    IF E1 THEN
        E = 'THIS RECORD NOT EXIST'
*** SCB UPG 20160623 - S
*        CALL ERR ; MESSAGE = 'REPEAT'
*        E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
    END ELSE
        WS.RECV.DATE = R.CA<CHQA.BRN.RECV.DATE>
        SND.DAT = R.CA<CHQA.SEND.TO.BRN.DATE>
        IF SND.DAT EQ '' THEN
            E = 'THIS RECORD NOT SENT FROM OPERATION CENTER'
*** SCB UPG 20160623 - S
*            CALL ERR ; MESSAGE = 'REPEAT'
*            E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END ELSE
            IF WS.RECV.DATE NE '' THEN
                E = 'THIS RECORD RECEIVED BEFORE'
*** SCB UPG 20160623 - S
*                CALL ERR ; MESSAGE = 'REPEAT'
*                E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END ELSE
                R.NEW(CHQA.BRN.RECV.DATE) = TODAY
            END
        END
    END

    IF V$FUNCTION = 'R' THEN
        TRN.DAT = R.NEW(CHQA.TRN.DATE)
        IF TRN.DAT NE '' THEN
            E = 'YOU CAN NOT REVERSE THIS RECORD'
*** SCB UPG 20160623 - S
*            CALL ERR ; MESSAGE = 'REPEAT'
*            E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END

    RETURN
END
