* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
********* RANIA  17/06/2003 **************

    SUBROUTINE VIR.LG.MT769

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.769


    FN.SCB.LG.769='F.SCB.LG.769';ID=ID.NEW;R.LG='';;F.LG=''
    CALL F.READ( FN.SCB.LG.769,ID, R.LG, F.LG, ETEXT)
    IF ETEXT THEN
        R.LG<SCB.AMOUNT.INCREASE> = ABS(R.NEW(LD.AMOUNT.INCREASE))
        R.LG<SCB.AMT.V.DATE> = R.NEW(LD.AMT.V.DATE)
        TEXT=  R.LG<SCB.AMOUNT.INCREASE> ;CALL REM
    END ELSE
        R.LG<SCB.AMOUNT.INCREASE> = ABS(R.NEW(LD.AMOUNT.INCREASE))
        R.LG<SCB.AMT.V.DATE> = R.NEW(LD.AMT.V.DATE)
    END

    CALL F.WRITE(FN.SCB.LG.769,ID,R.LG)


    RETURN
END
