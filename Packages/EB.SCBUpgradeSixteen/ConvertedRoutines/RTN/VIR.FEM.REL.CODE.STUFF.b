* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*---- ---- ---- ---- MAI SAAD ---- ---- ---- ----
* <Rating>-1</Rating>
*---- ---- ---- ---- ----------------------------
    SUBROUTINE VIR.FEM.REL.CODE.STUFF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    FN.CU   = "FBNK.CUSTOMER" ; F.CU   = ""
    CALL OPF (FN.CU,F.CU)

    FEM.COUNT = 0
    CUS.ID = ID.NEW
    RELATION.CODE = R.NEW(EB.CUS.RELATION.CODE)

*Line [ 40 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    REL.CODE.COUNT = DCOUNT(RELATION.CODE,@VM)
    FOR J = 1 TO REL.CODE.COUNT
        REL.CODE =  R.NEW(EB.CUS.RELATION.CODE)<1,J>
        IF REL.CODE EQ '89' THEN
            PARTICIPATION.PER = R.NEW(EB.CUS.ROLE.MORE.INFO)<1,J>
            FEM.COUNT = FEM.COUNT+1
            IF PARTICIPATION.PER EQ '' THEN
               ETEXT='���89 -����� ����� ���� ������ �������';CALL STORE.END.ERROR
*                E = '����� ����� ���� ������ �������'
*CALL ERR
*MESSAGE = 'REPEAT'
            END
        END
    NEXT J
    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OWNERSHIP.PER> = FEM.COUNT
    RETURN
END
