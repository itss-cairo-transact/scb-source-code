* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*----------------------------MAI 2020-------------------------------------------------
    SUBROUTINE VNC.ACC.UNI.DATE
*------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG

    IF V$FUNCTION = 'I' THEN
        TDR = TODAY
        CALL CDT('',TDR,"+1W")
        R.NEW(FT.CREDIT.VALUE.DATE)  = TDR

        USER = OPERATOR
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,USER,DEP.NO)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,USER,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEP.NO=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEP.NO,DEP.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEP.NO,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEP.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
        DESC.DEP.NAME = FIELD(DEP.NAME,".",2)
        R.NEW(FT.ORDERING.CUST)     = DESC.DEP.NAME
        R.NEW(FT.DEBIT.VALUE.DATE)  = TODAY
        R.NEW(FT.DEBIT.CURRENCY)    = 'EGP'
        R.NEW(FT.CREDIT.CURRENCY)   = 'EGP'
*       R.NEW(FT.CREDIT.ACCT.NO)    = '9949990010322001'
        R.NEW(FT.CREDIT.CUSTOMER)   = '99499900'

    END

    RETURN
END
