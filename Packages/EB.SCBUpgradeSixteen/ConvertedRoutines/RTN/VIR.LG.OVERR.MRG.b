* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.LG.OVERR.MRG

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OVERRIDE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> NE R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> THEN
            OLD.NEW.MRG =" OLD  ":R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>:"NEW ":R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
            GOSUB MSG.MRG
        END

        IF R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> NE R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> THEN
            OLD.NEW.MRG =" OLD  ":R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>:"NEW ":R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            GOSUB MSG.MRG
        END
    END
    RETURN
************
MSG.MRG:
    CHG.ACCT  = R.NEW(LD.CHRG.LIQ.ACCT)
    DR.ACCT   = R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DR.ACCT,DR.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CHG.ACCT,CHG.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CHG.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CHG.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>

    IF CHG.CAT EQ '1220' OR DR.CAT EQ '1220' THEN
*Line [ 51 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        TEXT = "LG.MRG.INC.DEC":@FM:OLD.NEW.MRG
        CALL STORE.OVERRIDE(CURR.NO)
    END
    RETURN
END
