* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHQ.ERR.CHECK.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CHQ = 'F.SCB.CHEQ.APPL' ; F.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    WS.APP.ID  = ID.NEW

    CALL F.READ(FN.CHQ,WS.APP.ID,R.CHQ,F.CHQ,E1)

    IF E1 THEN
        E = '��� ����� ��� �����'
*** SCB UPG 20160623 - S
*        CALL ERR ; MESSAGE = 'REPEAT'
*        E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
    END

    IF V$FUNCTION = 'R' THEN
        E = 'YOU CAN NOT REVERSE THIS RECORD'
*** SCB UPG 20160623 - S
*        CALL ERR ; MESSAGE = 'REPEAT'
*        E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
    END


    RETURN
END
