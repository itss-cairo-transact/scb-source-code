* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>281</Rating>
*-----------------------------------------------------------------------------
** ----- 17.6.2002 (MRA GROUP) -----

    SUBROUTINE VNC.AC.CUST.DEFAULTS.BR

*ROUTINE 1- TO CHECK THAT THIS RECORD IS RETREIVED FROM SAME VERSION IT IS INPUTTED FROM,ALSO
*           TO CHECK FOR EXISTING ACCOUNTS THAT BELONG TO THE SAME BRANCH AND THAT IT IS NOT
*           A CLOSURE ACCOUNT.                                                         ...MOHAMED...
*
*        2- TO CHECK THAT THE ID HAVE NUMERIC CHARACTERS ONLY.                        ... RANIA ...
*        3- TO DEFAULT LIMIT.REF & LINK TO LIMIT ACCORDING TO DEBIT ACCOUNTS.         ... RANIA ...
*        4- TO DEFAULT DEBIT.CREDIT WITH CREDIT.                                      ... ABEER ...
*        5- TO DEFAULT THE PASSBOOK FIELD TO 'YES' WITH ACCOUNT TYPE = SAVING.        ... ABEER ...


*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
**************************************************************
*  CUS=ID.NEW[1,8]
*   CUR=ID.NEW[9,2]
* TEXT= CUS ;CALL REM
*TEXT= CUR  ;CALL REM
*    R.NEW(AC.INTEREST.LIQU.ACCT) =CUS:CUR:"100101"
*    R.NEW(AC.CHARGE.ACCOUNT) =CUS:CUR:"100101"

**************************************************************


*----- (1)
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'A' THEN

        VERSION.NAME =  R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> ; ETEXT = ''
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT$NAU':@FM:AC.LOCAL.REF, ID.NEW, LOCAL.REF)
F.ITSS.ACCOUNT$NAU = 'F.ACCOUNT$NAU'
FN.F.ITSS.ACCOUNT$NAU = ''
CALL OPF(F.ITSS.ACCOUNT$NAU,FN.F.ITSS.ACCOUNT$NAU)
CALL F.READ(F.ITSS.ACCOUNT$NAU,ID.NEW,R.ITSS.ACCOUNT$NAU,FN.F.ITSS.ACCOUNT$NAU,ERROR.ACCOUNT$NAU)
LOCAL.REF=R.ITSS.ACCOUNT$NAU<AC.LOCAL.REF>
        VER.NAME = LOCAL.REF<1, ACLR.VERSION.NAME>
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ID.NEW, ACC.OFF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ID.NEW,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFF=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ID.NEW,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ID.NEW,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        ACC.OFF  = AC.COMP[8,2]

        IF NOT(ETEXT) THEN
            ACCT.OFF1 = R.NEW(AC.CO.CODE)
            ACCT.OFF2  = ACCT.OFF1[8,2]
            ACCT.OFF3  = TRIM(ACCT.OFF2, "0" , "L")
*IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
            IF ACCT.OFF3 AND ACCT.OFF3 NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
*   E='Only.Function.See.Is.Allowed'
                E='��� ����� ��������';CALL ERR;MESSAGE='REPEAT'
                V$FUNCTION = 'S'
            END
        END
*          IF NOT(ETEXT) AND VERSION.NAME # PGM.VERSION THEN
*          E = 'You.Must.Retreive.Record.From &' : @FM : VERSION.NAME
*          GOSUB MYERROR
*          END

** IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
**         E = 'This.Account.From.Other.Branch' ; GOSUB MYERROR
**     END

        IF  R.NEW(AC.POSTING.RESTRICT) GE '90' THEN
            E = 'This.Is.Closure.Account'
            E = '��� ������ ����' ; GOSUB MYERROR
        END

    END

*------------------------
    IF V$FUNCTION = 'I' THEN

        CATEGORY = ID.NEW [11,4]

*------(2)
* E = 'Bad.Account.Number'
        IF NOT(NUM(ID.NEW)) THEN E = '��� �� ��� ������' ; GOSUB MYERROR

*----- (3)
****************29-10-2004
        IF ( CATEGORY > 9000 AND CATEGORY < 9008 ) THEN
            R.NEW (AC.LIMIT.REF) = ''
            R.NEW (AC.LINK.TO.LIMIT) = 'NO'

        END ELSE

            E= 'category not in range 9001 to 9007';CALL ERR;MESSAGE='REPEAT'

        END

*----- (4)

        IF NOT (R.NEW( AC.LOCAL.REF)< 1, ACLR.DEBIT.CREDIT>) THEN
            R.NEW( AC.LOCAL.REF)< 1, ACLR.DEBIT.CREDIT> = 'C'
        END

*----- (5)
****************29-10-2004
*IF R.NEW(AC.CATEGORY)
        IF ( CATEGORY > 6000 AND CATEGORY < 6599 ) THEN R.NEW(AC.PASSBOOK) = 'NO'
        ELSE  R.NEW(AC.PASSBOOK) = 'NO'

*-----

    END

    RETURN

MYERROR:
    CALL ERR ; MESSAGE = 'REPEAT'

    RETURN


*-----
END
