* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BR.BILL.RETURN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

*    TEXT = "VNC.BR.BILL.RETURN" ; CALL REM

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = '' ; ETEXT = ''
    CALL OPF( FN.BR,F.BR)

    KEY.LIST=""; SELECTED="" ; ER.MSG=""
***   T.SEL ="SELECT FBNK.BILL.REGISTER WITH DRAWER EQ 1301751 AND MAT.DATE EQ 20100701 AND BILL.CHQ.STA EQ 2"

**    T.SEL ="SELECT FBNK.BILL.REGISTER WITH AMOUNT EQ 100 AND BIL.CHQ.TYPE EQ 7 AND BILL.CHQ.STA EQ 1 AND ( INPUTTER LIKE ...24341... OR INPUTTER LIKE ...70203... OR INPUTTER LIKE ...5142...) "
**    T.SEL =" SELECT FBNK.BILL.REGISTER WITH CONT.ACCT EQ 0130175110900301 AND MATURITY.EXT EQ 20100103 AND BILL.CHQ.STA EQ 2 "

    CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)

    CALL F.READ( FN.BR,KEY.LIST<I>, R.BR, F.BR, ETEXT)

 *   TEXT = SELECTED ; CALL REM

    FOR I =1 TO SELECTED

*TEXT = KEY.LIST<I> ; CALL REM

        R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = KEY.LIST<I>
        R.NEW(SCB.BT.RETURN.REASON)<1,I> = 313
    NEXT I

    RETURN
END
