* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.FAW.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

********************************************************************
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

*****************TO CHECK DEBIT AND CREDIT ACCOUNT ********************

    ACCC1 = R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC1,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCC1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF CATEG EQ '' THEN

        ETEXT = ',RESPONS.CODE=13' ;  CALL STORE.END.ERROR
    END
*****************TO STOP A DIFFERENT CUSTOMER************
    IF R.NEW(FT.CREDIT.CUSTOMER) = R.NEW(FT.DEBIT.CUSTOMER) THEN

        ETEXT = ',RESPONS.CODE=12' ; CALL STORE.END.ERROR

    END
*****************TO STOP THE SAME ACCOUNT*************
    IF R.NEW(FT.DEBIT.ACCT.NO)[1,8]  = R.NEW(FT.CREDIT.ACCT.NO)[1,8] THEN

        ETEXT = ',RESPONS.CODE=12' ; CALL STORE.END.ERROR
    END
*****************TO STOP MISSING AMOUNT************
    IF R.NEW(FT.DEBIT.AMOUNT) = '' THEN

        ETEXT = ',RESPONS.CODE =20' ; CALL STORE.END.ERROR

    END

*****************TO CHECK TRANSACTION************
    IF R.NEW(FT.TRANSACTION.TYPE) # 'ACFA' THEN

        ETEXT = ',RESPONS.CODE=01' ; CALL STORE.END.ERROR

    END

*****************TO CHECK DEBIT AND CREDIT ACCOUNT ********************
    ACCC3 = R.NEW(FT.DEBIT.ACCT.NO)
    CALL F.READ(FN.AC,ACCC3,R.AC,F.AC,E2)
    CUS.ID  = R.AC<AC.CUSTOMER>
    AC.POST = R.AC<AC.POSTING.RESTRICT>
*******
    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E3)
    CUS.POST = R.CU<EB.CUS.POSTING.RESTRICT>

    IF AC.POST NE '' OR CUS.POST NE '' THEN
        ETEXT = ',RESPONS.CODE=08' ;  CALL STORE.END.ERROR
    END
*********************************************************
    ACCC3 = R.NEW(FT.DEBIT.ACCT.NO)
    CALL F.READ(FN.AC,ACCC3,R.AC,F.AC,E2)
    IF E2 THEN
        ETEXT = ',RESPONS.CODE=03' ;  CALL STORE.END.ERROR
    END

    ACCC4 = R.NEW(FT.CREDIT.ACCT.NO)
    CALL F.READ(FN.AC,ACCC4,R.AC,F.AC,E3)
    IF E3 THEN
        ETEXT = ',RESPONS.CODE=03' ;  CALL STORE.END.ERROR
    END
****************TO CHECK THE CURRENCY********************

        IF R.NEW(FT.DEBIT.CURRENCY) # 'EGP'  THEN

            ETEXT = ',RESPONS.CODE=06' ; CALL STORE.END.ERROR
        END

*****************TO CHECK DEBIT ACCOUNT *********************
        DEB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)

        IF NUM(DEB.ACCT[1,3]) THEN
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR("ACCOUNT":@FM:AC.CATEGORY,DEB.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEB.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
            IF (CATEG >= 5000 AND CATEG <= 5999) THEN
                ETEXT = ',RESPONS.CODE=99' ; CALL STORE.END.ERROR
            END
        END ELSE
            ETEXT = ',RESPONS.CODE=99' ; CALL STORE.END.ERROR
        END

*****************TO CHECK CREDIT ACCOUNT *********************
        CRD.ACCT = R.NEW(FT.CREDIT.ACCT.NO)

        IF NUM(CRD.ACCT[1,3]) THEN
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR("ACCOUNT":@FM:AC.CATEGORY,CRD.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CRD.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
            IF (CATEG >= 5000 AND CATEG <= 5999) THEN
                ETEXT = ',RESPONS.CODE=99' ; CALL STORE.END.ERROR
            END


*        ETEXT = ',RESPONS.CODE=99' ; CALL STORE.END.ERROR
        END
*****************TO GET THE NET VALUE*********************
        SCB.ACT.CB = R.NEW(FT.DEBIT.ACCT.NO)
        SCB.CHK.AMT = FIELD(R.NEW(FT.AMOUNT.DEBITED),R.NEW(FT.DEBIT.CURRENCY),2)
*Line [ 159 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,SCB.ACT.CB,SCB.W.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
SCB.W.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
        SCB.W.BAL += SCB.CHK.AMT        ;*R.NEW(FT.DEBIT.AMOUNT)
        ACT.CATEG = SCB.ACT.CB[11,4]
*        IF (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR ACT.CATEG EQ 6511 OR ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1002 OR ACT.CATEG EQ 1003 OR ACT.CATEG EQ 1005 OR ACT.CATEG EQ 1006 OR (ACT.CATEG GE  1101 AND ACT.CATEG LE 1202) OR (ACT.CATEG GE  1290 AND ACT.CATEG LE 1599) THEN
*Line [ 169 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,SCB.ACT.CB,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
            IF LIM.REF THEN
*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.ACT.CB,ACT.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACT.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
                LIM.ID = ACT.CUS:".":"0000":LIM.REF
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM.ID,AVIL.BAL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVIL.BAL=R.ITSS.LIMIT<LI.AVAIL.AMT>
*Line [ 192 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('LIMIT':@FM:LI.EXPIRY.DATE,LIM.ID,SCB.EXP.DAT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
SCB.EXP.DAT=R.ITSS.LIMIT<LI.EXPIRY.DATE>
            END ELSE
                AVIL.BAL = 0
            END
            END.BAL.L22 = SCB.W.BAL + AVIL.BAL

*Line [ 204 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,SCB.ACT.CB,LOC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,SCB.ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOC.BAL=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 157 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            BALC = DCOUNT(LOC.BAL,@VM)
            BALCC = LOC.BAL<1,BALC>

            END.BAL.L = END.BAL.L22 - BALCC


            IF END.BAL.L LT SCB.CHK.AMT THEN

                ETEXT = ',RESPONS.CODE=14' ; CALL STORE.END.ERROR

            END

            IF SCB.EXP.DAT LE TODAY AND SCB.EXP.DAT THEN

                ETEXT = ',RESPONS.CODE=14' ; CALL STORE.END.ERROR

            END

***************APPROVED**********
            CALL F.READ(FN.FT,ID.NEW,R.FT,F.FT,E6)

            IF ID.NEW NE ''  THEN

                R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT> = ',RESPONS.CODE=00'

            END
            RETURN
        END
