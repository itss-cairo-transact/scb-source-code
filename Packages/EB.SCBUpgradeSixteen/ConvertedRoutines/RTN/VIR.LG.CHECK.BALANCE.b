* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>395</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LG.CHECK.BALANCE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF MESSAGE EQ 'VAL' THEN
        CU.NO = R.NEW(LD.CUSTOMER.ID)
        LG.AC = R.NEW( LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>

*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,LG.AC,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,LG.AC,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
        OLD.MARG = MYLOCAL<1,LDLR.MARGIN.AMT>

        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> GT OLD.MARG THEN
            DIFF.AMT=R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - OLD.MARG
        END ELSE
            IF  R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> LT OLD.MARG THEN
                DIFF.AMT=0
            END ELSE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> EQ OLD.MARG THEN
                    DIFF.AMT=0
                END
            END
        END
        IF LG.AC EQ R.NEW(LD.CHRG.LIQ.ACCT) THEN
*            IF CATEG EQ 1201 OR CATEG EQ 1202 THEN
             IF CATEG GE 1101 AND CATEG LE 1599 THEN
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,LG.AC,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
                LIM.REF = FMT(LIM.REF, "R%10")
                LIM  = CU.NO :".":LIM.REF
                TEXT=LIM:'LIM';CALL REM
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVL=R.ITSS.LIMIT<LI.AVAIL.AMT>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,LG.AC,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                TEXT=AVL:'AVL';CALL REM
                TEXT=WORK.BALANCE:'WB';CALL REM
                AMT  = DIFF.AMT + R.NEW(LD.CHRG.AMOUNT)<1,1>+ R.NEW(LD.CHRG.AMOUNT)<1,2>
                TEXT=AMT:'AMT';CALL REM
                ZERO = "0.00"
                BAL  =  WORK.BALANCE + AVL
                TEXT=BAL:'BAL';CALL REM
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,LG.AC,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
                BLOCK =LOCK.AMT<1,LOCK.NO>
                TEXT=BLOCK:'BLK';CALL REM
                BAL.BLOCK = BAL - BLOCK
                NET.BAL = BAL.BLOCK - AMT
                TEXT=NET.BAL:'NET.BAL';CALL REM
                IF NET.BAL LT ZERO THEN

                    TEXT = '���� ������ �� ����' ; CALL STORE.END.ERROR
                END
            END ELSE
********************************
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,LG.AC,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                AMT  = DIFF.AMT + R.NEW(LD.CHRG.AMOUNT)<1,1>+ R.NEW(LD.CHRG.AMOUNT)<1,2>
                ZERO = "0.00"
                BAL  =  WORK.BALANCE
*Line [ 138 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,LG.AC,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LG.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
                BLOCK =LOCK.AMT<1,LOCK.NO>
                BAL.BLOCK = BAL - BLOCK
                NET.BAL = BAL.BLOCK - AMT
                IF NET.BAL LT ZERO THEN
                    TEXT = '���� ������ �� ����' ; CALL STORE.END.ERROR
                END
**********************************

            END

        END ELSE
            TEXT='';CALL REM
        END
    END
    RETURN
END
