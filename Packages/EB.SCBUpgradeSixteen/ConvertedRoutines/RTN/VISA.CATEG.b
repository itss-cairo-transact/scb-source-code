* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE

    SUBROUTINE VISA.CATEG(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*****************************
    FN.AC = 'FBNK.ACCOUNT'
    F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.CARD ='FBNK.CARD.ISSUE'
    F.CARD =''
    CALL OPF(FN.CARD,F.CARD)


********************

    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (1205 1206 1207 1208) AND WORKING.BALANCE GT '0' AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    ENQ.LP   = '' ; OPER.VAL = ''
    KK = 0

*********************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            T.SEL1 = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":KEY.LIST<I>:" AND CANCELLATION.DATE EQ '' AND CARD.CODE IN (101 102 104 105 201 202 204 205 501 502 504 601 602 604 605)"
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

            IF SELECTED1 THEN
                FOR RR = 1 TO SELECTED1
                    KK++
                    CALL F.READ(FN.CARD,KEY.LIST1<RR>,ENQ.LP,F.CARD,CUS.ERR1)
**********************
                    ENQ<2,KK> = '@ID'
                    ENQ<3,KK> = 'EQ'
                    ENQ<4,KK> =  KEY.LIST1<RR>

                NEXT RR
            END
        NEXT I
    END

**********************
    RETURN
END
