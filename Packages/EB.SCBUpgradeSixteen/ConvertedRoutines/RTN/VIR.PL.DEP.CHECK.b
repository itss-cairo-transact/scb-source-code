* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*------------------------------------------------------------------------
* <Rating>-8</Rating>
*-------------------------------------------------------------------------
****NESSREEN AHMED 5/2/2018*********
***EDITED BY MAI SAAD 12/02/2018****
    SUBROUTINE VIR.PL.DEP.CHECK
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
**###PL RANGES
*  PL(60000-69998)
*  PL(56001-56019)
*  PL(52880-52900)
*  PL54036
**###
    COMP = C$ID.COMPANY
    COM.CODE      = COMP[8,2]
    IF COM.CODE[1,1]  EQ '0' THEN
        COM.CODE  = COMP[9,1]
    END ELSE
        COM.CODE  = COMP[8,2]
    END
    ACCT.FLD  = R.NEW(INF.MLT.ACCOUNT.NUMBER)
*Line [ 46 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    INF.COUNT = DCOUNT(ACCT.FLD,@VM)
    FOR MM1 = 1 TO INF.COUNT
        PL.CAT  = R.NEW(INF.MLT.PL.CATEGORY)<1,MM1>
**        TEXT = 'PL = ': PL.CAT ; CALL REM
        IF PL.CAT NE '' THEN
            IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                IF COMP EQ 'EG0010099' THEN
                    IF R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,MM1> = '' THEN
                        E = '��� ����� ��� �������'
                        CALL ERR ; MESSAGE = 'REPEAT'
                        CALL STORE.END.ERROR
                    END
                END ELSE
**               IF R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,MM1> = '' THEN
                    R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,MM1> = '7070'
**               END
                END
            END ELSE
                TEXT = 'PL = ': PL.CAT ; CALL REM
                IF COMP EQ 'EG0010099' THEN
                    IF R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,MM1> = '' THEN
                        R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,MM1> = COM.CODE
                    END
                END ELSE
                    R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,MM1> = COM.CODE
                END
            END
        END
    NEXT MM1
    CALL REBUILD.SCREEN
    RETURN
END
