* @ValidationCode : MjotNDU3MTg3OTk2OkNwMTI1MjoxNjQyMjcyMjY1MjM3OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 20:44:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.CD.NO.AMEND

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS


**********************
    CO.CODE=ID.COMPANY

*RECORD.STAT =  R.NEW(LD.RECORD.STATUS)
*CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
*MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
*REST      = 'Same.Version'

* IF NOT(ETEXT) THEN
*     IF MYVERNAME NE PGM.VERSION THEN
*    E = 'You.Must.Authorize.OR.Delete.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
* END ELSE
    IF V$FUNCTION = 'I' THEN
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
        FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
        CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
        CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
        MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
*        IF NOT(ETEXT) THEN
        IF MYLOCAL NE '' THEN
*Line [ 53 ] initialised REST - ITSS - R21 Upgrade - 2022-01-15
            REST = ''
            E = 'You.Must.Authorize.OR.Delete.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END

*    IF  RECORD.STAT EQ 'INAU' THEN
*       E = 'Cant.Amend.NAU.Record': REST; CALL ERR ;MESSAGE = 'REPEAT'
*  END  ELSE
    R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
* END
*END
*END


RETURN
END
