* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.SCB.WH.TRANS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*--------------------------------------------
*Line [ 31 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.ITEM = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)

    FOR I=1 TO DECOUNT.ITEM
        FN.WH.ITEMS = 'F.SCB.WH.ITEMS' ; F.WH.ITEMS = '' ; R.WH.ITEMS = ''
        CALL OPF( FN.WH.ITEMS,F.WH.ITEMS)
        CALL F.READ( FN.WH.ITEMS,R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>, R.WH.ITEMS, F.WH.ITEMS, ETEXT)

        IF R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE) = 'DR' THEN
            R.WH.ITEMS<SCB.WH.IT.UNITS.BALANCE> = R.WH.ITEMS<SCB.WH.IT.UNITS.BALANCE> - R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I>
            R.WH.ITEMS<SCB.WH.IT.VALUE.BALANCE> = R.WH.ITEMS<SCB.WH.IT.VALUE.BALANCE> - R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,I>
        END

        IF R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE) = 'AD' THEN
            R.WH.ITEMS<SCB.WH.IT.UNITS.BALANCE> = R.WH.ITEMS<SCB.WH.IT.UNITS.BALANCE> + R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I>
            R.WH.ITEMS<SCB.WH.IT.VALUE.BALANCE> = R.WH.ITEMS<SCB.WH.IT.VALUE.BALANCE> + R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,I>
        END

        IF R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE) = 'MOD' THEN
            R.WH.ITEMS<SCB.WH.IT.UNIT.PRICE>    = R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,I>
            R.WH.ITEMS<SCB.WH.IT.VALUE.BALANCE> = R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,I> * R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I>
        END

*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DECOUNT.NO = DCOUNT(R.WH.ITEMS<SCB.WH.IT.TRANSACTION.TYPE>,@VM)
        Z          = DECOUNT.NO +1
        R.WH.ITEMS<SCB.WH.IT.TRANSACTION.TYPE,Z> = R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE)
        R.WH.ITEMS<SCB.WH.IT.NO.OF.UNITS,Z>      = R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I>
        R.WH.ITEMS<SCB.WH.IT.VALUE.DATE,Z>       = R.NEW(SCB.WH.TRANS.VALUE.DATE)<1,I>
        R.WH.ITEMS<SCB.WH.IT.REFRENCE.ID,Z>      = ID.NEW

        CALL F.WRITE(FN.WH.ITEMS,R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>,R.WH.ITEMS)
    NEXT I
    RETURN
END
