* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
** ----- NESSREEN AHMED 16/10/2018 -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.TT.CR.SUEZ.NEXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*    TEXT = 'HI' ; CALL REM
    IF V$FUNCTION # 'R' THEN
      *  CR.ACCT  = R.NEW(TT.TE.ACCOUNT.1)
      *  CR.ACCT.1 = R.NEW(TT.TE.ACCOUNT.1)[1,8]
      *  CR.ACCT.2 = R.NEW(TT.TE.ACCOUNT.1)[11,6]
      *  CR.US.ACCT = CR.ACCT.1:"20":CR.ACCT.2
        CR.CCY   = 'USD'
      *  AMT.FCY  = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.PAID.AMT>
      *  TEXT = 'AMT.P=':AMT.FCY ; CALL REM
*       ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
       * REF.NO  = ID.NEW
      *  TEXT = 'REF=':REF.NO ; CALL REM
*       * COMP = C$ID.COMPANY
       * LINK.DATA<1> = CR.US.ACCT
       * LINK.DATA<2> = CR.CCY
       * LINK.DATA<3> = AMT.FCY
       * LINK.DATA<4> = REF.NO

        INPUT.BUFFER = C.U:' TELLER,SCB.CASH.CR.SUEZ.M2 I ':C.F
**        CALL EB.SET.NEXT.TASK(INPUT.BUFFER)
    END   ;***END OF IF V$FUNCTION # 'R' THEN
***********
    RETURN
END
