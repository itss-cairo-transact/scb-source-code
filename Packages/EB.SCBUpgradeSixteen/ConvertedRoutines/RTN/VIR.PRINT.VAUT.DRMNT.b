* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.PRINT.VAUT.DRMNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DRMNT.FILES
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CA = 'F.SCB.DRMNT.FILES' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    COMP = ID.COMPANY
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,COM.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COM.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    CUST.ID = R.NEW(SDF.CUST.NO)

    WS.DESC.1 = ' ������ ������� '
    WS.DESC.2 = '��� ���� ���� �� ��� ���� ������ ��� : ':COM.NAME
    WS.DESC.3 = '�������� ������� : '

    GOSUB PROCESS

    RETURN
*========================================================================
PROCESS:

    GOSUB PRINT.HEAD

    CUS.ID             = R.NEW(SDF.CUST.NO)
    CUST.NAME          = R.NEW(SDF.CUST.NAME)
    WS.SIGN.COUNT      = R.NEW(SDF.SIGN.COUNT.SYS)
    WS.DRMNT.DATE      = R.NEW(SDF.DRMNT.DATE)
    WS.DRMNT.DATE      = FMT(WS.DRMNT.DATE,"####/##/##")
    WS.NOTES           = R.NEW(SDF.NOTES)
    WS.SIGN.COUNT.FILE = R.NEW(SDF.SIGN.COUNT.FILE)

    XX   = SPACE(132)
    XX2  = SPACE(132)

    XX<1,1>[1,10]   = CUS.ID
    XX<1,1>[15,35]  = CUST.NAME
    XX<1,1>[55,10]  = WS.SIGN.COUNT
    XX2<1,1>[50,35] = WS.NOTES
    XX<1,1>[70,10]  = WS.DRMNT.DATE

    PRINT XX<1,1>
    PRINT XX2<1,1>

    XX = SPACE(132)
    XX2 = SPACE(132)
    XX2<1,1>[87,50] =  '���� ������� ������ : ' :WS.SIGN.COUNT.FILE
    PRINT XX2<1,1>

*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.FILE.DET.COUNT = DCOUNT(R.NEW(SDF.FILE.CONTENT),@VM)
    FOR X = 1 TO WS.FILE.DET.COUNT
        WS.FILE.CONTENT = R.NEW(SDF.FILE.CONTENT)<1,X>
        XX<1,1>[87,35] = WS.FILE.CONTENT

        PRINT XX<1,1>
    NEXT X
    PRINT STR('-',130)

    XX1  = SPACE(132)
    PRINT STR('=',130)
    XX1<1,1>[20,35]  = '��� ��������� �������� : ':WS.FILE.DET.COUNT
    PRINT XX1<1,1>
    PRINT STR('=',130)
    PRINT STR(' ',130)

    XX8 = SPACE(132)
    XX8<1,1>[55,35]  = '����� �������'

    PRINT XX8<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):'VIR.PRINT.VAUT.DRMNT'
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������ ��� ���� ���� ":SPACE(1):T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":WS.DESC.1:WS.DESC.2
    PR.HD :="'L'":WS.DESC.3
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(5):"��� ������":SPACE(25):"���� �������":SPACE(7):"����� ������":SPACE(5):"������� �����"
    PR.HD :="'L'":SPACE(50):"���������"
    PR.HD :="'L'":STR('_',130)

    HEADING PR.HD

    RETURN
*==============================================================
END
