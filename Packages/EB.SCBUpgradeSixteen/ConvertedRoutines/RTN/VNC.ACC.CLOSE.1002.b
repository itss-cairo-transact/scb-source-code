* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VNC.ACC.CLOSE.1002

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF V$FUNCTION = 'I' THEN
        FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
        CALL OPF(FN.AC,F.AC)
        CALL F.READ(FN.AC,ID.NEW,R.AC,F.AC,E1)

        CATEG = R.AC<AC.CATEGORY>
        BAL = R.AC<AC.ONLINE.ACTUAL.BAL>

        IF CATEG NE 1002 THEN
            E = '��� ���� ������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END

        IF BAL NE 0 AND BAL NE '' THEN
            E = '���� ���� �������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END

        RETURN
    END
