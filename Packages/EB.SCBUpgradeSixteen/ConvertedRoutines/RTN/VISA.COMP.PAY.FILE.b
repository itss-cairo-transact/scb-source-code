* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>147</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.COMP.PAY.FILE
**WRITTEN BY NESSREEN AHMED 04/08/2011

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.MNTLY.PAY.AMT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.DAILY.PAYMENT

*A REPORT TO MAKE A COMPARISION BETWEEN "SCB.VISA.MNTLY.PAY.AMT" AND SCB.VISA.DAILY.PAYMENT

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 55 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 56 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2022-01-15
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    RETURN
*===============================================================
CALLDB:

    FN.MNT.PAY = 'F.SCB.VISA.MNTLY.PAY.AMT' ; F.MNT.PAY = '' ; R.MNT.PAY = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.MNT.PAY,F.MNT.PAY)

    FN.DLY.PAY = 'F.SCB.VISA.DAILY.PAYMENT' ; F.DLY.PAY = '' ; R.DLY.PAY = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.DLY.PAY,F.DLY.PAY)

    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY3 = '' ; E3 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    REQ.COMP = "EG0010013"
    T.SEL = "SELECT F.SCB.VISA.DAILY.PAYMENT WITH (TRANS.DATE GE 20110720 AND TRANS.DATE LE 20110721) AND TRANS.TYPE NE SETT AND CARD.BR EQ ": REQ.COMP : " BY CARD.NO "
***************************************************************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    TEXT = '��� �� ����� �������=':SELECTED ; CALL REM
    XX= '' ; ZZ = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            VISA.NO = '' ; DB.ACCT = '' ; CR.ACCT = '' ; DB.AMT = '' ; CUST = '' ; CUST.NAM = ''
            REF.NO  = '' ; DB.ACCT.N = '' ; DB.AMT.N = ''  ; CARD.NO = ''

            KEY.TO.USE = KEY.LIST<I>

            CALL OPF(FN.DLY.PAY,F.DLY.PAY)
            CALL F.READ(FN.DLY.PAY, KEY.TO.USE, R.DLY.PAY, F.DLY.PAY, E2)

            CARD.NO     = R.DLY.PAY<SCB.PYM.CARD.NO>
            REF.NO      = R.DLY.PAY<SCB.PYM.REFER.NO>
            CR.ACCT.N   = R.DLY.PAY<SCB.PYM.CUST.ACCT>
            DB.AMT.N    = R.DLY.PAY<SCB.PYM.AMOUNT>

*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER,CR.ACCT.N, CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACCT.N,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)
            CUST.NAM = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>

            XX<1,ZZ>[1,16] = CARD.NO
            XX<1,ZZ>[20,35]= CUST.NAM
            XX<1,ZZ>[60,16]= REF.NO
            XX<1,ZZ>[80,16]= CR.ACCT.N
            XX<1,ZZ>[100,8]= DB.AMT.N

            PRINT XX<1,ZZ>
            ZZ=ZZ+1

            N.SEL = "SELECT F.SCB.VISA.MNTLY.PAY.AMT WITH YEAR.MONTH EQ ":COMI :" AND COMPANY.CO EQ ": REQ.COMP : " AND VISA.NO EQ " : CARD.NO
            SELECTED.N=""
            ER.MSG.N=""

            CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
            FOR NN = 1 TO SELECTED.N
                KEY.TO.USE.N = KEY.LIST.N<NN>

                CALL OPF(FN.MNT.PAY,F.MNT.PAY)
                CALL F.READ(FN.MNT.PAY, KEY.TO.USE.N, R.MNT.PAY, F.MNT.PAY, E1)

                VISA.NO  = R.MNT.PAY<MPA.VISA.NO>
                DB.ACCT  = R.MNT.PAY<MPA.DB.ACCT.NO>
                CR.ACCT  = R.MNT.PAY<MPA.CR.ACCT.NO>
                DB.AMT   = R.MNT.PAY<MPA.DEBIT.AMT>

                XX<1,ZZ>[60,16]= DB.ACCT
                XX<1,ZZ>[80,16]= CR.ACCT
                XX<1,ZZ>[100,8]= DB.AMT

                PRINT XX<1,ZZ>
                ZZ=ZZ+1
                XX = ''
            NEXT NN
            PRINT STR('-',132)
        NEXT I
    END
*********************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
**   TR.DATY = COMI
**   T.TR.DAY = TR.DATY[7,2]:'/':TR.DATY[5,2]:"/":TR.DATY[1,4]

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):"���� ����� ��� �� ��� ������ ����� � ��� ��� ��������� ������ �� ������"
    PR.HD :="'L'":SPACE(28):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ���������"::SPACE(10):"����� ��������":SPACE(15):"�������� �����":SPACE(15):"������� �������":SPACE(10):"���� ������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(9):STR('_',12):SPACE(14):STR('_',15):SPACE(25):STR('_',12):SPACE(8):STR('_',10)
    HEADING PR.HD
    RETURN
*==============================================================

END
