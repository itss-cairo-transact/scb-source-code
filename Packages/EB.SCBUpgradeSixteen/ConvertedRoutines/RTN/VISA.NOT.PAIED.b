* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
* PROGRAM VISA.NOT.PAIED
    SUBROUTINE VISA.NOT.PAIED
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*----------------------------------------------

    GOSUB INITIALISE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*-------------------------------------------
INITIALISE:

    REPORT.ID='VISA.NOT.PAIED'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.COM = 'F.COMPANY'  ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.CU = 'FBNK.CUSTOMER'  ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    MY.BRAN = ID.COMPANY
    TOT.AMT = 0

    RETURN
*-------------------------------------------
PROCESS:
* DEBUG

    SEQ.FILE.NAME = "&SAVEDLISTS&"
    RECORD.NAME = "VISA.MP.ERR.":TODAY
  *   RECORD.NAME = "VISA.MP.ERR.20120621"
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END

    EOF = ''
    LOOP WHILE NOT(EOF)

        CARD.NO   = ''
        CUST.NO   = ''
        CARD.TYPE = ''
        REJ.DESC  = ''
        BRAN.NO   = ''
        ERR.FOUND = ''
        CARD.NO.1 = ''
        CARD.TYPE.1 = ''

        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            CARD.NO   = FIELD(Y.MSG,"|",1,1)
            CUST.NO.1   = FIELD(Y.MSG,"|",2,1)
            CARD.TYPE.1  = FIELD(Y.MSG,"|",3,1)
            REJ.DESC  = FIELD(Y.MSG,"|",4,2)

            CUST.NO  = FIELD(CUST.NO.1,"=",2)
            CARD.TYPE   = FIELD(CARD.TYPE.1,"=",2)

            CALL F.READ(FN.CU,CUST.NO,R.CU,F.CU,E2)
            BRAN.NO = R.CU<EB.CUS.COMPANY.BOOK>
            CUST.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            CALL F.READ(FN.COM,BRAN.NO,R.COM,F.COM,E4)
            BRAN.NAME = R.COM<EB.COM.COMPANY.NAME,2>

            IF  BRAN.NO EQ MY.BRAN   THEN

                XX = SPACE(132)
                XX<1,1>[1,20]   = CARD.NO
                XX<1,1>[22,35]  = CUST.NAME
                XX<1,1>[63,35]  = CUST.NO
                XX<1,1>[85,25] = REJ.DESC

                PRINT XX<1,1>
                PRINT STR('-',130)
                ERR.FOUND = 1
            END

        END ELSE
            EOF = 1
        END
    REPEAT
    IF ERR.FOUND NE 1 THEN
        XX = SPACE(132)
        XX<1,1>[30,20]   = "������ �������������������������������������������"
    END

    CLOSESEQ SEQ.FILE.POINTER

    RETURN
*-------------------------------------------
PRINT.HEAD:
*CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ID.COMPANY,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,ID.COMPANY,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*    YYBRN = FIELD(BRANCH,'.',2)
    YYBRN = BRANCH

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� �������� ����� �� ��� ������ ��� ���� : ":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������" :SPACE(13):"�����":SPACE(32):"��� ������":SPACE(35):"��� �����"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD

    RETURN
*-------------------------------------------

END
