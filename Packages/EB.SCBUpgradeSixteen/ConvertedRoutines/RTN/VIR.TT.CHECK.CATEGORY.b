* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>96</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VIR.TT.CHECK.CATEGORY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TR.LOCAL.REFS
*********************
***NESSREEN AHMED 19/7/2007************
*************
*    DEBUG
    YY = ''
    E = ''
    ACCT =  R.NEW(TT.TE.ACCOUNT.1)
    TR.CODE = R.NEW(TT.TE.TRANSACTION.CODE)
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('TELLER.TRANSACTION':@FM:TT.TR.LOCAL.REF,TR.CODE,LOCAL.REF)
F.ITSS.TELLER.TRANSACTION = 'FBNK.TELLER.TRANSACTION'
FN.F.ITSS.TELLER.TRANSACTION = ''
CALL OPF(F.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION)
CALL F.READ(F.ITSS.TELLER.TRANSACTION,TR.CODE,R.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION,ERROR.TELLER.TRANSACTION)
LOCAL.REF=R.ITSS.TELLER.TRANSACTION<TT.TR.LOCAL.REF>
    TR.CATEG = LOCAL.REF<1,TRLR.CATEGORY>
***UPDATED BY NESSREEN AHMED 29/6/2014 ****
***    LOCATE CATEG IN TR.CATEG<1,1,1> SETTING YY ELSE E= '��� ����� �� ������ �� ��� ������� ��� �� ��� ������'  ; CALL ERR ; MESSAGE = 'REPEAT'
    LOCATE CATEG IN TR.CATEG<1,1,1> SETTING YY ELSE E= '��� ����� �� ������ �� ��� ������� ��� �� ��� ������'  ; CALL ERR ; MESSAGE = 'REPEAT'; CALL STORE.END.ERROR
***END OF UPDATE 29/6/2014********************
    RETURN
END
