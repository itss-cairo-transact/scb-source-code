* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BR.WRITE.CORRECT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    IF V$FUNCTION = 'I' THEN

        FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = '' ; ETEXT = ''
        CALL OPF( FN.BR,F.BR)

        KEY.LIST=""; SELECTED="" ; ER.MSG=""
        T.SEL ="SELECT FBNK.BILL.REGISTER WITH BIL.CHQ.TYPE NE 10 AND BILL.CHQ.STA EQ 5 AND COLL.DATE EQ ":TODAY

        CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)

        CALL F.READ( FN.BR,KEY.LIST<I>, R.BR, F.BR, ETEXT)

        FOR I =1 TO SELECTED
            R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = KEY.LIST<I>
        NEXT I
    END
    RETURN
END
