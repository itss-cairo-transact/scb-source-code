* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* Create By Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BR.EXP.A22

*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-15
$INSERT I_COMMON
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-15
$INSERT I_EQUATE
*Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-15
$INSERT I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-01-15
$INSERT I_F.USER
    $INSERT           I_F.USR.LOCAL.REF
    $INSERT           I_F.SCB.BR.CUS.CUR
*------------------------------------------
    ETEXT = ''
    E     = ''

    IF V$FUNCTION = 'A' THEN
        STA  = R.NEW(BR.CUS.CUR.RECORD.STATUS)[2,3]
        DEP  = R.USER<EB.USE.LOCAL.REF,USER.SCB.DEPT.CODE>

        IF STA EQ 'NA2' THEN
            IF DEP NE '5100'  THEN
                E = "��� �� ���� ���� ��� "
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END
        END
    END
*--------------------------------------------
    RETURN
END
