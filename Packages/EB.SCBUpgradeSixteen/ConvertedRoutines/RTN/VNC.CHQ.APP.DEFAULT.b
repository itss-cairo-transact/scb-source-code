* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
********AHMED EL MOHANDESS 07/10/2008*******
    SUBROUTINE VNC.CHQ.APP.DEFAULT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    KEY.LIST1 = "" ; SELECTED1 = "" ; ER.MSG1 = ""
    KEY.LIST  = "" ; SELECTED  = "" ; ER.MSG  = ""

    IF V$FUNCTION = 'I'  THEN
        EMP.ID  = FIELD(ID.NEW,".",1)
        NAME.ID = FMT(EMP.ID,'R%8')
        CUSTT   = NAME.ID
        CUST    = TRIM(CUSTT, "0" , "L")

        T.SEL1 = "SELECT F.SCB.CHEQ.APPL$HIS WITH CUST.NO EQ ":CUSTT:" AND @ID UNLIKE SCB... BY DATE.TIME"
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
        SERR2 = FIELD(KEY.LIST1<SELECTED1>,".",2)
        SERR1 = FIELD(SERR2,";",1)

        T.SEL = "SELECT F.SCB.CHEQ.APPL WITH CUST.NO EQ ":CUSTT:" BY DATE.TIME"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            SERR = FIELD(KEY.LIST<SELECTED>,".",2)
            IF SERR1 GT SERR THEN SERR = SERR1

            SER = SERR + 1

            SER.F = FMT(SER, "R%3")
            ID.NEW = CUSTT:".":SER.F
        END ELSE
            ID.NEW = CUSTT:".":"001"
        END

        CALL F.READ(FN.CU,CUST,R.CU,F.CU,E1)

        IF E1 THEN
            E = '��� ���� ������'
*** SCB UPG 20160621 - S
*            CALL ERR ; MESSAGE = 'REPEAT'
*            E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END

        R.NEW(CHQA.APP.DATE) = TODAY
        R.NEW(CHQA.CUST.NO)  = CUST

        FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = ''
        R.CUSTOMER  = ''           ; RETRY      = '' ; E1 = ''

        CALL OPF(FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E1)

        LOCAL.REF     = R.CUSTOMER<EB.CUS.LOCAL.REF>
        ARABIC.NAME   = LOCAL.REF<1,CULR.ARABIC.NAME>
        ARABIC.NAME.2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
***************UPDATED BY RIHAM R15 *****************
*        BRANCH        = R.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
        BRANCH        = R.CUSTOMER<EB.CUS.COMPANY.BOOK>[2]
***********************************************************
        R.NEW(CHQA.BRAN.NO)     = BRANCH
        R.NEW(CHQA.CUST.NAME)   = ARABIC.NAME
        R.NEW(CHQA.CUST.NAME.2) = ARABIC.NAME.2


    END

**    IF V$FUNCTION = 'R' THEN
**        TRN.DAT = R.NEW(CHQA.TRN.DATE)
**        IF TRN.DAT NE '' THEN
**            E = 'YOU CAN NOT REVERSE THIS RECORD'
*** SCB UPG 20160621 - S
*            CALL ERR ; MESSAGE = 'REPEAT'
*            E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
**            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
**        END
**END

    RETURN
END
