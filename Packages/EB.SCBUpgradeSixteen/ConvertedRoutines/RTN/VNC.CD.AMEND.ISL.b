* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*---------------------MAI OCT 2018-----------------------
* <Rating>98</Rating>
*--------------------------------------------------------
    SUBROUTINE VNC.CD.AMEND.ISL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.PREM.AMT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*************ADDED BY MAI***************
    COMP          = C$ID.COMPANY
    COM.CODE      = COMP[8,2]
    IF COM.CODE[1,1]  EQ '0' THEN
        COM.CODE  = COMP[9,1]
    END ELSE
        COM.CODE  = COMP[8,2]
    END
*  IF V$FUNCTION = 'I' THEN
    IF COM.CODE # 11 THEN
        E = '��� �������� ���� ���� ����� ���';
        CALL ERR ; MESSAGE = 'REPEAT'
    END
    IF ID.NEW # '' THEN
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CATEG)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
CATEG=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
* TEXT= "CATEGORY = ": CATEG ; CALL REM;
        IF CATEG # 21036 THEN
            E='This screen is for islamic certificates only'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*    END
*********************************
*TO CHECK  THAT IF CD NOT EXIST OR MATURED OR LIQUIDATED THEN ERROR MESSAGE WILL BE DISPLAYED
*TO DEFAULT VERSION NAME

    IF V$FUNCTION EQ 'I' THEN
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,MYMATR)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYMATR=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.FIN.MAT.DATE>
        IF ETEXT THEN
            E='��� �� ���� ����� ����� �� ���'; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            IF MYMATR LT TODAY THEN
                E='��� ������� ������';CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
*   IF R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=',SCB.CD.LIQ' THEN
*       E='��� ������� ������';CALL ERR;MESSAGE='REPEAT'
*   END ELSE
                R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
*   END
            END
        END

    END
    RETURN
END
