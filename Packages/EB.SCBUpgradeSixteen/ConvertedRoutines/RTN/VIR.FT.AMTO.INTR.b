* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.FT.AMTO.INTR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB NEW
    GOSUB LIVE
    GOSUB NAU
    GOSUB CHECK.VAR
    RETURN
**************************************************************
INITIATE:

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)


    FN.FT   = 'FBNK.FUNDS.TRANSFER'    ; F.FT   = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    FN.FT.NAU   = 'FBNK.FUNDS.TRANSFER$NAU'    ; F.FT.NAU   = '' ; R.FT.NAU  = ''
    CALL OPF(FN.FT.NAU,F.FT.NAU)

    FN.CUR = 'FBNK.CURRENCY'  ; F.CUR ='' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    KEY.LIST= "" ; SELECTED  = "" ; ER.MSG = ""
    KEY.LIST2= "" ; SELECTED2 = "" ; ER.MSG2 = ""

    CUS    = R.NEW(FT.DEBIT.CUSTOMER)
    CUR    = R.NEW(FT.DEBIT.CURRENCY)
    RETURN
********************************************************************
NEW:
    CUS    = R.NEW(FT.DEBIT.CUSTOMER)
    CUR    = R.NEW(FT.DEBIT.CURRENCY)

*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    NEW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>

    IF V$FUNCTION EQ 'I' THEN
        IF NEW.SECTOR EQ '4650' THEN
            IF CUR EQ 'EGP' THEN
                AMT.NEW.EGP.4650 = R.NEW(FT.DEBIT.AMOUNT)
                IF AMT.NEW.EGP.4650 GT '350000' THEN
                    ETEXT = ',RESPONS.CODE=D17,' ; CALL STORE.END.ERROR
                END
            END ELSE
                AMT.NEW.USD.4650 = R.NEW(FT.DEBIT.AMOUNT)
                CUR = R.NEW(FT.DEBIT.CURRENCY)
                CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ERR1)
                RATE                   = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                AMT.NEW.USD.4650       = AMT.NEW.USD.4650 * RATE
                IF AMT.NEW.USD.4650 GT '350000' THEN
                    ETEXT = ',RESPONS.CODE=D17,' ; CALL STORE.END.ERROR
                END
            END
        END ELSE
            IF CUR EQ 'EGP' THEN
                AMT.NEW.EGP = R.NEW(FT.DEBIT.AMOUNT)
                IF AMT.NEW.EGP GT '350000' THEN
                    ETEXT = ',RESPONS.CODE=D17,' ; CALL STORE.END.ERROR
                END
            END ELSE
                AMT.NEW.USD = R.NEW(FT.DEBIT.AMOUNT)
                CUR = R.NEW(FT.DEBIT.CURRENCY)
                CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ERR1)
                RATE                = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                AMT.NEW.USD         = AMT.NEW.USD * RATE

                IF AMT.NEW.USD GT '350000' THEN
                    ETEXT = ',RESPONS.CODE=D17,' ; CALL STORE.END.ERROR
                END
            END
        END
    END
    RETURN
**************************LIVE ONLY *******************************
LIVE:
****
    CUS = R.NEW(FT.DEBIT.CUSTOMER)
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'OT06' AND INPUTTER LIKE ...EIB123... AND DEBIT.CUSTOMER NE CREDIT.CUSTOMER AND DEBIT.CUSTOMER EQ ":CUS
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
*TEXT = SELECTED ;CALL REM
            CALL F.READ(FN.FT,KEY.LIST<Z>,R.FT,F.FT,E1)
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            NEW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
            IF NEW.SECTOR EQ '4650' THEN
                IF R.FT<FT.DEBIT.CURRENCY>  EQ 'EGP' THEN
                    AMT.LIVE.EGP.4650 += R.FT<FT.DEBIT.AMOUNT>
                END ELSE
                    AMT.LIVE.USD.4650 = R.FT<FT.DEBIT.AMOUNT>
                    Y.CUR = R.FT<FT.DEBIT.CURRENCY>
                    CALL F.READ(FN.CUR,Y.CUR,R.CUR,F.CUR,ERR1)
                    RATE               = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    AMT.LIVE.USD.4650.EQ  = AMT.LIVE.USD.4650 * RATE
                    AMT.LIVE.USD.4650.EQ.TOT  += AMT.LIVE.USD.4650.EQ
                END
            END ELSE
                IF R.FT<FT.DEBIT.CURRENCY> EQ 'EGP' THEN
                    AMT.LIVE.EGP += R.FT<FT.DEBIT.AMOUNT>
                END ELSE
                    Y.CUR     = R.FT<FT.DEBIT.CURRENCY>
                    AMT.LIVE.USD  = R.FT<FT.DEBIT.AMOUNT>
                    CALL F.READ(FN.CUR,Y.CUR,R.CUR,F.CUR,ERR1)
                    RATE               = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    AMT.LIVE.USD.EQ       = AMT.LIVE.USD * RATE
                    AMT.LIVE.USD.EQ.TOT  += AMT.LIVE.USD.EQ
                END
            END
        NEXT Z
    END
    RETURN

**************************NAU ONLY *******************************
NAU:
******
    CUS = R.NEW(FT.DEBIT.CUSTOMER)
    T.SEL2 = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH TRANSACTION.TYPE EQ 'OT06' AND INPUTTER LIKE ...EIB123... AND DEBIT.CUSTOMER NE CREDIT.CUSTOMER AND DEBIT.CUSTOMER EQ ":CUS
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR Z = 1 TO SELECTED2
*TEXT = SELECTED2 ;CALL REM
            CALL F.READ(FN.FT.NAU,KEY.LIST2<Z>,R.FT.NAU,F.FT.NAU,E1)
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            NEW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
            IF NEW.SECTOR EQ '4650' THEN
                IF R.FT.NAU<FT.DEBIT.CURRENCY> EQ 'EGP' THEN
                    AMT.NAU.EGP.4650 += R.FT.NAU<FT.DEBIT.AMOUNT>
                END ELSE
                    AMT.NAU.USD.4650 = R.FT.NAU<FT.DEBIT.AMOUNT>
*      TEXT = AMT.NAU.USD.4650 ;CALL REM
                    Y.CUR = R.FT.NAU<FT.DEBIT.CURRENCY>

                    CALL F.READ(FN.CUR,Y.CUR,R.CUR,F.CUR,ERR1)
                    RATE                = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    AMT.NAU.USD.4650.EQ   = AMT.NAU.USD.4650 * RATE
                    AMT.NAU.USD.4650.EQ.TOT += AMT.NAU.USD.4650.EQ
                END
            END ELSE
                IF R.FT.NAU<FT.DEBIT.CURRENCY> EQ 'EGP' THEN
                    AMT.NAU.EGP += R.FT.NAU<FT.DEBIT.AMOUNT>
                END ELSE
                    Y.CUR     = R.FT.NAU<FT.DEBIT.CURRENCY>
                    AMT.NAU.USD  = R.FT.NAU<FT.DEBIT.AMOUNT>
                    CALL F.READ(FN.CUR,Y.CUR,R.CUR,F.CUR,ERR1)
                    RATE          = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    AMT.NAU.USD.EQ   = AMT.NAU.USD * RATE
                    AMT.NAU.USD.EQ.TOT += AMT.NAU.USD.EQ
                END
            END
        NEXT Z
    END
    RETURN
**************************************************************************8
CHECK.VAR:
    IF V$FUNCTION EQ 'I' THEN
        TOT.EGP.4650 = AMT.NEW.EGP.4650 + AMT.LIVE.EGP.4650 + AMT.NAU.EGP.4650
        TOT.USD.4650 = AMT.NEW.USD.4650 + AMT.LIVE.USD.4650.EQ.TOT + AMT.NAU.USD.4650.EQ.TOT
        TOT.4650 = TOT.EGP.4650 + TOT.USD.4650
*TEXT = TOT.4650 ;CALL REM
        TOT.EGP = AMT.NEW.EGP + AMT.LIVE.EGP + AMT.NAU.EGP
        TOT.USD = AMT.NEW.USD + AMT.LIVE.USD.EQ.TOT + AMT.NAU.USD.EQ.TOT
        TOT = TOT.EGP + TOT.USD

        IF TOT.4650 GT '350000' THEN
            ETEXT = ',RESPONS.CODE=D16,' ; CALL STORE.END.ERROR
        END

        IF TOT GT '350000' THEN
            ETEXT = ',RESPONS.CODE=D16,' ; CALL STORE.END.ERROR
        END
    END
    RETURN
**********************************************************************
END
