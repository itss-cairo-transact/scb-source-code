* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SF.OVER

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)


    DEBIT.ACCT    = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
    COMM.ACCT     = R.NEW(SCB.SF.TR.ACCT.CHRG.COMM)
    MARG.VALUE    = R.NEW(SCB.SF.TR.MARGIN.VALUE)
    COMM.AMT      = R.NEW(SCB.SF.TR.COMMISSION.AMT)
    COMM.AMT2     = R.NEW(SCB.SF.TR.COMMISSION.AMT2)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DEBIT.ACCT,CAT.DB)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMM.ACCT,CAT.COMM)


    IF DEBIT.ACCT EQ COMM.ACCT THEN
        IF (CAT.DB EQ 6501 OR CAT.DB EQ 6502 OR CAT.DB EQ 6503 OR CAT.DB EQ 6504 OR CAT.DB EQ 6511) THEN
          *  TOTAL1 = MARG.VALUE + COMM.AMT + COMM.AMT2
             TOTAL1 = MARG.VALUE + COMM.AMT2
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,DEBIT.ACCT,BAL)


            TOTALL  = BAL - TOTAL1


            IF TOTALL LT 0 THEN
                E = '�� ���� ��� ������ ������ �� ���� '
                CALL ERR;MESSAGE='REPEAT'
                CALL STORE.END.ERROR

            END
        END
    END
    ELSE
        TOTAL2 = MARG.VALUE
    *    TOTAL3 = COMM.AMT + COMM.AMT2
         TOTAL3 = COMM.AMT2
        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,DEBIT.ACCT,BAL.DB)
        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,COMM.ACCT,BAL.COMM)

        TOTAL.DB    = BAL.DB - TOTAL2
        TOTAL.COMM  = BAL.COMM - TOTAL3

        IF TOTAL.DB LT 0 AND (CAT.DB EQ 6501 OR CAT.DB EQ 6502 OR CAT.DB EQ 6503 OR CAT.DB EQ 6504 OR CAT.DB EQ 6511) THEN
            E = '�� ���� ��� �������� ������ �� ����'
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR

        END
        IF TOTAL.COMM LT 0 AND (CAT.COMM EQ 6501 OR CAT.COMM EQ 6502 OR CAT.COMM EQ 6503 OR CAT.COMM EQ 6504 OR CAT.COMM EQ 6511) THEN
            E = '�� ���� ��� ������� ������ �� ����'
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR

        END

    END
***************************************************************************

    RETURN
END
