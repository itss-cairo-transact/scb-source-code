* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>544</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 29/05/2003***********

SUBROUTINE VIR.LC.COM.AMEND(CUSTOMER,DEAL.AMOUNT,DEAL.CURRENCY,CURRENCY.MARKET,CROSS.RATE,CROSS.CURRENCY,DRAWDOWN.CCY,T.DATA,CUST.COND,CHARGE.AMOUNT)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

  GOSUB INITI
   IF EXP.DATE #  OLD.EXP AND( AMT = OLD.AMT OR OLD.AMT = '' ) THEN GOSUB EXPIRY.D
    ELSE IF  OLD.AMT # AMT AND (EXP.DATE = OLD.EXP OR OLD.EXP = '')  THEN GOSUB AMOUNT.CH
     ELSE IF OLD.AMT # AMT AND OLD.EXP # EXP.DATE THEN GOSUB AM.D.CH

GOTO END.RTN

**********************************************************************************************
INITI:
   AMT = R.NEW(TF.LC.LC.AMOUNT)
   ISS.DATE = R.NEW(TF.LC.ISSUE.DATE)
   OLD.EXP =  R.OLD(TF.LC.ADVICE.EXPIRY.DATE)
   AMD.DATE = TODAY
   OLD.AMT = R.OLD(TF.LC.LC.AMOUNT)
   EXP.DATE = R.NEW(TF.LC.ADVICE.EXPIRY.DATE)
   RETURN
**********************************************************************************************
EXPIRY.D:

            CALL CDD("C",ISS.DATE,OLD.EXP,PER1)
            PER1  = PER1/90
            CALL CDD("C",ISS.DATE,EXP.DATE,PER2)
            PER2 = PER2/90
            INTG1 = INT(PER1)
            REMN1 = SSUB(PER1,INTG1)
            INTG2 = INT(PER2)
            REMN2 = SSUB(PER2,INTG2)
             IF REMN1 # '0' THEN
               PER1 = INTG1 + 1
             END
             IF REMN2 # '0' THEN
                PER2 = INTG2 + 1
             END
            PER.DIF = SSUB(PER1,PER2)
            PER.DIF = ABS(PER.DIF)
          IF PER.DIF = 0 THEN
            COM.AMT1 = (0.2 * AMT)/100

           CHARGE.AMOUNT =  COM.AMT1
         END ELSE
          COM.AMT1 =(PER.DIF * AMT * 1.25)/100 
           CHARGE.AMOUNT =  COM.AMT1
  END
RETURN
**************************************************************************************************************************************************************
 AMOUNT.CH:
     COM.AMT1 = ''
     PER1 = ''
     REMN1 = ''
     INTG1 = ''
     CALL CDD("C",AMD.DATE,EXP.DATE,PER1)
            PER1 = PER1/90
            INTG1 = INT(PER1)
            REMN1 = SSUB(PER1,INTG1)
             IF REMN1 # '0' THEN
               PER1 = INTG1 + 1
              END
              DIFF.AMT = SSUB(AMT,OLD.AMT)
              DIFF.AMT = ABS(DIFF.AMT)
              COM.AMT1 = (DIFF.AMT * PER1 * 1.25)/100
              CHARGE.AMOUNT =  COM.AMT1
 *END

RETURN
************************************************************************************************************************************************************
AM.D.CH:


 COM.AMT2 = '' ; COM.AMT1 = '' ; COM.AMT = '' ; PER1 = '' ; REMN1 = ''
 INTG1 = '' ; PER1 = '' ; REMN1 = '' ; INTG1 = '' ; DIFF.AMT = ''; PER.DIF = ''

      CALL CDD("C",ISS.DATE,OLD.EXP,PER1)
      CALL CDD("C",AMD.DATE,EXP.DATE,PER2)
      CALL CDD("C",ISS.DATE,EXP.DATE,PER3)

              PER1 = PER1/90
              INTG1 = INT(PER1)
              REMN1 = SSUB(PER1,INTG1)
              PER2 = PER2/90
              INTG2 = INT(PER2)
              REMN2 = SSUB(PER2,INTG2)
              PER3 = PER3/90
              INTG3 = INT(PER3)
              REMN3 = SSUB(PER3,INTG3)

              IF REMN1 # '0' THEN
                PER1 = INTG1 + 1
               END
               IF REMN2 # '0' THEN
                 PER2 = INTG2 + 1
               END
              IF REMN3 # '0' THEN
                  PER3 = INTG3 + 1
                END

             PER.DIF = SSUB(PER1,PER3)
             PER.DIF = ABS(PER.DIF)
           IF PER.DIF = 0 THEN
              COM.AMT1 = ( OLD.AMT * 0.2 )/100
              DIFF.AMT = SSUB(AMT,OLD.AMT)
              DIFF.AMT = ABS(DIFF.AMT)
             COM.AMT2 = (DIFF.AMT * PER2 * 1.25)/100
             COM.AMT =ADDS(COM.AMT1 , COM.AMT2)
             CHARGE.AMOUNT =  COM.AMT
           END ELSE
             IF PER.DIF GT 0 THEN
             DIFF.AMT = SSUB(AMT,OLD.AMT)
              COM.AMT1 = (DIFF.AMT * 1.25 * PER2)/100
              COM.AMT2 = (OLD.AMT * PER.DIF * 1.25)/100
              COM.AMT = ADDS(COM.AMT1 , COM.AMT2)
              CHARGE.AMOUNT =  COM.AMT
               END
            END


RETURN

*****************************************************************************************************************************************************************************

END.RTN:

RETURN
END
