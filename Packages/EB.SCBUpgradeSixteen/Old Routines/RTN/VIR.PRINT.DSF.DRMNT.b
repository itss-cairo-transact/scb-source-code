* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.PRINT.DSF.DRMNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DRMNT.FILES
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VIR.PRINT.DSF.DRMNT'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CA = 'F.SCB.DRMNT.FILES' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = ""
    CALL OPF(FN.CU,F.CU)

    CUST.ID = R.NEW(SDF.CUST.NO)
    CALL F.READ(FN.CU,CUST.ID,R.CU,F.CU,ERR.CU)
    WS.ADDRESS = ''
    GOSUB PROCESS

    RETURN
*========================================================================
PROCESS:

    GOSUB PRINT.HEAD

    CUS.ID         = R.NEW(SDF.CUST.NO)
    CUST.NAME      = R.NEW(SDF.CUST.NAME)
    WS.SIGN.COUNT  = R.NEW(SDF.SIGN.COUNT.FILE)
    WS.DRMNT.DATE  = R.NEW(SDF.DRMNT.DATE)
    WS.DRMNT.DATE  = FMT(WS.DRMNT.DATE,"####/##/##")
    WS.NOTES       = R.NEW(SDF.NOTES)
    WS.BRANCH      = R.NEW(SDF.BRANCH.NO)
    BRANCH = ""
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.BRANCH,BRANCH)
    ADDRESS1 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,1>
    ADDRESS2 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,2>
    ADDRESS3 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,3>
    WS.ADDRESS = ADDRESS1:'':ADDRESS2:'':ADDRESS3

    XX   = SPACE(132)
    XX1  = SPACE(132)
    XX2  = SPACE(132)
    XX3  = SPACE(132)
    XX4  = SPACE(132)

    XX<1,1>[1,132]   = CUS.ID
    PRINT XX<1,1>

*    XX<1,1> = ""
    XX1<1,1>[1,132]  = CUST.NAME
    PRINT XX1<1,1>

*    XX2<1,1>[1,132]  = WS.DRMNT.DATE
*    PRINT XX2<1,1>

    XX3<1,1>[1,132]  = WS.ADDRESS
    PRINT XX3<1,1>

    XX4<1,1>[1,132]  = BRANCH
    PRINT XX4<1,1>


    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")

*    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"
*    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"

    PR.HD  = "'L'"
    PR.HD := "'L'"
    PR.HD :="'L'":SPACE(1):'VIR.PRINT.DSF.DRMNT'
    PR.HD :="'L'":T.DAY
    PR.HD :="'L'"
    PR.HD :="'L'"
    HEADING PR.HD

    RETURN
*==============================================================
END
