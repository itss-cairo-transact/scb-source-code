* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 22/04/2003***********

SUBROUTINE VIR.LC.UPDATE.INCOLL
* A ROUTINE TO MAKE SURE THAT THE ID OF LCLR.ANNEX11 IS NEW

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.ANNEX.INDEX
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

ANN =  R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ANNEX11> 
TEXT = 'ANN=':ANN ; CALL REM
FN.SCB.LC.ANNEX.INDEX = 'F.SCB.LC.ANNEX.INDEX' ; F.SCB.LC.ANNEX.INDEX = '' ; R.SCB.LC.ANNEX.INDEX = ''
IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.ANNEX11> THEN
  CALL OPF( FN.SCB.LC.ANNEX.INDEX,F.SCB.LC.ANNEX.INDEX)
  CALL F.READ(FN.SCB.LC.ANNEX.INDEX,ANN,R.SCB.LC.ANNEX.INDEX,F.SCB.LC.ANNEX.INDEX, ETEXT)
  IF ETEXT THEN
  **NN**
    ETEXT = ''
    TEXT = 'NONEXIST' ; CALL REM
  R.SCB.LC.ANNEX.INDEX<SCB.LC.ANDX.LC.ID> = ID.NEW
   CALL F.WRITE(FN.SCB.LC.ANNEX.INDEX,ANN,R.SCB.LC.ANNEX.INDEX)
     *  CALL JOURNAL.UPDATE(ANN)
 END ELSE ETEXT = 'Annex.Must.Be.New'
 END
 IF V$FUNCTION = 'D' AND ANN THEN
 CALL F.DELETE(FN.SCB.LC.ANNEX.INDEX,ANN)
 END

RETURN
END
