* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.MLAD.LOAN.CHK.CATEG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.LOANS.PARM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.LOANS.PARM.CONT
*-----------------------------------------------------
    ETEXT = ''

*Line [ 40 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    II = DCOUNT(R.NEW(MLAD.LOANS.P.CATEGORY.FROM),@VM)
    JJ = II
    FOR I = 1 TO II
        CATEG.FROM = R.NEW(MLAD.LOANS.P.CATEGORY.FROM)<1,I>
        CATEG.TO   = R.NEW(MLAD.LOANS.P.CATEGORY.TO)<1,I>
        FOR J = 1 TO JJ
            IF I NE J THEN
                CAT.F = R.NEW(MLAD.LOANS.P.CATEGORY.FROM)<1,J>
                CAT.T = R.NEW(MLAD.LOANS.P.CATEGORY.TO)<1,J>
                IF ( CATEG.FROM GE CAT.F ) AND ( CATEG.FROM LE CAT.T ) THEN
                    TEXT = 'Duplicate.category' ; CALL REM
                    ETEXT = 'Duplicate.category'
                    CALL ERR ; MESSAGE = 'REPEAT'
                    CALL STORE.END.ERROR
                    RETURN
                END

                IF ( CATEG.TO GE CAT.F ) AND ( CATEG.TO LE CAT.T ) THEN
                    TEXT = 'Duplicate.category' ; CALL REM
                    ETEXT = 'Duplicate.category'
                    CALL STORE.END.ERROR
                    CALL ERR ; MESSAGE = 'REPEAT'
                    RETURN
                END
            END
        NEXT J
    NEXT I
*-----------------------------------------------------
    RETURN
END
