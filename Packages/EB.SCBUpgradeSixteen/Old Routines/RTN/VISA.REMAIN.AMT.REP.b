* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*********NESSREEN AHMED 12/12/2005
*-----------------------------------------------------------------------------
* <Rating>-97</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.REMAIN.AMT.REP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.REMAIN.AMT

*TO PRINT A REPORT CONTAING ALL CUSTOMERS THAT THEIR CUURENT ACCOUNT DIDN'T ALLOW TO DEBIT ALL THE REQUIRED AMOUNT


    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 52 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.REMAIN.AMT.REP'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    F.VISA.REMAIN = '' ; FN.VISA.REMAIN = 'F.SCB.VISA.REMAIN.AMT' ; R.VISA.REMAIN = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.VISA.REMAIN,F.VISA.REMAIN)

*****************************************************************************************************
    T.SEL = "SELECT F.SCB.VISA.REMAIN.AMT"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*===============================================================================================
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            DB.ACCT = '' ; W.B = '' ; DB.AMT = '' ; MIN.AMT = '' ; EMB.NAME = '' ; REMAIN.AMT = ''
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,KEY.LIST<I>,CUST)
*CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST,DEP.CODE)
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST,COMP.BOOK)
            DEP.CODE = COMP.BOOK[8,2]

*  IF DEP.CODE # TEMP.CODE THEN
*      TEMP.CODE = DEP.CODE
*      CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEP.CODE,DEP.CODEE)
*      DEP.CODEE=FIELD(DEP.CODEE,'.',2)
*      XX<1,ZZ>="_________________":"   ���    ": DEP.CODEE:"   _____________________"
*      PRINT XX<1,ZZ>
*      ZZ=ZZ+1
*  END
*****************************************************************************************************
            TEXT = 'KEY.LIST<I>=':KEY.LIST<I> ; CALL REM
            CALL F.READU(FN.VISA.REMAIN,KEY.LIST<I>,R.VISA.REMAIN,F.VISA.REMAIN,E1,RETRY1)
            DB.ACCT = KEY.LIST<I>
            EMB.NAME = R.VISA.REMAIN<SCB.REMAIN.EMB.NAME>
            MIN.AMT = R.VISA.REMAIN<SCB.REMAIN.MIN.DUE.AMT>
            W.B = R.VISA.REMAIN<SCB.REMAIN.WORKING.BAL>
            REMAIN.AMT = R.VISA.REMAIN<SCB.REMAIN.REMAIN.AMT>

            XX<1,1>[1,30]= EMB.NAME
            XX<1,1>[50,16]= DB.ACCT
            XX<1,1>[90,16]= MIN.AMT
            XX<1,1>[115,15]= W.B
            XX<1,1>[150,15] = REMAIN.AMT
            PRINT XX
            XX = ''

            ZZ=ZZ+1
        NEXT I
    END
    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ������� ����� �� ���� ������ ������� ������"
    PR.HD :="'L'":SPACE(38):STR('_',60)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"����� ��������":SPACE(35):"���� ���� ������":SPACE(15):"������ ������ �����":SPACE(12):"������ ������":SPACE(10):"������ ������� ����"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(33):STR('_',18):SPACE(13):STR('_',20):SPACE(10):STR('_',17):SPACE(08):STR('_',18)
    HEADING PR.HD
    RETURN
*==============================================================
    RETURN
END
