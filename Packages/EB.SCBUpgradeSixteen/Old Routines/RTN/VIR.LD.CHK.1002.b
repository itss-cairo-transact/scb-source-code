* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.LD.CHK.1002

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------

    WS.PRN.ACCT  = R.NEW(LD.PRIN.LIQ.ACCT)
    WS.INT.ACCT  = R.NEW(LD.INT.LIQ.ACCT)
    WS.CHRG.ACCT = R.NEW(LD.CHRG.LIQ.ACCT)
    WS.FEE.ACCT  = R.NEW(LD.FEE.PAY.ACCOUNT)

    WS.PRN.CATEG  = WS.PRN.ACCT[11,4]
    WS.INT.CATEG  = WS.INT.ACCT[11,4]
    WS.CHRG.CATEG = WS.CHRG.ACCT[11,4]
    WS.FEE.CATEG  = WS.FEE.ACCT[11,4]

    IF WS.PRN.CATEG EQ '1002' OR WS.INT.CATEG EQ '1002' OR WS.CHRG.CATEG EQ '1002' OR WS.FEE.CATEG EQ '1002' THEN
        ETEXT = '�� ���� ������� ��������'
        CALL STORE.END.ERROR
    END

    RETURN
END
