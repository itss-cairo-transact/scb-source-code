* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>102</Rating>
*-----------------------------------------------------------------------------
*------EDIT BY MAHMOUD MAGDY (COPIED)
    SUBROUTINE VNC.CHQ.PRES

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE T24.BP I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE TEMENOS.BP I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
* TO CHECK THAT THE CHEQUE IS ISSUED BEFORE THAT
* TO CHECK THAT THE CHEQUE IS NOT STOPPED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* TO CHECK THAT THE CHEQUE IS NOT CANCELLED

****************MAIN PROGRAM************************

    GOSUB INTIAL
    RECORD.STATUS = R.NEW(FT.RECORD.STATUS)[1,3]

    IF V$FUNCTION EQ 'A' THEN 
        IF ( RECORD.STATUS EQ 'INA' OR RECORD.STATUS EQ 'CNA' ) THEN 
            CHQ.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> 
            GOSUB STOP.CHQ 
            IF CURR = '' THEN
                GOSUB DRAFT.CHQ.ISSUE
            END 
        END
    END

    GOTO END.PROG
*************************************************************************************************
INTIAL:
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT ='' ; CURR = ''
    RECORD.STATUS = ''

    RETURN
*********************************************************************************************************
STOP.CHQ:

    CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(FT.CREDIT.ACCT.NO),CHQ.TYPE)

    TY = CHQ.TYPE
*Line [ 76 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT (TY,@VM)
    FOR X = 1 TO DD
        CHQ.ID = CHQ.TYPE<1,X>:'.':R.NEW(FT.CREDIT.ACCT.NO):'.':CHQ.NO 
        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR) 
        IF CURR THEN
            E = "����� ��� (&) ����� ":@FM:CHQ.NO;CALL STORE.END.ERROR 
        END 
    NEXT X
    RETURN
***************************************************************************
DRAFT.CHQ.ISSUE:
    ERR.MSG = ''
    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT) 
    SCB.CHQ.ID = R.NEW(FT.CREDIT.ACCT.NO):".": CHQ.NO   
    CALL F.READ(FN.CHQ.PRESENT,SCB.CHQ.ID,R.CHQ.PRESENT,F.CHQ.PRESENT,ERR.MSG)

    IF ERR.MSG THEN

    END
    ELSE
        CHQ.STAT = R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS> 
        CHQ.TRNS.PAY = R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT> 
        CHQ.PAY.DATE = R.CHQ.PRESENT<DR.CHQ.PAY.DATE> 
        CHQ.PAY.BRN = R.CHQ.PRESENT<DR.CHQ.PAY.BRN> 
        CHQ.AMT = R.CHQ.PRESENT<DR.CHQ.AMOUNT> 
        CHQ.REC.DATE = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE> 
        CHQ.BEN = R.CHQ.PRESENT<DR.CHQ.BEN> 
        IF CHQ.STAT = 1 THEN
            E = '����� ��� ������'  ; CALL STORE.END.ERROR 
        END
        ELSE
            IF CHQ.STAT = 2 AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
                E = "����� ��� ����" ; CALL STORE.END.ERROR 
            END
            ELSE
                IF CHQ.STAT = 3  THEN
                    E = "����� ����"   ; CALL STORE.END.ERROR 
                END
            END
        END
        RETURN
***************************************************************************
END.PROG:

        RETURN
    END


END
