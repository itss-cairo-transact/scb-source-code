* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1235</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.NEW.PROV.ACCT.DP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.LCC = 'FBNK.LETTER.OF.CREDIT' ; F.LCC  = ''  ; R.LCC = '';ID.LC    = ID.NEW[1,12]
    CALL OPF(FN.LCC,F.LCC)
    CALL F.READ(FN.LCC,ID.LC,R.LCC,F.LCC,READ.ERR)
    LC.AC.DR    = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC>

    LOC.REF = R.LCC<TF.LC.LOCAL.REF>
    TEMP.ACC= LOC.REF<1,LCLR.TEMP.PROV.ACC>
    CR.ACCT  = COMI
             TEXT=COMI:'CR.ACCT-COMI';CALL REM
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CR.ACCT,CATEG.CR)
    DRW.TYPE = R.NEW(TF.DR.DRAWING.TYPE)

    LC.ACCT= LC.REC(TF.LC.CREDIT.PROVIS.ACC)
TEXT=LC.ACCT:'LC REC ACCOUNT GLOBUS';CALL REM

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,LC.ACCT,CATEG.LC)
    DR.PER = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
    DR.AMT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT>
******************************
    IF COMI  NE '' THEN
*******************
        IF DR.PER EQ '0' THEN
            ETEXT='NOT ALLOWED TO ENTER ACCOUNT';CALL STORE.END.ERROR
        END ELSE
            IF DR.PER GT '0' OR DR.PER EQ '' OR DR.AMT GT '0' OR DR.AMT EQ '' THEN
                IF R.LCC<TF.LC.PROVISION> EQ 'Y' THEN
*    IF CATEG.LC NE '3013' THEN
                    IF CATEG.CR NE '3013' THEN
                        ETEXT = 'MUST BE 3013';CALL STORE.END.ERROR
                    END ELSE
**************************** MODIFIED ON 10/5/2012
                        IF CATEG.CR EQ '3013' THEN
                            PROV.AMT = R.LCC<TF.LC.PRO.OUT.AMOUNT> - R.LCC<TF.LC.PRO.AWAIT.REL>
                      R.NEW(TF.DR.LOCAL.REF)<1,DRLR.PROV.AMT.FULLY> = PROV.AMT
TEXT=R.NEW(TF.DR.LOCAL.REF)<1,DRLR.PROV.AMT.FULLY>:'FU';CALL REM
                        END
***********************10/5/2012

                    END
*   END
                END
            END
        END
***********************
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,LC.AC.DR,CUR.LC.DR)

        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,LC.ACCT,CUR.LC)
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CUR.NEW)

        IF LC.AC.DR NE '' THEN
            IF CUR.LC.DR NE CUR.LC THEN
                ETEXT = 'ACCOUNTS MUST BE THE SAME1';CALL STORE.END.ERROR
            END
        END
        IF CUR.NEW NE CUR.LC THEN
            ETEXT = 'ACCOUNTS MUST BE THE SAME2' ;CALL STORE.END.ERROR
        END
        IF LC.AC.DR NE '' THEN
            IF CUR.NEW NE CUR.LC.DR THEN
                ETEXT = 'ACCOUNTS MUST BE THE SAME3';CALL STORE.END.ERROR
            END
        END
**************
**ADDED
**ASK HYTHAM
**    TEMP.ACC = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.TEMP.PROV.ACC>
        IF TEMP.ACC NE '' THEN

            R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC> = TEMP.ACC
        END ELSE
** IF (COMI AND LC.AC.DR EQ '' )
            IF COMI NE '' THEN
                IF LC.AC.DR EQ ''  THEN
                    R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC>  = R.LCC<TF.LC.CREDIT.PROVIS.ACC>
                    TEXT=R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC>:'LC';CALL REM
                END
            END
        END
        LC.AC.DR    = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC>
**********************
    END ELSE
        IF R.LCC<TF.LC.PROVISION> EQ 'Y' THEN
            IF DR.PER EQ '' AND DR.AMT EQ '' THEN
                ETEXT='MUST ENTER ACCOUNT WITH CATEGORY 3013 PER';CALL STORE.END.ERROR
            END
            IF DR.PER NE '' AND DR.AMT EQ '' THEN
                IF DR.PER NE '0' THEN
                    ETEXT='MUST ENTER ACCOUNT WITH CATEGORY 3013 PER';CALL STORE.END.ERROR
                END
            END
            IF DR.PER EQ '' AND DR.AMT NE '' THEN
                IF DR.PER NE '0' THEN
                    ETEXT='MUST ENTER ACCOUNT WITH CATEGORY 3013 PER';CALL STORE.END.ERROR
                END
            END
        END
    END
************************
    RETURN
END
