* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
** ----- MOHAMED SABRY 17/05/2009-----**
    SUBROUTINE VIR.RISK.CLASS.P
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.CLASS.CODE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    GOSUB INITIATE

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.RISK.CLASS.P'
    CALL PRINTER.ON(REPORT.ID,'')
*    RETURN

************************************************************************
    ID.ITEM01 = '010000'
    ID.ITEM02 = '020000'
    ID.ITEM03 = '030000'
    ID.ITEM04 = '040000'
    ID.ITEM05 = '050000'
    ID.ITEM06 = '060000'
    ID.ITEM07 = '070000'
    ID.ITEM08 = '080000'
    ID.ITEM09 = '090000'
    ID.ITEM10 = '100000'
    WS.CLASS.NAME01 = ''
    WS.CLASS.NAME02 = ''
    WS.CLASS.NAME03 = ''
    WS.CLASS.NAME04 = ''
    WS.CLASS.NAME05 = ''
    WS.CLASS.NAME06 = ''
    WS.CLASS.NAME07 = ''
    WS.CLASS.NAME08 = ''
    WS.CLASS.NAME09 = ''
    WS.CLASS.NAME10 = ''
* R.NEW(RM.TOT.CUS)=WS.TOTAL.ALL
*************************************************************************

*CALL REBUILD.SCREEN

*************************** PRINT ***************************************
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C' THEN
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM01,WS.CLASS.NAME01)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM02,WS.CLASS.NAME02)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM03,WS.CLASS.NAME03)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM04,WS.CLASS.NAME04)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM05,WS.CLASS.NAME05)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM06,WS.CLASS.NAME06)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM07,WS.CLASS.NAME07)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM08,WS.CLASS.NAME08)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM09,WS.CLASS.NAME09)
        CALL DBR('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,ID.ITEM10,WS.CLASS.NAME10)

        XX.01                = SPACE(132)
        XX.01<1,1>[03,45]    = WS.CLASS.NAME01
        XX.01<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.01),'1')

        XX.02                = SPACE(132)
        XX.02<1,1>[03,45]    = WS.CLASS.NAME02
        XX.02<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.02),'1')

        XX.03                = SPACE(132)
        XX.03<1,1>[03,45]    = WS.CLASS.NAME03
        XX.03<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.03),'1')

        XX.04                = SPACE(132)
        XX.04<1,1>[03,45]    = WS.CLASS.NAME04
        XX.04<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.04),'1')

        XX.05                = SPACE(132)
        XX.05<1,1>[03,45]    = WS.CLASS.NAME05
        XX.05<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.05),'1')

        XX.06                = SPACE(132)
        XX.06<1,1>[03,45]    = WS.CLASS.NAME06
        XX.06<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.06),'1')

        XX.07                = SPACE(132)
        XX.07<1,1>[03,45]    = WS.CLASS.NAME07
        XX.07<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.07),'1')

        XX.08                = SPACE(132)
        XX.08<1,1>[03,45]    = WS.CLASS.NAME08
        XX.08<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.08),'1')

        XX.09                = SPACE(132)
        XX.09<1,1>[03,45]    = WS.CLASS.NAME09
        XX.09<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.09),'1')

        XX.10                = SPACE(132)
        XX.10<1,1>[03,45]    = WS.CLASS.NAME10
        XX.10<1,1>[50,15]    = DROUND(R.NEW(RM.CLASS.MAIN.ITEM.10),'1')

*   WS.TOTAL.CBE = R.NEW(RM.TOT.CUS)
        WS.TOTAL.CUS = (R.NEW(RM.TOT.CUS) * 10) - 4.5
        WS.CL.NAME   = R.NEW(RM.CUSTOMER.NAME)
        WS.CL.REF    = R.NEW(RM.CUSTOMER.ID)

        WS.GROUP.NAME = ''
        WS.GROUP.CODE = ''

        WS.GROUP.NAME = R.NEW(RM.GROUP.NAME)
        WS.GROUP.CODE = R.NEW(RM.GROUP.CODE)

        WS.CL.BR     = R.NEW(RM.CUS.BR)
        WS.SYS.DATE  = R.NEW(RM.SYSTEM.DATE)
        WS.INPUTTER   = R.NEW(RM.INPUTTER)
*Line [ 139 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.INP.COUNT  = DCOUNT (WS.INPUTTER,@VM)
        WS.INPUTTER.P = WS.INPUTTER<1,(WS.INP.COUNT+1)>
        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.CL.BR,WS.BR.NAME)
        XX.CL.REF = SPACE(132)
        XX.CL.NAME= SPACE(132)
        XX.CL.BR  = SPACE(132)

        XX.EMPTY  = SPACE(132)
*--------------------------------------------------------------------
        YYBRN  = FIELD(COMP,'.',2)

        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.CL.BR,WS.HD.BR.NAME)
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

        SYS.DATE  = WS.SYS.DATE[7,2]:'/':WS.SYS.DATE[5,2]:'/':WS.SYS.DATE[1,4]
        PR.HD  ="'L'":SPACE(1):"��� ���� ������":SPACE(45):"VIR.RISK.CLASS.P"
        PR.HD :="'L'":"����� : ": WS.HD.BR.NAME:SPACE(40): "������� : ":T.DAY
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(25):"������ ����� ������ ������� - �����"

        PR.HD :="'L'":SPACE(35):SYS.DATE
        PR.HD :="'L'":STR("-",80)
        PR.HD :="'L'":" "
        PRINT
        HEADING PR.HD

*--------------------------------------------------------------------

        XX.CL.REF<1,1>[03,20]  = "��� ���� ������"
        XX.CL.REF<1,1>[25,10]  = WS.CL.REF

        XX.CL.NAME<1,1>[03,20]  = "����� ��������"
        XX.CL.NAME<1,1>[25,50]  = WS.CL.NAME

        XX.CL.BR<1,1>[03,20] = "����� ���������"
        XX.CL.BR<1,1>[25,50] = WS.BR.NAME

* XX.GROUP.CODE = SPACE(132)
* XX.GROUP.CODE<1,1>[03,20]  = "���� ��������"
* XX.GROUP.CODE<1,1>[25,10]  = WS.GROUP.CODE

        XX.GROUP.NAME = SPACE(132)
        XX.GROUP.NAME<1,1>[03,20]  = "���� ���������"
        XX.GROUP.NAME<1,1>[25,50]  = WS.GROUP.NAME

        XX.TOT = SPACE(132)
        XX.TOT<1,1>[10,20] = "����� ������� ��������"
        XX.TOT<1,1>[50,15] = DROUND(WS.TOTAL.CUS,'1')

        XX.TOT.CBE = SPACE(132)
        XX.TOT.CBE<1,1>[10,20] = "���� ����� ������ "
        XX.TOT.CBE<1,1>[50,15] = DROUND(R.NEW(RM.TOT.CUS),'0')

        XX.INP = SPACE(132)
        XX.INP<1,1>[01,20] = "��������"
* XX.INP<1,1>[25,15] = WS.INPUTTER.P
        XX.INP<1,1>[25,15] = R.USER<EB.USE.SIGN.ON.NAME>


        PRINT XX.EMPTY
        PRINT XX.CL.REF<1,1>
        PRINT XX.EMPTY
        PRINT XX.CL.NAME<1,1>
*  PRINT XX.EMPTY
        PRINT XX.GROUP.NAME<1,1>
* PRINT XX.EMPTY
* PRINT XX.GROUP.NAME<1,1>
        PRINT XX.CL.BR<1,1>

* PRINT XX.GROUP.CODE
* PRINT XX.GROUP.CODE<1,1>
* PRINT XX.EMPTY
* PRINT XX.GROUP.NAME<1,1>
* PRINT XX.EMPTY
* PRINT XX.GROUP.NAME<1,1>

        PRINT XX.EMPTY
        PRINT STR("-",80)


        PRINT XX.01<1,1>
        PRINT XX.EMPTY
        PRINT XX.02<1,1>
        PRINT XX.EMPTY
        PRINT XX.03<1,1>
        PRINT XX.EMPTY
        PRINT XX.04<1,1>
        PRINT XX.EMPTY
        PRINT XX.05<1,1>
        PRINT XX.EMPTY
        PRINT XX.06<1,1>
        PRINT XX.EMPTY
        PRINT XX.07<1,1>
        PRINT XX.EMPTY
        PRINT XX.08<1,1>
        PRINT XX.EMPTY
        PRINT XX.09<1,1>
        PRINT XX.EMPTY
        PRINT XX.10<1,1>
        PRINT STR("-",80)
        PRINT XX.EMPTY
        PRINT XX.TOT
        PRINT XX.EMPTY
        PRINT XX.TOT.CBE
        PRINT STR("-",80)
        PRINT XX.EMPTY
        PRINT XX.INP
    END
*************************************************************************
    RETURN
END
