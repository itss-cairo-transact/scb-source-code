* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----MAI SAAD 03 SEP 2018-----------------
* <Rating>1623</Rating>
*----------------------
    SUBROUTINE  VISA.EXP.DATE
*  PROGRAM  VISA.EXP.DATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

    GOSUB INITIALISE
    RETURN

INITIALISE:
*----------------------------------------------------------------------* 
    FN.CARD  = 'FBNK.CARD.ISSUE' ; F.CARD = ''
    CALL OPF(FN.CARD , F.CARD) 
****************************************************
    DIR.NAME2 = '&SAVEDLISTS&'
    NEW.FILE2 = "CARD.ERR.":TODAY

    OPENSEQ DIR.NAME2,NEW.FILE2 TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME2:' ':NEW.FILE2
        HUSH OFF
        PRINT 'FILE ':NEW.FILE2:' DELETE FROM ':DIR.NAME2
    END
    OPENSEQ DIR.NAME2, NEW.FILE2 TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE2:' CREATED IN ':DIR.NAME2
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE2:' to ':DIR.NAME2
        END
    END
**-------------------------------------------------------
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "CARD.ISSUE"
    OFS.OPTIONS      = "CARD.EXPIRY"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    COMMA = ","
    DIRCY = '&SAVEDLISTS&'
    FILE.NAME = "CARD_EXP.txt"

    OPENSEQ DIRCY,FILE.NAME TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 1
    CARD.NO = ''
**CRD.COD = ''
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CHK.LIN      = FIELD(Line,",",1)[1,6]
            IF CHK.LIN # '' THEN
                IF CHK.LIN EQ  '404238' THEN
                    CARD.NO = "VICL.":FIELD(Line,",",1)
                END ELSE
                    IF CHK.LIN EQ  '404239' THEN
                        CARD.NO = "VIGO.":FIELD(Line,",",1)
                    END
                END
                IF CHK.LIN EQ  '524815' THEN
                    CARD.NO = "MACL.":FIELD(Line,",",1)
                END ELSE
                    IF CHK.LIN EQ  '525715' THEN
                        CARD.NO = "MAGO.":FIELD(Line,",",1)
                    END
                END
               ** TEXT = "CARD NO= ": CARD.NO ; CALL REM
                EXP.DATE = FIELD(Line,",",2)
                DD = EXP.DATE[1,2]
                MM = EXP.DATE[4,2]
                YYYY = EXP.DATE[7,4]
                CARD.EXP.DATE = YYYY:MM:DD

                CALL F.READ(FN.CARD, CARD.NO, R.CARD ,F.CARD, READ.ER.CARD)
** R.CARD<CARD.IS.EXPIRY.DATE> =  CARD.EXP.DATE
** CALL F.WRITE(FN.CARD,CARD.NO,R.CARD)
** CALL JOURNAL.UPDATE(CARD.NO)
*DEBUG
                EXP.DATE         = CARD.EXP.DATE
** ----------------------- OFS ----------------
                OFS.MESSAGE.DATA :="EXPIRY.DATE=":CARD.EXP.DATE:COMMA
                COMP =  R.CARD<CARD.IS.CO.CODE>
                BRN    = COMP[8,2]
                COMPO  = COMP
                OFS.USER.INFO     = "INPUTT":BRN:"//":COMPO

                OFS.IDD = CARD.NO
                F.PATH  = FN.OFS.IN
                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.IDD:COMMA:OFS.MESSAGE.DATA
                OFS.ID  = "CARD.":CARD.NO:"-":TODAY

                IF BRN NE '' THEN
                    XX = XX + 1
                    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-CARD.EXP' ON ERROR  TEXT = " ERROR ";CALL REM
                    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                END ELSE      ;**Else of not write**
                    CARD.DATA = CARD.NO
                    CARD.DATA := "|EXPIRY DATE="
                    CARD.DATA :=  CARD.EXP.DATE
                    CARD.DATA := "|Branch="
                    CARD.DATA :=  BRN
                    NM = NM + 1
                    WRITESEQ CARD.DATA TO V.FILE.IN ELSE
                        PRINT  'CAN NOT WRITE LINE ':CARD.DATA
                    END
                END
            END
            OFS.MESSAGE.DATA = ''
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath

    TEXT = '�� �������� �� ��������' ; CALL REM
    TEXT = '������ �������� �������':XX ; CALL REM
    TEXT = '��� �������� ���� �� ���������':NM ; CALL REM
    RETURN
***------------------------------------------------------------------***
END
