* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SCB.BS.CHECK

*   MAKE REGISTER AMOUNT EQ TOTAL AMOUNT & REGISTER NUMBER EQ TOTAL NUMBER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

*    IF R.NEW(SCB.BS.CO.CODE) EQ '' THEN
*    R.NEW(SCB.BS.REG.NO)  = R.NEW(SCB.BS.TOTAL.NO)
*    END
*    XX = R.NEW(SCB.BS.REG.AMT)
*    YY = R.NEW(SCB.BS.TOTAL.AMT)
*    ZZ = R.OLD(SCB.BS.TOTAL.AMT)
*    XX = YY - ZZ + XX

    R.NEW(SCB.BS.REG.NO)  = R.NEW(SCB.BS.TOTAL.NO)
    R.NEW(SCB.BS.REG.AMT) = R.NEW(SCB.BS.TOTAL.AMT)
    RETURN
END
