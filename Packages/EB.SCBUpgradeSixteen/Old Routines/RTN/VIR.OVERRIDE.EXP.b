* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>148</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.OVERRIDE.EXP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*----------------------------------------
*** CRETED BY NESSMA 2014/12/23
*** COPIED FROM VIR.OVERRIDE.WARNING

    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C' OR V$FUNCTION = 'H' OR V$FUNCTION = 'R' THEN
        IF APPLICATION EQ 'SCB.P.SER.NO' THEN
            IF PGM.VERSION EQ ',SCB.REV' THEN
                TEXT = "Need Approval to use this version"
                CALL STORE.OVERRIDE(CURR.NO)
            END
        END
        IF APPLICATION EQ 'SCB.P.CHEQ' THEN
            IF PGM.VERSION EQ ',SCB' THEN
                IF V$FUNCTION = 'R' THEN
                    WS.TRNS.DATE     = R.NEW(P.CHEQ.TRN.DAT)
                    IF WS.TRNS.DATE NE TODAY THEN
                        TEXT    = "Need Approval to use this version"
                        CALL STORE.OVERRIDE(CURR.NO)
                        RETURN
                    END
                END ELSE
                    TEXT    = "Need Approval to use this version"
                    CALL STORE.OVERRIDE(CURR.NO)
                END
            END
        END
        IF APPLICATION EQ 'SCB.FT.DR.CHQ' THEN
            TEXT = "Need Approval to use this version"
            CALL STORE.OVERRIDE(CURR.NO)
        END
*------------------------------------
        RETURN
    END
