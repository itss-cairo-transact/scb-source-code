* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>525</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.DOC.OVER

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    ACC1    = R.NEW(DOC.PRO.DEBIT.ACCT)
    ACC2    = R.NEW(DOC.PRO.COMM.ACCT)
    AMTT    = R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,1> + R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,2> + R.NEW( DOC.PRO.COMMISSION.AMOUNT)
    AMTT1   = R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,1> + R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,2>
    AMTT2   = R.NEW(DOC.PRO.COMMISSION.AMOUNT)

    ZZZ = '0000000'

    IF ACC1 EQ ACC2 THEN

        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC1,BAL)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC1,CUS.ID)
        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC1,CUS.LIM)

        LIMM = FIELD(CUS.LIM,'.',1)
        AA = 7 - LEN(LIMM)
        XX = CUS.ID:'.':ZZZ[1,AA]:CUS.LIM

        T.SEL2 = "SELECT FBNK.LIMIT WITH @ID EQ ":XX
        CALL EB.READLIST(T.SEL2,KEY.LIST,"",SELECTED,ER.MSG)
        CALL F.READ(FN.LIM,KEY.LIST,R.LIM,F.LIM,E2)
        TEXT = "SEL : " : SELECTED ; CALL REM
        LIMIT.AMT = R.LIM<LI.AVAIL.AMT,1>

        IF LIMIT.AMT EQ '' THEN LIMIT.AMT = 0

        TOTAL.DB  = BAL - AMTT
        TOTAL.CR  = TOTAL.DB + LIMIT.AMT

        IF TOTAL.CR LT 0 THEN
            ETEXT = "������ ����� ��� ������ ������ �� ���" ; CALL STORE.END.ERROR
        END
        AMT = ''

    END ELSE

        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC1,BAL1)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC1,CUS.ID1)
        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC1,CUS.LIM1)


        LIMM = FIELD(CUS.LIM1,'.',1)
        AA = 7 - LEN(LIMM)
        XXX = CUS.ID1:'.':ZZZ[1,AA]:CUS.LIM1

        T.SEL2 = "SELECT FBNK.LIMIT WITH @ID EQ ":XXX
        CALL EB.READLIST(T.SEL2,KEY.LIST,"",SELECTED,ER.MSG)
        CALL F.READ(FN.LIM,KEY.LIST,R.LIM,F.LIM,E2)
        TEXT = "SEL : " : SELECTED ; CALL REM
        LIMIT.AMT1 = R.LIM<LI.AVAIL.AMT,1>

        IF LIMIT.AMT1 EQ '' THEN LIMIT.AMT1 = 0

        TOTAL.DB1  = BAL1 - AMTT1
        TOTAL.CR1  = TOTAL.DB1 + LIMIT.AMT1
        TEXT = "AMT= " : TOTAL.CR1 ; CALL REM
        IF TOTAL.CR1 LT 0 THEN
            ETEXT = "������ ����� ��� ������ ������ �� ���" ; CALL STORE.END.ERROR
        END
        AMT = ''

*        -----------------------------------

        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC2,BAL2)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC2,CUS.ID2)
        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC2,CUS.LIM2)


        LIMM = FIELD(CUS.LIM2,'.',1)
        AA   = 7 - LEN(LIMM)
        XXXX = CUS.ID2:'.':ZZZ[1,AA]:CUS.LIM2

        T.SEL3 = "SELECT FBNK.LIMIT WITH @ID EQ ":XXXX
        CALL EB.READLIST(T.SEL3,KEY.LIST,"",SELECTED,ER.MSG)
        CALL F.READ(FN.LIM,KEY.LIST,R.LIM3,F.LIM,E2)
        TEXT = "SEL : " : SELECTED ; CALL REM
        LIMIT.AMT2 = R.LIM3<LI.AVAIL.AMT,1>

        IF LIMIT.AMT2 EQ '' THEN LIMIT.AMT2 = 0

        TOTAL.DB2  = BAL2 - AMTT2
        TOTAL.CR2  = TOTAL.DB2 + LIMIT.AMT2
        TEXT = "AMT2= " : TOTAL.CR2 ; CALL REM

        IF TOTAL.CR2 LT 0 THEN
            ETEXT = "������ ����� ��� ������ ������ �� ���" ; CALL STORE.END.ERROR

        END
        AMT = ''

    END
***************************************************************************

    RETURN
END
