* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1115</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.TRANS.TOT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS.TOT

***NESSREEN AHMED -SCB*******
    TEXT = '2' ; CALL REM
    F.VISA.TRANS = '' ; FN.VISA.TRANS = 'F.SCB.VISA.TRANS' ; R.VISA.TRANS = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.TRANS,F.VISA.TRANS)

    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    F.VISA.TRANS.TOT = '' ; FN.VISA.TRANS.TOT = 'F.SCB.VISA.TRANS.TOT' ; R.VISA.TRANS.TOT = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.VISA.TRANS.TOT,F.VISA.TRANS.TOT)
***UPDATED IN 30/1/2008*****
**YTEXT = "Enter the Start Date Of Month : "
**CALL TXTINP(YTEXT, 8, 22, "12", "A")
    DATEE = '' ; MT = '' ; MU= '' ; YYYY = '' ; MM = '' ; YYDD = '' ; MON = '' ; YEARN = ''
    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YEARN = YYYY-1
    END ELSE
        MU = MT-1
        YEARN = YYYY
    END
******************************************
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END
******************************************
    YYDD = YEARN:MON:'01'
****************************
    BR = R.USER<EB.USE.DEPARTMENT.CODE>
******UPDATED ON 14/10/2008****************************
**    T.SEL = "SELECT F.SCB.VISA.TRANS WITH POS.DATE GE ":YYDD :" AND BRANCH.NUMBER EQ ":BR :" BY CUST.ACCT "
    T.SEL = "SELECT F.SCB.VISA.TRANS WITH POS.DATE GE ":YYDD :" BY CUST.ACCT "
*******************************************************
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED.VISA.TRANS=':SELECTED ; CALL REM
    IF SELECTED THEN          ;* SELECTED OF I*
        FOR I = 1 TO SELECTED
            KEY.VISA = '' ; KEY.DAT = '' ; KEY.TO.USE = ''; CARD.CO = ''  ; ACCT.NO = '' ; TRANS.CODE = ''
            CUS.NAME = '' ; KEY.VISA = '' ; KEY.DAT = '' ; XX = '' ; BR.NO = ''
            CALL F.READ(FN.VISA.TRANS,KEY.LIST<I>,R.VISA.TRANS,F.VISA.TRANS,E1)
            CUS.NAME = R.VISA.TRANS<SCB.TRANS.CUST.NAME>
            ACCT.NO =R.VISA.TRANS<SCB.TRANS.CUST.ACCT>
            TRANS.CODE = R.VISA.TRANS<SCB.TRANS.TRANS.CODE>
****ADDED ON 31/1/2008**************************************
            BR.NO = R.VISA.TRANS<SCB.TRANS.BRANCH.NUMBER>
************************************************************
            KEY.VISA = KEY.LIST<I>[1,16]
            KEY.DAT = KEY.LIST<I>[17,8]
            N.SEL = "SELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO EQ ":KEY.VISA
            KEY.LIST.N ="" ; SELECTED.N="" ;  ER.MSG.N=""
            CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
            IF SELECTED.N THEN
                FOR TT = 1 TO SELECTED.N
                    KEY.TO.USE = ''
                    CALL F.READ(FN.CARD.ISSUE,KEY.LIST.N<TT>,R.CARD.ISSUE,F.CARD.ISSUE,E2)
                    CI.LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                    CARD.CO = CI.LOCAL.REF<1,LRCI.CARD.CODE>
                    IF CARD.CO = '101' OR CARD.CO EQ '102' OR CARD.CO EQ '201' OR CARD.CO EQ '202' OR CARD.CO = '104' OR CARD.CO = '105' OR CARD.CO = '204' OR CARD.CO = '205' THEN
                        KEY.TO.USE = KEY.LIST<I>
                        DD = '' ; DTC = ''
*Line [ 113 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DTC = DCOUNT(TRANS.CODE,@VM)
                        FOR DD = 1 TO DTC
                            TRANS.CODE = '' ; TRANS.CURR = '' ; TRANS.AMT = '' ; TRANS.POS.DAT = ''  ; TRANS.CODE.TOT = ''
                            DC.TOT = '' ; XX = ''
                            TRANS.CODE = R.VISA.TRANS<SCB.TRANS.TRANS.CODE,DD>
                            TRANS.CURR = R.VISA.TRANS<SCB.TRANS.TRANS.CURR,DD>
                            TRANS.AMT  = R.VISA.TRANS<SCB.TRANS.TRANS.AMT,DD>
                            TRANS.POS.DAT = R.VISA.TRANS<SCB.TRANS.POS.DATE,DD>
                            CALL F.READ(FN.VISA.TRANS.TOT,KEY.LIST<I>,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E3)
                            IF NOT(E3) THEN
                                TRANS.CODE.TOT = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 125 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                DC.TOT = DCOUNT(TRANS.CODE.TOT,@VM)
                                XX =  DC.TOT +1
                                R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE,XX>= TRANS.CODE
                                R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CURR,XX>= TRANS.CURR
                                R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT,XX>= TRANS.AMT
                                R.VISA.TRANS.TOT<VISA.TRANS.POS.DATE,XX>= TRANS.POS.DAT
                                CALL F.WRITE(FN.VISA.TRANS.TOT,KEY.LIST<I>, R.VISA.TRANS.TOT)
                                CALL JOURNAL.UPDATE(KEY.LIST<I>)
                            END ELSE
                                TRANS.CODE.TOT = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 136 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                DC.TOT = DCOUNT(TRANS.CODE.TOT,@VM)
                                XX =  DC.TOT +1
                                R.VISA.TRANS.TOT<VISA.TRANS.CUST.NAME>  = CUS.NAME
                                R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>  = ACCT.NO
                                R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE,XX>= TRANS.CODE
                                R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CURR,XX>= TRANS.CURR
                                R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT,XX>= TRANS.AMT
                                R.VISA.TRANS.TOT<VISA.TRANS.POS.DATE,XX>= TRANS.POS.DAT
****ADDED ON 31/1/2008*********************************************************
                                R.VISA.TRANS.TOT<VISA.TRANS.BRANCH.NUMBER> = BR.NO
*********************************************************************************
                                CALL F.WRITE(FN.VISA.TRANS.TOT,KEY.LIST<I>, R.VISA.TRANS.TOT)
                                CALL JOURNAL.UPDATE(KEY.LIST<I>)
                            END
                        NEXT DD
                    END ELSE
********UPDATED 3/11/2008**********
**  S.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":ACCT.NO : " AND (CARD.CODE EQ 101 OR CARD.CODE EQ 102 OR CARD.CODE EQ 104 OR CARD.CODE EQ 105 OR CARD.CODE EQ 201 OR CARD.CODE EQ 202 OR CARD.CODE EQ 204 OR CARD.CODE EQ 205)"
                        S.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":ACCT.NO : " AND (CARD.CODE EQ 101 OR CARD.CODE EQ 102 OR CARD.CODE EQ 104 OR CARD.CODE EQ 105 OR CARD.CODE EQ 201 OR CARD.CODE EQ 202 OR CARD.CODE EQ 204 OR CARD.CODE EQ 205) BY CARD.CODE"
***********************************
                        KEY.LIST.S ="" ; SELECTED.S="" ;  ER.MSG.S=""
                        CALL EB.READLIST(S.SEL,KEY.LIST.S,"",SELECTED.S,ER.MSG.S)
                        IF SELECTED.S THEN
                            CI.LOCAL.REF = '' ; E2 = '' ; RETRY2 = ''  ; KEY.TO.USE = ''
*********UPDATED 3/11/2008*********
**                   CALL F.READU(FN.CARD.ISSUE,KEY.LIST.S<1>,R.CARD.ISSUE,F.CARD.ISSUE,E2,RETRY2)
                            CALL F.READ(FN.CARD.ISSUE,KEY.LIST.S<SELECTED.S>,R.CARD.ISSUE,F.CARD.ISSUE,E2)
************************************
                            CI.LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                            VISA.CARD = CI.LOCAL.REF<1,LRCI.VISA.NO>
                            KEY.TO.USE = VISA.CARD:KEY.DAT
**********
*Line [ 169 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DTC = DCOUNT(TRANS.CODE,@VM)
                            FOR DD = 1 TO DTC
                                TRANS.CODE = '' ; TRANS.CURR = '' ; TRANS.AMT = '' ; TRANS.POS.DAT = ''  ; TRANS.CODE.TOT = ''
                                DC.TOT = '' ; XX = ''
                                TRANS.CODE = R.VISA.TRANS<SCB.TRANS.TRANS.CODE,DD>
                                TRANS.CURR = R.VISA.TRANS<SCB.TRANS.TRANS.CURR,DD>
                                TRANS.AMT  = R.VISA.TRANS<SCB.TRANS.TRANS.AMT,DD>
                                TRANS.POS.DAT = R.VISA.TRANS<SCB.TRANS.POS.DATE,DD>
                                CALL F.READU(FN.VISA.TRANS.TOT,KEY.TO.USE,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E3,RETRY3)
                                IF NOT(E3) THEN
                                    TRANS.CODE.TOT = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 181 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                    DC.TOT = DCOUNT(TRANS.CODE.TOT,@VM)
                                    XX =  DC.TOT +1
                                    R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE,XX>= TRANS.CODE
                                    R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CURR,XX>= TRANS.CURR
                                    R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT,XX>= TRANS.AMT
                                    R.VISA.TRANS.TOT<VISA.TRANS.POS.DATE,XX>= TRANS.POS.DAT
                                    CALL F.WRITE(FN.VISA.TRANS.TOT,KEY.TO.USE, R.VISA.TRANS.TOT)
                                    CALL JOURNAL.UPDATE(KEY.TO.USE)
                                END ELSE
                                    TRANS.CODE.TOT = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 192 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                    DC.TOT = DCOUNT(TRANS.CODE.TOT,@VM)
                                    XX =  DC.TOT +1
                                    R.VISA.TRANS.TOT<VISA.TRANS.CUST.NAME>  = CUS.NAME
                                    R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>  = ACCT.NO
                                    R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE,XX>= TRANS.CODE
                                    R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CURR,XX>= TRANS.CURR
                                    R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT,XX>= TRANS.AMT
                                    R.VISA.TRANS.TOT<VISA.TRANS.POS.DATE,XX>= TRANS.POS.DAT
****ADDED ON 31/1/2008*******************************************************************
                                    R.VISA.TRANS.TOT<VISA.TRANS.BRANCH.NUMBER> = BR.NO
*****************************************************************************************
                                    CALL F.WRITE(FN.VISA.TRANS.TOT,KEY.TO.USE, R.VISA.TRANS.TOT)
                                    CALL JOURNAL.UPDATE(KEY.TO.USE)
                                END
                            NEXT DD
**********
                        END

                    END       ;*IF CARD.CODE*
                NEXT TT
            END     ;*IF SELECTED.N*
**************************************************************************
        NEXT I
    END   ;*IF SELECTED*

    RETURN
END
