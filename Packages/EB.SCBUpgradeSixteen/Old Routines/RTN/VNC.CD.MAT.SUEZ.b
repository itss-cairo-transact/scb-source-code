* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VNC.CD.MAT.SUEZ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*TO CHANGE TO CALL READ

    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CAT.LD)
    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,FIN.MAT)
    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
    CD.TY = MYLOCAL<1,LDLR.CD.TYPE>
    FIN.MAT = MYLOCAL<1,LDLR.ACTUAL.EXP.DATE>

    TEXT=TODAY:'-':FIN.MAT;CALL REM
    IF FIN.MAT LE TODAY AND CAT.LD EQ '21101' THEN
        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CO.CODE,ID.NEW,LD.CODE)
*    LD.CODE     = R.LD<LD.CO.CODE>
        COMP =LD.CODE[8,2]
        IF CAT.LD EQ '' THEN
            E='��� �� ���� ����� ����� �� ���'
            CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
**************MODIFIED ON 1-10-2017
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.BLOCK.PURPOSE> THEN
                E='��� ������������ �����';CALL ERR; MESSAGE='REPEAT'
            END
        END
************************************
*****************************
        RECORD.STAT =  R.NEW(LD.RECORD.STATUS)
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
        REST      = 'Same.Version'

        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E = 'You.Must.Authorize.OR.Delete.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                IF V$FUNCTION = 'I' THEN
                    IF  RECORD.STAT EQ 'INAU' THEN
                        E = 'Cant.Amend.NAU.Record': REST; CALL ERR ;MESSAGE = 'REPEAT'
                    END
                END
            END
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
            R.NEW(LD.LOCAL.REF)<1,LDLR.PROJECT.NAME> = PGM.VERSION
            IF CD.TY EQ 'EGP-10-60M-SUEZ' THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = 'EGP16104000500':COMP

            END ELSE
                IF CD.TY EQ 'EGP-100-60M-SUEZ' THEN
                    R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = 'EGP16104000400':COMP

                END ELSE
                    R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT> = ''

                END
            END

            IF R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> EQ '' THEN
                AMT = R.NEW(LD.AMOUNT) * 1.8
                R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> = AMT
            END

            TEXT= R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT>:'-AMT';CALL REM
            R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT> ='EGP16169000100':COMP
            R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
            R.NEW(LD.LOCAL.REF)<1,LDLR.CD.NO> = ''
            R.NEW(LD.FIN.MAT.DATE)=  TODAY
        END
    END ELSE
        E='NOT MATURED YET OR INCORRECT CATEGORY';CALL ERR; MESSAGE='REPEAT'
    END
********************
    IF V$FUNCTION = 'R' THEN
        E = 'No REVERSE '; CALL ERR ; MESSAGE = 'REPEAT'
    END

    RETURN
END
