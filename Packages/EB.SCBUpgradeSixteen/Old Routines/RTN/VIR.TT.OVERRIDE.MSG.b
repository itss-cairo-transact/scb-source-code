* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
**** CREATED BY NESSREEN AHMED 2012/5/21****
*********************************************
    SUBROUTINE VIR.TT.OVERRIDE.MSG

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    GOSUB OPEN.FILES
    GOSUB CHK.APP.ID
    GOSUB CHK.ACCT.CATG
    RETURN
************************************************
OPEN.FILES:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CHQ = "F.SCB.FT.DR.CHQ" ; F.CHQ = '' ; R.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    WS.USED.LCY.AMT = 0

    RETURN
************************************************
CHK.APP.ID:
    WS.APP.ID = APPLICATION
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C'  THEN
        IF  WS.APP.ID = "TELLER" THEN
            GOSUB TT.APP
        END
    END
    RETURN
************************************************
TT.APP:
    WS.TRNS.AC.ID = R.NEW(TT.TE.ACCOUNT.1)
***UPDATED BY NESSREEN AHMED 1/7/2012**************
***WS.CHK.TRNS.AMT = R.NEW(TT.TE.NET.AMOUNT)
    WS.CHK.TRNS.AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
***END OF UPDATE 1/7/2012***************************
    IF WS.CHK.TRNS.AMT > 20000 THEN
        GOSUB CASH.OVER.MSG
    END
***UPDATED BY NESSREEN AHMED 7/3/2019*********
    DB.CR.MK = R.NEW(TT.TE.DR.CR.MARKER)
    IF (WS.TRNS.AC.ID = 'EGP1615100020099') AND (DB.CR.MK = 'DEBIT') THEN
        TEXT = "Need Approval to Debit Account"
*     CALL STORE.OVERRIDE(CURR.NO)
        CALL STORE.OVERRIDE(R.NEW(CURR.NO))
    END
***END OF UPDATE 7/3/2019*********************
    GOSUB CHK.FT.CHQ.NUM

    RETURN
************************************************
CASH.OVER.MSG:
    CALL F.SCB.ATH.LEVEL(WS.APP.ID,WS.CHK.TRNS.AMT,TEXT)
    CALL STORE.OVERRIDE(R.NEW(CURR.NO))
    WS.CHK.TRNS.AMT = 0
    RETURN
************************************************
**** ������ �� ����� ��� ������� �������� �� ���� ������� ****
CHK.FT.CHQ.NUM:
    WS.MSG = ''
    CALL F.SCB.CHK.DR.BANK.CHQ(R.NEW(TT.TE.ACCOUNT.1),R.NEW(TT.TE.CHEQUE.NUMBER),R.NEW(TT.TE.AMOUNT.LOCAL.1),WS.MSG)
    IF  WS.MSG # '' THEN
        TEXT = WS.MSG
        CALL STORE.OVERRIDE(R.NEW(CURR.NO))
        RETURN
    END

    RETURN
************************************************
CHK.ACCT.CATG:
*-------------
* ADDED BY NESSMA 2014/08/25
    IF PGM.VERSION NE ',SCB.DRAW.SUEZ' THEN
        ACCT.ID = R.NEW(TT.TE.ACCOUNT.1)
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.ID,CATEG)
        IF CATEG EQ '1005' THEN
            ETEXT = "��� ����� �� ����� �� ��� ������"
            CALL STORE.END.ERROR
        END
    END
    RETURN
************************************************
END
