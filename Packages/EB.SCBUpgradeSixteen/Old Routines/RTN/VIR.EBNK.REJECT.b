* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.EBNK.REJECT

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT TEMENOS.BP I_CU.LOCAL.REFS



    CUST.ID = R.NEW(FT.DEBIT.CUSTOMER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF)
    LOAN = LOC.REF<1,CULR.LOAN.TYPE>
*Line [ 29 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    POS = DCOUNT(LOAN,@SM)
    IF POS GT 0 THEN
        FOR X = 1 TO POS
            LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>
            IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                X = POS
                OVE.ARRAY = R.NEW(FT.OVERRIDE)
*Line [ 37 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                Y.CNT = DCOUNT(OVE.ARRAY,@VM)
                Y.ETEXT.ARR = ''
                IF OVE.ARRAY THEN
                    FOR I = 1 TO Y.CNT
                        Y.ETEXT.ARR<1,-1> = FIELD(OVE.ARRAY<1,I>,'}',1)
                    NEXT I

                END
                NOT.ERROR='AVAILABLE.BALANCE.OVERDRAFT'
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                Y.OVER.CNT = DCOUNT(Y.ETEXT.ARR,@VM)
                FOR J=1 TO Y.OVER.CNT
                    LOCATE Y.ETEXT.ARR<1,J> IN NOT.ERROR<1> SETTING POS THEN

                        ETEXT = ',RESPONS.CODE=D51,'
                        CALL STORE.END.ERROR
                    END

                NEXT J
            END
        NEXT X
    END
    RETURN
