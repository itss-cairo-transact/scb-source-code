* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VNC.ATM.MOBILE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.MOBILE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
**----------------------------------------
    FN.CRD = 'FBNK.CARD.ISSUE' ; F.CRD = ''
    CALL OPF(FN.CRD,F.CRD)

    WS.CRD.ID = "ATMC.":ID.NEW

    CALL F.READ(FN.CRD,WS.CRD.ID,R.CRD,F.CRD,E1)
    IF NOT(E1) THEN
        WS.ACC.NO = R.CRD<CARD.IS.ACCOUNT>

        IF WS.ACC.NO[1,1] EQ '0' THEN
            WS.CUST.NO = WS.ACC.NO[2,7]
        END ELSE
            WS.CUST.NO = WS.ACC.NO[1,8]
        END

        WS.CARD.CODE = R.CRD<CARD.IS.LOCAL.REF><1,LRCI.CARD.CODE>
        IF WS.CARD.CODE EQ '810' OR WS.CARD.CODE EQ '811' THEN
            WS.CARD.TYPE = 'S'
        END ELSE
            WS.CARD.TYPE = 'P'
        END

        R.NEW(SAM.CUSTOMER.NO)   = WS.CUST.NO
        R.NEW(SAM.CUSTOMER.NAME) = R.CRD<CARD.IS.NAME>
        R.NEW(SAM.CARD.TYPE)     = WS.CARD.TYPE
        R.NEW(SAM.SMS.STATEMENT) = 'T'
        R.NEW(SAM.SMS.ONLINE)    = 'T'
        R.NEW(SAM.RECV.DATE)     = TODAY
        R.NEW(SAM.STATUS)        = '1'

    END ELSE
        E = 'THIS CARD NOT EXIST'
        CALL ERR ; MESSAGE = 'REPEAT'
    END


    RETURN
END
