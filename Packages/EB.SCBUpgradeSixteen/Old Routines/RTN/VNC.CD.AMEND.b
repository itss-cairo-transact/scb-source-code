* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CD.AMEND

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.PREM.AMT

*TO CHECK  THAT IF CD NOT EXIST OR MATURED OR LIQUIDATED THEN ERROR MESSAGE WILL BE DISPLAYED
*TO DEFAULT VERSION NAME

    IF V$FUNCTION EQ 'I' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,MYMATR)
        IF ETEXT THEN
            E='��� �� ���� ����� ����� �� ���'; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            IF MYMATR LT TODAY THEN
                E='��� ������� ������';CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=',SCB.CD.LIQ' THEN
                    E='��� ������� ������';CALL ERR;MESSAGE='REPEAT'
                END ELSE
                    R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
                END
            END
        END
        IF R.NEW(LD.CATEGORY) EQ 21021 THEN
            IF R.NEW(LD.VALUE.DATE) GE 20111120 AND R.NEW(LD.VALUE.DATE) LE 20120618 THEN
                E='��� ����� ������ ��� �������' ; CALL ERR;MESSAGE = 'REPEAT'
            END
        END
    END
    RETURN
END
