* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
***********INGY&DALIA-SCB 06/04/2003***********
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VIR.LC.DOC.SERIAL


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.ADVICE.TEXT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.CLAUSES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT

*Line [ 40 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
  DCO1 = DCOUNT(R.NEW(TF.LC.DOCUMENT.TXT), @VM)
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
  DCO2 = DCOUNT(R.NEW(TF.LC.CLAUSES.TEXT), @VM)
TEXT = DCO1 :" ":DCO2 ; CALL REM
FOR I = 1 TO DCO1
  IF NOT(NUM(R.NEW(TF.LC.DOCUMENT.TXT))<1,I,1>)[1,1] AND R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1>[2,1] # "-" THEN
  R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1> = I:"-":R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1>
 END ELSE R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1>[1,1] = I
NEXT I
FOR Y = 1 TO  DCO2
IF NOT(NUM(R.NEW(TF.LC.DOCUMENT.TXT))<1,I,1>[1,1]) AND R.NEW(TF.LC.DOCUMENT.TXT)<1,I,1>[2,1] # "-" THEN
  R.NEW(TF.LC.CLAUSES.TEXT)<1,Y> = Y:"-":R.NEW(TF.LC.CLAUSES.TEXT)<1,Y>
END ELSE R.NEW(TF.LC.CLAUSES.TEXT)<1,Y>[1,1] = Y
NEXT Y

RETURN
END
