* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------

*********** WAEL **********

    SUBROUTINE VIR.LG.BIDBOND

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS

    IF R.NEW( LD.LOCAL.REF)< 1,LDLR.MARGIN.PERC> = '100' THEN

        MYPROD = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
        CALL DBR ('SCB.LG.PARMS':@FM:SCB.LG.LIMIT.REF,MYPROD,LIM.REF)
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.FIELD = DCOUNT(LIM.REF,@VM)
       **** R.NEW(LD.LIMIT.REFERENCE) = LIM.REF<1,NO.FIELD>
        CALL REBUILD.SCREEN

    END
*IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> LE 0 THEN E ='MARGIN.AMT.LE.ZERO' ; CALL ERR ; MESSAGE = 'REPEAT'
*IF R.NEW( LD.LOCAL.REF)< 1,LDLR.MARGIN.PERC> LE 0 THEN E ='MARGIN.PERC.LE.ZERO' ; CALL ERR ; MESSAGE = 'REPEAT'
    RETURN
END
