* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.NEW.ATM.POS.COMM
*******************************************************************************************************
*   BAKRY & KHALED 2021/01/17                                                                         *
*  CHECK THE ATM MESSAGE AT AUSORIZATION IF IT INTERNATIONAL OR PROCESSING CODE EQ 17 OR 12           *
*  COMI IS AC.LCK.LOC.ADDITIONAL.DATA                                                                 *
*  COMMISSION CODES :-                                                                                *
*  ATMSCBPOSI                                                                                         *
*  ATMSCBPOSL                                                                                         *
*******************************************************************************************************
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AT.ISO.COMMON

*-------------------------------
    PROCESSING.CODE = AT$INCOMING.ISO.REQ(3)
    WS.ADD.DATA = COMI

    IF V$FUNCTION = 'I' AND MESSAGE NE 'VAL' THEN
        IF WS.ADD.DATA[3] EQ 'Int' OR PROCESSING.CODE[1,2] = 17 OR PROCESSING.CODE[1,2] = 12 THEN
            WS.AMOUNT   = R.NEW(AC.LCK.LOCKED.AMOUNT)
            DEAL.AMT = R.NEW(AC.LCK.LOCKED.AMOUNT)
            CUST.ID = R.NEW(AC.LCK.ACCOUNT.NUMBER)[1,8] + 0
            DEAL.CCY = "EGP"
            CCY.MKT = 1

            FT.CHARGE.CODES = ""
            TOT.CHARGE.LCY = "0"
            TOT.CHARGE.FCY = "0"
            TOTAL.CHARGE = "0"
*-----------------------------------------------------------------------------------------------------------------------
            IF (PROCESSING.CODE[1,2] = 17 OR PROCESSING.CODE[1,2] = 12) AND WS.ADD.DATA[3] EQ 'Int' THEN

                FT.CHARGE.CODES = "ATMSCBPOSI"
                CALL CALCULATE.CHARGE(CUST.ID,DEAL.AMT,DEAL.CCY,CCY.MKT,"","","",FT.CHARGE.CODES,"",TOT.CHARGE.LCY,TOT.CHARGE.FCY)
                TOTAL.CHARGE = TOT.CHARGE.LCY
            END
            IF (PROCESSING.CODE[1,2] = 17 OR PROCESSING.CODE[1,2] = 12) AND WS.ADD.DATA[3] NE 'Int' THEN

                FT.CHARGE.CODES = "ATMSCBPOSL"
                CALL CALCULATE.CHARGE(CUST.ID,DEAL.AMT,DEAL.CCY,CCY.MKT,"","","",FT.CHARGE.CODES,"",TOT.CHARGE.LCY,TOT.CHARGE.FCY)
                TOTAL.CHARGE = TOT.CHARGE.LCY
            END
*-----------------------------------------------------------------------------------------------------------------------
            IF WS.ADD.DATA[3] EQ 'Int' THEN
                WS.PER        = 1.5 / 100
                WS.AMOUNT.PER = WS.AMOUNT * WS.PER
                WS.AMOUNT.PER = DROUND(WS.AMOUNT.PER,'2')
                R.NEW(AC.LCK.LOCKED.AMOUNT) = WS.AMOUNT + WS.AMOUNT.PER + TOTAL.CHARGE
            END ELSE
                R.NEW(AC.LCK.LOCKED.AMOUNT) = WS.AMOUNT + TOTAL.CHARGE
            END

        END
    END
