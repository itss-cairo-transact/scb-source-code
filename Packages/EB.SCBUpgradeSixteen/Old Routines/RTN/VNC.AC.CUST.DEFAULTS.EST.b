* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>181</Rating>
*-----------------------------------------------------------------------------
** ----- 17.06.2002 (MRA GROUP) -----

    SUBROUTINE VNC.AC.CUST.DEFAULTS.EST

*ROUTINE 1- TO CHECK THAT THIS RECORD IS RETREIVED FROM SAME VERSION IT IS INPUTTED FROM,ALSO
*           TO CHECK FOR EXISTING ACCOUNTS THAT BELONG TO THE SAME BRANCH AND THAT IT IS NOT
*           A CLOSURE ACCOUNT.                                                         ...MOHAMED...
*
*        2- TO CHECK THAT THE ID HAVE NUMERIC CHARACTERS ONLY.                        ... RANIA ...
*        3- TO DEFAULT LIMIT.REF & LINK TO LIMIT ACCORDING TO DEBIT ACCOUNTS.         ... RANIA ...
*        4- TO DEFAULT DEBIT.CREDIT WITH CREDIT.                                      ... ABEER ...
*        5- TO DEFAULT THE PASSBOOK FIELD TO 'YES' WITH ACCOUNT TYPE = SAVING.        ... ABEER ...


*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*----- (1)
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'A' THEN

        VERSION.NAME =  R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> ; ETEXT = ''
        CALL DBR( 'ACCOUNT$NAU':@FM:AC.LOCAL.REF, ID.NEW, LOCAL.REF)
        VER.NAME = LOCAL.REF<1, ACLR.VERSION.NAME>
*CALL DBR( 'ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ID.NEW, ACC.OFF)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ID.NEW,AC.COMP)
        ACC.OFF  = AC.COMP[8,2]

        IF NOT(ETEXT) THEN
            ACCT.OFF1 = R.NEW(AC.CO.CODE)
            ACCT.OFF2  = ACCT.OFF1[8,2]
            ACCT.OFF3 = TRIM(ACCT.OFF2, "0" , "L")
*IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
            IF ACCT.OFF3 AND ACCT.OFF3 NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
*   E='Only.Function.See.Is.Allowed'
                E='��� ����� ��������';CALL ERR;MESSAGE='REPEAT'
                V$FUNCTION = 'S'
            END
        END
*          IF NOT(ETEXT) AND VERSION.NAME # PGM.VERSION THEN
*          E = 'You.Must.Retreive.Record.From &' : @FM : VERSION.NAME
*          GOSUB MYERROR
*          END

** IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
**         E = 'This.Account.From.Other.Branch' ; GOSUB MYERROR
**     END

        IF  R.NEW(AC.POSTING.RESTRICT) GE '90' THEN
            E = 'This.Is.Closure.Account'
            E = '��� ������ ����' ; GOSUB MYERROR
        END

    END
*********************Nessreen 9/3/2005***********************
    ACC.EXIST = ''
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ID.NEW,ACC.EXIST)
    IF ACC.EXIST THEN TEXT = '�� ��� ���� ���� �� ���' ; CALL REM

*************************************************************
*------------------------
    IF V$FUNCTION = 'I' THEN

        CATEGORY = ID.NEW [11,4]

*------(2)
* E = 'Bad.Account.Number'
        IF NOT(NUM(ID.NEW)) THEN E = '��� �� ��� ������' ; GOSUB MYERROR

*----- (3)
***************
*IF  CATEGORY EQ 1001 THEN
        IF PGM.VERSION EQ ",SCB.CUSTAC.NEW1.EST" OR PGM.VERSION EQ ",SCB.CUSTAC.NEW1.CONV"THEN
        R.NEW (AC.LIMIT.REF) = ''
        R.NEW (AC.LINK.TO.LIMIT) = 'NO'

    END ELSE

*E= 'CATEGORY NOT EQ 1001';CALL ERR;MESSAGE='REPEAT'
        E= 'ERROR IN VERSION NAME';CALL ERR;MESSAGE='REPEAT'

    END

*----- (4)

    IF NOT (R.NEW( AC.LOCAL.REF)< 1, ACLR.DEBIT.CREDIT>) THEN
        R.NEW( AC.LOCAL.REF)< 1, ACLR.DEBIT.CREDIT> = 'C'
    END

*----- (5)
****************13-12-2004
*IF R.NEW(AC.CATEGORY) = 1200 THEN R.NEW(AC.PASSBOOK) = 'YES'
*ELSE  R.NEW(AC.PASSBOOK) = 'NO'

*-----

END

RETURN

MYERROR:
CALL ERR ; MESSAGE = 'REPEAT'

RETURN


*-----
END
