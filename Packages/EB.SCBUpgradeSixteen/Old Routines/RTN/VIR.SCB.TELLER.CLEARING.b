* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-40</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SCB.TELLER.CLEARING

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BRRR = '' ; ETEXT = ''
    T.SEL = '' ; KEY.LIST = '' ; SELECTED = ''
    CALL OPF( FN.BR,F.BR)

**  IF V$FUNCTION = 'A' AND  R.NEW(TT.TE.RECORD.STATUS)='INAU' THEN
    IF V$FUNCTION = 'I' THEN
        IF R.NEW(TT.TE.NARRATIVE.1)<1,1,1>[1,2] EQ "BR" THEN
            CALL F.READ( FN.BR,R.NEW(TT.TE.NARRATIVE.1)<1,1,1>, R.BRRR, F.BR, ETEXT)
            BR.ID = R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
        END ELSE
            T.SEL = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
            CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
            CALL F.READ( FN.BR,KEY.LIST, R.BRRR, F.BR, ETEXT)
            BR.ID = KEY.LIST
        END
        IF NOT(ETEXT) THEN
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>     = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.FT.REFERENCE>  = ID.NEW
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  = 8
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>   = TODAY
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>     = TODAY
        END

*WRITE R.BRRR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD ":BR.ID :" TO ":FN.BR
*END
        CALL F.WRITE (FN.BR,BR.ID,R.BRRR)
    END
** ---------------------------------------------------------------  **
**                           REV                                    **
** ---------------------------------------------------------------  **

**  IF V$FUNCTION = 'A' AND  R.NEW(TT.TE.RECORD.STATUS)='RNAU' THEN
    IF V$FUNCTION = 'R' THEN
        IF R.NEW(TT.TE.NARRATIVE.1)<1,1,1>[1,2] EQ "BR" THEN
            CALL F.READ( FN.BR,R.NEW(TT.TE.NARRATIVE.1)<1,1,1>, R.BRRR, F.BR, ETEXT)
            BR.ID = R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
        END ELSE
            T.SEL = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
            CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
            CALL F.READ( FN.BR,KEY.LIST, R.BRRR, F.BR, ETEXT)
            BR.ID = KEY.LIST
        END
        IF NOT(ETEXT) THEN
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.FT.REFERENCE>  = ID.NEW
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>   = TODAY

            MAT.EX = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
            PLACE  = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
            HH     = "+":PLACE:"W"
            CALL CDT('',MAT.EX,HH)

            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>     = MAT.EX
        END

*WRITE R.BRRR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD ": BR.ID :" TO ":FN.BR
*END
        CALL F.WRITE (FN.BR,BR.ID,R.BRRR)
    END
    RETURN

END
