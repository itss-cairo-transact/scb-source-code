* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
** ----- 13.10.2004 NESSREEN SCB -----
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SCB.AC.CUST.POST.RES

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*TO INPUT RESTERCTION IN THE CUSTOMER RECORD IF OPEN THE ACCOUNT WITHOUT PAY THE EXPENSES

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
    CUST = R.NEW(AC.CUSTOMER)
    CALL DBR( 'CUSTOMER.ACCOUNT':@FM:EB.CAC.ACCOUNT.NUMBER,CUST,ACCTNO)
    IF NOT(ACCTNO) THEN
        CALL OPF( FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ( FN.CUSTOMER,CUST, R.CUSTOMER, F.CUSTOMER, ETEXT)
        LC = R.CUSTOMER<EB.CUS.LOCAL.REF>
        VERNAME = LC<1,CULR.VERSION.NAME>
        IF VERNAME = ',SCB.PRIVATE' OR VERNAME = ',SCB.CORPORATE' THEN
            R.CUSTOMER<EB.CUS.POSTING.RESTRICT> = '11'
            CALL F.WRITE(FN.CUSTOMER,CUST,R.CUSTOMER)
        END
    END
    RETURN
END
