* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>389</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.CREDIT.CUST.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.USAGES.TOT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* WRITTEN BY NESSREEN AHMED-SCB
* TO DEBIT THE TOTAL USAGES FROM CUSTOMERS VISA ACCOUNT AND PRINTOUT A REPORT CONTANING
* THE CUSTOMERS AND TOTAL AMOUNT TO BE DEBITED

    TEXT = 'YARAB' ; CALL REM

    GOSUB INITIALISE
*Line [ 55 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
    RETURN
*==============================================================
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.ACC= 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS = "SCB.VISA"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
***NN***    COMP = C$ID.COMPANY
********    COM.CODE = COMP[8,2]
**    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
***UPDATED BY NESSREEN AHMED 2/4/2009****************
****nnn***    OFS.USER.INFO = "CAIRO22/1234567"
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*==============================================================
CALLDB:
    F.VISA.USAGES.TOT = '' ; FN.VISA.USAGES.TOT = 'F.SCB.VISA.USAGES.TOT' ; R.VISA.USAGES.TOT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.USAGES.TOT,F.VISA.USAGES.TOT)
*************UPDATED IN 31/1/2008*****
**    YTEXT = "Enter the Start Date Of Month : "
**    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    BR = R.USER<EB.USE.DEPARTMENT.CODE>

    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YY = YYYY-1
    END ELSE
        MU = MT-1
        YY = YYYY
    END
*****************************************
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END

******************************************
    YYDD = YY:MON:'01'

**T.SEL = "SELECT F.SCB.VISA.USAGES.TOT WITH POS.DATE GE ":COMI:" AND TOT.AMT.CR NE '' BY @ID "
*****************UPDATED 2008/12/03****************************************
** T.SEL = "SELECT F.SCB.VISA.USAGES.TOT WITH POS.DATE GE ":YYDD:" AND BRANCH.NUMBER EQ ":BR :" AND TOT.AMT.CR NE '' BY @ID "
    T.SEL = "SELECT F.SCB.VISA.USAGES.TOT WITH POS.DATE GE ":YYDD:" AND TOT.AMT.CR NE '' BY @ID "
*****************************************************
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            VISA.NO = '' ; ACCT.NO = '' ; CUST.NO = '' ; CUST.NAME = '' ; DEP.CODE = '' ; TOT.ALL = ''
            CALL F.READ(FN.VISA.USAGES.TOT,KEY.LIST<I>,R.VISA.USAGES.TOT,F.VISA.USAGES.TOT,E2)
            DEP.CODE=R.VISA.USAGES.TOT<USAGES.TOT.BRANCH.NUMBER>
            VISA.NO = KEY.LIST<I>[1,16]
            ACCT.NO<I> = R.VISA.USAGES.TOT<USAGES.TOT.ACCT.NO>
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<I>,CUST.NO)
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
            CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
            TOT.ALL.N = R.VISA.USAGES.TOT<USAGES.TOT.TOT.AMT.CR>
            TOT.ALL = ABS(TOT.ALL.N)
            IF LEN(DEP.CODE) < 2 THEN
                BR = '0':DEP.CODE
            END ELSE
                BR = DEP.CODE
            END
*CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO<I>,ACC.CURR)
            ACC.CURR = ACCT.NO<I>[9,2]
*******UPDATED ON 16/10/2008************************
**       BR.ACC = '994999':BR:ACC.CURR:'5010':'01'
******UPDATED BY NESSREEN AHMED ON 04/05/2010*****************
***            BR.ACC = '99499900':ACC.CURR:'5010':'01'
****UPDATED BY NESSREEN AHMED 8/6/2011******************************
****            BR.ACC = 'EGP1121400020099'
            BR.ACC = 'EGP1124200010099'
****END OF UPDATE 2/5/2011******************************************
*****************************************************
            COMMA = ","
            OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":'ACVU':COMMA
******UPDATED ON 16/10/2008*****************************************
***         OFS.MESSAGE.DATA := "DEBIT.CUSTOMER=":'99499901':COMMA
********************************************************************
            OFS.MESSAGE.DATA := "DEBIT.CUSTOMER=":'99499900':COMMA
            OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": 'EGP':COMMA
            OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":BR.ACC :COMMA
            OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TOT.ALL:COMMA
            OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
            OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": 'EGP':COMMA
            OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":ACCT.NO<I>:COMMA
            OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
            OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
            OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE':COMMA
************UPDATED BY NESSREN 02/06/2010*************************
            OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB'
************END OF UPDATE 02/06/2010*************************
************UPDATED BY NESSREEN 03/05/2009***************************
            CALL F.READ(FN.ACC,ACCT.NO<I>,R.ACC,F.ACC,E11)
            COMP = R.ACC<AC.CO.CODE>
            COM.CODE = COMP[8,2]
            OFS.USER.INFO = "VISAMAST//":COMP

**********************************************************************
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
**3/5/2009  OFS.ID = "T":TNO:".":OPERATOR:"_VISA":RND(10000):".":TODAY
            OFS.ID = "T":TNO:"_VISA":RND(10000):".":"CR":I:".":ACCT.NO<I>:".":TODAY:"-":COMP
            WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

**************************************************************************
        NEXT I
    END
    RETURN
END
