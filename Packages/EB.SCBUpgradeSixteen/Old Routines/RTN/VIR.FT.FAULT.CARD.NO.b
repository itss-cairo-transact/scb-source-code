* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
******NESSREEN-SCB 20/1/2014************
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.FT.FAULT.CARD.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS


    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.VISA.APP = '' ; RETRY1 = '' ; E1 = ''
    ACC.NO = '' ; CATEG = '' ; CARD.NO = '' ; CARD.NO.M = ''
    ACC.NO = R.NEW(FT.CREDIT.ACCT.NO)

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CATEG)
    IF (CATEG EQ 1205) OR (CATEG EQ 1206) THEN
        T.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE VI... AND CANCELLATION.DATE EQ '' AND CARD.CODE IN (101 102 104 105 201 202 204 205 ) AND ACCOUNT EQ  ": ACC.NO
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF NOT(SELECTED) THEN
            ETEXT = ',RESPONS.CODE=D58,' ;  CALL STORE.END.ERROR
        END ELSE
            IF SELECTED GT '1' THEN
                ETEXT = ',RESPONS.CODE=D59,' ;  CALL STORE.END.ERROR
            END ELSE
                CALL F.READ(FN.CARD.ISSUE, KEY.LIST<1>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                CARD.NO<1> = LOCAL.REF<1,LRCI.VISA.NO>
                R.NEW(FT.TRANSACTION.TYPE) = 'ACVC'
                R.NEW(FT.LOCAL.REF)<1,FTLR.VISA.NO> = CARD.NO<1>
                CALL REBUILD.SCREEN
            END
        END
    END ELSE
        IF (CATEG EQ 1207) OR (CATEG EQ 1208) THEN
**
*****UPDATED BY NESSREEN AHMED 9/10/2019*****************************
*****M.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE MA... AND CANCELLATION.DATE EQ '' AND CARD.CODE IN (501 502 504 505 601 602 604 605 ) AND ACCOUNT EQ  ": ACC.NO
     M.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE MA... AND CANCELLATION.DATE EQ '' AND CARD.CODE IN (501 502 504 505 601 602 604 605 701 702 704 705 1001 1004) AND ACCOUNT EQ  ": ACC.NO
            KEY.LIST.M=""
            SELECTED.M=""
            ER.MSG.M=""
            CALL EB.READLIST(M.SEL,KEY.LIST.M,"",SELECTED.M,ER.MSG.M)

            IF NOT(SELECTED.M) THEN
                ETEXT = ',RESPONS.CODE=D58,' ;  CALL STORE.END.ERROR
            END ELSE
                IF SELECTED.M GT 1 THEN
                    ETEXT = ',RESPONS.CODE=D59,' ;  CALL STORE.END.ERROR
                END ELSE
                    CALL F.READ(FN.CARD.ISSUE, KEY.LIST.M<1>, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                    LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                    CARD.NO.M<1> = LOCAL.REF<1,LRCI.VISA.NO>
                    R.NEW(FT.TRANSACTION.TYPE) = 'ACVM'
                    R.NEW(FT.LOCAL.REF)<1,FTLR.VISA.NO> = CARD.NO.M<1>
                    CALL REBUILD.SCREEN
                END
            END
**
        END
    END
    RETURN
END
