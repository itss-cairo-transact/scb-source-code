* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>206</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.COMP.FILE.PAY.2
**WRITTEN BY NESSREEN AHMED 09/08/2011

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.MNTLY.PAY.AMT
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.DAILY.PAYMENT

*A REPORT TO MAKE A COMPARISION BETWEEN "SCB.VISA.MNTLY.PAY.AMT" AND SCB.VISA.DAILY.PAYMENT

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 57 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    YYYYMM = ''
    REQ.COMP = ''

    YTEXT = "Enter the Date YYYYMM: "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    YYYYMM = COMI
    RETURN
*===============================================================
CALLDB:

    FN.MNT.PAY = 'F.SCB.VISA.MNTLY.PAY.AMT' ; F.MNT.PAY = '' ; R.MNT.PAY = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.MNT.PAY,F.MNT.PAY)

    FN.DLY.PAY = 'F.SCB.VISA.DAILY.PAYMENT' ; F.DLY.PAY = '' ; R.DLY.PAY = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.DLY.PAY,F.DLY.PAY)

    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY3 = '' ; E3 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

**  REQ.COMP = "EG0010013"
**    YTEXT = "Enter the Company : "
**    CALL TXTINP(YTEXT, 8, 22, "12", "A")

**    REQ.COMP = COMI

    T.SEL = "SELECT F.SCB.VISA.MNTLY.PAY.AMT WITH YEAR.MONTH EQ ":YYYYMM :" AND COMPANY.CO EQ ": REQ.COMP : " BY VISA.NO "
***************************************************************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    TEXT = '��� �� ��� ������=':SELECTED ; CALL REM
    XX= '' ; ZZ = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            VISA.NO = '' ; DB.ACCT = '' ; CR.ACCT = '' ; DB.AMT = '' ; CUST = '' ; CUST.NAM = ''
            REF.NO  = '' ; DB.ACCT.N = '' ; DB.AMT.N = ''

            KEY.TO.USE = KEY.LIST<I>

            CALL OPF(FN.MNT.PAY,F.MNT.PAY)
            CALL F.READ(FN.MNT.PAY, KEY.TO.USE, R.MNT.PAY, F.MNT.PAY, E1)

            VISA.NO  = R.MNT.PAY<MPA.VISA.NO>
            DB.ACCT  = R.MNT.PAY<MPA.DB.ACCT.NO>
            CR.ACCT  = R.MNT.PAY<MPA.CR.ACCT.NO>
            DB.AMT   = R.MNT.PAY<MPA.DEBIT.AMT>

            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER,DB.ACCT, CUST)
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)
            CUST.NAM = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>

            N.SEL = "SELECT F.SCB.VISA.DAILY.PAYMENT WITH (TRANS.DATE GE 20110919 AND TRANS.DATE LE 20110921) AND TRANS.TYPE NE SETT AND CARD.NO EQ ": VISA.NO : " AND AMOUNT EQ " : DB.AMT
            KEY.LIST.N=""
            SELECTED.N=""
            ER.MSG.N=""

            CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
            IF NOT(SELECTED.N) THEN
                N1 = VISA.NO[1,6]
                N2 = VISA.NO[13,4]
                VISA.NO.MSK = N1:"XXXXXX":N2

                XX<1,ZZ>[1,16] = VISA.NO.MSK
                XX<1,ZZ>[20,35]= CUST.NAM
                XX<1,ZZ>[60,16]= DB.ACCT
                XX<1,ZZ>[85,16]= CR.ACCT
                XX<1,ZZ>[110,8]= DB.AMT
                XX<1,ZZ>[125,3]= ")R("

                PRINT XX<1,ZZ>
                ZZ=ZZ+1
                XX = ''

                A.SEL = "SELECT F.SCB.VISA.DAILY.PAYMENT WITH (TRANS.DATE GE 20110919 AND TRANS.DATE LE 20110921) AND TRANS.TYPE NE SETT AND CARD.NO EQ ": VISA.NO
                KEY.LIST.A=""
                SELECTED.A=""
                ER.MSG.A=""

                CALL EB.READLIST(A.SEL,KEY.LIST.A,"",SELECTED.A,ER.MSG.A)
                FOR AA = 1 TO SELECTED.A
                    KEY.TO.USE.A = KEY.LIST.A<AA>
                    CALL OPF(FN.DLY.PAY,F.DLY.PAY)
                    CALL F.READ(FN.DLY.PAY, KEY.TO.USE.A, R.DLY.PAY, F.DLY.PAY, E2)

                    REF.NO      = R.DLY.PAY<SCB.PYM.REFER.NO>
                    CR.ACCT.N   = R.DLY.PAY<SCB.PYM.CUST.ACCT>
                    DB.AMT.N    = R.DLY.PAY<SCB.PYM.AMOUNT>

                    XX<1,ZZ>[60,16]= REF.NO
                    XX<1,ZZ>[85,16]=  CR.ACCT.N
                    XX<1,ZZ>[110,8]= DB.AMT.N
                    XX<1,ZZ>[125,3]= ")P("

                    PRINT XX<1,ZZ>
                    ZZ=ZZ+1
                    XX = ''

                NEXT AA
                PRINT STR('-',132)
            END
        NEXT I
    END
    PRINT
    PRINT
    PRINT STR('*',132)
    PRINT ")R(": "= ������ ������� � ������� ����� ���� ��� ����� ������ �� ������ "
** PRINT ")P(": "= ������ ������ ���� �� ������ �� ������ �� 19/7/2011 ��� 21/7/2011 �� ��� �����"
    PRINT ")P(": "= ������ ������ ���� �� ������ �� ������ �� 19/9/2011 ��� 21/9/2011 �� ��� �����"
    PRINT STR('*',132)
**********************************************************
    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
**  YYBRN = FIELD(BRANCH,'.',2)
***  YYBRN = "�������"
***  YYBRN = "����� ����"
    YTEXT = "Enter the Company : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    REQ.COMP = COMI

    COMP.NAME = ''
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,REQ.COMP,BR.NAME)
*** CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TEXT = 'COMP.NAME=':BR.NAME ; CALL REM
    YYBRN = BR.NAME

    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
**   TR.DATY = COMI
**   T.TR.DAY = TR.DATY[7,2]:'/':TR.DATY[5,2]:"/":TR.DATY[1,4]

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN

    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
**  PR.HD :="'L'":SPACE(30):" ���� ����� ��� ��� ��������� ������ �� ������ ������ 19/7/2011 �����"
    PR.HD :="'L'":SPACE(30):" ���� ����� ��� ��� ��������� ������ �� ����� ������ 19/9/2011 �����"
**  PR.HD :="'L'":SPACE(35):"� ��� �� ��� ������ �� ��� ����� �� ������ �� 19/7/2011 ��� 21/7/2011"
    PR.HD :="'L'":SPACE(35):"� ��� �� ��� ������ �� ��� ����� �� ������ �� 19/9/2011 ��� 21/9/2011"
    PR.HD :="'L'":SPACE(28):STR('_',80)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ���������"::SPACE(10):"����� ��������":SPACE(22):"�������� �����":SPACE(10):"������� �������":SPACE(10):"���� ������":SPACE(1):")R("
    PR.HD :="'L'":SPACE(60):"��� �������":SPACE(38):"���� ������":SPACE(1):")P("
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(9):STR('_',14):SPACE(20):STR('_',15):SPACE(11):STR('_',12):SPACE(8):STR('_',10)
    HEADING PR.HD
    RETURN
*==============================================================

END
