* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>388</Rating>
*-----------------------------------------------------------------------------
** ----- 17.06.2002 MOHAMED MAHMOUD (MRA GROUP) -----

    SUBROUTINE VNC.AC.HOLD.CHECK
*A ROUTINE SO AS 1- TO CHECK THAT THE MASK IS HOLD'S MASK.
*                2- NOT ACCEPT NEW HOLD FOR NOT AUTHORIZED RECORDS EXCEPT FOR USERS FROM SAME DEPARTMENT.
*                3- PREVENT USER FROM DIFFERENT DEPARTMENTS TO AUTHORISE THIS RECORD.
*                4- ADD THE ACCOUNT.NO IN THE SCB.ACCT.HOLDS(EXPIRE.DATE) FILE .

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.HOLD
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

*------ TO CHECK THAT THE MASK IS THE HOLD ACCOUNTS MASK  ----- (1)

    IF NOT(NUM(ID.NEW)) THEN
        E = 'Bad.Account.Number' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        ID.CAT=ID.NEW[11,4]
        IF ID.CAT GE 5000 AND  ID.CAT LE 5999 THEN
           * E = 'Not.Allowed.For.Nostro.Account'
           E = '��� ����� ������� ��������' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END

    CALL DBR("ACCOUNT":@FM:1,ID.NEW,MNE)
    *   E="ACCOUNT.DOS.NOT.EXIST"
    IF NOT(MNE) THEN E="������ ��� �����"; CALL ERR; MESSAGE ='REPEAT'
*------ TO GET DEPT.CODE & DEPT.NAME FOR THE LAST INPUTTER ------

    USER.ID = ''  ; DEPT.CODE = ''   ; DEPT.NAME = ''
*Line [ 60 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    INPUTTER.NO = DCOUNT(R.NEW( AC.INPUTTER), @SM)
    USER.ID = R.NEW(AC.INPUTTER)<1, INPUTTER.NO>
    USER.ID = FIELD(USER.ID,'_',2,1)
    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,USER.ID,DEPT.CODE)
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.CODE,DEPT.NAME)

*-------  TO CHECK THAT OTHER DEPT'S CAN'T INPUT NEW HOLD BEFORE AUTHORISE THE LAST HOLD  ------- (2)

    IF V$FUNCTION ='I' THEN

* IF NOT(R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME>) THEN
* R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> = PGM.VERSION
* END



        VERSION.NAME =  R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> ; ETEXT = ''
        CALL DBR( 'ACCOUNT$NAU':@FM:AC.LOCAL.REF<1, ACLR.VERSION.NAME>, ID.NEW, VER.NAME)
        IF NOT(ETEXT) THEN
            IF VERSION.NAME # PGM.VERSION THEN
              * E = 'You.Must.Retreive.Record.From &'
                E = '���� �� ������� ��� ����� ��&' : @FM : VERSION.NAME
                CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                IF DEPT.CODE NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
                    E = 'This.Account.NAU.OR.HLD.From &'
                    E = '��� ������ ��� ���� �� ���� ��&' : @FM : DEPT.NAME
                    CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END
        R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> = PGM.VERSION

    END ELSE

*------  AUTHORISE TO CHECK THAT THE AUTHORISER MUST BE IN THE SAME DEPT.  ------- (3)

        IF V$FUNCTION ='A' THEN

            VERSION.NAME =  R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> ; ETEXT = ''
            CALL DBR( 'ACCOUNT$NAU':@FM:AC.LOCAL.REF<1, ACLR.VERSION.NAME>, ID.NEW, VER.NAME)
            IF NOT(ETEXT) THEN
                IF VERSION.NAME # PGM.VERSION THEN
                 * E = 'You.Must.Retreive.Record.From &'
                    E = '���� �� ������� ��� ����� ��&' : @FM : VERSION.NAME
                    CALL ERR ; MESSAGE = 'REPEAT'
                END ELSE
                    IF DEPT.CODE NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
                     * E = 'This.Account.NAU.OR.HLD.From  &'
                        E = '��� ������ ��� ���� �� ���� ��&' : DEPT.NAME
                        CALL ERR ; MESSAGE = 'REPEAT'
                    END
                END
            END

*------ ADD THE ACCOUNT.NO IN THE EXPIRY.DATE FILE -------- (4)

            ACCTS = ''  ;  J = '' ; I = '' ; XX = ''
            F.COUNT = '' ; FN.COUNT = 'F.SCB.ACCT.HOLD'
            CALL OPF(FN.COUNT,F.COUNT)
*Line [ 121 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(R.NEW( AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE>,@SM)
                EXPIRY = R.NEW (AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE,I>
                CALL DBR('SCB.ACCT.HOLD':@FM:SCB.ACCOUNT.NO,EXPIRY,ACCTS)
                IF NOT(ETEXT) THEN
                    LOCATE ID.NEW IN ACCTS<1,1> SETTING J ELSE
*Line [ 127 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        XX = DCOUNT(ACCTS,@VM) + 1 ; GOSUB UPWR:
                    END
                END ELSE
                    XX = 1 ; GOSUB UPWR:
                END

            NEXT I

            CLOSE F.COUNT

*------

        END
    END

    GOTO FINAL:

*----- SUBROUTINE TO UPDATE/WRITE IN SCB.ACCT.HOLD FILE -----
UPWR:
    ACCTS<1,XX> = ID.NEW
    R.COUNT<SCB.ACCOUNT.NO> = ACCTS
    CALL F.WRITE(FN.COUNT,EXPIRY,R.COUNT)
    CALL JOURNAL.UPDATE(EXPIRY)
    RETURN
*------------------------------------------------------------

FINAL:

    RETURN
END
