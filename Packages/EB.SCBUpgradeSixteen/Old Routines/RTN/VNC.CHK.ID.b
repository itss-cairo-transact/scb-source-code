* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>345</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHK.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*-------------------------------------------
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    FN.TT='FBNK.TELLER';F.TT=''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H='FBNK.TELLER$HIS';F.TT.H=''
    CALL OPF(FN.TT.H,F.TT.H)


    FN.DR='FBNK.DRAWINGS';F.DR=''
    CALL OPF(FN.DR,F.DR)
    FN.DR.H='FBNK.DRAWINGS$HIS';F.DR.H=''
    CALL OPF(FN.DR.H,F.DR.H)
*---------------------------------
    ERROR.FLG = 0
    IF V$FUNCTION = 'I' THEN
        TEMP.ID = ID.NEW
        CUS.ID  = FIELD(TEMP.ID,'-',1)
        CUS.ID  = CUS.ID + 1 - 1
        ACC.ID  = FIELD(TEMP.ID,'-',1)

        CALL DBR ('CUSTOMER':@FM:EB.CUS.NAME.1,CUS.ID,NAME.CU)
        IF NOT(NAME.CU) THEN
            CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.ID,NAME.AC)
            IF NOT(NAME.AC) THEN
                ERROR.FLG = 1
            END
        END

        OP.ID = FIELD(TEMP.ID,'-',2)

        IF OP.ID[1,2] EQ 'FT' THEN
            CALL F.READ(FN.FT,OP.ID,R.FT,F.FT,E.FT)
            IF E.FT THEN
                FT.ID = OP.ID : ";1"
                CALL F.READ(FN.FT.HIS,FT.ID,R.FT.H,F.FT.HIS,E.FTH)
                IF E.FTH THEN
                    ERROR.FLG = 1
                END
            END
        END

        IF OP.ID[1,2] EQ 'TT' THEN
            CALL F.READ(FN.TT,OP.ID,R.TT,F.TT,E.TT)
            IF E.TT THEN
                TT.ID = OP.ID : ";1"
                CALL F.READ(FN.TT.H,TT.ID,R.TT.H,F.TT.H,E.TTH)
                IF E.TTH THEN
                    ERROR.FLG = 1
                END
            END
        END

        IF OP.ID[1,2] EQ 'TF' THEN
            CALL F.READ(FN.DR,OP.ID,R.DR,F.DR,E.DR)
            IF E.DR THEN
                YY = OP.ID : ";1"
                CALL F.READ(FN.DR.H,YY,R.DR.H,F.DR.H,E.DRH)
                IF E.DRH THEN
                    ERROR.FLG = 1
                END
            END
        END

        IF ERROR.FLG EQ 1 THEN
            E = 'INVALID ID.NEW RECORD'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*---------------------------------------------------
    RETURN
END
