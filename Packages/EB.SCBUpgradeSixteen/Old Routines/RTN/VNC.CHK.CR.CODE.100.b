* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
****CREATED BY NOHA
    SUBROUTINE VNC.CHK.CR.CODE.100

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CL.GROUPS
*---------------------------------------------------
    FN.CUS   = 'FBNK.CUSTOMER' ; F.CUS  = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

*---------------------------------------------------
    IF V$FUNCTION = 'I' THEN
        CUS.ID = COMI
        CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
        CR.CODE = R.CUS<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
* TEXT = CR.CODE; CALL REM
        IF CR.CODE NE 100 THEN
            E = "��� �� ���� ���� ������"
            CALL ERR
            MESSAGE = 'REPEAT'
            V$ERROR = 1
        END
    END
*---------------------------------------------------
    RETURN
END
