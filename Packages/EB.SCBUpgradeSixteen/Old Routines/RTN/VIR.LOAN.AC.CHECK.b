* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LOAN.AC.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC
*---------------------------------------------
    ETEXT   =''
    FN.LO   = 'F.SCB.LOAN.AC'    ; F.LO   = '' ; R.LO  = ''
    CALL OPF(FN.LO,F.LO)
    KEY.LIST= "" ; SELECTED = "" ; ER.MSG = ""

    REF     = R.NEW(FT.DEBIT.THEIR.REF)
    DB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DB.ACCT,DB.CUST)
    TEMP.ID = "...": DB.CUST : "..." : REF : "..."

    T.SEL = "SELECT F.SCB.LOAN.AC WITH @ID LIKE  ": TEMP.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED EQ 1 THEN
        ID.LOAN = KEY.LIST<1>
        CALL F.READ(FN.LO,ID.LOAN,R.LO,F.LO,READ.ERR)
        IF READ.ERR THEN
            E= "��� ������ ��� ����"  ; CALL ERR; MESSAGE='REPEAT'
            E = ''
        END ELSE
            IF R.LO<AC.LO.OUTSTAND.AMOUNT> LE '0' THEN
                E = '�� ������� �������'; CALL ERR; MESSAGE='REPEAT'
                E = ''
            END
        END
        CALL REBUILD.SCREEN
    END ELSE
        ETEXT = "CHECK THE REF "  ; CALL STORE.END.ERROR
        MESSAGE = 'REPEAT'
    END
*------------------------------------------------------
    RETURN
END
