* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*****WRITTEN BY NESSREEN AHMED 28/1/2010**************

    SUBROUTINE VISA.USAGES.DAYS

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS.TOT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.USG.DAYS

    F.VISA.DAYS = '' ; FN.VISA.DAYS = 'F.SCB.VISA.USG.DAYS' ; R.VISA.DAYS = '' ; E1 = ''
    CALL OPF(FN.VISA.DAYS,F.VISA.DAYS)

    F.VISA.TRANS.TOT = '' ; FN.VISA.TRANS.TOT = 'F.SCB.VISA.TRANS.TOT' ; R.VISA.TRANS.TOT = '' ; E2 = ''
    CALL OPF(FN.VISA.TRANS.TOT,F.VISA.TRANS.TOT)
******************************************
    YTEXT = "Enter the Start Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    YYDD = COMI

    T.SEL = "SELECT F.SCB.VISA.TRANS.TOT WITH POS.DATE GE ":YYDD :" BY BRANCH.NUMBER "

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED.VISA.TRANS=':SELECTED ; CALL REM
    IF SELECTED THEN          ;* SELECTED OF I*
        FOR I = 1 TO SELECTED
            KEY.TO.USE = ''; TRANS.CODE = ''
            TRANS.AMT = '' ; XX = '' ; BR.NO = ''

            CALL F.READ(FN.VISA.TRANS.TOT, KEY.LIST<I>, R.VISA.TRANS.TOT, F.VISA.TRANS.TOT, E2)

            TRANS.CODE = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 58 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DTC = DCOUNT(TRANS.CODE,@VM)
            FOR DD = 1 TO DTC
                TRANS.CODE = '' ; TRANS.AMT = '' ; TRANS.POS.DAT = ''
                TRANS.CODE = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE,DD>
                TRANS.AMT  = R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT,DD>
                TRANS.POS.DAT = R.VISA.TRANS.TOT<VISA.TRANS.POS.DATE,DD>

                CALL F.READ(FN.VISA.DAYS, TRANS.POS.DAT , R.VISA.DAYS, F.VISA.DAYS, E1)
                IF NOT(E1) THEN
                    BEGIN CASE
                    CASE TRANS.CODE = '1'
                        R.VISA.DAYS<USMD.AMT1> += TRANS.AMT
                    CASE TRANS.CODE = '2'
                        R.VISA.DAYS<USMD.AMT2> += TRANS.AMT
                    CASE TRANS.CODE = '3'
                        R.VISA.DAYS<USMD.AMT3> += TRANS.AMT
                    CASE TRANS.CODE = '4'
                        R.VISA.DAYS<USMD.AMT4> += TRANS.AMT
                    CASE TRANS.CODE = '5'
                        R.VISA.DAYS<USMD.AMT5> += TRANS.AMT
                    CASE TRANS.CODE = '6'
                        R.VISA.DAYS<USMD.AMT6> += TRANS.AMT
                    END CASE
                   CALL F.WRITE(FN.VISA.DAYS, TRANS.POS.DAT, R.VISA.DAYS)
                   CALL JOURNAL.UPDATE(TRANS.POS.DAT)

                END ELSE
                    BEGIN CASE
                    CASE TRANS.CODE = '1'
                        R.VISA.DAYS<USMD.AMT1> += TRANS.AMT
                    CASE TRANS.CODE = '2'
                        R.VISA.DAYS<USMD.AMT2> += TRANS.AMT
                    CASE TRANS.CODE = '3'
                        R.VISA.DAYS<USMD.AMT3> += TRANS.AMT
                    CASE TRANS.CODE = '4'
                        R.VISA.DAYS<USMD.AMT4> += TRANS.AMT
                    CASE TRANS.CODE = '5'
                        R.VISA.DAYS<USMD.AMT5> += TRANS.AMT
                    CASE TRANS.CODE = '6'
                        R.VISA.DAYS<USMD.AMT6> += TRANS.AMT
                    END CASE
                    CALL F.WRITE(FN.VISA.DAYS,TRANS.POS.DAT, R.VISA.DAYS)
                    CALL JOURNAL.UPDATE(TRANS.POS.DAT)
                END
            NEXT DD
        NEXT I
    END

    RETURN
END
