* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
    SUBROUTINE VIR.SCB.WH.REG

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*---------------------------------------------
*Line [ 31 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.ITEM = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)

    FOR I=1 TO DECOUNT.ITEM
        FN.WH.REG = 'F.SCB.WH.REGISTER' ; F.WH.REG = '' ; R.WH.REG = ''
        CALL OPF( FN.WH.REG,F.WH.REG)

        REG = FIELD(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>,'.',1)
        CALL F.READ( FN.WH.REG,REG, R.WH.REG, F.WH.REG, ETEXT)

        IF R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE) = 'DR' THEN
            R.WH.REG<SCB.WH.VALUE.BALANCE> = R.WH.REG<SCB.WH.VALUE.BALANCE> - R.NEW(SCB.WH.TRANS.TOTAL.BALANCE)<1,I>
        END

        IF R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE) = 'AD' THEN
            R.WH.REG<SCB.WH.VALUE.BALANCE> = R.WH.REG<SCB.WH.VALUE.BALANCE> + R.NEW(SCB.WH.TRANS.TOTAL.BALANCE)<1,I>
        END

        IF R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE) = 'MOD' THEN
            FN.ITEMS = "F.SCB.WH.ITEMS"   ; F.ITEMS = ""
            CALL OPF(FN.ITEMS,F.ITEMS)

            ITEMS.ID = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>
            CALL F.READ( FN.ITEMS,ITEMS.ID, R.ITEMS, F.ITEMS, ETEXT)

            R.WH.REG<SCB.WH.VALUE.BALANCE> = R.WH.REG<SCB.WH.VALUE.BALANCE> - R.ITEMS<SCB.WH.IT.VALUE.BALANCE>
            R.WH.REG<SCB.WH.VALUE.BALANCE> = R.WH.REG<SCB.WH.VALUE.BALANCE> + R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,I>
        END

        CALL F.WRITE(FN.WH.REG,REG<1,I>,R.WH.REG)
    NEXT I
*------------------------------------------------------------------
    RETURN
END
