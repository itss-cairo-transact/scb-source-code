* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1812</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.COMM.BR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.NEW
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.DAILY.TRN
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CURR.CODES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.ROUTINE.CHK

*****WRITTEN BY NESSREEN AHMED SCB*****
***************************************
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 56 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.COMM.BR'
    CALL PRINTER.ON(REPORT.ID,'')
    YTEXT = "Enter the Start Date Of Required Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    RETURN
*===============================================================
CALLDB:
    DATEE = '' ; YEAR1 = '' ; MONTH1= ''
    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    MT = TRIM(MM, "0" , "L")
    IF MT = '1' THEN
        MU = 12
        YY = YYYY-1
    END ELSE
        MU = MT-1
        YY = YYYY
    END
**************************************
    IF LEN(MU) < 2 THEN
        MON = '0':MU
    END ELSE
        MON = MU
    END
***************************************
    YYDD = YY:MON:'01'
    YEAR1= YY
    MONTH1 = MON
**    BR = R.USER<EB.USE.DEPARTMENT.CODE>
    BR = 1
    IF LEN(BR) < 2 THEN
        BRAN = '0':BR
    END ELSE
        BRAN = BR
    END

*****************************************************
    F.VISA.DAILY.TRN = '' ; FN.VISA.DAILY.TRN = 'F.SCB.VISA.DAILY.TRN' ; R.VISA.DAILY.TRN = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.VISA.DAILY.TRN,F.VISA.DAILY.TRN)
******************************************************************************
*****************����� ����� *************************************************
******************************************************************************
**  T.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ DBINT AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 193000 "
    T.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE EQ ":COMI :" AND ORG.MSG.TYPE EQ DBINT AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 193000 "
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    AMT.DBT = ''
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    TEXT = 'SELECTED=':SELECTED ; CALL REM

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST<I>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 121 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT(ORG.MSG,@VM)
            FOR TT = 1 TO DD
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT> = "DBINT" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT> = "193000" THEN
                    BILL.AMT = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT>
                    AMT.DBT = AMT.DBT + BILL.AMT
                END
            NEXT TT
        END
    NEXT I
*TEXT = 'AMT.DBT=':AMT.DBT ; CALL REM
    IF AMT.DBT = '' THEN AMT.DBT = "0.00"
*****************************************************************************
******************���� �������***********************************************
*****************************************************************************
**  I.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND (ORG.MSG.TYPE EQ SFEE OR ORG.MSG.TYPE EQ DBADJ) AND MSG.TYPE EQ 0220 AND (PROCESS.CODE EQ 190000 OR PROCESS.CODE EQ 020000 ) AND (REASON.CODE EQ JFEE OR REASON.CODE EQ jfee) "
    I.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE EQ ":COMI :" AND CARD.BR EQ ":BR:" AND (ORG.MSG.TYPE EQ SFEE OR ORG.MSG.TYPE EQ DBADJ) AND (MSG.TYPE EQ 0220 OR MSG.TYPE JFEE) AND (PROCESS.CODE EQ 190000 OR PROCESS.CODE EQ 020000) "

    KEY.LIST.I=""
    SELECTED.I=""
    ER.MSG.I=""

    AMT.ISSU = ''
    CALL EB.READLIST(I.SEL,KEY.LIST.I,"",SELECTED.I,ER.MSG.I)
*TEXT = 'SELECTED.I=':SELECTED.I ; CALL REM

    FOR ISS = 1 TO SELECTED.I
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.I<ISS>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG.I = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 151 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.I = DCOUNT(ORG.MSG.I,@VM)
            FOR TT.I = 1 TO DD.I
                ORG.MSG.TYPE.I = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.I>
                MSG.TYPE.I = R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.I>
                PROCESS.CODE.I = R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.I>
                REASON.CODE.I = R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.I>
                IF (ORG.MSG.TYPE.I= "SFEE" OR ORG.MSG.TYPE.I= "DBADJ") AND MSG.TYPE.I = "0220" AND (PROCESS.CODE.I = "190000" OR PROCESS.CODE.I = "020000") AND (REASON.CODE.I="JFEE" OR REASON.CODE.I="jfee") THEN
                    BILL.AMT.I = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.I>
                    AMT.ISSU = AMT.ISSU + BILL.AMT.I
                END
            NEXT TT.I
        END
    NEXT ISS
*TEXT = 'AMT.ISSU=':AMT.ISSU ; CALL REM
    IF AMT.ISSU = '' THEN AMT.ISSU = "0.00"
******************************************************************************
*****************���� ����� *************************************************
******************************************************************************
    RN.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ SFEE AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 190000 AND REASON.CODE EQ AFEE"

    KEY.LIST.RN=""
    SELECTED.RN=""
    ER.MSG.RN=""

    AMT.RN = ''
    CALL EB.READLIST(RN.SEL,KEY.LIST.RN,"",SELECTED.RN,ER.MSG.RN)
*    TEXT = 'SELECTED.RN=':SELECTED.RN ; CALL REM

    FOR RN = 1 TO SELECTED.RN
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.RN<RN>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
**  TEXT = 'KEY.RN=':KEY.LIST<RN> ; CALL REM
        IF NOT(E2) THEN
            ORG.MSG.RN = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 185 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.RN = DCOUNT(ORG.MSG.RN,@VM)
            FOR TT.RN = 1 TO DD.RN
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.RN> = "SFEE" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.RN> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.RN> = "190000" AND R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.RN> = "AFEE" THEN
                     BILL.AMT.RN = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.RN>
                    AMT.RN = AMT.RN + BILL.AMT.RN
                END
            NEXT TT.RN
        END
    NEXT RN
*TEXT = 'AMT.RN=':AMT.RN ; CALL REM
    IF  AMT.RN = '' THEN  AMT.RN = '0.00'
******************************************************************************
*****************���� ��� ���� ***********************************************
******************************************************************************
    RISS.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ SFEE AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 190000 AND REASON.CODE EQ RISU"

    KEY.LIST.RISS=""
    SELECTED.RISS=""
    ER.MSG.RISS=""

    AMT.RISS = ''
    CALL EB.READLIST(RISS.SEL,KEY.LIST.RISS,"",SELECTED.RISS,ER.MSG.RISS)
*TEXT = 'SELECTED.RISS=':SELECTED.RISS ; CALL REM

    FOR RISS = 1 TO SELECTED.RISS
        CALL F.READU(FN.VISA.DAILY.TRN,KEY.LIST.RISS<RISS>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2, RETRY2)
        IF NOT(E2) THEN
            ORG.MSG.RISS = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 214 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.RISS = DCOUNT(ORG.MSG.RISS,@VM)
            FOR TT.RISS = 1 TO DD.RISS
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.RISS> = "SFEE" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.RISS> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.RISS> = "190000" AND R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.RISS> = "RISU" THEN
                    BILL.AMT.RISS = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.RISS>
                    AMT.RISS = AMT.RISS + BILL.AMT.RISS
                END
            NEXT TT.RISS
        END
    NEXT RISS
*TEXT = 'AMT.RISS=':AMT.RISS ; CALL REM
    IF AMT.RISS = '' THEN AMT.RISS = '0.00'

******************************************************************************
*****************��� ���� ****************************************************
******************************************************************************
    RPRN.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ SFEE AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 190000 AND REASON.CODE EQ RFEE"

    KEY.LIST.RPRN=""
    SELECTED.RPRN=""
    ER.MSG.RPRN=""

    AMT.RPRN = ''
    CALL EB.READLIST(RPRN.SEL,KEY.LIST.RPRN,"",SELECTED.RPRN,ER.MSG.RPRN)
*TEXT = 'SELECTED.RPRN=':SELECTED.RPRN ; CALL REM

    FOR RPRN = 1 TO SELECTED.RPRN
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.RPRN<RPRN>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG.RPRN = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 244 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.RPRN = DCOUNT(ORG.MSG.RPRN,@VM)
            FOR TT.RPRN = 1 TO DD.RPRN
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.RPRN> = "SFEE" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.RPRN> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.RPRN> = "190000" AND R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.RPRN> = "RFEE" THEN
                    BILL.AMT.RPRN = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.RPRN>
                    AMT.RPRN = AMT.RPRN + BILL.AMT.RPRN
                END
            NEXT TT.RPRN
        END
    NEXT RPRN
*TEXT = 'AMT.RPRN=':AMT.RPRN ; CALL REM
    IF AMT.RPRN = '' THEN AMT.RPRN = "0.00"
******************************************************************************
*****************����� �����****************************************************
******************************************************************************
    OLMT.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ SFEE AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 193000 AND REASON.CODE EQ OLFE"

    KEY.LIST.OLMT=""
    SELECTED.OLMT=""
    ER.MSG.OLMT=""

    AMT.OLMT = ''
    CALL EB.READLIST(OLMT.SEL,KEY.LIST.OLMT,"",SELECTED.OLMT,ER.MSG.OLMT)
*TEXT = 'SELECTED.OLMT=':SELECTED.OLMT ; CALL REM

    FOR OLMT = 1 TO SELECTED.OLMT
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.OLMT<OLMT>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG.OLMT = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 273 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.OLMT = DCOUNT(ORG.MSG.OLMT,@VM)
            FOR TT.OLMT = 1 TO DD.OLMT
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.OLMT> = "SFEE" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.OLMT> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.OLMT> = "193000" AND R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.OLMT> = "OLFE" THEN
                    BILL.AMT.OLMT = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.OLMT>
                    AMT.OLMT = AMT.OLMT + BILL.AMT.OLMT
                END
            NEXT TT.OLMT
        END
    NEXT OLMT
*TEXT = 'AMT.OLMT=':AMT.OLMT ; CALL REM
    IF AMT.OLMT = '' THEN AMT.OLMT = '0.00'
******************************************************************************
*****************����� �����****************************************************
******************************************************************************
    LPAY.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ SFEE AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 193000 AND REASON.CODE EQ LFEE"

    KEY.LIST.LPAY=""
    SELECTED.LPAY=""
    ER.MSG.LPAY=""

    AMT.LPAY = ''
    CALL EB.READLIST(LPAY.SEL,KEY.LIST.LPAY,"",SELECTED.LPAY,ER.MSG.LPAY)
*TEXT = 'SELECTED.LPAY=':SELECTED.LPAY ; CALL REM

    FOR LPAY = 1 TO SELECTED.LPAY
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.LPAY<LPAY>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG.LPAY = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 302 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.LPAY = DCOUNT(ORG.MSG.LPAY,@VM)
            FOR TT.LPAY = 1 TO DD.LPAY
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.LPAY> = "SFEE" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.LPAY> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.LPAY> = "193000" AND R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.LPAY> = "LFEE" THEN
                    BILL.AMT.LPAY = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.LPAY>
                    AMT.LPAY = AMT.LPAY + BILL.AMT.LPAY
                END
            NEXT TT.LPAY
        END
    NEXT LPAY
*TEXT = 'AMT.LPAY=':AMT.LPAY ; CALL REM
    IF AMT.LPAY = '' THEN AMT.LPAY = '0.00'
******************************************************************************
*****************���� ���� �����****************************************************
******************************************************************************
    CRAD.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ CRADJ AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 220000 AND REASON.CODE EQ obal"

    KEY.LIST.CRAD=""
    SELECTED.CRAD=""
    ER.MSG.CRAD=""

    AMT.CRAD = ''
    CALL EB.READLIST(CRAD.SEL,KEY.LIST.CRAD,"",SELECTED.CRAD,ER.MSG.CRAD)
*TEXT = 'SELECTED.CRAD=':SELECTED.CRAD ; CALL REM

    FOR CRAD = 1 TO SELECTED.CRAD
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.CRAD<CRAD>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG.CRAD = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 331 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.CRAD = DCOUNT(ORG.MSG.CRAD,@VM)
            FOR TT.CRAD = 1 TO DD.CRAD
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.CRAD> = "CRADJ" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.LPAY> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.LPAY> = "220000" AND R.VISA.DAILY.TRN<SCB.DAILY.REASON.CODE,TT.LPAY> = "obal" THEN
                    BILL.AMT.CRAD = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.CRAD>
                    AMT.CRAD = AMT.CRAD + BILL.AMT.CRAD
                END
            NEXT TT.CRAD
        END
    NEXT CRAD
*TEXT = 'AMT.CRAD=':AMT.CRAD ; CALL REM
    IF AMT.CRAD = '' THEN AMT.CRAD = '0.00'
******************************************************************************
*****************���� ����� �����****************************************************
******************************************************************************
    REFN.SEL = "SELECT F.SCB.VISA.DAILY.TRN WITH POS.DATE GE ":YYDD :" AND CARD.BR EQ ":BR:" AND ORG.MSG.TYPE EQ CRADJ AND MSG.TYPE EQ 0220 AND PROCESS.CODE EQ 223000 AND DB.CR.FLAG EQ CR"

    KEY.LIST.REFN=""
    SELECTED.REFN=""
    ER.MSG.REFN=""

    AMT.REFN = ''
    CALL EB.READLIST(REFN.SEL,KEY.LIST.REFN,"",SELECTED.REFN,ER.MSG.REFN)
*TEXT = 'SELECTED.REFN=':SELECTED.REFN ; CALL REM

    FOR REFN = 1 TO SELECTED.REFN
        CALL F.READ(FN.VISA.DAILY.TRN,KEY.LIST.REFN<REFN>, R.VISA.DAILY.TRN, F.VISA.DAILY.TRN ,E2)
        IF NOT(E2) THEN
            ORG.MSG.REFN = R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE>
*Line [ 360 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD.REFN = DCOUNT(ORG.MSG.REFN,@VM)
            FOR TT.REFN = 1 TO DD.REFN
                IF R.VISA.DAILY.TRN<SCB.DAILY.ORG.MSG.TYPE,TT.REFN> = "CRADJ" AND R.VISA.DAILY.TRN<SCB.DAILY.MSG.TYPE,TT.REFN> = "0220" AND R.VISA.DAILY.TRN<SCB.DAILY.PROCESS.CODE,TT.REFN> = "223000" AND R.VISA.DAILY.TRN<SCB.DAILY.DB.CR.FLAG,TT.REFN> = "CR" THEN
                    BILL.AMT.REFN = R.VISA.DAILY.TRN<SCB.DAILY.BILL.AMT,TT.REFN>
                    AMT.REFN = AMT.REFN + BILL.AMT.REFN
                END
            NEXT TT.REFN
        END
    NEXT REFN
*TEXT = 'AMT.REFN=':AMT.REFN ; CALL REM
    IF AMT.REFN = '' THEN AMT.REFN = '0.00'

    XX = '' ; ZZ = ''
    XX<1,ZZ>[1,15]= '�������'
    XX<1,ZZ>[18,10]= AMT.ISSU
    XX<1,ZZ>[30,10]= AMT.RN
    XX<1,ZZ>[42,10]= AMT.RISS
    XX<1,ZZ>[54,10]= AMT.RPRN
    XX<1,ZZ>[66,10]= AMT.DBT
    XX<1,ZZ>[78,10]= AMT.OLMT
    XX<1,ZZ>[90,10]= AMT.LPAY
    XX<1,ZZ>[102,10]= AMT.CRAD
    XX<1,ZZ>[114,10]= AMT.REFN
    TOT.ALL= AMT.ISSU+AMT.RN+AMT.RISS+AMT.RPRN+AMT.DBT+AMT.OLMT+AMT.LPAY-AMT.CRAD-AMT.REFN
    XX<1,ZZ>[126,10]= TOT.ALL

    PRINT XX<1,ZZ>
    ZZ=ZZ+1
    RETURN
********************************************************************
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ������ �������� �������� ������ "
    PR.HD :="'L'":SPACE(38):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):STR('_',135)
    PR.HD :="'L'":SPACE(1):"������":SPACE(10):"����":SPACE(08):"����":SPACE(08):"���":SPACE(08):"���":SPACE(08):"�����" :SPACE(08):"�����" :SPACE(08):"�����":SPACE(08):"����":SPACE(08):"����":SPACE(08):"�������"
    PR.HD :="'L'":SPACE(16):"�������"  :SPACE(06):"�������":SPACE(06):"����" :SPACE(06):"����":SPACE(06):"�����":SPACE(09):"�����":SPACE(06):"�����":SPACE(06):"�.�����":SPACE(06):"�.�����"
    PR.HD :="'L'":SPACE(1):STR('_',135)
    HEADING PR.HD
    RETURN
*==============================================================

END
