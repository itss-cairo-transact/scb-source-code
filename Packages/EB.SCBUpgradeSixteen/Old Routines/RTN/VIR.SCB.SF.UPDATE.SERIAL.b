* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SCB.SF.UPDATE.SERIAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.AUTO.RESERVE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFE.SUBKEY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

* IF NOT(R.NEW(SCB.SAF.RIG.RESERVE.NO)) THEN
    FN.SCB.SF.AUTO.RESERVE = 'F.SCB.SF.AUTO.RESERVE' ; F.SCB.SF.AUTO.RESERVE = '' ; R.SCB.SF.AUTO.RESERVE = ''
    CALL OPF( FN.SCB.SF.AUTO.RESERVE,F.SCB.SF.AUTO.RESERVE)
    CALL F.READ( FN.SCB.SF.AUTO.RESERVE,R.USER< EB.USE.DEPARTMENT.CODE>, R.SCB.SF.AUTO.RESERVE, F.SCB.SF.AUTO.RESERVE, ETEXT)
    IF LEN(R.USER< EB.USE.DEPARTMENT.CODE>) = 1 THEN
        R.SCB.SF.AUTO.RESERVE<SCB.SF.RES.RESERVE.SERIAL.NO> =  R.NEW(SCB.SAF.RIG.RESERVE.NO)[2,LEN(R.USER< EB.USE.DEPARTMENT.CODE>)]
    END ELSE
        R.SCB.SF.AUTO.RESERVE<SCB.SF.RES.RESERVE.SERIAL.NO> =  R.NEW(SCB.SAF.RIG.RESERVE.NO)[3,LEN(R.USER< EB.USE.DEPARTMENT.CODE>)]
    END
    R.SCB.SF.AUTO.RESERVE<SCB.SF.RES.RESERVE.SERIAL.NO> = R.NEW(SCB.SAF.RIG.RESERVE.NO) + 1

    CALL F.WRITE(FN.SCB.SF.AUTO.RESERVE,R.USER< EB.USE.DEPARTMENT.CODE>,R.SCB.SF.AUTO.RESERVE)
**********************************************************************
    IDD = R.NEW(SCB.SAF.RIG.RESERVE.NO)
    FN.SCB.SAFE.SUBKEY = 'F.SCB.SAFE.SUBKEY' ; F.SCB.SAFE.SUBKEY = '' ; R.SCB.SAFE.SUBKEY = ''
    CALL OPF( FN.SCB.SAFE.SUBKEY,F.SCB.SAFE.SUBKEY)
    CALL F.READ( FN.SCB.SAFE.SUBKEY,R.USER< EB.USE.DEPARTMENT.CODE>:".":IDD,R.SCB.SAFE.SUBKEY, F.SCB.SAFE.SUBKEY, ETEXT)

    R.SCB.SAFE.SUBKEY<SCB.SUBKEY.SUB.KEY.NO> = R.NEW(SCB.SAF.RIG.RESERVE.NO)
    R.SCB.SAFE.SUBKEY<SCB.SUBKEY.ISSUE.DATE> = R.NEW(SCB.SAF.RIG.VALUE.DATE)
    R.SCB.SAFE.SUBKEY<SCB.SUBKEY.CUSTOMER.NO> = R.NEW(SCB.SAF.RIG.CUSTOMER.NO)


    CALL F.WRITE(FN.SCB.SAFE.SUBKEY,R.USER< EB.USE.DEPARTMENT.CODE>:".":IDD,R.SCB.SAFE.SUBKEY)
    RETURN
END
