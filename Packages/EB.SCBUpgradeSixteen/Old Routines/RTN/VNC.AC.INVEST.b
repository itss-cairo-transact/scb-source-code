* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
** ----- 5/6/2011 NOHA SCB -----
    SUBROUTINE VNC.AC.INVEST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.SERIAL
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        ACCT.NO = ID.NEW
        T.DATE = TODAY
        T.MONTH = T.DATE[5,2]
        IF T.MONTH EQ '01' OR T.MONTH EQ '04' OR T.MONTH EQ '07' OR T.MONTH EQ '10' THEN
            CALL ADD.MONTHS(T.DATE,'-1')
        END
        IF T.MONTH EQ '02' OR T.MONTH EQ '05' OR T.MONTH EQ '08' OR T.MONTH EQ '11' THEN
            CALL ADD.MONTHS(T.DATE,'-2')
        END
        IF T.MONTH EQ '03' OR T.MONTH EQ '06' OR T.MONTH EQ '09' OR T.MONTH EQ '12' THEN
            CALL ADD.MONTHS(T.DATE,'-3')
        END
        CORRECT.L.DATE = T.DATE
        CALL LAST.DAY(CORRECT.L.DATE)
        CALL GET.ENQ.BALANCE(ACCT.NO,CORRECT.L.DATE,OPEN.BAL)
        R.NEW(AC.LOCAL.REF)<1,ACLR.PREV.BAL>= OPEN.BAL
    END
    RETURN
END
**************************************************************************************************
