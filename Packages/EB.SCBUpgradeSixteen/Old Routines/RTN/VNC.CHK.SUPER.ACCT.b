* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
****NOHA HAMED 12/11/2014********
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHK.SUPER.ACCT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I'  THEN
        CUST.NO = ID.NEW[1,8]
        CAT.NO  = ID.NEW[11,4]
        CUST.ID = TRIM(CUST.NO, "0", "L")

        CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, CUST.ID, SEC)
        CALL DBR( 'CUSTOMER':@FM:EB.CUS.LOCAL.REF, CUST.ID, CUS.LOCAL.REF)
        PROFESSION = CUS.LOCAL.REF<1,CULR.PROFESSION>

        IF SEC NE 2000 THEN
            E='��� ����� ���� ���� ���� ����� �� �������';CALL ERR;MESSAGE='REPEAT'
        END
        IF CAT.NO NE 6505 AND CAT.NO NE 6506 AND CAT.NO NE 6507 AND CAT.NO NE 6508 AND CAT.NO NE 6513 THEN
            IF ID.COMPANY EQ "EG0010011" THEN
                E = '��� ������ ����� ������� ����� ������� ���';CALL ERR;MESSAGE='REPEAT'
            END ELSE
                E = '��� ������ ����� ������� ����� ������� ���';CALL ERR;MESSAGE='REPEAT'
            END
        END
        IF PROFESSION NE 42 THEN
            IF ID.COMPANY EQ "EG0010011" THEN
                E = '��� ����� ��� ������� ��� ���� �����';CALL ERR;MESSAGE='REPEAT'
            END ELSE
                E = '��� ����� ��� ������� ��� ���� �����';CALL ERR;MESSAGE='REPEAT'
            END
        END
        RETURN
    END
