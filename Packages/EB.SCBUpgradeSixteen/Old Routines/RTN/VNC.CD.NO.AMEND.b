* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CD.NO.AMEND

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


**********************
    CO.CODE=ID.COMPANY

*RECORD.STAT =  R.NEW(LD.RECORD.STATUS)
*CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
*MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
*REST      = 'Same.Version'

* IF NOT(ETEXT) THEN
*     IF MYVERNAME NE PGM.VERSION THEN
*    E = 'You.Must.Authorize.OR.Delete.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
* END ELSE
    IF V$FUNCTION = 'I' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
*        IF NOT(ETEXT) THEN
        IF MYLOCAL NE '' THEN
            E = 'You.Must.Authorize.OR.Delete.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END

*    IF  RECORD.STAT EQ 'INAU' THEN
*       E = 'Cant.Amend.NAU.Record': REST; CALL ERR ;MESSAGE = 'REPEAT'
*  END  ELSE
    R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
* END
*END
*END


    RETURN
END
