* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
************ Writen by Bakry 2011/06/30 ************************
*-----------------------------------------------------------------------------
* <Rating>290</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.OVERRIDE.WARNING

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FOREX
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*    $INCLUDE  TEMENOS.BP I_F.SCB.SF.EXPIRY
*------------------------------------------------------
*** UPDATED BY MOHAMED SABEY 2014/12/22
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C' OR V$FUNCTION = 'H' THEN
        IF APPLICATION EQ 'ACCOUNT' THEN
            IF PGM.VERSION EQ ',HISTORY.REST' THEN
                TEXT = "Need Approval to use this version"
                CALL STORE.OVERRIDE(CURR.NO)
            END
*** UPDATED BY MOHAMED SABEY 2015/2/17
            IF PGM.VERSION EQ ',SCB.CUSTAC.NEW1.LEGAL1' THEN
                WS.CU.ID = R.NEW(AC.CUSTOMER)
                CALL DBR('CUSTOMER':@FM:EB.CUS.CUSTOMER.STATUS,WS.CU.ID,CUS.STATUS)
                IF CUS.STATUS EQ 22 THEN
                    TEXT = "Need Approval to open a debit AC for a minor customer"
                    CALL STORE.OVERRIDE(CURR.NO)
                END
            END
        END
    END
*** END OF UPDATE

    IF V$FUNCTION = 'I'  THEN
*DEBUG
        SCB.DB.CUST = '' ; SCB.CR.CUST = ''

        IF APPLICATION EQ 'CUSTOMER' THEN
            GOSUB GETCU
        END
        IF APPLICATION EQ 'ACCOUNT' THEN
            GOSUB GETAC
        END
        IF APPLICATION EQ 'LETTER.OF.CREDIT' THEN
            GOSUB GETLCM
        END
        IF APPLICATION EQ 'DRAWINGS' THEN
            GOSUB GETLCD
        END
        IF APPLICATION EQ 'IM.DOCUMENT.IMAGE' THEN
            GOSUB GETSIGN
        END
        IF APPLICATION EQ 'BILL.REGISTER' THEN
            GOSUB GETBILL
        END
        IF APPLICATION EQ 'FUNDS.TRANSFER' THEN
            GOSUB GETFT
        END
        IF APPLICATION EQ 'TELLER' THEN
            GOSUB GETTT
        END
        IF APPLICATION EQ 'LD.LOANS.AND.DEPOSITS' THEN
            GOSUB GETLD
        END
        IF APPLICATION EQ 'INF.MULTI.TXN' THEN
            GOSUB GETIN
        END
        IF APPLICATION EQ 'CARD.ISSUE' THEN
            GOSUB GETCI
        END
        IF APPLICATION EQ 'LIMIT' THEN
            GOSUB GETLM
        END
        IF APPLICATION EQ 'COLLATERAL' THEN
            GOSUB GETCOL
        END
        IF APPLICATION EQ 'COLLATERAL.RIGHT' THEN
            GOSUB GETCOLR
        END
        IF APPLICATION EQ 'MM.MONEY.MARKET' THEN
            GOSUB GETMM
        END
        IF APPLICATION EQ 'FOREX' THEN
            GOSUB GETFX
        END

*$$$$$$$$$$$$$$$ EDIT BY NESSMA 2011/06/30 $$$$$$$$$$$$$$$$$$$$$$$$$
*--- WH ---
        IF APPLICATION EQ 'SCB.WH.REGISTER' THEN
            GOSUB GET.REG
        END

        IF APPLICATION EQ 'SCB.WH.ITEMS' THEN
            GOSUB GET.ITEM
        END

        IF APPLICATION EQ 'SCB.WH.TRANS' THEN
            GOSUB GET.TRANS
        END
*--- SF ---
        IF APPLICATION EQ 'SCB.SAFEKEEP.RIGHT' THEN
            GOSUB GET.SAFEKEEP.RIGHT
        END

        IF APPLICATION EQ 'SCB.SF.TRANS' THEN
            GOSUB GET.SAFEKEEP.TRANS
        END

*        IF APPLICATION EQ 'SCB.SF.EXPIRY' THEN
*            GOSUB GET.SF.EXPIRY
*        END
        RETURN
    END
*===============================================================
GET.SF.EXPIRY:
*-------------
    SF.EXP.ACCOUNT.NO = ''
    ACCOUNT.NUMBER = R.NEW(SF.EXP.ACCOUNT.NO)

    IF ACCOUNT.NUMBER THEN
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCOUNT.NUMBER,SCB.DB.CUST)
        GOSUB BUILD.OVERRIDE
    END
    RETURN
*===============================================================
GET.SAFEKEEP.TRANS:
*------------------
    SCB.DB.CUST  = R.NEW(SCB.SF.TR.CUSTOMER.NO)
    GOSUB BUILD.OVERRIDE
    RETURN
*===============================================================
GET.SAFEKEEP.RIGHT:
*------------
    SCB.DB.CUST  = R.NEW(SCB.SAF.RIG.CUSTOMER.NO)
    GOSUB BUILD.OVERRIDE
    RETURN
*===============================================================
GET.REG:
*------
    SCB.DB.CUST  = R.NEW(SCB.WH.CUSTOMER)
    GOSUB BUILD.OVERRIDE
    RETURN
*===============================================================
GET.ITEM:
*--------
    SCB.DB.CUST  = R.NEW(SCB.WH.IT.CUSTOMER)
    GOSUB BUILD.OVERRIDE
    RETURN
*===============================================================
GET.TRANS:
*--------
    SCB.DB.CUST  = R.NEW(SCB.WH.TRANS.CUSTOMER)
    GOSUB BUILD.OVERRIDE
    RETURN
*===============================================================
*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
GETCU:
*=====
    SCB.DB.CUST  = ID.NEW
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETAC:
*=====
    SCB.DB.CUST  = R.NEW(AC.CUSTOMER)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETLCM:
*=====
    SCB.DB.CUST  = R.NEW(TF.LC.APPLICANT.CUSTNO)
    SCB.CR.CUST  = R.NEW(TF.LC.BENEFICIARY.CUSTNO)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETLCD:
*=====
    SCB.CHK.CU = ''
    SCB.N.MULT = '' ; SCB.AC.ID = '' ; SCB.AC.CU = ''
    IF R.NEW(TF.DR.DRAWDOWN.ACCOUNT) THEN
        SCB.AC.ID = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.AC.ID,SCB.AC.CU)
        SCB.DB.CUST  = SCB.AC.CU
        GOSUB BUILD.OVERRIDE
    END
    RETURN
*============================================================================
GETSIGN:
*=====
    SCB.DB.CUST  = R.NEW(IM.DOC.IMAGE.REFERENCE)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETBILL:
*=====
    SCB.DB.CUST  = R.NEW(EB.BILL.REG.DRAWER)
    IF SCB.DB.CUST[1,1] = 0 THEN SCB.DB.CUST  = R.NEW(EB.BILL.REG.DRAWER) + 0
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETFT:
*=====
    SCB.DB.CUST = '' ; SCB.CR.CUST = ''
    SCB.DB.CUST  = R.NEW(FT.DEBIT.CUSTOMER)
    SCB.CR.CUST  = R.NEW(FT.CREDIT.CUSTOMER)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETTT:
*=====
    SCB.DB.CUST  = R.NEW(TT.TE.CUSTOMER.1)
    SCB.CR.CUST  = R.NEW(TT.TE.CUSTOMER.2)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETLD:
*=====
    SCB.DB.CUST  = R.NEW(LD.CUSTOMER.ID)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETIN:
*=====
    SCB.CHK.CU = ''
    SCB.N.MULT = '' ; SCB.AC.ID = '' ; SCB.AC.CU = ''
*Line [ 288 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    SCB.N.MULT = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
    FOR I = 1 TO SCB.N.MULT
        IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I> THEN
            SCB.AC.ID = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,I>
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.AC.ID,SCB.AC.CU)
            SCB.DB.CUST  = SCB.AC.CU
            LOCATE SCB.DB.CUST IN SCB.CHK.CU<1,1> SETTING POS THEN
            END ELSE
                SCB.CHK.CU<1,I> = SCB.DB.CUST
                GOSUB BUILD.OVERRIDE
            END
        END
    NEXT I
    RETURN
*============================================================================
GETCI:
*=====
    SCB.CHK.CU = ''
    SCB.N.MULT = '' ; SCB.AC.ID = '' ; SCB.AC.CU = ''
*Line [ 308 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    SCB.N.MULT = DCOUNT(R.NEW(CARD.IS.ACCOUNT),@VM)
    FOR I = 1 TO SCB.N.MULT
        IF R.NEW(CARD.IS.ACCOUNT)<1,I> THEN
            SCB.AC.ID = R.NEW(CARD.IS.ACCOUNT)<1,I>
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.AC.ID,SCB.AC.CU)
            SCB.DB.CUST  = SCB.AC.CU
            LOCATE SCB.DB.CUST IN SCB.CHK.CU<1,1> SETTING POS THEN
            END ELSE
                SCB.CHK.CU<1,I> = SCB.DB.CUST
                GOSUB BUILD.OVERRIDE
            END
        END
    NEXT I
    RETURN
*============================================================================
GETLM:
*=====
    SCB.DB.CUST  = R.NEW(LI.LIABILITY.NUMBER)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETCOL:
*=====
    SCB.DB.CUST  = FIELD(ID.NEW,".",1)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETCOLR:
*=====
    SCB.DB.CUST  = FIELD(ID.NEW,".",1)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETMM:
*=====
    SCB.DB.CUST  = R.NEW(MM.CUSTOMER.ID)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
GETFX:
*=====
    SCB.DB.CUST  = R.NEW(FX.COUNTERPARTY)
    GOSUB BUILD.OVERRIDE
    RETURN
*============================================================================
BUILD.OVERRIDE:
*--------------
    SCB.POS.DB = '' ; SCB.POS.CR = ''
    IF SCB.DB.CUST # '' THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,SCB.DB.CUST,SCB.POS.DB)
        WS.POST.NO.DB = SCB.POS.DB<1,CULR.SCB.POSTING>
        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,WS.POST.NO.DB,WS.POSTING.DB)
*** UPDATE BY MOHAMED SABRY  2013/12/5
        WS.CU.DB.DRMNT = SCB.POS.DB<1,CULR.DRMNT.CODE>
        IF WS.CU.DB.DRMNT NE '' THEN
            TEXT = "Dormant Customer"
            CALL STORE.OVERRIDE(CURR.NO)
            TEXT = "Dormant Customer ":SCB.DB.CUST
            CALL STORE.OVERRIDE(CURR.NO)
        END
*** END UPDATE BY MOHAMED SABRY 2013/12/5
    END
    IF SCB.CR.CUST # '' THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,SCB.CR.CUST,SCB.POS.CR)
        WS.POST.NO.CR = SCB.POS.CR<1,CULR.SCB.POSTING>
        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,WS.POST.NO.CR,WS.POSTING.CR)
*** UPDATE BY MOHAMED SABRY  2013/12/5
        WS.CU.CR.DRMNT = SCB.POS.CR<1,CULR.DRMNT.CODE>
        IF WS.CU.CR.DRMNT NE '' THEN
            TEXT = "Dormant Customer"
            CALL STORE.OVERRIDE(CURR.NO)
            TEXT = "Dormant Customer ":SCB.CR.CUST
            CALL STORE.OVERRIDE(CURR.NO)
        END
*** END UPDATE BY MOHAMED SABRY 2013/12/5
    END

*    WS.POST.NO = SCB.POS.CR<1,CULR.SCB.POSTING>
*    CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,WS.POST.NO,WS.POSTING)
    IF SCB.POS.DB<1,CULR.SCB.POSTING> = 19 OR  SCB.POS.DB<1,CULR.SCB.POSTING> = 29 THEN
        TEXT = "���� ":SCB.DB.CUST:" ":WS.POSTING.DB
        CALL STORE.OVERRIDE(CURR.NO)
        TEXT = "Need aproval from the Branch Manager"
        CALL STORE.OVERRIDE(CURR.NO)
    END

    IF SCB.DB.CUST # SCB.CR.CUST AND ( SCB.CR.CUST # '' OR SCB.DB.CUST # '' ) THEN
        IF SCB.POS.CR<1,CULR.SCB.POSTING> = 19 OR SCB.POS.CR<1,CULR.SCB.POSTING> = 29 THEN
            TEXT = "���� ":SCB.CR.CUST:" ":WS.POSTING.CR
            CALL STORE.OVERRIDE(CURR.NO)
            TEXT = "Need aproval from the Branch Manager"
            CALL STORE.OVERRIDE(CURR.NO)
        END
    END
    RETURN
