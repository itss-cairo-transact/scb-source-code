* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SCB.BR.SLIPS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

    CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.DRAWER,ID.NEW,BR.EXIT)
    IF NOT(BR.EXIT) THEN
        FN.SLIPS = 'FBNK.SCB.BR.SLIPS' ; F.SLIPS = '' ; R.SLIPS = ''
        SLIP.REF = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.SLIP.REFER>

**    T.SEL  = "SELECT FBNK.SCB.BR.SLIPS WITH @ID EQ ": SLIP.REF

        CALL OPF( FN.SLIPS,F.SLIPS)
        CALL F.READ( FN.SLIPS,SLIP.REF, R.SLIPS, F.SLIPS, ETEXT)

*IF R.SLIPS<SCB.BS.REG.AMT> # R.SLIPS<SCB.BS.TOTAL.AMT> THEN

        R.SLIPS<SCB.BS.REG.AMT> -= R.NEW(EB.BILL.REG.AMOUNT)
*END

*IF R.SLIPS<SCB.BS.REG.NO> # R.SLIPS<SCB.BS.TOTAL.NO> THEN
        R.SLIPS<SCB.BS.REG.NO>  -= 1
*END

        CALL F.WRITE(FN.SLIPS,SLIP.REF,R.SLIPS)

        RETURN

    END
END
