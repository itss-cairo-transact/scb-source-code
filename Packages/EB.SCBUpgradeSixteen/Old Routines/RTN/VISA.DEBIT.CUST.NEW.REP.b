* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1382</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.DEBIT.CUST.NEW.REP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS.TOT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.NEW

* TO DEBIT THE TOTAL USAGES FROM CUSTOMERS VISA ACCOUNT AND PRINTOUT A REPORT CONTANING
* THE CUSTOMERS AND TOTAL AMOUNT TO BE DEBITED


    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 54 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.DEBIT.CUST.NEW.REP'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    F.VISA.TRANS.TOT = '' ; FN.VISA.TRANS.TOT = 'F.SCB.VISA.TRANS.TOT' ; R.VISA.TRANS.TOT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.TRANS.TOT,F.VISA.TRANS.TOT)

    YTEXT = "Enter the Start Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    T.SEL = "SELECT F.SCB.VISA.TRANS.TOT WITH POS.DATE GE ":COMI :" BY @ID "
*  T.SEL = "SELECT F.SCB.VISA.TRANS.TOT WITH POS.DATE GE 20070301 BY @ID"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    IF SELECTED THEN

****************************************
        CALL F.READU(FN.VISA.TRANS.TOT,KEY.LIST<1>,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E2,RETRY2)
* DEP.CODE=R.VISA.TRANS.TOT<SCB.TRANS.BRANCH.NUMBER>
* IF DEP.CODE # TEMP.CODE THEN
*     TEMP.CODE = DEP.CODE
*     CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEP.CODE,DEP.CODEE)
*     DEP.CODEE=FIELD(DEP.CODEE,'.',2)
*     XX<1,ZZ>="_________________":"   ���    ": DEP.CODEE:"   _____________________"
*     PRINT XX<1,ZZ>
*     ZZ=ZZ+1
* END
*****************************************
        XX=''  ; ZZ=''
        ACCT.NO = ''
        TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''  ; TOT.CUS.ALL = ''
        VISA.NO = KEY.LIST<1>[1,16]

        ACCT.NO<1> = R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<1>,CUST.NO)
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
        CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>

        TRNS.CODE=R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 102 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
        FOR J= 1 TO TRNS.COUNT
            T.CODE=TRNS.CODE<1,J>
            IF T.CODE EQ '1' THEN
                TOT.USE=TOT.USE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END
            IF T.CODE EQ '2' THEN
                TOT.INT=TOT.INT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END
            IF T.CODE EQ '3' THEN
                TOT.COMM=TOT.COMM+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END

            IF T.CODE EQ '4' THEN
                TOT.FEE=TOT.FEE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END

            IF T.CODE EQ '5' THEN
                TOT.ACT=TOT.ACT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>

            END
            IF T.CODE EQ '6' THEN
                TOT.REV=TOT.REV+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
            END
        NEXT J
        TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV

***********************************
        FOR I = 2 TO SELECTED
            CALL F.READU(FN.VISA.TRANS.TOT,KEY.LIST<I>,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E2,RETRY2)
            ACCT.NO<I> = R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>

            IF ACCT.NO<I> # ACCT.NO<I-1> THEN

                XX<1,ZZ>[1,30]= CUST.NAME
                XX<1,ZZ>[50,16]= VISA.NO
                XX<1,ZZ>[80,16]= ACCT.NO<I-1>
                XX<1,ZZ>[110,15]=TOT.ALL

                PRINT XX<1,ZZ>
                ZZ=ZZ+1
                XX = ''
                CUST.NAME = '' ; VISA.NO = '' ; TOT.ALL = ''
                TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''
                TOT.CUS.ALL = ''
                VISA.NO = KEY.LIST<I>[1,16]
                ACCT.NO<I> = R.VISA.TRANS.TOT<VISA.TRANS.CUST.ACCT>
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO<I>,CUST.NO)
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
                CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
                TRNS.CODE=R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 154 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
                FOR J= 1 TO TRNS.COUNT
                    T.CODE=TRNS.CODE<1,J>
                    IF T.CODE EQ '1' THEN
                        TOT.USE=TOT.USE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '2' THEN
                        TOT.INT=TOT.INT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '3' THEN
                        TOT.COMM=TOT.COMM+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '4' THEN
                        TOT.FEE=TOT.FEE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '5' THEN
                        TOT.ACT=TOT.ACT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>

                    END
                    IF T.CODE EQ '6' THEN
                        TOT.REV=TOT.REV+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                NEXT J
                TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV

            END ELSE

                TRNS.CODE=R.VISA.TRANS.TOT<VISA.TRANS.TRANS.CODE>
*Line [ 185 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
                FOR J= 1 TO TRNS.COUNT
                    T.CODE=TRNS.CODE<1,J>
                    IF T.CODE EQ '1' THEN
                        TOT.USE=TOT.USE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '2' THEN
                        TOT.INT=TOT.INT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                    IF T.CODE EQ '3' THEN
                        TOT.COMM=TOT.COMM+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '4' THEN
                        TOT.FEE=TOT.FEE+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END

                    IF T.CODE EQ '5' THEN
                        TOT.ACT=TOT.ACT+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>

                    END
                    IF T.CODE EQ '6' THEN
                        TOT.REV=TOT.REV+R.VISA.TRANS.TOT<VISA.TRANS.TRANS.AMT><1,J>
                    END
                NEXT J
                TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV


            END
************************************************************
            IF I = SELECTED   THEN
                XX<1,ZZ>[1,30]= CUST.NAME
                XX<1,ZZ>[50,16]= VISA.NO
                XX<1,ZZ>[80,16]= ACCT.NO<I>
                XX<1,ZZ>[110,15]=TOT.ALL
                PRINT XX<1,ZZ>
                ZZ=ZZ+1
            END
**************************************************************************
        NEXT I
    END
    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ������ ��� ��������� ������ ��� ���� ������"
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"����� ��������":SPACE(35):"��� ������":SPACE(20):"��� ���� �����":SPACE(12):"����������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(33):STR('_',12):SPACE(18):STR('_',20):SPACE(10):STR('_',8)
    HEADING PR.HD
    RETURN
*==============================================================

END
