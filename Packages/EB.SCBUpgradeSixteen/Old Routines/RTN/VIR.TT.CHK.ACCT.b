* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY NESSREEN AHMED 2013/6/4****
*********************************************
    SUBROUTINE VIR.TT.CHK.ACCT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS


    COMP = C$ID.COMPANY
    ACCT = '' ; CATEG = ''
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    ACCT = R.NEW(TT.TE.ACCOUNT.1)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT,CATEG)

    IF CATEG NE 1015 THEN
******UPDATED BY NESSREEN AHMED 12/12/2017  TO ADD MOHANDSEEN BRANCH***
****UPDATED BY NESSREEN AHMED 23/2/2016  TO ADD ASHER BRANCH 1 ***
*****UPDATED 19/12/2013 TO ADD SADAT BRANCH 1*****
*****UPDATED 19/12/2013 TO REMOVE ADDING SADAT BRANCH 2*****
****  IF ((COMP NE 'EG0010001') AND (COMP NE 'EG0010011') AND (COMP NE 'EG0010012') AND (COMP NE 'EG0010013') AND (COMP NE 'EG0010015') AND (COMP NE 'EG0010023') AND (COMP NE 'EG0010040') AND (COMP NE 'EG0010002')) THEN
*       IF ((COMP NE 'EG0010001') AND (COMP NE 'EG0010011') AND (COMP NE 'EG0010012') AND (COMP NE 'EG0010013') AND (COMP NE 'EG0010015') AND (COMP NE 'EG0010023') AND (COMP NE 'EG0010040') AND (COMP NE 'EG0010090')) THEN
***IF ((COMP NE 'EG0010001') AND (COMP NE 'EG0010011') AND (COMP NE 'EG0010012') AND (COMP NE 'EG0010013') AND (COMP NE 'EG0010015') AND (COMP NE 'EG0010023') AND (COMP NE 'EG0010040') AND (COMP NE 'EG0010070')) THEN
   IF ((COMP NE 'EG0010001') AND (COMP NE 'EG0010011') AND (COMP NE 'EG0010012') AND (COMP NE 'EG0010013') AND (COMP NE 'EG0010015') AND (COMP NE 'EG0010023') AND (COMP NE 'EG0010040') AND (COMP NE 'EG0010070') AND (COMP NE 'EG0010004')) THEN
****END OF UPDATE 19/12/2013*********
****END OF UPDATE 23/2/2016*********************************
          ETEXT = '�� ���� ������� �� ��� ������'
          CALL STORE.END.ERROR
       END
    END

    RETURN
************************************************
END
