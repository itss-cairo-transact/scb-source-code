* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CHECK.LEN.AC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*----------------------------------------
*    IF V$FUNCTION ='I' THEN
    LEN.AC = LEN(ID.NEW)

    IF LEN.AC NE 16 THEN
        E = "������ ���� �� ���� 16 ���"
        CALL ERR ; MESSAGE = 'REPEAT'
    END

    IF LEN.AC EQ 16 THEN
        CUS.ID = ID.NEW[1,8]+1-1
        CUR.ID = ID.NEW[9,2]
        CAT.ID = ID.NEW[11,4]
        CALL DBR ('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUS.ID,VAR1)
        CALL DBR ('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.ID,VAR2)
        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CAT.ID,VAR3)

        IF NOT(VAR1) THEN
            E = "INVALID CUSTOMER ID"
            CALL ERR ; MESSAGE = 'REPEAT'
        END
        IF NOT(VAR2) THEN
            E = "INVALID CURRENCY ID"
            CALL ERR ; MESSAGE = 'REPEAT'
        END
        IF NOT(VAR3) THEN
            E  = "INVALID CATEGORY ID"
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*   END
*--------------------------------------------------------
    RETURN
END
