* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1326</Rating>
*-----------------------------------------------------------------------------
** ----- 15/07/2009 NESSREEN AHMED-----**

    SUBROUTINE VNC.AC.VOSTRO.CATEG.FAULT
*IF THE CATEGORY RANGE FROM 2000

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS


    IF V$FUNCTION='I' THEN
        GOSUB INTIAL
        GOSUB CHECK.CATEGORY ; IF E THEN  GOTO END.PROG
        GOSUB CHECK.SECTOR ; IF E THEN  GOTO END.PROG
        GOSUB CHECK.BRANCH.CLOSURE ; IF E THEN GOTO END.PROG
        GOSUB CHECK.IF.EXIST
        IF NOT(EXIST) THEN GOSUB ACCT.TITLE
        GOTO END.PROG
    END
***************************************************************************************
INTIAL:
    ETEXT=''   ; CATEG=''   ; LEN.ID = 0 ; CUST = '' ; MYSECTOR = ''
    CAT.TITLE='';CURR.TITLE ='';CUST.TITLE='';MYYIIDD='' ;MESSAGE='' ;EXIST='' ; E=''  ;ID.CAT='' ;ID.CURR='';ID.CUST=''
    RETURN
***************************************************************************************
CHECK.CATEGORY:
    LEN.ID = LEN(ID.NEW)
    IF LEN.ID = 14 THEN CATEG = ID.NEW[11,4] ; CUST = ID.NEW[1,8] ; CURR = ID.NEW[9,2]
    IF LEN.ID = 16 THEN CATEG = ID.NEW[11,4] ; CUST = ID.NEW[1,8] ; CURR = ID.NEW[9,2]
    IF (CATEG NE 2000) AND (CATEG NE 2001) THEN
        E = "category not allowed"
    END
    RETURN
****************************************************************************************
CHECK.SECTOR:
    CUST = TRIM( CUST, '0', 'L')        ;* remove leading zeros
    CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, CUST, MYSECTOR)
    IF ETEXT THEN
        ETEXT = '';E = 'CUSTOMER (&) DOES NOT EXIST' : @FM : CUST
    END ELSE
        IF MYSECTOR < 3000 OR MYSECTOR >4000 THEN
            E = 'SECTOR.NOT.TO.BANK1 (&)':MYSECTOR
        END
    END

    RETURN
****************************************************************************************
CHECK.BRANCH.CLOSURE:
************************UPDATED BY RIHAM R15 *******************
  *  IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
     BRANCH.ID = R.NEW(AC.CO.CODE)[8,2]
     AC.OFICER = TRIM(BRANCH.ID, "0" , "L")
    IF AC.OFICER NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
******************END OF UPDATED**************************
     E = 'This.Account.From.Other.Branch'
    END
    IF  R.NEW(AC.POSTING.RESTRICT) GE '90' THEN
        E = 'This.is.closure.Account'
    END
    RETURN
****************************************************************************************
CHECK.IF.EXIST:
*-------------TO CHECK IF IT EXISTS OR NOT IN LIVE FILE---------------------------*

    CALL DBR( 'ACCOUNT':@FM:AC.MNEMONIC, ID.NEW , MYYIIDD)
    IF NOT(ETEXT) THEN EXIST = 'Y'
* ----------- TO CHECK IF IT EXISTS OR NOT IN UNAUTHORIZED------------------------*
    ELSE
        ETEXT= ''
        CALL DBR( 'ACCOUNT$NAU':@FM:AC.MNEMONIC, ID.NEW , MYYIIDD)
**    IF NOT(ETEXT) THEN EXIST = 'Y' ; E = 'THIS ACCOUNT IS NOT AUTHORIZED &' : ID.NEW
    END
    RETURN
*****************************************************************************************
ACCT.TITLE:
    CURR=TRIM(CURR,'0','L')
    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,CATEG,CAT.TITLE)
    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CURR,CURR.TITLE)
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CURR.TITLE,CURR1)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,MYLOCAL)
****************************************************
    F.COUNT = '' ; FN.COUNT = 'F.CATEGORY';R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
    CALL F.READ(FN.COUNT,CATEG,R.COUNT,F.COUNT,ERRORR)
    CATTITLE1 = R.COUNT<EB.CAT.SHORT.NAME,2>
****************************************************
    F.COUNT = '' ; FN.COUNT = 'F.CURRENCY.PARAM';R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
    CALL F.READ(FN.COUNT,CURR.TITLE,R.COUNT,F.COUNT,ERRORR)
    CURRTITLE1 = R.COUNT<EB.CUP.CCY.NAME,2>
    ARABIC.TITLE= MYLOCAL<1,CULR.ARABIC.NAME>
    CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUST,CUST.TITLE)
    IF  ETEXT   THEN E= ETEXT ; ETEXT=''
    ELSE
* TEXT = CATEG; CALL REM
        R.NEW(AC.ACCOUNT.TITLE.1)= CUST.TITLE:'-':CAT.TITLE:'-':CURR1
        R.NEW(AC.SHORT.TITLE)=R.NEW(AC.ACCOUNT.TITLE.1)
        R.NEW(AC.LOCAL.REF)<1,ACLR.ARABIC.TITLE>=ARABIC.TITLE:'-':CATTITLE1:'-':CURRTITLE1
    END
*******************************************************************
    RETURN
******************************************************************************************
END.PROG:
    IF E THEN
        CALL ERR
        MESSAGE = 'REPEAT'
    END
    RETURN
END
