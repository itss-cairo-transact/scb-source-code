* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
****NESSREEN AHMED 08/12/2010***************
*-----------------------------------------------------------------------------
* <Rating>149</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.NO.CARDS.IN.MSCC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE.1 = "cards_not_found_MSCC"
    NEW.FILE.2 = "cards_not_found_MSCC_UPD"

    Path = '/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&/cards_not_found_MSCC'

    TEXT = 'HI' ; CALL REM
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

*****2ND FILE OPEN******************************************
    OPENSEQ DIR.NAME,NEW.FILE.2 TO V.FILE.IN.2 THEN
        CLOSESEQ V.FILE.IN.2
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE.2
        HUSH OFF
        PRINT 'FILE ':NEW.FILE.2:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE.2 TO V.FILE.IN.2 ELSE
        CREATE V.FILE.IN.2 THEN
            PRINT 'FILE ' :NEW.FILE.2:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE.2:' to ':DIR.NAME
        END
    END
************************************************************
    F.VISA.DB.AD = '' ; FN.VISA.DB.AD = 'F.SCB.READ.DB.ADVICE' ; R.VISA.DB.AD= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.DB.AD,F.VISA.DB.AD)

    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    TEXT = 'Start.Of.File' ; CALL REM
    EOF = ''
    LOOP WHILE NOT(EOF)
        CARD.NO = '' ; COMP.NAME = ''
        READSEQ Line FROM MyPath THEN
            CARD.NO           = Line[1,16]
            S.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
            KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
            CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)

            IF SELECTED.SS THEN
                FOR I = 1 TO SELECTED.SS
                    CR.CARD.NO = '' ; EXT.ACC.NEW = '' ; BR.CODE = '' ; INT.ACC = ''
                    EMB.NAME = ''
                    CALL F.READ(FN.CARD.ISSUE,KEY.LIST.SS<I>, R.CARD.ISSUE, F.CARD.ISSUE ,E3)
                    LOCAL.REF      = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                    CR.CARD.NO<I>  = LOCAL.REF<1,LRCI.VISA.NO>
                    EXT.ACC.NEW<I> = R.CARD.ISSUE<CARD.IS.ACCOUNT>
                    BR.CODE<I>     = R.CARD.ISSUE<CARD.IS.CO.CODE>
                    CALL DBR( 'COMPANY':@FM:EB.COM.COMPANY.NAME, BR.CODE<I> , COMP.NAME)
                    EMB.NAME<I>    = R.CARD.ISSUE<CARD.IS.NAME>

                    DIM VISA.DATA.ERR(3)

                    VISA.DATA.ERR(1) = CR.CARD.NO<I>
                    VISA.DATA.ERR(2) = COMP.NAME
                    VISA.DATA.ERR(3) = EMB.NAME<I>
                    FOR A = 1 TO 3
                        WRITESEQ VISA.DATA.ERR(A) TO V.FILE.IN.2 ELSE
                            PRINT  'CAN NOT WRITE LINE ':VISA.DATA.ERR(A)
                        END
                    NEXT A
                NEXT I
            END
        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath
    RETURN
END
