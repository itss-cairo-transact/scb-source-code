* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>973</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.TT.CHRG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

*---------------------
    PL.CHRG = "52115"
*---------------------
**  CATEG.CR   = R.NEW(TT.TE.ACCOUNT.2)[4,5]
    CURR       = 'EGP'
    DR.ACCT    = R.NEW(TT.TE.ACCOUNT.2)
    AMT        = 5
    V.DATE     = TODAY

    *CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,DR.ACCT,ACC.OFFICER)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,DR.ACCT,AC.COMP)
    ACC.OFFICER  = AC.COMP[8,2]
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DR.ACCT,CATEG.CR)

    IF V$FUNCTION EQ 'I' THEN

*-----
* CR
*-----
        Y.ACCT  = ''
        LCY.AMT = AMT
        FCY.AMT = ''
        CATEG   = CATEG.CR
        GOSUB AC.STMT.ENTRY
*----
* DR
*----
        Y.ACCT  = DR.ACCT
        PL.CHRG = ''
        LCY.AMT = AMT * -1
        FCY.AMT = ''
        CATEG   = CATEG.CR

        GOSUB AC.STMT.ENTRY
    END
    IF V$FUNCTION EQ 'R' THEN
*----
* CR
*----
        Y.ACCT  = DR.ACCT
        PL.CHRG = ''
        LCY.AMT = AMT
        FCY.AMT = ''
        CATEG   = CATEG.CR

        GOSUB AC.STMT.ENTRY
*----
* DR
*----
        Y.ACCT  = ''
        LCY.AMT = AMT * -1
        FCY.AMT = ''
        CATEG   = CATEG.CR
        GOSUB AC.STMT.ENTRY
    END
    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
    ENTRY = ""
    MULTI.ENTRIES = ""

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '263'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.CHRG
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
