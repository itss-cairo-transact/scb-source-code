* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BR.DFLT


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    ETEXT = ''
    E     = ''

    IF V$FUNCTION = 'I' THEN

        FN.BR = "FBNK.BILL.REGISTER"
        F.BR  = ""
        ETEXT = ""

        CALL OPF(FN.BR,F.BR)
        CALL F.READ(FN.BR,ID.NEW,R.BR,F.BR,ETEXT)
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA> = 7 OR R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA> = 8  THEN
            E = "��� ����� "
*** SCB UPG 20160621 - S
*   CALL ERR ; MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    IF V$FUNCTION = 'A' THEN
        STA  = R.NEW(EB.BILL.REG.RECORD.STATUS)
        DEP  = R.NEW(EB.USE.LOCAL.REF)<1,USER.SCB.DEPT.CODE>
        DEP  = R.USER<EB.USE.LOCAL.REF,USER.SCB.DEPT.CODE>
*       TEXT = "H" ; CALL REM
*       TEXT = "STA" : STA; CALL REM
*       TEXT = 'DEP' : DEP  ; CALL REM
        IF STA EQ 'INA2' THEN

            IF DEP NE '5100'  THEN
                E = "��� �� ���� ���� ��� "
*** SCB UPG 20160621 - S
* CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
            END
        END
    END

    RETURN
END
