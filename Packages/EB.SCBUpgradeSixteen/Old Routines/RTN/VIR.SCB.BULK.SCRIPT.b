* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.SCB.BULK.SCRIPT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

************UPDATE SCB.SR.SCRIPT

    FN.SR = 'F.SCB.SR.SCRIPT' ; F.SR = '' ; R.SR = '' ; ETEXT = ''
    CALL OPF( FN.SR,F.SR)

    T.SEL = "SELECT F.SCB.SR.SCRIPT WITH @ID EQ ": R.NEW(FT.BKCRAC.ORDERING.BK)<1,1>

    CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
    CALL F.READ( FN.SR,KEY.LIST, R.SR, F.SR, ETEXT)
    IF SELECTED THEN
        R.SR<SR.SCRIPT.TT.REFERENCE>  = ID.NEW
    END ELSE
        TEXT = "ERROR" ; CALL REM
    END

*WRITE R.SR TO F.SR , KEY.LIST ON ERROR
*   PRINT "CAN NOT WRITE RECORD ":KEY.LIST :" TO ":FN.SR
*END
    CALL F.WRITE (FN.SR,KEY.LIST,R.SR)
************UPDATE CUSTOMER
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ETEXT = ''
    CALL OPF( FN.CU,F.CU)

    T.SEL.CU = "SELECT FBNK.CUSTOMER WITH @ID EQ ": R.SR<SR.SCRIPT.CUSTOMER>

    CALL EB.READLIST( T.SEL.CU, KEY.LIST.CU, '', SELECTED.CU, E)
    CALL F.READ( FN.CU,KEY.LIST.CU, R.CU, F.CU, E)
    IF SELECTED.CU THEN
        R.CU<EB.CUS.LOCAL.REF,CULR.BROKER.TYPE>  = R.CU<EB.CUS.LOCAL.REF,CULR.BROKER.TYPE>  + R.NEW(FT.BKCRAC.ORDERING.BK)<1,2>
    END ELSE
        TEXT = "ERROR" ; CALL REM
    END

*WRITE R.CU TO F.CU , KEY.LIST.CU ON ERROR
*   PRINT "CAN NOT WRITE RECORD ":KEY.LIST.CU :" TO ":FN.CU
*END
    CALL F.WRITE (FN.CU,KEY.LIST.CU,R.CU)
************
    RETURN
END
