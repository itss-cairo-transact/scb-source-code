* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>220</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LG.CHECK.BALANCE.NOMARG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF MESSAGE EQ 'VAL' THEN
****   R.NEW(LD.CHRG.CODE) = 14
        CU.NO = R.NEW(LD.CUSTOMER.ID)
        IF PGM.VERSION = ',SCB.LG.AMDATE' THEN
            AC = R.NEW(LD.CHRG.LIQ.ACCT)
        END ELSE
            AC = R.NEW( LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
        END
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC,CATEG)
        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,AC,LIM.REF)
        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        OLD.MARG = MYLOCAL<1,LDLR.MARGIN.AMT>

        DIFF.AMT=0

*        IF CATEG EQ 1201 OR CATEG EQ 1202 THEN
        IF CATEG GE 1101 AND CATEG LE 1599 THEN
            CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,AC,LIM.REF)
            LIM  = CU.NO :".":"0000":LIM.REF

            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)

            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = DIFF.AMT + R.NEW(LD.CHRG.AMOUNT)<1,1>+ R.NEW(LD.CHRG.AMOUNT)<1,2>


            ZERO = "0.00"
            BAL  =  WORK.BALANCE + AVL

            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)

*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)

            BLOCK =LOCK.AMT<1,LOCK.NO>


            BAL.BLOCK = BAL - BLOCK
            NET.BAL = BAL.BLOCK - AMT


            IF NET.BAL LT ZERO THEN
**          ETEXT = '���� ������ �� ����' ; CALL STORE.END.ERROR
            END
        END ELSE
********************************
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = DIFF.AMT + R.NEW(LD.CHRG.AMOUNT)<1,1>+ R.NEW(LD.CHRG.AMOUNT)<1,2>

            ZERO = "0.00"
            BAL  =  WORK.BALANCE


            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
*Line [ 90 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            BLOCK =LOCK.AMT<1,LOCK.NO>

            BAL.BLOCK = BAL - BLOCK
            NET.BAL = BAL.BLOCK - AMT


            IF NET.BAL LT ZERO THEN
**          ETEXT = '���� ������ �� ����' ;CALL STORE.END.ERROR
            END
        END
**********************************
    END
    RETURN
END
