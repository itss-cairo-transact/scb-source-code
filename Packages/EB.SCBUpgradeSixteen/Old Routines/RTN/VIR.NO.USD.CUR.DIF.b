* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.NO.USD.CUR.DIF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

********************************************************************
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

*****************TO CHECK DEBIT AND CREDIT ACCOUNT ********************

    ACCC1 = R.NEW(FT.DEBIT.ACCT.NO)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC1,CCURR1)
    IF CCURR1 EQ '' THEN

        ETEXT = ',RESPONS.CODE=D75,' ;  CALL STORE.END.ERROR
    END

* ACCC2 = R.NEW(FT.CREDIT.ACCT.NO)
* CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC2,CCURR2)
* IF CCURR2 EQ '' THEN

*     ETEXT = ',RESPONS.CODE=D76,' ;  CALL STORE.END.ERROR

* END

*    IF ACCC1 EQ ACCC2 THEN
*        ETEXT = ',RESPONS.CODE=D77,' ;  CALL STORE.END.ERROR
*    END
*****************TO STOP A DIFFERENT CUSTOMER************
    IF R.NEW(FT.CREDIT.CUSTOMER) = R.NEW(FT.DEBIT.CUSTOMER) THEN

        ETEXT = ',RESPONS.CODE=D77,' ; CALL STORE.END.ERROR

    END
*****************TO STOP THE SAME ACCOUNT*************
    IF R.NEW(FT.DEBIT.ACCT.NO)[1,8]  = R.NEW(FT.CREDIT.ACCT.NO)[1,8] THEN

        ETEXT = ',RESPONS.CODE=D77,' ; CALL STORE.END.ERROR
    END
*****************TO STOP MISSING AMOUNT************
    IF R.NEW(FT.DEBIT.AMOUNT) = '' THEN

        ETEXT = ',RESPONS.CODE = D20,' ; CALL STORE.END.ERROR

    END

*****************TO CHECK TRANSACTION************
    IF R.NEW(FT.TRANSACTION.TYPE)[1,2] # 'AC' THEN

        ETEXT = ',RESPONS.CODE=D01,' ; CALL STORE.END.ERROR

    END

*****************TO CHECK DEBIT AND CREDIT ACCOUNT ********************
    ACCC3 = R.NEW(FT.DEBIT.ACCT.NO)
    CALL F.READ(FN.AC,ACCC3,R.AC,F.AC,E2)
    CUS.ID  = R.AC<AC.CUSTOMER>
    AC.POST = R.AC<AC.POSTING.RESTRICT>
*******
    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E3)
    CUS.POST = R.CU<EB.CUS.POSTING.RESTRICT>

    IF AC.POST NE '' OR CUS.POST NE '' THEN
        ETEXT = ',RESPONS.CODE=D50,' ;  CALL STORE.END.ERROR
    END
*        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E3)
*        CUS.POST = R.CU<EB.CUS.POSTING.RESTRICT>
*        IF CUS.POST NE '' THEN
*            E = ',RESPONS.CODE=D50,' ;  CALL STORE.END.ERROR
*        END
*    END
****************TO CHECK THE CURRENCY********************

    IF R.NEW(FT.DEBIT.CURRENCY) = 'EGP' AND R.NEW(FT.CREDIT.CURRENCY) # 'EGP' THEN

        ETEXT = ',RESPONS.CODE=D76,' ; CALL STORE.END.ERROR
    END

* IF R.NEW(FT.DEBIT.CURRENCY) # 'EGP' AND R.NEW(FT.CREDIT.CURRENCY) = 'EGP' THEN
*     ETEXT = ',RESPONS.CODE=D76,' ; CALL STORE.END.ERROR
* END

*****************TO CHECK DEBIT ACCOUNT *********************
    DEB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)

    IF NUM(DEB.ACCT[1,3]) THEN
        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,DEB.ACCT,CATEG)
        IF (CATEG >= 5000 AND CATEG <= 5999) THEN
            ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
        END
    END ELSE
        ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
    END

*****************TO CHECK CREDIT ACCOUNT *********************
    CRD.ACCT = R.NEW(FT.CREDIT.ACCT.NO)

    IF NUM(CRD.ACCT[1,3]) THEN
        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,CRD.ACCT,CATEG)
        IF (CATEG >= 5000 AND CATEG <= 5999) THEN
            ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
        END

*    END ELSE

*        ETEXT = ',RESPONS.CODE=D35,' ; CALL STORE.END.ERROR
    END
*****************TO GET THE NET VALUE*********************
    SCB.ACT.CB = R.NEW(FT.DEBIT.ACCT.NO)
    SCB.CHK.AMT = FIELD(R.NEW(FT.AMOUNT.DEBITED),R.NEW(FT.DEBIT.CURRENCY),2)
    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,SCB.ACT.CB,SCB.W.BAL)
    SCB.W.BAL += SCB.CHK.AMT  ;*R.NEW(FT.DEBIT.AMOUNT)
    ACT.CATEG = SCB.ACT.CB[11,4]
*    IF (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR ACT.CATEG EQ 6511 OR ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1002 OR ACT.CATEG EQ 1003 OR ACT.CATEG EQ 1005 OR ACT.CATEG EQ 1006 OR (ACT.CATEG GE  1101 AND ACT.CATEG LE 1202) OR (ACT.CATEG GE  1290 AND ACT.CATEG LE 1599) THEN
        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,SCB.ACT.CB,LIM.REF)
        IF LIM.REF THEN
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.ACT.CB,ACT.CUS)
            LIM.ID = ACT.CUS:".":"0000":LIM.REF
            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM.ID,AVIL.BAL)
            CALL DBR('LIMIT':@FM:LI.EXPIRY.DATE,LIM.ID,SCB.EXP.DAT)
        END ELSE
            AVIL.BAL = 0
        END
        END.BAL.L22 = SCB.W.BAL + AVIL.BAL
*        TEXT = 'SCB.W.BAL=' : SCB.W.BAL ;CALL REM
*        TEXT = 'AVIL.BAL=' : AVIL.BAL ;CALL REM
*        TEXT = 'END.BAL.L22=' : END.BAL.L22 ;CALL REM

        CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,SCB.ACT.CB,LOC.BAL)
*Line [ 171 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        BALC = DCOUNT(LOC.BAL,@VM)
        BALCC = LOC.BAL<1,BALC>

*        TEXT = 'END.BAL.LOC=' : BALCC ;CALL REM
        END.BAL.L = END.BAL.L22 - BALCC

*       TEXT = 'END.BAL.L=' : END.BAL.L ;CALL REM

*        IF END.BAL.L LT R.NEW(FT.DEBIT.AMOUNT) THEN
        IF END.BAL.L LT SCB.CHK.AMT THEN

           ETEXT = ',RESPONS.CODE=D51,' ; CALL STORE.END.ERROR

        END

        IF SCB.EXP.DAT LE TODAY AND SCB.EXP.DAT THEN

           ETEXT = ',RESPONS.CODE=D51,' ; CALL STORE.END.ERROR

        END

***************APPROVED**********
        CALL F.READ(FN.FT,ID.NEW,R.FT,F.FT,E6)

       IF ID.NEW NE ''  THEN

            R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT> = ',RESPONS.CODE=A00,'

       END
        RETURN
    END
