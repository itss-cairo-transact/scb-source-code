* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*******ABEER 6/04/2003*******************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.LG.INWARD.CHRG

*A Routine To Default chrg code&Amt In SCB.LG.CONFIRMED

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CONFIRMED
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

*Line [ 37 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    COUNT.AMT  = DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>,@SM)
*  DAT=R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
    DAT=R.NEW(LD.LOCAL.REF)<1,LDLR.MATUR.DATE>
**********************************************************************
    FN.SCB.LG.CONFIRMED='F.SCB.LG.CONFIRMED';ID=ID.NEW:".":DAT;R.CHARGE='';F.CHARGE=''
    CALL F.READ( FN.SCB.LG.CONFIRMED,ID, R.CHARGE, F.CHARGE, ETEXT)
    IF ETEXT THEN
        R.CHARGE<SCB.CURRENCY>= R.NEW(LD.CURRENCY)
        R.CHARGE<SCB.CONFIRMED>='NO'
        R.CHARGE<SCB.INPUTTER>= R.NEW(LD.INPUTTER)

        FOR I = 1 TO COUNT.AMT
            R.CHARGE<SCB.CODE,I>=R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE,I>
            R.CHARGE<SCB.AMOUNT,I>=R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT,I>
        NEXT I
********************************************************
        CALL F.WRITE(FN.SCB.LG.CONFIRMED,ID,R.CHARGE)

********************************************************
        R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.CODE>=''
        R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT>=''
    END
    RETURN
END
