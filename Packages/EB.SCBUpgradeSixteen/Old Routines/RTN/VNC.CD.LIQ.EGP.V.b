* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>373</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CD.LIQ.EGP.V

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.PREM.AMT
*TO CHECK  THAT IF CD NOT EXIST OR MATURED OR LIQUIDATED OR IN HOLD THEN ERROR MESSAGE WILL BE DISPLAYED
*TO DEFAULT VERSION NAME,APPROVAL DATE WITH TODAY

    IF V$FUNCTION EQ 'I' THEN


        R.NEW(LD.INT.LIQ.ACCT) = 'EGP11202000100':ID.COMPANY[8,2]
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CATTT)
        IF NOT(CATTT GE 21026 AND CATTT LE 21028 ) THEN
            E = ' ��� ��������� �� ������ ��� �������'
        END

        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,MYMATR)
        IF ETEXT THEN
            E='��� �� ���� ����� ����� �� ���'; CALL ERR ; MESSAGE = 'REPEAT'
**          E='CD Should Be Issued Before'; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.BLOCK.PURPOSE> THEN
                E='��� ������������ �����';CALL ERR; MESSAGE='REPEAT'
**                E='CD Is Holded'; CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF MYMATR LT TODAY THEN
                E='��� ������� ������';CALL ERR ; MESSAGE = 'REPEAT'
**                E='CD IS Matured'; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=',SCB.CD.LIQ' THEN
                    E='��� ������� ������';CALL ERR;MESSAGE='REPEAT'
**    E='CD Is Liquidated'; CALL ERR ; MESSAGE = 'REPEAT'
                END ELSE
                    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
                    MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
                    IF MYVERNAME EQ ',SCB.CD.OPEN1' THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = R.NEW(LD.VALUE.DATE)
                    END
                    R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
                    R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>=TODAY


                END
            END
        END
    END
    RETURN
END
