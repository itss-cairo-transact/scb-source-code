* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSixteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSixteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2985</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.TOT.PAY.DATE
**WRITTEN BY NESSREEN AHMED 04/10/2008

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.SETT

*A REPORT TO PRINT THE TOTAL USAGES TO BE DEBITED FROM CUSTOMERS' VISA ACCOUNT

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 53 ] Adding EB.SCBUpgradeSixteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.TOT.PAY.DATE'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    RETURN
*===============================================================
CALLDB:

**   YTEXT = "Enter the Date : "
**   CALL TXTINP(YTEXT, 8, 22, "12", "A")

    FN.TELLER = 'F.TELLER$HIS' ; F.TELLER = '' ; R.TELLER = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.TELLER,F.TELLER)

    TOT.TRN.ALL = '' ; TOT.TT = '' ; TOT.FT = '' ; TOT.AMT.ALL = '' ; TOT.SET = '' ; TOT.SET2 = ''
*=============TELLER SELECTION==========================================*
*****UPDATED BY NESSREEN 24/03/2009***************************************
**  T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 37 AND VALUE.DATE.1 EQ ":COMI
****UPDATED BY NESSREEN 31/03/2009****************************************
**  T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 37 AND AUTH.DATE EQ ":COMI
    T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE EQ 37 AND RECORD.STATUS EQ MAT AND AUTH.DATE EQ ":COMI :" BY CO.CODE "
***************************************************************************
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    TEXT = '��� ��������� �������=':SELECTED ; CALL REM
    XX= '' ; ZZ = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            VISA.NO = '' ; CUST.NAME = '' ; TRAN.DATE = '' ; TRANS.AMT = ''
            COMP.CO = ''
            KEY.TO.USE = KEY.LIST<I>
            CALL OPF(FN.TELLER,F.TELLER)
            CALL F.READ(FN.TELLER,  KEY.TO.USE, R.TELLER, F.TELLER, E1)

            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,TTLR.VISA.NUMBER>
            ACCT = R.TELLER<TT.TE.ACCOUNT.1>
            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACCT , CUST)

            FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)
            CUST.NAME = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            TRAN.DATE = R.TELLER<TT.TE.VALUE.DATE.1>
            TRANS.AMT = R.TELLER<TT.TE.NET.AMOUNT>
            COMP.CO   = R.TELLER<TT.TE.CO.CODE>

******UPDATED ON 26/10/2008******************************
            TOT.TT = TOT.TT + TRANS.AMT
***********************************************************
            XX<1,ZZ>[1,16]= VISA.NO
            XX<1,ZZ>[20,16]= ACCT
            XX<1,ZZ>[40,35]= CUST.NAME
            XX<1,ZZ>[80,8]= TRAN.DATE
            XX<1,ZZ>[95,8]= TRANS.AMT
            XX<1,ZZ>[110,10] = COMP.CO
            XX<1,ZZ>[125,5] = '����'
            PRINT XX<1,ZZ>
            PRINT STR('-',132)
            ZZ=ZZ+1
            XX = ''
***********************************************************
        NEXT I
    END
*========FT SELECTION=======================================
*****UPDATED BY NESSREEN 24/03/2009*******************************
**  N.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ ACVC AND CREDIT.VALUE.DATE EQ ":COMI
*****UPDATED BY NESSREEN 30/03/2009********************************
**  N.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ ACVC AND AUTH.DATE EQ ":COMI
    N.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ ACVC AND RECORD.STATUS EQ MAT AND AUTH.DATE EQ ":COMI : " BY CO.CODE"
*******************************************************************

    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    TEXT = '��� ��������� ��������=':SELECTED.N ; CALL REM

    FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = '' ; RETRY3= '' ; E3 = ''
    CALL OPF(FN.FT,F.FT)
    IF SELECTED.N THEN
        FOR NN = 1 TO SELECTED.N
            VISA.NO.FT = '' ; CUST.NAME.FT = '' ; TRAN.DATE.FT = '' ; TRANS.AMT.FT = ''
            COMP.CO.FT = ''
            FT.ID = KEY.LIST.N<NN>
            CALL F.READ(FN.FT,FT.ID, R.FT, F.FT , EERE3)
            VISA.NO.FT = R.FT<FT.LOCAL.REF,FTLR.VISA.NO>
            CUST.FT = R.FT<FT.DEBIT.CUSTOMER>

            FN.CUSTOMER.FT = 'FBNK.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
            CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
            CALL F.READ(FN.CUSTOMER.FT, CUST.FT, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)
            CUST.NAME.FT = R.CUSTOMER.FT<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            ACCT.FT = R.FT<FT.DEBIT.ACCT.NO>
            TRAN.DATE.FT = R.FT<FT.DEBIT.VALUE.DATE>
            TRANS.AMT.FT = R.FT<FT.DEBIT.AMOUNT>
******UPDATED ON 26/10/2008**********************
            TOT.FT = TOT.FT + TRANS.AMT.FT
*************************************************
            COMP.CO.FT = R.FT<FT.CO.CODE>

            XX<1,ZZ>[1,16]= VISA.NO.FT
            XX<1,ZZ>[20,16]= ACCT.FT
            XX<1,ZZ>[40,35]= CUST.NAME.FT
            XX<1,ZZ>[80,8]= TRAN.DATE.FT
            XX<1,ZZ>[95,8]= TRANS.AMT.FT
            XX<1,ZZ>[110,10]= COMP.CO.FT
            XX<1,ZZ>[125,5]= '�����'
            PRINT XX<1,ZZ>
            PRINT STR('-',132)
            ZZ=ZZ+1
            XX = ''

        NEXT NN
****UPDATED ON 17/11/2008******************************************************
    END ELSE
****UPDATED BY NESSREEN 24/03/2009**************************
**  N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ ACVC AND CREDIT.VALUE.DATE EQ ":COMI
        N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ ACVC AND AUTH.DATE EQ ":COMI
*************************************************************
        KEY.LIST.N=""
        SELECTED.N=""
        ER.MSG.N=""

        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        TEXT = '��� ��������� ��������=':SELECTED.N ; CALL REM

        FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY3= '' ; E3 = ''
        CALL OPF(FN.FT,F.FT)
        IF SELECTED.N THEN
            FOR NN = 1 TO SELECTED.N
                VISA.NO.FT = '' ; CUST.NAME.FT = '' ; TRAN.DATE.FT = '' ; TRANS.AMT.FT = ''
                FT.ID = KEY.LIST.N<NN>
                CALL F.READ(FN.FT,FT.ID, R.FT, F.FT , EERE3)
                VISA.NO.FT = R.FT<FT.LOCAL.REF,FTLR.VISA.NO>
                CUST.FT = R.FT<FT.DEBIT.CUSTOMER>

                FN.CUSTOMER.FT = 'FBNK.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
                CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
                CALL F.READ(FN.CUSTOMER.FT, CUST.FT, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)
                CUST.NAME.FT = R.CUSTOMER.FT<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
                ACCT.FT = R.FT<FT.DEBIT.ACCT.NO>
                TRAN.DATE.FT = R.FT<FT.DEBIT.VALUE.DATE>
                TRANS.AMT.FT = R.FT<FT.DEBIT.AMOUNT>
******UPDATED ON 26/10/2008**********************
                TOT.FT = TOT.FT + TRANS.AMT.FT
*************************************************
                XX<1,ZZ>[1,16]= VISA.NO.FT
                XX<1,ZZ>[25,16]= ACCT.FT
                XX<1,ZZ>[48,35]= CUST.NAME.FT
                XX<1,ZZ>[90,8]= TRAN.DATE.FT
                XX<1,ZZ>[110,8]= TRANS.AMT.FT
                PRINT XX<1,ZZ>
                PRINT STR('-',132)
                ZZ=ZZ+1
                XX = ''
            NEXT NN
        END
    END   ;**END OF ELSE
*=====END OF UODATED 17/11/2008
*===========================================================
******UPDATED ON 30/03/2009********************************************************************
***************CREDIT PAYMENT FOR SETTLEMENT***************************************************
    SET.SEL = "SELECT F.SCB.VISA.SETT WITH TRANS.DATE EQ ": COMI :" BY CO.CODE "
    KEY.LIST.SET=""
    SELECTED.SET=""
    ER.MSG.SET=""


    CALL EB.READLIST(SET.SEL,KEY.LIST.SET,"",SELECTED.SET,ER.MSG.SET)
    TEXT = '��� ��������=':SELECTED.SET ; CALL REM
    IF SELECTED.SET THEN
        FOR ST=1 TO SELECTED.SET
            FN.SET = 'F.SCB.VISA.SETT' ; F.SET = '' ; R.SET = '' ; RETRY3= '' ; E3 = ''
            SETT.COMP = ''
            SET.ID = KEY.LIST.SET<ST>
            CALL OPF(FN.SET,F.SET)
            CALL F.READ(FN.SET,  SET.ID, R.SET, F.SET, E3)
            VISA.NO.SET = R.SET<SETT.CARD.NO>
            CUST.FT.SET = R.SET<SETT.CUST.NO>

            FN.CUSTOMER.SET = 'F.CUSTOMER' ; F.CUSTOMER.SET = '' ; R.CUSTOMER.SET = '' ; RETRY.SET = '' ; E.SET = ''
            CALL OPF(FN.CUSTOMER.SET,F.CUSTOMER.SET)
            CALL F.READ(FN.CUSTOMER.SET, CUST.FT.SET, R.CUSTOMER.SET, F.CUSTOMER.SET, E.SET)

            EMB.NA.SET = R.CUSTOMER.SET<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            TRANS.SET = R.SET<SETT.TRN.AMT>

            TOT.SET = TOT.SET + TRANS.SET
            PAY.DATE.SET = R.SET<SETT.TRANS.DATE>
            SETT.COMP = R.SET<SETT.CO.CODE>

            XX<1,ZZ>[1,16]= VISA.NO.SET
            XX<1,ZZ>[20,16]= ''
            XX<1,ZZ>[40,35]= EMB.NA.SET
            XX<1,ZZ>[80,8]= PAY.DATE.SET
            XX<1,ZZ>[95,8]= TRANS.SET
            XX<1,ZZ>[110,10] = SETT.COMP
            XX<1,ZZ>[125,5] = '�����'
            PRINT XX<1,ZZ>
            PRINT STR('-',132)
            ZZ=ZZ+1
            XX = ''
        NEXT ST
    END
*****UPDATED ON 10/05/2009**************************************
*****DEBIT SETTLMENT *********************************************
    S.SEL2 = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ 'ACVS' AND RECORD.STATUS EQ MAT AND AUTH.DATE EQ ":COMI : " BY CO.CODE"
    KEY.LIST.SET2=""
    SELECTED.SET2=""
    ER.MSG.SET2=""


    CALL EB.READLIST(S.SEL2,KEY.LIST.SET2,"",SELECTED.SET2,ER.MSG.SET2)
    FN.SET2 = 'FBNK.FUNDS.TRANSFER$HIS' ; F.SET2 = '' ; R.SET2 = '' ; RETRY4= '' ; E4 = ''
    CALL OPF(FN.SET2,F.SET2)
    IF SELECTED.SET2 THEN
        TEXT = 'Debit Payment=':SELECTED.SET2 ; CALL REM
        FOR ST2 = 1 TO SELECTED.SET2
            SET.ID2 = '' ;   VISA.NO.SET2 = '' ; CUST.NAME.SET2 = '' ; TRAN.DATE.SET2 = '' ; TRANS.AMT.SET2 = '' ; CUST.SET2 = ''
            SET2.COMP = ''
            SET.ID2 = KEY.LIST.SET2<ST2>
            CALL F.READ(FN.SET2,SET.ID2, R.SET2, F.SET2 , EERE4)
            VISA.NO.SET2 = R.SET2<FT.LOCAL.REF,FTLR.VISA.NO>
            CUST.SET2 = R.SET2<FT.DEBIT.CUSTOMER>
            FN.CUSTOMER.SET2 = 'FBNK.CUSTOMER' ; F.CUSTOMER.SET2 = '' ; R.CUSTOMER.SET2 = '' ; RETRY.SET2 = '' ; E.SET2 = ''
            CALL OPF(FN.CUSTOMER.SET2,F.CUSTOMER.SET2)
            CALL F.READ(FN.CUSTOMER.SET2, CUST.SET2, R.CUSTOMER.SET2, F.CUSTOMER.SET2, E.SET2)
            CUST.NAME.SET2 = R.CUSTOMER.SET2<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            ACCT.SET2 = R.SET2<FT.DEBIT.ACCT.NO>
            TRAN.DATE.SET2 = R.SET2<FT.DEBIT.VALUE.DATE>
            TRANS.AMT.SET2 = R.SET2<FT.DEBIT.AMOUNT>
            TOT.SET2 = TOT.SET2 + TRANS.AMT.SET2
            SET2.COMP = R.FT<FT.CO.CODE>
*************************************************
            XX<1,ZZ>[1,16]= VISA.NO.SET2
            XX<1,ZZ>[20,16]= ACCT.SET2
            XX<1,ZZ>[40,35]= CUST.NAME.SET2
            XX<1,ZZ>[80,8]= TRAN.DATE.SET2
            XX<1,ZZ>[95,8]= TRANS.AMT.SET2
            XX<1,ZZ>[110,8]= SET2.COMP
            XX<1,ZZ>[125,5]= 'Debit Payment'
            PRINT XX<1,ZZ>
            PRINT STR('-',132)
            ZZ=ZZ+1
            XX = ''

        NEXT ST2
    END   ;**END OF IF SELECTED.SET2
**UPDATED BY NESSREEN ON 10/05/2009*****************************
*****UPDATED ON 30/3/2009***************************************
*=====UPDATED ON 26/10/2008*************************************
** TOT.TRN.ALL = SELECTED + SELECTED.N
** TOT.AMT.ALL = TOT.TT + TOT.FT
***    TOT.TRN.ALL = SELECTED + SELECTED.N + SELECTED.SET
***    TOT.AMT.ALL = TOT.TT + TOT.FT + TOT.SET
    TOT.TRN.ALL = SELECTED + SELECTED.N + SELECTED.SET + SELECTED.SET2
    TOT.AMT.ALL = TOT.TT + TOT.FT + TOT.SET + TOT.SET2
    PRINT
    PRINT

    XX<1,ZZ>[1,16]= "������ ��� �������"
    XX<1,ZZ>[25,16]= TOT.TRN.ALL
    TEXT = 'TOT.NO=':TOT.TRN.ALL ; CALL REM
    XX<1,ZZ>[90,8]= "������ ������"
    XX<1,ZZ>[110,8]= TOT.AMT.ALL
    TEXT = 'TOT.AMT=':TOT.AMT.ALL ; CALL REM
    PRINT XX<1,ZZ>
    ZZ=ZZ+1
    XX = ''

**END
**********************************************************
    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TR.DATY = COMI
    T.TR.DAY = TR.DATY[7,2]:'/':TR.DATY[5,2]:"/":TR.DATY[1,4]

    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"������ ���������� ������ ������ ������ �� ���" :SPACE(1):T.TR.DAY
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ���������"::SPACE(10):"���� ������":SPACE(10):"����� ��������":SPACE(25):"����� ������":SPACE(10):"���� ������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(9):STR('_',12):SPACE(8):STR('_',15):SPACE(25):STR('_',12):SPACE(8):STR('_',10)
    HEADING PR.HD
    RETURN
*==============================================================

END
