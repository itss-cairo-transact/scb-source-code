* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 03/10/2016******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CR.TOT.EQVUSD(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
    KEY.LIST.M = "" ; SELECTED.M = "" ;  ER.MSG.M = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0 ; CUST.TR = 0

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM
*************##########TOTAL DEPOSIT PRIVATE   ##################******************
    M.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) AND CONTRACT.GRP NE '' BY CUSTOMER.1"
    CALL EB.READLIST(M.SEL, KEY.LIST.M, "", SELECTED.M, ASD.M)

    TEXT = "SEL=":SELECTED.M ; CALL REM
    FOR BB = 1 TO SELECTED.M
        CALL F.READ( FN.TT,KEY.LIST.M<BB>, R.TT,F.TT, ERR.TT)
        CUST<BB>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<BB>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<BB>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<BB>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<BB>     = R.TT<TT.TE.AUTH.DATE>
        TT.DATE = AUTH.DAT<BB>
        KEY.ID<BB> = "USD":"-" :"EGP":"-":TT.DATE
        CALL F.READ(FN.CURR.DALY,KEY.ID<BB>,R.CURR.DALY,F.CURR.DALY,ERR2)
        DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
        IF CURR<BB> = "USD" THEN
            TOT.AMT.M = TOT.AMT.M + AMT.FCY<BB>
        END ELSE
            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING MM THEN
                SELL.RATE<BB> = R.CURR.DALY<SCCU.SELL.RATE,MM>
            END
            AMT.EQV.USD = AMT.LCY<BB> / SELL.RATE<BB>
            TOT.AMT.M = TOT.AMT.M + AMT.EQV.USD
        END         ;** END OF OTHER FCY**
    NEXT BB
    AMT.EQV.USD = '' ; SELL.RATE = ''
*************##########END OF TOTAL DEPOSIT PRIVATE##############******************
*************##########TOTAL DEPOSIT CORPORATE ##################******************
    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) AND CONTRACT.GRP EQ '' AND CUSTOMER.1 NE '99433300' BY CUSTOMER.1"
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL2=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    FOR I = 1 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        TT.DATE = AUTH.DAT<I>

        KEY.ID<I> = "USD":"-" :"EGP":"-":TT.DATE
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
        LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
            SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
        END
        IF CURR<I> = "USD" THEN
            TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
        END ELSE
            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
            END
            AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
            TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
        END         ;** END OF OTHER FCY**
    NEXT I
***********########END OF TOTAL DEPOSIT CORPORATE###########******************
    Y.RET.DATA<-1> = ST.DATE:"*":EN.DATE:"*":SELECTED.M :"*":TOT.AMT.M :"*":SELECTED.N :"*":TOT.AMT.N

    RETURN
END
