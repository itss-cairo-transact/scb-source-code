* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    PROGRAM CR.MOKHSAS.CR

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.LD.LOANS.AND.DEPOSITS
    $INSERT T24.BP I_F.LETTER.OF.CREDIT
    $INSERT T24.BP I_F.MM.MONEY.MARKET
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.DATES
    $INSERT T24.BP I_F.DRAWINGS
    $INSERT           I_F.SCB.FUND
    $INSERT           I_FT.LOCAL.REFS
    $INSERT           I_F.SCB.CUS.POS.TST
    $INSERT           I_CU.LOCAL.REFS
*--------------------------------------------------------*
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

    CALL OPF( FN.ACCOUNT,F.ACCOUNT)
*-----------------------------*
    GOSUB CR.SUB
    RETURN
*-----------------------------*
CR.SUB:
*------
    ID.NO = ""
    R.LETTER.OF.CREDIT = ''
    F.LETTER.OF.CREDIT = ''
    FN.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
    FN.DRAWINGS = 'FBNK.DRAWINGS' ; F.DRAWINGS = '' ; R.DRAWINGS = ''

    CALL OPF( FN.DRAWINGS,F.DRAWINGS)
    CALL OPF( FN.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT)

    DAT.ID = "EG0010001"
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,BNK.DATE1)

    T.SEL4 = "SELECT FBNK.DRAWINGS WITH MATURITY.REVIEW GT " : BNK.DATE1

    KEY.LISTBRTEST4=""
    SELECTED4=""
    ER.MSG4  =""
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
    PRINT SELECTED4
    IF KEY.LIST4 THEN
        FOR I = 1 TO SELECTED4
            CALL F.READ(FN.DRAWINGS,KEY.LIST4<I>, R.DRAWINGS,F.DRAWINGS, ETEXT1)
            CALL F.READ(FN.LETTER.OF.CREDIT,KEY.LIST4<I>[1,12], R.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT, ETEXT111)
            APL.NO = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.CUSTNO>
            BEN.NO = R.LETTER.OF.CREDIT<TF.LC.BENEFICIARY.CUSTNO>

            IF APL.NO EQ ''  THEN
                CUS.NOOO = BEN.NO
            END ELSE
                CUS.NOOO = APL.NO
            END
            CALL F.READ(FN.CUSS,CUS.NOOO, R.CUSS,F.CUSS, ETEXT1)
            CATTT = R.LETTER.OF.CREDIT<TF.LC.CATEGORY.CODE>
            ID.NO = CUS.NOOO : "*DR*BNK*" : R.DRAWINGS<TF.DR.DRAW.CURRENCY> :CATTT :"*" : KEY.LIST4<I> : "*" : TODAY

            CURR  = R.DRAWINGS<TF.DR.DRAW.CURRENCY>
            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100
            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            AMT = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>

            LCY.AMT  = (MID.RATE<1,1> * AMT)
            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>    = LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER>      = CUS.NOOO
            R.COUNT<CUPOS.DEAL.CCY>      = R.DRAWINGS<TF.DR.DRAW.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT>   = AMT
            R.COUNT<CUPOS.MATURITY.DATE> = R.DRAWINGS<TF.DR.MATURITY.REVIEW>
            R.COUNT<CUPOS.VALUE.DATE>    = R.DRAWINGS<TF.DR.MATURITY.REVIEW>
            R.COUNT<CUPOS.CATEGORY>      = CATTT
            R.COUNT<CUPOS.SYS.DATE> = TODAY
            R.COUNT<CUPOS.CO.CODE> =  R.LETTER.OF.CREDIT<TF.LC.CO.CODE>

            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
    RETURN
END
