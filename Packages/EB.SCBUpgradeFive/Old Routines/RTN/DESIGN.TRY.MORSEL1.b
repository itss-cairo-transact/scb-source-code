* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-189</Rating>
*-----------------------------------------------------------------------------
********* WAEL*********

    SUBROUTINE DESIGN.TRY.MORSEL1(OPER.CODE,YYBRN,BENF1,BENF2,ADDR1,ADDR2,ADDR3,ADDR,ADDR.COUNT,TYPE.NAME,LG.NO,LG.AMT,CRR,MYCODE,THIRD.NAME1,THIRD.NAME2,THRD.ADDR1,THRD.ADDR2,THIRD.ADDR1,THIRD.ADDR2,LG.CUST1,LG.CUST2,K)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

    GOSUB PRINT.HEAD

***  IF MYCODE = "1111" THEN
    IF MYCODE NE '' THEN
        GOSUB BENF.HEAD
*END ELSE
* GOSUB CUST.HEAD
    END

    RETURN
*===========================================================================
PRINT.HEAD:
    PR.HD ="'L'":SPACE(1):"��� ���� �����������"
   *** PR.HD :="'L'":SPACE(1):"����������":":":YYBRN
    PR.HD :="'L'":SPACE(1):YYBRN
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD
    RETURN
*=================
BENF.HEAD:
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2
    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12 ;LNTHD3 = LEN(ADDR3)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2 ;LNED3 = 50-LNTHD3
    PRINT

    PRINT " ________________________________________________"
    PRINT "| ������� : ":"��� ���� ������":SPACE(LNE1):"|"
    PRINT "|         : ":"��� :":BENF1:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
****************************************
* FOR I=1 TO ADDR.COUNT
*    SAM = FIELD(ADDR, "|", I)
*    LEN1 = LEN(SAM) + 12
*    LEN2 = 50       - LEN1
*    IF I = 1 THEN
*        PRINT "| ������� : ":SAM:SPACE(LEN2):"|"
*    END ELSE
*        PRINT "|         : ":SAM:SPACE(LNE2):"|"
*    END
*NEXT I
****************************************
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|         : ":ADDR3:SPACE(LNED3):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ��� � �: ":LG.NO: "(":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "������������ : ":" **":LG.AMT:"**":" ":CRR
    PRINT

    IF MYCODE[2,3]= "111" THEN
*** PRINT SPACE(20): "����� ��� ����� ������ : ":THIRD.NAME1
        PRINT SPACE(20): "�����:":THIRD.NAME1
        PRINT SPACE(20): "     :":THIRD.NAME2
       * IF K = 1 THEN
            PRINT SPACE(20): "� �����: "    :LG.CUST1
            PRINT SPACE(20): "              : ":LG.CUST2
       * END
** END ELSE
**   PRINT SPACE(20): "������� � ����� : ":THIRD.NAME1
**   PRINT SPACE(20): "                : ":THIRD.NAME2
* END

        PRINT
        PRINT SPACE(20):"________________________________________________"
        RETURN
*=================
CUST.HEAD:

        LNTHD1 = LEN(THIRD.NAME1)+12 ; LNTHD2 = LEN(THIRD.NAME2)+12
        THRD.NAME1 = 50-LNTHD1 ; THRD.NAME2 = 50-LNTHD2

        LNTHD1 = LEN(THIRD.ADDR1)+12 ; LNTHD2 = LEN(THIRD.ADDR2)+12
        THRD.ADDR1 = 50-LNTHD1 ; THRD.ADDR2 = 50-LNTHD2

        PRINT
        PRINT "__________________________________________________"
        PRINT "| ������� : ":THIRD.NAME1:SPACE(THRD.NAME1):"|"
        PRINT "|         : ":THIRD.NAME2:SPACE(THRD.NAME2):"|"
        PRINT "|":SPACE(49):"|"
        PRINT "| ������� : ":THIRD.ADDR1:SPACE(THRD.ADDR1):"|"
        PRINT "|         : ":THIRD.ADDR2:SPACE(THRD.ADDR2):"|"
        PRINT "|_________________________________________________|"
        PRINT
        PRINT
        PRINT SPACE(20): "���� ���� ��� : � �":LG.NO:" (":TYPE.NAME:")"
        PRINT
        PRINT SPACE(20): "������������� : ":" **":LG.AMT:"**":" ":CRR
        PRINT

        IF MYCODE[2,3]= "111" THEN
***PRINT SPACE(20): "����� ��� ����� ������ : ":BENF1
            PRINT SPACE(20): "������� � ����� :":BENF1
            PRINT SPACE(20): "              : ":BENF2
        END ELSE
            PRINT SPACE(20): "������� � ����� : ":BENF1
            PRINT SPACE(20): "                : ":BENF2
        END
        PRINT SPACE(20): "________________________________________________"
        RETURN
*==================
    END
