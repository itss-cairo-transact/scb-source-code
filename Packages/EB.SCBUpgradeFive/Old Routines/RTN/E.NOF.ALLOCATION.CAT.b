* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>748</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.ALLOCATION.CAT(Y.ALL.REC)

***Mahmoud Elhawary******9/5/2010*****************************
* Nofile routine to get transactions and balances            *
* of accounts from a group of categories                     *
**************************************************************

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
**    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    KK = 0
    XX = SPACE(120)
    STE.BAL  = ''
    CAT.GRP  = ''
    CAT.GRP1 = '12110 12111 12112 12113 12114 12115 12116 12117 12118'
    CAT.GRP2 = '12410 12415 12425 12435 12513 12540 12625'
    CAT.GRP3 = '17003 17004 17202 17203 17204 17205 17206'
    CAT.GRP4 = '12210 12211'
    CAT.GRP5 = '12014 12028 12037 12036 12033 12031 12040 12043 12044 12048 12054 18145 18146 12067 12068 12069 12070 12038 12057 12071'
    CAT.GRP6 = '12210 12211 11373 11378 1701 11160 11161 11162 11163 11164 11165 11166 11167 11168 11170 11171 11172 11173 11174 11175 11076 1721 11027 11029 11254 11351 11354 11358 11362 11369 11512 11387 11390 11068 11069 11352 11017 11019 11022 11023 11028 11033 11035 11048 11061 11062 11070 11212 11215 11216 11218 11222 11224 11225 11227 11281 11292 11140 11039 11031 11301 10300 10301 11036 11214 11251 11253 11260 11272 11274 11391 11400 11042 11241 11313 11020 11024 11047 11355 11259 11392 11410 13100'
    CAT.GRP7 = '16601 16602 16603 16604 16605 16606 16607 16608 16609 16610 16611 16612 11078 11079 11080 11075 11077 1021 1022 11045 16110 16120 16123 16124 16520 11054 16125 11052 16122 16374 16377 16378 16380 16381 16382 16384 16385 16386 16387 16201 16388 18117 16379 16506 3050 10200 10201 11050 11058 16112 16121 16189 16205 16209 16301 16318 16389 16401 16426 16500 16504 16510 16511 16514 16521 16522 16523 16528 16529 16530 16531 16532 16534 16535 16536 16538 16533 16307 16422 16541 10800 16540 16537 11057 16508 16513 16187 16254 3020 16210 16208 16250 16111 16303 16304 16305 16306 16383 16524 16539 16154 16410 3205 14301 3206 3208 13100'
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CATEGORY.GROUP" IN D.FIELDS<1> SETTING YGRP.POS THEN CATEG.GROUP  = D.RANGE.AND.VALUE<YGRP.POS> ELSE RETURN
    LOCATE "START.DATE" IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE  = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE" IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    BEGIN CASE
    CASE CATEG.GROUP EQ 1
        CAT.GRP = CAT.GRP1
    CASE CATEG.GROUP EQ 2
        CAT.GRP = CAT.GRP2
    CASE CATEG.GROUP EQ 3
        CAT.GRP = CAT.GRP3
    CASE CATEG.GROUP EQ 4
        CAT.GRP = CAT.GRP4
    CASE CATEG.GROUP EQ 5
        CAT.GRP = CAT.GRP5
    CASE CATEG.GROUP EQ 6
        CAT.GRP = CAT.GRP6
    CASE CATEG.GROUP EQ 7
        CAT.GRP = CAT.GRP7
    CASE OTHERWISE
        RETURN
    END CASE

**    CC.SEL  = "SELECT ":FN.ACCT:" WITH CO.CODE EQ ":COMP

    CC.SEL  = "SELECT ":FN.ACCT:" WITH CATEGORY IN (":CAT.GRP:")"
    CC.SEL := " BY @ID BY CATEGORY"

    CALL EB.READLIST(CC.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE ACCT.ID FROM K.LIST SETTING POS.ACC
        WHILE ACCT.ID:POS.ACC
            CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
*****************************
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
            STE.BAL = OPENING.BAL
            TOT.DR.MVMT = 0
            TOT.CR.MVMT = 0
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS
            WHILE STE.ID:POS
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.CUR = R.STE<AC.STE.CURRENCY>
                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                STE.VAL = R.STE<AC.STE.VALUE.DATE>
                STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                COMP = R.STE<AC.STE.COMPANY.CODE>
		STE.EX.RATE = R.STE<AC.STE.EXCHANGE.RATE>


                CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,STE.TXN,STE.TRN)
                IF STE.CUR NE 'EGP' THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                STE.BAL += STE.AMT
                IF STE.AMT LT 0 THEN
                    TOT.DR.MVMT += STE.AMT
                END ELSE
                    TOT.CR.MVMT += STE.AMT
                END
                IF STE.ID THEN
                    GOSUB RET.DATA
                END
            REPEAT
        REPEAT
    END
    RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = COMP:"*":CATEG.GROUP:"*":ACCT.ID:"*":FROM.DATE:"*":END.DATE:"*":OPENING.BAL:"*":STE.DAT:"*":STE.AMT:"*":STE.BAL:"*":STE.ID:"*":STE.OUR:"*":STE.TXN:"*":STE.TRN:"*":STE.VAL:"*":STE.ID:"*":STE.CUR:"*":STE.EX.RATE
    RETURN
**************************************
END
