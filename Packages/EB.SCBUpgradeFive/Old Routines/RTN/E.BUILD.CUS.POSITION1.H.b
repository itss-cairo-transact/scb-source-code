* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE E.BUILD.CUS.POSITION1.H(ENQUIRY.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''


** 19/12/97 - GB9701470
**            Build the data dependent on the rebuild flag being set
*
*  08/06/05 - EN_10002549
*             Set C$CUS.POS.UPDATE.XREF to 0, so that cache is used
*             instead on CUSTOMER.POSITION.XREF.

    LOCATE 'REBUILD.DATA' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        REBUILD = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END
    IF REBUILD[1,1] NE 'N' THEN         ;* Do not execute on level dwon
        LOCATE "CUSTOMER.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
            CUST.ID = ENQUIRY.DATA<4,CUS.POS>
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CONVERT " " TO @VM IN CUST.ID

*Line [ 52 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF DCOUNT(CUST.ID,@VM) GT 1 OR CUST.ID = "ALL" THEN
                ENQ.ERROR = "ONLY ONE CUSTOMER ALLOWED"
            END ELSE
*********************************************
                FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
                CALL OPF(FN.CUS,F.CUS)
                KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""



                CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,READ.ERR1)

                LOOP
                    REMOVE AC.ID FROM R.CUS SETTING POS.CUST
                WHILE AC.ID:POS.CUST

                    IF ( AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 ) OR ( AC.ID[11,4] EQ 1710 OR AC.ID[11,4] EQ 1711 ) THEN
                        CUST.ID = "DUMMY"
                    END

                    CALL OPF( FN.ACCOUNT,F.ACC)
                    CALL F.READ(FN.ACCOUNT,AC.ID,R.ACC,F.ACC, ETEXT)
                    SUSP = R.ACC<AC.INT.NO.BOOKING>

                    IF  SUSP EQ "SUSPENSE"  THEN
                        CUST.ID = "DUMMY"
                    END
                REPEAT


********************************************************

                C$CUST.POS.UPDATE.XREF = 0        ;* EN_10002549
                CALL CUS.BUILD.POSITION.DATA(CUST.ID)
                C$CUST.POS.UPDATE.XREF = 1        ;* EN_10002549 Reset to 1
            END
        END
    END
*******UPDATED BY NESSREEN AHMED ON 30/12/2008**********************
    FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...*1002)"

    CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)
***Updated by Nessreen Ahmed 25/9/2016 *************************
        DELETE F.CUS.POS , KEY.LIST<I>
***     CALL F.DELETE (FN.CUS.POS , KEY.LIST<I>)
***End of Update 25/9/2016 9 ****************************
    NEXT I

*************************************************************

    FN.CUS.POSN = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POSN = '' ; R.CUS.POSN = ''
    CALL OPF(FN.CUS.POSN,F.CUS.POSN)
    KEY.LISTN="" ; SELECTEDN="" ; ER.MSGN=""

    T.SELLN = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID:"... AND DEAL.AMOUNT EQ '' "

    CALL EB.READLIST(T.SELLN,KEY.LISTN"",SELECTEDN,ER.MSGN)
    TEXT = "SELECTEDN" : SELECTEDN ; CALL REM
    FOR II = 1 TO SELECTEDN
        CALL F.READ(FN.CUS.POSN,KEY.LISTN<II>,R.CUS.POSN,F.CUS.POSN,READ.ERRN)
***Updated by Nessreen Ahmed 25/9/2016 *************************
        DELETE F.CUS.POS , KEY.LIST<II>
***     CALL F.DELETE (FN.CUS.POS , KEY.LIST<II>)
***End of Update 25/9/2016 9 ****************************
    NEXT II
******************************************
    RETURN

END
