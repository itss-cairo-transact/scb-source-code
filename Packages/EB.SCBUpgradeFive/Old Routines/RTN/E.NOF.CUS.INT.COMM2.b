* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1271</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CUS.INT.COMM2(Y.RET.DATA)
*#    PROGRAM E.NOF.CUS.INT.COMM2
****Mahmoud Elhawary******3/8/2010****************************
*   nofile routine to get customer debit interest & charges  *
**************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
    GOSUB INITIATE
*Line [ 54 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    CUS.ID    = ''
    CTE.CAT   = ''
    CTE.CUR   = ''
    CTE.BAL   = 0
    MNTH.NUM  = ''
    NO.OF.MONTHS = ''
    TOTAL.INT = 0
    TOTAL.COM = 0
    TOT.INT.ALL = 0
    TOT.COM.ALL = 0
    AR.CUS.COMP = ''
    XX = ''
    KK = 0
    CC = 0
    CATEG.GRP = '51001.51000.52053.52414.52050.52052.52054.52057.52058.52452.52459.52453.52300.52040.52301.52302.52303.52304.52305.52306.52307.52001.52028.52315.52309.52405.52042.52317.52406.52407.52413.52400.52161.52152.52163.52158.52878.52151.52157.52110.52218.52871.52872.52514.52006.52114.52251.52875.52882.52015.52113.52877.52605.52026.52109.52213.52014.52620.52205.52515.52538.52517.52518.52212.52516.52271.52535.52502.52503.52505.52534.52513.52270.52272.52011.52025.52156.52172.52203.54025.52106.52051.52101.'
    CATEG.NUM = COUNT(CATEG.GRP,'.')
    CRT CATEG.NUM
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.CUS.H = 'FBNK.CUSTOMER$HIS' ; F.CUS.H = '' ; R.CUS.H = '' ; ER.CUS.H = ''
    CALL OPF(FN.CUS.H,F.CUS.H)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.CAT = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = '' ; ER.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CTE = 'FBNK.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.SDR = 'FBNK.STMT.ACCT.DR' ; F.SDR = '' ; R.SDR = '' ; ER.SDR = ''
    CALL OPF(FN.SDR,F.SDR)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO" IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

*#    PROMPT 'CUSTOMER  : ' ;INPUT CUS.ID
*#    PROMPT 'START.DATE: ' ;INPUT FROM.DATE
*#    PROMPT 'END.DATE  : ' ;INPUT END.DATE

    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
    CUS.COM = R.CUS<EB.CUS.COMPANY.BOOK>
    CUS.LCL = R.CUS<EB.CUS.LOCAL.REF>
    CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
    CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
    CRNT.NO = R.CUS<EB.CUS.CURR.NO>
    IF CUS.COM NE COMP THEN
        RETURN
    END
    IF CRD.STA NE '' THEN
        RETURN
    END
    IF CRD.COD EQ 110 OR CRD.COD EQ 120 THEN
        RETURN
    END
*#################################
    CC = 1
    AR.CUS.COMP<CC> = CUS.COM
    GOSUB GET.CUS.COMP
    CO.NUM = DCOUNT(AR.CUS.COMP,@FM)
    CRT "CO.NUM = ":CO.NUM
*#################################
    CUR.SEL = "SELECT ":FN.CUR
    CALL EB.READLIST(CUR.SEL,K.CUR,'',SELECTED.CUR,ERRR1)
    FOR MCAT = 1 TO CATEG.NUM
        CTE.BAL = 0
        CAT.ID = FIELD(CATEG.GRP,'.',MCAT)
        FOR AX1 = 1 TO SELECTED.CUR
            CUR.ID = K.CUR<AX1>
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CAT.ID,CAT.NAME)
            IF CAT.ID EQ '51000' OR CAT.ID EQ '52006' THEN
                IF CAT.ID EQ '51000' THEN
                    GOSUB GET.CUS.ACC
                    CTE.BAL = TOT.INT.ALL
                END
                IF CAT.ID EQ '52006' THEN
                    GOSUB GET.CUS.ACC
                    CTE.BAL = TOT.COM.ALL
                END
            END ELSE
                FOR CO1 = 1 TO CO.NUM
                    ID.COMPANY = AR.CUS.COMP<CO1>
                    CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,END.DATE,CAT.ID,CATEG.LIST)
                    LOOP
                        REMOVE CTE.ID FROM CATEG.LIST SETTING POS.CTE.ID
                    WHILE CTE.ID:POS.CTE.ID
                        CALL F.READ(FN.CTE,CTE.ID,R.CTE,F.CTE,ER.CTE)
                        CTE.CUR = R.CTE<AC.CAT.CURRENCY>
                        CTE.AMT = R.CTE<AC.CAT.AMOUNT.LCY>
                        CTE.DAT = R.CTE<AC.CAT.BOOKING.DATE>
                        CTE.CUS = R.CTE<AC.CAT.CUSTOMER.ID>
                        IF CTE.CUR NE 'EGP' THEN
                            CTE.AMT = R.CTE<AC.CAT.AMOUNT.FCY>
                        END
                        IF CTE.CUS EQ CUS.ID THEN
                            IF CTE.CUR EQ CUR.ID THEN
                                CTE.BAL += CTE.AMT
                            END
                        END
                    REPEAT
                NEXT CO1
            END
            IF CTE.BAL NE 0 THEN
                GOSUB RET.DATA
            END
        NEXT AX1
    NEXT MCAT
    RETURN
************************************************************************
GET.CUS.ACC:
*-----------
    TOT.INT.ALL = 0
    TOT.COM.ALL = 0
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ERR.CUS.ACC)
    LOOP
        REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
    WHILE ACCT.ID:POS.CUS.ACC
        TOTAL.INT = 0
        TOTAL.COM = 0
        GOSUB GET.DR.INT
        TOT.INT.ALL += TOTAL.INT
        TOT.COM.ALL += TOTAL.COM
    REPEAT
    RETURN
************************************************************************
GET.DR.INT:
*----------
    CALL MONTHS.BETWEEN(FROM.DATE,END.DATE,NO.OF.MONTHS)
    MNTH.NUM = NO.OF.MONTHS + 1
    FRDT = FROM.DATE
    FOR XX = 1 TO MNTH.NUM
        CALL LAST.DAY(FRDT)
        SDR.ID = ACCT.ID:"-":FRDT
        CALL F.READ(FN.SDR,SDR.ID,R.SDR,F.SDR,Y.SDR.ERR)
        SDR.DATE = FIELD(SDR.ID,'-',2)
        TOT.INT = R.SDR<IC.STMDR.TOTAL.INTEREST>
        TOT.COM = R.SDR<IC.STMDR.TOTAL.CHARGE>
        INT.DT  = R.SDR<IC.STMDR.DR.INT.DATE>
        LIQ.CCY = R.SDR<IC.STMDR.LIQUIDITY.CCY>
        M.RATE  = R.SDR<IC.STMDR.USED.MIDDLE.RATE>
*        IF LIQ.CCY NE 'EGP' THEN
*            TOT.INT = TOT.INT * M.RATE
*            TOT.COM = TOT.COM * M.RATE
*        END
        IF LIQ.CCY EQ CUR.ID THEN
            TOTAL.COM += TOT.COM
            IF SDR.DATE[1,6] NE FROM.DATE[1,6] AND SDR.DATE[1,6] NE END.DATE[1,6] THEN
                TOTAL.INT += TOT.INT
            END ELSE
*Line [ 219 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                SS = DCOUNT(INT.DT,@VM)
                FOR II = 1 TO SS
                    INT.DR.DT  = R.SDR<IC.STMDR.DR.INT.DATE,II>
*                IF INT.DR.DT GE FROM.DATE AND INT.DR.DT LE END.DATE THEN
*                    IF LIQ.CCY NE 'EGP' THEN
*                        TOTAL.INT +=  R.SDR<IC.STMDR.DR.INT.AMT,II> * R.SDR<IC.STMDR.USED.MIDDLE.RATE>
*                    END ELSE
*                        TOTAL.INT +=  R.SDR<IC.STMDR.DR.INT.AMT,II>
*                    END
*                END
                    IF LIQ.CCY EQ CUR.ID THEN
                        TOTAL.INT +=  R.SDR<IC.STMDR.DR.INT.AMT,II>
                    END
                NEXT II

            END
            CALL ADD.MONTHS(FRDT,'1')
        END
    NEXT XX
    RETURN
************************************************************************
GET.CUS.COMP:
*-----------
    FOR CU.H = 1 TO CRNT.NO
        CUS.ID.H = CUS.ID:";":CU.H
        CRT CUS.ID.H
        CALL F.READ(FN.CUS.H,CUS.ID.H,R.CUS.H,F.CUS.H,ER.CUS.H)
        IF NOT(ER.CUS.H) THEN
            CUS.COM.H = R.CUS.H<EB.CUS.COMPANY.BOOK>
            CRT CUS.COM.H
*Line [ 250 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUS.COM.H IN AR.CUS.COMP<1> SETTING POS1.COMP THEN '' ELSE
                CC++
                AR.CUS.COMP<CC> = CUS.COM.H
            END
        END
    NEXT CU.H
    CRT AR.CUS.COMP
    RETURN
************************************************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = CUS.COM:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":CAT.ID:"*":CUR.ID:"*":CTE.BAL
    CTE.BAL = 0
*#    KK++
*#    XX<1,KK>[1 ,15] = CAT.ID
*#    XX<1,KK>[20,30] = CAT.NAME
*#    XX<1,KK>[55,20] = FMT(CTE.BAL,"L2,")
*#    PRINT XX<1,KK>
    RETURN
**************************************
END
