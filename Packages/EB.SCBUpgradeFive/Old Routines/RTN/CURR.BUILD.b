* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-------------------------------NI7OOOOOOOOOOOO----------------------------------------------
    SUBROUTINE CURR.BUILD(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GL.CURR
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GL.CUR.BAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    FN.CURR ='F.SCB.GL.CURR'
    F.CURR  =''
    CALL OPF(FN.CURR,F.CURR)

    FN.CURR.BAL ='F.SCB.GL.CUR.BAL'
    F.CURR.BAL   =''
    CALL OPF(FN.CURR.BAL,F.CURR.BAL)
   XX = ''
** XX = '20100421'
    XX1 = 1:'-':XX
    XX2 = 17:'-':XX
    XX5 = 28:'-':XX
    XX3 = 19:'-':XX
    XX6 = 31:'-':XX
    XX4 = 26:'-':XX
    XX7 = 33:'-':XX

    COMP = ID.COMPANY
    ***TEXT = COMP ; CALL REM
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,COMP,TD)
    ***TEXT = TD ; CALL REM
    XX = '...' : TD
   ** XX = '...20100831'
    ***TEXT = XX ; CALL REM
    T.SEL="SELECT F.SCB.GL.CUR.BAL.MARKZY WITH @ID LIKE ":XX:" AND NEWNO EQ 1 "
***    T.SEL="SELECT F.SCB.GL.CUR.BAL WITH @ID UNLIKE 10-... OR @ID UNLIKE 4-... OR @ID UNLIKE -... OR @ID UNLIKE
***  T.SEL="SELECT F.SCB.GL.CUR.BAL WITH @ID LIKE ...20100421"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ***TEXT = SELECTED ; CALL REM
* CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
*TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
