* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-55</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.TOT.PL.TYP.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
**----------------------------------------
    CCY         = 'EGP'
    AMT.FCY     = 0
    CUR.CAT     = 0
    TOT.FCY.AMT = 0
    CURR.O.DATA = CCY

    TOT.FCY.AMT.MON.1 = 0
    TOT.FCY.AMT.MON.2 = 0
    TOT.FCY.AMT.MON.3 = 0
    TOT.FCY.AMT.MON.4 = 0
    TOT.FCY.AMT.MON.5 = 0

    TYP = O.DATA

    YF.CATEG.MONTH = "F.CATEG.MONTH"
    F.CATEG.MONTH = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)
*
    YF.CATEG.ENTRY = "F.CATEG.ENTRY"
    F.CATEG.ENTRY = ""
    CALL OPF(YF.CATEG.ENTRY,F.CATEG.ENTRY)
*
    YF.CATEGORY = "F.CATEGORY"
    F.CATEGORY = ""
    CALL OPF(YF.CATEGORY,F.CATEGORY)
*
    F.CATEG.ENT.TODAY = ""
    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)
*
    F.CATEG.ENT.FWD = ''
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD'
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    FN.COMP = 'F.COMPANY'     ;  F.COMP = '' ; R.COMP  = '' ; ETEXT = ''
    CALL OPF( FN.COMP,F.COMP)
**--------------------------------------------------------------------**
    IF TYP EQ 1 THEN
        GOSUB GET.TOT.MON.1
        O.DATA   =  TOT.FCY.AMT.MON.1
    END

    IF TYP EQ 2 THEN
        GOSUB GET.TOT.MON.2
        O.DATA   =  TOT.FCY.AMT.MON.2
    END

    IF TYP EQ 3 THEN
        GOSUB GET.TOT.MON.3
        O.DATA   =  TOT.FCY.AMT.MON.3
    END

    IF TYP EQ 4 THEN
        GOSUB GET.TOT.MON.4
        O.DATA   =  TOT.FCY.AMT.MON.4
    END

    IF TYP EQ 5 THEN
        GOSUB GET.TOT.MON.1
        GOSUB GET.TOT.MON.2
        GOSUB GET.TOT.MON.3
        GOSUB GET.TOT.MON.4

        TOT.FCY.AMT.MON.5 = TOT.FCY.AMT.MON.1 + TOT.FCY.AMT.MON.2 + TOT.FCY.AMT.MON.3 + TOT.FCY.AMT.MON.4

        O.DATA   =  TOT.FCY.AMT.MON.5
    END
    RETURN
**----------------------------------------------------------------------
GET.TOT.MON.1:
*-------------
    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-1')
    TO.DATE     = TODAY

    T.SEL2 = "SELECT F.COMPANY "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)
    FOR XX = 1 TO SELECTED2
        ID.COMPANY  = KEY.LIST2<XX>
        CHARGE.CODE = "CHQCOOLLN"
        CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,CATEG.PL)
        Y.CAT.NO    = CATEG.PL

        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE

            CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)
            CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
            CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
            V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
            B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
            LCY.AMT  = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>

            CUST.ID  = YR.CATEG.ENTRY<AC.CAT.CUSTOMER.ID>
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SECT)

            IF LCY.AMT EQ 300 AND SECT NE 1100 THEN
                TOT.FCY.AMT.MON.1 = TOT.FCY.AMT.MON.1 + LCY.AMT
            END
            IF LCY.AMT EQ 150 AND SECT EQ 1100 THEN
                TOT.FCY.AMT.MON.1 = TOT.FCY.AMT.MON.1 + LCY.AMT
            END
        REPEAT
    NEXT XX
    RETURN
*-----------------------------------------------
GET.TOT.MON.2:
*-------------
    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-1')
    TO.DATE     = TODAY

    CHARGE.CODE = "CHQCOOLLN"
    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,CATEG.PL)
    Y.CAT.NO    = CATEG.PL

    T.SEL2 = "SELECT F.COMPANY"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)
    FOR XX = 1 TO SELECTED2
        ID.COMPANY  = KEY.LIST2<XX>

        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE

            CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)
            CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
            CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
            V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
            B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
            LCY.AMT  = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>

            CUST.ID  = YR.CATEG.ENTRY<AC.CAT.CUSTOMER.ID>
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SECT)

            IF LCY.AMT EQ 400 AND SECT NE 1100 THEN
                TOT.FCY.AMT.MON.2 = TOT.FCY.AMT.MON.2 + LCY.AMT
            END
            IF LCY.AMT EQ 200 AND SECT EQ 1100 THEN
                TOT.FCY.AMT.MON.2 = TOT.FCY.AMT.MON.2 + LCY.AMT
            END
        REPEAT
    NEXT XX
    RETURN
*----------------------------------------------
GET.TOT.MON.3:
*-------------
    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-1')
    TO.DATE     = TODAY

    T.SEL2 = "SELECT F.COMPANY "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)
    FOR XX = 1 TO SELECTED2
        ID.COMPANY  = KEY.LIST2<XX>
        CHARGE.CODE = "CHQCOOLLN"
        CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,CATEG.PL)
        Y.CAT.NO    = CATEG.PL

        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE

            CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)
            CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
            CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
            V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
            B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
            LCY.AMT  = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>

            CUST.ID  = YR.CATEG.ENTRY<AC.CAT.CUSTOMER.ID>
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SECT)

            IF LCY.AMT EQ 600 AND SECT NE 1100 THEN
                TOT.FCY.AMT.MON.3 = TOT.FCY.AMT.MON.3 + LCY.AMT
            END
            IF LCY.AMT EQ 300 AND SECT EQ 1100 THEN
                TOT.FCY.AMT.MON.3 = TOT.FCY.AMT.MON.3 + LCY.AMT
            END
        REPEAT
    NEXT XX
    RETURN
*----------------------------------------------------------
GET.TOT.MON.4:
*-------------
    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-1')
    TO.DATE     = TODAY

    T.SEL2 = "SELECT F.COMPANY "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)
    FOR XX = 1 TO SELECTED2
        ID.COMPANY  = KEY.LIST2<XX>
        CHARGE.CODE = "CHQCOOLLN"
        CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,CATEG.PL)
        Y.CAT.NO    = CATEG.PL

        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE

            CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)
            CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
            CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
            V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
            B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
            LCY.AMT  = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>

            CUST.ID  = YR.CATEG.ENTRY<AC.CAT.CUSTOMER.ID>
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SECT)

            IF LCY.AMT EQ 1000 AND SECT NE 1100 THEN
                TOT.FCY.AMT.MON.4 = TOT.FCY.AMT.MON.4 + LCY.AMT
            END
            IF LCY.AMT EQ 500 AND SECT EQ 1100 THEN
                TOT.FCY.AMT.MON.4 = TOT.FCY.AMT.MON.4 + LCY.AMT
            END
        REPEAT
    NEXT XX
    RETURN
**---------------------------------------------------------------------
GET.TOT.MON.5:
*-------------
    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-1')
    TO.DATE     = TODAY

    T.SEL2 = "SELECT F.COMPANY"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)
    FOR XX = 1 TO SELECTED2-1
        ID.COMPANY  = KEY.LIST2<XX>
        CHARGE.CODE = "CHQCOOLLN"

        CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,CATEG.PL)
        Y.CAT.NO    = CATEG.PL

        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE

            CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)
            CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
            CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
            V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
            B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
            LCY.AMT  = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>
            CUST.ID  = YR.CATEG.ENTRY<AC.CAT.CUSTOMER.ID>
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SECT)

            IF CUST.ID AND SECT NE '' THEN
                TOT.FCY.AMT.MON.5 = TOT.FCY.AMT.MON.5 + LCY.AMT
            END
        REPEAT
    NEXT XX
    RETURN
*------------------------------------------------------
END
