* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
************MAHMOUD ******************************
*-----------------------------------------------------------------------------
* <Rating>7870</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CBE.DEPOSITS.3(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.RANGE
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*Line [ 66 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB INITIATE
    GOSUB GET.CURR
    RETURN
*========================================
INITIATE:
*-------
    K  = 0
    K2 = 0
    K3 = 0
    SSS= 0
    CATEG = ""
    CAT.RG.ARG = ""
***********************
    TTMM = R.DATES(EB.DAT.LAST.PERIOD.END)
***********************
    TTD1 = TTMM[1,6]:"01"
    MON.DAYS = TTMM[2]
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''
***    AC.CAT.GRP = ' 1001 1002 1008 1102 1201 1202 1205 1206 1207 1208 1301 1302 1303 1390 1401 1405 1406 1477 1499 1501 1502 1503 1504 1507 1508 1509 1510 1512 1514 1518 1599 16170 2006 6501 6502 6503 6504 6511 6512 1012 1013 1014 1015 1016 1019 3010 3011 3012 3013 3014 3017 3005 16113 16170'
    CRNT.GRP   = ' 1001 1002 1008 1102 1201 1202 1205 1206 1207 1208 1301 1302 1303 1390 1401 1405 1406 1477 1499 1501 1502 1503 1504 1507 1508 1509 1510 1512 1514 1518 1599 16170 2006'
    LE2W.GRP   = ' 21001 21002'
    LE1M.GRP   = ' 21003'
    LE3M.GRP   = ' 21004 21005 6512'
    LE6M.GRP   = ' 21006'
    LE1Y.GRP   = ' 21007'
    LE3Y.GRP   = ' 21008 21009 21010 21013'
    CD3Y.GRP   = ' 21020 21021 21023 21024 21027 21028 21032'
    SVNG.GRP   = ' 6501 6502 6503 6504 6511'
    OTHR.GRP   = ' 1012 1013 1014 1015 1016 1019 16113 3005 3010 3011 3012 3013 3014 3017 3060 3065'

    ALL.CAT.GRP= CRNT.GRP:" ":LE2W.GRP:" ":LE1M.GRP:" ":LE3M.GRP:" ":LE6M.GRP:" ":LE1Y.GRP:" ":LE3Y.GRP:" ":CD3Y.GRP:" ":SVNG.GRP:" ":OTHR.GRP

    CRNT.ACR.L = 'SCBPLFIN.0070-SCBPLFIN.2560'
    LE2W.ACR.L = 'GENDETALL.1077-GENDETALL.2063-GENDETALL.1079-GENDETALL.2065'
    LE1M.ACR.L = 'GENDETALL.1081-GENDETALL.2067'
    LE3M.ACR.L = 'GENDETALL.1083-GENDETALL.2069-GENDETALL.1085-GENDETALL.2071-GENDETALL.1097-GENDETALL.2083'
    LE6M.ACR.L = 'GENDETALL.1087-GENDETALL.2073'
    LE1Y.ACR.L = 'GENDETALL.1089-GENDETALL.2075'
    LE3Y.ACR.L = 'GENDETALL.1091-GENDETALL.2077-GENDETALL.1093-GENDETALL.2079-GENDETALL.1095-GENDETALL.2081'
    CD3Y.ACR.L = 'SCBPLFIN.0130-SCBPLFIN.2620'
    SVNG.ACR.L = 'SCBPLFIN.0090-SCBPLFIN.0091-SCBPLFIN.2580-SCBPLFIN.2581'
    OTHR.ACR.L = 'SCBPLFIN.0110-SCBPLFIN.2600'

    CRNT.GRP.L   = 'GENLED.0510-GENLED.0710-GENLED.0556-GENLED.0707'
    LE2W.GRP.L   = 'GENDET.4092-GENDET.6092-GENDET.6094-GENDET.4094'
    LE1M.GRP.L   = 'GENDET.4096-GENDET.6096'
    LE3M.GRP.L   = 'GENDET.6100-GENDET.6099-GENDET.4100-GENDET.4098-GENDET.6098'
    LE6M.GRP.L   = 'GENDET.6102-GENDET.4102'
    LE1Y.GRP.L   = 'GENDET.4104-GENDET.6104'
    LE3Y.GRP.L   = 'GENDET.4109-GENDET.6106-GENDET.4106-GENDET.6108-GENDET.4108'
    CD3Y.GRP.L   = 'GENLED.0570-GENLED.0723'
    SVNG.GRP.L   = 'GENLED.0520-GENLED.0720-GENLED.0525-GENLED.0725'
    OTHR.GRP.L   = 'GENLED.0540-GENLED.0740-GENLED.0550-GENLED.0750'

    ALL.GRP.L    =  CRNT.GRP.L:"-":LE2W.GRP.L:"-":LE1M.GRP.L:"-":LE3M.GRP.L:"-":LE6M.GRP.L:"-":LE1Y.GRP.L:"-":LE3Y.GRP.L:"-":CD3Y.GRP.L:"-":SVNG.GRP.L:"-":OTHR.GRP.L

    Y.CNT.L = DCOUNT(ALL.GRP.L,".")
    FOR LL.X = 1 TO Y.CNT.L
        LN.ID = FIELD(ALL.GRP.L,"-",LL.X)
        GOSUB GET.CATEG
        ALL.CAT.GRP :=" ":CATEG
    NEXT LL.X
    RETURN
******************************************************
CLEAR.VAR:
*----------
    BAL.CRNT = 0
    AVR.CRNT = 0
    MIN.CRNT = 0
    MAX.CRNT = 0
    BAL.LE2W = 0
    AVR.LE2W = 0
    MIN.LE2W = 0
    MAX.LE2W = 0
    BAL.LE1M = 0
    AVR.LE1M = 0
    MIN.LE1M = 0
    MAX.LE1M = 0
    BAL.LE3M = 0
    AVR.LE3M = 0
    MIN.LE3M = 0
    MAX.LE3M = 0
    BAL.LE6M = 0
    AVR.LE6M = 0
    MIN.LE6M = 0
    MAX.LE6M = 0
    BAL.LE1Y = 0
    AVR.LE1Y = 0
    MIN.LE1Y = 0
    MAX.LE1Y = 0
    BAL.LE3Y = 0
    AVR.LE3Y = 0
    MIN.LE3Y = 0
    MAX.LE3Y = 0
    BAL.CD3Y = 0
    AVR.CD3Y = 0
    MIN.CD3Y = 0
    MAX.CD3Y = 0
    BAL.SVNG = 0
    AVR.SVNG = 0
    MIN.SVNG = 0
    MAX.SVNG = 0
    BAL.OTHR = 0
    AVR.OTHR = 0
    MIN.OTHR = 0
    MAX.OTHR = 0

    REC.ACR.BAL = 0
    REC.INT1 = 0
    ACR.CRNT = 0
    ACR.LE2W = 0
    ACR.LE1M = 0
    ACR.LE3M = 0
    ACR.LE6M = 0
    ACR.LE1Y = 0
    ACR.LE3Y = 0
    ACR.CD3Y = 0
    ACR.SVNG = 0
    ACR.OTHR = 0

    CNT.1  = 0
    CNT.2  = 0
    CNT.3  = 0
    CNT.4  = 0
    CNT.5  = 0
    CNT.6  = 0
    CNT.7  = 0
    CNT.8  = 0
    CNT.9  = 0
    CNT.10 = 0
    TOT.1  = 0
    TOT.2  = 0
    TOT.3  = 0
    TOT.4  = 0
    TOT.5  = 0
    TOT.6  = 0
    TOT.7  = 0
    TOT.8  = 0
    TOT.9  = 0
    TOT.10 = 0

    RETURN
*=======================================
CALLDB:
*-------
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = '' ; R.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)
    FN.AC.INT = 'FBNK.ACCOUNT.CREDIT.INT' ; F.AC.INT = '' ; R.AC.INT = ''
    CALL OPF(FN.AC.INT,F.AC.INT)
    FN.ST.CR = 'FBNK.STMT.ACCT.CR' ; F.ST.CR = '' ; R.ST.CR = ''
    CALL OPF(FN.ST.CR,F.ST.CR)
    FN.LM  = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.LM = '' ; R.LM = ''
    CALL OPF(FN.LM,F.LM)
    FN.BI  = 'FBNK.BASIC.INTEREST' ; F.BI = '' ; R.BI = ''
    CALL OPF(FN.BI,F.BI)
    FN.LL  = 'F.RE.STAT.LINE.BAL' ; F.LL = '' ; R.LL = ''
    CALL OPF(FN.LL,F.LL)
    FN.LL2  = 'F.RE.STAT.LINE.BAL' ; F.LL2 = '' ; R.LL2 = ''
    CALL OPF(FN.LL2,F.LL2)
    FN.LN  = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.LN = 'F.RE.STAT.REP.LINE'      ; F.LN = ''  ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.CTE = 'F.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.CPL = 'FBNK.CONSOLIDATE.PRFT.LOSS' ; F.CPL = '' ; R.CPL = '' ; ER.CPL = ''
    CALL OPF(FN.CPL,F.CPL)
    FN.SLC = 'F.RE.STAT.LINE.CONT' ; F.SLC = '' ; R.SLC = '' ; ER.SLC = ''
    CALL OPF(FN.SLC,F.SLC)
    FN.RG = "F.RE.STAT.RANGE" ; F.RG = "" ; R.RG = "" ; ERR.RG = ""
    RETURN
*========================================
GET.CURR:
*-------
    CUR.SEL = "SSELECT ":FN.CUR:" WITH @ID EQ EGP USD"
    CALL EB.READLIST(CUR.SEL,CUR.LIST,'',SELECTED.CUR,ERR.MSG)
    LOOP
        REMOVE CUR.ID FROM CUR.LIST SETTING POSS.CUR
    WHILE CUR.ID:POSS.CUR
        SSS++
        GOSUB CLEAR.VAR
        GOSUB REC.ACC
        GOSUB REC.LD
        GOSUB REC.LD.H
        GOSUB CALC.AVR
*        GOSUB CALC.AVR1
        GOSUB RET.REC
    REPEAT
    RETURN
*========================================
REC.ACC:
*-------
***************************//AC//**************************************
    SEL.CMD  = "SELECT ":FN.ACC:" WITH OPEN.ACTUAL.BAL NE ''"
    SEL.CMD := " AND CURRENCY EQ ":CUR.ID
    SEL.CMD := " AND OPEN.ACTUAL.BAL GT 0 "
    SEL.CMD := " AND ( CATEGORY IN (":ALL.CAT.GRP:")"
    SEL.CMD := CAT.RG.ARG
    SEL.CMD := ")"
**    CRT SEL.CMD
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.MSG)
**    CRT SELECTED
    LOOP
        REMOVE ACC.ID FROM KEY.LIST SETTING POSS
    WHILE ACC.ID:POSS
        K++
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ERR.1)
        REC.COM = R.ACC<AC.CO.CODE>
        REC.CUS = R.ACC<AC.CUSTOMER>
        REC.CAT = R.ACC<AC.CATEGORY>
        REC.CUR = R.ACC<AC.CURRENCY>
        REC.BAL = R.ACC<AC.OPEN.ACTUAL.BAL>
        REC.DAT = TTMM
        REC.VER = ''
        AA.CAT = " ":REC.CAT
        FINDSTR AA.CAT IN CRNT.GRP SETTING POS.123 THEN
            GOSUB ACTUAL.RATE
        END ELSE
            TDX1 = TTMM[1,6]:"01"
            AC.INT.ID = ACC.ID:"-":TDX1
            CALL F.READ(FN.AC.INT,AC.INT.ID,R.AC.INT,F.AC.INT,EERR.1)
            REC.INT1 = MAXIMUM(R.AC.INT<IC.ACI.CR.INT.RATE>)
            IF REC.INT1 EQ 0 THEN
                ST.AC.ID = ACC.ID:"-":TTMM
                CALL F.READ(FN.ST.CR,ST.AC.ID,R.ST.CR,F.ST.CR,EERR.123)
                IF R.ST.CR THEN
                    REC.ACR.BAL = R.ST.CR<IC.STMCR.TOTAL.INTEREST>
                    REC.INT1 = MAXIMUM(R.ST.CR<IC.STMCR.CR.INT.RATE>)
                END ELSE
                    REC.ACR.BAL = 0
                    REC.INT1 = 0
                END
            END
        END
        IF REC.INT1 LT 0 THEN
            REC.INT1 = 0
        END
        GOSUB CALC.REC
    REPEAT
    RETURN
*========================================
ACTUAL.RATE:
*---------
    REC.ACR.BAL = 0
    TOT.CR.BAL  = 0
    AVR.CR.BAL  = 0
    R.ST.CR = ''
    ST.AC.ID = ACC.ID:"-":TTMM
    CALL F.READ(FN.ST.CR,ST.AC.ID,R.ST.CR,F.ST.CR,EERR.1)
    ST.AC.DATE  = R.ST.CR<IC.STMCR.CR.INT.DATE>
    MON.DAYS = TTMM[2]
*Line [ 326 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AAA = DCOUNT(ST.AC.DATE,@VM)
    FOR MM = 1 TO AAA
        CR.DAYS = R.ST.CR<IC.STMCR.CR.NO.OF.DAYS,MM>
        CR.BAL  = R.ST.CR<IC.STMCR.CR.VAL.BALANCE,MM>
        TOT.CR.BAL += (CR.BAL * CR.DAYS)
    NEXT MM
    AVR.CR.BAL = TOT.CR.BAL / MON.DAYS
    REC.ACR.BAL = R.ST.CR<IC.STMCR.TOTAL.INTEREST>
    IF REC.ACR.BAL LT 1 THEN
        REC.ACR.BAL = 0
    END
    IF AVR.CR.BAL NE 0 THEN
        REC.INT1 = REC.ACR.BAL / AVR.CR.BAL * ( 365 / MON.DAYS ) * 100
    END ELSE
        REC.INT1 = 0
    END
    RETURN
*========================================
REC.LD:
*-------
***************************//LD//*****************************************
    SEL.CMD2  = "SELECT ":FN.LD:" WITH CUSTOMER.ID NE ''"
    SEL.CMD2 := " AND CURRENCY EQ ":CUR.ID
    SEL.CMD2 := " AND ((VALUE.DATE LE ":TTMM:" AND AMOUNT NE 0)"
    SEL.CMD2 := "  OR  (FIN.MAT.DATE GT ":TTMM:" AND AMOUNT EQ 0))"
    SEL.CMD2 := " AND ( CATEGORY IN (":ALL.CAT.GRP:")"
    SEL.CMD2 := CAT.RG.ARG
    SEL.CMD2 := ")"
**    CRT SEL.CMD2

*#    SEL.CMD2 := " AND ((CATEGORY GE 21001 AND CATEGORY LE 21010)"
*#    SEL.CMD2 := "  OR  (CATEGORY GE 21020 AND CATEGORY LE 21029)"
*#    SEL.CMD2 := "  OR  (CATEGORY EQ 21032))"

**    SEL.CMD2 := " BY CURRENCY BY CATEGORY"

    CALL EB.READLIST(SEL.CMD2,KEY.LIST2,'',SELECTED2,ERR.2)
**    CRT SELECTED2
    LOOP
        REMOVE LD.ID FROM KEY.LIST2 SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        REC.COM  = R.LD<LD.CO.CODE>
        REC.CUS  = R.LD<LD.CUSTOMER.ID>
        REC.CAT  = R.LD<LD.CATEGORY>
        REC.CUR  = R.LD<LD.CURRENCY>
        REC.INT  = R.LD<LD.INTEREST.RATE,1>
        REC.SPR  = R.LD<LD.INTEREST.SPREAD>
        INT.KEY  = R.LD<LD.INTEREST.KEY>
        REC.BAL  = R.LD<LD.AMOUNT>
        REC.DAT  = R.LD<LD.VALUE.DATE>
        REC.LCL  = R.LD<LD.LOCAL.REF>
        REC.VER  = REC.LCL<1,LDLR.VERSION.NAME>
        IF REC.BAL EQ 0 THEN
            REC.BAL  = R.LD<LD.REIMBURSE.AMOUNT>
        END
*===========================================
        IF REC.INT EQ '' OR REC.INT EQ 0 THEN
            BAS.INT.ID = INT.KEY:REC.CUR:TTMM[1,6]:"01"
            CALL F.READ(FN.BI,BAS.INT.ID,R.BI,F.BI,ERR.BI)
            REC.INT = R.BI<EB.BIN.INTEREST.RATE>
        END
        REC.INT1 = REC.INT + REC.SPR
*        LD.ACR.ID     = LD.ID:'00'
*        CALL F.READ(FN.LM,LD.ACR.ID, R.LM, F.LM, EE.A.11)
**        REC.ACR.BAL = R.LM<LD27.OUTS.CUR.ACC.I.PAY>
*        REC.ACR.BAL = R.LD<LD.TOT.INTEREST.AMT>
        IF REC.INT1 LT 0 THEN
            REC.INT1 = 0
        END
        GOSUB CALC.REC
*===========================================
    REPEAT
    RETURN
*========================================
REC.LD.H:
*--------
*****************************//LD.H//************************************
    SEL.CMD3  = "SELECT ":FN.LD.H:" WITH CUSTOMER.ID NE ''"
    SEL.CMD3 := " AND CURRENCY EQ ":CUR.ID
    SEL.CMD3 := " AND CURR.NO EQ 1"
    SEL.CMD3 := " AND VALUE.DATE LIKE ":TTMM[1,6]:"..."
    SEL.CMD3 := " AND CATEGORY IN (":ALL.CAT.GRP:")"
    SEL.CMD3 := CAT.RG.ARG
    SEL.CMD3 := ")"
**    CRT SEL.CMD3
*#    SEL.CMD3 := " AND ((CATEGORY GE 21001 AND CATEGORY LE 21010)"
*#    SEL.CMD3 := "  OR  (CATEGORY GE 21020 AND CATEGORY LE 21029)"
*#    SEL.CMD3 := "  OR  (CATEGORY EQ 21032))"

**    SEL.CMD3 := " BY CURRENCY BY CATEGORY"

    CALL EB.READLIST(SEL.CMD3,KEY.LIST3,'',SELECTED3,ERR.3)
**    CRT SELECTED3
    LOOP
        REMOVE LD.ID.H FROM KEY.LIST3 SETTING POSS3
    WHILE LD.ID.H:POSS3
        K3++
        CALL F.READ(FN.LD.H,LD.ID.H,R.LD.H,F.LD.H,ERR.13)
        REC.COM  = R.LD.H<LD.CO.CODE>
        REC.CUS  = R.LD.H<LD.CUSTOMER.ID>
        REC.CAT  = R.LD.H<LD.CATEGORY>
        REC.CUR  = R.LD.H<LD.CURRENCY>
        REC.INT  = R.LD.H<LD.INTEREST.RATE,1>
        REC.SPR  = R.LD.H<LD.INTEREST.SPREAD>
        INT.KEY  = R.LD.H<LD.INTEREST.KEY>
        REC.BAL  = 0
        REC.DAT  = R.LD.H<LD.VALUE.DATE>
        REC.LCL  = R.LD.H<LD.LOCAL.REF>
        REC.VER  = REC.LCL<1,LDLR.VERSION.NAME>
*===========================================
        IF REC.INT EQ '' OR REC.INT EQ 0 THEN
            BAS.INT.ID = INT.KEY:REC.CUR:TTMM[1,6]:"01"
            CALL F.READ(FN.BI,BAS.INT.ID,R.BI,F.BI,ERR.BI)
            REC.INT = R.BI<EB.BIN.INTEREST.RATE>
        END
        REC.INT1 = REC.INT + REC.SPR
*        LD.ACR.ID     = FIELD(LD.ID.H,';',1):'00'
*        CALL F.READ(FN.LM,LD.ACR.ID, R.LM, F.LM, EE.A.11)
**        REC.ACR.BAL = R.LM<LD27.OUTS.CUR.ACC.I.PAY>
*        REC.ACR.BAL = R.LD.H<LD.TOT.INTEREST.AMT>
        IF REC.INT1 LT 0 THEN
            REC.INT1 = 0
        END
        GOSUB CALC.REC
*===========================================
    REPEAT
    RETURN
*========================================
*****************************************************************
CALC.REC:
*--------
    REC.CAT = " ":REC.CAT
    CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,REC.CUS,CUS.SEC)
    IF (REC.DAT[1,6] EQ TTMM[1,6]) AND (CUS.SEC NE 1100 AND CUS.SEC NE 1200 AND CUS.SEC NE 1300 AND CUS.SEC NE 1400) AND (REC.CUS NE 1304955 AND REC.CUS NE 1101254 AND REC.CUS NE 1101253 AND REC.CUS NE 1305067 ) AND (REC.VER NE ',SCB.ELIQ') THEN
        INT.FLAG = 1
    END ELSE
        INT.FLAG = 0
    END
    FINDSTR REC.CAT IN CRNT.GRP SETTING POS.1 THEN
        IF REC.BAL GT 0 THEN
            BAL.CRNT += REC.BAL
            ACR.CRNT += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.1 += REC.INT1
                CNT.1++
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.CRNT THEN
                        MIN.CRNT = REC.INT1
                    END
                    IF REC.INT1 GE MAX.CRNT THEN
                        MAX.CRNT = REC.INT1
                        TOP.CUS.CRNT  = REC.CUS
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE2W.GRP SETTING POS.2 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE2W += REC.BAL
            ACR.LE2W += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.2 += REC.INT1
                CNT.2++
                IF MIN.LE2W = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE2W = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE2W THEN
                        MIN.LE2W = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE2W THEN
                        MAX.LE2W = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE1M.GRP SETTING POS.3 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE1M += REC.BAL
            ACR.LE1M += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.3 += REC.INT1
                CNT.3++
                IF MIN.LE1M = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE1M = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE1M THEN
                        MIN.LE1M = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE1M THEN
                        MAX.LE1M = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE3M.GRP SETTING POS.4 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE3M += REC.BAL
            ACR.LE3M += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                IF REC.INT1 NE 0 THEN
                    TOT.4 += REC.INT1
                    CNT.4++
                END
                IF MIN.LE3M = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE3M = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE3M THEN
                        MIN.LE3M = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE3M THEN
                        MAX.LE3M = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE6M.GRP SETTING POS.5 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE6M += REC.BAL
            ACR.LE6M += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.5 += REC.INT1
                CNT.5++
                IF MIN.LE6M = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE6M = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE6M THEN
                        MIN.LE6M = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE6M THEN
                        MAX.LE6M = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE1Y.GRP SETTING POS.6 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE1Y += REC.BAL
            ACR.LE1Y += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.6 += REC.INT1
                CNT.6++
                IF MIN.LE1Y = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE1Y = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE1Y THEN
                        MIN.LE1Y = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE1Y THEN
                        MAX.LE1Y = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE3Y.GRP SETTING POS.7 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE3Y += REC.BAL
            ACR.LE3Y += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.7 += REC.INT1
                CNT.7++
                IF MIN.LE3Y = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE3Y = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE3Y THEN
                        MIN.LE3Y = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE3Y THEN
                        MAX.LE3Y = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN CD3Y.GRP SETTING POS.8 THEN
        IF REC.BAL GT 0 THEN
            BAL.CD3Y += REC.BAL
            ACR.CD3Y += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.8 += REC.INT1
                CNT.8++
                IF MIN.CD3Y = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.CD3Y = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.CD3Y THEN
                        MIN.CD3Y = REC.INT1
                    END
                    IF REC.INT1 GE MAX.CD3Y THEN
                        MAX.CD3Y = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN SVNG.GRP SETTING POS.9 THEN
        IF REC.BAL GT 0 THEN
            BAL.SVNG += REC.BAL
            ACR.SVNG += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                IF REC.INT1 NE 0 THEN
                    TOT.9 += REC.INT1
                    CNT.9++
                END
                IF MIN.SVNG = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.SVNG = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.SVNG THEN
                        MIN.SVNG = REC.INT1
                    END
                    IF REC.INT1 GE MAX.SVNG THEN
                        MAX.SVNG = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN OTHR.GRP SETTING POS.10 THEN
        IF REC.BAL GT 0 THEN
            BAL.OTHR += REC.BAL
            ACR.OTHR += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                IF REC.INT1 NE 0 THEN
                    TOT.10 += REC.INT1
                    CNT.10++
                END
                IF MIN.OTHR = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.OTHR = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.OTHR THEN
                        MIN.OTHR = REC.INT1
                    END
                    IF REC.INT1 GE MAX.OTHR THEN
                        MAX.OTHR = REC.INT1
                    END
                END
            END
        END
    END
    RETURN
*-----------------------------------
CALC.AVR1:
*---------
    IF CNT.1 NE 0 THEN
        AVR.CRNT = TOT.1 / CNT.1
    END
    IF CNT.2 NE 0 THEN
        AVR.LE2W = TOT.2 / CNT.2
    END
    IF CNT.3 NE 0 THEN
        AVR.LE1M = TOT.3 / CNT.3
    END
    IF CNT.4 NE 0 THEN
        AVR.LE3M = TOT.4 / CNT.4
    END
    IF CNT.5 NE 0 THEN
        AVR.LE6M = TOT.5 / CNT.5
    END
    IF CNT.6 NE 0 THEN
        AVR.LE1Y = TOT.6 / CNT.6
    END
    IF CNT.7 NE 0 THEN
        AVR.LE3Y = TOT.7 / CNT.7
    END
    IF CNT.8 NE 0 THEN
        AVR.CD3Y = TOT.8 / CNT.8
    END
    IF CNT.9 NE 0 THEN
        AVR.SVNG = TOT.9 / CNT.9
    END
    IF CNT.10 NE 0 THEN
        AVR.OTHR = TOT.10 / CNT.10
    END
    RETURN
*-----------------------------------
CALC.AVR:
*--------
    GRP.LL  = CRNT.GRP.L
    GRP.PLL = CRNT.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.CRNT = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "CRNT  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL  = LE2W.GRP.L
    GRP.PLL = LE2W.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE2W = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "LE2W  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL  = LE1M.GRP.L
    GRP.PLL = LE1M.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE1M = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "LE1M  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL  = LE3M.GRP.L
    GRP.PLL = LE3M.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE3M = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "LE3M  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL  = LE6M.GRP.L
    GRP.PLL = LE6M.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE6M = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "LE6M  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL  = LE1Y.GRP.L
    GRP.PLL = LE1Y.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE1Y = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "LE1Y  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL  = LE3Y.GRP.L
    GRP.PLL = LE3Y.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE3Y = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "LE3Y  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL = CD3Y.GRP.L
    GRP.PLL= CD3Y.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.CD3Y = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "CD3Y  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL = SVNG.GRP.L
    GRP.PLL= SVNG.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.SVNG = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "SVNG  = ":ACR.BAL:" / ":AVVR.BAL
    END
    GRP.LL = OTHR.GRP.L
    GRP.PLL= OTHR.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.OTHR = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
*        CRT "OTHR  = ":ACR.BAL:" / ":AVVR.BAL
    END
*--------------------------
    RETURN
*====================================
GET.AVVR.BAL:
*---------
    TOT.L.BAL  = 0
    TOT.PL.BAL = 0
    AVVR.BAL   = 0
    ACR.BAL    = 0
    LL.ACR.BAL = 0
    LL.BAL     = 0
    PLL.BAL    = 0
    CO.SEL = "SSELECT F.COMPANY"
    CALL EB.READLIST(CO.SEL,CO.LIST,'',SELECTED.CO,ER.MSG.CO)
    GRP.LL.COUNT  = COUNT(GRP.LL,'.')
    GRP.PLL.COUNT = COUNT(GRP.PLL,'.')
*    CRT SELECTED.CO
    FOR CCC = 1 TO SELECTED.CO
        CO.ID = CO.LIST<CCC>
        FOR QQ = 1 TO GRP.LL.COUNT
            LL.BAL = 0
            LL.NO = FIELD(FIELD(GRP.LL,'-',QQ),'.',1):"-":FIELD(FIELD(GRP.LL,'-',QQ),'.',2)
            C.DATE = TTD1
            CALL CDT("",C.DATE,'-1C')
            LOOP
                CALL CDT("",C.DATE,'+1C')
            WHILE C.DATE LE TTMM
                LL.ID = LL.NO:"-":CUR.ID:"-":C.DATE:"*":CO.ID
                GOSUB CALC.AVVR
            REPEAT
        NEXT QQ
        FOR YY = 1 TO GRP.PLL.COUNT
            PLL.BAL = 0
            PLL.NO = FIELD(FIELD(GRP.PLL,'-',YY),'.',1):"-":FIELD(FIELD(GRP.PLL,'-',YY),'.',2)
            PLL.ID = PLL.NO:"-":CUR.ID:"-":TTMM:"*":CO.ID
            GOSUB CALC.ACR
        NEXT YY
    NEXT CCC
    AVVR.BAL = TOT.L.BAL / MON.DAYS
    ACR.BAL = TOT.PL.BAL
    IF AVVR.BAL LT 0 THEN
        AVVR.BAL = AVVR.BAL * -1
    END
    IF ACR.BAL LT 0 THEN
        ACR.BAL = ACR.BAL * -1
    END
    RETURN
*====================================
CALC.AVVR:
*--------
    CALL F.READ(FN.LL,LL.ID,R.LL,F.LL,ER.LL1)
    IF R.LL THEN
        LL.BAL = R.LL<RE.SLB.CLOSING.BAL>
    END ELSE
        IF C.DATE[2] EQ '01' THEN
            GOSUB PRV.LL.BAL
        END
    END
    TOT.L.BAL += LL.BAL
    RETURN
*====================================
PRV.LL.BAL:
*---------
    AA.DATE = C.DATE
    CALL CDT("",AA.DATE,'-1C')
    XLL.ID = LL.NO:"-":CUR.ID:"-":AA.DATE:"*":CO.ID
    CALL DBR('RE.STAT.LINE.BAL':@FM:RE.SLB.CLOSING.BAL,XLL.ID,XLL.BAL)
    LL.BAL = XLL.BAL
    RETURN
*====================================
CALC.ACR:
*--------
    CALL F.READ(FN.LL2,PLL.ID,R.LL2,F.LL2,ER.LL1)
    IF TTMM[5,2] NE '01' THEN
        PLL.BAL = R.LL2<RE.SLB.CR.MVMT.MTH>+R.LL2<RE.SLB.DB.MVMT.MTH>
    END ELSE
        PLL.BAL = R.LL2<RE.SLB.CLOSING.BAL>
    END
    TOT.PL.BAL += PLL.BAL
    RETURN
*====================================
GET.CATEG:
*---------
    Z = 1
    CATEG.DET = ""
    CALL F.READ(FN.LN,LN.ID,R.LN,F.LN,ERR.LN)
*Line [ 893 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    PROD.NO = DCOUNT(R.LN<RE.SRL.ASSET1>,@VM)
    FOR XX = 1 TO PROD.NO
        PRODUCT = R.LN<RE.SRL.ASSET1,XX>
        IF PRODUCT[1,1] EQ '*' THEN
            Z++
***            GOSUB GETRANGE
*===========================================
            SS = LEN(PRODUCT) - 1
            RG.ID = PRODUCT[2,SS]
            CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
            CATFR = R.RG<RE.RNG.START.RANGE>
            CATTO = R.RG<RE.RNG.END.RANGE>
            CAT.RG.ARG := " OR ( CATEGORY GE ":CATFR:" AND CATEGORY LE ":CATTO:")"

            CATTT = CATFR - 1
            LOOP
                CATTT++
            WHILE CATTT LE CATTO
*Line [ 912 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR " ":CATTT IN CATEG.DET SETTING POS.CATT1 THEN '' ELSE
                    CATEG.DET = CATEG.DET:" ":CATTT
                END
            REPEAT

*============================================
        END ELSE
*Line [ 920 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR " ":PRODUCT IN CATEG SETTING POS.CATT THEN '' ELSE
*Line [ 922 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR " ":PRODUCT IN ALL.CAT.GRP SETTING POS.CATT2 THEN '' ELSE
                    CATEG = CATEG:" ":PRODUCT
                END
            END
*Line [ 927 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FINDSTR " ":PRODUCT IN CATEG.DET SETTING POS.PROD1 THEN '' ELSE
                CATEG.DET = CATEG.DET:" ":PRODUCT
            END
        END
    NEXT XX
*Line [ 933 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN CRNT.GRP.L SETTING POS.CRNT.L THEN CRNT.GRP = CRNT.GRP:" ":CATEG.DET ELSE NULL
*Line [ 935 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN LE2W.GRP.L SETTING POS.LE2W.L THEN LE2W.GRP = LE2W.GRP:" ":CATEG.DET ELSE NULL
*Line [ 937 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN LE1M.GRP.L SETTING POS.LE1M.L THEN LE1M.GRP = LE1M.GRP:" ":CATEG.DET ELSE NULL
*Line [ 939 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN LE3M.GRP.L SETTING POS.LE3M.L THEN LE3M.GRP = LE3M.GRP:" ":CATEG.DET ELSE NULL
*Line [ 941 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN LE6M.GRP.L SETTING POS.LE6M.L THEN LE6M.GRP = LE6M.GRP:" ":CATEG.DET ELSE NULL
*Line [ 943 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN LE1Y.GRP.L SETTING POS.LE1Y.L THEN LE1Y.GRP = LE1Y.GRP:" ":CATEG.DET ELSE NULL
*Line [ 945 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN LE3Y.GRP.L SETTING POS.LE3Y.L THEN LE3Y.GRP = LE3Y.GRP:" ":CATEG.DET ELSE NULL
*Line [ 947 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN CD3Y.GRP.L SETTING POS.CD3Y.L THEN CD3Y.GRP = CD3Y.GRP:" ":CATEG.DET ELSE NULL
*Line [ 949 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN SVNG.GRP.L SETTING POS.SVNG.L THEN SVNG.GRP = SVNG.GRP:" ":CATEG.DET ELSE NULL
*Line [ 951 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR LN.ID IN OTHR.GRP.L SETTING POS.OTHR.L THEN OTHR.GRP = OTHR.GRP:" ":CATEG.DET ELSE NULL
    RETURN
***********************************************
GETRANGE:
********
    SS = LEN(PRODUCT) - 1
    RG.ID = PRODUCT[2,SS]
    CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
    CATFR = R.RG<RE.RNG.START.RANGE>
    CATTO = R.RG<RE.RNG.END.RANGE>
    CATTT = CATFR - 1
    LOOP
        CATTT++
    WHILE CATTT LE CATTO
*Line [ 966 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR CATTT IN CATEG SETTING POS.CATT1 THEN '' ELSE
            CATEG = CATEG:" ":CATTT
        END
    REPEAT
    RETURN
*-----------------------*

*...................................
RET.REC:
********
    Y.RET.DATA<-1>=BAL.CRNT:"*":AVR.CRNT:"*":MIN.CRNT:"*":MAX.CRNT:"*":BAL.LE2W:"*":AVR.LE2W:"*":MIN.LE2W:"*":MAX.LE2W:"*":BAL.LE1M:"*":AVR.LE1M:"*":MIN.LE1M:"*":MAX.LE1M:"*":BAL.LE3M:"*":AVR.LE3M:"*":MIN.LE3M:"*":MAX.LE3M:"*":BAL.LE6M:"*":AVR.LE6M:"*":MIN.LE6M:"*":MAX.LE6M:"*":BAL.LE1Y:"*":AVR.LE1Y:"*":MIN.LE1Y:"*":MAX.LE1Y:"*":BAL.LE3Y:"*":AVR.LE3Y:"*":MIN.LE3Y:"*":MAX.LE3Y:"*":BAL.CD3Y:"*":AVR.CD3Y:"*":MIN.CD3Y:"*":MAX.CD3Y:"*":BAL.SVNG:"*":AVR.SVNG:"*":MIN.SVNG:"*":MAX.SVNG:"*":BAL.OTHR:"*":AVR.OTHR:"*":MIN.OTHR:"*":MAX.OTHR:"*":CUR.ID:"*":TTMM:"*":SSS
    RETURN
*************************************************
END
