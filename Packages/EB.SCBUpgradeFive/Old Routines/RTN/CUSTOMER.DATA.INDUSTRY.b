* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>129</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CUSTOMER.DATA.INDUSTRY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 41 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    PRINT " ������� = " :SELECTED
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='CUSTOMER.DATA.INDUSTRY'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.LD = 'FBNK.CUSTOMER' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    T.SEL = "SELECT FBNK.CUSTOMER WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE> :" BY INDUSTRY"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED

        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
        CALL DBR ('INDUSTRY':@FM:EB.SEC.DESCRIPTION,R.LD<EB.CUS.INDUSTRY>,SECDESC)
        SEC = R.LD<EB.CUS.INDUSTRY>
        YY<1,1> = SECDESC
        PRINT YY<1,1>
        Y = 0
        FOR Z = I TO SELECTED

            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,E1)
            SEC2 = R.LD<EB.CUS.INDUSTRY>
            IF SEC # SEC2 THEN
            GOSUB ZZZZ
            RETURN
            END
            LOCAL.REF     = R.LD<EB.CUS.LOCAL.REF>
            CUST.NO       = KEY.LIST<Z>
            CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            CALL DBR ('SECTOR':@FM:EB.SEC.DESCRIPTION,R.LD<EB.CUS.SECTOR>,CUSSEC)
            CUST.SECTOR   =  CUSSEC
            CALL DBR ('INDUSTRY':@FM:EB.IND.DESCRIPTION,R.LD<EB.CUS.INDUSTRY>,CUSINDUS)
            CUST.INDUSTRY =  CUSINDUS
            Y = Y +1
            YY = SPACE(120)
            YY<1,2>[1,10] = CUST.NO
            YY<1,2>[15,35] = CUST.NAME
            YY<1,2>[55,35] = CUST.ADDRESS
            YY<1,2>[105,15] = CUSSEC
            IF I # SELECTED THEN
           *     YY<1,3>[1,70] = STR('-',120)
            END ELSE
                YY<1,3>[1,70] = STR('=',120)
            END
            IF I # SELECTED THEN
                PRINT YY<1,2>
            END
            IF I # SELECTED THEN
                PRINT YY<1,3>
            END

        NEXT Z
ZZZZ:
        IF I # SELECTED THEN
            PRINT " ������� = " :Y
            YY<1,4>[1,70] = STR('*',120)
            PRINT YY<1,4>
            IF Z # SELECTED THEN
                I = Z - 1
            END ELSE
                I = Z
            END
        END
    NEXT I
  RETURN
*ZZZZ:
*PRINT " ������� = " :Y
*        I = Z
*    NEXT I
*PRINT " ������� = " :SELECTED
*RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
*    PR.HD ="'L'":SPACE(50):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
*    PR.HD :="'L'":SPACE(50):" ������ ������� ����� ���� ������ ����� ������� "
    PR.HD :="'L'":SPACE(50):"������� ������� ���� ������ ����� ������� "
    PR.HD :="'L'":SPACE(45):STR('_',55)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

  * PR.HD :="'L'":SPACE(1):" �����":SPACE(15):" �����" :SPACE(30):"������� ":SPACE(35):"������ "
    PR.HD :="'L'":SPACE(1):" �����":SPACE(15):" �����" :SPACE(30):" ������� ":SPACE(35):"����� ������ "
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    RETURN
*==============================================================
END
