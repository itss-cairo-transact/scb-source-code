* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE CONV.TELLER.AC2
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
************************************************************
    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
    RETURN
************************************************************
INIT:
    XX = O.DATA
    AMT = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.TEL = 'FBNK.TELLER'
    F.TEL = ''
    R.TEL = ''
    RETURN
************************************************************
OPEN:
    CALL OPF(FN.TEL,F.TEL)
    RETURN
PROCESS:
************************************************************
    T.SEL = "SELECT ":FN.TEL: " WITH CURRENCY.1 EQ ":XX:" AND TELLER.ID.1 LIKE ...99 AND DR.CR.MARKER EQ 'DEBIT'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.TEL,KEY.LIST<I>,R.TEL,F.TEL,EER)
            AMT =  AMT +  R.TEL<TT.TE.NET.AMOUNT>
        NEXT I
    END
    O.DATA = AMT
    RETURN
END
