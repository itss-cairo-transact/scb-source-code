* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 12/10/2010***********
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CUST.CORP.UPD(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FLAG = 0

    FN.CUS   = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    N.SEL = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.CORPORATE.UPDATE' AND DATE.TIME GE 1010070700 BY COMPANY.BOOK"
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
**    TEXT = "SEL=":SELECTED.N ; CALL REM
    ENT.NO = ''
    LOOP
        REMOVE CUST.ID  FROM KEY.LIST.N SETTING POS
    WHILE CUST.ID:POS
        IF LEN(CUST.ID) = '7' THEN
            IF CUST.ID[2,1] # '3' THEN
                CALL F.READ( FN.CUS,CUST.ID, R.CUS,F.CUS, ETEXT)
                CUR.NO    = R.CUS<EB.CUS.CURR.NO>
                DAT.TIM   = R.CUS<EB.CUS.DATE.TIME>
                CUS.COMP  = R.CUS<EB.CUS.COMPANY.BOOK>
                SECTOR    = R.CUS<EB.CUS.SECTOR>
                CUS.LOCAL = R.CUS<EB.CUS.LOCAL.REF>
                CUS.NAME  = CUS.LOCAL<1,CULR.ARABIC.NAME>
                NO.KED    = CUS.LOCAL<1,CULR.COM.REG.NO>
                KED.DATE  = CUS.LOCAL<1,CULR.COM.REG.EXP.D>
                KED.EX.D  = CUS.LOCAL<1,CULR.LIC.EXP.DATE>
                LICE.PL   = CUS.LOCAL<1,CULR.LEGAL.STATUS>
                NO.K.LIC  = CUS.LOCAL<1,CULR.TAX.EXEMPTION>
                ISS.LIC.D = CUS.LOCAL<1,CULR.LIC.IND.EXP.D>

                Y.RET.DATA<-1> = CUS.COMP:"*":CUST.ID:"*":CUS.NAME:"*":NO.KED:"*":KED.DATE:"*":KED.EX.D:"*":LICE.PL:"*":NO.K.LIC:"*":ISS.LIC.D:"*":SECTOR
            END
        END ELSE
            IF LEN(CUST.ID) = '8' THEN
                IF CUST.ID[3,1] # '3' THEN
                    CALL F.READ( FN.CUS,CUST.ID, R.CUS,F.CUS, ETEXT)
                    CUR.NO    = R.CUS<EB.CUS.CURR.NO>
                    DAT.TIM   = R.CUS<EB.CUS.DATE.TIME>
                    CUS.COMP = R.CUS<EB.CUS.COMPANY.BOOK>
                    CUS.LOCAL = R.CUS<EB.CUS.LOCAL.REF>
                    CUS.NAME  = CUS.LOCAL<1,CULR.ARABIC.NAME>
                    NO.KED    = CUS.LOCAL<1,CULR.COM.REG.NO>
                    KED.DATE  = CUS.LOCAL<1,CULR.COM.REG.EXP.D>
                    KED.EX.D  = CUS.LOCAL<1,CULR.LIC.EXP.DATE>
                    LICE.PL   = CUS.LOCAL<1,CULR.LEGAL.STATUS>
                    NO.K.LIC  = CUS.LOCAL<1,CULR.TAX.EXEMPTION>
                    ISS.LIC.D = CUS.LOCAL<1,CULR.LIC.IND.EXP.D>
                    SECTOR    = R.CUS<EB.CUS.SECTOR>
                    Y.RET.DATA<-1> = CUS.COMP:"*":CUST.ID:"*":CUS.NAME:"*":NO.KED:"*":KED.DATE:"*":KED.EX.D:"*":LICE.PL:"*":NO.K.LIC:"*":ISS.LIC.D:"*":SECTOR
                END
            END
        END
    REPEAT

    RETURN
END
