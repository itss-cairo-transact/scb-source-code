* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    PROGRAM DEPOSIT.R.TEXT.4

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "/home/signat" , "DEPOSIT_PRICES4" TO BB THEN
        CLOSESEQ BB

        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"DEPOSIT_PRICES4"
        HUSH OFF
    END
    OPENSEQ "/home/signat" , "DEPOSIT_PRICES4" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DEPOSIT_PRICES4 CREATED IN /home/signat'
        END ELSE
            STOP 'Cannot create DEPOSIT_PRICES4 File IN /home/signat'
        END
    END

    RETURN

*========================================================================
PROCESS:
    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
*-------------------------------------------
    BI.ID = '75EGP...'
    T.SEL  = "SELECT ":FN.BI:" WITH @ID LIKE ":BI.ID:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CALL F.READ(FN.BI,KEY.LIST<SELECTED>,R.BI,F.BI,E1)
        WS.RATE = R.BI<EB.BIN.INTEREST.RATE>
        WS.CCY  = 'EGP'

        WS.RATE.1W = WS.RATE - 5.5
        WS.RATE.2W = WS.RATE - 5.5
        WS.RATE.1M = WS.RATE - 3
        WS.RATE.2M = WS.RATE - 3
        WS.RATE.3M = WS.RATE - 2.5

*** LE 100K ***

        WS.RATE.6M = WS.RATE - 5
        WS.RATE.9M = WS.RATE - 5
        WS.RATE.1Y = WS.RATE - 4.5

        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1W:" |"
        BB.DATA := WS.RATE.2W:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.1Y:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END


*** 100K - 500K ***

        WS.RATE.6M = WS.RATE - 4
        WS.RATE.9M = WS.RATE - 4
        WS.RATE.1Y = WS.RATE - 3.5


        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1W:" |"
        BB.DATA := WS.RATE.2W:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.1Y:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*** 500K - 1M ***

        WS.RATE.6M = WS.RATE - 3
        WS.RATE.9M = WS.RATE - 3
        WS.RATE.1Y = WS.RATE - 2.5


        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1W:" |"
        BB.DATA := WS.RATE.2W:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.1Y:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*** 1M - 5M ***

        WS.RATE.6M = WS.RATE - 2.5
        WS.RATE.9M = WS.RATE - 2.5
        WS.RATE.1Y = WS.RATE - 2


        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1W:" |"
        BB.DATA := WS.RATE.2W:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.1Y:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END


*** 5M - 10M ***

        WS.RATE.6M = WS.RATE - 2
        WS.RATE.9M = WS.RATE - 2
        WS.RATE.1Y = WS.RATE - 1.5


        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1W:" |"
        BB.DATA := WS.RATE.2W:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.1Y:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*** 10M+ ***

        WS.RATE.6M = WS.RATE - 1.5
        WS.RATE.9M = WS.RATE - 1.5
        WS.RATE.1Y = WS.RATE - 1


        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1W:" |"
        BB.DATA := WS.RATE.2W:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.1Y:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    END
    RETURN
END
