* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------NI7OOOOOOOOOOOOOOOOOOOO------------------------------------------
    SUBROUTINE CUSOFFICE(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    FN.CUS='FBNK.CUSTOMER'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

**** T.SEL="SELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ 11 "
     T.SEL="SELECT FBNK.CUSTOMER WITH  ACCOUNT.OFFICER EQ 11 AND POSTING.RESTRICT NE 99 AND POSTING.RESTRICT NE 90 "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    TEXT = "SELECTED :": SELECTED ; CALL REM
    IF SELECTED THEN
        TEXT = "START " ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUSTOMER,KEY.LIST<I>, R.CUS,F.CUS, ETEXT)
            NAMEE= R.CUS<EB.CUS.NAME.1>
            SHORT= R.CUS<EB.CUS.SHORT.NAME>

**** IF SHORT[1,2] EQ SHORT[3,2] THEN
            FOR ENQ.LP = 1 TO SELECTED
                IF SHORT[1,2] EQ SHORT[4,2] THEN
                    TEXT = SHORT ; CALL REM
                    ENQ<2,ENQ.LP> = '@ID'
                    ENQ<3,ENQ.LP> = 'EQ'
                    ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
                END
            NEXT ENQ.LP
        NEXT I
    END
    TEXT = "END" ; CALL REM
**********************
    RETURN
END
