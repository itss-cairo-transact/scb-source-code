* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
*-----------------------------------------------------------------------------
* <Rating>-9</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.BUILD.USR.AC(ENQUIRY.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.LIABILITY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*****UPDATED BY NESSREEN AHMED 8/10/2019****
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.RESTRICTED
*****END OF UPDATED 8/10/2019****************


    LOCATE 'ACCOUNT' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        ACCC = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END

    FN.ACC = 'FBNK.ACCOUNT' ;F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
*** SCB R15 UPG 01 SEP 2016
*    CALL F.READ(FN.AC,ACCC,R.ACC,F.ACC,READ.ERR)
    CALL F.READ(FN.ACC,ACCC,R.ACC,F.ACC,READ.ERR)
*** SCB R15 UPG 01 SEP 2016
    CUS.ID = R.ACC<AC.CUSTOMER>
    CATEG = R.ACC<AC.CATEGORY>
*****UPDATED BY NESSREEN AHMED 20/10/2019****
     AC.CODE = R.ACC<AC.CO.CODE>
*****END OF UPDATE 20/10/2019****************
*****////  UPDATE BY MAHMOUD 4/9/2016 FOR EXCEPTION APPROVAL LETTER NO.( 1661 SEND/ 171 RECIEVED ) DATED 1/3/2016

EXP.CUST = " 7300650 7300651 7300652 7300655 "
EXP.USER = " SCB.7935 SCB.67202 SCB.55255 SCB.13692 SCB.99201 "
USO.USER = R.USER<EB.USE.SIGN.ON.NAME>
CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,USO.USER,USR.ID)
FINDSTR USR.ID:" " IN EXP.USER SETTING POS.USR THEN USR.FLAG = 1 ELSE USR.FLAG = ''
FINDSTR CUS.ID:" " IN EXP.CUST SETTING POS.CUST THEN CUST.FLAG = 1 ELSE CUST.FLAG = ''

********************************
    FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""

    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,READ.ERR1)

    LOOP
        REMOVE AC.ID FROM R.CUS SETTING POS.CUST
    WHILE AC.ID:POS.CUST

        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.ID,CATT)
        IF AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 THEN
            IF USR.FLAG EQ '' OR CUST.FLAG EQ '' THEN    ;*** ADDED BY MAHMOUD 4/9/2016
                ENQUIRY.DATA<2,1> = 'ACCOUNT'
                ENQUIRY.DATA<3,1> = 'EQ'
                ENQUIRY.DATA<4,1> = 'SECRET INFORMATION'
            END                                          ;*** ADDED BY MAHMOUD 4/9/2016
*****//// END OF UPDATE - MAHMOUD 4/9/2016
        END
    REPEAT
***************************************
*CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC,CATT)

* IF CATT EQ '12005'  THEN
*    ENQUIRY.DATA<2,1> = 'ACCOUNT'
*   ENQUIRY.DATA<3,1> = 'EQ'
*  ENQUIRY.DATA<4,1> = 'SECRET INFORMATION'
*   END
********************************

    IF ( CATEG EQ '1002' OR CATEG EQ '12005' ) THEN
        FN.CU = 'FBNK.CUSTOMER' ;F.CU = '' ; R.CU = ''
        CALL OPF(FN.CU,F.CU)
        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,READ.ERR1)

        EMP.NO = R.CU<EB.CUS.LOCAL.REF,CULR.EMPLOEE.NO>
*****UPDATED BY NESSREEN AHMED 12/11/2013************
***** FN.USR = 'FBNK.USER' ;F.USR = '' ; R.USR = ''
        FN.USR = 'F.USER' ;F.USR = '' ; R.USR = ''
        CALL OPF(FN.USR,F.USR)
*****END OF UPDATED 12/11/2013**********************

        USR.N =  R.USER<EB.USE.SIGN.ON.NAME>
        USR.ID = TRIM(USR.N, "0", "L")
        IF  EMP.NO NE USR.ID THEN
            TEXT = "SECRET.INFORMATION " ; CALL REM
            ENQUIRY.DATA<2,1> = 'ACCOUNT'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET.INFORMATION'
        END
*****UPDATED BY NESSREEN AHMED 20/10/2019**********
*****END
     END ELSE
       AC.CODE = ''
        FN.CUS.REST = 'F.SCB.CUS.RESTRICTED' ;F.CUS.REST = '' ; R.CUS.REST = ''
        CALL OPF(FN.CUS.REST,F.CUS.REST)

        CALL F.READ(FN.CUS.REST,CUS.ID,R.CUS.REST,F.CUS.REST,ERR.CUS)
        IF  NOT(ERR.CUS) THEN
            CALL F.READ(FN.ACC, ACCC,R.ACC,F.ACC,ERR.ACC)
            AC.CODE = R.ACC<AC.CO.CODE>
            IF AC.CODE # ID.COMPANY THEN
                TEXT = "SECRET.INFORMATION " ; CALL REM
                ENQUIRY.DATA<2,1> = 'ACCOUNT'
                ENQUIRY.DATA<3,1> = 'EQ'
                ENQUIRY.DATA<4,1> = 'SECRET.INFORMATION'
            END
     END
    RETURN
END
