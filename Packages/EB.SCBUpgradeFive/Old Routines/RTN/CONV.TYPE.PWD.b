* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* BY NESSMA MOHAMMAD
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.TYPE.PWD

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT T24.BP I_F.PASSWORD.RESET
*-------------------------------------------
    R.PWR = ""   ; E11 = ""
    FN.PWR = "F.PASSWORD.RESET"  ; F.PWR = ""
    CALL OPF(FN.PWR , F.PWR)

    ID.TMP = O.DATA
    CALL F.READ(FN.PWR,ID.TMP, R.PWR, F.PWR, E11)
    RES  = R.PWR<EB.PWR.USER.RESET>

    IF RES EQ '' THEN
        O.DATA = "A"
    END ELSE
        O.DATA = "T"
    END
*-------------------------------------------
    RETURN
END
