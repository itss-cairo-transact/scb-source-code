* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 21/07/2020******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.DB.GE.EQV.30USD.LWD(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CURR.DALY = 'FBNK.CURRENCY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    IDDD = "EG0010001"
   

    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,IDDD,LAST.W.DAY)
    TEXT = 'LSTW=':LAST.W.DAY ; CALL REM

    COMP = C$ID.COMPANY

    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0 ;  ID.NO = '' ; CU.SECT = ''

    *N.SEL = "SELECT FBNK.TELLER WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND CUSTOMER.1 NE '99433300' BY CUSTOMER.1 BY CURRENCY.1"
    *CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
     N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND CUSTOMER.1 NE '99433300' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE EQ ":LAST.W.DAY : " BY CUSTOMER.1 BY CURRENCY.1"
     CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)


    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    CUST<1>         = R.TT<TT.TE.CUSTOMER.1>
    CURR<1>         = R.TT<TT.TE.CURRENCY.1>
    AMT.FCY<1>      = R.TT<TT.TE.AMOUNT.FCY.1>
    AMT.LCY<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1>     = R.TT<TT.TE.AUTH.DATE>
    TT.DATE = AUTH.DAT<1>
    COMP.CODE<1>    = R.TT<TT.TE.CO.CODE>
     KEY.ID<1> = "USD"
    CALL F.READ(FN.CURR.DALY,KEY.ID<1>,R.CURR.DALY,F.CURR.DALY,ERR2)
   * DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
    CURR.MARK = R.CURR.DALY<EB.CUR.CURRENCY.MARKET>
    LOCATE '10' IN R.CURR.DALY<EB.CUR.CURRENCY.MARKET,1> SETTING NN THEN
        SELL.RATE<1> = R.CURR.DALY<EB.CUR.SELL.RATE ,NN>
    END
    IF CURR<1> = "USD" THEN
        TOT.AMT.N = TOT.AMT.N + AMT.FCY<1>
    END ELSE
****OTHER FCY CURR*******************************
        AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
        TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
    END
**********END OF FIRST RECORD**********************************
    FOR I = 2 TO SELECTED.N
        ID.NO = ''
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        COMP.CODE<I>    = R.TT<TT.TE.CO.CODE>
        TT.DATE = AUTH.DAT<I>
         COMP.CODE<I>   = R.TT<TT.TE.CO.CODE>
        KEY.ID<I> = "USD"
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        LOCATE '10' IN R.CURR.DALY<EB.CUR.CURRENCY.MARKET,1> SETTING NN THEN
            SELL.RATE<I> = R.CURR.DALY<EB.CUR.SELL.RATE,NN>
        END
        IF CUST<I> # CUST<I-1> THEN
            IF  TOT.AMT.N GE 30000 THEN
                CALL F.READ(FN.CUSTOMER, CUST<I-1>, R.CUSTOMER, F.CUSTOMER, E1)
                CU.SECT   = R.CUSTOMER<EB.CUS.SECTOR>
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NSN       = LOCAL.REF<1,CULR.NSN.NO>
                REG.NO    = LOCAL.REF<1,CULR.COM.REG.NO>
                IF NSN NE '' THEN
                  ID.NO = NSN
                END ELSE
                  ID.NO = REG.NO

                END

                CUST.COMP = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
                Y.RET.DATA<-1> = CUST<I-1> :"*":CU.SECT :"*": ID.NO :"*": CURR<I-1>:"*":TOT.AMT.N:"*":COMP:"*":LAST.W.DAY:"*":COMP.CODE<I-1>

                TOT.AMT.N = 0 ; REG.NO = ''
            END ELSE
                TOT.AMT.N = 0
            END
            IF CURR<I> = "USD" THEN
                    TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
            END ELSE
                LOCATE '10' IN R.CURR.DALY<EB.CUR.CURRENCY.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<EB.CUR.SELL.RATE,NN>
                END
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
            END     ;** END OF OTHER FCY**
*****SAME CURRENCY*************
        END ELSE
            IF CURR<I> = "USD" THEN
                    TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
            END ELSE
****OTHER FCY**********
                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                END
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
            END     ;**END OF OTHER FCY**
*****END OF SAME CURRENCY******
        END         ;**END OF SAME CURRENCY**
        IF I = SELECTED.N THEN
         *   TEXT = 'LAST RECORD' ; CALL REM
            IF TOT.AMT.N GE 30000 THEN
                CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
               NSN = LOCAL.REF<1,CULR.NSN.NO>
                REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
                 IF NSN NE '' THEN
                  ID.NO = NSN
                END ELSE
                  ID.NO = REG.NO

                END
                 CUST.COMP = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
          Y.RET.DATA<-1> = CUST<I> :"*":CU.SECT :"*": ID.NO:"*": CURR<I>:"*":TOT.AMT.N:"*":COMP:"*":LAST.W.DAY:"*":COMP.CODE<I>
          END
        END
        CU.SECT = '' ; ID.NO = ''
    NEXT I
    RETURN
END
