* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 18/02/2013******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.DB.EQV.USD.50000(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    TOT.TT = 0 ; TOT.AMT = 0
    TOT.AMT.SUN = 0 ; TOT.AMT.MON = 0 ; TOT.AMT.TUE = 0 ; TOT.AMT.WED = 0 ; TOT.AMT.THU = 0
    TOT.ALL.DAY = 0

    YTEXT = "����� ����� ����� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
    YTEXT = "���� ����� ����� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND CONTRACT.GRP EQ '' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CUSTOMER.1 BY AUTH.DATE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    CUST<1>         = R.TT<TT.TE.CUSTOMER.1>
    CURR<1>         = R.TT<TT.TE.CURRENCY.1>
    AMT.FCY<1>      = R.TT<TT.TE.AMOUNT.FCY.1>
    AMT.LCY<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1>     = R.TT<TT.TE.AUTH.DATE>
    TT.DATE = AUTH.DAT<1>
    DAY.NUM = ICONV(TT.DATE,"DW")
    DAY.NUM2= OCONV(DAY.NUM,"DW")

    IF CURR<1> = "USD" THEN
        BEGIN CASE
        CASE DAY.NUM2 = 7
            TOT.AMT.SUN = TOT.AMT.SUN + AMT.FCY<1>
        CASE DAY.NUM2 = 1
            TOT.AMT.MON = TOT.AMT.MON + AMT.FCY<1>
        CASE DAY.NUM2 = 2
            TOT.AMT.TUE = TOT.AMT.TUE + AMT.FCY<1>
        CASE DAY.NUM2 = 3
            TOT.AMT.WED = TOT.AMT.WED + AMT.FCY<1>
        CASE DAY.NUM2 = 4
            TOT.AMT.THU = TOT.AMT.THU + AMT.FCY<1>
        END CASE
    END ELSE
****OTHER FCY CURR*******************************
        BEGIN CASE
        CASE DAY.NUM2 = 7
            RATE.SUN = 6.8281
            AMT.EQV.USD = AMT.LCY<1> / RATE.SUN
            TOT.AMT.SUN = TOT.AMT.SUN + AMT.EQV.USD
        CASE DAY.NUM2 = 1
            RATE.MON = 6.8289
            AMT.EQV.USD = AMT.LCY<1> / RATE.MON
            TOT.AMT.MON = TOT.AMT.MON + AMT.EQV.USD
        CASE DAY.NUM2 = 2
            RATE.TUE = 6.8289
            AMT.EQV.USD = AMT.LCY<1> / RATE.TUE
            TOT.AMT.TUE = TOT.AMT.TUE + AMT.EQV.USD
        CASE DAY.NUM2 = 3
            RATE.WED = 6.8296
            AMT.EQV.USD = AMT.LCY<1> / RATE.WED
            TOT.AMT.WED = TOT.AMT.WED + AMT.EQV.USD
        CASE DAY.NUM2 = 4
            RATE.THU = 6.8331
            AMT.EQV.USD = AMT.LCY<1> / RATE.THU
            TOT.AMT.THU = TOT.AMT.THU + AMT.EQV.USD
        END CASE
    END
**********END OF FIRST RECORD**********************************
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
*TEXT = 'CURR=':CURR<I> ; CALL REM
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
*TEXT = 'AMT.FCY=':AMT.FCY<I> ; CALL REM
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        TT.DATE = AUTH.DAT<I>
        DAY.NUM = ICONV(TT.DATE,"DW")
        DAY.NUM2= OCONV(DAY.NUM,"DW")
*TEXT = 'DAT.NO=': DAY.NUM2 ; CALL REM
        IF CUST<I> # CUST<I-1> THEN
            TOT.ALL.DAYS = TOT.AMT.SUN + TOT.AMT.MON + TOT.AMT.TUE + TOT.AMT.WED + TOT.AMT.THU
*   TEXT = 'TOT.DAYS=':TOT.ALL.DAYS ; CALL REM
            IF TOT.ALL.DAYS > 50000 THEN
                Y.RET.DATA<-1> = CUST<I-1> :"*": CURR<I-1>:"*":TOT.AMT.SUN:"*":TOT.AMT.MON:"*":TOT.AMT.TUE:"*":TOT.AMT.WED:"*":TOT.AMT.THU:"*":TOT.ALL.DAYS
                TOT.AMT.SUN = 0 ; TOT.AMT.MON = 0 ; TOT.AMT.TUE = 0 ; TOT.AMT.WED = 0 ; TOT.AMT.THU = 0 ; TOT.ALL.DAYS = 0
            END ELSE
                TOT.AMT.SUN = 0 ; TOT.AMT.MON = 0 ; TOT.AMT.TUE = 0 ; TOT.AMT.WED = 0 ; TOT.AMT.THU = 0 ; TOT.ALL.DAYS = 0
            END
            IF CURR<I> = "USD" THEN
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.FCY<I>
                CASE DAY.NUM2 = 1
                    TOT.AMT.MON = TOT.AMT.MON + AMT.FCY<I>
                CASE DAY.NUM2 = 2
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.FCY<I>
                CASE DAY.NUM2 = 3
                    TOT.AMT.WED = TOT.AMT.WED + AMT.FCY<I>
                CASE DAY.NUM2 = 4
                    TOT.AMT.THU = TOT.AMT.THU + AMT.FCY<I>
                END CASE
            END ELSE
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    RATE.SUN = 6.8281
                    AMT.EQV.USD = AMT.LCY<I> / RATE.SUN
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.EQV.USD
                CASE DAY.NUM2 = 1
                    RATE.MON = 6.8289
                    AMT.EQV.USD = AMT.LCY<I> / RATE.MON
                    TOT.AMT.MON = TOT.AMT.MON + AMT.EQV.USD
                CASE DAY.NUM2 = 2
                    RATE.TUE = 6.8289
                    AMT.EQV.USD = AMT.LCY<I> / RATE.TUE
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.EQV.USD
                CASE DAY.NUM2 = 3
                    RATE.WED = 6.8296
                    AMT.EQV.USD = AMT.LCY<I> / RATE.WED
                    TOT.AMT.WED = TOT.AMT.WED + AMT.EQV.USD
                CASE DAY.NUM2 = 4
                    RATE.THU = 6.8331
                    AMT.EQV.USD = AMT.LCY<I> / RATE.THU
                    TOT.AMT.THU = TOT.AMT.THU + AMT.EQV.USD
                END CASE
            END     ;** END OF OTHER FCY**
*****SAME CUSTOMER*************
        END ELSE
            IF CURR<I> = "USD" THEN
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.FCY<I>
                CASE DAY.NUM2 = 1
                    TOT.AMT.MON = TOT.AMT.MON + AMT.FCY<I>
                CASE DAY.NUM2 = 2
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.FCY<I>
                CASE DAY.NUM2 = 3
                    TOT.AMT.WED = TOT.AMT.WED + AMT.FCY<I>
                CASE DAY.NUM2 = 4
                    TOT.AMT.THU = TOT.AMT.THU + AMT.FCY<I>
                END CASE
            END ELSE
****OTHER FCY**********
                BEGIN CASE
                CASE DAY.NUM2 = 7
                    RATE.SUN = 6.8281
                    AMT.EQV.USD = AMT.LCY<I> / RATE.SUN
                    TOT.AMT.SUN = TOT.AMT.SUN + AMT.EQV.USD
                CASE DAY.NUM2 = 1
                    RATE.MON = 6.8289
                    AMT.EQV.USD = AMT.LCY<I> / RATE.MON
                    TOT.AMT.MON = TOT.AMT.MON + AMT.EQV.USD
                CASE DAY.NUM2 = 2
                    RATE.TUE = 6.8289
                    AMT.EQV.USD = AMT.LCY<I> / RATE.TUE
                    TOT.AMT.TUE = TOT.AMT.TUE + AMT.EQV.USD
                CASE DAY.NUM2 = 3
                    RATE.WED = 6.8296
                    AMT.EQV.USD = AMT.LCY<I> / RATE.WED
                    TOT.AMT.WED = TOT.AMT.WED + AMT.EQV.USD
                CASE DAY.NUM2 = 4
                    RATE.THU = 6.8331
                    AMT.EQV.USD = AMT.LCY<I> / RATE.THU
                    TOT.AMT.THU = TOT.AMT.THU + AMT.EQV.USD
                END CASE
            END     ;**END OF OTHER FCY**
*****END OF SAME CUSTOMER******
        END         ;**END OF SAME CUSTOMER**
        IF I = SELECTED.N THEN
            TEXT = 'LAST RECORD' ; CALL REM
            TOT.ALL.DAYS = TOT.AMT.SUN + TOT.AMT.MON + TOT.AMT.TUE + TOT.AMT.WED + TOT.AMT.THU
            TEXT = 'TOT.DAYS=':TOT.ALL.DAYS ; CALL REM
            IF TOT.ALL.DAYS > 50000 THEN
                Y.RET.DATA<-1> = CUST<I> :"*": CURR<I>:"*":TOT.AMT.SUN:"*":TOT.AMT.MON:"*":TOT.AMT.TUE:"*":TOT.AMT.WED:"*":TOT.AMT.THU:"*":TOT.ALL.DAYS
            END
        END
    NEXT I
    RETURN
END
