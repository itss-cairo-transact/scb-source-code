* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>226</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CUSTOMER.DUBLECT.TEST
*PROGRAM CUSTOMER.DUBLECT.TEST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE

*TEXT = 'HI ' ;CALL REM
CALL TXTINP(" INPUT THE BRANCH CODE ", 8, 22, 3.1, T.PAR1)
BR.CODE = COMI
IF BR.CODE AND NUM(BR.CODE) THEN
   GOSUB INITIATE
   GOSUB PRINT.HEAD
*Line [ 41 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
   GOSUB CALLDB

   CALL PRINTER.OFF
   CALL PRINTER.CLOSE(REPORT.ID,0,'')
   *TEXT = "DFSF" ; CALL REM
END
ELSE
   TEXT = " INVALID BRANCH NUMBER "  ; CALL REM
END

GOTO PROGRAM.END
*==============================================================
INITIATE:
    REPORT.ID='CUSTOMER.DUBLECT.TEST'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
   FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = ''
   CALL OPF(FN.CUST,F.CUST)

   T.SEL = "SELECT FBNK.CUSTOMER WITH ACCOUNT.OFFICER EQ ":BR.CODE:" AND POSTING.RESTRICT NE 99 BY @ID"
   T.SEL := ' EVAL"@ID':":'|':":"ARABIC.NAME":":'|':":'OLD.CUST.ID"'
   KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
   CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
   TEXT = SELECTED:" CUSTOMER SELECTED" ; CALL REM



*   T.SEL.IM = "SELECT F.IM.DOCUMENT.IMAGE WITH IMAGE.REFERENCE NE '' AND ": 'EVAL"LEN(IMAGE.REFERENCE)" = 7 BY IMAGE.REFERENCE '
*   T.SEL.IM := " AND IMAGE.REFERENCE LIKE 5... "
   T.SEL.IM = "SELECT F.IM.DOCUMENT.IMAGE WITH IMAGE.REFERENCE NE '' AND DEPT.CODE EQ ":BR.CODE
*   IF LEN(BR.CODE) = 1 THEN
*      T.SEL.IM :=  ' AND EVAL"LEN(IMAGE.REFERENCE)" = 7 '
*   END
   T.SEL.IM := ' BY IMAGE.REFERENCE  EVAL"IMAGE.REFERENCE':":'|':":"INPUTTER":":'|':":"AUTHORISER":'" '
*   TEXT = T.SEL.IM ; CALL REM
   KEY.LIST.IM="" ; SELECTED.IM="" ;  ER.MSG.IM=""
   CALL EB.READLIST(T.SEL.IM,KEY.LIST.IM,"",SELECTED.IM,ER.MSG.IM)
*   TEXT = SELECTED.IM:" IMAGE SELECTED" ; CALL REM

*   NEXT.START = 1
   EXISTED.CNT = '' ; NOT.EXISTED.CNT = ''
   IF SELECTED AND SELECTED.IM THEN
      FOR I = 1 TO SELECTED
*      FOR I = 1 TO 10

          CUST.IMG.EXIST = ''
          CUST.NO = FIELD(KEY.LIST<I> ,"|",1)
          NAME1 = FIELD(KEY.LIST<I> ,"|",2)
          OLD.CUST.NO = FIELD(KEY.LIST<I> ,"|",3)
          CUST.IMG.EXIST = ''
*          FOR IMG.LP = NEXT.START TO SELECTED.IM WHILE CUST.IMG.EXIST = ''
          FOR IMG.LP = 1 TO SELECTED.IM WHILE CUST.IMG.EXIST = ''
*          IF CUST.NO = "90200270" AND IMAGE.REF.VAR GT 90200200 THEN
*             IMAGE.REF.VAR = FIELD(KEY.LIST.IM<IMG.LP> ,"|",1)
*             TEXT = "IM.REF  = ":IMAGE.REF.VAR ; CALL REM
*          END
*              TEXT = KEY.LIST.IM<IMG.LP> ; CALL REM
              SEPERATOR.PARM = ''
              SEPERATOR.PARM = INDEX(KEY.LIST.IM<IMG.LP>,'|',2)
              IF SEPERATOR.PARM THEN
                 IMAGE.REF.VAR = FIELD(KEY.LIST.IM<IMG.LP> ,"|",1)
                 INPUTTER.VAR = FIELD(KEY.LIST.IM<IMG.LP> ,"|",2)
                 AUTORISER.VAR = FIELD(KEY.LIST.IM<IMG.LP> ,"|",3)
*                 IF IMG.LP < 10 THEN
*                    TEXT = IMAGE.REF.VAR:"  INPUTTER =":INPUTTER.VAR ; CALL REM
*                    TEXT = IMAGE.REF.VAR:"  AUTHORISER =":AUTORISER.VAR ; CALL REM
*                 END
                 IF TRIM(CUST.NO) = TRIM(IMAGE.REF.VAR) THEN
   *                 NEXT.START = IMG.LP+1
                    CUST.IMG.EXIST = 'YES'
                 END
              END
          NEXT IMG.LP

             XX = SPACE(80)
             XX<1,1>[1,40]   = NAME1
             XX<1,1>[42,1]   = " "
             XX<1,1>[50,10]  = OLD.CUST.NO
             XX<1,1>[74,10]  = CUST.NO
             IF CUST.IMG.EXIST = 'YES' THEN
                IMAGE = '**����**'
                EXISTED.CNT += 1
                XX<1,1>[100,10]  = IMAGE
                XX<1,1>[115,10] = FIELD(INPUTTER.VAR,"_",2)
                XX<1,1>[135,10] = FIELD(AUTORISER.VAR,"_",2)

             END ELSE
                IMAGE = '�� ����'
                NOT.EXISTED.CNT += 1
                XX<1,1>[100,10]  = IMAGE
             END
             PRINT XX<1,1>
             *PRINT NAME1:SPACE(15):OLD.CUST.NO
             *IF I # SELECTED THEN PRINT STR('-',75)
*          END
      NEXT I

      PRINT STR('=',120)
      PRINT
      PRINT "����    = ":EXISTED.CNT:" ����"
      PRINT "�� ���� = ":NOT.EXISTED.CNT:" ����"
      PRINT "������� = ":SELECTED:" ����"
   END ELSE
       ENQ.ERROR = "NO RECORDS FOUND"
   END


   TEXT = 'END '; CALL REM
*   PRINT TEXT
RETURN
*===============================================================
PRINT.HEAD:
*Line [ 156 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL DBR("DEPT.ACCT.OFFICER":@FM:2,BR.CODE,BR.NAME)
    IF NOT(ETEXT) THEN
       BR.NAME = FIELD(BR.NAME , "." , 2)
       DATY = TODAY
       T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
       PR.HD ="'L'":SPACE(1):" ��� ���� ������ "
       PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(80):"��� ������ : ":"'P'"
       PR.HD :="'L'":" "
       PR.HD :="'L'":SPACE(58):BR.NAME
       PR.HD :="'L'":SPACE(55):STR('_',20)
       PR.HD :="'L'":" "
       PR.HD :="'L'":SPACE(1):" �����":SPACE(37):"��� ������ ������":SPACE(10):"��� ������ ������":SPACE(10):"�������" :SPACE(10):"���� ��������":SPACE(10):"����"
       PR.HD :="'L'":" "
       PR.HD :="'L'":SPACE(1):STR('_',120)
    END

    HEADING PR.HD
RETURN
*==============================================================
PROGRAM.END:

RETURN
END
