* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>2718</Rating>
*-----------------------------------------------------------------------------
**************************MOHAMED MOSSTAFA-2013/09/11********************************************
    SUBROUTINE E.NOF.CUSTOMER.OPEN(Y.RET.DATA)
*    PROGRAM E.NOF.CUSTOMER.OPEN
*************************************************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.SYSTEM.ID
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

**------------------------------------
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 62 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
*-------------------------------------------------------------------------
    RETURN

*==============================================================
INITIATE1:

    RETURN
*==============================================================
INITIATE:
    SCB.AMOUNT = ''
    FROM.DATE = ''
    END.DATE = ''
    FROM.TRN.DATE = ''
    END.TRN.DATE = ''
    RETURN
*===============================================================
CALLDB:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF (FN.CU,F.CU)

    FN.CUS.ACC ='FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC  =''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.ACC='FBNK.ACCOUNT' ; F.ACC=''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC.HIS='FBNK.ACCOUNT$HIS' ; F.ACC.HIS='' ; R.ACC.HIS = ''
    CALL OPF(FN.ACC.HIS,F.ACC.HIS)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.ACC.CLSED ='FBNK.ACCOUNT.CLOSED' ; F.ACC.CLSED  =''
    CALL OPF(FN.ACC.CLSED,F.ACC.CLSED)

*    FN.SYSID = 'F.EB.SYSTEM.ID' ; F.SYSID = '' ; R.SYSID = ''
*    CALL OPF(FN.SYSID,F.SYSID)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*=========================================================================================================================
    LOCATE "FROM.DATE"     IN D.FIELDS<1> SETTING YCRV.POS THEN FROM.DATE      = D.RANGE.AND.VALUE<YCRV.POS> ELSE RETURN
    LOCATE "TO.DATE"       IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE       = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
    LOCATE "FROM.TRN.DATE" IN D.FIELDS<1> SETTING YCRV.POS THEN FROM.TRN.DATE  = D.RANGE.AND.VALUE<YCRV.POS> ELSE RETURN
    LOCATE "TO.TRN.DATE"   IN D.FIELDS<1> SETTING YEDT.POS THEN END.TRN.DATE   = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
    LOCATE "AMOUNT"        IN D.FIELDS<1> SETTING YEDT.POS THEN SCB.AMOUNT     = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
*=========================================================================================================================
    T.SEL = "SELECT ":FN.CU:" WITH CONTACT.DATE GE ":FROM.DATE:" AND CONTACT.DATE LE ":END.DATE:" AND COMPANY.BOOK EQ ":COMP:" BY @ID"
*    T.SEL = "SELECT ":FN.CU:" WITH @ID EQ 11217948"
*    T.SEL = "SELECT ":FN.CU:" WITH @ID EQ 11300917"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    TEXT = SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
*            FLG1 = 0
            CUSID = KEY.LIST<I>

            CALL F.READ(FN.CU,CUSID,R.CU,F.CU,ERRCU)
            SCB.CRD.NM = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            CALL F.READ(FN.CUS.ACC,CUSID,R.CUS.ACC,F.CUS.ACC,ERR1)
            LOOP
                REMOVE ACC.NO FROM R.CUS.ACC SETTING POS
            WHILE ACC.NO:POS
                CALL F.READ(FN.ACC,ACC.NO,R.ACC,F.ACC,ACC.ERR1)
                CATEG = R.ACC<AC.CATEGORY>
*                FLG1 = 1
                IF CATEG = 1001 OR CATEG = 1002 OR ( CATEG GE 6501 AND CATEG LE 6512 ) THEN
                    SCB.CONT.DATE = R.CU<EB.CUS.CONTACT.DATE>
                    SCB.COMP.CRD = R.ACC<AC.CO.CODE>
                    CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.COMP.CRD,SCB.COMP.CRD.NM)
                    GOSUB GET.STMT
                END
*-------------------------------------------------------------------------------------------
            REPEAT
        NEXT I
*------------------------------- IF ACCOUNT IS CLOSED ---------------------------------------
        T.SEL1 = "SELECT FBNK.ACCOUNT.CLOSED WITH @ID LIKE ":CUSID:"..."
        CALL EB.READLIST(T.SEL1, KEY.LIST1, "", SELECTED1, ASD1)
        IF SELECTED1 THEN
            FOR II = 1 TO SELECTED1
                ACC.NO = KEY.LIST1<II>
                ACC.NO1 = ACC.NO:";1"
                CALL F.READ(FN.ACC.HIS,ACC.NO1,R.ACC.HIS,F.ACC.HIS,ACC.ERR1.HIS)
                CATEG = R.ACC.HIS<AC.CATEGORY>
*                FLG1 = 1
                IF CATEG = 1001 OR CATEG = 1002 OR ( CATEG GE 6501 AND CATEG LE 6512 ) THEN
                    SCB.CONT.DATE = R.CU<EB.CUS.CONTACT.DATE>
                    SCB.COMP.CRD = R.ACC.HIS<AC.CO.CODE>
                    CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.COMP.CRD,SCB.COMP.CRD.NM)
                    GOSUB GET.STMT
                END
            NEXT II
        END
*        END
    END
    RETURN
*--------------------------------------------------------------------------------------------
GET.STMT:
*--------
    CALL EB.ACCT.ENTRY.LIST(ACC.NO<1>,FROM.TRN.DATE,END.TRN.DATE,ID.LIST,OPENING.BAL,ER)

    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS1
    WHILE STE.ID:POS1

        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        IF R.STE<AC.STE.AMOUNT.LCY> GE SCB.AMOUNT THEN
*------------------------------------------------------------------------------------
            REC.ID         = '' ; SCB.CRD.AMOUNT = '' ; SCB.CRD.ACCT   = '' ; SCB.SYS.ID     = ''
            SCB.TRN.DATE   = '' ; SCB.INP.NO     = '' ; SCB.AUTH.NO    = '' ; SCB.INP.NO     = ''
            SCB.AUTH.NO    = '' ; SCB.INP.NM     = '' ; SCB.AUTH.NM    = '' ; FLG = ''
*------------------------------------------------------------------------------------
            REC.ID         = R.STE<AC.STE.OUR.REFERENCE>
            SCB.CRD.AMOUNT = R.STE<AC.STE.AMOUNT.LCY>
            SCB.CRD.ACCT   = R.STE<AC.STE.ACCOUNT.NUMBER>
            SCB.SYS.ID     = R.STE<AC.STE.SYSTEM.ID>
            SCB.TRN.DATE   = R.STE<AC.STE.BOOKING.DATE>
            SCB.INP.NO     = R.STE<AC.STE.INPUTTER>
            SCB.AUTH.NO    = R.STE<AC.STE.AUTHORISER>
            SCB.INP.NO     = FIELD (SCB.INP.NO,'_',2)
            SCB.AUTH.NO    = FIELD (SCB.AUTH.NO,'_',2)

            CALL DBR('USER':@FM: EB.USE.USER.NAME,SCB.INP.NO,SCB.INP.NM)
            CALL DBR('USER':@FM: EB.USE.USER.NAME,SCB.AUTH.NO,SCB.AUTH.NM)

            IF SCB.SYS.ID = "FT" THEN REC.ID = R.STE<AC.STE.OUR.REFERENCE>:";1" ; GOSUB GETFT
            IF REC.ID[1,2] = "IN" THEN REC.ID = R.STE<AC.STE.TRANS.REFERENCE> ; GOSUB GETIN
            IF ( SCB.SYS.ID EQ "FT" OR REC.ID[1,2] = "IN" ) AND FLG = 1 THEN
*Line [ 194 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                KK = DCOUNT(SCB.DBT.ACCT.M,@VM)
                Y.RET.DATA <-1>  = ACC.NO:"*":SCB.CRD.NM :"*":SCB.COMP.CRD.NM :"*":SCB.COMP.TRN.NM:"*":SCB.CONT.DATE:"*":SCB.AMOUNT.FCY:"*":SCB.DBT.ACCT:"*":SCB.DBT.NM:"*":SCB.TRN.DATE:"*":SCB.INP.NM:"*":SCB.AUTH.NM:"*":REC.ID:"*":CUSID:"*":SCB.DBT.CUR:"*":SCB.AMOUNT.FCY.M<1,KK>
                IF KK GT 1 THEN
                    FOR ZZ = 1 TO KK-1
                        Y.RET.DATA <-2>  = "*":"*":"*":"*":SCB.CONT.DATE:"*":"*":SCB.DBT.ACCT.M<1,ZZ>:"*":SCB.DBT.NM.M<1,ZZ>:"*":SCB.TRN.DATE:"*":"*":"*":"*":CUSID:"*":"*":SCB.AMOUNT.FCY.M<1,ZZ>
                    NEXT ZZ
                    SCB.DBT.ACCT.M = '' ; SCB.AMOUNT.FCY.M = ''
                END
                SCB.AMOUNT.FCY.M = ''

*                Y.RET.DATA   = ACC.NO:"*":SCB.CRD.NM :"*":SCB.COMP.CRD.NM :"*":SCB.COMP.TRN.NM:"*":SCB.CONT.DATE:"*":SCB.AMOUNT.FCY:"*":SCB.DBT.ACCT:"*":SCB.DBT.NM:"*":SCB.TRN.DATE:"*":SCB.INP.NM:"*":SCB.AUTH.NM:"*":REC.ID:"*":CUSID
*                PRINT Y.RET.DATA
            END

        END
    REPEAT
    RETURN
*-----------------------------------------------------------------------------------------------------------------
GETFT:
*-----
    FN.FT  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    CALL F.READ(FN.FT,REC.ID,R.FT,F.FT,E2)
    IF R.FT<FT.TRANSACTION.TYPE> NE "AC12" AND R.FT<FT.TRANSACTION.TYPE> NE "AC13" THEN
        IF R.FT<FT.CREDIT.CUSTOMER> NE R.FT<FT.DEBIT.CUSTOMER> THEN
            FLG = 1
            IF R.FT<FT.DEBIT.AMOUNT> THEN
                SCB.AMOUNT.FCY = R.FT<FT.DEBIT.AMOUNT>
            END ELSE
                IF R.FT<FT.CREDIT.AMOUNT> THEN SCB.AMOUNT.FCY = R.FT<FT.CREDIT.AMOUNT>
            END

            SCB.DBT.ACCT = R.FT<FT.DEBIT.ACCT.NO>
            SCB.DBT.CU = R.FT<FT.DEBIT.CUSTOMER>
            SCB.DBT.CUR = R.FT<FT.DEBIT.CURRENCY>
            IF SCB.DBT.ACCT THEN
                IF NOT(SCB.DBT.CU) AND SCB.DBT.ACCT[1,3] EQ 994 THEN SCB.DBT.CU = SCB.DBT.ACCT[1,8]
                IF NUM(SCB.DBT.ACCT) THEN
                    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,SCB.DBT.CU,LOCAL.REF)
                    SCB.DBT.NM = LOCAL.REF<1,CULR.ARABIC.NAME>
                END ELSE
                    SCB.CAT.NO = R.FT<FT.DEBIT.ACCT.NO>[11,4]
                    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,SCB.DBT.ACCT,SCB.DBT.NM)
                    IF ETEXT THEN
                        SCB.CAT.NO = R.FT<FT.DEBIT.ACCT.NO>[3,5]
                        CALL DBR ('CATEGORY':@FM:EB.CAT.SHORT.NAME,SCB.CAT.NO,SCB.DBT.NM)

                    END
                END
            END
            SCB.COMP.TRN = R.FT<FT.CO.CODE>
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.COMP.TRN,SCB.COMP.TRN.NM)

        END
    END
    RETURN
*-----------------------------------------------------------------------------------------------------------------
GETIN:
*-----
*DEBUG
    FN.INF  = 'F.INF.MULTI.TXN' ; F.INF = '' ; R.INF = ''
    CALL OPF(FN.INF,F.INF)
    IF INDEX(REC.ID,"\",1) THEN  REC.ID = FIELD (REC.ID,'\',1)
    IF INDEX(REC.ID,"/",1) THEN  REC.ID = FIELD (REC.ID,'/',1)
    CALL F.READ(FN.INF,REC.ID,R.INF,F.INF,E.INF)
*--------------------------------------------------------------------------------------
    SCB.CHK.CU = '' ; SCB.COMP.TRN.NM = '' ; J = 0
    SCB.N.MULT = '' ; SCB.AC.ID = '' ; SCB.AC.CU = '' ; SCB.AC.ID.H = ''
    SCB.DBT.ACCT = '' ; SCB.AC.ID.H = '' ; SCB.DBT.ACCT   = '' ; SCB.DBT.CUR    = ''
    SCB.DBT.NM   = '' ; SCB.CAT.NO  = '' ; SCB.AMOUNT.FCY = '' ; SCB.AMOUNT.LCY = ''
    SCB.AMOUNT.FCY.M = '' :  SCB.DBT.ACCT.M = '' ; SCB.DBT.NM.M = ''
*--------------------------------------------------------------------------------------

*Line [ 268 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    SCB.N.MULT = DCOUNT(R.INF<INF.MLT.ACCOUNT.NUMBER>,@VM)

    FOR K = 1 TO SCB.N.MULT
        IF R.INF<INF.MLT.ACCOUNT.NUMBER,K> # SCB.CRD.ACCT AND R.INF<INF.MLT.SIGN,K> = "DEBIT" THEN
            FLG = 1 ; J ++
            SCB.AC.ID = R.INF<INF.MLT.ACCOUNT.NUMBER,K>

            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,SCB.AC.ID,SCB.DBT.CU)
            IF ETEXT THEN
                SCB.AC.ID.H = SCB.AC.ID:";1"
                CALL DBR('ACCOUNT$HIS':@FM:AC.CUSTOMER,SCB.AC.ID.H,SCB.DBT.CU)
            END

            SCB.DBT.ACCT = R.INF<INF.MLT.ACCOUNT.NUMBER,K>
            SCB.DBT.CUR  = R.INF<INF.MLT.CURRENCY,K>
            IF SCB.DBT.ACCT THEN
                IF NOT(SCB.DBT.CU) AND SCB.DBT.ACCT[1,3] EQ 994 THEN SCB.DBT.CU = SCB.DBT.ACCT[1,8]
                IF NUM(SCB.DBT.ACCT[1,3]) THEN
                    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,SCB.DBT.CU,LOCAL.REF)
                    SCB.DBT.NM = LOCAL.REF<1,CULR.ARABIC.NAME>
                END ELSE
                    SCB.CAT.NO = SCB.DBT.ACCT[11,4]
                    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,SCB.DBT.ACCT,SCB.DBT.NM)
                    IF ETEXT THEN
                        SCB.CAT.NO = R.FT<FT.DEBIT.ACCT.NO>[3,5]
                        CALL DBR ('CATEGORY':@FM:EB.CAT.SHORT.NAME,SCB.CAT.NO,SCB.DBT.NM)
                    END
                END
                IF SCB.DBT.ACCT THEN
                    SCB.DBT.ACCT.M<1,J> = SCB.DBT.ACCT
                    SCB.DBT.NM.M<1,J> = SCB.DBT.NM
                    IF SCB.DBT.CUR NE "EGP" THEN
                        SCB.AMOUNT.FCY.M<1,J> = R.INF<INF.MLT.AMOUNT.FCY,K>
                    END ELSE
                        SCB.AMOUNT.FCY.M<1,J> = R.INF<INF.MLT.AMOUNT.LCY,K>
                    END
                END


                SCB.COMP.TRN = R.INF<INF.MLT.CO.CODE>
                CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.COMP.TRN,SCB.COMP.TRN.NM)
            END
        END
        IF R.INF<INF.MLT.ACCOUNT.NUMBER,K> = SCB.CRD.ACCT THEN
            SCB.DBT.CUR  = R.INF<INF.MLT.CURRENCY,K>
            IF SCB.DBT.CUR NE "EGP" THEN
                SCB.AMOUNT.FCY = R.INF<INF.MLT.AMOUNT.FCY,K>
            END ELSE
                SCB.AMOUNT.FCY = R.INF<INF.MLT.AMOUNT.LCY,K>
            END
        END
    NEXT K
    RETURN
*-------------------------------------------------------------------------------------------------------------------
