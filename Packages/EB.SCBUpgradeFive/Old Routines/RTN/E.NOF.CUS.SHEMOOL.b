* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 16/5/2017******************
*-----------------------------------------------------------------------------
* <Rating>775</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CUS.SHEMOOL(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.SHEMOOL
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""

    FN.CUS.SHM = 'F.SCB.CUS.SHEMOOL' ; F.CUS.SHM = '' ; R.CUS.SHM = '' ; RETRY1 = ''  ; E1 = ''
    CALL OPF(FN.CUS.SHM,F.CUS.SHM)


    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    TOT.F.GV = 0 ; TOT.M.GV = 0  ; TOT.CUS.L15 = 0 ; TOT.CUS.RG16TO21 = 0
    TOT.CUS.RG22TO30 = 0 ; TOT.CUS.RG31TO40 = 0 ;TOT.CUS.RG41TO60 = 0 ; TOT.CUS.G61 = 0
    N.SEL = "SELECT F.SCB.CUS.SHEMOOL BY BR.GOVR BY CUS.AGE.AT.OP BY CUS.GENDER "
**    N.SEL = "SELECT F.SCB.CUS.SHEMOOL WITH BR.GOVR EQ '1' BY CUS.AGE.AT.OP BY CUS.GENDER "
    CALL EB.READLIST(N.SEL, KEY.LIST, "", SELECTED, ASD.N)

    TEXT = "SEL=":SELECTED ; CALL REM
    CALL F.READ(FN.CUS.SHM, KEY.LIST<1>, R.CUS.SHM ,F.CUS.SHM, E1)
    CUS.GOV<1>      = R.CUS.SHM<CSHM.BR.GOVR>
    CUS.GEN<1>      = R.CUS.SHM<CSHM.CUS.GENDER>
    IF CUS.GEN<1> = 'Female'  THEN
        TOT.F.GV = TOT.F.GV + 1
    END ELSE
        IF CUS.GEN<1> = 'Male' THEN
            TOT.M.GV = TOT.M.GV + 1
        END
    END
    CUS.AGE<1>      = R.CUS.SHM<CSHM.CUS.AGE.AT.OP>
    BEGIN CASE
    CASE CUS.AGE<1> LE '15'
        TOT.CUS.L15 = TOT.CUS.L15 + 1
**  TEXT = 'AGELT15'
**  TEXT = 'TOT.CUS.L15=' :TOT.CUS.L15 ; CALL REM
    CASE ((CUS.AGE<1> GE '16') AND (CUS.AGE<1> LE '21'))
        TOT.CUS.RG16TO21 = TOT.CUS.RG16TO21 + 1
**     TEXT = 'CUSGE16' ; CALL REM
**     TEXT = 'TOT16=':TOT.CUS.RG16TO21 ; CALL REM
    CASE ((CUS.AGE<1> GE '22') AND (CUS.AGE<1> LE '30'))
        TOT.CUS.RG22TO30 = TOT.CUS.RG22TO30 + 1
    CASE ((CUS.AGE<1> GE '31') AND (CUS.AGE<1> LE '40'))
        TOT.CUS.RG31TO40 = TOT.CUS.RG31TO40 + 1
    CASE ((CUS.AGE<1> GE '41') AND (CUS.AGE<1> LE '60'))
        TOT.CUS.RG41TO60 = TOT.CUS.RG41TO60 + 1
    CASE CUS.AGE<1> GE '61'
        TOT.CUS.G61 = TOT.CUS.G61 + 1
    END CASE
    FOR I = 2 TO SELECTED
**    FOR I = 2 TO 5
        CALL F.READ(FN.CUS.SHM, KEY.LIST<I>, R.CUS.SHM, F.CUS.SHM, E2)
        CUS.GOV<I>      = R.CUS.SHM<CSHM.BR.GOVR>
        IF CUS.GOV<I> # CUS.GOV<I-1> THEN
            Y.RET.DATA<-1> = CUS.GOV<I-1> :"*":TOT.CUS.L15:"*":TOT.CUS.RG16TO21:"*":TOT.CUS.RG22TO30:"*":TOT.CUS.RG31TO40:"*":TOT.CUS.RG41TO60:"*":TOT.CUS.G61:"*":TOT.M.GV:"*":TOT.F.GV
            TOT.CUS.L15 = 0 ; TOT.CUS.RG16TO21 = 0 ; TOT.CUS.RG22TO30= 0 ; TOT.F.GV = 0 ; TOT.M.GV = 0 ; TOT.CUS.RG31TO40 = 0 ; TOT.CUS.RG41TO60 = 0 ; TOT.CUS.G61 = 0
*****SAME GOVERNORATE*************
        END ELSE
            CUST<I>         = R.CUS.SHM<CSHM.CUST.NO>
            CUS.GEN<I>      = R.CUS.SHM<CSHM.CUS.GENDER>
            IF CUS.GEN<I> = 'Female' THEN
                TOT.F.GV = TOT.F.GV + 1
            END ELSE
                IF CUS.GEN<I> = 'Male' THEN
                    TOT.M.GV = TOT.M.GV + 1
*    TEXT = 'M' ; CALL REM
*    TEXT = 'TOT.M=':TOT.M.GV ; CALL REM
                END
            END
            CUS.GOV<I>      = R.CUS.SHM<CSHM.BR.GOVR>
            CUS.AGE<I>      = R.CUS.SHM<CSHM.CUS.AGE.AT.OP>
            BEGIN CASE
            CASE CUS.AGE<I> LE '15'
                TOT.CUS.L15 = TOT.CUS.L15 + 1
**      TEXT = 'AGELT15'
**      TEXT = 'TOT.CUS.L15=' :TOT.CUS.L15 ; CALL REM
            CASE ((CUS.AGE<I> GE '16') AND (CUS.AGE<I> LE '21'))
**      TEXT = 'CUSGE16' ; CALL REM
                TOT.CUS.RG16TO21 = TOT.CUS.RG16TO21 + 1
**        TEXT = 'TOT16=':TOT.CUS.RG16TO21 ; CALL REM
            CASE ((CUS.AGE<I> GE '22') AND (CUS.AGE<I> LE '30'))
                TOT.CUS.RG22TO30 = TOT.CUS.RG22TO30 + 1
            CASE ((CUS.AGE<I> GE '31') AND (CUS.AGE<I> LE '40'))
                TOT.CUS.RG31TO40 = TOT.CUS.RG31TO40 + 1
            CASE ((CUS.AGE<I> GE '41') AND (CUS.AGE<I> LE '60'))
                TOT.CUS.RG41TO60 = TOT.CUS.RG41TO60 + 1
            CASE CUS.AGE<I> GE '61'
                TOT.CUS.G61 = TOT.CUS.G61 + 1
            END CASE
        END
*****END OF SAME GOVERNORATE*************
        IF I = SELECTED THEN
            Y.RET.DATA<-1> = CUS.GOV<I> :"*":TOT.CUS.L15:"*":TOT.CUS.RG16TO21:"*":TOT.CUS.RG22TO30:"*":TOT.CUS.RG31TO40:"*":TOT.CUS.RG41TO60:"*":TOT.CUS.G61:"*":TOT.M.GV:"*":TOT.F.GV
        END
    NEXT I
    RETURN
END
