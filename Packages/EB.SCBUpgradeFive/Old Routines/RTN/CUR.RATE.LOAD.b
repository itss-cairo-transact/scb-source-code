* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>2323</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CUR.RATE.LOAD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    IF ID.COMPANY NE "EG0010001" THEN
        TEXT = "COMPANY MUST BE EG0010001" ; CALL REM
    END ELSE
*-----------------------------------------------------
        EXECUTE "sh CUR.RAT.LOD/lo_cur.sh"
*-----------------------------------------------------
        OPENSEQ "CUR.RAT.LOD" , "CURRENCY.RATE.OUT" TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"CUR.RAT.LOD":' ':"CURRENCY.TEMP.OUT.AFT"
            HUSH OFF
        END
        OPENSEQ "CUR.RAT.LOD" , "CURRENCY.RATE.OUT" TO BB ELSE
            CREATE BB THEN
            END ELSE
                STOP 'Cannot create CURRENCY.RATE.OUT.AFT File IN CUR.RAT.LOD'
            END
        END
*-----------------------------------------------------
        OPENSEQ "CUR.RAT.LOD" , "Currency.txt" TO BB1 THEN
            CLOSESEQ BB1
            HUSH ON
            EXECUTE 'DELETE ':"CUR.RAT.LOD":' ':"Currency.txt"
            HUSH OFF
        END
        OPENSEQ "CUR.RAT.LOD" , "Currency.txt" TO BB1 ELSE
            CREATE BB1 THEN
            END ELSE
                STOP 'Cannot create Currency.txt File IN CUR.RAT.LOD'
            END
        END
*-----------------------------------------------------
        OPENSEQ "CUR.RAT.LOD" , "Currencyn.txt" TO BB2 THEN
            CLOSESEQ BB2
            HUSH ON
            EXECUTE 'DELETE ':"CUR.RAT.LOD":' ':"Currencyn.txt"
            HUSH OFF
        END
        OPENSEQ "CUR.RAT.LOD" , "Currencyn.txt" TO BB2 ELSE
            CREATE BB2 THEN
            END ELSE
                STOP 'Cannot create Currency.txt File IN CUR.RAT.LOD'
            END
        END
*-----------------------------------------------------
        OPENSEQ "/home/signat" , "SCBINTERCUR.txt" TO BB3 THEN
            CLOSESEQ BB3
            HUSH ON
            EXECUTE 'DELETE ':"/home/signat":' ':"SCBINTERCUR.txt"
            HUSH OFF
        END
        OPENSEQ "/home/signat" , "SCBINTERCUR.txt" TO BB3 ELSE
            CREATE BB3 THEN
            END ELSE
                STOP 'Cannot create SCBINTERCUR.txt File IN CUR.RAT.LOD'
            END
        END
*-----------------------------------------------------
        SCB.OFS.SOURCE = "TESTOFS"
        SCB.VERSION  = "TEMP"
        SEQ.FILE.NAME = 'CUR.RAT.LOD'
        RECORD.NAME = 'cur_txt.l'
        OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
            TEXT = 'Unable to Locate ':SEQ.FILE.POINTER  ; CALL REM
            STOP
            RETURN
        END
        EOF = ''
        LOOP WHILE NOT(EOF)
            READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
*-----------------------------------------------------
                CUR.NAM = ''
                GOTO CREATE.REC
            END ELSE
                EOF = 1

            END
            CLOSESEQ SEQ.FILE.POINTER
            EXECUTE "sh CUR.RAT.LOD/s_cur.sh"
            RETURN

*==========================================================================
CREATE.REC:
*---------
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "US DOLLAR"  THEN CUR.NAM = "USD"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "STERLING POUND"  THEN CUR.NAM = "GBP"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "AUSTRALIAN DOLLAR"  THEN CUR.NAM = "AUD"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "EUR"  THEN CUR.NAM = "EUR"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "CANADIAN DOLLAR"  THEN CUR.NAM = "CAD"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "DANISH KRONE"  THEN CUR.NAM = "DKK"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "NORWEGIAN KRONE"  THEN CUR.NAM = "NOK"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "SWEDISH KRONE"  THEN CUR.NAM = "SEK"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "SWISS FRANC"  THEN CUR.NAM = "CHF"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "YEN (100)"  THEN CUR.NAM = "JPY"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "SAUDI RIAL"  THEN CUR.NAM = "SAR"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "KUWAITI DINAR"  THEN CUR.NAM = "KWD"
            IF  TRIM(FIELD(Y.MSG,"|",1))  = "UAE DIRHAM"  THEN CUR.NAM = "AED"
*-----------------------------------------------------------------------------------------------
            BUY.RATE.1  = FIELD(Y.MSG,"|",2)
            SELL.RATE.1 = FIELD(Y.MSG,"|",3)

            BUY.RATE.2  = FIELD(Y.MSG,"|",4)
            SELL.RATE.2 = FIELD(Y.MSG,"|",5)
            BUY.RATE.1  = FIELD(BUY.RATE.1,".",1):".":FMT(FIELD(BUY.RATE.1,".",2),'L%4')
            SELL.RATE.1 = FIELD(SELL.RATE.1,".",1):".":FMT(FIELD(SELL.RATE.1,".",2),'L%4')
            BUY.RATE.2  = FIELD(BUY.RATE.2,".",1):".":FMT(FIELD(BUY.RATE.2,".",2),'L%4')
            SELL.RATE.2 = FIELD(SELL.RATE.2,".",1):".":FMT(FIELD(SELL.RATE.2,".",2),'L%4')


            IF BUY.RATE.1 GT 0 THEN OFS.MESSAGE.DATA  = ",BUY.RATE:1:1=":BUY.RATE.1
            IF SELL.RATE.1 GT 0 THEN OFS.MESSAGE.DATA := ",SELL.RATE:1:1=":SELL.RATE.1

            IF BUY.RATE.2 GT 0 THEN OFS.MESSAGE.DATA := ",BUY.RATE:2:1=":BUY.RATE.2
            IF SELL.RATE.2 GT 0 THEN OFS.MESSAGE.DATA := ",SELL.RATE:2:1=":SELL.RATE.2

            SCB.APPL = "CURRENCY"
            SCB.OFS.HEADER  = SCB.APPL:",":SCB.VERSION
            SCB.OFS.MESSAGE = SCB.OFS.HEADER:"/I/PROCESS,,":CUR.NAM:OFS.MESSAGE.DATA
*            IF CUR.NAM THEN
*                TEXT = CUR.NAM:"-TRNS-BUY=":BUY.RATE.1 ; CALL REM
*                TEXT = CUR.NAM:"-TRNS-SELL=":SELL.RATE.1 ; CALL REM
*                TEXT = CUR.NAM:"-BANK-BUY=":BUY.RATE.2 ; CALL REM
*                TEXT = CUR.NAM:"-BANK-SELL=":SELL.RATE.2 ; CALL REM
*            END
            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END

* SCB R15 UPG 20160717 - S
*            CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE,SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE,SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E

            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END
            IF CUR.NAM = 'JPY' THEN
                BUY.RATE.1  = FIELD(Y.MSG,"|",2)  / 100
                SELL.RATE.1 = FIELD(Y.MSG,"|",3)  / 100

                BUY.RATE.2  = FIELD(Y.MSG,"|",4)  / 100
                SELL.RATE.2 = FIELD(Y.MSG,"|",5)  / 100
            END
*===================================== CREATE TEXT FILES =================================================================
            BUY.RATE.1.C = DROUND(BUY.RATE.1,6)
            SELL.RATE.1.C = DROUND(SELL.RATE.1,6)
            BUY.RATE.2.C = DROUND(BUY.RATE.2,6)
            SELL.RATE.2.C = DROUND(SELL.RATE.2,6)
*-------------------------------------------------------------------------------------------------------------------------
            BUY.RATE.1 = DROUND(BUY.RATE.1,5)
            SELL.RATE.1 = DROUND(SELL.RATE.1,5)
            BUY.RATE.2 = DROUND(BUY.RATE.2,5)
            SELL.RATE.2 = DROUND(SELL.RATE.2,5)
*-------------------------------------------------------------------------------------------------------------------------
            BB1.DATA = TRIM(FIELD(Y.MSG,"|",1))
            BB1.DATA := ",":FMT(FIELD(BUY.RATE.2,".",1), 'R%2'):".":FMT(FIELD(BUY.RATE.2,".",2),'L%4')
            BB1.DATA := ",":FMT(FIELD(SELL.RATE.2,".",1), 'R%2'):".":FMT(FIELD(SELL.RATE.2,".",2),'L%4')
            BB1.DATA := ",":FMT(FIELD(BUY.RATE.1,".",1), 'R%2'):".":FMT(FIELD(BUY.RATE.1,".",2),'L%4')
            BB1.DATA := ",":FMT(FIELD(SELL.RATE.1,".",1), 'R%2'):".":FMT(FIELD(SELL.RATE.1,".",2),'L%4')
            BB2.DATA = TRIM(FIELD(Y.MSG,"|",1))
            BB2.DATA := ",":FIELD(BUY.RATE.2.C,".",1):".":FMT(FIELD(BUY.RATE.2.C,".",2),'L%4')
            BB2.DATA := ",":FIELD(SELL.RATE.2.C,".",1):".":FMT(FIELD(SELL.RATE.2.C,".",2),'L%4')
            BB2.DATA := ",":FIELD(BUY.RATE.1.C,".",1):".":FMT(FIELD(BUY.RATE.1.C,".",2),'L%4')
            BB2.DATA := ",":FIELD(SELL.RATE.1.C,".",1):".":FMT(FIELD(SELL.RATE.1.C,".",2),'L%4')
*================================= CURRENCY FILE FOR INTERNET BANKING ===================================================
            INTE.CUR.NM = ''
*====================================================================================
            BB3.DATA = CUR.NAM
            BB3.DATA := ",":TRIM(FIELD(Y.MSG,"|",1))
            BB3.DATA := ",":FIELD(BUY.RATE.2.C,".",1):".":FMT(FIELD(BUY.RATE.2.C,".",2),'L%4')
            BB3.DATA := ",":FIELD(SELL.RATE.2.C,".",1):".":FMT(FIELD(SELL.RATE.2.C,".",2),'L%4')
            BB3.DATA := ",":FIELD(BUY.RATE.1.C,".",1):".":FMT(FIELD(BUY.RATE.1.C,".",2),'L%4')
            BB3.DATA := ",":FIELD(SELL.RATE.1.C,".",1):".":FMT(FIELD(SELL.RATE.1.C,".",2),'L%6')
*===================================== CURRENCY.TXT ======================================================================
            WRITESEQ BB1.DATA TO BB1 ELSE
            END
            PRINT BB1.DATA
*===================================== CURRENCYN.TXT =====================================================================
            WRITESEQ BB2.DATA TO BB2 ELSE
            END
            PRINT BB2.DATA
*===================================== SCBINTERCUR.txt =====================================================================
            WRITESEQ BB3.DATA TO BB3 ELSE
            END
            PRINT BB3.DATA
*=========================================================================================================================
        REPEAT
    END
    RETURN
END
