* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-68</Rating>
*-----------------------------------------------------------------------------
***    PROGRAM DALTEX.BANK.TEXT.2

    SUBROUTINE DALTEX.BANK.TEXT.2.1

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*-------------------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN
*------------------------------
INITIALISE:
*-----------
    FN.IN    = '/hq/opce/bclr/user/dltx'
    F.IN     = 0
    DD.REC   = ""
    RETURN
*----------------------------------------------------
BUILD.RECORD:
*------------

    COMP       = C$ID.COMPANY
    USR.DEP    = "1700":COMP[8,2]
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
    COMP.CO    = COMP[8,2]
    DAL.TODAY = TODAY
    IF LOCT EQ '1'  THEN
*        CALL CDT('',DAL.TODAY,"+1W")
    END ELSE
        CALL CDT('',DAL.TODAY,"+1W")
    END

    DAL.YEAR  = DAL.TODAY[1,4]
    DAL.MONTH = DAL.TODAY[5,2]
    DAL.DAY   = DAL.TODAY[7,2]

    DAL.DAY.1 = DAL.TODAY[1,4]:DAL.TODAY[5,2]:DAL.TODAY[7,2]
    CALL CDT('',DAL.DAY.1,"-1W")

    DAL.DAY.1.YEAR  = DAL.DAY.1[1,4]
    DAL.DAY.1.MONTH = DAL.DAY.1[5,2]
    DAL.DAY.1.DAY   = DAL.DAY.1[7,2]

    COMP      = C$ID.COMPANY
    COM.CODEE = COMP[8,2]
    COM.CODE  = TRIM(COM.CODEE, "0", "L")

    OPENSEQ "/hq/opce/bclr/user/dltx" , DAL.YEAR:"-":DAL.MONTH:"-":DAL.DAY:"-":"Sess2OUT":COM.CODE:".txt" TO DD THEN
        CLOSESEQ DD
        HUSH ON
        EXECUTE 'DELETE ':"/hq/opce/bclr/user/dltx":' ':DAL.YEAR:"-":DAL.MONTH:"-":DAL.DAY:"-":"Sess2OUT":COM.CODE:".txt"
        HUSH OFF
    END
    OPENSEQ "/hq/opce/bclr/user/dltx" , DAL.YEAR:"-":DAL.MONTH:"-":DAL.DAY:"-":"Sess2OUT":COM.CODE:".txt" TO DD ELSE
        CREATE DD THEN
            PRINT 'FILE CREATED IN /hq/opce/bclr/user/dltx'
        END
        ELSE
            STOP 'Cannot create File IN /hq/opce/bclr/user/dltx'
        END
    END

    EOF = ''

    FN.BR= 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    T.DAY = TODAY
    IF LOCT EQ '1'  THEN
*        CALL CDT('',T.DAY,"+1W")
    END ELSE
        CALL CDT('',T.DAY,"+1W")
    END
    T.SEL  = "SSELECT FBNK.BILL.REGISTER WITH (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BILL.CHQ.STA EQ 5 AND MATURITY.EXT EQ ":T.DAY:" AND BANK LIKE 00... AND CO.CODE EQ " : COMP

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
            CALL OPF( FN.BR,F.BR)

            CALL F.READ( FN.BR,KEY.LIST<I>, R.BR, F.BR, ETEXT)
            DD.DATA  = "120,"
            DD.DATA := KEY.LIST<I>:","
            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>:","
            BRANCH   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>[3,4]
            DD.DATA := BRANCH:","
            XX = ""
            IF R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.DR.CITY> EQ '' THEN
                XX = 1
            END ELSE
                XX =  R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.DR.CITY>
            END
            DD.DATA := XX:","
            DD.DATA := "17":","
            DD.DATA := R.BR<EB.BILL.REG.DEPT.CODE>:","

            LIQ.AC   = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,LIQ.AC,CUS.ID)

            IF CUS.ID NE '' THEN
                DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>:","
            END ELSE
                DD.DATA := '994999':COMP.CO:'105001':COMP.CO:","
            END

            DD.DATA := R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.NO>:","
            DD.DATA := R.BR<EB.BILL.REG.AMOUNT>:","

            SAM.YEAR  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MAT.DATE>[1,4]
            SAM.MONTH = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MAT.DATE>[5,2]
            SAM.DATE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MAT.DATE>[7,2]
            SAM       = SAM.DATE:"/":SAM.MONTH:"/":SAM.YEAR
            DD.DATA := SAM

            F.PATH  = FN.IN

            WRITESEQ DD.DATA TO DD ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    TEXT = "FINISHED " : SELECTED : " CHQs." ; CALL REM
    RETURN
END
