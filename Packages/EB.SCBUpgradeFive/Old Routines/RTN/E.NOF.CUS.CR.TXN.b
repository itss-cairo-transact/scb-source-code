* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1057</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CUS.CR.TXN(Y.ALL.REC)
*#    PROGRAM E.NOF.CUS.CR.TXN
***Mahmoud Elhawary******4/11/2010****************************
* Nofile routine to get CREDIT TXN                           *
* of customers accounts                                      *
**************************************************************

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 52 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    PREV.DAY  = FROM.DATE
    KK = 0
    II = 0
    XX = SPACE(100)
    STE.BAL  = ''
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CREDIT.VALUE" IN D.FIELDS<1> SETTING YCRV.POS THEN CR.VAL    = D.RANGE.AND.VALUE<YCRV.POS> ELSE RETURN
    LOCATE "START.DATE"   IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"     IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

*#    INPUT CR.VAL
*#    INPUT FROM.DATE
*#    INPUT END.DATE

    PREV.DAY = FROM.DATE

    CC.SEL  = "SELECT FBNK.CUSTOMER.ACCOUNT WITH COMPANY.CO EQ ":COMP
    CALL EB.READLIST(CC.SEL, K.LIST, "", SELECTED, ERR.CUS)
**    CRT "SELECTED = ":SELECTED
    LOOP
        REMOVE CUS.ID FROM K.LIST SETTING POS.CUS
    WHILE CUS.ID:POS.CUS
        CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
        CUS.POST = R.CUS<EB.CUS.POSTING.RESTRICT>
        CUS.LCL  = R.CUS<EB.CUS.LOCAL.REF>
        CUS.STAT = CUS.LCL<1,CULR.CREDIT.STAT>
        CUS.CRDT = CUS.LCL<1,CULR.CREDIT.CODE>
        IF CUS.POST LE 89 THEN
            IF CUS.STAT EQ '' AND ( CUS.CRDT NE '110' AND CUS.CRDT NE '120' ) THEN
                CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
                LOOP
                    REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.ACC
                WHILE ACCT.ID:POS.ACC
                    CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
                    ACCT.CAT = R.ACCT<AC.CATEGORY>
                    ACCT.CUR = R.ACCT<AC.CURRENCY>
                    IF ACCT.CAT NE 1002 THEN
*****************************
                        CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
                        STE.BAL     = OPENING.BAL
                        LOOP
                            REMOVE STE.ID FROM ID.LIST SETTING POS
                        WHILE STE.ID:POS
                            IF STE.ID THEN
                                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                                STE.CUR = R.STE<AC.STE.CURRENCY>
                                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                                STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                                STE.VAL = R.STE<AC.STE.VALUE.DATE>
                                STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
*FINDSTR '\B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'\',1)
                                STE.FCY = R.STE<AC.STE.AMOUNT.FCY>
                                STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                                STE.BAL += STE.AMT 
                                IF STE.AMT GT 0 THEN
                                    IF STE.AMT GE CR.VAL THEN
                                        GOSUB RET.DATA
                                    END
                                END
                            END
                        REPEAT
                    END
                REPEAT
            END
        END
    REPEAT
    RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = COMP:"*":CR.VAL:"*":FROM.DATE:"*":END.DATE:"*":CUS.ID:"*":ACCT.ID:"*":STE.AMT:"*":STE.FCY:"*":STE.REF:"*":STE.DAT
*#    KK++
*#    XX<1,KK>[1, 20] = ACCT.ID
*#    XX<1,KK>[20,20] = FMT(STE.AMT,"L2,")
*#    XX<1,KK>[40,20] = FMT(STE.FCY,"L2,")
*#    XX<1,KK>[60,20] = STE.OUR
*#    PRINT XX<1,KK>
    RETURN
**************************************
END
