* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 04/10/2016******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.DB.EQV.USD.DAILY.PRIV(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0

    YTEXT = "���� ������� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    RQ.DATE = COMI

    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND CUSTOMER.1 NE '99433300' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE EQ ":RQ.DATE : " AND CONTRACT.GRP NE '' BY CUSTOMER.1 BY CURRENCY.1"
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    FOR I = 1 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        ACCT<I>         = R.TT<TT.TE.ACCOUNT.1>
        CO.CODE<I>      = R.TT<TT.TE.CO.CODE>
        NARRAT<I>       = R.TT<TT.TE.NARRATIVE.2>
        TT.LOCAL.REF    = R.TT<TT.TE.LOCAL.REF>
        SER.NO<I>       = TT.LOCAL.REF<1,TTLR.SER.NO>
        CHQ.NO<I>       = TT.LOCAL.REF<1,TTLR.CHEQUE.NO>
        TT.DATE = AUTH.DAT<I>
        KEY.ID<I> = "USD":"-" :"EGP":"-":TT.DATE
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        IF CURR<I> = "USD" THEN
            TOT.AMT.N = AMT.FCY<I>
        END ELSE
            LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
            END
            AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
            TOT.AMT.N = AMT.EQV.USD
        END         ;**END OF OTHER FCY**
        CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
        LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
*NSN = LOCAL.REF<1,CULR.NSN.NO>
*REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
        CUST.COMP = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
        Y.RET.DATA<-1> = CUST<I> :"*":ACCT<I>:"*": CURR<I>:"*":AMT.FCY<I>:"*":NARRAT<I>:"*":CO.CODE<I>:"*":KEY.LIST.N<I>:"*":SER.NO<I>:"*":CHQ.NO<I>:"*":AUTH.DAT<I>:"*":TOT.AMT.N
        TOT.AMT.N = 0
*****SAME CURRENCY*************
    NEXT I
    RETURN
END
