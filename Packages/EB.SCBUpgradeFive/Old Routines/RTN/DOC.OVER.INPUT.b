* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
*--------------------------------NI7OOOOOOOOOO---------------------------------------------

    SUBROUTINE DOC.OVER.INPUT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    ACC2    = R.NEW(DOC.PRO.DEBIT.ACCT)
    AMT22   = COMI
*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DC.AMT2 = DCOUNT(AMT22,@VM)
    ZZZ = '0000000'
***************************************************************************
    FOR I = 1 TO DC.AMT2
        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC2,BAL)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC2,CUS.ID)
        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC2,CUS.LIM)

        LIMM = FIELD(CUS.LIM,'.',1)
        AA = 7 - LEN(LIMM)
        XX = CUS.ID:'.':ZZZ[1,AA]:CUS.LIM

        T.SEL2 = "SELECT FBNK.LIMIT WITH @ID EQ ":XX
        CALL EB.READLIST(T.SEL2,KEY.LIST,"",SELECTED,ER.MSG)

        CALL F.READ(FN.LIM,KEY.LIST,R.LIM,F.LIM,E2)
        LIMIT.AMT = R.LIM<LI.AVAIL.AMT>

        AMT2      = AMT22

        TOTAL.DB  = BAL - AMT2

        TOTAL.CR  = TOTAL.DB + LIMIT.AMT
*** IF BAL LT AMT2 THEN
        IF TOTAL.CR LE 0 THEN
            ETEXT = "������ ����� ��� ������ ������ �� ���"
        END
        AMT2 = ''
    NEXT I
***************************************************************************

    RETURN
END
