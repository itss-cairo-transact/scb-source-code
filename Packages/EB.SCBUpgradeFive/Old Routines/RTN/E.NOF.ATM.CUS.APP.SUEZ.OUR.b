* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***NESSREEN AHMED 22/09/2014*********************************
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.ATM.CUS.APP.SUEZ.OUR(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CARD.TYPE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*-----------------------------------------------------------------------*
    FN.CUST = 'F.CUSTOMER' ; F.CUST = '' ; R.CUST = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.CUST,F.CUST)
    FN.ATM.APP = 'F.SCB.ATM.APP' ; F.ATM.APP = '' ; R.ATM.APP = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.ATM.APP,F.ATM.APP)

    T.SEL = "SELECT F.SCB.ATM.APP WITH ATM.APP.DATE GE '20140904' BY CO.CODE BY CUSTOMER BY DATE.TIME"

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SEL.ATM=':SELECTED ; CALL REM

    CALL F.READ(FN.ATM.APP, KEY.LIST<1>, R.ATM.APP, F.ATM.APP, E1)
    CUST.NO<1>    =  R.ATM.APP<SCB.VISA.CUSTOMER>
    FOR I = 2 TO SELECTED
        CALL F.READ(FN.ATM.APP, KEY.LIST<I>, R.ATM.APP, F.ATM.APP, E1)
        CUST.NO<I>     =  R.ATM.APP<SCB.VISA.CUSTOMER>
*APP.DAT<I> =  R.ATM.APP<SCB.VISA.ATM.APP.DATE>
        IF CUST.NO<I> NE CUST.NO<I-1> THEN
            CALL F.READ(FN.ATM.APP, KEY.LIST<I-1>, R.ATM.APP, F.ATM.APP, E2)
            APP.DAT<I-1> =  R.ATM.APP<SCB.VISA.ATM.APP.DATE>
            APP.COMP<I-1>=  R.ATM.APP<SCB.VISA.CO.CODE>
           CALL F.READ(FN.CUST, CUST.NO<I-1>, R.CUST, F.CUST, ER.C)
    **        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO<I-1>,LOC.REF)
            LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>
            CUST.BANK<I-1> = LOCAL.REF<1,CULR.CU.BANK>
            CUST.SCCD<I-1> = LOCAL.REF<1,CULR.SCCD.CUSTOMER>
            IF ((CUST.BANK<I-1> EQ '') OR (CUST.BANK<I-1> EQ '0017')) AND (CUST.SCCD<I-1> EQ 'YES' ) THEN
                Y.RET.DATA<-1> = CUST.NO<I-1> :"*":APP.DAT<I-1>:"*":APP.COMP<I-1> :"*":CUST.BANK<I-1>
                CUST = '' ; APP.DAT = '' ; APP.COMP = ''
            END
        END
        IF I = SELECTED  THEN
            CALL F.READ(FN.ATM.APP, KEY.LIST<I>, R.ATM.APP, F.ATM.APP, E2)
            APP.DAT<I> =  R.ATM.APP<SCB.VISA.ATM.APP.DATE>
            APP.COMP<I>=  R.ATM.APP<SCB.VISA.CO.CODE>
         **  CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO<I>,LOC.REF)
            CALL F.READ(FN.CUST, CUST.NO<I>, R.CUST, F.CUST, ER.C)
            LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>
            CUST.BANK<I> = LOCAL.REF<1,CULR.CU.BANK>
            CUST.SCCD<I> = LOCAL.REF<1,CULR.SCCD.CUSTOMER>
            IF ((CUST.BANK<I> EQ '') OR (CUST.BANK<I> EQ '0017')) AND (CUST.SCCD<I> EQ 'YES' ) THEN
                Y.RET.DATA<-1> = CUST.NO<I> :"*":APP.DAT<I>:"*":APP.COMP<I> :"*":CUST.BANK<I>
            END
        END
    NEXT I
*-----------------------------------------------------------------------*
    RETURN
END
