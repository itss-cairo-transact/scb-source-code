* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1490</Rating>
*-----------------------------------------------------------------------------
**************************MOHAMED MOSSTAFA-2013/09/11********************************************
    SUBROUTINE E.NOF.DBT.CRDT.TRN(Y.RET.DATA)
*    PROGRAM E.NOF.NOF.DBT.CRDT.TRN
*************************************************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.SYSTEM.ID
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

**------------------------------------
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 64 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
*-------------------------------------------------------------------------
    RETURN

*==============================================================
INITIATE1:

    RETURN
*==============================================================
INITIATE:
    SCB.AMOUNT = ''
    FROM.DATE = ''
    END.DATE = ''
    FROM.TRN.DATE = ''
    END.TRN.DATE = ''
    RETURN
*===============================================================
CALLDB:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF (FN.CU,F.CU)

    FN.CUS.ACC ='FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC  =''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.ACC='FBNK.ACCOUNT' ; F.ACC=''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC.HIS='FBNK.ACCOUNT$HIS' ; F.ACC.HIS='' ; R.ACC.HIS = ''
    CALL OPF(FN.ACC.HIS,F.ACC.HIS)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)

*    FN.SYSID = 'F.EB.SYSTEM.ID' ; F.SYSID = '' ; R.SYSID = ''
*    CALL OPF(FN.SYSID,F.SYSID)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*=========================================================================================================================
    LOCATE "FROM.TRN.DATE" IN D.FIELDS<1> SETTING YCRV.POS THEN FROM.TRN.DATE  = D.RANGE.AND.VALUE<YCRV.POS> ELSE RETURN
    LOCATE "TO.TRN.DATE"   IN D.FIELDS<1> SETTING YEDT.POS THEN END.TRN.DATE   = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
*=========================================================================================================================
    T.SEL  = "SELECT ":FN.CU:" WITH ( CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120 ) AND COMPANY.BOOK EQ ":COMP:" BY @ID"
    T.SEL := " BY COMPANY.BOOK"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    TEXT = SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CUSID = KEY.LIST<I>

            CALL F.READ(FN.CU,CUSID,R.CU,F.CU,ERRCU)
            SCB.CRD.NM = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            CALL F.READ(FN.CUS.ACC,CUSID,R.CUS.ACC,F.CUS.ACC,ERR1)
            LOOP
                REMOVE ACC.NO FROM R.CUS.ACC SETTING POS
            WHILE ACC.NO:POS
                CALL F.READ(FN.ACC,ACC.NO,R.ACC,F.ACC,ACC.ERR1)
                CATEG = R.ACC<AC.CATEGORY>
           *     IF CATEG = 9090 OR CATEG = 1001 OR ( CATEG GE 1220 AND CATEG LE 1227 ) THEN
                    SCB.CONT.DATE = R.CU<EB.CUS.CONTACT.DATE>
                    SCB.COMP.CRD = R.ACC<AC.CO.CODE>
                    CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.COMP.CRD,SCB.COMP.CRD.NM)
                    GOSUB GET.STMT
           *     END
*-------------------------------------------------------------------------------------------
            REPEAT
        NEXT I
    END
    RETURN
*--------------------------------------------------------------------------------------------
GET.STMT:
*--------
    CALL EB.ACCT.ENTRY.LIST(ACC.NO<1>,FROM.TRN.DATE,END.TRN.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS1
    WHILE STE.ID:POS1
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        IF NOT(ER.STE) THEN
*------------------------------------------------------------------------------------
            REC.ID         = '' ; SCB.CRD.AMOUNT = '' ; SCB.CRD.ACCT   = '' ; SCB.SYS.ID     = ''
            SCB.TRN.DATE   = '' ; SCB.INP.NO     = '' ; SCB.AUTH.NO    = '' ; SCB.INP.NO     = ''
            SCB.AUTH.NO    = '' ; SCB.INP.NM     = '' ; SCB.AUTH.NM    = ''
*------------------------------------------------------------------------------------
            REC.ID         = R.STE<AC.STE.OUR.REFERENCE>
            IF NOT(REC.ID) THEN
                REC.ID = R.STE<AC.STE.TRANS.REFERENCE >
            END
            SCB.CRD.AMOUNT = R.STE<AC.STE.AMOUNT.LCY>
            SCB.CRD.ACCT   = R.STE<AC.STE.ACCOUNT.NUMBER>
            SCB.SYS.ID     = R.STE<AC.STE.SYSTEM.ID>
            SCB.TRN.DATE   = R.STE<AC.STE.BOOKING.DATE>
            SCB.INP.NO     = R.STE<AC.STE.INPUTTER>
            SCB.AUTH.NO    = R.STE<AC.STE.AUTHORISER>
            SCB.INP.NO     = FIELD (SCB.INP.NO,'_',2)
            SCB.AUTH.NO    = FIELD (SCB.AUTH.NO,'_',2)
            SCB.DBT.CUR    = R.STE<AC.STE.CURRENCY>
            SCB.COMP.TRN   = R.STE<AC.STE.COMPANY.CODE>
            SCB.TRN.DESC   = R.STE<AC.STE.TRANSACTION.CODE>
            IF R.STE<AC.STE.CURRENCY> = LCCY THEN
                SCB.AMOUNT.FCY = R.STE<AC.STE.AMOUNT.LCY>
            END ELSE
                SCB.AMOUNT.FCY = R.STE<AC.STE.AMOUNT.FCY>
            END
            CALL DBR ('TRANSACTION':@FM:AC.TRA.NARRATIVE,SCB.TRN.DESC,SCB.TRN.DESC.N)
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.COMP.TRN,SCB.COMP.TRN.NM)

            CALL DBR('USER':@FM:EB.USE.USER.NAME,SCB.INP.NO,SCB.INP.NM)
            CALL DBR('USER':@FM:EB.USE.USER.NAME,SCB.AUTH.NO,SCB.AUTH.NM)
*====================================================================================================
*DEBUG
            IF SCB.SYS.ID = "MLT" THEN
                IF INDEX(REC.ID,"\",1) THEN  REC.ID = FIELD (REC.ID,'\',1)
                IF INDEX(REC.ID,"/",1) THEN  REC.ID = FIELD (REC.ID,'/',1)
                IF INDEX(REC.ID,"-",1) THEN  REC.ID = FIELD (REC.ID,'-',1)
            END
            IF SCB.SYS.ID = "IC4" THEN
                SCB.APPL.NM = "STMT.ACCT.DR"
            END
            SCB.APPL.NM = ''
            CALL DBR('EB.SYSTEM.ID':@FM:SID.APPLICATION,SCB.SYS.ID,SCB.APPL.NM)
            IF ETEXT THEN
                IF REC.ID[1,2] EQ 'BR' THEN
                    SCB.APPL.NM = 'BILL.REGISTER'
                END
                IF REC.ID[1,2] EQ 'IN' THEN
                    SCB.APPL.NM = 'INF.MULTI.TXN'
                END
                IF REC.ID[1,2] EQ 'BT' THEN
                    SCB.APPL.NM = 'SCB.BT.BATCH'
                END
                IF SCB.SYS.ID EQ 'CQ' THEN
                    IF REC.ID[1,3] EQ 'SCB' THEN
                        SCB.APPL.NM = 'CHEQUE.ISSUE'
                    END
                END
            END     ;*ELSE
            SCB.SEE.APPL = SCB.APPL.NM:" S ":REC.ID
*            END
            IF NOT(SCB.APPL.NM) THEN SCB.SEE.APPL = "STMT.ENTRY S ":STE.ID
            Y.RET.DATA <-1>  = ACC.NO:"*":SCB.CRD.NM :"*":SCB.COMP.CRD.NM :"*":SCB.DBT.CUR:"*":SCB.CONT.DATE:"*":SCB.AMOUNT.FCY:"*":SCB.TRN.DESC.N:"*":"*":SCB.TRN.DATE:"*":SCB.INP.NM:"*":SCB.AUTH.NM:"*":REC.ID:"*":CUSID:"*":SCB.SEE.APPL
        END
    REPEAT
    RETURN
*-----------------------------------------------------------------------------------------------------------------
