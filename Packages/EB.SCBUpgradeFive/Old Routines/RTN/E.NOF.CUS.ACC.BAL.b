* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*    PROGRAM E.NOF.CUS.ACC.BAL
    SUBROUTINE E.NOF.CUS.ACC.BAL(Y.RET.DATA)

***Mahmoud Elhawary******20/5/2010****************************
*   nofile routine to get customer accounts balances         *
**************************************************************

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 45 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    CUS.ID    = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    Y.OPEN.BAL= ''
    KK = 0
    XX = SPACE(70)
    STE.BAL = ''
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.AC.CL = 'FBNK.ACCOUNT.CLOSED' ; F.AC.CL = '' ; R.AC.CL = '' ; ER.AC.CL = ''
    CALL OPF(FN.AC.CL,F.AC.CL)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO"  IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "BALANCE.DATE" IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN

    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
    CUS.COM = R.CUS<EB.CUS.COMPANY.BOOK>

    MMM = LEN(CUS.ID)
    IF MMM LT 8 THEN
        CUS.LK = "0":CUS.ID:"..."
    END ELSE
        CUS.LK = CUS.ID:"..."
    END

    SS.SEL  = "SELECT ":FN.AC.CL:" WITH @ID LIKE ":CUS.LK
    SS.SEL := " AND ACCT.CLOSE.DATE GE ":FROM.DATE
    CALL EB.READLIST(SS.SEL,K.LIST.CL,'',SELECTED.CL,ER.CL)
    LOOP
        REMOVE ACCT.CL FROM K.LIST.CL SETTING POS.ACC.CL
    WHILE ACCT.CL:POS.ACC.CL
        AC.LEN = LEN(ACCT.CL)
        IF AC.LEN EQ 16 THEN
            CUR.CODE = ACCT.CL[9,2]
            CR.SEL = "SELECT ":FN.CUR:" WITH NUMERIC.CCY.CODE EQ ":CUR.CODE
            CALL EB.READLIST(CR.SEL,K.LIST.CR,'',SELECTED.CR,ER.CR)
            AC.CUR = K.LIST.CR<1>
            AC.CAT = ACCT.CL[11,4]
            IF AC.CAT NE 1002 THEN
                ACCT.ID = ACCT.CL
                CALL GET.ENQ.BALANCE(ACCT.ID,FROM.DATE,Y.OPEN.BAL)
                GOSUB RET.DATA
            END
        END
    REPEAT
    ACCT.ID = ''
    AC.CAT  = ''
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
    LOOP
        REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
    WHILE ACCT.ID:POS.CUS.ACC
        CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
        AC.CUR = R.ACCT<AC.CURRENCY>
        AC.CAT = R.ACCT<AC.CATEGORY>
        IF AC.CAT NE 1002 THEN
            CALL GET.ENQ.BALANCE(ACCT.ID,FROM.DATE,Y.OPEN.BAL)
            GOSUB RET.DATA
        END
    REPEAT
    RETURN
**************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = CUS.COM:"*":CUS.ID:"*":FROM.DATE:"*":ACCT.ID:"*":AC.CAT:"*":Y.OPEN.BAL:"*":AC.CUR
    RETURN
**************************************
END
