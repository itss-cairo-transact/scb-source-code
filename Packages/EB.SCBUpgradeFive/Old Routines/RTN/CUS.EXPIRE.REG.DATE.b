* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CUS.EXPIRE.REG.DATE(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    KEY.LIST = ""
    SELECTED = ""
    R.CU     = ""
    ER.MSG   = ""
    TODAY.DATE = TODAY
    CALL ADD.MONTHS( TODAY.DATE,+1)

    FN.CU = 'FBNK.CUSTOMER'; F.CU = ''
    CALL OPF(FN.CU,F.CU)
*IDESC LIC.EXP.D
* T.SEL  = "SELECT FBNK.CUSTOMER WITH NEW.SECTOR NE 4650 AND POSTING.RESTRICT LE 91 AND POSTING.RESTRICT NE 70 AND POSTING.RESTRICT NE 18 AND POSTING.RESTRICT NE 12 AND SCCD.CUSTOMER NE 'YES' AND CREDIT.CODE LE 110 AND "
    T.SEL  =  "SELECT FBNK.CUSTOMER WITH NEW.SECTOR NE 4650 AND POSTING.RESTRICT LE 89 AND POSTING.RESTRICT NE 70 AND POSTING.RESTRICT NE 18 AND POSTING.RESTRICT NE 12 AND SCCD.CUSTOMER NE 'YES' AND DRMNT.CODE NE 1 AND DRMNT.CODE NE 110 AND CREDIT.CODE NE 110 AND COM.REG.NO NE '' AND (LIC.EXP.DATE LE ":TODAY.DATE:") AND COMPANY.BOOK EQ ":ID.COMPANY:" BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SELECTED=   "  : SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO CODES FOUND"

    END
    RETURN
END
