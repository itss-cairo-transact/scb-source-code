* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-26</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.BR.TOT(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

**------------------------------------------------------------------**

    FN.ACC     = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.TRNS  = 'FBNK.ACCT.ENT.LWORK.DAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)
** ----------------------------------CR----------------------------------- **
    II     = 0
    T.SEL  = " SELECT FBNK.ACCOUNT WITH CUSTOMER EQ 99433300 AND CURRENCY EQ EGP AND ( CATEGORY EQ 5082 OR CATEGORY EQ 5083 ) BY CO.CODE "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    TRNS.NO = ''
    ENT.NO  = ''
    LOOP
        REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
    WHILE ACCT.ID:POS
        CALL F.READ( FN.TRNS,ACCT.ID, R.TRNS,F.TRNS, ETEXT)
        LOOP
            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)

            IF R.STMT<AC.STE.TRANS.REFERENCE>[1,2] EQ 'FT' THEN

                II  = II + 1

                ST.AMT.CR += R.STMT<AC.STE.AMOUNT.LCY>
            END


        REPEAT
    REPEAT

    Y.RET.DATA<-1>  =  ST.AMT.CR :"*":II :"*": "������� ������� ��� ��� ������" :"*":R.STMT<AC.STE.BOOKING.DATE>


** ----------------------------------DR----------------------------------- **
    III    = 0
    T.SEL  = " SELECT FBNK.ACCOUNT WITH CUSTOMER EQ 99433300 AND CURRENCY EQ EGP AND  CATEGORY EQ 5085 BY CO.CODE "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    TRNS.NO = ''
    ENT.NO  = ''
    LOOP
        REMOVE ACCT.ID  FROM KEY.LIST SETTING POS
    WHILE ACCT.ID:POS
        CALL F.READ( FN.TRNS,ACCT.ID, R.TRNS,F.TRNS, ETEXT)
        LOOP
            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)

            IF R.STMT<AC.STE.TRANS.REFERENCE>[1,2] EQ 'BR' THEN

                III  = III + 1

                ST.AMT.DR += R.STMT<AC.STE.AMOUNT.LCY>
            END


        REPEAT
    REPEAT
    TOT.AMT.DR      =  ST.AMT.DR * -1
    Y.RET.DATA<-1>  =  TOT.AMT.DR :"*":III:"*":"������� ������� ����� �������":"*":R.STMT<AC.STE.BOOKING.DATE>

    RETURN
*==============================================================
END
