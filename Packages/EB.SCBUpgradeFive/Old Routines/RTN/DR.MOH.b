* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE DR.MOH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='DR.MOH'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DR='FBNK.STMT.ACCT.DR';F.DR=''
    CALL OPF(FN.DR,F.DR)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    DB.MOV2 = 0
    CR.MOV2 = 0
    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')

    YTEXT = "Enter Account No. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    ID = COMI
TEXT = "ID :":ID ; CALL REM
*********************************************
    YTEXT = "Enter Date. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
     ID2 = COMI
  ***  ID = COMI:"-":DAT
 TEXT = "ID2 :":ID2 ; CALL REM
     ID3  = ID:"-":ID2
TEXT = "ID3 :":ID3  ; CALL REM
*------------------------------------------------------------------------
*T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...":DAT
*T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID EQ '0130148410150203-20081031'"
    T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID EQ ":ID3

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL : " : SELECTED ; CALL REM
    IF SELECTED THEN
*TEXT = 'SELECTED = ':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.DR,KEY.LIST<I>,R.DR,F.DR,E2)
            DR.ID = KEY.LIST<I>
            ACC.NO = FIELD(DR.ID,'-',1)
            INT.DATE = FIELD(DR.ID,'-',2)
            V.DATE  = INT.DATE[7,2]:'/':INT.DATE[5,2]:"/":INT.DATE[1,4]
            DATE2 = INT.DATE[1,6]
            ACTIVE.ID = ACC.NO:'-':DATE2
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

            DR.AMOUNT = R.DR<IC.STMDR.HIGHEST.DR.BAL>
            TOT.INT = R.DR<IC.STMDR.GRAND.TOTAL>
            TOT.AMT = R.DR<IC.STMDR.HIGHEST.DR.AMT>
            INT.AMT = R.DR<IC.STMDR.DR.INT.RATE>

            CALL DBR ('ACCT.ACTIVITY':@FM:IC.ACT.TURNOVER.DEBIT,ACTIVE.ID,DB.MOV)
*Line [ 113 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DB = DCOUNT(DB.MOV,@VM)
            FOR X = 1 TO DB
                DB.MOV2 += DB.MOV<1,X>
            NEXT X

            CALL DBR ('ACCT.ACTIVITY':@FM:IC.ACT.TURNOVER.CREDIT,ACTIVE.ID,CR.MOV)
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CR = DCOUNT(CR.MOV,@VM)
            FOR J = 1 TO CR
                CR.MOV2 += CR.MOV<1,J>
            NEXT J

*------------------------------------------------------------------------
            CALL DBR ('ACCT.ACTIVITY':@FM:IC.ACT.BALANCE,ACTIVE.ID,BAL)
*Line [ 128 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            BALD = DCOUNT(BAL,@VM)

            FOR K = 1 TO BALD
                BAL1 = BAL<1,1>
                BAL2 = BAL<1,BALD>
            NEXT K
*------------------------------------------------------------------------


            XX   = SPACE(132)  ; XX3  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,15]   = "��� ������   : ":' ':ACC.NO
            XX<1,1>[45,35]  = '����� : ':' ':CUST.NAME


            XX1<1,1>[3,15]  = '����������     : ':' ':CATEG
            XX5<1,1>[3,15] = '��������     : ':' ':CUR

            XX2<1,1>[3,15] = '����� ������� : ':' ':V.DATE


            XX3<1,1>[3,15] = '���� ���� ���� : ':' ':DR.AMOUNT
            XX3<1,1>[45,15] = '���� �������  : ':' ':TOT.INT

            XX4<1,1>[3,15] = '���� �������':' ':TOT.AMT
            XX4<1,1>[45,15] = '��� �������   : ':' ':INT.AMT

*XX5<1,1>[3,15]  = '����� ���� ����� : ':DB.MOV2
*XX5<1,1>[45,15] = '����� ���� �����':CR.MOV2

            XX6<1,1>[3,15]  = '���� ������� : ':BAL1
            XX6<1,1>[45,15]  = '���� ����� : ':BAL2
*-------------------------------------------------------------------
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN :SPACE(10):"������� �� ������ ����� ����� ������"
**** PR.HD :="'L'":SPACE(30)"������� �� ������ ����� ����� ������"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX5<1,1>
            PRINT XX2<1,1>
            PRINT STR(' ',82)
            PRINT STR('=',82)
            PRINT STR(' ',82)
            PRINT XX3<1,1>
            PRINT XX4<1,1>
*PRINT XX5<1,1>
            PRINT XX6<1,1>
            PRINT STR('-',82)
        NEXT I
    END
*===============================================================
    RETURN
END
