* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.TOT.PL.SF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
**----------------------------------------
    CCY         = 'EGP'
    AMT.FCY     = 0
    CUR.CAT     = 0
    TOT.FCY.AMT = 0
    CURR.O.DATA = CCY

    YF.CATEG.MONTH = "F.CATEG.MONTH"
    F.CATEG.MONTH = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)
*
    YF.CATEG.ENTRY = "F.CATEG.ENTRY"
    F.CATEG.ENTRY = ""
    CALL OPF(YF.CATEG.ENTRY,F.CATEG.ENTRY)
*
    YF.CATEGORY = "F.CATEGORY"
    F.CATEGORY = ""
    CALL OPF(YF.CATEGORY,F.CATEGORY)
*
    F.CATEG.ENT.TODAY = ""
    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)
*
    F.CATEG.ENT.FWD = ''
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD'
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    FN.COMP = 'F.COMPANY'     ;  F.COMP = '' ; R.COMP  = '' ; ETEXT = ''
    CALL OPF( FN.COMP,F.COMP)
**--------------------------------------------------------------------**
    GOSUB GET.TOT.MON
    O.DATA   =  TOT.FCY.AMT.MON
    RETURN
**----------------------------------------------
GET.TOT.MON:
*-----------
    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-1')
    TO.DATE     = TODAY
    ID.COMPANY  = O.DATA
    CHARGE.CODE = "CHQCOOLLN"
    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,CATEG.PL)
    Y.CAT.NO    = CATEG.PL

    CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)
    LOOP
        REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
    WHILE YENTRY.KEY:YTYPE

        CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)
        CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
        CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
        V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
        B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>

        FCY.AMT         = YR.CATEG.ENTRY<AC.CAT.AMOUNT.LCY>
        TOT.FCY.AMT.MON = TOT.FCY.AMT.MON + FCY.AMT
    REPEAT
    RETURN
**---------------------------------------------------------------
END
