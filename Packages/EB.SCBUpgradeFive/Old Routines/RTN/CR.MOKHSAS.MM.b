* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    PROGRAM CR.MOKHSAS.MM

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.LD.LOANS.AND.DEPOSITS
    $INSERT T24.BP I_F.LETTER.OF.CREDIT
    $INSERT T24.BP I_F.MM.MONEY.MARKET
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.DRAWINGS
    $INSERT           I_F.SCB.FUND
    $INSERT           I_FT.LOCAL.REFS
    $INSERT           I_F.SCB.CUS.POS.TST
    $INSERT           I_CU.LOCAL.REFS
*--------------------------------------------------------*
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

    CALL OPF( FN.ACCOUNT,F.ACCOUNT)
*--------------------------------------------------------*
    GOSUB MM.SUB
    RETURN
*-----------------------------*
MM.SUB:
*------
    ID.NO = ""
    FN.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET' ; F.MM.MONEY.MARKET = '' ; R.MM.MONEY.MARKET = ''

    CALL OPF( FN.MM.MONEY.MARKET,F.MM.MONEY.MARKET)
    T.SEL3 = "SELECT FBNK.MM.MONEY.MARKET WITH PRINCIPAL GT 0 "

    KEY.LIST3=""
    SELECTED3=""
    ER.MSG3=""
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    PRINT SELECTED3
    IF KEY.LIST3 THEN
        FOR I = 1 TO SELECTED3
            CALL F.READ(FN.MM.MONEY.MARKET,KEY.LIST3<I>, R.MM.MONEY.MARKET,F.MM.MONEY.MARKET, ETEXT)
            ID.NO = R.MM.MONEY.MARKET<MM.CUSTOMER.ID>:"*LD*BNK*":R.MM.MONEY.MARKET<MM.CURRENCY>:R.MM.MONEY.MARKET<MM.CATEGORY>:"*":KEY.LIST3<I>:"*":TODAY

            CURR= R.MM.MONEY.MARKET<MM.CURRENCY>
            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100

            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            AMT = R.MM.MONEY.MARKET<MM.PRINCIPAL>

            LCY.AMT = (MID.RATE<1,1> * AMT)
            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>= LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER> = R.MM.MONEY.MARKET<MM.CUSTOMER.ID>
            R.COUNT<CUPOS.DEAL.CCY> = R.MM.MONEY.MARKET<MM.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT> = R.MM.MONEY.MARKET<MM.PRINCIPAL>
            R.COUNT<CUPOS.MATURITY.DATE> = R.MM.MONEY.MARKET<MM.MATURITY.DATE>
            R.COUNT<CUPOS.VALUE.DATE> = R.MM.MONEY.MARKET<MM.VALUE.DATE>
            R.COUNT<CUPOS.CATEGORY> = R.MM.MONEY.MARKET<MM.CATEGORY>
            R.COUNT<CUPOS.SYS.DATE> = TODAY
            R.COUNT<CUPOS.CO.CODE> = R.MM.MONEY.MARKET<MM.CO.CODE>

            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
    RETURN
END
