* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>587</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CPY.HOLD.RPT
*    PROGRAM CPY.HOLD.RPT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

*OPEN '','F.HOLD.CONTROL' TO HCFL ELSE STOP "CAN'T OPEN HOLDCTRL"

    PRINT " COPY DAILY REPORTS "
    PRINTER OFF
    DT   = ""
    NID  = ""
    CNT  = 0
    CNTN = ""
    NID1 = ""
    HREC = ""

*PRINT "ENTRY THE DATE (YYYYMMDD) :"
*INPUT DT
    IDD  = "EG0010001"
    DREC = ""
*OPEN '','F.DATES' TO DATFILE ELSE STOP "CAN'T OPEN DATES FILE"
    CALL OPF('F.DATES',DATFILE)
    CALL F.READ('F.DATES',IDD,DREC,DATFILE,DD.ERR)
***    READ DREC FROM DATFILE,IDD ELSE STOP
    DT = DREC<EB.DAT.LAST.WORKING.DAY>
    DT56 = ' AND RUN.IN.BATCH EQ Y AND REPORT.NAME NE USER.ACTIVITY AND REPORT.NAME NE APPLICATION.ACTIVITY AND REPORT.NAME UNLIKE ...STMT....'
    SEL1 = "SELECT F.HOLD.CONTROL WITH BANK.DATE EQ " : DT : DT56
    EXECUTE SEL1
* OPEN '','F.HOLD.CONTROL' TO HCFL ELSE STOP "CAN'T OPEN HOLDCTRL"
    CALL OPF('F.HOLD.CONTROL',HCFL)
    FLG = 0
    CALL !HUSHIT(1)
    LOOP UNTIL FLG = 1
        READNEXT ID ELSE FLG = 1
        IF FLG = 1 THEN
            GOTO END1
        END
        HREC = ""
        CALL F.READ('F.HOLD.CONTROL',ID,HREC,HCFL,HH.ERR)
***        READ HREC FROM HCFL,ID ELSE STOP
        COMP = HREC<3>
        CNT = CNT + 1
*NID1 = HREC<2> : "." : HREC<8> : "." : CNT
        NID1 = HREC<8> : "." : HREC<2> : "." : CNT
        NID = CHANGE(NID1,".","-",.5)
        CPY1 = "COPY FROM &HOLD& TO RPT.HOLD " : ID : "," : NID:".":COMP
*PRINT "REPORT NAME:  " : NID
        EXECUTE CPY1
END1:
    REPEAT
    CALL !HUSHIT(0)
    COMAND = "sh RPT.HOLD/OBJ/srep.sh"
    EXECUTE COMAND

END
