* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
*-----------------------------------------------------------------------------
* <Rating>-9</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.ENQ.CUS.POS.DATA(ENQUIRY.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.LIABILITY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.AC.EXP
*****UPDATED BY NESSREEN AHMED 8/10/2019****
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.RESTRICTED
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*****END OF UPDATED 8/10/2019****************

    ERR.CUS = ''

    LOCATE 'CUSTOMER.ID' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        CUS.ID = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END

    FN.CUS.DATA = 'F.SCB.CUS.AC.EXP' ;F.CUS.DATA= '' ; R.CUS.DATA = ''
    CALL OPF(FN.CUS.DATA,F.CUS.DATA)

    IF CUS.ID EQ '40300900' THEN

        CALL F.READ(FN.CUS.DATA,OPERATOR,R.CUS.DATA,F.CUS.DATA,READ.ERR.CUS.DATA)

        IF  READ.ERR.CUS.DATA THEN
            TEXT = "SECRET.INFORMATION " ; CALL REM
            ENQUIRY.DATA<2,1> = 'CUSTOMER.ID'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET.INFORMATION'
        END
*****UPDATED BY NESSREEN AHMED 8/10/2019****
*****END
    END ELSE
        FN.CUS.REST = 'F.SCB.CUS.RESTRICTED' ;F.CUS.REST = '' ; R.CUS.REST = ''
        CALL OPF(FN.CUS.REST,F.CUS.REST)

        CALL F.READ(FN.CUS.REST,CUS.ID,R.CUS.REST,F.CUS.REST,ERR.CUS)
        IF  NOT(ERR.CUS) THEN
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,CUS.COMP)
            IF CUS.COMP # ID.COMPANY THEN
                TEXT = "SECRET.INFORMATION " ; CALL REM
                ENQUIRY.DATA<2,1> = 'CUSTOMER.ID'
                ENQUIRY.DATA<3,1> = 'EQ'
                ENQUIRY.DATA<4,1> = 'SECRET.INFORMATION'
            END
        END

    END
****END OF UPDATE 8/10/2019******************
    RETURN
END
