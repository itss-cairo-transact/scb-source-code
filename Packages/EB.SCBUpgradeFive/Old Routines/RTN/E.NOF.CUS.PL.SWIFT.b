* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE E.NOF.CUS.PL.SWIFT(Y.RET.DATA)
****Mahmoud Elhawary******15/6/2011***************************
*   nofile routine to get customer SWIFT charges             *
**************************************************************

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
    GOSUB INITIATE
*Line [ 44 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    CUS.ID    = ''
    CTE.CAT   = ''
    CTE.CUR   = ''
    CTE.BAL   = 0
    MNTH.NUM  = ''
    NO.OF.MONTHS = ''
    TOTAL.INT = 0
    TOTAL.COM = 0
    TOT.INT.ALL = 0
    TOT.COM.ALL = 0
    XX = ''
    KK = 0
    CATEG.GRP = '52030.52031.52159.52630.52151.52152.'
    CATEG.NUM = COUNT(CATEG.GRP,'.')
    CRT CATEG.NUM
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.CAT = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = '' ; ER.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CTE = 'FBNK.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO" IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    GOSUB CHECK.CUSTOMER

    CUR.SEL = "SELECT ":FN.CUR
    CALL EB.READLIST(CUR.SEL,K.CUR,'',SELECTED.CUR,ERRR1)
    FOR MCAT = 1 TO CATEG.NUM
        CTE.BAL = 0
        CAT.ID = FIELD(CATEG.GRP,'.',MCAT)
        FOR AX1 = 1 TO SELECTED.CUR
            CUR.ID = K.CUR<AX1>
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CAT.ID,CAT.NAME)
            CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,END.DATE,CAT.ID,CATEG.LIST)
            LOOP
                REMOVE CTE.ID FROM CATEG.LIST SETTING POS.CTE.ID
            WHILE CTE.ID:POS.CTE.ID
                CALL F.READ(FN.CTE,CTE.ID,R.CTE,F.CTE,ER.CTE)
                CTE.CUR = R.CTE<AC.CAT.CURRENCY>
                CTE.AMT = R.CTE<AC.CAT.AMOUNT.LCY>
                CTE.DAT = R.CTE<AC.CAT.BOOKING.DATE>
                CTE.CUS = R.CTE<AC.CAT.CUSTOMER.ID>
                IF CTE.CUR NE 'EGP' THEN
                    CTE.AMT = R.CTE<AC.CAT.AMOUNT.FCY>
                END
                IF CTE.CUS EQ CUS.ID THEN
                    IF CTE.CUR EQ CUR.ID THEN
                        CTE.BAL += CTE.AMT
                    END
                END
            REPEAT
            IF CTE.BAL NE 0 THEN
                GOSUB RET.DATA
            END
        NEXT AX1
    NEXT MCAT
    RETURN
*******************************************************
CHECK.CUSTOMER:
*--------------
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
    CUS.COM = R.CUS<EB.CUS.COMPANY.BOOK>
    CUS.LCL = R.CUS<EB.CUS.LOCAL.REF>
    CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
    CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
    IF CUS.COM NE COMP THEN
        RETURN
    END
    IF CRD.STA NE '' THEN
        RETURN
    END
    IF CRD.COD EQ 110 OR CRD.COD EQ 120 THEN
        RETURN
    END
    RETURN
************************************************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = CUS.COM:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":CAT.ID:"*":CUR.ID:"*":CTE.BAL
    CTE.BAL = 0
    RETURN
**************************************
END
