* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
    SUBROUTINE E.BUILD.CUS.POSITION1(ENQUIRY.DATA)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*****UPDATED BY NESSREEN AHMED 8/10/2019****
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.RESTRICTED

    FN.CUS.REST = 'F.SCB.CUS.RESTRICTED' ;F.CUS.REST = '' ; R.CUS.REST = ''
    CALL OPF(FN.CUS.REST,F.CUS.REST)

*****END OF UPDATED 8/10/2019****************
    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
** 19/12/97 - GB9701470
**            Build the data dependent on the rebuild flag being set
*
*  08/06/05 - EN_10002549
*             Set C$CUS.POS.UPDATE.XREF to 0, so that cache is used
*             instead on CUSTOMER.POSITION.XREF.

    LOCATE 'REBUILD.DATA' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        REBUILD = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END
    IF REBUILD[1,1] NE 'N' THEN         ;* Do not execute on level dwon
        LOCATE "CUSTOMER.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
            CUST.ID = ENQUIRY.DATA<4,CUS.POS>
*Line [ 66 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CONVERT " " TO @VM IN CUST.ID
*Line [ 68 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF DCOUNT(CUST.ID,@VM) GT 1 OR CUST.ID = "ALL" THEN
                ENQ.ERROR = "ONLY ONE CUSTOMER ALLOWED"
            END ELSE
*********************************************
                FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
                CALL OPF(FN.CUS,F.CUS)
                KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""

                CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,READ.ERR1)

                LOOP
                    REMOVE AC.ID FROM R.CUS SETTING POS.CUST
                WHILE AC.ID:POS.CUST

                    CALL OPF( FN.ACCOUNT,F.ACC)
                    CALL F.READ(FN.ACCOUNT,AC.ID,R.ACC,F.ACC, ETEXT)

*****////  UPDATE FOR EXCEPTION APPROVAL LETTER NO.( 1661 SEND/ 171 RECIEVED ) DATED 1/3/2016
****////   UPDATE BY MAHMOUD 30/8/2016

                    EXP.CUST = " 13200946 7300650 7300651 7300652 7300655 32300005 "
                    EXP.USER = " SCB.38547 SCB.7935 SCB.67202 SCB.55255 SCB.13692 SCB.99201 SCB.2950 SCB.19208 SCB.10294 SCB.6921 SCB.11142 SCB.20249 SCB.77691 SCB.24384 SCB.11665 "
                    USO.USER = R.USER<EB.USE.SIGN.ON.NAME>
                    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,USO.USER,USR.ID)
                    FINDSTR USR.ID:" " IN EXP.USER SETTING POS.USR THEN USR.FLAG = 1 ELSE USR.FLAG = ''
                    FINDSTR CUST.ID:" " IN EXP.CUST SETTING POS.CUST THEN CUST.FLAG = 1 ELSE CUST.FLAG = ''

                    IF ( AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 ) OR ( AC.ID[11,4] EQ 1710 OR AC.ID[11,4] EQ 1711 ) THEN
                        IF USR.FLAG EQ '' OR CUST.FLAG EQ '' THEN     ;*** ADDED BY MAHMOUD 30/8/2016
                            CUST.ID = "DUMMY"
                        END   ;*** ADDED BY MAHMOUD 30/8/2016
                    END

                    SUSP = R.ACC<AC.INT.NO.BOOKING>
                    IF  SUSP EQ "SUSPENSE"  THEN
                        IF USR.FLAG EQ '' OR CUST.FLAG EQ '' THEN     ;*** ADDED BY MAHMOUD 30/8/2016
                            CUST.ID = "DUMMY"
                        END   ;*** ADDED BY MAHMOUD 30/8/2016
                    END
***********ABEER  AS OF 6/5/2018
                    FN.CU = 'FBNK.CUSTOMER' ;F.CU = '' ; R.CU = ''
                    CALL OPF(FN.CU,F.CU)
                    CALL F.READ(FN.CU,CUST.ID,R.CU,F.CU,ER.CU)
                    CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
                    CRD.COD = CU.LCL<1,CULR.CREDIT.CODE>
                    AC.CATEG = R.ACC<AC.CATEGORY>
                    IF AC.CATEG EQ '9014' OR AC.CATEG EQ '9015' THEN
*                        IF CRD.COD EQ '111' THEN
                        CUST.ID = "DUMMY"
                    END
*                   END
****************** END OF MODIFICATION
*****//// END OF UPDATE - MAHMOUD 30/8/2016
**TEXT = R.ACC<AC.INT.NO.BOOKING> ; CALL REM

                REPEAT

**TEXT = CUST.ID ; CALL REM

********************************************************
                C$CUST.POS.UPDATE.XREF = 0        ;* EN_10002549
                CALL CUS.BUILD.POSITION.DATA(CUST.ID)
                C$CUST.POS.UPDATE.XREF = 1        ;* EN_10002549 Reset to 1
            END
        END
    END
*******UPDATED BY NESSREEN AHMED ON 30/12/2008**********************
    FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...*1002)"

    CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)
***Updated by Nessreen Ahmed 26/9/2016************
        DELETE F.CUS.POS , KEY.LIST<I>
***        CALL F.DELETE (FN.CUS.POS , KEY.LIST<I>)
***End of Update 26/9/2016************************
    NEXT I

*************************************************************
*****UPDATED BY NESSREEN AHMED 20/10/2019**************************
    CALL F.READ(FN.CUS.REST,CUST.ID,R.CUS.REST,F.CUS.REST,ERR.CUS)
    IF  NOT(ERR.CUS) THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.ID,CUS.COMP)
        IF CUS.COMP # ID.COMPANY THEN
            TEXT = "SECRET.INFORMATION " ; CALL REM
            ENQUIRY.DATA<2,1> = 'CUSTOMER.ID'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET.INFORMATION'
        END
    END
*****END OF UPDATE 20/10/2019**************************************

*
    RETURN
*
END
