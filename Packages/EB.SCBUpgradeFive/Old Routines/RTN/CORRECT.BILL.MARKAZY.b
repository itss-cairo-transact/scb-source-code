* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------
* CREATE BY NESSMA ON 2013/04/14
*-----------------------------------------
*    PROGRAM CORRECT.BILL.MARKAZY
    SUBROUTINE CORRECT.BILL.MARKAZY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*-----------------------------------------------
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BRRR = '' ; ETEXT = ''
    CALL OPF( FN.BR,F.BR)

    YTEXT = "Enter the MATURITY.DATE : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    MAT.DAT = COMI

    T.SEL   = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ ":MAT.DAT
    T.SEL  := " AND CURRENCY EQ EGP "
    T.SEL  := " AND ( BILL.CHQ.STA NE 7 OR BILL.CHQ.STA NE 8"
    T.SEL  := " OR BILL.CHQ.STA NE 13 OR BILL.CHQ.STA NE 14 )"
    T.SEL  := " AND PAY.PLACE NE '' "

    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ER.MSG)

    FOR I =1 TO SELECTED

        CALL F.READ(FN.BR,KEY.LIST<I>,R.BRRR,F.BR,ETEXT)

        MAT.EX = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
        CALL AWD("EG00",MAT.EX,HOL)

        IF HOL EQ 'H' THEN
            HH     = "+1W"
            CALL CDT('',MAT.EX,HH)
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT> = MAT.EX

            PLACE  = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
            HH     = "+":PLACE:"W"
            CALL CDT('',MAT.EX,HH)
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE> = MAT.EX
        END ELSE
            COLL.DAT = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>
            CALL AWD("EG00",COLL.DAT,HOL)
*     IF HOL EQ 'H' THEN
            PLACE  = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
            HH     = "+":PLACE:"W"
            CALL CDT('',MAT.EX,HH)
            R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE> = MAT.EX
*    END
        END

        CALL F.WRITE(FN.BR ,KEY.LIST<I>, R.BRRR)
        CALL JOURNAL.UPDATE(KEY.LIST<I>)
    NEXT I

    TEXT = "PROGRAM END" ; CALL REM
    RETURN
END
