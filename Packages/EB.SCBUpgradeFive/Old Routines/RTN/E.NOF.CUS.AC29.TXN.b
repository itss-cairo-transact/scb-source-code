* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>700</Rating>
*-----------------------------------------------------------------------------
*****************MAHMOUD 23/6/2014 ***********************************
* TO GET AC29 TRANSACTIONS ON DEBIT BALANCE THROUGH THE LAST QUARTER *
**********************************************************************
    SUBROUTINE E.NOF.CUS.AC29.TXN(Y.RET.DATA)

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
**    COMP = 'EG0010001'

*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 46 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*==============================================================
INITIATE:
*--------
*???????????????????????????
    TD1  = TODAY
*#### For Test Only #### CALL CDT('',TD1,'+1W')
*???????????????????????????
    IF TD1[5,2] = '01' OR TD1[5,2] = '02' OR TD1[5,2] = '03' THEN
        TDLY = TD1[1,4] - 1
        FRDT = TDLY:'1001'
        TODT = TDLY:'1231'
        CALL LAST.WDAY(TODT)
    END
    IF TD1[5,2] = '04' OR TD1[5,2] = '05' OR TD1[5,2] = '06' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0101'
        TODT = TDCY:'0331'
        CALL LAST.WDAY(TODT)
    END
    IF TD1[5,2] = '07' OR TD1[5,2] = '08' OR TD1[5,2] = '09' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0401'
        TODT = TDCY:'0630'
        CALL LAST.WDAY(TODT)
    END
    IF TD1[5,2] = '10' OR TD1[5,2] = '11' OR TD1[5,2] = '12' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0701'
        TODT = TDCY:'0930'
        CALL LAST.WDAY(TODT)
    END
    CRT FRDT:" - ":TODT
    CUST.NAME    = ''
    Y.OPEN.BAL   = 0
    Y.MIN.BAL    = ''
    XX           = SPACE(130)
    K = 0
    RETURN
*===============================================================
CALLDB:
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ACC.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACC.ACT = ''
    CALL OPF(FN.ACC.ACT,F.ACC.ACT)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ER.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FTH = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FTH = '' ; R.FTH = '' ; ER.FTH = ''
    CALL OPF(FN.FTH,F.FTH)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    RETURN
*************************************************************************
PROCESS:
*-------
    S.SEL = "SSELECT FBNK.CUSTOMER.ACCOUNT WITH COMPANY.CO EQ ":COMP
    CALL EB.READLIST(S.SEL,KEY.LIST,'',SELECTED,ER.MSG)
    LOOP
        REMOVE CUS.ID FROM KEY.LIST SETTING POS.CUS
    WHILE CUS.ID:POS.CUS
        CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,CUS.ACC.ERR)
********************************************************************
        LOOP
            REMOVE Y.ACC.ID FROM R.CUS.ACC SETTING POSS1
        WHILE Y.ACC.ID:POSS1
*************************************
            CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,TODT,TODT,ID.LIST,OPENING.BAL,ER)
*************************************
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS.STE
            WHILE STE.ID:POS.STE
                IF STE.ID THEN
                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                    STE.CUR = R.STE<AC.STE.CURRENCY>
                    IF STE.CUR NE LCCY THEN
                        STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                    END ELSE
                        STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                    END
                    STE.LCY.AMT = R.STE<AC.STE.AMOUNT.LCY>
                    STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                    STE.VAL = R.STE<AC.STE.VALUE.DATE>
                    STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                    STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
*Line [ 137 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR '\B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'\',1) ELSE NULL
                    STE.FCY = R.STE<AC.STE.AMOUNT.FCY>
                    STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                    IF STE.REF[1,2] EQ 'FT' THEN
                        R.FT  = ''
                        CALL F.READ(FN.FT,STE.REF,R.FT,F.FT,ER.FT)
                        FT.TYPE = R.FT<FT.TRANSACTION.TYPE>
                        REC.STA = R.FT<FT.RECORD.STATUS>
                        DR.CUS  = R.FT<FT.DEBIT.CUSTOMER>
                        CR.CUS  = R.FT<FT.CREDIT.CUSTOMER>
                        IF NOT(R.FT) THEN
                            R.FTH = ''
*    STE.REF = STE.REF : ";1"
                          *  TEXT = STE.REF;CALL REM

                            CALL F.READ.HISTORY(FN.FTH,STE.REF,R.FTH,F.FTH,ER.FTH)
                            IF STE.REF[13,2] EQ ';1' THEN
                                FT.TYPE = R.FTH<FT.TRANSACTION.TYPE>
                                REC.STA = R.FTH<FT.RECORD.STATUS>
                                DR.CUS  = R.FTH<FT.DEBIT.CUSTOMER>
                                CR.CUS  = R.FTH<FT.CREDIT.CUSTOMER>
                            END
                        END
                        IF FT.TYPE EQ 'AC29' THEN
                            GOSUB GET.MAX.DR
                            GOSUB RET.DATA
                        END
                    END
******************************************************************
                END
            REPEAT
        REPEAT
    REPEAT
    RETURN
************************************
GET.MAX.DR:
*--------
    MIN.DR.BAL = 0
    MIN.BAL.AC = 0
    Y.MIN.BAL  = ''
    KK1 = 0
    CALL GET.ENQ.BALANCE(Y.ACC.ID,FRDT,Y.OPEN.BAL)
    KK1++
    Y.MIN.BAL<KK1> = Y.OPEN.BAL
    ACT.DATE = FRDT
    LOOP
    WHILE ACT.DATE LE TODT
        ACT.MNTH = ACT.DATE[1,6]
        ACC.ACT.ID = Y.ACC.ID:"-":ACT.MNTH
        CALL F.READ(FN.ACC.ACT,ACC.ACT.ID,R.ACC.ACT,F.ACC.ACT,Y.ERR1)
        IF NOT(Y.ERR1) THEN
            MIN.BAL.AC = MINIMUM(R.ACC.ACT<IC.ACT.BALANCE>)
            IF MIN.BAL.AC NE '' AND MIN.BAL.AC LT 0 THEN
                KK1++
                Y.MIN.BAL<KK1> = MIN.BAL.AC
            END
        END
        CALL ADD.MONTHS(ACT.DATE,'1')
    REPEAT
    MIN.DR.BAL = MINIMUM(Y.MIN.BAL)

    RETURN
************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = COMP:"*":FRDT:"*":TODT:"*":CUS.ID:"*":Y.ACC.ID:"*":STE.REF:"*":STE.AMT:"*":MIN.DR.BAL:"*":STE.LCY.AMT
    RETURN
*==============================================================
END
