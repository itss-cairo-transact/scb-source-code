* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1203</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CUS.POS.ACC05(Y.CUS.DATA)
**************************20/4/2011********************************************************
*   CREATED from E.NOF.CUS.POS.TEST by Mahmoud Elhawry And Mohamed Sabry                  *
*   NOFILE ENQUIRY for SCB customer position of LD depending on drawdown account input    *
*******************************************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
    $INCLUDE T24.BP I_F.LETTER.OF.CREDIT          ;*TF.LC.
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
    $INCLUDE T24.BP I_F.NUMERIC.CURRENCY          ;*EB.NCN.CURRENCY.CODE
    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM         ;*RE.BCP.
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 75 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB CHK.CG
    GOSUB SEND.DATA
    RETURN
****************************************
INITIATE:
*--------
    AC.ID         = ''
    CU.ID         = ''
    CO.ID         = ''
    WS.GL.ID      = ''
    WS.CY.ID      = ''
    WS.WRK.AMT    = 0
    WS.ACT.AMT    = 0
    XX            = ''
    WS.GL.IND     = ''
    WS.CY.IND     = ''
    POS.ARR       = 0
    ARR.ACT.AMT   = 0
    ARR.WRK.AMT   = 0
    ARR.BLK.AMT   = 0
    WS.LCY.AMT    = 0
    WS.DR.LCY.AMT = 0
    WS.CR.LCY.AMT = 0
    KK1           = 0
    USR.DEPT = R.USER<EB.USE.LOCAL.REF,USER.SCB.DEPT.CODE>
    RETURN
****************************************
CALLDB:
*--------

*Master files

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ER.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD = '' ; ER.PD = ''
    CALL OPF(FN.PD,F.PD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = '' ; R.MM = '' ; ER.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.LC = "FBNK.LETTER.OF.CREDIT"      ; F.LC = "" ; R.LC = '' ; ER.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = "FBNK.DRAWINGS"      ; F.DR = "" ; R.DR = '' ; ER.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.COL   = "FBNK.COLLATERAL"         ; F.COL  = '' ;  R.COL = '' ; ER.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    FN.LIM = "FBNK.LIMIT"                ; F.LIM = "" ; R.LIM = ''  ; ER.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.N.CUR = 'F.NUMERIC.CURRENCY' ; F.N.CUR = '' ; R.N.CUR = '' ; ER.N.CUR = '' ; ER.N.CUR = ''
    CALL OPF(FN.N.CUR,F.N.CUR)

    FN.CO = 'F.COMPANY' ; F.CO = '' ; R.CO = '' ; ER.CO = ''
    CALL OPF(FN.CO,F.CO)

    FN.CG = "F.SCB.CUSTOMER.GROUP"  ; F.CG  = "" ; R.CG = ""
    CALL OPF (FN.CG,F.CG)

* Index files

* Index file for account
    FN.IND.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC = '' ; R.IND.AC = '' ; ER.IND.AC = ''
    CALL OPF(FN.IND.AC,F.IND.AC)

*Index file for LD's , LG's and MM's
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

*Collateral (Received)  Index
    FN.IND.R.COL = "FBNK.COLLATERAL.RIGHT.CUST"   ; F.IND.R.COL = ""
    CALL OPF(FN.IND.R.COL,F.IND.R.COL)

*Collateral (Given)  Index
    FN.IND.G.COL = "FBNK.CUSTOMER.COLLATERAL"   ; F.IND.G.COL = ""
    CALL OPF(FN.IND.G.COL,F.IND.G.COL)

*Collateral Index by useing Collateral (Received)  Index or Collateral (Given)  Index
    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)

*LETTER.OF.CREDIT Index !!! (import)
    FN.IND.LCA = "FBNK.LC.APPLICANT"         ; F.IND.LCA = ""
    CALL OPF(FN.IND.LCA,F.IND.LCA)

* FOR TEST LC.ISSUING.BANK
*    FN.IND.LCA = "FBNK.LC.ISSUING.BANK"         ; F.IND.LCA = ""

**LETTER.OF.CREDIT Index  !!! (exp)
    FN.IND.LCB = "FBNK.LC.BENEFICIARY"   ; F.IND.LCB = ""
    CALL OPF(FN.IND.LCB,F.IND.LCB)

*Index file for limit
    FN.IND.LI = "FBNK.LIMIT.LIABILITY"   ; F.IND.LI = ""
    CALL OPF(FN.IND.LI,F.IND.LI)

    RETURN
****************************************
CHK.CG:
*-------
    LOCATE "CUSTOMER.ID"    IN D.FIELDS<1> SETTING YCUS.POS THEN ID.NO = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "ACCOUNT.NUMBER" IN D.FIELDS<1> SETTING YACC.POS THEN AC.NO = D.RANGE.AND.VALUE<YACC.POS> ELSE RETURN

*    CALL F.READ(FN.CG,ID.NO,R.CG,F.CG,ER.CG)
*    IF NOT(ER.CG)  THEN
*        WS.CUS.COUNT = DCOUNT(R.CG<CG.CUSTOMER.ID>,@VM)
*        WS.GROUP.STAT = R.CG<CG.GROUP.STATUS>
*        IF  WS.GROUP.STAT GE 110 THEN
*            IF USR.DEPT NE 9905 THEN
*                RETURN
*            END
*        END
*        FOR ICG = 1 TO WS.CUS.COUNT
*            CU.ID =  R.CG<CG.CUSTOMER.ID,ICG>
*            GOSUB PROCESS
*            CU.COM  = R.CG<CG.COMPANY.BOOK>
*            CU.ID   = ID.NO
*            AC.ID = AC.NO
*        NEXT ICG
*    END ELSE
    CU.ID = ID.NO
    AC.ID = AC.NO
    GOSUB PROCESS
*    END

    RETURN
****************************************
PROCESS:
*-------

    CALL F.READ(FN.CU,CU.ID,R.CU,F.CU,ER.CU)
    CU.COM  = R.CU<EB.CUS.COMPANY.BOOK>
    CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
    CRD.STA = CU.LCL<1,CULR.CREDIT.STAT>
    CRD.COD = CU.LCL<1,CULR.CREDIT.CODE>
    EMP.NO  = CU.LCL<1,CULR.EMPLOEE.NO>
    SS.ON   = (1000000 + EMP.NO)[2,6]

    IF CRD.STA NE '' OR CRD.COD GE 110 THEN
        IF USR.DEPT NE 9905 AND USR.DEPT NE 9912 THEN
            RETURN
        END
    END
    GOSUB AC.REC
    GOSUB LD.IND.REC
*    GOSUB COL.GIV.REC
*    GOSUB COL.RCV.REC
*    GOSUB LC.A.REC
*    GOSUB LC.B.REC
*    GOSUB LIM.REC
    RETURN
****************************************
AC.REC:
*--------
    CALL F.READ(FN.IND.AC,CU.ID,R.IND.AC,F.IND.AC,ER.IND.AC)
    LOOP
        REMOVE AC.ID FROM R.IND.AC SETTING POS.AC
    WHILE AC.ID:POS.AC
        IF AC.NO THEN
            IF AC.ID EQ AC.NO THEN
                CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
                WS.GL.ID = R.AC<AC.CATEGORY>
                WS.CY.ID = R.AC<AC.CURRENCY>
*Line [ 255 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR WS.GL.ID:' ' IN "1710 1711 1721 1701 5000 5001 5081 5082 5083 5084 5085 5090 5091 1021 1022 5079 5080 21078 21079 " SETTING POS.NOSHOW THEN '' ELSE
*#        IF WS.GL.ID # 1701 AND WS.GL.ID # 1710 AND WS.GL.ID # 1711 AND WS.GL.ID # 1021 AND WS.GL.ID # 1022 THEN
                    WS.WRK.AMT  = R.AC<AC.WORKING.BALANCE>
                    WS.ACT.AMT  = R.AC<AC.ONLINE.ACTUAL.BAL>
*Line [ 260 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    WS.BLK.AMT  = R.AC<AC.LOCKED.AMOUNT,(DCOUNT(R.AC<AC.LOCKED.AMOUNT>,@VM))>
                    IF WS.GL.ID EQ 1002 OR WS.GL.ID EQ 1407 OR WS.GL.ID EQ 1408 OR WS.GL.ID EQ 1413 OR WS.GL.ID EQ 1445 OR WS.GL.ID EQ 1455 THEN
                        IF R.USER<EB.USE.SIGN.ON.NAME> EQ SS.ON THEN
                            GOSUB FILL.ARRAY
                        END
                    END ELSE
                        GOSUB FILL.ARRAY
                    END
                END
            END
        END
    REPEAT
    RETURN
****************************************
LD.IND.REC:
*--------
    CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            GOSUB LD.REC
        END ELSE
            GOSUB MM.REC
        END
    REPEAT
    RETURN
****************************************
LD.REC:
*--------
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
    REC.STA     = R.LD<LD.STATUS>
    REC.DRW     = R.LD<LD.DRAWDOWN.ACCOUNT>
    IF REC.DRW EQ AC.NO THEN
        IF REC.STA NE 'LIQ' THEN
            WS.GL.ID = R.LD<LD.CATEGORY>
            WS.CY.ID = R.LD<LD.CURRENCY>
            LD.COL.ID   = R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
            WS.WRK.AMT  = R.LD<LD.AMOUNT>
            WS.ACT.AMT  = 0
            CALL DBR('COLLATERAL':@FM:COLL.LOCAL.REF,LD.COL.ID,COLL.LCL)
            WS.BLK.AMT  = COLL.LCL<1,COLR.COLL.AMOUNT>
            IF WS.GL.ID EQ 21096 OR WS.GL.ID EQ 21097 THEN
                WS.WRK.AMT = WS.WRK.AMT * -1
            END
            IF WS.GL.ID GE 21050 AND WS.GL.ID LE 21063 THEN
                WS.WRK.AMT = WS.WRK.AMT * -1
            END
            GOSUB FILL.ARRAY
            IF WS.GL.ID GE 21050 AND WS.GL.ID LE 21063 THEN
                PD.ID = 'PD':LD.ID
                GOSUB PD.REC
            END

        END
    END
    RETURN
****************************************
PD.REC:
*--------
    CALL F.READ(FN.PD,PD.ID,R.PD,F.PD,ER.PD)
* WS.GL.ID = R.PD<PD.CATEGORY>
    IF R.PD<PD.TOTAL.OVERDUE.AMT> GT 0  THEN
        WS.GL.ID    = 21150
        WS.CY.ID    = R.PD<PD.CURRENCY>
        WS.WRK.AMT  = R.PD<PD.TOTAL.OVERDUE.AMT> * -1
        WS.ACT.AMT  = 0
        WS.BLK.AMT  = 0
        GOSUB FILL.ARRAY
    END
    RETURN
****************************************
MM.REC:
*--------
    CALL F.READ(FN.MM,LD.ID,R.MM,F.MM,ER.MM)
    IF R.MM<MM.PRINCIPAL> GT 0 AND R.MM<MM.STATUS> NE 'LIQ' THEN
        WS.GL.ID    = R.MM<MM.CATEGORY>
        WS.CY.ID    = R.MM<MM.CURRENCY>
        IF WS.GL.ID EQ 21030 OR WS.GL.ID EQ 21031 THEN
            IF R.MM<MM.VALUE.DATE> LE TODAY THEN
                WS.WRK.AMT  = R.MM<MM.PRINCIPAL>
                WS.ACT.AMT  = 0
            END
            IF R.MM<MM.VALUE.DATE> GT TODAY THEN
                WS.WRK.AMT  = 0
                WS.ACT.AMT  = R.MM<MM.PRINCIPAL>
            END
        END ELSE
            IF R.MM<MM.VALUE.DATE> LE TODAY THEN
                WS.WRK.AMT  = R.MM<MM.PRINCIPAL> * -1
                WS.ACT.AMT  = 0
            END
            IF R.MM<MM.VALUE.DATE> GT TODAY THEN
                WS.WRK.AMT  = 0
                WS.ACT.AMT  = R.MM<MM.PRINCIPAL> * -1
            END
        END
        WS.BLK.AMT  = 0
        GOSUB FILL.ARRAY
    END
    RETURN
****************************************
LC.A.REC:
*--------
    CALL F.READ(FN.IND.LCA,CU.ID,R.IND.LCA,F.IND.LCA,ER.IND.LCA)
    LOOP
        REMOVE LCA.ID FROM R.IND.LCA SETTING POS.LCA
    WHILE LCA.ID:POS.LCA
        CALL F.READ(FN.LC,LCA.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                WS.GL.ID    = R.LC<TF.LC.CATEGORY.CODE>
                WS.CY.ID    = R.LC<TF.LC.LC.CURRENCY>
                IF WS.GL.ID GE 23100 AND WS.GL.ID LE 23299 THEN
                    WS.WRK.AMT  = R.LC<TF.LC.LIABILITY.AMT> * -1
                END ELSE
                    WS.WRK.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                END
                WS.ACT.AMT  = 0
                WS.BLK.AMT  = 0
                GOSUB FILL.ARRAY
            END
            WS.GL.ID    = R.LC<TF.LC.CATEGORY.CODE>
            WS.DR.SUB.ID = LCA.ID
            GOSUB DR.REC
        END
    REPEAT
    RETURN
****************************************
LC.B.REC:
*--------
    CALL F.READ(FN.IND.LCB,CU.ID,R.IND.LCB,F.IND.LCB,ER.IND.LCB)
    LOOP
        REMOVE LCB.ID FROM R.IND.LCB SETTING POS.LCB
    WHILE LCB.ID:POS.LCB
        CALL F.READ(FN.LC,LCB.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                WS.GL.ID    = R.LC<TF.LC.CATEGORY.CODE>
                WS.CY.ID    = R.LC<TF.LC.LC.CURRENCY>
                IF WS.GL.ID GE 23100 AND WS.GL.ID LE 23299 THEN
                    WS.WRK.AMT  = R.LC<TF.LC.LIABILITY.AMT> * -1
                END ELSE
                    WS.WRK.AMT  = R.LC<TF.LC.LIABILITY.AMT>
                END
                WS.ACT.AMT  = 0
                WS.BLK.AMT  = 0
                GOSUB FILL.ARRAY
            END
            WS.GL.ID    = R.LC<TF.LC.CATEGORY.CODE>
            WS.DR.SUB.ID = LCB.ID
            GOSUB DR.REC
        END
    REPEAT
    RETURN
****************************************
DR.REC:
*--------
    WS.LC.GL  = WS.GL.ID
    WS.SRL.DR = ''
    WS.NEXT.DR = WS.NEXT.DR + 0
    FOR IDR = 1 TO WS.NEXT.DR
        WS.SRL.ID = ( IDR + 100 )[2,2]
        WS.DR.ID = WS.DR.SUB.ID : WS.SRL.ID
        CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
        IF NOT(ER.DR) THEN
            IF R.DR<TF.DR.MATURITY.REVIEW> GT TODAY THEN
                IF WS.LC.GL GE 23100 AND WS.LC.GL LE 23299 THEN
                    WS.WRK.AMT = R.DR<TF.DR.DOCUMENT.AMOUNT> * -1
                    WS.GL.ID   = 50000
                END ELSE
                    WS.WRK.AMT = R.DR<TF.DR.DOCUMENT.AMOUNT>
                    WS.GL.ID   = 50001
                END
                WS.CY.ID   = R.DR<TF.DR.DRAW.CURRENCY>
                WS.ACT.AMT = 0
                WS.BLK.AMT = 0
                GOSUB FILL.ARRAY
            END
        END
    NEXT IDR
    WS.LC.GL = 0
    RETURN
****************************************
COL.GIV.REC:
*--------
    CALL F.READ(FN.IND.G.COL,CU.ID,R.IND.G.COL,F.IND.G.COL,ER.IND.G.COL)
    LOOP
        REMOVE COL.G.ID FROM R.IND.G.COL SETTING POS.G.COL
    WHILE COL.G.ID:POS.G.COL
        CALL F.READ(FN.COL.R,COL.G.ID,R.COL.R,F.COL.R,EER.R)
        WS.PRC.NO = DCOUNT(R.COL.R<COLL.RIGHT.PERCENT.ALLOC>,@VM)
        FOR X = 1 TO WS.PRC.NO
            WS.REF.CUS= R.COL.R<COLL.RIGHT.LIMIT.REF.CUST,X>
            IF WS.REF.CUS # CU.ID THEN
                WS.COL.ID =  COL.G.ID
                WS.CUS.ID.CHK = FIELD (WS.COL.ID,'.',1)
                WS.PRC =  R.COL.R<COLL.RIGHT.PERCENT.ALLOC,X>
                WS.PRC.MIA = WS.PRC / 100
                WS.GL.ID    = 34000
                GOSUB  SUB.GET.COL.AMT
            END
        NEXT X
    REPEAT
    RETURN
****************************************
COL.RCV.REC:
*--------
    CALL F.READ(FN.IND.R.COL,CU.ID,R.IND.R.COL,F.IND.R.COL,ER.IND.R.COL)
    LOOP
        REMOVE COL.R.ID FROM R.IND.R.COL SETTING POS.R.COL
    WHILE COL.R.ID:POS.R.COL
        CALL F.READ(FN.COL.R,COL.R.ID,R.COL.R,F.COL.R,EER.R)
        WS.PRC.NO = DCOUNT(R.COL.R<COLL.RIGHT.PERCENT.ALLOC>,@VM)
        FOR X = 1 TO WS.PRC.NO
            WS.REF.CUS= R.COL.R<COLL.RIGHT.LIMIT.REF.CUST,X>
            IF WS.REF.CUS EQ CU.ID THEN
                WS.COL.ID =  COL.R.ID
                WS.CUS.ID.CHK = FIELD (WS.COL.ID,'.',1)
                IF WS.CUS.ID.CHK # CU.ID THEN
                    WS.PRC =  R.COL.R<COLL.RIGHT.PERCENT.ALLOC,X>
                    WS.PRC.MIA = WS.PRC / 100
                    WS.GL.ID    = 35000
                    GOSUB  SUB.GET.COL.AMT
                END
            END
            WS.PRC.MIA = 0
        NEXT X
    REPEAT
    RETURN
****************************************
SUB.GET.COL.AMT:
    COL.AMT.P = 0
    CALL F.READ(FN.RIGHT.COL,WS.COL.ID,R.RIGHT.COL,F.RIGHT.COL,ER.RIGHT.COL)
    LOOP
        REMOVE RIGHT.COL.ID FROM R.RIGHT.COL SETTING POS.RIGHT.COL
    WHILE RIGHT.COL.ID:POS.RIGHT.COL
        CALL F.READ(FN.COL,RIGHT.COL.ID,R.COL,F.COL,EER)
*CY.CODE =  R.COL<COLL.CURRENCY>
        WS.CY.ID =  R.COL<COLL.CURRENCY>
*CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CY.CODE,WS.CY.NUM)
        WS.APP.ID  =  R.COL<COLL.APPLICATION.ID>[1,2]
        WS.EXP.DATE=  R.COL<COLL.EXPIRY.DATE>
        COL.AMT =  R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
        IF WS.APP.ID = 'LD' THEN
            IF  WS.EXP.DATE GE TODAY THEN
                WS.WRK.AMT  = COL.AMT *  WS.PRC.MIA
                WS.ACT.AMT  = 0
                WS.BLK.AMT  = 0
                GOSUB FILL.ARRAY
            END
        END
        IF WS.APP.ID # 'LD' THEN
            WS.WRK.AMT  = COL.AMT *  WS.PRC.MIA
            WS.ACT.AMT  = 0
            WS.BLK.AMT  = 0
            GOSUB FILL.ARRAY
        END
    REPEAT
    RETURN
************************************************
LIM.REC:
*--------
    CALL F.READ(FN.IND.LI,CU.ID,R.IND.LI,F.IND.LI,ER.IND.LI)
    LOOP
        REMOVE LI.ID FROM R.IND.LI SETTING POS.LI
    WHILE LI.ID:POS.LI
        IF FIELD(LI.ID,'.',4) EQ '' THEN
            CALL F.READ(FN.LIM,LI.ID,R.LIM,F.LIM,ER.LIM)
            WS.LI.PROD    = R.LIM<LI.LIMIT.PRODUCT>
            WS.PRO.ALL  = R.LIM<LI.PRODUCT.ALLOWED>
            IF  WS.PRO.ALL NE '' AND WS.LI.PROD NE 2000 AND WS.LI.PROD NE 5100 THEN
                WS.ON.LI.NO = DCOUNT(R.LIM<LI.ONLINE.LIMIT>,@VM)
                IF WS.LI.PROD NE 9700 AND WS.LI.PROD NE 9800 AND WS.LI.PROD NE 9900 THEN
                    WS.GL.ID        = 41000
                    WS.CY.ID    = R.LIM<LI.LIMIT.CURRENCY>
                    WS.WRK.AMT  = R.LIM<LI.ONLINE.LIMIT,1>
                    WS.ACT.AMT  = 0
                    WS.BLK.AMT  = 0
                    GOSUB FILL.ARRAY
                END
            END
        END
    REPEAT
    RETURN
****************************************
FILL.ARRAY:
*----------
    GOSUB SEPARATE.DR.CR

    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,WS.CY.ID,WS.CY.NUM)
    WS.GL.ID = (100000 + WS.GL.ID)[2,5]
*Line [ 555 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.GL.ID IN WS.GL.IND<1> SETTING POS.GL THEN '' ELSE
        KK1++
        WS.GL.IND<KK1> = WS.GL.ID
    END
*Line [ 560 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR WS.CY.NUM:"." IN WS.CY.IND SETTING POS.CY THEN '' ELSE
        WS.CY.IND := WS.CY.NUM :"."
    END

    ARR.ACT.AMT<WS.GL.ID,WS.CY.NUM> += WS.ACT.AMT
    ARR.WRK.AMT<WS.GL.ID,WS.CY.NUM> += WS.WRK.AMT
    ARR.BLK.AMT<WS.GL.ID,WS.CY.NUM> += WS.BLK.AMT
    RETURN
****************************************
SEND.DATA:
*--------
    PRINT WS.GL.IND
    WS.GL.IND   = SORT(WS.GL.IND)
    PRINT WS.GL.IND

    WS.GL.COUNT = DCOUNT(WS.GL.IND,@FM)
    WS.CY.COUNT = COUNT(WS.CY.IND,'.')
    FOR IGL = 1 TO WS.GL.COUNT
        WS.GL.POS = WS.GL.IND<IGL> + 0
        FOR ICY = 1 TO WS.CY.COUNT
            WS.CY.POS = FIELD(WS.CY.IND,'.',ICY)
            CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,WS.CY.POS,WS.CY.ID)
            IF ARR.WRK.AMT<WS.GL.POS,WS.CY.POS> NE '' THEN
                WS.GL.MASK = WS.GL.POS
                IF WS.GL.POS = 50000 THEN
                    WS.GL.MASK = 'LCDI'
                END
                IF WS.GL.POS = 50001 THEN
                    WS.GL.MASK = 'LCDE'
                END
                Y.CUS.DATA<-1> = CU.COM:"*":CU.ID:"*":WS.GL.MASK:"*":WS.CY.ID:"*":ARR.WRK.AMT<WS.GL.POS,WS.CY.POS>:"*":ARR.ACT.AMT<WS.GL.POS,WS.CY.POS>:"*":ARR.BLK.AMT<WS.GL.POS,WS.CY.POS>:"*":WS.CR.LCY.AMT:"*":WS.DR.LCY.AMT
            END
        NEXT ICY
    NEXT IGL
    RETURN
****************************************
SEPARATE.DR.CR:
    IF  WS.GL.ID GE 30000 THEN
        RETURN
    END
    IF  WS.GL.ID EQ 21096 THEN
        RETURN
    END
    IF  WS.GL.ID GE 21101 AND WS.GL.ID LE 21103 THEN
        RETURN
    END
    IF  WS.GL.ID[1,1] EQ 9 THEN
        RETURN
    END
    IF  WS.GL.ID[1,2] EQ 23 THEN
        RETURN
    END
    GOSUB GET.RATE

    WS.LCY.AMT  = WS.WRK.AMT * WS.RATE

    IF  WS.LCY.AMT LT 0 THEN
        WS.DR.LCY.AMT += WS.LCY.AMT
    END

    IF  WS.LCY.AMT GT 0 THEN
        WS.CR.LCY.AMT += WS.LCY.AMT
    END
    RETURN
****************************************
**** ---------------------- GET MID.RATE FROM CURRENCY FILE -------------------- ****
GET.RATE:
    WS.RATE = 0
    IF  WS.CY.ID NE 'EGP' THEN
        CALL F.READ(FN.CUR,WS.CY.ID,R.CUR,F.CUR,E1)
        WS.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
        IF WS.CY.ID EQ 'JPY' THEN
            WS.RATE = ( WS.RATE / 100 )
        END
    END ELSE
        WS.RATE = 1
    END
    RETURN
****************************************
END
