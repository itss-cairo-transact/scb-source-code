* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-45</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE COPY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.PAYMENT

*****
    OPENSEQ "/life/MUBOOK/NT24/bnk/bnk.run/&SAVEDLISTS&" , "DDREQ_SUZ_0000001.txt" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/life/MUBOOK/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"DDREQ_SUZ_0000001.txt"
        HUSH OFF
    END
    OPENSEQ "/life/MUBOOK/NT24/bnk/bnk.run/&SAVEDLISTS&" , "DDREQ_SUZ_0000001.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DDREQ_SUZ_0000001.txt CREATED IN /life/MUBOOK/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create DDREQ_SUZ_0000001.txt File IN /life/MUBOOK/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END

*****

*****CREATE NEW TEXT***

    BB.DATA =  "C0004829075;A0004866629;00001;"
    BB.DATA := "00015;00024;SUZ00100000000000002;"
    BB.DATA := "995;0110018810100101;03;2009;100;"
    BB.DATA := "20090401;1111"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    CLOSESEQ BB

END
