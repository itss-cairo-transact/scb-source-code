* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
******************* MAI SAAD 05/11/2017 ******************
*----------------------------------------------------------------------*
* <Rating>-10</Rating>
*----------------------------------------------------------------------*
    SUBROUTINE E.NOF.ACC1006.ONLINE.BAL(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*----------------------------------------------------------------------*
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    TOT.ACC = 0 ; TOT.AMT.RG100.TO.499K = 0 ; TOT.AMT.EG500.TO.999K = 0 ;
    TOT.AMT.RG1.TO.5M = 0 ; TOT.AMT.GT5M = 0;
    TOT.BRANCH = 0 ;TOT.ALL=0;ACC.COUNTER=0;ACC.ALL = 0

    ALL.RG100.TO.499K=0;ALL.EG500.TO.999K=0; ALL.RG1.TO.5M=0;ALL.GT5M=0
    ACC1.COUNTER=0;ACC2.COUNTER=0;ACC3.COUNTER=0;ACC4.COUNTER=0

    N.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 1006 AND OPEN.ACTUAL.BAL GE 100000 BY CO.CODE BY OPEN.ACTUAL.BAL"
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ER.MSG.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ(FN.ACC,KEY.LIST.N<1>, R.ACC,F.ACC, ERR.ACC)
** ACCT<1>   = R.ACC<@ID>
    BAL<1>       = R.ACC<AC.OPEN.ACTUAL.BAL>
    CO.CODE<1>   = R.ACC<AC.CO.CODE>

    ACC.COUNTER = ACC.COUNTER+1; ACC.ALL = ACC.ALL + 1
    BEGIN CASE
    CASE (BAL<1> GE 100000) AND (BAL<1> LT 500000)
        TOT.AMT.RG100.TO.499K = TOT.AMT.RG100.TO.499K+BAL<1>
        ALL.RG100.TO.499K = ALL.RG100.TO.499K+BAL<1>
        ACC1.COUNTER = ACC1.COUNTER+1
***********
    CASE (BAL<1> GE 500000) AND (BAL<1> LT 1000000)
        TOT.AMT.EG500.TO.999K = TOT.AMT.EG500.TO.999K+BAL<1>
        ALL.EG500.TO.999K = ALL.EG500.TO.999K+BAL<1>
        ACC2.COUNTER = ACC2.COUNTER+1
**************
** CASE (BAL<1> GE 1000000) AND  (BAL<1> LT 2000000)
**     TOT.AMT.RG1.TO.2M = TOT.AMT.RG1.TO.2M+BAL<1>
**     ALL.RG1.TO.2M = ALL.RG1.TO.2M + BAL<1>
******************
    CASE (BAL<1> GE 1000000) AND  (BAL<1>  LT 5000000)
        TOT.AMT.RG1.TO.5M = TOT.AMT.RG1.TO.5M+BAL<1>
        ALL.RG1.TO.5M = ALL.RG1.TO.5M +BAL<1>
        ACC3.COUNTER = ACC3.COUNTER+1
********************
    CASE (BAL<1> GE 5000000)
        TOT.AMT.GT5M = TOT.AMT.GT5M+BAL<1>
        ALL.GT5M = ALL.GT5M+BAL<1>
        ACC4.COUNTER = ACC4.COUNTER+1
    END CASE
    TOT.BRANCH = TOT.AMT.RG100.TO.499K+TOT.AMT.EG500.TO.999K+TOT.AMT.RG1.TO.5M+TOT.AMT.GT5M
   ** TOT.ALL = TOT.ALL+TOT.BRANCH
**********************END OF FIRST RECORD*****************************
    FOR I = 2 TO SELECTED.N
        CALL F.READ(FN.ACC,KEY.LIST.N<I>, R.ACC,F.ACC, ERR.ACC)
        ACCT<I>    = R.ACC<AC.MNEMONIC>
        BAL<I>     = R.ACC<AC.OPEN.ACTUAL.BAL>
        CO.CODE<I> = R.ACC<AC.CO.CODE>

        IF CO.CODE<I> # CO.CODE<I-1> THEN
**  Y.RET.DATA<-1> = CO.CODE<I-1> :"*": TOT.AMT.RG100.TO.499K:"*":TOT.AMT.EG500.TO.999K:"*":TOT.AMT.RG1.TO.5M:"*":TOT.AMT.GT5M:"*":TOT.BRANCH:"*":ACC.COUNTER:"*":ACC1.COUNTER:"*":ACC2.COUNTER:"*":ACC3.COUNTER:"*":ACC4.COUNTER
            Y.RET.DATA<-1> = CO.CODE<I-1> :"*": TOT.AMT.RG100.TO.499K:"*":TOT.AMT.EG500.TO.999K:"*":TOT.AMT.RG1.TO.5M:"*":TOT.AMT.GT5M:"*":TOT.BRANCH:"*":ACC.COUNTER

            TOT.AMT.RG100.TO.499K=0; TOT.AMT.EG500.TO.999K=0; TOT.AMT.RG1.TO.5M=0; TOT.AMT.GT5M=0;TOT.BRANCH = 0

            ACC.COUNTER=0;ACC1.COUNTER=0;ACC2.COUNTER=0;ACC3.COUNTER=0;ACC4.COUNTER=0
        END
        ACC.COUNTER = ACC.COUNTER+1;ACC.ALL = ACC.ALL + 1
        BEGIN CASE
        CASE (BAL<I> GE 100000) AND (BAL<I> LT 500000)
            TOT.AMT.RG100.TO.499K = TOT.AMT.RG100.TO.499K+BAL<I>
            ALL.RG100.TO.499K = ALL.RG100.TO.499K+BAL<I>
            ACC1.COUNTER = ACC1.COUNTER+1
***********
        CASE   (BAL<I> GE 500000) AND (BAL<I> LT 1000000)
            TOT.AMT.EG500.TO.999K = TOT.AMT.EG500.TO.999K+BAL<I>
            ALL.EG500.TO.999K = ALL.EG500.TO.999K+BAL<I>
            ACC2.COUNTER = ACC2.COUNTER+1
**************
**   CASE (BAL<I> GE 1000000) AND (BAL<I> LT 2000000)
**       TOT.AMT.RG1.TO.2M = TOT.AMT.RG1.TO.2M+BAL<I>
**      ALL.RG1.TO.2M = ALL.RG1.TO.2M + BAL<I>
******************
        CASE (BAL<I> GE 1000000) AND  (BAL<I>  LT 5000000)
            TOT.AMT.RG1.TO.5M = TOT.AMT.RG1.TO.5M+BAL<I>
            ALL.RG1.TO.5M = ALL.RG1.TO.5M +BAL<I>
            ACC3.COUNTER = ACC3.COUNTER+1
********************
        CASE (BAL<I> GE 5000000)
            TOT.AMT.GT5M = TOT.AMT.GT5M+BAL<I>
            ALL.GT5M = ALL.GT5M +BAL<I>
            ACC4.COUNTER = ACC4.COUNTER+1
        END CASE

        TOT.BRANCH = TOT.AMT.RG100.TO.499K+TOT.AMT.EG500.TO.999K+TOT.AMT.RG1.TO.5M+TOT.AMT.GT5M
       ** TOT.ALL = TOT.ALL + TOT.BRANCH
**********************LAST RECORD*****************
        IF I = SELECTED.N THEN
**            Y.RET.DATA<-1> = CO.CODE<I> :"*": TOT.AMT.RG100.TO.499K:"*":TOT.AMT.EG500.TO.999K:"*":TOT.AMT.RG1.TO.5M:"*":TOT.AMT.GT5M:"*":TOT.BRANCH:"*":ACC.COUNTER:"*":ACC1.COUNTER:"*":ACC2.COUNTER:"*":ACC3.COUNTER:"*":ACC4.COUNTER
            Y.RET.DATA<-1> = CO.CODE<I> :"*": TOT.AMT.RG100.TO.499K:"*":TOT.AMT.EG500.TO.999K:"*":TOT.AMT.RG1.TO.5M:"*":TOT.AMT.GT5M:"*":TOT.BRANCH:"*":ACC.COUNTER

        END
    NEXT I

**    Y.RET.DATA<-1> = '-------------':"*": "-------------":"*":"--------------":"*":"------------":"*":"------------":"*":"----------":"*":"-----------":"*":"---------------":"*":"-------------":"*":"-----------":"*":"-----------":"*":"-------------"
    Y.RET.DATA<-1> = '-------------':"*": "-------------":"*":"--------------":"*":"------------":"*":"------------":"*":"----------":"*":"-----------":"*":"---------------"

    Y.RET.DATA<-1> = 'Bank.Totals':"*": ALL.RG100.TO.499K:"*":ALL.EG500.TO.999K:"*":ALL.RG1.TO.5M:"*":ALL.GT5M:"*":"":"*":ACC.ALL
    RETURN
END
