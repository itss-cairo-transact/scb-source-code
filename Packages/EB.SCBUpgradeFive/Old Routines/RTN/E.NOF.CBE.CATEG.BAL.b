* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>3154</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CBE.CATEG.BAL(Y.ALL.REC)
***    PROGRAM E.NOF.CBE.CATEG.BAL

***Mahmoud Elhawary******22/7/2010****************************
* Nofile routine to get balances                             *
* of PL categories from a group of categories                *
**************************************************************

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 52 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    KK = 0
    XX = SPACE(120)

    PER.BAL1.EGP = 0
    COM.BAL1.EGP = 0
    PER.BAL2.EGP = 0
    COM.BAL2.EGP = 0
    PER.BAL3.EGP = 0
    COM.BAL3.EGP = 0

    PER.BAL1.FRN = 0
    COM.BAL1.FRN = 0
    PER.BAL2.FRN = 0
    COM.BAL2.FRN = 0
    PER.BAL3.FRN = 0
    COM.BAL3.FRN = 0

    CTE.AMT  = ''
    CTE.AMT.DET = ''
    CTE.BAL  = ''
    CAT.GRP  = ''
    CAT.GRP1  = '50000 50010 50161 50160 50162 54032 50180 50181 60010 60011 60012 60018 60019 60030 60014 60015 60307 60308 60335 60336 60340 60900 60301 60302 60304 60306 60102 60103 60104 60105 60106 60108 60107 60207 60206 60061 60062 60063 60072 62156 60310 60311 60343 60305 61502 61504 61505 61507 61508 61513 61005 62708 '
    CAT.GRP1 := '61110 62701 62772 62253 62704 62756 62759 62763 62762 62761 62774 62068 62054 62069 62066 62067 62058 56001 56002 56003 56004 56005 56006 56007 56008 56009 56010 56011 56012 56013 56014 56015 56016 56017 56018 56019 60461 61011 61041 62112 62354 62405 62400 62709 62401 60215 60309 60312 60339 62011 62019 62070 '
    CAT.GRP1 := '61010 61101 61115 61185 61200 62715 62720 61215 61135 61105 61235 61205 61225 62101 62304 62805 62106 62305 62757 62022 62211 62255 61100 62203 61315 62204 62656 62700 62014 62717 62716 60303 60531 60532 62108 60536 60537 62154 62109 61111 61112 62020 62200 '
    CAT.GRP1 := '62714 62718 62719 62804 64050 62755 62053 61500 61120 62013 62015 62661 62705 66341 62500 62507 62501 62503 62502 64875 64900 64905 62001 62006 62007 62005 62003 62004 62008 52881 62884 62885 62764 62779 62721 62706 63101 62024 62029 62030 62032 61902 '
    CAT.GRP2  = '51000 51164 57005 57011 57022 51154 57028 51020 51095 52006 54034 51075 51076 51073 51000 51074 54033 51080 51081 54001 54041 54002 54042 54062 54005 54038 52300 52030 52032 52033 52040 52301 52302 52303 52304 52305 52306 52307 52001 52028 52315 52112 52309 52405 52042 52317 52406 52407 52413 52400 52053 52414 '
    CAT.GRP2 := '57003 57004 57006 57007 57008 57009 57010 57012 57013 57014 57015 57016 57017 57018 57019 57020 57021 57023 57024 57025 57026 57027 57029 57030 57031 57032 57033 57034 57035 57036 57037 57038 57039 57040 57041 57042 57043 57044 52011 52025 52156 52157 52172 52203 54025 52161 52163 52106 52158 52051 52101 52878 '
    CAT.GRP2 := '52050 52052 52054 52057 52058 52461 52452 52453 52043 52110 52114 52151 52251 52871 52872 52875 52882 52015 52113 52873 52877 52605 52026 52109 52883 52213 52215 52620 52205 52218 52508 52514 52515 52538 51031 52517 52518 52212 54010 52516 52271 52535 52502 52503 52505 52534 52010 52513 52270 52272 52648 52649 57001 57002 '
    CAT.GRP2 := '54011 54012 53003 53006 53001 53410 53005 54065 54007 54018 54039 53107 53110 54035 52012 52014 52018 52022 52159 52171 52510 52600 52631 54015 54023 54024 54026 52630 54020 52024 54021 52023 52238 52045 54022 52234 54030 54027 52031 '

    REP.GRP1  = ' 1102 1404 1414 1206 1208 1401 1402 1405 1406 11240 11238 11232 11234 11239 1480 1481 1499 1483 1493 1202 1212 1301 1302 303  1377 1390 1399 1502 1503 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591 1001 1002 1003 1059 1205 1207 1216 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 559 1582 1407 1413 1408 6501 6502 6503 6504 6511 1558 1595 1223 1225 1221 1224 1222 1227 1220 12043'
    REP.GRP2  = ' 21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 6512 21020 21026 21021 21022 21027 21032 21028 21023 21024 21025 21029 6501 6502 6503 6504 6511 1012 1013 1014 1015 1016 1019 16170 '
    REP.GRP3  = ' 1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 1208 1211 1212 1216 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 11238 1407 1413 11240 11232 11234 11239 1480 1481 1499 1408 1582 1483 1595 1493 1558 '

    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''

    RRR = ''

    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CAT = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = '' ; ER.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CTE = 'F.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.CPL = 'FBNK.CONSOLIDATE.PRFT.LOSS' ; F.CPL = '' ; R.CPL = '' ; ER.CPL = ''
    CALL OPF(FN.CPL,F.CPL)
    FN.SLC = 'F.RE.STAT.LINE.CONT' ; F.SLC = '' ; R.SLC = '' ; ER.SLC = ''
    CALL OPF(FN.SLC,F.SLC)
    FN.SRL = 'F.RE.STAT.REP.LINE' ; F.SRL = '' ; R.SRL = '' ; ER.SRL = ''
    CALL OPF(FN.SRL,F.SRL)

    RETURN
****************************************
PROCESS:
*-------
*Line [ 127 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE "BRANCH"   IN D.FIELDS<1> SETTING YBRN.POS THEN CCMP     = D.RANGE.AND.VALUE<YBRN.POS> ELSE NULL

***    CCMP = '1'

    END.DATE = TODAY
    CALL CDT("",END.DATE,'-1W')
    CAT.GRP = CAT.GRP1:CAT.GRP2
*    CRT END.DATE

    IF CCMP THEN
        AA = LEN(CCMP)
        IF AA EQ 1 THEN
            CCMP = '0':CCMP
        END
        CCMP = 'EG00100':CCMP
    END
    COMP.SEL = "SSELECT F.COMPANY"
    IF CCMP THEN
        COMP.SEL := " WITH @ID EQ ":CCMP
    END
    CALL EB.READLIST(COMP.SEL,K.LIST1,'',SELECTED1,ER.MSG1)
    LOOP
        REMOVE COMP.ID FROM K.LIST1 SETTING POS.COMP
    WHILE COMP.ID:POS.COMP
        SLC.SEL  = "SELECT ":FN.SLC:" WITH @ID EQ SCBPLFIN.0070.":COMP.ID
        SLC.SEL := " SCBPLFIN.0080.":COMP.ID:" SCBPLFIN.0090.":COMP.ID
        SLC.SEL := " SCBPLFIN.0130.":COMP.ID:" SCBPLFIN.5040.":COMP.ID
        SLC.SEL := " SCBPLFIN.5050.":COMP.ID:" SCBPLFIN.5060.":COMP.ID
        SLC.SEL := " SCBPLFIN.5070.":COMP.ID:" SCBPLFIN.5090.":COMP.ID
        SLC.SEL := " SCBPLFIN.5160.":COMP.ID:" SCBPLFIN.0110.":COMP.ID
        SLC.SEL := " SCBPLFIN.5161.":COMP.ID:" SCBPLFIN.5030.":COMP.ID
        SLC.SEL := " SCBPLFIN.2570.":COMP.ID:" SCBPLFIN.2620.":COMP.ID
        SLC.SEL := " SCBPLFIN.5340.":COMP.ID
        SLC.SEL := " SCBPLFIN.7550.":COMP.ID:" SCBPLFIN.7570.":COMP.ID
        SLC.SEL := " SCBPLFIN.7840.":COMP.ID:" SCBPLFIN.0081.":COMP.ID
        SLC.SEL := " SCBPLFIN.0091.":COMP.ID:" SCBPLFIN.2571.":COMP.ID
        SLC.SEL := " SCBPLFIN.2581.":COMP.ID
        SLC.SEL := " BY @ID"
        CALL EB.READLIST(SLC.SEL,K.LIST,'',SELECTED,ER.MSG)
        IF SELECTED THEN
            LOOP
                REMOVE SLC.ID FROM K.LIST SETTING POS.SLC
            WHILE SLC.ID:POS.SLC
                CALL F.READ(FN.SLC,SLC.ID,R.SLC,F.SLC,ER.SLC)
                LI.ID = FIELD(SLC.ID,".",2)
*Line [ 173 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                KK1 = DCOUNT(R.SLC<RE.SLC.PRFT.CONSOL.KEY>,@VM)
                FOR I = 1 TO KK1
                    CPL.ID = R.SLC<RE.SLC.PRFT.CONSOL.KEY,I>
                    SLC.CUR= R.SLC<RE.SLC.PROFIT.CCY,I>
                    CALL F.READ(FN.CPL,CPL.ID,R.CPL,F.CPL,ER.CPL)
                    CPL.CUR  = R.CPL<RE.PTL.CURRENCY>
                    CON.SEC  = R.CPL<RE.PTL.VARIABLE.6>
                    CPL.DPT  = R.CPL<RE.PTL.VARIABLE.2>
                    CTE.PROD = R.CPL<RE.PTL.VARIABLE.4>
                    CTE.AMT  = 0
*Line [ 184 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    SS =  DCOUNT(CPL.CUR,@VM)
                    FOR II = 1 TO SS
                        CPL.CCY = R.CPL<RE.PTL.CURRENCY,II>
                        IF R.CPL<RE.PTL.BALANCE,II>         = '' THEN R.CPL<RE.PTL.BALANCE,II>         = 0
                        IF R.CPL<RE.PTL.CREDIT.MOVEMENT,II> = '' THEN R.CPL<RE.PTL.CREDIT.MOVEMENT,II> = 0
                        IF R.CPL<RE.PTL.DEBIT.MOVEMENT,II>  = '' THEN R.CPL<RE.PTL.DEBIT.MOVEMENT,II>  = 0
                        IF R.CPL<RE.PTL.BALANCE.YTD,II>     = '' THEN R.CPL<RE.PTL.BALANCE.YTD,II>     = 0
                        IF R.CPL<RE.PTL.CCY.BALANCE,II>     = '' THEN R.CPL<RE.PTL.CCY.BALANCE,II>     = 0
                        IF R.CPL<RE.PTL.CCY.CREDT.MVE,II>   = '' THEN R.CPL<RE.PTL.CCY.CREDT.MVE,II>   = 0
                        IF R.CPL<RE.PTL.CCY.DEBIT.MVE,II>   = '' THEN R.CPL<RE.PTL.CCY.DEBIT.MVE,II>   = 0
                        IF R.CPL<RE.PTL.CCY.BALANCE.YTD,II> = '' THEN R.CPL<RE.PTL.CCY.BALANCE.YTD,II> = 0
*                        IF CPL.CCY EQ 'EGP' THEN
                        LOCATE CPL.CCY IN SLC.CUR<1,1,1> SETTING POS.CUR THEN
                            CTE.AMT.DET  = R.CPL<RE.PTL.BALANCE,II> + R.CPL<RE.PTL.CREDIT.MOVEMENT,II> + R.CPL<RE.PTL.DEBIT.MOVEMENT,II> + R.CPL<RE.PTL.BALANCE.YTD,II>
                            CTE.AMT  += CTE.AMT.DET
*                        END ELSE
*                            BALL     = R.CPL<RE.PTL.CCY.BALANCE,II>
*                            CR.MOV   = R.CPL<RE.PTL.CCY.CREDT.MVE,II>
*                            DR.MOV   = R.CPL<RE.PTL.CCY.DEBIT.MVE,II>
*                            BAL.YTD  = R.CPL<RE.PTL.CCY.BALANCE.YTD,II>
*                            CALL EGP.BASE.CURR(BALL,CPL.CCY)
*                            CALL EGP.BASE.CURR(CR.MOV,CPL.CCY)
*                            CALL EGP.BASE.CURR(DR.MOV,CPL.CCY)
*                            CALL EGP.BASE.CURR(BAL.YTD,CPL.CCY)
*                            CTE.AMT.DET = BALL + CR.MOV + DR.MOV + BAL.YTD
*                            CTE.AMT    += CTE.AMT.DET
                        END
                    NEXT II
                    IF CTE.AMT THEN
                        IF LI.ID EQ '5040' OR LI.ID EQ '5050' OR LI.ID EQ '5060' OR LI.ID EQ '5070' OR LI.ID EQ '5090' OR LI.ID EQ '5160' OR LI.ID EQ '5340' OR LI.ID EQ '7550' OR LI.ID EQ '7570' OR LI.ID EQ '7840' OR LI.ID EQ '5161' OR LI.ID EQ '5030' THEN
                            IF CON.SEC EQ 2000 OR (CON.SEC GE 1000 AND CON.SEC LE 1400) THEN
                                IF CPL.CUR NE 'EGP' THEN
                                    PER.BAL1.FRN += CTE.AMT
                                END ELSE
                                    PER.BAL1.EGP += CTE.AMT
                                END
                            END ELSE
                                IF CPL.CUR NE 'EGP' THEN
                                    COM.BAL1.FRN += CTE.AMT
                                END ELSE
                                    COM.BAL1.EGP += CTE.AMT
                                END
                            END
                        END
                        IF LI.ID EQ '0080' OR LI.ID EQ '0090' OR LI.ID EQ '0130' OR LI.ID EQ '2570' OR LI.ID EQ '2620' OR LI.ID EQ '0081' OR LI.ID EQ '0091' OR LI.ID EQ '2571' OR LI.ID EQ '2581' THEN
                            IF CON.SEC EQ 2000 OR (CON.SEC GE 1000 AND CON.SEC LE 1400) THEN
                                IF CPL.CUR NE 'EGP' THEN
                                    PER.BAL2.FRN += CTE.AMT
                                END ELSE
                                    PER.BAL2.EGP += CTE.AMT
                                END
                            END ELSE
                                IF CPL.CUR NE 'EGP' THEN
                                    COM.BAL2.FRN += CTE.AMT
                                END ELSE
                                    COM.BAL2.EGP += CTE.AMT
                                END
                            END
                        END
                        IF LI.ID EQ '0070' OR LI.ID EQ '0110' THEN
                            IF CON.SEC EQ 2000 OR (CON.SEC GE 1000 AND CON.SEC LE 1400) THEN
                                IF CPL.CUR NE 'EGP' THEN
                                    PER.BAL3.FRN += CTE.AMT
                                END ELSE
                                    PER.BAL3.EGP += CTE.AMT
                                END
                            END ELSE
                                IF CPL.CUR NE 'EGP' THEN
                                    COM.BAL3.FRN += CTE.AMT
                                END ELSE
                                    COM.BAL3.EGP += CTE.AMT
                                END
                            END
                        END
                    END
                NEXT I
            REPEAT
        END
    REPEAT
    GOSUB RET.DATA
    RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = END.DATE:"*":PER.BAL1.EGP:"*":PER.BAL1.FRN:"*":COM.BAL1.EGP:"*":COM.BAL1.FRN:"*":PER.BAL2.EGP:"*":PER.BAL2.FRN:"*":COM.BAL2.EGP:"*":COM.BAL2.FRN:"*":PER.BAL3.EGP:"*":PER.BAL3.FRN:"*":COM.BAL3.EGP:"*":COM.BAL3.FRN:"*":LI.ID:"*":CCMP
*    CRT LI.ID
*    CRT "1*PER *":PER.BAL1.EGP:SPACE(20-LEN(PER.BAL1.EGP)):"*":PER.BAL1.FRN:SPACE(20-LEN(PER.BAL1.FRN)):"*":FMT(PER.BAL1.EGP+PER.BAL1.FRN,"L2,")
*    CRT "1*COM *":COM.BAL1.EGP:SPACE(20-LEN(COM.BAL1.EGP)):"*":COM.BAL1.FRN:SPACE(20-LEN(COM.BAL1.FRN)):"*":FMT(COM.BAL1.EGP+COM.BAL1.FRN,"L2,")
*    CRT "2*PER *":PER.BAL2.EGP:SPACE(20-LEN(PER.BAL2.EGP)):"*":PER.BAL2.FRN:SPACE(20-LEN(PER.BAL2.FRN)):"*":FMT(PER.BAL2.EGP+PER.BAL2.FRN,"L2,")
*    CRT "2*COM *":COM.BAL2.EGP:SPACE(20-LEN(COM.BAL2.EGP)):"*":COM.BAL2.FRN:SPACE(20-LEN(COM.BAL2.FRN)):"*":FMT(COM.BAL2.EGP+COM.BAL2.FRN,"L2,")
*    CRT "3*PER *":PER.BAL3.EGP:SPACE(20-LEN(PER.BAL3.EGP)):"*":PER.BAL3.FRN:SPACE(20-LEN(PER.BAL3.FRN)):"*":FMT(PER.BAL3.EGP+PER.BAL3.FRN,"L2,")
*    CRT "3*COM *":COM.BAL3.EGP:SPACE(20-LEN(COM.BAL3.EGP)):"*":COM.BAL3.FRN:SPACE(20-LEN(COM.BAL3.FRN)):"*":FMT(COM.BAL3.EGP+COM.BAL3.FRN,"L2,")
*    CRT STR("=",60)
*    CRT "1*TOT *":FMT(PER.BAL1.EGP + PER.BAL1.FRN + COM.BAL1.EGP+COM.BAL1.FRN,"L2,")
*    CRT "2*TOT *":FMT(PER.BAL2.EGP + PER.BAL2.FRN + COM.BAL2.EGP+COM.BAL2.FRN,"L2,")
*    CRT "3*TOT *":FMT(PER.BAL3.EGP + PER.BAL3.FRN + COM.BAL3.EGP+COM.BAL3.FRN,"L2,")
*    CRT STR("*",60)
    RETURN
**************************************
END
