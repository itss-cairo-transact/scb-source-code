* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>663</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE DEPOSIT.INT.RATES

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

    GOSUB INITIATE
*Line [ 35 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
****** REPORT.ID='DEPOSIT.INT.RATES'
       REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    YYYY = COMI[1,4]
    MM = COMI[5,2]
    DD = COMI[7,2]

    DATFMT = YYYY:"/":MM:"/":DD
    F.LD.TYPE= '' ; FN.LD.TYPE = 'F.SCB.LD.TYPE.LEVEL' ; R.LD.TYPE = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.LD.TYPE,F.LD.TYPE)

    CURR.LD = ''
    CURR.LD<1> = 'EGP'
    CURR.LD<2> = 'SAR'
    CURR.LD<3> = 'USD'
    CURR.LD<4> = 'GBP'
    CURR.LD<5> = 'EUR'
    CURR.LD<6> = 'CHF'

    FOR CCYN = 1 TO 6         ;* LOOP FOR CURRENCY
        AMTFR = '' ; AMTTO = '' ; RANGETEXT = '' ; XX = '' ; RATT = ''
        J = '' ; VAL.DESC = '' ; VAL.WK = '' ; VAL.1M = '' ; VAL.2M = '' ; VAL.3M = '' ; VAL.6M = '' ; VAL.1Y = ''
        GG = '' ; SS = '' ; PP = ''

        PRINT SPACE(20):"CUSTOMERS DEPOSIT INTEREST RATES AT VALUE ":DATFMT
        PRINT SPACE(31):"CURRENCY :" : CURR.LD<CCYN>
        PRINT SPACE(20):"-------------------------------------------------------"
        PRINT
        PRINT SPACE(1):"-------------------------------------------------------------------------------------------"
        PRINT SPACE(1):"| RANGE                      | 1 WK    | 1 MTH   | 2 MTH   | 3 MTH   | 6 MTH   | 1 YR    | "
        PRINT SPACE(1):"-------------------------------------------------------------------------------------------"


        T.SEL = "SELECT F.SCB.LD.TYPE.LEVEL WITH @ID LIKE ...":COMI :" BY @ID"
        KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
        IF SELECTED THEN
            FOR PP = 1 TO SELECTED      ;* LOOP FOR PERIODS
              **  SS = '' ; GG = ''
                CALL F.READ(FN.LD.TYPE,KEY.LIST<PP>, R.LD.TYPE, F.LD.TYPE ,E1)
                LD.CURR = R.LD.TYPE<LDTL.CURRENCY>
                LOCATE CURR.LD<CCYN> IN LD.CURR<1,1> SETTING DD THEN
                    SS = '' ; GG = ''
                    LD.AMTFR = R.LD.TYPE<LDTL.AMT.FROM,DD>
*Line [ 94 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    SS = DCOUNT(LD.AMTFR,@SM)
                    FOR GG = 1 TO SS    ;* LOOP FOR THE RANGES FOR EVERY CURRENCY
                        AMTFR<GG> = R.LD.TYPE<LDTL.AMT.FROM,DD,GG>
                        AMTTO<GG> = R.LD.TYPE<LDTL.AMT.TO,DD,GG>
                        RANGE = AMTFR<GG>:"-": AMTTO<GG>
                        RANFMT = STR(" ",27 - LEN(RANGE))
                        RANGETEXT<GG> = "| ":RANGE:RANFMT:"|":" "
                        VAL.DESC<GG,1> = RANGETEXT<GG>
                        AMTRAT = R.LD.TYPE<LDTL.RATE,DD,GG>
                        IF LEN(AMTRAT) = '1' THEN
                            RATT<GG> = AMTRAT:".00000":" |"
                        END ELSE
                            AMTRATSP = STR("0",7 - LEN(AMTRAT))
                            RATT<GG> = AMTRAT:AMTRATSP:" |"
                        END

                        IF  KEY.LIST<PP>[1,5] = '21001' THEN
                            VAL.WK<GG,2> = RATT<GG>
                        END
                        IF  KEY.LIST<PP>[1,5] = '21003' THEN
                            VAL.1M<GG,3> = RATT<GG>
                        END
                        IF  KEY.LIST<PP>[1,5] = '21004' THEN
                            VAL.2M<GG,4> = RATT<GG>
                        END
                        IF  KEY.LIST<PP>[1,5] = '21005' THEN
                            VAL.3M<GG,5> = RATT<GG>
                        END
                        IF  KEY.LIST<PP>[1,5] = '21006' THEN
                            VAL.6M<GG,6> = RATT<GG>
                        END
                        IF  KEY.LIST<PP>[1,5] = '21007' THEN
                            VAL.1Y<GG,7> = RATT<GG>
                        END
                  NEXT GG   ;* NEXT RANGE
                END ELSE      ;* IF CURR NOT FOUND
                        IF  KEY.LIST<PP>[1,5] = '21001' THEN
                            VAL.WK<GG,2> =  "0000000":" |"
                        END
                        IF  KEY.LIST<PP>[1,5] = '21003' THEN
                            VAL.1M<GG,3> = "0000000":" |"
                        END
                        IF  KEY.LIST<PP>[1,5] = '21004' THEN
                            VAL.2M<GG,4> = "0000000":" |"
                        END
                        IF  KEY.LIST<PP>[1,5] = '21005' THEN
                            VAL.3M<GG,5> = "0000000":" |"
                        END
                        IF  KEY.LIST<PP>[1,5] = '21006' THEN
                            VAL.6M<GG,6> = "0000000":" |"
                        END
                        IF  KEY.LIST<PP>[1,5] = '21007' THEN
                            VAL.1Y<GG,7> = "0000000":" |"
                        END
              **      RATT<PP> = "0000000":" |"
                END ;* IF CURR NOT FOUND

            NEXT PP ;* NEW PERIOD

            FOR GG = 1 TO SS
                XX<1,GG>[2,28] = VAL.DESC<GG,1>
                XX<1,GG>[33,10] = VAL.WK<GG,2>
                XX<1,GG>[43,10] = VAL.1M<GG,3>
                XX<1,GG>[53,10] = VAL.2M<GG,4>
                XX<1,GG>[63,10] = VAL.3M<GG,5>
                XX<1,GG>[73,10] = VAL.6M<GG,6>
                XX<1,GG>[83,10] = VAL.1Y<GG,7>
                PRINT  XX<1,GG>
                PRINT SPACE(1):"-------------------------------------------------------------------------------------------"
            NEXT GG ;*NEW RANGE FOR CURRENCY*
            PRINT
            PRINT
            PRINT
            PRINT
            PRINT
            PRINT
            PRINT
            PRINT

*********************************************
        END         ;* OF SELECTED
    NEXT CCYN       ;*NEW CURRENCY*
    TEXT = '��� ������� �����' ; CALL REM
**END
    RETURN
*===============================================================
END
