* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*----------------------------NI7OOOOOOOOOOOOOOOOOOO------------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.TF.LEN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS

    FN.CAT   = 'F.CATEGORY' ; F.CAT = '' ; R.CAT = ''
    CALL OPF( FN.CAT,F.CAT)

    FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    CALL OPF( FN.LC,F.LC)

    FN.DR   = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
    CALL OPF( FN.DR,F.DR)

    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF( FN.LD,F.LD)

* CALL F.READ( FN.DR,O.DATA, R.DR, F.DR,ETEXT)


    XX = FIELD(O.DATA,'*',1)
    YY = FIELD(O.DATA,'*',2)


    IF LEN(XX) = 14 AND XX[1,2] EQ 'TF' THEN
        DD = XX[1,12]
        CALL F.READ( FN.LC,DD, R.LC, F.LC,ETEXT)
        LCTYPE = R.LC<TF.LC.LC.TYPE>
        CONF   = R.LC<TF.LC.CONFIRM.INST>
        LCT    = LCTYPE[1,2]
        IF LCT EQ 'LE' AND CONF EQ 'WITHOUT' THEN
            O.DATA = "����� � �  ������� ��� ���� "
        END
        IF LCT EQ 'LE' AND CONF EQ 'CONFIRM' THEN
            O.DATA = "����� �  ������� ��� ���� "
        END

        IF LCT EQ 'LI' THEN
            O.DATA = "������� �������� ��� ����"
        END
    END
    IF LEN(XX) = 12 AND XX[1,2] = 'TF' THEN
        FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
        CALL OPF( FN.LC,F.LC)
        CALL F.READ( FN.LC,XX[1,12], R.LC, F.LC,ETEXT1)
        CATT = R.LC<TF.LC.CATEGORY.CODE>
**TEXT = CATT ; CALL REM
        CALL F.READ( FN.CAT,CATT, R.CAT, F.CAT,ETEXT)
        NAME = R.CAT<EB.CAT.DESCRIPTION><1,2>
**TEXT = NAME ; CALL REM
        O.DATA = NAME
    END ELSE
        CALL F.READ( FN.CAT,YY, R.CAT, F.CAT,NAME22)
       O.DATA = R.CAT<EB.CAT.DESCRIPTION><1,2>
    END

    RETURN
END
