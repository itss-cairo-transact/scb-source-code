* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1151</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  E.NOF.CUST.CHQ2(CHQ.TT.DR)

***Mahmoud Elhawary******16/6/2011****************************
**************************************************************

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 49 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    KK1 = 0
    XX1 = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ER.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = '' ; ER.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)
    FN.TT = 'FBNK.TELLER' ; F.TT = '' ; R.TT = '' ; ER.TT = ''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H = 'FBNK.TELLER$HIS' ; F.TT.H = '' ; R.TT.H = '' ; ER.TT.H = ''
    CALL OPF(FN.TT.H,F.TT.H)
    FN.CHQ = 'F.SCB.FT.DR.CHQ' ; F.CHQ = '' ; R.CHQ = '' ; ER.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO" IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,CUS.COMP)
*#    IF COMP EQ CUS.COMP THEN
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
    LOOP
        REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
    WHILE ACCT.ID:POS.CUS.ACC
        CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
        AC.CAT = R.ACCT<AC.CATEGORY>
*****************************
        CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
        LOOP
            REMOVE STE.ID FROM ID.LIST SETTING POS
        WHILE STE.ID:POS
            CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
            STE.CUR = R.STE<AC.STE.CURRENCY>
            STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
            STE.DAT = R.STE<AC.STE.BOOKING.DATE>
            STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
            STE.REF = R.STE<AC.STE.OUR.REFERENCE>
            IF NOT(STE.REF) THEN
                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
            END
            FINDSTR '\B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'\',1) ; BR.FLG = 1  ELSE BR.FLG = ''
            IF STE.AMT LT 0 THEN
                IF STE.CUR NE 'EGP' THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                CALL DBR('TRANSACTION':@FM:1,STE.TXN,STE.TRN)
*******************
                R.TT = '' ; ER.TT = ''
                CALL F.READ(FN.TT,STE.REF,R.TT,F.TT,ER.TT)
                IF NOT(R.TT) THEN
                    CALL F.READ(FN.TT.H,STE.REF:';1',R.TT,F.TT.H,ER.TT.H)
                END
*******************
                CHQ.NO = R.TT<TT.TE.CHEQUE.NUMBER>
                TR.TYP = R.TT<TT.TE.TRANSACTION.CODE>
                TT.CO  = R.TT<TT.TE.CO.CODE>
                TT.INP = R.TT<TT.TE.INPUTTER>
                TT.BEN = R.TT<TT.TE.NARRATIVE.2>
                IF R.TT THEN
                    IF CHQ.NO THEN
                        GOSUB FILL.ARRAY
                    END
                END
            END
        REPEAT
    REPEAT
*#    END
    IF XX1 THEN
        XX1 = SORT(XX1)
        FOR XY = 1 TO KK1
            GOSUB RET.DATA
        NEXT XY
    END
    RETURN
**************************************
FILL.ARRAY:
*----------
    KK1++
    XX1<KK1> = COMP:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":STE.DAT:"*":CHQ.NO:"*":STE.TXN:"*":STE.REF:"*":STE.AMT:"*":STE.CUR:"*":TT.BEN:"*":TT.CO:"*":TT.INP
    RETURN
**************************************
RET.DATA:
*--------
    IF FIELD(XX1<XY>,'*',6) THEN
        CHQ.TT.DR<-1>= XX1<XY>
    END
*#    CHQ.TT.DR<-1>= COMP:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":STE.DAT:"*":CHQ.NO:"*":STE.TXN:"*":STE.REF:"*":STE.AMT:"*":STE.CUR:"*":TT.BEN:"*":TT.CO
    RETURN
**************************************
END
