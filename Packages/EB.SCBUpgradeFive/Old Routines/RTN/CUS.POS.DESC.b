* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE CUS.POS.DESC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ENTRY.STMT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.TYPE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

    ETEXT = ''
    IF O.DATA[1,2] EQ 'TF' THEN

        FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
        CALL OPF( FN.LC,F.LC)
        FN.DR   = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
        CALL OPF( FN.DR,F.DR)
        CALL F.READ( FN.DR,O.DATA, R.DR, F.DR,ETEXT)
        IF NOT(ETEXT) THEN

            CALL F.READ( FN.LC,O.DATA[1,12], R.LC, F.LC,ETEXT1)
            CATT = R.LC<TF.LC.CATEGORY.CODE>

            IF CATT EQ '23100' OR CATT EQ '23102' OR CATT EQ '23152' OR CATT EQ '23155' OR CATT EQ '23150' OR CATT EQ '23010' OR CATT EQ '23014' OR CATT EQ '23030' OR CATT EQ '23031' THEN
                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,DESC2)

                DESC1   = "�������� ��� ���� ��"

                O.DATA  = DESC1 : " " :DESC2
*                TEXT = O.DATA ; CALL REM

            END ELSE
                O.DATA = ""
            END
        END ELSE
            CALL F.READ( FN.LC,O.DATA[1,12], R.LC, F.LC,ETEXT1)
            CATT  = R.LC<TF.LC.CATEGORY.CODE>
            TYPE1 = R.LC<TF.LC.LOCAL.REF><1,LCLR.SCB.TYPE>

            CALL DBR('SCB.LC.TYPE':@FM:1,TYPE1,DESC1)
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,DESC2)
            O.DATA = DESC2:' ':DESC1

        END
    END ELSE
        O.DATA = ""
    END
    RETURN
END
