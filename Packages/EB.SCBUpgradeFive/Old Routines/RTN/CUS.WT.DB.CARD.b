* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*----------------------------MAI SAAD OCT 2019-------------------------------------------------
* <Rating>1812</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CUS.WT.DB.CARD
*    PROGRAM CUS.WT.DB.CARD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

    YTEXT = "Enter branch number:"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    BR.CODE = COMI
    IF LEN(BR.CODE) < 2 THEN
        BR.CODE = '0': BR.CODE
    END
    DIRC = "&SAVEDLISTS&/MAI-TEST"
    FILE.NAME1 =  "CUST.WT.DB.CARD-":BR.CODE:".CSV"
*   FILE.NAME2 =  "CUST.DB.CARD.CSV-":BR.CODE:".CSV"


    OPENSEQ DIRC , FILE.NAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':DIRC:' ':FILE.NAME1
        HUSH OFF
    END
    OPENSEQ DIRC, FILE.NAME1 TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ': FILE.NAME1: 'CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ':FILE.NAME1  :'File IN &SAVEDLISTS&'
        END
    END

    BB.DATA = "BRANCH,CUSTOMER.ID,CUSTOMER.NAME,LEDGER,ACTUAL.BAL,CURRENCY"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    F.ACCOUNT = '' ; FN.ACCOUNT = 'FBNK.ACCOUNT' ; R.ACCOUNT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    N.SEL =  "SELECT FBNK.ACCOUNT WITH ((CATEGORY GT 1000 AND CATEGORY LT 1205)"
    N.SEL:=  " OR (CATEGORY GT 1208 AND CATEGORY LT 1500)"
    N.SEL:=  " OR (CATEGORY LIKE 65...))"
    N.SEL:=  " AND CO.CODE EQ EG00100":BR.CODE:" BY CUSTOMER"

    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""
    PRINT 'BEGIN'
    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    IF SELECTED.N THEN
        CUST.N = ''
*        PRINT "SELECTED.N = ": SELECTED.N
        FOR NN = 1 TO SELECTED.N
            CUST.N = ''
            AC.ID = KEY.LIST.N<NN>
            IF AC.ID MATCH '0N' THEN
                CALL F.READ(FN.ACCOUNT, KEY.LIST.N<NN>, R.ACCOUNT, F.ACCOUNT, E1)
                CUST.N = R.ACCOUNT<AC.CUSTOMER>
                CALL F.READ(FN.CUSTOMER, CUST.N, R.CUSTOMER, F.CUSTOMER, E2)
                LOCAL.REF.CUS = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NEW.SEC = LOCAL.REF.CUS<1,CULR.NEW.SECTOR>
                IF NEW.SEC EQ '4650' THEN
                    T.SEL = "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC... AND VISA.CUST.NO EQ ":CUST.N

                    KEY.LIST=""
                    SELECTED=""
                    ER.MSG=""

                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                    IF SELECTED THEN
                        FOR I = 1 TO SELECTED
                            BANK.ACC.2 = ''    ; CUST = ''
                            FN.CARD.ISSUE = 'FBNK.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
                            KEY.TO.USE = KEY.LIST<I>
                            CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                            CALL F.READ(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                            LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                            CUST.DEPT = LOCAL.REF<1,LRCI.BRANCH.NO>
                            CR.CARD.NO = LOCAL.REF<1,LRCI.VISA.NO>
                        NEXT I
                    END ELSE
                        BRANCH  = R.ACCOUNT<AC.CO.CODE>
                        CATEG   = AC.ID[11,4]
                        BAL     = R.ACCOUNT<AC.OPEN.ACTUAL.BAL>
                        CURR    = R.ACCOUNT<AC.CURRENCY>
                        CUS.NAME = R.CUSTOMER<EB.CUS.NAME.1><1,1>
******** WRITE TO FILE *********
                        BB.DATA = BRANCH:",":CUST.N:",":CUS.NAME:",":CATEG:",":BAL:",":CURR

                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END
                    END
                END
            END
        NEXT NN
    END
    TEXT= "PROGRAM IS FINISHED "; CALL REM;
    RETURN
END
