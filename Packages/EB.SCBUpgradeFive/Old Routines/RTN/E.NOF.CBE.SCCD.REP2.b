* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***************************MAHMOUD 31/8/2014******************************
    SUBROUTINE E.NOF.CBE.SCCD.REP2(Y.RET.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 39 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    KK = 0
***********************
**    TTMM = R.DATES(EB.DAT.LAST.PERIOD.END)
    DYNO = 0
    TDD = TODAY
    TD1 = TODAY
    DYNO = FMT(ICONV(TD1,'D'),'DW')
    TDYNO = '-':DYNO:'C'
    CALL CDT('',TD1,TDYNO)
    CRT TD1
***********************
    ARR.DATA = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

    ARR.DATA<1,1> = 1 ; ARR.DATA<2,1> = '�����'
    ARR.DATA<1,2> = 2 ; ARR.DATA<2,2> = '���� ��������'
    ARR.DATA<1,3> = 3 ; ARR.DATA<2,3> = '����� �����'
    ARR.DATA<1,4> = 4 ; ARR.DATA<2,4> = '����� ���� ����'

    RETURN
******************************************************
CLEAR.VAR:
*----------

    RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    RETURN
*========================================
PROCESS:
*-------
***************************//LD//*****************************************
    SEL.CMD  = "SELECT ":FN.LD:" WITH CATEGORY IN(":SCCD.GRP:") AND STATUS EQ 'CUR' "
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.2)
    CRT SELECTED
    LOOP
        REMOVE LD.ID FROM KEY.LIST SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        REC.COM  = R.LD<LD.CO.CODE>
        REC.CUS  = R.LD<LD.CUSTOMER.ID>
        REC.CAT  = R.LD<LD.CATEGORY>
        REC.CUR  = R.LD<LD.CURRENCY>
        REC.AMT  = R.LD<LD.AMOUNT>
        REC.VAL  = R.LD<LD.VALUE.DATE>
        REC.LCL  = R.LD<LD.LOCAL.REF>
        REC.FLG  = REC.LCL<1,LDLR.CHK.FLG>
        REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
        IF REC.QTY EQ '' THEN REC.QTY = 1
        REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
*Line [ 107 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
        IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
            REC.TYP = "EGP-1000-3M-60M-SUEZ"
        END
*    IF REC.TYP EQ "EGP-10-60M-SUEZ.BNK" THEN
*        REC.TYP = "EGP-10-60M-SUEZ"
*    END
*    IF REC.TYP EQ "EGP-100-60M-SUEZ.BNK" THEN
*        REC.TYP = "EGP-100-60M-SUEZ"
*    END
*    IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
*        REC.TYP = "EGP-1000-3M-60M-SUEZ"
*    END
        CALL F.READ(FN.CU,REC.CUS,R.CU,F.CU,ERR.CU)
        CU.LCL = R.CU<EB.CUS.LOCAL.REF>
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
        IF REC.FLG EQ '' OR REC.FLG NE '�����' THEN
            IF REC.SEC EQ '4650' THEN
                REC.FLG = '�����'
            END ELSE
                REC.FLG = '���� ��������'
            END
        END
        REC.BNK = CU.LCL<1,CULR.CU.BANK>
        IF REC.BNK NE '0017' AND REC.BNK NE '' THEN
            REC.BNK = '999999'
        END ELSE
            REC.BNK = '0017'
        END
*===========================================
        GOSUB FILL.ARR
*===========================================
    REPEAT
    ARR.REC = SORT(ARR.REC)
    MXX1 = DCOUNT(ARR.REC,@FM)
    FOR AAA1 = 1 TO MXX1
        GOSUB RET.REC
    NEXT AAA1
    RETURN
*****************************************************************
FILL.ARR:
*---------
    KK++
    ARR.REC<KK> = REC.BNK:"*":REC.FLG:"*":REC.TYP:"*":REC.VAL:"*":REC.AMT:"*":REC.QTY:"*":REC.CAT:"*":REC.CUS:"*":REC.COM
    RETURN
*****************************************************************
RET.REC:
********
    Y.RET.DATA<-1>= ARR.REC<AAA1>
    RETURN
*************************************************
END
