* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>56</Rating>
*-----------------------------------------------------------------------------
**************************MUHAMMAD-ELSAYED-21/9/2014********************************************

*****************************************************************************
    SUBROUTINE E.NOF.CUS.BNK(Y.RET.DATA)


* PROGRAM E.NOF.CUS.BNK

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

**------------------------------------
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 58 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB CHK.AC
    GOSUB LD.IND.REC
*-------------------------------------------------------------------------
    RETURN
*==============================================================
INITIATE:
    CUST.NAME        = ''
    CUST.NAME1       = ''
    BEN.CUS.NU       = ''
    CONF.INST        = ''
    ARABIC.NAME      = ''
    TD1 = TODAY

    RETURN
*===============================================================
CALLDB:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC = '' ; R.CU.AC = ''
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ER.LD = ''
*Index file for LD's , LG's and MM's
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CU.AC,F.CU.AC)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
*========================================================================
    T.SEL = "SELECT FBNK.CUSTOMER WITH BANK NE '0017' AND BANK NE '' AND POSTING.RESTRICT LT 90"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    ENT.NO = ''
    LOOP

        REMOVE CU.ID  FROM KEY.LIST SETTING POS
    WHILE CU.ID:POS
        CALL F.READ (FN.CU, CU.ID, R.CU, F.CU, E1)
        COMP.BOOK    = R.CU<EB.CUS.COMPANY.BOOK>[2] +0
        CU.AC.OFFICER = R.CU<EB.CUS.ACCOUNT.OFFICER>
        WS.CU.NAME = R.CU<EB.CUS.LOCAL.REF>
        WS.CU.NAME = WS.CU.NAME<1,CULR.ARABIC.NAME>
        CU.NUMBER         = CU.ID
        IF COMP.BOOK NE CU.AC.OFFICER THEN
            Y.RET.DATA <-1>  = CU.ID :"*": WS.CU.NAME :"*":R.CU<EB.CUS.COMPANY.BOOK> :"*":R.CU<EB.CUS.ACCOUNT.OFFICER>:"*":"CAS1"
        END

    REPEAT

    RETURN
*----------------------------------------------------------------
CHK.AC:
    R.CU      = ''
    COMP.BOOK = ''
    IF SELECTED THEN
        FOR IAC = 1 TO SELECTED
            WS.CU.ID = KEY.LIST<IAC>
            CALL F.READ(FN.CU.AC,WS.CU.ID,R.CU.AC,F.CU.AC,ER.CU.AC)
            LOOP
                REMOVE WS.AC.ID FROM R.CU.AC SETTING POS
            WHILE WS.AC.ID:POS
                CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,ER.AC)
                CO.CODE    = R.AC<AC.CO.CODE>[2] +0
                AC.OFFICER = R.AC<AC.ACCOUNT.OFFICER>
                WS.NAME.TITLE = R.AC<AC.LOCAL.REF>
                WS.NAME.TITLE = WS.NAME.TITLE<1,ACLR.ARABIC.TITLE>

                IF CO.CODE NE AC.OFFICER THEN
                    Y.RET.DATA <-1>  = WS.AC.ID :"*": WS.NAME.TITLE :"*": CO.CODE :"*": AC.OFFICER:"*":"CAS3"
                END

                CALL F.READ (FN.CU, WS.CU.ID, R.CU, F.CU, E1)
                COMP.BOOK    = R.CU<EB.CUS.COMPANY.BOOK>[2] +0

                IF CO.CODE NE COMP.BOOK THEN
                    Y.RET.DATA <-1>  =   WS.AC.ID :"*": WS.NAME.TITLE :"*": R.AC<AC.CO.CODE> :"*": R.CU<EB.CUS.COMPANY.BOOK> :"*":"CAS2"
                END

            REPEAT
        NEXT IAC
    END
    RETURN
*----------------------------------------------------------------
LD.IND.REC:
*--------
    R.CU = ''
    WS.CU.CODE = ''
    IF SELECTED THEN
        FOR ILD = 1 TO SELECTED
            WS.CU.ID = KEY.LIST<ILD>
            CALL F.READ (FN.CU, WS.CU.ID, R.CU, F.CU, E1)
            WS.CU.CODE =  R.CU<EB.CUS.COMPANY.BOOK>[2] +0
            CALL F.READ(FN.IND.LD,WS.CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
            LOOP
                REMOVE WS.LD.ID FROM R.IND.LD SETTING POS.LD
            WHILE WS.LD.ID:POS.LD
                IF WS.LD.ID[1,2] EQ 'LD' THEN
                    GOSUB LD.REC
                END
            REPEAT
        NEXT ILD
    END
    RETURN
*----------------------------------------------------------------
LD.REC:
*--------

    CALL F.READ(FN.LD,WS.LD.ID,R.LD,F.LD,ER.LD)
    REC.STA     = R.LD<LD.STATUS>
    IF REC.STA NE 'LIQ' THEN
        WS.GL.ID = R.LD<LD.CATEGORY>
        WS.CY.ID = R.LD<LD.CURRENCY>
        WS.LD.NAME = R.LD<LD.LOCAL.REF,LDLR.IN.RESPECT.OF>
        WS.LD.CODE = R.LD<LD.CO.CODE>[2] + 0
        IF WS.GL.ID EQ 21103  THEN
            IF WS.LD.CODE NE WS.CU.CODE THEN
                Y.RET.DATA <-1>  =   WS.LD.ID :"*": WS.LD.NAME :"*": R.LD<LD.CO.CODE> :"*": R.CU<EB.CUS.COMPANY.BOOK> :"*":"CAS4"
            END
        END
    END
    RETURN
*----------------------------------------------------------------
    RETURN
END
