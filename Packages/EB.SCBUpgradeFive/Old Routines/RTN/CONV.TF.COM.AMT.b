* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE CONV.TF.COM.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.ACCOUNT.BALANCES


    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL F.READ(FN.ACC,O.DATA,R.ACC,F.ACC,E1)

*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.CHARGE = DCOUNT(R.ACC<LCAC.CHRG.CODE>,@VM)

****START OF THE YEAR****
    DAT.YE = TODAY[1,4]
    DAT.ALL =DAT.YE:'0101'

    FOR I = 1 TO DECOUNT.CHARGE
        CHARGE.DAT.REC =  R.ACC<LCAC.DATE.RECEIVED><1,I>
        CHARGE.CODE      = R.ACC<LCAC.CHRG.CODE><1,I>
        CHARGE.AMOUNT    = R.ACC<LCAC.AMT.REC><1,I>
        CHARGE.AMT.LCY   = R.ACC<LCAC.CHRG.LCCY.AMT><1,I>
**      CHARGE.CURR      = R.ACC<LCAC.CHRG.CCY><1,I>

        IF CHARGE.DAT.REC GE DAT.ALL THEN
**       TOT +=   CHARGE.AMOUNT
            TOT.LCY += CHARGE.AMT.LCY
        END
** TOT.ALL += CHARGE.AMOUNT
        TOT.ALL.LCY += CHARGE.AMT.LCY
    NEXT I
    O.DATA = TOT.LCY:'-':TOT.ALL.LCY
    RETURN
END
