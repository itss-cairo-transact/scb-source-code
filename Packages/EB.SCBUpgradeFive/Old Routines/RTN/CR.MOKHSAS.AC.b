* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    PROGRAM CR.MOKHSAS.AC

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.LD.LOANS.AND.DEPOSITS
    $INSERT T24.BP I_F.LETTER.OF.CREDIT
    $INSERT T24.BP I_F.MM.MONEY.MARKET
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.DRAWINGS
    $INSERT           I_F.SCB.FUND
    $INSERT           I_FT.LOCAL.REFS
    $INSERT           I_F.SCB.CUS.POS.TST
    $INSERT           I_CU.LOCAL.REFS
*--------------------------------------------------------*
    GOSUB INIT
    GOSUB AC.SUB
INIT:
*-----
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''
    CALL OPF( FN.ACCOUNT,F.ACCOUNT)

    RETURN
*-----------------------------*
AC.SUB:
*------
    T.SEL  = "SELECT FBNK.ACCOUNT WITH OPEN.ACTUAL.BAL NE ''"
    T.SEL := " AND CATEGORY NE 1701 "
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACCOUNT,KEY.LIST<I>, R.ACCOUNT,F.ACCOUNT, ETEXT)
            CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.ACCOUNT<AC.CURRENCY>,CURR.NO)
            ID.NO = R.ACCOUNT<AC.CUSTOMER>:"*AC*BNK*":CURR.NO:R.ACCOUNT<AC.CATEGORY>:"*":KEY.LIST<I>:"*":TODAY

            CALL F.READ(FN.CUSS,R.ACCOUNT<AC.CUSTOMER>, R.CUSS,F.CUSS, ETEXT1)

            CURR = R.ACCOUNT<AC.CURRENCY>
            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100

            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            AMT = R.ACCOUNT<AC.OPEN.ACTUAL.BAL>

            LCY.AMT  = (MID.RATE<1,1> * AMT)
            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>  = LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER>    = R.ACCOUNT<AC.CUSTOMER>
            R.COUNT<CUPOS.DEAL.CCY>    = R.ACCOUNT<AC.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT> = R.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            R.COUNT<CUPOS.CATEGORY>    = R.ACCOUNT<AC.CATEGORY>
            R.COUNT<CUPOS.SYS.DATE>    = TODAY
            R.COUNT<CUPOS.CO.CODE>     = R.ACCOUNT<AC.CO.CODE>

            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
    RETURN
END
