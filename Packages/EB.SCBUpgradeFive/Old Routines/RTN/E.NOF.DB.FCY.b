* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 24/03/2011******************
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.DB.FCY(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    TOT.TT = 0 ; TOT.AMT = 0

    YTEXT = "Enter the Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI

    YTEXT = "Enter the End Date  : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI

**  N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CURRENCY.1 EQ 'USD' AND CUSTOMER.1 NE '' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE GE '20110125' BY CUSTOMER.1 BY AUTH.DATE "
    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND (CURRENCY.1 NE 'USD' AND CURRENCY.1 NE 'EGP') AND CUSTOMER.1 NE '' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CUSTOMER.1 BY AUTH.DATE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM

    FOR I = 1 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>     = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>     = R.TT<TT.TE.CURRENCY.1>
        AMT<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AUTH.DAT<I> = R.TT<TT.TE.AUTH.DATE>
        Y.RET.DATA<-1> = CUST<I>:"*":AMT<I>:"*":AUTH.DAT<I>:"*":KEY.LIST.N<I>:"*":CURR<I>
    NEXT I

    RETURN
END
