* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
**************************************MAHMOUD 9/11/2015*******************************************
    SUBROUTINE E.NOF.DEPO.BAL.TD.VAR(REC.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 41 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    K  = 0
    K2 = 0
    DDY.NO = 0
    TDD1 = TODAY
    FROM.DATE = TDD1
    END.DATE  = TDD1
    KEY.LIST  = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2 = '' ; SELECTED2 = '' ; ERR.2 = ''
    PERS.DR = ''
    PERS.CR = ''
    COMP.DR = ''
    COMP.CR = ''
    CURR.CODE = 'EGP'
    REC.TITLE = ''
    REC.TITLE<1> = '����'
    REC.TITLE<2> = '�����'
    REC.TITLE<3> = '�����'
    REC.TITLE<4> = '������'
    YLL = DCOUNT(REC.TITLE,@FM)
    FOR XXY = 1 TO YLL
        PERS.DR<XXY>= 0
        PERS.CR<XXY>= 0
        COMP.DR<XXY>= 0
        COMP.CR<XXY>= 0
    NEXT XXY

*========================================
CALLDB:
*-------
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC = '' ; R.CU.AC = ''
    CALL OPF(FN.CU.AC,F.CU.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ENT = 'FBNK.ACCT.ENT.TODAY' ; F.ENT = '' ; R.ENT = ''
    CALL OPF(FN.ENT,F.ENT)
    FN.LMM.CU = 'FBNK.LMM.CUSTOMER' ; F.LMM.CU = '' ; R.LMM.CU = ''
    CALL OPF(FN.LMM.CU,F.LMM.CU)

    RETURN
*========================================
PROCESS:
*-------
*----------------------------------AC REC---------------------------------------------
    SEL.ACC = " SELECT FBNK.ACCOUNT WITH ( CATEGORY IN( 1001 1002 1005 2006 ) OR CATEGORY LIKE 65... ) AND CURRENCY EQ ":CURR.CODE
    CALL EB.READLIST(SEL.ACC,K.LIST,'',SELECTED,ERR.MSG)
    CRT SELECTED
    LOOP
        REMOVE ACC.ID FROM K.LIST SETTING POSS.ACC
    WHILE ACC.ID:POSS.ACC
        K++
        CALL F.READ(FN.AC,ACC.ID,R.AC,F.AC,ERR.1)
        REC.CAT = R.AC<AC.CATEGORY>
        REC.CUR = R.AC<AC.CURRENCY>
        REC.ONL.BAL = R.AC<AC.ONLINE.ACTUAL.BAL>
        REC.PRV.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
        CU.ID = R.AC<AC.CUSTOMER>
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CU.ID,CU.LCL)
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
        IF REC.SEC EQ '4650' THEN
            IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' OR REC.CAT EQ '1002' THEN
                PERS.CR<1> += REC.ONL.BAL
                PERS.DR<1> += REC.PRV.BAL
            END ELSE
                IF REC.CAT EQ '6512' THEN
                    PERS.CR<3> += REC.ONL.BAL
                    PERS.DR<3> += REC.PRV.BAL
                END ELSE
                    PERS.CR<2> += REC.ONL.BAL
                    PERS.DR<2> += REC.PRV.BAL
                END
            END
        END ELSE
            IF REC.CAT EQ '1001' OR REC.CAT EQ '1005' OR REC.CAT EQ '2006' OR REC.CAT EQ '1002' THEN
                COMP.CR<1> += REC.ONL.BAL
                COMP.DR<1> += REC.PRV.BAL
            END ELSE
                IF REC.CAT EQ '6512' THEN
                    COMP.CR<3> += REC.ONL.BAL
                    COMP.DR<3> += REC.PRV.BAL
                END ELSE
                    COMP.CR<2> += REC.ONL.BAL
                    COMP.DR<2> += REC.PRV.BAL
                END
            END
        END
    REPEAT
*----------------------------------LD REC---------------------------------------------
*    SEL.CMD2  = "SELECT ":FN.LD:" WITH CUSTOMER.ID NE ''"
*    SEL.CMD2 := " AND CURRENCY EQ ":CURR.CODE
*    SEL.CMD2 := " AND (( VALUE.DATE LE ":TDD1:" AND AMOUNT NE 0 )"
*    SEL.CMD2 := "  OR  ( FIN.MAT.DATE GE ":TDD1:" AND AMOUNT EQ 0 ))"
*    SEL.CMD2 := " AND (( CATEGORY GE 21001 AND CATEGORY LE 21010 )"
*    SEL.CMD2 := "  OR  ( CATEGORY GE 21017 AND CATEGORY LE 21029 )"
*    SEL.CMD2 := "  OR  ( CATEGORY EQ 21041 OR CATEGORY EQ 21042 OR CATEGORY EQ 21013 ))"
    SEL.CMD2 = " SELECT FBNK.LMM.CUSTOMER "

    CALL EB.READLIST(SEL.CMD2,KEY.LIST2,'',SELECTED2,ERR.2)
    CRT SELECTED2
    LOOP
        REMOVE CU.ID FROM KEY.LIST2 SETTING POSS2
    WHILE CU.ID:POSS2
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CU.ID,CU.LCL)
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
        CALL F.READ(FN.LMM.CU,CU.ID,R.LMM.CU,F.LMM.CU,ERR.12)
        LOOP
            REMOVE LD.ID FROM R.LMM.CU SETTING POSS22
        WHILE LD.ID:POSS22
            IF LD.ID[1,2] EQ "LD" THEN
                K2++
                CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
                LD.CAT  = R.LD<LD.CATEGORY>
                LD.CUR  = R.LD<LD.CURRENCY>
                IF LD.CUR EQ 'EGP' THEN
                    IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT GE 21017 AND LD.CAT LE 21029 ) OR ( LD.CAT EQ 21013 OR LD.CAT EQ 21041 OR LD.CAT EQ 21042 ) THEN
                        LD.COM  = R.LD<LD.CO.CODE>
                        LD.VAL  = R.LD<LD.VALUE.DATE>
                        LD.STA  = R.LD<LD.STATUS>
                        LD.FIN  = R.LD<LD.FIN.MAT.DATE>
                        LD.LCL  = R.LD<LD.LOCAL.REF>
                        LD.REN  = LD.LCL<1,LDLR.RENEW.IND>
                        LD.OLD  = LD.LCL<1,LDLR.OLD.NO>
                        LD.BAL  = R.LD<LD.AMOUNT>
                        LD.CUS = R.LD<LD.CUSTOMER.ID>
*************************************
**??                    IF LD.FIN GT TDD1 AND LD.VAL LE TDD1 AND LD.STA EQ 'CUR' THEN
                        IF REC.SEC EQ '4650' THEN
                            IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                                PERS.CR<3> += LD.BAL
                            END ELSE
                                PERS.CR<4> += LD.BAL
                            END
                        END ELSE
                            IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                                COMP.CR<3> += LD.BAL
                            END ELSE
                                COMP.CR<4> += LD.BAL
                            END
                        END
**??                    END
                        CALL DBR('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.FIN.MAT.DATE,LD.ID:';1',LD.FIRST.FIN)
**??                        IF (LD.STA EQ 'CUR' AND LD.VAL LT TDD1 ) OR ( LD.STA EQ 'LIQ' AND LD.FIN EQ TDD1 AND LD.VAL LT TDD1 AND ((LD.REN NE 'YES') OR ( LD.FIRST.FIN NE LD.FIN ))) THEN
                        IF LD.VAL LT TDD1 AND LD.FIN GE TDD1 THEN
                            IF LD.BAL EQ 0 THEN
                                LD.BAL  = R.LD<LD.REIMBURSE.AMOUNT>
                            END
                            IF REC.SEC EQ '4650' THEN
                                IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                                    PERS.DR<3> += LD.BAL
                                END ELSE
                                    PERS.DR<4> += LD.BAL
                                END
                            END ELSE
                                IF (LD.CAT GE 21001 AND LD.CAT LE 21010) OR ( LD.CAT EQ 21013 ) THEN
                                    COMP.DR<3> += LD.BAL
                                END ELSE
                                    COMP.DR<4> += LD.BAL
                                END
                            END
                        END
                    END
                END
            END
        REPEAT
    REPEAT
    GOSUB PRINT.REC
    RETURN
*========================================
PRINT.REC:
*----------
    FOR REP.NO = 1 TO 2
        IF REP.NO EQ 2 THEN
            PERS.DR<1>= 205027467.30
            PERS.DR<2>= 1558238108.68
            PERS.DR<3>= 576053929.46
            PERS.DR<4>= 2208596000
            COMP.DR<1>= 2928601315.45
            COMP.DR<2>= 0
            COMP.DR<3>= 5284787643.97
            COMP.DR<4>= 0
        END

        LL = DCOUNT(REC.TITLE,@FM)
        FOR XX1 = 1 TO LL
            REC.DATA<-1>=REP.NO:"*":REC.TITLE<XX1>:"*":PERS.DR<XX1>:"*":PERS.CR<XX1>:"*":COMP.DR<XX1>:"*":COMP.CR<XX1>
        NEXT XX1
    NEXT REP.NO
    RETURN
*========================================
END
