* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 21/01/2015******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CR.EQV.USD.100000(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0

* YTEXT = "����� ����� ����� ������� : "
    YTEXT = "���� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
* YTEXT = "���� ����� ����� ������� : "
* CALL TXTINP(YTEXT, 8, 22, "12", "A")
* EN.DATE = COMI
* TEXT = "EN.DATE=":EN.DATE ; CALL REM

*    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND CONTRACT.GRP EQ '' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CUSTOMER.1 BY AUTH.DATE "
    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE EQ ":ST.DATE :" BY CUSTOMER.1 "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

*    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    CUST<1>         = R.TT<TT.TE.CUSTOMER.1>
    CURR<1>         = R.TT<TT.TE.CURRENCY.1>
    AMT.FCY<1>      = R.TT<TT.TE.AMOUNT.FCY.1>
    AMT.LCY<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1>     = R.TT<TT.TE.AUTH.DATE>
    TT.DATE = AUTH.DAT<1>
*DAY.NUM = ICONV(TT.DATE,"DW")
*DAY.NUM2= OCONV(DAY.NUM,"DW")
    KEY.ID<1> = "USD":"-" :"EGP":"-":TT.DATE
    CALL F.READ(FN.CURR.DALY,KEY.ID<1>,R.CURR.DALY,F.CURR.DALY,ERR2)
    DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
    CURR.MARK = R.CURR.DALY<SCCU.CURR.MARKET>
    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
        SELL.RATE<1> = R.CURR.DALY<SCCU.SELL.RATE ,NN>
    END
    IF CURR<1> = "USD" THEN
        TOT.AMT.N = TOT.AMT.N + AMT.FCY<1>
    END ELSE
****OTHER FCY CURR*******************************
        AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
        TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
    END
**********END OF FIRST RECORD**********************************
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        TT.DATE = AUTH.DAT<I>
**       DAY.NUM = ICONV(TT.DATE,"DW")
**       DAY.NUM2= OCONV(DAY.NUM,"DW")
        KEY.ID<I> = "USD":"-" :"EGP":"-":TT.DATE
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
        LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
            SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
        END
        IF CUST<I> # CUST<I-1> THEN
            IF  TOT.AMT.N > 100000 THEN
                CALL F.READ(FN.CUSTOMER, CUST<I-1>, R.CUSTOMER, F.CUSTOMER, E1)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NSN = LOCAL.REF<1,CULR.NSN.NO>
                REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
                Y.RET.DATA<-1> = CUST<I-1> :"*": CURR<I-1>:"*":REG.NO:"*":TOT.AMT.N
                TOT.AMT.N = 0 ; REG.NO = ''
            END ELSE
                TOT.AMT.N = 0
            END
            IF CURR<I> = "USD" THEN
                    TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
            END ELSE
                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                END
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
            END     ;** END OF OTHER FCY**
*****SAME CUSTOMER*************
        END ELSE
            IF CURR<I> = "USD" THEN
                    TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
            END ELSE
****OTHER FCY**********
                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                END
                    AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                    TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
            END     ;**END OF OTHER FCY**
*****END OF SAME CUSTOMER******
        END         ;**END OF SAME CUSTOMER**
        IF I = SELECTED.N THEN
         *   TEXT = 'LAST RECORD' ; CALL REM
            IF TOT.AMT.N > 100000 THEN
                CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NSN = LOCAL.REF<1,CULR.NSN.NO>
                REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
                Y.RET.DATA<-1> = CUST<I> :"*": CURR<I>:"*":REG.NO:"*":TOT.AMT.N
            END
        END
    NEXT I
    RETURN
END
