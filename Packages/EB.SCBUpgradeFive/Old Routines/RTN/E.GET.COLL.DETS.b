* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.GET.COLL.DETS
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL

    O.DATA = ""
    FN.RIGHT.COLLATERAL = "F.RIGHT.COLLATERAL"
    FV.RIGHT.COLLATERAL = ""
    CALL OPF(FN.RIGHT.COLLATERAL,FV.RIGHT.COLLATERAL)

    FN.COLLATERAL = "F.COLLATERAL"
    FV.COLLATERAL = ""
    CALL OPF(FN.COLLATERAL,FV.COLLATERAL)

    NO.OF.COLLS = 0
*Line [ 41 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    NO.OF.COLL.RIGHTS = DCOUNT(R.RECORD<LI.COLLAT.RIGHT,1>,@SM)
    FOR COLL.RIGHT.IND = 1 TO NO.OF.COLL.RIGHTS
        COLL.RIGHT.ID = R.RECORD<LI.COLLAT.RIGHT,1,COLL.RIGHT.IND>
        CALL F.READ(FN.RIGHT.COLLATERAL,COLL.RIGHT.ID,R.RIGHT.COLLATERAL,FV.RIGHT.COLLATERAL,"")
*Line [ 46 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        NO.OF.COLLATERALS = DCOUNT(R.RIGHT.COLLATERAL,@FM)
        FOR COLL.IND = 1 TO NO.OF.COLLATERALS
            COLL.ID = R.RIGHT.COLLATERAL<COLL.IND>
            CALL F.READ(FN.COLLATERAL,COLL.ID,R.COLLATERAL,FV.COLLATERAL,"")
            NO.OF.COLLS += 1
            O.DATA<1,NO.OF.COLLS> = COLL.ID:"|":R.COLLATERAL<COLL.NOMINAL.VALUE>:"|":R.COLLATERAL<COLL.EXECUTION.VALUE>
        NEXT COLL.IND
    NEXT COLL.RIGHT.IND

*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    @VM.COUNT = NO.OF.COLLS

    RETURN
END
