* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>472</Rating>
*-----------------------------------------------------------------------------
    PROGRAM DEL.NO.ACTIVE.CUST

****Mahmoud Elhawary******28/1/2009*************************
*delete records from AC.STMT.HANDOFF for inactive Customers*
************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.STMT.HANDOFF


    GOSUB INITIAL
    GOSUB CALL.DB
    GOSUB PROCESS
    RETURN
*******************************************
CALL.DB:
*-------
    FN.SHO = 'FBNK.AC.STMT.HANDOFF'
    F.SHO = ""
    R.SHO = ""
    CALL OPF(FN.SHO,F.SHO)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ""
    R.ACC = ""
    CALL OPF(FN.ACC,F.ACC)
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUST  = ""
    CALL OPF(FN.CUST,F.CUST)
    RETURN
*****************************************
INITIAL:
*-------
    K.LIST ="" ; SELECTED ="" ; ER.MSG =""
    C.LIST ="" ; SELECTED.C = "" ; CUS.LIST = ""
    TTY = TODAY[1,6]:"01"
    CALL CDT("",TTY,'-1W')
    TTX = '.':TTY[1,6]
    CRT TTY
    CRT TTX
    RETURN
************************************************
PROCESS:
*--------
    T.SEL  ="SELECT ":FN.SHO:" WITH TO.DATE EQ ":TTY
    T.SEL :=" BY CUSTOMER"
    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CRT @(10,10):SELECTED
            CRT @(10,11):I
            CRT @(10,12):KK1
            CR.CUST = ""
            DR.CUST = ""
            CR.BANK = ""
            DR.BANK = ""
            OPEN.BAL= 0
            AC.ID = K.LIST<I>
            CALL F.READ(FN.SHO,AC.ID,R.SHO,F.SHO,ER.MSG1)
            CUST   = R.SHO<AC.STH.CUSTOMER>
            CATEG  = R.SHO<AC.STH.ACCT.CATEGORY>
            OP.DT  = R.SHO<AC.STH.OPENING.DATE>
            SHO.CO = R.SHO<AC.STH.CO.CODE>

            IF SHO.CO EQ 'EG0010099' THEN
                KK1++
                GOSUB DEL.REC
                GOTO NXT.REC
            END

            IF CUST EQ "" THEN
                KK1++
                GOSUB DEL.REC
                GOTO NXT.REC
            END

            IF CATEG NE '1001' AND CATEG NE '1003' AND CATEG NE '1006' AND CATEG NE '2000' AND CATEG NE '2001' AND CATEG NE '6501' AND CATEG NE '6502' AND CATEG NE '6503' AND CATEG NE '6504' AND CATEG NE '6511' AND CATEG NE '6512' THEN
                IF CATEG LT '1101' OR CATEG GT '1599' THEN
                    KK1++
                    GOSUB DEL.REC
                    GOTO NXT.REC
                END
            END

*********CUST DATA
            CALL F.READ(FN.CUST,CUST,R.CUST,F.CUST,ERR1.CUST)
            CUS.POST = R.CUST<EB.CUS.POSTING.RESTRICT>
            CUST.SEC = R.CUST<EB.CUS.SECTOR>
            CUST.LCL = R.CUST<EB.CUS.LOCAL.REF>
            ACT.CUST = CUST.LCL<1,CULR.ACTIVE.CUST>
            CR.STAT  = CUST.LCL<1,CULR.CREDIT.STAT>
            CR.CODE  = CUST.LCL<1,CULR.CREDIT.CODE>
            CUST.GOV = CUST.LCL<1,CULR.GOVERNORATE>

*********ACCT DATA
            ACC.ID = FIELD(AC.ID,'.',1)
            CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ER.ACC1)
            CR.CUST = R.ACC<AC.DATE.LAST.CR.CUST>
            DR.CUST = R.ACC<AC.DATE.LAST.DR.CUST>
            CR.BANK = R.ACC<AC.DATE.LAST.CR.BANK>
            DR.BANK = R.ACC<AC.DATE.LAST.DR.BANK>
            OPEN.BAL= R.ACC<AC.OPEN.ACTUAL.BAL>
            AC.OPEN = R.ACC<AC.OPENING.DATE>
            AC.COMPANY = R.ACC<AC.CO.CODE>
*********
            IF OP.DT EQ "" THEN
                OP.DT = AC.OPEN
            END

            IF CUS.POST GT 89 THEN
                KK1++
**                DELETE F.SHO,K.LIST<I>
                GOSUB DEL.REC
                GOTO NXT.REC
            END
            IF OPEN.BAL EQ 0 THEN
                IF CR.CUST LT OP.DT AND DR.CUST LT OP.DT AND CR.BANK LT OP.DT AND DR.BANK LT OP.DT THEN
                    KK1++
                    GOSUB DEL.REC
                    GOTO NXT.REC
                END
            END
            IF OPEN.BAL EQ "" THEN
                KK1++
                GOSUB DEL.REC
                GOTO NXT.REC
            END
            IF CR.STAT NE "" THEN
                KK1++
                GOSUB DEL.REC
                GOTO NXT.REC
            END
            IF CR.CODE EQ '110' OR CR.CODE EQ '120' THEN
                KK1++
                GOSUB DEL.REC
                GOTO NXT.REC
            END

***********EMP.CUST
            IF CUST.SEC EQ 1100 THEN
                IF CUST.GOV EQ '98' THEN
                    KK1++
                    CRT @(10,15):CUST
                    GOSUB DEL.REC
                    GOTO NXT.REC
                END
            END
***********
NXT.REC:
        NEXT I
    END
    RETURN
*************************************************
DEL.REC:
*-------
    DELETE F.SHO,K.LIST<I>
**    CALL F.DELETE(FN.SHO,K.LIST<I>)
**    CALL JOURNAL.UPDATE(K.LIST<I>)
    RETURN
*************************************************
END
