* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***************************MAHMOUD 17/9/2014******************************
*-----------------------------------------------------------------------------
* <Rating>2309</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CBE.SCCD.GEO(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
*Line [ 42 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    KK = 0
    K2 = 0
***********************
    DYNO = 0
    TDD = TODAY
    TD1 = TODAY
    DYNO = FMT(ICONV(TD1,'D'),'DW')
    TDYNO = '-':DYNO:'C'
    CALL CDT('',TD1,TDYNO)
    CRT TD1
***********************
    ARR.REC  = ''
    ARR.DATA = ''
    REC.GEO  = ''
    REC.QTY = 0
    REC.AMT = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

    GRAND.CAIRO.GRP = " EG0010001 EG0010002 EG0010003 EG0010004 EG0010005 EG0010006 EG0010007 EG0010009 EG0010010 EG0010011 EG0010012 EG0010013 EG0010014 EG0010015 EG0010016 "
    LOWER.EGYPT.GRP = " EG0010020 EG0010021 EG0010022 EG0010023 EG0010031 EG0010032 EG0010060 EG0010070 EG0010090 "
    UPPER.EGYPT.GRP = " EG0010080 EG0010081 "
    SUEZ.CANAL.GRP  = " EG0010030 EG0010040 EG0010050 EG0010051 "
    OTHER.GRP       = " EG0010035 "
    RETURN
******************************************************
CLEAR.VAR:
*----------

    RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    RETURN
*========================================
PROCESS:
*-------
***************************//LD//*****************************************
    SEL.CMD  = "SELECT ":FN.LD:" WITH CATEGORY IN(":SCCD.GRP:") AND STATUS EQ 'CUR' "
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.2)
    CRT SELECTED
    LOOP
        REMOVE LD.ID FROM KEY.LIST SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        REC.COM  = R.LD<LD.CO.CODE>
*Line [ 102 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR REC.COM IN GRAND.CAIRO.GRP SETTING POS.GEO1 THEN REC.GEO = 1 ELSE NULL
*Line [ 104 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR REC.COM IN LOWER.EGYPT.GRP SETTING POS.GEO2 THEN REC.GEO = 2 ELSE NULL
*Line [ 106 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR REC.COM IN UPPER.EGYPT.GRP SETTING POS.GEO3 THEN REC.GEO = 3 ELSE NULL
*Line [ 108 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR REC.COM IN SUEZ.CANAL.GRP  SETTING POS.GEO4 THEN REC.GEO = 4 ELSE NULL
*Line [ 110 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR REC.COM IN OTHER.GRP       SETTING POS.GEO5 THEN REC.GEO = 5 ELSE NULL
        REC.CUS  = R.LD<LD.CUSTOMER.ID>
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,REC.CUS,CU.LCL)
        REC.BNK = CU.LCL<1,CULR.CU.BANK>
        IF REC.BNK NE '0017' AND REC.BNK NE '' THEN
            GOTO NXT.REC
        END ELSE
            REC.BNK = '0017'
        END
        REC.CAT  = R.LD<LD.CATEGORY>
        REC.CUR  = R.LD<LD.CURRENCY>
        REC.AMT  = R.LD<LD.AMOUNT>
        REC.VAL  = R.LD<LD.VALUE.DATE>
        REC.LCL  = R.LD<LD.LOCAL.REF>
        REC.FLG  = REC.LCL<1,LDLR.CHK.FLG>
        REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
        IF REC.QTY EQ '' THEN REC.QTY = 1
        REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
*Line [ 129 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
        IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
            REC.TYP = "EGP-1000-3M-60M-SUEZ"
        END 
*===========================================
        GOSUB FILL.ARR
*===========================================
NXT.REC:
    REPEAT
    MXX1 = DCOUNT(ARR.REC,@FM)
    FOR AAA1 = 1 TO MXX1
        REC.GEO = AAA1
        GOSUB RET.REC
    NEXT AAA1
    RETURN
*****************************************************************
FILL.ARR:
*---------
    ARR.REC<REC.GEO,1> = REC.GEO
    ARR.REC<REC.GEO,2> += REC.AMT
    RETURN
*****************************************************************
RET.REC:
********
*Line [ 154 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.GEO,1> = 1 THEN GEO.NAME = '������� ������' ELSE NULL
*Line [ 156 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.GEO,1> = 2 THEN GEO.NAME = '��� ����' ELSE NULL
*Line [ 158 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.GEO,1> = 3 THEN GEO.NAME = '��� ����' ELSE NULL
*Line [ 160 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.GEO,1> = 4 THEN GEO.NAME = '��� ������' ELSE NULL
*Line [ 162 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF ARR.REC<REC.GEO,1> = 5 THEN GEO.NAME = '����' ELSE NULL

    Y.RET.DATA<-1>= ARR.REC<REC.GEO,1>:"*":GEO.NAME:"*":ARR.REC<REC.GEO,2>

    RETURN
*************************************************
END
