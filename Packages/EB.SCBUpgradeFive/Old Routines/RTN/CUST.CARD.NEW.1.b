* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOO---------------------------------------------
    SUBROUTINE CUST.CARD.NEW.1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
** $INCLUDE GLOBUS.BP I_F.CARD.ISSUE.ACCOUNT


    FN.CUS='FBNK.CUSTOMER';F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

    FN.AC='FBNK.ACCOUNT';F.AC=''
    CALL OPF(FN.AC,F.AC)


    FN.CARD='FBNK.CARD.ISSUE';F.CARD=''
    CALL OPF(FN.CARD,F.CARD)

    FN.CARD.ACC='FBNK.CARD.ISSUE.ACCOUNT';F.CARD.ACC=''
    CALL OPF(FN.CARD.ACC,F.CARD.ACC)
    ERR   = ''
    ETEXT = ''
    CU = O.DATA
    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)
    CALL F.READ( FN.CUS.AC,CU, R.CUS.AC, F.CUS.AC,ETEXT1)
**-----------------------------------------------------------------**
    ACC = O.DATA
    CALL F.READ(FN.CARD.ACC,ACC,R.CARD.ACC,F.CARD.ACC,ERR)
    IF ERR THEN
        XX =  "�� ����"
    END ELSE
        XX = R.CARD.ACC[6,16]
    END
    O.DATA = XX
    RETURN
END
