* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.USD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*----------------------------------------------------
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.NUM = 'F.NUMERIC.CURRENCY' ; F.NUM = ''
    CALL OPF(FN.NUM,F.NUM)

    ENQ.DATA = O.DATA
    CURR     = FIELD(ENQ.DATA,'*',2)
    AMT      = FIELD(ENQ.DATA,'*',1)

    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,E1)
    RATE.USD = R.CUR<EB.CUR.MID.REVAL.RATE,1>

    IF CURR EQ 10 THEN

        BAL.USD =  AMT/RATE.USD
        CALL EB.ROUND.AMOUNT ('USD',BAL.USD,'',"2")
        O.DATA =  BAL.USD
    END

    IF CURR EQ 20 THEN

        CALL EB.ROUND.AMOUNT ('USD',AMT,'',"2")
        O.DATA =  AMT
    END


    IF CURR NE 20 AND CURR NE 10 THEN
        CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,E1)
        RATE.USD = R.CUR<EB.CUR.MID.REVAL.RATE,1>

        CALL F.READ(FN.NUM,CURR,R.NUM,F.NUM,E2)
        CURR.NAME = R.NUM<EB.NCN.CURRENCY.CODE>
*TEXT = "CURR.NAME ":CURR.NAME; CALL REM

        CALL F.READ(FN.CUR,CURR.NAME,R.CUR,F.CUR,E3)
        RATE.CURR = R.CUR<EB.CUR.MID.REVAL.RATE,1>
*TEXT = "RATE ":RATE.CURR; CALL REM

        BAL.EGP = AMT * RATE.CURR
        IF CURR EQ 45 THEN
            BAL.USD = BAL.EGP / (RATE.USD * 100)
        END
        ELSE
            BAL.USD = BAL.EGP /  RATE.USD
        END

        CALL EB.ROUND.AMOUNT ('USD',BAL.USD,'',"2")
        O.DATA = BAL.USD
    END

*----------------------------------------------------
    RETURN
END
