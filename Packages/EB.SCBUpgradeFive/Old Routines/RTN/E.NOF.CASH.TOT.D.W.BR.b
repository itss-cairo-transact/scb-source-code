* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***NESSREEN AHMED 4/7/2012*********************************
*-----------------------------------------------------------------------------
* <Rating>-44</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CASH.TOT.D.W.BR(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TELLER.D.W

*-----------------------------------------------------------------------*
    TD1 = ''

    KEY.LIST.W = "" ; SELECTED.W = "" ;  ER.MSG.W = ""
    KEY.LIST.D = "" ; SELECTED.D = "" ;  ER.MSG.D = ""

    FN.TT.D.W   = 'F.SCB.TELLER.D.W' ; F.TT.D.W = '' ; ERR.TT.D.W = ''
    CALL OPF(FN.TT.D.W,F.TT.D.W)

    AMT.CR   = 0
    AMT.DR   = 0  ; REQ.D = ''

    YTEXT = "Enter Required Company : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    COMP = COMI
    YTEXT = "Enter Numer of Periods : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    NO.PER = COMI
    FOR I = 1 TO NO.PER
        YTEXT = "Enter Start.Date : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        ST.D = COMI
        YTEXT = "Enter End.Date : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        ED.D = COMI

        TOT.AMT.LCY.W = '' ; TOT.AMT.LCY.D = ''
        WW.NN = 0 ; DD.NN = 0

        W.SEL = "SELECT F.SCB.TELLER.D.W WITH COMP.CO EQ " : COMP : " AND (AUTH.DATE GE " : ST.D : " AND AUTH.DATE LE " :ED.D : " ) BY AUTH.DATE "
        CALL EB.READLIST(W.SEL, KEY.LIST.W, "", SELECTED.W, ER.MSG.W)

        FOR WW = 1 TO SELECTED.W
            KEY.USE.WW = ''
            TT.R.W = '' ; REF.WW = '' ; TT.REF.W = '' ; AMT.LCY.W = ''
            CALL F.READ( FN.TT.D.W,KEY.LIST.W<WW>, R.TT.D.W, F.TT.D.W, ERR.TT.D.W)
            TR.FLAG   = R.TT.D.W<TELL.D.W.TR.FLAG>
            AMT.LCY = R.TT.D.W<TELL.D.W.AMT.LCY>
            IF TR.FLAG EQ 'D' THEN
                TOT.AMT.LCY.D = TOT.AMT.LCY.D + AMT.LCY
                DD.NN = DD.NN + 1
            END
            IF TR.FLAG EQ 'W' THEN
                TOT.AMT.LCY.W = TOT.AMT.LCY.W + AMT.LCY
                WW.NN = WW.NN + 1
            END
        NEXT WW
        Y.RET.DATA<-1> = ST.D:"*":ED.D:"*":DD.NN:"*":TOT.AMT.LCY.D:"*":WW.NN:"*":TOT.AMT.LCY.W:"*":COMP
        TOT.AMT.LCY.D = '' ; DD.NN = '' ; TOT.AMT.LCY.W = '' ; WW.NN = ''
        ST.D = '' ; ED.D = ''
    NEXT I
    RETURN
END
