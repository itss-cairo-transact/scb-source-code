* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>2677</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CREDIT.CUST.2ND(Y.ALL.REC)
***Mahmoud Elhawary******3/11/2011****************************
* Nofile routine to get credit customers                     *
* of second degree total txn                                 *
**************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 61 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS


    RETURN
****************************************
INITIATE:
*--------

    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    PREV.DAY  = FROM.DATE
    KK = 0
    II = 0
    XX = SPACE(100)
    REC.STA  = ''
    STE.BAL  = ''
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    TTD1 = FMT(TODAY,'####/##/##')

    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN
*Line [ 87 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE "CUSTOMER.ID" IN D.FIELDS<1> SETTING YCUS.POS THEN CUST.ID   = D.RANGE.AND.VALUE<YCUS.POS> ELSE NULL


    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,COMP.NAME)

    FOLDER   = "&SAVEDLISTS&"
    FILENAME = "SBR.CREDIT.CUST.2ND.CSV"
    OPENSEQ FOLDER , FILENAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER:' ':FILENAME
        HUSH OFF
    END
    OPENSEQ FOLDER , FILENAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILENAME:' CREATED IN ':FOLDER
        END ELSE
            STOP 'Cannot create ':FILENAME:' File IN ':FOLDER
        END
    END

    HEAD.DESC  = "�����":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� �������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "������ �������":","
    HEAD.DESC := "������ �������":","
    HEAD.DESC := "���� ���� ����":","
    HEAD.DESC := "���� ���� ����":","
    HEAD.DESC := "���� �������":","
    HEAD.DESC := "���� �������":","
    HEAD.DESC := "������ �� ":FMT(FROM.DATE,'####/##/##') :" ��� ":FMT(END.DATE,'####/##/##'):" ,"
**    HEAD.DESC := "����� : ":COMP.NAME:","
    HEAD.DESC := "Report date : ":TTD1:","

    BB.DATA = HEAD.DESC

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ER.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FTH = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FTH = '' ; R.FTH = '' ; ER.FTH = ''
    CALL OPF(FN.FTH,F.FTH)
    FN.SDR = 'FBNK.STMT.ACCT.DR' ; F.SDR = ''   ; R.SDR = ''
    CALL OPF(FN.SDR,F.SDR)

    RETURN
****************************************
PROCESS:
*-------

    PREV.DAY = FROM.DATE

    CC.SEL  = "SSELECT FBNK.CUSTOMER.ACCOUNT "
***CC.SEL := " WITH COMPANY.CO EQ ":COMP
    IF CUST.ID THEN
        CC.SEL := " '":CUST.ID:"'"
    END
***CC.SEL := " BY COMPANY.CO"
    CALL EB.READLIST(CC.SEL, K.LIST, "", SELECTED, ERR.CUS)
    LOOP
        REMOVE CUS.ID FROM K.LIST SETTING POS.CUS
    WHILE CUS.ID:POS.CUS
        CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
        CUS.POST = R.CUS<EB.CUS.POSTING.RESTRICT>
        CUS.SECT = R.CUS<EB.CUS.SECTOR>
        CUS.LCL  = R.CUS<EB.CUS.LOCAL.REF>
        CUS.NAME1= CUS.LCL<1,CULR.ARABIC.NAME>
        CUS.NAME2= CUS.LCL<1,CULR.ARABIC.NAME.2>
        CUS.NAME = CUS.NAME1:" ":CUS.NAME2
        CUS.STAT = CUS.LCL<1,CULR.CREDIT.STAT>
        CUS.CRDT = CUS.LCL<1,CULR.CREDIT.CODE>
        CUS.GRP  = CUS.LCL<1,CULR.GROUP.NUM>
        CUS.NEW.SEC = CUS.LCL<1,CULR.NEW.SECTOR>
        IF CUS.POST LE 89 THEN
            IF CUS.SECT NE 1100 AND CUS.SECT NE 1200 AND CUS.SECT NE 1300 AND CUS.SECT NE 1400 THEN
                IF CUS.CRDT EQ 100 AND CUS.GRP[1,3] NE '999' THEN
                    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
                    LOOP
                        REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.ACC
                    WHILE ACCT.ID:POS.ACC

                        TOT.DR.AMT = 0
                        TOT.CR.AMT = 0
                        TOT.DR.LCY = 0
                        TOT.CR.LCY = 0
                        TOTAL.INT  = 0
                        TOTAL.COM  = 0
                        BOOK.BAL   = 0
                        MAX.BAL.DR = 0
                        MIN.BAL.DR = 0

                        CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)

                        ACCT.CAT = R.ACCT<AC.CATEGORY>
                        ACCT.CUR = R.ACCT<AC.CURRENCY>
                        ACCT.COMP= R.ACCT<AC.CO.CODE>
                        IF ACCT.CAT NE 1002 AND ACCT.CAT NE 1205 AND ACCT.CAT NE 1206 AND ACCT.CAT NE 1207 AND ACCT.CAT NE 1208 AND ACCT.CAT NE 1407 AND ACCT.CAT NE 1408 AND ACCT.CAT NE 1413 THEN
*Line [ 202 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                            IF CUS.NEW.SEC EQ 4650 AND NOT( ACCT.CAT GE 1100 AND ACCT.CAT LE 1499 ) THEN GOTO NXT.AC ELSE NULL
*****************************
                            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
                            STE.BAL      = OPENING.BAL
                            IF OPENING.BAL LT 0 THEN
                                MAX.BOOK.BAL = OPENING.BAL
                                MIN.BOOK.BAL = OPENING.BAL
                            END
                            LOOP
                                REMOVE STE.ID FROM ID.LIST SETTING POS
                            WHILE STE.ID:POS
                                REC.STA = ''
                                DR.CUS  = ''
                                CR.CUS  = ''
                                BOOK.D1 = ''
                                BOOK.D2 = ''
                                ZZ = 0
                                IF STE.ID THEN
                                    ZZ++
                                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                                    STE.CUR = R.STE<AC.STE.CURRENCY>
                                    IF STE.CUR NE LCCY THEN
                                        STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                                    END ELSE
                                        STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                                    END
                                    STE.LCY.AMT = R.STE<AC.STE.AMOUNT.LCY>
                                    STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                                    STE.VAL = R.STE<AC.STE.VALUE.DATE>
                                    STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                                    STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
                                    FINDSTR '\B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'\',1)
                                    STE.FCY = R.STE<AC.STE.AMOUNT.FCY>
                                    STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                                    IF STE.REF[1,2] EQ 'FT' THEN
                                        R.FT  = ''
                                        CALL F.READ(FN.FT,STE.REF,R.FT,F.FT,ER.FT)
                                        REC.STA = R.FT<FT.RECORD.STATUS>
                                        DR.CUS  = R.FT<FT.DEBIT.CUSTOMER>
                                        CR.CUS  = R.FT<FT.CREDIT.CUSTOMER>
                                        IF NOT(R.FT) THEN
                                            R.FTH = ''
                                            CALL F.READ.HISTORY(FN.FTH,STE.REF,R.FTH,F.FTH,ER.FTH)
                                            REC.STA = R.FTH<FT.RECORD.STATUS>
                                            DR.CUS  = R.FTH<FT.DEBIT.CUSTOMER>
                                            CR.CUS  = R.FTH<FT.CREDIT.CUSTOMER>
                                        END
                                    END
                                    STE.BAL += STE.AMT
                                    IF REC.STA NE 'REVE' THEN
***IF DR.CUS NE CR.CUS THEN
                                        IF STE.AMT GT 0 THEN
                                            TOT.CR.AMT += STE.AMT
                                            TOT.CR.LCY += STE.LCY.AMT
                                        END ELSE
                                            TOT.DR.AMT += STE.AMT
                                            TOT.DR.LCY += STE.LCY.AMT
                                        END
***END ELSE
***IF NOT(DR.CUS:CR.CUS) THEN
***IF STE.AMT GT 0 THEN
***TOT.CR.AMT += STE.AMT
***TOT.CR.LCY += STE.LCY.AMT
***END ELSE
***TOT.DR.AMT += STE.AMT
***TOT.DR.LCY += STE.LCY.AMT
***END
***END
***END
                                    END
******************************************************************
                                    IF BOOK.D1 EQ '' THEN
                                        BOOK.D1 = STE.DAT
                                        GOSUB GET.BOOK.BAL
                                    END
                                    IF BOOK.D1 LT STE.DAT THEN
                                        GOSUB GET.BOOK.BAL
                                        BOOK.D1 = STE.DAT
                                    END
                                    IF BOOK.BAL LT 0 THEN
                                        IF MAX.BAL.DR EQ 0 THEN
                                            MAX.BAL.DR = BOOK.BAL
                                        END
                                        IF MIN.BAL.DR EQ 0 THEN
                                            MIN.BAL.DR = BOOK.BAL
                                        END
                                        IF BOOK.BAL LT MAX.BAL.DR THEN
                                            MAX.BAL.DR = BOOK.BAL
                                        END
                                        IF BOOK.BAL GT MIN.BAL.DR THEN
                                            MIN.BAL.DR = BOOK.BAL
                                        END
                                    END
******************************************************************
                                END
                            REPEAT
                            GOSUB GET.COMM.CHRG
*******************************************************
                            TOTAL.AMT = ABS(TOT.CR.AMT) + ABS(TOT.DR.AMT) + ABS(TOTAL.INT) + ABS(TOTAL.COM)
                            IF TOTAL.AMT NE 0 THEN

                                GOSUB RET.DATA
                            END
*******************************************************
NXT.AC:
                        END
                    REPEAT
                END
            END
        END
    REPEAT
    RETURN
**************************************
GET.BOOK.BAL:
*------------

    CALL GET.ENQ.BALANCE(ACCT.ID,STE.DAT,BOOK.BAL)

    RETURN
**************************************
GET.COMM.CHRG:
*------------
    SDR.DATE = FROM.DATE
    CALL LAST.DAY(SDR.DATE)
    LOOP
    WHILE SDR.DATE LE END.DATE

        SDR.ID = ACCT.ID:"-":SDR.DATE
        CALL F.READ(FN.SDR,SDR.ID,R.SDR,F.SDR,Y.SDR.ERR)
        TOT.INT = R.SDR<IC.STMDR.TOTAL.INTEREST>
        TOT.COM = R.SDR<IC.STMDR.TOTAL.CHARGE>
        TOTAL.INT += TOT.INT
        TOTAL.COM += TOT.COM
        CALL ADD.MONTHS(SDR.DATE,'1')
    REPEAT
    RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = ACCT.COMP:"*":FROM.DATE:"*":END.DATE:"*":CUS.ID:"*":ACCT.ID:"*":TOT.DR.AMT:"*":TOT.CR.AMT:"*":MIN.BAL.DR:"*":MAX.BAL.DR:"*":TOTAL.COM:"*":TOTAL.INT:"*":TOT.DR.LCY:"*":TOT.CR.LCY
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ACCT.COMP,ACCT.COMP.NAME)
    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,ACCT.CAT,CAT.NAME)
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,ACCT.CUR,CUR.NAME)

    REC.LINE = ACCT.COMP.NAME:",":CUS.ID:",":CUS.NAME:",'":ACCT.ID:",":CAT.NAME:",":CUR.NAME:",":TOT.DR.AMT:",":TOT.CR.AMT:",":MIN.BAL.DR:",":MAX.BAL.DR:",":TOTAL.COM:",":TOTAL.INT


    BB.DATA = REC.LINE
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*#    KK++
*#    XX<1,KK>[1, 20] = ACCT.ID
*#    XX<1,KK>[20,15] = FMT(TOT.DR.AMT,"L2,")
*#    XX<1,KK>[35,15] = FMT(TOT.CR.AMT,"L2,")
*#    XX<1,KK>[50,15] = FMT(MAX.BAL.DR,"L2,")
*#    XX<1,KK>[65,15] = FMT(MIN.BAL.DR,"L2,")
*#    PRINT XX<1,KK>
    RETURN
**************************************
END
