* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*** TRANSFER STMT.ACCT.DR TO TEXT FILE ***
*** CREATED BY KHALED ***
***=================================

** PROGRAM CONV.STMTDR.TEXT.EGP
    SUBROUTINE CONV.STMTDR.TEXT.EGP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "STMT.DR.EGP" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"STMT.DR.EGP"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "STMT.DR.EGP" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE STMT.DR.EGP CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create STMT.DR.EGP File IN &SAVEDLISTS&'
        END
    END
*-----------------------------------------------------------------------
    FN.AC = 'FBNK.STMT.ACCT.DR' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATD = TODAY
    DATM = DATD[1,6]
    DAT = DATM:"01"
    CALL CDT ('',DAT,'-1C')
    TD = "...":DAT

    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ":TD:" AND LIQUIDITY.CCY EQ 'EGP' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CATEG = KEY.LIST<I>[11,4]
            IF CATEG NE 9090 AND CATEG NE 1220 AND CATEG NE 1221 AND CATEG NE 1222 AND CATEG NE 1223 AND CATEG NE 1224 AND CATEG NE 1225 AND CATEG NE 1226 AND CATEG NE 1227 THEN
**BRN = KEY.LIST<I>[1,2]

                AC.NO = FIELD(KEY.LIST<I>,"-",1)
                CALL F.READ(FN.ACCT,AC.NO,R.ACCT,F.ACCT,E1)

                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,AC.NO,COMP)
                CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRN)

                CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUST.ID)
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
                CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME>
                INT.AMT = R.AC<IC.STMDR.DR.INT.RATE>
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                INTD = DCOUNT(INT.AMT,@VM)
                INTF = INT.AMT<1,INTD>
                HI.PER1 = R.AC<IC.STMDR.HIGHEST.DR.PERC>
                IF HI.PER1 EQ '' THEN
                    HI.PER = '0.0015'
                END ELSE
                    HI.PER = HI.PER1
                END
                HI.AMT = R.AC<IC.STMDR.DR.VAL.BALANCE>
*Line [ 97 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                AMTD = DCOUNT(HI.AMT,@VM)
**AMTF = HI.AMT<1,AMTD>
                AMTF = R.ACCT<AC.OPEN.ACTUAL.BAL>

*--------------------------------------------------------
                BB.DATA  = BRN:"|"
                BB.DATA := AC.NO:"|"
                BB.DATA := CUST.NAME1:"|"
                BB.DATA := AMTF:"|"
                BB.DATA := INTF:"|"
                BB.DATA := HI.PER:"|"
*----------------------------------------------------------
**PRINT KEY.LIST<I>:" ":BB.DATA
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I
    END
END
