* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.BUILD.CUS.POSITION(ENQUIRY.DATA)
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*
** 19/12/97 - GB9701470
**            Build the data dependent on the rebuild flag being set
*
*  08/06/05 - EN_10002549
*             Set C$CUS.POS.UPDATE.XREF to 0, so that cache is used
*             instead on CUSTOMER.POSITION.XREF.

    LOCATE 'REBUILD.DATA' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        REBUILD = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END
    IF REBUILD[1,1] NE 'N' THEN         ;* Do not execute on level dwon
        LOCATE "CUSTOMER.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
            CUST.ID = ENQUIRY.DATA<4,CUS.POS>
*Line [ 45 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CONVERT " " TO @VM IN CUST.ID
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF DCOUNT(CUST.ID,@VM) GT 1 OR CUST.ID = "ALL" THEN
                ENQ.ERROR = "ONLY ONE CUSTOMER ALLOWED"
            END ELSE
                C$CUST.POS.UPDATE.XREF = 0        ;* EN_10002549
                CALL CUS.BUILD.POSITION.DATA(CUST.ID)
                C$CUST.POS.UPDATE.XREF = 1        ;* EN_10002549 Reset to 1
            END
        END
    END
******UPDATED BY NESSREEN AHMED ON 30/12/2008**********************
    FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...*1002)"

    CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)
***UPDATED BY NESSREEN AHMED 25/9/2016****
        DELETE F.CUS.POS , KEY.LIST<I>
    ***    CALL F.DELETE (FN.CUS.POS , KEY.LIST<I>)
***END OF UPDATE 25/9/2016**************
    NEXT I

*************************************************************

*
    RETURN
*
END
