* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***********NESSREEN AHMED 10/11/2017**********************
    SUBROUTINE E.NOF.AC.1006.CR.TXN(Y.RET.DATA)
*** PROGRAM E.NOF.AC.1006.CR.TXN

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    GOSUB INITIATE
*Line [ 42 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*===========================================
INITIATE:
*--------
* MAX.DAYS = 7
    MM  = 0
    ZZ  = 0
    CRN = 0
    DBN = 0
    DB.ARR   = ''
    CR.ARR   = ''
    RET.REC  = ''
    DATA.ARR = ''
**  FROM.DATE = '20170901'
**  END.DATE  = TODAY
**  END.DATE = '20171112'
*#CALL ADD.MONTHS(FROM.DATE,'-48')
*#CRT FROM.DATE:"-":END.DATE
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    SRC.FUND = ''
    FT.TYP = ''
  
    RETURN
*===========================================
CALLDB:
*------
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    F.FT = '' ; FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    F.BR = '' ; FN.BR = 'FBNK.BILL.REGISTER' ; R.BR = ''
    CALL OPF(FN.BR,F.BR)
    F.TT = '' ; FN.TT = 'FBNK.TELLER$HIS' ; R.TT = ''
    CALL OPF(FN.TT,F.TT)
    RETURN
*===========================================
PROCESS:
*---------
    LOCATE "START.DATE"   IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"     IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    AA.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 1006 AND OPEN.ACTUAL.BAL GE '100000' BY CO.CODE BY OPEN.ACTUAL.BAL "
    CALL EB.READLIST(AA.SEL,K.LIST,'',SELECTED,ER.MSG)
    TEXT = 'SELECTED.AC=':SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
**    FOR I = 1 TO 5
        ACCT.ID = K.LIST<I>
        PRINT ACCT.ID
        CALL F.READ(FN.AC,ACCT.ID,R.AC,F.AC,ER.AC)
        AC.ONLINE.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
        AC.CUR         = R.AC<AC.CURRENCY>
        AC.COM         = R.AC<AC.CO.CODE>
        OP.DATE        = R.AC<AC.OPENING.DATE>
        CUST.NO        = R.AC<AC.CUSTOMER>
        CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
        LOOP
            REMOVE STE.ID FROM ID.LIST SETTING POS.STE
        WHILE STE.ID:POS.STE
            IF STE.ID THEN
**  PRINT STE.ID
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.VAL     = R.STE<AC.STE.VALUE.DATE>
                STE.DAT     = R.STE<AC.STE.BOOKING.DATE>
                STE.AMT.LCY = R.STE<AC.STE.AMOUNT.LCY>
*-----------------------------------
                STE.OUR.REF = R.STE<AC.STE.OUR.REFERENCE>
                IF NOT(STE.OUR.REF) THEN STE.OUR.REF = R.STE<AC.STE.TRANS.REFERENCE>
                STE.SYSTEM.ID = R.STE<AC.STE.SYSTEM.ID>
                STE.AC.NO   = R.STE<AC.STE.ACCOUNT.NUMBER>
                STE.TR.TYPE = R.STE<AC.STE.TRANSACTION.CODE>

                FT.TYP = ''
                SRC.FUND = ''
                BEGIN CASE
                CASE STE.OUR.REF[1,2] EQ 'FT'
                    STT.REF = STE.OUR.REF
                    DBREF = STT.REF[1,12]:";1"
                    CALL F.READ(FN.FT,DBREF,R.FT,F.FT,FT.ERR)
                    FT.TR.TYPE = R.FT<FT.TRANSACTION.TYPE>
                  **  TEXT = 'FT.TYPE=':FT.TR.TYPE ; CALL REM
                    IF FT.TR.TYPE EQ "IT01" OR FT.TR.TYPE EQ "IT" OR FT.TR.TYPE EQ "IT05" OR FT.TR.TYPE EQ "IM" THEN
                        SRC.FUND = "FRESH"
                        FT.TYP = FT.TR.TYPE
                    END ELSE
                        IF FT.TR.TYPE[1,2] EQ "AC" THEN
                            FT.DB.ACCT = R.FT<FT.DEBIT.ACCT.NO>
                            FT.CR.ACCT = R.FT<FT.CREDIT.ACCT.NO>
                            SRC.FUND = "INTERNAL"
                            FT.TYP = FT.TR.TYPE
                        END
                    END
                CASE STE.OUR.REF[1,2] EQ 'TT'
                    SRC.FUND = "FRESH"
                CASE STE.OUR.REF[1,2] EQ 'BR'
                    SRC.FUND = "FRESH"
                CASE STE.OUR.REF[1,2] EQ 'LD'
                    IF STE.TR.TYPE EQ '420' THEN
                       SRC.FUND = "INTERNAL"
                    END
                    IF STE.TR.TYPE EQ '424' THEN
                       SRC.FUND = "FRESH"
                    END
                CASE STE.OUR.REF[1,2] EQ 'IN'
                    SRC.FUND = "INTERNAL"
                END CASE
                STE.CUR     = R.STE<AC.STE.CURRENCY>
                IF STE.CUR EQ LCCY THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                END ELSE
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                STE.INP     = FIELD(R.STE<AC.STE.INPUTTER>,'_',2)
                STE.ATH     = FIELD(R.STE<AC.STE.AUTHORISER>,'_',2)
                IF STE.INP NE 'INPUTTCOB' THEN
                    IF STE.AMT GT 0 THEN
                        GOSUB SEND.REC
                    END
                END
            END
*-----------------------------------
        REPEAT
    NEXT I
    RETURN
*=============================================
SEND.REC:
*---------
*** RET.REC1 = ACCT.ID:"*":STE.VAL:"*":STE.DAT:"*":STE.CUR:"*":STE.INP:"*":STE.OUR.REF:"*":STE.AMT:"*":STE.TR.TYPE
***    CRT RET.REC1
**     RET.REC = AC.COM:"*":CU.ID:"*":STE.CUR:"*":STE.DAT:"*":ACCT.ID:"*":STE.VAL:"*":STE.OUR.REF:"*":STE.AMT:"*":STE.AMT.LCY:"*":STE.INP:"*":STE.ATH:"*":ACCT.ID

    RET.REC = AC.COM:"*":ACCT.ID:"*":STE.CUR:"*":STE.AMT:"*":STE.DAT:"*":STE.OUR.REF:"*":STE.TR.TYPE:"*":CUST.NO:"*":SRC.FUND:"*":FT.TYP:"*":FROM.DATE:"*":END.DATE
    Y.RET.DATA<-1> = RET.REC
    SRC.FUND = ''
    RETURN
**********************************************
END
