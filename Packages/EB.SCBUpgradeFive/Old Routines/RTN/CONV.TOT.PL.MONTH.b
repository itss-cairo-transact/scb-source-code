* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>163</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM CONV.TOT.PL.MONTH

    SUBROUTINE CONV.TOT.PL.MONTH

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY


**-------------------------------------------------------

    CCY = O.DATA

*    O.DATA  = 'USD'

    AMT.FCY     = 0
    CUR.CAT     = 0
    TOT.FCY.AMT = 0
    CURR.O.DATA = CCY

    YF.CATEG.MONTH = "F.CATEG.MONTH"
    F.CATEG.MONTH = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)
*
    YF.CATEG.ENTRY = "F.CATEG.ENTRY"
    F.CATEG.ENTRY = ""
    CALL OPF(YF.CATEG.ENTRY,F.CATEG.ENTRY)
*
    YF.CATEGORY = "F.CATEGORY"
    F.CATEGORY = ""
    CALL OPF(YF.CATEGORY,F.CATEGORY)
*
    F.CATEG.ENT.TODAY = ""
    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)
*
    F.CATEG.ENT.FWD = ''
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD'
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    FN.COMP = 'F.COMPANY'     ;  F.COMP = '' ; R.COMP  = '' ; ETEXT = ''
    CALL OPF( FN.COMP,F.COMP)
*


**--------------------------------------------------------------------**


    GOSUB GET.TOT.TODAY
    GOSUB GET.TOT.MON


    O.DATA   =  TOT.FCY.AMT.MON + TOT.FCY.AMT.T
    PRINT  "O.DATA " :  O.DATA

    RETURN
**----------------------------------------------

GET.TOT.MON:

    AMT.FCY         = 0
    CUR.CAT         = 0
    TOT.FCY.AMT.MON = 0

    FROM.DATE = TODAY[1,6]:'01'
    TO.DATE   = TODAY

    T.SEL2 = "SELECT F.COMPANY "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)

    FOR XX = 1 TO SELECTED2
        ID.COMPANY      = KEY.LIST2<XX>
        T.SEL3 = "SELECT F.CATEGORY WITH @ID GE 50000 AND @ID LT 69999 "
        CALL EB.READLIST(T.SEL3,KEY.LIST3,'',SELECTED3,ER.MSG3)
        FOR I = 1 TO SELECTED3

            Y.CAT.NO  = KEY.LIST3<I>
            CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)

            LOOP
                REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
            WHILE YENTRY.KEY:YTYPE

                CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,E1)

*            PRINT  YENTRY.KEY
                CATG.CUR = YR.CATEG.ENTRY<AC.CAT.CURRENCY>
                CATEG.PL = YR.CATEG.ENTRY<AC.CAT.PL.CATEGORY>
                V.DATE   = YR.CATEG.ENTRY<AC.CAT.VALUE.DATE>
                B.DATE   = YR.CATEG.ENTRY<AC.CAT.BOOKING.DATE>
                TRS.CODE = YR.CATEG.ENTRY<AC.CAT.TRANSACTION.CODE>

*    IF V.DATE GE FROM.DATE THEN
*       IF B.DATE GE FROM.DATE THEN

                IF CURR.O.DATA EQ CATG.CUR THEN
                    IF ( CATEG.PL NE '53000' AND CATEG.PL NE '53088' AND CATEG.PL NE '53001' ) THEN

                        IF ( TRS.CODE NE '198' AND TRS.CODE NE '199' ) THEN

                            FCY.AMT         = YR.CATEG.ENTRY<AC.CAT.AMOUNT.FCY>
                            TOT.FCY.AMT.MON = TOT.FCY.AMT.MON + FCY.AMT
                        END

                    END
                END
*      END
*  END
            REPEAT

        NEXT I
    NEXT XX

    PRINT " TOT.FCY.AMT.MON  "  : TOT.FCY.AMT.MON
    RETURN
**----------                                      --------------**

GET.TOT.TODAY:

    AMT.FCY.T       = 0
    CUR.CAT       = 0
    TOT.FCY.AMT.T = 0
    CURR.O.DATA   = CCY


    FN.CAT  = 'FBNK.CATEG.ENT.TODAY' ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.CATEG  = 'FBNK.CATEG.ENTRY' ; F.CATEG = '' ; R.CATEG = ''
    CALL OPF(FN.CATEG,F.CATEG)

    T.SEL  = "SELECT FBNK.CATEG.ENT.TODAY "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    LOOP
        REMOVE CAT.ID  FROM KEY.LIST SETTING POS
    WHILE CAT.ID:POS
        CALL F.READ( FN.CAT,CAT.ID, R.CAT,F.CAT, ETEXT)
        LOOP
            REMOVE TRNS.NO.T FROM R.CAT  SETTING POS1
        WHILE TRNS.NO.T:POS1
            CALL F.READ(FN.CATEG,TRNS.NO.T,R.CATEG,F.CATEG,ERR1)


            AMT.FCY.T  = R.CATEG<AC.CAT.AMOUNT.FCY>
            CUR.CAT    = R.CATEG<AC.CAT.CURRENCY>
            CATEG.PL.T = R.CATEG<AC.CAT.PL.CATEGORY>
            IF ( CATEG.PL.T NE '53000' AND CATEG.PL.T NE '53088' AND CATEG.PL.T NE '69999' AND CATEG.PL.T NE '53001') THEN

                IF CUR.CAT EQ CURR.O.DATA THEN
                    TOT.FCY.AMT.T =   TOT.FCY.AMT.T + AMT.FCY.T
                END
            END
        REPEAT
    REPEAT

    PRINT " TOT.FCY.AMT.T  "  : TOT.FCY.AMT.T

    RETURN
**---------------------------------------------------------------
*
END
