* @ValidationCode : MjotMTA3ODk3OTc5OkNwMTI1MjoxNjQwODczODQ0OTMyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 16:17:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM CR.MOKHSAS.CR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.MM.MONEY.MARKET
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.DRAWINGS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
*$INSERT I_F.SCB.FUND
*Line [ 44 ] Removed directory from $INCLUDE - Missing Layout I_FT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-30
*  $INSERT I_FT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.SCB.CUS.POS.TST
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_CU.LOCAL.REFS
*--------------------------------------------------------*
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

    CALL OPF( FN.ACCOUNT,F.ACCOUNT)
*-----------------------------*
    GOSUB CR.SUB
RETURN
*-----------------------------*
CR.SUB:
*------
    ID.NO = ""
    R.LETTER.OF.CREDIT = ''
    F.LETTER.OF.CREDIT = ''
    FN.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
    FN.DRAWINGS = 'FBNK.DRAWINGS' ; F.DRAWINGS = '' ; R.DRAWINGS = ''

    CALL OPF( FN.DRAWINGS,F.DRAWINGS)
    CALL OPF( FN.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT)

    DAT.ID = "EG0010001"
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,BNK.DATE1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
BNK.DATE1=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    T.SEL4 = "SELECT FBNK.DRAWINGS WITH MATURITY.REVIEW GT " : BNK.DATE1

    KEY.LISTBRTEST4=""
    SELECTED4=""
    ER.MSG4  =""
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
    PRINT SELECTED4
    IF KEY.LIST4 THEN
        FOR I = 1 TO SELECTED4
            CALL F.READ(FN.DRAWINGS,KEY.LIST4<I>, R.DRAWINGS,F.DRAWINGS, ETEXT1)
            CALL F.READ(FN.LETTER.OF.CREDIT,KEY.LIST4<I>[1,12], R.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT, ETEXT111)
            APL.NO = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.CUSTNO>
            BEN.NO = R.LETTER.OF.CREDIT<TF.LC.BENEFICIARY.CUSTNO>

            IF APL.NO EQ ''  THEN
                CUS.NOOO = BEN.NO
            END ELSE
                CUS.NOOO = APL.NO
            END
            CALL F.READ(FN.CUSS,CUS.NOOO, R.CUSS,F.CUSS, ETEXT1)
            CATTT = R.LETTER.OF.CREDIT<TF.LC.CATEGORY.CODE>
            ID.NO = CUS.NOOO : "*DR*BNK*" : R.DRAWINGS<TF.DR.DRAW.CURRENCY> :CATTT :"*" : KEY.LIST4<I> : "*" : TODAY

            CURR  = R.DRAWINGS<TF.DR.DRAW.CURRENCY>
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100
            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            AMT = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>

            LCY.AMT  = (MID.RATE<1,1> * AMT)
            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>    = LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER>      = CUS.NOOO
            R.COUNT<CUPOS.DEAL.CCY>      = R.DRAWINGS<TF.DR.DRAW.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT>   = AMT
            R.COUNT<CUPOS.MATURITY.DATE> = R.DRAWINGS<TF.DR.MATURITY.REVIEW>
            R.COUNT<CUPOS.VALUE.DATE>    = R.DRAWINGS<TF.DR.MATURITY.REVIEW>
            R.COUNT<CUPOS.CATEGORY>      = CATTT
            R.COUNT<CUPOS.SYS.DATE> = TODAY
            R.COUNT<CUPOS.CO.CODE> =  R.LETTER.OF.CREDIT<TF.LC.CO.CODE>

            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
RETURN
END
