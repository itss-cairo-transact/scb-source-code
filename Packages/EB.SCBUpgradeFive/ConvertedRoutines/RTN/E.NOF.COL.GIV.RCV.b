* @ValidationCode : MjotMTg3NDIyNzc3MTpDcDEyNTI6MTY0MTkxMzA5MDAxNTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 11 Jan 2022 16:58:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>637</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.COL.GIV.RCV(Y.CUS.DATA)
*    PROGRAM E.NOF.COL.GIV.RCV
**************************11/2/2014****************************
*   CREATED BY Mohamed Sabry              *
***************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
    $INCLUDE  I_F.NUMERIC.CURRENCY       ;*EB.NCN.CURRENCY.CODE
    $INCLUDE I_F.RE.BASE.CCY.PARAM      ;*RE.BCP.
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USR.LOCAL.REF
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 56 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 57 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB CHK.CG
    GOSUB SEND.DATA
RETURN
****************************************
INITIATE:
*--------
    AC.ID         = ''
    CU.ID         = ''
    CO.ID         = ''
    WS.GL.ID      = ''
    WS.CY.ID      = ''
    WS.WRK.AMT    = 0
    WS.ACT.AMT    = 0
    XX            = ''
    WS.GL.IND     = ''
    WS.CY.IND     = ''
    POS.ARR       = 0
    ARR.ACT.AMT   = 0
    ARR.WRK.AMT   = 0
    ARR.BLK.AMT   = 0
    WS.LCY.AMT    = 0
    WS.DR.LCY.AMT = 0
    WS.CR.LCY.AMT = 0
    KK1           = 0
    USR.DEPT = R.USER<EB.USE.LOCAL.REF,USER.SCB.DEPT.CODE>
RETURN
****************************************
CALLDB:
*--------

*Master files

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.COL   = "FBNK.COLLATERAL"         ; F.COL  = '' ;  R.COL = '' ; ER.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.N.CUR = 'F.NUMERIC.CURRENCY' ; F.N.CUR = '' ; R.N.CUR = '' ; ER.N.CUR = '' ; ER.N.CUR = ''
    CALL OPF(FN.N.CUR,F.N.CUR)

    FN.CO = 'F.COMPANY' ; F.CO = '' ; R.CO = '' ; ER.CO = ''
    CALL OPF(FN.CO,F.CO)

* Index files

*Collateral (Received)  Index
    FN.IND.R.COL = "FBNK.COLLATERAL.RIGHT.CUST"   ; F.IND.R.COL = ""
    CALL OPF(FN.IND.R.COL,F.IND.R.COL)

*Collateral (Given)  Index
    FN.IND.G.COL = "FBNK.CUSTOMER.COLLATERAL"   ; F.IND.G.COL = ""
    CALL OPF(FN.IND.G.COL,F.IND.G.COL)

*Collateral Index by useing Collateral (Received)  Index or Collateral (Given)  Index
    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)

RETURN
****************************************
CHK.CG:
*-------
    LOCATE "CUSTOMER.ID" IN D.FIELDS<1> SETTING YCUS.POS THEN ID.ALL = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN

    ID.LEN =  LEN(ID.ALL)

    IF ID.LEN       = 10 THEN
        CU.ID       = ID.ALL[1,8] + 0
        WS.CUR.ID   = ID.ALL[9,2]
    END
    IF ID.LEN       = 9 THEN
        CU.ID       = ID.ALL[1,7]
        WS.CUR.ID   = ID.ALL[8,2]
    END

*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,WS.CUR.ID,WS.SEL.CY)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,WS.CUR.ID,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
WS.SEL.CY=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
    GOSUB PROCESS

RETURN
****************************************
PROCESS:
*-------

    CALL F.READ(FN.CU,CU.ID,R.CU,F.CU,ER.CU)
    CU.COM  = R.CU<EB.CUS.COMPANY.BOOK>
    CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
    CRD.STA = CU.LCL<1,CULR.CREDIT.STAT>
    CRD.COD = CU.LCL<1,CULR.CREDIT.CODE>
    EMP.NO  = CU.LCL<1,CULR.EMPLOEE.NO>
    SS.ON   = (1000000 + EMP.NO)[2,6]

    IF CRD.STA NE '' OR CRD.COD GE 110 THEN
        IF USR.DEPT NE 9905 THEN
            RETURN
        END
    END
    GOSUB COL.GIV.REC
    GOSUB COL.RCV.REC
RETURN
****************************************
COL.GIV.REC:
*--------
    CALL F.READ(FN.IND.G.COL,CU.ID,R.IND.G.COL,F.IND.G.COL,ER.IND.G.COL)
    LOOP
        REMOVE COL.G.ID FROM R.IND.G.COL SETTING POS.G.COL
    WHILE COL.G.ID:POS.G.COL
        CALL F.READ(FN.COL.R,COL.G.ID,R.COL.R,F.COL.R,EER.R)
        WS.PRC.NO = DCOUNT(R.COL.R<COLL.RIGHT.PERCENT.ALLOC>,@VM)
        FOR X = 1 TO WS.PRC.NO
            WS.REF.CUS= R.COL.R<COLL.RIGHT.LIMIT.REF.CUST,X>
            IF WS.REF.CUS # CU.ID THEN
                WS.COL.ID =  COL.G.ID
                WS.CUS.ID.CHK = FIELD (WS.COL.ID,'.',1)
                WS.PRC =  R.COL.R<COLL.RIGHT.PERCENT.ALLOC,X>
                WS.PRC.MIA = WS.PRC / 100
                WS.GL.ID    = 34000
                GOSUB  SUB.GET.COL.AMT
            END
        NEXT X
    REPEAT
RETURN
****************************************
COL.RCV.REC:
*--------
    CALL F.READ(FN.IND.R.COL,CU.ID,R.IND.R.COL,F.IND.R.COL,ER.IND.R.COL)
    LOOP
        REMOVE COL.R.ID FROM R.IND.R.COL SETTING POS.R.COL
    WHILE COL.R.ID:POS.R.COL
        CALL F.READ(FN.COL.R,COL.R.ID,R.COL.R,F.COL.R,EER.R)
        WS.PRC.NO = DCOUNT(R.COL.R<COLL.RIGHT.PERCENT.ALLOC>,@VM)
        FOR X = 1 TO WS.PRC.NO
            WS.REF.CUS= R.COL.R<COLL.RIGHT.LIMIT.REF.CUST,X>
            IF WS.REF.CUS EQ CU.ID THEN
                WS.COL.ID =  COL.R.ID
                WS.CUS.ID.CHK = FIELD (WS.COL.ID,'.',1)
                IF WS.CUS.ID.CHK # CU.ID THEN
                    WS.PRC =  R.COL.R<COLL.RIGHT.PERCENT.ALLOC,X>
                    WS.PRC.MIA = WS.PRC / 100
                    WS.GL.ID    = 35000
                    GOSUB  SUB.GET.COL.AMT
                END
            END
            WS.PRC.MIA = 0
        NEXT X
    REPEAT
RETURN
****************************************
SUB.GET.COL.AMT:
    COL.AMT.P = 0
    CALL F.READ(FN.RIGHT.COL,WS.COL.ID,R.RIGHT.COL,F.RIGHT.COL,ER.RIGHT.COL)
    LOOP
        REMOVE RIGHT.COL.ID FROM R.RIGHT.COL SETTING POS.RIGHT.COL
    WHILE RIGHT.COL.ID:POS.RIGHT.COL
        CALL F.READ(FN.COL,RIGHT.COL.ID,R.COL,F.COL,EER)
*CY.CODE =  R.COL<COLL.CURRENCY>
        WS.CY.ID =  R.COL<COLL.CURRENCY>
*CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CY.CODE,WS.CY.NUM)
        WS.APP.ID  =  R.COL<COLL.APPLICATION.ID>[1,2]
        WS.EXP.DATE=  R.COL<COLL.EXPIRY.DATE>
        COL.AMT =  R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
        IF WS.APP.ID = 'LD' THEN
            IF  WS.EXP.DATE GE TODAY THEN
                WS.WRK.AMT  = COL.AMT *  WS.PRC.MIA
                WS.ACT.AMT  = 0
                WS.BLK.AMT  = 0
                GOSUB FILL.ARRAY
            END
        END
        IF WS.APP.ID # 'LD' THEN
            WS.WRK.AMT  = COL.AMT *  WS.PRC.MIA
            WS.ACT.AMT  = 0
            WS.BLK.AMT  = 0
            GOSUB FILL.ARRAY
        END
    REPEAT
RETURN
************************************************
FILL.ARRAY:
*----------
*Line [ 250 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,WS.CY.ID,WS.CY.NUM)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,WS.CY.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
WS.CY.NUM=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
    WS.GL.ID = (100000 + WS.GL.ID)[2,5]

    LOCATE WS.GL.ID IN WS.GL.IND<1> SETTING POS.GL THEN NULL ELSE
        KK1++
        WS.GL.IND<KK1> = WS.GL.ID
    END

    FINDSTR WS.CY.NUM:"." IN WS.CY.IND SETTING POS.CY THEN NULL ELSE
        WS.CY.IND := WS.CY.NUM :"."
    END

    ARR.ACT.AMT<WS.GL.ID,WS.CY.NUM> += WS.ACT.AMT
    ARR.WRK.AMT<WS.GL.ID,WS.CY.NUM> += WS.WRK.AMT
    ARR.BLK.AMT<WS.GL.ID,WS.CY.NUM> += WS.BLK.AMT
RETURN
****************************************
SEND.DATA:
*--------
    Y.CUS.DATA = ''
    WS.GIV.AMT = 0
    WS.RCV.AMT = 0
*    PRINT WS.GL.IND
    WS.GL.IND   = SORT(WS.GL.IND)
*    PRINT WS.GL.IND

    WS.GL.COUNT = DCOUNT(WS.GL.IND,@FM)
    WS.CY.COUNT = COUNT(WS.CY.IND,'.')
    FOR IGL = 1 TO WS.GL.COUNT
        WS.GL.POS = WS.GL.IND<IGL> + 0
        FOR ICY = 1 TO WS.CY.COUNT
            WS.CY.POS = FIELD(WS.CY.IND,'.',ICY)
*Line [ 288 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,WS.CY.POS,WS.CY.ID)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,WS.CY.POS,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
WS.CY.ID=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
            IF ARR.WRK.AMT<WS.GL.POS,WS.CY.POS> NE '' THEN
                WS.GL.MASK = WS.GL.POS
                IF WS.GL.POS = 50000 THEN
                    WS.GL.MASK = 'LCDI'
                END
                IF WS.GL.POS = 50001 THEN
                    WS.GL.MASK = 'LCDE'
                END
                IF WS.GL.POS = 34000  THEN
                    IF WS.CY.ID EQ WS.SEL.CY THEN
                        WS.GIV.AMT = ARR.WRK.AMT<WS.GL.POS,WS.CY.POS>
                    END
                END
                IF WS.GL.POS = 35000  THEN
                    IF WS.CY.ID EQ WS.SEL.CY THEN
                        WS.RCV.AMT = ARR.WRK.AMT<WS.GL.POS,WS.CY.POS>
                    END
                END
* Y.CUS.DATA<-1> = CU.COM:"*":CU.ID:"*":WS.GL.MASK:"*":WS.CY.ID:"*":ARR.WRK.AMT<WS.GL.POS,WS.CY.POS>:"*":ARR.ACT.AMT<WS.GL.POS,WS.CY.POS>:"*":ARR.BLK.AMT<WS.GL.POS,WS.CY.POS>:"*":WS.CR.LCY.AMT:"*":WS.DR.LCY.AMT
            END
        NEXT ICY
    NEXT IGL
    IF WS.GIV.AMT GT 0 OR WS.RCV.AMT GT 0 THEN
        Y.CUS.DATA<-1> = 'A00,':CU.ID:',':WS.GIV.AMT:',':WS.RCV.AMT:',':'0,':WS.CY.ID
*       Y.CUS.DATA     = 'A00,':CU.ID:',':WS.GIV.AMT:',':WS.RCV.AMT:',':'0,':WS.CY.ID
    END
    IF WS.GIV.AMT EQ 0 AND WS.RCV.AMT EQ 0 THEN
        Y.CUS.DATA<-1> = ',D02'
*       Y.CUS.DATA    = ',D02'
    END
RETURN
****************************************
END
