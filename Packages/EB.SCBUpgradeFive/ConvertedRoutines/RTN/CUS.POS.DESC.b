* @ValidationCode : MjotMTg1Mzc3MDEyMjpDcDEyNTI6MTY0MDcyOTk0MTc5Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 00:19:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------

SUBROUTINE CUS.POS.DESC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.POSITION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ENTRY.STMT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 42 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.LC.TYPE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.LC.TYPE
*Line [ 44 ] Removed directory from $INCLUDE - Missing Layout I_LC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LC.LOCAL.REFS

    ETEXT = ''
    IF O.DATA[1,2] EQ 'TF' THEN

        FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
        CALL OPF( FN.LC,F.LC)
        FN.DR   = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
        CALL OPF( FN.DR,F.DR)
        CALL F.READ( FN.DR,O.DATA, R.DR, F.DR,ETEXT)
        IF NOT(ETEXT) THEN

            CALL F.READ( FN.LC,O.DATA[1,12], R.LC, F.LC,ETEXT1)
            CATT = R.LC<TF.LC.CATEGORY.CODE>

            IF CATT EQ '23100' OR CATT EQ '23102' OR CATT EQ '23152' OR CATT EQ '23155' OR CATT EQ '23150' OR CATT EQ '23010' OR CATT EQ '23014' OR CATT EQ '23030' OR CATT EQ '23031' THEN
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,DESC2)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
DESC2=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

                DESC1   = "�������� ��� ���� ��"

                O.DATA  = DESC1 : " " :DESC2
*                TEXT = O.DATA ; CALL REM

            END ELSE
                O.DATA = ""
            END
        END ELSE
            CALL F.READ( FN.LC,O.DATA[1,12], R.LC, F.LC,ETEXT1)
            CATT  = R.LC<TF.LC.CATEGORY.CODE>
            TYPE1 = R.LC<TF.LC.LOCAL.REF><1,LCLR.SCB.TYPE>

*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.LC.TYPE':@FM:1,TYPE1,DESC1)
F.ITSS.SCB.LC.TYPE = 'F.SCB.LC.TYPE'
FN.F.ITSS.SCB.LC.TYPE = ''
CALL OPF(F.ITSS.SCB.LC.TYPE,FN.F.ITSS.SCB.LC.TYPE)
CALL F.READ(F.ITSS.SCB.LC.TYPE,TYPE1,R.ITSS.SCB.LC.TYPE,FN.F.ITSS.SCB.LC.TYPE,ERROR.SCB.LC.TYPE)
DESC1=R.ITSS.SCB.LC.TYPE<1>
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATT,DESC2)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
DESC2=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
            O.DATA = DESC2:' ':DESC1

        END
    END ELSE
        O.DATA = ""
    END
RETURN
END
