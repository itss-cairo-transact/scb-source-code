* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE E.NOF.CR.MV.CUST(Y.ALL.REC)

***Mahmoud Elhawary******9/5/2010*****************************
* Nofile routine to get credit transactions                  *
* of customers accounts for Settlement & rearrange dept.s    *
**************************************************************

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 46 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 47 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    KK = 0
    XX = SPACE(120)
    STE.BAL  = ''
    CUS.GRP  = ''
    CUS.GRP1 = '110'
    CUS.GRP2 = '120'
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.GROUP" IN D.FIELDS<1> SETTING YCUS.POS THEN CUST.GROUP  = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE" IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE  = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE" IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    BEGIN CASE
    CASE CUST.GROUP EQ 1
        CUS.GRP = CUS.GRP1
    CASE CUST.GROUP EQ 2
        CUS.GRP = CUS.GRP2
    CASE OTHERWISE
        RETURN
    END CASE

    CC.SEL  = "SELECT ":FN.CUS:" WITH CREDIT.CODE EQ ":CUS.GRP
    CC.SEL := " BY @ID BY COMPANY.BOOK"

    CALL EB.READLIST(CC.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE CUS.ID FROM K.LIST SETTING POS.CUS
        WHILE CUS.ID:POS.CUS
            CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
            LOOP
                REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.ACC
            WHILE ACCT.ID:POS.ACC
                CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
                ACCT.CAT = R.ACCT<AC.CATEGORY>
                IF ACCT.CAT NE 1002 THEN
*****************************
                    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
                    STE.BAL = OPENING.BAL
                    TOT.DR.MVMT = 0
                    TOT.CR.MVMT = 0
                    LOOP
                        REMOVE STE.ID FROM ID.LIST SETTING POS
                    WHILE STE.ID:POS
                        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                        STE.CUR = R.STE<AC.STE.CURRENCY>
                        STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                        STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                        STE.VAL = R.STE<AC.STE.VALUE.DATE>
                        STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                        STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,STE.TXN,STE.TRN)
F.ITSS.TRANSACTION = 'F.TRANSACTION'
FN.F.ITSS.TRANSACTION = ''
CALL OPF(F.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION)
CALL F.READ(F.ITSS.TRANSACTION,STE.TXN,R.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION,ERROR.TRANSACTION)
STE.TRN=R.ITSS.TRANSACTION<AC.TRA.NARRATIVE>
                        IF STE.CUR NE 'EGP' THEN
                            STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                        END
                        STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                        STE.BAL += STE.AMT
                        IF STE.AMT LT 0 THEN
                            TOT.DR.MVMT += STE.AMT
                        END ELSE
                            TOT.CR.MVMT += STE.AMT
                        END
                        IF STE.AMT GT 0 THEN
                            GOSUB RET.DATA
                        END
                    REPEAT
                END
            REPEAT
        REPEAT
    END
    RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = COMP:"*":CUST.GROUP:"*":ACCT.ID:"*":FROM.DATE:"*":END.DATE:"*":OPENING.BAL:"*":STE.DAT:"*":STE.AMT:"*":STE.BAL:"*":STE.ID:"*":STE.OUR:"*":STE.TXN:"*":STE.TRN:"*":STE.VAL:"*":STE.ID:"*":STE.REF
    RETURN
**************************************
END
