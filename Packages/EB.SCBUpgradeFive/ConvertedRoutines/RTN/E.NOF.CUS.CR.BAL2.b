* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1110</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CUS.CR.BAL2(Y.ALL.REC)
*#    PROGRAM E.NOF.CUS.CR.BAL2
***Mahmoud Elhawary******12/9/2010*****************************
* Nofile routine to get credit balances                       *
* of customers accounts                                      *
**************************************************************

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 51 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 52 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    PREV.DAY  = FROM.DATE
    KK = 0
    II = 0
    XX = SPACE(100)
    STE.BAL  = ''
    CUS.GRP  = ''
    CUS.GRP1 = '110'
    CUS.GRP2 = '120'
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    MAX.CR.BAL  = 0
    MIN.CR.BAL  = 0
    AVR.CR.BAL  = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO" IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

*#    INPUT CUS.ID
*#    INPUT FROM.DATE
*#    INPUT END.DATE

    PREV.DAY = FROM.DATE

    DDAYS = 'C'
    CALL CDD("",FROM.DATE,END.DATE,DDAYS)
    C.DATE = FROM.DATE
    EE.DATE = END.DATE
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
    CUS.POST = R.CUS<EB.CUS.POSTING.RESTRICT>
    CUS.COMP = R.CUS<EB.CUS.COMPANY.BOOK>
    CUS.LCL  = R.CUS<EB.CUS.LOCAL.REF>
    CUS.STAT = CUS.LCL<1,CULR.CREDIT.STAT>
    CUS.CRDT = CUS.LCL<1,CULR.CREDIT.CODE>
    IF CUS.POST GT 89 THEN
        RETURN
    END
    IF CUS.STAT NE '' OR CUS.CRDT EQ '110' OR CUS.CRDT EQ '120' THEN
        RETURN
    END
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
    LOOP
        REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.ACC
    WHILE ACCT.ID:POS.ACC
        STE.BAL     = 0
        MAX.CR.BAL  = 0
        MIN.CR.BAL  = 0
        AVR.CR.BAL  = 0
        TOT.CR.BAL  = 0
        TOT.CR.BAL.DAYS = 0
        II = 0
        CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
        ACCT.CAT = R.ACCT<AC.CATEGORY>
        ACCT.CUR = R.ACCT<AC.CURRENCY>

        C.DATE = FROM.DATE
        CALL CDT("",C.DATE,'-1C')

        IF ACCT.CAT NE 1002 THEN
            CRT ACCT.ID
            LOOP
                CALL CDT("",C.DATE,'+1C')
            WHILE C.DATE LE EE.DATE
*****************************
                CALL GET.ENQ.BALANCE(ACCT.ID,C.DATE,Y.OPEN.BAL)
*****************************
                STE.BAL = Y.OPEN.BAL
                IF STE.BAL GT 0 THEN
                    CRT C.DATE:"  ":Y.OPEN.BAL
                    TOT.CR.BAL += STE.BAL
                    TOT.CR.BAL.DAYS++
                    IF MIN.CR.BAL EQ 0 THEN
                        MIN.CR.BAL = STE.BAL
                    END
                    IF MAX.CR.BAL EQ 0 THEN
                        MAX.CR.BAL = STE.BAL
                    END
                    IF STE.BAL LT MIN.CR.BAL THEN
                        MIN.CR.BAL = STE.BAL
                    END
                    IF STE.BAL GT MAX.CR.BAL THEN
                        MAX.CR.BAL = STE.BAL
                    END
                END
            REPEAT
            IF TOT.CR.BAL.DAYS EQ 0 THEN
                IF STE.BAL GE 0 THEN
                    AVR.CR.BAL = STE.BAL
                END
            END
            IF TOT.CR.BAL.DAYS NE 0 THEN
                AVR.CR.BAL = TOT.CR.BAL / TOT.CR.BAL.DAYS
            END
            AA.REC = MAX.CR.BAL + MIN.CR.BAL + AVR.CR.BAL
            IF AA.REC NE 0 THEN
                GOSUB RET.DATA
            END
        END
    REPEAT
    RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = COMP:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":ACCT.ID:"*":MAX.CR.BAL:"*":MIN.CR.BAL:"*":AVR.CR.BAL
*#    KK++
*#    XX<1,KK>[1, 20] = ACCT.ID
*#    XX<1,KK>[20,20] = FMT(MAX.CR.BAL,"L2,")
*#    XX<1,KK>[40,20] = FMT(MIN.CR.BAL,"L2,")
*#    XX<1,KK>[60,20] = FMT(AVR.CR.BAL,"L2,")
***    CRT COMP:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":ACCT.ID:"*":MAX.CR.BAL:"*":MIN.CR.BAL:"*":AVR.CR.BAL
*#    PRINT XX<1,KK>
    RETURN
**************************************
END
