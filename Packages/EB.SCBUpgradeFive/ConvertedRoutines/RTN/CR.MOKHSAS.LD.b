* @ValidationCode : MjotMTQxNjAwNDY3NjpDcDEyNTI6MTY0MDY5NTkzMDgxOTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:52:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM CR.MOKHSAS.LD

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.DRAWINGS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.FUND not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.FUND
*Line [ 46 ] Removed directory from $INCLUDE - Missing Layout I_FT.LOCAL.REFS not used - ITSS - R21 Upgrade - 2021-12-23
*    $INCLUDE I_FT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CUS.POS.TST - ITSS - R21 Upgrade - 2021-12-23
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.SCB.CUS.POS.TST
*--------------------------------------------------------*
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    GOSUB LD.SUB
RETURN
*-----------------------
LD.SUB:
*------
    ID.NO = ""
    DATD  = TODAY
    DATM  = DATD[1,6]
    DAT   = DATM:"01"
    CALL CDT ('',DAT,'-1C')
    WS.DATE = TODAY
    FN.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD.LOANS.AND.DEPOSITS = '' ; R.LD.LOANS.AND.DEPOSITS = ''

    CALL OPF( FN.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS)

    DAT.ID = 'EG0010001'
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.DATE.LWD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.DATE.LWD=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    T.SEL1  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH"
    T.SEL1 := " (( CATEGORY GE 21096 AND CATEGORY LE 21097 )"
    T.SEL1 := " OR ( CATEGORY GE 21050 AND CATEGORY LE 21074 ))"
    T.SEL1 := " AND STATUS NE 'FWD'"
    T.SEL1 := " AND (( VALUE.DATE LE ":DAT:" AND FIN.MAT.DATE GT ":WS.DATE.LWD:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":WS.DATE.LWD:" AND AMOUNT EQ 0 ))"
    T.SEL1 := " AND CATEGORY NE '' "

    KEY.LIST1=""
    SELECTED1=""
    ER.MSG1 = ""
    CATTTG = ''
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF KEY.LIST1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.LD.LOANS.AND.DEPOSITS,KEY.LIST1<I>, R.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS, ETEXT1)
*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.LD.LOANS.AND.DEPOSITS<LD.CURRENCY>,CURR.NO)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.LD.LOANS.AND.DEPOSITS<LD.CURRENCY>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR.NO=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

            CATTTG = '' ; ID.NO = ''

            CATTTG = R.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
            IF ( CATTTG EQ 21096  OR CATTTG EQ 21097 ) THEN
                ID.NO = R.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>:"*LG*BNK*":CURR.NO:R.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>:"*":KEY.LIST1<I>:"*":WS.DATE
            END
            IF (CATTTG GE 21050  AND CATTTG LE 21074) THEN
                ID.NO = R.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>:"*LO*BNK*":CURR.NO:R.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>:"*":KEY.LIST1<I>:"*":WS.DATE
            END
            IF ( CATTTG GE "21001 " AND CATTTG LE "21010" )  THEN
                ID.NO = R.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>:"*LD*BNK*":CURR.NO:R.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>:"*":KEY.LIST1<I>:"*":WS.DATE
            END
            IF ( CATTTG GE "21020 " AND CATTTG LE "21032" ) THEN
                ID.NO = R.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>:"*CD*BNK*":CURR.NO:R.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>:"*":KEY.LIST1<I>:"*":WS.DATE
            END

            CALL F.READ(FN.CUSS,R.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>, R.CUSS,F.CUSS, ETEXT1)


            CURR= R.LD.LOANS.AND.DEPOSITS<LD.CURRENCY>
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100

            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            IF R.LD.LOANS.AND.DEPOSITS<LD.AMOUNT> NE 0 THEN
                AMT = R.LD.LOANS.AND.DEPOSITS<LD.AMOUNT>
            END ELSE
                AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT>
            END

            LCY.AMT = (MID.RATE<1,1> * AMT)

            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>= LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER> = R.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>
            R.COUNT<CUPOS.DEAL.CCY> = R.LD.LOANS.AND.DEPOSITS<LD.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT> = AMT
            R.COUNT<CUPOS.MATURITY.DATE> = R.LD.LOANS.AND.DEPOSITS<LD.FIN.MAT.DATE>
            R.COUNT<CUPOS.VALUE.DATE> = R.LD.LOANS.AND.DEPOSITS<LD.VALUE.DATE>
            R.COUNT<CUPOS.INT.RATE> = R.LD.LOANS.AND.DEPOSITS<LD.INTEREST.RATE> + R.LD.LOANS.AND.DEPOSITS<LD.INTEREST.SPREAD>
            R.COUNT<CUPOS.CATEGORY> = R.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
            R.COUNT<CUPOS.SYS.DATE> = WS.DATE
            R.COUNT<CUPOS.CO.CODE> = R.LD.LOANS.AND.DEPOSITS<LD.CO.CODE>
            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
RETURN
*--------------------------
