* @ValidationCode : MjotMzc4MDM4OTA3OkNwMTI1MjoxNjQwNzAzNDQ3MjcwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:57:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*------------------------- NI7OOOOOOOOOOOO ------------------
SUBROUTINE CURR.BUILD.2(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.GL.CURR not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.GL.CURR
*Line [ 33 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.GL.CUR.BAL not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.GL.CUR.BAL
*----------------------------------------------------------
    FN.CURR ='F.SCB.GL.CURR'  ;  F.CURR  =''
    CALL OPF(FN.CURR,F.CURR)

    FN.CURR.BAL ='F.SCB.GL.CUR.BAL' ;  F.CURR.BAL   =''
    CALL OPF(FN.CURR.BAL,F.CURR.BAL)

    XX = ""
    XX1 = 1:'-':XX
    XX2 = 17:'-':XX
    XX5 = 28:'-':XX
    XX3 = 19:'-':XX
    XX6 = 31:'-':XX
    XX4 = 26:'-':XX
    XX7 = 33:'-':XX

    COMP = ID.COMPANY
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,COMP,TD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,COMP,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    XX = '...' : TD
    T.SEL="SELECT F.SCB.GL.CUR.BAL.MARKZY WITH @ID LIKE ":XX:" AND NEWNO EQ 2"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ENQ.LP   = '' ;
    OPER.VAL = ''

    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END
*-----------------------------------------------
RETURN
END
