* @ValidationCode : MjotNzkyOTEzOTUxOkNwMTI1MjoxNjQwODY3OTgxOTU0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:39:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 13/02/2010******************
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.DB.GT.50000.EGP2(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    TOT.TT = 0 ; TOT.AMT = 0

    YTEXT = "Enter the Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI

    YTEXT = "Enter the End Date  : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI

**  N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CURRENCY.1 EQ 'USD' AND CUSTOMER.1 NE '' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE GE '20110125' BY CUSTOMER.1 BY AUTH.DATE "
    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CURRENCY.1 EQ 'EGP' AND CUSTOMER.1 NE '' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CUSTOMER.1 BY AUTH.DATE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM

    CUST<1>     = R.TT<TT.TE.CUSTOMER.1>
    AMT<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1> = R.TT<TT.TE.AUTH.DATE>
    TOT.AMT = TOT.AMT + AMT<1>
    TOT.TT  = TOT.TT + 1
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>     = R.TT<TT.TE.CUSTOMER.1>
        AMT<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I> = R.TT<TT.TE.AUTH.DATE>
        IF CUST<I> # CUST<I-1> THEN
            IF TOT.AMT GT 50000 THEN
                FOR NN = 1 TO TOT.TT
                    Y.RET.DATA<-1> = CUST<I-NN>:"*":AMT<I-NN>:"*":AUTH.DAT<I-NN>:"*":KEY.LIST.N<I-NN>
                NEXT NN
                TOT.TT = 0 ; TOT.AMT = ''
            END ELSE
                TOT.TT = 0 ; TOT.AMT = ''
            END
            TOT.AMT = TOT.AMT + AMT<I>
            TOT.TT  = TOT.TT + 1
        END ELSE
            IF AUTH.DAT<I> = AUTH.DAT<I-1> THEN
                TOT.AMT = TOT.AMT + AMT<I>
                TOT.TT  = TOT.TT + 1
            END ELSE
                IF TOT.AMT GT 50000 THEN
                    FOR NN = 1 TO TOT.TT
                        Y.RET.DATA<-1> = CUST<I-NN>:"*":AMT<I-NN>:"*":AUTH.DAT<I-NN>:"*":KEY.LIST.N<I-NN>
                    NEXT NN
                    TOT.AMT = 0 ; TOT.TT = 0
                END ELSE
                    TOT.AMT = 0 ; TOT.TT = 0
                    TOT.AMT = TOT.AMT + AMT<I>
                    TOT.TT  = TOT.TT + 1
                END
            END
            IF I = SELECTED.N THEN
                TEXT = 'Last Record' ; CALL REM
                IF TOT.AMT GT 50000 THEN
** FOR NN = 1 TO TOT.TT
                    Y.RET.DATA<-1> = CUST<I>:"*":AMT<I>:"*":AUTH.DAT<I>:"*":KEY.LIST.N<I>
** NEXT NN
                END
            END
        END
    NEXT I

RETURN
END
