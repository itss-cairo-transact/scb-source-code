* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***ABEER 2017-09-12 
   SUBROUTINE E.NOF.CD.VAR.CUS.NEW(Y.RET.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*************
    YTEXT = " Enter Start Date YYYY/MM/DD"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    CON.ST.DATE = COMI
    YTEXT = " Enter End Date YYYY/MM/DD"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    CON.EN.DATE = COMI
*************

    FN.LD ='F.LD.LOANS.AND.DEPOSITS'
    F.LD  =''
    CALL OPF(FN.LD,F.LD)
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH VALUE.DATE GE ": CON.ST.DATE :" AND VALUE.DATE LE ": CON.EN.DATE :" AND CATEGORY GE 21026 AND CATEGORY LE 21028 BY CO.CODE BY CUSTOMER.ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CUS.NO = '';CD.AMT = '';CUS.CODE='';CUS.CODE1 = ''
    CD.CODE = '';CD.CODE1 = '';CON.CUS = '';CON.CUS1 = ''
    TEXT=SELECTED;CALL REM
*********************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            CALL F.READ(FN.LD,KEY.LIST<I+1>,R.LD1,F.LD,E2)

            CUS.CODE       =  R.LD<LD.CUSTOMER.ID>
            CUS.CODE1      =  R.LD1<LD.CUSTOMER.ID>

            CD.CODE       = R.LD<LD.CO.CODE>
            CD.CODE1      = R.LD1<LD.CO.CODE>
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.CONTACT.DATE,CUS.CODE,CON.CUS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.CODE,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CON.CUS=R.ITSS.CUSTOMER<EB.CUS.CONTACT.DATE>
*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.CONTACT.DATE,CUS.CODE1,CON.CUS1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.CODE1,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CON.CUS1=R.ITSS.CUSTOMER<EB.CUS.CONTACT.DATE>

            IF CD.CODE EQ CD.CODE1 THEN
                IF CUS.CODE1 NE CUS.CODE THEN
                    IF CON.CUS GE CON.ST.DATE AND CON.CUS LE CON.EN.DATE THEN
                        CUS.NO = 1 + CUS.NO
                        CD.AMT = CD.AMT + R.LD<LD.AMOUNT>
                    END
                END ELSE
                    IF CON.CUS GE CON.ST.DATE AND CON.CUS LE CON.EN.DATE THEN
                        CD.AMT = CD.AMT + R.LD<LD.AMOUNT>
                    END
                END
            END ELSE
                IF CON.CUS GE CON.ST.DATE AND CON.CUS LE CON.EN.DATE THEN
                    CUS.NO = 1 + CUS.NO
                    CD.AMT = CD.AMT + R.LD<LD.AMOUNT>
                END
                IF CUS.NO NE '' AND CD.AMT NE '0' THEN
                    Y.RET.DATA<-1> = CUS.NO :"*":R.LD<LD.CO.CODE>:"*":CD.AMT
                END
                CUS.NO = ''
                CD.AMT = '0'
            END
        NEXT I
    END
**********************
    RETURN
END
