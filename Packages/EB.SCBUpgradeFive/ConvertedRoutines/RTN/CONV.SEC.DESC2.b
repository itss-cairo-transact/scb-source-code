* @ValidationCode : MjotMjA2NTM4OTU4NTpDcDEyNTI6MTY0MDY4NjE5NjE5Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:09:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.SEC.DESC2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - Missing Layout "I_F.SBM.LG.SECTOR.1"- ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SBM.LG.SECTOR.1

    HH  = O.DATA

    BEGIN CASE
        CASE HH = 1
            RR = "���� ������� �����"
        CASE HH = 2
            RR = "����� ���� �������� "
        CASE  HH = 3
            RR =  "������ ����"
        CASE OTHERWISE
            RETURN
    END CASE

    O.DATA = RR
RETURN
END
