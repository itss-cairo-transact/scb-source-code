* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
    SUBROUTINE E.BUILD.CUS.POS.TST3(ENQUIRY.DATA)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.LIABILITY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION

** 19/12/97 - GB9701470
**            Build the data dependent on the rebuild flag being set
*
*  08/06/05 - EN_10002549
*             Set C$CUS.POS.UPDATE.XREF to 0, so that cache is used
*             instead on CUSTOMER.POSITION.XREF.
    LOCATE 'REBUILD.DATA' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        REBUILD = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END
    IF REBUILD[1,1] NE 'N' THEN         ;* Do not execute on level dwon

*    LOCATE "CUSTOMER.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
        LOCATE "LIABILITY" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
            CUST.ID = ENQUIRY.DATA<4,CUS.POS>
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CONVERT " " TO @VM IN CUST.ID
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF DCOUNT(CUST.ID,@VM) GT 1 OR CUST.ID = "ALL" THEN
                ENQ.ERROR = "ONLY ONE CUSTOMER ALLOWED"
            END ELSE
******************************************************

                FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
                CALL OPF(FN.CUS.POS,F.CUS.POS)
                KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""

                T.SELLL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID:"..."

                CALL EB.READLIST(T.SELLL,KEY.LIST.LLL,"",SELECTED.LLL,ER.MSG.LLL)
                FOR DD = 1 TO SELECTED.LLL

                    CALL F.READ(FN.CUS.POS,KEY.LIST.LLL<DD>,R.CUS.POS,F.CUS.POS,READ.ERR)
*DELETE F.CUS.POS , KEY.LIST.LLL<DD>
                    CALL F.DELETE (FN.CUS.POS,KEY.LIST.LLL<DD>)
                NEXT DD

********************************************************************
                C$CUST.POS.UPDATE.XREF = 0        ;* EN_10002549
                CALL CUS.BUILD.POSITION.DATA(CUST.ID)
                C$CUST.POS.UPDATE.XREF = 1        ;* EN_10002549 Reset to 1
            END
        END
    END
*************************************************************************

    FN.CUS.LIAB = 'FBNK.CUSTOMER.LIABILITY' ;F.CUS.LIAB = '' ; R.CUS.LIAB = ''
    CALL OPF(FN.CUS.LIAB,F.CUS.LIAB)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
    T.SEL = "SELECT FBNK.CUSTOMER.LIABILITY WITH @ID EQ ": CUST.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.CUS.LIAB,KEY.LIST,R.CUS.LIAB,F.CUS.LIAB,READ.ERR)
    FLG = 0
    H = 1
    LOOP WHILE FLG = 0
        IF R.CUS.LIAB<H,EB.CUL.LIABILITY.INCLUDED> = '' THEN
            FLG = 1
        END
        CUSS = R.CUS.LIAB<H,EB.CUL.LIABILITY.INCLUDED>

        C$CUST.POS.UPDATE.XREF = 0      ;* EN_10002549
        CALL CUS.BUILD.POSITION.DATA(CUSS)
        C$CUST.POS.UPDATE.XREF = 1      ;* EN_10002549 Reset to 1

        H = H + 1

    REPEAT
**********************************************************
    FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...LIAL... OR @ID LIKE ...LIUT...)"

    CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED

        CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)

        CUSSS=FIELD(KEY.LIST<I>,"*",1)
        LIABB=FIELD(KEY.LIST<I>,"*",2)

        IF CUSSS NE LIABB THEN
*DELETE F.CUS.POS , KEY.LIST<I>
            CALL F.DELETE (FN.CUS.POS,KEY.LIST<I>)
        END
    NEXT I
******UPDATED BY NESSREEN AHMED ON 30/12/2008**********************
    FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...*1002)"

    CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)
***Updated by Nessreen Ahmed 25/9/2016 *************************
        DELETE F.CUS.POS , KEY.LIST<I>
***     CALL F.DELETE (FN.CUS.POS , KEY.LIST<I>)
***End of Update 25/9/2016 9 ****************************
    NEXT I

*************************************************************

    RETURN

END
