* @ValidationCode : MjotMTg1MjE1NjUxNTpDcDEyNTI6MTY0MTkxMjU4MTA1Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 11 Jan 2022 16:49:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 18/3/2013*********************************
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CURR.DALY.UPD
*************************************************************
*************************************************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 39 ] Removed directory from $INCLUDE  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY.USD.RATE.FCY

    FN.CURR  = 'F.CURRENCY' ; F.CURR = '' ;R.CURR = '' ; ERR1 = '' ; RETRY1= ''
    CALL OPF(FN.CURR,F.CURR)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)

    FN.CURR.USD  = "F.CURRENCY.USD.RATE.FCY"  ; F.CURR.USD   = "" ; R.CURR.USD = '' ; ERR3 = ''
    CALL OPF(FN.CURR.USD,F.CURR.USD)

    CURR.DAT = TODAY
****####### Foriegn Currency Against EGP ##########**********
    N.SEL = "SELECT FBNK.CURRENCY WITH (@ID NE 'NZD' AND @ID NE 'EGP' AND @ID NE 'ITL') BY NUMERIC.CCY.CODE "
    CALL EB.READLIST(N.SEL , KEY.LIST.N,'',SELECTED.N,ER.MSG.N)
    FOR I = 1 TO SELECTED.N
        CURR = KEY.LIST.N<I>
        KEY.ID<I> = CURR:"-" :"EGP":"-":CURR.DAT
        CALL F.READ(FN.CURR,KEY.LIST.N<I>,R.CURR,F.CURR,ERR1)
        CURR.CODE = R.CURR<EB.CUR.NUMERIC.CCY.CODE>
        CURR.NAME = R.CURR<EB.CUR.CCY.NAME>
        CURR.MARK = R.CURR<EB.CUR.CURRENCY.MARKET>
*Line [ 62 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD = DCOUNT(CURR.MARK,@VM)
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        FOR POS = 1 TO DD
            CURR.MARK.1<POS> = R.CURR<EB.CUR.CURRENCY.MARKET,POS>
            MID.RAT.1<POS>   = R.CURR<EB.CUR.MID.REVAL.RATE,POS>
            BUY.RAT.1<POS>   = R.CURR<EB.CUR.BUY.RATE,POS>
            SELL.RAT.1<POS>  = R.CURR<EB.CUR.SELL.RATE,POS>

            R.CURR.DALY<SCCU.CURR.MARKET,POS>  = CURR.MARK.1<POS>
            R.CURR.DALY<SCCU.MID.RATE,POS>     = MID.RAT.1<POS>
            R.CURR.DALY<SCCU.BUY.RATE,POS>     = BUY.RAT.1<POS>
            R.CURR.DALY<SCCU.SELL.RATE,POS>    = SELL.RAT.1<POS>
****Updated by Nessreen Ahmed 26/3/2013****************

            DAY.NUM = ICONV(CURR.DAT,"DW")
            DAY.NUM2= OCONV(DAY.NUM,"DW")

            R.CURR.DALY<SCCU.DAY.NUMBER>  = DAY.NUM2
            R.CURR.DALY<SCCU.RATE.DATE>   = CURR.DAT
            R.CURR.DALY<SCCU.CURR.1>      = CURR
            R.CURR.DALY<SCCU.CURR.2>      = "EGP"

****End of Update 26/3/2013*****************************

            CALL F.WRITE(FN.CURR.DALY,KEY.ID<I>, R.CURR.DALY)
** CALL JOURNAL.UPDATE(KEY.ID<I>)
        NEXT POS
    NEXT I
*TEXT = 'End Of Currency' ; CALL REM
*************************************************************
****######## Foreign Currency against USD ###########************
    BUY.RATE = '' ; SELL.RATE = '' ; MID.RATE = ''
    S.SEL = "SELECT F.CURRENCY.USD.RATE.FCY BY @ID "
    CALL EB.READLIST(S.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CALL F.READ(FN.CURR.USD,KEY.LIST<SELECTED>,R.CURR.USD,F.CURR.USD,ERR3)
        CUR.A = R.CURR.USD<CUR.CURRENCY>

*Line [ 101 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CUR.N = DCOUNT(CUR.A,@VM)
        FOR NN = 1 TO CUR.N
            BUY.RATE = '' ; SELL.RATE = '' ; MID.RATE = ''
            CUR.US       = R.CURR.USD<CUR.CURRENCY><1,NN>
            SELL.RATE    = R.CURR.USD<CUR.SELL.CUS><1,NN>
            BUY.RATE     = R.CURR.USD<CUR.BUY.CUS><1,NN>
            MID.RATE     = (SELL.RATE + BUY.RATE ) / 2
            KEY.ID.US<NN> = CUR.US:"-" :"USD":"-":CURR.DAT
            CALL F.READ(FN.CURR.DALY,KEY.ID.US<NN>,R.CURR.DALY,F.CURR.DALY,ERR2)
            R.CURR.DALY<SCCU.BUY.RATE,1>     = BUY.RATE
            R.CURR.DALY<SCCU.SELL.RATE,1>    = SELL.RATE
            R.CURR.DALY<SCCU.MID.RATE,1>     = MID.RATE
****Updated by Nessreen Ahmed 26/3/2013****************

            DAY.NUM = ICONV(CURR.DAT,"DW")
            DAY.NUM2= OCONV(DAY.NUM,"DW")

            R.CURR.DALY<SCCU.DAY.NUMBER>  = DAY.NUM2
            R.CURR.DALY<SCCU.RATE.DATE>   = CURR.DAT
            R.CURR.DALY<SCCU.CURR.1>      = CUR.US
            R.CURR.DALY<SCCU.CURR.2>      = "USD"

****End of Update 26/3/2013*****************************

            CALL F.WRITE(FN.CURR.DALY,KEY.ID.US<NN>, R.CURR.DALY)
**  CALL JOURNAL.UPDATE(KEY.ID.US<NN>)

        NEXT NN
    END
****
*TEXT = 'End Of FCY Currency' ; CALL REM

RETURN
END
