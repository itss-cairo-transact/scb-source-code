* @ValidationCode : Mjo3ODA4OTgyODg6Q3AxMjUyOjE2NDA2ODUzNTA5NTM6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:55:50
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
SUBROUTINE CONV.TELLER.AC
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 25 ] Removed directory from $INCLUDE - Missing Layout I_CO.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_CO.LOCAL.REFS
************************************************************
    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
RETURN
************************************************************
INIT:
    XX = O.DATA
    AMT = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.TEL = 'FBNK.TELLER'
    F.TEL = ''
    R.TEL = ''
RETURN
************************************************************
OPEN:
    CALL OPF(FN.TEL,F.TEL)
RETURN
PROCESS:
************************************************************
    T.SEL = "SELECT ":FN.TEL: " WITH CURRENCY.1 EQ ":XX:" AND TELLER.ID.1 LIKE ...99 AND DR.CR.MARKER EQ 'CREDIT'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.TEL,KEY.LIST<I>,R.TEL,F.TEL,EER)
            AMT += R.TEL<TT.TE.NET.AMOUNT>
        NEXT I
    END
    O.DATA = AMT
RETURN
END
