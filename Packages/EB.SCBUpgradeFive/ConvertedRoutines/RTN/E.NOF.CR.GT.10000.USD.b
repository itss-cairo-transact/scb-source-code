* @ValidationCode : MjotNDAxMzY0OTI1OkNwMTI1MjoxNjQwODYyMDg5OTUxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 13:01:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 13/02/2010******************
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CR.GT.10000.USD(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - Missing Layout I_TT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    TOT.TT = 0 ; TOT.AMT = 0

    N.SEL = "SELECT FBNK.TELLER WITH DR.CR.MARKER EQ 'CREDIT' AND CURRENCY.1 EQ 'USD' AND CUSTOMER.1 NE '' BY CUSTOMER.1 "
*  N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CURRENCY.1 EQ 'USD' AND CUSTOMER.1 NE '' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE EQ '20110210' BY CUSTOMER.1 "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM

    CUST<1> = R.TT<TT.TE.CUSTOMER.1>
    AMT<1>  = R.TT<TT.TE.AMOUNT.FCY.1>
    TOT.AMT = TOT.AMT + AMT<1>
    TOT.TT  = TOT.TT + 1
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I> = R.TT<TT.TE.CUSTOMER.1>
        AMT<I>  = R.TT<TT.TE.AMOUNT.FCY.1>
        IF CUST<I> # CUST<I-1> THEN
            IF TOT.AMT GT 10000 THEN
                Y.RET.DATA<-1> = CUST<I-1>:"*":TOT.TT:"*":TOT.AMT
                TOT.TT = 0 ; TOT.AMT = ''
            END ELSE
                TOT.TT = 0 ; TOT.AMT = ''
            END
            TOT.AMT = TOT.AMT + AMT<I>
            TOT.TT  = TOT.TT + 1
        END ELSE
            TOT.AMT = TOT.AMT + AMT<I>
            TOT.TT  = TOT.TT + 1
        END
        IF I = SELECTED.N THEN
            IF TOT.AMT GT 10000 THEN
                Y.RET.DATA<-1> = CUST<I>:"*":TOT.TT:"*":TOT.AMT
            END
        END

    NEXT I

RETURN
END
