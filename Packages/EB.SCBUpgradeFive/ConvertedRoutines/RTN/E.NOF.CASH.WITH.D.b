* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***NESSREEN AHMED 21/11/2010*********************************
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE E.NOF.CASH.WITH.D(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

*-----------------------------------------------------------------------*
    TD1 = ''

    KEY.LIST.W = "" ; SELECTED.W = "" ;  ER.MSG.W = ""
    KEY.LIST.D = "" ; SELECTED.D = "" ;  ER.MSG.D = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = '' ; ERR.TT = ''
    CALL OPF(FN.TT,F.TT)

    AMT.CR   = 0
    AMT.DR   = 0  ; REQ.D = ''

*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.TODAY>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]

***  MDATE = YYYY:MM:'01'
***  CALL CDT("",MDATE,'-1W')
***  END.DATE = MDATE

***  FROM.DATE = MDATE[1,6]:'01'

    YTEXT = "Enter Required Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    REQ.D = COMI
    TOT.AMT.LCY.W = '' ; TOT.AMT.LCY.D = ''
    DESC.WW = '' ; DESC.DD = '' ; WW.NN = 0 ; DD.NN = 0
    W.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(2 54 41 40 86 84 82 8 36 34 25 100 20 63 21 17 11 12 92 14 97 64 105 95 107 98 65 106 108 96) AND RECORD.STATUS NE 'REVE' AND AUTH.DATE EQ " : REQ.D
    CALL EB.READLIST(W.SEL, KEY.LIST.W, "", SELECTED.W, ER.MSG.W)

    TEXT = 'SELECTED.W=':SELECTED.W ; CALL REM
    FOR WW = 1 TO SELECTED.W
        KEY.USE.WW = ''
        TT.R.W = '' ; REF.WW = '' ; TT.REF.W = '' ; AMT.LCY.W = ''
        REF.WW = KEY.LIST.W<WW>
        TT.REF.W  = FIELD(REF.WW, ";" ,1)
        TT.R.W = TT.REF.W:";2"
        CALL F.READ( FN.TT,TT.R.W, R.TT, F.TT, ERR.TT)
        IF ERR.TT THEN
            KEY.USE.WW = TT.REF.W:";1"
            CALL F.READ( FN.TT,KEY.USE.WW, R.TT, F.TT, E.TT)
            AMT.LCY.W = R.TT<TT.TE.AMOUNT.LOCAL.1>
            TOT.AMT.LCY.W = TOT.AMT.LCY.W + AMT.LCY.W
            WW.NN = WW.NN + 1
        END
        ERR.TT = ''
    NEXT WW
    DESC.WW = '������ �����'
    D.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(9 62 53 42 66 37 38 7 32 33 101 102) AND RECORD.STATUS NE 'REVE' AND AUTH.DATE EQ " : REQ.D
    CALL EB.READLIST(D.SEL, KEY.LIST.D, "", SELECTED.D, ER.MSG.D)

    TEXT = 'SELECTED.D=':SELECTED.D ; CALL REM
    FOR DD = 1 TO SELECTED.D
        R.TT = '' ; ERR.TT = '' ; KEY.USE.DD = ''
        TT.R.D = '' ; REF.DD = '' ; TT.REF.D = '' ; AMT.LCY.D = ''
        REF.DD = KEY.LIST.D<DD>
        TT.REF.D  = FIELD(REF.DD, ";" ,1)
        TT.R.D = TT.REF.D:";2"
        CALL F.READ( FN.TT,TT.R.D, R.TT, F.TT, ERR.TT)
        IF ERR.TT THEN
            KEY.USE.DD = TT.REF.D:";1"
            CALL F.READ( FN.TT,KEY.USE.DD, R.TT, F.TT, E.TT)
            AMT.LCY.D = R.TT<TT.TE.AMOUNT.LOCAL.1>
            TOT.AMT.LCY.D = TOT.AMT.LCY.D + AMT.LCY.D
            DD.NN = DD.NN + 1
        END
    NEXT DD
    DESC.DD = '������ �������'
    Y.RET.DATA<-1> = WW.NN:"*":TOT.AMT.LCY.W:"*":DESC.WW:"*":DD.NN:"*":TOT.AMT.LCY.D:"*":DESC.DD:"*":REQ.D

    RETURN
END
