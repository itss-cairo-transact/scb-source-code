* @ValidationCode : MjotMTQ1NTIwMjU3OTpDcDEyNTI6MTY0MDg2NjIzOTk5Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:10:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 12/9/2014******************
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CUST.OVR.200000.1005(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - Missing Layout I_TT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.TELLER.DPST.1005 - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TELLER.DPST.1005
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""


    FN.TT.DPST = 'F.SCB.TELLER.DPST.1005' ; F.TT.DPST = ''
    CALL OPF(FN.TT.DPST,F.TT.DPST)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    TT.SEL = "SELECT F.SCB.TELLER.DPST.1005 WITH CUST.DATE GE '20140904' BY CUST.NO"

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            CUST<TTI> = KEY.LIST.TT<TTI>
* TEXT = 'CUST1=':CUST<TTI> ; CALL REM
            KEY.LIST.DD = ''
            SELECTED.DD = ''
            DD.N = ''

            DD.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CUST<TTI>
            CALL EB.READLIST(DD.SEL, KEY.LIST.DD, "", SELECTED.DD, DD.N)
            IF SELECTED.DD EQ '1' THEN
                CALL F.READ(FN.ACC,KEY.LIST.DD<1>,R.ACC,F.ACC, ERR.ACC)
                CATEG = R.ACC<AC.CATEGORY>
*  TEXT = 'CATEG=':CATEG ; CALL REM
                AC.COMP = R.ACC<AC.CO.CODE>
                IF CATEG EQ '1005' THEN
                    CALL F.READ( FN.TT.DPST, CUST<TTI>, R.TT.DPST, F.TT.DPST, ERR1)
                    CUST = CUST<TTI>
                    AMT  = R.TT.DPST<TT.DPST.AMT.LCY>
                    CONTACT.DATE = R.TT.DPST<TT.DPST.CUST.DATE>
                    Y.RET.DATA<-1> = CUST:"*" : AMT :"*": CONTACT.DATE : "*":KEY.LIST.DD :"*":AC.COMP
                    CUST = '' ; AMT = '' ; CONTACT.DATE = '' ; AC.COMP = ''
                END
            END
        NEXT TTI
    END

RETURN
END
