* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>47</Rating>
*-----------------------------------------------------------------------------
*   PROGRAM CONV.TOT.PL

    SUBROUTINE CONV.TOT.PL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY

*    O.DATA  = 'USD'

    AMT.FCY     = 0
    CUR.CAT     = 0
    TOT.FCY.AMT = 0
    CURR.O.DATA = O.DATA


    FN.CAT  = 'FBNK.CATEG.ENT.TODAY' ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.CATEG  = 'FBNK.CATEG.ENTRY' ; F.CATEG = '' ; R.CATEG = ''
    CALL OPF(FN.CATEG,F.CATEG)

    T.SEL  = "SELECT FBNK.CATEG.ENT.TODAY "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    LOOP
        REMOVE CAT.ID  FROM KEY.LIST SETTING POS
    WHILE CAT.ID:POS
        CALL F.READ( FN.CAT,CAT.ID, R.CAT,F.CAT, ETEXT)
        LOOP
            REMOVE TRNS.NO FROM R.CAT  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.CATEG,TRNS.NO,R.CATEG,F.CATEG,ERR1)


            AMT.FCY    = R.CATEG<AC.CAT.AMOUNT.FCY>
            CUR.CAT    = R.CATEG<AC.CAT.CURRENCY>
            CATEG.PL.T = R.CATEG<AC.CAT.PL.CATEGORY>

            IF ( CATEG.PL.T NE '53000' AND CATEG.PL.T NE '53088' AND CATEG.PL.T NE '69999' AND CATEG.PL.T NE '53001' ) THEN

                IF CUR.CAT EQ  CURR.O.DATA THEN
                    TOT.FCY.AMT =   TOT.FCY.AMT + AMT.FCY
                END

            END

        REPEAT
    REPEAT

    IF TOT.FCY.AMT LT 0 THEN

        DESC   = "  SHORT   "
    END ELSE
        DESC   = "  LONG    "
    END

    O.DATA   =  TOT.FCY.AMT

    PRINT O.DATA

    RETURN
END
