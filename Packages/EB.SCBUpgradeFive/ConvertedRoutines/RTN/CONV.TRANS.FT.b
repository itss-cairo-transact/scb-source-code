* @ValidationCode : MjotMTQ5NjkzNjA5NTpDcDEyNTI6MTY0MDY4NzkzNTI4NDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:38:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.TRANS.FT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.TRANS.TODAY - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY

    XX = O.DATA
    FN.FT = 'FBNK.FUNDS.TRANSFER'
    F.FT = '' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.LC = 'FBNK.LETTER.OF.CREDIT'
    F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)

    IF XX[1,2] EQ 'FT' THEN
        CALL F.READ(FN.FT,XX,R.FT,F.FT,E1)
        CONF.MSG = R.FT<FT.DEBIT.THEIR.REF>
        IF CONF.MSG EQ 'CONFIRMATION' THEN
            O.DATA = '�����'
        END ELSE
            O.DATA = ''
        END
    END
****UPDATED BY NESSREEN AHMED 18/10/2011******************
    IF XX[1,2] EQ 'TF' THEN
        ID.USE = XX[1,12]
**    TF.LC.LOCAL.REF
        CALL F.READ(FN.LC,ID.USE,R.LC,F.LC,E1.LC)
        OLD.LC = R.LC<TF.LC.OLD.LC.NUMBER>
        O.DATA = OLD.LC
    END
****END OF UPDATE 18/10/2011***********************************
RETURN
