* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>400</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CUSTOMER.OVERRIDE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    IF R.OLD(EB.CUS.SECTOR) # '' THEN
        IF R.NEW(EB.CUS.SECTOR) # R.OLD(EB.CUS.SECTOR) THEN TEXT = "No.Change.Allowed.In.Sector"  ; CALL STORE.OVERRIDE(R.NEW(EB.CUS.CURR.NO)+1)
    END

    IF R.OLD(EB.CUS.INDUSTRY) # '' THEN
        IF R.NEW(EB.CUS.INDUSTRY) # R.OLD(EB.CUS.INDUSTRY) THEN TEXT = "No.Change.Allowed.In.Industry" ; CALL STORE.OVERRIDE(R.NEW(EB.CUS.CURR.NO)+1)
    END
********UPDATED BY NESSREEN AHMED 02/09/2010*****************************************
*** IF R.OLD(EB.CUS.LOCAL.REF)<1,CULR.LEGAL.FORM> # '' THEN
***     IF R.NEW(EB.CUS.LOCAL.REF)<1,CULR.LEGAL.FORM> # R.OLD(EB.CUS.LOCAL.REF)<1,CULR.LEGAL.FORM> THEN TEXT = "No.Change.Allowed.In.Legal.Form" ; CALL STORE.OVERRIDE(R.NEW(EB.CUS.CURR.NO)+1)
*** END
********END OF UPDATED 02/09/2010*****************************************************
    IF R.OLD(EB.CUS.CUSTOMER.STATUS) # '' THEN
        IF R.NEW(EB.CUS.CUSTOMER.STATUS) # R.OLD(EB.CUS.CUSTOMER.STATUS) THEN TEXT = "No.Change.Allowed.In.Customer.Status" ; CALL STORE.OVERRIDE(R.NEW(EB.CUS.CURR.NO)+1)
    END

    RETURN
END
