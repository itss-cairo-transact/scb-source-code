* @ValidationCode : MjoxMDAzNDM1ODY5OkNwMTI1MjoxNjQwNjkzOTYzOTUyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:19:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.USD.TOT.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - Missing Layout I_F.CBE.STATIC.AC.LD.DETAIL - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*----------------------------------------------------
    FN.TMP = "F.CBE.STATIC.AC.LD.DETAIL" ; F.TMP = ""
    CALL OPF(FN.TMP,F.TMP)
    TOT = 0

    CUS.ID = O.DATA
    T.SEL  = "SELECT F.CBE.STATIC.AC.LD.DETAIL WITH CBEM.CUSTOMER.CODE EQ " : CUS.ID
    T.SEL := " AND CBEM.CATEG IN ( 5000 5001 21076 ) AND CBEM.CY NE 'EGP' BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.TMP,KEY.LIST<II>,R.TMP,F.TMP,TMP.ERR)
            CURR = R.TMP<STD.CBEM.CY>
            
            
            IF CURR NE 'USD' THEN
                AMT = R.TMP<STD.CBEM.IN.LCY>

                TOT = TOT + AMT
            END
        NEXT II
    END

    O.DATA = TOT
*----------------------------------------------------
RETURN
END
