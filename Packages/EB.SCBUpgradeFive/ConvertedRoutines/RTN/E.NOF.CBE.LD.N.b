* @ValidationCode : MjoxNzg4MjcxNjUwOkNwMTI1MjoxNjQxOTEyOTA2OTEzOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 11 Jan 2022 16:55:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1612</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* CREATED BY NESSMA
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CBE.LD.N(Y.RET.DATA)
*PROGRAM E.NOF.CBE.LD.N

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.RANGE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.MAS.D
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.TYPE.LEVEL
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.GEN.CONDITION
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CREDIT.INT
*-----------------------------------------*
    FN.CATMAS = "F.CATEG.MAS.D"   ; F.CATMAS = ''  ; R.CATMAS = ''
    CALL OPF(FN.CATMAS,F.CATMAS)

    FN.COMP = "F.COMPANY"         ; F.COMP = ''    ; R.COMP   =''
    CALL OPF(FN.COMP,F.COMP)

    FN.LN = 'F.RE.STAT.REP.LINE'      ; F.LN = ''  ; R.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.RG = 'F.RE.STAT.RANGE'         ; F.RG = ''  ; R.RG = ''
    CALL OPF(FN.RG,F.RG)

    FN.CUS  = 'F.SCB.LD.TYPE.LEVEL'   ; F.CUS  = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.CAT = 'F.CATEGORY'     ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.AGC  = 'FBNK.ACCT.GEN.CONDITION'; F.AGC = '' ; R.AGC = ''
    CALL OPF(FN.AGC,F.AGC)

    FN.GC   = 'FBNK.GROUP.CREDIT.INT'  ; F.GC  = ''
    CALL OPF(FN.GC,F.GC)

*----------------------------------------------------------------------*
    ZEFT = 1
    ARRAY.CATEGORY = ''

    TT       = TODAY
    KK1      = 0
    MM       = 0
    CATEG    = ''
    CAT.CRIT = ''
    XX       = ''
    M.MONTHS = ''

    AMT.2.A = ''
    AMT.2.B = ''
    AMT.2.C = ''
    AMT.2.D = ''
    AMT.2.E = ''
    AMT.2.F = ''

    AMT.1 = ''
    AMT.2 = ''
    AMT.3 = ''
    AMT.4 = ''
    AMT.5 = ''

    RATE.1 = '' ; MAX.1 = '' ; MIN.1 = ''
    RATE.2 = '' ; MAX.2 = '' ; MIN.2 = ''
    RATE.3 = '' ; MAX.3 = '' ; MIN.3 = ''
    RATE.4 = '' ; MAX.4 = '' ; MIN.4 = ''
    RATE.5 = '' ; MAX.5 = '' ; MIN.5 = ''

    RATE.0510 = '' ; MAX.0510 = '' ; MIN.0510 = ''
    RATE.0527 = '' ; MAX.0527 = '' ; MIN.0527 = ''
    RATE.0570 = '' ; MAX.0570 = '' ; MIN.0570 = ''
    RATE.0520 = '' ; MAX.0520 = '' ; MIN.0520 = ''
    RATE.0525 = '' ; MAX.0525 = '' ; MIN.0525 = ''
    RATE.0540 = '' ; MAX.0540 = '' ; MIN.0540 = ''
    RATE.0550 = '' ; MAX.0550 = '' ; MIN.0550 = ''


    Z        = 1
    AMT      = 0
    COMP     = ''
    R.ACC    = ''
    R.MIN    = ''
    R.MAX    = ''
    R.RAT1   = ''
    R.RATE1  = ''
    ARR.CAT  = ''
    RATE.TOT = ''

    ARR.NN  = SPACE(130)
    ARR.NN<1,1> = 'GENLED.0510'
    ARR.NN<1,2> = 'GENLED.0556'
    ARR.NN<1,3> = 'GENLED.0527'
    ARR.NN<1,4> = 'GENLED.0530'
    ARR.NN<1,5> = 'GENLED.0570'
    ARR.NN<1,6> = 'GENLED.0520'
    ARR.NN<1,7> = 'GENLED.0525'
    ARR.NN<1,8> = 'GENLED.0540'
    ARR.NN<1,9> = 'GENLED.0550'
    SEL.N  = 9

    FOR IIS = 1 TO SEL.N      ;*LOOP ON LINES
        LN.ID  = ARR.NN<1,IIS>

        GOSUB GETCATEG

        T.SEL = "SELECT F.COMPANY BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        AMT = 0

        FOR NN = 1 TO SELECTED          ;*LOOP ON COMPANIES
            COMP   = KEY.LIST<NN>
            FOR NI = 1 TO Z   ;*LOOP ON CATEGORIES
                CAT.N     = ARR.CAT<1,NI>
                ID.CATMAS = COMP:"*":LN.ID[8,4]:"*":CAT.N:"*":"EGP"

                CALL F.READ(FN.CATMAS,ID.CATMAS,R.CATMAS,F.CATMAS,E.CATMAS)

                IF LN.ID EQ 'GENLED.0527' THEN
                    AMT.2.C = AMT.2.C + R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END
                IF LN.ID EQ 'GENLED.0530' AND CAT.N EQ '21001' THEN
                    AMT.2.A =  AMT.2.A + R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END
                IF LN.ID EQ 'GENLED.0530' AND CAT.N EQ '21003' THEN
                    AMT.2.B = AMT.2.B + R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END
                IF LN.ID EQ 'GENLED.0530' AND CAT.N EQ '21005' THEN
                    AMT.2.C = AMT.2.C +  R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END
                IF LN.ID EQ 'GENLED.0530' AND CAT.N EQ '21006' THEN
                    AMT.2.D = AMT.2.D + R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END
                IF LN.ID EQ 'GENLED.0530' AND CAT.N EQ '21007' THEN
                    AMT.2.E = AMT.2.E + R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END
                IF LN.ID EQ 'GENLED.0530' AND CAT.N EQ '21010' THEN
                    AMT.2.F = AMT.2.F +  R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
                END

                AMT = AMT + R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
            NEXT NI
        NEXT NN
*-----------------------*
        GOSUB CALC.RATE
********************* COMPUTE MIN && MAX
        RATE.Z  = 0
        R.MAX.Z = 0
        R.MIN.Z = 99999

        FOR DD =  1 TO ZEFT
            RATE.Z = RATE.Z + ARRAY.CATEGORY<1,DD>

            IF  ARRAY.CATEGORY<1,DD> LE R.MIN.Z THEN
                R.MIN.Z = ARRAY.CATEGORY<1,DD>
            END

            IF ARRAY.CATEGORY<1,DD> GT R.MAX.Z THEN
                R.MAX.Z = ARRAY.CATEGORY<1,DD>
            END

        NEXT DD
**  IF  R.MIN.Z EQ 99999 THEN R.MIN.Z = 0

        RATE.Z = RATE.Z / ZEFT
**********************
        IF LN.ID EQ 'GENLED.0510' THEN
            RATE.0510 = RATE.Z
            MAX.0510  = R.MAX.Z
            MIN.0510  = R.MIN.Z
            AMT.1 = AMT.1 +  AMT
        END
        IF LN.ID EQ 'GENLED.0556' THEN
            RATE.0556 = RATE.Z
            MAX.0556  = R.MAX.Z
            MIN.0556  = R.MIN.Z
            AMT.1 = AMT.1 +  AMT
        END
        IF LN.ID EQ 'GENLED.0520' THEN
            RATE.0520 = RATE.Z
            MAX.0520  = R.MAX.Z
            MIN.0520  = R.MIN.Z

            AMT.4 = AMT.4 + AMT
        END
        IF LN.ID EQ 'GENLED.0525' THEN
            RATE.0525 = RATE.Z
            MAX.0525  = R.MAX.Z
            MIN.0525  = R.MIN.Z

            AMT.4 = AMT.4 + AMT
        END
        IF LN.ID EQ 'GENLED.0540' THEN
            RATE.0540 = RATE.Z
            MAX.0540  = R.MAX.Z
            MIN.0540  = R.MIN.Z

            AMT.5 = AMT.5 + AMT
        END
        IF LN.ID EQ 'GENLED.0550' THEN
            RATE.0550 = RATE.Z
            MAX.0550  = R.MAX.Z
            MIN.0550  = R.MIN.Z

            AMT.5 = AMT.5 + AMT
        END
        IF LN.ID EQ 'GENLED.0570' THEN
            RATE.0570 = RATE.Z
            MAX.0570 = R.MAX.Z
            MIN.0570 = R.MIN.Z

            AMT.3 = AMT.3 + AMT
        END
        IF LN.ID EQ 'GENLED.0527' THEN
            RATE.0527 = RATE
            MAX.0527 = R.MAX
            MIN.0527 = R.MIN

            AMT.2.CC = AMT.2.CC + AMT
        END
        AMT      = 0
        RATE.Z   = 0
    NEXT IIS
*---------------------------------*
    AMOUNT.1 = AMT.1
    W.1      = (RATE.0510 + RATE.0556 ) / 2
    IF MIN.0510 LT MIN.0556 THEN
        MIN.RATE.1 = MIN.0510
    END ELSE
        MIN.RATE.1 = MIN.0556
    END
    IF MAX.0510 GT MAX.0556 THEN
        MAX.RATE.1 = MAX.0510
    END ELSE
        MAX.RATE.1 = MAX.0556
    END
    R.RAT1 = 0
*-----------*
    AMOUNT.4 = AMT.4
    W.4      = (RATE.0520 + RATE.0525 ) / 2
    IF MIN.0520 LT MIN.0525 THEN
        MIN.RATE.4 = MIN.0520
    END ELSE
        MIN.RATE.4 = MIN.0525
    END
    IF MAX.0520 GT MAX.0525 THEN
        MAX.RATE.4 = MAX.0520
    END ELSE
        MAX.RATE.4 = MAX.0525
    END
    R.RAT1 = 0
*---------------*
    AMOUNT.5 = AMT.5
    W.5      = (RATE.0540 + RATE.0550 ) / 2
    IF MIN.0540 LT MIN.0550 THEN
        MIN.RATE.5 = MIN.0540
    END ELSE
        MIN.RATE.5 = MIN.0550
    END
    IF MAX.0540 GT MAX.0550 THEN
        MAX.RATE.5 = MAX.0540
    END ELSE
        MAX.RATE.5 = MAX.0550
    END
    R.RAT1 = 0
*---------------*
    AMOUNT.3   = AMT.3
    W.3        = RATE.0570
    MIN.RATE.3 = MIN.0570
    MAX.RATE.3 = MAX.0570
*---------------*
    AMOUNT.A = AMT.2.A
    CATG.ID = "21001" : TT
    CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 314 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
    FOR II = 1 TO N.COUNT
        IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
            RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
            BB = II
            II = N.COUNT
            R.RAT1 = R.CUS<LDTL.RATE><1,BB>
        END
    NEXT II

    W.A  = R.RAT1
    R.RAT1 = 0
*---------------*
    AMOUNT.B = AMT.2.B
    CATG.ID = "21003" : TT
    CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 331 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
    FOR II = 1 TO N.COUNT
        IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
            RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
            BB = II
            II = N.COUNT
            R.RAT1 = R.CUS<LDTL.RATE><1,BB>
        END
    NEXT II

    W.B  = R.RAT1
    R.RAT1 = 0
*----------------*
    AMOUNT.C = AMT.2.C + AMT.2.CC
    CATG.ID = "21005" : TT
    CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 348 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
    FOR II = 1 TO N.COUNT
        IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
            RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
            BB = II
            II = N.COUNT
            R.RAT1 = R.CUS<LDTL.RATE><1,BB>
        END
    NEXT II

    W.C  = (R.RAT1 + RATE.0527 ) / 2
    IF R.RAT1 LT MIN.0527 THEN
        MIN.RATE.C = R.RAT1
    END ELSE
        MIN.RATE.C = MIN.0527
    END
    IF R.RAT1 GT MAX.0527 THEN
        MAX.RATE.C = R.RAT1
    END ELSE
        MAX.RATE.C = MAX.0527
    END
    R.RAT1 = 0
*-----------------*
    AMOUNT.D = AMT.2.D
    CATG.ID = "21006" : TT
    CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 375 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
    FOR II = 1 TO N.COUNT
        IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
            RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
            BB = II
            II = N.COUNT
            R.RAT1 = R.CUS<LDTL.RATE><1,BB>
        END
    NEXT II

    W.D  = R.RAT1
    R.RAT1 = 0
*---------------*
    AMOUNT.E = AMT.2.E
    CATG.ID = "21007" : TT
    CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 392 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
    FOR II = 1 TO N.COUNT
        IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
            RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
            BB = II
            II = N.COUNT
            R.RAT1 = R.CUS<LDTL.RATE><1,BB>
        END
    NEXT II

    W.E  = R.RAT1
    R.RAT1 = 0
*--------------------------*
    AMOUNT.F = AMT.2.F
    CATG.ID = "21010" : TT
    CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 409 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
    FOR II = 1 TO N.COUNT
        IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
            RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
            BB = II
            II = N.COUNT
            R.RAT1 = R.CUS<LDTL.RATE><1,BB>
        END
    NEXT II

    W.F  = R.RAT1
    R.RAT1 = 0
*--------------------------------------------------*
    Y.RET.DATA<-1> = AMOUNT.1:"*":W.1:"*":MIN.RATE.1:"*":MAX.RATE.1:"*":AMOUNT.A:"*":W.A:"*":W.A:"*":W.A:"*":AMOUNT.B:"*":W.B:"*":W.B:"*":W.B:"*":AMOUNT.C:"*":W.C:"*":MIN.RATE.C:"*":MAX.RATE.C:"*":AMOUNT.D:"*":W.D:"*":W.D:"*":W.D:"*":AMOUNT.E:"*":W.E:"*":AMOUNT.F:"*":W.F:"*":W.F:"*":W.F:"*":AMOUNT.3:"*":W.3:"*":MIN.RATE.3:"*":MAX.RATE.3:"*":AMOUNT.4:"*":W.4:"*":MIN.RATE.4:"*":MAX.RATE.4:"*":AMOUNT.5:"*":W.5:"*":MIN.RATE.5:"*":MAX.RATE.5
*  PRINT     AMOUNT.1:"*":W.1:"*":MIN.RATE.1:"*":MAX.RATE.1:"*":AMOUNT.A:"*":W.A:"*":W.A:"*":W.A:"*":AMOUNT.B:"*":W.B:"*":W.B:"*":W.B:"*":AMOUNT.C:"*":W.C:"*":MIN.RATE.C:"*":MAX.RATE.C:"*":AMOUNT.D:"*":W.D:"*":W.D:"*":W.D:"*":AMOUNT.F:"*":W.F:"*":W.F:"*":W.F:"*":AMOUNT.3:"*":W.3:"*":MIN.RATE.3:"*":MAX.RATE.3:"*":AMOUNT.4:"*":W.4:"*":MIN.RATE.4:"*":MAX.RATE.4:"*":AMOUNT.5:"*":W.5:"*":MIN.RATE.5:"*":MAX.RATE.5
*--------------------------------------------------*
GETCATEG:
*********
    Z = 1
    CALL F.READ(FN.LN,LN.ID,R.LN,F.LN,ERR.LN)
*Line [ 430 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    PROD.NO = DCOUNT(R.LN<RE.SRL.ASSET1>,@VM)

    FOR XX = 1 TO PROD.NO
        PRODUCT = R.LN<RE.SRL.ASSET1,XX>
        IF PRODUCT[1,1] EQ '*' THEN
            MM++
            GOSUB GETRANGE
        END ELSE
            CATEG = CATEG:" ":R.LN<RE.SRL.ASSET1,XX>

            NESS = R.LN<RE.SRL.ASSET1,XX>

            FINDSTR NESS IN ARR.CAT SETTING NESS1 THEN NULL ELSE
                ARR.CAT<1,Z> = NESS
                Z = Z + 1
            END
        END
    NEXT XX
RETURN
***********************************************
GETRANGE:
********
    SS = LEN(PRODUCT) - 1
    RG.ID = PRODUCT[2,SS]
    CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
    CATFR = R.RG<RE.RNG.START.RANGE>
    CATTO = R.RG<RE.RNG.END.RANGE>
    IF MM EQ 1 THEN
        ORR = ''
    END ELSE
        ORR = ' OR '
    END
    DIFF.CAT = CATTO - CATFR

    NESS = CATFR

    FINDSTR NESS IN ARR.CAT SETTING NESS1 THEN NULL ELSE
        ARR.CAT<1,Z> = NESS
        Z = Z + 1
    END

* ARR.CAT<1,Z> = CATFR
* Z = Z + 1
    FOR NN = 1 TO DIFF.CAT

        NESS = CATFR + NN

        FINDSTR NESS IN ARR.CAT SETTING NESS1 THEN NULL ELSE
            ARR.CAT<1,Z> = NESS
            Z = Z + 1
        END
    NEXT NN
    CAT.CRIT = CAT.CRIT:ORR:" ( CATEGORY GE ":CATFR:" AND CATEGORY LE ":CATTO:" )"
RETURN
*-----------------------*
CALC.RATE:
    FOR CZ = 1 TO Z
        CATG.ID = ARR.CAT<1,CZ> : TT
        CAT.ID =  ARR.CAT<1,CZ>
        CALL F.READ(FN.CAT,CAT.ID,R.CAT,F.CAT,ERR.CAT)
        APP.ID = R.CAT<EB.CAT.SYSTEM.IND>
        IF APP.ID EQ 'LD' THEN
            CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 494 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
            FOR II = 1 TO N.COUNT
                IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
                    RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
                    BB = II
                    II = N.COUNT
                    R.RAT1 = R.CUS<LDTL.RATE><1,BB>
                END
            NEXT II
            IF R.MIN EQ ''  THEN
                R.MIN = R.RAT1
            END
            IF R.MAX EQ ''  THEN
                R.MAX = R.RAT1
            END
            IF R.MIN > R.RAT1 THEN
                R.MIN = R.RAT1
            END
            IF R.MAX <  R.RAT1 THEN
                R.MAX = R.RAT1
            END
        END
    NEXT CZ
    RATE = RATE.TOT / CZ

    IF APP.ID EQ 'AC' THEN
        MIN = 999999999999  ; MAX = 0

*TEXT = "AC= ":CAT.ID ; CALL REM
        T.SEL1 = "SELECT ":FN.AGC:" WITH VALUE EQ ":CAT.ID:" BY @ID"
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
        IF SELECTED1 THEN
            FOR KK = 1 TO SELECTED1
                CALL F.READ(FN.AGC,KEY.LIST1<KK>,R.AGC,F.AGC,E1)
                GROUP.ID = KEY.LIST1<KK>
                GC.ID = GROUP.ID:"EGP":"..."

                T.SEL2 = "SELECT ":FN.GC:" WITH @ID LIKE ":GC.ID:" BY @ID"
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
                IF SELECTED2 THEN
                    CALL F.READ(FN.GC,KEY.LIST2<SELECTED2>,R.GC,F.GC,E22)
                    INT.RATE = R.GC<IC.GCI.CR.INT.RATE>
                    BAS.MRGN = R.GC<IC.GCI.CR.MARGIN.RATE>
                    FIN.RATE = INT.RATE + BAS.MRGN

                    IF FIN.RATE LE MIN THEN
                        MIN = FIN.RATE
                    END
                    IF FIN.RATE GT MIN THEN
                        MAX = FIN.RATE
                    END
                END
            NEXT KK
        END

**IF MIN EQ 999999999999 THEN MIN = 0

        ARRAY.CATEGORY<1,ZEFT>   = MIN
        ARRAY.CATEGORY<1,ZEFT+1> = MAX
        ZEFT = ZEFT + 1
    END
RETURN
*----------------------------------------------------------------------*
END
