* @ValidationCode : MjotNTAwMjExOTg2OkNwMTI1MjoxNjQwNzkwNzM3Nzc4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:12:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 30/3/2020******************
*-----------------------------------------------------------------------------
* <Rating>775</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CR.10KEGP.P(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
    KEY.LIST.M = "" ; SELECTED.M = "" ;  ER.MSG.M = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0 ; TOT.AMT.LT = 0 ; NARRT = ''
    CUST.NTR = 0  ; REF.NO = '' ; DPST.P = ''

    YTEXT = "���� ������� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    RQ.DATE = COMI


    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 EQ 'EGP' AND RECORD.STATUS EQ 'MAT' AND AUTH.DATE EQ ":RQ.DATE:" AND CONTRACT.GRP NE '' AND CONTRACT.GRP NE 'STAFF' AND CUSTOMER.1 NE '99433300' BY CUSTOMER.1 BY CO.CODE "

    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    CUST<1>         = R.TT<TT.TE.CUSTOMER.1>
    CURR<1>         = R.TT<TT.TE.CURRENCY.1>
    AMT.FCY<1>      = R.TT<TT.TE.AMOUNT.FCY.1>
    AMT.LCY<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1>     = R.TT<TT.TE.AUTH.DATE>
    TT.DATE         = AUTH.DAT<1>
    ACCT<1>         = R.TT<TT.TE.ACCOUNT.1>
    CO.CODE<1>      = R.TT<TT.TE.CO.CODE>
    NARRT<1>        = R.TT<TT.TE.NARRATIVE.2>
    LOCAL.REF.T     = R.TT<TT.TE.LOCAL.REF>
    DPST.P<1>       = LOCAL.REF.T<1,TTLR.DEPOSIT.PURPOSE>
*   KEY.ID<1> = "USD":"-" :"EGP":"-":TT.DATE
*   CALL F.READ(FN.CURR.DALY,KEY.ID<1>,R.CURR.DALY,F.CURR.DALY,ERR2)
*   DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
*   CURR.MARK = R.CURR.DALY<SCCU.CURR.MARKET>
*   LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
*       SELL.RATE<1> = R.CURR.DALY<SCCU.SELL.RATE ,NN>
*   END
*   IF CURR<1> = "USD" THEN
*       TOT.AMT.N = TOT.AMT.N + AMT.FCY<1>
*   END ELSE
****OTHER FCY CURR*******************************
*
*       TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
*   END
**********END OF FIRST RECORD**********************************
    TOT.AMT.N = TOT.AMT.N+ AMT.LCY<1>
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        LOCAL.REF.T     = R.TT<TT.TE.LOCAL.REF>
        ACCT<I>         = R.TT<TT.TE.ACCOUNT.1>
        CO.CODE<I>      = R.TT<TT.TE.CO.CODE>
        NARRT<I>        = R.TT<TT.TE.NARRATIVE.2>
        DPST.P<I>       = LOCAL.REF.T<1,TTLR.DEPOSIT.PURPOSE>
        IF CUST<I> # CUST<I-1> THEN
            CALL F.READ(FN.CUSTOMER, CUST<I-1>, R.CUSTOMER, F.CUSTOMER, E1)
            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
* REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
            NW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
            REF.NO = KEY.LIST.N<I-1>
            IF  TOT.AMT.N GE 10000 THEN
                Y.RET.DATA<-1> = REF.NO:"*":CUST<I-1> :"*":ACCT<I-1>:"*": CURR<I-1>:"*":TOT.AMT.N:"*": AUTH.DAT<I-1>:"*":CO.CODE<I-1>:"*":NARRT<I-1>:"*":DPST.P<I-1>
                TOT.AMT.N = 0
            END ELSE
                TOT.AMT.N = 0
            END
            TOT.AMT.N = TOT.AMT.N+ AMT.LCY<I>
*****SAME CUSTOMER*************
        END ELSE
            TOT.AMT.N = TOT.AMT.N+ AMT.LCY<I>
******END OF SAME CUSTOMER******
        END         ;**END OF SAME CUSTOMER**
****LAST RECORD*******************************************************
        IF I = SELECTED.N THEN
            IF TOT.AMT.N GE 10000 THEN
                TEXT = 'LAST RECORD' ; CALL REM
                CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
*  TOT.AMT.N = TOT.AMT.N+ AMT.LCY<I>
                REF.NO = KEY.LIST.N<I>
                Y.RET.DATA<-1> = REF.NO:"*":CUST<I> :"*":ACCT<I>:"*": CURR<I>:"*":TOT.AMT.N:"*": AUTH.DAT<I>:"*":CO.CODE<I>:"*":NARRT<I> :"*":DPST.P<I>

            END
        END
*************************************************************************
    NEXT I

RETURN
END
