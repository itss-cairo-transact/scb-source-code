* @ValidationCode : MjoxOTAxMzU5OTQ2OkNwMTI1MjoxNjQwODY4NjYxNzE5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:51:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* BY NESSMA MOHAMMAD
*-----------------------------------------------------------------------------
SUBROUTINE CONV.TYPE.PWD

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_ENQUIRY.COMMON
    $INSERT  I_F.PASSWORD.RESET
*-------------------------------------------
    R.PWR = ""   ; E11 = ""
    FN.PWR = "F.PASSWORD.RESET"  ; F.PWR = ""
    CALL OPF(FN.PWR , F.PWR)

    ID.TMP = O.DATA
    CALL F.READ(FN.PWR,ID.TMP, R.PWR, F.PWR, E11)
    RES  = R.PWR<EB.PWR.USER.RESET>

    IF RES EQ '' THEN
        O.DATA = "A"
    END ELSE
        O.DATA = "T"
    END
*-------------------------------------------
RETURN
END
