* @ValidationCode : MjotNTY0MTczMjU2OkNwMTI1MjoxNjQwODc0MjMxMDM1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 16:23:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM CR.MOKHSAS.LC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.MM.MONEY.MARKET
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.DRAWINGS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
*$INSERT I_F.SCB.FUND
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
* $INSERT I_FT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.SCB.CUS.POS.TST
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_CU.LOCAL.REFS
*--------------------------------------------------------*
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

    CALL OPF( FN.ACCOUNT,F.ACCOUNT)
*--------------------------------------------------------*
    GOSUB LC.SUB
RETURN
*-----------------------------*
LC.SUB:
*------
    ID.NO = ""
    FN.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER.OF.CREDIT = '' ; R.LETTER.OF.CREDIT = ''

    CALL OPF(FN.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT)
    PRINT "OPF OK"
    T.SEL2 =  "SELECT FBNK.LETTER.OF.CREDIT WITH APPLICANT.CUSTNO NE '' AND LIABILITY.AMT GT 0 "

    KEY.LIST2=""
    SELECTED2=""
    ER.MSG2=""
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    PRINT SELECTED2
    IF KEY.LIST2 THEN
        FOR I = 1 TO SELECTED2
            CALL F.READ(FN.LETTER.OF.CREDIT,KEY.LIST2<I>, R.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT, ETEXT)

            APL.NO = R.LETTER.OF.CREDIT<TF.LC.APPLICANT.CUSTNO>
            BEN.NO = R.LETTER.OF.CREDIT<TF.LC.BENEFICIARY.CUSTNO>

            IF APL.NO EQ ''  THEN
                CUS.NOO = BEN.NO
            END ELSE
                CUS.NOO = APL.NO
            END
            CALL F.READ(FN.CUSS,CUS.NOO, R.CUSS,F.CUSS, ETEXT1)


            ID.NO = CUS.NOO:"*LC*BNK*":R.LETTER.OF.CREDIT<TF.LC.LC.CURRENCY>:R.LETTER.OF.CREDIT<TF.LC.CATEGORY.CODE>:"*":KEY.LIST2<I>:"*":TODAY

            CURR= R.LETTER.OF.CREDIT<TF.LC.LC.CURRENCY>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100

            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            AMT = R.LETTER.OF.CREDIT<TF.LC.LIABILITY.AMT>

            LCY.AMT = (MID.RATE<1,1> * AMT)
            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>= LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER> = CUS.NOO
            R.COUNT<CUPOS.DEAL.CCY> = R.LETTER.OF.CREDIT<TF.LC.LC.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT> = R.LETTER.OF.CREDIT<TF.LC.LIABILITY.AMT>
            R.COUNT<CUPOS.MATURITY.DATE> = R.LETTER.OF.CREDIT<TF.LC.ADVICE.EXPIRY.DATE>
            R.COUNT<CUPOS.VALUE.DATE> = R.LETTER.OF.CREDIT<TF.LC.ISSUE.DATE>
            R.COUNT<CUPOS.CATEGORY> = R.LETTER.OF.CREDIT<TF.LC.CATEGORY.CODE>
            R.COUNT<CUPOS.SYS.DATE> = TODAY
            R.COUNT<CUPOS.CO.CODE> =  R.LETTER.OF.CREDIT<TF.LC.CO.CODE>

            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
RETURN
END
