* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
******************MAHMOUD 3/10/2012**********************
    SUBROUTINE E.NOF.ALL.CU.AC.TXN(Y.RET.DATA)
***    PROGRAM E.NOF.ALL.CU.AC.TXN

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
*Line [ 38 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 39 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*===========================================
INITIATE:
*--------
    MAX.DAYS = 7
    MM  = 0
    ZZ  = 0
    CRN = 0
    DBN = 0
    DB.ARR   = ''
    CR.ARR   = ''
    RET.REC  = ''
    DATA.ARR = ''
    FROM.DATE = '20081101'
    END.DATE  = TODAY
*#CALL ADD.MONTHS(FROM.DATE,'-48')
*#CRT FROM.DATE:"-":END.DATE
    RETURN
*===========================================
CALLDB:
*------
    FN.CU = "FBNK.CUSTOMER"  ; F.CU = ""
    CALL OPF(FN.CU,F.CU)
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)
    FN.CU.AC = "FBNK.CUSTOMER.ACCOUNT"  ; F.CU.AC = ""
    CALL OPF(FN.CU.AC,F.CU.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    RETURN
*===========================================
PROCESS:
*---------
*    LOCATE "CATEGORY" IN D.FIELDS<1> SETTING YCAT.POS THEN CATEG  = D.RANGE.AND.VALUE<YCAT.POS> ELSE RETURN

    AA.SEL = "SELECT ":FN.CU.AC:" BY COMPANY.CO BY @ID "
    CALL EB.READLIST(AA.SEL,K.LIST,'',SELECTED,ER.MSG)
    CRT SELECTED
    LOOP
        REMOVE CU.ID FROM K.LIST SETTING POS.CU
    WHILE CU.ID:POS.CU
        CALL F.READ(FN.CU.AC,CU.ID,R.CU.AC,F.CU.AC,ER.CU.AC)
        LOOP
            REMOVE ACCT.ID FROM R.CU.AC SETTING POS.AC
        WHILE ACCT.ID:POS.AC
            CALL F.READ(FN.AC,ACCT.ID,R.AC,F.AC,ER.AC)
            AC.ONLINE.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
            AC.CUR         = R.AC<AC.CURRENCY>
            AC.CAT         = R.AC<AC.CATEGORY>
            AC.COM         = R.AC<AC.CO.CODE>
            OP.DATE        = R.AC<AC.OPENING.DATE>
            IF ( AC.CAT GE 1100 AND AC.CAT LE 1599 ) OR ( AC.CAT EQ 1001 ) THEN
                CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
                LOOP
                    REMOVE STE.ID FROM ID.LIST SETTING POS.STE
                WHILE STE.ID:POS.STE
                    IF STE.ID THEN
                        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                        STE.VAL     = R.STE<AC.STE.VALUE.DATE>
                        STE.DAT     = R.STE<AC.STE.BOOKING.DATE>
                        STE.AMT.LCY = R.STE<AC.STE.AMOUNT.LCY>
                        IF STE.AMT.LCY LT 0 THEN
                            IF STE.DAT THEN
                                MAX.VAL = STE.DAT
                                CALL CDT("",MAX.VAL,'+7C')
                            END
*-----------------------------------
                            IF STE.VAL GT MAX.VAL THEN
                                STE.OUR.REF = R.STE<AC.STE.OUR.REFERENCE>
                                IF NOT(STE.OUR.REF) THEN STE.OUR.REF = R.STE<AC.STE.TRANS.REFERENCE>
                                STE.AC.NO   = R.STE<AC.STE.ACCOUNT.NUMBER>
                                STE.TR.TYPE = R.STE<AC.STE.TRANSACTION.CODE>
                                STE.CUR     = R.STE<AC.STE.CURRENCY>
                                IF STE.CUR EQ LCCY THEN
                                    STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                                END ELSE
                                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                                END
                                STE.INP     = FIELD(R.STE<AC.STE.INPUTTER>,'_',2)
                                STE.ATH     = FIELD(R.STE<AC.STE.AUTHORISER>,'_',2)
                                IF STE.INP NE 'INPUTTCOB' THEN
                                    GOSUB SEND.REC
                                END
                            END
                        END
                    END
*-----------------------------------
                REPEAT
            END
        REPEAT
    REPEAT
    RETURN
*=============================================
SEND.REC:
*---------
    RET.REC1 = ACCT.ID:"*":STE.VAL:"*":STE.DAT:"*":STE.CUR:"*":STE.INP:"*":STE.OUR.REF:"*":STE.AMT
    CRT RET.REC1
    RET.REC = AC.COM:"*":CU.ID:"*":STE.CUR:"*":STE.DAT:"*":ACCT.ID:"*":STE.VAL:"*":STE.OUR.REF:"*":STE.AMT:"*":STE.AMT.LCY:"*":STE.INP:"*":STE.ATH:"*":ACCT.ID
    Y.RET.DATA<-1> = RET.REC
    RETURN
**********************************************
END
