* @ValidationCode : MjotMTk5MDc4ODUxNjpDcDEyNTI6MTY0MDc4NjkwOTM4Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:08:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
SUBROUTINE DR.MOH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - Missing Layout I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='DR.MOH'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DR='FBNK.STMT.ACCT.DR';F.DR=''
    CALL OPF(FN.DR,F.DR)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    DB.MOV2 = 0
    CR.MOV2 = 0
    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')

    YTEXT = "Enter Account No. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    ID = COMI
    TEXT = "ID :":ID ; CALL REM
*********************************************
    YTEXT = "Enter Date. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    ID2 = COMI
***  ID = COMI:"-":DAT
    TEXT = "ID2 :":ID2 ; CALL REM
    ID3  = ID:"-":ID2
    TEXT = "ID3 :":ID3  ; CALL REM
*------------------------------------------------------------------------
*T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...":DAT
*T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID EQ '0130148410150203-20081031'"
    T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID EQ ":ID3

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL : " : SELECTED ; CALL REM
    IF SELECTED THEN
*TEXT = 'SELECTED = ':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.DR,KEY.LIST<I>,R.DR,F.DR,E2)
            DR.ID = KEY.LIST<I>
            ACC.NO = FIELD(DR.ID,'-',1)
            INT.DATE = FIELD(DR.ID,'-',2)
            V.DATE  = INT.DATE[7,2]:'/':INT.DATE[5,2]:"/":INT.DATE[1,4]
            DATE2 = INT.DATE[1,6]
            ACTIVE.ID = ACC.NO:'-':DATE2
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BRANCH.ID=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            CATEG.ID  = ACC.NO[11,4]
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>

            DR.AMOUNT = R.DR<IC.STMDR.HIGHEST.DR.BAL>
            TOT.INT = R.DR<IC.STMDR.GRAND.TOTAL>
            TOT.AMT = R.DR<IC.STMDR.HIGHEST.DR.AMT>
            INT.AMT = R.DR<IC.STMDR.DR.INT.RATE>

*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCT.ACTIVITY':@FM:IC.ACT.TURNOVER.DEBIT,ACTIVE.ID,DB.MOV)
F.ITSS.ACCT.ACTIVITY = 'FBNK.ACCT.ACTIVITY'
FN.F.ITSS.ACCT.ACTIVITY = ''
CALL OPF(F.ITSS.ACCT.ACTIVITY,FN.F.ITSS.ACCT.ACTIVITY)
CALL F.READ(F.ITSS.ACCT.ACTIVITY,ACTIVE.ID,R.ITSS.ACCT.ACTIVITY,FN.F.ITSS.ACCT.ACTIVITY,ERROR.ACCT.ACTIVITY)
DB.MOV=R.ITSS.ACCT.ACTIVITY<IC.ACT.TURNOVER.DEBIT>
*Line [ 113 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DB = DCOUNT(DB.MOV,@VM)
            FOR X = 1 TO DB
                DB.MOV2 += DB.MOV<1,X>
            NEXT X

*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCT.ACTIVITY':@FM:IC.ACT.TURNOVER.CREDIT,ACTIVE.ID,CR.MOV)
F.ITSS.ACCT.ACTIVITY = 'FBNK.ACCT.ACTIVITY'
FN.F.ITSS.ACCT.ACTIVITY = ''
CALL OPF(F.ITSS.ACCT.ACTIVITY,FN.F.ITSS.ACCT.ACTIVITY)
CALL F.READ(F.ITSS.ACCT.ACTIVITY,ACTIVE.ID,R.ITSS.ACCT.ACTIVITY,FN.F.ITSS.ACCT.ACTIVITY,ERROR.ACCT.ACTIVITY)
CR.MOV=R.ITSS.ACCT.ACTIVITY<IC.ACT.TURNOVER.CREDIT>
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CR = DCOUNT(CR.MOV,@VM)
            FOR J = 1 TO CR
                CR.MOV2 += CR.MOV<1,J>
            NEXT J

*------------------------------------------------------------------------
*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCT.ACTIVITY':@FM:IC.ACT.BALANCE,ACTIVE.ID,BAL)
F.ITSS.ACCT.ACTIVITY = 'FBNK.ACCT.ACTIVITY'
FN.F.ITSS.ACCT.ACTIVITY = ''
CALL OPF(F.ITSS.ACCT.ACTIVITY,FN.F.ITSS.ACCT.ACTIVITY)
CALL F.READ(F.ITSS.ACCT.ACTIVITY,ACTIVE.ID,R.ITSS.ACCT.ACTIVITY,FN.F.ITSS.ACCT.ACTIVITY,ERROR.ACCT.ACTIVITY)
BAL=R.ITSS.ACCT.ACTIVITY<IC.ACT.BALANCE>
*Line [ 128 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            BALD = DCOUNT(BAL,@VM)

            FOR K = 1 TO BALD
                BAL1 = BAL<1,1>
                BAL2 = BAL<1,BALD>
            NEXT K
*------------------------------------------------------------------------


            XX   = SPACE(132)  ; XX3  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,15]   = "��� ������   : ":' ':ACC.NO
            XX<1,1>[45,35]  = '����� : ':' ':CUST.NAME


            XX1<1,1>[3,15]  = '����������     : ':' ':CATEG
            XX5<1,1>[3,15] = '��������     : ':' ':CUR

            XX2<1,1>[3,15] = '����� ������� : ':' ':V.DATE


            XX3<1,1>[3,15] = '���� ���� ���� : ':' ':DR.AMOUNT
            XX3<1,1>[45,15] = '���� �������  : ':' ':TOT.INT

            XX4<1,1>[3,15] = '���� �������':' ':TOT.AMT
            XX4<1,1>[45,15] = '��� �������   : ':' ':INT.AMT

*XX5<1,1>[3,15]  = '����� ���� ����� : ':DB.MOV2
*XX5<1,1>[45,15] = '����� ���� �����':CR.MOV2

            XX6<1,1>[3,15]  = '���� ������� : ':BAL1
            XX6<1,1>[45,15]  = '���� ����� : ':BAL2
*-------------------------------------------------------------------
*Line [ 226 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN :SPACE(10):"������� �� ������ ����� ����� ������"
**** PR.HD :="'L'":SPACE(30)"������� �� ������ ����� ����� ������"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX5<1,1>
            PRINT XX2<1,1>
            PRINT STR(' ',82)
            PRINT STR('=',82)
            PRINT STR(' ',82)
            PRINT XX3<1,1>
            PRINT XX4<1,1>
*PRINT XX5<1,1>
            PRINT XX6<1,1>
            PRINT STR('-',82)
        NEXT I
    END
*===============================================================
RETURN
END
