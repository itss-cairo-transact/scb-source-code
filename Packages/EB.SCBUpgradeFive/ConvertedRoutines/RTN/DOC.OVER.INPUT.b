* @ValidationCode : MjoxOTEyMDQwMjcwOkNwMTI1MjoxNjQwNzg2MzU2OTQzOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 15:59:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
*--------------------------------NI7OOOOOOOOOO---------------------------------------------

SUBROUTINE DOC.OVER.INPUT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.DOCUMENT.PROCURE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 37 ] Removed directory from $INCLUDE - Missing Layout I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_AC.LOCAL.REFS


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    ACC2    = R.NEW(DOC.PRO.DEBIT.ACCT)
    AMT22   = COMI
*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DC.AMT2 = DCOUNT(AMT22,@VM)
    ZZZ = '0000000'
***************************************************************************
    FOR I = 1 TO DC.AMT2
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC2,BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC2,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC2,CUS.LIM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.LIM=R.ITSS.ACCOUNT<AC.LIMIT.REF>

        LIMM = FIELD(CUS.LIM,'.',1)
        AA = 7 - LEN(LIMM)
        XX = CUS.ID:'.':ZZZ[1,AA]:CUS.LIM

        T.SEL2 = "SELECT FBNK.LIMIT WITH @ID EQ ":XX
        CALL EB.READLIST(T.SEL2,KEY.LIST,"",SELECTED,ER.MSG)

        CALL F.READ(FN.LIM,KEY.LIST,R.LIM,F.LIM,E2)
        LIMIT.AMT = R.LIM<LI.AVAIL.AMT>

        AMT2      = AMT22

        TOTAL.DB  = BAL - AMT2

        TOTAL.CR  = TOTAL.DB + LIMIT.AMT
*** IF BAL LT AMT2 THEN
        IF TOTAL.CR LE 0 THEN
            ETEXT = "������ ����� ��� ������ ������ �� ���"
        END
        AMT2 = ''
    NEXT I
***************************************************************************

RETURN
END
