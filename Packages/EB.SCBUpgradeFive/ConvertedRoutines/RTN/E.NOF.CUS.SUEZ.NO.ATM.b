* @ValidationCode : MjoxODExODgyOTczOkNwMTI1MjoxNjQwODY2MDU1Mjk2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:07:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***NESSREEN AHMED 09/11/2014*********************************
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CUS.SUEZ.NO.ATM(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - Missing Layout I_CI.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_CI.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.VISA.CARD.TYPE - ITSS - R21 Upgrade - 2021-12-23
*   $INCLUDE I_F.SCB.VISA.CARD.TYPE
*Line [ 42 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.ATM.APP - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_F.SCB.ATM.APP
*-----------------------------------------------------------------------*
    FN.CUST = 'F.CUSTOMER' ; F.CUST = '' ; R.CUST = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.CUST,F.CUST)

    FN.ATM.APP = 'F.SCB.ATM.APP' ; F.ATM.APP = '' ; R.ATM.APP = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.ATM.APP,F.ATM.APP)

    N.SEL = "SELECT FBNK.CUSTOMER WITH (BANK NE '' AND  NE '0017' )AND SCCD.CUSTOMER EQ 'YES' BY BANK "

    KEY.LIST.N =""
    SELECTED.N =""
    ER.MSG.N =""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    TEXT = 'SEL.ATM=':SELECTED.N ; CALL REM

    FOR SS = 1 TO SELECTED.N

        CALL F.READ(FN.CUST, KEY.LIST.N<SS>, R.CUST, F.CUST, E2)
        LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>
        CUST.BANK<SS> = LOCAL.REF<1,CULR.CU.BANK>

        T.SEL = "SELECT F.SCB.ATM.APP WITH ATM.APP.DATE GE '20140904' AND CUSTOMER EQ " : KEY.LIST.N<SS>

        KEY.LIST=""
        SELECTED=""
        ER.MSG=""

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF NOT(SELECTED) THEN
            Y.RET.DATA<-1> = KEY.LIST.N<SS> :"*":CUST.BANK<SS>
**  TEXT = "BANK=":CUST.BANK<SS> ; CALL REM
        END

    NEXT SS
*-----------------------------------------------------------------------*
RETURN
END
