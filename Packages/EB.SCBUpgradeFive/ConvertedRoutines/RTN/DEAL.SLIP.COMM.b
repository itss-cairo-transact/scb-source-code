* @ValidationCode : Mjo0Nzc3NDU1OTk6Q3AxMjUyOjE2NDA3ODIxOTQ3NjU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:49:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE

*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE DEAL.SLIP.COMM(ARG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.LG.CHARGE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE
  
    MYID = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>:".":R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.TAX.AMOUNT,MYID,MYAMT)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
MYAMT=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.TAX.AMOUNT>
    COM1 = MYAMT<1,1>
    COM2 = MYAMT<1,2>
    MYSTAMP = COM1 +COM2
*MYSTAMP = SUM(R.NEW(SCB.LG.CH.TAX.AMOUNT))
    ARG = SUM(R.NEW(LD.CHRG.AMOUNT))
    ARG = ARG + MYSTAMP
* ARG = FMT(ARG,"19R,/2")
 
RETURN
END
