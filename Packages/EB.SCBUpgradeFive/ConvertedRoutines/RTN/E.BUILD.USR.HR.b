* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
    SUBROUTINE E.BUILD.USR.HR(ENQUIRY.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.LIABILITY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    LOCATE 'ACCOUNT' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        ACCC = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END

    FN.ACC = 'FBNK.ACCOUNT' ;F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    CALL F.READ(FN.AC,ACCC,R.ACC,F.ACC,READ.ERR)
    CUS.ID = R.ACC<AC.CUSTOMER>
    CATEG = R.ACC<AC.CATEGORY>

********************************
    FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""

    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,READ.ERR1)

    LOOP
        REMOVE AC.ID FROM R.CUS SETTING POS.CUST
    WHILE AC.ID:POS.CUST

        IF AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 THEN
            ENQUIRY.DATA<2,1> = 'ACCOUNT'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET INFORMATION'
        END
    REPEAT

********************************


    RETURN
END
