* @ValidationCode : MjotMTYxMjg1Nzk4MjpDcDEyNTI6MTY0MDc4Mjc5NjIwODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:59:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM DEPOSIT.R.TEXT.3

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.LD.RATE.PREPAID - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.RATE.PREPAID
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "/home/signat" , "DEPOSIT_PRICES3" TO BB THEN
        CLOSESEQ BB

        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"DEPOSIT_PRICES3"
        HUSH OFF
    END
    OPENSEQ "/home/signat" , "DEPOSIT_PRICES3" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DEPOSIT_PRICES3 CREATED IN /home/signat'
        END ELSE
            STOP 'Cannot create DEPOSIT_PRICES3 File IN /home/signat'
        END
    END

RETURN

*========================================================================
PROCESS:
    FN.EH = 'F.SCB.LD.RATE.PREPAID' ; F.EH = ''
    CALL OPF(FN.EH,F.EH)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
*-------------------------------------------

    T.SEL  = "SELECT ":FN.EH:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CALL F.READ(FN.EH,KEY.LIST<SELECTED>,R.EH,F.EH,E1)

        WS.CCY = 'EGP'

        WS.RATE.1M  = R.EH<LRP.RATE.1M>
        WS.RATE.2M  = R.EH<LRP.RATE.2M>
        WS.RATE.3M  = R.EH<LRP.RATE.3M>
        WS.RATE.6M  = R.EH<LRP.RATE.6M>
        WS.RATE.9M  = R.EH<LRP.RATE.9M>
        WS.RATE.12M = R.EH<LRP.RATE.12M>


        BB.DATA  = WS.CCY:" |"
        BB.DATA := WS.RATE.1M:" |"
        BB.DATA := WS.RATE.2M:" |"
        BB.DATA := WS.RATE.3M:" |"
        BB.DATA := WS.RATE.6M:" |"
        BB.DATA := WS.RATE.9M:" |"
        BB.DATA := WS.RATE.12M:" |"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END


    END
RETURN
END
