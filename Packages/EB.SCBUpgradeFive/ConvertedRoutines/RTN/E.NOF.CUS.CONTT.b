* @ValidationCode : MjotMTU5NTk2NzU2NDpDcDEyNTI6MTY0MTkxMzI4OTIxNTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 11 Jan 2022 17:01:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1243</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CUS.CONTT(Y.RET.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
    $INCLUDE I_F.STMT.ENTRY   ;*AC.STE.
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
**------------------------------------
    LOCATE "ACCT" IN D.FIELDS<1> SETTING ACC.POS THEN
        ACCTT  = D.RANGE.AND.VALUE<ACC.POS>
    END
    LOCATE "FROM.DATE" IN D.FIELDS<1> SETTING FOR.POS THEN
        F.DAT  = D.RANGE.AND.VALUE<FOR.POS>
    END
    LOCATE "TO.DATE" IN D.FIELDS<1> SETTING TO.POS THEN
        T.DAT  = D.RANGE.AND.VALUE<TO.POS>
    END
**------------------------------------
    FRDT     = F.DAT
    TODT     = T.DAT
    Y.ACC.ID = ACCTT
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 66 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 67 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
*-------------------------------------------------------------------------
RETURN
*==============================================================
INITIATE:
    CUST.NAME    = ''
    Y.OPEN.BAL   = 0
    Y.LAST.BAL   = 0
    Y.TOT.DR     = 0
    Y.TOT.CR     = 0
    Y.MAX.DR.BAL = 0
    Y.MIN.DR.BAL = 0
    Y.COMM       = 0
    K = 0
    MMM = 0
    JJ = 0
    PRVBAL = 0
    PRVBAL1= 0
    BAL1 = 0
    BAL2 = 0
    DB.MOV = 0
    DB.MOV2= 0
    DB.MOV3= 0
    CR.MOV = 0
    CR.MOV2= 0
    BAL.AC = 0
    CBAL   = 0
    STE.REF = ''
    STE.TTYP = ''
    STE.DR.CUST = ''
    STE.CR.CUST = ''
    TOT.INT = 0
    TOT.COM = 0
    TOTAL.INT = 0
    TOTAL.COM = 0
    JJ  = 0
    ACC.NX = ''
    CUS.NX = ''
    XX  = SPACE(130)
    XX1 = SPACE(130)
    TD1 = TODAY

RETURN
*===============================================================
CALLDB:
    FN.ACC = 'FBNK.ACCOUNT'       ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.STE = 'FBNK.STMT.ENTRY'    ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.BIL = 'FBNK.BILL.REGISTER' ; F.BIL = '' ; R.BIL = ''
    CALL OPF(FN.BIL,F.BIL)
    FN.BAT = 'F.SCB.BT.BATCH' ; F.BAT = '' ; R.BAT = ''
    CALL OPF(FN.BAT,F.BAT)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST.STE ="" ; SELECTED.STE ="" ;  ER.MSG.STE =""
*************************************************************************
*========================================================================
    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACC,F.ACC,Y.ACC.ERR)
    CUS.ID    = R.ACC<AC.CUSTOMER>
    AC.COM    = R.ACC<AC.CO.CODE>
    Y.CURR    = R.ACC<AC.CURRENCY>
    Y.CAT     = R.ACC<AC.CATEGORY>
    MMM       = 0
    TT        = 0
    TTX       = 0
    PRVBAL    = 0
    PRVBAL1   = 0
    BAL1      = 0
    BAL2      = 0
    DB.MOV    = 0
    DB.MOV2   = 0
    DB.MOV3   = 0
    CR.MOV    = 0
    CR.MOV2   = 0
    TOT.INT   = 0
    TOT.COM   = 0
    TOTAL.INT = 0
    TOTAL.COM = 0
    CBAL      = 0
    STE.AMT   = 0
    MORT11    = 0
    MORT22    = 0
    MORT      = 0
    MOHS      = 0
    BR.CM     = 0
    BR.CH     = 0
    TOT.CM    = 0
    TOT.CH    = 0
*************************************
    CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,FRDT,TODT,STE.LIST,PRVBAL,ER111)

    LOOP
        REMOVE Y.STE.ID FROM STE.LIST SETTING POS2
    WHILE Y.STE.ID:POS2
        JJ++
        CALL F.READ(FN.STE,Y.STE.ID,R.STE,F.STE,Y.STE.ERR)
        STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
        STE.STA = R.STE<AC.STE.RECORD.STATUS>
        STE.BOOK.DATE = R.STE<AC.STE.BOOKING.DATE>
        IF Y.CURR NE 'EGP' THEN
            STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
        END ELSE
            STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
        END
****************************************************************
        BR.ID = R.STE<AC.STE.OUR.REFERENCE>
        CALL F.READ(FN.BIL,BR.ID,R.BIL,F.BIL,BIL.ERR)
        IF R.BIL THEN
            BR.CM = 0
            BR.CH = 0
            IF STE.AMT LT 0 THEN
                DB.MOV2 += STE.AMT
            END ELSE
                CR.MOV2 += STE.AMT
            END
****************************************************************
            CBAL += STE.AMT
**--------------------------------------------------------------
            BIL.STA     = R.BIL<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
            REASON.CODE = R.BIL<EB.BILL.REG.LOCAL.REF,BRLR.RETURN.REASON>
            BILL.AMT  = R.BIL<EB.BILL.REG.AMOUNT>
******MAHMOUD 10/5/2011***********
            IF ( BIL.STA EQ '7'  OR BIL.STA EQ '14' OR BIL.STA EQ '8' OR BIL.STA EQ '15' OR BIL.STA EQ '13' ) THEN
                BILL.CUR  = R.BIL<EB.BILL.REG.CURRENCY>
                BILL.COMM = R.BIL<EB.BILL.REG.LOCAL.REF,BRLR.COMM.CCY.AMT>
                BILL.CHRG = R.BIL<EB.BILL.REG.LOCAL.REF,BRLR.CH.CCY.AMT>
*#                IF BIL.STA EQ '8'  OR BIL.STA EQ '15' THEN
*Line [ 195 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                COMM.CNT  =  DCOUNT(BILL.COMM,@SM)
*Line [ 197 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                CHRG.CNT  =  DCOUNT(BILL.CHRG,@SM)
                FOR CM = 1 TO COMM.CNT
                    BR.COMMS = R.BIL<EB.BILL.REG.LOCAL.REF,BRLR.COMM.CCY.AMT,COMM.CNT>
                    FINDSTR BILL.CUR IN BR.COMMS SETTING POS.CM THEN BR.CM += FIELD(BR.COMMS,BILL.CUR,2) ELSE BR.CM += BR.COMMS
                NEXT CM
                FOR CH = 1 TO CHRG.CNT
                    BR.CHRGS = R.BIL<EB.BILL.REG.LOCAL.REF,BRLR.CH.CCY.AMT,CHRG.CNT>
                    FINDSTR BILL.CUR IN BR.CHRGS SETTING POS.CH THEN BR.CH += FIELD(BR.CHRGS,BILL.CUR,2) ELSE BR.CH += BR.CHRGS
                NEXT CH
*#                END
                TOT.CM += BR.CM
                TOT.CH += BR.CH
            END
**********************************
            IF ( BIL.STA EQ '7'  OR BIL.STA EQ '14' ) THEN
                IF STE.AMT LT 0 THEN
                    MORT += STE.AMT
                    IF REASON.CODE NE 313 THEN
                        MORT11 += STE.AMT
                    END ELSE
                        MORT22 += STE.AMT
                    END
                END
            END
            IF ( BIL.STA EQ '8' OR BIL.STA EQ '15' OR BIL.STA EQ '13' ) THEN
                IF STE.AMT LT 0 THEN
                    MOHS += STE.AMT
                END
            END
        END
    REPEAT
    GOSUB RET.DATA
RETURN
**--------------------------------------------------------------------
RET.DATA:
*--------
    BAL2 = PRVBAL + DB.MOV2 + CR.MOV2
    ACC.NX = Y.ACC.ID
    CUS.NX = CUS.ID

    DB.MOV3          = ABS(MOHS) + ABS(MORT11)

    PERC.MOHS       =  ABS(( MOHS    ) / ( DB.MOV3  )) * 100
    PERC.MORT       =  ABS(( MORT11  ) / ( DB.MOV3  )) * 100

    Y.RET.DATA<-1>  =  Y.ACC.ID :"*":PRVBAL:"*":BAL2:"*":CR.MOV2:"*":MORT11:"*":MOHS:"*":PERC.MOHS:"*":PERC.MORT:"*":MORT22:"*":TOT.CM:"*":TOT.CH

RETURN
*==============================================================
END
