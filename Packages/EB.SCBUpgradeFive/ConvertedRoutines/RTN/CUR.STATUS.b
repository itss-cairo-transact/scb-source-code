* @ValidationCode : MjoxMDQxNzUwMTE4OkNwMTI1MjoxNjQwNjk4MjQxOTMyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:30:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>1651</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

SUBROUTINE CUR.STATUS

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.PARMS not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.PARMS

*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE          ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL        ;* Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION      ;* Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

*       IF 21000 > ID.NEW[1,5]  OR ID.NEW[1,5] > 21300 THEN
*         E = 'This.Is.Wrong.Category' ; CALL ERR ; MESSAGE = 'REPEAT'
*       END
*         CURR = ''
*        CALL DBR('CURRENCY':@FM:@ID,ID.NEW[6,3],CURR)
*         IF ETEXT THEN E = 'This.Is.Wrong.CURRENCY' ; CALL ERR ; MESSAGE = 'REPEAT'
*         MM = ''
*         MM = TODAY
*         IF NOT(ID.NEW[9,11]) THEN ID.NEW[9,11] = MM
*_________________________________________________________________________________________
    E = ''
    V$ERROR = ''
*---- CATEGORY PART ----
    ID.CATEGORY = ID.NEW[ 1, 5]
    IF NOT(E) THEN
        ETEXT = ''
*Line [ 224 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME:@FM:'L',ID.CATEGORY,CATEGORY.ENRI)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,ID.CATEGORY,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEGORY.ENRI=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME:@FM:'L'>
**  IF ETEXT THEN ETEXT = '' ; E = 'CATEGORY (&) DOES NOT EXIT' : @FM : ID.CATEGORY
** ELSE
*** IF ID.CATEGORY > 21300 OR ID.CATEGORY < 21000 THEN E = 'CATEGORY (&) NOT SUPPORTED' :@FM: ID.CATEGORY
** END
** END
*----- CURRENCY PART ----
        ID.CCY = ID.NEW[ 6, 3]
        IF NOT( E) THEN
            ETEXT = ''
*Line [ 240 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME:@FM:'L',ID.CCY,CCY.ENRI)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,ID.CCY,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CCY.ENRI=R.ITSS.CURRENCY<EB.CUR.CCY.NAME:@FM:'L'>
            IF ETEXT THEN ETEXT = '' ; E = 'CURRENCY (&) DOES NOT EXIT' : @FM : ID.CCY
        END
*---- DATE PART ----
        ID.DATE = ID.NEW[ 9, 100]
        IF NOT(E) THEN
            IF NOT(ID.DATE) AND V$FUNCTION = 'I' THEN ID.DATE = TODAY ; ID.NEW = ID.CATEGORY : ID.CCY : ID.DATE
            ELSE
                NOTHING = ICONV( ID.DATE, 'D4')
                IF STATUS() THEN E = 'BAD DATE FORMAT'
                ELSE
                    IF ID.DATE > TODAY THEN E = 'FUTURE DATE NOT ALLOWED'
                END
            END
        END

*--- --- ---
        ID.ENRI = CATEGORY.ENRI : ' ' : CCY.ENRI
        HEADER = ''
        HEADER.LINE = 0
        HEADER.LINE = 0

* HEADER.LINE += 1 ; HEADER< HEADER.LINE ,1> = 3 ; HEADER< HEADER.LINE, 2> = 1 ; HEADER< HEADER.LINE, 3> = ID.CATEGORY:' ' : CATEGORY.ENRI
* HEADER.LINE += 1 ; HEADER< HEADER.LINE ,1> = 3 ; HEADER< HEADER.LINE, 2> = 2 ; HEADER< HEADER.LINE, 3> = ID.CCY :' ': CCY.ENRI

        HEADER.LINE = 1 ; HEADER< HEADER.LINE ,1> = ID.CATEGORY:' ' : CATEGORY.ENRI
        HEADER.LINE = 1 ; HEADER< HEADER.LINE ,1> = ID.CCY :' ': CCY.ENRI

        CALL UPDATE.HEADER( HEADER)
* --- --- ---

*_________________________________________________________________________________________

        IF E THEN CALL ERR ; V$ERROR = 1

        RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

        RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

*  FOR I = 1 TO DCOUNT(R.NEW(SCB.PAR.MIN.AMT),VM)
*         J = I - 1
*            IF R.NEW(SCB.PARMS.MIN.AMT)<1,I> < R.NEW(SCB.PARMS.MIN.AMT)<1,J> THEN
*             ETEXT = 'Enter.Smaller.Amount'
*            END
*
*        NEXT I

        IF E THEN
            T.SEQU = "IFLD"
            CALL ERR
        END

        RETURN

*************************************************************************

CROSS.VALIDATION:

*
        V$ERROR = ''
        ETEXT = ''
        TEXT = ''
*
REM > CALL XX.CROSSVAL

*     FOR I = 2 TO DCOUNT(R.NEW(SCB.PAR.MIN.AMT),VM)
*         J = I - 1
*            IF R.NEW(SCB.PARMS.MIN.AMT)<1,I> < R.NEW(SCB.PARMS.MIN.AMT)<1,J> THEN
*             ETEXT = 'Enter.Smaller.Amount'
*            END
*
*        NEXT I
*
* If END.ERROR has been set then a cross validation error has occurred
*
        IF END.ERROR THEN
            A = 1
            LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
            T.SEQU = A
            V$ERROR = 1
            MESSAGE = 'ERROR'
        END
        RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
        V$ERROR = ''
        ETEXT = ''
        TEXT = ''
REM > CALL XX.OVERRIDE
*

*
        IF TEXT = "NO" THEN       ;* Said NO to override
            V$ERROR = 1
            MESSAGE = "ERROR"     ;* Back to field input

        END
        RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


        RETURN

*************************************************************************

CHECK.DELETE:


        RETURN

*************************************************************************

CHECK.REVERSAL:


        RETURN

*************************************************************************
DELIVERY.PREVIEW:

        RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

        IF TEXT = "NO" THEN       ;* Said No to override
            CALL TRANSACTION.ABORT          ;* Cancel current transaction
            V$ERROR = 1
            MESSAGE = "ERROR"     ;* Back to field input
            RETURN
        END

*
* Additional updates should be performed here
*
REM > CALL XX...



        RETURN

*************************************************************************

AFTER.UNAU.WRITE:


        RETURN

*************************************************************************

AFTER.AUTH.WRITE:


        RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

        BEGIN CASE
            CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
            CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

        END CASE

        RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

        IF INDEX('V',V$FUNCTION,1) THEN
            E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
            CALL ERR
            V$FUNCTION = ''
        END

        RETURN

*************************************************************************

INITIALISE:

        RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

        MAT F = ""  ; MAT N = "" ;  MAT T = ""
        MAT CHECKFILE = "" ; MAT CONCATFILE = ""
        ID.CHECKFILE = ""
        ID.CONCATFILE = ""

        ID.F  = "CATEGORY.CCY.DATE"; ID.N  = "16.8"; ID.T  = "A"
        ID.T< 4> = 'R##### ### DDDDDDDD'

        Z = 0

        Z+=1 ; F(Z)= "DESCRIPION"        ; N(Z) = "40"      ; T(Z)= "A"
        Z+=1 ; F(Z)= "DATE"              ; N(Z) = "20"      ; T(Z)= "A"
        Z+=1 ; F(Z)= "AMOUNT.FCY"        ; N(Z) = "20"      ; T(Z)= "AMT"
        Z+=1 ; F(Z)= "AMOUNT.LCY"        ; N(Z) = "20"      ; T(Z)= "AMT"
        Z+=1 ; F(Z)= "CO.CODE"           ; N(Z) = "20"      ; T(Z)= "A"
        Z+=1 ; F(Z)= "CURRENCY"          ; N(Z) = "20"      ; T(Z)= "A"
        Z+=1 ; F(Z)= "LINENO"            ; N(Z) = "10"      ; T(Z)= ""
        Z+=1 ; F(Z)= "DESCLINE"          ;  N(Z) = "45"    ;  T(Z)= "A"

        Z+=1 ; F(Z)= "RESERVED9"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED8"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED7"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED6"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED5"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED4"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED3"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED2"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'
        Z+=1 ; F(Z)= "RESERVED1"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
        T(Z)<3> = 'NOINPUT'

        V = Z + 9
        RETURN

*************************************************************************

    END
