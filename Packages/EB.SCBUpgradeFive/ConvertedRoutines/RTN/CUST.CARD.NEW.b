* @ValidationCode : Mjo1NTU2MjcxMDA6Q3AxMjUyOjE2NDA3MzE4NDgwMDA6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 00:50:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOO---------------------------------------------
SUBROUTINE CUST.CARD.NEW

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - Missing Layout I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
**  $INCLUDE GLOBUS.BP I_F.CARD.ISSUE.ACCOUNT

    FN.CUS='FBNK.CUSTOMER';F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

    FN.AC='FBNK.ACCOUNT';F.AC=''
    CALL OPF(FN.AC,F.AC)


    FN.CARD='FBNK.CARD.ISSUE';F.CARD=''
    CALL OPF(FN.CARD,F.CARD)

    FN.CARD.ACC='FBNK.CARD.ISSUE.ACCOUNT';F.CARD.ACC=''
    CALL OPF(FN.CARD.ACC,F.CARD.ACC)

    CU = O.DATA
    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)
    CALL F.READ( FN.CUS.AC,CU, R.CUS.AC, F.CUS.AC,ETEXT1)
    IF ETEXT1 THEN
        O.DATA = ''
***  RETURN
    END
**-----------------------------------------------------------------**
    LOOP

        REMOVE ACC FROM R.CUS.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.CARD.ACC,ACC,R.CARD.ACC,F.CARD.ACC,ERR11)
        IF ERR11 THEN
            XX = 2
        END

        LOOP
            REMOVE CARD.ACC FROM R.CARD.ACC  SETTING POS11
        WHILE CARD.ACC:POS11
            IF CARD.ACC[1,4] EQ 'ATMC' THEN
                XX =  "1"
                O.DATA = R.CARD.ACC[6,16]
                RETURN
            END ELSE
                XX =  "2"
            END
        REPEAT
    REPEAT

    IF XX = 2 THEN
        O.DATA = "�� ����"
    END ELSE
        O.DATA = R.CARD.ACC[6,16]
    END
RETURN
END
