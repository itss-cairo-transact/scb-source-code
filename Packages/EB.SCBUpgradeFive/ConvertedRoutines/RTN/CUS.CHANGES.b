* @ValidationCode : MjotMTEzOTczNTgzNjpDcDEyNTI6MTY0MDcwODAwNjAzMjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 18:13:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-112</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CUS.CHANGES
*PROGRAM CUS.CHANGES

*-------- CREATED BY NOHA HAMED 24/11/2014 ---

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - Missing Layout I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='CUS.CHANGES'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FLAG1 = ''
    FLAG2 = ''
    KEY.LIST  = ""  ; SELECTED=""  ;  ER.MSG=""
    DATEE = ''
    HDATE = ''
RETURN

*========================================================================
PROCESS:

*------------------TODAY FATCA SELECTION-------------------------
*DEBUG
    DATEE    = TODAY[3,6]
    CO.CODE  = C$ID.COMPANY
    DEP.CODE = CO.CODE[8,2]

    T.SEL  = "SELECT FBNK.CUSTOMER WITH DATE.TIME LIKE ":DATEE:"... AND COMPANY.BOOK EQ ":CO.CODE:" BY @ID"

*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CUS.ID     = KEY.LIST<I>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,ER.CU)
            CUS.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

            GOSUB  REPORT.WRITE
        NEXT I

        YY             = ''
        PRINT YY
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END

RETURN

*===============================================================
REPORT.WRITE:

    XX  = SPACE(132)
    XX<1,I>[1,8]    = CUS.ID
    XX<1,I>[15,35]  = CUS.NAME
    PRINT XX<1,I>

RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):"����� ���� ���������� ���� ��� ��� ��� �������"
*   PR.HD :="'L'":SPACE(40):""
    PR.HD :="'L'":SPACE(40):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(5):"��� ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*----------
END
