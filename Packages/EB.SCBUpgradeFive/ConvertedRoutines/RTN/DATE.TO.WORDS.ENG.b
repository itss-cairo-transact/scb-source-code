* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>92</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE DATE.TO.WORDS.ENG(IN.DATE,OUT.DATE,ER.MSG)
                      **********************************
                      * WRITTEN BY BAKRY - 2013/06/12  *
                      **********************************
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

    IF IN.DATE THEN
        ER.MSG = ""
        GOSUB VERIFY.DATE
        IF NOT(ER.MSG) THEN
            IF IN.DATE[1,2] = "19" ! IN.DATE[1,2] = "20" ! IN.DATE[1,2] = "21" THEN
                GOSUB STORE.DAY
                GOSUB STORE.MONTH
                GOSUB BUILD.WORD
            END
            ELSE ER.MSG = "TOO MUCH FORWARDED OR BACKVALUED"
        END
    END
    GOTO PROGRAM.END
**************************************************************************************************************************
BUILD.WORD:
*==========
    YEAR = "" ; IN.AMOUNT = "" ; OUT.AMOUNT = "" ; NO.OF.LINES = "" ; OUT.DATE = ""
    IN.AMOUNT = IN.DATE[1,4]
    CALL DE.O.PRINT.WORDS(IN.AMOUNT,OUT.AMOUNT,"GB",78,NO.OF.LINES,ERR.MS)
    OUT.AMOUNT = CHANGE(OUT.AMOUNT,'*' , " ")
    OUT.AMOUNT = TRIM(OUT.AMOUNT)
    IF NOT(ER.MSG) THEN
        IF IN.DATE[7,1] = 0 THEN
            OUT.DATE = "The ":DAY1<IN.DATE[7,2]>:" ":"OF":" ":MONTH<IN.DATE[5,2]>:" , ":OUT.AMOUNT ;*:" ":"Year"
        END ELSE
            OUT.DATE = DAY<IN.DATE[7,2]>:" ":"OF":" ":MONTH<IN.DATE[5,2]>:" , ":OUT.AMOUNT ;*:" ":"Year"
        END
        RETURN
**************************************************************************************************************************
STORE.MONTH:
*==========
        MONTH = ""
        MONTH<1> = "JANUARY"
        MONTH<2> = "FEBRUARY"
        MONTH<3> = "MARCH"
        MONTH<4> = "APRIL"
        MONTH<5> = "MAY"
        MONTH<6> = "JUNE"
        MONTH<7> = "JULY"
        MONTH<8> = "AUGUST"
        MONTH<9> = "SEPTEMBER"
        MONTH<10> = "OCTOBER"
        MONTH<11> = "NOVEMBER"
        MONTH<12> = "DECEMBER"
        RETURN
**************************************************************************************************************************
VERIFY.DATE:
*==========
        OLD.COMI = ""
        OLD.COMI = COMI
        COMI = IN.DATE
        CALL IN2D("11.1","D")
        IN.DATE = COMI
        IF ETEXT THEN
            ER.MSG = IN.DATE:"INVALID DATE" ; ETEXT = ''
        END
        COMI = OLD.COMI
        RETURN
**************************************************************************************************************************
STORE.DAY:
*---------
        WAW = "And"
        DAY = ""
        DAY1 = ""
*********************************** FIRST 9 DAYS *******************
        DAY1<1> = "First"
        DAY1<2> = "Second"
        DAY1<3> = "Third"
        DAY1<4> = "Fourth"
        DAY1<5> = "Fifth"
        DAY1<6> = "Sixth"
        DAY1<7> = "Seventh"
        DAY1<8> = "Eighth"
        DAY1<9> = "Ninth"
********************************************************************
        DAY<1> = "First"
        DAY<2> = "Two"
        DAY<3> = "Three"
        DAY<4> = "four"
        DAY<5> = "Five"
        DAY<6> = "Six"
        DAY<7> = "Seven"
        DAY<8> = "Eight"
        DAY<9> = "Nine"
        DAY<10> = "Ten"
        DAY<11> = "Eleven"
        DAY<12> = "Twelve"
        DAY<13> = "Thirteen"
        DAY<14> = "Fourteen"
        DAY<15> = "Fifteen"
        DAY<16> = "Sexteen"
        DAY<17> = "Seventeen"
        DAY<18> = "Eighteen"
        DAY<19> = "Nineteen"
        DAY<20> = "Twenty"
        DAY<21> = DAY<20>:" ONE"
        DAY<22> = DAY<20>:" ":DAY<2>
        DAY<23> = DAY<20>:" ":DAY<3>
        DAY<24> = DAY<20>:" ":DAY<4>
        DAY<25> = DAY<20>:" ":DAY<5>
        DAY<26> = DAY<20>:" ":DAY<6>
        DAY<27> = DAY<20>:" ":DAY<7>
        DAY<28> = DAY<20>:" ":DAY<8>
        DAY<29> = DAY<20>:" ":DAY<9>
        DAY<30> = "Thirty"
        DAY<31> = DAY<30>:" ONE"
        RETURN
*************************************************************************************************************************
PROGRAM.END:
*===========
        RETURN
    END
