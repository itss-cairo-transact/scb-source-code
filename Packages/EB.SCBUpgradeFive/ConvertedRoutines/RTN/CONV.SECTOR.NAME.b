* @ValidationCode : MjotNzIwMjYyODc4OkNwMTI1MjoxNjQwNjg0MTY5NTc5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:36:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
SUBROUTINE CONV.SECTOR.NAME

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE -  Missing Layout I_F.CBE.STATIC.AC.LD - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.CBE.STATIC.AC.LD

    SECTOR.ID = O.DATA

    IF SECTOR.ID EQ 210 THEN
        DESC = "����� ������� ����� ���������"
    END
    IF SECTOR.ID EQ 220 THEN
        DESC = "����� ������� ������"
    END
    IF SECTOR.ID EQ 230 THEN
        DESC = "����� �������"
    END
    IF SECTOR.ID EQ 240 THEN
        DESC = "������ ���������"
    END
    IF SECTOR.ID EQ 250 THEN
        DESC = "����� �����"
    END
    IF SECTOR.ID EQ 260 THEN
        DESC = "����� ���� ����� ������� �����"
    END

    O.DATA = DESC

RETURN
