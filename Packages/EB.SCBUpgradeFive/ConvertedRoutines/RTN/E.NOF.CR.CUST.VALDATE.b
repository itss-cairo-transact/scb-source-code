* @ValidationCode : Mjo2NTc0MjI1NTg6Q3AxMjUyOjE2NDA3OTA5MDg2MDA6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:15:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 28/09/2017******************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CR.CUST.VALDATE(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*   $INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0

    YTEXT = "���� ����� ������� : "
    CALL TXTINP(YTEXT, 10, 22, "12", "A")
    REQ.COMP = COMI

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CUSTOMER.1 UNLIKE '994...' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) AND CO.CODE EQ ":REQ.COMP :"  AND CONTRACT.GRP NE 'STAFF' BY CUSTOMER.1 BY AUTH.DATE"
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    FOR I = 1 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        IF CURR<I> = 'EGP' THEN
            AMT = AMT.LCY<I>
        END ELSE
            AMT = AMT.FCY<I>
        END
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        VAL.DAT<I>      = R.TT<TT.TE.VALUE.DATE.1>
        ACCT<I>         = R.TT<TT.TE.ACCOUNT.1>
        CO.CODE<I>      = R.TT<TT.TE.CO.CODE>
        NARRAT<I>       = R.TT<TT.TE.NARRATIVE.2>
        TT.LOCAL.REF    = R.TT<TT.TE.LOCAL.REF>
        TT.DATE = AUTH.DAT<I>
        CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
        LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
*REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
*CUST.COMP = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
        CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
        IF VAL.DAT<I> = AUTH.DAT<I> THEN
            ST.DAT<I> = ST.DATE
            ED.DAT<I> = EN.DATE
            Y.RET.DATA<-1> = CUST<I> :"*":CUST.NAME:"*": CURR<I>:"*":AMT:"*":KEY.LIST.N<I>:"*":AUTH.DAT<I>:"*":VAL.DAT<I>:"*":ST.DAT<I>:"*":ED.DAT<I>:"*":CO.CODE<I>
        END
        TOT.AMT.N = 0
    NEXT I
RETURN
END
