* @ValidationCode : MjotMTA4NTk1NTg0MDpDcDEyNTI6MTY0MDcwMzE2NjQwNzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:52:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM CREDIT.CBE.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CREDIT.CBE not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CREDIT.CBE
*Line [ 26 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CREDIT.CBE.NEW - ITSS - R21 Upgrade - 2021-12-23
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.SCB.CREDIT.CBE.NEW

*********************** OPENING FILES *****************************
    FN.ORG  = "F.SCB.CREDIT.CBE"      ; F.ORG   = ""
    FN.CPY  = "F.SCB.CREDIT.CBE.NEW"  ; F.CPY   = ""

    CALL OPF (FN.ORG,F.ORG)
    CALL OPF (FN.CPY,F.CPY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

*------------------------------------------------------------------
    T.SEL = "SELECT ":FN.ORG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ORG,KEY.LIST<I>,R.ORG,F.ORG,E1)
            CALL F.READ(FN.CPY,KEY.LIST<I>,R.CPY,F.CPY,E2)
            CBE.ID = KEY.LIST<I>
********************** COPY NEW DATA ******************************
            R.CPY<SCB.C.CBE.BNK.DATE>  =  R.ORG<SCB.C.CBE.BNK.DATE>
            R.CPY<SCB.C.CBE.CBE.NO>    =  R.ORG<SCB.C.CBE.CBE.NO>
            R.CPY<SCB.C.CBE.TOT.US.1>  =  R.ORG<SCB.C.CBE.TOT.US.1>
            R.CPY<SCB.C.CBE.TOT.CR.1>  =  R.ORG<SCB.C.CBE.TOT.CR.1>
            R.CPY<SCB.C.CBE.TOT.US.2>  =  R.ORG<SCB.C.CBE.TOT.US.2>
            R.CPY<SCB.C.CBE.TOT.CR.2>  =  R.ORG<SCB.C.CBE.TOT.CR.2>
            R.CPY<SCB.C.CBE.TOT.US.3>  =  R.ORG<SCB.C.CBE.TOT.US.3>
            R.CPY<SCB.C.CBE.TOT.CR.3>  =  R.ORG<SCB.C.CBE.TOT.CR.3>
            R.CPY<SCB.C.CBE.TOT.US.4>  =  R.ORG<SCB.C.CBE.TOT.US.4>
            R.CPY<SCB.C.CBE.TOT.CR.4>  =  R.ORG<SCB.C.CBE.TOT.CR.4>
            R.CPY<SCB.C.CBE.TOT.US.5>  =  R.ORG<SCB.C.CBE.TOT.US.5>
            R.CPY<SCB.C.CBE.TOT.CR.5>  =  R.ORG<SCB.C.CBE.TOT.CR.5>
            R.CPY<SCB.C.CBE.TOT.US.6>  =  R.ORG<SCB.C.CBE.TOT.US.6>
            R.CPY<SCB.C.CBE.TOT.CR.6>  =  R.ORG<SCB.C.CBE.TOT.CR.6>
            R.CPY<SCB.C.CBE.TOT.US.7>  =  R.ORG<SCB.C.CBE.TOT.US.7>
            R.CPY<SCB.C.CBE.TOT.CR.7>  =  R.ORG<SCB.C.CBE.TOT.CR.7>
            R.CPY<SCB.C.CBE.TOT.US.8>  =  R.ORG<SCB.C.CBE.TOT.US.8>
            R.CPY<SCB.C.CBE.TOT.CR.8>  =  R.ORG<SCB.C.CBE.TOT.CR.8>
            R.CPY<SCB.C.CBE.TOT.US.9>  =  R.ORG<SCB.C.CBE.TOT.US.9>
            R.CPY<SCB.C.CBE.TOT.CR.9>  =  R.ORG<SCB.C.CBE.TOT.CR.9>
            R.CPY<SCB.C.CBE.TOT.US.10> =  R.ORG<SCB.C.CBE.TOT.US.10>
            R.CPY<SCB.C.CBE.TOT.CR.10> =  R.ORG<SCB.C.CBE.TOT.CR.10>
            R.CPY<SCB.C.CBE.TOT.US.11> =  R.ORG<SCB.C.CBE.TOT.US.11>
            R.CPY<SCB.C.CBE.TOT.CR.11> =  R.ORG<SCB.C.CBE.TOT.CR.11>
            R.CPY<SCB.C.CBE.TOT.US.12> =  R.ORG<SCB.C.CBE.TOT.US.12>
            R.CPY<SCB.C.CBE.TOT.CR.12> =  R.ORG<SCB.C.CBE.TOT.CR.12>
            R.CPY<SCB.C.CBE.TOT.US.99> =  R.ORG<SCB.C.CBE.TOT.US.99>
            R.CPY<SCB.C.CBE.TOT.CR.99> =  R.ORG<SCB.C.CBE.TOT.CR.99>
            R.CPY<SCB.C.CBE.TOT.US.13> =  R.ORG<SCB.C.CBE.TOT.US.13>
            R.CPY<SCB.C.CBE.TOT.CR.13> =  R.ORG<SCB.C.CBE.TOT.CR.13>
            R.CPY<SCB.C.CBE.CO.CODE>   =  R.ORG<SCB.C.CBE.CO.CODE>

            US.99 = R.ORG<SCB.C.CBE.TOT.US.99> ; CR.99 = R.ORG<SCB.C.CBE.TOT.CR.99>
            US.13 = R.ORG<SCB.C.CBE.TOT.US.13> ; CR.13 = R.ORG<SCB.C.CBE.TOT.CR.13>
            IF (US.99 GE 100 OR CR.99 GE 100 OR US.13 GE 100 OR CR.13 GE 100) THEN
                R.CPY<SCB.C.CBE.GE30.FLAG> = 'Y'
            END

            IF R.CPY<SCB.C.CBE.GE30.FLAG> EQ 'Y' THEN
                R.CPY<SCB.C.CBE.GE30.FLAG> = 'Y'
            END

            TOT.FLG = US.99 + CR.99 + US.13 + CR.13
            IF TOT.FLG EQ 0 THEN
                R.CPY<SCB.C.CBE.GE30.FLAG> = 'N'
            END

            CALL F.WRITE(FN.CPY,CBE.ID,R.CPY)
            CALL  JOURNAL.UPDATE(CBE.ID)

        NEXT I
    END


******** CHECK DELETED CUSTOMER IN ORGINAL FILE *************

    T.SEL1 = "SELECT ":FN.CPY
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR X = 1 TO SELECTED1
            CALL F.READ(FN.ORG,KEY.LIST1<X>,R.ORG,F.ORG,E3)
            CALL F.READ(FN.CPY,KEY.LIST1<X>,R.CPY,F.CPY,E4)
            CBE1.ID = KEY.LIST1<X>
            IF E3 THEN
                R.CPY<SCB.C.CBE.GE30.FLAG> = 'D'
            END
            CALL F.WRITE(FN.CPY,CBE1.ID,R.CPY)
            CALL  JOURNAL.UPDATE(CBE1.ID)

        NEXT X
    END

********** DELETE CUSTOMER NOT FOUND FROM NEW FILE **********

    T.SEL2 = "SELECT ":FN.CPY:" WITH GE30.FLAG EQ 'D'"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR D = 1 TO SELECTED2
            CALL F.READ(FN.CPY,KEY.LIST2<D>,R.CPY,F.CPY,E5)
            CALL F.DELETE (FN.CPY, KEY.LIST2<D>)
***           DELETE F.CPY , KEY.LIST2<D>
        NEXT D
    END

END
