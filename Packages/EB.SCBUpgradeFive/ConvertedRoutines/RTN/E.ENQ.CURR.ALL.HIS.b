* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  E.ENQ.CURR.ALL.HIS(ENQ.DATA)

*   PROGRAM     E.ENQ.CURR.HIS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

*=======================================
    FN.CURR = "FBNK.CURRENCY$HIS"
    F.CURR = ''
    R.CURR = ''
    CALL OPF(FN.CURR,F.CURR)
    XX  = 1

    LOCATE '@ID' IN ENQ.DATA<2,1> SETTING RB.POS THEN
        CURR = ENQ.DATA<4,RB.POS>
    END
    LOCATE 'DATE.TIME' IN ENQ.DATA<2,1> SETTING RB.POS THEN
        D.T  = ENQ.DATA<4,RB.POS>
        D.T1 = D.T[3,6]
        D.T2 = D.T[12,6]
    END
*   TEXT = D.T1:"-":D.T2; CALL REM
    SEL.CMD   = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE ":CURR:"... AND DATE.TIME GE ":D.T1:"0001 AND DATE.TIME LE ":D.T2:"0001 BY NUMERIC.CCY.CODE BY DATE.TIME "
*   TEXT = SEL.CMD; CALL REM
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NOREC,RTNCD)
    FOR I = 1 TO  NOREC

        CALL F.READ(FN.CURR,SEL.LIST<I>,R.CURR,F.CURR,ERR.CURR)
        CALL F.READ(FN.CURR,SEL.LIST<I+1>,R.CURR.1,F.CURR,ERR.CURR.1)

        DTIME   = R.CURR<EB.CUR.DATE.TIME>[1,4]
        DTIME.1 = R.CURR.1<EB.CUR.DATE.TIME>[1,4]

        NOMCUR  = R.CURR<EB.CUR.NUMERIC.CCY.CODE>
        NOMCUR.1  = R.CURR.1<EB.CUR.NUMERIC.CCY.CODE>

        CUR.BK    = NOMCUR:DTIME
        CUR.BK.1    = NOMCUR.1:DTIME.1

        IF CUR.BK  NE CUR.BK.1 THEN
            ENQ.DATA<2,XX> = '@ID'
            ENQ.DATA<3,XX> = 'EQ'
            ENQ.DATA<4,XX> = SEL.LIST<I>

            XX ++
        END

    NEXT I
    RETURN
END
