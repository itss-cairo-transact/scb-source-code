* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* WAGDY MOUNIR <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.VER.SPR

*    PROGRAM    CONV.VER.SPR

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION

    FN.VR = 'F.VERSION' ; F.VR = ''
    CALL OPF(FN.VR,F.VR)

    IDD = O.DATA
    PRE = IDD[1,5]
    DAT = IDD[6,14]
*TEXT =" WA " :PRE :" GGG ": DAT

    IF PRE EQ '83USD' AND DAT GT 20100526 THEN

        VER.ID = "LD.LOANS.AND.DEPOSITS,SCB.CD.USD.V"

        CALL F.READ(FN.VR,VER.ID,R.VR,F.VR,E1)
        XX = R.VR<EB.VER.AUTOM.FIELD.NO>

*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        FOR I = 1 TO DCOUNT(XX,@VM)

            IF XX<1,I> EQ 'INTEREST.SPREAD' THEN
                O.DATA = R.VR<EB.VER.AUT.NEW.CONTENT><1,I>
                RETURN
            END

        NEXT I

    END ELSE
        O.DATA = 0
    END
END
