* @ValidationCode : MjoxOTY0MTMyMTY6Q3AxMjUyOjE2NDA2OTQyMzk3MzY6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:23:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.USD.TOT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - Missing Layout "I_F.CBE.STATIC.AC.LD.DETAIL" - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*----------------------------------------------------
    FN.TMP = "F.CBE.STATIC.AC.LD.DETAIL" ; F.TMP = ""
    CALL OPF(FN.TMP,F.TMP)
    TOT = 0

    CUS.ID = O.DATA
    T.SEL  = "SELECT F.CBE.STATIC.AC.LD.DETAIL WITH CBEM.CUSTOMER.CODE EQ " : CUS.ID
    T.SEL := " AND CBEM.CATEG IN ( 5000 5001 21076 ) BY @ID"
**    T.SEL := " AND CBEM.CY EQ 'USD'"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.TMP,KEY.LIST<II>,R.TMP,F.TMP,TMP.ERR)
            CURR = R.TMP<STD.CBEM.CY>


            IF CURR EQ 'USD' THEN
                AMT = R.TMP<STD.CBEM.IN.LCY>

                TOT = TOT + AMT
            END
        NEXT II
    END

    O.DATA = TOT
*----------------------------------------------------
RETURN
END
