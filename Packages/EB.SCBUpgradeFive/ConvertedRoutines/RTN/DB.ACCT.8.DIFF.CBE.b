* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*******---------BAKRY-------2020/06/09-----------******
    SUBROUTINE DB.ACCT.8.DIFF.CBE(ENQ)
*    PROGRAM DB.ACCT.8.DIFF.CBE

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.DR'
    F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    TT = TODAY
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    TTS = DAT
*    TTS = "20200430"
    ACCC=''
    YTEXT = " Enter End Of Month Date :  "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    TTS = COMI

*============================================================
*    FN.CONT = 'F.RE.STAT.LINE.CONT'     ; F.CONT = ''
*    FN.LN   = 'F.RE.STAT.REP.LINE'      ; F.LN   = ''
*    FN.CONS.ACC = 'F.RE.CONSOL.CONTRACT' ; F.CONS.ACC = ''
*    CALL OPF(FN.CONT,F.CONT)
*    CALL OPF(FN.LN,F.LN)
*    CALL OPF(FN.CONS.ACC,F.CONS.ACC)

    FN.ACCT = 'FBNK.ACCOUNT'     ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    NET.AMT     = 0
    NET.AMT.500 = 0
    SER = 0
    KK1 = 0
*============================================================
    T.SEL.ACCT    = "SELECT ":FN.ACCT:" WITH (CATEGORY GE 1585 AND CATEGORY LE 1587) BY @ID"
    CALL EB.READLIST(T.SEL.ACCT,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            NET.AMT  = 0
            ACC.NO   = KEY.LIST<HH>
            CALL F.READ(FN.ACCT,KEY.LIST<HH>,R.ACCT,F.ACCT,E1)
            GOSUB GETREC
        NEXT HH
    END
    RETURN
*============================================================
GETREC:
    ACC.ID = ACC.NO:"-":TTS
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
    IF AC.COM THEN
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = ACC.ID
    END ELSE
        KK1++
        ENQ<2,KK1> = "@ID"
        ENQ<3,KK1> = "EQ"
        ENQ<4,KK1> = "NOLIST"
    END
*PRINT ACC.ID
*NEXT I
*END ELSE
*KK1++
*ENQ<2,KK1> = "@ID"
*ENQ<3,KK1> = "EQ"
*ENQ<4,KK1> = "NOLIST"
*END
    RETURN
END
