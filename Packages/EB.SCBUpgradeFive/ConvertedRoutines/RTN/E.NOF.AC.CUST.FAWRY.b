* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***********NESSREEN AHMED 18/03/2018**********************
    SUBROUTINE E.NOF.AC.CUST.FAWRY(Y.RET.DATA)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER


    GOSUB INITIATE
*Line [ 42 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 43 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    GOSUB RET.DATA

    RETURN
****************************************
****************************************
INITIATE:
*--------
    ACCT.NO = '' ; NSN.NO = '' ; CUST.NO = '' ; NSN.CU = '' ; ER.AC = '' ; RESP = ''

    RETURN
*-----------------------------------------

CALLDB:
*-------
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = "" ; ER.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.CU = "FBNK.CUSTOMER"  ; F.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)

    RETURN
*------------------------------
****************************************
PROCESS:
*-------
    LOCATE "ACCOUNT.NO"   IN D.FIELDS<1> SETTING YACCT.POS THEN ACCT.NO =   D.RANGE.AND.VALUE<YACCT.POS> ELSE RETURN
    LOCATE "NATIONAL.ID.NO" IN D.FIELDS<1> SETTING YNID.POS THEN NSN.NO  = D.RANGE.AND.VALUE<YNID.POS> ELSE RETURN
**   TEXT = 'NSN=':NSN.NO ; CALL REM
**   TEXT = 'ACCT.NO=':ACCT.NO ; CALL REM
    CALL F.READ(FN.AC,ACCT.NO,R.AC,F.AC,ER.AC)
    IF ER.AC THEN
        ACC.INV = 1
    END
    AC.ONLINE.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
    AC.CUR         = R.AC<AC.CURRENCY>
    AC.COM         = R.AC<AC.CO.CODE>
    CUST.NO        = R.AC<AC.CUSTOMER>
*   TEXT = 'CUST.NO=': CUST.NO ; CALL REM
    CALL F.READ(FN.CU, CUST.NO, R.CU, F.CU, ER.CU)
    LOCAL.REF = R.CU<EB.CUS.LOCAL.REF>
    NSN.CU = LOCAL.REF<1,CULR.NSN.NO>
*   TEXT = 'NSN.CU=': NSN.CU ; CALL REM
    BEGIN CASE
    CASE (NSN.NO = NSN.CU) AND (ER.AC EQ '' ) AND (AC.CUR EQ 'EGP')
**        TEXT = 'EQUAL'; CALL REM
        RESP = "00"
    CASE ER.AC NE ''
        RESP = "02"
    CASE NSN.NO # NSN.CU
        RESP = "04"
    CASE AC.CUR NE 'EGP'
        RESP = "06"
    END CASE
    RETURN
********************
**************************************
RET.DATA:
*--------
    RET.REC = RESP :"*":ACCT.NO:"*":NSN.NO
    Y.RET.DATA<-1> = RET.REC

    RETURN
**********************************************
END
