* @ValidationCode : Mjo1MjEyMzUwOTM6Q3AxMjUyOjE2NDA3OTAxODA2NTU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:03:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*    SUBROUTINE E.NOF.CBE.LD(Y.RET.DATA)
PROGRAM E.NOF.CBE.LD

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.RANGE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.TYPE.LEVEL
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*------------------------------------------------*
    COMP = ID.COMPANY

    FN.ACC = 'FBNK.ACCOUNT'           ; F.ACC = ''  ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''  ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.LN = 'F.RE.STAT.REP.LINE'      ; F.LN = ''  ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.RG = 'F.RE.STAT.RANGE'         ; F.RG = ''  ; R.RG = ''
    CALL OPF(FN.RG,F.RG)
    FN.CUS  = 'F.SCB.LD.TYPE.LEVEL'   ; F.CUS  = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    TT       = TODAY
    KK1      = 0
    MM       = 0
    CATEG    = ''
    CAT.CRIT = ''
    XX       = ''
    M.MONTHS = ''
    RATE.1 = '' ; MAX.1 = '' ; MIN.1 = ''
    RATE.2 = '' ; MAX.2 = '' ; MIN.2 = ''
    RATE.3 = '' ; MAX.3 = '' ; MIN.3 = ''
    RATE.4 = '' ; MAX.4 = '' ; MIN.4 = ''
    RATE.5 = '' ; MAX.5 = '' ; MIN.5 = ''
    RATE.TOT = ''
    R.MIN = ''
    R.MAX = ''
    R.RAT1 = ''
    AMT2.2W    = ''
    AMT2.2W.1M = ''
    AMT2.1M.3M = ''
    AMT2.3M.6M = ''
    AMT2.6M.1Y = ''
    AMT2.1Y.3Y = ''
    AMT.1 = '' ; AMT.3 = '' ; AMT.4 = '' ; AMT.5 = ''
    RATE.1 = '' ; RATE.2 = '' ; RATE.3 = '' ; RATE.4 = '' ; RATE.5 = ''
    MAX.1  = '' ; MAX.2  = '' ; MAX.3  = '' ; MAX.4  = '' ; MAX.5  = ''
    MIN.1  = '' ; MIN.2  = '' ; MIN.3  = '' ; MIN.4  = '' ; MIN.5  = ''
*----------------------------------------------*
    Z       = 1
    ARR.CAT = ''
    ARR.NN  = SPACE(130)
    ARR.NN<1,1> = 'GENLED.0510'
    ARR.NN<1,2> = 'GENLED.0556'
    ARR.NN<1,3> = 'GENLED.0527'
****LD**************************
    ARR.NN<1,4> = 'GENLED.0530'
    ARR.NN<1,5> = 'GENLED.0570'
********************************
    ARR.NN<1,6> = 'GENLED.0520'
    ARR.NN<1,7> = 'GENLED.0525'
    ARR.NN<1,8> = 'GENLED.0540'
    ARR.NN<1,9> = 'GENLED.0550'
    SEL.N  = 9

    FOR II = 1 TO SEL.N
        LN.ID  = ARR.NN<1,II>
        GOSUB GETCATEG
        IF II EQ 4 OR II EQ 5 THEN
            GOSUB SEL.LD
        END ELSE
            GOSUB SEL.ACC
        END
        RATE     = ''
        RATE.TOT = ''
        R.RAT1   = ''
        R.MIN    = ''
        R.MAX    = ''
    NEXT II
*-----------*
*    Y.RET.DATA<-1> =  AMT.1:"*":AMT2.2W:"*":AMT2.2W.1M:"*":AMT2.1M.3M:"*":AMT2.3M.6M:"*":AMT2.6M.1Y:"*":AMT.3:"*":AMT2.1Y.3Y:"*":AMT.4:"*":AMT.5:"*"
    CRT AMT.1:"*":AMT2.2W:"*": AMT2.2W.1M:"*":AMT2.1M.3M:"*":AMT2.3M.6M:"*":AMT2.6M.1Y:"*":AMT.3:"*":AMT2.1Y.3Y:"*":AMT.4:"*":AMT.5
    CRT "RATES: "
*******1******************
    RATE.1 = ( RATE.0510 + RATE.0556) / 2
    IF MAX.0510 GT MAX.0556 THEN
        MAX.1 = MAX.0510
    END ELSE
        MAX.1 = MAX.0556
    END
    IF MIN.0510 LT MIN.0556 THEN
        MIN.1 = MIN.0510
    END ELSE
        MIN.1 = MIN.0556
    END
*******2******************
    RATE.2 = ( RATE.0527 + RATE.0530 ) / 2
    IF MAX.0527 GT MAX.0530 THEN
        MAX.2 = MAX.0527
    END ELSE
        MAX.2 = MAX.0530
    END
    IF MIN.0527 LT MIN.0530 THEN
        MIN.2 = MIN.0527
    END ELSE
        MIN.2 = MIN.0530
    END
*******3******************
    RATE.3 = RATE.0570
    MAX.3  = MAX.0570
    MIN.3  = MIN.0570
*******4******************
    RATE.4 = ( RATE.0520 + RATE.0525 ) / 2
    IF MAX.0520 GT MAX.0525 THEN
        MAX.4 = MAX.0520
    END ELSE
        MAX.4 = MAX.0525
    END
    IF MIN.0520 LT MIN.0525 THEN
        MIN.4 = MIN.0520
    END ELSE
        MIN.4 = MIN.0525
    END
*******5******************
    RATE.5 = ( RATE.0540 + RATE.0550 ) / 2
    IF MAX.0520 GT MAX.0550 THEN
        MAX.5 = MAX.0540
    END ELSE
        MAX.5 = MAX.0525
    END
    IF MIN.0540 LT MIN.0550 THEN
        MIN.5 = MIN.0540
    END ELSE
        MIN.5 = MIN.0550
    END
**************************
    CRT RATE.1:"/":MAX.1:"/":MIN.1
    CRT RATE.2:"/":MAX.2:"/":MIN.2
    CRT RATE.3:"/":MAX.3:"/":MIN.3
    CRT RATE.4:"/":MAX.4:"/":MIN.4
    CRT RATE.5:"/":MAX.5:"/":MIN.5
RETURN
*----------------------------------------------*
SEL.LD:
    T.SEL  = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    T.SEL  = "SELECT ":FN.LD:" WITH AMOUNT GT 0 AND AMOUNT NE '' AND ( ":CAT.CRIT
    T.SEL := " OR ( CATEGORY IN ( ":CATEG:" )))"
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
*-------------------------------*
        GOSUB CALC.RATE
*-------------------------------*
        FOR IH = 1 TO SELECTED
            CALL F.READ(FN.LD,K.LIST<IH>,R.LD,F.LD,ERR.LD)
            IF R.LD THEN
                CATGRY = R.LD<LD.CATEGORY>
                V.DATE = R.LD<LD.VALUE.DATE>
                M.DATE = R.LD<LD.FIN.MAT.DATE>
                CALL CDD("",M.DATE,V.DATE,N.DAYS)
                CALL MONTHS.BETWEEN(M.DATE,V.DATE,N.MONTHS)
                IF LN.ID EQ 'GENLED.0530' THEN
                    RATE.0530 = RATE
                    MAX.0530  = R.MAX
                    MIN.0530  = R.MIN

                    IF N.DAYS GE 14 AND N.MONTHS LE 1 THEN
                        AMT2.2W = AMT2.2W + R.LD<LD.AMOUNT>
                    END
                    IF N.DAYS LE 14 THEN
                        AMT2.2W.1M = AMT2.2W.1M + R.LD<LD.AMOUNT>
                    END
                    IF M.MONTHS GT 1 AND N.MONTHS LE 3 THEN
                        AMT2.1M.3M = AMT2.1M.3M + R.LD<LD.AMOUNT>
                    END
                    IF N.MONTHS GT 6 AND N.MONTHS LE 12 THEN
                        AMT2.6M.1Y = AMT2.6M.1Y + R.LD<LD.AMOUNT>
                    END
                    IF N.MONTHS GT 3 AND N.MONTHS LE 6 THEN
                        AMT2.3M.6M = AMT2.3M.6M + R.LD<LD.AMOUNT>
                    END
                    IF N.MONTHS GT 12 AND N.MONTHS LE 36 THEN
                        AMT2.1Y.3Y = AMT2.1Y.3Y + R.LD<LD.AMOUNT>
                    END
                END
            END
*-----------------*
            IF LN.ID EQ 'GENLED.0570' THEN
                RATE.0570 = RATE
                MAX.0570  = R.MAX
                MIN.0570  = R.MIN

                IF N.MONTHS GE 36 THEN
                    AMT.3 = AMT.3 +  R.LD<LD.AMOUNT>
                END
            END
        NEXT IH
    END
RETURN
*----------------------------------------------*
SEL.ACC:
    T.SEL  = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    T.SEL  = "SELECT ":FN.ACC:" WITH OPEN.ACTUAL.BAL GT 0 AND OPEN.ACTUAL.BAL NE ''"
    T.SEL := " AND ( ":CAT.CRIT
    T.SEL := " OR ( CATEGORY IN ( ":CATEG:" )))"
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
*----------------------------------*
        GOSUB CALC.RATE
*----------------------------------*
        FOR IH = 1 TO SELECTED
            CALL F.READ(FN.ACC,K.LIST<IH>,R.ACC,F.ACC,ERR.ACC)
            CATGRY = R.ACC<AC.CATEGORY>
*   IF R.ACC<AC.OPEN.ACTUAL.BAL> GT 0 THEN
            IF LN.ID EQ 'GENLED.0510' THEN
                RATE.0510 = RATE
                MAX.0510  = R.MAX
                MIN.0510  = R.MIN

                AMT.1  = AMT.1 +  R.ACC<AC.OPEN.ACTUAL.BAL>
            END
            IF LN.ID EQ 'GENLED.0556' THEN
                RATE.0556 = RATE
                MAX.0556  = R.MAX
                MIN.0556  = R.MIN

                AMT.1 = AMT.1 +  R.ACC<AC.OPEN.ACTUAL.BAL>
            END
            IF LN.ID EQ 'GENLED.0527' THEN
                RATE.0527 = RATE
                MAX.0527  = R.MAX
                MIN.0527  = R.MIN

                AMT2.1M.3M = AMT2.1M.3M + R.ACC<AC.OPEN.ACTUAL.BAL>
            END
            IF LN.ID EQ 'GENLED.0520' THEN
                RATE.0520 = RATE
                MAX.0520  = R.MAX
                MIN.0520  = R.MIN

                AMT.4 = AMT.4 + R.ACC<AC.OPEN.ACTUAL.BAL>
            END
            IF LN.ID EQ 'GENLED.0525' THEN
                RATE.0525 = RATE
                MAX.0525  = R.MAX
                MIN.0525  = R.MIN

                AMT.4 = AMT.4 + R.ACC<AC.OPEN.ACTUAL.BAL>
            END
            IF LN.ID EQ 'GENLED.0540' THEN
                RATE.0540 = RATE
                MAX.0540  = R.MAX
                MIN.0540  = R.MIN

                AMT.5 = AMT.5 + R.ACC<AC.OPEN.ACTUAL.BAL>
            END
            IF LN.ID EQ 'GENLED.0550' THEN
                RATE.0550 = RATE
                MAX.0550  = R.MAX
                MIN.0550  = R.MIN

                AMT.5 = AMT.5 + R.ACC<AC.OPEN.ACTUAL.BAL>
            END
*END
        NEXT IH
    END
RETURN
*--------------------------------------------------*
GETCATEG:
*********
    CALL F.READ(FN.LN,LN.ID,R.LN,F.LN,ERR.LN)
*Line [ 299 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    PROD.NO = DCOUNT(R.LN<RE.SRL.ASSET1>,@VM)
    FOR XX = 1 TO PROD.NO
        PRODUCT = R.LN<RE.SRL.ASSET1,XX>
        IF PRODUCT[1,1] EQ '*' THEN
            MM++
            GOSUB GETRANGE
        END ELSE
            CATEG = CATEG:" ":R.LN<RE.SRL.ASSET1,XX>
            ARR.CAT<1,Z> = R.LN<RE.SRL.ASSET1,XX>
            Z = Z + 1
        END
    NEXT XX
RETURN
***********************************************
GETRANGE:
********
    SS = LEN(PRODUCT) - 1
    RG.ID = PRODUCT[2,SS]
    CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
    CATFR = R.RG<RE.RNG.START.RANGE>
    CATTO = R.RG<RE.RNG.END.RANGE>
    IF MM EQ 1 THEN
        ORR = ''
    END ELSE
        ORR = ' OR '
    END
    DIFF.CAT = CATTO - CATFR
    ARR.CAT<1,Z> = CATFR
    Z = Z + 1
    FOR NN = 1 TO DIFF.CAT
        ARR.CAT<1,Z> = CATFR + NN
        Z = Z + 1
    NEXT NN
    CAT.CRIT = CAT.CRIT:ORR:" ( CATEGORY GE ":CATFR:" AND CATEGORY LE ":CATTO:" )"
RETURN
*-----------------------*
CALC.RATE:
    FOR CZ = 1 TO Z
        CATG.ID = ARR.CAT<1,CZ> : TT
        CALL F.READ(FN.CUS,CATG.ID,R.CUS,F.CUS,ERR.CUS)
*Line [ 340 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        N.COUNT = DCOUNT(R.CUS<LDTL.CURRENCY>,@VM)
        FOR II = 1 TO N.COUNT
            IF R.CUS<LDTL.CURRENCY><1,II> EQ 'EGP' THEN
                RATE.TOT = RATE.TOT + R.CUS<LDTL.RATE><1,II>
                BB = II
                II = N.COUNT
                R.RAT1 = R.CUS<LDTL.RATE><1,BB>
            END
        NEXT II
        IF R.MIN EQ ''  THEN
            R.MIN = R.RAT1
        END
        IF R.MAX EQ ''  THEN
            R.MAX = R.RAT1
        END
        IF R.MIN > R.RAT1 THEN
            R.MIN = R.RAT1
        END
        IF R.MAX <  R.RAT1 THEN
            R.MAX = R.RAT1
        END
    NEXT CZ
    RATE = RATE.TOT / CZ
RETURN
***********************************************
