* @ValidationCode : MjoyMDkwNDUyNjI2OkNwMTI1MjoxNjQwNjk3NTU0OTU5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 15:19:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM CR.MOKHSAS.PD

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.DRAWINGS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.FUND not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.FUND
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - Missing Layout "I_F.SCB.CUS.POS.TST - ITSS - R21 Upgrade - 2021-12-23
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
$INCLUDE I_F.SCB.CUS.POS.TST
*--------------------------------------------------------*
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.PD = '' ; FN.PD = 'FBNK.PD.PAYMENT.DUE' ; R.PD = ''
    CALL OPF(FN.PD,F.PD)
*--------------------------------------------------------*
    GOSUB PD.SUB
RETURN
*--------------------------------------------------------*
PD.SUB:
*------
    ID.NO   = ""
    WS.DATE = TODAY

    DAT.ID = 'EG0010001'
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.DATE.LWD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.DATE.LWD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    T.SEL1  = "SELECT ":FN.PD:" WITH"
    T.SEL1 := " CATEGORY GE 21050 AND CATEGORY LE 21074"
    T.SEL1 := " BY @ID"

    KEY.LIST1 = ""
    SELECTED1 = ""
    ER.MSG1   = ""
    CATTTG    = ''
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    PRINT SELECTED1 : "   = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

    IF KEY.LIST1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.PD,KEY.LIST1<I>, R.PD,F.PD, ETEXT1)
            CATTTG = R.PD<PD.CATEGORY>
            CURR   = R.PD<PD.CURRENCY>
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CURR,CURR.NO)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR.NO=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

            ID.NO    = R.PD<PD.CUSTOMER>:"*PD*BNK*":CURR.NO:R.PD<PD.CATEGORY>:"*":KEY.LIST1<I>:"*":WS.DATE
            AMT      = R.PD<PD.TOTAL.OVERDUE.AMT>
            LCY.AMTT = DROUND(AMT,2)

            R.COUNT<CUPOS.LCY.AMOUNT>    = LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER>      = R.PD<PD.CUSTOMER>
            R.COUNT<CUPOS.DEAL.CCY>      = R.PD<PD.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT>   = AMT
            R.COUNT<CUPOS.CATEGORY>      = R.PD<PD.CATEGORY>
            R.COUNT<CUPOS.MATURITY.DATE> = R.PD<PD.FINAL.DUE.DATE>
            R.COUNT<CUPOS.VALUE.DATE>    = R.PD<PD.START.DATE>
            R.COUNT<CUPOS.INT.RATE>      = R.PD<PD.PENALTY.RATE>
            R.COUNT<CUPOS.SYS.DATE>      = WS.DATE
            R.COUNT<CUPOS.CO.CODE>       = R.PD<PD.CO.CODE>
            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)

**     WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
**     END
        NEXT I
    END
RETURN
