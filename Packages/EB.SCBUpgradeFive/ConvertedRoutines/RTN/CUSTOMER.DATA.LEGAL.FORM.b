* @ValidationCode : MjotMTM1MjU5NzEzNDpDcDEyNTI6MTY0ODQ4NjA4ODEzMzpNb3VuaXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Mar 2022 18:48:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>145</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CUSTOMER.DATA.LEGAL.FORM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INDUSTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SECTOR
*Line [ 39 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CUS.LEGAL.FORM - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.LEGAL.FORM
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 43 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 44 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    PRINT " ������� = " :SELECTEDX

    CALL PRINTER.OFF

    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
RETURN
*==============================================================
INITIATE:
    REPORT.ID='CUSTOMER.DATA.LEGAL.FORM'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    T.SELX = "SELECT FBNK.CUSTOMER WITH VERSION.NAME EQ ',SCB.CORPORATE' AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" BY LEGAL.FORM"
* T.SELX = "SELECT FBNK.CUSTOMER WITH @ID EQ 1301622 OR @ID EQ 1301271 OR @ID EQ 1303408 BY LEGAL.FORM"
    KEY.LISTX ="" ; SELECTEDX="" ;  ER.MSGX=""
    CALL EB.READLIST(T.SELX,KEY.LISTX,"",SELECTEDX,ER.MSGX)
*    TEXT = SELECTEDX ; CALL REM
    FOR I = 1 TO SELECTEDX

        CALL F.READ(FN.CU,KEY.LISTX<I>,R.CU,F.CU,E1)
        LOC.REF = R.CU<EB.CUS.LOCAL.REF>
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.CUS.LEGAL.FORM':@FM:LEG.DESCRIPTION,LOC.REF<1,CULR.LEGAL.FORM>,CUSLEGALFORM)
        F.ITSS.SCB.CUS.LEGAL.FORM = 'F.SCB.CUS.LEGAL.FORM'
        FN.F.ITSS.SCB.CUS.LEGAL.FORM = ''
        CALL OPF(F.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM)
        CALL F.READ(F.ITSS.SCB.CUS.LEGAL.FORM,CULR.LEGAL.FORM,R.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM,ERROR.SCB.CUS.LEGAL.FORM)
        CUSLEGALFORM=R.ITSS.SCB.CUS.LEGAL.FORM<@FM:LEG.DESCRIPTION,LOC.REF><1>
        SEC =  R.CU<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
        YY<1,1> = CUSLEGALFORM
        PRINT YY<1,1>
        Y = 0
        FOR Z = I TO SELECTEDX

            CALL F.READ(FN.CU,KEY.LISTX<Z>,R.CU,F.CU,E1)
            SEC2 = R.CU<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
            IF SEC # SEC2 THEN
                GOSUB ZZZZ
                RETURN
            END
            LOCAL.REF     = R.CU<EB.CUS.LOCAL.REF>
            CUST.NO       = KEY.LISTX<Z>
            CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME,1>
            CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SECTOR':@FM:EB.SEC.DESCRIPTION,R.CU<EB.CUS.SECTOR>,CUSSEC)
            F.ITSS.SECTOR = 'F.SECTOR'
            FN.F.ITSS.SECTOR = ''
            CALL OPF(F.ITSS.SECTOR,FN.F.ITSS.SECTOR)
            CALL F.READ(F.ITSS.SECTOR,R.CU<EB.CUS.SECTOR>,R.ITSS.SECTOR,FN.F.ITSS.SECTOR,ERROR.SECTOR)
            CUSSEC=R.ITSS.SECTOR<EB.SEC.DESCRIPTION>
            CUST.SECTOR   =  CUSSEC
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('INDUSTRY':@FM:EB.IND.DESCRIPTION,R.CU<EB.CUS.INDUSTRY>,CUSINDUS)
            F.ITSS.INDUSTRY = 'F.INDUSTRY'
            FN.F.ITSS.INDUSTRY = ''
            CALL OPF(F.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY)
            CALL F.READ(F.ITSS.INDUSTRY,R.CU<EB.CUS.INDUSTRY>,R.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY,ERROR.INDUSTRY)
            CUSINDUS=R.ITSS.INDUSTRY<EB.IND.DESCRIPTION>
            CUST.INDUSTRY =  CUSINDUS
*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.CUS.LEGAL.FORM':@FM:LEG.DESCRIPTION,LOC.REF<1,CULR.LEGAL.FORM>,CUSLEGALFORM)
            F.ITSS.SCB.CUS.LEGAL.FORM = 'F.SCB.CUS.LEGAL.FORM'
            FN.F.ITSS.SCB.CUS.LEGAL.FORM = ''
            CALL OPF(F.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM)
            CALL F.READ(F.ITSS.SCB.CUS.LEGAL.FORM,CULR.LEGAL.FORM,R.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM,ERROR.SCB.CUS.LEGAL.FORM)
            CUSLEGALFORM=R.ITSS.SCB.CUS.LEGAL.FORM<@FM:LEG.DESCRIPTION,LOC.REF><1>
            CUST.LEGFORM  =  CUSLEGALFORM
            Y = Y +1
            YY = SPACE(120)
            YY<1,2>[1,10] = CUST.NO
            YY<1,2>[10,35] = CUST.NAME
            YY<1,2>[45,35] = CUST.ADDRESS
            YY<1,2>[80,20] = CUSSEC
            YY<1,2>[100,20] = CUSINDUS
            IF I # SELECTEDX THEN       ;*SELECTED
*     YY<1,3>[1,70] = STR('-',120)
            END ELSE
                YY<1,3>[1,70] = STR('=',120)
            END
            IF I # SELECTEDX   THEN
                PRINT YY<1,2>
            END
            IF I # SELECTEDX   THEN
                PRINT YY<1,3>
            END

        NEXT Z
ZZZZ:
        IF I # SELECTEDX   THEN
            PRINT " ������� = " :Y
            YY<1,4>[1,70] = STR('*',120)
            PRINT YY<1,4>
            IF Z # SELECTEDX THEN
                I = Z - 1
            END ELSE
                I = Z
            END
        END
    NEXT I
*    IF I = SELECTEDX  THEN
*        PRINT " ������� = " :SELECTEDX
*    END
RETURN

*ZZZZ:
*PRINT " ������� = " :Y
*        I = Z
*    NEXT I
*PRINT " ������� = " :SELECTEDX
*RETURN
*===============================================================
PRINT.HEAD:
*Line [ 163 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
*    PR.HD ="'L'":SPACE(50):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
*    PR.HD :="'L'":SPACE(50):"������ ������� ���� ����� ��������"
    PR.HD :="'L'":SPACE(50):"������� ������� ���� ����� �������� "
    PR.HD :="'L'":SPACE(45):STR('_',43)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

*   PR.HD :="'L'":SPACE(1):" �����":SPACE(10):" �����" :SPACE(25):"������� ":SPACE(25):"������ ":SPACE(10):"�������"
    PR.HD :="'L'":SPACE(1):" �����":SPACE(10):" �����": SPACE(25):"������� ":SPACE(25):"����� ������":SPACE(10):" ����� ����� ������� "
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
RETURN
*==============================================================
END
