* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>139</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE DATE.TO.WORDS.AR(IN.DATE,OUT.DATE,ER.MSG)
*SUBROUTINE DATE.TO.WORDS.AR
*
* WRITTEN BY BAKRY - 20/05/02
*
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*
   IF IN.DATE THEN
      ER.MSG = ""
      GOSUB VERIFY.DATE
      IF NOT(ER.MSG) THEN
         IF IN.DATE[1,2] = "19" ! IN.DATE[1,2] = "20" ! IN.DATE[1,2] = "21" THEN
            GOSUB STORE.DAY
            GOSUB STORE.MONTH
            GOSUB BUILD.WORD
         END
         ELSE ER.MSG = "TOO MUCH FORWARDED OR BACKVALUED"
      END
   END
*
   GOTO PROGRAM.END
*
**************************************************************************************************************************
BUILD.WORD:
*
   YEAR = "" ; IN.AMOUNT = "" ; OUT.AMOUNT = "" ; NO.OF.LINES = "" ; OUT.DATE = ""
*
 IN.AMOUNT = IN.DATE[1,4]
*Line [ 50 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
  CALL EB.SCBUpgradeFive.WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
   IF NOT(ER.MSG) THEN
     OUT.DATE = DAY<IN.DATE[7,2]>:" �� ":MONTH<IN.DATE[5,2]>:"��� ":OUT.AMOUNT
 *     TEXT = OUT.DATE ; CALL REM
END
   RETURN
*
**************************************************************************************************************************
STORE.MONTH:
*
   MONTH = ""
   MONTH<1> = "����� "
   MONTH<2> = "������ "
   MONTH<3> = "���� "
   MONTH<4> = "����� "
   MONTH<5> = "���� "
   MONTH<6> = "����� "
   MONTH<7> = "����� "
   MONTH<8> = "����� "
   MONTH<9> = "������ "
   MONTH<10> = "������ "
   MONTH<11> = "������ "
   MONTH<12> = "������ "
*
   RETURN
*
**************************************************************************************************************************
VERIFY.DATE:
*
   OLD.COMI = ""
   OLD.COMI = COMI
   COMI = IN.DATE
   CALL IN2D("11.1","D")
   IN.DATE = COMI
   IF ETEXT THEN
      ER.MSG = IN.DATE:"INVALID DATE" ; ETEXT = ''
   END
   COMI = OLD.COMI
*
   RETURN
*
**************************************************************************************************************************
STORE.DAY:
*
   WAW = "�"
   DAY = ""
   DAY<1> = "����� "
   DAY<2> = "������ "
   DAY<3> = "������ "
   DAY<4> = "������ "
   DAY<5> = "������ "
   DAY<6> = "������ "
   DAY<7> = "������ "
   DAY<8> = "������ "
   DAY<9> = "������ "
   DAY<10> = "������ "
   DAY<11> = "������ ���"
   DAY<12> = "������ ���"
   DAY<13> = "������ ���"
   DAY<14> = "������ ���"
   DAY<15> = "������ ���"
   DAY<16> = "������ ���"
   DAY<17> = "������ ���"
   DAY<18> = "������ ���"
   DAY<19> = "������ ���"
   DAY<20> = "�������"
   DAY<21> = "������ ":WAW:" ":DAY<20>
   DAY<22> = DAY<2>:" ":WAW:DAY<20>
   DAY<23> = DAY<3>:" ":WAW:DAY<20>
   DAY<24> = DAY<4>:" ":WAW:DAY<20>
   DAY<25> = DAY<5>:" ":WAW:DAY<20>
   DAY<26> = DAY<6>:" ":WAW:DAY<20>
   DAY<27> = DAY<7>:" ":WAW:DAY<20>
   DAY<28> = DAY<8>:" ":WAW:DAY<20>
   DAY<29> = DAY<9>:" ":WAW:DAY<20>
   DAY<30> = "��������"
   DAY<31> = "������ ":WAW:" ":DAY<30>
*
   RETURN
*
*************************************************************************************************************************
PROGRAM.END:
*
   RETURN
END
