* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE E.NOF.CD.VAR.CUS(Y.RET.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    FN.LD ='F.LD.LOANS.AND.DEPOSITS'
    F.LD  =''
    CALL OPF(FN.LD,F.LD)
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY GE 21026 AND CATEGORY LE 21028 BY CO.CODE BY CUSTOMER.ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
CUS.NO = ''
*********************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            CALL F.READ(FN.LD,KEY.LIST<I+1>,R.LD1,F.LD,E2)

            CUS.CODE       =  R.LD<LD.CUSTOMER.ID>
            CUS.CODE1      =  R.LD1<LD.CUSTOMER.ID>

            CD.CODE       = R.LD<LD.CO.CODE>
            CD.CODE1      = R.LD1<LD.CO.CODE>

            IF CD.CODE EQ CD.CODE1 THEN
                IF CUS.CODE1 NE CUS.CODE THEN
                    CUS.NO = 1 + CUS.NO
                END
            END ELSE
            **    PRINT CUS.NO :'-':R.LD<LD.CO.CODE>
                 Y.RET.DATA<-1> = CUS.NO :"*":R.LD<LD.CO.CODE>
                CUS.NO = '1'
            END
***********************
        NEXT I
    END
**********************
    RETURN
END
