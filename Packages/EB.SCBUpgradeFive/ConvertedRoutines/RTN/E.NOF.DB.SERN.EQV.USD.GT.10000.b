* @ValidationCode : MjotMTEyOTQ2MzIyMjpDcDEyNTI6MTY0MDg2ODMwNjg2Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:45:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 16/06/2013******************
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.DB.SERN.EQV.USD.GT.10000(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - Missing Layout I_TT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CURRENCY.DAILY - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)

    SELL.RATE = 0 ; AMT.FCY = 0 ; AMT.LCY = 0 ;AMT.EQV.USD = 0

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 NE '' AND (CURRENCY.1 EQ 'USD' OR CURRENCY.1 EQ 'EUR' OR CURRENCY.1 EQ 'SAR' ) AND RECORD.STATUS EQ 'MAT' AND CONTRACT.GRP NE '' AND SER.NO NE '' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) BY CO.CODE BY AUTH.DATE "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    FOR I = 1 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        CO.CODE<I>      = R.TT<TT.TE.CO.CODE>
*        TT.DATE = EN.DATE
*        KEY.ID<I> = "USD":"-" :"EGP":"-":TT.DATE
*        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
*        LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
*            SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
*        END
*  FCY.CURR = CURR<I>
**       TEXT = 'CUR=':CURR<I> ; CALL REM
        BEGIN CASE
            CASE CURR<I> = "USD"
                IF AMT.FCY<I> > 10000 THEN
                    Y.RET.DATA<-1> = CO.CODE<I> :"*": AUTH.DAT<I> :"*": CUST<I> :"*": CURR<I>:"*":AMT.FCY<I>:"*":KEY.LIST.N<I>:"*":ST.DATE:"*":EN.DATE
                END
**       TEXT = 'USD':CURR<I> ; CALL REM
            CASE CURR<I> = "EUR"
                IF AMT.FCY<I> > 8000 THEN
                    Y.RET.DATA<-1> = CO.CODE<I> :"*": AUTH.DAT<I> :"*": CUST<I> :"*": CURR<I>:"*":AMT.FCY<I>:"*":KEY.LIST.N<I>:"*":ST.DATE:"*":EN.DATE
                END
            CASE CURR<I> = "SAR"
                IF AMT.FCY<I> > 37500 THEN
                    Y.RET.DATA<-1> = CO.CODE<I> :"*": AUTH.DAT<I> :"*": CUST<I> :"*": CURR<I>:"*":AMT.FCY<I>:"*":KEY.LIST.N<I>:"*":ST.DATE:"*":EN.DATE
                END
        END CASE
    NEXT I
RETURN
END
