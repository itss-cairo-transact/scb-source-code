* @ValidationCode : MjotMTYzODY1NjI5NzpDcDEyNTI6MTY0MDc4MjMwODg0Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:51:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE DEAL.SLIP.STAMP(ARG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.LG.CHARGE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE

    MYID = ARG :".":R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.TAX.AMOUNT,MYID,MYAMT)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
MYAMT=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.TAX.AMOUNT>
    COM1 = MYAMT<1,1>
    COM2 = MYAMT<1,2>
    TOTAL = COM1 + COM2
    ARG = TOTAL

*ARG = SUM(R.NEW(SCB.LG.CH.TAX.AMOUNT))

RETURN
END
