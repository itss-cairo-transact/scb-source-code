* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>-192</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CUSTOMER.DATA

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR

    GOSUB INITIATE
    IF COMI THEN

        GOSUB PRINT.HEAD
*Line [ 44 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 45 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
        GOSUB CALLDB

        TEXT = " NO.CUSTOMERS ":SELECTED:" ����" ; CALL REM
        TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM

    END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:

    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT  ="      ": "     ���� ��� ������� ������ �� �������          "
    YTEXT :="      ": "���� ������ ����� ���� ���� ������ 1              "
    YTEXT :="      ": "     ���� ������ ����� ���� ������� 2             "
    YTEXT :="      ": "     ���� ������ ����� ���� ������ ����� ������� 3"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")

    WS.COMP = ID.COMPANY

    PGM.NAME = "CUSTOMER.DATE"

    RETURN
*===============================================================
CALLDB:

    FN.LD = 'FBNK.CUSTOMER' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    IF COMI = 1 THEN
*        T.SEL = "SELECT FBNK.CUSTOMER WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" BY  @ID"
        T.SEL = "SELECT FBNK.CUSTOMER WITH COMPANY.BOOK EQ ":WS.COMP:" BY @ID"
    END
    IF COMI = 2 THEN
*        T.SEL = "SELECT FBNK.CUSTOMER WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" BY  SECTOR"
        T.SEL = "SELECT FBNK.CUSTOMER WITH COMPANY.BOOK EQ ":WS.COMP:" BY  SECTOR"
    END
    IF COMI = 3 THEN
*        T.SEL = "SELECT FBNK.CUSTOMER WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" BY  INDUSTRY"
        T.SEL = "SELECT FBNK.CUSTOMER WITH COMPANY.BOOK EQ ":WS.COMP:" BY  INDUSTRY"
    END

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN
**        FOR I = 1 TO SELECTED
        FOR I = 1 TO 100
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LOCAL.REF     = R.LD<EB.CUS.LOCAL.REF>
            CUST.NO       = KEY.LIST<I>
            CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SECTOR':@FM:EB.SEC.DESCRIPTION,R.LD<EB.CUS.SECTOR>,CUSSEC)
F.ITSS.SECTOR = 'F.SECTOR'
FN.F.ITSS.SECTOR = ''
CALL OPF(F.ITSS.SECTOR,FN.F.ITSS.SECTOR)
CALL F.READ(F.ITSS.SECTOR,R.LD<EB.CUS.SECTOR>,R.ITSS.SECTOR,FN.F.ITSS.SECTOR,ERROR.SECTOR)
CUSSEC=R.ITSS.SECTOR<EB.SEC.DESCRIPTION>
            CUST.SECTOR   =  CUSSEC
*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('INDUSTRY':@FM:EB.IND.DESCRIPTION,R.LD<EB.CUS.INDUSTRY>,CUSINDUS)
F.ITSS.INDUSTRY = 'F.INDUSTRY'
FN.F.ITSS.INDUSTRY = ''
CALL OPF(F.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY)
CALL F.READ(F.ITSS.INDUSTRY,R.LD<EB.CUS.INDUSTRY>,R.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY,ERROR.INDUSTRY)
CUSINDUS=R.ITSS.INDUSTRY<EB.IND.DESCRIPTION>
            CUST.INDUSTRY =  CUSINDUS

            YY = SPACE(120)

            YY<1,1>[1,8]   = CUST.NO
            YY<1,1>[10,30]  = CUST.NAME
            YY<1,1>[40,30]  = CUST.ADDRESS
            YY<1,1>[70,25]  = CUSSEC
            YY<1,1>[105,15] = CUSINDUS

            PRINT YY<1,1>

        NEXT I
        PRINT STR('=',120)
        PRINT ; PRINT "������� = ":SELECTED:" ����"
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN

*===============================================================
PRINT.HEAD:
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):PGM.NAME
*    PR.HD :="'L'":" "
    IF COMI = 1 THEN
        PR.HD :="'L'":SPACE(50):"  ���� ������ ����� ���� ���� ������ "
        PR.HD :="'L'":SPACE(45):STR('_',50)
    END
    IF COMI = 2 THEN
        PR.HD :="'L'":SPACE(50):" ���� ������ ����� ���� ������� "
        PR.HD :="'L'":SPACE(45):STR('_',50)
    END
    IF COMI = 3 THEN
        PR.HD :="'L'":SPACE(50):" ���� ������ ����� ���� ������ ����� �������"
        PR.HD :="'L'":SPACE(45):STR('_',60)
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" �����":SPACE(10):" �����" :SPACE(30):"������� ":SPACE(22):"������ ":SPACE(17):"����� ����� ������� "
*    PR.HD :="'L'":SPACE(1):" �����":SPACE(10):" �����" :SPACE(30):"������� ":SPACE(22):"������ ":SPACE(17):" �������"
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    RETURN
*==============================================================
END
