* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE CPY.HOLD.CBE.502

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*---------------------------------------
    EXECUTE "CLEAR-FILE CBE.REP.502"
    PRINT "CLEAR-FILE CBE.REP.502"

    FN.HC = 'F.HOLD.CONTROL'   ; F.HC = ''
    CALL OPF(FN.HC,F.HC)

    T.SEL = '' ; SELECTED = '' ; KEY.LIST = ''
    DT    = ""
    DT    = TODAY

    WS.SEL = "800"
    GOSUB PROCESS

    WS.SEL = "300"
    GOSUB PROCESS

    WS.SEL = "310"
    GOSUB PROCESS

    WS.SEL = "400"
    GOSUB PROCESS

    WS.SEL = "1550"
    GOSUB PROCESS

*    WS.SEL = "1600" + 1610 +1620 + 1621
    WS.SEL = "16"
    GOSUB PROCESS

    WS.SEL = "SCB.CBE"
    GOSUB PROCESS

    TEXT = "PROGRAM DONE" ; CALL REM
    RETURN
*--------------------------------------------------
PROCESS:
    T.SEL  = "SELECT F.HOLD.CONTROL WITH REPORT.NAME LIKE " : WS.SEL : "..."
    T.SEL := " AND DATE.CREATED EQ ":DT:"  BY TIME.CREATED "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.HC,KEY.LIST<I>,R.HC,F.HC,E1)
            REP.NAM = R.HC<HCF.REPORT.NAME>
            CPY1    = "COPY FROM &HOLD& TO CBE.REP.502 " : KEY.LIST<I> :",": REP.NAM:".txt OVERWRITING"
            EXECUTE CPY1
        NEXT I
    END
    RETURN
END
