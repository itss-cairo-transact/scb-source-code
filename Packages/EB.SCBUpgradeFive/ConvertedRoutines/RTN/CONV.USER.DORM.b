* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE CONV.USER.DORM

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*----------------------------------------------------------------------**
    COMP = ID.COMPANY
    USER.ID   = ''
    USER.ID   = O.DATA
    FN.USE   = 'F.USER' ; F.USE    = ''
    CALL OPF(FN.USE,F.USE)

    FLG = ""
***--------------------------------------------------------------****
    CALL F.READ(FN.USE,USER.ID,R.USE,F.USE,E11)
    USER.OVARR =  R.USE<EB.USE.OVERRIDE.CLASS>
*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    OVARR = DCOUNT(USER.OVARR,@VM)

    FOR II = 1 TO OVARR
        USER.OVAL = R.USE<EB.USE.OVERRIDE.CLASS><1,II>

        IF USER.OVAL EQ "DORM" THEN
            FLG = "YES"
        END
    NEXT II
    O.DATA    = FLG
    RETURN
*==============================================================********
END
