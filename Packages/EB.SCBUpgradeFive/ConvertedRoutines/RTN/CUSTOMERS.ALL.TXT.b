* @ValidationCode : Mjo1NjkwMTgyNzU6Q3AxMjUyOjE2NDE5MTI2NDgyNDk6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 11 Jan 2022 16:50:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*****NESSREEN AHMED 29/1/2019***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CUSTOMERS.ALL.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SECTOR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.TITLE
*Line [ 44 ] Removed directory from $INCLUDE  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 48 ] Removed directory from $INCLUDE  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMP.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.PROFESSION


    TEXT = 'HELLO' ; CALL REM

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)



**********OPEN FILE********************************
    OPENSEQ "&SAVEDLISTS&" , "CUST.ACT.ALL.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CUST.ACT.ALL.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CUST.ACT.ALL.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CUST.ACT.ALL.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create CUST.DEBIT.CARD.csv File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
    BB.DATA = "Customer ID ":",":"Arabic Name":",":"Customer Branch":",":"Contact Date":",":"Gender":",":"Birth Date":",":"Age Today":",":"Account Officer Code":",":"Account Officer Name":",":"Sector Code":",":"Sector Name":",":"New Sector Code":",":"New Sector Name":",":"Nationality":",":"Residence":",":"Profession Code":",":"Profession Name"
**:",":"secondaryId":",":"secondaryIdType":",":"parentCardId":",":"parentNationalId":",":"parentsecondaryId":",":"parentsecondaryIdType":",":"closingDate":",":"statusId":",":"statusReason"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    S.SEL = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT LT 70 AND POSTING.RESTRICT NE 18) AND GENDER NE '' AND CONTACT.DATE NE '' AND BIRTH.INCORP.DATE NE '' BY COMPANY.BOOK BY CONTACT.DATE"
    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)

    TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM
    IF SELECTED.SS THEN
        FOR I = 1 TO SELECTED.SS
            AR.NAME = '' ; CU.GENDER = '' ; CUS.COMP = '' ; COMP.NAME = '' ; CU.BIRTH.D = '' ; CU.CONT.D = '' ; CUS.AGE = ''; CU.ACCT.OFF = '' ; CU.SECTOR = '' ; CU.NEW.SECT = '' ; CU.NATIONALITY = '' ; CU.RESIDENCE = '' ; CU.PROFESSION = ''
            SEC.NAME = '' ; NEW.SECT.NAME = ''  ; CU.ACCT.OFF.NAME = '' ; CU.PROFESSION.NAME = ''
            CALL F.READ(FN.CUSTOMER,KEY.LIST.SS<I>, R.CUSTOMER, F.CUSTOMER ,E3)
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
            AR.NAME    = LOCAL.REF.C<1,CULR.ARABIC.NAME>
            CU.GENDER  = LOCAL.REF.C<1,CULR.GENDER>
            CUS.COMP   = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CUS.COMP,COMP.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CUS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COMP.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
            CU.BIRTH.D = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
            CU.BRD.YEAR = CU.BIRTH.D[1,4]
            TODAY.D =  TODAY
            TODAY.YEAR = TODAY.D[1,4]
            CUS.AGE = TODAY.YEAR - CU.BRD.YEAR
            CU.CONT.D  = R.CUSTOMER<EB.CUS.CONTACT.DATE>
            CU.ACCT.OFF = R.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,CU.ACCT.OFF,CU.ACCT.OFF.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,CU.ACCT.OFF,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
CU.ACCT.OFF.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            CU.SECTOR  = R.CUSTOMER<EB.CUS.SECTOR>
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SECTOR':@FM:EB.SEC.DESCRIPTION,CU.SECTOR,SEC.NAME)
F.ITSS.SECTOR = 'F.SECTOR'
FN.F.ITSS.SECTOR = ''
CALL OPF(F.ITSS.SECTOR,FN.F.ITSS.SECTOR)
CALL F.READ(F.ITSS.SECTOR,CU.SECTOR,R.ITSS.SECTOR,FN.F.ITSS.SECTOR,ERROR.SECTOR)
SEC.NAME=R.ITSS.SECTOR<EB.SEC.DESCRIPTION>
            CU.NEW.SECT = LOCAL.REF.C<1,CULR.NEW.SECTOR>
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.NEW.SECTOR':@FM:C.SCB.NEW.SECTOR.NAME,CU.NEW.SECT,NEW.SECT.NAME)
F.ITSS.SCB.NEW.SECTOR = 'F.SCB.NEW.SECTOR'
FN.F.ITSS.SCB.NEW.SECTOR = ''
CALL OPF(F.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR)
CALL F.READ(F.ITSS.SCB.NEW.SECTOR,CU.NEW.SECT,R.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR,ERROR.SCB.NEW.SECTOR)
NEW.SECT.NAME=R.ITSS.SCB.NEW.SECTOR<C.SCB.NEW.SECTOR.NAME>
            CU.NATIONALITY = R.CUSTOMER<EB.CUS.NATIONALITY>
            CU.RESIDENCE = R.CUSTOMER<EB.CUS.RESIDENCE>
            CU.PROFESSION = LOCAL.REF.C<1,CULR.PROFESSION>
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,CU.PROFESSION,CU.PROFESSION.NAME)
F.ITSS.SCB.CUS.PROFESSION = 'F.SCB.CUS.PROFESSION'
FN.F.ITSS.SCB.CUS.PROFESSION = ''
CALL OPF(F.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION)
CALL F.READ(F.ITSS.SCB.CUS.PROFESSION,CU.PROFESSION,R.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION,ERROR.SCB.CUS.PROFESSION)
CU.PROFESSION.NAME=R.ITSS.SCB.CUS.PROFESSION<SCB.PRF.DESCRIPTION>

            BB.DATA = KEY.LIST.SS<I>:",":AR.NAME:",":COMP.NAME:",":CU.CONT.D:",":CU.GENDER:",":CU.BIRTH.D:",":CUS.AGE:",":CU.ACCT.OFF:",":CU.ACCT.OFF.NAME:",":CU.SECTOR:",":SEC.NAME:",":CU.NEW.SECT:",":NEW.SECT.NAME:",":CU.NATIONALITY:",":CU.RESIDENCE:",":CU.PROFESSION:",":CU.PROFESSION.NAME
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END   ;***End of main selection from Card.Issue***
    TEXT = '�� �������� �� �������� ' ;CALL REM;
RETURN
END
