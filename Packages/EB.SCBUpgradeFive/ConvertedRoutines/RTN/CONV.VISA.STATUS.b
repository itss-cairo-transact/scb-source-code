* @ValidationCode : MjoyMjIzNzc1NzY6Q3AxMjUyOjE2NDA2OTUxODM3Mjc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:39:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****CREATED BY MOHAMED SABRY  ****
SUBROUTINE CONV.VISA.STATUS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 26 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.READ.DB.ADVICE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.READ.DB.ADVICE
    WS.DATA =''
    WS.CARD.NO = O.DATA
    FN.VF = 'F.SCB.READ.DB.ADVICE'
    F.VF = '' ; R.VF = ''
    CALL OPF(FN.VF,F.VF)
    WS.TODAY = TODAY
    WS.DATE.YM = WS.TODAY[1,6]
    WS.DATE.YM = WS.DATE.YM : '01'
    CALL CDT('',WS.DATE.YM,'-1C')
    WS.DATE.YM = WS.DATE.YM[1,6]
*TEXT = WS.DATE.YM ; CALL REM
************************************************************
    T.SEL = "SELECT ":FN.VF:" WITH CARD.NO EQ ":WS.CARD.NO:" AND CARD.STAT EQ 'LVE' AND @ID LIKE ....":WS.DATE.YM:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*TEXT = SELECTED ; CALL REM
    CALL F.READ(FN.VF,KEY.LIST<SELECTED>,R.VF,F.VF,EER.R)
    WS.DATA = R.VF<SCB.DB.CONTRACT.STAT>:'*':R.VF<SCB.DB.ACCT.BAL>
    O.DATA = WS.DATA
RETURN
