* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE CONV.STMT.AC.READ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.ACH  = 'FBNK.ACCOUNT$HIS' ; F.ACH = '' ; R.ACH = ''
    CALL OPF(FN.ACH,F.ACH)

    WS.AC.ID = O.DATA

    CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,ER.AC)
    IF NOT(ER.AC) THEN
        WS.CU.ID    = R.AC<AC.CUSTOMER>
        WS.AC.TITLE = R.AC<AC.SHORT.TITLE>
        WS.AC.GL    = R.AC<AC.CATEGORY>
    END ELSE
        CALL F.READ.HISTORY(FN.ACH,WS.AC.ID,R.ACH,F.ACH,ER.ACH)
        WS.CU.ID    = R.ACH<AC.CUSTOMER>
        WS.AC.TITLE = R.ACH<AC.SHORT.TITLE>
        WS.AC.GL    = R.ACH<AC.CATEGORY>
    END
    O.DATA = WS.AC.TITLE :'*':WS.CU.ID:'*':WS.AC.GL
    RETURN
END
