* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.TT.ACCT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
    TT.ID = O.DATA
    IF TT.ID THEN
        F.TT = "FBNK.TELLER" ; FVAR.TT = ''
*OPEN F.TT TO FVAR.TT THEN
        CALL OPF(F.TT,FVAR.TT)
        CALL F.READ(F.TT,TT.ID,R.TT,FVAR.TT,TT.ERR)
 **       READ R.TT FROM FVAR.TT , TT.ID THEN
            TT.TR.CODE = R.TT<TT.TE.TRANSACTION.CODE>

            IF TT.TR.CODE = 37 OR TT.TR.CODE = 38 OR TT.TR.CODE = 32 OR TT.TR.CODE = 7  THEN
                O.DATA = R.TT<TT.TE.ACCOUNT.2>
            END ELSE
                O.DATA = R.TT<TT.TE.ACCOUNT.1><1,AV>
            END
*        END
*END
        RETURN
    END
