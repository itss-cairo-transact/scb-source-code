* @ValidationCode : MjoxNjM2NzY2Njg1OkNwMTI1MjoxNjQwODYyMjA4MjgzOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 13:03:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****NESSREEN AHMED 17/01/2017******************
*-----------------------------------------------------------------------------
* <Rating>775</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.CR.LT.EQV.USD100000.C(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BATCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - Missing Layout I_TT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CURRENCY.DAILY - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------------------*
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
    KEY.LIST.M = "" ; SELECTED.M = "" ;  ER.MSG.M = ""

    FN.TT   = 'FBNK.TELLER$HIS' ; F.TT = ''
    CALL OPF(FN.TT,F.TT)

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)


    TOT.TT = 0 ; TOT.AMT = 0 ; TOT.AMT.N = 0 ; TOT.AMT.LT = 0
    CUST.NTR = 0
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    TEXT = "ST.DATE=":ST.DATE ; CALL REM

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    TEXT = "EN.DATE=":EN.DATE ; CALL REM

    N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) AND CONTRACT.GRP EQ '' AND CUSTOMER.1 NE '99433300' BY CUSTOMER.1 "
**      N.SEL = "SELECT FBNK.TELLER$HIS WITH DR.CR.MARKER EQ 'CREDIT' AND CUSTOMER.1 NE '' AND CURRENCY.1 NE 'EGP' AND RECORD.STATUS EQ 'MAT' AND (AUTH.DATE GE ":ST.DATE :" AND AUTH.DATE LE " :EN.DATE :" ) AND CONTRACT.GRP EQ '' AND CUSTOMER.1 LIKE '63...' BY CUSTOMER.1 "
    CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)

    TEXT = "SEL=":SELECTED.N ; CALL REM
    CALL F.READ( FN.TT,KEY.LIST.N<1>, R.TT,F.TT, ERR.TT)

    CUST<1>         = R.TT<TT.TE.CUSTOMER.1>
    CURR<1>         = R.TT<TT.TE.CURRENCY.1>
    AMT.FCY<1>      = R.TT<TT.TE.AMOUNT.FCY.1>
    AMT.LCY<1>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
    AUTH.DAT<1>     = R.TT<TT.TE.AUTH.DATE>
    TT.DATE = AUTH.DAT<1>

    KEY.ID<1> = "USD":"-" :"EGP":"-":TT.DATE
    CALL F.READ(FN.CURR.DALY,KEY.ID<1>,R.CURR.DALY,F.CURR.DALY,ERR2)
    DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
    CURR.MARK = R.CURR.DALY<SCCU.CURR.MARKET>
    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
        SELL.RATE<1> = R.CURR.DALY<SCCU.SELL.RATE ,NN>
    END
    IF CURR<1> = "USD" THEN
        TOT.AMT.N = TOT.AMT.N + AMT.FCY<1>
    END ELSE
****OTHER FCY CURR*******************************
        AMT.EQV.USD = AMT.LCY<1> / SELL.RATE<1>
        TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
    END
**********END OF FIRST RECORD**********************************
    FOR I = 2 TO SELECTED.N
        CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
        CUST<I>         = R.TT<TT.TE.CUSTOMER.1>
        CURR<I>         = R.TT<TT.TE.CURRENCY.1>
        AMT.FCY<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
        AMT.LCY<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
        AUTH.DAT<I>     = R.TT<TT.TE.AUTH.DATE>
        TT.DATE = AUTH.DAT<I>
        LOCAL.REF.T     = R.TT<TT.TE.LOCAL.REF>

        KEY.ID<I> = "USD":"-" :"EGP":"-":TT.DATE
        CALL F.READ(FN.CURR.DALY,KEY.ID<I>,R.CURR.DALY,F.CURR.DALY,ERR2)
        DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
        LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
            SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
        END
        IF CUST<I> # CUST<I-1> THEN
            CALL F.READ(FN.CUSTOMER, CUST<I-1>, R.CUSTOMER, F.CUSTOMER, E1)
            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
            REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
            NW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
            IF  TOT.AMT.N LT 100000 THEN
**Y.RET.DATA<-1> = CUST<I-1> :"*": CURR<I-1>:"*":REG.NO:"*":NW.SECTOR :"*":TOT.AMT.N
                BEGIN CASE
                    CASE (NW.SECTOR GE 2110) AND (NW.SECTOR LE 2260)
                        TOT.AMT.I = TOT.AMT.I + TOT.AMT.N
                    CASE (NW.SECTOR GE 3110) AND (NW.SECTOR LE 3260)
                        TOT.AMT.C = TOT.AMT.C + TOT.AMT.N
                    CASE (NW.SECTOR GE 4110) AND (NW.SECTOR LE 4260)
                        TOT.AMT.S = TOT.AMT.S + TOT.AMT.N
                END CASE
                TOT.AMT.N = 0
            END ELSE
                TOT.AMT.N = 0
            END
            IF CURR<I> = "USD" THEN
                TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
            END ELSE
                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                END
                AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
            END     ;** END OF OTHER FCY**
*****SAME CUSTOMER*************
        END ELSE
            IF CURR<I> = "USD" THEN
                TOT.AMT.N = TOT.AMT.N + AMT.FCY<I>
            END ELSE
****OTHER FCY**********
                LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
                    SELL.RATE<I> = R.CURR.DALY<SCCU.SELL.RATE,NN>
                END
                AMT.EQV.USD = AMT.LCY<I> / SELL.RATE<I>
                TOT.AMT.N = TOT.AMT.N + AMT.EQV.USD
            END     ;**END OF OTHER FCY**
*****END OF SAME CUSTOMER******
        END         ;**END OF SAME CUSTOMER**
        IF I = SELECTED.N THEN
            IF TOT.AMT.N LT 100000 THEN
                TEXT = 'LAST RECORD' ; CALL REM
                CALL F.READ(FN.CUSTOMER, CUST<I>, R.CUSTOMER, F.CUSTOMER, E1)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                NW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
                BEGIN CASE
                    CASE (NW.SECTOR GE 2110) AND (NW.SECTOR LE 2260)
                        TOT.AMT.I = TOT.AMT.I + TOT.AMT.N
                    CASE (NW.SECTOR GE 3110) AND (NW.SECTOR LE 3260)
                        TOT.AMT.C = TOT.AMT.C + TOT.AMT.N
                    CASE (NW.SECTOR GE 4110) AND (NW.SECTOR LE 4260)
                        TOT.AMT.S = TOT.AMT.S + TOT.AMT.N
                END CASE
            END
        END
    NEXT I
    Y.RET.DATA<-1> = "�����" :"*":TOT.AMT.I
    Y.RET.DATA<-1> = "�����" :"*":TOT.AMT.C
    Y.RET.DATA<-1> = "����"  :"*":TOT.AMT.S

RETURN
END
