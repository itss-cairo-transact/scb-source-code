* @ValidationCode : MjoxMDkyMjUyNzAyOkNwMTI1MjoxNjQwNzkwNDA2NzcwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:06:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
****************MAHMOUD
***    PROGRAM    E.NOF.CBE.LOANS
SUBROUTINE E.NOF.CBE.LOANS(Y.RET.DATA)

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.RANGE
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 67 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 68 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB GET.CURR
RETURN
*========================================
INITIATE:
*-------
    K  = 0
    K2 = 0
    K3 = 0
    SSS= 0
***********************
    TTMM = TODAY[1,6]:"01"
    CALL CDT("",TTMM,'-1C')
    CRT TTMM
*#    TTMM = R.DATES(EB.DAT.LAST.PERIOD.END)
***********************
    TTD1 = TTMM[1,6]:"01"
    MON.DAYS = TTMM[2]
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''
    OVER.GRP   = ''
    LE1Y.GRP   = ' 1201 1202 1211 1212 1102 1401 1404 1405 1406 1416 1477 1480 1481 1483 1001 1501 1502 1503 1504 1507 1508 1509 1511 1512 1513 1514 1518 1519 1534 1544 1577 1582 1588 1591 1599 1402 1414 1301 1216 1560'
    LE3Y.GRP   = ' 1390 1303 1595 1216 1510 1560 1407 1408 1413'
    GT3Y.GRP   = ''
    AUTO.GRP   = ''
    RLST.GRP   = ''
    PERS.GRP   = ''
    CRDT.GRP   = ' 11232 11234 11238 11240 11313 1206 1208 1205 1207'
    OTHR.GRP   = ''

    AC.CAT.GRP = OVER.GRP:LE1Y.GRP:LE3Y.GRP:GT3Y.GRP:AUTO.GRP:RLST.GRP:PERS.GRP:CRDT.GRP:OTHR.GRP

    CRNT.ACR.L = '-'
    LE2W.ACR.L = ''
    LE1M.ACR.L = ''
    LE3M.ACR.L = ''
    LE6M.ACR.L = ''
    LE1Y.ACR.L = ''
    LE3Y.ACR.L = ''
    CD3Y.ACR.L = ''
    SVNG.ACR.L = ''
    OTHR.ACR.L = ''

    CRNT.GRP.L   = ''
    LE2W.GRP.L   = ''
    LE1M.GRP.L   = ''
    LE3M.GRP.L   = ''
    LE6M.GRP.L   = ''
    LE1Y.GRP.L   = ''
    LE3Y.GRP.L   = ''
    CD3Y.GRP.L   = ''
    SVNG.GRP.L   = ''
    OTHR.GRP.L   = ''

RETURN
******************************************************
CLEAR.VAR:
*----------
    BAL.OVER = 0
    AVR.OVER = 0
    MIN.OVER = 0
    MAX.OVER = 0
    BAL.LE1Y = 0
    AVR.LE1Y = 0
    MIN.LE1Y = 0
    MAX.LE1Y = 0
    BAL.LE3Y = 0
    AVR.LE3Y = 0
    MIN.LE3Y = 0
    MAX.LE3Y = 0
    BAL.GT3Y = 0
    AVR.GT3Y = 0
    MIN.GT3Y = 0
    MAX.GT3Y = 0
    BAL.AUTO = 0
    AVR.AUTO = 0
    MIN.AUTO = 0
    MAX.AUTO = 0
    BAL.RLST = 0
    AVR.RLST = 0
    MIN.RLST = 0
    MAX.RLST = 0
    BAL.PERS = 0
    AVR.PERS = 0
    MIN.PERS = 0
    MAX.PERS = 0
    BAL.CRDT = 0
    AVR.CRDT = 0
    MIN.CRDT = 0
    MAX.CRDT = 0
    BAL.OTHR = 0
    AVR.OTHR = 0
    MIN.OTHR = 0
    MAX.OTHR = 0

    REC.ACR.BAL = 0
    REC.INT1 = 0
    ACR.OVER = 0
    ACR.LE1Y = 0
    ACR.LE3Y = 0
    ACR.GT3Y = 0
    ACR.AUTO = 0
    ACR.RLST = 0
    ACR.PERS = 0
    ACR.CRDT = 0
    ACR.OTHR = 0

    CNT.1  = 0
    CNT.2  = 0
    CNT.3  = 0
    CNT.4  = 0
    CNT.5  = 0
    CNT.6  = 0
    CNT.7  = 0
    CNT.8  = 0
    CNT.9  = 0
    CNT.10 = 0
    TOT.1  = 0
    TOT.2  = 0
    TOT.3  = 0
    TOT.4  = 0
    TOT.5  = 0
    TOT.6  = 0
    TOT.7  = 0
    TOT.8  = 0
    TOT.9  = 0
    TOT.10 = 0

RETURN
*=======================================
CALLDB:
*-------
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = '' ; R.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)
    FN.AC.INT = 'FBNK.ACCOUNT.DEBIT.INT' ; F.AC.INT = '' ; R.AC.INT = ''
    CALL OPF(FN.AC.INT,F.AC.INT)
    FN.ST.DR = 'FBNK.STMT.ACCT.DR' ; F.ST.DR = '' ; R.ST.DR = ''
    CALL OPF(FN.ST.DR,F.ST.DR)
    FN.LM  = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.LM = '' ; R.LM = ''
    CALL OPF(FN.LM,F.LM)
    FN.BI  = 'FBNK.BASIC.INTEREST' ; F.BI = '' ; R.BI = ''
    CALL OPF(FN.BI,F.BI)
    FN.LL  = 'F.RE.STAT.LINE.BAL' ; F.LL = '' ; R.LL = ''
    CALL OPF(FN.LL,F.LL)
    FN.LL2  = 'F.RE.STAT.LINE.BAL' ; F.LL2 = '' ; R.LL2 = ''
    CALL OPF(FN.LL2,F.LL2)
    FN.LN  = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.LN = 'F.RE.STAT.REP.LINE'      ; F.LN = ''  ; R.LN = ''
    CALL OPF(FN.LN,F.LN)
    FN.CTE = 'F.CATEG.ENTRY' ; F.CTE = '' ; R.CTE = '' ; ER.CTE = ''
    CALL OPF(FN.CTE,F.CTE)
    FN.CPL = 'FBNK.CONSOLIDATE.PRFT.LOSS' ; F.CPL = '' ; R.CPL = '' ; ER.CPL = ''
    CALL OPF(FN.CPL,F.CPL)
    FN.SLC = 'F.RE.STAT.LINE.CONT' ; F.SLC = '' ; R.SLC = '' ; ER.SLC = ''
    CALL OPF(FN.SLC,F.SLC)

RETURN
*========================================
GET.CURR:
*-------
    CUR.SEL = "SSELECT ":FN.CUR:" WITH @ID EQ EGP USD"
    CALL EB.READLIST(CUR.SEL,CUR.LIST,'',SELECTED.CUR,ERR.MSG)
    LOOP
        REMOVE CUR.ID FROM CUR.LIST SETTING POSS.CUR
    WHILE CUR.ID:POSS.CUR
        SSS++
        GOSUB CLEAR.VAR
        GOSUB REC.ACC
*        GOSUB REC.LD
*        GOSUB REC.LD.H
*        GOSUB CALC.AVR
        GOSUB RET.REC
    REPEAT
RETURN
*========================================
REC.ACC:
*-------
***************************//AC//**************************************
    SEL.CMD  = "SELECT ":FN.ACC:" WITH OPEN.ACTUAL.BAL NE ''"
    SEL.CMD := " AND CURRENCY EQ ":CUR.ID
    SEL.CMD := " AND OPEN.ACTUAL.BAL LT 0 "
    SEL.CMD := " AND CATEGORY IN (":AC.CAT.GRP:")"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.MSG)
    CRT SELECTED
    LOOP
        REMOVE ACC.ID FROM KEY.LIST SETTING POSS
    WHILE ACC.ID:POSS
        K++
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ERR.1)
        REC.COM = R.ACC<AC.CO.CODE>
        REC.CUS = R.ACC<AC.CUSTOMER>
        REC.CAT = R.ACC<AC.CATEGORY>
        REC.CUR = R.ACC<AC.CURRENCY>
**        REC.BAL = R.ACC<AC.OPEN.ACTUAL.BAL>
        REC.DAT = TTMM
        CALL GET.ENQ.BALANCE(ACC.ID,TTMM,REC.BAL)
        REC.BAL = ABS(REC.BAL)
        REC.VER = ''
        AA.CAT = " ":REC.CAT
**        FINDSTR AA.CAT IN CRNT.GRP SETTING POS.123 THEN
**        GOSUB ACTUAL.RATE
**        END ELSE
        TDX1 = TTMM[1,6]:"01"
        AC.INT.ID = ACC.ID:"-":TDX1
        CALL F.READ(FN.AC.INT,AC.INT.ID,R.AC.INT,F.AC.INT,EERR.1)
        REC.INT1 = MAXIMUM(R.AC.INT<IC.ADI.DR.INT.RATE>)
        IF REC.INT1 EQ 0 THEN
            ST.AC.ID = ACC.ID:"-":TTMM
            CALL F.READ(FN.ST.DR,ST.AC.ID,R.ST.DR,F.ST.DR,EERR.123)
            IF R.ST.DR THEN
                REC.ACR.BAL = R.ST.DR<IC.STMDR.TOTAL.INTEREST>
                REC.INT1 = MAXIMUM(R.ST.DR<IC.STMDR.DR.INT.RATE>)
            END ELSE
                REC.ACR.BAL = 0
                REC.INT1 = 0
            END
        END
**        END
        IF REC.INT1 LT 0 THEN
            REC.INT1 = 0
        END
        GOSUB CALC.REC
    REPEAT
RETURN
*========================================
ACTUAL.RATE:
*---------
    REC.ACR.BAL = 0
    TOT.DR.BAL  = 0
    AVR.DR.BAL  = 0
    R.ST.DR = ''
    ST.AC.ID = ACC.ID:"-":TTMM
    CALL F.READ(FN.ST.DR,ST.AC.ID,R.ST.DR,F.ST.DR,EERR.1)
    ST.AC.DATE  = R.ST.DR<IC.STMDR.DR.INT.DATE>
    MON.DAYS = TTMM[2]
*Line [ 310 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AAA = DCOUNT(ST.AC.DATE,@VM)
    FOR MM = 1 TO AAA
        DR.DAYS = R.ST.DR<IC.STMDR.DR.NO.OF.DAYS,MM>
        DR.BAL  = R.ST.DR<IC.STMDR.DR.VAL.BALANCE,MM>
        TOT.DR.BAL += (DR.BAL * DR.DAYS)
    NEXT MM
    AVR.DR.BAL = TOT.DR.BAL / MON.DAYS
    REC.ACR.BAL = R.ST.DR<IC.STMDR.TOTAL.INTEREST>
    IF REC.ACR.BAL LT 1 THEN
        REC.ACR.BAL = 0
    END
    IF AVR.DR.BAL NE 0 THEN
        REC.INT1 = REC.ACR.BAL / AVR.DR.BAL * ( 365 / MON.DAYS ) * 100
    END ELSE
        REC.INT1 = 0
    END
RETURN
*========================================
REC.LD:
*-------
***************************//LD//*****************************************
    SEL.CMD2  = "SELECT ":FN.LD:" WITH CUSTOMER.ID NE ''"
    SEL.CMD2 := " AND CURRENCY EQ ":CUR.ID
    SEL.CMD2 := " AND ((VALUE.DATE LE ":TTMM:" AND AMOUNT NE 0)"
    SEL.CMD2 := "  OR  (FIN.MAT.DATE GT ":TTMM:" AND AMOUNT EQ 0))"
    SEL.CMD2 := " AND ((CATEGORY GE 21001 AND CATEGORY LE 21010)"
    SEL.CMD2 := "  OR  (CATEGORY GE 21020 AND CATEGORY LE 21029)"
    SEL.CMD2 := "  OR  (CATEGORY EQ 21032))"

**    SEL.CMD2 := " BY CURRENCY BY CATEGORY"

    CALL EB.READLIST(SEL.CMD2,KEY.LIST2,'',SELECTED2,ERR.2)
    CRT SELECTED2
    LOOP
        REMOVE LD.ID FROM KEY.LIST2 SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        REC.COM  = R.LD<LD.CO.CODE>
        REC.CUS  = R.LD<LD.CUSTOMER.ID>
        REC.CAT  = R.LD<LD.CATEGORY>
        REC.CUR  = R.LD<LD.CURRENCY>
        REC.INT  = R.LD<LD.INTEREST.RATE,1>
        REC.SPR  = R.LD<LD.INTEREST.SPREAD>
        INT.KEY  = R.LD<LD.INTEREST.KEY>
        REC.BAL  = R.LD<LD.AMOUNT>
        REC.DAT  = R.LD<LD.VALUE.DATE>
        REC.LCL  = R.LD<LD.LOCAL.REF>
        REC.VER  = REC.LCL<1,LDLR.VERSION.NAME>
        IF REC.BAL EQ 0 THEN
            REC.BAL  = R.LD<LD.REIMBURSE.AMOUNT>
        END
*===========================================
        IF REC.INT EQ '' OR REC.INT EQ 0 THEN
            BAS.INT.ID = INT.KEY:REC.CUR:TTMM[1,6]:"01"
            CALL F.READ(FN.BI,BAS.INT.ID,R.BI,F.BI,ERR.BI)
            REC.INT = R.BI<EB.BIN.INTEREST.RATE>
        END
        REC.INT1 = REC.INT + REC.SPR
*        LD.ACR.ID     = LD.ID:'00'
*        CALL F.READ(FN.LM,LD.ACR.ID, R.LM, F.LM, EE.A.11)
**        REC.ACR.BAL = R.LM<LD27.OUTS.CUR.ACC.I.PAY>
*        REC.ACR.BAL = R.LD<LD.TOT.INTEREST.AMT>
        IF REC.INT1 LT 0 THEN
            REC.INT1 = 0
        END
        GOSUB CALC.REC
*===========================================
    REPEAT
RETURN
*========================================
REC.LD.H:
*--------
*****************************//LD.H//************************************
    SEL.CMD3  = "SELECT ":FN.LD.H:" WITH CUSTOMER.ID NE ''"
    SEL.CMD3 := " AND CURRENCY EQ ":CUR.ID
    SEL.CMD3 := " AND CURR.NO EQ 1"
    SEL.CMD3 := " AND VALUE.DATE LIKE ":TTMM[1,6]:"..."
    SEL.CMD3 := " AND ((CATEGORY GE 21001 AND CATEGORY LE 21010)"
    SEL.CMD3 := "  OR  (CATEGORY GE 21020 AND CATEGORY LE 21029)"
    SEL.CMD3 := "  OR  (CATEGORY EQ 21032))"

**    SEL.CMD3 := " BY CURRENCY BY CATEGORY"

    CALL EB.READLIST(SEL.CMD3,KEY.LIST3,'',SELECTED3,ERR.3)
    CRT SELECTED3
    LOOP
        REMOVE LD.ID.H FROM KEY.LIST3 SETTING POSS3
    WHILE LD.ID.H:POSS3
        K3++
        CALL F.READ(FN.LD.H,LD.ID.H,R.LD.H,F.LD.H,ERR.13)
        REC.COM  = R.LD.H<LD.CO.CODE>
        REC.CUS  = R.LD.H<LD.CUSTOMER.ID>
        REC.CAT  = R.LD.H<LD.CATEGORY>
        REC.CUR  = R.LD.H<LD.CURRENCY>
        REC.INT  = R.LD.H<LD.INTEREST.RATE,1>
        REC.SPR  = R.LD.H<LD.INTEREST.SPREAD>
        INT.KEY  = R.LD.H<LD.INTEREST.KEY>
        REC.BAL  = 0
        REC.DAT  = R.LD.H<LD.VALUE.DATE>
        REC.LCL  = R.LD.H<LD.LOCAL.REF>
        REC.VER  = REC.LCL<1,LDLR.VERSION.NAME>
*===========================================
        IF REC.INT EQ '' OR REC.INT EQ 0 THEN
            BAS.INT.ID = INT.KEY:REC.CUR:TTMM[1,6]:"01"
            CALL F.READ(FN.BI,BAS.INT.ID,R.BI,F.BI,ERR.BI)
            REC.INT = R.BI<EB.BIN.INTEREST.RATE>
        END
        REC.INT1 = REC.INT + REC.SPR
        IF REC.INT1 LT 0 THEN
            REC.INT1 = 0
        END
        GOSUB CALC.REC
*===========================================
    REPEAT
RETURN
*========================================
*****************************************************************
CALC.REC:
*--------
    REC.CAT = " ":REC.CAT
    IF REC.CUS NE '' THEN
*Line [ 434 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,REC.CUS,CUS.SEC)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,REC.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
    END
    IF (REC.DAT[1,6] EQ TTMM[1,6]) AND (CUS.SEC NE 1100 AND CUS.SEC NE 1200 AND CUS.SEC NE 1300 AND CUS.SEC NE 1400) AND (REC.CUS NE 1304955 AND REC.CUS NE 1101254 AND REC.CUS NE 1101253 AND REC.CUS NE 1305067 ) AND (REC.VER NE ',SCB.ELIQ') THEN
        INT.FLAG = 1
    END ELSE
        INT.FLAG = 0
    END
    FINDSTR REC.CAT IN OVER.GRP SETTING POS.1 THEN
        IF REC.BAL GT 0 THEN
            BAL.OVER += REC.BAL
            ACR.OVER += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.1 += REC.INT1
                CNT.1++
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.OVER THEN
                        MIN.OVER = REC.INT1
                    END
                    IF REC.INT1 GE MAX.OVER THEN
                        MAX.OVER = REC.INT1
                        TOP.CUS.OVER  = REC.CUS
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE1Y.GRP SETTING POS.2 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE1Y += REC.BAL
            ACR.LE1Y += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.2 += REC.INT1
                CNT.2++
                IF MIN.LE1Y = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE1Y = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE1Y THEN
                        MIN.LE1Y = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE1Y THEN
                        MAX.LE1Y = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN LE3Y.GRP SETTING POS.3 THEN
        IF REC.BAL GT 0 THEN
            BAL.LE3Y += REC.BAL
            ACR.LE3Y += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.3 += REC.INT1
                CNT.3++
                IF MIN.LE3Y = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.LE3Y = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.LE3Y THEN
                        MIN.LE3Y = REC.INT1
                    END
                    IF REC.INT1 GE MAX.LE3Y THEN
                        MAX.LE3Y = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN GT3Y.GRP SETTING POS.4 THEN
        IF REC.BAL GT 0 THEN
            BAL.GT3Y += REC.BAL
            ACR.GT3Y += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                IF REC.INT1 NE 0 THEN
                    TOT.4 += REC.INT1
                    CNT.4++
                END
                IF MIN.GT3Y = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.GT3Y = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.GT3Y THEN
                        MIN.GT3Y = REC.INT1
                    END
                    IF REC.INT1 GE MAX.GT3Y THEN
                        MAX.GT3Y = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN AUTO.GRP SETTING POS.5 THEN
        IF REC.BAL GT 0 THEN
            BAL.AUTO += REC.BAL
            ACR.AUTO += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.5 += REC.INT1
                CNT.5++
                IF MIN.AUTO = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.AUTO = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.AUTO THEN
                        MIN.AUTO = REC.INT1
                    END
                    IF REC.INT1 GE MAX.AUTO THEN
                        MAX.AUTO = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN RLST.GRP SETTING POS.6 THEN
        IF REC.BAL GT 0 THEN
            BAL.RLST += REC.BAL
            ACR.RLST += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.6 += REC.INT1
                CNT.6++
                IF MIN.RLST = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.RLST = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.RLST THEN
                        MIN.RLST = REC.INT1
                    END
                    IF REC.INT1 GE MAX.RLST THEN
                        MAX.RLST = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN PERS.GRP SETTING POS.7 THEN
        IF REC.BAL GT 0 THEN
            BAL.PERS += REC.BAL
            ACR.PERS += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.7 += REC.INT1
                CNT.7++
                IF MIN.PERS = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.PERS = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.PERS THEN
                        MIN.PERS = REC.INT1
                    END
                    IF REC.INT1 GE MAX.PERS THEN
                        MAX.PERS = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN CRDT.GRP SETTING POS.8 THEN
        IF REC.BAL GT 0 THEN
            BAL.CRDT += REC.BAL
            ACR.CRDT += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                TOT.8 += REC.INT1
                CNT.8++
                IF MIN.CRDT = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.CRDT = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.CRDT THEN
                        MIN.CRDT = REC.INT1
                    END
                    IF REC.INT1 GE MAX.CRDT THEN
                        MAX.CRDT = REC.INT1
                    END
                END
            END
        END
    END
    FINDSTR REC.CAT IN OTHR.GRP SETTING POS.10 THEN
        IF REC.BAL GT 0 THEN
            BAL.OTHR += REC.BAL
            ACR.OTHR += REC.ACR.BAL
            IF INT.FLAG EQ 1 THEN
                IF REC.INT1 NE 0 THEN
                    TOT.10 += REC.INT1
                    CNT.10++
                END
                IF MIN.OTHR = 0 THEN
                    IF REC.INT1 GT 0 THEN
                        MIN.OTHR = REC.INT1
                    END
                END
                IF REC.INT1 GT 0 THEN
                    IF REC.INT1 LE MIN.OTHR THEN
                        MIN.OTHR = REC.INT1
                    END
                    IF REC.INT1 GE MAX.OTHR THEN
                        MAX.OTHR = REC.INT1
                    END
                END
            END
        END
    END
RETURN
*-----------------------------------
CALC.AVR:
*--------
    GRP.LL  = CRNT.GRP.L
    GRP.PLL = CRNT.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.CRNT = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL  = LE2W.GRP.L
    GRP.PLL = LE2W.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE2W = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL  = LE1M.GRP.L
    GRP.PLL = LE1M.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE1M = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL  = LE3M.GRP.L
    GRP.PLL = LE3M.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE3M = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL  = LE6M.GRP.L
    GRP.PLL = LE6M.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE6M = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL  = LE1Y.GRP.L
    GRP.PLL = LE1Y.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE1Y = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL  = LE3Y.GRP.L
    GRP.PLL = LE3Y.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.LE3Y = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL = CD3Y.GRP.L
    GRP.PLL= CD3Y.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.CD3Y = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL = SVNG.GRP.L
    GRP.PLL= SVNG.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.SVNG = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
    GRP.LL = OTHR.GRP.L
    GRP.PLL= OTHR.ACR.L
    GOSUB GET.AVVR.BAL
    IF AVVR.BAL NE 0 THEN
        AVR.OTHR = (ACR.BAL / AVVR.BAL) * ( 365 / MON.DAYS ) * 100
    END
*--------------------------
RETURN
*====================================
GET.AVVR.BAL:
*---------
    TOT.L.BAL  = 0
    TOT.PL.BAL = 0
    AVVR.BAL   = 0
    ACR.BAL    = 0
    LL.ACR.BAL = 0
    LL.BAL     = 0
    PLL.BAL    = 0
    CO.SEL = "SSELECT F.COMPANY"
    CALL EB.READLIST(CO.SEL,CO.LIST,'',SELECTED.CO,ER.MSG.CO)
    GRP.LL.COUNT  = COUNT(GRP.LL,'.')
    GRP.PLL.COUNT = COUNT(GRP.PLL,'.')
    CRT SELECTED.CO
    FOR CCC = 1 TO SELECTED.CO
        CO.ID = CO.LIST<CCC>
        FOR QQ = 1 TO GRP.LL.COUNT
            LL.BAL = 0
            LL.NO = FIELD(FIELD(GRP.LL,'-',QQ),'.',1):"-":FIELD(FIELD(GRP.LL,'-',QQ),'.',2)
            C.DATE = TTD1
            CALL CDT("",C.DATE,'-1C')
            LOOP
                CALL CDT("",C.DATE,'+1C')
            WHILE C.DATE LE TTMM
                LL.ID = LL.NO:"-":CUR.ID:"-":C.DATE:"*":CO.ID
                GOSUB CALC.AVVR
            REPEAT
        NEXT QQ
        FOR YY = 1 TO GRP.PLL.COUNT
            PLL.BAL = 0
            PLL.NO = FIELD(FIELD(GRP.PLL,'-',YY),'.',1):"-":FIELD(FIELD(GRP.PLL,'-',YY),'.',2)
            PLL.ID = PLL.NO:"-":CUR.ID:"-":TTMM:"*":CO.ID
            GOSUB CALC.ACR
        NEXT YY
    NEXT CCC
    AVVR.BAL = TOT.L.BAL / MON.DAYS
    ACR.BAL = TOT.PL.BAL
    IF AVVR.BAL LT 0 THEN
        AVVR.BAL = AVVR.BAL * -1
    END
    IF ACR.BAL LT 0 THEN
        ACR.BAL = ACR.BAL * -1
    END
RETURN
*====================================
CALC.AVVR:
*--------
    CALL F.READ(FN.LL,LL.ID,R.LL,F.LL,ER.LL1)
    IF R.LL THEN
        LL.BAL = R.LL<RE.SLB.CLOSING.BAL>
    END
    TOT.L.BAL += LL.BAL
RETURN
*====================================
CALC.ACR:
*--------
    CALL F.READ(FN.LL2,PLL.ID,R.LL2,F.LL2,ER.LL1)
    PLL.BAL = R.LL2<RE.SLB.CR.MVMT.MTH>+R.LL2<RE.SLB.DB.MVMT.MTH>
    TOT.PL.BAL += PLL.BAL
RETURN
*====================================
GET.CATEG:
*---------
    Z = 1
    CALL F.READ(FN.LN,LN.ID,R.LN,F.LN,ERR.LN)
*Line [ 779 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    PROD.NO = DCOUNT(R.LN<RE.SRL.ASSET1>,@VM)
    FOR XX = 1 TO PROD.NO
        PRODUCT = R.LN<RE.SRL.ASSET1,XX>
        IF PRODUCT[1,1] EQ '*' THEN
            MM++
            GOSUB GETRANGE
        END ELSE
            FINDSTR " ":PRODUCT IN CATEG SETTING POS.CATT THEN NULL ELSE
                CATEG = CATEG:" ":PRODUCT
            END
        END
    NEXT XX
RETURN
***********************************************
GETRANGE:
********
    SS = LEN(PRODUCT) - 1
    RG.ID = PRODUCT[2,SS]
    CALL F.READ(FN.RG,RG.ID,R.RG,F.RG,ERR.RG)
    CATFR = R.RG<RE.RNG.START.RANGE>
    CATTO = R.RG<RE.RNG.END.RANGE>
    CATTT = CATTT - 1
    LOOP
        CATTT++
    WHILE CATTT LE CATTO
        FINDSTR CATTT IN CATEG SETTING POS.CATT1 THEN NULL ELSE
            CATEG = CATEG:" ":CATTT
        END
    REPEAT
RETURN
*-----------------------*

*...................................
RET.REC:
********
    CRT    TTMM
    CRT    "BAL.OVER = ": BAL.OVER
    CRT    "AVR.OVER = ": AVR.OVER
    CRT    "MIN.OVER = ": MIN.OVER
    CRT    "MAX.OVER = ": MAX.OVER
    CRT    "BAL.LE1Y = ": BAL.LE1Y
    CRT    "AVR.LE1Y = ": AVR.LE1Y
    CRT    "MIN.LE1Y = ": MIN.LE1Y
    CRT    "MAX.LE1Y = ": MAX.LE1Y
    CRT    "BAL.LE3Y = ": BAL.LE3Y
    CRT    "AVR.LE3Y = ": AVR.LE3Y
    CRT    "MIN.LE3Y = ": MIN.LE3Y
    CRT    "MAX.LE3Y = ": MAX.LE3Y
    CRT    "BAL.GT3Y = ": BAL.GT3Y
    CRT    "AVR.GT3Y = ": AVR.GT3Y
    CRT    "MIN.GT3Y = ": MIN.GT3Y
    CRT    "MAX.GT3Y = ": MAX.GT3Y
    CRT    "BAL.AUTO = ": BAL.AUTO
    CRT    "AVR.AUTO = ": AVR.AUTO
    CRT    "MIN.AUTO = ": MIN.AUTO
    CRT    "MAX.AUTO = ": MAX.AUTO
    CRT    "BAL.RLST = ": BAL.RLST
    CRT    "AVR.RLST = ": AVR.RLST
    CRT    "MIN.RLST = ": MIN.RLST
    CRT    "MAX.RLST = ": MAX.RLST
    CRT    "BAL.PERS = ": BAL.PERS
    CRT    "AVR.PERS = ": AVR.PERS
    CRT    "MIN.PERS = ": MIN.PERS
    CRT    "MAX.PERS = ": MAX.PERS
    CRT    "BAL.CRDT = ": BAL.CRDT
    CRT    "AVR.CRDT = ": AVR.CRDT
    CRT    "MIN.CRDT = ": MIN.CRDT
    CRT    "MAX.CRDT = ": MAX.CRDT
    CRT    "BAL.OTHR = ": BAL.OTHR
    CRT    "AVR.OTHR = ": AVR.OTHR
    CRT    "MIN.OTHR = ": MIN.OTHR
    CRT    "MAX.OTHR = ": MAX.OTHR
    Y.RET.DATA<-1>=BAL.OVER:"*": AVR.OVER:"*": MIN.OVER:"*":MAX.OVER:"*":BAL.LE1Y:"*":AVR.LE1Y:"*":MIN.LE1Y:"*":MAX.LE1Y:"*":BAL.LE3Y:"*":AVR.LE3Y:"*":MIN.LE3Y:"*":MAX.LE3Y:"*":BAL.GT3Y:"*":AVR.GT3Y:"*":MIN.GT3Y:"*":MAX.GT3Y:"*":BAL.AUTO:"*":AVR.AUTO:"*":MIN.AUTO:"*":MAX.AUTO:"*":BAL.RLST:"*":AVR.RLST:"*":MIN.RLST:"*":MAX.RLST:"*":BAL.PERS:"*":AVR.PERS:"*":MIN.PERS:"*":MAX.PERS:"*":BAL.CRDT:"*":AVR.CRDT:"*":MIN.CRDT:"*":MAX.CRDT:"*":BAL.OTHR:"*":AVR.OTHR:"*":MIN.OTHR:"*":MAX.OTHR:"*":CUR.ID:"*":TTMM:"*":SSS
RETURN
*************************************************
END
