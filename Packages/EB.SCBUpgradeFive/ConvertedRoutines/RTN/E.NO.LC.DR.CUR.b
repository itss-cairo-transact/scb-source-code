* @ValidationCode : MjotNDczNzExODQ2OkNwMTI1MjoxNjQwNzg4NjkwODc0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:38:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
SUBROUTINE E.NO.LC.DR.CUR(ENQUIRY.DATA)
**    PROGRAM E.NO.LC.DR.CUR

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.LC.DR - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LC.DR

**TEXT='E.NO.LC.DR.CUR';CALL REM
    COMP.ID = ID.COMPANY; TD1 = TODAY; LC.NU = ''; LC.CUR  = ''; LC.AMT  = ''; LIAB.AMT  = ''
    KEY.LIST = '' ; SELECTED ='' ;  ER.MSG =''
    KEY.LIST1 = '' ; SELECTED1='' ;  ER.MSG1=''
***************************
    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    FN.DRW = 'FBNK.DRAWINGS' ; F.DRW = '' ; R.DRW = ''
    FN.LOG ='F.SCB.LC.DR'   ;F.LOG=''    ;R.LOG=''

    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DRW,F.DRW)
    CALL OPF(FN.LOG,F.LOG)
******************************* TO GET LC RECORDS
**TD1='20141224'
** COMP.ID ='EG0010032'
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH  LC.TYPE LIKE 'LED...' AND EXPIRY.DATE GT ": TD1 :" AND CO.CODE EQ ": COMP.ID :" AND LIABILITY.AMT GE 0  BY LC.TYPE "

    CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED,ASD)
**PRINT SELECTED
    FOR I = 1 TO SELECTED
        LC.ID =KEY.LIST<I>

        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)
        CALL F.READ(FN.LOG,LC.ID,R.LOG,F.LOG,ERRLOG)

        R.LOG<LC.LC.AMOUNT>     = R.LC<TF.LC.LC.AMOUNT>
        R.LOG< LC.LC.CURRENCY>  = R.LC<TF.LC.LC.CURRENCY>
        R.LOG<LC.LC.LIAB>       = R.LC<TF.LC.LIABILITY.AMT>
        R.LOG< LC.LC.TYPE>      = R.LC<TF.LC.LC.TYPE>
        R.LOG<LC.LC.CO.CODE>   = COMP.ID
        R.LOG<LC.LC.NO>   =  1
        DR.NEX  = R.LC<TF.LC.NEXT.DRAWING>
        DR.NO   = DR.NEX - 1

        IF DR.NO EQ '0' THEN
            CALL F.WRITE(FN.LOG,LC.ID,R.LOG)
            CALL JOURNAL.UPDATE(LC.ID)
        END ELSE
            FOR XX = 1 TO DR.NO

                IF XX LT 10 THEN
                    XX ="0":XX
                    DRW.ID =  LC.ID : XX
                END ELSE
                    DRW.ID =  LC.ID : XX
                END
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DR.CON=  DCOUNT(R.LOG<LC.DRAWING>,@VM)
                DR.CON=  DR.CON+1

                CALL F.READ(FN.DRW,DRW.ID,R.DRW,F.DRW,ERR11)
*********************
                IF R.DRW<TF.DR.DRAWING.TYPE>  EQ 'DP' THEN
                    LOCATE DRW.ID IN R.LOG<LC.DRAWING,1> SETTING POS THEN
                    END ELSE
                        DP.NO=DP.NO+1
                        R.LOG<LC.DRAWING,DR.CON>    =  DRW.ID
                        R.LOG<LC.DRW.AMT,DR.CON>    =  R.DRW<TF.DR.DOCUMENT.AMOUNT>
                        R.LOG<LC.DRW.CURR,DR.CON>   =  R.DRW<TF.DR.DRAW.CURRENCY>
                        R.LOG<LC.DRW.TYPE,DR.CON>   =  R.DRW<TF.DR.DRAWING.TYPE>
                        R.LOG<LC.DRW.CON,DR.CON>    =  R.DRW<TF.DR.LC.CREDIT.TYPE>
                        R.LOG<LC.DRW.DATE,DR.CON>   =  R.DRW<TF.DR.MATURITY.REVIEW>
                        R.LOG<LC.DR.NO>   =  DP.NO
                    END
                END ELSE
*****************
                    IF R.DRW<TF.DR.DRAWING.TYPE>  EQ 'MD' THEN
                        LOCATE DRW.ID IN R.LOG<LC.DRAWING,1> SETTING POSS THEN
                            R.LOG<LC.DRW.TYPE,POSS> = 'MD'
                            R.LOG<LC.DRW.AMT,POSS>    = ''
                        END
                    END
                END
*****************
                R.LOG<LC.DR.NO>   =  DP.NO
                CALL F.WRITE(FN.LOG,LC.ID,R.LOG)
                CALL JOURNAL.UPDATE(LC.ID)
**********************
            NEXT XX
        END
    NEXT I
***********TO GET DRAWINGS RECORDS
    T.SEL1 = "SELECT FBNK.DRAWINGS WITH LC.CREDIT.TYPE LIKE LED...  AND CO.CODE EQ ": COMP.ID :" BY @ID "
    CALL EB.READLIST(T.SEL1, KEY.LIST1, "", SELECTED1, ASD1)

    FOR J = 1 TO SELECTED1

        DRW.ID = KEY.LIST1<J>
        LC.ID = DRW.ID[1,12]

        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)
        CALL F.READ(FN.LOG,LC.ID,R.LOG,F.LOG,ERRLOG)
        EXP.DAT      = R.LC<TF.LC.EXPIRY.DATE>

        CALL F.READ(FN.DRW,DRW.ID,R.DRW,F.DRW,ERR11)
        DR.TYPE = R.DRW<TF.DR.DRAWING.TYPE>
        IF DR.TYPE EQ 'DP' THEN
            IF EXP.DAT LT TODAY THEN
*********************************************
                LOCATE KEY.LIST1<J> IN R.LOG<LC.DRAWING,1> SETTING POS THEN
                END ELSE
                    R.LOG<LC.LC.AMOUNT>     = R.LC<TF.LC.LC.AMOUNT>
                    R.LOG< LC.LC.CURRENCY>  = R.LC<TF.LC.LC.CURRENCY>
                    R.LOG<LC.LC.LIAB>       = R.LC<TF.LC.LIABILITY.AMT>
                    R.LOG< LC.LC.TYPE>      = R.LC<TF.LC.LC.TYPE>
                    R.LOG<LC.LC.CO.CODE>    = COMP.ID
                    IF R.LOG<LC.LC.AMOUNT> NE '' THEN
                        R.LOG<LC.LC.NO>         =  1
                    END
***********************************************
                    DP.NO = DP.NO+1
*Line [ 143 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DR.CON=  DCOUNT(R.LOG<LC.DRAWING>,@VM)
                    DR.CON=  DR.CON+1
                    CALL F.READ(FN.DRW,DRW.ID,R.DRW,F.DRW,ERR11)
                    R.LOG<LC.DRAWING,DR.CON>    =  DRW.ID
                    R.LOG<LC.DRW.AMT,DR.CON>    =  R.DRW<TF.DR.DOCUMENT.AMOUNT>
                    R.LOG<LC.DRW.CURR,DR.CON>   =  R.DRW<TF.DR.DRAW.CURRENCY>
                    R.LOG<LC.DRW.TYPE,DR.CON>   =  R.DRW<TF.DR.DRAWING.TYPE>
                    R.LOG<LC.DRW.CON,DR.CON>    =  R.DRW<TF.DR.LC.CREDIT.TYPE>
                    R.LOG<LC.DRW.DATE,DR.CON>   =  R.DRW<TF.DR.MATURITY.REVIEW>
                    R.LOG<LC.DR.NO>   =  DP.NO
*****************************************
                    CALL F.WRITE(FN.LOG,LC.ID,R.LOG)
                    CALL JOURNAL.UPDATE(LC.ID)
                END
            END
        END ELSE
            IF DR.TYPE  EQ 'MD' THEN
                LOCATE DRW.ID IN R.LOG<LC.DRAWING,1> SETTING POSD THEN
                    R.LOG<LC.DRW.TYPE,POSD>   = 'MD'
                    R.LOG<LC.DRW.AMT,POSD>    = ''
                    CALL F.WRITE(FN.LOG,LC.ID,R.LOG)
                    CALL JOURNAL.UPDATE(LC.ID)

                END
            END
        END

    NEXT J
RETURN
END
