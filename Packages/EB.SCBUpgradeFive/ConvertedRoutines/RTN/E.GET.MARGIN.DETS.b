* @ValidationCode : MjotNjA5NTgyNzE5OkNwMTI1MjoxNjQwNzg4NjA3NTA4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:36:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.GET.MARGIN.DETS
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.LG.CUS - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CUS

    FN.SCB.LG.CUS = "F.SCB.LG.CUS"
    FV.SCB.LG.CUS = ""
    CALL OPF(FN.SCB.LG.CUS,FV.SCB.LG.CUS)

    SCB.LG.CUS.ID = "C-":O.DATA
    CALL F.READ(FN.SCB.LG.CUS,SCB.LG.CUS.ID,R.SCB.LG.CUS,FV.SCB.LG.CUS,"")
    O.DATA = R.SCB.LG.CUS<SCB.LGCS.MARGIN.PERC>

RETURN
END
