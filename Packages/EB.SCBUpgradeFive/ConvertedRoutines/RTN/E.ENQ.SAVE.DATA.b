* @ValidationCode : MjoxNjkyMzQ0NzMyOkNwMTI1MjoxNjQwNzg4MzAzODI4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:31:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.ENQ.SAVE.DATA(ENQUIRY.DATA)
**  PROGRAM      E.ENQ.SAVE.DATA

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.ENQ.SAVE.DATA - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ENQ.SAVE.DATA


*=====================================================

*DATE.TIME
*-----------
    SAM    = TIMEDATE()
    O.DATA = SAM
    TIM    = O.DATA[1,8]
    DAT    = O.DATA[10,11]
    DD     = DAT[1,2]
    MM     = DAT[4,3]
    YY     = DAT[8,4]
    DDD    = TIM:"-":TODAY

*=================================================
*ID.COMPANY
    ENQ.NAME =ENQUIRY.DATA<1>
    ID.ALL= ENQ.NAME:'.':ID.COMPANY:'.':OPERATOR:'.':DDD
*=================================================
    FN.ENQ.SAV = 'F.SCB.ENQ.SAVE.DATA' ;F.ENQ.SAV = '' ; R.ENQ.SAV = '' ; READ.ERR = '' ; RETRY = ''
    CALL OPF(FN.ENQ.SAV,F.ENQ.SAV)
    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""

    CALL F.READ(FN.ENQ.SAV,ID.ALL,R.ENQ.SAV,F.ENQ.SAV,READ.ERR)


    R.ENQ.SAV<ENQ.SAVE.ENQ.USERID>    = OPERATOR
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('USER':@FM:EB.USE.USER.NAME,OPERATOR,USERNAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,OPERATOR,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USERNAME=R.ITSS.USER<EB.USE.USER.NAME>
    R.ENQ.SAV<ENQ.SAVE.ENQ.USERNAME>  = USERNAME
    R.ENQ.SAV<ENQ.SAVE.ENQ.DATE>      = TODAY
    R.ENQ.SAV<ENQ.SAVE.ENQ.TIME>      = TIM
    R.ENQ.SAV<ENQ.SAVE.ENQ.COMPANY>   = ID.COMPANY
    R.ENQ.SAV<ENQ.SAVE.ENQ.NAME>      = ENQUIRY.DATA<1>

*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    MULTI.RECORD = DCOUNT(ENQUIRY.DATA<2>,@VM)
    FOR I = 1 TO MULTI.RECORD
        R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.NAME,I> =   ENQUIRY.DATA<2,I>
        R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.OPER,I> =   ENQUIRY.DATA<3,I>
        R.ENQ.SAV<ENQ.SAVE.ENQ.FLD.DATA,I> =   ENQUIRY.DATA<4,I>

    NEXT I
    CALL F.WRITE (FN.ENQ.SAV, ID.ALL , R.ENQ.SAV )
    CALL JOURNAL.UPDATE(ID.ALL)
***    WRITE  R.ENQ.SAV TO F.ENQ.SAV , ID.ALL ON ERROR
***        PRINT "CAN NOT WRITE RECORD":KEY.LIST:"TO" :FN.ENQ.SAV
***    END

RETURN
END
