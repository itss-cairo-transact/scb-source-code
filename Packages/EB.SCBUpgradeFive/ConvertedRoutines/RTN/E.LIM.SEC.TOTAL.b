* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
      SUBROUTINE E.LIM.SEC.TOTAL
* This suboutine calculates the total valuation amount
* and the total margin value  for all customer portfolios
* portfolios
*
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SEC.ACC.MASTER
*
      WCUSTOMER = O.DATA
*
      SEC.RECORD = ""
      F.SEC.ACC.CUST = ''
      CALL OPF('F.SEC.ACC.CUST',F.SEC.ACC.CUST)
*
      R.SEC.ACC.CUST = ''
      CALL F.READ('F.SEC.ACC.CUST',WCUSTOMER,R.SEC.ACC.CUST,F.SEC.ACC.CUST,ER)
      SEC.TOTAL = 0
      MARGIN.TOTAL = 0
      IF R.SEC.ACC.CUST THEN
*Line [ 44 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
         NB.SAM = DCOUNT(R.SEC.ACC.CUST,@FM)
         FOR I = 1 TO NB.SAM
            CALL F.READ("F.SEC.ACC.MASTER",R.SEC.ACC.CUST<I>,SEC.RECORD,F.SEC.ACC.MASTER,SEC.READ.ERR)
            IF NOT(SEC.READ.ERR) THEN
               SEC.TOTAL = SEC.TOTAL + SEC.RECORD<SC.SAM.VALUATION.AMT>
               MARGIN.TOTAL = MARGIN.TOTAL + SEC.RECORD<SC.SAM.MARGIN.VALUE>
            END
         NEXT I
         O.DATA = SEC.TOTAL:'/':MARGIN.TOTAL
      END
      RETURN
   END
