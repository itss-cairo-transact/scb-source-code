* @ValidationCode : Mjo1MTI2Mzk0NDU6Q3AxMjUyOjE2NDA4Njg2OTY5NzE6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 14:51:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
******* noha *******

SUBROUTINE CONV.UNDER.BAL.PER.MONTH

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCR.ACCT.CR
*Line [ 33 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.STMT.ACCT.SAVE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.STMT.ACCT.SAVE

    FN.ST  = 'F.SCB.STMT.ACCT.SAVE'    ; F.ST = '' ; R.ST = ''
    CALL OPF(FN.ST,F.ST)

    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]
    IF MONTH[1,1] EQ 0 THEN
        MONTH = MONTH[2,1]
    END


*========== CALC DATE MINUS ONE WORKING DAY ==========
    IF MONTH EQ 1 THEN
        NEW.MONTH = 12
        NEW.YEAR  = YEAR - 1
    END
    ELSE
        NEW.MONTH = MONTH - 1
        NEW.YEAR  = YEAR
    END

    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END

    DATE.B4    = NEW.YEAR:NEW.MONTH

*=====================================================
    IF O.DATA THEN
        ACC.NO = O.DATA
        STMT.ACCT  = ACC.NO:"-":DATE.B4

        CALL F.READ( FN.ST,STMT.ACCT, R.ST, F.ST, STMT.ERR)
        BAL    = R.ST<STMT.SAVE.CR.INT.AMT>

        O.DATA = BAL
    END


RETURN
END
