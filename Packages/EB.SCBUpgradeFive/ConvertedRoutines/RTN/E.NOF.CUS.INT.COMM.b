* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE E.NOF.CUS.INT.COMM(Y.RET.DATA)

****Mahmoud Elhawary******3/8/2010****************************
*   nofile routine to get customer debit interest & charges  *
**************************************************************

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 48 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 49 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
****************************************
INITIATE:
*--------
    ACCT.ID   = ''
    CUS.ID    = ''
    AC.CAT    = ''
    AC.CUR    = ''
    MNTH.NUM = ''
    NO.OF.MONTHS = ''
    XX = ''
    RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.SDR = 'FBNK.STMT.ACCT.DR' ; F.SDR = '' ; R.SDR = '' ; ER.SDR = ''
    CALL OPF(FN.SDR,F.SDR)

    RETURN
****************************************
PROCESS:
*-------
    LOCATE "CUSTOMER.NO" IN D.FIELDS<1> SETTING YCUS.POS THEN CUS.ID    = D.RANGE.AND.VALUE<YCUS.POS> ELSE RETURN
    LOCATE "START.DATE"  IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE"    IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
    CUS.COM = R.CUS<EB.CUS.COMPANY.BOOK>
    CUS.LCL = R.CUS<EB.CUS.LOCAL.REF>
    CRD.STA = CUS.LCL<1,CULR.CREDIT.STAT>
    CRD.COD = CUS.LCL<1,CULR.CREDIT.CODE>
    IF CRD.STA NE '' THEN
        RETURN
    END ELSE
        IF CRD.COD EQ 110 OR CRD.COD EQ 120 THEN
            RETURN
        END ELSE
            CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ER.CUS.ACC)
            LOOP
                REMOVE ACCT.ID FROM R.CUS.ACC SETTING POS.CUS.ACC
            WHILE ACCT.ID:POS.CUS.ACC
                TOTAL.INT = 0
                TOTAL.COM = 0
                CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
                AC.CUR = R.ACCT<AC.CURRENCY>
                AC.CAT = R.ACCT<AC.CATEGORY>
                IF AC.CAT NE 1002 THEN
                    GOSUB GET.DR.INT
                    GOSUB RET.DATA
                END
            REPEAT
        END
    END
    RETURN
************************************************************************
GET.DR.INT:
*----------
    CALL MONTHS.BETWEEN(FROM.DATE,END.DATE,NO.OF.MONTHS)
    MNTH.NUM = NO.OF.MONTHS + 1
    FRDT = FROM.DATE
    FOR XX = 1 TO MNTH.NUM
        CALL LAST.DAY(FRDT)
        SDR.ID = ACCT.ID:"-":FRDT
        CALL F.READ(FN.SDR,SDR.ID,R.SDR,F.SDR,Y.SDR.ERR)
        SDR.DATE = FIELD(SDR.ID,'-',2)
        TOT.INT = R.SDR<IC.STMDR.TOTAL.INTEREST>
        TOT.COM = R.SDR<IC.STMDR.TOTAL.CHARGE>
        INT.DT  = R.SDR<IC.STMDR.DR.INT.DATE>
        TOTAL.COM += TOT.COM
        IF SDR.DATE[1,6] NE FROM.DATE[1,6] AND SDR.DATE[1,6] NE END.DATE[1,6] THEN
            TOTAL.INT += TOT.INT
        END ELSE
*Line [ 131 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            SS = DCOUNT(INT.DT,@VM)
            FOR II = 1 TO SS
                INT.DR.DT  = R.SDR<IC.STMDR.DR.INT.DATE,II>
                IF INT.DR.DT GE FROM.DATE AND INT.DR.DT LE END.DATE THEN
                    TOTAL.INT +=  R.SDR<IC.STMDR.DR.INT.AMT,II>
                END
            NEXT II
        END
        CALL ADD.MONTHS(FRDT,'1')
    NEXT XX
    RETURN
***********************************************************
RET.DATA:
*--------
    Y.RET.DATA<-1> = CUS.COM:"*":CUS.ID:"*":FROM.DATE:"*":END.DATE:"*":ACCT.ID:"*":AC.CAT:"*":AC.CUR:"*":TOTAL.INT:"*":TOTAL.COM
    RETURN
**************************************
END
