* @ValidationCode : MjotNDg1MzAzNjU6Q3AxMjUyOjE2NDA4NzM5NDgzODM6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 30 Dec 2021 16:19:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
PROGRAM CR.MOKHSAS.AC
*Line [ 17 to 27 ] Remove T24.BP - ITSS - R21 Upgrade - 2021-12-23
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.MM.MONEY.MARKET
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.DRAWINGS
*Line [ 40 ] Missing Layout I_F.SCB.FUND- ITSS - R21 Upgrade - 2021-12-23
*$INSERT           I_F.SCB.FUND
*Line [ 42 ] Missing Layout I_FT.LOCAL.REFS- ITSS - R21 Upgrade - 2021-12-23
*  $INSERT         I_FT.LOCAL.REFS

*Line [ 45 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CUS.POS.TST - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_F.SCB.CUS.POS.TST
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-30
    $INSERT I_CU.LOCAL.REFS
*--------------------------------------------------------*
    GOSUB INIT
    GOSUB AC.SUB
INIT:
*-----
    F.COUNT = '' ; FN.COUNT = 'F.SCB.CUS.POS.TST'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.CUSS = '' ; FN.CUSS = 'FBNK.CUSTOMER'; R.CUSS = ''
    CALL OPF(FN.CUSS,F.CUSS)

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''
    CALL OPF( FN.ACCOUNT,F.ACCOUNT)

RETURN
*-----------------------------*
AC.SUB:
*------
    T.SEL  = "SELECT FBNK.ACCOUNT WITH OPEN.ACTUAL.BAL NE ''"
    T.SEL := " AND CATEGORY NE 1701 "
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACCOUNT,KEY.LIST<I>, R.ACCOUNT,F.ACCOUNT, ETEXT)
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.ACCOUNT<AC.CURRENCY>,CURR.NO)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.ACCOUNT<AC.CURRENCY>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR.NO=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
            ID.NO = R.ACCOUNT<AC.CUSTOMER>:"*AC*BNK*":CURR.NO:R.ACCOUNT<AC.CATEGORY>:"*":KEY.LIST<I>:"*":TODAY

            CALL F.READ(FN.CUSS,R.ACCOUNT<AC.CUSTOMER>, R.CUSS,F.CUSS, ETEXT1)

            CURR = R.ACCOUNT<AC.CURRENCY>
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100

            R.COUNT<CUPOS.RATE> = MID.RATE<1,1>

            AMT = R.ACCOUNT<AC.OPEN.ACTUAL.BAL>

            LCY.AMT  = (MID.RATE<1,1> * AMT)
            LCY.AMTT = DROUND(LCY.AMT,2)
            R.COUNT<CUPOS.LCY.AMOUNT>  = LCY.AMTT
            R.COUNT<CUPOS.CUSTOMER>    = R.ACCOUNT<AC.CUSTOMER>
            R.COUNT<CUPOS.DEAL.CCY>    = R.ACCOUNT<AC.CURRENCY>
            R.COUNT<CUPOS.DEAL.AMOUNT> = R.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            R.COUNT<CUPOS.CATEGORY>    = R.ACCOUNT<AC.CATEGORY>
            R.COUNT<CUPOS.SYS.DATE>    = TODAY
            R.COUNT<CUPOS.CO.CODE>     = R.ACCOUNT<AC.CO.CODE>

            CALL F.WRITE (FN.COUNT, ID.NO , R.COUNT )
            CALL JOURNAL.UPDATE(ID.NO)
        NEXT I
    END
RETURN
END
