* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE DEL.LINE.MVMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*----------------------------------------------
    NK = ''
    NK<1,1>='NK'  ; NK<1,2>='02'  ; NK<1,3>='03'  ; NK<1,4>='04'  ; NK<1,5>='05'
    NK<1,6>='06'  ; NK<1,7>='07'  ; NK<1,8>='09'  ; NK<1,9>='10'  ; NK<1,10>='11'
    NK<1,11>='12' ; NK<1,12>='13' ; NK<1,13>='14' ; NK<1,14>='15' ; NK<1,15>='20'
    NK<1,16>='21' ; NK<1,17>='22' ; NK<1,18>='23' ; NK<1,19>='30' ; NK<1,20>='31'
    NK<1,21>='32' ; NK<1,22>='35' ; NK<1,23>='40' ; NK<1,24>='50'
    NK<1,25>='51' ; NK<1,26>='60'
    NK<1,27>='70' ; NK<1,28>='80' ; NK<1,29>='81' ; NK<1,30>='90' ; NK<1,31>='99'
    CALL !HUSHIT(1)

    FOR KK = 1 TO 31
        FN.L.CONT = "FB":NK<1,KK>:".RE.SLC.NZD"
        F.L.CONT  = ''
        CALL OPF(FN.L.CONT,F.L.CONT)
        SEL1 = "SELECT ":FN.L.CONT:" WITH @ID LIKE GENLED.... OR @ID LIKE SCBPLFIN...."
        EXECUTE SEL1
        OPEN '',FN.L.CONT TO HCFL ELSE STOP "CAN'T OPEN HOLDCTRL"
        FLG = 0
        LOOP UNTIL FLG = 1
            READNEXT ID ELSE FLG = 1
            IF FLG = 1 THEN
                GOTO END1
            END
            HREC = ""
**            READ HREC FROM HCFL,ID ELSE STOP
CALL F.READ(HCFL,ID,HREC,HCFL,ERR.A1)
            WS.MOV.NO = DCOUNT(HREC<16>,@VM)
            FOR I = 1 TO WS.MOV.NO
                IF HREC<16,I> EQ 'LINEMVMT' THEN
                    DEL  HREC<15,I>
                    DEL  HREC<16,I>
                    DEL  HREC<17,I>
                    PRINT HREC<15,I>
                    I--
                END
            NEXT I
            WRITE HREC TO F.L.CONT , ID ON ERROR
                PRINT "CAN NOT WRITE RECORD ":ID :" TO ":F.L.CONT
            END

*-----------------------------------------------------------------
END1:
        REPEAT
        CALL !HUSHIT(0)
    NEXT KK
END
