* @ValidationCode : MjoxMDE0NTI1MDgwOkNwMTI1MjoxNjQwNzg3Mjg3MTM5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:14:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
SUBROUTINE E.BUILD.CUS.POSITION1.N(ENQUIRY.DATA)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.POSITION
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*****UPDATED BY NESSREEN AHMED 8/10/2019****
*Line [ 37 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.CUS.RESTRICTED - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CUS.RESTRICTED

    FN.CUS.REST = 'F.SCB.CUS.RESTRICTED' ;F.CUS.REST = '' ; R.CUS.REST = ''
    CALL OPF(FN.CUS.REST,F.CUS.REST)

*****END OF UPDATED 8/10/2019****************

    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''


** 19/12/97 - GB9701470
**            Build the data dependent on the rebuild flag being set
*
*  08/06/05 - EN_10002549
*             Set C$CUS.POS.UPDATE.XREF to 0, so that cache is used
*             instead on CUSTOMER.POSITION.XREF.

    LOCATE 'REBUILD.DATA' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
        REBUILD = ENQUIRY.DATA<4,RB.POS>
    END ELSE
        REBUILD = 'Y'
    END
    IF REBUILD[1,1] NE 'N' THEN         ;* Do not execute on level dwon
        LOCATE "CUSTOMER.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
            CUST.ID = ENQUIRY.DATA<4,CUS.POS>
*Line [ 63 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CONVERT " " TO @VM IN CUST.ID
*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF DCOUNT(CUST.ID,@VM) GT 1 OR CUST.ID = "ALL" THEN
                ENQ.ERROR = "ONLY ONE CUSTOMER ALLOWED"
            END ELSE
*********************************************
                FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
                CALL OPF(FN.CUS,F.CUS)
                KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""
                CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,READ.ERR1)
                LOOP
                    REMOVE AC.ID FROM R.CUS SETTING POS.CUST
                WHILE AC.ID:POS.CUST

                    IF ( AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 ) OR ( AC.ID[11,4] EQ 1710 OR AC.ID[11,4] EQ 1711 ) THEN
                        CUST.ID = "DUMMY"
                    END
                    CALL OPF( FN.ACCOUNT,F.ACC)
                    CALL F.READ(FN.ACCOUNT,AC.ID,R.ACC,F.ACC, ETEXT)
                    SUSP = R.ACC<AC.INT.NO.BOOKING>

                    IF  SUSP EQ "SUSPENSE"  THEN
                        CUST.ID = "DUMMY"
                    END
                REPEAT
********************************************************
                C$CUST.POS.UPDATE.XREF = 0        ;* EN_10002549
                CALL CUS.BUILD.POSITION.DATA(CUST.ID)
                C$CUST.POS.UPDATE.XREF = 1        ;* EN_10002549 Reset to 1
            END
        END
    END
*******UPDATED BY NESSREEN AHMED ON 30/12/2008**********************
    FN.CU = 'FBNK.CUSTOMER' ;F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)
    CALL F.READ(FN.CU,CUST.ID,R.CU,F.CU,READ.ERR1)

    EMP.NO = R.CU<EB.CUS.LOCAL.REF,CULR.EMPLOEE.NO>

    USR.N =  R.USER<EB.USE.SIGN.ON.NAME>
    USR.ID = TRIM(USR.N, "0", "L")
    IF  EMP.NO NE USR.ID THEN
        FN.CUS.POS = 'FBNK.CUSTOMER.POSITION' ;F.CUS.POS = '' ; R.CUS.POS = ''
        CALL OPF(FN.CUS.POS,F.CUS.POS)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""

*--------------------------20100328------------------------------
**      T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...*1002)"
        T.SELL = "SELECT FBNK.CUSTOMER.POSITION WITH @ID LIKE ":CUST.ID :"... AND (@ID LIKE ...*1002 OR @ID LIKE ...*1408 OR @ID LIKE ...*1407 OR @ID LIKE ...*1413 )"
*--------------------------20100328------------------------------

        CALL EB.READLIST(T.SELL,KEY.LIST,"",SELECTED,ER.MSG)
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,READ.ERR)
***Updated by Nessreen Ahmed 25/9/2016 *************************
            DELETE F.CUS.POS , KEY.LIST<I>
***         CALL F.DELETE (FN.CUS.POS , KEY.LIST<I>)
***End of Update 25/9/2016 9 ****************************
        NEXT I
    END
*************************************************************
*
*****UPDATED BY NESSREEN AHMED 20/10/2019**************************
    CALL F.READ(FN.CUS.REST,CUST.ID,R.CUS.REST,F.CUS.REST,ERR.CUS)
    IF  NOT(ERR.CUS) THEN
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.ID,CUS.COMP)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.COMP=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
        IF CUS.COMP # ID.COMPANY THEN
            TEXT = "SECRET.INFORMATION " ; CALL REM
            ENQUIRY.DATA<2,1> = 'CUSTOMER.ID'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET.INFORMATION'
        END
    END
*****END OF UPDATE 20/10/2019**************************************
RETURN
*
END
