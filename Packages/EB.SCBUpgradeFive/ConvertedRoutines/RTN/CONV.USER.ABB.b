* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
    SUBROUTINE CONV.USER.ABB

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*----------------------------------------------------------------------**
    COMP = ID.COMPANY
    USER.ID   = ''
    USER.ID   = O.DATA
    FN.USE    = 'F.USER.ABBREVIATION' ; F.USE    = ''
    CALL OPF(FN.USE,F.USE)

    NUM.COUNT = 0
    NN        = ""
***--------------------------------------------------------------****
    CALL F.READ(FN.USE,USER.ID,R.USE,F.USE,E11)
    USER.ABB  = R.USE<EB.UAB.ABBREVIATION>

*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NUM.COUNT = DCOUNT(USER.ABB,@VM)
    FOR I = 1 TO NUM.COUNT
        NN   = " / ":"[" :R.USE<EB.UAB.ORIGINAL.TEXT><1,I>:"]": R.USE<EB.UAB.ABBREVIATION><1,I> : NN
    NEXT I 
    O.DATA    = NN
    RETURN
*==============================================================********
END
