* @ValidationCode : MjotMTIyNDE1NTExNjpDcDEyNTI6MTY0MDc4OTQ2NTUyODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:51:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
***NESSREEN AHMED 24/09/2014*********************************
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE E.NOF.ATM.CUS.APP.SUEZ.BNK.D(Y.RET.DATA)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - Missing Layout I_CI.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_CI.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.VISA.CARD.TYPE - ITSS - R21 Upgrade - 2021-12-23
*    $INCLUDE I_F.SCB.VISA.CARD.TYPE
*Line [ 42 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.ATM.APP - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.APP
*-----------------------------------------------------------------------*
    FN.CUST = 'F.CUSTOMER' ; F.CUST = '' ; R.CUST = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.CUST,F.CUST)
    FN.ATM.APP = 'F.SCB.ATM.APP' ; F.ATM.APP = '' ; R.ATM.APP = '' ; RETRY2 = '' ; E2 = ''
    CALL OPF(FN.ATM.APP,F.ATM.APP)

    T.SEL = "SELECT F.SCB.ATM.APP WITH ATM.APP.DATE GE '20140904' BY ATM.APP.DATE"

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SEL.ATM=':SELECTED ; CALL REM

    CALL F.READ(FN.ATM.APP, KEY.LIST<1>, R.ATM.APP, F.ATM.APP, E1)
    CUST.NO<1>    =  R.ATM.APP<SCB.VISA.CUSTOMER>
    FOR I = 2 TO SELECTED
        CALL F.READ(FN.ATM.APP, KEY.LIST<I>, R.ATM.APP, F.ATM.APP, E1)
        CUST.NO<I>     =  R.ATM.APP<SCB.VISA.CUSTOMER>
        IF CUST.NO<I> NE CUST.NO<I-1> THEN
            CALL F.READ(FN.ATM.APP, KEY.LIST<I-1>, R.ATM.APP, F.ATM.APP, E2)
            APP.DAT<I-1> =  R.ATM.APP<SCB.VISA.ATM.APP.DATE>
            APP.COMP<I-1>=  R.ATM.APP<SCB.VISA.CO.CODE>
            CALL F.READ(FN.CUST, CUST.NO<I-1>, R.CUST, F.CUST, ER.C)
            LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>
            CUST.BANK<I-1> = LOCAL.REF<1,CULR.CU.BANK>
            CUST.SCCD<I-1> = LOCAL.REF<1,CULR.SCCD.CUSTOMER>
            IF (CUST.BANK<I-1> NE '') AND (CUST.SCCD<I-1> EQ 'YES' ) THEN
                IF CUST.BANK<I-1> NE '0017' THEN
                    Y.RET.DATA<-1> = CUST.NO<I-1> :"*":APP.DAT<I-1>:"*":APP.COMP<I-1> :"*":CUST.BANK<I-1>
                    CUST = '' ; APP.DAT = '' ; APP.COMP = ''
                END
            END
        END
        IF I = SELECTED  THEN
            CALL F.READ(FN.ATM.APP, KEY.LIST<I>, R.ATM.APP, F.ATM.APP, E2)
            APP.DAT<I> =  R.ATM.APP<SCB.VISA.ATM.APP.DATE>
            APP.COMP<I>=  R.ATM.APP<SCB.VISA.CO.CODE>
            CALL F.READ(FN.CUST, CUST.NO<I>, R.CUST, F.CUST, ER.C)
            LOCAL.REF = R.CUST<EB.CUS.LOCAL.REF>
            CUST.BANK<I> = LOCAL.REF<1,CULR.CU.BANK>
            CUST.SCCD<I> = LOCAL.REF<1,CULR.SCCD.CUSTOMER>
            IF (CUST.BANK<I> NE '') AND (CUST.SCCD<I> EQ 'YES' ) THEN
                IF CUST.BANK<I> NE '0017' THEN
                    Y.RET.DATA<-1> = CUST.NO<I> :"*":APP.DAT<I>:"*":APP.COMP<I> :"*":CUST.BANK<I>
                END
            END
        END
    NEXT I
*-----------------------------------------------------------------------*
RETURN
END
