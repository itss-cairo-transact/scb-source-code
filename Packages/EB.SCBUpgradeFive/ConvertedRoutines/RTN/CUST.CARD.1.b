* @ValidationCode : MjoxNzMwMzg2OTQzOkNwMTI1MjoxNjQwNzMxNTk4NTYyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 00:46:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOO---------------------------------------------
SUBROUTINE CUST.CARD.1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - Missing Layout I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE


    FN.CUS='FBNK.CUSTOMER';F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
    FN.AC='FBNK.ACCOUNT';F.AC=''
    CALL OPF(FN.AC,F.AC)
    FN.CARD='FBNK.CARD.ISSUE';F.CARD=''
    CALL OPF(FN.CARD,F.CARD)


    XX  = O.DATA
***TEXT = XX ; CALL REM
    T.SEL = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":XX
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
***TEXT = "SEL : " : SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            AC.ID = KEY.LIST<I>
***     TEXT = "AC.ID : " : AC.ID ; CALL REM
            T.SEL1 = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":AC.ID:" AND @ID LIKE ATMC..."
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
****   TEXT = "SEL1 : " : SELECTED1 ; CALL REM
            IF SELECTED1 = 0 THEN
                O.DATA = ''
            END
            IF SELECTED1 NE 0 THEN
                O.DATA = KEY.LIST1
            END
**** TEXT = "O.DATA : " : O.DATA ; CALL REM
        NEXT I
    END
RETURN
END
