* @ValidationCode : MjoxMjQwNjYxMDMyOkNwMTI1MjoxNjQwNjg3NTUyODMwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:32:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.TRANS.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.POSITION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.TRANS.TODAY - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY

*** BOOKING.DATE = O.DATA

*** CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,LOK,LOCK.AMT)
    IF O.DATA = 1 THEN
        T.SEL = "SELECT F.SCB.TRANS.TODAY WITH BOOKING.DATE LIKE ...0902... AND ACCOUNT.OFFICER EQ 1 AND INPUTTER UNLIKE ...SCB... AND INPUTTER LIKE ...OFS..."
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            O.DATA = SELECTED
        END ELSE
            O.DATA = ''
        END
        RETURN
    END
