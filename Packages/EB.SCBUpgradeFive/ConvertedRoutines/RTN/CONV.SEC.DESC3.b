* @ValidationCode : Mjo1MjkxMzg4Mzk6Q3AxMjUyOjE2NDA2ODM3OTUzMDQ6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:29:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.SEC.DESC3

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - Missing Layout "I_F.SBM.LG.SECTOR.1" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SBM.LG.SECTOR.1

    HH  = O.DATA

    BEGIN CASE
        CASE HH = 11
            RR = "����� ���� ����� � �������� ������ � �����"
        CASE HH = 21
            RR = "�������� ������� "
        CASE HH = 31
            RR =  "�������� �����"
        CASE HH = 41
            RR =  "������ ����"
        CASE HH = 51
            RR =  "������ ���� ����� ���� ��� ��� ���� ������"
        CASE HH = 61
            RR =  "�������� ����� �� ������ ���� ��������� ����������"
        CASE OTHERWISE
            RR = ""
    END CASE

    O.DATA = RR
RETURN
END
