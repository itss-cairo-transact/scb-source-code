* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*** Copy of E.BUILD.USR.AC with modifications by Mahmoud 15/3/2009***
* Version 4 02/06/00  GLOBUS Release No. R06.005 16/04/07
    SUBROUTINE E.BUILD.USR.AC2(ENQUIRY.DATA)

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CUSTOMER.POSITION.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.LIABILITY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


*    LOCATE 'ACCOUNT' IN ENQUIRY.DATA<2,1> SETTING RB.POS THEN
*        ACCC = ENQUIRY.DATA<4,RB.POS>
*    END ELSE
*        REBUILD = 'Y'
*    END

    FN.ACC = 'FBNK.ACCOUNT' ;F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    CALL F.READ(FN.AC,ACCC,R.ACC,F.ACC,READ.ERR)
    CUS.ID = R.ACC<AC.CUSTOMER>
    CATEG = R.ACC<AC.CATEGORY>

********************************
    FN.CUS = 'FBNK.CUSTOMER.ACCOUNT' ;F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    KEY.LIST.LLL= "" ; SELECTED.LLL= "" ; ER.MSG.LLL= ""

    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,READ.ERR1)

    LOOP
        REMOVE AC.ID FROM R.CUS SETTING POS.CUST
    WHILE AC.ID:POS.CUST

        IF AC.ID[11,4] GE 1220 AND AC.ID[11,4] LE 1227 THEN
            ENQUIRY.DATA<2,1> = 'ACCOUNT'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET INFORMATION'
        END
    REPEAT

********************************

    IF CATEG EQ 1002 THEN
        TEXT = 'CATEG=':CATEG ; CALL REM
        FN.CU = 'FBNK.CUSTOMER' ;F.CU = '' ; R.CU = ''
        CALL OPF(FN.CU,F.CU)
        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,READ.ERR1)

        EMP.NO = R.CU<EB.CUS.LOCAL.REF,CULR.EMPLOEE.NO>
**        TEXT = "EMP=   " :  EMP.NO ; CALL REM
**        TEXT = 'CUS.ID=':CUS.ID  ; CALL REM

        FN.USR = 'FBNK.USER' ;F.USR = '' ; R.USR = ''
        CALL OPF(FN.USR,F.USR)

        USR.N =  R.USER<EB.USE.SIGN.ON.NAME>
        USR.ID = TRIM(USR.N, "0", "L")
**        TEXT = "USR.N =  ": USR.N ; CALL REM
        IF  EMP.NO NE USR.ID THEN
**           TEXT = "NESREEN A7MED " ; CALL REM
            ENQUIRY.DATA<2,1> = 'ACCOUNT'
            ENQUIRY.DATA<3,1> = 'EQ'
            ENQUIRY.DATA<4,1> = 'SECRET INFORMATION'

        END
    END

    RETURN
END
