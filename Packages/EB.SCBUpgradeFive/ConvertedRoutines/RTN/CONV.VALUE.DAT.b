* @ValidationCode : MjotODU1NTc3NjYxOkNwMTI1MjoxNjQwNjkxNzMwOTI2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 13:42:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
*****NESSREEN AHMED 20/11/2011*********************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.VALUE.DAT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - Missing Layout I_F.SCB.TRANS.TODAY - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY

    XX = O.DATA

    FN.LC = 'FBNK.LETTER.OF.CREDIT'
    F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = 'FBNK.DRAWINGS'
    F.DR = '' ; R.DR = ''
    CALL OPF(FN.DR,F.DR)

    IF XX[1,2] EQ 'TF' THEN
        ID.USE = XX[1,14]
        CALL F.READ(FN.DR,ID.USE,R.DR,F.DR,E1.DR)
        DR.TYPE = R.DR<TF.DR.DRAWING.TYPE>
        VAL.DAT = R.DR<TF.DR.VALUE.DATE>
        MAT.DAT = R.DR<TF.DR.MATURITY.REVIEW>
        IF DR.TYPE EQ 'DP' THEN
            O.DATA = MAT.DAT
        END ELSE
            O.DATA = VAL.DAT
        END
    END

RETURN
