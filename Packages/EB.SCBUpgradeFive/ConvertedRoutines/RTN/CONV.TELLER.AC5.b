* @ValidationCode : Mjo5NDA5MzE1MDpDcDEyNTI6MTY0MTkxMjM3NTE4OTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 11 Jan 2022 16:46:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
SUBROUTINE CONV.TELLER.AC5
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE
    $INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 25 ] Removed directory from $INCLUDE - Missing Layout I_CO.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
************************************************************
    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
RETURN
************************************************************
INIT:
    XX = O.DATA
    AMT = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.TEL = 'F.SCB.TRANS.TODAY'
    F.TEL = ''
    R.TEL = ''
    AMTLCY = 0
    AMTFCY = 0
RETURN
************************************************************
OPEN:
    CALL OPF(FN.TEL,F.TEL)
RETURN
PROCESS:
************************************************************
    T.SEL = "SELECT ":FN.TEL: " WITH CURRENCY EQ ":XX:" AND AMOUNT.LCY LT 0 AND ACCOUNT.NUMBER LIKE ...990001"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.TEL,KEY.LIST<I>,R.TEL,F.TEL,EER)
            IF  XX  EQ "EGP" THEN
                AMTLCY = R.TEL'TRANS.AMOUNT.LCY'
                AMT += AMTLCY
            END ELSE
                AMTFCY = R.TEL<TRANS.AMOUNT.FCY>
                AMT += AMTFCY
            END
        NEXT I
    END
    O.DATA = AMT
RETURN
END
