* @ValidationCode : MjotNTg2NTE4NjM0OkNwMTI1MjoxNjQxOTEyOTkxNjc0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 11 Jan 2022 16:56:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFive  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFive
*DONE
SUBROUTINE E.NOF.CHECK.CHQ(Y.ALL.REC)
***    PROGRAM E.NOF.CHECK.CHQ
***Mahmoud Elhawary******3/1/2011*****************************
* Nofile routine to get transactions and balances            *
* of accounts from a group of categories                     *
**************************************************************

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TRANSACTION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*   $INCLUDE I_FT.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*   $INCLUDE I_TT.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_INF.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
*Line [ 58 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 59 ] Adding EB.SCBUpgradeFive. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
RETURN
****************************************
INITIATE:
*--------
    Y.ALL.REC = ''
    ACCT.ID   = ''
    FROM.DATE = ''
    END.DATE  = ''
    Y.DATE    = FROM.DATE
    KK = 0
    XX = SPACE(120)
    STE.BAL  = ''
    CAT.GRP  = ''
    CAT.GRP1 = '16151'
    TOT.DR.MVMT = 0
    TOT.CR.MVMT = 0
    CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''
RETURN
****************************************
CALLDB:
*-------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; ER.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = '' ; ER.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; ER.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FTH = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FTH = '' ; R.FTH = '' ; ER.FTH = ''
    CALL OPF(FN.FTH,F.FTH)
    FN.TT = 'FBNK.TELLER' ; F.TT = '' ; R.TT = '' ; ER.TT = ''
    CALL OPF(FN.TT,F.TT)
    FN.TTH = 'FBNK.TELLER$HIS' ; F.TTH = '' ; R.TTH = '' ; ER.TTH = ''
    CALL OPF(FN.TTH,F.TTH)
    FN.INF = 'F.INF.MULTI.TXN' ; F.INF = '' ; R.INF = '' ; ER.INF = ''
    CALL OPF(FN.INF,F.INF)

RETURN
****************************************
PROCESS:
*-------
    LOCATE "START.DATE" IN D.FIELDS<1> SETTING YSDT.POS THEN FROM.DATE  = D.RANGE.AND.VALUE<YSDT.POS> ELSE RETURN
    LOCATE "END.DATE" IN D.FIELDS<1> SETTING YEDT.POS THEN END.DATE  = D.RANGE.AND.VALUE<YEDT.POS> ELSE RETURN

***    PROMPT "START.DATE :";INPUT FROM.DATE
***    PROMPT "END.DATE   :";INPUT END.DATE

    CAT.GRP = CAT.GRP1

    CC.SEL  = "SELECT ":FN.ACCT
    CC.SEL := " WITH CATEGORY IN (":CAT.GRP:")"
    CC.SEL := " BY CO.CODE BY @ID BY CATEGORY"

    CALL EB.READLIST(CC.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        LOOP
            REMOVE ACCT.ID FROM K.LIST SETTING POS.ACC
        WHILE ACCT.ID:POS.ACC
            CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,ER.ACCT)
*****************************
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
*****************************
            STE.BAL = OPENING.BAL
            TOT.DR.MVMT = 0
            TOT.CR.MVMT = 0
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS
            WHILE STE.ID:POS
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.COM = R.STE<AC.STE.COMPANY.CODE>
                STE.CUR = R.STE<AC.STE.CURRENCY>
                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                STE.VAL = R.STE<AC.STE.VALUE.DATE>
                STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                STE.TRF = R.STE<AC.STE.TRANS.REFERENCE>
                STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,STE.TXN,STE.TRN)
F.ITSS.TRANSACTION = 'F.TRANSACTION'
FN.F.ITSS.TRANSACTION = ''
CALL OPF(F.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION)
CALL F.READ(F.ITSS.TRANSACTION,STE.TXN,R.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION,ERROR.TRANSACTION)
STE.TRN=R.ITSS.TRANSACTION<AC.TRA.NARRATIVE>
                IF STE.CUR NE 'EGP' THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                IF STE.OUR EQ '' THEN
                    FINDSTR "\B" IN STE.TRF SETTING POS.STE THEN STE.OUR = FIELD(STE.TRF,'\',1) ELSE STE.OUR = STE.TRF
                END
                IF STE.OUR[1,2] EQ 'FT' THEN
                    FT.ID  = STE.OUR
                    FT.IDH = STE.OUR:";1"
                    CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
                    IF R.FT THEN
                        CHQ.NO = R.FT<FT.CHEQUE.NUMBER>
                    END ELSE
                        CALL F.READ(FN.FTH,FT.IDH,R.FTH,F.FTH,ER.FTH)
                        CHQ.NO = R.FTH<FT.CHEQUE.NUMBER>
                    END
                END
                IF STE.OUR[1,2] EQ 'TT' THEN
                    TT.ID  = STE.OUR
                    TT.IDH = STE.OUR:";1"
                    CALL F.READ(FN.TT,TT.ID,R.TT,F.TT,ER.TT)
                    IF R.TT THEN
                        CHQ.NO = R.TT<TT.TE.CHEQUE.NUMBER>
                    END ELSE
                        CALL F.READ(FN.TTH,TT.IDH,R.TTH,F.TTH,ER.TTH)
                        CHQ.NO = R.TTH<TT.TE.CHEQUE.NUMBER>
                    END
                END
                IF STE.OUR[1,2] EQ 'IN' THEN
                    IN.ID  = FIELD(STE.OUR,'-',1)
                    POSS   = FIELD(STE.OUR,'-',2)
                    CALL F.READ(FN.INF,IN.ID,R.INF,F.INF,ER.INF)
                    CHQ.NO = R.INF<INF.MLT.CHEQUE.NUMBER,POSS>
                    STE.OUR = IN.ID
                END
                STE.BAL += STE.AMT
                IF STE.AMT LT 0 THEN
                    TOT.DR.MVMT += STE.AMT
                END ELSE
                    TOT.CR.MVMT += STE.AMT
                END
                IF STE.ID THEN
                    IF CHQ.NO EQ '' THEN
                        GOSUB RET.DATA
                    END
                END
            REPEAT
        REPEAT
    END
RETURN
**************************************
RET.DATA:
*--------
    Y.ALL.REC<-1> = COMP:"*":ACCT.ID:"*":FROM.DATE:"*":END.DATE:"*":OPENING.BAL:"*":STE.DAT:"*":STE.AMT:"*":STE.BAL:"*":STE.ID:"*":STE.OUR:"*":STE.TXN:"*":STE.TRN:"*":STE.VAL:"*":STE.ID
***    CRT STE.COM:"*":ACCT.ID:"*":STE.OUR:"*":STE.DAT:"*":STE.AMT
RETURN
**************************************
END
