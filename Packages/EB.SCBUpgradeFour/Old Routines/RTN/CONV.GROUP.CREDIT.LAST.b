* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.GROUP.CREDIT.LAST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
    Z = ""
    CALL DBR("ACCOUNT":@FM:AC.CONDITION.GROUP,O.DATA,MYGRP)
    CALL DBR("ACCOUNT":@FM:AC.CURRENCY,O.DATA,MYCUR)

    T.SEL = "SELECT FBNK.GROUP.CREDIT.INT WITH @ID LIKE ":MYGRP:MYCUR:"... BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    TMP.DATA = '' ; TTT = ''

    TMP.DATA = KEY.LIST<SELECTED>



    FN.BASIC ='FBNK.GROUP.CREDIT.INT' ;R.BASIC='';F.BASIC=''
    CALL OPF(FN.BASIC,F.BASIC)
    CALL F.READ(FN.BASIC,TMP.DATA, R.BASIC, F.BASIC,E)

    IF R.BASIC<IC.GCI.CR.MARGIN.OPER> EQ "ADD" THEN
        Z = R.BASIC<IC.GCI.CR.INT.RATE> + R.BASIC<IC.GCI.CR.MARGIN.RATE>
    END ELSE
        Z = R.BASIC<IC.GCI.CR.INT.RATE> - R.BASIC<IC.GCI.CR.MARGIN.RATE>
        Z = R.BASIC<IC.GCI.CR.INT.RATE> + R.BASIC<IC.GCI.CR.MARGIN.RATE>
    END
    O.DATA = Z :"%"
    RETURN

END
