* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GENLEDALL.CUR.TOTAL.NEW.Y2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LINE.GENLEDALL
    COMP = ID.COMPANY
*-----------------------------------------------------------------------

    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.CUR.TOTAL.NEW" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"GENLEDALL.CUR.TOTAL.NEW"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.CUR.TOTAL.NEW" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE GENLEDALL.CUR.TOTAL.NEW CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create GENLEDALL.CUR.TOTAL.NEW File IN &SAVEDLISTS&'
        END
    END

*----------------------------------------------------------------

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    FN.BAL = 'F.RE.STAT.LINE.BAL' ; F.BAL = '' ; R.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''
    FN.SLN = 'F.SCB.LINE.GENLEDALL' ; F.SLN = '' ; R.SLN = ''
    CALL OPF(FN.LN,F.LN) ; CALL OPF(FN.BAL,F.BAL) ; CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.SLN,F.SLN)

    TD2 = TODAY[1,6]:'01'
    CALL CDT("",TD2,'-1C')
    TD1 = TD2:'CL'

    TDD = TODAY
    CALL CDT("",TDD,'-1W')


    KK = 'EG00100'
    NK = ''

    NK<1,1>='01'  ; NK<1,2>='02'  ; NK<1,3>='03'  ; NK<1,4>='04'  ; NK<1,5>='05'
    NK<1,6>='06'  ; NK<1,7>='07'  ; NK<1,8>='09'  ; NK<1,9>='10'  ; NK<1,10>='11'
    NK<1,11>='12' ; NK<1,12>='13' ; NK<1,13>='14' ; NK<1,14>='15' ; NK<1,15>='16' ; NK<1,16>='20'
    NK<1,17>='21' ; NK<1,18>='22' ; NK<1,19>='23' ; NK<1,20>='30' ; NK<1,21>='31'
    NK<1,22>='32' ; NK<1,23>='35' ; NK<1,24>='40' ; NK<1,25>='50'
    NK<1,26>='51' ; NK<1,27>='60' ; NK<1,28>='70' ; NK<1,29>='80' ; NK<1,30>='81'
    NK<1,31>='90' ; NK<1,32>='99' ; NK<1,33>='17' ; NK<1,34>='19' ; NK<1,35>='52' ; NK<1,36>='53'
    NK<1,37>='18' ; NK<1,38>='24' ; NK<1,39>='25' ; NK<1,40>='41' ; NK<1,41>='26' ; NK<1,42>='27'
    NK<1,43>='29' ; NK<1,44>='33' ; NK<1,45>='34' ; NK<1,46>='82' ; NK<1,47>='83' ; NK<1,48>='36'
    NK<1,49>='45' ; NK<1,50>='37'


    LINE.PRINT = ''
    LINE.BR    = ''
    HEAD.DESC  = "������":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "���� ����":"|"
    HEAD.DESC := "����� ������":"|"
    HEAD.DESC := "���� ��������":"|"
    HEAD.DESC := "�� ������":"|"
    HEAD.DESC := "���� ������":"|"
    HEAD.DESC := "���� �����":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "����� ����":"|"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    GOSUB PROCESS.ASST
    GOSUB PROCESS.PRFT
    GOSUB PROCESS.CONT
    RETURN
*--------------------------------------------------------------------
PROCESS.ASST:
    T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENLEDALL.0005 AND @ID LT GENLEDALL.0580 ) BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 124 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            CLOSE.BAL.LCL  = 0
            CLOSE.BAL.LCL2 = 0
            CLOSE.BAL.LCY  = 0
            CLOSE.BAL  = 0
            CLOSE.BAL2 = 0
            NEW.CUR = ''
*************************************************************************
            FOR X = 1 TO POS
                CLOSE.BAL.LCL = 0
                FOR C = 1 TO 50
                    BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":KK:NK<1,C>
                    CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                    OLD.CUR = R.CCY<RE.BCP.ORIGINAL.CCY,X>

                    NEW.CUR = OLD.CUR
                    IF NEW.CUR NE OLD.CUR THEN
                        CLOSE.BAL.LCL = 0
                    END


                    IF NEW.CUR EQ OLD.CUR THEN
                        CLOSE.BAL.LCL  += R.BAL<RE.SLB.CLOSING.BAL.LCL>

                    END
                    CLOSE.BAL.LCY   += R.BAL<RE.SLB.CLOSING.BAL.LCL>


                    LINE.PRINT<1,X>   = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                    LINE.PRINT<1,X+1> = CLOSE.BAL.LCL
                    FLAG = 0
                    IF LINE.PRINT<1,X> EQ 'EGP' THEN
                        LINE.BR<1,2> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'USD' THEN
                        LINE.BR<1,3> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'GBP' THEN
                        LINE.BR<1,4> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'JPY' THEN
                        LINE.BR<1,5> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'CHF' THEN
                        LINE.BR<1,6> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'SAR' THEN
                        LINE.BR<1,7> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'EUR' THEN
                        LINE.BR<1,8> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END

                    IF FLAG = 0 THEN
                        CLOSE.BAL.LCL2 += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                        LINE.BR<1,9>    = CLOSE.BAL.LCL2

                    END

                NEXT C
            NEXT X

            LINE.BR<1,1>  = CLOSE.BAL.LCY

            BB.DATA  = DESC:"|"
            BB.DATA := LINE.BR<1,1>:"|"
            BB.DATA := LINE.BR<1,2>:"|"
            BB.DATA := LINE.BR<1,3>:"|"
            BB.DATA := LINE.BR<1,4>:"|"
            BB.DATA := LINE.BR<1,5>:"|"
            BB.DATA := LINE.BR<1,6>:"|"
            BB.DATA := LINE.BR<1,7>:"|"
            BB.DATA := LINE.BR<1,8>:"|"
            BB.DATA := LINE.BR<1,9>:"|"

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I

    END
    RETURN
*--------------------------------------------------------
PROCESS.PRFT:
    T.SEL1 = "SELECT F.SCB.LINE.GENLEDALL"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    FOR XX = 1 TO SELECTED1
        CALL F.READ(FN.SLN,XX,R.SLN,F.SLN,E2)
        EGP.ID = R.SLN<LALL.EGP.ID>
        FCY.ID = R.SLN<LALL.FCY.ID>

        CALL F.READ(FN.LN,EGP.ID,R.LN,F.LN,E4)

        DESC = R.LN<RE.SRL.DESC>
        DESC.ID.EGP = FIELD(EGP.ID,".",2)
        DESC.ID.FCY = FIELD(FCY.ID,".",2)
        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
        CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 231 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        POS = DCOUNT(CCURR,@VM)
        CLOSE.BAL.LCL  = 0
        CLOSE.BAL.LCL2 = 0
        CLOSE.BAL.LCY  = 0
        CLOSE.BAL  = 0
        CLOSE.BAL2 = 0
        NEW.CUR = ''
*************************************************************************
        FOR X = 1 TO POS
            CLOSE.BAL.LCL  = 0
            FOR C = 1 TO 50
                FLAG2 = 0
                CUR.ID = R.CCY<RE.BCP.ORIGINAL.CCY,X>

                BAL.ID2  = FIELD(EGP.ID,".",1):"-":DESC.ID.EGP:"-":CUR.ID:"-":TD1:"*":KK:NK<1,C>
                CALL F.READ(FN.BAL,BAL.ID2,R.BAL,F.BAL,E2)

                CLOSE.BAL.LCL   += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                CLOSE.BAL.LCY   += R.BAL<RE.SLB.CLOSING.BAL.LCL>

                FLAG2 = 1
                GOSUB CALC
*---------------------------------
                BAL.ID   = FIELD(FCY.ID,".",1):"-":DESC.ID.FCY:"-":CUR.ID:"-":TD1:"*":KK:NK<1,C>
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)

                OLD.CUR = R.CCY<RE.BCP.ORIGINAL.CCY,X>

                NEW.CUR = OLD.CUR
                IF NEW.CUR NE OLD.CUR THEN
                    CLOSE.BAL.LCL = 0
                END

                IF NEW.CUR EQ OLD.CUR THEN
                    CLOSE.BAL.LCL  += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                END

                CLOSE.BAL.LCY   += R.BAL<RE.SLB.CLOSING.BAL.LCL>

                FLAG2 = 2
                GOSUB CALC
*----------------------------------
            NEXT C
        NEXT X

        LINE.BR<1,1>  = CLOSE.BAL.LCY

        BB.DATA  = DESC:"|"
        BB.DATA := LINE.BR<1,1>:"|"
        BB.DATA := LINE.BR<1,2>:"|"
        BB.DATA := LINE.BR<1,3>:"|"
        BB.DATA := LINE.BR<1,4>:"|"
        BB.DATA := LINE.BR<1,5>:"|"
        BB.DATA := LINE.BR<1,6>:"|"
        BB.DATA := LINE.BR<1,7>:"|"
        BB.DATA := LINE.BR<1,8>:"|"
        BB.DATA := LINE.BR<1,9>:"|"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    NEXT XX
    RETURN
*---------------------------------------------------------
CALC:
    LINE.PRINT<1,X>   = R.CCY<RE.BCP.ORIGINAL.CCY,X>
    LINE.PRINT<1,X+1> = CLOSE.BAL.LCL
    FLAG = 0
    IF LINE.PRINT<1,X> EQ 'EGP' THEN
        LINE.BR<1,2> = LINE.PRINT<1,X+1>
        FLAG = 1
    END
    IF LINE.PRINT<1,X> EQ 'USD' THEN
        LINE.BR<1,3> = LINE.PRINT<1,X+1>
        FLAG = 1
    END
    IF LINE.PRINT<1,X> EQ 'GBP' THEN
        LINE.BR<1,4> = LINE.PRINT<1,X+1>
        FLAG = 1
    END
    IF LINE.PRINT<1,X> EQ 'JPY' THEN
        LINE.BR<1,5> = LINE.PRINT<1,X+1>
        FLAG = 1
    END
    IF LINE.PRINT<1,X> EQ 'CHF' THEN
        LINE.BR<1,6> = LINE.PRINT<1,X+1>
        FLAG = 1
    END
    IF LINE.PRINT<1,X> EQ 'SAR' THEN
        LINE.BR<1,7> = LINE.PRINT<1,X+1>
        FLAG = 1
    END
    IF LINE.PRINT<1,X> EQ 'EUR' THEN
        LINE.BR<1,8> = LINE.PRINT<1,X+1>
        FLAG = 1
    END

    IF FLAG2 = 2 THEN
        IF FLAG = 0 THEN
            CLOSE.BAL.LCL2 += R.BAL<RE.SLB.CLOSING.BAL.LCL>
            LINE.BR<1,9>     = CLOSE.BAL.LCL2
        END
    END

    IF FLAG2 = 1 THEN
        IF FLAG = 0 THEN
            CLOSE.BAL.LCL2 += R.BAL<RE.SLB.CLOSING.BAL.LCL>
            LINE.BR<1,9>     = CLOSE.BAL.LCL2
        END
    END
    RETURN
*--------------------------------------------------------------------
PROCESS.CONT:
    T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENLEDALL.9000 AND @ID LT GENLEDALL.9990 ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 355 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            CLOSE.BAL.LCL  = 0
            CLOSE.BAL.LCL2 = 0
            CLOSE.BAL.LCY  = 0
            CLOSE.BAL  = 0
            CLOSE.BAL2 = 0
            NEW.CUR = ''
*************************************************************************
            FOR X = 1 TO POS
                CLOSE.BAL.LCL = 0
                FOR C = 1 TO 50
                    BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":KK:NK<1,C>
                    CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                    OLD.CUR = R.CCY<RE.BCP.ORIGINAL.CCY,X>

                    NEW.CUR = OLD.CUR
                    IF NEW.CUR NE OLD.CUR THEN
                        CLOSE.BAL.LCL = 0
                    END

                    IF NEW.CUR EQ OLD.CUR THEN
                        CLOSE.BAL.LCL  += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                    END
                    CLOSE.BAL.LCY   += R.BAL<RE.SLB.CLOSING.BAL.LCL>

                    LINE.PRINT<1,X>   = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                    LINE.PRINT<1,X+1> = CLOSE.BAL.LCL
                    FLAG = 0
                    IF LINE.PRINT<1,X> EQ 'EGP' THEN
                        LINE.BR<1,2> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'USD' THEN
                        LINE.BR<1,3> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'GBP' THEN
                        LINE.BR<1,4> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'JPY' THEN
                        LINE.BR<1,5> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'CHF' THEN
                        LINE.BR<1,6> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'SAR' THEN
                        LINE.BR<1,7> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END
                    IF LINE.PRINT<1,X> EQ 'EUR' THEN
                        LINE.BR<1,8> = LINE.PRINT<1,X+1>
                        FLAG = 1
                    END

                    IF FLAG = 0 THEN
                        CLOSE.BAL.LCL2 += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                        LINE.BR<1,9>    = CLOSE.BAL.LCL2
                    END

                NEXT C
            NEXT X

            LINE.BR<1,1>  = CLOSE.BAL.LCY

            BB.DATA  = DESC:"|"
            BB.DATA := LINE.BR<1,1>:"|"
            BB.DATA := LINE.BR<1,2>:"|"
            BB.DATA := LINE.BR<1,3>:"|"
            BB.DATA := LINE.BR<1,4>:"|"
            BB.DATA := LINE.BR<1,5>:"|"
            BB.DATA := LINE.BR<1,6>:"|"
            BB.DATA := LINE.BR<1,7>:"|"
            BB.DATA := LINE.BR<1,8>:"|"
            BB.DATA := LINE.BR<1,9>:"|"

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I

    END
    RETURN
END
