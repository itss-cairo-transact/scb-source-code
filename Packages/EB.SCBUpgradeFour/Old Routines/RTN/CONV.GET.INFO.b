* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*--------------------------------------------------
    SUBROUTINE CONV.GET.INFO
*--------------------------------------------------
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_ENQUIRY.COMMON
*--------------------------------------------------
    FN.USR = "F.USER"             ; F.USR = ""
    CALL OPF(FN.USR,F.USR)

    FN.USR.H = "F.USER$HIS"       ; F.USR.H = ""
    CALL OPF(FN.USR.H,F.USR.H)
*---------------------------------------------------
    CALL F.READ(FN.USR,O.DATA,R.USRR,F.USR,E.USR)
    CURR.NO = R.USRR<EB.USE.CURR.NO>
    END.TIM = R.USRR<EB.USE.END.TIME>

    FOR II = 1 TO CURR.NO
        HIS.II    = O.DATA : ";" : 0II
        WW        = II - 1
        HIS.WW    = O.DATA : ";" : WW

        CALL F.READ(FN.USR.H,HIS.II,R.USRR.H,F.USR.H,E.USR.H)
        END.TIM.H = R.USRR<EB.USE.END.TIME>

        CALL F.READ(FN.USR.H,HIS.WW,R.USRR.H,F.USR.H,E.USR.H)
        END.TIM.W = R.USRR.H<EB.USE.END.TIME>

        IF END.TIM.W NE END.TIM.H THEN
            IF END.TIM.H GT 1730 THEN
                INP  = R.USRR<EB.USE.INPUTTER>
                AUTH = R.USRR<EB.USE.AUTHORISER>
                DAT  = R.USRR<EB.USE.DATE.TIME,1>
            END
        END
    NEXT II

    INP  = FIELD(INP,'_',2)
    AUTH = FIELD(AUTH,'_',2)
    DAT  = "20":DAT[1,6]
*---------------------------------------------------
    O.DATA = INP : "*" : AUTH : "*" : DAT
*---------------------------------------------------
    RETURN
END
