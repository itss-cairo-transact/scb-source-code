* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.EGSERV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EGSRV.INDEX

    WS.CU.ID = O.DATA
    O.DATA = ' '
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CU.ID,WS.LOCAL.REF)

    WS.EGSERV.ID = WS.LOCAL.REF<1,CULR.EGSRV.SRL>
    WS.CU.GOV    = WS.LOCAL.REF<1,CULR.GOVERNORATE>

    CALL DBR ('SCB.EGSRV.INDEX':@FM:EGSRV.EGSERV.CODE,WS.EGSERV.ID,WS.EGSERV.CODE)
    CALL DBR ('SCB.EGSRV.INDEX':@FM:EGSRV.EGSERV.BAR,WS.EGSERV.ID,WS.EGSERV.BAR)

*Line [ 44 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF (NOT(WS.EGSERV.BAR) OR WS.CU.GOV EQ '98' OR WS.CU.GOV EQ '97') THEN WS.EGSERV.BAR = STR('_',26) ELSE NULL

    WS.CU.ID = 100000000 + WS.CU.ID

    O.DATA = WS.CU.ID[8] :' ': WS.EGSERV.ID :' ': WS.EGSERV.CODE :' ': WS.EGSERV.BAR

    RETURN
END
