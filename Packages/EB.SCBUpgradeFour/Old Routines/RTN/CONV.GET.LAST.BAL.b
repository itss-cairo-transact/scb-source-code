* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-32</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.GET.LAST.BAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*-----------------------------------------
    GOSUB PROCESS
    RETURN
*-----------------------------------------
CALC.CR.ACCT:
*------------
    IF SYS.ID EQ "TT" THEN
        FN.TT = "FBNK.TELLER$HIS"  ; F.TT = ""
        CALL OPF(FN.TT, F.TT)
        CALL F.READ(FN.TT,OUR.REF,R.TT,F.TT,ER.TT)
        IF R.TT<TT.TE.DR.CR.MARKER> EQ 'DEBIT' THEN
            CR.ACCT = R.TT<TT.TE.ACCOUNT.2>
        END ELSE
            CR.ACCT = R.TT<TT.TE.ACCOUNT.1>
        END
    END

    IF SYS.ID EQ "FT" THEN
        FN.FT = "FBNK.FUNDS.TRANSFER$HIS" ; F.FT = ""
        CALL OPF(FN.FT, F.FT)
        CALL F.READ(FN.FT,OUR.REF,R.FT,F.FT,ER.FT)
        IF STMT.AMT LE 0 THEN
            CR.ACCT = R.FT<FT.CREDIT.ACCT.NO>
        END ELSE
            CR.ACCT = R.FT<FT.DEBIT.ACCT.NO>
        END
    END
    RETURN
*-----------------------------------------
PROCESS:
*-------
    XXD     = TODAY
    ACCT.ID = O.DATA
    FN.ACC  = "FBNK.ACCOUNT$HIS" ; F.ACC = ""
    CALL OPF(FN.ACC, F.ACC)

    CALL F.READ(FN.ACC,ACCT.ID,R.ACC,F.ACC,ER.ACC)
    DAT.1 = R.ACC<AC.DATE.LAST.CR.CUST>
    DAT.2 = R.ACC<AC.DATE.LAST.DR.CUST>

    DAT.3 = R.ACC<AC.DATE.LAST.CR.AUTO>
    DAT.4 = R.ACC<AC.DATE.LAST.DR.AUTO>

    DAT.5 = R.ACC<AC.DATE.LAST.CR.BANK>
    DAT.6 = R.ACC<AC.DATE.LAST.DR.BANK>

    ARR.DATES      = ""
    ARR.DATES<1,1> = DAT.1
    ARR.DATES<1,2> = DAT.2
    ARR.DATES<1,3> = DAT.3
    ARR.DATES<1,4> = DAT.4
    ARR.DATES<1,5> = DAT.5
    ARR.DATES<1,6> = DAT.6

    MAX.DATE = MAXIMUM(ARR.DATES)
*--------------------
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)

    ARRAY.REF = ""
    ARRAY.DAT = ""
    ARRAY.AMT = ""
    ARRAY.OUR = ""
    II        = 1
    ACC.ID    = FIELD(ACCT.ID,";",1)

    CALL EB.ACCT.ENTRY.LIST(ACC.ID<1>,MAX.DATE,XXD,ID.LIST,OPENING.BAL,ER)
*    CALL EB.ACCT.ENTRY.LIST(ACC.ID<1>,MAX.DATE,MAX.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STD.ID FROM ID.LIST SETTING POS
    WHILE STD.ID:POS
        CALL F.READ(FN.STE,STD.ID,R.STE,F.STE,ER.STE)
        DAT.TIME = R.STE<AC.STE.DATE.TIME,1>
        OUR.REF  = R.STE<AC.STE.TRANS.REFERENCE> :";1"
        SYS.ID   = R.STE<AC.STE.SYSTEM.ID>
        STMT.AMT = R.STE<AC.STE.AMOUNT.LCY>

        GOSUB CALC.CR.ACCT
        ARRAY.REF<1,II> = CR.ACCT
        ARRAY.DAT<1,II> = DAT.TIME
        ARRAY.AMT<1,II> = STMT.AMT
        ARRAY.OUR<1,II> = OUR.REF
        II ++
    REPEAT
*-----------------------------------------
    MAX.REF.DAT = ARRAY.DAT<1,1>
    OUR.REF.VAL = ARRAY.REF<1,1>
    OUR.REF.AMT = ARRAY.AMT<1,1>
    OUR.REF.OUR = ARRAY.OUR<1,1>

    FOR YY = 2 TO II
        IF ARRAY.DAT<1,YY> GT MAX.REF.DAT THEN
            MAX.REF.DAT = ARRAY.DAT<1,YY>

            OUR.REF.VAL = ARRAY.REF<1,YY>
            OUR.REF.AMT = ARRAY.AMT<1,YY>
            OUR.REF.OUR = ARRAY.OUR<1,YY>
        END
    NEXT II

    O.DATA = OUR.REF.AMT :"*": OUR.REF.VAL :"*": OUR.REF.OUR
    RETURN
END
