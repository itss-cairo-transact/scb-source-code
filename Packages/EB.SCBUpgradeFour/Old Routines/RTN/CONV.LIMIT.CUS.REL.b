* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LIMIT.CUS.REL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RELATION.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    XX = O.DATA
    FN.RCU = 'FBNK.RELATION.CUSTOMER' ; F.RCU = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.RCU,F.RCU)
    CALL OPF(FN.CU,F.CU)
    CALL F.READ(FN.RCU,XX,R.RCU,F.RCU,E1)

    REL.COD = R.RCU<EB.RCU.IS.RELATION>
*K1 = DCOUNT(REL.COD,VM)
*FOR J = 1 TO K1
* IF R.RCU<EB.RCU.IS.RELATION> = 62 OR R.RCU<EB.RCU.IS.RELATION> = 72 THEN
    CUS.ID = R.RCU<EB.RCU.OF.CUSTOMER>
* END
    O.DATA = CUS.ID
*NEXT J
    RETURN
*==============================================================
END
