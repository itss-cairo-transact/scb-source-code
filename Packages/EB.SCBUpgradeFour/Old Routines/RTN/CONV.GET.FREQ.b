* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GET.FREQ
*---------------------------
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT TEMENOS.BP I_F.SCB.CUSTOMER.STMT
*----------------------------------------------
    IF O.DATA[1,1] EQ 'D' THEN
        O.DATA = "����"
    END

    IF O.DATA[1,1] EQ 'W' THEN
        O.DATA = "������"
    END

    IF O.DATA[1,1] EQ 'M' THEN
        O.DATA = "����"
    END
*----------------------------------------------
    RETURN
END
