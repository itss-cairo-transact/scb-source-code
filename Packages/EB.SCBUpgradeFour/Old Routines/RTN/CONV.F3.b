* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*--------------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.F3
*--------------------------------------------------
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*--------------------------------------------------
    WS.COMP = ID.COMPANY
    WS.ID   = O.DATA

    FN.RISK = "F.SCB.RISK.MAST"   ; F.RISK = ""
    CALL OPF(FN.RISK,F.RISK)

    WS.TOT = 0
*---------------------------------------------------
    *TEXT = WS.ID ; CALL REM
    CALL F.READ(FN.RISK,WS.ID,R.RISK,F.RISK,E.TXT)

    F7 = R.RISK<RM.CLASS.ITEM.COD.03>
    WS.TOT = WS.TOT + F7[5,2]

    F8 = R.RISK<RM.CLASS.ITEM.COD.04>
    WS.TOT = WS.TOT + F8[5,2]

    F9 = R.RISK<RM.CLASS.ITEM.COD.05>
    WS.TOT = WS.TOT + F9[5,2]

    F10 = R.RISK<RM.CLASS.ITEM.COD.06>
    WS.TOT = WS.TOT + F10[5,2]

    F11 = R.RISK<RM.CLASS.ITEM.COD.07>
    WS.TOT = WS.TOT + F11[5,2]

    F12 = R.RISK<RM.CLASS.ITEM.COD.08>
    WS.TOT = WS.TOT + F12[5,2]
*----------------------------------------
    O.DATA = WS.TOT
*----------------------------------------
    RETURN
END
