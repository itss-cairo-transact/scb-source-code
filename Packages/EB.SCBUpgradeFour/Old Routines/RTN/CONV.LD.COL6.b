* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LD.COL6

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
    RETURN
INIT:
    XX = O.DATA
    FN.COL = "FBNK.COLLATERAL" ; F.COL  = '' ; R.COL  = ''
    COLL = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    RETURN
OPEN:
    CALL OPF(FN.COL,F.COL)
    CALL OPF(FN.LIM,F.LIM)
    RETURN
PROCESS:
************************************************************
    CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,EER1)
*Line [ 49 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    COL.NO = DCOUNT(R.LIM<LI.COLLAT.RIGHT>,@SM)
    FOR HH = 1 TO COL.NO
        COLX = R.LIM<LI.COLLAT.RIGHT,1,HH>
        T.SEL = "SELECT ":FN.COL: " WITH @ID LIKE ":COLX:"..."
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.COL,KEY.LIST<I>,R.COL,F.COL,EER)
                COLL =  R.COL<COLL.VALUE.DATE>
            NEXT I
        END
    NEXT HH
    O.DATA = COLL
    RETURN
END
