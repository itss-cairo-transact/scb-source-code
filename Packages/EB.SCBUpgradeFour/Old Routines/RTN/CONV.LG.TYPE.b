* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*----RANIA & ABEER ------*
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LG.TYPE


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS


    IF O.DATA THEN

        MYID=FIELD(O.DATA,"/",1)
        MYTYPE=FIELD(O.DATA,"/",2)
        FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '';R.CUSTOMER=''
        CALL OPF(FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ(FN.CUSTOMER,MYID,R.CUSTOMER,F.CUSTOMER,E1)
        MYLANGUAGE = R.CUSTOMER<EB.CUS.LANGUAGE>

        FN.SCB.LG.PARMS='F.SCB.LG.PARMS';F.SCB.LG.PARMS='';R.SCB.LG.PARMS=''
        CALL OPF(FN.SCB.LG.PARMS,F.SCB.LG.PARMS)
        CALL F.READ(FN.SCB.LG.PARMS,MYTYPE,R.SCB.LG.PARMS,F.SCB.LG.PARMS,E1)
        MYENDESC = R.SCB.LG.PARMS<SCB.LGP.DESCRIPTION,1>
        MYARDESC=R.SCB.LG.PARMS<SCB.LGP.DESCRIPTION,2>

        IF MYLANGUAGE = '1' THEN
            O.DATA = MYENDESC
        END ELSE
            O.DATA = MYARDESC
        END

    END
    RETURN
END
