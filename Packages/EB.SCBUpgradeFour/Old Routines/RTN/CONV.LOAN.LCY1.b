* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LOAN.LCY1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD


    KH = O.DATA
    FN.CBE = 'F.CBE.MAST.AC.LD' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    Y.AMOUNT.LCY = 0

    T.SEL = "SELECT ":FN.CBE: " WITH CBEM.CATEG GE 1101 AND CBEM.CATEG LE 1599 AND CBEM.CY EQ 'EGP' AND @ID EQ ":KH
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR K = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<K>,R.CBE,F.CBE,E3)
            Y.AMOUNT.LCY = Y.AMOUNT.LCY + R.CBE<C.CBEM.IN.LCY>
        NEXT K
    END
    O.DATA = Y.AMOUNT.LCY
    RETURN
END
