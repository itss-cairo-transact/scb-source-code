* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LAST.AC.HANDOFF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.STMT.HANDOFF
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT

    IF O.DATA THEN
*        T.SEL = "SELECT FBNK.AC.STMT.HANDOFF WITH @ID LIKE ":O.DATA:"... BY @ID"
*        KEY.LIST="" ; SELECTED="" ; ER.MSG=""
*        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*        ID.HNDOFF = KEY.LIST<SELECTED>
*        CALL DBR ('AC.STMT.HANDOFF':@FM:AC.STH.OPENING.BALANCE,ID.HNDOFF,BALNC)
**** CHANGE ROUTINE BY MOHAMED SABRY 2012/10/02
        WS.AC.ID = O.DATA
        CALL DBR ('ACCOUNT.STATEMENT':@FM:AC.STA.FQU1.LAST.DATE,WS.AC.ID,WS.FQU1)
        WS.HNDOFF.ID = WS.AC.ID:'.':WS.FQU1:'.1'
        CALL DBR ('AC.STMT.HANDOFF':@FM:AC.STH.OPENING.BALANCE,WS.HNDOFF.ID,BALNC)
**** END OF CHANGE BY MOHAMED SABRY 2012/10/02
        O.DATA  = BALNC
    END
    RETURN
END
