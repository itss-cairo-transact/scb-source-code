* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*---------------------------NI7OOOOOOOOOOOOO --- --------------------
*-----------------------------------------------------------------------------
* <Rating>144</Rating>
*-----------------------------------------------------------------------------
    PROGRAM CONV.SAVE.TEXT.AA

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.GEN.CONDITION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*-----------------------------------------------------------------------
*to create a file text  of data we must empty this file first
*fill the data
********************************************************************
**** OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "CONV.SAVE.TXT.A" TO BB THEN
    OPENSEQ "/home/signat" , "CONV.SAVE.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
*****  EXECUTE 'DELETE ':"/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"CONV.SAVE.TXT.A"
        EXECUTE 'DELETE ':"/home/signat":' ':"CONV.SAVE.TXT"
        HUSH OFF
    END
***** OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "CONV.SAVE.TXT.A" TO BB ELSE
    OPENSEQ "/home/signat" , "CONV.SAVE.TXT" TO BB ELSE
        CREATE BB THEN
******* PRINT 'FILE CONV.SAVE.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
            PRINT 'FILE CONV.SAVE.TXT CREATED IN /home/signat'
        END ELSE
***** STOP 'Cannot create CONV.SAVE.TXT.A File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
            STOP 'Cannot create CONV.SAVE.TXT File IN /home/signat'
        END
    END

***** READ DATE FROM TEXT *****

    SEQ.FILE.NAME = '&SAVEDLISTS&'
    RECORD.NAME = 'DOKKI.DATE.TXT'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            WS.DATE   = Y.MSG
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

*******************************



*----------------------------------------------------------------
****  DEBUG
    FN.AGC = 'FBNK.ACCT.GEN.CONDITION'; F.AGC = '' ; R.AGC = ''
    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = '' ; R.GCI = ''
    FN.CURR = 'FBNK.CURRENCY' ; F.CURR = '' ; R.CURR = ''
    K = ''
    BB.DATA = ''
    CALL OPF(FN.AGC,F.AGC)
    CALL OPF(FN.GCI,F.GCI)
    CALL OPF(FN.CURR,F.CURR)


    KEY.LIST="" ; SELECTED="" ;
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""  ;T.SEL2= ""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

*    T.SEL.AGC = "SELECT ":FN.AGC:" WITH @ID EQ 33 OR @ID EQ 66 OR @ID EQ 67 OR @ID EQ 68 BY @ID"
    T.SEL.AGC = "SELECT ":FN.AGC:" WITH @ID EQ 33 OR @ID EQ 54 OR @ID EQ 66 OR @ID EQ 67 OR @ID EQ 68 OR @ID EQ 143 "
    T.SEL.AGC := " OR @ID EQ 144 OR @ID EQ 145 OR @ID EQ 146 OR @ID EQ 147 BY @ID"

    CALL EB.READLIST(T.SEL.AGC,KEY.LIST.AGC,"",SELECTED.AGC,ER.MSGAA)

    IF SELECTED.AGC THEN
        FOR II = 1 TO SELECTED.AGC
            CALL F.READ(FN.AGC,KEY.LIST.AGC<II>,R.AGC,F.AGC,E31)
            Y.DESC = R.AGC<EB.AGC.DESCRIPTION,2>
            T.SEL.CUR = "SELECT FBNK.CURRENCY WITH @ID EQ 'EGP' OR @ID EQ 'USD' OR @ID EQ 'EUR' OR @ID EQ 'GBP'"
            CALL EB.READLIST(T.SEL.CUR,KEY.LIST.CUR,"",SELECTED.CUR,ER.MSG.CD)
            FOR III = 1 TO SELECTED.CUR

                IF KEY.LIST.AGC<II> = 54 THEN
                    XX = KEY.LIST.AGC<II>:KEY.LIST.CUR<III>:'...':WS.DATE
                END ELSE
                    IF KEY.LIST.AGC<II> = 147 THEN
                        XX = KEY.LIST.AGC<II>:KEY.LIST.CUR<III>:'...':WS.DATE
                    END ELSE
                        XX = KEY.LIST.AGC<II>:KEY.LIST.CUR<III>:'...'
                    END
                END

                T.SEL.GCI = "SELECT ":FN.GCI:" WITH @ID LIKE ":XX:" BY @ID "
                CALL EB.READLIST(T.SEL.GCI,KEY.LIST.GCI,"",SELECTED.GCI,ER.MSG1OOO)
                CALL F.READ(FN.GCI,KEY.LIST.GCI<SELECTED.GCI>,R.GCI,F.GCI,E3222)
                IF SELECTED.GCI THEN
                    IF KEY.LIST.AGC<II> LT 100 THEN
                        Y.ID.GCI   = KEY.LIST.GCI<SELECTED.GCI>[3,3]
                    END ELSE
                        Y.ID.GCI   = KEY.LIST.GCI<SELECTED.GCI>[4,3]
                    END
                    Y.ID.GCI2  = KEY.LIST.GCI<SELECTED.GCI>
                    IF KEY.LIST.AGC<II> LT 100 THEN
                        Y.RATE2    = R.GCI<IC.GCI.CR.INT.RATE>
                    END ELSE
                        Y.RATE2    = R.GCI<IC.GCI.CR.INT.RATE,2> + R.GCI<IC.GCI.CR.MARGIN.RATE,2>
                    END
                    Y.RATE     = FMT(Y.RATE2,"L,")
                    PRINT "DD== " : Y.ID.GCI
*Line [ 140 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    Y.RATE.COUNT = DCOUNT(Y.RATE2,@VM)
                    IF Y.RATE.COUNT GT 1 THEN
                        Y.RATE = ''
                        FOR X = 1 TO Y.RATE.COUNT
                            Y.RATE := Y.RATE2<1,X>:'|'
                        NEXT X
                    END


                    BB.DATA = "|":Y.DESC:",":Y.ID.GCI:",":Y.RATE

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                END
            NEXT III
        NEXT II
    END
*************************************************************************
END
