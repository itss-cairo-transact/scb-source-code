* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.FT.REV

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    FT.ID.ALL = O.DATA
    FT.FLG = FT.ID.ALL[15,1]
    FT.ID.2 =  FT.ID.ALL[1,14]

    FT.ID = FT.ID.2[1,12]

    FT.ID.1 = FT.ID : ";1"

    FN.FT   = 'F.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)

    CALL F.READ(FN.FT,FT.ID.1, R.FT, F.FT, ETEXT)

    IF FT.FLG EQ '1' THEN
        O.DATA = FT.ID.1
    END

    IF FT.FLG EQ '2' THEN
        O.DATA = R.FT<FT.DEBIT.ACCT.NO>
    END

    IF FT.FLG EQ '3' THEN
        O.DATA = R.FT<FT.DEBIT.AMOUNT>
    END

    IF FT.FLG EQ '4' THEN
        O.DATA = R.FT<FT.DATE.TIME>
    END

    IF FT.FLG EQ '5' THEN
        O.DATA = R.FT<FT.LOCAL.REF><1,39>
    END

    RETURN
END
