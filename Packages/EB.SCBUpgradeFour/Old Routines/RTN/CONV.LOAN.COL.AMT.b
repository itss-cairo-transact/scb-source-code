* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LOAN.COL.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*--------------------------------------------
    LD.ID    = FIELD(O.DATA,"-",1)
    COL.R.ID = FIELD(O.DATA,"-",2)

    COL.ID = COL.R.ID:'...'

    FN.COL = 'FBNK.COLLATERAL' ; F.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COLR = 'FBNK.COLLATERAL.RIGHT' ; F.COLR = ''
    CALL OPF(FN.COLR,F.COLR)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,E2)
    LD.LIMIT.REF  = R.LD<LD.LIMIT.REFERENCE>
    WS.CUS.ID     = R.LD<LD.CUSTOMER.ID>
    LD.LIMIT.REF1 = FMT(LD.LIMIT.REF,'R%10')
    WS.LIMIT.REF  = WS.CUS.ID:'.':LD.LIMIT.REF1

    CALL F.READ(FN.COLR,COL.R.ID,R.COLR,F.COLR,E3)

*Line [ 57 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.LIMIT.REF IN R.COLR<COLL.RIGHT.LIMIT.REFERENCE,1> SETTING POS ELSE NULL
    WS.PERC = R.COLR<COLL.RIGHT.PERCENT.ALLOC,POS>

    T.SEL = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.COL,KEY.LIST<I>,R.COL,F.COL,E1)
            COL.AMT1 += R.COL<COLL.LOCAL.REF><1,COLR.COLL.AMOUNT>
        NEXT I
    END

    COL.AMT = COL.AMT1 * WS.PERC / 100
    O.DATA  = COL.AMT

    RETURN
