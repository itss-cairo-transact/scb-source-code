* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
   *** PROGRAM CONV.MM.MAST.TEXT
    SUBROUTINE CONV.MM.MAST.TEXT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
    COMP = ID.COMPANY

*-----------------------------------------------------------------------

    OPENSEQ "&SAVEDLISTS&" , "FIN.MAST.TEXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"FIN.MAST.TEXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "FIN.MAST.TEXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FIN.MAST.TEXT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create MM.MAST.TEXT File IN &SAVEDLISTS&'
        END
    END

*----------------------------------------------------------------

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    FN.BAL = 'F.RE.STAT.LINE.BAL' ; F.BAL = '' ; R.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''

    CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)

    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    Y.CLOSE.BALF10 = 0 ; Y.CLOSE.BALF101 = 0

    DAT.ID = COMP
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

*-----------------------------------------------------------
    T.SEL1 = "SELECT ":FN.LN: " WITH @ID LIKE GENLED...."
    T.SEL1 := " BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR X = 1 TO SELECTED1
            CALL F.READ(FN.LN,KEY.LIST1<X>,R.LN,F.LN,E2)
            DESC.ID1 = FIELD(KEY.LIST1<X>,".",2)
  **          IF DESC.ID1 LT 9990 THEN
              IF DESC.ID1 LT 1000 THEN
                CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
                CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                POS = DCOUNT(CCURR,@VM)
                FOR KK = 1 TO POS
                    BAL.ID1 = FIELD(KEY.LIST1<X>,".",1):"-":DESC.ID1:"-":R.CCY<RE.BCP.ORIGINAL.CCY,KK>:"-":TD1:"*":COMP

                    RATE = R.CCY<RE.BCP.RATE,KK>
                    BRN  = COMP[2]

                    T.DAY  = TDD[7,2]:'/':TDD[5,2]:"/":TDD[1,4]

                    CALL F.READ(FN.BAL,BAL.ID1,R.BAL,F.BAL,E2)
                    IF NOT(E2) THEN
                        CUR.ID = BAL.ID1[13,3]
                        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR)

                        Y.CLOSE.BALF101 = R.BAL<RE.SLB.CLOSING.BAL>
                        Y.CLOSE.BALF10 += Y.CLOSE.BALF101 * RATE
                        ACC.NO   = '000000'
                        OT.CATEG = FIELD(R.LN<RE.SRL.DESC,1,1>,"-",2)
*                    OT.CATEG = R.LN<RE.SRL.DESC,1>[6,3]
*DEBUG

                        IF DESC.ID1 EQ '0013' THEN OT.CATEG = 20 ; ACC.NO = "000005"
                        IF DESC.ID1 EQ '0014' THEN OT.CATEG = 20 ; ACC.NO = "900700"
                        IF DESC.ID1 EQ '0203' THEN OT.CATEG = 20 ; ACC.NO = "000001"
                        IF DESC.ID1 EQ '0737' THEN OT.CATEG = 615
                        IF DESC.ID1 EQ '0100' THEN OT.CATEG = 240
                        IF DESC.ID1 EQ '0110' THEN OT.CATEG = 10
                        IF DESC.ID1 EQ '0285' THEN OT.CATEG = 240
                        IF DESC.ID1 EQ '0310' THEN OT.CATEG = 10
                        IF DESC.ID1 EQ '0560' THEN OT.CATEG = 500
                        IF DESC.ID1 EQ '0600' THEN OT.CATEG = 570
                        IF DESC.ID1 EQ '0620' THEN OT.CATEG = 40
                        IF DESC.ID1 EQ '0760' THEN OT.CATEG = 500
                        IF DESC.ID1 EQ '0800' THEN OT.CATEG = 570
                        IF DESC.ID1 EQ '0311' THEN OT.CATEG = 40
                        IF DESC.ID1 EQ '0107' THEN OT.CATEG = 265
                        IF DESC.ID1 EQ '0108' THEN OT.CATEG = 265
                        IF DESC.ID1 EQ '0145' THEN ACC.NO = '000001'
                        IF DESC.ID1 EQ '0120' THEN
                            OT.CATEG = 280
                            Y.CLOSE.BALF10 = R.BAL<RE.SLB.CLOSING.BAL.LCL>
                        END
                        IF DESC.ID1 EQ '0320' THEN OT.CATEG = 280

                        IF DESC.ID1 EQ '0630' THEN
                           OT.CATEG = 640
                           Y.CLOSE.BALF10 = R.BAL<RE.SLB.CLOSING.BAL.LCL>
                        END
                        IF DESC.ID1 EQ '0840' THEN OT.CATEG = 640

                        BRAN.NO = '99'
                        PLC.TYPE = 1
                        ACC.TYPE = 1
                        ACC.TYP  = 1
                        CATEG    = OT.CATEG

                        BAL1      = Y.CLOSE.BALF101         ;*Y.CLOSE.BALF10
                        BAL2      = Y.CLOSE.BALF101         ;*Y.CLOSE.BALF10
                        BAL3      = Y.CLOSE.BALF101         ;*Y.CLOSE.BALF10
                        BAL4      = Y.CLOSE.BALF101         ;*Y.CLOSE.BALF10
                        BAL5      = Y.CLOSE.BALF101         ;*Y.CLOSE.BALF10
                        INT.PRIC  = 0
                        LST.UPBLC = TODAY
                        LST.UPBLC = LST.UPBLC[7,2]:LST.UPBLC[5,2]:LST.UPBLC[1,4]

*-------------------------------------------------------------------
                        BB.DATA  = '99':"|"
                        BB.DATA := 1:"|"
                        BB.DATA := 1:"|"
                        BB.DATA := 1:"|"
                        BB.DATA := ACC.NO:"|"
                        BB.DATA := CATEG:"|"
                        BB.DATA := CUR:"|"
                        BB.DATA := BAL1:"|"
                        BB.DATA := BAL2:"|"
                        BB.DATA := BAL3:"|"
                        BB.DATA := BAL4:"|"
                        BB.DATA := BAL5:"|"
                        BB.DATA := '0':"|"
                        BB.DATA := TODAY
                        IF Y.CLOSE.BALF10 NE 0 AND Y.CLOSE.BALF10 NE '' THEN
                            WRITESEQ BB.DATA TO BB ELSE
                                PRINT " ERROR WRITE FILE "
                            END
                        END
                        Y.CLOSE.BALF10 = 0
                    END
                NEXT KK
            END
        NEXT X

*--------------------------------------------------------
    END
******************************************************
END
