* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.RSR.COD.DSCR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.CLASS.CODE

    WS.ITEM.ID = ''
    WS.ITEM.ID = O.DATA
    FN.RCC = 'F.SCB.RISK.CLASS.CODE'
    F.RCC = '' ; R.RCC = ''
* KEY.CCY = 'NZD'
    CALL OPF(FN.RCC,F.RCC)
************************************************************
    WS.RSS.TITLE =''
    WS.ID.4 = WS.ITEM.ID[1,4]
    WS.ID.2 = WS.ITEM.ID[3,2]
    WS.ID.ZERO ='00'
    WS.ID.SUB.ITEM = WS.ID.4 : WS.ID.ZERO
* TEXT = 'ITEM.ID=' WS.ITEM.ID ; CALL REM
* TEXT = 'ITEM.4 =' WS.ID.4    ; CALL REM
* TEXT = 'ITEM.2 =' WS.ID.2    ; CALL REM
* TEXT = 'ID.SUB =' WS.ID.SUB.ITEM ; CALL REM
    CALL DBR ('SCB.RISK.CLASS.CODE':@FM:RCC.CLASS.NAME.1,WS.ID.SUB.ITEM,WS.RSS.TITLE)
    O.DATA = WS.ID.SUB.ITEM : "     " : WS.RSS.TITLE<1,1>
    RETURN
