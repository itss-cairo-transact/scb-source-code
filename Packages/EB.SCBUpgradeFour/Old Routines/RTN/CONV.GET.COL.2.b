* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-- CREATE BY NESSMA
    SUBROUTINE CONV.GET.COL.2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*-------------------------------------------------
    COD     = ""
    VAR.COL = 0
    TMP.ID  = ""
    TMP.ID  = O.DATA

    IF TMP.ID[5,1] EQ "A" THEN
        COD    = "B"
        TMP.ID = TMP.ID[1,4]:COD:TMP.ID[6,19]
        CALL DBR('SCB.LQDDTY.DAILY.FILE':@FM:LQDF.COLUMN.12,TMP.ID,VAR.COL)
    END ELSE
        COD = "A"
        TMP.ID = TMP.ID[1,4]:COD:TMP.ID[6,19]
        CALL DBR('SCB.LQDDTY.DAILY.FILE':@FM:LQDF.COLUMN.06,TMP.ID,VAR.COL)
    END

    O.DATA = VAR.COL

    RETURN
END
