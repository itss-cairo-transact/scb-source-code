* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LINE.MOV4

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''
    CALL OPF(FN.BAL,F.BAL)

    FN.LN = 'FBNK.RE.STAT.REP.LINE' ; F.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    TD1 = O.DATA ; CR.BAL = '' ; DB.BAL = ''
    CR.BAL1 = ''
*************************************************************************
    T.SEL = "SELECT ":FN.LN: " WITH @ID GE GENLED.0710 AND @ID LE GENLED.0840"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,2,2>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":"...":"-":TD1:"*":COMP
*************************************************************************
            T.SEL2 = "SELECT ":FN.BAL: " WITH @ID LIKE ":BAL.ID:" AND @ID UNLIKE ...LOCAL..."
            CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
            IF SELECTED2 THEN
                FOR K = 1 TO SELECTED2
                    CALL F.READ(FN.BAL,KEY.LIST2<K>,R.BAL,F.BAL,E3)
                    CUR = KEY.LIST2<K>[13,3]
                    CR.BAL1 = R.BAL<RE.SLB.CR.MOVEMENT>
                    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 71 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                    RATE = R.CCY<RE.BCP.RATE,POS>
                    CR.BAL += CR.BAL1 * RATE
                    CR.BAL = DROUND(CR.BAL,'2')
                NEXT K
            END
*************************************************************************
        NEXT I
    END
    O.DATA =  CR.BAL
    RETURN
*==============================================================
END
