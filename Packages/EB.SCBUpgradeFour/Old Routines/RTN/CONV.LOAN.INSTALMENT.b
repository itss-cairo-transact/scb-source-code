* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LOAN.INSTALMENT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS


    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    WS.ID   = O.DATA
    LD.ID   = FIELD(WS.ID,"-",1)
    WS.RATE = FIELD(WS.ID,"-",2)

    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,E2)

    WS.VALUE.DATE = R.LD<LD.VALUE.DATE>
    WS.MAT.DATE   = R.LD<LD.FIN.MAT.DATE>
    WS.AMT        = R.LD<LD.DRAWDOWN.ISSUE.PRC>


    DAYS = "C"
    CALL CDD("",WS.VALUE.DATE,WS.MAT.DATE,DAYS)

    WS.MONTHS = DAYS / 30
    WS.MONTHS = DROUND(WS.MONTHS,'0')

    WS.INSTALMENT.1 = ( WS.AMT * WS.RATE ) / 100
    WS.INSTALMENT.2 = WS.INSTALMENT.1 / 12
    WS.INSTALMENT.3 = WS.INSTALMENT.2 * WS.MONTHS

    WS.INSTALMENT = ( WS.INSTALMENT.3 + WS.AMT ) / WS.MONTHS
    WS.INSTALMENT = DROUND(WS.INSTALMENT,'2')

    O.DATA = WS.INSTALMENT

    RETURN
