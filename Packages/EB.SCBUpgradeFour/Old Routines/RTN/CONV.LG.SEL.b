* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>38</Rating>
*-----------------------------------------------------------------------------
*********** WAEL***********

    SUBROUTINE CONV.LG.SEL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    AMT='';INCRS.AMT='' ; TOTAL1 = 0 ; TOTAL2 = 0
    O.DATA = '1300002'
*================================================================================
    IF O.DATA THEN
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CUSTOMER.ID EQ ":O.DATA:" AND (OPERATION.CODE EQ ":1232:" OR OPERATION.CODE EQ ":1234:" OR OPERATION.CODE EQ ":1262:" OR OPERATION.CODE EQ ":1111:")"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        GOSUB LIVE

        T.SEL.H = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH CUSTOMER.ID EQ ":O.DATA:" AND (OPERATION.CODE EQ ":1232:" OR OPERATION.CODE EQ ":1234:" OR OPERATION.CODE EQ ":1262:" OR OPERATION.CODE EQ ":1111:")"
        CALL EB.READLIST(T.SEL.H,KEY.LIST.H,"",SELECTED.H,ER.MSG.H)
        GOSUB HISTORY

        O.DATA = TOTAL+TOTAL.H
        TEXT = 'GRAND.TOTAL=':O.DATA ; CALL REM
    END

*==============================================================================
    LIVE:
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,KEY.LIST<I>,OPER.CODE)
            OPER.CODE = OPER.CODE<1,52>
            IF OPER.CODE = 1111 THEN
                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,KEY.LIST<I>,AMT)
                TOTAL1 = TOTAL1 + AMT
            END ELSE
*-------------- GET THE AMOUNT.INCREASE ----------------------------
                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,KEY.LIST<I>,AMT1)
                T.SEL2 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":KEY.LIST<I>:"..."
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)

                IF KEY.LIST2 THEN
                    CALL DBR ('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.AMOUNT,KEY.LIST2<SELECTED2>,AMT2)
                    INCRS.AMT = AMT1-AMT2
                END
                TOTAL2 = TOTAL2 + INCRS.AMT
*------------------------------------------------------------------

            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    TOTAL = TOTAL1+TOTAL2
    TEXT = 'TOTAL.LIVE=':TOTAL ; CALL REM
    RETURN
*===================================================
    HISTORY:
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL DBR ('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.LOCAL.REF,KEY.LIST<I>,OPER.CODE)
            OPER.CODE = OPER.CODE<1,52>
            IF OPER.CODE = 1111 THEN
                CALL DBR ('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.AMOUNT,KEY.LIST<I>,AMT)
                TOTAL1 = TOTAL1 + AMT
            END ELSE
*-------------- GET THE AMOUNT.INCREASE ----------------------------
                CALL DBR ('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.AMOUNT,KEY.LIST<I>,AMT1)
                T.SEL2 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":KEY.LIST<I>:"..."
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)

                IF KEY.LIST2 THEN
                    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,KEY.LIST2<SELECTED2>,AMT2)
                    INCRS.AMT = AMT2-AMT1
                END
                TOTAL2 = TOTAL2 + INCRS.AMT
*------------------------------------------------------------------

            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    TOTAL.H = TOTAL1+TOTAL2
    TEXT = 'TOTAL.HISTORY=':TOTAL.H ; CALL REM
    RETURN
*====================================================
    RETURN
END
