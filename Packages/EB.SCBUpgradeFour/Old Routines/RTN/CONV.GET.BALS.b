* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.GET.BALS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    DATA.ID = O.DATA
    DATE.1  = FIELD(DATA.ID,'*',1)
    DATE.2  = FIELD(DATA.ID,'*',2)
    ACCT.NO = FIELD(DATA.ID,'*',3)

    FN.STE = 'FBNK.STMT.ENTRY'    ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ACC = 'FBNK.ACCOUNT'       ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)


    DB.MOV = 0 ; CR.MOV = 0
    CALL F.READ(FN.ACC,ACCT.NO,R.ACC,F.ACC,Y.ACC.ERR)
    Y.CURR    = R.ACC<AC.CURRENCY>

    CALL EB.ACCT.ENTRY.LIST(ACCT.NO<1>,DATE.1,DATE.2,STE.LIST,PRVBAL,ER111)

    LOOP
        REMOVE Y.STE.ID FROM STE.LIST SETTING POS
    WHILE Y.STE.ID:POS

        CALL F.READ(FN.STE,Y.STE.ID,R.STE,F.STE,Y.STE.ERR)

        IF Y.CURR NE 'EGP' THEN
            STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
        END ELSE
            STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
        END

        IF STE.AMT LT 0 THEN
            DB.MOV += STE.AMT
        END ELSE
            CR.MOV += STE.AMT
        END

    REPEAT
    CR.MOV = FMT(CR.MOV, "L0,")
    DB.MOV = FMT(DB.MOV, "L0,")
    O.DATA = CR.MOV : " *  " : DB.MOV
    RETURN
END
