* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GET.UA
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*---------------------------------------
    USR.ID = O.DATA
    FN.USR = "F.USER" ; F.USR = ""
    CALL OPF(FN.USR, F.USR)

    FN.UA = "F.USER.ABBREVIATION" ; F.UA = ""
    CALL OPF(FN.UA, F.UA)
*---------------------------------------
    CALL F.READ(FN.UA,USR.ID,R.UA,F.UA,E.UA)
*Line [ 36 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NON  = DCOUNT(R.UA<EB.UAB.ABBREVIATION>,@VM)
    ABB  = ""
*    TEXT = NON ; CALL REM
    FOR HH = 1 TO NON
        ABB = ABB : "#" : R.UA<EB.UAB.ABBREVIATION,HH>
    NEXT HH
    O.DATA = ABB
*---------------------------------------
    RETURN
END
