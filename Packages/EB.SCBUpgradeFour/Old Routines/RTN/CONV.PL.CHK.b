* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.PL.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
    XX = O.DATA

    FN.GL = 'F.CATEGORY'
    F.GL = '' ; R.GL = ''
    CALL OPF(FN.GL,F.GL)
    FROM.DATE = "20090101"
    TO.DATE = TODAY
************************************************************
*    T.SEL  = "SELECT ":FN.GL:" WITH DATA EQ '":WS.ENQ.NAME:"' OR DATA EQ '":WS.RTN.NAME:"' OR JOB.NAME EQ ":O.DATA
*    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,O.DATA,Y.ENTRY.FILE)
    IF Y.ENTRY.FILE THEN
        O.DATA = 'YES'
    END ELSE
        O.DATA = ' * '
    END
    RETURN
