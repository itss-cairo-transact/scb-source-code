* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOO---------------------------------------------
    SUBROUTINE CONV.REFREF

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS


    FN.CR='FBNK.STMT.ENTRY';F.CR=''
    CALL OPF(FN.CR,F.CR)
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS';F.LD=''
    CALL OPF(FN.LD,F.LD)
    FN.LDH='FBNK.LD.LOANS.AND.DEPOSITS';F.LDH=''
    CALL OPF(FN.LDH,F.LDH)
    FN.TT='FBNK.TELLER';F.TT=''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H='FBNK.TELLER$HIS';F.TT.H=''
    CALL OPF(FN.TT.H,F.TT.H)


    XX  = O.DATA
    CALL F.READ(FN.CR,XX,R.CR,F.CR,E2)
    REFS   = R.CR<AC.STE.OUR.REFERENCE>
    ACS   = R.CR<AC.STE.SYSTEM.ID>
    THIER = R.CR<AC.STE.THEIR.REFERENCE>
    REF   = R.CR<AC.STE.TRANS.REFERENCE>
    ACTT  = R.CR<AC.STE.ACCOUNT.NUMBER>

    IF  REFS[1,2] EQ 'FT' THEN
        REF2 = R.CR<AC.STE.OUR.REFERENCE>:';1'

        CALL F.READ(FN.FT.HIS,REF2,R.FT.HIS,F.FT.HIS,E2)

        DB.TH = R.FT.HIS<FT.DEBIT.THEIR.REF>

        O.DATA =  DB.TH
    END ELSE
        O.DATA = ''
    END
*************************************************************
    RETURN
END
