* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>

    SUBROUTINE CONV.SAV.TOTAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR

*   IF O.DATA THEN

*      CONT = DCOUNT(O.DATA,@VM)
*     TMP.DATA = '' ; TTT = ''
*  XXX = O.DATA<1,CONT>

*    IF CONT > 1 THEN
*  O.DATA = XXX

*       FOR I = 1 TO CONT-1
*          TMP.DATA += O.DATA<1,I>
*     NEXT I
*    O.DATA = TMP.DATA
**************************************
*    O.DATA = O.DATA<CONT>
* END ELSE
*    O.DATA = O.DATA
*END

************************************88
    IF O.DATA NE '' THEN

        CONT = DCOUNT(O.DATA,@VM)
        TMP.DATA = '' ; TTT = ''

        IF CONT >= 1 THEN
            TMP.DATA1 = O.DATA<1,CONT>

            TMP.DATA = MINIMUM (TMP.DATA1)
            O.DATA = TMP.DATA
        END
    END



    RETURN
END
