* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.INT.BOOK.CCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    XX      = O.DATA
    AMT     = 0

    FN.AC   = 'FBNK.ACCOUNT'
    F.AC    = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CCY  = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY   = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)
************************************************************

    CALL F.READ(FN.AC,XX,R.AC,F.AC,E1)
    AMTT   =  R.AC<AC.ACCR.DR.SUSP>
    CURR   =  R.AC<AC.CURRENCY>

*Line [ 46 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AMT.NO = DCOUNT(AMTT,@VM)
    FOR I  = 1 TO AMT.NO
        AMT   = AMT + AMTT<1,I>
    NEXT I

    IF CURR NE 'EGP' THEN
        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 54 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
        RATE   = R.CCY<RE.BCP.RATE,POS>
    END
    ELSE
        RATE = 1
    END

    O.DATA = RATE * AMT
    RETURN
END
