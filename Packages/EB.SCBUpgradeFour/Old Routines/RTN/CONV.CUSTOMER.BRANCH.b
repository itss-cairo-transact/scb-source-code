* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*----RANIA & ABEER ------*
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CUSTOMER.BRANCH

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER


    IF O.DATA THEN
        FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = ''
        CALL OPF(FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ(FN.CUSTOMER,O.DATA,R.CUSTOMER,F.CUSTOMER,E1)
        MYLANGUAGE = R.CUSTOMER<EB.CUS.LANGUAGE>
        COMP.BOOK  = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
        CUS.BR     = COMP.BOOK[2]
        MYACCTOFF  = TRIM(CUS.BR, "0" , "L")
        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,MYACCTOFF,BRANCH)
        ENBRANCH=FIELD(BRANCH,'.',1)
        ARBRANCH=FIELD(BRANCH,'.',2)
        IF MYLANGUAGE = '1' THEN
            O.DATA = ENBRANCH
        END  ELSE
            O.DATA = ARBRANCH
        END

    END
    RETURN
END
