* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GET.GATEG

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.LOANS.PARM
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.LOANS.PARM.CONT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D
*---------------------------------------------------
    G.VALUE    = 0
    RECIEVE.ID = O.DATA
    FN.TMP     = "F.SCB.MLAD.LOANS.PARM"   ; F.TMP = ""
    CALL OPF(FN.TMP,F.TMP)

    FN.MAS = "F.CATEG.MAS.D"               ; F.MAS = ""
    CALL OPF(FN.MAS,F.MAS)
*---------------------------------------------------
* "*" : 3 : "*" : 4 : "*" : CURR
    ID.TEMP = FIELD(RECIEVE.ID,'*',1)
    CAT.FRM = FIELD(RECIEVE.ID,'*',2)
    CAT.TO  = FIELD(RECIEVE.ID,'*',3)
    CURR    = FIELD(RECIEVE.ID,'*',4)

    N.SEL  = "SELECT ":FN.MAS:" WITH CATD.CATEG.CODE GE ":CAT.FRM
    N.SEL := " AND CATD.CATEG.CODE LE ":CAT.TO
    N.SEL := " AND CATD.CY.ALPH.CODE EQ ":CURR
    CALL EB.READLIST(N.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.MAS,KEY.LIST<II>,R.MAS,F.MAS,EER.MAS)
            G.VALUE += R.MAS<CAT.CATD.BAL.IN.ACT.CY>
        NEXT II
    END

    O.DATA = G.VALUE
*---------------------------------------------------
    RETURN
END
