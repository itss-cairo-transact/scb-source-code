* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LG.AMOUNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

*   O.DATA <-------- LD

    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK LIKE ": O.DATA:"...":" AND ":'EVAL"LEN(CREDIT.ACCT.NO)" > 12 '

    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FN.FUND ='FBNK.FUNDS.TRANSFER' ;R.FUND='';F.FUND=''
    CALL OPF(FN.FUND,F.FUND)

    CALL F.READ(FN.FUND,KEY.LIST, R.FUND, F.FUND,E)

    SAM = ABS(KEY.LIST)

********************************************************************
    T.SEL1 = "SELECT FBNK.STMT.ENTRY WITH TRANS.REFERENCE EQ ": SAM

    KEY.LIST1 = ""
    SELECTED1 = ""
    ER.MSG1   = ""

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    FN.STMT ='FBNK.STMT.ENTRY' ;R.STMT='';F.STMT=''
    CALL OPF(FN.STMT,F.STMT)

    CALL F.READ(FN.STMT,KEY.LIST1,R.STMT, F.STMT,E)
    SAMSAM = R.STMT<AC.STE.AMOUNT.LCY>

    IF SELECTED EQ 0 THEN
        O.DATA = R.RECORD<LD.AMOUNT>
    END ELSE
        O.DATA = ABS(SAMSAM)
    END

    RETURN
END
