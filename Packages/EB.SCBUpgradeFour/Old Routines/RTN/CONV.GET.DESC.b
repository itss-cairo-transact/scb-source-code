* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GET.DESC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*--------------------------------------
    WS.DESC = ""
    TEMP.ID = O.DATA
    WS.D1   = FIELD(TEMP.ID,"*",1)
    WS.D2   = FIELD(TEMP.ID,"*",2)

    IF WS.D2 EQ "1M" THEN
        WS.D3 = " ��� "
    END
    IF WS.D2 EQ "2M" THEN
        WS.D3 = " ����� "
    END
    IF WS.D2 EQ "3M" THEN
        WS.D3 = "3 ���� "
    END
    IF WS.D2 EQ "6M" THEN
        WS.D3 = " 6 ���� "
    END
    IF WS.D2 EQ "9M" THEN
        WS.D3 = " 9 ���� "
    END
    IF WS.D2 EQ "12M" THEN
        WS.D3 = " ��� "
    END

    XX = ""
*    XX = FIELD(WS.D2,"M", 1)
*    IF XX GT 12 THEN
*TEXT = "IN"  ; CALL REM
*TEXT = WS.D2 : "*" : XX ; CALL REM
*        WS.D3 = " ���� �� ��� "
*    END

    IF WS.D3 EQ '' THEN
        WS.DESC = WS.D1
    END ELSE
        WS.DESC = WS.D1 :" ���� ": WS.D3
    END

    O.DATA  = WS.DESC
*---------------------------------------
    RETURN
END
