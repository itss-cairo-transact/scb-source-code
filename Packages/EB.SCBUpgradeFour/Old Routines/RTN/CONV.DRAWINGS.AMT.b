* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.DRAWINGS.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

    IF O.DATA THEN
        FN.DRW = "FBNK.DRAWINGS"
        F.DRW  = ""
        R.DRW = ""
        CALL OPF( FN.DRW,F.DRW)

        DRW.AMT  = 0
        LC.NO    = FIELD(O.DATA,'-',1)
        DRW.NOO  = FIELD(O.DATA,'-',2)

        FOR I = 1 TO DRW.NOO -1

            IF I LT 10 THEN
                DRW.ID = LC.NO:"0":I
            END ELSE
                DRW.ID = LC.NO:I
            END

            CALL F.READ(FN.DRW,DRW.ID,R.DRW,F.DRW,ERR.DRW)
            IF R.DRW<TF.DR.DRAWING.TYPE> = "DP" THEN
                IF R.DRW<TF.DR.MATURITY.REVIEW> GT TODAY THEN
                    DRW.AMT = DRW.AMT + R.DRW<TF.DR.DOCUMENT.AMOUNT>
                END
            END
        NEXT I
        O.DATA = DRW.AMT

    END
    RETURN
END
