* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.GET.MAIL.ADDRESS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*----------------------------------------
    RTN.MAIL = ''
    ACCT.ID  = O.DATA
    FN.AC    = "FBNK.ACCOUNT"   ; F.AC =""
    CALL OPF(FN.AC,F.AC)

*****************************************
    CALL F.READ(FN.AC,ACCT.ID,R.AC,F.AC,EER.AC)
    AC.MAIL = R.AC<AC.LOCAL.REF><1,ACLR.EMAIL.ADDRESS>
    IF AC.MAIL EQ '' THEN
*-- CHK CUS
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.ID,CUS.ID)
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
        RTN.MAIL = LOCAL.REF<1,CULR.EMAIL.ADDRESS>
    END ELSE
        RTN.MAIL = AC.MAIL
    END
*--- V1
    IF RTN.MAIL<1,1,2> NE '' THEN
        IF RTN.MAIL<1,1,1>[1] EQ ',' THEN
            O.DATA = RTN.MAIL<1,1,1>:RTN.MAIL<1,1,2>
        END ELSE
            O.DATA = RTN.MAIL<1,1,1>:',':RTN.MAIL<1,1,2>
        END
    END ELSE
        O.DATA = RTN.MAIL
    END
    RETURN
