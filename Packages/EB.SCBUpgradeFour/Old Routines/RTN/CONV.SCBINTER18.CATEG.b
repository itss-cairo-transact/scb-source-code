* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>900</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.SCBINTER18.CATEG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

    XX = O.DATA

    IF XX EQ 21001 THEN O.DATA = 582
    IF XX EQ 21002 THEN O.DATA = 583
    IF XX EQ 21003 THEN O.DATA = 584
    IF XX EQ 21004 THEN O.DATA = 585
    IF XX EQ 21005 THEN O.DATA = 586
    IF XX EQ 21006 THEN O.DATA = 587
    IF XX EQ 21007 THEN O.DATA = 588
    IF XX EQ 21008 THEN O.DATA = 590
    IF XX EQ 21010 THEN O.DATA = 591

    RETURN
