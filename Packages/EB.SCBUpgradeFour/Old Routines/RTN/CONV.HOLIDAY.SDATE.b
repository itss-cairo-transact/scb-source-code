* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.HOLIDAY.SDATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMPLOYEE.HOLIDAY

    FN.HOL = 'F.SCB.EMPLOYEE.HOLIDAY' ; F.HOL = ''
    CALL OPF(FN.HOL,F.HOL)

    XX       = O.DATA
    REC.ID   = FIELD(XX,"-",1)
    INP.DATE = FIELD(XX,"-",2)
************************************************************

    CALL F.READ(FN.HOL,REC.ID,R.HOL,F.HOL,E1)
*Line [ 39 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE INP.DATE IN R.HOL<EH.INPUT.DATE,1> SETTING POS ELSE NULL
    ST.DATE = R.HOL<EH.START.DATE,POS>
    O.DATA  = ST.DATE

    RETURN
