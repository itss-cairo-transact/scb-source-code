* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>40</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LG.TRANSACTION.FT.CATEG.DEBIT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    ID.NO=FIELD(O.DATA,".",1)
    ID.NO=ID.NO:'...'
    DB.ACCT=FIELD(O.DATA,".",2)

    T.SEL= "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK LIKE ": ID.NO
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FN.FUNDS ='FBNK.FUNDS.TRANSFER' ;R.FUNDS='';F.FUNDS=''
    CALL OPF(FN.FUNDS,F.FUNDS)
    O.DATA=''

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.FUNDS,KEY.LIST<I>, R.FUNDS, F.FUNDS,E)
        ORDER.BANK = R.FUNDS<FT.ORDERING.BANK>
        SAM =FIELD(ORDER.BANK,".",2)
        SAM1=FIELD(ORDER.BANK,".",3)
        SAM2=SAM:'.':SAM1
        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,SAM2,DESC1)

        ACCT= R.FUNDS<FT.CREDIT.ACCT.NO>
        IF NUM(ACCT) THEN
            IF DB.ACCT EQ R.FUNDS<FT.DEBIT.ACCT.NO> THEN
                O.DATA<1,I>="-":R.FUNDS<FT.DEBIT.AMOUNT>:" " : DESC1
            END ELSE
                IF DB.ACCT EQ R.FUNDS<FT.CREDIT.ACCT.NO> THEN
                    O.DATA<1,I>=R.FUNDS<FT.DEBIT.AMOUNT>:" " : DESC1
                END
            END
        END
    NEXT I
    RETURN
END
