* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.SCCD.LOAN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.LIQ.RATE


    IF O.DATA THEN

        DAYS.RATE='C'
***********
        YTEXT = " Enter Date :  "
        CALL TXTINP(YTEXT, 8, 22, "17", "A")
        TO.DATE  = COMI
        TEXT=TO.DATE;CALL REM
        IF COMI LT TODAY THEN
            O.DATA = ""
        END ELSE
******************
            DATE.VAL = O.DATA
            CALL CDD("",DATE.VAL,TO.DATE,DAYS.RATE)
            TEXT=DAYS.RATE;CALL REM
            CATEG = '21103'
            F.CD.LIQ = '' ; FN.CD.LIQ = 'F.SCB.CD.LIQ.RATE' ; R.SCB.CD.LIQ = ''
            CALL OPF(FN.CD.LIQ,F.CD.LIQ)
            CALL F.READ(FN.CD.LIQ,CATEG,R.CD.LIQ,F.CD.LIQ,ETEXT)

*Line [ 50 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT =  DCOUNT (R.CD.LIQ<SCB.CD.LIQ.MIN.DAY>,@VM)
            FOR I = 1 TO TEMP.COUNT
                IF R.CD.LIQ<SCB.CD.LIQ.MIN.DAY,I> LE DAYS.RATE AND R.CD.LIQ<SCB.CD.LIQ.MAX.DAY,I> GE DAYS.RATE THEN
                    MAX.DA=R.CD.LIQ<SCB.CD.LIQ.MAX.DAY,I>
                    MIN.DA= R.CD.LIQ<SCB.CD.LIQ.MIN.DAY,I>
                    I = TEMP.COUNT
                END
            NEXT I
******************
            IF MIN.DA GE '731' AND MAX.DA  LE '1095' THEN
                O.DATA = '0.792':'-':COMI
            END
            IF MIN.DA GE '1096' AND MAX.DA  LE '1460' THEN
                O.DATA= '0.7583':'-':COMI
            END
            IF MIN.DA GE '1461' AND MAX.DA  LE '1825' THEN
                O.DATA = '0.7321':'-':COMI
            END
        END
    END
    RETURN
END
