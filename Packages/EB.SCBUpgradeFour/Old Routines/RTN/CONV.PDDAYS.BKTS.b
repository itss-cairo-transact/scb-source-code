* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.PDDAYS.BKTS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

    PD.DAYS = O.DATA

    IF PD.DAYS = 0 THEN BKTS = 0
    IF PD.DAYS GE 1 AND PD.DAYS LE 30 THEN BKTS = 1
    IF PD.DAYS GE 31 AND PD.DAYS LE 60 THEN BKTS = 2
    IF PD.DAYS GE 61 AND PD.DAYS LE 90 THEN BKTS = 3
    IF PD.DAYS GE 91 AND PD.DAYS LE 120 THEN BKTS = 4
    IF PD.DAYS GE 121 AND PD.DAYS LE 150 THEN BKTS = 5
    IF PD.DAYS GE 151 AND PD.DAYS LE 180 THEN BKTS = 6
    IF PD.DAYS GT 180 THEN BKTS = 7


    O.DATA = BKTS


    RETURN
