* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE

    SUBROUTINE CONV.LC.COMM

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE


    FN.FT.COM = 'F.FT.COMMISSION.TYPE';F.FT.COM = '';R.FT.COM=''
    CALL OPF(FN.FT.COM,F.FT.COM)

    FN.FT.CHRG = 'F.FT.CHARGE.TYPE'     ;  F.FT.CHRG = '' ; R.FT.CHRG  = '' ; ETEXT = ''
    CALL OPF(FN.FT.CHRG,F.FT.CHRG)
**********************
    CHRG.CODE = O.DATA
    CALL F.READ(FN.FT.COM,CHRG.CODE,R.FT.COM,F.FT.COM,E1)
    PL.COM =  R.FT.COM<FT4.CATEGORY.ACCOUNT>
*    FLT.AMT=  R.FT.COM<FT4.FLAT.AMT>
    PERC=R.FT.COM<FT4.PERCENTAGE>
    IF E1 THEN
        CALL F.READ(FN.FT.CHRG,CHRG.CODE,R.FT.CHRG,F.FT.CHRG,E11)
        PL.COM = R.FT.CHRG<FT5.CATEGORY.ACCOUNT>
        FLT.AMT = R.FT.CHRG<FT5.FLAT.AMT>
    END
    O.DATA = PL.COM:'-':FLT.AMT:'-':PERC
    RETURN
END
