* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LI1.FCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBM.LG.SECTOR.1

    XX = O.DATA
    AMT = FIELD(XX,"-",1)
    LIS = FIELD(XX,"-",2)
    SEC = FIELD(XX,"-",3)
    MRG.500.FCY.1 = 0
    MRG.110.FCY.1 = 0
    MRG.OTH.FCY.1 = 0
    MRG.500.FCY.2 = 0
    MRG.110.FCY.2 = 0
    MRG.OTH.FCY.2 = 0
    FIN.AMT       = 0

    FN.LGS = 'F.SBM.LG.SECTOR.1' ; F.LGS  = ''
    CALL OPF(FN.LGS,F.LGS)
************************************************************
    CALL F.READ(FN.LGS,'MRG',R.LGS,F.LGS,E1)

    MRG.500.FCY.1 = R.LGS<RE.MRG.500.FCY.1>
    MRG.110.FCY.1 = R.LGS<RE.MRG.110.FCY.1>
    MRG.OTH.FCY.1 = R.LGS<RE.MRG.OTH.FCY.1>
    MRG.500.FCY.2 = R.LGS<RE.MRG.500.FCY.2>
    MRG.110.FCY.2 = R.LGS<RE.MRG.110.FCY.2>
    MRG.OTH.FCY.2 = R.LGS<RE.MRG.OTH.FCY.2>

    BEGIN CASE
    CASE ( LIS EQ 1 AND SEC EQ 1 )
        FIN.AMT = AMT - MRG.500.FCY.1

    CASE ( LIS EQ 1 AND SEC EQ 2 )
        FIN.AMT = AMT - MRG.110.FCY.1

    CASE ( LIS EQ 1 AND SEC EQ 3 )
        FIN.AMT = AMT - MRG.OTH.FCY.1

    CASE ( LIS EQ 2 AND SEC EQ 1 )
        FIN.AMT = AMT - MRG.500.FCY.2

    CASE ( LIS EQ 2 AND SEC EQ 2 )
        FIN.AMT = AMT - MRG.110.FCY.2

    CASE ( LIS EQ 2 AND SEC EQ 3 )
        FIN.AMT = AMT - MRG.OTH.FCY.2

    CASE OTHERWISE
        FIN.AMT = AMT
    END CASE


    O.DATA = FIN.AMT
    RETURN
