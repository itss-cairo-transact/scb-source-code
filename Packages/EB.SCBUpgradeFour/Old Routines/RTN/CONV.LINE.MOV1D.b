* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LINE.MOV1D

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''
    CALL OPF(FN.BAL,F.BAL)

    FN.LN = 'FBNK.RE.STAT.REP.LINE' ; F.LN = ''
    CALL OPF(FN.LN,F.LN)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    TD1 = O.DATA  ; CR.BAL = '' ; DB.BAL = ''
*************************************************************************
    T.SEL = "SELECT ":FN.LN: " WITH @ID GE GENLED.0020 AND @ID LE GENLED.0120"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,2,2>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":"EGP":"-":TD1:"*":COMP
*************************************************************************
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
            DB.BAL += R.BAL<RE.SLB.DB.MVMT.LCL>
*************************************************************************
        NEXT I
    END
    O.DATA =  DB.BAL
    RETURN
*==============================================================
END
