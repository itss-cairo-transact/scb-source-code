* @ValidationCode : Mjo4MDcwMjI4OTQ6Q3AxMjUyOjE2NDEyMDM0NDY1NTM6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 03 Jan 2022 11:50:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.REF.BEN

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed I_FT.LOCAL.REFS Not Used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 32 ] Removed I_TT.LOCAL.REFS Not Used - ITSS - R21 Upgrade - 2021-12-23
*   $INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
**********************************
    FN.FT  = "FBNK.FUNDS.TRANSFER" ; F.FT = ""
    CALL OPF(FN.FT,F.FT)

    FN.AC  = "FBNK.ACCOUNT" ; F.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.TT  = "FBNK.TELLER" ; F.TT = ""
    CALL OPF(FN.TT,F.TT)

    FN.FT.H  = "FBNK.FUNDS.TRANSFER$HIS" ; F.FT.H = ""
    CALL OPF(FN.FT.H,F.FT.H)

    FN.TT.H  = "FBNK.TELLER$HIS" ; F.TT.H = ""
    CALL OPF(FN.TT.H,F.TT.H)

    FN.FT.ARC  = "FBNK.FUNDS.TRANSFER$ARC" ; F.FT.ARC = ""
    CALL OPF(FN.FT.ARC,F.FT.ARC)

    FN.TT.ARC  = "FBNK.TELLER$ARC" ; F.TT.ARC = ""
    CALL OPF(FN.TT.ARC,F.TT.ARC)

    REF     = O.DATA
    REF.ARC = O.DATA:';1'

    IF REF[1,2] EQ 'FT' THEN
        CALL F.READ(FN.FT,REF,R.FT,F.FT,ER.FT)
        IF NOT(ER.FT) THEN
            FT.BEN.AC = R.FT<FT.CREDIT.ACCT.NO>
            CALL F.READ(FN.AC,FT.BEN.AC,R.AC,F.AC,ERR.AC)
            FT.BEN = R.AC<AC.ACCOUNT.TITLE.1>
        END ELSE
            CALL F.READ.HISTORY(FN.FT.H,REF,R.FT.H,F.FT.H,ER.FT.H)
            IF NOT(ER.FT.H) THEN
                FT.BEN.AC.H = R.FT.H<FT.CREDIT.ACCT.NO>
                CALL F.READ(FN.AC,FT.BEN.AC.H,R.AC,F.AC,ERR.AC)
                FT.BEN = R.AC<AC.ACCOUNT.TITLE.1>
            END ELSE
                CALL F.READ(FN.FT.ARC,REF.ARC,R.FT.ARC,F.FT.ARC,ER.FT.ARC)
                FT.BEN.AC.ARC = R.FT.ARC<FT.CREDIT.ACCT.NO>
                CALL F.READ(FN.AC,FT.BEN.AC.ARC,R.AC,F.AC,ERR.AC)
                FT.BEN = R.AC<AC.ACCOUNT.TITLE.1>
            END
        END
        CUS.BEN = FT.BEN
    END

    IF REF[1,2] EQ 'TT' THEN
        CALL F.READ(FN.TT,REF,R.TT,F.TT,ER.TT)
        IF NOT(ER.TT) THEN
            TT.BEN = R.TT<TT.TE.NARRATIVE.1>
            IF TT.BEN EQ '' THEN
                TT.BEN = R.TT<TT.TE.NARRATIVE.2>
            END
        END ELSE
            CALL F.READ.HISTORY(FN.TT.H,REF,R.TT.H,F.TT.H,ER.TT.H)
            IF NOT(ER.TT.H) THEN
                TT.BEN = R.TT.H<TT.TE.NARRATIVE.1>
                IF TT.BEN EQ '' THEN
                    TT.BEN = R.TT.H<TT.TE.NARRATIVE.2>
                END
            END ELSE
                CALL F.READ(FN.TT.ARC,REF.ARC,R.TT.ARC,F.TT.ARC,ER.TT.ARC)
                TT.BEN = R.TT.ARC<TT.TE.NARRATIVE.1>
                IF TT.BEN EQ '' THEN
                    TT.BEN = R.TT.ARC<TT.TE.NARRATIVE.2>
                END
            END
        END
        CUS.BEN = TT.BEN
    END

    O.DATA = CUS.BEN

RETURN
