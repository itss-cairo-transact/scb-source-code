* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.LG.OVERRIDE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

    LG.OVV      = O.DATA

    LG.AC=  FIELD(LG.OVV,'LGAC',2)
    IF LG.AC NE '' THEN AC = 'LGAC'
**
    LG.CO = FIELD(LG.OVV,'LGCO',2)

    IF LG.CO NE '' THEN CO = 'LGCO'
**
    LG.TH = FIELD(LG.OVV,'LGTH',2)
    IF LG.TH NE '' THEN TH = 'LGTH'
**
    LG.DA = FIELD(LG.OVV,'LGDA',2)
    IF LG.DA NE '' THEN DA = 'LGDA'
**
    LG.SE = FIELD(LG.OVV,'LGSE',2)
    IF LG.SE NE '' THEN SE = 'LGSE'
**
    O.DATA = CO:'-':DA:'-':TH:'-':SE:'-':AC

    RETURN
END
