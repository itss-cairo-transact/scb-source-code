* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LG.PERC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '';R.LD=''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,O.DATA,R.LD,F.LD,E1)

    LOCAL.REF=R.LD<LD.LOCAL.REF>
    LG.PERC=LOCAL.REF<1,LDLR.MARGIN.PERC>

    LG.PERC1=FIELD(LG.PERC,'.',1)
    LG.PERC2=FIELD(LG.PERC,'.',2)[1,2]
    IF LG.PERC2 NE '' THEN
        LG.PERC=LG.PERC1:'.':LG.PERC2
    END ELSE
        LG.PERC=LG.PERC
    END
    O.DATA=LG.PERC
    RETURN
END
