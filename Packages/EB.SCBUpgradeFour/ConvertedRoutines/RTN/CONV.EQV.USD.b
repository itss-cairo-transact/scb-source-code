* @ValidationCode : MjoxOTkxNjE1NTk6Q3AxMjUyOjE2NDEwOTc2Njc4NDY6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:27:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
****NESSREEN AHMED 18/03/2015******************
*-----------------------------------------------------------------------------
* <Rating>374</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.EQV.USD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CURRENCY.DAILY
*missing layout I_F.SCB.CURRENCY.DAILY
*-----------------------------------------------------------------------*
    FN.CURR.DALY = 'F.SCB.CURRENCY.DAILY' ; F.CURR.DALY = '' ; R.CURR.DALY = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CURR.DALY,F.CURR.DALY)

    TT.DATE = O.DATA
    KEY.ID = "USD":"-" :"EGP":"-":TT.DATE
    CALL F.READ(FN.CURR.DALY,KEY.ID,R.CURR.DALY,F.CURR.DALY,ERR2)
    DAY.NUM2 = R.CURR.DALY<SCCU.DAY.NUMBER>
    CURR.MARK = R.CURR.DALY<SCCU.CURR.MARKET>
    LOCATE '10' IN R.CURR.DALY<SCCU.CURR.MARKET,1> SETTING NN THEN
        SELL.RATE = R.CURR.DALY<SCCU.SELL.RATE ,NN>
    END
    O.DATA = SELL.RATE

RETURN
END
