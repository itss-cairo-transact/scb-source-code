* @ValidationCode : MjoxNTg2NTYzNTM3OkNwMTI1MjoxNjQyNTgzMzUzNDg1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 19 Jan 2022 11:09:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------NI7OOOOOOOOOOOOOOOOOOO------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.GOV


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 33 ]  ADDED I_F.SCB.CUS.REGION - ITSS - R21 Upgrade - 2021-12-23
  

    YY = O.DATA

**** TEXT = "REG :" : YY ; CALL REM
    IF   YY = 999 OR YY = 998 THEN
        O.DATA = ''
*** TEXT = "REG2 " : O.DATA ; CALL REM
    END ELSE
       
        
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.CUS.REGION':@FM:REG.DESCRIPTION,YY,CUST.ADD2)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,YY,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
CUST.ADD2=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
        
        O.DATA = CUST.ADD2
    END
RETURN
END
