* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LD.CCY.A

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

    XX = O.DATA
    YY = FIELD(XX,'*',1)
    ZZ = FIELD(XX,'*',2)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)

***********************************************************
    IF YY EQ 'USD' THEN
        WW = ZZ
        O.DATA = ZZ
    END ELSE

        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 46 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE YY IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
        RATE1 = R.CCY<RE.BCP.RATE,POS>

        PP = RATE1
        WW1 = RATE1 * ZZ

        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 54 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE 'USD' IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
        RATE2 = R.CCY<RE.BCP.RATE,POS>

        WW  = WW1 / RATE2

        O.DATA = WW
    END

    RETURN
