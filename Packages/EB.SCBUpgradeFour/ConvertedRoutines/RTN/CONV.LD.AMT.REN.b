* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LD.AMT.REN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    LD.ID = O.DATA
    FN.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD.LOANS.AND.DEPOSITS = '' ; R.LD.LOANS.AND.DEPOSITS = ''
    CALL OPF( FN.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS)
    CALL F.READ(FN.LD.LOANS.AND.DEPOSITS,LD.ID, R.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS, ETEXT)

    CAP.REN = R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.RENEW.METHOD>
    TOT.INT = R.LD.LOANS.AND.DEPOSITS<LD.TOT.INTEREST.AMT>

    IF CAP.REN = 1 OR CAP.REN = 7 OR CAP.REN = '' THEN
        AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT>
    END
    IF CAP.REN = 2 OR CAP.REN = 8 THEN
        AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> + R.LD.LOANS.AND.DEPOSITS<LD.TOT.INTEREST.AMT>
    END
    IF CAP.REN = 3 THEN
        IF NOT(R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT>) THEN
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> + R.LD.LOANS.AND.DEPOSITS<LD.TOT.INTEREST.AMT>
        END ELSE
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> + R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT> + R.LD.LOANS.AND.DEPOSITS<LD.TOT.INTEREST.AMT>
        END
    END
    IF CAP.REN = 5 THEN
        IF NOT(R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT>) THEN
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT>
        END ELSE
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> + R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT>
        END
    END
    IF CAP.REN = 4 THEN
        IF NOT(R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT>) THEN
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> + R.LD.LOANS.AND.DEPOSITS<LD.TOT.INTEREST.AMT>
        END ELSE
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> -  R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT> + R.LD.LOANS.AND.DEPOSITS<LD.TOT.INTEREST.AMT>
        END
    END
    IF CAP.REN = 6 THEN
        IF NOT(R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT>) THEN
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT>
        END ELSE
            AMT = R.LD.LOANS.AND.DEPOSITS<LD.REIMBURSE.AMOUNT> -  R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.LD.RENEWAL.AMT>
        END
    END

    O.DATA = AMT
    RETURN
END
