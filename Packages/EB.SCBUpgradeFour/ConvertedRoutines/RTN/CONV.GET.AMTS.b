* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GET.AMTS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*-----------------------------------------------
    COMP.CAT = O.DATA
    COMP.ID  = FIELD(O.DATA,'*',1)
    CAT.ID   = FIELD(O.DATA,'*',2)
    LW.DATE  = R.DATES(EB.DAT.LAST.WORKING.DAY)
    FN.LD    = "FBNK.LD.LOANS.AND.DEPOSITS" ; F.LD = ""
    CALL OPF(FN.LD , F.LD)

    LD.AMT1 = 0
    LD.AMT2 = 0     ;* TOTAL INTERNAL
    LD.AMT3 = 0
    LD.AMT4 = 0
    LD.AMT5 = 0
*** FOR INTERNAL AMOUNTS
    LD.AMT6 = 0 ; LD.AMT7=0 ; LD.AMT8=0 ; LD.AMT9=0 ; LD.AMT10=0

    TOT1 = 0 ; TOT2 = 0 ; TOT3 = 0

    T.SEL  = "SELECT ":FN.LD:" WITH CO.CODE EQ ":COMP.ID
    T.SEL := " AND CATEGORY EQ ":CAT.ID
    T.SEL := " AND CURRENCY EQ 'EGP'"
    T.SEL := " AND VALUE.DATE GE '20161110'"
    T.SEL := " AND VALUE.DATE LE ":LW.DATE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<HH>,R.LD,F.LD,E1)
            SEL.TYPE = R.LD<LD.LOCAL.REF><1,LDLR.INVESTED.AMOUNT>

            IF SEL.TYPE EQ "FRESH FUND" THEN
                LD.AMT1  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "FRESH LOBNK" THEN
                LD.AMT4  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "INTERNAL" THEN
                LD.AMT2  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "FRESH FORBNK" THEN
                LD.AMT5  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "FINANCE-LOAN" THEN
                LD.AMT3  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "INTRL-SAV-AC" THEN
                LD.AMT6  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "INTRL-CUR-AC" THEN
                LD.AMT7  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "INTRL-TIM-DP" THEN
                LD.AMT8  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "INTRL-CER-DP" THEN
                LD.AMT9  += R.LD<LD.AMOUNT>
            END
            IF SEL.TYPE EQ "INTRL-BRK-DP" THEN
                LD.AMT10  += R.LD<LD.AMOUNT>
            END
        NEXT HH
    END
    TOT.INT  = 0
    TOT.INT  =  LD.AMT6+LD.AMT7+LD.AMT8+LD.AMT9+LD.AMT10

    LD.AMT1  =  LD.AMT1 / 1000
    LD.AMT2  =  LD.AMT2 / 1000
    LD.AMT3  =  LD.AMT3 / 1000
    LD.AMT4  =  LD.AMT4 / 1000
    LD.AMT5  =  LD.AMT5 / 1000
    LD.AMT6  =  LD.AMT6 / 1000
    LD.AMT7  =  LD.AMT7 / 1000
    LD.AMT8  =  LD.AMT8 / 1000
    LD.AMT9  =  LD.AMT9 / 1000
    LD.AMT10 =  LD.AMT10 / 1000

    TOT1 = LD.AMT1 + LD.AMT2 + LD.AMT3 + TOT.INT
    TOT2 = LD.AMT4 + LD.AMT5
    TOT3 = TOT1 + TOT2

    O.DATA  = LD.AMT1:"*":LD.AMT2:"*":LD.AMT3:"*":TOT1:"*":LD.AMT4:"*"
    O.DATA := LD.AMT5:"*":TOT2:"*":TOT3:"*"
    O.DATA := LD.AMT6:"*":LD.AMT7:"*":LD.AMT8:"*":LD.AMT9:"*":LD.AMT10
*----------------------------------------------------------------------
    RETURN
END
