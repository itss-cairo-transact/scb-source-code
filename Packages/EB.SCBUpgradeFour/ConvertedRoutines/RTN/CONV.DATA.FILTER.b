* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/07/04  ***
********************************************
    SUBROUTINE CONV.DATA.FILTER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CU   = 'FBNK.CUSTOMER'   ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
****

    WS.GT.100.DSCR = "  ������� ���� �� �����   "
    WS.1002.DSCR   = "  ������� �������������   "

****
    WS.SYS.ID = FIELD(O.DATA,"*",1)
    WS.CUS.ID = FIELD(O.DATA,"*",2)

    IF WS.CUS.ID EQ '' THEN
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,WS.SYS.ID,WS.AC.NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,WS.SYS.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WS.AC.NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
        O.DATA = WS.SYS.ID :'.':WS.AC.NAME
        RETURN
    END
    IF WS.CUS.ID NE '' THEN
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,WS.LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
WS.LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        WS.CREDIT.CODE = WS.LOCAL.REF<1,CULR.CREDIT.CODE>
        WS.CREDIT.STAT = WS.LOCAL.REF<1,CULR.CREDIT.STAT>
        WS.CUS.NAME    = WS.LOCAL.REF<1,CULR.ARABIC.NAME>

        IF WS.CREDIT.CODE GT 100 OR WS.CREDIT.STAT NE '' THEN
            WS.NEW.ID = WS.SYS.ID[9,8]
            WS.RET.ID = 'XXXXXXXX':WS.NEW.ID
            O.DATA    = WS.RET.ID :'.':WS.GT.100.DSCR
            RETURN
        END

*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,WS.SYS.ID,WS.GL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,WS.SYS.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WS.GL=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF WS.GL EQ 1002 THEN
            WS.NEW.ID = WS.SYS.ID[9,8]
            WS.RET.ID = 'XXXXXXXX':WS.NEW.ID
            O.DATA    = WS.RET.ID :'.':WS.1002.DSCR
            RETURN
        END
        O.DATA        = WS.SYS.ID :'.':WS.CUS.NAME
        RETURN
    END

    RETURN
