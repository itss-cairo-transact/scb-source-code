* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LD.CCY.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    XX = O.DATA
    CUR = FIELD(XX,":",1)
    AMO = FIELD(XX,":",2)
    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = '' ; R.CCY = ''
    KEY.CCY = CUR
    CALL OPF(FN.CCY,F.CCY)
************************************************************
    CALL F.READ(FN.CCY,CUR,R.CCY,F.CCY,E1)
    IF CUR EQ 'EGP' THEN
        RATE = '1'
    END ELSE
        IF CUR NE 'JPY' THEN
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
        END ELSE
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1> / 100
        END
    END
    AMT.EG = AMO * RATE
************************************************
    IF CUR NE 'USD' THEN
        CUR1 = 'USD'
        CALL F.READ(FN.CCY,CUR1,R.CCY,F.CCY,E1)
        RATE1 = R.CCY<EB.CUR.MID.REVAL.RATE,1>

        AMT.USD = AMT.EG / RATE1
    END ELSE
        AMT.USD = AMO
    END

    O.DATA = AMT.USD

    RETURN
END
