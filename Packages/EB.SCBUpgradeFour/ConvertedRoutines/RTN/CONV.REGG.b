* @ValidationCode : MjotNTU5MTc0NDIwOkNwMTI1MjoxNjQxMjA1NjExNTcyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 03 Jan 2022 12:26:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*------------------------------------------NI7OOOOOOOOOOOO-----------------------------------
SUBROUTINE CONV.REGG

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE



    IF O.DATA = 999 OR O.DATA = 98 THEN
** TEXT = "GOV: " : O.DATA ; CALL REM
        O.DATA = ' '
** TEXT = "GOV3 : " : O.DATA ; CALL REM

    END ELSE
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,O.DATA,XX)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,O.DATA,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
XX=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
        O.DATA =XX
    END
RETURN
END
