* @ValidationCode : MjotMTIwMjQ2OTg1NjpDcDEyNTI6MTY0MTEwNDY2NzQ2MDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 02 Jan 2022 08:24:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-- CREATE BY NESSMA
SUBROUTINE CONV.GET.COL.2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*-------------------------------------------------
    COD     = ""
    VAR.COL = 0
    TMP.ID  = ""
    TMP.ID  = O.DATA

    IF TMP.ID[5,1] EQ "A" THEN
        COD    = "B"
        TMP.ID = TMP.ID[1,4]:COD:TMP.ID[6,19]
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.LQDDTY.DAILY.FILE':@FM:LQDF.COLUMN.12,TMP.ID,VAR.COL)
F.ITSS.SCB.LQDDTY.DAILY.FILE = 'F.SCB.LQDDTY.DAILY.FILE'
FN.F.ITSS.SCB.LQDDTY.DAILY.FILE = ''
CALL OPF(F.ITSS.SCB.LQDDTY.DAILY.FILE,FN.F.ITSS.SCB.LQDDTY.DAILY.FILE)
CALL F.READ(F.ITSS.SCB.LQDDTY.DAILY.FILE,TMP.ID,R.ITSS.SCB.LQDDTY.DAILY.FILE,FN.F.ITSS.SCB.LQDDTY.DAILY.FILE,ERROR.SCB.LQDDTY.DAILY.FILE)
VAR.COL=R.ITSS.SCB.LQDDTY.DAILY.FILE<LQDF.COLUMN.12>
    END ELSE
        COD = "A"
        TMP.ID = TMP.ID[1,4]:COD:TMP.ID[6,19]
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.LQDDTY.DAILY.FILE':@FM:LQDF.COLUMN.06,TMP.ID,VAR.COL)
F.ITSS.SCB.LQDDTY.DAILY.FILE = 'F.SCB.LQDDTY.DAILY.FILE'
FN.F.ITSS.SCB.LQDDTY.DAILY.FILE = ''
CALL OPF(F.ITSS.SCB.LQDDTY.DAILY.FILE,FN.F.ITSS.SCB.LQDDTY.DAILY.FILE)
CALL F.READ(F.ITSS.SCB.LQDDTY.DAILY.FILE,TMP.ID,R.ITSS.SCB.LQDDTY.DAILY.FILE,FN.F.ITSS.SCB.LQDDTY.DAILY.FILE,ERROR.SCB.LQDDTY.DAILY.FILE)
VAR.COL=R.ITSS.SCB.LQDDTY.DAILY.FILE<LQDF.COLUMN.06>
    END

    O.DATA = VAR.COL

RETURN
END
