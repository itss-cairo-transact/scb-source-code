* @ValidationCode : MjozNzk4NDQ4NDc6Q3AxMjUyOjE2NDExMzY5NDM5NjI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 17:22:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.LOAN.CREDIT.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 32 ] Removed I_CO.LOCAL.REFS Not Used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_CO.LOCAL.REFS
*--------------------------------------------
    COL.ID = O.DATA:'...'

    FN.COL = 'FBNK.COLLATERAL' ; F.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COLR = 'FBNK.COLLATERAL.RIGHT' ; F.COLR = ''
    CALL OPF(FN.COLR,F.COLR)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.SCH = 'FBNK.LD.SCHEDULE.DEFINE' ; F.SCH = ''
    CALL OPF(FN.SCH,F.SCH)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    WS.INT.AMT = 0

    T.SEL = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.COL,KEY.LIST<I>,R.COL,F.COL,E1)
            WS.LD.ID = R.COL<COLL.APPLICATION.ID>

            CALL F.READ(FN.LD,WS.LD.ID,R.LD,F.LD,E3)
            WS.INT.AMT += R.LD<LD.TOT.INTEREST.AMT>

            CALL F.READ(FN.SCH,WS.LD.ID,R.SCH,F.SCH,E4)
            IF NOT(E4) THEN
                WS.INT.DATE = R.SCH<LD.SD.DATE,1>
            END ELSE
                WS.INT.DATE = R.LD<LD.FIN.MAT.DATE>
            END

        NEXT I
    END

    O.DATA  = WS.INT.AMT:'-':WS.INT.DATE

RETURN
