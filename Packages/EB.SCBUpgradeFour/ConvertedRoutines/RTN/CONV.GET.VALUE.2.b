* @ValidationCode : Mjo4NTkzNjc1Mjc6Q3AxMjUyOjE2NDExMTA1ODQ0MTQ6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 10:03:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-- CREATE BY NESSMA
SUBROUTINE CONV.GET.VALUE.2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] removed $INCLUDE I_F.SCB.LQDDTY.DAILY.FILE NOT USED - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*-------------------------------------------------
    O.DATAX = 0
    TMP.ID  = O.DATA
    VAL.A = 0 ; VAL.B = 0  ; VAL.C = 0

    VAL.B = FIELD(TMP.ID , "*" , 1)
    VAL.A = FIELD(TMP.ID , "*" , 2)
    VAL.C = VAL.A + VAL.B

    IF VAL.B LT 0 AND VAL.B NE '' THEN
        O.DATAX = VAL.C
    END ELSE
        O.DATAX = 0
    END

    O.DATA = O.DATAX
RETURN
END
