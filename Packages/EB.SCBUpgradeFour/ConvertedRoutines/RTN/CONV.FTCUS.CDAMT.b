* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.FTCUS.CDAMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

**********************************
    WS.CU.ID      = O.DATA
    WS.LCY.AMT    = 0

    IF WS.CU.ID EQ '' THEN
        RETURN
    END

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ER.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)


    CALL F.READ(FN.IND.LD,WS.CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN

            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
            REC.STA        = R.LD<LD.STATUS>
            IF REC.STA NE 'LIQ' THEN
                WS.GL.ID   = R.LD<LD.CATEGORY>
                IF WS.GL.ID GE 21101 AND WS.GL.ID LE 21103 THEN
                    WS.LD.AMT  += R.LD<LD.AMOUNT>
                END

            END
        END
    REPEAT
    O.DATA = WS.LD.AMT
    RETURN
