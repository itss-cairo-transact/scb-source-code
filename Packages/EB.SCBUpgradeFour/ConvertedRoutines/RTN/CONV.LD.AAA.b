* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LD.AAA


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
    XX = O.DATA
    ID    = FIELD(XX,":",1)
    DRAW  = FIELD(XX,":",2)
    CON   = FIELD(XX,":",3)
    AMT   = FIELD(XX,":",4)
    LIAB  = FIELD(XX,":",5)

*  IF DRAW EQ '0.00' AND CON EQ '' THEN
*      O.DATA = LIAB
*  END
*  IF DRAW NE '0.00'  AND CON EQ '' THEN
*      O.DATA = AMT
*  END
*  IF DRAW NE '0.00' AND CON NE '' THEN
*      O.DATA = CON
*  END
*  IF LIAB NE '0.00' THEN
*     O.DATA = LIAB
*   END
*  IF DRAW NE '0.00' THEN
*      O.DATA = DRAW
*  END
    IF LIAB EQ '0.00' THEN
        O.DATA = DRAW
*        RETURN
    END
    IF DRAW EQ '0.00' THEN
        O.DATA = LIAB
*        RETURN
    END
    IF (DRAW NE '0.00' AND LIAB NE '0.00') AND CON EQ '' AND ID NE 'TF1201126284' THEN
        O.DATA = LIAB + DRAW

        RETURN
    END

    IF (DRAW NE '0.00' AND LIAB NE '0.00') AND CON EQ '' AND ID EQ 'TF1201126284' THEN
         O.DATA = LIAB

RETURN
END



    IF (DRAW NE '0.00' AND LIAB NE '0.00') AND CON NE '' THEN
        O.DATA = CON

        RETURN
    END

    IF (DRAW EQ '0.00' AND LIAB NE '0.00') AND CON NE '' THEN
        O.DATA = CON

        RETURN

    END


************************************************
    RETURN
END
