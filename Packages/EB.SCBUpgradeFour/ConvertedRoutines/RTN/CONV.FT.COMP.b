* @ValidationCode : Mjo2NTgzNDg5NzQ6Q3AxMjUyOjE2NDEwOTg0ODk5MTI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:41:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
****NESSREEN AHMED 21/3/2010***********

SUBROUTINE CONV.FT.COMP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed  $INCLUDE I_F.SCB.VISA.APP NOT USED  - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.VISA.APP

    DC.CO = O.DATA
    USR.CO = FIELD(DC.CO, "*" ,1)
    CR.CO =  FIELD(DC.CO, "*" ,2)
    DB.CO =  FIELD(DC.CO, "*" ,3)
    BR.NO =  FIELD(DC.CO, "*" ,4)

    IF BR.NO # '' THEN
        BR = STR('0',2 - LEN(BR.NO)):BR.NO
        BR.CO = "EG00100":BR
    END

    IF USR.CO = CR.CO THEN
        IF USR.CO = DB.CO THEN
            O.DATA = BR.CO
        END ELSE
            O.DATA = DB.CO
        END
    END ELSE
        O.DATA = CR.CO
    END

**  TEXT = 'O.DATA=':O.DATE ; CALL REM
RETURN
END
