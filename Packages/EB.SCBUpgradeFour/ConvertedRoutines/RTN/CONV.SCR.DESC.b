* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
      SUBROUTINE CONV.SCR.DESC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

      ALL.NAME = ''
      OP.ID = ''
      MODULE.NAME = ''

      ALL.NAME = O.DATA
      X.MODULE.NAME = FIELD(ALL.NAME,',',1)
      OP.ID = FIELD(ALL.NAME,'*',2)
      MODULE.NAME = TRIMB(X.MODULE.NAME)

      O.DATA = ''
BEGIN CASE

   CASE MODULE.NAME EQ 'FUNDS.TRANSFER'
      GOSUB FUNDS.TRANSFER

   CASE MODULE.NAME EQ 'BILL.REGISTER'
      GOSUB BILL.REGISTER

   CASE MODULE.NAME = 'ACCOUNT'
      GOSUB ACCOUNT

   CASE MODULE.NAME = 'CUSTOMER'
      GOSUB CUSTOMER

   CASE MODULE.NAME = 'LD.LOANS.AND.DEPOSITS'
      GOSUB LD.LOANS.AND.DEPOSITS
***********
   CASE  MODULE.NAME = 'TELLER'
      GOSUB TELLER

   CASE  MODULE.NAME = 'CURRENCY'
      GOSUB CURRENCY
 END CASE
      RETURN
************************************************************************
BILL.REGISTER:
      O.DATA = '����� � ��������'
      RETURN

FUNDS.TRANSFER:
      O.DATA = '����� ��� ������'
      RETURN


ACCOUNT:
      O.DATA = '������ �������'
      RETURN

CUSTOMER:
      O.DATA = '������ �������'
      RETURN

LD.LOANS.AND.DEPOSITS:
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*      CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,OP.ID,CAT.NO)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,OP.ID,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
CAT.NO=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>

      IF CAT.NO GE 21001 AND CAT.NO LE 21010 THEN
         O.DATA = '�������'
      END

      IF CAT.NO GE 21020 AND CAT.NO LE 21025 THEN
         O.DATA = '��������'
      END

      IF CAT.NO EQ 21096 THEN
         O.DATA = '������ ������'
      END

      RETURN
TELLER:
      O.DATA = '����� ������'
      RETURN

CURRENCY:
      O.DATA = '�����'
      RETURN
   END
