* @ValidationCode : Mjo3NTcxMjc4OTg6Q3AxMjUyOjE2NDEyMDY4Nzg1MDU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 03 Jan 2022 12:47:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.SEC.DESC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed I_F.SBM.LG.SECTOR Not Used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SBM.LG.SECTOR

    HH  = O.DATA

    BEGIN CASE
        CASE HH = 1
            RR = "���� �����"
        CASE HH = 2
            RR = "���� ����� ��� "
        CASE  HH = 3
            RR =  "���� ���"
        CASE  HH = 4
            RR =   "���� ���"
        CASE  HH = 5
            RR = "������ ����"
        CASE OTHERWISE
            RETURN
    END CASE

    O.DATA = RR
RETURN
END
