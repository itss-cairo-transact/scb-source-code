* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.INIT.APP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MAINMENU
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*----------------------------------------------------------------------**
    COMP = ID.COMPANY
    USE.ID   = ''
    USE.ID   = O.DATA
    FN.USE   = 'F.USER'            ; F.USE    = ''
    CALL OPF(FN.USE,F.USE)
    FN.APP = 'F.HELPTEXT.MAINMENU'  ; F.APP    = ''
    CALL OPF(FN.APP,F.APP)
    NUM.C    = 0
    XX       = ""
    HMM.ID   = ''
***--------------------------------------------------------------****
    CALL F.READ(FN.USE,USE.ID,R.USE,F.USE,E11)
    INIT.APP.NN  = R.USE<EB.USE.INIT.APPLICATION>
    HMM.ID.NN    = FIELD(INIT.APP.NN,'?',2)

    CALL F.READ(FN.APP,HMM.ID.NN,R.APP,F.APP,E12)
    APP.NAM  = R.APP<EB.MME.DESCRIPT>
*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NUM.C    = DCOUNT(APP.NAM,@VM)

    FOR I = 1 TO NUM.C
        XX = XX :" / ":APP.NAM<1,I>
    NEXT I

    O.DATA    = XX
    RETURN
*==============================================================********
END
