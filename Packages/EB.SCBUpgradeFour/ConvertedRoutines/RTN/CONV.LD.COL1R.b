* @ValidationCode : MjotODc1NTQ2NTQ1OkNwMTI1MjoxNjQxMTI1MzQ1OTM3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 14:09:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.LD.COL1R
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 27 ] Removed*$INCLUDE I_CO.LOCAL.REFS not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_CO.LOCAL.REFS

    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
RETURN
INIT:
    XX = O.DATA
    FN.COL = "F.COLLATERAL"
    F.COL  = ''
    R.COL  = ''
    COLL = 0

    FN.COLR = "F.COLLATERAL.RIGHT"
    F.COLR  = ''
    R.COLR  = ''

RETURN

OPEN:
    CALL OPF(FN.COL,F.COL)
    CALL OPF(FN.COLR,F.COLR)
RETURN

PROCESS:
    T.SEL = "SELECT ":FN.COLR: " WITH LIMIT.REFERENCE EQ ":XX
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            T.SEL1 = "SELECT ":FN.COL: " WITH @ID LIKE ":KEY.LIST<I>:"..."
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            CALL F.READ(FN.COL,KEY.LIST1,R.COL,F.COL,EER)
            COLL = R.COL<COLL.CURRENCY>

        NEXT I
        O.DATA = COLL
    END
RETURN
END
