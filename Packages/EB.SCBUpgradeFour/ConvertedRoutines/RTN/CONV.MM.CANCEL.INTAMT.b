* @ValidationCode : MjoxODUzNTE2NjA1OkNwMTI1MjoxNjQxMTM4MTc5MjYwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 17:42:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.MM.CANCEL.INTAMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_MM.LOCAL.REFS

    XX = O.DATA
    FN.MM = 'FBNK.MM.MONEY.MARKET'
    F.MM = '' ; R.MM = ''
    CALL OPF(FN.MM,F.MM)

    CALL F.READ(FN.MM,XX,R.MM,F.MM,E1)

    NEW.RATE    = R.MM<MM.NEW.INTEREST.RATE>
    OLD.INT.AMT = R.MM<MM.TOT.INTEREST.AMT>
    NEW.INT.AMT = R.MM<MM.NEXT.INT.AMOUNT>

    IF NEW.RATE NE '' THEN
        AMT1 = NEW.INT.AMT
    END ELSE
        AMT1 = OLD.INT.AMT
    END

    IND = R.MM<MM.LOCAL.REF><1,MMLR.CANCEL.IND>
    IF IND EQ 'Y' THEN
        AMT = AMT1
    END ELSE
        AMT = 0
    END
    O.DATA = AMT

RETURN
