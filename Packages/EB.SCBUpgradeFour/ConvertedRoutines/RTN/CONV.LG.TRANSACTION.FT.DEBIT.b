* @ValidationCode : MjotMTI1ODI4NzA2ODpDcDEyNTI6MTY0MjQxODc5MzY3MTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 17 Jan 2022 13:26:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.LG.TRANSACTION.FT.DEBIT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] ADDED I_F.SCB.LG.CHARGE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE
    ID.NO=FIELD(O.DATA,".",1)
    ID.NO=ID.NO:'...'

    T.SEL= "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK LIKE ": ID.NO

*T.SEL    = "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK EQ ": O.DATA
*T.SEL    = "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK LIKE ": O.DATA :"..."

    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FN.FUNDS ='FBNK.FUNDS.TRANSFER' ;R.FUNDS='';F.FUNDS=''
    CALL OPF(FN.FUNDS,F.FUNDS)

    FOR I = 1 TO SELECTED

        CALL F.READ(FN.FUNDS,KEY.LIST<I>,R.FUNDS,F.FUNDS,E)
        ORDER.BANK = R.FUNDS<FT.ORDERING.BANK>
        SAM =FIELD(ORDER.BANK,".",2)
        SAM1=FIELD(ORDER.BANK,".",3)
        SAM2=SAM:'.':SAM1
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,SAM2,DESC1)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,SAM2,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
DESC1=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.DESCRIPTION>

        ACCT= R.FUNDS<FT.CREDIT.ACCT.NO>
        IF NOT(NUM(ACCT)) THEN
            O.DATA<1,I> = "-":R.FUNDS<FT.DEBIT.AMOUNT>:" ":DESC1
        END

    NEXT I

*SAM = DCOUNT(O.DATA<1,I>, "|")
*
*FOR I = 1 TO SAM
*
*I =
*
*NEXT

RETURN
END
