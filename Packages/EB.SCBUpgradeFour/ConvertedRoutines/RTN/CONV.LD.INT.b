* @ValidationCode : MjotMTQ3NTg5NzIyMzpDcDEyNTI6MTY0ODQ4NTE5MTEzMzpNb3VuaXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Mar 2022 18:33:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE

*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.LD.INT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS

*    T.SEL = "SELECT FBNK.BASIC.INTEREST WITH @ID LIKE ":O.DATA:"..."
*   KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*  CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*   * CALL DBR("BASIC.INTEREST":@FM:1,KEY.LIST<SELECTED>,RATE)
    F.ITSS.BASIC.INTEREST = 'F.BASIC.INTEREST'
    FN.F.ITSS.BASIC.INTEREST = ''
    CALL OPF(F.ITSS.BASIC.INTEREST,FN.F.ITSS.BASIC.INTEREST)
    CALL F.READ(F.ITSS.BASIC.INTEREST,KEY.LIST,R.ITSS.BASIC.INTEREST,FN.F.ITSS.BASIC.INTEREST,ERROR.BASIC.INTEREST)
    RATE=R.ITSS.BASIC.INTEREST<1>
    O.DATA = O.DATA :" %"

RETURN
END
