* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
***************************CREATED BY MAHMOUD MAGDY 2018/06/25
    SUBROUTINE CONV.NEXT.UPDATE
*    PROGRAM CONV.NEXT.UPDATE

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    CUS.ID = O.DATA

    FN.CUS = 'FBNK.CUSTOMER'
    F.CUS =''
    UPDATE.DATE = ''
    QLTY.RATE = ''
    YEAR.DAYS = 365

    CALL OPF(FN.CUS,F.CUS)
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,E1)
    CRT 'PASSED'
    UPDATE.DATE = R.CUS<EB.CUS.LOCAL.REF,CULR.UPDATE.DATE>
    QLTY.RATE = R.CUS<EB.CUS.LOCAL.REF,CULR.QLTY.RATE>

    IF UPDATE.DATE NE '' AND QLTY.RATE NE '' THEN
        IF QLTY.RATE EQ 1  OR QLTY.RATE EQ 2 THEN
            QLTY.DAYS = 4 * YEAR.DAYS
        END
        ELSE IF QLTY.RATE EQ 3 THEN
            QLTY.DAYS = 2 * YEAR.DAYS
        END
        QLTY.FMT = "+":QLTY.DAYS:"C"
        CRT 'QLTY FORMAT ':QLTY.FMT
        CALL CDT("",UPDATE.DATE,QLTY.FMT)
        CRT 'QLTY DAYS ':QLTY.DAYS
        CRT 'NEXT UPDATE DATE ':UPDATE.DATE
    END
    ELSE
        O.DATA = ''
    END
    UPDATE.DATE = UPDATE.DATE[1,4]:'/':UPDATE.DATE[5,2]:'/':UPDATE.DATE[7,2]
    O.DATA = UPDATE.DATE

END
