* @ValidationCode : MjotNzY0NzgzODQ1OkNwMTI1MjoxNjQxMTA1ODk2NjA3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 08:44:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.GET.GATEGORY.TXT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed $INCLUDE I_F.SCB.CUS.POS NOT USED - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CUS.POS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY

*-----------------------------------------------------
**TEXT = "O.DATA1 " : O.DATA ; CALL REM
    CATEG.ID  = O.DATA
    FLG = ''
*--------
    IF CATEG.ID EQ '21096' THEN

* CATEG.TXT = "**����**"
        CATEG.TXT = -1
        FLG = 1
    END

    IF (CATEG.ID GE '23100' AND CATEG.ID LE '23202' ) THEN
*  CATEG.TXT = "**����**"
        CATEG.TXT = -1
        FLG = 1
    END
    IF (CATEG.ID GE '23300' AND CATEG.ID LE '23600' ) THEN
* CATEG.TXT = "**����**"
        CATEG.TXT = 1
        FLG = 1
    END
*--------

    IF FLG EQ 1 THEN
        O.DATA = CATEG.TXT
    END ELSE
        O.DATA = 1
    END
*-----------------------------------------------------

RETURN
END
