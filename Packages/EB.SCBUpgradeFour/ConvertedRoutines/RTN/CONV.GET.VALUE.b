* @ValidationCode : MjozOTc5OTU4ODE6Q3AxMjUyOjE2NDExMTA3OTg2MzI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 10:06:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-- CREATE BY NESSMA
SUBROUTINE CONV.GET.VALUE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] RemovedI_F.SCB.LQDDTY.DAILY.FILE NOT USED - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*-------------------------------------------------
    TMP.ID  = O.DATA
    VAL.A = "" ; VAL.B = ""  ; VAL.C = ""

    VAL.A = FIELD(TMP.ID , "*" , 1)
    VAL.B = FIELD(TMP.ID , "*" , 2)
    VAL.C = VAL.A + VAL.B

    IF VAL.A EQ '' THEN
        O.DATA = 0
    END
    IF VAL.A LT 0 THEN
        O.DATA = VAL.C
    END ELSE
        IF VAL.A GE 0 THEN
            O.DATA = 0
        END

        RETURN
    END
