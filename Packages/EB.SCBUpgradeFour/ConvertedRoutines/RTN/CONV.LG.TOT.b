* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LG.TOT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    CUST.ID=O.DATA
    MARKET='1'

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CUSTOMER.ID EQ ": CUST.ID :" AND CATEGORY GE ": " 21096" :" AND LE ": " 21097"
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '';R.LD=''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
        CURR=R.LD<LD.CURRENCY>
        AMT=R.LD<LD.AMOUNT>
        LOCAL.REF=R.LD<LD.LOCAL.REF>
        MARG.AMT=LOCAL.REF<1,LDLR.MARGIN.AMT>

        IF CURR NE LCCY THEN
            CALL MIDDLE.RATE.CONV.CHECK(AMT,CURR,RATE,MARKET,EQUAMT,DIF.AMT,DIF.RATE)

            CALL MIDDLE.RATE.CONV.CHECK(MARG.AMT,CURR,RATE,MARKET,EQUMARG,DIF.AMT,DIF.RATE)
            TOT.AMT = TOT.AMT+EQUAMT
            TOT.MARG = TOT.MARG+EQUMARG

            EQUAMT = ''
            EQUMARG=''
        END
        ELSE
            IF CURR EQ LCCY THEN
                TOT.AMTL = TOT.AMTL+AMT
                TOT.MARGL=TOT.MARGL+MARG.AMT
            END
        END
    NEXT I
* TOT<1,10>=TOT.AMT+TOT.AMTL

*    TOT<1,5>=TOT.MARG+TOT.MARGL

    TOT= TOT.AMT+TOT.AMTL
    O.DATA=TOT
    RETURN
END
