* @ValidationCode : MjoxMTEyOTMzNTIyOkNwMTI1MjoxNjQyNDE5MTI3NDg3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Jan 2022 13:32:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
PROGRAM CONV.RISK.N.ALL.NEW
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CATEGORY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.LETTER.OF.CREDIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.MM.MONEY.MARKET
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.RE.STAT.LINE.BAL
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COMPANY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DEPT.ACCT.OFFICER
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.RE.BASE.CCY.PARAM
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.SCB.FUND
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_FT.LOCAL.REFS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_AC.LOCAL.REFS
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_CU.LOCAL.REFS
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_LD.LOCAL.REFS
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_LC.LOCAL.REFS
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.SCB.CUS.POS.TST
*Line [ 67 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.SCB.CREDIT.CBE
*Line [ 69 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.SBD.CURRENCY
*------------------------------------------------------

    DATE.1 = TODAY
    NK     = ""
*=============================
    OPENSEQ "/home/signat" , "ledg_bal1c.ALL":".CSV" TO BB THEN
*    OPENSEQ "&SAVEDLISTS&"  , "ledg_bal1c.ALL":".CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"ledg_bal1c.ALL":".CSV"
        HUSH OFF
    END

    OPENSEQ "/home/signat" , "ledg_bal1c.ALL":".CSV" TO BB ELSE
*    OPENSEQ "&SAVEDLISTS&"  , "ledg_bal1c.ALL":".CSV" TO BB THEN
        CREATE BB THEN
        END ELSE
        END
    END

    BB.DATA1  =    ",,"
    BB.DATA1 :=    ",���������"
    BB.DATA1 :=    " �������"
    BB.DATA1 :=    ",�������"
    BB.DATA1 :=    " �������"
    BB.DATA1 :=    ",�������"
    BB.DATA1 :=    " �������"
    BB.DATA1 :=    ",���������"
    BB.DATA1 :=    " �������"
    BB.DATA1 :=    ",������"
    BB.DATA1 :=    " ������"
    BB.DATA1 :=    ",����� ������"
    WRITESEQ BB.DATA1 TO BB ELSE

        PRINT " ERROR WRITE FILE "
    END
    BB.DATA1 = ''
    BB.DATA1 =      "�����"
    BB.DATA1 :=     ",��� ������"
    BB.DATA1 :=     ",��� ������"
    BB.DATA1 :=     ",����"
    BB.DATA1 :=     ",����"
    BB.DATA1 :=     ",����"
    BB.DATA1 :=     ",����"
    BB.DATA1 :=     ",����"
    WRITESEQ BB.DATA1 TO BB ELSE

        PRINT " ERROR WRITE FILE "
    END
*-----------------------------------------------------------------------
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    FN.CUS.POS='F.SCB.CUS.POS.TST'; F.CP=''
    FN.AC6 = 'FBNK.ACCOUNT' ; F.AC6 = '' ; R.AC6 = ''
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; R.AF='' ;F.AF=''
    FN.ACCT.OFF='F.DEPT.ACCT.OFFICER' ; F.ACCT.OFF = '' ; R.ACCT.OFF = ''

    FN.BASE = 'F.SBD.CURRENCY' ; F.BASE = ''

    OLD.ACC = '' ; OLD.LEDG = '' ; OLD.CUR = ''
    TOT.AWAED=0
    TOT.DAMAN=0
    TOTALD=0
    TOT.LG=0
    TOT.LC=0
    TOTALDR=0
    TOTALTMLG=0
    TOTALTMLC=0

    CALL OPF( FN.AC,F.AC)
    CALL OPF( FN.AC6,F.AC6)
    CALL OPF( FN.CU,F.CU)
    CALL OPF( FN.CUS.POS,F.CUS.POS)
    CALL OPF( FN.BASE,F.BASE)
*=============================
    FN.CO = "F.COMPANY" ; F.CO = ""
    CALL OPF(FN.CO, F.CO)

    NN.SEL = "SELECT F.COMPANY WITH @ID NE EG0010088 BY @ID"
    CALL EB.READLIST(NN.SEL,NN.LIST,"",SELECTED.NN,ER.MSG.NN)
    FOR NN = 1 TO SELECTED.NN
        COMP.N = NN.LIST<NN>
        PRINT COMP.N
*Line [ 153 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.N,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.N,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*        NK<1,NN> = NN.LIST<NN>[2]
*------------------------------------------------------

*----------------------------------------------------------------
        GOSUB GETACC
    NEXT NN
RETURN
**********************************
GETACC:
*-----
    CCC.NO = ''
    T.SEL = "SELECT ":FN.CUS.POS:" WITH SYS.DATE EQ ":DATE.1:" AND CO.CODE EQ ":COMP.N
    T.SEL := " AND (( CATEGORY GE 1101 AND CATEGORY LE 1699"
    T.SEL := " AND LCY.AMOUNT LT 0)"
    T.SEL := " OR ( CATEGORY GE 1001 AND CATEGORY LE 1007 AND"
    T.SEL := " LCY.AMOUNT LT 0 )"
    T.SEL := " OR ( CATEGORY GE 1220 AND CATEGORY LE 1227 AND"
    T.SEL := " LCY.AMOUNT LT 0 )"
    T.SEL := " OR ( CATEGORY EQ 2003 AND LCY.AMOUNT LT 0 )"
    T.SEL := " OR ( CATEGORY EQ 21096 AND LCY.AMOUNT NE 0 )"
    T.SEL := " OR ( CATEGORY EQ 3201 AND LCY.AMOUNT LT 0 )"
    T.SEL := " OR ( CATEGORY GE 21050 AND CATEGORY LE 21074 AND LCY.AMOUNT NE 0 )"
    T.SEL := " OR ( CATEGORY EQ 23100 OR CATEGORY EQ 23102 OR CATEGORY EQ 23103"
    T.SEL := " OR CATEGORY EQ 23152 OR CATEGORY EQ 23153 OR CATEGORY EQ 23155 "
    T.SEL := " OR CATEGORY EQ 23553 OR CATEGORY EQ 23150 OR CATEGORY EQ 23155 OR CATEGORY EQ 23202 ) AND LCY.AMOUNT NE 0"
    T.SEL := " OR ( CATEGORY EQ 3005 OR CATEGORY EQ 3017 OR CATEGORY EQ 3010 OR CATEGORY"
    T.SEL := " EQ 3011 OR CATEGORY EQ 3012 OR CATEGORY EQ 3013 )"
    T.SEL := " OR ( CATEGORY EQ 9090 AND LCY.AMOUNT NE 0 ) OR ( CATEGORY EQ 9091 AND LCY.AMOUNT NE 0 ) )"
    T.SEL := " AND CATEGORY NE 23200"
    T.SEL := " AND CATEGORY NE 23552"
    T.SEL := " AND CUSTOMER NE '' "
    T.SEL := " BY CUSTOMER BY CATEGORY"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN

        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CUS.POS,KEY.LIST<I>,R.CUS.POS,F.CUS.POS,E1)
            CUS.ID = R.CUS.POS<CUPOS.CUSTOMER>


            IF I EQ 1 THEN CCC.NO = CUS.ID

            IF CCC.NO # CUS.ID THEN
                GOSUB PRINTDATA
            END

            CURR = R.CUS.POS<CUPOS.DEAL.CCY>
            CALL F.READ(FN.BASE,CURR,R.BASE,F.BASE,E3)

            IF R.CUS.POS<CUPOS.LCY.AMOUNT> LT 0 THEN
                IF CURR = 'EGP' THEN
                    TOTAL = R.CUS.POS<CUPOS.LCY.AMOUNT> * -1
                END ELSE
                    IF CURR EQ 'JPY' THEN
                        TOTAL = R.CUS.POS<CUPOS.DEAL.AMOUNT> * -1
                        RATE  = R.BASE<SBD.CURR.MID.RATE,1> / 100
                        TOTAL = TOTAL * RATE
                    END ELSE

                        TOTAL = R.CUS.POS<CUPOS.DEAL.AMOUNT> * -1
                        RATE  = R.BASE<SBD.CURR.MID.RATE,1>
*PRINT CURR:"== ":RATE
                        TOTAL = TOTAL * RATE
                    END
                END
            END ELSE
                IF R.CUS.POS<CUPOS.DEAL.CCY> = 'EGP' THEN
                    TOTAL = R.CUS.POS<CUPOS.LCY.AMOUNT>
                END ELSE
                    IF CURR EQ 'JPY' THEN
                        TOTAL = R.CUS.POS<CUPOS.DEAL.AMOUNT>
                        RATE  = R.BASE<SBD.CURR.MID.RATE,1> / 100
                        TOTAL = TOTAL * RATE
                    END ELSE
                        TOTAL = R.CUS.POS<CUPOS.DEAL.AMOUNT>
                        RATE  = R.BASE<SBD.CURR.MID.RATE,1>
                        TOTAL = TOTAL * RATE
                    END
                END
            END

            TOTAL = DROUND(TOTAL,0)

            CALL F.READ( FN.CU,CUS.ID,R.CU, F.CU, CUSERR)
            CUS.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            OLD.ACC    = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
            CUS.BRANCH = R.CU<EB.CUS.CO.CODE>
*Line [ 250 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,CUS.BRANCH,BRAN.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CUS.BRANCH,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRAN.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
***************************** noha 27/11/2008***************
            CAT.CASE = R.CUS.POS<CUPOS.CATEGORY>
            BEGIN CASE
******* ����� ������
                CASE CAT.CASE=9090 OR CAT.CASE=9091
                    TOT.AWAED +=TOTAL
******* LETTER OF CREDIT
                CASE CAT.CASE=23100 OR CAT.CASE=23102 OR CAT.CASE=23103 OR CAT.CASE=23152 OR CAT.CASE=23155 OR CAT.CASE=23553 OR CAT.CASE=23202 OR CAT.CASE EQ 23150 OR CAT.CASE EQ 23153 OR CAT.CASE=23155
                    TOT.LC +=TOTAL
******* LETTER OF GRANTE
                CASE CAT.CASE=21096
                    TOT.LG +=TOTAL
******* MARGIN LETTER OF GRANTE
                CASE CAT.CASE=3005
                    TOTALTMLG = TOTALTMLG + TOTAL
******* MARGINE LETTER OF CREDIT
                CASE CAT.CASE=3010 OR CAT.CASE=3011 OR CAT.CASE=3012 OR CAT.CASE=3013 OR CAT.CASE=3017
                    TOTALTMLC = TOTALTMLC + TOTAL
******* CAT 21001 AND 21010
                CASE CAT.CASE GE 21001 AND CAT.CASE LE 21010
                    TOTALD +=TOTAL
******* ���� ����
                CASE  OTHERWISE
                    TOTALDR += TOTAL
            END CASE
****************************
            CCC.NO = CUS.ID
****************************
        NEXT I

        IF I = SELECTED THEN GOSUB PRINTDATA
    END
RETURN
**************************************************************************
PRINTDATA:
*--------

    LETTER=TOT.LC+TOT.LG
    MARGIN=TOTALTMLG+TOTALTMLC
    BB.DATA  = BRANCH:","     ;*BRAN.NAME:","
    IF TOT.AWAED THEN TOTALDR += TOT.AWAED
    BB.DATA := CCC.NO:","
    BB.DATA := CUS.NAME:","
    BB.DATA := TOTALDR:","
    BB.DATA := TOT.AWAED:","
    BB.DATA := TOTALD:","
    BB.DATA := LETTER:","
    BB.DATA := MARGIN:","
    BB.DATA := OLD.ACC:","

    WRITESEQ BB.DATA TO BB ELSE

        PRINT " ERROR WRITE FILE "
    END
    TOT.AWAED=0
    TOT.DAMAN=0
    TOTALD=0
    TOT.LG=0
    TOT.LC=0
    TOTALDR=0
    TOTALTMLG=0
    TOTALTMLC=0
RETURN
END
