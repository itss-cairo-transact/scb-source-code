* @ValidationCode : MjoxNTE0NzI0NTEyOkNwMTI1MjoxNjQxMTA0ODI3MDAxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 08:27:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
***** CREATED BY MOHAMED SABRY **************
*-----------------------------------------------------------------------------
* <Rating>-44</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.GET.CR.COL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
    FN.CCY = 'FBNK.CURRENCY'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
RETURN
INIT:
*    FN.CUST.COL = "FBNK.CUSTOMER.COLLATERAL"   ; F.CUST.COL = ""
    FN.CUST.COL = "FBNK.COLLATERAL.RIGHT.CUST"   ; F.CUST.COL = ""
    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    WS.FINAL.COL.AMT =0
    LIMIT.ID  =  O.DATA
    FN.COL   = "FBNK.COLLATERAL"
    FN.COL.R = "FBNK.COLLATERAL.RIGHT"
    F.COL  = '' ; R.COL  = '' ; COLL = 0 ; X.COLL = ''
    F.COL.R  = '' ; R.COL.R  = ''
    T.SEL = ''   ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    T.SEL.C.R='' ; KEY.LIST.C.R = ''  ;  SELECTED.C.R = ''  ;  ER.MSG.C.R = ''
    T.SEL.R = ''  ; KEY.LIST.R = ''  ;  SELECTED.R = ''  ;  ER.MSG.R = ''
    FN.LIM = 'FBNK.LIMIT'
    F.LIM = '' ; R.LIM = ''
RETURN
OPEN:
    CALL OPF(FN.COL,F.COL)
    CALL OPF(FN.COL.R,F.COL.R)
    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.CUST.COL,F.CUST.COL)
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)
RETURN
PROCESS:
************************************************************

*----------------------------- COLLATERAL.RIGHT --------------------------
    WS.CUST.NO = FIELD(LIMIT.ID,'.',1)
    CALL F.READ(FN.CUST.COL,WS.CUST.NO,R.CUST.COL,F.CUST.COL,ER.CUST.COL)
    LOOP
        REMOVE CUST.COL.ID FROM R.CUST.COL SETTING POS.CUST.COL
    WHILE CUST.COL.ID:POS.CUST.COL
        CALL F.READ(FN.COL.R,CUST.COL.ID,R.COL.R,F.COL.R,EER.R)
        WS.LIMIT.NO = DCOUNT(R.COL.R<COLL.RIGHT.LIMIT.REFERENCE>,@VM)
        FOR X = 1 TO WS.LIMIT.NO
            WS.LI.ID.C.R =  R.COL.R<COLL.RIGHT.LIMIT.REFERENCE,X>
            IF WS.LI.ID.C.R EQ LIMIT.ID THEN
                WS.COL.ID =  CUST.COL.ID
                WS.PRC =  R.COL.R<COLL.RIGHT.PERCENT.ALLOC,X>
                WS.PRC.MIA = WS.PRC / 100
                GOSUB  SUB.GET.COL.AMT
                WS.FINAL.COL.AMT +=  COL.AMT.P  * WS.PRC.MIA
            END
        NEXT X
    REPEAT
    O.DATA = WS.FINAL.COL.AMT
RETURN

*---------------------------------------------------------------------------
SUB.GET.COL.AMT:
    COL.AMT.P = 0
    CALL F.READ(FN.RIGHT.COL,WS.COL.ID,R.RIGHT.COL,F.RIGHT.COL,ER.RIGHT.COL)
    LOOP
        REMOVE RIGHT.COL.ID FROM R.RIGHT.COL SETTING POS.RIGHT.COL
    WHILE RIGHT.COL.ID:POS.RIGHT.COL
        CALL F.READ(FN.COL,RIGHT.COL.ID,R.COL,F.COL,EER)
        CY.CODE =  R.COL<COLL.CURRENCY>
        WS.APP.ID  =  R.COL<COLL.APPLICATION.ID>[1,2]
        WS.EXP.DATE=  R.COL<COLL.EXPIRY.DATE>
        COL.AMT =  R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
        IF WS.APP.ID = 'LD' AND WS.EXP.DATE GE TODAY THEN
            IF CY.CODE NE 'EGP' THEN
                CALL F.READ(FN.CCY,CY.CODE,R.CCY,F.CCY,E1)
                RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
                WS.COL.AMT = RATE * COL.AMT
            END ELSE
                WS.COL.AMT = 1 * COL.AMT
            END
        END ELSE
            IF CY.CODE NE 'EGP' THEN
                CALL F.READ(FN.CCY,CY.CODE,R.CCY,F.CCY,E1)
                RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
                WS.COL.AMT = RATE * COL.AMT
            END ELSE
                WS.COL.AMT = 1 * COL.AMT
            END
        END
        COL.AMT.P += WS.COL.AMT
    REPEAT
RETURN
END
