* @ValidationCode : MjotMjkwODQxMjk3OkNwMTI1MjoxNjQxMDk5Njk1NzU2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 07:01:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>6474</Rating>
*-----------------------------------------------------------------------------
**    PROGRAM CONV.GENLEDALL.BRANCH.EGP
SUBROUTINE CONV.GENLEDALL.BRANCH.EGP
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LINE.GENLEDALL
    COMP = ID.COMPANY
*-----------------------------------------------------------------------

    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.BRANCH.EGP" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"GENLEDALL.BRANCH.EGP"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.BRANCH.EGP" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE GENLEDALL.BRANCH.EGP CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create GENLEDALL.BRANCH.EGP File IN &SAVEDLISTS&'
        END
    END

*----------------------------------------------------------------

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = '' ; R.LN = ''
    FN.BAL = 'F.RE.STAT.LINE.BAL' ; F.BAL = '' ; R.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''
    FN.SLN = 'F.SCB.LINE.GENLEDALL' ; F.SLN = '' ; R.SLN = ''

    CALL OPF(FN.LN,F.LN) ; CALL OPF(FN.BAL,F.BAL) ; CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.SLN,F.SLN)

    DAT.ID = COMP
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD1=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

*    TD1 = '20100504'

    KK = 'EG00100'
    NK = ''
    NK<1,1>='01'  ; NK<1,2>='02'  ; NK<1,3>='03'  ; NK<1,4>='04'  ; NK<1,5>='05'
    NK<1,6>='06'  ; NK<1,7>='07'  ; NK<1,8>='09'  ; NK<1,9>='10'  ; NK<1,10>='11'
    NK<1,11>='12' ; NK<1,12>='13' ; NK<1,13>='14' ; NK<1,14>='15' ; NK<1,15>='16' ; NK<1,16>='20'
    NK<1,17>='21' ; NK<1,18>='22' ; NK<1,19>='23' ; NK<1,20>='30' ; NK<1,21>='31'
    NK<1,22>='32' ; NK<1,23>='35' ; NK<1,24>='40' ; NK<1,25>='50'
    NK<1,26>='51' ; NK<1,27>='60'
    NK<1,28>='70' ; NK<1,29>='80' ; NK<1,30>='81'
    NK<1,31>='90' ; NK<1,32>='99'

    LINE.PRINT = ''
    LINE.BR    = ''
    HEAD.DESC  = "������":"|"
    HEAD.DESC := "�.�����":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "�.�������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "���������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "�.���":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "����� ����":"|"
    HEAD.DESC := "�.6 ������":"|"
    HEAD.DESC := "����� 6 ������":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "����� �����":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "���������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "����� ������":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "������ �� �����":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "������ ������":"|"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    GOSUB PROCESS.ASST
    GOSUB PROCESS.PRFT
    GOSUB PROCESS.CONT
RETURN

*--------------------------------------------------------------------
PROCESS.ASST:

    T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENLEDALL.0005 AND @ID LT GENLEDALL.0580 ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 149 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            CLOSE.BAL.LCL  = 0
            CLOSE.BAL  = 0
            OLD.BRN = ''
            NEW.BRN = ''
*************************************************************************
            FOR C = 1 TO 32
                CLOSE.BAL.LCL  = 0
                FOR X = 1 TO POS
                    CUR = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                    IF CUR EQ 'EGP' THEN
                        BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":KK:NK<1,C>
                        CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                        NEW.BRN = KK:NK<1,C>
                        OLD.BRN = NEW.BRN
                        IF NEW.BRN NE OLD.BRN THEN
                            CLOSE.BAL.LCL = 0
                        END

                        IF NEW.BRN EQ OLD.BRN THEN
                            CLOSE.BAL.LCL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                        END

                        LINE.PRINT<1,C>   = KK:NK<1,C>
                        LINE.PRINT<1,C+1> = CLOSE.BAL.LCL

                        IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                            LINE.BR<1,1> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                            LINE.BR<1,2> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                            LINE.BR<1,3> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                            LINE.BR<1,4> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                            LINE.BR<1,5> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                            LINE.BR<1,6> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                            LINE.BR<1,7> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                            LINE.BR<1,8> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                            LINE.BR<1,9> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                            LINE.BR<1,10> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                            LINE.BR<1,11> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                            LINE.BR<1,12> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                            LINE.BR<1,13> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                            LINE.BR<1,14> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                            LINE.BR<1,15> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                            LINE.BR<1,16> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                            LINE.BR<1,17> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                            LINE.BR<1,18> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                            LINE.BR<1,19> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                            LINE.BR<1,20> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                            LINE.BR<1,21> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                            LINE.BR<1,22> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                            LINE.BR<1,23> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                            LINE.BR<1,24> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                            LINE.BR<1,25> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                            LINE.BR<1,26> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                            LINE.BR<1,27> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                            LINE.BR<1,28> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                            LINE.BR<1,29> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                            LINE.BR<1,30> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                            LINE.BR<1,31> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                            LINE.BR<1,32> = LINE.PRINT<1,C+1>
                        END

                    END
                NEXT X
            NEXT C

            BB.DATA  = DESC:"|"
            BB.DATA := LINE.BR<1,1>:"|"
            BB.DATA := LINE.BR<1,2>:"|"
            BB.DATA := LINE.BR<1,3>:"|"
            BB.DATA := LINE.BR<1,4>:"|"
            BB.DATA := LINE.BR<1,5>:"|"
            BB.DATA := LINE.BR<1,6>:"|"
            BB.DATA := LINE.BR<1,7>:"|"
            BB.DATA := LINE.BR<1,8>:"|"
            BB.DATA := LINE.BR<1,9>:"|"
            BB.DATA := LINE.BR<1,10>:"|"
            BB.DATA := LINE.BR<1,11>:"|"
            BB.DATA := LINE.BR<1,12>:"|"
            BB.DATA := LINE.BR<1,13>:"|"
            BB.DATA := LINE.BR<1,14>:"|"
            BB.DATA := LINE.BR<1,15>:"|"
            BB.DATA := LINE.BR<1,16>:"|"
            BB.DATA := LINE.BR<1,17>:"|"
            BB.DATA := LINE.BR<1,18>:"|"
            BB.DATA := LINE.BR<1,19>:"|"
            BB.DATA := LINE.BR<1,20>:"|"
            BB.DATA := LINE.BR<1,21>:"|"
            BB.DATA := LINE.BR<1,22>:"|"
            BB.DATA := LINE.BR<1,23>:"|"
            BB.DATA := LINE.BR<1,24>:"|"
            BB.DATA := LINE.BR<1,25>:"|"
            BB.DATA := LINE.BR<1,26>:"|"
            BB.DATA := LINE.BR<1,27>:"|"
            BB.DATA := LINE.BR<1,28>:"|"
            BB.DATA := LINE.BR<1,29>:"|"
            BB.DATA := LINE.BR<1,30>:"|"
            BB.DATA := LINE.BR<1,31>:"|"
            BB.DATA := LINE.BR<1,32>:"|"

            FOR AK = 1 TO 32
                LINE.BR<1,33> += LINE.BR<1,AK>
            NEXT AK

            BB.DATA := LINE.BR<1,33>:"|"


            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            LINE.BR<1,33> = 0
        NEXT I

    END
RETURN
*--------------------------------------------------------
PROCESS.PRFT:
    T.SEL1 = "SELECT F.SCB.LINE.GENLEDALL"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    FOR XX = 1 TO SELECTED1
        CALL F.READ(FN.SLN,XX,R.SLN,F.SLN,E2)
        EGP.ID = R.SLN<LALL.EGP.ID>
        FCY.ID = R.SLN<LALL.FCY.ID>

        CALL F.READ(FN.LN,EGP.ID,R.LN,F.LN,E4)

        DESC = R.LN<RE.SRL.DESC>
        DESC.ID.EGP = FIELD(EGP.ID,".",2)
        DESC.ID.FCY = FIELD(FCY.ID,".",2)
        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
        CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 373 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        POS = DCOUNT(CCURR,@VM)
        CLOSE.BAL.LCL  = 0
        CLOSE.BAL.LCL2 = 0
        CLOSE.BAL  = 0
        CLOSE.BAL2 = 0
        NEW.CUR = ''
        OLD.BRN = ''
        NEW.BRN = ''
*************************************************************************
        FOR C = 1 TO 32
            CLOSE.BAL.LCL  = 0
            FOR X = 1 TO POS
                CUR.ID = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                IF CUR.ID EQ 'EGP' THEN
                    BAL.ID2  = FIELD(EGP.ID,".",1):"-":DESC.ID.EGP:"-":"LOCAL":"-":TD1:"*":KK:NK<1,C>
                    CALL F.READ(FN.BAL,BAL.ID2,R.BAL,F.BAL,E2)
                    CLOSE.BAL.LCL   += R.BAL<RE.SLB.CLOSING.BAL.LCL>

                    NEW.BRN = KK:NK<1,C>
                    OLD.BRN = NEW.BRN
                    IF NEW.BRN NE OLD.BRN THEN
                        CLOSE.BAL.LCL = 0
                    END

                    IF NEW.BRN EQ OLD.BRN THEN
                        CLOSE.BAL.LCL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                    END

                    LINE.PRINT<1,C>   = KK:NK<1,C>
                    LINE.PRINT<1,C+1> = CLOSE.BAL.LCL

                    IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                        LINE.BR<1,1> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                        LINE.BR<1,2> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                        LINE.BR<1,3> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                        LINE.BR<1,4> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                        LINE.BR<1,5> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                        LINE.BR<1,6> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                        LINE.BR<1,7> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                        LINE.BR<1,8> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                        LINE.BR<1,9> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                        LINE.BR<1,10> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                        LINE.BR<1,11> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                        LINE.BR<1,12> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                        LINE.BR<1,13> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                        LINE.BR<1,14> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                        LINE.BR<1,15> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                        LINE.BR<1,16> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                        LINE.BR<1,17> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                        LINE.BR<1,18> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                        LINE.BR<1,19> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                        LINE.BR<1,20> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                        LINE.BR<1,21> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                        LINE.BR<1,22> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                        LINE.BR<1,23> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                        LINE.BR<1,24> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                        LINE.BR<1,25> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                        LINE.BR<1,26> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                        LINE.BR<1,27> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                        LINE.BR<1,28> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                        LINE.BR<1,29> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                        LINE.BR<1,30> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                        LINE.BR<1,31> = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                        LINE.BR<1,32> = LINE.PRINT<1,C+1>
                    END
                END
            NEXT X
        NEXT C

        BB.DATA  = DESC:"|"
        BB.DATA := LINE.BR<1,1>:"|"
        BB.DATA := LINE.BR<1,2>:"|"
        BB.DATA := LINE.BR<1,3>:"|"
        BB.DATA := LINE.BR<1,4>:"|"
        BB.DATA := LINE.BR<1,5>:"|"
        BB.DATA := LINE.BR<1,6>:"|"
        BB.DATA := LINE.BR<1,7>:"|"
        BB.DATA := LINE.BR<1,8>:"|"
        BB.DATA := LINE.BR<1,9>:"|"
        BB.DATA := LINE.BR<1,10>:"|"
        BB.DATA := LINE.BR<1,11>:"|"
        BB.DATA := LINE.BR<1,12>:"|"
        BB.DATA := LINE.BR<1,13>:"|"
        BB.DATA := LINE.BR<1,14>:"|"
        BB.DATA := LINE.BR<1,15>:"|"
        BB.DATA := LINE.BR<1,16>:"|"
        BB.DATA := LINE.BR<1,17>:"|"
        BB.DATA := LINE.BR<1,18>:"|"
        BB.DATA := LINE.BR<1,19>:"|"
        BB.DATA := LINE.BR<1,20>:"|"
        BB.DATA := LINE.BR<1,21>:"|"
        BB.DATA := LINE.BR<1,22>:"|"
        BB.DATA := LINE.BR<1,23>:"|"
        BB.DATA := LINE.BR<1,24>:"|"
        BB.DATA := LINE.BR<1,25>:"|"
        BB.DATA := LINE.BR<1,26>:"|"
        BB.DATA := LINE.BR<1,27>:"|"
        BB.DATA := LINE.BR<1,28>:"|"
        BB.DATA := LINE.BR<1,29>:"|"
        BB.DATA := LINE.BR<1,30>:"|"
        BB.DATA := LINE.BR<1,31>:"|"
        BB.DATA := LINE.BR<1,32>:"|"

        FOR AK = 1 TO 32
            LINE.BR<1,33> += LINE.BR<1,AK>
        NEXT AK

        BB.DATA := LINE.BR<1,33>:"|"


        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
        LINE.BR<1,33> = 0

    NEXT XX

RETURN
*--------------------------------------------------------------------
PROCESS.CONT:

    T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENLEDALL.9000 AND @ID LT GENLEDALL.9990 ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 597 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            CLOSE.BAL.LCL  = 0
            CLOSE.BAL  = 0
            OLD.BRN = ''
            NEW.BRN = ''
*************************************************************************
            FOR C = 1 TO 32
                CLOSE.BAL.LCL  = 0
                FOR X = 1 TO POS
                    CUR = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                    IF CUR EQ 'EGP' THEN
                        BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":KK:NK<1,C>
                        CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                        NEW.BRN = KK:NK<1,C>
                        OLD.BRN = NEW.BRN
                        IF NEW.BRN NE OLD.BRN THEN
                            CLOSE.BAL.LCL = 0
                        END

                        IF NEW.BRN EQ OLD.BRN THEN
                            CLOSE.BAL.LCL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                        END

                        LINE.PRINT<1,C>   = KK:NK<1,C>
                        LINE.PRINT<1,C+1> = CLOSE.BAL.LCL

                        IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                            LINE.BR<1,1> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                            LINE.BR<1,2> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                            LINE.BR<1,3> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                            LINE.BR<1,4> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                            LINE.BR<1,5> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                            LINE.BR<1,6> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                            LINE.BR<1,7> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                            LINE.BR<1,8> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                            LINE.BR<1,9> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                            LINE.BR<1,10> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                            LINE.BR<1,11> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                            LINE.BR<1,12> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                            LINE.BR<1,13> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                            LINE.BR<1,14> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                            LINE.BR<1,15> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                            LINE.BR<1,16> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                            LINE.BR<1,17> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                            LINE.BR<1,18> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                            LINE.BR<1,19> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                            LINE.BR<1,20> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                            LINE.BR<1,21> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                            LINE.BR<1,22> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                            LINE.BR<1,23> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                            LINE.BR<1,24> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                            LINE.BR<1,25> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                            LINE.BR<1,26> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                            LINE.BR<1,27> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                            LINE.BR<1,28> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                            LINE.BR<1,29> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                            LINE.BR<1,30> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                            LINE.BR<1,31> = LINE.PRINT<1,C+1>
                        END

                        IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                            LINE.BR<1,32> = LINE.PRINT<1,C+1>
                        END
                    END
                NEXT X
            NEXT C

            BB.DATA  = DESC:"|"
            BB.DATA := LINE.BR<1,1>:"|"
            BB.DATA := LINE.BR<1,2>:"|"
            BB.DATA := LINE.BR<1,3>:"|"
            BB.DATA := LINE.BR<1,4>:"|"
            BB.DATA := LINE.BR<1,5>:"|"
            BB.DATA := LINE.BR<1,6>:"|"
            BB.DATA := LINE.BR<1,7>:"|"
            BB.DATA := LINE.BR<1,8>:"|"
            BB.DATA := LINE.BR<1,9>:"|"
            BB.DATA := LINE.BR<1,10>:"|"
            BB.DATA := LINE.BR<1,11>:"|"
            BB.DATA := LINE.BR<1,12>:"|"
            BB.DATA := LINE.BR<1,13>:"|"
            BB.DATA := LINE.BR<1,14>:"|"
            BB.DATA := LINE.BR<1,15>:"|"
            BB.DATA := LINE.BR<1,16>:"|"
            BB.DATA := LINE.BR<1,17>:"|"
            BB.DATA := LINE.BR<1,18>:"|"
            BB.DATA := LINE.BR<1,19>:"|"
            BB.DATA := LINE.BR<1,20>:"|"
            BB.DATA := LINE.BR<1,21>:"|"
            BB.DATA := LINE.BR<1,22>:"|"
            BB.DATA := LINE.BR<1,23>:"|"
            BB.DATA := LINE.BR<1,24>:"|"
            BB.DATA := LINE.BR<1,25>:"|"
            BB.DATA := LINE.BR<1,26>:"|"
            BB.DATA := LINE.BR<1,27>:"|"
            BB.DATA := LINE.BR<1,28>:"|"
            BB.DATA := LINE.BR<1,29>:"|"
            BB.DATA := LINE.BR<1,30>:"|"
            BB.DATA := LINE.BR<1,31>:"|"
            BB.DATA := LINE.BR<1,32>:"|"

            FOR AK = 1 TO 32
                LINE.BR<1,33> += LINE.BR<1,AK>
            NEXT AK

            BB.DATA := LINE.BR<1,33>:"|"


            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            LINE.BR<1,33> = 0
        NEXT I

    END
RETURN
*--------------------------------------------------------
