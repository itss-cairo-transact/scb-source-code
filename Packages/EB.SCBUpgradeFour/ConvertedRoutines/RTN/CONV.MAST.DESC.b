* @ValidationCode : Mjo4NDA2MDEyMTU6Q3AxMjUyOjE2NDExMzczMzkwOTg6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 17:28:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*****NESSREEN AHMED 4/12/2013****************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.MAST.DESC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 42 ] Removed I_FT.LOCAL.REFS Not Used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 44 ] Removed I_BR.LOCAL.REFS Not used - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_BR.LOCAL.REFS
*Line [ 46 ] Removed I_F.INF.MULTI.TXN Not Used- ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.INF.MULTI.TXN
*Line [ 48 ] Removed I_F.SCB.MAST.DAILY.PAYMENT Not Used- ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.MAST.DAILY.PAYMENT
*****

    TR.TYPE = O.DATA
    DESC1   = "���� ����"
    DESC2   ="���� �����"
    DESC3   = "����� ��� ����� � ����� ������"
    DESC4   = "����� ��� ����� ���"

    IF TR.TYPE EQ '38' THEN
        XXX = DESC1
    END
    IF TR.TYPE EQ 'ACVM' THEN
        XXX = DESC2
    END
    IF TR.TYPE EQ 'ACMS' THEN
        XXX =  DESC3
    END
    IF TR.TYPE EQ 'SETT' THEN
        XXX =   DESC4
    END
    O.DATA = XXX

*********************************************************
RETURN
END
