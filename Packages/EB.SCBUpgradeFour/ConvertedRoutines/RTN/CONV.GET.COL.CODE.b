* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.GET.COL.CODE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*------------------------------------
    FN.TMP = "FBNK.LIMIT" ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    KEY.ID = O.DATA
    NN     = 1
    IDD    = FIELD(KEY.ID,"*",1)
    COL    = FIELD(KEY.ID,"*",2)
    CALL F.READ(FN.TMP,IDD,R.TMP,F.TMP,ERR.TMP)

    LOCATE COL IN R.TMP<LI.COLLATERAL.CODE,1> SETTING NN THEN
        COL.CODE = R.TMP<LI.COLLATERAL.CODE,NN>
        COL.AMT  = R.TMP<LI.MAXIMUM.SECURED,NN>
    END
    O.DATA = COL.CODE : "*" : COL.AMT
*------------------------------------
    RETURN
END
