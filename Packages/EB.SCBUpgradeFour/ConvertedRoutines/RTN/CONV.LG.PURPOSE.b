* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*----RANIA & ABEER ------*
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.LG.PURPOSE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    LG.ID=O.DATA

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '';R.LD=''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,E1)

    LOCAL.REF=R.LD<LD.LOCAL.REF>
    LG.PURPOSE=LOCAL.REF<1,LDLR.IN.RESPECT.OF>

*Line [ 44 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    LG.PUR.COUNT=DCOUNT(LG.PURPOSE,@SM)

    FOR I=1 TO LG.PUR.COUNT
        LG.PURP:=LOCAL.REF<1,LDLR.IN.RESPECT.OF,I>:'.'
    NEXT I

    O.DATA=LG.PURP

    RETURN
END
