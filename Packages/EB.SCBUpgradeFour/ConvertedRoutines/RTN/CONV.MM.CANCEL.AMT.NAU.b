* @ValidationCode : MjotMTkxMDYzNDkyNzpDcDEyNTI6MTY0MTEzODA1NjM5NDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 02 Jan 2022 17:40:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.MM.CANCEL.AMT.NAU

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_MM.LOCAL.REFS

    XX = O.DATA
    FN.MM = 'FBNK.MM.MONEY.MARKET$NAU'
    F.MM = '' ; R.MM = ''
    CALL OPF(FN.MM,F.MM)

    CALL F.READ(FN.MM,XX,R.MM,F.MM,E1)
    IND = R.MM<MM.LOCAL.REF><1,MMLR.CANCEL.IND>
    IF IND EQ 'Y' THEN
        AMT = R.MM<MM.PRINCIPAL>
    END ELSE
        AMT = 0
    END
    O.DATA = AMT

RETURN
