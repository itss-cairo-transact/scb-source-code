* @ValidationCode : MjotMTI1NTA1NDYyOkNwMTI1MjoxNjQxMTI1MjQzMTYyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 14:07:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.LC.DR.CONF
*    PROGRAM  CONV.LC.DR.CONF

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LC.DR


    FN.LOG = 'F.SCB.LC.DR' ; F.LOG = '' ; R.LOG = ''
    CALL OPF(FN.LOG,F.LOG)

    FN.CUR  = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CUR.ID =O.DATA
*CUR.ID ='EUR'
***********************
    CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,EEE)
    CCY.MAR = '1'
    LOCATE CCY.MAR IN R.CUR<EB.CUR.CURRENCY.MARKET,1>  SETTING I THEN
        EQV.RAT = R.CUR< EB.CUR.MID.REVAL.RATE,I>
    END
    IF CUR.ID EQ 'EGP' THEN
        EQV.RAT = 1
    END
    IF CUR.ID EQ 'JPY' THEN
        EQV.RAT = (EQV.RAT / 100)
    END
*******************************
    TD1=TODAY ;COMP.ID =ID.COMPANY;   KEY.LIST = '' ; SELECTED = '' ;  ER.MSG = '';LC.ID ='' ;LC.TYP ='';LC.CON = '';LC.CON.TOT = '';LC.UNCON ='';LC.UNCON.TOT=''
    DR.UNCON.TOT ='';DR.CON ='';DR.CON.TOT ='';DR.UNCON =''
    EQUI.LC.CON.TOT ='';EQUI.DR.CON.TOT='';EQUI.LC.UNCON.TOT='';EQUI.DR.UNCON.TOT='';EQUI.LC.UNCON.TOT='';EQUI.DR.UNCON.TOT=''
    T.SEL = "SELECT F.SCB.LC.DR WITH LC.TYPE LIKE 'LED...' AND LC.CO.CODE EQ ": COMP.ID :" AND LC.CURRENCY EQ " : CUR.ID :" BY LC.CURRENCY "
    CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED,ASD)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LC.ID =KEY.LIST<I>
            CALL F.READ(FN.LOG,LC.ID,R.LOG,F.LOG,ERR1)
            LC.TYP           = R.LOG<LC.LC.TYPE>
            IF LC.TYP EQ 'LEDC' THEN
                LC.CON = R.LOG<LC.LC.NO> + LC.CON
                LC.CON.TOT = R.LOG<LC.LC.LIAB> + LC.CON.TOT
                EQUI.LC.CON.TOT = LC.CON.TOT * EQV.RAT
                CALL EB.ROUND.AMOUNT ('EGP',EQUI.LC.CON.TOT,'',"2")
**DR
                IF R.LOG<LC.LC.NO> EQ '' THEN
                    IF  R.LOG<LC.DR.NO> NE '' THEN
                        DR.CON = 1
                    END

                END  ELSE
                    DR.CON = '0'
                END
***************
*Line [ 81 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DR.NOS=  DCOUNT(R.LOG<LC.DRAWING>,@VM)
***************
                FOR YY =1 TO DR.NOS
                    IF R.LOG<LC.DRW.TYPE><1,YY> EQ 'DP' THEN
                        DR.CON.TOT =   R.LOG<LC.DRW.AMT><1,YY> + DR.CON.TOT
                        EQUI.DR.CON.TOT=DR.CON.TOT*EQV.RAT
                        CALL EB.ROUND.AMOUNT ('EGP',EQUI.DR.CON.TOT,'',"2")
                    END
                NEXT YY
            END
***************
            IF LC.TYP EQ 'LEDU' THEN
                LC.UNCON  =  R.LOG<LC.LC.NO> + LC.UNCON
                LC.UNCON.TOT = R.LOG<LC.LC.LIAB> + LC.UNCON.TOT
                EQUI.LC.UNCON.TOT = LC.UNCON.TOT * EQV.RAT
                CALL EB.ROUND.AMOUNT ('EGP',EQUI.LC.UNCON.TOT,'',"2")
********DR
*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DR.NOSU=  DCOUNT(R.LOG<LC.DRAWING>,@VM)

                IF R.LOG<LC.LC.NO> EQ ''  THEN
                    IF R.LOG<LC.DR.NO> NE '' THEN
                        DR.UNCON = 1
                    END
                END ELSE
                    DR.UNCON = '0'
                END
                FOR XX = 1 TO  DR.NOSU
                    IF R.LOG<LC.DRW.TYPE><1,XX> EQ 'DP' THEN
                        DR.UNCON.TOT =  R.LOG<LC.DRW.AMT><1,XX> + DR.UNCON.TOT
                        EQUI.DR.UNCON.TOT = DR.UNCON.TOT * EQV.RAT
                        CALL EB.ROUND.AMOUNT ('EGP',EQUI.DR.UNCON.TOT,'',"2")
                    END
                NEXT XX
            END
        NEXT I

    END
*****************************************
    TOT.CON.NO     =  LC.CON + DR.CON
    TOT.CON.AMT    =  LC.CON.TOT   +  DR.CON.TOT
    TOT.UNCON.NO   =  LC.UNCON +  DR.UNCON

    TOT.UNCON.AMT  =  LC.UNCON.TOT + DR.UNCON.TOT
    TOT.EQUI.CON   =  EQUI.LC.CON.TOT + EQUI.DR.CON.TOT
    TOT.EQUI.UNCON =  EQUI.LC.UNCON.TOT + EQUI.DR.UNCON.TOT

    TOT.NO.ALL =TOT.CON.NO +TOT.UNCON.NO
    TOT.EQU.ALL =TOT.EQUI.CON+TOT.EQUI.UNCON
    TOT.ALL = TOT.CON.NO:'-':TOT.CON.AMT:'-':TOT.EQUI.CON:'-':TOT.UNCON.NO :'-':TOT.UNCON.AMT:'-':TOT.EQUI.UNCON:'-':TOT.NO.ALL:'-':TOT.EQU.ALL
    O.DATA =TOT.ALL

RETURN
END
