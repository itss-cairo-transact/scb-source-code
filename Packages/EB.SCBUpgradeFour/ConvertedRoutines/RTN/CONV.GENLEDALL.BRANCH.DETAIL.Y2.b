* @ValidationCode : MjoxOTg0MjEyOTMwOkNwMTI1MjoxNjQxMDk5MTk1NDA5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:53:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.GENLEDALL.BRANCH.DETAIL.Y2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LINE.GENDETALL
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.GENDETALL.FILE

    COMP = ID.COMPANY
*-----------------------------------------------------------------------

    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.BRANCH.DETAIL" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"GENLEDALL.BRANCH.DETAIL"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.BRANCH.DETAIL" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE GENLEDALL.BRANCH.DETAIL CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create GENLEDALL.BRANCH.DETAIL File IN &SAVEDLISTS&'
        END
    END

*----------------------------------------------------------------

    FN.LN  = 'F.RE.STAT.REP.LINE'     ; F.LN  = '' ; R.LN = ''
    FN.BAL = 'F.RE.STAT.LINE.BAL'     ; F.BAL = '' ; R.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''
    FN.SLN = 'F.SCB.LINE.GENDETALL'   ; F.SLN = '' ; R.SLN = ''
    FN.DLN = 'F.SCB.GENDETALL.FILE'   ; F.DLN = '' ; R.DLN = ''

    CALL OPF(FN.LN,F.LN)   ; CALL OPF(FN.BAL,F.BAL) ; CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.SLN,F.SLN) ; CALL OPF(FN.DLN,F.DLN)

    TD2 = TODAY[1,6]:'01'
    CALL CDT("",TD2,'-1C')
    TD1 = TD2:'CL'

    TDD = TODAY
    CALL CDT("",TDD,'-1W')

    KK = 'EG00100'
    NK = ''
    NK<1,1>='01'  ; NK<1,2>='02'  ; NK<1,3>='03'  ; NK<1,4>='04'  ; NK<1,5>='05'
    NK<1,6>='06'  ; NK<1,7>='07'  ; NK<1,8>='09'  ; NK<1,9>='10'  ; NK<1,10>='11'
    NK<1,11>='12' ; NK<1,12>='13' ; NK<1,13>='14' ; NK<1,14>='15' ; NK<1,15>='16' ; NK<1,16>='20'
    NK<1,17>='21' ; NK<1,18>='22' ; NK<1,19>='23' ; NK<1,20>='30' ; NK<1,21>='31'
    NK<1,22>='32' ; NK<1,23>='35' ; NK<1,24>='40' ; NK<1,25>='50'
    NK<1,26>='51' ; NK<1,27>='60' ; NK<1,28>='70' ; NK<1,29>='80' ; NK<1,30>='81'
    NK<1,31>='90' ; NK<1,32>='99' ; NK<1,33>='17' ; NK<1,34>='19' ; NK<1,35>='52' ; NK<1,36>='53'
    NK<1,37>='18' ; NK<1,38>='24' ; NK<1,39>='25' ; NK<1,40>='41' ; NK<1,41>='26' ; NK<1,42>='27'
    NK<1,43>='29' ; NK<1,44>='33' ; NK<1,45>='34' ; NK<1,46>='82' ; NK<1,47>='83' ; NK<1,48>='36'
    NK<1,49>='45' ; NK<1,50>='37'

    LINE.PRINT = ''
    LINE.BR    = ''
    HEAD.DESC  = "�������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "�.�����":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "�.�������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "���������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "�.���":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "����� ����":"|"
    HEAD.DESC := "�.6 ������":"|"
    HEAD.DESC := "����� 6 ������":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "����� �����":"|"
    HEAD.DESC := "��������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "���������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "����� ������":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "������ �� �����":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "�������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "����� ������ ���������":"|"
    HEAD.DESC := "��������� �������":"|"
    HEAD.DESC := "��� ������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "����������� ����":"|"
    HEAD.DESC := "���� ����":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "���� ������":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "����":"|"
    HEAD.DESC := "���":"|"
    HEAD.DESC := "��� ����":"|"
    HEAD.DESC := "���� �����":"|"
    HEAD.DESC := "���� ����":"|"
    HEAD.DESC := "�����":"|"
    HEAD.DESC := "������ ������":"|"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    NEW.LINE = ''
    TOT.LINE = ''
    GOSUB PROCESS.ASST
    GOSUB PROCESS.PRFT
    GOSUB PROCESS.CONT
RETURN
*--------------------------------------------------------------------
PROCESS.ASST:
**  T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENDETALL.0001 AND @ID LT GENDETALL.1061 ) OR ( @ID GE GENDETALL.4000 AND @ID LE GENDETALL.4010 ) BY @ID"
    T.SEL = "SELECT ":FN.DLN:" WITH APPLIC.ID EQ 'Asset.Liability' BY SORT.NO"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC><1,1>
            LINE.NO = R.LN<RE.SRL.DESC><1,2>

            IF I EQ 1 THEN NEW.LINE = LINE.NO
            IF NEW.LINE NE LINE.NO THEN
                NN.DATA  = "******":"|"
                NN.DATA := "������ ":NEW.LINE:"|"
                NN.DATA := TOT.LINE<1,1>:"|"
                NN.DATA := TOT.LINE<1,2>:"|"
                NN.DATA := TOT.LINE<1,3>:"|"
                NN.DATA := TOT.LINE<1,4>:"|"
                NN.DATA := TOT.LINE<1,5>:"|"
                NN.DATA := TOT.LINE<1,6>:"|"
                NN.DATA := TOT.LINE<1,7>:"|"
                NN.DATA := TOT.LINE<1,8>:"|"
                NN.DATA := TOT.LINE<1,9>:"|"
                NN.DATA := TOT.LINE<1,10>:"|"
                NN.DATA := TOT.LINE<1,11>:"|"
                NN.DATA := TOT.LINE<1,12>:"|"
                NN.DATA := TOT.LINE<1,13>:"|"
                NN.DATA := TOT.LINE<1,14>:"|"
                NN.DATA := TOT.LINE<1,15>:"|"
                NN.DATA := TOT.LINE<1,16>:"|"
                NN.DATA := TOT.LINE<1,17>:"|"
                NN.DATA := TOT.LINE<1,18>:"|"
                NN.DATA := TOT.LINE<1,19>:"|"
                NN.DATA := TOT.LINE<1,20>:"|"
                NN.DATA := TOT.LINE<1,21>:"|"
                NN.DATA := TOT.LINE<1,22>:"|"
                NN.DATA := TOT.LINE<1,23>:"|"
                NN.DATA := TOT.LINE<1,24>:"|"
                NN.DATA := TOT.LINE<1,25>:"|"
                NN.DATA := TOT.LINE<1,26>:"|"
                NN.DATA := TOT.LINE<1,27>:"|"
                NN.DATA := TOT.LINE<1,28>:"|"
                NN.DATA := TOT.LINE<1,29>:"|"
                NN.DATA := TOT.LINE<1,30>:"|"
                NN.DATA := TOT.LINE<1,31>:"|"
                NN.DATA := TOT.LINE<1,32>:"|"
                NN.DATA := TOT.LINE<1,33>:"|"
                NN.DATA := TOT.LINE<1,34>:"|"
                NN.DATA := TOT.LINE<1,35>:"|"
                NN.DATA := TOT.LINE<1,36>:"|"
                NN.DATA := TOT.LINE<1,37>:"|"
                NN.DATA := TOT.LINE<1,38>:"|"
                NN.DATA := TOT.LINE<1,39>:"|"
                NN.DATA := TOT.LINE<1,40>:"|"
                NN.DATA := TOT.LINE<1,41>:"|"
                NN.DATA := TOT.LINE<1,42>:"|"
                NN.DATA := TOT.LINE<1,43>:"|"
                NN.DATA := TOT.LINE<1,44>:"|"
                NN.DATA := TOT.LINE<1,45>:"|"
                NN.DATA := TOT.LINE<1,46>:"|"
                NN.DATA := TOT.LINE<1,47>:"|"
                NN.DATA := TOT.LINE<1,48>:"|"
                NN.DATA := TOT.LINE<1,49>:"|"
                NN.DATA := TOT.LINE<1,50>:"|"
                NN.DATA := TOT.LINE<1,51>:"|"

                WRITESEQ NN.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOT.LINE = ''
            END

            DESC.ID = FIELD(KEY.LIST<I>,".",2)

            PROD.CATEG = R.LN<RE.SRL.ASSET1,1>
            PL.CATEG   = R.LN<RE.SRL.PROFIT1,1>

            IF PROD.CATEG EQ '' THEN
                CATEG = PL.CATEG
            END ELSE
                CATEG = PROD.CATEG
            END

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 247 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            CLOSE.BAL.LCL  = 0
            CLOSE.BAL  = 0
            OLD.BRN = ''
            NEW.BRN = ''
*************************************************************************
            FOR C = 1 TO 50
                CLOSE.BAL.LCL  = 0
                FOR X = 1 TO POS
                    BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":KK:NK<1,C>
                    CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                    NEW.BRN = KK:NK<1,C>

                    OLD.BRN = NEW.BRN

                    IF NEW.BRN NE OLD.BRN THEN
                        CLOSE.BAL.LCL = 0
                    END

                    IF NEW.BRN EQ OLD.BRN THEN
                        CLOSE.BAL.LCL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                    END

                    LINE.PRINT<1,C>   = KK:NK<1,C>
                    LINE.PRINT<1,C+1> = CLOSE.BAL.LCL

                    IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                        LINE.BR<1,1>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                        LINE.BR<1,2>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                        LINE.BR<1,3>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                        LINE.BR<1,4>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                        LINE.BR<1,5>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                        LINE.BR<1,6>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                        LINE.BR<1,7>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                        LINE.BR<1,8>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                        LINE.BR<1,9>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                        LINE.BR<1,10>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                        LINE.BR<1,11>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                        LINE.BR<1,12>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                        LINE.BR<1,13>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                        LINE.BR<1,14>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                        LINE.BR<1,15>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                        LINE.BR<1,16>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                        LINE.BR<1,17>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                        LINE.BR<1,18>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                        LINE.BR<1,19>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                        LINE.BR<1,20>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                        LINE.BR<1,21>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                        LINE.BR<1,22>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                        LINE.BR<1,23>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                        LINE.BR<1,24>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                        LINE.BR<1,25>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                        LINE.BR<1,26>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                        LINE.BR<1,27>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                        LINE.BR<1,28>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                        LINE.BR<1,29>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                        LINE.BR<1,30>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                        LINE.BR<1,31>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                        LINE.BR<1,32>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010017' THEN
                        LINE.BR<1,33>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010019' THEN
                        LINE.BR<1,34>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010052' THEN
                        LINE.BR<1,35>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010053' THEN
                        LINE.BR<1,36>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010018' THEN
                        LINE.BR<1,37>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010024' THEN
                        LINE.BR<1,38>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010025' THEN
                        LINE.BR<1,39>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010041' THEN
                        LINE.BR<1,40>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010026' THEN
                        LINE.BR<1,41>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010027' THEN
                        LINE.BR<1,42>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010029' THEN
                        LINE.BR<1,43>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010033' THEN
                        LINE.BR<1,44>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010034' THEN
                        LINE.BR<1,45>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010082' THEN
                        LINE.BR<1,46>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010083' THEN
                        LINE.BR<1,47>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010036' THEN
                        LINE.BR<1,48>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010045' THEN
                        LINE.BR<1,49>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010037' THEN
                        LINE.BR<1,50>   = LINE.PRINT<1,C+1>
                    END

                NEXT X

                IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                    TOT.LINE<1,1> += LINE.BR<1,1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                    TOT.LINE<1,2> += LINE.BR<1,2>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                    TOT.LINE<1,3> += LINE.BR<1,3>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                    TOT.LINE<1,4> +=  LINE.BR<1,4>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                    TOT.LINE<1,5> +=   LINE.BR<1,5>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                    TOT.LINE<1,6> +=  LINE.BR<1,6>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                    TOT.LINE<1,7> +=  LINE.BR<1,7>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                    TOT.LINE<1,8> +=  LINE.BR<1,8>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                    TOT.LINE<1,9> +=  LINE.BR<1,9>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                    TOT.LINE<1,10> +=  LINE.BR<1,10>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                    TOT.LINE<1,11> +=  LINE.BR<1,11>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                    TOT.LINE<1,12> +=  LINE.BR<1,12>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                    TOT.LINE<1,13> +=  LINE.BR<1,13>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                    TOT.LINE<1,14> +=  LINE.BR<1,14>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                    TOT.LINE<1,15> +=  LINE.BR<1,15>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                    TOT.LINE<1,16> +=  LINE.BR<1,16>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                    TOT.LINE<1,17> +=  LINE.BR<1,17>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                    TOT.LINE<1,18> +=  LINE.BR<1,18>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                    TOT.LINE<1,19> +=  LINE.BR<1,19>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                    TOT.LINE<1,20> +=  LINE.BR<1,20>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                    TOT.LINE<1,21> +=  LINE.BR<1,21>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                    TOT.LINE<1,22> +=  LINE.BR<1,22>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                    TOT.LINE<1,23> +=  LINE.BR<1,23>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                    TOT.LINE<1,24> +=  LINE.BR<1,24>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                    TOT.LINE<1,25> +=  LINE.BR<1,25>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                    TOT.LINE<1,26> +=  LINE.BR<1,26>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                    TOT.LINE<1,27> +=  LINE.BR<1,27>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                    TOT.LINE<1,28> +=  LINE.BR<1,28>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                    TOT.LINE<1,29> +=  LINE.BR<1,29>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                    TOT.LINE<1,30> +=  LINE.BR<1,30>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                    TOT.LINE<1,31> +=  LINE.BR<1,31>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                    TOT.LINE<1,32> +=  LINE.BR<1,32>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010017' THEN
                    TOT.LINE<1,33> +=  LINE.BR<1,33>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010019' THEN
                    TOT.LINE<1,34> +=  LINE.BR<1,34>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010052' THEN
                    TOT.LINE<1,35> +=  LINE.BR<1,35>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010053' THEN
                    TOT.LINE<1,36> +=  LINE.BR<1,36>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010018' THEN
                    TOT.LINE<1,37> +=  LINE.BR<1,37>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010024' THEN
                    TOT.LINE<1,38> +=  LINE.BR<1,38>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010025' THEN
                    TOT.LINE<1,39> +=  LINE.BR<1,39>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010041' THEN
                    TOT.LINE<1,40> +=  LINE.BR<1,40>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010026' THEN
                    TOT.LINE<1,41> +=  LINE.BR<1,41>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010027' THEN
                    TOT.LINE<1,42> +=  LINE.BR<1,42>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010029' THEN
                    TOT.LINE<1,43> +=  LINE.BR<1,43>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010033' THEN
                    TOT.LINE<1,44> +=  LINE.BR<1,44>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010034' THEN
                    TOT.LINE<1,45> +=  LINE.BR<1,45>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010082' THEN
                    TOT.LINE<1,46> +=  LINE.BR<1,46>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010083' THEN
                    TOT.LINE<1,47> +=  LINE.BR<1,47>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010036' THEN
                    TOT.LINE<1,48> +=  LINE.BR<1,48>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010045' THEN
                    TOT.LINE<1,49> +=  LINE.BR<1,49>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010037' THEN
                    TOT.LINE<1,50> +=  LINE.BR<1,50>
                END


            NEXT C

            BB.DATA  = CATEG:"|"
            BB.DATA := DESC:"|"
            BB.DATA := LINE.BR<1,1>:"|"
            BB.DATA := LINE.BR<1,2>:"|"
            BB.DATA := LINE.BR<1,3>:"|"
            BB.DATA := LINE.BR<1,4>:"|"
            BB.DATA := LINE.BR<1,5>:"|"
            BB.DATA := LINE.BR<1,6>:"|"
            BB.DATA := LINE.BR<1,7>:"|"
            BB.DATA := LINE.BR<1,8>:"|"
            BB.DATA := LINE.BR<1,9>:"|"
            BB.DATA := LINE.BR<1,10>:"|"
            BB.DATA := LINE.BR<1,11>:"|"
            BB.DATA := LINE.BR<1,12>:"|"
            BB.DATA := LINE.BR<1,13>:"|"
            BB.DATA := LINE.BR<1,14>:"|"
            BB.DATA := LINE.BR<1,15>:"|"
            BB.DATA := LINE.BR<1,16>:"|"
            BB.DATA := LINE.BR<1,17>:"|"
            BB.DATA := LINE.BR<1,18>:"|"
            BB.DATA := LINE.BR<1,19>:"|"
            BB.DATA := LINE.BR<1,20>:"|"
            BB.DATA := LINE.BR<1,21>:"|"
            BB.DATA := LINE.BR<1,22>:"|"
            BB.DATA := LINE.BR<1,23>:"|"
            BB.DATA := LINE.BR<1,24>:"|"
            BB.DATA := LINE.BR<1,25>:"|"
            BB.DATA := LINE.BR<1,26>:"|"
            BB.DATA := LINE.BR<1,27>:"|"
            BB.DATA := LINE.BR<1,28>:"|"
            BB.DATA := LINE.BR<1,29>:"|"
            BB.DATA := LINE.BR<1,30>:"|"
            BB.DATA := LINE.BR<1,31>:"|"
            BB.DATA := LINE.BR<1,32>:"|"
            BB.DATA := LINE.BR<1,33>:"|"
            BB.DATA := LINE.BR<1,34>:"|"
            BB.DATA := LINE.BR<1,35>:"|"
            BB.DATA := LINE.BR<1,36>:"|"
            BB.DATA := LINE.BR<1,37>:"|"
            BB.DATA := LINE.BR<1,38>:"|"
            BB.DATA := LINE.BR<1,39>:"|"
            BB.DATA := LINE.BR<1,40>:"|"
            BB.DATA := LINE.BR<1,41>:"|"
            BB.DATA := LINE.BR<1,42>:"|"
            BB.DATA := LINE.BR<1,43>:"|"
            BB.DATA := LINE.BR<1,44>:"|"
            BB.DATA := LINE.BR<1,45>:"|"
            BB.DATA := LINE.BR<1,46>:"|"
            BB.DATA := LINE.BR<1,47>:"|"
            BB.DATA := LINE.BR<1,48>:"|"
            BB.DATA := LINE.BR<1,49>:"|"
            BB.DATA := LINE.BR<1,50>:"|"

            FOR AK = 1 TO 50
                LINE.BR<1,51> += LINE.BR<1,AK>
            NEXT AK
            BB.DATA := LINE.BR<1,51>:"|"

            TOT.LINE<1,51> += LINE.BR<1,51>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            LINE.BR<1,51> = 0
            NEW.LINE = LINE.NO
        NEXT I
        IF I EQ SELECTED THEN
            NN.DATA  = "******":"|"
            NN.DATA := "������ ":NEW.LINE:"|"
            NN.DATA := TOT.LINE<1,1>:"|"
            NN.DATA := TOT.LINE<1,2>:"|"
            NN.DATA := TOT.LINE<1,3>:"|"
            NN.DATA := TOT.LINE<1,4>:"|"
            NN.DATA := TOT.LINE<1,5>:"|"
            NN.DATA := TOT.LINE<1,6>:"|"
            NN.DATA := TOT.LINE<1,7>:"|"
            NN.DATA := TOT.LINE<1,8>:"|"
            NN.DATA := TOT.LINE<1,9>:"|"
            NN.DATA := TOT.LINE<1,10>:"|"
            NN.DATA := TOT.LINE<1,11>:"|"
            NN.DATA := TOT.LINE<1,12>:"|"
            NN.DATA := TOT.LINE<1,13>:"|"
            NN.DATA := TOT.LINE<1,14>:"|"
            NN.DATA := TOT.LINE<1,15>:"|"
            NN.DATA := TOT.LINE<1,16>:"|"
            NN.DATA := TOT.LINE<1,17>:"|"
            NN.DATA := TOT.LINE<1,18>:"|"
            NN.DATA := TOT.LINE<1,19>:"|"
            NN.DATA := TOT.LINE<1,20>:"|"
            NN.DATA := TOT.LINE<1,21>:"|"
            NN.DATA := TOT.LINE<1,22>:"|"
            NN.DATA := TOT.LINE<1,23>:"|"
            NN.DATA := TOT.LINE<1,24>:"|"
            NN.DATA := TOT.LINE<1,25>:"|"
            NN.DATA := TOT.LINE<1,26>:"|"
            NN.DATA := TOT.LINE<1,27>:"|"
            NN.DATA := TOT.LINE<1,28>:"|"
            NN.DATA := TOT.LINE<1,29>:"|"
            NN.DATA := TOT.LINE<1,30>:"|"
            NN.DATA := TOT.LINE<1,31>:"|"
            NN.DATA := TOT.LINE<1,32>:"|"
            NN.DATA := TOT.LINE<1,33>:"|"
            NN.DATA := TOT.LINE<1,34>:"|"
            NN.DATA := TOT.LINE<1,35>:"|"
            NN.DATA := TOT.LINE<1,36>:"|"
            NN.DATA := TOT.LINE<1,37>:"|"
            NN.DATA := TOT.LINE<1,38>:"|"
            NN.DATA := TOT.LINE<1,39>:"|"
            NN.DATA := TOT.LINE<1,40>:"|"
            NN.DATA := TOT.LINE<1,41>:"|"
            NN.DATA := TOT.LINE<1,42>:"|"
            NN.DATA := TOT.LINE<1,43>:"|"
            NN.DATA := TOT.LINE<1,44>:"|"
            NN.DATA := TOT.LINE<1,45>:"|"
            NN.DATA := TOT.LINE<1,46>:"|"
            NN.DATA := TOT.LINE<1,47>:"|"
            NN.DATA := TOT.LINE<1,48>:"|"
            NN.DATA := TOT.LINE<1,49>:"|"
            NN.DATA := TOT.LINE<1,50>:"|"
            NN.DATA := TOT.LINE<1,51>:"|"

            WRITESEQ NN.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            TOT.LINE = ''
        END

    END
RETURN
*------------------------------------------
PROCESS.PRFT:
    T.SEL1 = "SELECT F.SCB.LINE.GENDETALL"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    FOR XX = 1 TO SELECTED1
        CALL F.READ(FN.SLN,XX,R.SLN,F.SLN,E2)
        EGP.ID = R.SLN<DALL.EGP.ID>
        FCY.ID = R.SLN<DALL.FCY.ID>

        CALL F.READ(FN.LN,EGP.ID,R.LN,F.LN,E4)

        DESC.ID.EGP = FIELD(EGP.ID,".",2)
        DESC.ID.FCY = FIELD(FCY.ID,".",2)

        DESC = R.LN<RE.SRL.DESC><1,1>
        LINE.NO = R.LN<RE.SRL.DESC><1,2>

        IF XX EQ 1 THEN NEW.LINE = LINE.NO
        IF NEW.LINE NE LINE.NO THEN
            NN.DATA  = "******":"|"
            NN.DATA := "������ ":NEW.LINE:"|"
            NN.DATA := TOT.LINE<1,1>:"|"
            NN.DATA := TOT.LINE<1,2>:"|"
            NN.DATA := TOT.LINE<1,3>:"|"
            NN.DATA := TOT.LINE<1,4>:"|"
            NN.DATA := TOT.LINE<1,5>:"|"
            NN.DATA := TOT.LINE<1,6>:"|"
            NN.DATA := TOT.LINE<1,7>:"|"
            NN.DATA := TOT.LINE<1,8>:"|"
            NN.DATA := TOT.LINE<1,9>:"|"
            NN.DATA := TOT.LINE<1,10>:"|"
            NN.DATA := TOT.LINE<1,11>:"|"
            NN.DATA := TOT.LINE<1,12>:"|"
            NN.DATA := TOT.LINE<1,13>:"|"
            NN.DATA := TOT.LINE<1,14>:"|"
            NN.DATA := TOT.LINE<1,15>:"|"
            NN.DATA := TOT.LINE<1,16>:"|"
            NN.DATA := TOT.LINE<1,17>:"|"
            NN.DATA := TOT.LINE<1,18>:"|"
            NN.DATA := TOT.LINE<1,19>:"|"
            NN.DATA := TOT.LINE<1,20>:"|"
            NN.DATA := TOT.LINE<1,21>:"|"
            NN.DATA := TOT.LINE<1,22>:"|"
            NN.DATA := TOT.LINE<1,23>:"|"
            NN.DATA := TOT.LINE<1,24>:"|"
            NN.DATA := TOT.LINE<1,25>:"|"
            NN.DATA := TOT.LINE<1,26>:"|"
            NN.DATA := TOT.LINE<1,27>:"|"
            NN.DATA := TOT.LINE<1,28>:"|"
            NN.DATA := TOT.LINE<1,29>:"|"
            NN.DATA := TOT.LINE<1,30>:"|"
            NN.DATA := TOT.LINE<1,31>:"|"
            NN.DATA := TOT.LINE<1,32>:"|"
            NN.DATA := TOT.LINE<1,33>:"|"
            NN.DATA := TOT.LINE<1,34>:"|"
            NN.DATA := TOT.LINE<1,35>:"|"
            NN.DATA := TOT.LINE<1,36>:"|"
            NN.DATA := TOT.LINE<1,37>:"|"
            NN.DATA := TOT.LINE<1,38>:"|"
            NN.DATA := TOT.LINE<1,39>:"|"
            NN.DATA := TOT.LINE<1,40>:"|"
            NN.DATA := TOT.LINE<1,41>:"|"
            NN.DATA := TOT.LINE<1,42>:"|"
            NN.DATA := TOT.LINE<1,43>:"|"
            NN.DATA := TOT.LINE<1,44>:"|"
            NN.DATA := TOT.LINE<1,45>:"|"
            NN.DATA := TOT.LINE<1,46>:"|"
            NN.DATA := TOT.LINE<1,47>:"|"
            NN.DATA := TOT.LINE<1,48>:"|"
            NN.DATA := TOT.LINE<1,49>:"|"
            NN.DATA := TOT.LINE<1,50>:"|"
            NN.DATA := TOT.LINE<1,51>:"|"

            WRITESEQ NN.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            TOT.LINE = ''
        END


        PROD.CATEG = R.LN<RE.SRL.ASSET1,1>
        PL.CATEG   = R.LN<RE.SRL.PROFIT1,1>

        IF PROD.CATEG EQ '' THEN
            CATEG = PL.CATEG
        END ELSE
            CATEG = PROD.CATEG
        END

        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
        CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 866 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        POS = DCOUNT(CCURR,@VM)
        CLOSE.BAL.LCL  = 0
        CLOSE.BAL  = 0
        OLD.BRN = ''
        NEW.BRN = ''
*************************************************************************
        FOR C = 1 TO 50
            CLOSE.BAL.LCL  = 0
            FOR X = 1 TO POS
                CUR.ID = R.CCY<RE.BCP.ORIGINAL.CCY,X>
                IF CUR.ID EQ 'EGP' THEN
                    BAL.ID2 = FIELD(EGP.ID,".",1):"-":DESC.ID.EGP:"-":"LOCAL":"-":TD1:"*":KK:NK<1,C>
                    CALL F.READ(FN.BAL,BAL.ID2,R.BAL,F.BAL,E2)
                    CLOSE.BAL.LCL   += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                END

                BAL.ID = FIELD(FCY.ID,".",1):"-":DESC.ID.FCY:"-":CUR.ID:"-":TD1:"*":KK:NK<1,C>
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                NEW.BRN = KK:NK<1,C>

                OLD.BRN = NEW.BRN

                IF NEW.BRN NE OLD.BRN THEN
                    CLOSE.BAL.LCL = 0
                END

                IF NEW.BRN EQ OLD.BRN THEN
                    CLOSE.BAL.LCL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                END


                LINE.PRINT<1,C>   = KK:NK<1,C>
                LINE.PRINT<1,C+1> = CLOSE.BAL.LCL

                IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                    LINE.BR<1,1>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                    LINE.BR<1,2>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                    LINE.BR<1,3>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                    LINE.BR<1,4>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                    LINE.BR<1,5>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                    LINE.BR<1,6>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                    LINE.BR<1,7>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                    LINE.BR<1,8>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                    LINE.BR<1,9>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                    LINE.BR<1,10>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                    LINE.BR<1,11>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                    LINE.BR<1,12>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                    LINE.BR<1,13>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                    LINE.BR<1,14>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                    LINE.BR<1,15>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                    LINE.BR<1,16>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                    LINE.BR<1,17>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                    LINE.BR<1,18>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                    LINE.BR<1,19>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                    LINE.BR<1,20>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                    LINE.BR<1,21>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                    LINE.BR<1,22>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                    LINE.BR<1,23>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                    LINE.BR<1,24>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                    LINE.BR<1,25>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                    LINE.BR<1,26>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                    LINE.BR<1,27>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                    LINE.BR<1,28>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                    LINE.BR<1,29>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                    LINE.BR<1,30>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                    LINE.BR<1,31>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                    LINE.BR<1,32>   = LINE.PRINT<1,C+1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010017' THEN
                    LINE.BR<1,33>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010019' THEN
                    LINE.BR<1,34>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010052' THEN
                    LINE.BR<1,35>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010053' THEN
                    LINE.BR<1,36>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010018' THEN
                    LINE.BR<1,37>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010024' THEN
                    LINE.BR<1,38>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010025' THEN
                    LINE.BR<1,39>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010041' THEN
                    LINE.BR<1,40>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010026' THEN
                    LINE.BR<1,41>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010027' THEN
                    LINE.BR<1,42>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010029' THEN
                    LINE.BR<1,43>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010033' THEN
                    LINE.BR<1,44>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010034' THEN
                    LINE.BR<1,45>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010082' THEN
                    LINE.BR<1,46>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010083' THEN
                    LINE.BR<1,47>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010036' THEN
                    LINE.BR<1,48>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010045' THEN
                    LINE.BR<1,49>   = LINE.PRINT<1,C+1>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010037' THEN
                    LINE.BR<1,50>   = LINE.PRINT<1,C+1>
                END


            NEXT X

            IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                TOT.LINE<1,1> += LINE.BR<1,1>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                TOT.LINE<1,2> += LINE.BR<1,2>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                TOT.LINE<1,3> += LINE.BR<1,3>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                TOT.LINE<1,4> +=  LINE.BR<1,4>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                TOT.LINE<1,5> +=   LINE.BR<1,5>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                TOT.LINE<1,6> +=  LINE.BR<1,6>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                TOT.LINE<1,7> +=  LINE.BR<1,7>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                TOT.LINE<1,8> +=  LINE.BR<1,8>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                TOT.LINE<1,9> +=  LINE.BR<1,9>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                TOT.LINE<1,10> +=  LINE.BR<1,10>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                TOT.LINE<1,11> +=  LINE.BR<1,11>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                TOT.LINE<1,12> +=  LINE.BR<1,12>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                TOT.LINE<1,13> +=  LINE.BR<1,13>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                TOT.LINE<1,14> +=  LINE.BR<1,14>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                TOT.LINE<1,15> +=  LINE.BR<1,15>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                TOT.LINE<1,16> +=  LINE.BR<1,16>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                TOT.LINE<1,17> +=  LINE.BR<1,17>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                TOT.LINE<1,18> +=  LINE.BR<1,18>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                TOT.LINE<1,19> +=  LINE.BR<1,19>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                TOT.LINE<1,20> +=  LINE.BR<1,20>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                TOT.LINE<1,21> +=  LINE.BR<1,21>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                TOT.LINE<1,22> +=  LINE.BR<1,22>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                TOT.LINE<1,23> +=  LINE.BR<1,23>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                TOT.LINE<1,24> +=  LINE.BR<1,24>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                TOT.LINE<1,25> +=  LINE.BR<1,25>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                TOT.LINE<1,26> +=  LINE.BR<1,26>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                TOT.LINE<1,27> +=  LINE.BR<1,27>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                TOT.LINE<1,28> +=  LINE.BR<1,28>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                TOT.LINE<1,29> +=  LINE.BR<1,29>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                TOT.LINE<1,30> +=  LINE.BR<1,30>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                TOT.LINE<1,31> +=  LINE.BR<1,31>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                TOT.LINE<1,32> +=  LINE.BR<1,32>
            END

            IF LINE.PRINT<1,C> EQ 'EG0010017' THEN
                TOT.LINE<1,33> +=  LINE.BR<1,33>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010019' THEN
                TOT.LINE<1,34> +=  LINE.BR<1,34>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010052' THEN
                TOT.LINE<1,35> +=  LINE.BR<1,35>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010053' THEN
                TOT.LINE<1,36> +=  LINE.BR<1,36>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010018' THEN
                TOT.LINE<1,37> +=  LINE.BR<1,37>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010024' THEN
                TOT.LINE<1,38> +=  LINE.BR<1,38>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010025' THEN
                TOT.LINE<1,39> +=  LINE.BR<1,39>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010041' THEN
                TOT.LINE<1,40> +=  LINE.BR<1,40>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010026' THEN
                TOT.LINE<1,41> +=  LINE.BR<1,41>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010027' THEN
                TOT.LINE<1,42> +=  LINE.BR<1,42>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010029' THEN
                TOT.LINE<1,43> +=  LINE.BR<1,43>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010033' THEN
                TOT.LINE<1,44> +=  LINE.BR<1,44>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010034' THEN
                TOT.LINE<1,45> +=  LINE.BR<1,45>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010082' THEN
                TOT.LINE<1,46> +=  LINE.BR<1,46>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010083' THEN
                TOT.LINE<1,47> +=  LINE.BR<1,47>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010036' THEN
                TOT.LINE<1,48> +=  LINE.BR<1,48>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010045' THEN
                TOT.LINE<1,49> +=  LINE.BR<1,49>
            END
            IF LINE.PRINT<1,C> EQ 'EG0010037' THEN
                TOT.LINE<1,50> +=  LINE.BR<1,50>
            END


        NEXT C

        BB.DATA  = CATEG:"|"
        BB.DATA := DESC:"|"
        BB.DATA := LINE.BR<1,1>:"|"
        BB.DATA := LINE.BR<1,2>:"|"
        BB.DATA := LINE.BR<1,3>:"|"
        BB.DATA := LINE.BR<1,4>:"|"
        BB.DATA := LINE.BR<1,5>:"|"
        BB.DATA := LINE.BR<1,6>:"|"
        BB.DATA := LINE.BR<1,7>:"|"
        BB.DATA := LINE.BR<1,8>:"|"
        BB.DATA := LINE.BR<1,9>:"|"
        BB.DATA := LINE.BR<1,10>:"|"
        BB.DATA := LINE.BR<1,11>:"|"
        BB.DATA := LINE.BR<1,12>:"|"
        BB.DATA := LINE.BR<1,13>:"|"
        BB.DATA := LINE.BR<1,14>:"|"
        BB.DATA := LINE.BR<1,15>:"|"
        BB.DATA := LINE.BR<1,16>:"|"
        BB.DATA := LINE.BR<1,17>:"|"
        BB.DATA := LINE.BR<1,18>:"|"
        BB.DATA := LINE.BR<1,19>:"|"
        BB.DATA := LINE.BR<1,20>:"|"
        BB.DATA := LINE.BR<1,21>:"|"
        BB.DATA := LINE.BR<1,22>:"|"
        BB.DATA := LINE.BR<1,23>:"|"
        BB.DATA := LINE.BR<1,24>:"|"
        BB.DATA := LINE.BR<1,25>:"|"
        BB.DATA := LINE.BR<1,26>:"|"
        BB.DATA := LINE.BR<1,27>:"|"
        BB.DATA := LINE.BR<1,28>:"|"
        BB.DATA := LINE.BR<1,29>:"|"
        BB.DATA := LINE.BR<1,30>:"|"
        BB.DATA := LINE.BR<1,31>:"|"
        BB.DATA := LINE.BR<1,32>:"|"
        BB.DATA := LINE.BR<1,33>:"|"
        BB.DATA := LINE.BR<1,34>:"|"
        BB.DATA := LINE.BR<1,35>:"|"
        BB.DATA := LINE.BR<1,36>:"|"
        BB.DATA := LINE.BR<1,37>:"|"
        BB.DATA := LINE.BR<1,38>:"|"
        BB.DATA := LINE.BR<1,39>:"|"
        BB.DATA := LINE.BR<1,40>:"|"
        BB.DATA := LINE.BR<1,41>:"|"
        BB.DATA := LINE.BR<1,42>:"|"
        BB.DATA := LINE.BR<1,43>:"|"
        BB.DATA := LINE.BR<1,44>:"|"
        BB.DATA := LINE.BR<1,45>:"|"
        BB.DATA := LINE.BR<1,46>:"|"
        BB.DATA := LINE.BR<1,47>:"|"
        BB.DATA := LINE.BR<1,48>:"|"
        BB.DATA := LINE.BR<1,49>:"|"
        BB.DATA := LINE.BR<1,50>:"|"

        FOR AK = 1 TO 50
            LINE.BR<1,51> += LINE.BR<1,AK>
        NEXT AK
        BB.DATA := LINE.BR<1,51>:"|"

        TOT.LINE<1,51> += LINE.BR<1,51>

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
        LINE.BR<1,51> = 0
        NEW.LINE = LINE.NO

    NEXT XX

    IF XX EQ SELECTED1 THEN
        NN.DATA  = "******":"|"
        NN.DATA := "������ ":NEW.LINE:"|"
        NN.DATA := TOT.LINE<1,1>:"|"
        NN.DATA := TOT.LINE<1,2>:"|"
        NN.DATA := TOT.LINE<1,3>:"|"
        NN.DATA := TOT.LINE<1,4>:"|"
        NN.DATA := TOT.LINE<1,5>:"|"
        NN.DATA := TOT.LINE<1,6>:"|"
        NN.DATA := TOT.LINE<1,7>:"|"
        NN.DATA := TOT.LINE<1,8>:"|"
        NN.DATA := TOT.LINE<1,9>:"|"
        NN.DATA := TOT.LINE<1,10>:"|"
        NN.DATA := TOT.LINE<1,11>:"|"
        NN.DATA := TOT.LINE<1,12>:"|"
        NN.DATA := TOT.LINE<1,13>:"|"
        NN.DATA := TOT.LINE<1,14>:"|"
        NN.DATA := TOT.LINE<1,15>:"|"
        NN.DATA := TOT.LINE<1,16>:"|"
        NN.DATA := TOT.LINE<1,17>:"|"
        NN.DATA := TOT.LINE<1,18>:"|"
        NN.DATA := TOT.LINE<1,19>:"|"
        NN.DATA := TOT.LINE<1,20>:"|"
        NN.DATA := TOT.LINE<1,21>:"|"
        NN.DATA := TOT.LINE<1,22>:"|"
        NN.DATA := TOT.LINE<1,23>:"|"
        NN.DATA := TOT.LINE<1,24>:"|"
        NN.DATA := TOT.LINE<1,25>:"|"
        NN.DATA := TOT.LINE<1,26>:"|"
        NN.DATA := TOT.LINE<1,27>:"|"
        NN.DATA := TOT.LINE<1,28>:"|"
        NN.DATA := TOT.LINE<1,29>:"|"
        NN.DATA := TOT.LINE<1,30>:"|"
        NN.DATA := TOT.LINE<1,31>:"|"
        NN.DATA := TOT.LINE<1,32>:"|"
        NN.DATA := TOT.LINE<1,33>:"|"
        NN.DATA := TOT.LINE<1,34>:"|"
        NN.DATA := TOT.LINE<1,35>:"|"
        NN.DATA := TOT.LINE<1,36>:"|"
        NN.DATA := TOT.LINE<1,37>:"|"
        NN.DATA := TOT.LINE<1,38>:"|"
        NN.DATA := TOT.LINE<1,39>:"|"
        NN.DATA := TOT.LINE<1,40>:"|"
        NN.DATA := TOT.LINE<1,41>:"|"
        NN.DATA := TOT.LINE<1,42>:"|"
        NN.DATA := TOT.LINE<1,43>:"|"
        NN.DATA := TOT.LINE<1,44>:"|"
        NN.DATA := TOT.LINE<1,45>:"|"
        NN.DATA := TOT.LINE<1,46>:"|"
        NN.DATA := TOT.LINE<1,47>:"|"
        NN.DATA := TOT.LINE<1,48>:"|"
        NN.DATA := TOT.LINE<1,49>:"|"
        NN.DATA := TOT.LINE<1,50>:"|"
        NN.DATA := TOT.LINE<1,51>:"|"

        WRITESEQ NN.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
        TOT.LINE = ''
    END
RETURN
*--------------------------------------------------------
PROCESS.CONT:
    T.SEL = "SELECT ":FN.DLN:" WITH APPLIC.ID EQ 'Contingent' BY SORT.NO"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC><1,1>
            LINE.NO = R.LN<RE.SRL.DESC><1,2>

            IF I EQ 1 THEN NEW.LINE = LINE.NO
            IF NEW.LINE NE LINE.NO THEN
                NN.DATA  = "******":"|"
                NN.DATA := "������ ":NEW.LINE:"|"
                NN.DATA := TOT.LINE<1,1>:"|"
                NN.DATA := TOT.LINE<1,2>:"|"
                NN.DATA := TOT.LINE<1,3>:"|"
                NN.DATA := TOT.LINE<1,4>:"|"
                NN.DATA := TOT.LINE<1,5>:"|"
                NN.DATA := TOT.LINE<1,6>:"|"
                NN.DATA := TOT.LINE<1,7>:"|"
                NN.DATA := TOT.LINE<1,8>:"|"
                NN.DATA := TOT.LINE<1,9>:"|"
                NN.DATA := TOT.LINE<1,10>:"|"
                NN.DATA := TOT.LINE<1,11>:"|"
                NN.DATA := TOT.LINE<1,12>:"|"
                NN.DATA := TOT.LINE<1,13>:"|"
                NN.DATA := TOT.LINE<1,14>:"|"
                NN.DATA := TOT.LINE<1,15>:"|"
                NN.DATA := TOT.LINE<1,16>:"|"
                NN.DATA := TOT.LINE<1,17>:"|"
                NN.DATA := TOT.LINE<1,18>:"|"
                NN.DATA := TOT.LINE<1,19>:"|"
                NN.DATA := TOT.LINE<1,20>:"|"
                NN.DATA := TOT.LINE<1,21>:"|"
                NN.DATA := TOT.LINE<1,22>:"|"
                NN.DATA := TOT.LINE<1,23>:"|"
                NN.DATA := TOT.LINE<1,24>:"|"
                NN.DATA := TOT.LINE<1,25>:"|"
                NN.DATA := TOT.LINE<1,26>:"|"
                NN.DATA := TOT.LINE<1,27>:"|"
                NN.DATA := TOT.LINE<1,28>:"|"
                NN.DATA := TOT.LINE<1,29>:"|"
                NN.DATA := TOT.LINE<1,30>:"|"
                NN.DATA := TOT.LINE<1,31>:"|"
                NN.DATA := TOT.LINE<1,32>:"|"
                NN.DATA := TOT.LINE<1,33>:"|"
                NN.DATA := TOT.LINE<1,34>:"|"
                NN.DATA := TOT.LINE<1,35>:"|"
                NN.DATA := TOT.LINE<1,36>:"|"
                NN.DATA := TOT.LINE<1,37>:"|"
                NN.DATA := TOT.LINE<1,38>:"|"
                NN.DATA := TOT.LINE<1,39>:"|"
                NN.DATA := TOT.LINE<1,40>:"|"
                NN.DATA := TOT.LINE<1,41>:"|"
                NN.DATA := TOT.LINE<1,42>:"|"
                NN.DATA := TOT.LINE<1,43>:"|"
                NN.DATA := TOT.LINE<1,44>:"|"
                NN.DATA := TOT.LINE<1,45>:"|"
                NN.DATA := TOT.LINE<1,46>:"|"
                NN.DATA := TOT.LINE<1,47>:"|"
                NN.DATA := TOT.LINE<1,48>:"|"
                NN.DATA := TOT.LINE<1,49>:"|"
                NN.DATA := TOT.LINE<1,50>:"|"
                NN.DATA := TOT.LINE<1,51>:"|"

                WRITESEQ NN.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOT.LINE = ''
            END

            DESC.ID = FIELD(KEY.LIST<I>,".",2)

            PROD.CATEG = R.LN<RE.SRL.ASSET1,1>
            PL.CATEG   = R.LN<RE.SRL.PROFIT1,1>

            IF PROD.CATEG EQ '' THEN
                CATEG = PL.CATEG
            END ELSE
                CATEG = PROD.CATEG
            END

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CCURR = R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 1487 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(CCURR,@VM)
            CLOSE.BAL.LCL  = 0
            CLOSE.BAL  = 0
            OLD.BRN = ''
            NEW.BRN = ''
*************************************************************************
            FOR C = 1 TO 50
                CLOSE.BAL.LCL  = 0
                FOR X = 1 TO POS
                    BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":R.CCY<RE.BCP.ORIGINAL.CCY,X>:"-":TD1:"*":KK:NK<1,C>
                    CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)
                    NEW.BRN = KK:NK<1,C>

                    OLD.BRN = NEW.BRN

                    IF NEW.BRN NE OLD.BRN THEN
                        CLOSE.BAL.LCL = 0
                    END


                    IF NEW.BRN EQ OLD.BRN THEN
                        CLOSE.BAL.LCL += R.BAL<RE.SLB.CLOSING.BAL.LCL>
                    END

                    LINE.PRINT<1,C>   = KK:NK<1,C>
                    LINE.PRINT<1,C+1> = CLOSE.BAL.LCL

                    IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                        LINE.BR<1,1>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                        LINE.BR<1,2>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                        LINE.BR<1,3>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                        LINE.BR<1,4>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                        LINE.BR<1,5>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                        LINE.BR<1,6>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                        LINE.BR<1,7>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                        LINE.BR<1,8>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                        LINE.BR<1,9>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                        LINE.BR<1,10>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                        LINE.BR<1,11>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                        LINE.BR<1,12>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                        LINE.BR<1,13>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                        LINE.BR<1,14>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                        LINE.BR<1,15>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                        LINE.BR<1,16>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                        LINE.BR<1,17>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                        LINE.BR<1,18>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                        LINE.BR<1,19>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                        LINE.BR<1,20>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                        LINE.BR<1,21>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                        LINE.BR<1,22>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                        LINE.BR<1,23>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                        LINE.BR<1,24>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                        LINE.BR<1,25>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                        LINE.BR<1,26>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                        LINE.BR<1,27>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                        LINE.BR<1,28>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                        LINE.BR<1,29>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                        LINE.BR<1,30>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                        LINE.BR<1,31>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                        LINE.BR<1,32>   = LINE.PRINT<1,C+1>
                    END

                    IF LINE.PRINT<1,C> EQ 'EG0010017' THEN
                        LINE.BR<1,33>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010019' THEN
                        LINE.BR<1,34>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010052' THEN
                        LINE.BR<1,35>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010053' THEN
                        LINE.BR<1,36>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010018' THEN
                        LINE.BR<1,37>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010024' THEN
                        LINE.BR<1,38>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010025' THEN
                        LINE.BR<1,39>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010041' THEN
                        LINE.BR<1,40>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010026' THEN
                        LINE.BR<1,41>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010027' THEN
                        LINE.BR<1,42>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010029' THEN
                        LINE.BR<1,43>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010033' THEN
                        LINE.BR<1,44>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010034' THEN
                        LINE.BR<1,45>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010082' THEN
                        LINE.BR<1,46>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010083' THEN
                        LINE.BR<1,47>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010036' THEN
                        LINE.BR<1,48>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010045' THEN
                        LINE.BR<1,49>   = LINE.PRINT<1,C+1>
                    END
                    IF LINE.PRINT<1,C> EQ 'EG0010037' THEN
                        LINE.BR<1,50>   = LINE.PRINT<1,C+1>
                    END


                NEXT X

                IF LINE.PRINT<1,C> EQ 'EG0010099' THEN
                    TOT.LINE<1,1> += LINE.BR<1,1>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010001' THEN
                    TOT.LINE<1,2> += LINE.BR<1,2>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010002' THEN
                    TOT.LINE<1,3> += LINE.BR<1,3>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010003' THEN
                    TOT.LINE<1,4> +=  LINE.BR<1,4>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010004' THEN
                    TOT.LINE<1,5> +=   LINE.BR<1,5>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010005' THEN
                    TOT.LINE<1,6> +=  LINE.BR<1,6>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010006' THEN
                    TOT.LINE<1,7> +=  LINE.BR<1,7>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010007' THEN
                    TOT.LINE<1,8> +=  LINE.BR<1,8>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010009' THEN
                    TOT.LINE<1,9> +=  LINE.BR<1,9>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010010' THEN
                    TOT.LINE<1,10> +=  LINE.BR<1,10>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010011' THEN
                    TOT.LINE<1,11> +=  LINE.BR<1,11>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010012' THEN
                    TOT.LINE<1,12> +=  LINE.BR<1,12>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010013' THEN
                    TOT.LINE<1,13> +=  LINE.BR<1,13>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010014' THEN
                    TOT.LINE<1,14> +=  LINE.BR<1,14>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010015' THEN
                    TOT.LINE<1,15> +=  LINE.BR<1,15>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010016' THEN
                    TOT.LINE<1,16> +=  LINE.BR<1,16>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010020' THEN
                    TOT.LINE<1,17> +=  LINE.BR<1,17>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010021' THEN
                    TOT.LINE<1,18> +=  LINE.BR<1,18>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010022' THEN
                    TOT.LINE<1,19> +=  LINE.BR<1,19>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010023' THEN
                    TOT.LINE<1,20> +=  LINE.BR<1,20>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010030' THEN
                    TOT.LINE<1,21> +=  LINE.BR<1,21>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010031' THEN
                    TOT.LINE<1,22> +=  LINE.BR<1,22>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010032' THEN
                    TOT.LINE<1,23> +=  LINE.BR<1,23>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010035' THEN
                    TOT.LINE<1,24> +=  LINE.BR<1,24>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010040' THEN
                    TOT.LINE<1,25> +=  LINE.BR<1,25>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010050' THEN
                    TOT.LINE<1,26> +=  LINE.BR<1,26>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010051' THEN
                    TOT.LINE<1,27> +=  LINE.BR<1,27>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010060' THEN
                    TOT.LINE<1,28> +=  LINE.BR<1,28>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010070' THEN
                    TOT.LINE<1,29> +=  LINE.BR<1,29>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010080' THEN
                    TOT.LINE<1,30> +=  LINE.BR<1,30>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010081' THEN
                    TOT.LINE<1,31> +=  LINE.BR<1,31>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010090' THEN
                    TOT.LINE<1,32> +=  LINE.BR<1,32>
                END

                IF LINE.PRINT<1,C> EQ 'EG0010017' THEN
                    TOT.LINE<1,33> +=  LINE.BR<1,33>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010019' THEN
                    TOT.LINE<1,34> +=  LINE.BR<1,34>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010052' THEN
                    TOT.LINE<1,35> +=  LINE.BR<1,35>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010053' THEN
                    TOT.LINE<1,36> +=  LINE.BR<1,36>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010018' THEN
                    TOT.LINE<1,37> +=  LINE.BR<1,37>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010024' THEN
                    TOT.LINE<1,38> +=  LINE.BR<1,38>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010025' THEN
                    TOT.LINE<1,39> +=  LINE.BR<1,39>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010041' THEN
                    TOT.LINE<1,40> +=  LINE.BR<1,40>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010026' THEN
                    TOT.LINE<1,41> +=  LINE.BR<1,41>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010027' THEN
                    TOT.LINE<1,42> +=  LINE.BR<1,42>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010029' THEN
                    TOT.LINE<1,43> +=  LINE.BR<1,43>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010033' THEN
                    TOT.LINE<1,44> +=  LINE.BR<1,44>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010034' THEN
                    TOT.LINE<1,45> +=  LINE.BR<1,45>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010082' THEN
                    TOT.LINE<1,46> +=  LINE.BR<1,46>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010083' THEN
                    TOT.LINE<1,47> +=  LINE.BR<1,47>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010036' THEN
                    TOT.LINE<1,48> +=  LINE.BR<1,48>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010045' THEN
                    TOT.LINE<1,49> +=  LINE.BR<1,49>
                END
                IF LINE.PRINT<1,C> EQ 'EG0010037' THEN
                    TOT.LINE<1,50> +=  LINE.BR<1,50>
                END


            NEXT C

            BB.DATA  = CATEG:"|"
            BB.DATA := DESC:"|"
            BB.DATA := LINE.BR<1,1>:"|"
            BB.DATA := LINE.BR<1,2>:"|"
            BB.DATA := LINE.BR<1,3>:"|"
            BB.DATA := LINE.BR<1,4>:"|"
            BB.DATA := LINE.BR<1,5>:"|"
            BB.DATA := LINE.BR<1,6>:"|"
            BB.DATA := LINE.BR<1,7>:"|"
            BB.DATA := LINE.BR<1,8>:"|"
            BB.DATA := LINE.BR<1,9>:"|"
            BB.DATA := LINE.BR<1,10>:"|"
            BB.DATA := LINE.BR<1,11>:"|"
            BB.DATA := LINE.BR<1,12>:"|"
            BB.DATA := LINE.BR<1,13>:"|"
            BB.DATA := LINE.BR<1,14>:"|"
            BB.DATA := LINE.BR<1,15>:"|"
            BB.DATA := LINE.BR<1,16>:"|"
            BB.DATA := LINE.BR<1,17>:"|"
            BB.DATA := LINE.BR<1,18>:"|"
            BB.DATA := LINE.BR<1,19>:"|"
            BB.DATA := LINE.BR<1,20>:"|"
            BB.DATA := LINE.BR<1,21>:"|"
            BB.DATA := LINE.BR<1,22>:"|"
            BB.DATA := LINE.BR<1,23>:"|"
            BB.DATA := LINE.BR<1,24>:"|"
            BB.DATA := LINE.BR<1,25>:"|"
            BB.DATA := LINE.BR<1,26>:"|"
            BB.DATA := LINE.BR<1,27>:"|"
            BB.DATA := LINE.BR<1,28>:"|"
            BB.DATA := LINE.BR<1,29>:"|"
            BB.DATA := LINE.BR<1,30>:"|"
            BB.DATA := LINE.BR<1,31>:"|"
            BB.DATA := LINE.BR<1,32>:"|"
            BB.DATA := LINE.BR<1,33>:"|"
            BB.DATA := LINE.BR<1,34>:"|"
            BB.DATA := LINE.BR<1,35>:"|"
            BB.DATA := LINE.BR<1,36>:"|"
            BB.DATA := LINE.BR<1,37>:"|"
            BB.DATA := LINE.BR<1,38>:"|"
            BB.DATA := LINE.BR<1,39>:"|"
            BB.DATA := LINE.BR<1,40>:"|"
            BB.DATA := LINE.BR<1,41>:"|"
            BB.DATA := LINE.BR<1,42>:"|"
            BB.DATA := LINE.BR<1,43>:"|"
            BB.DATA := LINE.BR<1,44>:"|"
            BB.DATA := LINE.BR<1,45>:"|"
            BB.DATA := LINE.BR<1,46>:"|"
            BB.DATA := LINE.BR<1,47>:"|"
            BB.DATA := LINE.BR<1,48>:"|"
            BB.DATA := LINE.BR<1,49>:"|"
            BB.DATA := LINE.BR<1,50>:"|"

            FOR AK = 1 TO 50
                LINE.BR<1,51> += LINE.BR<1,AK>
            NEXT AK
            BB.DATA := LINE.BR<1,51>:"|"

            TOT.LINE<1,51> += LINE.BR<1,51>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            LINE.BR<1,51> = 0
            NEW.LINE = LINE.NO

        NEXT I

        IF I EQ SELECTED THEN
            NN.DATA  = "******":"|"
            NN.DATA := "������ ":NEW.LINE:"|"
            NN.DATA := TOT.LINE<1,1>:"|"
            NN.DATA := TOT.LINE<1,2>:"|"
            NN.DATA := TOT.LINE<1,3>:"|"
            NN.DATA := TOT.LINE<1,4>:"|"
            NN.DATA := TOT.LINE<1,5>:"|"
            NN.DATA := TOT.LINE<1,6>:"|"
            NN.DATA := TOT.LINE<1,7>:"|"
            NN.DATA := TOT.LINE<1,8>:"|"
            NN.DATA := TOT.LINE<1,9>:"|"
            NN.DATA := TOT.LINE<1,10>:"|"
            NN.DATA := TOT.LINE<1,11>:"|"
            NN.DATA := TOT.LINE<1,12>:"|"
            NN.DATA := TOT.LINE<1,13>:"|"
            NN.DATA := TOT.LINE<1,14>:"|"
            NN.DATA := TOT.LINE<1,15>:"|"
            NN.DATA := TOT.LINE<1,16>:"|"
            NN.DATA := TOT.LINE<1,17>:"|"
            NN.DATA := TOT.LINE<1,18>:"|"
            NN.DATA := TOT.LINE<1,19>:"|"
            NN.DATA := TOT.LINE<1,20>:"|"
            NN.DATA := TOT.LINE<1,21>:"|"
            NN.DATA := TOT.LINE<1,22>:"|"
            NN.DATA := TOT.LINE<1,23>:"|"
            NN.DATA := TOT.LINE<1,24>:"|"
            NN.DATA := TOT.LINE<1,25>:"|"
            NN.DATA := TOT.LINE<1,26>:"|"
            NN.DATA := TOT.LINE<1,27>:"|"
            NN.DATA := TOT.LINE<1,28>:"|"
            NN.DATA := TOT.LINE<1,29>:"|"
            NN.DATA := TOT.LINE<1,30>:"|"
            NN.DATA := TOT.LINE<1,31>:"|"
            NN.DATA := TOT.LINE<1,32>:"|"
            NN.DATA := TOT.LINE<1,33>:"|"
            NN.DATA := TOT.LINE<1,34>:"|"
            NN.DATA := TOT.LINE<1,35>:"|"
            NN.DATA := TOT.LINE<1,36>:"|"
            NN.DATA := TOT.LINE<1,37>:"|"
            NN.DATA := TOT.LINE<1,38>:"|"
            NN.DATA := TOT.LINE<1,39>:"|"
            NN.DATA := TOT.LINE<1,40>:"|"
            NN.DATA := TOT.LINE<1,41>:"|"
            NN.DATA := TOT.LINE<1,42>:"|"
            NN.DATA := TOT.LINE<1,43>:"|"
            NN.DATA := TOT.LINE<1,44>:"|"
            NN.DATA := TOT.LINE<1,45>:"|"
            NN.DATA := TOT.LINE<1,46>:"|"
            NN.DATA := TOT.LINE<1,47>:"|"
            NN.DATA := TOT.LINE<1,48>:"|"
            NN.DATA := TOT.LINE<1,49>:"|"
            NN.DATA := TOT.LINE<1,50>:"|"
            NN.DATA := TOT.LINE<1,51>:"|"

            WRITESEQ NN.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            TOT.LINE = ''
        END

    END
RETURN
