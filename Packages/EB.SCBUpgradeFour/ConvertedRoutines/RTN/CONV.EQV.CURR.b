* @ValidationCode : MjoyMTA4NDY5MTI5OkNwMTI1MjoxNjQxMDk3NjM4MjQyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:27:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
****NESSREEN AHMED
SUBROUTINE CONV.EQV.CURR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VISA.APP
*missing layout
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY
*missing layout
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

*-------------------------------------------
*****UPDATED BY NESSREEN AHMED 16/1/2012***************************
*****ID.TO.USE = 'NZD'
    CURR      = O.DATA

*****    FN.BASE.CURR = 'F.RE.BASE.CCY.PARAM' ; F.BASE.CURR = '' ; R.BASE.CURR = '' ; RETRY = '' ; E1 = ''
*****    CALL OPF(FN.BASE.CURR,F.BASE.CURR)
*****    CALL F.READ(FN.BASE.CURR, ID.TO.USE, R.BASE.CURR, F.BASE.CURR, E1)

*****    LOCATE CURR IN R.BASE.CURR<RE.BCP.ORIGINAL.CCY,1> SETTING CC THEN
*****       EQV.RAT = R.BASE.CURR<RE.BCP.RATE,CC>
*****    END

********************REHAM YOUSSIF 1/3/2012*********
    FN.CUR  = 'F.SBD.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ECAA)
    EQV.RAT = R.CUR<SBD.CURR.MID.RATE>
    IF CURR EQ 'EGP' THEN
        EQV.RAT = 1
    END

* CASH.C = '1'
* LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
    IF CURR EQ 'JPY' THEN

*CUR.RATE = ( CUR.RATE / 100 )
        EQV.RAT = (EQV.RAT / 100)
    END
*******************END UPDATE *******************

*END ELSE
*    EQV.RAT = 1
*END
    O.DATA = EQV.RAT
*---------------------------------------------
RETURN
END
