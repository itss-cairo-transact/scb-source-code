* @ValidationCode : MjotMTc2NjY3NTg3NDpDcDEyNTI6MTY0MTA5ODA1MzIxNTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:34:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*--------------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.F9
*--------------------------------------------------
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RISK.MAST
*--------------------------------------------------
    WS.COMP = ID.COMPANY
    WS.ID   = O.DATA

    FN.RISK = "F.SCB.RISK.MAST"   ; F.RISK = ""
    CALL OPF(FN.RISK,F.RISK)

    WS.TOT = 0
*---------------------------------------------------
    CALL F.READ(FN.RISK,WS.ID,R.RISK,F.RISK,E.TXT)
    F7 = R.RISK<RM.CLASS.ITEM.COD.23>
    IF F7[1,2] EQ '09' THEN
        WS.TOT = WS.TOT + F7[5,2]
    END

    F8 = R.RISK<RM.CLASS.ITEM.COD.24>
    IF F8[1,2] EQ '09' THEN
        WS.TOT = WS.TOT + F8[5,2]
    END
*----------------------------------------
    O.DATA = WS.TOT
*----------------------------------------
RETURN
END
