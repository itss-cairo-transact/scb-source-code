* @ValidationCode : MjotODM2NzYxNDM0OkNwMTI1MjoxNjQxMTM3MTE3NzU3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 17:25:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.LQDF.TOT.ROW.A

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LQDDTY.DAILY.FILE

    WS.LQDF.ID = O.DATA:"...."
* TEXT= WS.LQDF.ID ; CALL REM
    FN.LQDF = 'F.SCB.LQDDTY.DAILY.FILE'
    F.LQDF = '' ; R.LQDF = ''
    CALL OPF(FN.LQDF,F.LQDF)
************************************************************
    T.SEL  = "SELECT ":FN.LQDF:" WITH @ID LIKE ":WS.LQDF.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LQDF,KEY.LIST<I>,R.LQDF,F.LQDF,EER)
            TOT.ROW.A.AMT  +=  R.LQDF<LQDF.TOTAL.ROW.A>
        NEXT I
    END
    O.DATA = TOT.ROW.A.AMT
RETURN
