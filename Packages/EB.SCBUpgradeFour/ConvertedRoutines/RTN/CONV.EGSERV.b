* @ValidationCode : MjoxODA1NDM1MjU0OkNwMTI1MjoxNjQxMDk3NDc2MDgxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:24:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.EGSERV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EGSRV.INDEX
*missing layout

    WS.CU.ID = O.DATA
    O.DATA = ' '
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CU.ID,WS.LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CU.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
WS.LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

    WS.EGSERV.ID = WS.LOCAL.REF<1,CULR.EGSRV.SRL>
    WS.CU.GOV    = WS.LOCAL.REF<1,CULR.GOVERNORATE>

*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.EGSRV.INDEX':@FM:EGSRV.EGSERV.CODE,WS.EGSERV.ID,WS.EGSERV.CODE)
F.ITSS.SCB.EGSRV.INDEX = 'F.SCB.EGSRV.INDEX'
FN.F.ITSS.SCB.EGSRV.INDEX = ''
CALL OPF(F.ITSS.SCB.EGSRV.INDEX,FN.F.ITSS.SCB.EGSRV.INDEX)
CALL F.READ(F.ITSS.SCB.EGSRV.INDEX,WS.EGSERV.ID,R.ITSS.SCB.EGSRV.INDEX,FN.F.ITSS.SCB.EGSRV.INDEX,ERROR.SCB.EGSRV.INDEX)
WS.EGSERV.CODE=R.ITSS.SCB.EGSRV.INDEX<EGSRV.EGSERV.CODE>
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.EGSRV.INDEX':@FM:EGSRV.EGSERV.BAR,WS.EGSERV.ID,WS.EGSERV.BAR)
F.ITSS.SCB.EGSRV.INDEX = 'F.SCB.EGSRV.INDEX'
FN.F.ITSS.SCB.EGSRV.INDEX = ''
CALL OPF(F.ITSS.SCB.EGSRV.INDEX,FN.F.ITSS.SCB.EGSRV.INDEX)
CALL F.READ(F.ITSS.SCB.EGSRV.INDEX,WS.EGSERV.ID,R.ITSS.SCB.EGSRV.INDEX,FN.F.ITSS.SCB.EGSRV.INDEX,ERROR.SCB.EGSRV.INDEX)
WS.EGSERV.BAR=R.ITSS.SCB.EGSRV.INDEX<EGSRV.EGSERV.BAR>

*Line [ 44 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    IF (NOT(WS.EGSERV.BAR) OR WS.CU.GOV EQ '98' OR WS.CU.GOV EQ '97') THEN WS.EGSERV.BAR = STR('_',26) ELSE NULL

    WS.CU.ID = 100000000 + WS.CU.ID

    O.DATA = WS.CU.ID[8] :' ': WS.EGSERV.ID :' ': WS.EGSERV.CODE :' ': WS.EGSERV.BAR

RETURN
END
