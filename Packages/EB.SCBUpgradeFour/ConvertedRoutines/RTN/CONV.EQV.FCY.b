* @ValidationCode : MjotMTc3MDY2OTgwMjpDcDEyNTI6MTY0MTA5ODI3MzQwODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 02 Jan 2022 06:37:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
****NESSREEN AHMED 22/4/2010***********
SUBROUTINE CONV.EQV.FCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 32 ] Removed I_F.SCB.VISA.APP cause it is not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.VISA.APP

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

    EQV.AMT = ''
*****UPDATED BY NESSREEN AHMED 15/1/2012****************************
*****ID.TO.USE = 'NZD'
    CURR = O.DATA[1,3]
    XX = (LEN(O.DATA) - 3)
    AMT = O.DATA[4,XX]
***** FN.BASE.CURR = 'F.RE.BASE.CCY.PARAM' ; F.BASE.CURR = '' ; R.BASE.CURR = '' ; RETRY = '' ; E1 = ''
***** CALL OPF(FN.BASE.CURR,F.BASE.CURR)
***** CALL F.READ(FN.BASE.CURR, ID.TO.USE, R.BASE.CURR, F.BASE.CURR, E1)
***** IF CURR EQ 'EGP' THEN
***** EQV.AMT = AMT
***** END ELSE
***** LOCATE CURR IN R.BASE.CURR<RE.BCP.ORIGINAL.CCY,1> SETTING CC THEN
***** EQV.RAT = R.BASE.CURR<RE.BCP.RATE,CC>
***** EQV.AMT = EQV.RAT * AMT
***** END
***** END
    IF CURR NE 'EGP' THEN
        FN.CUR  = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ECAA)
        CASH.C = '1'
*Line [ 59 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
        EQV.RAT = R.CUR<EB.CUR.MID.REVAL.RATE,POS>
        EQV.AMT = EQV.RAT * AMT
    END ELSE
        EQV.AMT = AMT
    END
****** END OF UPDATE 15/1/2012*************************************
**  O.DATA = EQV.RAT
    O.DATA = EQV.AMT

RETURN
END
