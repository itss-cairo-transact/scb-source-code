* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE CONV.LAST.VALUE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR

*   IF O.DATA THEN

    TD1 = TODAY[1,6]:"01"
    CALL CDT("",TD1,'-1C')
    TDD = O.DATA:"-":TD1

*        T.SEL = "SELECT FBNK.STMT.ACCT.CR WITH @ID LIKE ":TDD:" BY PERIOD.FIRST.DATE"
*        KEY.LIST=""
*        SELECTED=""
*        ER.MSG=""

*       CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    O.DATA = TDD
*KEY.LIST<SELECTED>
*END
*   ------------------------------------------------------------------------------------
**    IF O.DATA THEN
**        TD1 = TODAY
**        CALL CDT("",TD1,'-2W')
**        TDD = O.DATA:"-":TD1[1,6]:"..."

**        T.SEL = "SELECT FBNK.STMT.ACCT.CR WITH @ID LIKE ":TDD:" BY PERIOD.FIRST.DATE"
**        KEY.LIST=""
**        SELECTED=""
**        ER.MSG=""

**        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**        O.DATA = KEY.LIST<SELECTED>

**    END

    RETURN
END
