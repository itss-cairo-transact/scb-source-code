* @ValidationCode : MjotMTE1MzgyMDIzOTpDcDEyNTI6MTY0MjQxODEzMjE2Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 17 Jan 2022 13:15:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*----RANIA & ABEER ------*
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.LG.LIMIT


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 31 ] ADDED I_F.LIMIT - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT

    IF O.DATA THEN

        MYID=FIELD(O.DATA,"/",1)
        LIM.REF = FMT(LIM.REF, "R%10")
        LIM  = O.DATA :".":LIM.REF
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVL=R.ITSS.LIMIT<LI.AVAIL.AMT>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LIMIT':@FM:LI.COMMT.AMT.AVAIL,LIM,USED.AVL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
USED.AVL=R.ITSS.LIMIT<LI.COMMT.AMT.AVAIL>
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LIMIT':@FM:LI.INTERNAL.AMOUNT,LIM,INT.AMT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
INT.AMT=R.ITSS.LIMIT<LI.INTERNAL.AMOUNT>
        O.DATA = AVL:"/":USED.AVL:"/":INT.AMT
    END
RETURN
END
