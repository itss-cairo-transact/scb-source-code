* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
    SUBROUTINE CONV.HM.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MENU
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MAINMENU
    XX = O.DATA
    WS.ENQ.NAME = 'ENQ ':O.DATA
    FN.HM  = 'F.HELPTEXT.MENU'
    FN.HMM = 'F.HELPTEXT.MAINMENU'
    F.HM  = '' ; R.HM  = ''
    F.HMM = '' ; R.HMM = ''
    CALL OPF(FN.HM,F.HM)
    CALL OPF(FN.HMM,F.HMM)
************************************************************
    T.SEL  = "SELECT ":FN.HM:" WITH APPLICATION EQ '":WS.ENQ.NAME:"' OR APPLICATION EQ ":O.DATA
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        O.DATA = 'YES'
    END ELSE
        T.SEL2  = "SELECT ":FN.HMM:" WITH ID.OF.HELP.MENU EQ ":O.DATA
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED1,ER.MSG)
        IF SELECTED1 THEN
            O.DATA = 'YES'
        END  ELSE
            O.DATA = ' * '
        END
    END
    RETURN
