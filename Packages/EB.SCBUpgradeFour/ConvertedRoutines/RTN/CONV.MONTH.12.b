* @ValidationCode : MjotODUyOTQ4Mzc5OkNwMTI1MjoxNjQxMjAyMzU0MTE3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 03 Jan 2022 11:32:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.MONTH.12

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_ENQUIRY.COMMON
    $INSERT  I_F.USER
    $INSERT  I_F.SCB.PASSWORD.UPD
*----------------------------------------------------------
    FN.PASS  = 'F.SCB.PASSWORD.UPD'  ; F.PASS = ''
    CALL OPF(FN.PASS,F.PASS)

    GETT.IDD  = O.DATA
    USER.ID   = FIELD(GETT.IDD,'*',1)
    COMP.ID   = FIELD(GETT.IDD,'*',2)
    SEL.COUNT = 0
*-----------------------------------------------
    DATE.TOD  = TODAY
    CALL ADD.MONTHS(DATE.TOD, '-1')

    FROM.DATE = DATE.TOD[1,6] : "01"
    END.DATE  = FROM.DATE
    CALL LAST.DAY(END.DATE)

    T.SEL  = "SELECT ":FN.PASS :" WITH USER.ID EQ ":USER.ID
    T.SEL := " AND BOOKING.DATE GE " : FROM.DATE
    T.SEL := " AND BOOKING.DATE LE " : END.DATE
    T.SEL := " AND COMPANY.ID EQ "   : COMP.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.PASS,KEY.LIST<II>,R.PASS,F.PASS,ER.PASS)
            CURR.NO    = R.PASS<PWR.CURR.NO>
            SEL.COUNT += CURR.NO
        NEXT II
    END

    O.DATA = SEL.COUNT:"*":FROM.DATE[1,4]:"/":FROM.DATE[5,2]
RETURN
END
