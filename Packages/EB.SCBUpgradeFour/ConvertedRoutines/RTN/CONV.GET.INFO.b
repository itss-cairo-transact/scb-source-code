* @ValidationCode : MjotNzM2NjI1MjE6Q3AxMjUyOjE2NDEzODExMzY3NDc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 05 Jan 2022 13:12:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
*--------------------------------------------------
SUBROUTINE CONV.GET.INFO
*--------------------------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_ENQUIRY.COMMON
*--------------------------------------------------
    FN.USR = "F.USER"             ; F.USR = ""
    CALL OPF(FN.USR,F.USR)

    FN.USR.H = "F.USER$HIS"       ; F.USR.H = ""
    CALL OPF(FN.USR.H,F.USR.H)
*---------------------------------------------------
    CALL F.READ(FN.USR,O.DATA,R.USRR,F.USR,E.USR)
    CURR.NO = R.USRR<EB.USE.CURR.NO>
    END.TIM = R.USRR<EB.USE.END.TIME>

    FOR II = 1 TO CURR.NO
* HIS.II    = O.DATA : ";" : 0II
*Line [ 40 ] removed 0 from 0II  - ITSS - R21 Upgrade - 2021-12-23
        HIS.II    = O.DATA : ";" : II
        WW        = II - 1
        HIS.WW    = O.DATA : ";" : WW

        CALL F.READ(FN.USR.H,HIS.II,R.USRR.H,F.USR.H,E.USR.H)
        END.TIM.H = R.USRR<EB.USE.END.TIME>

        CALL F.READ(FN.USR.H,HIS.WW,R.USRR.H,F.USR.H,E.USR.H)
        END.TIM.W = R.USRR.H<EB.USE.END.TIME>

        IF END.TIM.W NE END.TIM.H THEN
            IF END.TIM.H GT 1730 THEN
                INP  = R.USRR<EB.USE.INPUTTER>
                AUTH = R.USRR<EB.USE.AUTHORISER>
                DAT  = R.USRR<EB.USE.DATE.TIME,1>
            END
        END
    NEXT II

    INP  = FIELD(INP,'_',2)
    AUTH = FIELD(AUTH,'_',2)
    DAT  = "20":DAT[1,6]
*---------------------------------------------------
    O.DATA = INP : "*" : AUTH : "*" : DAT
*---------------------------------------------------
RETURN
END
