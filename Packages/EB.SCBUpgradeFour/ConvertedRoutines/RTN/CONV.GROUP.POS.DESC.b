* @ValidationCode : Mjo0NjE4MzA4MTk6Q3AxMjUyOjE2NDEyODY4NTQzNzI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 04 Jan 2022 11:00:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.GROUP.POS.DESC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.GROUP

    WS.DESC = ': ������ ��������� '
    WS.CUS.ID = O.DATA

*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    WS.GROUP.NUM  = LOCAL.REF<1,CULR.GROUP.NUM>

*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUSTOMER.GROUP':@FM:CG.GROUP.NAME,WS.GROUP.NUM,WS.GROUP.NAME)
F.ITSS.SCB.CUSTOMER.GROUP = 'F.SCB.CUSTOMER.GROUP'
FN.F.ITSS.SCB.CUSTOMER.GROUP = ''
CALL OPF(F.ITSS.SCB.CUSTOMER.GROUP,FN.F.ITSS.SCB.CUSTOMER.GROUP)
CALL F.READ(F.ITSS.SCB.CUSTOMER.GROUP,WS.GROUP.NUM,R.ITSS.SCB.CUSTOMER.GROUP,FN.F.ITSS.SCB.CUSTOMER.GROUP,ERROR.SCB.CUSTOMER.GROUP)
WS.GROUP.NAME=R.ITSS.SCB.CUSTOMER.GROUP<CG.GROUP.NAME>
    IF WS.GROUP.NUM[1,3] EQ '999' THEN
        O.DATA = ''
        RETURN
    END
    IF WS.GROUP.NUM # '' THEN
        O.DATA = WS.GROUP.NAME : "    " : WS.GROUP.NUM : "         " : WS.DESC
    END ELSE
        O.DATA = ''
    END
RETURN
