* @ValidationCode : MjotNDgzMDkyOTY6Q3AxMjUyOjE2NDExMzY3MDIwNDA6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 02 Jan 2022 17:18:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.LI1.LCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBM.LG.SECTOR.1

    XX = O.DATA
    AMT = FIELD(XX,"-",1)
    LIS = FIELD(XX,"-",2)
    SEC = FIELD(XX,"-",3)
    MRG.500.LCY.1 = 0
    MRG.110.LCY.1 = 0
    MRG.OTH.LCY.1 = 0
    MRG.500.LCY.2 = 0
    MRG.110.LCY.2 = 0
    MRG.OTH.LCY.2 = 0
    FIN.AMT       = 0

    FN.LGS = 'F.SBM.LG.SECTOR.1' ; F.LGS  = ''
    CALL OPF(FN.LGS,F.LGS)
************************************************************
    CALL F.READ(FN.LGS,'MRG',R.LGS,F.LGS,E1)

    MRG.500.LCY.1 = R.LGS<RE.MRG.500.LCY.1>
    MRG.110.LCY.1 = R.LGS<RE.MRG.110.LCY.1>
    MRG.OTH.LCY.1 = R.LGS<RE.MRG.OTH.LCY.1>
    MRG.500.LCY.2 = R.LGS<RE.MRG.500.LCY.2>
    MRG.110.LCY.2 = R.LGS<RE.MRG.110.LCY.2>
    MRG.OTH.LCY.2 = R.LGS<RE.MRG.OTH.LCY.2>

    BEGIN CASE
        CASE ( LIS EQ 1 AND SEC EQ 1 )
            FIN.AMT = AMT - MRG.500.LCY.1

        CASE ( LIS EQ 1 AND SEC EQ 2 )
            FIN.AMT = AMT - MRG.110.LCY.1

        CASE ( LIS EQ 1 AND SEC EQ 3 )
            FIN.AMT = AMT - MRG.OTH.LCY.1

        CASE ( LIS EQ 2 AND SEC EQ 1 )
            FIN.AMT = AMT - MRG.500.LCY.2

        CASE ( LIS EQ 2 AND SEC EQ 2 )
            FIN.AMT = AMT - MRG.110.LCY.2

        CASE ( LIS EQ 2 AND SEC EQ 3 )
            FIN.AMT = AMT - MRG.OTH.LCY.2

        CASE OTHERWISE
            FIN.AMT = AMT
    END CASE


    O.DATA = FIN.AMT
RETURN
