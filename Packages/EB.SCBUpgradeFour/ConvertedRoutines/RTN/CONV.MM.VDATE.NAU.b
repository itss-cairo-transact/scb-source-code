* @ValidationCode : MjoxOTUxOTQ4OkNwMTI1MjoxNjQxMTk2MzQ5MzkxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 03 Jan 2022 09:52:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFour  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFour
*DONE
SUBROUTINE CONV.MM.VDATE.NAU

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 26 ] Removed I_MM.LOCAL.REFS not used - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_MM.LOCAL.REFS

    XX = O.DATA
    FN.MM = 'FBNK.MM.MONEY.MARKET$NAU'
    F.MM = '' ; R.MM = ''
    CALL OPF(FN.MM,F.MM)

    CALL F.READ(FN.MM,XX,R.MM,F.MM,E1)
    ROL.DATE = R.MM<MM.ROLLOVER.DATE>
    INT.DATE = R.MM<MM.INT.PERIOD.START>
    VAL.DATE = R.MM<MM.VALUE.DATE>

    IF ROL.DATE NE '' THEN
        O.DATA = ROL.DATE
    END ELSE
        IF INT.DATE NE '' THEN
            O.DATA = INT.DATE
        END ELSE
            IF VAL.DATE NE '' THEN
                O.DATA = VAL.DATE
            END
        END
    END


RETURN
