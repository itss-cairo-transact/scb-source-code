public method ConvCustomerBranch
 (
)
{ 
	 jBC: CONV.CUSTOMER.BRANCH

}


public method ConvDataFilter
 (
)
{ 
	 jBC: CONV.DATA.FILTER

}


public method ConvDataInp
 (
)
{ 
	 jBC: CONV.DATA.INP

}


public method ConvDateFormt
 (
)
{ 
	 jBC: CONV.DATE.FORMT

}


public method ConvDateStmt
 (
)
{ 
	 jBC: CONV.DATE.STMT

}


public method ConvDepBucket
 (
)
{ 
	 jBC: CONV.DEP.BUCKET

}


public method ConvDepRate
 (
)
{ 
	 jBC: CONV.DEP.RATE

}


public method ConvDepositAmount
 (
)
{ 
	 jBC: CONV.DEPOSIT.AMOUNT

}


public method ConvDepositInterest
 (
)
{ 
	 jBC: CONV.DEPOSIT.INTEREST

}


public method ConvDepositPriceList
 (
)
{ 
	 jBC: CONV.DEPOSIT.PRICE.LIST

}


public method ConvDesc
 (
)
{ 
	 jBC: CONV.DESC

}


public method ConvDrawingsAmt
 (
)
{ 
	 jBC: CONV.DRAWINGS.AMT

}


public method ConvEgserv
 (
)
{ 
	 jBC: CONV.EGSERV

}


public method ConvEngDis
 (
)
{ 
	 jBC: CONV.ENG.DIS

}


public method ConvEnqRepChk
 (
)
{ 
	 jBC: CONV.ENQ.REP.CHK

}


public method ConvEquiv
 (
)
{ 
	 jBC: CONV.EQUIV

}


public method ConvEqvCur
 (
)
{ 
	 jBC: CONV.EQV.CUR

}


public method ConvEqvCurr
 (
)
{ 
	 jBC: CONV.EQV.CURR

}


public method ConvEqvFcy
 (
)
{ 
	 jBC: CONV.EQV.FCY

}


public method ConvEqvUsd
 (
)
{ 
	 jBC: CONV.EQV.USD

}


public method ConvExcCur
 (
)
{ 
	 jBC: CONV.EXC.CUR

}


public method ConvF3
 (
)
{ 
	 jBC: CONV.F3

}


public method ConvF4
 (
)
{ 
	 jBC: CONV.F4

}


public method ConvF5
 (
)
{ 
	 jBC: CONV.F5

}


public method ConvF6
 (
)
{ 
	 jBC: CONV.F6

}


public method ConvF7
 (
)
{ 
	 jBC: CONV.F7

}


public method ConvF9
 (
)
{ 
	 jBC: CONV.F9

}


public method ConvFatcaUsd
 (
)
{ 
	 jBC: CONV.FATCA.USD

}


public method ConvFormCurH
 (
)
{ 
	 jBC: CONV.FORM.CUR.H

}


public method ConvFormDat
 (
)
{ 
	 jBC: CONV.FORM.DAT

}


public method ConvFormDatH
 (
)
{ 
	 jBC: CONV.FORM.DAT.H

}


public method ConvFormDatHh
 (
)
{ 
	 jBC: CONV.FORM.DAT.HH

}


public method ConvFormTime
 (
)
{ 
	 jBC: CONV.FORM.TIME

}


public method ConvFtComp
 (
)
{ 
	 jBC: CONV.FT.COMP

}


public method ConvFtInterbankEgpCr
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.EGP.CR

}


public method ConvFtInterbankEgpCrNau
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.EGP.CR.NAU

}


public method ConvFtInterbankEgpDr
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.EGP.DR

}


public method ConvFtInterbankEgpDrNau
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.EGP.DR.NAU

}


public method ConvFtInterbankUsdCr
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.USD.CR

}


public method ConvFtInterbankUsdCrNau
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.USD.CR.NAU

}


public method ConvFtInterbankUsdDr
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.USD.DR

}


public method ConvFtInterbankUsdDrNau
 (
)
{ 
	 jBC: CONV.FT.INTERBANK.USD.DR.NAU

}


public method ConvFtRev
 (
)
{ 
	 jBC: CONV.FT.REV

}


public method ConvFtTrnsCode
 (
)
{ 
	 jBC: CONV.FT.TRNS.CODE

}


public method ConvFtcusCdamt
 (
)
{ 
	 jBC: CONV.FTCUS.CDAMT

}


public method ConvFxTotalEgpBuy
 (
)
{ 
	 jBC: CONV.FX.TOTAL.EGP.BUY

}


public method ConvFxTotalEgpBuyNau
 (
)
{ 
	 jBC: CONV.FX.TOTAL.EGP.BUY.NAU

}


public method ConvFxTotalEgpSold
 (
)
{ 
	 jBC: CONV.FX.TOTAL.EGP.SOLD

}


public method ConvFxTotalEgpSoldNau
 (
)
{ 
	 jBC: CONV.FX.TOTAL.EGP.SOLD.NAU

}


public method ConvFxTotalUsdBuy
 (
)
{ 
	 jBC: CONV.FX.TOTAL.USD.BUY

}


public method ConvFxTotalUsdBuyNau
 (
)
{ 
	 jBC: CONV.FX.TOTAL.USD.BUY.NAU

}


public method ConvFxTotalUsdSold
 (
)
{ 
	 jBC: CONV.FX.TOTAL.USD.SOLD

}


public method ConvFxTotalUsdSoldNau
 (
)
{ 
	 jBC: CONV.FX.TOTAL.USD.SOLD.NAU

}


public method ConvGciCur
 (
)
{ 
	 jBC: CONV.GCI.CUR

}


public method ConvGciDate
 (
)
{ 
	 jBC: CONV.GCI.DATE

}


public method ConvGciId
 (
)
{ 
	 jBC: CONV.GCI.ID

}


public method ConvGdiCur
 (
)
{ 
	 jBC: CONV.GDI.CUR

}


public method ConvGdiDate
 (
)
{ 
	 jBC: CONV.GDI.DATE

}


public method ConvGdiId
 (
)
{ 
	 jBC: CONV.GDI.ID

}


public method ConvGenledallBranchDetailAvrg
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.DETAIL.AVRG

}


public method ConvGenledallBranchDetail
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.DETAIL

}


public method ConvGenledallBranchDetailEgp
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.DETAIL.EGP

}


public method ConvGenledallBranchDetailFcy
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.DETAIL.FCY

}


public method ConvGenledallBranchDetailY2
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.DETAIL.Y2

}


public method ConvGenledallBranchEgp
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.EGP

}


public method ConvGenledallBranchEgpNewAvrg
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.EGP.NEW.AVRG

}


public method ConvGenledallBranchEgpNew
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.EGP.NEW

}


public method ConvGenledallBranchEgpNewY2
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.EGP.NEW.Y2

}


public method ConvGenledallBranchFcy
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.FCY

}


public method ConvGenledallBranchFcyNewAvrg
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.FCY.NEW.AVRG

}


public method ConvGenledallBranchFcyNew
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.FCY.NEW

}


public method ConvGenledallBranchFcyNewY2
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.FCY.NEW.Y2

}


public method ConvGenledallBranchTotalAvrg
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.TOTAL.AVRG

}


public method ConvGenledallBranchTotal
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.TOTAL

}


public method ConvGenledallBranchTotalY2
 (
)
{ 
	 jBC: CONV.GENLEDALL.BRANCH.TOTAL.Y2

}


public method ConvGenledallCurDetail
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.DETAIL

}


public method ConvGenledallCurDetailNewAvrg
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.DETAIL.NEW.AVRG

}


public method ConvGenledallCurDetailNew
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.DETAIL.NEW

}


public method ConvGenledallCurDetailNewY2
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.DETAIL.NEW.Y2

}


public method ConvGenledallCurTotal
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.TOTAL

}


public method ConvGenledallCurTotalNewAvrg
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.TOTAL.NEW.AVRG

}


public method ConvGenledallCurTotalNew
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.TOTAL.NEW

}


public method ConvGenledallCurTotalNewY2
 (
)
{ 
	 jBC: CONV.GENLEDALL.CUR.TOTAL.NEW.Y2

}


public method ConvGetAmtCol
 (
)
{ 
	 jBC: CONV.GET.AMT.COL

}


public method ConvGetAmts
 (
)
{ 
	 jBC: CONV.GET.AMTS

}


public method ConvGetBals
 (
)
{ 
	 jBC: CONV.GET.BALS

}


public method ConvGetCol2
 (
)
{ 
	 jBC: CONV.GET.COL.2

}


public method ConvGetCol
 (
)
{ 
	 jBC: CONV.GET.COL

}


public method ConvGetColCode
 (
)
{ 
	 jBC: CONV.GET.COL.CODE

}


public method ConvGetCrCol
 (
)
{ 
	 jBC: CONV.GET.CR.COL

}


public method ConvGetDesc
 (
)
{ 
	 jBC: CONV.GET.DESC

}


public method ConvGetDrmntDat
 (
)
{ 
	 jBC: CONV.GET.DRMNT.DAT

}


public method ConvGetEmail
 (
)
{ 
	 jBC: CONV.GET.EMAIL

}


public method ConvGetFreq
 (
)
{ 
	 jBC: CONV.GET.FREQ

}


public method ConvGetGateg
 (
)
{ 
	 jBC: CONV.GET.GATEG

}


public method ConvGetGategoryTxt
 (
)
{ 
	 jBC: CONV.GET.GATEGORY.TXT

}


public method ConvGetInfo
 (
)
{ 
	 jBC: CONV.GET.INFO

}


public method ConvGetLastBal
 (
)
{ 
	 jBC: CONV.GET.LAST.BAL

}


public method ConvGetMailAddress
 (
)
{ 
	 jBC: CONV.GET.MAIL.ADDRESS

}


public method ConvGetOpnBal2
 (
)
{ 
	 jBC: CONV.GET.OPN.BAL.2

}


public method ConvGetOpnBal
 (
)
{ 
	 jBC: CONV.GET.OPN.BAL

}


public method ConvGetUa
 (
)
{ 
	 jBC: CONV.GET.UA

}


public method ConvGetValue2
 (
)
{ 
	 jBC: CONV.GET.VALUE.2

}


public method ConvGetValue
 (
)
{ 
	 jBC: CONV.GET.VALUE

}


public method ConvGov
 (
)
{ 
	 jBC: CONV.GOV

}


public method ConvGroupCredit
 (
)
{ 
	 jBC: CONV.GROUP.CREDIT

}


public method ConvGroupCreditLast
 (
)
{ 
	 jBC: CONV.GROUP.CREDIT.LAST

}


public method ConvGroupPosDesc
 (
)
{ 
	 jBC: CONV.GROUP.POS.DESC

}


public method ConvHmChk
 (
)
{ 
	 jBC: CONV.HM.CHK

}


public method ConvHoldacctCancelPeriod (
	INOUT Enq string, 
)
{ 
	 jBC: CONV.HOLDACCT.CANCEL.PERIOD
}


public method ConvHoldacctCreatPeriod (
	INOUT Enq string, 
)
{ 
	 jBC: CONV.HOLDACCT.CREAT.PERIOD
}


public method ConvHoldacctTot
 (
)
{ 
	 jBC: CONV.HOLDACCT.TOT

}


public method ConvHolidayEdate
 (
)
{ 
	 jBC: CONV.HOLIDAY.EDATE

}


public method ConvHolidayNodays
 (
)
{ 
	 jBC: CONV.HOLIDAY.NODAYS

}


public method ConvHolidaySdate
 (
)
{ 
	 jBC: CONV.HOLIDAY.SDATE

}


public method ConvHolidayType
 (
)
{ 
	 jBC: CONV.HOLIDAY.TYPE

}


public method ConvIban
 (
)
{ 
	 jBC: CONV.IBAN

}


public method ConvIdd
 (
)
{ 
	 jBC: CONV.IDD

}


public method ConvInitApp
 (
)
{ 
	 jBC: CONV.INIT.APP

}


public method ConvIntBookCcy
 (
)
{ 
	 jBC: CONV.INT.BOOK.CCY

}


public method ConvIntBookSusp
 (
)
{ 
	 jBC: CONV.INT.BOOK.SUSP

}


public method ConvIntDate
 (
)
{ 
	 jBC: CONV.INT.DATE

}


public method ConvIntTot
 (
)
{ 
	 jBC: CONV.INT.TOT

}


public method ConvLastAcHandoff
 (
)
{ 
	 jBC: CONV.LAST.AC.HANDOFF

}


public method ConvLastCharge
 (
)
{ 
	 jBC: CONV.LAST.CHARGE

}


public method ConvLastDeb
 (
)
{ 
	 jBC: CONV.LAST.DEB

}


public method ConvLastInt
 (
)
{ 
	 jBC: CONV.LAST.INT

}


public method ConvLastValue
 (
)
{ 
	 jBC: CONV.LAST.VALUE

}


public method ConvLcComm
 (
)
{ 
	 jBC: CONV.LC.COMM

}


public method ConvLcDrConf
 (
)
{ 
	 jBC: CONV.LC.DR.CONF

}


public method ConvLdAa
 (
)
{ 
	 jBC: CONV.LD.AA

}


public method ConvLdAaa
 (
)
{ 
	 jBC: CONV.LD.AAA

}


public method ConvLdAll
 (
)
{ 
	 jBC: CONV.LD.ALL

}


public method ConvLdAll2
 (
)
{ 
	 jBC: CONV.LD.ALL2

}


public method ConvLdAll3
 (
)
{ 
	 jBC: CONV.LD.ALL3

}


public method ConvLdAll4
 (
)
{ 
	 jBC: CONV.LD.ALL4

}


public method ConvLdAll5
 (
)
{ 
	 jBC: CONV.LD.ALL5

}


public method ConvLdAll6
 (
)
{ 
	 jBC: CONV.LD.ALL6

}


public method ConvLdAmtRen
 (
)
{ 
	 jBC: CONV.LD.AMT.REN

}


public method ConvLd
 (
)
{ 
	 jBC: CONV.LD

}


public method ConvLdCancelK1
 (
)
{ 
	 jBC: CONV.LD.CANCEL.K1

}


public method ConvLdCancelK2
 (
)
{ 
	 jBC: CONV.LD.CANCEL.K2

}


public method ConvLdCancelK3
 (
)
{ 
	 jBC: CONV.LD.CANCEL.K3

}


public method ConvLdCancelK4
 (
)
{ 
	 jBC: CONV.LD.CANCEL.K4

}


public method ConvLdCancelK5
 (
)
{ 
	 jBC: CONV.LD.CANCEL.K5

}


public method ConvLdCcy1
 (
)
{ 
	 jBC: CONV.LD.CCY.1

}


public method ConvLdCcy2
 (
)
{ 
	 jBC: CONV.LD.CCY.2

}


public method ConvLdCcyA
 (
)
{ 
	 jBC: CONV.LD.CCY.A

}


public method ConvLdCcy
 (
)
{ 
	 jBC: CONV.LD.CCY

}


public method ConvLdCcyUsd
 (
)
{ 
	 jBC: CONV.LD.CCY.USD

}


public method ConvLdCcy2
 (
)
{ 
	 jBC: CONV.LD.CCY2

}


public method ConvLdCol1R
 (
)
{ 
	 jBC: CONV.LD.COL1R

}


public method ConvLdCol3
 (
)
{ 
	 jBC: CONV.LD.COL3

}


public method ConvLdCol3R1
 (
)
{ 
	 jBC: CONV.LD.COL3R1

}


public method ConvLdCol5
 (
)
{ 
	 jBC: CONV.LD.COL5

}


public method ConvLdCol6
 (
)
{ 
	 jBC: CONV.LD.COL6

}


public method ConvLdCol7
 (
)
{ 
	 jBC: CONV.LD.COL7

}


public method ConvLdCol8
 (
)
{ 
	 jBC: CONV.LD.COL8

}


public method ConvLdCusBank
 (
)
{ 
	 jBC: CONV.LD.CUS.BANK

}


public method ConvLdInt
 (
)
{ 
	 jBC: CONV.LD.INT

}


public method ConvLdNumber
 (
)
{ 
	 jBC: CONV.LD.NUMBER

}


public method ConvLdOut
 (
)
{ 
	 jBC: CONV.LD.OUT

}


public method ConvLdStatus
 (
)
{ 
	 jBC: CONV.LD.STATUS

}


public method ConvLdType1
 (
)
{ 
	 jBC: CONV.LD.TYPE.1

}


public method ConvLgAmount
 (
)
{ 
	 jBC: CONV.LG.AMOUNT

}


public method ConvLgAmtEng
 (
)
{ 
	 jBC: CONV.LG.AMT.ENG

}


public method ConvLgBenfDetails
 (
)
{ 
	 jBC: CONV.LG.BENF.DETAILS

}


public method ConvLgBenfName
 (
)
{ 
	 jBC: CONV.LG.BENF.NAME

}


public method ConvLgComAmt
 (
)
{ 
	 jBC: CONV.LG.COM.AMT

}


public method ConvLgCusPosAmt
 (
)
{ 
	 jBC: CONV.LG.CUS.POS.AMT

}


public method ConvLgCustomerName
 (
)
{ 
	 jBC: CONV.LG.CUSTOMER.NAME

}


public method ConvLgLimit
 (
)
{ 
	 jBC: CONV.LG.LIMIT

}


public method ConvLgMatList
 (
)
{ 
	 jBC: CONV.LG.MAT.LIST

}


public method ConvLgNo
 (
)
{ 
	 jBC: CONV.LG.NO

}


public method ConvLgNumber
 (
)
{ 
	 jBC: CONV.LG.NUMBER

}


public method ConvLgOutAmt
 (
)
{ 
	 jBC: CONV.LG.OUT.AMT

}


public method ConvLgOverride
 (
)
{ 
	 jBC: CONV.LG.OVERRIDE

}


public method ConvLgPer
 (
)
{ 
	 jBC: CONV.LG.PER

}


public method ConvLgPerc
 (
)
{ 
	 jBC: CONV.LG.PERC

}


public method ConvLgPurpose
 (
)
{ 
	 jBC: CONV.LG.PURPOSE

}


public method ConvLgSector
 (
)
{ 
	 jBC: CONV.LG.SECTOR

}


public method ConvLgSector2
 (
)
{ 
	 jBC: CONV.LG.SECTOR2

}


public method ConvLgSector3
 (
)
{ 
	 jBC: CONV.LG.SECTOR3

}


public method ConvLgSector4
 (
)
{ 
	 jBC: CONV.LG.SECTOR4

}


public method ConvLgSel
 (
)
{ 
	 jBC: CONV.LG.SEL

}


public method ConvLgTot
 (
)
{ 
	 jBC: CONV.LG.TOT

}


public method ConvLgTotMarg
 (
)
{ 
	 jBC: CONV.LG.TOT.MARG

}


public method ConvLgTransCategDesc
 (
)
{ 
	 jBC: CONV.LG.TRANS.CATEG.DESC

}


public method ConvLgTransactionCategAcct
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.CATEG.ACCT

}


public method ConvLgTransactionCateg
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.CATEG

}


public method ConvLgTransactionFtAcct
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.FT.ACCT

}


public method ConvLgTransactionFt
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.FT

}


public method ConvLgTransactionFtCateg
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.FT.CATEG

}


public method ConvLgTransactionFtCategDebit
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.FT.CATEG.DEBIT

}


public method ConvLgTransactionFtDebit
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.FT.DEBIT

}


public method ConvLgTransactionFtDesc
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.FT.DESC

}


public method ConvLgTransactionStmt
 (
)
{ 
	 jBC: CONV.LG.TRANSACTION.STMT

}


public method ConvLgType
 (
)
{ 
	 jBC: CONV.LG.TYPE

}


public method ConvLgWordsAra
 (
)
{ 
	 jBC: CONV.LG.WORDS.ARA

}


public method ConvLgWordsDate
 (
)
{ 
	 jBC: CONV.LG.WORDS.DATE

}


public method ConvLgWordsEng
 (
)
{ 
	 jBC: CONV.LG.WORDS.ENG

}


public method ConvLi1Fcy
 (
)
{ 
	 jBC: CONV.LI1.FCY

}


public method ConvLi1Lcy
 (
)
{ 
	 jBC: CONV.LI1.LCY

}


public method ConvLimitCusRel
 (
)
{ 
	 jBC: CONV.LIMIT.CUS.REL

}


public method ConvLimitMain
 (
)
{ 
	 jBC: CONV.LIMIT.MAIN

}


public method ConvLimitMainM
 (
)
{ 
	 jBC: CONV.LIMIT.MAIN.M

}


public method ConvLineMov1
 (
)
{ 
	 jBC: CONV.LINE.MOV1

}


public method ConvLineMov1D
 (
)
{ 
	 jBC: CONV.LINE.MOV1D

}


public method ConvLineMov2
 (
)
{ 
	 jBC: CONV.LINE.MOV2

}


public method ConvLineMov2D
 (
)
{ 
	 jBC: CONV.LINE.MOV2D

}


public method ConvLineMov3
 (
)
{ 
	 jBC: CONV.LINE.MOV3

}


public method ConvLineMov3D
 (
)
{ 
	 jBC: CONV.LINE.MOV3D

}


public method ConvLineMov4
 (
)
{ 
	 jBC: CONV.LINE.MOV4

}


public method ConvLineMov4D
 (
)
{ 
	 jBC: CONV.LINE.MOV4D

}


public method ConvLoanColAmt
 (
)
{ 
	 jBC: CONV.LOAN.COL.AMT

}


public method ConvLoanCreditAmt
 (
)
{ 
	 jBC: CONV.LOAN.CREDIT.AMT

}


public method ConvLoanEmpRate
 (
)
{ 
	 jBC: CONV.LOAN.EMP.RATE

}


public method ConvLoanInstalment
 (
)
{ 
	 jBC: CONV.LOAN.INSTALMENT

}


public method ConvLoanLcy1
 (
)
{ 
	 jBC: CONV.LOAN.LCY1

}


public method ConvLqdfTotRowA
 (
)
{ 
	 jBC: CONV.LQDF.TOT.ROW.A

}


public method ConvMastDesc
 (
)
{ 
	 jBC: CONV.MAST.DESC

}


public method ConvMmCancelAmt
 (
)
{ 
	 jBC: CONV.MM.CANCEL.AMT

}


public method ConvMmCancelAmtNau
 (
)
{ 
	 jBC: CONV.MM.CANCEL.AMT.NAU

}


public method ConvMmCancelIntamt
 (
)
{ 
	 jBC: CONV.MM.CANCEL.INTAMT

}


public method ConvMmCancelIntamtNau
 (
)
{ 
	 jBC: CONV.MM.CANCEL.INTAMT.NAU

}


public method ConvMmMastText
 (
)
{ 
	 jBC: CONV.MM.MAST.TEXT

}


public method ConvMmNewIntamt
 (
)
{ 
	 jBC: CONV.MM.NEW.INTAMT

}


public method ConvMmNewIntamtNau
 (
)
{ 
	 jBC: CONV.MM.NEW.INTAMT.NAU

}


public method ConvMmNewRate
 (
)
{ 
	 jBC: CONV.MM.NEW.RATE

}


public method ConvMmNewRateNau
 (
)
{ 
	 jBC: CONV.MM.NEW.RATE.NAU

}


public method ConvMmVdate
 (
)
{ 
	 jBC: CONV.MM.VDATE

}


public method ConvMmVdateNau
 (
)
{ 
	 jBC: CONV.MM.VDATE.NAU

}


public method ConvMonth1
 (
)
{ 
	 jBC: CONV.MONTH.1

}


public method ConvMonth10
 (
)
{ 
	 jBC: CONV.MONTH.10

}


public method ConvMonth11
 (
)
{ 
	 jBC: CONV.MONTH.11

}


public method ConvMonth12
 (
)
{ 
	 jBC: CONV.MONTH.12

}


public method ConvMonth2
 (
)
{ 
	 jBC: CONV.MONTH.2

}


public method ConvMonth3
 (
)
{ 
	 jBC: CONV.MONTH.3

}


public method ConvMonth4
 (
)
{ 
	 jBC: CONV.MONTH.4

}


public method ConvMonth5
 (
)
{ 
	 jBC: CONV.MONTH.5

}


public method ConvMonth6
 (
)
{ 
	 jBC: CONV.MONTH.6

}


public method ConvMonth7
 (
)
{ 
	 jBC: CONV.MONTH.7

}


public method ConvMonth8
 (
)
{ 
	 jBC: CONV.MONTH.8

}


public method ConvMsgAmt
 (
)
{ 
	 jBC: CONV.MSG.AMT

}


public method ConvMsgCust
 (
)
{ 
	 jBC: CONV.MSG.CUST

}


public method ConvMsgIn
 (
)
{ 
	 jBC: CONV.MSG.IN

}


public method ConvMsgName
 (
)
{ 
	 jBC: CONV.MSG.NAME

}


public method ConvMsgOut
 (
)
{ 
	 jBC: CONV.MSG.OUT

}


public method ConvMultiField
 (
)
{ 
	 jBC: CONV.MULTI.FIELD

}


public method ConvMultiV
 (
)
{ 
	 jBC: CONV.MULTI.V

}


public method ConvMultiValue
 (
)
{ 
	 jBC: CONV.MULTI.VALUE

}


public method ConvNextUpdate
 (
)
{ 
	 jBC: CONV.NEXT.UPDATE

}


public method ConvNo
 (
)
{ 
	 jBC: CONV.NO

}


public method ConvNoCbe
 (
)
{ 
	 jBC: CONV.NO.CBE

}


public method ConvNoDays
 (
)
{ 
	 jBC: CONV.NO.DAYS

}


public method ConvNoPost
 (
)
{ 
	 jBC: CONV.NO.POST

}


public method ConvNoPostH
 (
)
{ 
	 jBC: CONV.NO.POST.H

}


public method ConvNoPostNew
 (
)
{ 
	 jBC: CONV.NO.POST.NEW

}


public method ConvOfsMsgAcct
 (
)
{ 
	 jBC: CONV.OFS.MSG.ACCT

}


public method ConvOfsMsgAmt
 (
)
{ 
	 jBC: CONV.OFS.MSG.AMT

}


public method ConvOfsMsgErr
 (
)
{ 
	 jBC: CONV.OFS.MSG.ERR

}


public method ConvPdDays
 (
)
{ 
	 jBC: CONV.PD.DAYS

}


public method ConvPdFirst
 (
)
{ 
	 jBC: CONV.PD.FIRST

}


public method ConvPdIn
 (
)
{ 
	 jBC: CONV.PD.IN

}


public method ConvPdPe
 (
)
{ 
	 jBC: CONV.PD.PE

}


public method ConvPdPr
 (
)
{ 
	 jBC: CONV.PD.PR

}


public method ConvPddaysBkts
 (
)
{ 
	 jBC: CONV.PDDAYS.BKTS

}


public method ConvPddaysStatus
 (
)
{ 
	 jBC: CONV.PDDAYS.STATUS

}


public method ConvPlCateg50
 (
)
{ 
	 jBC: CONV.PL.CATEG.50

}


public method ConvPlCateg51
 (
)
{ 
	 jBC: CONV.PL.CATEG.51

}


public method ConvPlCateg
 (
)
{ 
	 jBC: CONV.PL.CATEG

}


public method ConvPlChk
 (
)
{ 
	 jBC: CONV.PL.CHK

}


public method ConvRateCy
 (
)
{ 
	 jBC: CONV.RATE.CY

}


public method ConvReLineNo
 (
)
{ 
	 jBC: CONV.RE.LINE.NO

}


public method ConvRef
 (
)
{ 
	 jBC: CONV.REF

}


public method ConvRefBen
 (
)
{ 
	 jBC: CONV.REF.BEN

}


public method ConvRef1
 (
)
{ 
	 jBC: CONV.REF1

}


public method ConvRefref
 (
)
{ 
	 jBC: CONV.REFREF

}


public method ConvReg
 (
)
{ 
	 jBC: CONV.REG

}


public method ConvRegg
 (
)
{ 
	 jBC: CONV.REGG

}


public method ConvReggg
 (
)
{ 
	 jBC: CONV.REGGG

}


public method ConvRes32
 (
)
{ 
	 jBC: CONV.RES32

}


public method ConvRiskNAll
 (
)
{ 
	 jBC: CONV.RISK.N.ALL

}


public method ConvRiskNAllNew
 (
)
{ 
	 jBC: CONV.RISK.N.ALL.NEW

}


public method ConvRiskNAllTst
 (
)
{ 
	 jBC: CONV.RISK.N.ALL.TST

}


public method ConvRiskNAllTstNew
 (
)
{ 
	 jBC: CONV.RISK.N.ALL.TST.NEW

}


public method ConvRound
 (
)
{ 
	 jBC: CONV.ROUND

}


public method ConvRsrCodDscr
 (
)
{ 
	 jBC: CONV.RSR.COD.DSCR

}


public method ConvRsrSubDscr
 (
)
{ 
	 jBC: CONV.RSR.SUB.DSCR

}


public method ConvRunText
 (
)
{ 
	 jBC: CONV.RUN.TEXT

}


public method ConvSavTotal
 (
)
{ 
	 jBC: CONV.SAV.TOTAL

}


public method ConvSavTotalCr
 (
)
{ 
	 jBC: CONV.SAV.TOTAL.CR

}


public method ConvSavTotalCr2
 (
)
{ 
	 jBC: CONV.SAV.TOTAL.CR2

}


public method ConvSavTotalH
 (
)
{ 
	 jBC: CONV.SAV.TOTAL.H

}


public method ConvSaveTextAa
 (
)
{ 
	 jBC: CONV.SAVE.TEXT.AA

}


public method ConvScbBrCorrect
 (
)
{ 
	 jBC: CONV.SCB.BR.CORRECT

}


public method ConvScbinter18Categ
 (
)
{ 
	 jBC: CONV.SCBINTER18.CATEG

}


public method ConvSccdLoan
 (
)
{ 
	 jBC: CONV.SCCD.LOAN

}


public method ConvScrDesc
 (
)
{ 
	 jBC: CONV.SCR.DESC

}


public method ConvSecDesc
 (
)
{ 
	 jBC: CONV.SEC.DESC

}


