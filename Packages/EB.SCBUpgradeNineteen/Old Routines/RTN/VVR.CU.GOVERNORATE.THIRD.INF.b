* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
** ----- NESSREEN AHMED SCB -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.GOVERNORATE.THIRD.INF

*IF COMI IS DIFFERENT THAN THE  R.NEW OF THE SAME FIELD THEN CLEAR FIELD REGION
*HOW TO CLEAR THE DROP DOWN LIST AFTER CHOOSING ANOTHER GOVERNERATE

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE

    ETEXT = ""

    IF COMI THEN
        CALL DBR ('SCB.CUS.GOVERNORATE':@FM:2.1,COMI,GEO.SEC1)


        R.NEW(EB.CUS.TOWN.COUNTRY)<1,1> = GEO.SEC1<1,1>
        R.NEW(EB.CUS.TOWN.COUNTRY)<1,2> = GEO.SEC1<1,2>


    END


    IF COMI # R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.GOVERNORATE> THEN

        R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.REGION> = ''

    END

    CALL REBUILD.SCREEN
    RETURN
END
