* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*************DINA-SCB************
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DEP.BLOCK.UPDATE
*A ROUTINE TO EMPTY:ACCOUNT.1,ACCOUNT.2,CUSTOMER.2,CURRENCY.1,CURRENCY.2 IF CUSTOMR IS CHANGED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL

    IF R.NEW(LD.AMOUNT.INCREASE) THEN
        IF R.NEW(LD.AMOUNT.INCREASE) LT 0  THEN

            COL.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.COLLATERAL.ID>
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.COLLATERAL.ID> THEN
                CALL DBR('COLLATERAL':@FM:COLL.NOMINAL.VALUE,COL.ID,NOM.VAL)
                CALL DBR('COLLATERAL':@FM:COLL.THIRD.PARTY.VALUE,COL.ID,THRD.VAL)
                COL.VAL = NOM.VAL - THRD.VAL

                LD.AMT.NEW = R.NEW(LD.AMOUNT) + R.NEW(LD.AMOUNT.INCREASE)

                IF LD.AMT.NEW LT COL.VAL THEN
                    ETEXT= '���� ������� ��� �� ������ '
                END
                CALL REBUILD.SCREEN
            END
        END
        RETURN
    END
