* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>471</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.RET

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    ETEXT = ''
    E = ''
    BAL = 0

    IF MESSAGE = '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN ETEXT = '��� ����� ������';CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
        ETEXT = ''
        IF COMI GT 20000 THEN
            ETEXT = '��� �� �� ���� ������ ���� �� 20000' ; RETURN
            CALL STORE.END.ERROR
        END
**********NESSREEN 16/10/2006***************
        IF R.NEW(TT.TE.CURRENCY.1) = LCCY THEN
********************************************
            ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
            AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV>
            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
********NESSREEN AHMED 09/01/2007*********************************************
*  IF CATEGG = '1202' THEN
            IF (CATEGG GE 1101 AND CATEGG LE 1450 ) THEN
             **   TEXT = '1202' ; CALL REM
                CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
             **   TEXT = 'LIMTREF=':LIMTREF ; CALL REM
                CUST = R.NEW(TT.TE.CUSTOMER.1)
                IDD = FMT(LIMTREF, "R%10")
                KEY.ID = CUST:'.':IDD
             **   TEXT = 'ID=':KEY.ID ; CALL REM
                CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
             **   TEXT = 'AVLM=':AVLM ; CALL REM
             **   TEXT = 'BAL=':BAL ; CALL REM
                EQT = AVLM + BAL - COMI
              **  TEXT = 'EQTLCCY=':EQT ; CALL REM
                ETEXT = ''
                IF EQT < 0 THEN
               **     TEXT = 'NOOOO' ; CALL REM
                    ETEXT = "�� ��� �� ������ ������ �� �����"
                    CALL STORE.END.ERROR
                END
            END

****************************************************
            IF NOT (CATEGG GE 1500 AND CATEGG LE 1590 )  THEN
                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 83 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
                NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
***********NESSREEN  17/9/2006***********************************
**16/1/2007 IF LOCK.AMT1 > 0 THEN
                IF LOCK.AMT<1,LOCK.NO> > 0 THEN
**16/1/2007  TEXT = '������ ������� ����=':LOCK.AMT1 ; CALL REM
                    TEXT = '������ ������� ����=':LOCK.AMT<1,LOCK.NO> ; CALL REM
                    TEXT = '������ ������� =': NET.BAL ; CALL REM
                END
*****************************************************************
** CATEG = ACCT[11,4]
*IF COMI GT NET.BAL AND CATEGG NE '1202' THEN
* TEXT = 'COMI=':COMI ; CALL REM
*  TEXT = 'NET.BAL=':NET.BAL ; CALL REM
*   ETEXT = '������ �� ����'
*    CALL STORE.END.ERROR
* END
******************************************
            END
********************************************
        END         ;**IF CURR = LCCY
    END   ;**IF MESSAGE = VAL
***************************************************
    RETURN
END
