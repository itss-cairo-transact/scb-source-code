* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>189</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BT.TT.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.TT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH


    IF V$FUNCTION = 'A' THEN RETURN
    IF V$FUNCTION = 'R' THEN RETURN

    FN.BATCH = 'FBNK.BILL.REGISTER' ; F.BATCH = '' ; R.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)
    E     = ''
    ETEXT = ''
    GOSUB GET.BR
    GOSUB DEF.DATA

    IF MESSAGE EQ 'VAL' THEN
        GOSUB CHECK.REF
    END

    RETURN
****---------------------------------------------------------****
GET.BR:
    IF COMI[1,2] NE 'BR' THEN
        COO = R.NEW(BT.TT.BR.BRN)
        IF LEN(COO) EQ 1 THEN
            COMP  = 'EG001000':COO
        END ELSE
            COMP  = 'EG00100':COO
        END
        T.SEL  = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ":COMI:" AND BILL.CHQ.STA EQ 2  AND CO.CODE EQ ":COMP:" AND CURRENCY EQ EGP "

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            BR.ID = KEY.LIST
            COMI  = KEY.LIST
            CALL REBUILD.SCREEN
        END
    END ELSE
        IF COMI[1,2] EQ 'BR' THEN
            BR.ID = COMI
        END
    END

    CALL F.READ(FN.BATCH, BR.ID, R.BATCH, F.BATCH, ETEXT)
    IF NOT(ETEXT) THEN
**
        BEGIN CASE
        CASE R.BATCH<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 15
            ETEXT = "�����"

        CASE  R.BATCH<EB.BILL.REG.CURRENCY> NE "EGP"
            ETEXT = "��� �� ���� ������ ����"

        CASE R.BATCH<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 14
            ETEXT = "�����"

        CASE NOT(R.BATCH<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 2 )
            ETEXT = "��� ������� �� �������"
        CASE OTHERWISE
            RETURN
        END CASE
    END ELSE
        E = "���� �� �����"
    END
    RETURN
***----------------------------------------------------------***
DEF.DATA:
    R.NEW(BT.TT.BR.AMT)<1,AV>            = R.BATCH<EB.BILL.REG.AMOUNT>
    R.NEW(BT.TT.CREDIT.ACCOUNT)<1,AV>    = R.BATCH<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
    R.NEW(BT.TT.DR.NAME)<1,AV>           = R.BATCH<EB.BILL.REG.LOCAL.REF,BRLR.DR.NAME>

    CALL REBUILD.SCREEN

    RETURN
**------------------------------------------------------------**
CHECK.REF:
    HH = 0
*Line [ 113 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M   = DCOUNT(R.NEW(BT.TT.OUR.REFERENCE),@VM)
    FOR K  = 1 TO NO.M

        IF R.NEW(BT.TT.OUR.REFERENCE)<1,AV> EQ R.NEW(BT.TT.OUR.REFERENCE)<1,K> THEN

            HH = HH +1
        END
    NEXT K
    IF HH GT 1 THEN
        ETEXT = "CHECK REFERENCE "
    END


    RETURN
**----------------------------------------------------------**
END
