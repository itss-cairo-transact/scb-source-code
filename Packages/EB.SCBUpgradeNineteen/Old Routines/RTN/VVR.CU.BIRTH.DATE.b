* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1248</Rating>
*-----------------------------------------------------------------------------
** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
** ----- 9.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.CU.BIRTH.DATE

*VALIDATEION ROUTINE CHECK IF THE AGE OF CUSTOMER IS LESS THAN 5 THEN CLEAR THE EDUCATION AND PROFESSION FIELDS
*AND DEFAULT THE MARITAL STATUS FIELD WITH SINGLE
*IF THE AGE OF CUSTOMER IS LESS THAN 16 THEN DEFAULT THE MARITAL STATUS FIELD WITH SINGLE

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
***UPDATED BY NESSREEN AHMED 28/11/2011**********
IF MESSAGE NE 'VAL' THEN
***END OF UPDATE 28/11/2011**********************
*    R.NEW(EB.CUS.CUSTOMER.STATUS) =""
        CALL DBR('CUSTOMER.STATUS':@FM:EB.CST.DESCRIPTION:@FM:'L',22,ENRI)
        CALL DBR('CUSTOMER.STATUS':@FM:EB.CST.DESCRIPTION:@FM:'L',2,ENRI1)
        IF COMI AND COMI >= TODAY THEN ETEXT = 'THE DATE MUST BE LT TODAY'
        ELSE
            YEARS.05 = ( COMI[ 1, 4] +  5 ) : COMI[ 5, 4]
            YEARS.16 = ( COMI[ 1, 4] + 16 ) : COMI[ 5, 4]
            YEARS.21 = ( COMI[ 1, 4] + 21 ) : COMI[ 5, 4]

            BEGIN CASE
**************************************************************************
            CASE YEARS.05 > TODAY
**************************************************************************
                R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.PROFESSION> = ''
                R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ACCOM.TYPE> = ''
                R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ACCOM.LEGALITY> = ''
                R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.INCOME> = ''
****NESS****
                IF COMI # R.NEW(EB.CUS.BIRTH.INCORP.DATE) THEN
                    R.NEW(EB.CUS.LOCAL.REF)< 1, CULR.EDUCATION> = ''
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS> = ''
                    R.NEW(EB.CUS.LOCAL.REF)< 1, CULR.TITLE> = ''
**   R.NEW(EB.CUS.RELATION.CODE,1)= ''
                    R.NEW(EB.CUS.RELATION.CODE) = ''
***************************************************************
                    R.NEW(EB.CUS.REVERS.REL.CODE)= ''
                    R.NEW(EB.CUS.CUSTOMER.STATUS)='2'
                END

                IF NOT (R.NEW(EB.CUS.LOCAL.REF)< 1, CULR.EDUCATION>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.EDUCATION> = '08.Under Educ. Age-��� �� �������'
                END
                IF NOT (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS> = 'Single-����'
                END
                IF NOT (R.NEW(EB.CUS.LOCAL.REF)< 1, CULR.TITLE>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.TITLE> = '4'
                END
                R.NEW(EB.CUS.CUSTOMER.STATUS)='22'
************
****NESS****
                IF NOT (R.NEW(EB.CUS.RELATION.CODE)) THEN
                    R.NEW(EB.CUS.RELATION.CODE)='42'
                    R.NEW(EB.CUS.REVERS.REL.CODE) = '24'
                END
************
                T.ENRI<EB.CUS.CUSTOMER.STATUS> = ENRI

************************************************************************
            CASE YEARS.16 > TODAY
************************************************************************
                IF NOT (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS> = 'Single-����'
                END
                IF NOT (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.TITLE>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.TITLE> = '4'
                END
                R.NEW(EB.CUS.CUSTOMER.STATUS)='22'
                T.ENRI<EB.CUS.CUSTOMER.STATUS> = ENRI

****NESS****
                IF NOT (R.NEW(EB.CUS.RELATION.CODE)) THEN
                    R.NEW(EB.CUS.RELATION.CODE)='42'
                    R.NEW(EB.CUS.REVERS.REL.CODE) = '24'
                END
************
*************************************************************************
            CASE YEARS.21 > TODAY
*************************************************************************
                IF NOT (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.TITLE>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.TITLE> = '4'
                END

                R.NEW(EB.CUS.CUSTOMER.STATUS)='22'

                T.ENRI<EB.CUS.CUSTOMER.STATUS> = ENRI

****NESS****
                IF NOT (R.NEW(EB.CUS.RELATION.CODE)) THEN
                    R.NEW(EB.CUS.RELATION.CODE)='42'
                    R.NEW(EB.CUS.REVERS.REL.CODE) = '24'
                END
************

************************************************************************
            CASE OTHERWISE
************************************************************************
                IF NOT ( R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.MARITAL.STATUS> = ''
                END

                IF NOT (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.EDUCATION>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.EDUCATION> = ''
                END
                IF NOT (R.NEW(EB.CUS.RELATION.CODE)) THEN
                    R.NEW(EB.CUS.RELATION.CODE)=''
                END
                IF NOT (R.NEW(EB.CUS.REVERS.REL.CODE)) THEN
                    R.NEW(EB.CUS.REVERS.REL.CODE) = ''
                END
                IF NOT (R.NEW(EB.CUS.LOCAL.REF)< 1, CULR.TITLE>) THEN
                    R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.TITLE> = ''
                END

                T.ENRI<EB.CUS.CUSTOMER.STATUS> = ENRI1


*************************************************************************
            END CASE
****UPDATED BY NESSREEN AHMED 4/1/2010************************
        **    R.NEW(EB.CUS.CUSTOMER.LIABILITY)= ""
        **    R.NEW(EB.CUS.CUSTOMER.LIABILITY)=ID.NEW
***************************************************************
            CALL REBUILD.SCREEN

        END

    END
***28/11/2011**********************
END
    RETURN
END
