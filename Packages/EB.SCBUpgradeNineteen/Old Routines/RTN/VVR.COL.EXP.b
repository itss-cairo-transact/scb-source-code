* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------


    SUBROUTINE VVR.COL.EXP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    IF COMI[1,2] ="LD" THEN
        CALL DBR("LD.LOANS.AND.DEPOSITS":@FM:LD.FIN.MAT.DATE,COMI,EXP.DATE)
        R.NEW(COLL.EXPIRY.DATE) = EXP.DATE
        T(COLL.EXPIRY.DATE)<3> = 'NOINPUT'

        CALL DBR("LD.LOANS.AND.DEPOSITS":@FM:LD.CURRENCY,COMI,CUR)
        R.NEW(COLL.CURRENCY) = CUR

        CALL DBR("LD.LOANS.AND.DEPOSITS":@FM:LD.VALUE.DATE,COMI,V.DATE)
        R.NEW(COLL.VALUE.DATE) = V.DATE

        CALL DBR("LD.LOANS.AND.DEPOSITS":@FM:LD.AMOUNT,COMI,AMT)
        IF R.NEW(COLL.COLLATERAL.CODE) NE '108' THEN
            R.NEW(COLL.EXECUTION.VALUE) = AMT
        END
        CUS.ID=FIELD(ID.NEW,'.',1)
*TEXT =" CUS.ID " : CUS.ID ; CALL REM
        CALL DBR("LD.LOANS.AND.DEPOSITS":@FM:LD.CUSTOMER.ID,COMI,ID)
*TEXT =" ID " : ID ; CALL REM
        IF CUS.ID NE ID THEN
            ETEXT = '��� ������� �� ��� ������'
            CALL STORE.END.ERROR
        END

        CALL REBUILD.SCREEN
    END ELSE
        R.NEW(COLL.EXPIRY.DATE) = ''
        T(COLL.EXPIRY.DATE)<3> = ''
    END
    CALL REBUILD.SCREEN
    IF R.NEW(COLL.COLLATERAL.CODE) NE '108' THEN
        R.NEW(COLL.THIRD.PARTY.VALUE)=   R.NEW(COLL.EXECUTION.VALUE) -  R.NEW(COLL.LOCAL.REF)<1,COLR.COLL.AMOUNT>
    END
    CALL REBUILD.SCREEN

    RETURN
END
