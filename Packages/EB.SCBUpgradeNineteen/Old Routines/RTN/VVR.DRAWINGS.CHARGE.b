* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DRAWINGS.CHARGE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

*APPL.ACCT = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
    APPL.ACCT = COMI
    CURR      = COMI[9,2]
    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CURR,CURR.NAME)

    IF MESSAGE NE "VAL" THEN
        IF R.NEW(TF.DR.WAIVE.CHARGES) = 'NO' THEN
*Line [ 37 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            SAM = DCOUNT(R.NEW(TF.DR.CHARGE.CODE),@VM)
            FOR I = 1 TO SAM
                R.NEW(TF.DR.CHARGE.ACCOUNT)<1,I>   = APPL.ACCT
                R.NEW(TF.DR.CHARGE.STATUS)<1,I>    = '2'
                R.NEW(TF.DR.CHARGE.CURRENCY)<1,I>  = CURR.NAME
                R.NEW(TF.DR.CHARGE.AMOUNT)<1,I>   = ''
                R.NEW(TF.DR.AMORT.CHARGES)<1,I>   = 'NO'
                R.NEW(TF.DR.PARTY.CHARGED)<1,I>   = 'O'

            NEXT I
            CALL REBUILD.SCREEN ; P = 0
        END
    END
    RETURN
END
