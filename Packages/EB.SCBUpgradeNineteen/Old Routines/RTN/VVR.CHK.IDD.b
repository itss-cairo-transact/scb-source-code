* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>197</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHK.IDD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*--------------------------------------------
    IF PGM.VERSION EQ ',SCB.CD.O.SUEZ.CUMU' THEN
        IF COMI THEN
            IDD     = COMI
            LEN.ID  = LEN(IDD)
            CHK.FLG = R.NEW(LD.LOCAL.REF)<1,LDLR.CHK.FLG>
            IF CHK.FLG EQ '�����' THEN
                IF LEN.ID NE 14 THEN
                    ETEXT = 'MUST BE 14 DIGITS'
                    CALL STORE.END.ERROR
                END
            END
        END
    END

    IF PGM.VERSION EQ ',SCB.CD.O.SUEZ.CUMU.BNK' THEN
        IDD     = COMI
        LEN.ID  = LEN(IDD)
        CUS.NO  = R.NEW(LD.CUSTOMER.ID)
        FN.CU   = "FBNK.CUSTOMER"  ; F.CU = ""
        CALL OPF(FN.CU,F.CU)

        CALL F.READ(FN.CU,CUS.NO,R.CU,F.CU,E.CU)
        SEC.ID  = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
        ID.NO     = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
        COMM.NO   = R.CU<EB.CUS.LOCAL.REF><1,CULR.COM.REG.NO>
**    LIC.ID.NO = R.CU<EB.CUS.LOCAL.REF><1,CULR.TAX.EXEMPTION>
        IF SEC.ID EQ '4650' THEN
            IF LEN.ID NE 14 THEN
                ETEXT = 'MUST BE 14 DIGITS'
                CALL STORE.END.ERROR
            END  ELSE
                IF COMI NE ID.NO  THEN
                    ETEXT='����� ������ ��� ����� ���� ������ ������';CALL STORE.END.ERROR
                END
            END
        END   ELSE
            IF COMM.NO THEN
                IF COMI NE COMM.NO THEN
                    ETEXT='��� ����� ������� ��� ����� ���� ������� ������';CALL STORE.END.ERROR
                END
            END ELSE
**          IF COMI NE LIC.ID.NO THEN
                IF COMI NE CUS.NO  THEN
**      ETEXT='��� ������� ��� ����� ���� ����� ������';CALL STORE.END.ERROR
                    ETEXT='��� ������ ��� �� ����  ����';CALL STORE.END.ERROR
                END
            END
        END
    END
*-----------------------------------------------------
    FN.TEMP = "F.SCB.CD.NATIONAL.ID"    ; F.TEMP = ""
    CALL OPF(FN.TEMP,F.TEMP)

***---------------hytham
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CHK.FLG>  EQ '�����' THEN
        NATIONAL.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PLACE.ID.ISSUE>
    END ELSE
        NATIONAL.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>
    END

    CD.CHK.ID =  NATIONAL.ID
    CALL F.READ(FN.TEMP,CD.CHK.ID,R.TEMP,F.TEMP,ERR1)

*    CALL F.READ(FN.TEMP,COMI,R.TEMP,F.TEMP,ERR1)
**----------------------------

    BAL = R.TEMP<SCB.CD.BALANCE>

    TOT.LD.AMT  = R.NEW(LD.AMOUNT)
    MAX.VAL     = 990
    TOT.BAL     = BAL + TOT.LD.AMT
    IF TOT.BAL GT 990 THEN
        ETEXT='CDS AMOUNT EXCEED 990';CALL STORE.END.ERROR
    END

    RETURN
END
