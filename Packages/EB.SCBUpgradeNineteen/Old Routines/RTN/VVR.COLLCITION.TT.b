* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>828</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.COLLCITION.TT

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

*IF V$FUNCTION = 'I' THEN

    IF COMI  THEN
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = 'SCB.':R.NEW(TT.TE.ACCOUNT.2):'-':COMI
    CHQ.ID = 'SCB.':R.NEW(TT.TE.ACCOUNT.2):'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED, CHQ.ID, REPRESENT)
    CALL DBR ('CHEQUES.PRESENTED':@FM:CC.CRS.DATE.PRESENTED, CHQ.ID, REPRESENT)
****END OF UPDATE 7/3/2016*****************************
*ETEXT = ''
        IF REPRESENT THEN ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
        ACCT.NO = R.NEW(TT.TE.ACCOUNT.2)
        CHQ='SCB':'.':ACCT.NO:'...'
        CHQ.CUS = 'SCB':'.':ACCT.NO
        T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED EQ '0' THEN
*  TEXT = 'NOT ISSUED' ; CALL REM
            ETEXT='�� ���� ����� ����� ���� ������ &': CHQ
            CALL STORE.END.ERROR
        END ELSE
            CALL DBR ('CHEQUE.REGISTER':@FM:CHEQUE.REG.CHEQUE.NOS, CHQ.CUS, CHQ.NOS)
            LF   = FIELD (CHQ.NOS, "-", 1)
            RH   = FIELD (CHQ.NOS, "-", 2)
            IF COMI GT RH OR COMI LT LF THEN
                ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
*TEXT = 'NOT.INVOLVED.IN.CHQ' ; CALL REM
            END
            FN.PAYMENT.STOP = 'F.PAYMENT.STOP' ; F.PAYMENT.STOP ='' ; R.PAYMENT.STOP = '' ; E= ''
            CALL OPF(FN.PAYMEN.STOP,F.PAYMENT.STOP)
            CALL F.READ(FN.PAYMENT.STOP,ACCT.NO, R.PAYMENT.STOP, F.PAYMENT.STOP , E)
            FIRST.NO = R.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
            LAST.NO = R.PAYMENT.STOP<AC.PAY.LAST.CHEQUE.NO>
            CHQ.TYPE = R.PAYMENT.STOP<AC.PAY.CHEQUE.TYPE>
            STOP.TYPE = R.PAYMENT.STOP<AC.PAY.PAYM.STOP.TYPE>

*Line [ 79 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT (STOP.TYPE,@VM)
            FOR X = 1 TO DD
                IF CHQ.TYPE<1,X> = 'SCB' THEN
                    IF LAST.NO<1,X> = "" THEN
                        IF COMI = FIRST.NO<1,X> THEN ETEXT = '��� ����� �����' ; CALL STORE.END.ERROR
                    END ELSE
                        FOR I = FIRST.NO<1,X> TO LAST.NO<1,X>
                            IF COMI = I THEN ETEXT = '��� ����� �����'; CALL STORE.END.ERROR
                        NEXT I
                    END
                END
            NEXT X
            CALL F.RELEASE(FN.PAYMENT.STOP,ACCT.NO,F.PAYMENT.STOP)
        END
    END
    RETURN
END
