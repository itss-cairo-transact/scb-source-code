* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>993</Rating>
*-----------------------------------------------------------------------------
*************DINA-SCB***********

SUBROUTINE VVR.CU.REL.CUS.THIRD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*To Inforce the user to enter only the Rel. Customer whose Sector is Related to the allowed
*Sector for his version

IF V$FUNCTION = 'I' THEN
 Y=''
 IF COMI THEN
F.COUNT = '' ; FN.COUNT = 'F.SCB.VER.IND.SEC.LEG';R.COUNT = '';R.COUNT1 = '';R.COUNT2 = '' ;R.COUNT3 = ''
VER1 = ',SCB.PRIVATE'
VER2 = ',SCB.CORPORATE'
*VER3 = ',SCB.BANK'
VER4 = ',SCB.THIRD'
 CALL OPF(FN.COUNT,F.COUNT)
 CALL F.READ(FN.COUNT,VER1,R.COUNT,F.COUNT,ERRORR)
 CALL F.READ(FN.COUNT,VER2,R.COUNT1,F.COUNT,ERRORR)
*CALL F.READ(FN.COUNT,VER3,R.COUNT2,F.COUNT,ERRORR)
 CALL F.READ(FN.COUNT,VER4,R.COUNT3,F.COUNT,ERRORR)
 CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI, SEC)
IND1 = R.COUNT<VISL.INDUSTRY>
SEC1 = R.COUNT<VISL.SECTOR>
IND2 = R.COUNT1<VISL.INDUSTRY>
SEC2 = R.COUNT1<VISL.SECTOR>
*IND3 = R.COUNT2<VISL.INDUSTRY>
*SEC3 = R.COUNT2<VISL.SECTOR>
IND4 = R.COUNT3<VISL.INDUSTRY>
SEC4 = R.COUNT3<VISL.SECTOR>

*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
FOR I = 1 TO DCOUNT(IND1,@VM)
    LOCATE SEC IN SEC1<1,I,1> SETTING M THEN   RETURN
  ELSE Y='YES'
NEXT I
*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
FOR I = 1 TO DCOUNT(IND2,@VM)
   LOCATE SEC IN SEC2<1,I,1> SETTING M THEN  RETURN
  ELSE Y='YES'
NEXT I
*FOR I = 1 TO DCOUNT(IND3,VM)
  *LOCATE SEC IN SEC3<1,I,1> SETTING M THEN  RETURN
* ELSE Y='YES'
*NEXT I
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
 FOR I = 1 TO DCOUNT(IND4,@VM)
   LOCATE SEC IN SEC4<1,I,1> SETTING M THEN  RETURN
  ELSE Y='YES'
 NEXT I

IF Y='YES' THEN ETEXT = 'SECTOR.OF.CUSTOMER.NOT.ALLOWED'

END
END

RETURN
END
