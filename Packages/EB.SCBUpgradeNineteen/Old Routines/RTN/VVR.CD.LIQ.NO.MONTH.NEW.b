* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>805</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.LIQ.NO.MONTH.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.LIQ.RATE

*TO DEFAULT FEILD CHRG.AMOUNT WITH APPROPRIATE AMOUNT ACCORDING TO THE NUMBER OF MONTH LIQUIDATED IN

*    IF MESSAGE NE 'VAL' THEN
    IF COMI THEN
        DAYS = "C"
        R.NEW(LD.FIN.MAT.DATE) = COMI
        DATE.VAL = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        IF DATE.VAL GT R.NEW(LD.FIN.MAT.DATE) THEN
            DATE.VAL = R.NEW(LD.VALUE.DATE)
        END ELSE
            DATE.VAL =R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        END
*****************************************
        IF DATE.VAL LE 20070907 THEN
            ETEXT='Must Be Grater Than 20070907'  ;CALL STORE.END.ERROR
            RETURN
        END
*****************************************
        DATE.EXP = COMI[1,6]:"01"
        CALL CDT ('',DATE.EXP,'-1C')
        CALL CDD("",DATE.VAL,COMI,DAYS)
        TEXT  = "DAYS":DAYS ; CALL REM

        IF DAYS LE "180" THEN
            ETEXT='��� �� ���� ���� �� 6����'  ;CALL STORE.END.ERROR
        END ELSE
*****************************************
            CON.ID = ID.NEW:"00"
            CALL DBR('LMM.ACCOUNT.BALANCES':@FM:LD27.START.PERIOD.INT,CON.ID,MYSTART)
*****************************************
*========== UPDATE BY BAKRY TO FIX DATE ERROR IN LMM.ACCOUNT.BALANCES 20130320 =============
            IF R.NEW(LD.CATEGORY) = 21021 THEN
                CHK.DATE = MYSTART
                SCB.N.O.D = "C"
                CALL CDD("",CHK.DATE,TODAY,SCB.N.O.D)
                IF SCB.N.O.D GT 92 THEN
                    TEXT = SCB.N.O.D ; CALL REM
                    ETEXT='���� ���� ����� ���������'  ;CALL STORE.END.ERROR
                END
            END
*========== END OF UPDATE BY BAKRY 20130320 =============

*                *TEXT = "MYSTART ": MYSTART ; CALL REM
*                *TEXT = "MYSTART[5,2]":MYSTART[5,2] ; CALL REM
*                *TEXT = "COMI[5,2]":COMI[5,2] ; CALL REM
            IF MYSTART # TODAY THEN
**                    IF MYSTART[5,2] = COMI[5,2] THEN
                R.NEW(LD.NEW.INT.RATE) = "0.00"
                R.NEW(LD.INT.RATE.V.DATE) =MYSTART
**                    END ELSE

**                        R.NEW(LD.NEW.INT.RATE) = "0.00"
**                        R.NEW(LD.INT.RATE.V.DATE) = COMI[1,6]:"01"
**                    END
            END
*******************************************
            IF DATE.VAL GE 20081012 THEN
                CATEG = R.NEW(LD.CATEGORY):"-081012"
            END ELSE
                CATEG = R.NEW(LD.CATEGORY)
            END
**-------------------------HYTHAM   20111123
***MODIEFID ON 4-6-2012
            IF  R.NEW(LD.CATEGORY) EQ '21021' THEN
                IF DATE.VAL GE 20111120  THEN
                    CATEG = R.NEW(LD.CATEGORY):"-111120"
                END
            END
**-------------------------HYTHAM   20111123
            IF  R.NEW(LD.CATEGORY) EQ '21020' THEN
                IF DATE.VAL GE 20120520  THEN
                    CATEG = R.NEW(LD.CATEGORY):"-120520"
                END
            END
**----------------   20130829
            LD.CAT = R.NEW(LD.CATEGORY)
            IF  ( LD.CAT EQ '21017' OR LD.CAT EQ '21018' OR LD.CAT EQ '21020' OR LD.CAT EQ '21021' ) THEN
                IF DATE.VAL GE 20130516  THEN
                    CATEG = R.NEW(LD.CATEGORY):"-130516"
                END
            END
*********************START OF MODIFCATION ABEER AS OF 20170418
            IF  ( LD.CAT EQ '21017' OR LD.CAT EQ '21018' OR LD.CAT EQ '21020' OR LD.CAT EQ '21021' ) THEN
                IF DATE.VAL GE 20161110  THEN
                    CATEG = R.NEW(LD.CATEGORY):"-161110"
                END
            END
            IF  ( LD.CAT EQ '21017' OR LD.CAT EQ '21018' OR LD.CAT EQ '21020' OR LD.CAT EQ '21021' ) THEN
                IF DATE.VAL GE 20161215  THEN
                    CATEG = R.NEW(LD.CATEGORY):"-161215"
                END
            END
***********************END OF MODIFCATION
*********** ADDED BY KHALED 2020/11/11 ******
            IF LD.CAT EQ '21025' THEN
                CATEG = R.NEW(LD.CATEGORY):"-200614"
            END
*********************************************
*TEXT  = "CAT=" : CATEG ; CALL REM

            F.CD.LIQ = '' ; FN.CD.LIQ = 'F.SCB.CD.LIQ.RATE' ; R.SCB.CD.LIQ = ''
            CALL OPF(FN.CD.LIQ,F.CD.LIQ)
            CALL F.READ(FN.CD.LIQ, CATEG, R.CD.LIQ, F.CD.LIQ, ETEXT)
*Line [ 139 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT =  DCOUNT (R.CD.LIQ<SCB.CD.LIQ.MIN.DAY>,@VM)
*               *TEXT ="TEMP.COUNT = ": TEMP.COUNT ; CALL REM
            FOR I = 1 TO TEMP.COUNT
                IF R.CD.LIQ<SCB.CD.LIQ.MIN.DAY,I> LE DAYS AND R.CD.LIQ<SCB.CD.LIQ.MAX.DAY,I> GE DAYS THEN
                    NEW.RATE= R.CD.LIQ<SCB.CD.LIQ.RATE,I>
                    AMT = R.NEW(LD.AMOUNT)
                    I = TEMP.COUNT
                END
            NEXT I

*TEXT = NEW.RATE ; CALL REM
*==========CALC INTREST AMOUNT PAYED ======================================
            ACT.DAYS = "C"
            CALL CDD("",DATE.VAL,MYSTART,ACT.DAYS)
            ACT.M = INT(ACT.DAYS/30)

            ACT.INT.RATE = R.NEW(LD.INTEREST.RATE)
            ACT.INT = ((AMT*ACT.INT.RATE)*(ACT.M/12)/100)
*TEXT = AMT:" ":ACT.INT.RATE:" ":(ACT.M/12) :" ":ACT.INT ; CALL REM
*==========CALC INTREST AMOUNT AFTER PENALTY RATE =========================
            NEW.ACT.INT.RATE = ACT.INT.RATE - NEW.RATE
            DAYSSS = "C"
            CALL CDD("",DATE.VAL,DATE.EXP,DAYSSS)
            DAYSSS = DAYSSS + 1
*TEXT = "NEW.RATE= " : NEW.RATE ; CALL REM
            NEW.INT = ((AMT*NEW.ACT.INT.RATE)*(DAYSSS/366)/100)
*TEXT = "NEW.ACT.INT.RATE= " : NEW.ACT.INT.RATE ; CALL REM
*TEXT = "DAYSSS=  " : DAYSSS ; CALL REM
            ACT.AMT = ACT.INT - NEW.INT
*TEXT = 'ACT.AMT= ' : ACT.AMT ; CALL REM
*TEXT = 'ACT.INT= ' : ACT.INT ; CALL REM
*TEXT = NEW.INT ; CALL REM
*============================================================================
            IF ACT.AMT GT 0 THEN
                CALL EB.ROUND.AMOUNT ('EGP',ACT.AMT,'',"")
                R.NEW(LD.CHRG.AMOUNT) = ACT.AMT
                R.NEW(LD.CHRG.CLAIM.DATE) = TODAY
                R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> = (AMT-ACT.AMT)
            END ELSE
                IF ACT.AMT LT 0 THEN
*TEXT = "��� ������� ����� ���� �����":(ACT.AMT * -1) ; CALL REM
                    AMTT = ACT.AMT * -1
                    CALL EB.ROUND.AMOUNT ('EGP',AMTT,'',"")
                    R.NEW(LD.REIMBURSE.PRICE)= ""
                    R.NEW(LD.REIMBURSE.AMOUNT) = AMTT +  R.NEW(LD.AMOUNT)
                    R.NEW(LD.CHRG.AMOUNT)= "0.00"
                    R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> =  AMTT +  R.NEW(LD.AMOUNT)
                END
            END
        END
        IF COMI # TODAY THEN ETEXT='��� �� ���� ����� �����'  ;CALL STORE.END.ERROR
*    END
        CALL REBUILD.SCREEN
        RETURN
    END
