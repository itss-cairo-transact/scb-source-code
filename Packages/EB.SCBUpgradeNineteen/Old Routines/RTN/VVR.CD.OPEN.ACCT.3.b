* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.ACCT.3

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN
*********************
        CD.CURR= R.NEW(LD.CURRENCY)
        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COMI,ACC.CUR)
        IF CD.CURR NE ACC.CUR THEN
            ETEXT='MUST BE SAME CD CURRENCY';CALL STORE.END.ERROR
        END ELSE
            R.NEW(LD.INT.LIQ.ACCT) = COMI
        END
**********************
    END
    IF COMI NE '' THEN
        IF COMI[9,2] NE '10' THEN
            ETEXT = ' Must Be EGP ';CALL STORE.END.ERROR
        END
    END
    CALL REBUILD.SCREEN

*TO CHECK IF WORKING BALANCE LESS THAN CD AMOUNT THEN ERROR MESSAGE WILL BE DISPLAYED
    IF MESSAGE EQ 'VAL' THEN
        CD.AMT=R.NEW(LD.AMOUNT)
        CD.DR.ACCT=R.NEW(LD.DRAWDOWN.ACCOUNT)
        CALL DBR ('ACCOUNT':@FM:AC.WORKING.BALANCE,CD.DR.ACCT,MYBAL)
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,CD.DR.ACCT,CATEG)

*        IF CATEG NE '1202' THEN
        IF NOT(CATEG GE '1101' AND CATEG LE '1599') THEN

            CD.NOM = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>

            IF CD.NOM NE '' THEN
                CD.AMT = CD.NOM * R.NEW(LD.AMOUNT)
*    TEXT = "AMT=  ":  CD.AMT ; CALL REM
            END ELSE
                CD.AMT=R.NEW(LD.AMOUNT)
            END
*      IF R.NEW(LD.DEPT.CODE) NE '12' THEN
            IF MYBAL LT CD.AMT THEN
                ETEXT = 'Working Balance Less Than Zero' ;CALL STORE.END.ERROR
            END
*     END
        END
    END


    RETURN
END
