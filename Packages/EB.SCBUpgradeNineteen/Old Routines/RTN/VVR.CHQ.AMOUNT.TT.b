* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHQ.AMOUNT.TT

* TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER
* AND TO CHECK ALSO THAT THE WITHDRAWAL AMOUNT IS LESS TTHAN OR EQUAL THE
* AVAILABLE BALANCE


*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER


    IF MESSAGE # 'VAL' THEN
* IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1) THEN ETEXT = '��� ����� ������'
* R.NEW(TT.TE.AMOUNT.LOCAL.1) = COMI
* CALL REBUILD.SCREEN
    END
    IF MESSAGE # 'VAL' THEN

        ACCT =  R.NEW(TT.TE.ACCOUNT.2)
*AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)
      *  IF ACCT[11,4] # '1201' THEN
        IF NOT(ACCT[11,4] GE 1500 AND ACCT[11,4] LE 1590) THEN
            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            FOR I=1 TO LOCK.NO
                CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
                LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
            NEXT I
            NET.BAL=BAL-LOCK.AMT1

*IF AMT1 GT BAL THEN
            IF COMI GT NET.BAL THEN
                ETEXT = '������ �� ����'
            END
        END
    END
    RETURN
END
