* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CHQ.APP.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    CUS.NO = R.NEW(CHQA.CUST.NO)
    NO.CHQ = R.NEW(CHQA.NO.OF.BOOKS)
    CHQ.TYP = R.NEW(CHQA.CHEQ.TYPE)

    OK.FLG = 0

    CALL DBR('CUSTOMER':@FM:EB.CUS.POSTING.POSTING.RESTRICT,CUS.NO,P.RESTRICT)
    IF P.RESTRICT NE '' THEN
        OK.FLG = 1
        E = 'RESPONS.CODE=75' ;  CALL ERR; MESSAGE='REPEAT'
    END
    IF NO.CHQ LE 0 OR NO.CHQ GT 99 THEN
        OK.FLG = 1
        E = 'RESPONS.CODE=76' ;  CALL ERR; MESSAGE='REPEAT'
    END

    IF  CHQ.TYP NE 25 AND CHQ.TYP NE 50 THEN
        OK.FLG = 1
        E = 'RESPONS.CODE=77' ;  CALL ERR; MESSAGE='REPEAT'
    END
    IF  OK.FLG = 0 THEN
        E = 'RESPONS.CODE=A00' ;  CALL ERR; MESSAGE='REPEAT'
    END

END
