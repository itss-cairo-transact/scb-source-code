* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1235</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHRG.DEF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF MESSAGE # 'VAL' THEN
        IF COMI EQ 'NO'  THEN

            E        = ''
            ETEXT    = ''
            ERR      = ''
            MESSAGE  = ''

            R.NEW(TF.DR.CHARGE.CODE)<1,1> = "TFIMCOLAVV"
            R.NEW(TF.DR.CHARGE.CODE)<1,2> = "TFIMPLOCAL"
            R.NEW(TF.DR.CHARGE.CODE)<1,3> = "TFECOLLDIS"
            R.NEW(TF.DR.CHARGE.CODE)<1,4> = "TFICOLLTRN"
            R.NEW(TF.DR.CHARGE.CODE)<1,5> = "TFIMPFROPY"
            R.NEW(TF.DR.CHARGE.CODE)<1,6> = "TFIMPCHSWCH"
            R.NEW(TF.DR.CHARGE.CODE)<1,7> = "TFIMPPHOTCH"

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
