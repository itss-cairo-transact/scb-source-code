* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DR.MATURITY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLIDAY

    IF R.NEW(TF.DR.DRAWING.TYPE)  = 'MD' THEN
        IF COMI THEN
            BACK.DAT = TODAY
            CALL CDT('',BACK.DAT,'-3W')


            FWD.DAT = TODAY
TEXT=FWD.DAT:'B4';CALL REM
            CALL CDT('',FWD.DAT,'+10')

            TEXT =FWD.DAT:'FWD.DAT';CALL REM
            BEGIN CASE
            CASE COMI LT BACK.DAT
**                ETEXT = "MUST BE GT ":BACK.DAT ;CALL STORE.END.ERROR
            CASE COMI GT TODAY
*  ETEXT = "CANT BE FORWARD " ;CALL STORE.END.ERROR
            CASE OTHERWISE
              *  R.NEW(TF.DR.VALUE.DATE) =   COMI
            END CASE
            CALL REBUILD.SCREEN
        END
    END

    RETURN
END
