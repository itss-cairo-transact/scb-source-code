* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CALC.TOT.AMT.3

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*-------------------------------------------------
    AMTT = 0 ; TOTA = 0 ; X = 0 ; Y = 0
*Line [ 38 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M   = DCOUNT(R.NEW(SCB.BT.OUR.REFERENCE),@VM)

    FOR K  = 1 TO NO.M
        IF R.NEW(SCB.BT.RETURN.REASON)<1,K> EQ '' THEN
            X = R.NEW(SCB.BT.OUR.REFERENCE)<1,K>
            CALL DBR('BILL.REGISTER':@FM: EB.BILL.REG.AMOUNT,X,AMTT)

            Y    += R.NEW(SCB.BT.CHRG.AMT)<1,K>
            TOTA += AMTT
        END ELSE
            Y    += R.NEW(SCB.BT.CHRG.AMT)<1,K>
            TEXT = K:" = ":R.NEW(SCB.BT.CHRG.AMT)<1,K> ; CALL REM
        END
    NEXT K

    R.NEW(SCB.BT.TOT.DEBIT.AMT)  = TOTA
    R.NEW(SCB.BT.TOT.CHRG.AMT)   = Y
    R.NEW(SCB.BT.TOT.CREDIT.AMT) = TOTA - Y
    TEXT = Y ; CALL REM
    CALL REBUILD.SCREEN

    RETURN
END
