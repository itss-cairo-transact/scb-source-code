* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
****14/04/2005 NESSREEN *************
*-----------------------------------------------------------------------------
* <Rating>93</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHQ.ISSUE.START.NO


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER

    CHQ.NO =  R.NEW(CHEQUE.IS.NUMBER.ISSUED)
****UPDATED BY NESSREEN AHMED 22/2/2010*******************
**    R.NEW(CHEQUE.IS.CHG.AMOUNT) = CHQ.NO / 2
**    R.NEW(CHEQUE.IS.CHG.CODE) = 'ISSUCHQ'
**********************************************************
    IF MESSAGE EQ '' THEN
        LAST.ISSUE = ''
*   ZZ = R.NEW(CHEQUE.IS.CHQ.NO.START)
*    TEXT = "CHEQUE.IS.CHQ.NO.START" : ZZ ; CALL REM
        ACC.NO = ''
        IF ID.NEW[1,3] = 'SCB' THEN
            ACC.NO = ID.NEW[1,20]
*TEXT = "ACC.NO1" : ACC.NO ; CALL REM
        END ELSE
            ACC.NO = ID.NEW[1,21]
*TEXT = "ACC.NO2" : ACC.NO ; CALL REM
        END
*TEXT = ACC.NO ; CALL REM
        CALL DBR ('CHEQUE.REGISTER':@FM:CHEQUE.REG.ISSUED.THIS.PD,ACC.NO,LAST.ISSUE)
        IF ETEXT THEN ETEXT = ''
*TEXT = LAST.ISSUE ; CALL REM
        IF LAST.ISSUE THEN
            NEW.CHQ.STRT.NO= LAST.ISSUE+1
            R.NEW(CHEQUE.IS.CHQ.NO.START)=NEW.CHQ.STRT.NO
            CALL REBUILD.SCREEN
        END ELSE
            R.NEW(CHEQUE.IS.CHQ.NO.START)=1
            CALL REBUILD.SCREEN
        END
* TEXT = R.NEW(CHEQUE.IS.CHQ.NO.START) ;CALL REM
    END
    RETURN
END
