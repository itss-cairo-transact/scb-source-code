* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>47</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHQ.NO.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

*    IF MESSAGE # 'VAL' THEN
        ACC.CAT = ''
        ACC.NO = ''
        XX1 = ''
*Line [ 32 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YYCOUNT = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
        FOR XX1 = 1 TO YYCOUNT
            IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,XX1> THEN
                ACC.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,XX1>
                IN.SIGN= R.NEW(INF.MLT.SIGN)<1,XX1>
                CALL DBR('ACCOUNT':@FM:2,ACC.NO,ACC.CAT)
*                TEXT = ACC.CAT ; CALL REM
*                TEXT = "ACC.NO = ":ACC.NO ; CALL REM
                IF ACC.CAT EQ '16151' AND IN.SIGN EQ 'CREDIT' THEN
                    IF NOT(R.NEW(INF.MLT.CHEQUE.NUMBER)<1,XX1>) THEN
                        AF = INF.MLT.CHEQUE.NUMBER ; AV = XX1 ; AS = 1
                        ETEXT = 'INPUT MISSING'
                        CALL STORE.END.ERROR
                    END
                END
            END
        NEXT XX1
*    END
    CALL REBUILD.SCREEN
    RETURN
END
