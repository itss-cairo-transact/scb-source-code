* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHQ.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ

    ACC.CAT = ''
    ACC.NO = ''
    XX1 = ''
    FN.CHQ1 = 'F.SCB.FT.DR.CHQ' ; R.CHQ1 = '' ; F.CHQ1 = '' ERR.CH = ''
    CALL OPF(FN.CHQ1,F.CHQ1)
    IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,AV> THEN
        ACC.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,AV>
        IN.SIGN= R.NEW(INF.MLT.SIGN)<1,AV>
        CALL DBR('ACCOUNT':@FM:2,ACC.NO,ACC.CAT)
    END
    IF COMI THEN
        GOSUB FINDCHQ
    END ELSE
        IF ACC.CAT EQ '16151' AND IN.SIGN EQ 'CREDIT' THEN
            IF NOT(R.NEW(INF.MLT.CHEQUE.NUMBER)<1,AV>) THEN
                ETEXT = '����� ����� ��� �����'
*                CALL STORE.END.ERROR
            END
        END
    END
    CALL REBUILD.SCREEN
    RETURN
********************************************************
FINDCHQ:
*-------
    CH.ID = ACC.NO:".":COMI
    CALL F.READ(FN.CHQ1,CH.ID,R.CHQ1,F.CHQ1,ERR.CH)
    IF R.CHQ1 THEN
        ETEXT = '��� ����� ���� �� ���' ;* CALL ERR ; MESSAGE = 'REPEAT'
    END
    RETURN
********************************************************
END
