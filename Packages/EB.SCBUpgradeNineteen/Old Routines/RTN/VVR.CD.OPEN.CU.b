* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
***ABEER ***
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.CU

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    STO5=POSS<1,5>
    STO6=POSS<1,6>
    STO7=POSS<1,7>
    STO8=POSS<1,8>
    STO9=POSS<1,9>
    STO10=POSS<1,10>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
    END

*TO CHECK IF CUSTOMER.ID CHANGED FIELDS CATEGORY,CURRENCY,AMOUNT,FIN.MAT.DATE WILL BE EMPTY
    IF MESSAGE EQ '' THEN
        IF COMI NE R.NEW(LD.CUSTOMER.ID) THEN
            R.NEW(LD.CATEGORY)=''
            R.NEW(LD.CURRENCY)=''
            R.NEW(LD.AMOUNT)=''
            R.NEW(LD.VALUE.DATE)=TODAY
            R.NEW(LD.FIN.MAT.DATE)=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>=''
*  R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY.NAU>=''
            R.NEW(LD.DRAWDOWN.ACCOUNT)=''
            R.NEW(LD.INTEREST.RATE)=''
            R.NEW(LD.INT.LIQ.ACCT)=''
        END
        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,COMI,MYLOCAL)
        NAME.CD = MYLOCAL<1,CULR.ARABIC.NAME>
        R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF> = NAME.CD
        CALL REBUILD.SCREEN
    END
    RETURN
END
