* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1196</Rating>
*-----------------------------------------------------------------------------
*************DINA-SCB***********

    SUBROUTINE VVR.CU.REL.CUS.PRIVATE1
*the routine inforce user to enter the guardian with the customer who is
*under 21 years old . and make sure that the guardian with suitable sector
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF R.NEW(EB.CUS.CUSTOMER.STATUS)= '22' OR R.NEW(EB.CUS.RELATION.CODE)= '42' THEN
**************************************
* R.NEW(EB.CUS.CUSTOMER.LIABILITY) = COMI
***************************************

*1 5 00821
************UPDATED BY NESSREEN AHMED 04/01/2010
**     IF LEN(COMI) EQ 7 THEN
**         IF COMI[2,1] EQ 5 THEN
**             R.NEW(EB.CUS.CUSTOMER.LIABILITY) = ID.NEW
**         END ELSE
**             R.NEW(EB.CUS.CUSTOMER.LIABILITY) = COMI
**         END
**     END
*11 5 00821
**     IF LEN(COMI) EQ 8 THEN
**         IF COMI[3,1] EQ 5 THEN
**             R.NEW(EB.CUS.CUSTOMER.LIABILITY) = ID.NEW
**         END ELSE
**             R.NEW(EB.CUS.CUSTOMER.LIABILITY) = COMI
**         END

**     END
        END
**********
        CALL REBUILD.SCREEN
*IF V$FUNCTION = 'I' THEN
        Y=''
        IF COMI THEN
            F.COUNT = '' ; FN.COUNT = 'F.SCB.VER.IND.SEC.LEG';R.COUNT = '';R.COUNT1 = '';R.COUNT2 = ''
*VER1 = ',SCB.STAFF'
            VER2 = ',SCB.THIRD'
            VER3 = ',SCB.PRIVATE'
            CALL OPF(FN.COUNT,F.COUNT)
*CALL F.READ(FN.COUNT,VER1,R.COUNT,F.COUNT,ERRORR)
            CALL F.READ(FN.COUNT,VER2,R.COUNT1,F.COUNT,ERRORR)
            CALL F.READ(FN.COUNT,VER3,R.COUNT2,F.COUNT,ERRORR)
            CALL DBR('CUSTOMER':@FM:EB.CUS.BIRTH.INCORP.DATE,COMI,BIRTH.DATE)
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI, SEC)

            BIRTH1 = R.NEW(EB.CUS.BIRTH.INCORP.DATE)
            AGE1 = ( BIRTH1[ 1, 4] + 21 ) : BIRTH1[ 5, 4]
            IF AGE1 > TODAY THEN
                AGE = ( BIRTH.DATE[ 1, 4] + 21 ) : BIRTH.DATE[ 5, 4]
****UPDATED BY NESSREEN AHMED 11/7/2018*************
****            IF AGE > TODAY THEN ETEXT ='GUARDIAN.MUST.BE.ADULT'
                REL.CODE = R.NEW(EB.CUS.RELATION.CODE)
                IF REL.CODE NE 110 THEN
                    IF AGE > TODAY THEN ETEXT ='GUARDIAN.MUST.BE.ADULT'
                END
****END OF UPDATE 11/7/2018*************************
            END
*************************************************************************************************
*IND1 = R.COUNT<VISL.INDUSTRY>
*SEC1 = R.COUNT<VISL.SECTOR>
            IND2 = R.COUNT1<VISL.INDUSTRY>
            SEC2 = R.COUNT1<VISL.SECTOR>
            IND3 = R.COUNT2<VISL.INDUSTRY>
            SEC3 = R.COUNT2<VISL.SECTOR>
*FOR I = 1 TO DCOUNT(IND1,VM)
*LOCATE SEC IN SEC1<1,I,1> SETTING M THEN   RETURN
*ELSE Y='YES'
*NEXT I
*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(IND2,@VM)
                LOCATE SEC IN SEC2<1,I,1> SETTING M THEN  RETURN
                ELSE Y='YES'
            NEXT I
*Line [ 104 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(IND3,@VM)
                LOCATE SEC IN SEC3<1,I,1> SETTING M THEN  RETURN
                ELSE Y='YES'
            NEXT I
* IF Y = 'YES' THEN ETEXT = 'SECTOR.OF.CUSTOMER.NOT.ALLOWED'

        END
    END
**********

**********
    RETURN
END
