* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DRAW.PERC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

***************************
    ETEXT=''
***************************************************

    IF MESSAGE EQ 'VAL' THEN
        FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
        CALL OPF(FN.LCC,F.LCC)
        ID.LC = ID.NEW[1,12]
        CALL F.READ(FN.LCC,ID.LC,R.LCC,F.LCC,READ.ERR)
**-----------HYTHA  20110714
*       CR.ACCT  = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
*    IF CR.ACCT NE "" THEN
*       CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CR.ACCT,CATEG.CR)
*   END
**-----------HYTHA  20110714

        DRW.TYPE = R.NEW(TF.DR.DRAWING.TYPE)

        LC.ACCT= LC.REC(TF.LC.CREDIT.PROVIS.ACC)

        IF LC.ACCT NE "" THEN
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,LC.ACCT,CATEG.LC)
        END
        ETEXT = ''
        IF R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>  NE '' THEN

            LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)
            TEXT=LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>:"---":LC.REC(TF.LC.PROVIS.PERCENT);CALL REM
            TEXT=R.LCC<TF.LC.PROVIS.PERCENT>:'PROV PERC';CALL REM

            LC.REC(TF.LC.PROVIS.PERCENT) = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
            TEXT=LC.REC(TF.LC.PROVIS.PERCENT):'****':R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>;CALL REM

            R.LCC<TF.LC.PROVIS.PERCENT> = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
*******************************UPDATED BY RIHAM R15**********************
*            WRITE  R.LCC TO F.LCC , ID.LC ON ERROR
*                PRINT "CAN NOT WRITE RECORD":ID.LC:"TO" :FN.LCC
*            END
            CALL F.WRITE(FN.LCC,ID.LC,R.LCC)
********************************************************************
        END
************
    END

    RETURN
END
