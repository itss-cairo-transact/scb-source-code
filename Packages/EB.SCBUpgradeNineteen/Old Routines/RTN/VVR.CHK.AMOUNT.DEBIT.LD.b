* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.LD

* TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER
* TO CHECK THE AVAILABLE BALANCE TO WITHDRAWAL

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION

    IF COMI THEN

***HYTHAM*****20101214***************

        IF R.NEW(LD.AMOUNT) NE '' THEN

            IF COMI NE R.NEW(LD.AMOUNT) THEN
                R.NEW(LD.INTEREST.RATE) = ''
            END

        END

***HYTHAM*****20101214***************



        BEGIN CASE
        CASE R.NEW(LD.CURRENCY) = "EGP"
            IF COMI LT "2000" THEN
                ETEXT = '��� ������ ��� �� ������� �� ������ �������'
                CALL REBUILD.SCREEN

            END
        CASE R.NEW(LD.CURRENCY) = "USD"
            IF COMI LT "500" THEN
                ETEXT = '��� ������ ��� �� ������� �� ������ �������'
                CALL REBUILD.SCREEN

            END
        CASE R.NEW(LD.CURRENCY) = "SAR"
            IF COMI LT "1874.92" THEN
                ETEXT = '��� ������ ��� �� ������� �� ������ �������'
                CALL REBUILD.SCREEN

            END
        CASE R.NEW(LD.CURRENCY) = "GBP"
            IF COMI LT "254.71" THEN
                ETEXT = '��� ������ ��� �� ������� �� ������ �������'
                CALL REBUILD.SCREEN

            END
        CASE R.NEW(LD.CURRENCY) = "EUR"
            IF COMI LT "350" THEN
**    ETEXT = '��� ������ ��� �� ������� �� ������ �������'
                CALL REBUILD.SCREEN

            END
        CASE R.NEW(LD.CURRENCY) = "CHF"
            IF COMI LT "609.35" THEN
                ETEXT = '��� ������ ��� �� ������� �� ������ �������'
                CALL REBUILD.SCREEN

            END
        CASE OTHERWISE
            ETEXT = '���� ���'
        END CASE



    END
    IF MESSAGE = 'VAL' THEN
        CURR =  R.NEW(LD.CURRENCY)
        AMT = COMI
        CATEG = R.NEW(LD.CATEGORY)
        F.LD.TXN = '' ; FN.LD.TXN = 'FBNK.LD.TXN.TYPE.CONDITION' ; R.LD.TXM = ''
        CALL OPF(FN.LD.TXN,F.LD.TXN)
        CALL F.READ(FN.LD.TXN, CATEG, R.LD.TXN, F.LD.TXN, ETEXT)
        LOCATE CURR IN R.LD.TXN<LTTC.CURRENCY,1> SETTING POS THEN
*            TEXT = R.LD.TXN<LTTC.MIN.INIT.AMT,POS> ; CALL REM
            IF AMT LT R.LD.TXN<LTTC.MIN.INIT.AMT,POS> THEN
                ETEXT = '��� ������ ��� �� ������� ��'
                CALL REBUILD.SCREEN
            END
        END

    END
    IF MESSAGE # 'VAL' THEN
        IF COMI # R.NEW(LD.AMOUNT) THEN ETEXT = '��� ����� ������'
        R.NEW(LD.AMOUNT) = COMI
        CALL REBUILD.SCREEN
    END
    IF MESSAGE = 'VAL' THEN

        ACCT =  R.NEW( LD.DRAWDOWN.ACCOUNT)
*   AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)
*  IF ACCT[11,4] # '1201' THEN
        IF NOT(ACCT[11,4] GE 1500 AND ACCT[11,4] LE 1590 ) THEN
            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 126 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            LOCK.AMT1=LOCK.AMT<1,LOCK.NO>
            FOR I=1 TO LOCK.NO
                CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*                LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>

            NEXT I
            NET.BAL=BAL-LOCK.AMT1
*    IF AMT1 GT BAL THEN
            IF R.NEW(LD.RECORD.STATUS) EQ '' THEN
                IF COMI GT NET.BAL AND NOT(LOCK.AMT1)  THEN
                    ETEXT = '������ �� ���� ';CALL STORE.END.ERROR
                END
                IF COMI GT NET.BAL AND LOCK.AMT1 THEN
                    ETEXT = '������ �� ���� ����� ���� ';CALL STORE.END.ERROR
                END
            END
        END
    END
    RETURN
END
