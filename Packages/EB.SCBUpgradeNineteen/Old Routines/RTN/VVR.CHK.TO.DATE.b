* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
********NESSREEN AHMED 16/12/2009*******
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHK.TO.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.DATE

    E = '' ; TDATE = '' ; TYEAR = '' ; YYYY = '' ; MM = '' ; MM1 = '' ; DD.DATE = '' ; S.DAT = ''
    DATE.FR =  R.NEW(VPD.DATE.FROM)

    IF COMI THEN
        YYYY = COMI[1,4]
        MM = COMI[5,2]
        DD = COMI[7,2]
        YYDD = COMI[1,6]

        TDATE = TODAY
        TYEAR = TDATE[1,4]
        TMON  = TDATE[5,2]
        TDD   = TDATE[7,2]
        TTYYDD = TDATE[1,6]

        IF YYDD # TTYYDD THEN
            ETEXT = '��� �� ���� ����� ������'
            CALL STORE.END.ERROR
            ETEXT = ''
        END
        IF DD < 15 OR DD > 24 THEN
            ETEXT = '��� �� ���� ������'
            CALL STORE.END.ERROR
            ETEXT = ''
        END
        IF COMI < DATE.FR THEN
            ETEXT = '��� �� ���� ����� ������ ������ ���� �� �������'
            CALL STORE.END.ERROR
            ETEXT = ''
        END
    END

    RETURN
END
