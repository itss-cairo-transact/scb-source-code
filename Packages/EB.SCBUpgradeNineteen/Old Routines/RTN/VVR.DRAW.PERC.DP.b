* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>700</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DRAW.PERC.DP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

***************************
    ETEXT=''
    FN.DR.N = 'FBNK.DRAWINGS$NAU' ;F.DR.N = '' ; R.DR.N = ''
    CALL OPF(FN.DR.N,F.DR.N)
    CALL F.READ(FN.DR.N,ID.NEW,R.DR.N,F.DR.N,READ.ERR)

    LOC.REF.NAU = R.DR.N<TF.DR.LOCAL.REF>
    DRW.PER.NAU = LOC.REF.NAU<1,DRLR.DRAEING.PERCENT>
    DOC.AMT.NAU = R.DR.N<TF.DR.DOCUMENT.AMOUNT>
*******
    FN.DR = 'FBNK.DRAWINGS' ;F.DR = '' ; R.DR = ''
    CALL OPF(FN.DR,F.DR)
    CALL F.READ(FN.DR,ID.NEW,R.DR,F.DR,READ.ERR)

    LOC.REF = R.DR<TF.DR.LOCAL.REF>
    DRW.PER = LOC.REF<1,DRLR.DRAEING.PERCENT>
    DOC.AMT = R.DR<TF.DR.DOCUMENT.AMOUNT>
************
    IF COMI  NE '' THEN
        IF R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT> EQ '' THEN
            IF DOC.AMT.NAU NE '' THEN
                IF DRW.PER.NAU NE '' THEN
                    IF COMI NE DRW.PER.NAU THEN
                        ETEXT='CANT CHANE IN DRW PERC';CALL STORE.END.ERROR
                    END
                END ELSE
                    IF COMI THEN
                        ETEXT='CANT CHANE IN DRW PERC';CALL STORE.END.ERROR
                    END
                END
            END ELSE
                IF DOC.AMT NE '' THEN
                    IF DRW.PER NE '' THEN
                        IF COMI NE DRW.PER THEN
                            ETEXT='CANT CHANE IN DRW PERC';CALL STORE.END.ERROR
                        END   ELSE
                            ETEXT=''
                        END
                    END
                END ELSE
                END
            END
        END ELSE
            ETEXT='ONLY AMT OR PERC';CALL STORE.END.ERROR
        END
    END ELSE
        IF COMI EQ '' OR COMI EQ '0' THEN
            IF DRW.PER.NAU NE '' THEN
                IF COMI NE DRW.PER.NAU THEN
                    ETEXT='CANT CHANE IN DRW PERC';CALL STORE.END.ERROR
                END
            END ELSE
                IF DRW.PER NE '' THEN
                    IF COMI NE DRW.PER THEN
                        ETEXT='CANT CHANE IN DRW PERC';CALL STORE.END.ERROR
                    END
                END   ELSE
                    IF COMI EQ '0' THEN
                        R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>=''
TEXT=COMI:'D';CALL REM
                        CALL REBUILD.SCREEN
                    END
                END

            END
        END
    END
***************************************************
    RETURN
END
