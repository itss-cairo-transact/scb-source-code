* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CUS.COLL.LIMIT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COLL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    IF MESSAGE EQ '' THEN
        IF COMI THEN
            CATEG = R.NEW(AC.CATEGORY)
            COLL.ID = COMI :".": CATEG
** TEXT = "COLL.ID" : COLL.ID ; CALL REM
            CUS.ID = R.NEW(AC.CUSTOMER)
            CALL DBR('SCB.COLL':@FM:SCB.COLL.LIMIT.REF,COLL.ID,LIMIT.REF)
**TEXT = "LIMIT.REF" : LIMIT.REF ; CALL REM
            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''

            T.SEL  = "SSELECT FBNK.LIMIT WITH @ID LIKE ": CUS.ID : "...":LIMIT.REF :"..."

            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
**TEXT ="T.SEL" : T.SEL ; CALL REM
            IF NOT(SELECTED) THEN
                R.NEW(AC.LIMIT.REF) = ""
                ETEXT = "NO LIMIT"
            END ELSE
                R.NEW(AC.LIMIT.REF)= LIMIT.REF
            END
*        IF COMI NE R.NEW(AC.LOCAL.REF)<ACLR.REP.COLLAT.CODE> THEN
*           R.NEW(AC.LIMIT.REF) = ""
*      END

**    CALL REBUILD.SCREEN
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
