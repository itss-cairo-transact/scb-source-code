* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*----------------MAI 26 FEB 2020--------------------------------
* <Rating>-8</Rating>
*---------------------------------------------------------------
    SUBROUTINE  VVR.DEFAULT.ATM.ACC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    COMP = C$ID.COMPANY
    ETEXT = '' ; ACC.NO = ''

    IF COMI THEN
        F.ATM.AC = '' ; FN.ATM.AC = 'F.SCB.ATM.TERMINAL.ID' ; R.ATM.AC = '' ; E1 = '' ; RETRY1 = ''
        CALL OPF(FN.ATM.AC,F.ATM.AC)

        ATM.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.NUMBER>
        TEXT = 'ATM.NO=':ATM.NO ; CALL REM

        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE, COMI,CURR)

        CALL F.READ(FN.ATM.AC,ATM.NO, R.ATM.AC, F.ATM.AC, E1)

        IF COMI EQ 'EGP' THEN
            R.NEW(FT.CREDIT.ACCT.NO) = ''
            ETEXT='Only foriegn currencies are allowed'  ;CALL STORE.END.ERROR
        END ELSE
            CURR = COMI
            TEXT = 'CURR=' :CURR ; CALL REM
            LOCATE CURR IN R.ATM.AC<SCB.ATM.CURRENCY,1> SETTING NN THEN
                ATM.FX.ACC = R.ATM.AC<SCB.ATM.ACCOUNT.NUMBER.FOREX, NN>
                TEXT = 'ATM.FX.ACC=':ATM.FX.ACC ; CALL REM
            END
            R.NEW(FT.CREDIT.ACCT.NO) = ATM.FX.ACC
            R.NEW(FT.DEBIT.ACCT.NO) = COMI:'1000099990099'
        END

        CALL REBUILD.SCREEN
    END

    RETURN
END
