* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>482</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.FCY

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF MESSAGE = '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> THEN ETEXT = '��� ����� ������' ; CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
        ETEXT = ''
        ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
********NESSREEN AHMED 09/01/2007*****************************************
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
***UPDATED BY NESSREEN AHMED 6/12/2017**************************
        TRANS.CODE = R.NEW(TT.TE.TRANSACTION.CODE)
        IF (TRANS.CODE EQ 2) OR (TRANS.CODE EQ 41) THEN
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT, CURR)
            IF CURR NE 'EGP' THEN
                R.NEW(TT.TE.WAIVE.CHARGES) = 'YES'
            END
        END
***END OF UPDATED 6/12/2017***********************************
***
*****NESSREEN AHMED 18/03/2009 *********************************
        IF CATEGG NE 5010 THEN
****************************************************************
            IF (CATEGG GE 1101 AND CATEGG LE 1590 ) THEN
                CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
                CUST = R.NEW(TT.TE.CUSTOMER.1)
                IDD = FMT(LIMTREF, "R%10")
                KEY.ID = CUST:'.':IDD
                CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
**TEXT = '���� ������=':AVLM ; CALL REM
**TEXT = '������ ������=':BAL ; CALL REM
                EQT = AVLM + BAL - COMI
**TEXT = '������ ������ �����=':EQT ; CALL REM
                ETEXT = ''
                IF EQT < 0 THEN
                    ETEXT = "�� ��� �� ������ ������ �� �����"
                    CALL STORE.END.ERROR
                END
            END     ;*END OF IF (CATEGG
****************************************************
            IF NOT( ACCT[11,4] GE 1101  AND ACCT[11,4] LE 1590) THEN
                CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
*********NESSREEN 16/1/2007*************************************
                FOR I=1 TO LOCK.NO
                    CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
                    LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
                NEXT I
                NET.BAL=BAL-LOCK.AMT1
                NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
***********NESSREEN  17/9/2006***********************************
                IF LOCK.AMT1 > 0 THEN
                    TEXT = '������ ������� ����=':LOCK.AMT1 ; CALL REM
                    TEXT = '������ ������� =': NET.BAL ; CALL REM
                END
*****************************************************************
                IF COMI GT NET.BAL THEN
                    ETEXT = '������ �� ����'
                END
            END
        END
********************************************************************
    END   ;***END OF MESSAGE#VAL
*************UPDATED IN 4/1/2007*********
    IF R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> GT 20000 THEN ETEXT = '��� �� �� ���� ������ ���� �� 20000'
****************************************

    RETURN
END
