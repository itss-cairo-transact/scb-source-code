* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>132</Rating>
*-----------------------------------------------------------------------------
**** this routine To Default Customer Name by close if posting restriction eq 99 for customer banks ******
**** and not close the customer if have any application id
**** all check dependent for POSTING.RESTRICT FLD *** regenerate by Nessma 2010/11/28
**********************************************************************************************************
    SUBROUTINE VVR.CU.POS.REST
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------------------
    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"   ; F.LD = ""

    FN.LMM = "FBNK.LMM.CUSTOMER"   ; F.LMM = ""

    FN.LI = "FBNK.LIMIT.LIABILITY"   ; F.LI = ""

    FN.LC = "FBNK.LC.APPLICANT"   ; F.LC = ""
    FN.LCC = "FBNK.LETTER.OF.CREDIT"   ; F.LCC = ""
    FN.RC  = "FBNK.RELATION.CUSTOMER"  ; F.RC  = ""
    FN.IM.REF  = "F.IM.REFERENCE"  ; F.IM.REF  = ""
*------------------------------------------------------
    FLAG = 0
*** Stoped by mohamed sabry 2012/12/17

*   IF PGM.VERSION NE ',ALL.CUS.DATA' AND PGM.VERSION NE ',ALL.CUS.DATA.CLEAR' THEN
*UPDATE BY MOHAMED SABRY & NESSMA MOHAMED 2011/06/29 TO STOP IF COMI
*       IF COMI = '18' THEN
*           ETEXT = '��� ����� ������� �� ��� ������'
*           CALL STORE.END.ERROR
*       END
*       IF R.NEW(EB.CUS.POSTING.RESTRICT) # '' THEN
*           IF (R.NEW(EB.CUS.POSTING.RESTRICT) = '18') AND (COMI # R.NEW(EB.CUS.POSTING.RESTRICT)) THEN
*               ETEXT = '��� ����� ������� �� ��� ������'
*               CALL STORE.END.ERROR
*           END
*       END
*------------------------------------------------------
****Updated by Nessreen Ahmed 23/9/2021***------------------------
    IF COMI # '' THEN
****End of update 23/9/2021***--------------------------------------
*----- EDIT BY NESSMA 2010/11/28-----------------------
        IF COMI GT 89 THEN
            CUST.NO          = ID.NEW
            POSTING.RESTRICT = COMI
            CALL DBR('CUSTOMER.ACCOUNT':@FM:EB.CAC.ACCOUNT.NUMBER,CUST.NO,CUS.ACC)
            IF CUS.ACC NE '' THEN
                ETEXT = "��� ����� ������ ������ ����"
                CALL STORE.END.ERROR
                FLAG = 1
            END
            IF FLAG EQ 0 THEN
                CALL OPF(FN.LD, F.LD)
                CALL OPF(FN.LMM,F.LMM)
                GOSUB CHK.LD
            END
            IF FLAG EQ 0 THEN
                CALL OPF(FN.LC,F.LC)
                CALL OPF(FN.LCC,F.LCC)
                GOSUB CHK.LC
            END
            IF FLAG EQ 0 THEN
                CALL OPF(FN.LI,F.LI)
                GOSUB CHK.LIMIT
            END

*** UPDATED BY MOHAMED SABRY 2015/1/15
*        IF FLAG EQ 0 THEN
*            CALL OPF(FN.RC,F.RC)
*            GOSUB CHK.RELATION
*        END

            IF FLAG EQ 0 THEN
                CALL OPF(FN.IM.REF,F.IM.REF)
                GOSUB CHK.IM
            END
            IF COMI EQ '98' THEN
                ETEXT = '��� ����� �������� ��� �����'
                CALL STORE.END.ERROR
            END
            IF ((COMI EQ '99') AND (FLAG EQ 0)) THEN
                WS.CHK.BANK.ID = ID.NEW[1,3]
                IF WS.CHK.BANK.ID EQ '994' THEN
                    R.NEW(EB.CUS.SHORT.NAME)  = 'CLOSED-NOT USED'
                    R.NEW(EB.CUS.NAME.1) = 'CLOSED-NOT USED'
                    R.NEW(EB.CUS.NAME.2) = 'CLOSED-NOT USED'
                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ARABIC.NAME> = 'CLOSED-NOT USED'
                    CALL REBUILD.SCREEN
                END
            END
        END
****Updated by Nessreen Ahmed 23/9/2021***------------------------
        IF FLAG EQ 0 THEN
            GOSUB DEF.DATE
        END
    END ELSE        ;*IF COMI # ''
        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.POST.REST.DATE> = ''
        CALL REBUILD.SCREEN
    END
****End of update 23/9/2021***--------------------------------------
    RETURN
*-------------------------------------------------------
CHK.LD:
*------
    CALL F.READ(FN.LMM,CUST.NO,R.LMM,F.LMM,ER.LMM)
    LOOP
        REMOVE CONTRACT.ID FROM R.LMM SETTING POS.LMM
    WHILE CONTRACT.ID:POS.LMM
        TEXT = CONTRACT.ID:POS.LMM ; CALL REM
        IF CONTRACT.ID[1,2] EQ 'LD' THEN
            LD.ID = CONTRACT.ID
            CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ETEXT.LD)
            AMT.LD  = R.LD<LD.AMOUNT>
            LD.STAT = R.LD<LD.STATUS>
            IF AMT.LD GT 0 AND LD.STAT NE 'LIQ' THEN
                FLAG   = 1
                ETEXT = "���� ����� �� ������ �� ������ ����"
                CALL STORE.END.ERROR
            END
        END ELSE
            ETEXT = "���� ����� ����"
            CALL STORE.END.ERROR
        END
    REPEAT
    RETURN
*-----------------------------------------------------
CHK.LIMIT:

    CALL F.READ(FN.LI,CUST.NO,R.LI,F.LI,ER.LI )
    LOOP
        REMOVE LI.ID FROM R.LI SETTING POS.LI
    WHILE LI.ID:POS.LI
        VAR.Y = LI.ID
        VAR.Y = FIELD(VAR.Y,'.',2)

* TEXT = VAR.Y ; CALL REM

        IF VAR.Y NE '0009900' AND VAR.Y NE '0009700' THEN
            ETEXT = "��� ����� ���� ����� ������ �������"
            CALL STORE.END.ERROR
            FLAG = 1
        END
    REPEAT
    RETURN
*-----------------------------------------------------
CHK.LC:
    CALL F.READ(FN.LC,CUST.NO,R.LC,F.LC,ER.LC )
    LOOP
        REMOVE LC.ID FROM R.LC SETTING POS.LC
    WHILE LC.ID:POS.LC
*TEXT = LC.ID ; CALL REM
        CALL F.READ(FN.LCC,LC.ID,R.LCC,F.LCC,ETEXT.LD)
        WS.LCC.CHK =          R.LCC<TF.LC.OPERATION>
        IF WS.LCC.CHK NE "" THEN
            ETEXT = "���� ��������  �������"
            CALL STORE.END.ERROR
            FLAG = 1
        END
    REPEAT
    RETURN
*-----------------------------------------------------
*** MSABRY 2012/02/19
***------------------
CHK.RELATION:
    CALL F.READ(FN.RC,CUST.NO,R.RC,F.RC,ER.RC )
    IF NOT(ER.RC) THEN
        ETEXT = "������ ����� ������ ������ ���� ��� ��� �������"
        CALL STORE.END.ERROR
        FLAG = 1
    END
    RETURN
*-----------------------------------------------------
*** MSABRY 2012/06/3
***------------------
CHK.IM:
    CALL F.READ(FN.IM.REF,CUST.NO,R.IM.REF,F.IM.REF,ER.IM.REF )
    IF NOT(ER.IM.REF) THEN
        ETEXT = "������ ����� ���������� ����"
        CALL STORE.END.ERROR
        FLAG = 1
    END
    RETURN
*--------------------------------------------------------------
*****Updated by Nessreen Ahmed 22/9/2021*********
DEF.DATE:
    IF COMI # '' THEN
        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.POST.REST.DATE> = TODAY
        CALL REBUILD.SCREEN
    END
**    IF R.NEW(EB.CUS.POSTING.RESTRICT) = '' THEN
**        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.POST.REST.DATE> = ''
**        CALL REBUILD.SCREEN
**    END
    RETURN
*****End of update 22/9/2021**********************
*--------------------- END OF ROUTINE ----------------
END
