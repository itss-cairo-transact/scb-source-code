* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.DR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*IF INT LIQ ACCT IS EMPTY THEN DEFUALT IT WITH FEILD DRAWDOWN.ACCOUNT
*    IF COMI EQ '' THEN
*     R.NEW(LD.INT.LIQ.ACCT)=COMI
 *       COMI = R.NEW(LD.DRAWDOWN.ACCOUNT)
  *  END ELSE
        CD.CURR = R.NEW(LD.CURRENCY)
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,ACC.CUR)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,COMI,ACC.CUST)
        IF CD.CURR NE ACC.CUR THEN
            ETEXT = 'MUST BE SAME CD CURRENCY';CALL STORE.END.ERROR
        END
*************
        CD.CUST = R.NEW(LD.CUSTOMER.ID)
TEXT=CD.CUST:'=LD';CALL REM
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,COMI,ACC.CUST)
TEXT=ACC.CUST:'-ACC.CUST';CALL REM
        IF CD.CUST NE ACC.CUST THEN
            ETEXT='MUST BE SAME CD CUSTOMER';CALL STORE.END.ERROR
        END
***************

   * END
    CALL REBUILD.SCREEN
    RETURN
END
