* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.COM.BR.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

    IF MESSAGE EQ '' THEN

        CU.NO      = R.NEW(EB.BILL.REG.DRAWER)
        ACT        = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
        DR.AMTT    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
        LEN.AMT    = LEN(R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>)
        LEN.NEW    = LEN.AMT - 3
        AMT        = DR.AMTT[4,LEN.NEW]

        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACT,CATEG)

        IF (CATEG GE 1101 AND CATEG LE 1590 ) THEN
            CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACT,LIM.REF)
            IF LEN(LIM.REF) EQ '6' THEN
                LIM  = CU.NO :".":"0000":LIM.REF
            END ELSE
                LIM  = CU.NO :".":"00":LIM.REF
            END
            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = COMI
            ZERO = "0.00"
            BAL  =  WORK.BALANCE + AVL
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
            TEXT = "LOOK.AMT" : LOCK.AMT ; CALL REM
*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            BLOCK =LOCK.AMT<1,LOCK.NO>

            BAL.BLOCK = BAL - BLOCK
            NET.BAL = BAL.BLOCK - AMT

            IF NET.BAL LT ZERO THEN
                E = 'AMOUNT IS NOT AVILABLE' ; CALL ERR; MESSAGE='REPEAT'
            END

        END ELSE
*-------------------------------------------------------------------*
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = COMI
            ZERO = "0.00"
            BAL  =  WORK.BALANCE

            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            BLOCK =LOCK.AMT<1,LOCK.NO>
            BAL.BLOCK = BAL - BLOCK
            NET.BAL = BAL.BLOCK - AMT

            IF NET.BAL LT ZERO THEN
                E = 'AMOUNT IS NOT AVILABLE' ; CALL ERR; MESSAGE='REPEAT'
            END
*---------------------------------------------------------------------*
        END
    END

*                                                                     *
    RETURN
END
