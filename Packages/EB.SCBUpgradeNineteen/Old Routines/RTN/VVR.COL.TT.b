* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>296</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.COL.TT


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

 * TO CHECK IF DEBIT ACCOUNT EQ CREDIT ACCOUNT THEN PRODUCE ERROR MESSAGE
 * AND IF ACCOUNT IS NOT NOSTRO ACCT. THEN PRODUCE ERROR MESSAGE
 * IF ACCOUNT CURRENCY NOT EQUAL CURRENCY.1 THEN PRODUCE AN ERROR MESSAGE

    IF V$FUNCTION = 'I' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
        IF COMI = R.NEW(TT.TE.ACCOUNT.1) THEN ETEXT='��� �� ���� ���� �����'
*ETEXT ='YOU CAN NOT USE THE SAME ACCOUNT' ;

        IF CATEG < 5000 OR CATEG > 5999 THEN ETEXT = 'Only.Nostro.Account'
*IF  ( CATEG >= 5000 AND CATEG <= 5999 )  THEN ETEXT='��� ����� ���� �������'
*ETEXT = 'CATEGORY NOT ALLOWOED'


        IF CURR # R.NEW(TT.TE.CURRENCY.1) THEN ETEXT='��� �� ���� ��� ������'
*ETEXT = 'YOU CANT USE DIFFERENT CURRENCY' ;
    END
    RETURN
END
