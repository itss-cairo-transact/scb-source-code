* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BT.TT.AMT.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.TT

    AMTT   = 0
*Line [ 29 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M   = DCOUNT(R.NEW(BT.TT.BR.AMT),@VM)
    FOR K  = 1 TO NO.M
        AMTT    = R.NEW(BT.TT.BR.AMT)<1,K> + AMTT
    NEXT K

    R.NEW(BT.TT.TOTAL.CREDIT.AMT)= AMTT
    R.NEW(BT.TT.TOTAL.DEBIT.AMT) = AMTT
    R.NEW(BT.TT.TOTAL.CREDIT.NO) = NO.M
    R.NEW(BT.TT.TOTAL.DEBIT.NO)  = '1'
    CALL REBUILD.SCREEN

    IF COMI NE AMTT THEN

        ETEXT = "CHECK TOTAL "
    END

    RETURN
END
