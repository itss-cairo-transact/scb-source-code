* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*------------------------------------------------------------------------
* <Rating>-8</Rating>
*-------------------------------------------------------------------------
    SUBROUTINE VVR.CUSS.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN


    IF MESSAGE # 'VAL' THEN
        IF R.NEW(INF.MLT.CUSTOMER.ID)<1,AV> THEN
            CUSS = R.NEW(INF.MLT.CUSTOMER.ID)<1,AV>
            ACC  = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,AV>
            IF ACC THEN
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CUS)
                IF CUSS NE '' THEN
                    IF CUSS NE CUS THEN
                        ETEXT = 'ACC NE CUS' ; CALL STORE.END.ERROR
                    END
                END
            END
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
