* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CHQ.EXEC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP

    FN.CHQ = 'F.SCB.CHEQ.APPL' ; F.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    FN.STP = 'FBNK.PAYMENT.STOP' ; F.STP = ''
    CALL OPF(FN.STP,F.STP)

    WS.CHQ.START = COMI

    T.SEL = "SELECT F.SCB.CHEQ.APPL WITH CHQ.NO.START EQ ":WS.CHQ.START
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CHQ,KEY.LIST<I>,R.CHQ,F.CHQ,E2)

            WS.CUS.NO.CHQ.APPL   = R.CHQ<CHQA.CUST.NO>
            WS.CUS.NO.PAY.STOP   = R.NEW(AC.PAY.CUSTOMER.NO)

            IF WS.CUS.NO.CHQ.APPL NE WS.CUS.NO.PAY.STOP THEN
                ETEXT = '��� ������� ����� ��� ������'
                CALL STORE.END.ERROR
            END ELSE

                WS.CHQ.TYPE = R.CHQ<CHQA.CHEQ.TYPE>
                WS.NO.CHQ   = R.CHQ<CHQA.NO.OF.BOOKS>
                WS.CHQ.END  = (((WS.CHQ.TYPE * WS.NO.CHQ) + WS.CHQ.START) - 1)
                R.NEW(AC.PAY.LAST.CHEQUE.NO)<1,AV> = WS.CHQ.END
                R.NEW(AC.PAY.CHEQUE.TYPE)<1,AV>    = "SCB"
                R.NEW(AC.PAY.STOP.DATE)<1,AV>      = TODAY
                R.NEW(AC.PAY.WAIVE.CHARGE)<1,AV>   = "YES"
            END
        NEXT I
    END ELSE
        ETEXT = '��� ������� ��� �����'
        CALL STORE.END.ERROR
    END

    CALL REBUILD.SCREEN

    RETURN
END
