* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.SUEZ.WB

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*TO CHECK IF WORKING BALANCE LESS THAN CD AMOUNT THEN ERROR MESSAGE WILL BE DISPLAYED
    CD.CURR= R.NEW(LD.CURRENCY)
    CD.AMT = R.NEW(LD.AMOUNT)

    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COMI,ACC.CUR)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,ACC.CAT)
    CALL DBR ('ACCOUNT':@FM:AC.WORKING.BALANCE,COMI,MYBAL)
******************
    BEGIN CASE

    CASE ACC.CAT EQ '1002'
        ETEXT = 'CATEGORY 1002 NOT ALLOWED';CALL STORE.END.ERROR
    CASE CD.CURR NE ACC.CUR
        ETEXT='MUST BE SAME CD CURRENCY';CALL STORE.END.ERROR
    CASE MYBAL LT CD.AMT
        ETEXT = 'Working Balance Less Than Zero' ;CALL STORE.END.ERROR
    CASE OTHERWISE
        IF MESSAGE NE 'VAL' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.ACCT.STMP> = COMI
            R.NEW(LD.INT.LIQ.ACCT)                = COMI
            R.NEW(LD.CHRG.LIQ.ACCT)               = COMI
            CALL REBUILD.SCREEN
        END
    END CASE
******************
    RETURN
END
