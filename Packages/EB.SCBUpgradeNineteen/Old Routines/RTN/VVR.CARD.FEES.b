* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CARD.FEES

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CARD.FEES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.FEE.EXCP
*----------------------------------------------
    CUSTOMER.ID = "" ; TARGT = ""
    CUSTOMER.ID = FIELD(ID.NEW , '.' , 1 )
    CALL DBR('CUSTOMER':@FM:EB.CUS.TARGET,CUSTOMER.ID,TARGT)

****UPDATED BY NESSREEN AHMED 2/10/2019********
***    IF TARGT NE 8 THEN
    CH.FEE = ''
****END OF UPDATE 2/10/2019********************
    FN.CF = "F.SCB.ATM.CARD.FEES" ; F.CF = "" ; R.CF = ""
    CALL OPF(FN.CF, F.CF)

    FN.EXP = "F.SCB.CUS.FEE.EXCP" ; F.EXP = "" ; R.EXP = ""
    CALL OPF(FN.EXP, F.EXP)

    CARD.ACTION = R.NEW(SCB.VISA.CARD.ACTION)
    CUS.PAY     = R.NEW(SCB.VISA.PAYROLL.CUST)

****UPDATED BY NESSREEN AHMED 2/10/2019********
    CARD.TYPE  = R.NEW(SCB.VISA.CARD.TYPE)
***UPDATED BY NESSREEN AHMED 2/8/2021**********************
*** IF (TARGT EQ 8 OR TARGT EQ 30 )AND CARD.ACTION EQ 1 AND (CARD.TYPE EQ 801 OR CARD.TYPE EQ 810) THEN
    CH.FEE = ''
    IF TARGT EQ 30 AND CARD.ACTION EQ 1 AND (CARD.TYPE EQ 801 OR CARD.TYPE EQ 810) THEN
        CH.FEE = 'NO'
    END
    CARD.PRODUCT = R.NEW(SCB.VISA.CARD.PRODUCT)
    IF TARGT EQ 8 AND CARD.ACTION EQ 1 AND CARD.PRODUCT EQ 'MEEZA' AND (CARD.TYPE EQ 801 OR CARD.TYPE EQ 810) THEN
*** END OF UPDATE 2/8/2021**********************************
        CH.FEE = 'NO'
    END ELSE
        CH.FEE = 'YES'
    END
*****UPDATED BY NESSREEN AHMED 7/6/2021***********************
    IF (TARGT EQ 5850 OR TARGT EQ 5860 OR TARGT EQ 5870 OR TARGT EQ 5800 OR TARGT EQ 5900 )AND CARD.ACTION EQ 1 AND (CARD.TYPE EQ 801 OR CARD.TYPE EQ 810) THEN
* TEXT = 'ELITE' ; CALL REM
        IF (CARD.TYPE EQ 801) THEN
            CH.FEE = 'NO'
        END ELSE
* TEXT = 'ELITE SUP' ; CALL REM
            T.SEL =  "SELECT F.SCB.ATM.APP WITH CARD.TYPE EQ 810 AND ACC1.CUST EQ " :CUSTOMER.ID

            KEY.LIST=""
            SELECTED=""
            ER.MSG=""

            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                CH.FEE = 'YES'
*  TEXT = 'ELITE 1 SUPP BF' ; CALL REM
            END ELSE
                CH.FEE = 'NO'
*  TEXT = 'ELIST 1ST SUP' ; CALL REM
            END
        END
*****END OF UPDATE 7/6/2021***********************************
    END
*    TEXT = 'CH.FEE=':CH.FEE ; CALL REM
    IF CH.FEE = 'YES' THEN
****END OF UPDATE 2/10/2019********************
        IF CUS.PAY THEN
            CALL F.READ(FN.EXP,CUS.PAY,R.EXP,F.EXP,E.EXP)

            IF CARD.ACTION EQ 1 THEN R.NEW(SCB.VISA.FEES.AMT) = R.EXP<FEE.DR.ISSUANCE.FEE>
            IF CARD.ACTION EQ 2 THEN R.NEW(SCB.VISA.FEES.AMT) = R.EXP<FEE.PIN.REISSUE.FEE>
            IF CARD.ACTION EQ 3 THEN R.NEW(SCB.VISA.FEES.AMT) = R.EXP<FEE.DR.REISSUE.CARD.FEE>
            IF CARD.ACTION EQ 4 THEN R.NEW(SCB.VISA.FEES.AMT) = R.EXP<FEE.DR.REISSUE.CARD.FEE>
        END ELSE
            TEMP.ID = R.NEW(SCB.VISA.CARD.PRODUCT):".":R.NEW(SCB.VISA.CARD.TYPE)
            CALL F.READ(FN.CF,TEMP.ID,R.CF,F.CF,E.CF)
            IF CARD.ACTION EQ 1 THEN R.NEW(SCB.VISA.FEES.AMT) = R.CF<CARD.FEES.NEW.ISSUE>
            IF CARD.ACTION EQ 2 THEN R.NEW(SCB.VISA.FEES.AMT) = R.CF<CARD.FEES.PIN.REISSUE>
            IF CARD.ACTION EQ 3 THEN R.NEW(SCB.VISA.FEES.AMT) = R.CF<CARD.FEES.REISSUE.CARD>
            IF CARD.ACTION EQ 4 THEN R.NEW(SCB.VISA.FEES.AMT) = R.CF<CARD.FEES.CARD.REPLACEMENT>
        END
    END ELSE
        R.NEW(SCB.VISA.FEES.AMT) = ""
    END

    CALL REBUILD.SCREEN
    RETURN
END
