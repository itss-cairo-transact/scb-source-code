* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------

* <Rating>4464</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.LIQ.NO.MONTH

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.PREM.AMT

*TO DEFAULT FEILD CHRG.AMOUNT WITH APPROPRIATE AMOUNT ACCORDING TO THE NUMBER OF MONTH LIQUIDATED IN
    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN

            CD.TYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
            NO.MON=FIELD(CD.TYPE,'-',3)
            ID=ID.NEW:'00'
            CD.OLD=R.NEW(LD.LOCAL.REF)<1,LDLR.OLD.NO>

*******************************************
********************* ADDED BY BAKRY
            ID=ID.NEW:'00'
            CALL DBR('LMM.ACCOUNT.BALANCES':@FM:LD27.START.PERIOD.INT,ID,MYSTART)
************************************************************************
            IF CD.OLD NE '' THEN
                MYVAL=R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            END ELSE
                MYVAL=R.NEW(LD.VALUE.DATE)
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> > R.NEW(LD.VALUE.DATE) THEN
                MYVAL=R.NEW(LD.VALUE.DATE)
            END
            IF MYVAL GT 20070907 THEN
                ETEXT='Must Be Less Than 20070907'  ;CALL STORE.END.ERROR
                RETURN
            END
********************* ADDED BY BAKRY
            CHK.MON  = 0
            CHK.YEAR = 0
            CHK.YEAR = COMI[1,4] - MYVAL[1,4]
            CHK.MON = CHK.YEAR * 12
            TOT.DIFF = CHK.MON + (COMI[5,2] - MYVAL[5,2])
            CHK.DAY =  COMI[7,2] - MYVAL[7,2]
            TEXT = CHK.DAY:" ���":" ":TOT.DIFF:" ���" ; CALL REM
            IF CHK.DAY # 0 THEN
                IF CHK.DAY < 0 THEN
                    TOT.DIFF = TOT.DIFF - 1
                END ELSE
                    TOT.DIFF = TOT.DIFF + 1
                END
            END
********************************************
            IF MYSTART # TODAY THEN
                R.NEW(LD.INT.RATE.V.DATE)= MYSTART
                R.NEW(LD.NEW.INT.RATE)= 0
            END
            R.NEW(LD.FIN.MAT.DATE)=TODAY
********************* ADDED BY BAKRY
********************************************
            CD.TYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
            FN.CD.PREM = 'F.SCB.CD.PREM.AMT' ; F.CD.PREM = '';R.CD.PREM=''
            CALL OPF(FN.CD.PREM,F.CD.PREM)
            CALL F.READ(FN.CD.PREM,CD.TYPE,R.CD.PREM,F.CD.PREM,E1)
            PREM.MON=R.CD.PREM<SCB.CDPA.MONTH>
            LOCATE TOT.DIFF IN PREM.MON<1,1> SETTING POS THEN
                PREM.AMT=R.CD.PREM<SCB.CDPA.AMOUNT><1,POS>
                R.NEW(LD.LOCAL.REF)<1,LDLR.FREE.TEXT>=PREM.MON<1,POS>
                PRINC=FIELD(CD.TYPE,'-',2)
                R.NEW(LD.CHRG.AMOUNT)=PRINC-PREM.AMT
                R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT>=PREM.AMT
            END ELSE
                TEXT='TOT.DEF= ': TOT.DIFF :" ":NO.MON ;CALL REM
                IF TOT.DIFF LT '6' THEN
**  ETEXT='��� �� ���� ���� �� 6����'  ;CALL STORE.END.ERROR
                    ETEXT='Must Be More Than 6 Months'  ;CALL STORE.END.ERROR
                END
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
