* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>233</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.AUTH2

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF MESSAGE = '' THEN
        IF R.NEW(TT.TE.CURRENCY.1) = 'EGP' THEN
            IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN ETEXT = '��� ����� ������' ;CALL STORE.END.ERROR
            R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
            CALL REBUILD.SCREEN
****UPDATED 03/09/2008 BY NESSREEN AHMED*************************
***            IF COMI LT 20000 THEN ETEXT = '��� �� �� ��� ������ �� 20000'
***            CALL STORE.END.ERROR
            IF COMI LE 150000 THEN
                ETEXT = '��� �� �� ��� ������ �� 150000'
                CALL STORE.END.ERROR
            END ELSE
                IF COMI GT 500000 THEN
                    ETEXT = '��� �� ������ ������ �� 500000'
                    CALL STORE.END.ERROR
                END
            END
***********************************************************
            ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
            AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV>
*********UPDATED BY NESSREEN AHMED 18/03/2009***************************
            IF ACCT[11,4] NE 5010 THEN
*************************************************************************
                IF NOT( ACCT[11,4] GE 1500 AND ACCT[11,4] LE 1590) THEN
                    CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
**TEXT = 'BAL=':BAL ; CALL REM
                    CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 62 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    LOCK.NO=DCOUNT(LOCK.AMT,@VM)
                    NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
**TEXT = 'NET.BAL=':NET.BAL ; CALL REM
                    CATEG = ACCT[11,4]
**TEXT=CATEG:'CATEG';CALL REM
** NESSREEN 19/6/2006 IF AMT1 GT BAL THEN
                    IF COMI GT NET.BAL THEN
*****UPDATED BY NESSREEN AHMED 07/06/2010*********************************
*****  IF NOT (CATEG GE 1101 AND CATEG LE 1450 ) THEN
                        IF NOT (CATEG GE 1101 AND CATEG LE 1477) AND NOT(CATEG EQ 1481) THEN
*****END OF UPDATE 07/06/2010*********************************************
                            ETEXT = '������ �� ����'
                            CALL STORE.END.ERROR
                        END
                    END
                END ;*IF NOT (ACCT
*******************************************
            END     ;*IF NE 5010
*******************************************
        END         ;*IF R.NEW
    END   ;*IF MESSAGE = ''
******************************************************
    RETURN
END
