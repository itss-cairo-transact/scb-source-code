* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHK.TT.ID.NC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*--------------------------------------------
    IF MESSAGE NE 'VAL' THEN
        IDD     = COMI
        LEN.ID  = LEN(IDD)
        SEC.ID  = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHK.FLG>
        IF SEC.ID EQ '�����' THEN
            PLISS.REGNO = COMI
            IF LEN.ID NE 14 THEN
                ETEXT = 'MUST BE 14 DIGITS'
                CALL STORE.END.ERROR
            END
        END ELSE
            IF SEC.ID EQ '�����' THEN
                PL.ISS      = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.PLACE.ID.ISSUE>
                REG.NO      = COMI
                PLISS.REGNO = REG.NO:'.':PL.ISS
                CHEK.IDD    = PLISS.REGNO
            END ELSE
                PLISS.REGNO = COMI
            END
        END
    END
*-----------------------------------------------------
    FN.TEMP = "F.SCB.CD.NATIONAL.ID"    ; F.TEMP = ""
    CALL OPF(FN.TEMP,F.TEMP)

    CALL F.READ(FN.TEMP,PLISS.REGNO,R.TEMP,F.TEMP,ERR1)
    BAL = R.TEMP<SCB.CD.BALANCE>

    TT.AMT      = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    MAX.VAL     = 990
    TOT.BAL     = BAL + TT.AMT
    AVAL.AMT    = MAX.VAL - TOT.BAL

    IF TOT.BAL GT 990 THEN
        ETEXT='CDS AMOUNT EXCEED 990' ;CALL STORE.END.ERROR
    END ELSE
        TEXT='AVAILABLE AMOUNT IS':AVAL.AMT;CALL REM
    END

    CALL REBUILD.SCREEN
*-----------------------------------------------------
    RETURN
END
