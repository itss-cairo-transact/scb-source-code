* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
******** DINA_SCB *********

    SUBROUTINE VVR.CU.NO.CHANGE.RANK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*TO INFORCE THE USER TO PUT THE RANK SUITABLE WITH THE SECTOR OF THE CUSTOMER

    IF V$FUNCTION = 'I' THEN
        CALL DBR ('SCB.VER.IND.SEC.LEG':@FM:VISL.SEC,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,SEC)
        IF NOT(ETEXT) THEN
            LOCATE R.NEW(EB.CUS.SECTOR) IN SEC<1,1> SETTING YY THEN
                CALL DBR ('SCB.VER.IND.SEC.LEG':@FM:VISL.RANK,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,RANK)
                IF COMI THEN LOCATE COMI IN RANK<1,YY,1> SETTING M ELSE ETEXT = 'THIS RANK NOT ALLWED'
            END
        END
    END

    RETURN
END
