* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1199</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.RE

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    ETEXT = ''
    E = ''
    BAL = 0

    IF MESSAGE = '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN ETEXT = '��� ����� ������';CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
        ETEXT = ''
**NN END
        IF COMI GT 20000 THEN
            ETEXT = '��� �� �� ���� ������ ���� �� 20000'
            CALL STORE.END.ERROR
        END
**********NESSREEN 16/10/2006***************
        IF R.NEW(TT.TE.CURRENCY.1) = LCCY THEN
********************************************
            ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
            AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV>
            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
********NESSREEN AHMED 09/01/2007*********************************************
*  IF CATEGG = '1202' THEN
            IF (CATEGG GE 1101 AND CATEGG GE 1450) THEN
                TEXT = '1202' ; CALL REM
                CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
                TEXT = 'LIMTREF=':LIMTREF ; CALL REM
                CUST = R.NEW(TT.TE.CUSTOMER.1)
                IDD = FMT(LIMTREF, "R%10")
                KEY.ID = CUST:'.':IDD
                TEXT = 'ID=':KEY.ID ; CALL REM
                CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
                TEXT = 'AVLM=':AVLM ; CALL REM
                TEXT = 'BAL=':BAL ; CALL REM
                EQT = AVLM + BAL - COMI
                TEXT = 'EQTLCCY=':EQT ; CALL REM
                ETEXT = ''
                IF EQT < 0 THEN
                    TEXT = 'NOOOO' ; CALL REM
                    ETEXT = "�� ��� �� ������ ������ �� �����"
                    CALL STORE.END.ERROR
                END
            END

****************************************************
* IF CATEGG # '1201' THEN
            IF NOT( CATEGG GE 1500 AND CATEGG LE 1590) THEN
                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
**************NESSREEN 16/1/2007**********************
* FOR I=1 TO LOCK.NO
*   CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*   LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
* NEXT I
* NET.BAL=BAL-LOCK.AMT1
                NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
***********NESSREEN  17/9/2006***********************************
**16/1/2007 IF LOCK.AMT1 > 0 THEN
                IF LOCK.AMT<1,LOCK.NO> > 0 THEN
**16/1/2007  TEXT = '������ ������� ����=':LOCK.AMT1 ; CALL REM
                    TEXT = '������ ������� ����=':LOCK.AMT<1,LOCK.NO> ; CALL REM
                    TEXT = '������ ������� =': NET.BAL ; CALL REM
                END
*****************************************************************
** CATEG = ACCT[11,4]
*IF COMI GT NET.BAL AND CATEGG NE '1202' THEN
*  TEXT = 'COMI=':COMI ; CALL REM
*   TEXT = 'NET.BAL=':NET.BAL ; CALL REM
*    ETEXT = '������ �� ����'
*     CALL STORE.END.ERROR
*  END
******************************************
            END
********************************************
        END         ;**IF CURR = LCCY
    END   ;**IF MESSAGE = VAL
***************************************************
    IF MESSAGE EQ 'VAL' THEN
        FN.OFS.SOURCE ="F.OFS.SOURCE"
        F.OFS.SOURCE = ""

        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*        CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
        FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        F.OFS.IN = 0
        OFS.REC = ""
        OFS.OPERATION = "SCB.CHQ.RETURN"
        OFS.OPTIONS = "INP"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
        COMP = C$ID.COMPANY
        COM.CODE = COMP[8,2]
        OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

        OFS.TRANS.ID = ""
        OFS.MESSAGE.DATA = ""
        COMMA = ","
        AMT = COMI
*Line [ 147 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL TXTINP('THE SIGNATURE IS CORRECT', 8, 23, '1.1', @FM:'Y_N')
        IF COMI[1,1] = 'Y' THEN
***********************CHECK BALAMCE
            LOCK.AMT1 = ''
            ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
            TEXT = "ACCT.NO=":ACCT ; CALL REM
            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT.NO, BAL)
            TEXT = "BAL=":BAL ; CALL REM
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
*Line [ 157 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            FOR I=1 TO LOCK.NO
                CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
                LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
            NEXT I
            NET.BAL=BAL-LOCK.AMT1
            CATEG = ACCT.NO[11,4]
**  IF R.NEW(TT.TE.AMOUNT.LOCAL.1) THEN
            IF AMT THEN
**  AMT.LOC = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                AMT.LOC = AMT
            END ELSE
                AMT.LOC = R.NEW(TT.TE.AMOUNT.FCY.1)
            END
            IF AMT.LOC GT NET.BAL THEN
************************************
*Line [ 174 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                CALL TXTINP('THE AMOUNT IS NOT AVILABLE DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
*     CUST = ''
                IF COMI[1,1] = 'Y' THEN
                    ETEXT = 'AMOUNT NOT AVILABLE'
                    CALL STORE.END.ERROR


                    CURR = R.NEW(TT.TE.CURRENCY.1)
                    ACCT = R.NEW(TT.TE.ACCOUNT.1)
**    AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                    AMT.LCY = AMT
                    AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
                    CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
                    NRTV = R.NEW(TT.TE.NARRATIVE.2)
                    STOP.TYPE = '20'
                    CHEQUE.TYPE = 'SCB'
                    STOP.DATE = TODAY
                END ELSE
                    ETEXT = 'AMOUNT NOT AVILABLE'
                    CALL STORE.END.ERROR

                END
            END
        END ELSE
*Line [ 199 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            CALL TXTINP('DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
            IF COMI[1,1] = 'Y' THEN
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR

***************************************
                CURR = R.NEW(TT.TE.CURRENCY.1)
                ACCT = R.NEW(TT.TE.ACCOUNT.1)
                AMT.LCY = AMT
                AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
                CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
                NRTV = R.NEW(TT.TE.NARRATIVE.2)
                STOP.TYPE = '21'
                CHEQUE.TYPE = 'SCB'
                STOP.DATE = TODAY
            END ELSE
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR

            END
        END


        MUL.NO = 1

        CALL DBR( 'SCB.CHQ.RETURN':@FM:SCB.CHQR.PAYM.STOP.TYPE, ACCT,ST.TYPE)
        TEXT = ST.TYPE ; CALL REM
        IF ST.TYPE THEN

*Line [ 229 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            MULL.NOO=DCOUNT(ST.TYPE,@VM)
            TEXT = MULL.NOO ; CALL REM
            MUL.NO = MUL.NO + MULL.NOO

        END

        IF CURR THEN
* OFS.MESSAGE.DATA =  "CUSTOMER=":CUST:COMMA
*OFS.MESSAGE.DATA :=  "CURRENCY=":CURR:COMMA
            OFS.MESSAGE.DATA :=  "PAYM.STOP.TYPE:":MUL.NO:"=":STOP.TYPE:COMMA
            OFS.MESSAGE.DATA :=  "FIRST.CHEQUE.NO:":MUL.NO:"=":CHQ.NO:COMMA
*        OFS.MESSAGE.DATA :=  "LAST.CHEQUE.NO=":CHQ.NO:COMMA
            OFS.MESSAGE.DATA :=  "CHEQUE.TYPE:":MUL.NO:"=":CHEQUE.TYPE:COMMA
            OFS.MESSAGE.DATA :=  "STOP.DATE:":MUL.NO:"=":STOP.DATE:COMMA
            IF AMT.LCY THEN
                OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.LCY:COMMA
            END ELSE
                OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.FCY:COMMA
            END
            OFS.MESSAGE.DATA :=  "BENEFICIARY:":MUL.NO:"=":NRTV


            ID.KEY.LIST= ACCT
            TEXT = "ID.KEY.LIST = ":ID.KEY.LIST ; CALL REM
            F.PATH = FN.OFS.IN
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ID.KEY.LIST:COMMA:OFS.MESSAGE.DATA
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.KEY.LIST:"-":TODAY ON ERROR
                TEXT = " ERROR "
                CALL REM
            END
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
******************************
        END
*****************************************
        COMI=AMT
        CALL REBUILD.SCREEN
    END

***************************************************
    RETURN
END
