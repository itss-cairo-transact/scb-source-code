* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
*************DINA-SCB***********

    SUBROUTINE VVR.CU.REL.CUS

*TO INFORCE THE USER TO CHOOSE THE SECTOR THAT IS RELATED TO HIS VERSION
*To inforce the user that the Sector of the related customer is the Sector
*related to the user version

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        ETEXT = ''
        Y=''
        IF COMI THEN
            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI, SEC)
            CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.INDUSTRY,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,IND1)
            CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.SECTOR,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,SEC1)
*Line [ 45 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(IND1,@VM)
                LOCATE SEC IN SEC1<1,I,1> SETTING M THEN RETURN
                ELSE Y='YES'
            NEXT I
        END
*IF Y='YES' THEN ETEXT = 'SECTOR OF CUSTOMER NOT ALLOWED'
    END
    IF MESSAGE = "VAL"   THEN
        IF R.NEW(EB.CUS.LOCAL.REF)<1,CULR.STAFF.RANK>= '70' THEN
            IF R.NEW(EB.CUS.RELATION.CODE) = '' OR R.NEW(EB.CUS.REL.CUSTOMER) = '' THEN
                ETEXT = '��� ����� ������ '
            END
        END



    END
    RETURN
END
