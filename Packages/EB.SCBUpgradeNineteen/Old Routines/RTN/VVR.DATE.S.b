* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
****CREATED BY NESSMA
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DATE.S

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.INST
*-------------------------------------------
    ALLOW.TIME = COMI

    LOAN.DATE  = R.NEW(LINST.RESERVED1)
    CALL ADD.MONTHS(LOAN.DATE,ALLOW.TIME)
    START.DATE = LOAN.DATE

    R.NEW(LINST.START.DATE) = START.DATE
    PERD.INSTALL = R.NEW(LINST.PERIODIC.INSTALLMENT)
    NO.INSTALL   = R.NEW(LINST.NO.OF.INSTALLMENT)
    R.NEW(LINST.INSTALLMENT.DATE)<1,1> = START.DATE

    FOR NN = 2 TO NO.INSTALL
        CALL ADD.MONTHS(START.DATE,PERD.INSTALL)
        R.NEW(LINST.INSTALLMENT.DATE)<1,NN> = START.DATE
    NEXT NN

    R.NEW(LINST.MATURITY.DATE) =  START.DATE
*-------------------------------------------
    CALL REBUILD.SCREEN
*-------------------------------------------
    RETURN
END
