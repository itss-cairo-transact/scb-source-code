* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>347</Rating>
*-----------------------------------------------------------------------------

** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.CU.ID.ISSUE.DATE

*A VALIDATION ROUTINE TO VALIDATE IF THE ID TYPE IS ENTERED THE ISSUE DATE MUST BE ENTERED
*AND IT MUST BE GREATER OR EQUAL TODAY
*AND IF ID TYPE IS NOT ENTERED SO THE ISSUE DATE MUST NOT  BE ENTERED  TOO

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF MESSAGE = 'VAL' THEN

***Mahmoud 8/6/2009***
***Modified to validate ISSUE.DATE with NSN ID ***

***      IF  R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ID.TYPE> AND R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ID.TYPE> # '5' THEN
        IF  (R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> AND R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> # '5') OR (R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO> # '') THEN
*** MSABRY 2012/02/21
            IF R.NEW(EB.CUS.CUSTOMER.STATUS) # '22' THEN
*********************
                IF NOT(COMI) THEN ETEXT = 'ENTER ISSUE DATE'
                ELSE
                    IF COMI GT TODAY THEN ETEXT = 'THE DATE MUST BE LE TODAY'
                END
            END ELSE
***            IF COMI THEN ETEXT = 'NO ID TYPE'
****Updated by NESSREEN AHMED 5/8/2020***********
****IF COMI THEN ETEXT = 'NO ID TYPE OR NO NSN NUMBER'
                IF R.NEW(EB.CUS.NATIONALITY) EQ 'EG' THEN
                    IF COMI THEN ETEXT = 'NO ID TYPE OR NO NSN NUMBER'
                END
****End of update 5/8/2020************************
            END
        END
    END
    RETURN
END
