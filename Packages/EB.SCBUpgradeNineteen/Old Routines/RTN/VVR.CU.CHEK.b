* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>47</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.CHEK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
    IF MESSAGE EQ ' ' THEN

        ETEXT = ""
        IF MESSAGE NE 'VAL' THEN
            APPL.ACCT  = R.NEW(TF.LC.APPLICANT.ACC)
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,APPL.ACCT,CU1)
*    TEXT = "CU1 " : CU1 ; CALL REM

            ACC.CHAR = COMI
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC.CHAR,CU2)
*   TEXT = "CU2 " : CU2 ; CALL REM

            ACC.CH = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CHARGE.AC>

            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC.CH,CU3)
*  TEXT = "CU3 " : CU3 ; CALL REM

            IF CU1 # CU2 THEN
                IF CU3 # CU2 THEN
                    ETEXT = "��� ����� ��������"
                END
            END

            CALL REBUILD.SCREEN ; P = 0
        END
    END
    RETURN
END
