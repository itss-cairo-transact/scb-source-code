* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DR.TODAY.SP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS

    START.DATE = TODAY
    CALL CDT('EG', START.DATE, '-5C')
    IF R.NEW(TF.DR.DRAWING.TYPE) EQ 'SP' THEN
        IF COMI THEN
            BEGIN CASE
            CASE COMI LT START.DATE
                ETEXT = "MUST BE GT ":START.DATE ;CALL STORE.END.ERROR
            CASE COMI GT TODAY
                ETEXT = "MUST BE LE TODAY " ;CALL STORE.END.ERROR
            CASE OTHERWISE
                **R.NEW(TF.DR.DEBIT.VALUE) =   COMI
                CALL REBUILD.SCREEN
            END CASE
        END
    END

****************************************************
    RETURN
END
