* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.EMP.LOAN.TOTAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

*-----------------------------------------------

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    WS.CUS.ID = R.NEW(LD.CUSTOMER.ID)
    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21055 AND CUSTOMER.ID EQ ":WS.CUS.ID:" AND STATUS NE 'LIQ'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E4)
            WS.AMT += R.LD<LD.DRAWDOWN.ISSUE.PRC>

        NEXT I
    END

    WS.NEW.AMT   = COMI
    WS.TOTAL.AMT = WS.NEW.AMT + WS.AMT

    TEXT = '������ ���� ������ = ':WS.TOTAL.AMT ; CALL REM

    IF PGM.VERSION EQ ',EMP.LOAN' THEN
        IF WS.TOTAL.AMT LE 500000 THEN
            TEXT = '����� ��� ���� ������� ������' ; CALL REM
            R.NEW(LD.INTEREST.KEY) = 31
        END ELSE
            TEXT = '����� ��� ���� ������� �������' ; CALL REM
            R.NEW(LD.INTEREST.KEY) = 32
        END
    END

    CALL REBUILD.SCREEN

    RETURN
END
