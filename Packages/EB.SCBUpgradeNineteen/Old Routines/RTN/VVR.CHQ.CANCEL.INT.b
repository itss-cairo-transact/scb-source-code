* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>60</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHQ.CANCEL.INT

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
****UPDATED BY NESSREEN AHMED 6/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 6/3/2016**************************
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.DRFT.NO
*****UPDATED BY NESSREEN AHMED 19/8/2017*************
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP.TYPE
*****END OF UPDATE 19/8/2017*************************

*** CHEACK IF THE CHEQU IS EXIST
*** CHEACK IF THE CHEQU IS STPOPED
*** CHECK IF THE CHEQUE ISSUED
*** CHECK IF THE CHEQUE STOPPED
*** CHECK IF THE CHEQUE PRESENTED



****************MAIN PROGRAM************************

    GOSUB INTIAL
    GOSUB CHQ.STOP.SUB
    CALL REBUILD.SCREEN
    GOTO END.PROG
*************************************************************************************************
INTIAL:
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT =''  ; CURR =''
*****UPDATED BY NESSREEN AHMED 19/8/2017********************
    FN.CHQ.REG.SUP ='FBNK.CHEQUE.REGISTER.SUPPLEMENT' ; R.CHQ.REG.SUP = ''    ;  F.CHQ.REG.SUP   =''
    CALL OPF(FN.CHQ.REG.SUP,F.CHQ.REG.SUP)
*****END OF UPDATE 19/8/2017********************************
    RETURN
*********************************************************************************************************
CHQ.STOP.SUB:
****UPDATED BY NESSREEN AHMED 5/9/2016 for R15****
****    CHQ.ID.STOP = R.NEW(FT.DEBIT.ACCT.NO):"*": COMI
****    CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
    ACCT.NO = R.NEW(FT.DEBIT.ACCT.NO)
**    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.AC)
    CHQ.NO = TRIM(COMI, "0" , "L")
**  KEY.ID = "...":CUST.AC:"....":CHQ.NO
    KEY.ID = "...":ACCT.NO:".":CHQ.NO
    N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND STATUS EQ STOPPED "
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""
    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    IF SELECTED.N THEN
****    IF CURR THEN
*****UPDATED BY NESSREEN AHMED 19/8/2017*******************
***** ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
        CALL F.READ( FN.CHQ.REG.SUP,KEY.LIST.N, R.CHQ.REG.SUP, F.CHQ.REG.SUP,ERR.SUP)
        STOP.TYPE = R.CHQ.REG.SUP<CC.CRS.PAYM.STOP.TYPE>
        CALL DBR ('PAYMENT.STOP.TYPE':@FM:AC.PAT.DESCRIPTION,STOP.TYPE,STOP.DESC)
        ETEXT = "����� ��� (&) ����� ":@FM:COMI:' * ':STOP.DESC
        CALL STORE.END.ERROR
*****END OF UPDATE  19/8/2017*******************
    END ELSE
        CHQ.ID = R.NEW(FT.DEBIT.ACCT.NO):".": COMI
        TEXT = "CHQ.ID":CHQ.ID ; CALL REM
        FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = '' R.CHQ.PRESENT = ''
        CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)
        CALL F.READ(FN.CHQ.PRESENT,CHQ.ID,R.CHQ.PRESENT,F.CHQ.PRESENT,ERR.MSG)
        IF ERR.MSG THEN
            ETEXT = '�� ���� ��� ����� ���� ����� ���'  ; CALL STORE.END.ERROR
        END
        CHQ.STAT = R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS>
        CHQ.TRNS.PAY = R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT>
        CHQ.PAY.DATE = R.CHQ.PRESENT<DR.CHQ.PAY.DATE>
        CHQ.PAY.BRN = R.CHQ.PRESENT<DR.CHQ.PAY.BRN>
        CHQ.AMT = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
        CHQ.REC.DATE = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
        CHQ.BEN = R.CHQ.PRESENT<DR.CHQ.BEN>
        IF CHQ.STAT = 2 THEN
            ETEXT = "����� ��� ����" ; CALL STORE.END.ERROR
        END ELSE
            IF CHQ.STAT = 3  THEN
                ETEXT = "����� ����"   ; CALL STORE.END.ERROR
            END ELSE
* TEXT = "UPDATE.SCB.FT.DR.CHQ" ; CALL REM

*                TEXT = "READ . FROM " ; CALL REM
                IF MESSAGE # 'VAL' THEN
                    R.NEW(FT.DEBIT.AMOUNT) = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
*               TEXT = "AMOUNT":R.CHQ.PRESENT<DR.CHQ.AMOUNT> ; CALL REM
                    R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE> = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
                    R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
                    R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER<EB.USE.DEPARTMENT.CODE>
                    R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>
**********ADDED BY MAHMOUD 11/4/2010 ****************************
****                R.NEW(FT.CREDIT.ACCT.NO) = R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>
                    IF R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT> NE '' THEN
                        R.NEW(FT.CREDIT.ACCT.NO) = R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>
                    END
*****************************************************************

************* STOPED AT 20081126 BY BAKRY
*               R.NEW(FT.CREDIT.CURRENCY) = R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,3]
************
                    R.NEW(FT.CREDIT.CURRENCY) = CHQ.ID[1,3]
*      R.NEW(FT.CREDIT.CUSTOMER) = R.CHQ.PR.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,8]
*     CCY.NUM = R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[9,2]
*     CALL DBR ('NUMERIC.CURRENCY':@FM:1 , CCY.NUM , CCY.VAL)
*    IF NOT(ETEXT) THEN
*       R.NEW(FT.CREDIT.CURRENCY) = CCY.VAL
*  END
                END
            END
        END
    END

***************************************************************************
END.PROG:
    RETURN
END
