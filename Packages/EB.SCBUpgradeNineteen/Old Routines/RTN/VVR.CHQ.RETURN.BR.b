* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>144</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN AHMED 16/11/2009
    SUBROUTINE VVR.CHQ.RETURN.BR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW

    ETEXT = "" ; CHQ.NOS = "" ; ST.NO = "" ; ED.NO = "" ; CHQ.NO = ""
    FLAG = "0"

    FN.CHQ.REG = 'F.CHEQUE.REGISTER' ; F.CHQ.REG ='' ; R.CHQ.REG = '' ; ERR.CHQ= ''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)

    IF COMI THEN
        CALL DBR( 'CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,COMI ,CUST.BR)
        BR.CODE =  TRIM(CUST.BR[2],"0","L")
*TEXT = 'BR=':BR.CODE ; CALL REM
        R.NEW(CHQ.RET.CHEQUE.BR  ) = BR.CODE

        CALL REBUILD.SCREEN
        CHQ.NO = R.NEW(CHQ.RET.CHEQUE.NO)
*TEXT = 'CHQ.NO=':CHQ.NO ; CALL REM
        T.SEL ="SELECT FBNK.CHEQUE.REGISTER WITH CUSTOMER EQ ": COMI
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.CHQ.REG,KEY.LIST<I>, R.CHQ.REG, F.CHQ.REG , ERR.CHQ)
                CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>

*Line [ 59 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(CHQ.NOS,@VM)
                FOR X = 1 TO DD
                    CHN  = CHQ.NOS<1,X>
****UPDATE BY NESSREEN AHMED 25/5/2011*********************************
                    SS = COUNT(CHN, "-")
                    IF SS > 0 THEN
                        ST.NO   = FIELD(CHN ,"-", 1)
                        ED.NO   = FIELD(CHN ,"-", 2)
                    END ELSE
                        ST.NO   = CHN
                        ED.NO   = CHN
                    END
****END OF UPDATE 25/5/2011*******************************************
                    IF (CHQ.NO GE ST.NO) AND (CHQ.NO LE ED.NO) THEN
                        FLAG = "1"
                        TEXT = 'RANGE' ; CALL REM
                    END ELSE
                    END
                NEXT X
            NEXT I
        END
        IF FLAG = "0" THEN
            ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
        END
    END

    RETURN
END
