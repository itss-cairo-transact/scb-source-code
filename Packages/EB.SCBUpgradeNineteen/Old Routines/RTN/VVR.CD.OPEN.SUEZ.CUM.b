* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
****ABEER ****
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.SUEZ.CUM

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES

*TO DEFAULT FIELDS CATEGORY,CURRENCY,AMOUNT,VALUE DATE,FIN.MAT.DATE,INTEREST RATE FROM CD TYPE CHOSEN BY USER
*AND PROMPT THE USER TO ENTER THE NO OF CDS TO BE CREADTED FROM THE CHOSEN TYPE

    IF MESSAGE NE 'VAL' THEN


        DEFFUN SHIFT.DATE( )
        FN.CD.TYPE = 'F.SCB.CD.TYPES' ; F.CD.TYPE = '';R.CD.TYPE=''

        CALL OPF(FN.CD.TYPE,F.CD.TYPE)
        CALL F.READ(FN.CD.TYPE,COMI,R.CD.TYPE,F.CD.TYPE,E1)

        R.NEW(LD.CATEGORY)=R.CD.TYPE<CD.TYPES.CATEGORY>
        R.NEW(LD.CURRENCY)=FIELD(COMI,'-',1)
**     R.NEW(LD.AMOUNT)=FIELD(COMI,'-',2)
        CDS.AMT = FIELD(COMI,'-',2)


**        R.NEW(LD.DRAWDOWN.NET.AMT)  = FIELD(COMI,'-',2)
**      R.NEW(LD.DRAWDOWN.ISSUE.PRC)= FIELD(COMI,'-',2)
**    R.NEW(LD.ISSUE.PL.AMOUNT)   = '0.00'

        R.NEW(LD.VALUE.DATE)=TODAY

        NO.MON=FIELD(COMI,'-',3)
        VAL.DATE=R.NEW(LD.VALUE.DATE)
        FIN.MAT=SHIFT.DATE(VAL.DATE,NO.MON,UP)

        CALL CDT('EG',FIN.MAT,'1C')
*       IF R.NEW(LD.CATEGORY) EQ '21020' OR R.NEW(LD.CATEGORY) EQ '21021' OR R.NEW(LD.CATEGORY) EQ '21017' OR R.NEW(LD.CATEGORY) EQ '21018' THEN
*           CALL CDT('EG',FIN.MAT,'-1C')
*       END

        R.NEW(LD.FIN.MAT.DATE)=FIN.MAT
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = FIN.MAT

        R.NEW(LD.LOCAL.REF)<1,LDLR.PERCENT> = R.CD.TYPE<CD.TYPES.CD.RATE>
        R.NEW( LD.INTEREST.RATE)='0'
        R.NEW( LD.L.C.U.TYPE) = 'REVOLVING'
        R.NEW( LD.STA........Y.N)='NO'


        CD.TYPE=COMI
        YTEXT="No Of Cd To Be ISsued For This Type "
        CALL TXTINP(YTEXT, 8, 32, "12", "A")
        IF COMI EQ '' THEN
            CD.NO = 1
        END ELSE
            CD.NO  = COMI
        END
        R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY> = CD.NO

        DIGIT.NO = FIELD(COMI,'.',2)
        IF DIGIT.NO NE '' THEN
            ETEXT='CANT ENTER DIGIT';CALL STORE.END.ERROR
        END ELSE
            IF COMI EQ '' THEN COMI = '1'
            CDS.AMT1     =  CDS.AMT * COMI
*TEXT=CDS.AMT:'';CALL REM
            R.NEW(LD.AMOUNT)=CDS.AMT1

            COMI = CD.TYPE
            CALL REBUILD.SCREEN
        END

    END
    RETURN
END
