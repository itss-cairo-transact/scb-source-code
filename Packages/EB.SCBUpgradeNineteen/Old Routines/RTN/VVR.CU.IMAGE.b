* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.IMAGE
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    DEPT.COD = ''
***UPDATED BY NESSREEN AHMED 6/8/2019*************
*** DEPT.COD = R.USER<EB.USE.DEPARTMENT.CODE>
    ID.BRANCH.N = ID.COMPANY[2]
    DEPT.COD   = TRIM(ID.BRANCH.N,"0","L")
***END OF UPDATE 6/8/2019**********************
    IF COMI THEN
***UPDATED BY NESSREEN AHMED 18/7/2019***************
        CALL DBR('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,COMI,POST)
        IF POST EQ 26 THEN
            ETEXT = '��� ����� ������ ����� ��� ������� ��������'   ; CALL STORE.END.ERROR
        END ELSE
***END OF UPDATE 18/7/2019***************************
            CALL DBR('CUSTOMER':@FM:EB.CUS.MNEMONIC,COMI,MNOM)
            CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,COMI,SHORTNAM)
**************UPDATED BY RIHAM R15 ****************
*        CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,COMI,DEPT)
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,COMI,COMP.BOOK)
            CUS.BR = COMP.BOOK[8,2]
            DEPT = TRIM(CUS.BR, "0" , "L")
**************************

***
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,COMI,MYLOCAL)
            ARNAME = MYLOCAL<1,CULR.ARABIC.NAME>
***
            IF DEPT # DEPT.COD THEN ETEXT = 'THIS CUSTOMER IS NOT IN YOUR BRANCH'
            R.NEW(IM.DOC.DESCRIPTION)= MNOM
*        R.NEW(IM.DOC.SHORT.DESCRIPTION)= SHORTNAM
            R.NEW(IM.DOC.SHORT.DESCRIPTION)= ARNAME
            CALL REBUILD.SCREEN

        END

    END

    RETURN
END
