* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
** ----- 04.01.2005 INGY-SCB -----
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DEFAULT.COLL.CODE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.CODE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    IF COMI THEN

        CUST = R.NEW(LD.CUSTOMER.ID)
        IF  LEN(CUST) = 8 AND CUST[1,1] = 0 THEN
            CUST1 = CUST[2,8]
        END ELSE
            CUST1 = CUST
        END


        CALL DBR('CUSTOMER':@FM:EB.CUS.CUSTOMER.LIABILITY,CUST1,CUST.LIAB)
        T.SEL = 'SELECT FBNK.LIMIT WITH LIABILITY.NUMBER EQ ': CUST.LIAB
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,E)
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
*CALL DBR('LIMIT':@FM:LI.PRODUCT.ALLOWED,KEY.LIST<I>,LI.PRO)
* IF LI.PRO = '2000' THEN
                CALL DBR('LIMIT':@FM:LI.COLLATERAL.CODE,KEY.LIST<I>,COLL.CODE)
                IF COLL.CODE THEN
                    CALL DBR('COLLATERAL.CODE':@FM:COLL.CODE.DESCRIPTION,COLL.CODE,DESC)
                    IF NOT(ETEXT) THEN
                        R.NEW(LD.SECURED..Y.N) = 'YES'
                        R.NEW(LD.LOCAL.REF)<1,LDLR.COLL.TYP> = DESC

                    END
                END
* END
            NEXT I
        END

    END

    RETURN
END
