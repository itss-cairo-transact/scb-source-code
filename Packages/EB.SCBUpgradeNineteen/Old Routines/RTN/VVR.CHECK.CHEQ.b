* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CHECK.CHEQ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*-----------------------------------------------

    IF MESSAGE EQ 'VAL' THEN

        FN.PAY = "F.PAYMENT.STOP"   ; F.PAY = ""
        CALL OPF(FN.PAY, F.PAY)

        FN.TMP = "F.SCB.FT.DR.CHQ"   ; F.TMP = ""
        CALL OPF(FN.TMP, F.TMP)

        ACC.NO = ID.NEW
        CHEQ.X = R.NEW(AC.PAY.FIRST.CHEQUE.NO)
        CHEQ.Y = R.NEW(AC.PAY.LAST.CHEQUE.NO)
        CNUM   = R.NEW(AC.PAY.NO.OF.LEAVES)

        CHEQ.NUM = CHEQ.X
        FOR NN = 1 TO CNUM
            TEMP.ID = ACC.NO :".": CHEQ.NUM
            CHEQ.NUM++
            CALL F.READ(FN.TMP,TEMP.ID,R.TEMP,F.TMP,ERR.TMP)
            IF NOT(ERR.TMP) THEN
                PREV.STAT = R.TEMP<DR.CHQ.CHEQ.STATUS>
                R.TEMP<DR.CHQ.PREV.STATUS> = PREV.STAT
                R.TEMP<DR.CHQ.CHEQ.STATUS> = "4"
            END ELSE
                TEXT = "�� ���� ��� ����� �����"
                CALL REM
                E = "�� ���� ��� ����� �����"
                ETEXT = "�� ���� ��� ����� �����"
                CALL ERR
                CALL STORE.END.ERROR
                NN = CNUM
            END
        NEXT NN

    END
    RETURN
END
