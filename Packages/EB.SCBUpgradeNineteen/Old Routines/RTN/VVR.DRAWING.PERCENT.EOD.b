* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1235</Rating>
*-----------------------------------------------------------------------------
*PROGRAM  VVR.DRAWING.PERCENT.EOD

    SUBROUTINE VVR.DRAWING.PERCENT.EOD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON



    FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
    CALL OPF(FN.LCC,F.LCC)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LOCAL.PERCENT  NE ''  "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    PRINT SELECTED

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.LCC,KEY.LIST<I>,R.LCC,F.LCC,READ.ERR)

        PRINT KEY.LIST<I>

* LC.REC(TF.LC.PROVIS.PERCENT) = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>
* LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT> = ''

        R.LCC<TF.LC.PROVIS.PERCENT> = R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PERCENT>
        R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PERCENT> = ''

*******************************UPDATED BY RIHAM R15**********************
*        WRITE  R.LCC TO F.LCC , KEY.LIST<I> ON ERROR
*            PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.LCC
*        END
        CALL F.WRITE(FN.LCC,KEY.LIST<I>,R.LCC)
***************************************************************************
    NEXT I
************************************************************
    RETURN
END
