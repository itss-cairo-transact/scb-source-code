* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*** CREATED BY MOHAMED SABRY 2011/06/30 ***
*******************************************
    SUBROUTINE VVR.CHK.MRG.CG
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
    COMP = ID.COMPANY

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    WS.CU.ID = COMI
    IF MESSAGE # 'VAL' THEN
*Line [ 38 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = DCOUNT(R.NEW(CG.CUSTOMER.ID),@VM)
        IF WS.COUNT NE 1 THEN
            WS.COUNT = WS.COUNT -1
            FOR I = 1 TO WS.COUNT
                WS.ALL.CUS :=R.NEW(CG.CUSTOMER.ID)<1,I>:"."
            NEXT I
        END
        FINDSTR COMI:"." IN WS.ALL.CUS SETTING POS.CY THEN
            ETEXT = '��� ������ ���� �� ��� ��������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
    CALL F.READ(FN.CU,WS.CU.ID,R.CU,F.CU,E1)
    IF NOT(E1) THEN
        WS.LOCAL.REF    = R.CU<EB.CUS.LOCAL.REF>
        WS.CU.GROUP.NUM = WS.LOCAL.REF<1,CULR.GROUP.NUM>
        WS.CU.CREDIT    = WS.LOCAL.REF<1,CULR.CREDIT.CODE>
        WS.CU.RISK.R    = WS.LOCAL.REF<1,CULR.RISK.RATE>
        IF WS.CU.GROUP.NUM # '' THEN
            IF WS.CU.GROUP.NUM # ID.NEW THEN
                ETEXT = '��� ������ ���� ������� ���� '
                CALL STORE.END.ERROR
                RETURN
            END
        END
*       IF  WS.CU.CREDIT LT 100 THEN
*           ETEXT = '������ ��� ���� ������'
*           CALL STORE.END.ERROR
*           RETURN
*       END
*        IF  WS.CU.CREDIT GE 110 THEN
*            ETEXT = '�� ���� ����� ��� ������ ���������'
*            CALL STORE.END.ERROR
*            RETURN
*        END
*        IF  WS.CU.RISK.R GT 1 THEN
*            ETEXT = '������ ��� ���� ���� '
*            CALL STORE.END.ERROR
*            RETURN
*        END
        R.NEW(CG.COMPANY.BOOK)<1,AV> = R.CU<EB.CUS.COMPANY.BOOK>
        CALL REBUILD.SCREEN
    END
    RETURN
END
