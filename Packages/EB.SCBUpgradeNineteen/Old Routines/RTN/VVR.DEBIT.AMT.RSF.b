* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* REHAM YOUSSEF 6/5/2015
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DEBIT.AMT.RSF

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN


    FN.FT    = "FBNK.FUNDS.TRANSFER$HIS"; F.FT   = '' ; R.FT   = '' ; CALL OPF(FN.FT,F.FT)
    FN.TT    = "FBNK.TELLER$HIS" ; F.TT = '' ;R.TT = '' ; CALL OPF(FN.TT,F.TT)
    FN.STE   = "FBNK.STMT.ENTRY" ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
    AMT.TOT1 = 0
    AMT.TOT2 = 0
    AMT.TOT3 = 0
    AMT.TOT4 = 0
*********************************************************
    IF MESSAGE # 'VAL' THEN
        ZZ = COMI +1
        IF COMI AND NOT(R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,ZZ>) THEN
            R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,ZZ> = ''
*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
            FOR K = 2 TO NO.M
                R.NEW(INF.MLT.SIGN)<1,K> = 'DEBIT'
                R.NEW(INF.MLT.TXN.CODE)<1,K> = 78
                R.NEW(INF.MLT.CURRENCY.MARKET)<1,K> = 1
            NEXT K
        END
        IF COMI THEN

*************FOR  AC.ID = '9949990010321401' BEFORE (2:59)*******************
            ACCT.ID = '9949990010321401'
            TD1 = TODAY
            CALL CDT("",TD1,'-1W')
            FROM.DATE1 = TD1
            END.DATE1  = TD1
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE1,END.DATE1,ID.LIST,OPENING.BAL,ER)
            LOOP

                REMOVE STE.ID FROM ID.LIST SETTING POS.STE
            WHILE STE.ID:POS.STE
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>[1,12]
                FT.ID  = STE.REF:';1'
                CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
                NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
                CO.CODE           = R.FT<FT.CO.CODE>
                IF ER.FT THEN
                    CALL F.READ(FN.TT,FT.ID,R.TT,F.TT,ER.TT)
                    NOTE.CREDIT       = R.TT<TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME>
                    CO.CODE       = R.TT<TT.TE.CO.CODE>
                END
                LENN = LEN(NOTE.CREDIT)
                IF LENN EQ '1' THEN
                    IF CO.CODE NE 'EG0010099' THEN
                        IF NOTE.CREDIT LE '3' THEN
                            IF NOTE.CREDIT NE '.' THEN
                                DEBIT.AMT1         = R.STE<AC.STE.AMOUNT.LCY>
                                AMT.TOT1 +=DEBIT.AMT1
                            END
                        END
                    END
                END
            REPEAT
*************FOR  AC.ID = '9949990010321401' AFTER (2:59)*******************
            ACCT.ID = '9949990010321401'
            TD2 = TODAY
            CALL CDT("",TD2,'-2W')
            FROM.DATE2 = TD2
            END.DATE2  = TD2
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE2,END.DATE2,ID.LIST2,OPENING.BAL,ER)
            LOOP
                REMOVE STE.ID2 FROM ID.LIST2 SETTING POS.STE2
            WHILE STE.ID2:POS.STE2
                CALL F.READ(FN.STE,STE.ID2,R.STE2,F.STE,ER.STE)
                STE.REF = R.STE2<AC.STE.TRANS.REFERENCE>[1,12]
                FT.ID = STE.REF:';1'
                CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
                NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
                CO.CODE           = R.FT<FT.CO.CODE>
                IF ER.FT THEN
                    CALL F.READ(FN.TT,FT.ID,R.TT,F.TT,ER.TT)
                    NOTE.CREDIT       = R.TT<TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME>
                    CO.CODE = R.TT<TT.TE.CO.CODE>
                END
                LENN = LEN(NOTE.CREDIT)
                IF LENN EQ '1' THEN
                    IF CO.CODE NE 'EG0010099' THEN
                        IF NOTE.CREDIT GT '3' THEN
                            IF NOTE.CREDIT NE '.' THEN
                                DEBIT.AMT2         = R.STE2<AC.STE.AMOUNT.LCY>
                                AMT.TOT2 +=DEBIT.AMT2
                            END
                        END
                    END
                END
            REPEAT
*********TOTAL1************************************************
            WORK.BAL = AMT.TOT1 + AMT.TOT2
            R.NEW(INF.MLT.AMOUNT.LCY)<1,2> = WORK.BAL
**************************************************************

*************FOR  AC.ID = 'EGP1611400040099' BEFORE (2:59)*******************
            ACCT.ID = 'EGP1611400040099'
            TD1 = TODAY
            CALL CDT("",TD1,'-1W')
            FROM.DATE1 = TD1
            END.DATE1  = TD1
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE1,END.DATE1,ID.LIST,OPENING.BAL,ER)
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS.STE
            WHILE STE.ID:POS.STE
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>[1,12]
                FT.ID = STE.REF:';1'
                CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
                NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
                CO.CODE =R.FT<FT.CO.CODE>
                IF ER.FT THEN
                    CALL F.READ(FN.TT,FT.ID,R.TT,F.TT,ER.TT)
                    NOTE.CREDIT       = R.TT<TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME>
                    CO.CODE = R.TT<TT.TE.CO.CODE>
                END
                LENN = LEN(NOTE.CREDIT)
                IF LENN EQ '1' THEN
                    IF CO.CODE NE 'EG0010099' THEN
                        IF NOTE.CREDIT NE '.' THEN
                            IF NOTE.CREDIT LE '3' THEN
                                DEBIT.AMT3 = R.STE<AC.STE.AMOUNT.LCY>
                                AMT.TOT3 + = DEBIT.AMT3
                            END
                        END
                    END
                END
            REPEAT
*************FOR  AC.ID = 'EGP1611400040099' AFTER (2:59)*******************
            ACCT.ID = 'EGP1611400040099'
            TD2 = TODAY
            CALL CDT("",TD2,'-2W')
            FROM.DATE2 = TD2
            END.DATE2  = TD2
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE2,END.DATE2,ID.LIST2,OPENING.BAL,ER)
            LOOP
                REMOVE STE.ID2 FROM ID.LIST2 SETTING POS.STE2
            WHILE STE.ID2:POS.STE2
                CALL F.READ(FN.STE,STE.ID2,R.STE,F.STE,ER.STE)
                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>[1,12]
                FT.ID = STE.REF:';1'
                CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
                NOTE.CREDIT       = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
                CO.CODE = R.FT<FT.CO.CODE>
                IF ER.FT THEN
                    CALL F.READ(FN.TT,FT.ID,R.TT,F.TT,ER.TT)
                    NOTE.CREDIT       = R.TT<TT.TE.LOCAL.REF><1,TTLR.EFIN.TIME>
                    CO.CODE = R.TT<TT.TE.CO.CODE>
                END
                LENN = LEN(NOTE.CREDIT)
                IF LENN EQ '1' THEN
                    IF CO.CODE NE 'EG0010099' THEN
                        IF NOTE.CREDIT GT '3' THEN
                            IF NOTE.CREDIT NE '.' THEN
                                DEBIT.AMT4 = R.STE<AC.STE.AMOUNT.LCY>
                                AMT.TOT4 +=DEBIT.AMT4
                            END
                        END
                    END
                END
            REPEAT
*********TOTAL2************************************************
            OPEN.BAL = AMT.TOT3 + AMT.TOT4
            R.NEW(INF.MLT.AMOUNT.LCY)<1,3> = OPEN.BAL
*********************************************************
            R.NEW(INF.MLT.AMOUNT.LCY)<1,1>  = OPEN.BAL  + WORK.BAL
            R.NEW(INF.MLT.CURRENCY)<1,1> = 'EGP'
            R.NEW(INF.MLT.CURRENCY)<1,2> = 'EGP'
            R.NEW(INF.MLT.CURRENCY)<1,3> = 'EGP'
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,1> = '1'
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,2> = '1'
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,3> = '1'
            R.NEW(INF.MLT.TXN.CODE)<1,1> = '79'
            R.NEW(INF.MLT.TXN.CODE)<1,2> = '78'
            R.NEW(INF.MLT.TXN.CODE)<1,3> = '78'
            R.NEW(INF.MLT.SIGN)<1,1> = 'CREDIT'
            R.NEW(INF.MLT.SIGN)<1,2> = 'DEBIT'
            R.NEW(INF.MLT.SIGN)<1,3> = 'DEBIT'
            R.NEW(INF.MLT.VALUE.DATE)<1,1> = TODAY
            R.NEW(INF.MLT.VALUE.DATE)<1,2> = TODAY
            R.NEW(INF.MLT.VALUE.DATE)<1,3> = TODAY
        END
        RETURN
    END
END
