* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CD.LIQ.SUEZ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.LIQ.RATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SCCD.INT

    IF MESSAGE NE 'VAL' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
        LOC.CD          = R.LD<LD.LOCAL.REF>

        OLD.CD.NO       = LOC.CD<1,LDLR.CD.QUANTITY>
        OLD.CD.AMT.PER  = R.LD<LD.AMOUNT> / OLD.CD.NO
        OLD.CD.SER.END  = LOC.CD<1,LDLR.ARTICLE.NO>
        OLD.FIN.DATE    = R.LD<LD.FIN.MAT.DATE>


        IF COMI LE OLD.CD.NO THEN
            NEW.CD.NO = LOC.CD<1,LDLR.CD.QUANTITY> - COMI
            R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY> = NEW.CD.NO
            NEW.LIQ.AMT  = COMI * OLD.CD.AMT.PER
            CALL REBUILD.SCREEN
*****************************************************
            SHIFT ='' ; ROUND =''
            DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)
***********TO CALCULATE THE INTERST TAKEN
            VAL.DATE = R.NEW(LD.VALUE.DATE)
            START.DATE =VAL.DATE
******************** TO BE REMOVED
            TO.DATE  = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
            NEXT.INT = R.NEW(LD.LOCAL.REF)<1,LDLR.NEXT.INT.DATE>

            DAYS = "C"
            DATE.VAL = R.NEW(LD.VALUE.DATE)
            CALL CDD("",DATE.VAL,TO.DATE,DAYS)

            IF DAYS LE "365" THEN
                ETEXT='��� �� ���� ���� �� ���'  ;CALL STORE.END.ERROR
            END ELSE
**********************TO CALUC INTEREST TAKEN WITH OLD RATE*******************
                DATE.VAL   = R.NEW(LD.VALUE.DATE)
                CALL CDT ('',DATE.VAL,'1C')
                START.DATE = DATE.VAL
                FOR J = 1 TO 60
                    START.DATE = SHIFT.DATE(START.DATE,'3M','UP')
                    TMP.DATE = START.DATE
                    IF TMP.DATE GE NEXT.INT THEN
                        COMM.NO = J  ; J = 60
                    END
                NEXT J


                IF NEXT.INT GT TODAY THEN COMM.NO = COMM.NO -1
***----------------20150826----------
                INT.OLD.RATE='12' / 100
                INT.OLD.AMT = NEW.LIQ.AMT  * INT.OLD.RATE

                PER.INT.OLD.AMT = INT.OLD.AMT /4

                IF COMM.NO EQ '8' THEN
                    INT.OLD.AMT.CLAIM =  PER.INT.OLD.AMT * COMM.NO

                END ELSE
                    IF COMM.NO GT '8' THEN
                        OLD.AMT.12 =  PER.INT.OLD.AMT * 8
************
                        INT.OLD.RATE.15='0.155'
                        INT.OLD.AMT.15 = NEW.LIQ.AMT  * INT.OLD.RATE.15
                        PER.INT.OLD.AMT.15 = INT.OLD.AMT.15 /4

**                        NEW.AMT.15 = PER.INT.OLD.AMT.15
**NEW MODIFICATION
                        COMM.NO.NEW = COMM.NO - 8
                        NEW.AMT.15 = PER.INT.OLD.AMT.15 * COMM.NO.NEW
*****************
                        INT.OLD.AMT.CLAIM = OLD.AMT.12 + NEW.AMT.15
                        *TEXT= OLD.AMT.12 :'OLD AMOUNT':NEW.AMT.15 ;CALL REM
                    END
                END
****************************************END OF CAL INTEREST TAKEN WITH OLD RATE
**********************TO CALCULATE  INTEREST TO BE PAID WITH NEW RATE*****
                DAYS.RATE='C'

                TO.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>

                CALL CDD("",DATE.VAL,TO.DATE,DAYS.RATE)

                CATEG='21103'
                F.CD.LIQ = '' ; FN.CD.LIQ = 'F.SCB.CD.LIQ.RATE' ; R.SCB.CD.LIQ = ''
                CALL OPF(FN.CD.LIQ,F.CD.LIQ)
                CALL F.READ(FN.CD.LIQ, CATEG, R.CD.LIQ, F.CD.LIQ, ETEXT)
*Line [ 118 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                TEMP.COUNT =  DCOUNT (R.CD.LIQ<SCB.CD.LIQ.MIN.DAY>,@VM)

                FOR I = 1 TO TEMP.COUNT
                    IF R.CD.LIQ<SCB.CD.LIQ.MIN.DAY,I> LE DAYS.RATE AND R.CD.LIQ<SCB.CD.LIQ.MAX.DAY,I> GE DAYS.RATE THEN
                        NEW.RATE= R.CD.LIQ<SCB.CD.LIQ.RATE,I>
                        I = TEMP.COUNT
                    END
                NEXT I

**---------------20150826
                IF COMM.NO EQ '8' THEN
                    NEW.RATE ='8'
                    NEW.INT = ((((NEW.LIQ.AMT*NEW.RATE) / 100 ) / 4 ) * COMM.NO )
                END ELSE
                    IF COMM.NO GT '8' THEN
                        NEW.RATE.12 ='8'
                        NEW.INT.12 = ((((NEW.LIQ.AMT*NEW.RATE.12) / 100 ) / 4 ) * 8)
***                     NEW.RATE.15 ='10.333'
**NEW MODIFICATION
                        NEW.RATE.15 = NEW.RATE
                        *TEXT=NEW.RATE.15:'NEW ABE';CALL REM
                        COMM.NO.NEW = COMM.NO - 8
                        *TEXT=COMM.NO.NEW:'COMM.NO.NEW';CALL REM
                        NEW.INT.15 = ((((NEW.LIQ.AMT*NEW.RATE.15) / 100 ) / 4 ) * COMM.NO.NEW )
                        *TEXT=NEW.INT.15;CALL REM
***
                        NEW.INT =NEW.INT.12 + NEW.INT.15
                    END
                END
**TEXT  = "NEW.INT" : NEW.INT ; CALL REM
                CALL EB.ROUND.AMOUNT ('EGP',NEW.INT,'',"")
***************************
                INT.PENAL = INT.OLD.AMT.CLAIM - NEW.INT
                AMT.LIQ = NEW.LIQ.AMT - INT.PENAL
*****************
                CALL EB.ROUND.AMOUNT ('EGP',AMT.LIQ,'',"")
                R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> = AMT.LIQ
                R.NEW(LD.LOCAL.REF)<1,LDLR.T.CHRG.AMT> = INT.PENAL
**----------20150826
*                BNK.COM = NEW.LIQ.AMT /100
                BNK.COM = INT.PENAL * (20 / 100 )
                CALL EB.ROUND.AMOUNT ('EGP',BNK.COM,'',"")
**----------20150826
                R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT> = BNK.COM
                CBE.COM=INT.PENAL - BNK.COM
                R.NEW(LD.LOCAL.REF)<1,LDLR.INSUR.AMT> = CBE.COM
*******************
                CD.LIQ.AMT = OLD.CD.AMT.PER * COMI
                CD.AMT =R.NEW(LD.AMOUNT)
                IF CD.LIQ.AMT NE CD.AMT THEN
                    R.NEW(LD.AMOUNT.INCREASE) =  (OLD.CD.AMT.PER * COMI)*-1
                    R.NEW(LD.AMT.V.DATE)      =  TODAY
                    R.NEW(LD.FIN.MAT.DATE)    = OLD.FIN.DATE
                END ELSE
                    R.NEW(LD.AMOUNT.INCREASE) =  ''
                    R.NEW(LD.AMT.V.DATE)      =  ''
                    R.NEW(LD.FIN.MAT.DATE)    =  TODAY
                END
*************MODIFIED AT 21-9-2015 AND ACTIVATED AT 29-9-2015
                IF COMI LT OLD.CD.NO  THEN
                    NEW.CD.NO = COMI
                    NEW.CD.SER.NO = OLD.CD.SER.END - NEW.CD.NO
                END
                IF COMI EQ OLD.CD.NO THEN
                    NEW.CD.NO = COMI - 1
                    NEW.CD.SER.NO = OLD.CD.SER.END - NEW.CD.NO
                END
***********MODIFIED AT 21-9-2015 AND ACTIVATED AT 29-9-2015

                IF LEN(NEW.CD.SER.NO) LT 14 THEN
                    NEW.CD.SER.NO = '0':NEW.CD.SER.NO
                END
                R.NEW(LD.LOCAL.REF)<1,LDLR.ARTICLE.NO> =  NEW.CD.SER.NO
********************
                CALL REBUILD.SCREEN
            END
**        IF COMI # TODAY THEN ETEXT='??? ?? ???? ????? ?????'  ;CALL STORE.END.ERROR
        END ELSE
            ETEXT = 'CD Liq No Should be less than or Equal CD No';CALL STORE.END.ERROR
        END
*****************************************************
    END

    RETURN
END
