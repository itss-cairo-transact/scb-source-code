* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
**** CREATED BY MOHAMED SABRY 2011/01/10
**********************************************************************************************************
    SUBROUTINE VVR.CU.NEW.INDUSTRY
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------------------
*------------------------------------------------------
    WS.SECTOR.CODE =  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
    WS.SECTOR.CODE =  WS.SECTOR.CODE[2,3]

    IF ( WS.SECTOR.CODE EQ '130' ) OR ( WS.SECTOR.CODE EQ '120' ) THEN
        IF COMI EQ '' THEN
            ETEXT = '��� ����� ��� ������'
            CALL STORE.END.ERROR
        END
    END
    IF ( WS.SECTOR.CODE EQ '130' ) THEN
        IF ( COMI LT 2000 AND COMI NE '' ) THEN
            ETEXT = '��� ������ �� ������ �� ��� ������ �����'
            CALL STORE.END.ERROR
        END
    END
    IF ( WS.SECTOR.CODE EQ '120' ) THEN
        IF COMI GE 2000 THEN
            ETEXT = '��� ������ �� ������ �� ��� ���� �������'
            CALL STORE.END.ERROR
        END
    END

    IF ( WS.SECTOR.CODE NE '130' ) AND ( WS.SECTOR.CODE NE '120' ) THEN
        IF COMI NE '' THEN
            ETEXT = '��� ����� ��� ������ �� ��� ������'
            CALL STORE.END.ERROR
        END
    END
*    CALL REBUILD.SCREEN
*--------------------- END OF ROUTINE ----------------
END
