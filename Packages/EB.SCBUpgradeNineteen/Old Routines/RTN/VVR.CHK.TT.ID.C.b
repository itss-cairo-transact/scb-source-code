* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHK.TT.ID.C

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*--------------------------------------------
    IDD     = COMI
    LEN.ID  = LEN(IDD)
    CUS.NO  = R.NEW(TT.TE.CUSTOMER.1)
    FN.CU   = "FBNK.CUSTOMER"  ; F.CU = ""
    CALL OPF(FN.CU,F.CU)

    CALL F.READ(FN.CU,CUS.NO,R.CU,F.CU,E.CU)
    SEC.ID = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
    ID.NO  = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
    COMM.NO = R.CU<EB.CUS.LOCAL.REF><1,CULR.COM.REG.NO>
**    LIC.ID.NO  = R.CU<EB.CUS.LOCAL.REF><1,CULR.TAX.EXEMPTION>
    IF SEC.ID EQ '4650' THEN
        IF LEN.ID NE 14 THEN
            ETEXT = 'MUST BE 14 DIGITS'
            CALL STORE.END.ERROR
        END ELSE
********UPATED
            IF COMI NE ID.NO THEN
                ETEXT = '��� ������ ��� ����� ����� ������ ������';CALL STORE.END.ERROR
            END
*****************
        END
    END ELSE
        IF COMM.NO THEN
            IF COMI NE COMM.NO THEN
                ETEXT = '��� ����� ������� ��� ����� ���� ����� ������� ������';CALL STORE.END.ERROR
            END
        END ELSE
**            IF COMI NE LIC.ID.NO THEN
            IF COMI NE  CUS.NO THEN
             **   ETEXT = '��� ������� ��� ����� ���� �������  ������' ;CALL STORE.END.ERROR
ETEXT= '��� �� ���� ��� ������ ����';CALL STORE.END.ERROR

            END
        END
    END

    CALL REBUILD.SCREEN
*-----------------------------------------------------
    RETURN
END
