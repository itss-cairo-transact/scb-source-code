* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>457</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.FCY.AUT3

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF MESSAGE = '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> THEN ETEXT = '��� ����� ������' ; CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
***********************************************************
        ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
******UPDATED BY NESSREEN AHMED 26/03/2009**********************
        IF CATEGG NE 5010 THEN
****************************************************************

            IF ( CATEGG GE 1101 AND CATEGG LE 1450 ) THEN
**   TEXT = '1202' ; CALL REM
                CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
**TEXT = 'LIMTREF=':LIMTREF ; CALL REM
                CUST = R.NEW(TT.TE.CUSTOMER.1)
                IDD = FMT(LIMTREF, "R%10")
                KEY.ID = CUST:'.':IDD
**TEXT = 'ID=':KEY.ID ; CALL REM
                CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
**TEXT = 'AVLM=':AVLM ; CALL REM
**TEXT = 'BAL=':BAL ; CALL REM
                EQT = AVLM + BAL - COMI
**TEXT = 'EQT=':EQT ; CALL REM
                IF EQT < 0 THEN
                    ETEXT = "�� ��� �� ������ ������ �� �����"
                    CALL STORE.END.ERROR
                END
            END
            IF NOT(CATEGG GE 1500 AND CATEGG LE 1590 )  THEN
                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*Line [ 69 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
                NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
***********NESSREEN  17/9/2006***********************************
**16/1/2007 IF LOCK.AMT1 > 0 THEN
                IF LOCK.AMT<1,LOCK.NO> > 0 THEN
**16/1/2007  TEXT = '������ ������� ����=':LOCK.AMT1 ; CALL REM
                    TEXT = '������ ������� ����=':LOCK.AMT<1,LOCK.NO> ; CALL REM
                    TEXT = '������ ������� =': NET.BAL ; CALL REM
                END
*****************************************************************
                IF COMI GT NET.BAL THEN
*****UPDATED BY NESSREEN AHMED 07/06/2010************************
***** IF NOT(CATEGG GE 1101 AND CATEGG LE 1450) THEN
                    IF NOT (CATEGG GE 1101 AND CATEGG LE 1477) AND NOT(CATEGG EQ 1481) THEN
*****END OF UPDATE 07/06/2010*********************************
                        ETEXT = '������ �� ����'
                        CALL STORE.END.ERROR
                    END
                END
*******************************************************
            END     ;* IF NOT CATEEG
********************************
        END         ;* IF CATEGG NE 5010
********************************
    END   ;* IF MESSAGE = ''

    RETURN
END
