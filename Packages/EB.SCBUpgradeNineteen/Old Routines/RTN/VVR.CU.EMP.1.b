* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.EMP.1

* PREVENT USER FORM INPUTING A NEW RECORD
* PREVENT USER FROM MODIFYING A RECORD
* FOR A CUSTOMER THAT IS BANK

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    IF V$FUNCTION = 'I' THEN

        IF NOT(COMI) THEN
            IF R.NEW(EB.CUS.SECTOR) = 1100 OR R.NEW(EB.CUS.SECTOR) = 1300   THEN ETEX = 'MUST.ENTER.EMPLOYEE.NO'
        END


        IF R.NEW(EB.CUS.SECTOR)EQ  1100 OR R.NEW(EB.CUS.SECTOR)EQ 1300 THEN
            FN.CU = 'F.CUSTOMER' ; F.CU= '' ; R.CUSTOMER = ''

            CALL OPF( FN.CU,F.CU)
            T.SEL = "SSELECT FBNK.CUSTOMER WITH EMPLOEE.NO EQ " : COMI
            KEY.LIST=""
            SELECTED=""
            ER.MSG=""
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF KEY.LIST THEN
* ETEXT = " HHHHHHHHHH" ; CALL REM
                ETEXT = '��� ����� ����� �����'
            END
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
