* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>194</Rating>
*-----------------------------------------------------------------------------
******** DINA_SCB ********

    SUBROUTINE VVR.CU.NO.CHANGE.STATUS

*to check that the user should select from drop down list and if he entered anything else, an error message

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.CU.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,STAT1)
        IF NOT(ETEXT) THEN
            LOCATE COMI IN STAT1<1,1> SETTING M ELSE ETEXT = ' NOT ALLOWED STATUS'
        END
        IF COMI # R.NEW(EB.CUS.CUSTOMER.STATUS) THEN
            R.NEW(EB.CUS.RELATION.CODE) = ""
            R.NEW( EB.CUS.REL.CUSTOMER) = ""
            R.NEW( EB.CUS.REVERS.REL.CODE) = ""
            CALL REBUILD.SCREEN
        END
********NESSREEN*****
* CALL DBR( 'CUSTOMER':@FM:EB.CUS.SHORT.NAME, ID.NEW , MYID)
* IF NOT(ETEXT) THEN EXIST = 'Y'
* IF EXIST = 'Y' THEN
*    CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.RESTRICT.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,R.STAT)
*   LOCATE COMI IN R.STAT<1,1> SETTING M ELSE ETEXT = 'NOT ALLOWED STATUS'
* END
********************
        IF R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.OPENING.DATE> = TODAY THEN
            CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.CU.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,T.STAT)
            LOCATE COMI IN T.STAT<1,1> SETTING M ELSE ETEXT = 'NOT ALLOWED STATUS'

        END
*************REHAM UPDATEED 20/01/2009**************
        CC = R.NEW(EB.CUS.BIRTH.INCORP.DATE)
        TR = CC[1,4]
        TC= CC[5,4]
        TN = TR + 21
        TNN = TN : TC
*****UPDATED BY NESSREEN AHMED 28/11/2011**********
        IF TNN GT TODAY AND COMI NE 22 THEN
*****            ETEXT = '������ ����� ����'
        END
******28/11/2011
        IF TNN LE TODAY AND COMI EQ 22 THEN
            ETEXT = '������ �� �� ����� '
        END
        RETURN
    END
