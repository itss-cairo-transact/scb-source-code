* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>646</Rating>
*-----------------------------------------------------------------------------

** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.CU.ID.EXPIRY.DATE

*A VALIDATION ROUTINE TO VALIDATE IF THE ID TYPE IS EQ "3 PASSPORT" OR "4 GUN" SO THE EXPIRY DATE MUST BE ENTERED
*AND IT MUST BE GREATER THAN TODAY AND ISSUE DATE
*AND IF ID TYPE IS NOT ENTERED SO THE EXPIRY DATE MUST NOT  BE ENTERED  TOO

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF MESSAGE = 'VAL' THEN

***Mahmoud 8/6/2009 ***
***Modified to validate EXPIRY.DATE with NSN ID ***

        IF R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO> # '' THEN
*** MSABRY 2012/02/21
            IF R.NEW(EB.CUS.CUSTOMER.STATUS) # '22' THEN
**************************
                IF NOT(COMI) THEN ETEXT = 'ENTER EXPIRY DATE'
                ELSE
                    ISSUE.D = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.ISSUE.DATE>
                    ISSUE.Y = ISSUE.D[1,4]
                    EXP.Y   = ISSUE.Y + 7
                    EXP.DD  = EXP.Y : ISSUE.D[5,4]
                    IF COMI NE EXP.DD THEN ETEXT = 'MUST BE ISSUE.DATE YEAR + 7'
                END
            END
        END

***IF R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ID.TYPE> AND R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ID.TYPE> # 5 THEN
        IF (R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> AND R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> # 5) OR (R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO> # '')  THEN
*******************
            IF INDEX('34', R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE>,1) THEN
*** MSABRY 2012/02/21
                IF R.NEW(EB.CUS.CUSTOMER.STATUS) # '22' THEN
                    IF NOT(COMI) THEN ETEXT = 'ENTER EXPIRY DATE'


***MAHMOUD 22/6/2009***
***ELSE
***IF COMI LE TODAY THEN ETEXT = 'THE DATE MUST BE GT TODAY OR ISSUE'
***END

                END ELSE
*****Updated by NESSREEN AHMED 5/8/2020*****************************
*****      IF COMI THEN ETEXT = 'NO EXP DATE FOR THIS TYPE OF ID'
                    IF R.NEW(EB.CUS.NATIONALITY) EQ 'EG' THEN
                        IF COMI THEN ETEXT = 'NO EXP DATE FOR THIS TYPE OF ID'
                    END
*****End of update 5/8/2020******************************************
                END
            END ELSE
                IF COMI THEN ETEXT = 'NO ID TYPE'
            END
        END
        RETURN
    END
