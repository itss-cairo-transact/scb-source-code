* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>103</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/03/18 ***
*******************************************

    SUBROUTINE VVR.CU.SECURITY.CHK
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.SECURITY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB GET.DATA
    GOSUB CALC.SECURITY.KEY
*    GOSUB PROCESS
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.CU  = "FBNK.CUSTOMER"            ; F.CU   = ""
    FN.SCS = "F.SCB.CUSTOMER.SECURITY"  ; F.SCS  = "" ; R.SCS = ""

    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.SCS,F.SCS)
    RETURN
*-----------------------------------------------------
GET.DATA:
    WS.NATION           = R.NEW(EB.CUS.NATIONALITY)
    WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
    WS.CUSTOMER.STATUS  = R.NEW(EB.CUS.CUSTOMER.STATUS)
    WS.CONT.DATE        = R.NEW(EB.CUS.CONTACT.DATE)
    WS.PASSPORT.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER>
    WS.NATIONAL.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO>
    WS.PLACE.ISSUE      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE>
    WS.REG.NO           = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.COM.REG.NO>
    WS.ID.TYPE          = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE>
    WS.NEW.SECTOR.NEW   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
    WS.NEW.SECTOR.OLD   = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>

*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.NEW.SECTOR':@FM:C.PROVE.CODE,WS.NEW.SECTOR.NEW,WS.PROVE.CODE.NEW)
F.ITSS.SCB.NEW.SECTOR = 'F.SCB.NEW.SECTOR'
FN.F.ITSS.SCB.NEW.SECTOR = ''
CALL OPF(F.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR)
CALL F.READ(F.ITSS.SCB.NEW.SECTOR,WS.NEW.SECTOR.NEW,R.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR,ERROR.SCB.NEW.SECTOR)
WS.PROVE.CODE.NEW=R.ITSS.SCB.NEW.SECTOR<C.PROVE.CODE>
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.NEW.SECTOR':@FM:C.PROVE.CODE,WS.NEW.SECTOR.OLD,WS.PROVE.CODE.OLD)
F.ITSS.SCB.NEW.SECTOR = 'F.SCB.NEW.SECTOR'
FN.F.ITSS.SCB.NEW.SECTOR = ''
CALL OPF(F.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR)
CALL F.READ(F.ITSS.SCB.NEW.SECTOR,WS.NEW.SECTOR.OLD,R.ITSS.SCB.NEW.SECTOR,FN.F.ITSS.SCB.NEW.SECTOR,ERROR.SCB.NEW.SECTOR)
WS.PROVE.CODE.OLD=R.ITSS.SCB.NEW.SECTOR<C.PROVE.CODE>

    WS.PROVE.CODE = WS.PROVE.CODE.NEW
    WS.NEW.SECTOR = WS.NEW.SECTOR.NEW

    RETURN
*-----------------------------------------------------
CALC.SECURITY.KEY:

    IF WS.PROVE.CODE EQ 1 THEN
        IF WS.CUSTOMER.STATUS NE 3 AND WS.ID.TYPE NE 5 THEN
            IF WS.NATION EQ 'EG' THEN
                IF  WS.NATIONAL.ID NE '' THEN
                    COMI = WS.NATION:'.':WS.NATIONAL.ID
                END
            END ELSE
                IF  WS.PASSPORT.ID EQ '' THEN
*                    ETEXT = '��� ����� ��� �������'
*                    CALL STORE.END.ERROR
                    RETURN
                END
                IF  WS.PASSPORT.ID NE '' THEN
                    COMI = WS.NATION:'.':WS.PASSPORT.ID
                END
            END
            IF COMI NE '' THEN
                GOSUB  CHK.SECURITY.FILE
            END
        END
    END
    IF WS.PROVE.CODE EQ 2 THEN
        IF  WS.PLACE.ISSUE EQ '' AND WS.NEW.SECTOR NE '4250' AND WS.POST.REST NE 78 THEN
*            ETEXT = '��� ����� ���� ����� �����'
*            CALL STORE.END.ERROR
            RETURN
        END
        IF  WS.REG.NO EQ '' AND WS.NEW.SECTOR.NEW NE '4250' AND WS.POST.REST NE 78 THEN
*            ETEXT = '��� ����� ��� ����� �������'
*            CALL STORE.END.ERROR
            RETURN
        END
        IF WS.PLACE.ISSUE NE '' AND WS.REG.NO NE '' AND WS.POST.REST NE 78 THEN
            COMI = WS.NATION:'.':WS.PLACE.ISSUE:'.':WS.REG.NO
        END
        IF COMI NE '' THEN
            GOSUB CHK.SECURITY.FILE
        END
    END
    IF WS.PROVE.CODE EQ '' THEN
        COMI = ''
    END
    RETURN
*-----------------------------------------------------
CHK.SECURITY.FILE:

    WS.SCS.ID = COMI

    CALL F.READ(FN.SCS,WS.SCS.ID,R.SCS,F.SCS,E2)
    IF NOT(E2) THEN
*Line [ 128 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.SCS.COUNT  = DCOUNT((R.SCS<SCS.CUSTOMER.ID>),@VM)
        FOR I.SCS = 1 TO WS.SCS.COUNT
            IF ID.NEW = R.SCS<SCS.CUSTOMER.ID,(I.SCS)> THEN
                RETURN
            END
        NEXT I.SCS
        IF R.SCS<SCS.CHECK.FLAG> = 'Y' THEN
            IF WS.CONT.DATE GE 20120426 THEN
                ETEXT = '������� ����� ��� ������ �� ����'
                CALL STORE.END.ERROR
                CALL REBUILD.SCREEN
                RETURN
            END
        END
    END

    RETURN
*-----------------------------------------------------
