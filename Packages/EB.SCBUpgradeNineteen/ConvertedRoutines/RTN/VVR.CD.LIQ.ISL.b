* @ValidationCode : MjotMTg2MDE0MDUyMzpDcDEyNTI6MTY0MTc5MTIyOTgwMTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 21:07:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
SUBROUTINE VVR.CD.LIQ.ISL
*-----------------------MAI OCT 2018--------------------------------
*                   <Rating>462</Rating>
*-------------------------------------------------------------------
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.SCHEDULE.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.SCHEDULES.PAST
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CD.RATES

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE # 'VAL' THEN
*            TEXT = 'FUNCTION = ' : V$FUNCTION; CALL REM;
*            TEXT = 'MESSAGE = ' : MESSAGE; CALL REM;

            IF COMI THEN
                IF COMI # TODAY  THEN
                    ETEXT = 'Must Be today date'  ;CALL STORE.END.ERROR
                END ELSE
*--------------------------------------------------------------------*
*                          INITIALISE                                *
*--------------------------------------------------------------------*
* TEXT = 'HI' ; CALL REM;
                    F.LMM.ACC  = '' ; FN.LMM.ACC = 'FBNK.LMM.ACCOUNT.BALANCES'
                    R.LMM.ACC  = ''
                    CALL OPF(FN.LMM.ACC,F.LMM.ACC)

                    F.SCH.DATE = '' ; FN.SCH.DATE = 'FBNK.LMM.SCHEDULE.DATES'
                    R.SCH.DATE = ''
                    CALL OPF(FN.SCH.DATE,F.SCH.DATE)

                    F.SCH.PAST = '' ; FN.SCH.PAST = 'FBNK.LMM.SCHEDULES.PAST'
                    R.SCH.PAST = ''
                    CALL OPF(FN.SCH.PAST,F.SCH.PAST)

*F.BAS.INT  = '' ; FN.BAS.INT  = 'FBNK.BASIC.INTEREST'; R.BAS.INT  = ''
*CALL OPF(FN.BAS.INT,F.BAS.INT)

*F.SCB.RATE = '' ; FN.SCB.RATE = 'F.SCB.CD.RATES'; R.SCB.RATE = ''
*CALL OPF(FN.SCB.RATE,F.SCB.RATE)

                    SHIFT = '' ; ROUND = '' ; START.DATE = ""

                    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)

                    KEY.LIST   = "" ; SELECTED    = "" ; ER.MSG = ""
                    HH         = 0
*--------------------------------------------------------------------*
                    CD.RECOVERED = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.NO>
                    CD.QUAN      = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
*                IF CD.RECOVERED GT CD.QUAN THEN
*                    ETEXT='Must be less than or equal number of CDs'  ;CALL STORE.END.ER
*                    CALL REBUILD.SCREEN
*                END
********************
                    MYVAL=R.NEW(LD.VALUE.DATE)
****************
                    ST.DATE=R.NEW(LD.VALUE.DATE)

                    NO.INT.MON ='6M'
                    ST.DATE = SHIFT.DATE(ST.DATE,NO.INT.MON,UP)
************************
**                    ST.DATE= 20180201
                    IF ST.DATE GT COMI THEN
                        ETEXT = 'Must Be More Than 6 Months'  ;CALL STORE.END.ERROR
                    END ELSE
**********************************************
*          CD.QUAN        = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
*          CD.RECOVERED   = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.NO>
*                    TEXT='CD.QUAN=': CD.QUAN;CALL REM
*                    TEXT='CD.RECOVERED=':CD.RECOVERED;CALL REM
                        IF CD.RECOVERED LE CD.QUAN THEN
                            REMAINING.CD = CD.QUAN  - CD.RECOVERED
*                        TEXT='REMAINING.CD=':REMAINING.CD;CALL REM
                        END
****CONDITION BECAUSE IF THERE ARE ANOTHER CDs
                        IF REMAINING.CD EQ 0 THEN
                            R.NEW(LD.FIN.MAT.DATE) = COMI
                            R.NEW(LD.INT.LIQ.ACCT) = 'EGP1120300010011'
                        END ELSE
***IF MORE THAN 0
                            R.NEW(LD.DRAWDOWN.ACCOUNT) = 'EGP1631900010011'
                        END

                        IDD  = ID.NEW:"00"
                        CALL F.READ(FN.SCH.DATE, IDD, R.SCH.DATE, F.SCH.DATE,ETEXT1231)
*TEXT=ETEXT;CALL REM
*Line [ 118 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DATE.LIST= CONVERT(@VM,"*", R.SCH.DATE)
                        LOOP
                            REMOVE ID.PAST FROM DATE.LIST  SETTING POS
                        WHILE ID.PAST:POS
                            SCHEDULE.DATES     = FIELDS(ID.PAST,"*",1,1)
                            SCHEDULE.PROCESSED = FIELDS(ID.PAST,"*",2,1)

                            FLAG.D = SCHEDULE.PROCESSED
                            IF FLAG.D EQ 'D' THEN
                                IDD.PAST = IDD[1,12]:SCHEDULE.DATES:'00'
                                CALL F.READ(FN.SCH.PAST, IDD.PAST, R.SCH.PAST, F.SCH.PAST, ETEXT1)
                                HH = HH + 1
*DECOUNT.INT  = DCOUNT(R.SCH.PAST<LD28.INT.RATE.USED>,VM)
*TEXT = 'DCOUNT OF ID.PAST  = ': DECOUNT.INT  ; CALL REM;
*FOR XX = 1 TO  DECOUNT.INT
                                INTT         = R.SCH.PAST<LD28.INT.RATE.USED><1,1>

                                INT.100      = INTT/100

                                INT.AMT.RECIEVED      = R.SCH.PAST<LD28.INT.RECEIVED> /INT.100
                                PAST.AMT  = R.SCH.PAST<LD28.OUTS.PRNCPL.AMT><1,1>
                                PAST.QUAN = PAST.AMT / 1000
                                INT.AMT.PER.CD  =  INT.AMT.RECIEVED / PAST.QUAN
*INT.AMT.PER.CD  =  INT.AMT.RECIEVED / CD.QUAN
                                INT.AMT = INT.AMT.PER.CD * CD.RECOVERED

***TO CALCULATE THE PENALTY ACCODRING NO OF DAYS
                                ACT.DAYS = "C"
                                DATE.VAL = R.NEW(LD.VALUE.DATE)
* AMT    = R.NEW(LD.AMOUNT)
                                CD.PRICE  = R.NEW(LD.AMOUNT)/CD.QUAN
*                            TEXT = 'CD.PRICE = ': CD.PRICE;CALL REM;
                                AMT = CD.PRICE * CD.RECOVERED
*                            TEXT = 'AMT = ': AMT;CALL REM;
                                FIN.DATE = TODAY
                                CALL CDD("",DATE.VAL,FIN.DATE,ACT.DAYS)
* TEXT = 'NUMBER OF DAYS =':ACT.DAYS;CALL REM;
                                IF ( ACT.DAYS LT 365 AND ACT.DAYS GE 180 ) THEN
                                    NEW.INT.RATE = INT.100 - 0.05
*TEXT = 'NEW.INT.RATE = ': NEW.INT.RATE;CALL REM;
                                END
                                IF ( ACT.DAYS LT 730 AND ACT.DAYS GE 365 ) THEN
                                    NEW.INT.RATE = INT.100 - 0.04
                                END
                                IF ( ACT.DAYS LT 1095 AND ACT.DAYS GE 730 ) THEN
                                    NEW.INT.RATE = INT.100 - 0.03
                                END
*-----ROUNDING SHOULD E USED
                                NEW.INTT.AMT  = INT.AMT * NEW.INT.RATE
*                               TEXT = '  NEW.INTT.AMT = ':   NEW.INTT.AMT;CALL REM;

                                NEW.AMT.CALC +=NEW.INTT.AMT
*                               TEXT = 'NEW.AMT.CALC = ':NEW.AMT.CALC;CALL REM;

                                CALL EB.ROUND.AMOUNT ('EGP',NEW.INTT.AMT,'',"")

**FIN.DAT      = R.SCH.PAST<LD28.DATE.TO><1,XX>
**TEXT='FIN.DAT = ':  FIN.DAT;CALL REM;

                                DATE.LAST.INT =  R.SCH.PAST<LD28.DATE.INT.REC>
*                            TEXT='  DATE.LAST.INT = ':   DATE.LAST.INT;CALL REM;

* AMT.CD.REC =  R.SCH.PAST<LD28.INT.RECEIVED> / CD.QUAN
                                AMT.CD.REC =  R.SCH.PAST<LD28.INT.RECEIVED> / PAST.QUAN
*   TEXT = 'AMT.CD.REC':AMT.CD.REC; CALL REM;
                                INT.AMT.PAID  = AMT.CD.REC * CD.RECOVERED
*--------------------------------------------------------------------*
*---                         AMOUNT PAYED                        ----*
*--------------------------------------------------------------------*
*                            TEXT = ' AMT.PAYED= ': AMT.PAYED ; CALL REM;
*                            TEXT = ' INT.AMT': INT.AMT ; CALL REM;
                                AMT.PAYED   += INT.AMT.PAID
*TEXT = ' AMT.PAYED =': AMT.PAYED ; CALL REM;
*NEXT XX
                            END
                        REPEAT
***********************************************************
                        CALL F.READ(FN.LMM.ACC,IDD, R.LMM.ACC, F.LMM.ACC, ETEXT21)
                        NEW.INT.D = R.LMM.ACC<LD27.DATE.INT.ACC.TO>

                        NO.INT =DCOUNT(NEW.INT.D,@VM)
                        LAST.MON = TODAY[1,6]:"01"
                        FOR JJ = 1 TO NO.INT
                            NEW.INT.D.EACH = R.LMM.ACC<LD27.DATE.INT.ACC.TO><1,JJ>
                            IF NEW.INT.D.EACH LT LAST.MON THEN
                                REST.INT.AMT.TODATE  = R.LMM.ACC<LD27.INT.AMT.TODATE><1,JJ>
                                REST.INT.RATE = R.LMM.ACC<LD27.AC.INT.RATE><1,JJ>
                                REST.INT.RATE.100 = REST.INT.RATE/100
                                REST.INT.AMT.ALL    = REST.INT.AMT.TODATE/REST.INT.RATE.100
                                CURR.AMT =  R.LMM.ACC<LD27.PRINC.INT.BAL><1,JJ>
                                LM.CURR.QUAN =  CURR.AMT / 1000
*                                REST.INT.AMT.PER.CD =  REST.INT.AMT.ALL / CD.QUAN
                                REST.INT.AMT.PER.CD =  REST.INT.AMT.ALL / LM.CURR.QUAN
                                REST.INT.AMT = REST.INT.AMT.PER.CD * CD.RECOVERED
*TEXT = 'CD.RECOVER = ':  CD.RECOVERED;CALL REM;
*****NEW PENALTY
                                IF ( ACT.DAYS LT 365 AND ACT.DAYS GE 180 ) THEN
                                    REST.NEW.INT.RATE =  REST.INT.RATE.100 - 0.05
                                END
                                IF ( ACT.DAYS LT 730 AND ACT.DAYS GE 365 ) THEN
                                    REST.NEW.INT.RATE =  REST.INT.RATE.100 - 0.04
                                END
                                IF ( ACT.DAYS LT 1095 AND ACT.DAYS GE 730 ) THEN
                                    REST.NEW.INT.RATE =  REST.INT.RATE.100 - 0.03
                                END
                                NEW.REST.INT.AMT  = REST.INT.AMT * REST.NEW.INT.RATE
                                CALL EB.ROUND.AMOUNT ('EGP',NEW.REST.INT.AMT,'',"")
                                REST.NEW.AMT.CALC +=NEW.REST.INT.AMT
*TEXT = REST.NEW.AMT.CALC:'-REST OF MONTH';CALL REM
**********************************
**WHICH WAS CALCULATED**
*REST.INT.AMT.TODATE.CD = REST.INT.AMT.TODATE / CD.QUAN
                                REST.INT.AMT.TODATE.CD = REST.INT.AMT.TODATE / LM.CURR.QUAN

                                REST.INT.AMT.TODATE.REC = REST.INT.AMT.TODATE.CD * CD.RECOVERED
                                REST.INT.PAY += REST.INT.AMT.TODATE.REC
*                               *TEXT = REST.NEW.AMT.CALC:'-REST OF MONTH';CALL REM
                            END
                        NEXT JJ

*TEXT = 'WHAT WAS CALCULATED = ':  REST.INT.PAY ;CALL REM;
                        DIFF.REST.ACT.AMT =  REST.INT.PAY - REST.NEW.AMT.CALC
**************************************************************
*                       *TEXT=AMT.PAYED:'-INT PAYED';CALL REM
*TEXT='NEW.AMT.CALC=':NEW.AMT.CALC;CALL REM
*TEXT=' REST.NEW.AMT.CALC=':REST.NEW.AMT.CALC;CALL REM
                        IF CD.RECOVERED EQ CD.QUAN THEN
                            REST.ACT.AMT =  NEW.AMT.CALC + REST.NEW.AMT.CALC
                        END ELSE
                            REST.ACT.AMT =  NEW.AMT.CALC - DIFF.REST.ACT.AMT
                        END
*                       *TEXT=NEW.AMT.CALC:'-NEW INT AMT ';CALL REM
*                       *TEXT=REST.NEW.AMT.CALC:'-REST OF INT AMT';CALL REM

                        ACT.AMT = AMT.PAYED - REST.ACT.AMT
*   if he will not take any interest at current quarter
*   ACT.AMT = AMT.PAYED - NEW.AMT.CALC
*                    TEXT=ACT.AMT:'NET AMT';CALL REM
                        IF ACT.AMT GT 0 THEN
                            R.NEW(LD.CHRG.CODE) = 8
                            CALL EB.ROUND.AMOUNT ('EGP',ACT.AMT,'',"")
                            R.NEW(LD.CHRG.AMOUNT)                  = ACT.AMT
                            R.NEW(LD.CHRG.CLAIM.DATE)              = TODAY
                            R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> = ( AMT - ACT.AMT )
                        END ELSE
                            IF ACT.AMT LT 0 THEN
                                AMTT = ACT.AMT * -1
                                CALL EB.ROUND.AMOUNT ('EGP',AMTT,'',"")
                                IF REMAINING.CD GT 0 THEN
                                    R.NEW(LD.CHRG.CODE) = 20
                                    R.NEW(LD.CHRG.AMOUNT)         = AMTT
                                    R.NEW(LD.CHRG.CLAIM.DATE)     = TODAY
                                END ELSE
                                    R.NEW(LD.CHRG.CODE) = 8
                                    R.NEW(LD.REIMBURSE.PRICE)      = ""
                                    R.NEW(LD.REIMBURSE.AMOUNT)     = AMTT + AMT
                                    R.NEW(LD.CHRG.AMOUNT)          = "0.00"
                                END
                                R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> =  AMTT +  AMT
                            END
                        END
***UPDATE AMOUNT OF LD AND DATE AND QUANTITY***

                        IF REMAINING.CD # 0 THEN
                            R.NEW(LD.AMOUNT.INCREASE) = -1 * AMT

                            IF DATE.LAST.INT GT LAST.MON THEN
                                R.NEW(LD.AMT.V.DATE) = DATE.LAST.INT
                            END ELSE
                                R.NEW(LD.AMT.V.DATE) = LAST.MON
                            END
                        END
                        NEW.CD.QUAN = REMAINING.CD
                        R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY> = NEW.CD.QUAN

                        CALL REBUILD.SCREEN
                    END
                END
            END
        END
    END
RETURN
END
