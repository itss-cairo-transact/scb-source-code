* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DRAW.DOC.AMT.DP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON

    CR.ACCT  = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
TEXT=CR.ACCT:'CR.ACCT';CALL REM
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CR.ACCT,CATEG.CR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG.CR=R.ITSS.ACCOUNT<AC.CATEGORY>

    DRW.TYPE=R.NEW(TF.DR.DRAWING.TYPE)

*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DRAWINGS':@FM:TF.DR.DOCUMENT.AMOUNT,ID.NEW,DOC.AMT)
F.ITSS.DRAWINGS = 'FBNK.DRAWINGS'
FN.F.ITSS.DRAWINGS = ''
CALL OPF(F.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS)
CALL F.READ(F.ITSS.DRAWINGS,ID.NEW,R.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS,ERROR.DRAWINGS)
DOC.AMT=R.ITSS.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>

    IF DOC.AMT NE '' THEN
        IF CATEG.CR EQ '3013' AND DRW.TYPE EQ 'DP' THEN

            IF DOC.AMT NE COMI THEN
                ETEXT='CHANGE AMOUNT NOT ALLOWED';CALL STORE.END.ERROR

            END
        END
    END ELSE
        ETEXT=''
    END
************************************************************
    RETURN
END
