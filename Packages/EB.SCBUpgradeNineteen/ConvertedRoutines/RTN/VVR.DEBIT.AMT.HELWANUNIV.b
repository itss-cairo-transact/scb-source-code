* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* REHAM YOUSSEF 6/5/2015 , EDITED BY MAHMOUD MAGDY 29/4/2018
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DEBIT.AMT.HELWANUNIV


*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    IF MESSAGE # 'VAL' THEN
        ZZ = COMI +1
        IF COMI AND NOT(R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,ZZ>) THEN
            R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,ZZ> = ''
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
            FOR K = 2 TO NO.M
                R.NEW(INF.MLT.SIGN)<1,K> = 'DEBIT'
                R.NEW(INF.MLT.TXN.CODE)<1,K> = 78
                R.NEW(INF.MLT.CURRENCY.MARKET)<1,K> = 1
            NEXT K
        END


        IF COMI THEN
            AC.ID = '9949990010321601'
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC.ID,WORK.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
            R.NEW(INF.MLT.AMOUNT.LCY)<1,2> = WORK.BAL

            AC.ID2 = 'EGP1611400060099'
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,AC.ID2,OPEN.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.ID2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
OPEN.BAL=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            R.NEW(INF.MLT.AMOUNT.LCY)<1,3>  = OPEN.BAL

            R.NEW(INF.MLT.AMOUNT.LCY)<1,1>  = OPEN.BAL  + WORK.BAL
            R.NEW(INF.MLT.CURRENCY)<1,1> = 'EGP'
            R.NEW(INF.MLT.CURRENCY)<1,2> = 'EGP'
            R.NEW(INF.MLT.CURRENCY)<1,3> = 'EGP'
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,1> = '1'
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,2> = '1'
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,3> = '1'
            R.NEW(INF.MLT.TXN.CODE)<1,1> = '79'
            R.NEW(INF.MLT.TXN.CODE)<1,2> = '78'
            R.NEW(INF.MLT.TXN.CODE)<1,3> = '78'
            R.NEW(INF.MLT.SIGN)<1,1> = 'CREDIT'
            R.NEW(INF.MLT.SIGN)<1,2> = 'DEBIT'
            R.NEW(INF.MLT.SIGN)<1,3> = 'DEBIT'
            R.NEW(INF.MLT.VALUE.DATE)<1,1> = TODAY
            R.NEW(INF.MLT.VALUE.DATE)<1,2> = TODAY
            R.NEW(INF.MLT.VALUE.DATE)<1,3> = TODAY
        END
        RETURN
    END
