* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>180</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.AUTH.RET3

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF MESSAGE = '' THEN
        IF R.NEW(TT.TE.CURRENCY.1) = 'EGP' THEN
*   IF MESSAGE # 'VAL' THEN
*TEXT = R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> ; CALL REM
            IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN ETEXT = '��� ����� ������' ;CALL STORE.END.ERROR
            R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
            CALL REBUILD.SCREEN
****UPDATED ON 03/09/2008 BY NESSREEN AHMED***********************
            IF COMI LE 500000 THEN
                ETEXT = '��� �� �� ��� ������ �� 500000'
                CALL STORE.END.ERROR
            END
******************************************************************
            ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
            AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV>
*     IF ACCT[11,4] # '1201' THEN
            IF NOT(ACCT[11,4] GE '1500' AND ACCT[11,4] LE '1590') THEN

*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                **TEXT = 'BAL=':BAL ; CALL REM
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO=DCOUNT(LOCK.AMT,@VM)
*****NESSREEN 16/1/2007*************************************
* FOR I=1 TO LOCK.NO
*     CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*     LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
*     TEXT = 'LOCKAMT=' :LOCK.AMT ; CALL REM
* NEXT I
* NET.BAL=BAL-LOCK.AMT1
                NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
*****************************************************
                **TEXT = 'NET.BAL=':NET.BAL ; CALL REM
                CATEG = ACCT[11,4]
                **TEXT=CATEG:'CATEG';CALL REM
** NESSREEN 19/6/2006 IF AMT1 GT BAL THEN
*IF COMI GT NET.BAL AND CATEG NE '1202' THEN
*  ETEXT = '������ �� ����'
*   CALL STORE.END.ERROR
*END
**  END
**    END
            END
        END
        RETURN
    END
