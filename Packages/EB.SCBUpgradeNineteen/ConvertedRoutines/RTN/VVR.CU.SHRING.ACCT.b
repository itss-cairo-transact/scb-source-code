* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>138</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.SHRING.ACCT
*if the comi is sharing.account the rest of id fields must be empty
*if the customer age is grater than 16 then the field id must be entered
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        IF COMI = '5' THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER> = ''
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.ISSUE.DATE> = ''
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.EXPIRY.DATE> = ''
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> = ''
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO> = ''
            CALL REBUILD.SCREEN
        END
        YEARS.16 = ( R.NEW(EB.CUS.BIRTH.INCORP.DATE)[ 1, 4] + 16 ) : R.NEW(EB.CUS.BIRTH.INCORP.DATE)[ 5, 4]
        YEARS.21 = ( R.NEW(EB.CUS.BIRTH.INCORP.DATE)[ 1, 4] + 21 ) : R.NEW(EB.CUS.BIRTH.INCORP.DATE)[ 5, 4]
*        IF YEARS.16 < TODAY THEN
*** UPDATED BY MSABRY 2015/1/18
        IF V$FUNCTION = 'I' THEN
            IF R.NEW(EB.CUS.POSTING.RESTRICT) GE 90 THEN
                RETURN
            END
        END

        IF YEARS.21 < TODAY THEN
            IF NOT(R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO>) THEN
                IF COMI='' THEN ETEXT = '��� ����� ��� �������'
            END
        END ELSE
*            IF YEARS.16 > TODAY THEN
*                IF COMI THEN ETEXT = 'DONT ENTER INFO IN THE TAB'
*                IF COMI THEN
*                    COMI = ''
*                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER> = ''
*                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.ISSUE.DATE> = ''
*                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.EXPIRY.DATE> = ''
*                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> = ''
*                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO> = ''
*                    CALL REBUILD.SCREEN
*                END
*            END
        END
        RETURN
    END
