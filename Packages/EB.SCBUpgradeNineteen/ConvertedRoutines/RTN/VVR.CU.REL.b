* @ValidationCode : MjoxNTY1MjQ5NzkxOkNwMTI1MjoxNjQ1MTI5MTM1MjgwOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 17 Feb 2022 12:18:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.CU.REL
* PREVENT USER FORM INPUTING A NEW RECORD
* PREVENT USER FROM MODIFYING A RECORD
* FOR A CUSTOMER THAT IS BANK

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG.1

    ETEXT = ''
    IF V$FUNCTION = 'I' THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.CU.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,STAT1)
        F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
        FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
        CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
        CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,STAT1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
        STAT1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.CU.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
        IF NOT(ETEXT) THEN
            LOCATE R.NEW(EB.CUS.CUSTOMER.STATUS) IN STAT1<1,1> SETTING YY THEN
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.REL,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,REL1)
                F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
                FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
                CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
                CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,REL1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
                REL1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.REL,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
                IF COMI THEN LOCATE COMI IN REL1<1,YY,1> SETTING M ELSE ETEXT = 'THIS RELATION NOT ALLWED'
            END
        END
    END
    IF COMI EQ '' THEN
        IF R.NEW(EB.CUS.SECTOR) EQ  1200 OR R.NEW(EB.CUS.SECTOR)EQ 1400 THEN
*            ETEXT = '��� ����� �������'

        END
    END ELSE
        ETEXT = ''
    END

RETURN
END
