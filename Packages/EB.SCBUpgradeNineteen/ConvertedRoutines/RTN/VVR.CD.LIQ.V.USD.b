* @ValidationCode : MjoxMDU0MjEzMzA4OkNwMTI1MjoxNjQxNzkxMzE1NjkzOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 21:08:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>161</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.CD.LIQ.V.USD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.SCHEDULE.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.SCHEDULES.PAST
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CD.RATES
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.TYPE.LEVEL


    DAYS     = "C"
    R.NEW(LD.FIN.MAT.DATE) = COMI
    DATE.VAL = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
    IF DATE.VAL GT R.NEW(LD.FIN.MAT.DATE) THEN
        DATE.VAL = R.NEW(LD.VALUE.DATE)
    END ELSE
        DATE.VAL = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
    END

    DATE.EXP = COMI[1,6]:"01"
    CALL CDT ('',DATE.EXP,'-1C')
    CALL CDD("",DATE.VAL,COMI,DAYS)

    IF DAYS LE "180" THEN
        ETEXT='��� �� ���� ���� �� 6����'  ;CALL STORE.END.ERROR
    END ELSE

*--------------------------------------------------------------------*
*                          INITIALISE                                *
*--------------------------------------------------------------------*

        F.LMM.ACC  = '' ; FN.LMM.ACC = 'FBNK.LMM.ACCOUNT.BALANCES'
        R.LMM.ACC  = ''
        CALL OPF(FN.LMM.ACC,F.LMM.ACC)

        F.SCH.DATE = '' ; FN.SCH.DATE = 'FBNK.LMM.SCHEDULE.DATES'
        R.SCH.DATE = ''
        CALL OPF(FN.SCH.DATE,F.SCH.DATE)

        F.SCH.PAST = '' ; FN.SCH.PAST = 'FBNK.LMM.SCHEDULES.PAST'
        R.SCH.PAST = ''
        CALL OPF(FN.SCH.PAST,F.SCH.PAST)

        F.BAS.INT  = '' ; FN.BAS.INT  = 'FBNK.BASIC.INTEREST'
        R.BAS.INT  = ''
        CALL OPF(FN.BAS.INT,F.BAS.INT)

        F.SCB.RATE = '' ; FN.SCB.RATE = 'F.SCB.CD.RATES'
        R.SCB.RATE = ''
        CALL OPF(FN.SCB.RATE,F.SCB.RATE)

        F.SCB.LEV  = '' ; FN.SCB.LEV = 'F.SCB.LD.TYPE.LEVEL'
        R.SCB.LEV  = ''
        CALL OPF(FN.SCB.LEV,F.SCB.SCB.LEV)

        SHIFT = '' ; ROUND = '' ; START.DATE = ""

        DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)

        KEY.LIST   = "" ; SELECTED    = "" ; ER.MSG = ""
        HH         = 0
*--------------------------------------------------------------------*
*--------------------------------------------------------------------*
        R.NEW(LD.FIN.MAT.DATE) = COMI
        IDD  = ID.NEW:"00"

        CALL F.READ(FN.SCH.DATE, IDD, R.SCH.DATE, F.SCH.DATE, ETEXT)
*Line [ 102 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DATE.LIST= CONVERT(@VM,"*", R.SCH.DATE)

        LOOP
            REMOVE ID.PAST FROM DATE.LIST  SETTING POS

        WHILE ID.PAST:POS

            SCHEDULE.DATES     = FIELDS(ID.PAST,"*",1,1)
            SCHEDULE.PROCESSED = FIELDS(ID.PAST,"*",2,1)

            FLAG.D = SCHEDULE.PROCESSED
            PRINT "FLAAAG = " : FLAG.D
            IF FLAG.D EQ 'D' THEN
                IDD.PAST = IDD[1,12]:SCHEDULE.DATES:'00'

                CALL F.READ(FN.SCH.PAST, IDD.PAST, R.SCH.PAST, F.SCH.PAST, ETEXT1)
                HH = HH + 1
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DECOUNT.INT  = DCOUNT(R.SCH.PAST<LD28.INT.RATE.USED>,@VM)
                FOR XX = 1 TO  DECOUNT.INT
                    INTT         = R.SCH.PAST<LD28.INT.RATE.USED><1,XX>
                    INT.AMT      = R.SCH.PAST<LD28.INT.RECEIVED><1,XX>
                    FIN.DAT      = R.SCH.PAST<LD28.DATE.TO><1,XX>

*---------------------------------------------------------------------*
*---                        AMOUNT PAYED                          ----*
*---------------------------------------------------------------------*
                    AMT.PAYED   += INT.AMT

                NEXT XX
            END
        REPEAT
*---------------------------------------------------------------------*
*---                  AMOUNT AFTER PENALTY RATE                   ----*
*------------------------- --------------------------------------------*
        ACT.DAYS = "C"
        ACT.DAYS.NEW = "C"
        DATE.VAL = R.NEW(LD.VALUE.DATE)
        AMT      = R.NEW(LD.AMOUNT)
        FIN.DATE = TODAY[1,6]:'01'
        FIN.DATE.NEW = TODAY
        CALL CDD("",DATE.VAL,FIN.DATE,ACT.DAYS)
        CALL CDD("",DATE.VAL,FIN.DATE.NEW,ACT.DAYS.NEW)

        IF ( ACT.DAYS.NEW LE 365 AND ACT.DAYS.NEW GT 180 ) THEN
            ID.RATE  = '21006':R.NEW(LD.VALUE.DATE)
        END

        IF ( ACT.DAYS.NEW LE 1905 AND ACT.DAYS.NEW GT 365 ) THEN
            ID.RATE  = '21007':R.NEW(LD.VALUE.DATE)
        END

**---------------------------------------------------------------------*
****                           GET.NEW.RATE                         ****
**---------------------------------------------------------------------*

        CALL F.READ(FN.SCB.LEV,ID.RATE, R.SCB.LEV, F.SCB.LEV, ETEXT.LEV)
        W.CUR      = 'USD'
        LOCATE W.CUR IN R.SCB.LEV<LDTL.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
        AMT.TO.ALL = R.SCB.LEV<LDTL.AMT.TO,J>
        AMT.MAX    = MAXIMUM(AMT.TO.ALL)
        LOCATE AMT.MAX IN R.SCB.LEV<LDTL.AMT.TO,J,1> SETTING XX ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
        NEW.RATE   = R.SCB.LEV<LDTL.RATE><1,J,XX>

**---------------------------------------------------------------------*
        NEW.INT.AMT  = (AMT*NEW.RATE*ACT.DAYS)/(365*100)
        CALL EB.ROUND.AMOUNT ('EGP',NEW.INT.AMT,'',"")
        ACT.AMT  =  AMT.PAYED - NEW.INT.AMT
*  -----------------------------------------------------------------  *

        IF ACT.AMT GT 0 THEN
            CALL EB.ROUND.AMOUNT ('EGP',ACT.AMT,'',"")
            R.NEW(LD.CHRG.AMOUNT)                      = ACT.AMT
            R.NEW(LD.CHRG.CLAIM.DATE)                  = TODAY
            R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT>     = (AMT-ACT.AMT)
* R.NEW(LD.NEW.INT.RATE)                     = "0.00"
* R.NEW(LD.INT.RATE.V.DATE)                  = COMI[1,6]:"01"

        END ELSE
            IF ACT.AMT LT 0 THEN
                AMTT = ACT.AMT * -1
                CALL EB.ROUND.AMOUNT ('EGP',AMTT,'',"")
                R.NEW(LD.REIMBURSE.PRICE)              = ""
                R.NEW(LD.REIMBURSE.AMOUNT)             = AMTT +  R.NEW(LD.AMOUNT)
                R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> = AMTT +  R.NEW(LD.AMOUNT)
*   R.NEW(LD.CHRG.AMOUNT)                  = "0.00"
*   R.NEW(LD.NEW.INT.RATE)                 = "0.00"
                R.NEW(LD.INT.RATE.V.DATE)              = COMI[1,6]:"01"

            END
        END
        CALL REBUILD.SCREEN
    END
* ------------------------------------------------------------------ *
RETURN
END
