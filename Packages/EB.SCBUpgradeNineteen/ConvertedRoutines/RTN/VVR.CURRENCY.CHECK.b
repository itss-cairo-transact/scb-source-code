* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*-----WAEL 21-7-2002 ----

SUBROUTINE VVR.CURRENCY.CHECK

*IF YOU CHANGE THE CURRENCY THEN EMPTY ALL THE FIELDS
*IF YOU INPUT A CURRENCY THEN
*IF CURRENCY IS LOCAL CURRENCY THEN MAKE (AMOUNT.FCY.1)  NO INPUT
*IF CURRENCY IS FOREIGN CURRENCY THEN MAKE (AMOUNT.LOCAL.1)  NO INPUT



*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER


IF COMI THEN
IF MESSAGE # 'VAL'  THEN


         R.NEW(TT.TE.ACCOUNT.1)=''
         R.NEW(TT.TE.ACCOUNT.2)=''
         R.NEW(TT.TE.CURRENCY.2) = COMI
         R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
         R.NEW(TT.TE.AMOUNT.FCY.1) = ''
END

    IF COMI = LCCY THEN
      T(TT.TE.AMOUNT.LOCAL.1)<3>=''
      T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
    END ELSE
    T(TT.TE.AMOUNT.FCY.1)<3>=''
    T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
   END

 CALL REBUILD.SCREEN
 END


RETURN
END
