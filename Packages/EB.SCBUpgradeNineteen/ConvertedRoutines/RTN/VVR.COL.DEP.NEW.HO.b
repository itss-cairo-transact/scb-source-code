* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
**------------ BAKRY & M.ELSAYED ---------------*

    SUBROUTINE VVR.COL.DEP.NEW.HO

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_MM.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    IF V$FUNCTION = 'A' THEN
        IF R.NEW(COLL.APPLICATION.ID) # '' AND R.NEW(COLL.RECORD.STATUS) # 'RNAU' THEN
            IF R.NEW(COLL.APPLICATION.ID)[1,2] = "MM" THEN
                IF R.NEW(COLL.COLLATERAL.CODE) = "991" THEN

                    MM.NUM = R.NEW(COLL.APPLICATION.ID)
                    FN.MM = 'F.MM.MONEY.MARKET' ; F.MM = '' ; R.MM = ''
                    CALL OPF(FN.MM,F.MM)
                    CALL F.READ(FN.MM,MM.NUM, R.MM, F.MM, ETEXT)
                    IF NOT(ETEXT) THEN
TEXT = MM.NUM ; CALL REM
                        R.MM<MM.LOCAL.REF,MMLR.COLLATERAL.ID> = ID.NEW

                        LIN = INDEX(ID.NEW,".",2)
                        COL.RIGHT = ID.NEW[1,LIN-1]
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR("COLLATERAL.RIGHT":@FM:COLL.RIGHT.LIMIT.REFERENCE,COL.RIGHT,XXX)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,COL.RIGHT,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
XXX=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.LIMIT.REFERENCE>
                        CALL F.WRITE(FN.MM,MM.NUM,R.MM)
                    END
                END
            END
        END
    END

    IF V$FUNCTION = 'A' AND R.NEW(COLL.RECORD.STATUS)='RNAU' AND R.NEW(COLL.APPLICATION.ID)[1,2] = "MM" AND R.NEW(COLL.COLLATERAL.CODE) = "991" THEN

        MM.NUM = R.NEW(COLL.APPLICATION.ID)
        TEXT = MM.NUM ; CALL REM
        FN.MM = 'F.MM.MONEY.MARKET' ; F.MM = '' ; R.MM = ''
        CALL OPF(FN.MM,F.MM)
        CALL F.READ(FN.MM,MM.NUM, R.MM, F.MM, ETEXT)
        IF NOT(ETEXT) THEN
            R.MM<MM.LOCAL.REF,MMLR.COLLATERAL.ID> = ''
            CALL F.WRITE(FN.MM,MM.NUM,R.MM)
        END
    END
    RETURN
END
