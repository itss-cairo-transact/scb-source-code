* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
********NESSREEN AHMED 12/11/2008*******
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CI.FAULT.FIELDS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF MESSAGE  = ''  THEN
        IF COMI THEN
           ID.USE = COMI:'.1'
**   R.NEW(CARD.IS.LOCAL.REF)<1,LRCI.VISA.CUST.NO> = COMI
            FN.ATM.APP = 'F.SCB.ATM.APP' ; F.ATM.APP = '' ; R.ATM.APP = '' ; E1 = ''
            FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; E2 = ''

            CALL OPF(FN.ATM.APP,F.ATM.APP)
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)

            CALL F.READ(FN.ATM.APP, ID.USE, R.ATM.APP, F.ATM.APP, E1)
            CALL F.READ(FN.CUSTOMER, COMI, R.CUSTOMER, F.CUSTOMER, E2)

            ACC1 = R.ATM.APP<SCB.VISA.ACC.N1>
            ACC2 = R.ATM.APP<SCB.VISA.ACC.N2>
            ACC3 = R.ATM.APP<SCB.VISA.AAC.N3>
            FACC = R.ATM.APP<SCB.VISA.FAST.ACC>

            R.NEW(CARD.IS.ACCOUNT)<1,1> = ACC1
            IF ACC2 # '' THEN
                R.NEW(CARD.IS.ACCOUNT)<1,2> = ACC2
            END
            IF ACC3 # '' THEN
                R.NEW(CARD.IS.ACCOUNT)<1,3> = ACC3
            END
            R.NEW(CARD.IS.LOCAL.REF)<1,LRCI.FAST.ACCT.NO> = FACC

            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
            CUS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
*************UPDATED BY RIHAM R15*****************

*            BR.NO = R.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
            BR.NO = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
            R.NEW(CARD.IS.NAME) = CUS.NAME
            R.NEW(CARD.IS.LOCAL.REF)<1,LRCI.BRANCH.NO> = BR.NO

            CALL REBUILD.SCREEN

        END

    END
    RETURN
END
