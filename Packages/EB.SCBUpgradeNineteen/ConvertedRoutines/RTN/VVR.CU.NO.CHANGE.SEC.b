* @ValidationCode : MjotMTMwOTc1NjYwMTpDcDEyNTI6MTY0NTEyODk1NDY4NDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 12:15:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>400</Rating>
*-----------------------------------------------------------------------------
******** DINA_SCB ********

SUBROUTINE VVR.CU.NO.CHANGE.SEC

*To force the user to enter only the sector related to his version

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS


*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.INDUSTRY,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,IND1)
    F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
    FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
    CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
    CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,IND1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
    IND1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.INDUSTRY,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
    IF NOT(ETEXT) THEN
        LOCATE R.NEW(EB.CUS.INDUSTRY) IN IND1<1,1> SETTING MM THEN
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.SECTOR,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,SEC1)
            F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
            FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
            CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
            CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,SEC1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
            SEC1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.SECTOR,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
            LOCATE COMI IN SEC1<1,MM,1> SETTING YY ELSE ETEXT = 'THIS SECTOR NOT ALLWED'
****************NESSREEN******************************
**         IF COMI = '1200' THEN R.NEW(EB.CUS.LOCAL.REF)<1,CULR.STAFF.RANK> = '70'
**         IF COMI = '1400' THEN R.NEW(EB.CUS.LOCAL.REF)<1,CULR.STAFF.RANK> = '71'
            IF COMI # R.NEW(EB.CUS.SECTOR) THEN T.ENRI<EB.CUS.LOCAL.REF,CULR.STAFF.RANK> = ''
            CALL REBUILD.SCREEN
******************************************************
        END
    END
RETURN
END
