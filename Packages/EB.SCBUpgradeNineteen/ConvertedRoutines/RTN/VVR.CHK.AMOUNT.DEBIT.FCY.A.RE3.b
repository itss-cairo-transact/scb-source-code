* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>322</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT.FCY.A.RE3

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF MESSAGE # 'VAL' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> THEN ETEXT = '��� ����� ������' ; CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
***********************************************************
******12/2/2008**********NESSREEN AHMED********************
***        IF COMI LT 20000 THEN ETEXT = '��� �� �� ��� ������ �� 20000'
***        CALL STORE.END.ERROR
*****************************************************************
******03/09/2008*********NESSREEN AHMED*******************
        IF R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> LE 500000 THEN
            ETEXT = '��� �� �� ��� ������ �� 500000'
            CALL STORE.END.ERROR
        END
*****************************************************************
        ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEGG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*       IF CATEGG = '1202' THEN
        IF (CATEGG GE 1101 AND CATEGG LE 1590 ) THEN
            **TEXT = '1202' ; CALL REM
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIMTREF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
            **TEXT = 'LIMTREF=':LIMTREF ; CALL REM
            CUST = R.NEW(TT.TE.CUSTOMER.1)
            IDD = FMT(LIMTREF, "R%10")
            KEY.ID = CUST:'.':IDD
            **TEXT = 'ID=':KEY.ID ; CALL REM
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,KEY.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVLM=R.ITSS.LIMIT<LI.AVAIL.AMT>
            **TEXT = 'AVLM=':AVLM ; CALL REM
            **TEXT = 'BAL=':BAL ; CALL REM
            EQT = AVLM + BAL - COMI
            **TEXT = 'EQT=':EQT ; CALL REM
            IF EQT < 0 THEN
                ETEXT = "�� ��� �� ������ ������ �� �����"
                CALL STORE.END.ERROR
            END
        END
*        IF CATEGG # '1201' THEN
        IF NOT(CATEGG GE '1500' AND CATEGG LE '1590')  THEN
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
**************NESSREEN 16/1/2007**********************
* FOR I=1 TO LOCK.NO
*   CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*   LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
* NEXT I
* NET.BAL=BAL-LOCK.AMT1
            NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
***********NESSREEN  17/9/2006***********************************
**16/1/2007 IF LOCK.AMT1 > 0 THEN
            IF LOCK.AMT<1,LOCK.NO> > 0 THEN
**16/1/2007  TEXT = '������ ������� ����=':LOCK.AMT1 ; CALL REM
                TEXT = '������ ������� ����=':LOCK.AMT<1,LOCK.NO> ; CALL REM
                TEXT = '������ ������� =': NET.BAL ; CALL REM
            END
*****************************************************************
** CATEG = ACCT[11,4]
*IF COMI GT NET.BAL AND CATEGG NE '1202' THEN
*TEXT = 'COMI=':COMI ; CALL REM
* TEXT = 'NET.BAL=':NET.BAL ; CALL REM
*   ETEXT = '������ �� ����'
*  CALL STORE.END.ERROR
*END
******************************************
*END
*******************************************************
        END
        RETURN
    END
