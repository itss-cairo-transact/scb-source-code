* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.CU.INDUSTRY
*A ROUTINE TO:
            * MAKE SURE THAT THE INDUSTRY DOESN'T INCLUDE ** (MAIN INDUSTRY)
            * EMPTY SECTOR,LEGAL.FORM IF THE INDUSTRY IS CHANGED
            * MAKE SURE THAT THE INDUSTRY YOU CHOOSE IS IN THE DROP DOWN LIST

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*CALL DBR("INDUSTRY":@FM:1,COMI,XX)
F.ITSS.INDUSTRY = 'F.INDUSTRY'
FN.F.ITSS.INDUSTRY = ''
CALL OPF(F.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY)
CALL F.READ(F.ITSS.INDUSTRY,COMI,R.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY,ERROR.INDUSTRY)
XX=R.ITSS.INDUSTRY<1>
IF INDEX(XX,'**',1)  THEN
     ETEXT = 'YOU.CANT.USE.MAIN.INDUSTRY'
END
IF COMI # R.NEW(EB.CUS.INDUSTRY) THEN
****UPDATED BY NESSREEN 06/08/2008*******
***  R.NEW(EB.CUS.SECTOR) = ""
***  R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.LEGAL.FORM> = ""

***R.NEW(EB.CUS.CUSTOMER.LIABILITY)= ""
*******************************************
****UPDATED BY NESSREEN AHMED 04/01/2010***
**R.NEW(EB.CUS.CUSTOMER.LIABILITY)=ID.NEW
*******************************************

 CALL REBUILD.SCREEN
END

RETURN
END
