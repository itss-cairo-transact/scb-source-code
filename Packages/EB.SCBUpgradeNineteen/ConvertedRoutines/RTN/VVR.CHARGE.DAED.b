* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>97</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHARGE.DAED

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE

    IF V$FUNCTION = 'I' THEN

*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE,COMI,BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
        CCY.N = R.NEW(FT.DEBIT.CURRENCY)
        COM.ID = "FTCOMDEAD"
        FN.COMM ="F.FT.COMMISSION.TYPE"
        F.COMM = ""

        CALL OPF(FN.COMM,F.COMM)
        CALL F.READ(FN.COMM,COM.ID,COMM.REC,F.COMM,E)
*        CALL F.READ(FN.COMM,COMM.REC,F.COMM,E)
*Line [ 43 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        EXP.COUNT = DCOUNT(COMM.REC<FT4.CURRENCY>,@VM)
        *TEXT = COMM.REC<FT4.CURRENCY> ; CALL REM

        FOR I = 1 TO EXP.COUNT
            IF CCY.N EQ COMM.REC<FT4.CURRENCY,I> THEN
                PERS = COMM.REC<FT4.PERCENTAGE,I>
                CHRG = (PERS * BAL)/100
                *TEXT = CHRG ; CALL REM
                IF CHRG LT COMM.REC<FT4.MINIMUM.AMT,I> THEN
                    AMOUNT = COMM.REC<FT4.MINIMUM.AMT,I>
                END ELSE
                    AMOUNT = CHRG
                END
            END

        NEXT I


        R.NEW(FT.DEBIT.AMOUNT) = AMOUNT

    END




RETURN
END
