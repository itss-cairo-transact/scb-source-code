* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>197</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VVR.CURRENCY.CHECK.LD

* TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER
* TO CHECK THE AVAILABLE BALANCE TO WITHDRAWAL

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER


****UPDATE ON 20101213**********MR Hares and Hytham *********************

    IF R.NEW(LD.CURRENCY) NE '' THEN

        IF COMI NE R.NEW(LD.CURRENCY) THEN
            R.NEW(LD.INTEREST.RATE) = ''
        END
    END

****UPDATE ON 20101213**********MR Hares and Hytham *********************

    IF COMI EQ 'SAR' OR COMI EQ 'CHF' THEN
        ETEXT = "��� ����� "
        CALL REBUILD.SCREEN
    END

    IF R.NEW(LD.CURRENCY) EQ 'SAR' OR R.NEW(LD.CURRENCY) EQ 'CHF' THEN
        ETEXT = "��� ����� "
        CALL REBUILD.SCREEN
    END

    CUST = R.NEW(LD.CUSTOMER.ID)

*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST,CUS.SEC)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>

***------------------MOHAMED MOSTAFA    20/01/2011-----------------------------------

***( ������� (����� �������) � (������ ��������)
    IF R.NEW(LD.CUSTOMER.ID) NE '1101254' AND R.NEW(LD.CUSTOMER.ID) NE '1101253' THEN

***----------------------------------------------------------------------------------
    IF (CUS.SEC EQ '1100' OR CUS.SEC EQ '1200' OR CUS.SEC EQ '1300' OR CUS.SEC EQ '1400' ) AND COMI EQ 'EGP'  THEN
        T(LD.INTEREST.SPREAD)<3> ='NOINPUT'
    END
    END
    RETURN
END
