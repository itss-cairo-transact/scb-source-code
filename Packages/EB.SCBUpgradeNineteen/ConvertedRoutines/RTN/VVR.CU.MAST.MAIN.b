* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
** ----- 03/11.2010 NESSREEN AHMED -----
*-----------------------------------------------------------------------------
* <Rating>249</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.MAST.MAIN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.APP

*TO ENFOURCE THE USER TO ENTER THE MAIN CUSTOMER IF ENTER IN CARD.STATUS IS SUPPLEMENTARY FOR NEW CARD

    CUST.ID =  FIELD(ID.NEW,'.',1)

    IF MESSAGE = 'VAL' THEN
        IF R.NEW(SCB.MAST.CARD.STATUS) = 'MAIN' THEN
            IF COMI # CUST.ID THEN ETEXT = 'Should.be.Same.as.ID'
        END ELSE
            IF R.NEW(SCB.MAST.CARD.STATUS) = 'SUPPLEMENTARY FOR EXIST CARD' THEN
                IF COMI = CUST.ID THEN ETEXT = 'Should.be.Differrent.than.ID'
            END
        END
    END

    RETURN
END
