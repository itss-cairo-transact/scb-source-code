* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>782</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DRAW.ALL.DP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF MESSAGE EQ 'VAL' THEN
****************************
        FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
        CALL OPF(FN.LCC,F.LCC)
        ID.LC = ID.NEW[1,12]
        CALL F.READ(FN.LCC,ID.LC,R.LCC,F.LCC,READ.ERR)

        DR.PER.LIVE  =     R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
        DR.AMT.LIVE  =     R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT>
        DR.ACCT.LIVE =     R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DR.ACCT.LIVE,CATEG.CR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACCT.LIVE,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG.CR=R.ITSS.ACCOUNT<AC.CATEGORY>

        LC.ACCT = LC.REC(TF.LC.CREDIT.PROVIS.ACC)
        TEXT = LC.ACCT : 'LC REC ACCT GLOBUS -ALL.DP';CALL REM
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,LC.ACCT,CATEG.LC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LC.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG.LC=R.ITSS.ACCOUNT<AC.CATEGORY>

        IF CATEG.LC NE '3013' THEN
            IF CATEG.CR EQ '3013' THEN
                IF R.LCC<TF.LC.PROVISION> EQ 'Y' THEN
*********************PER
                    IF DR.PER.LIVE  NE '0' THEN
                        IF DR.PER.LIVE NE '' THEN
                            LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)
                            TEXT=  LC.REC(TF.LC.PROVIS.PERCENT):'LC REC PROV GLO PERC-ALL DP';CALL REM
                            TEXT=LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>:'LC REC LOC PER-ALLDP';CALL REM
* R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)
                            LC.REC(TF.LC.PROVIS.PERCENT) = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
                            TEXT=LC.REC(TF.LC.PROVIS.PERCENT):'PROV PERC NEW-ALL DP';CALL REM
                            R.LCC<TF.LC.PROVIS.PERCENT> = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
*******************************UPDATED BY RIHAM R15**********************
*                            WRITE  R.LCC TO F.LCC , ID.LC ON ERROR
*                                PRINT "CAN NOT WRITE RECORD":ID.LC:"TO" :FN.LCC
*                            END
                            CALL F.WRITE(FN.LCC,ID.LC,R.LCC)
********************************************************************************

                        END
                    END ELSE
                        IF DR.PER.LIVE EQ '0' THEN
                            ETEXT='NOT ALLOWED TO ENTER ACCOUNT VAL';CALL STORE.END.ERROR
                        END
                    END
******************AMT
                    IF DR.AMT.LIVE  NE '0' THEN
                        IF DR.AMT.LIVE NE '' THEN
                            LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>    = LC.REC(TF.LC.PROVIS.PERCENT)
*R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)
                            LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV> = LC.REC(TF.LC.PROVIS.AMOUNT)
*R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PROV>=LC.REC(TF.LC.PROVIS.AMOUNT)
                            PROV.PERC=  100*(R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT>/R.NEW(TF.DR.DOCUMENT.AMOUNT))
                            CALL EB.ROUND.AMOUNT('',PROV.PERC,'',"2")
                            LC.REC(TF.LC.PROVIS.PERCENT) =PROV.PERC
* R.LCC<TF.LC.PROVIS.PERCENT> = PROV.PERC
*******************************UPDATED BY RIHAM R15**********************
*                            WRITE  R.LCC TO F.LCC , ID.LC ON ERROR
*                                PRINT "CAN NOT WRITE RECORD":ID.LC:"TO" :FN.LCC
*                            END
                            CALL F.WRITE(FN.LCC,ID.LC,R.LCC)
***************************************************************************
                        END
                    END ELSE
                        IF DR.AMT.LIVE EQ '0' THEN
                            ETEXT='NOT ALLOWED TO ENTER ACCOUNT VAL';CALL STORE.END.ERROR
                        END
                    END
********************
                    IF DR.PER.LIVE EQ '' OR DR.AMT.LIVE EQ '' THEN
                        LC.REC(TF.LC.LOCAL.REF)<1,LCLR.TEMP.PROV.ACC>= LC.REC(TF.LC.CREDIT.PROVIS.ACC)
                        R.LCC<TF.LC.LOCAL.REF,LCLR.TEMP.PROV.ACC> = LC.REC(TF.LC.CREDIT.PROVIS.ACC)

                        LC.REC(TF.LC.CREDIT.PROVIS.ACC) = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
                        R.LCC<TF.LC.CREDIT.PROVIS.ACC>  = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>


*******************************UPDATED BY RIHAM R15**********************
*                        WRITE  R.LCC TO F.LCC , ID.LC ON ERROR
*                            PRINT "CAN NOT WRITE RECORD":ID.LC:"TO" :FN.LCC
*                        END
                        CALL F.WRITE(FN.LCC,ID.LC,R.LCC)
                    END
************************
                END ELSE
                    ETEXT='CATEGORY 3013 NOT ALLOWED VAL';CALL STORE.END.ERROR
                END
            END ELSE
                IF CATEG.CR EQ '' OR CATEG.CR NE '3013' THEN
                    IF LC.REC(TF.LC.PROVIS.PERCENT) NE '0' THEN
                        IF R.LCC<TF.LC.PROVISION> EQ 'Y' THEN
                            IF DR.PER.LIVE  EQ '0' THEN
**TEXT=LC.REC(TF.LC.PROVIS.PERCENT);CALL REM
                                LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)
**TEXT=LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>:'LC.REC':LC.REC(TF.LC.PROVIS.PERCENT);CALL REM
                                R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)
                                IF DR.AMT.LIVE EQ '0' THEN
                                    DR.PER.LIVE='0'
                                END
                                LC.REC(TF.LC.PROVIS.PERCENT) = '0'
                                R.LCC<TF.LC.PROVIS.PERCENT> = '0'
*******************************UPDATED BY RIHAM R15**********************
*                                WRITE  R.LCC TO F.LCC , ID.LC ON ERROR
*                                    PRINT "CAN NOT WRITE RECORD":ID.LC:"TO" :FN.LCC
*                                END
                                CALL F.WRITE(FN.LCC,ID.LC,R.LCC)
**********************************************************************
                            END
                        END
                    END
                END
            END
        END
************************
    END
    RETURN
END
