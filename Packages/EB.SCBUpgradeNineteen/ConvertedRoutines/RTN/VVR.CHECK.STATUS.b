* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*******************************NI7OOOOOOOOOOOO***************8
    SUBROUTINE VVR.CHECK.STATUS

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    TD   = TODAY
    COMP = ID.COMPANY
    DEPT = COMP[2]
    ID   = DEPT:99

    FN.TEL = "FBNK.TELLER.ID"
    F.TEL  = ""
    CALL OPF(FN.TEL,F.TEL)

    STAT = R.NEW(TT.TID.STATUS)
*IF (ID.NEW EQ ID) AND (STAT EQ 'CLOSE') THEN
    IF (ID.NEW[2] EQ '99' ) AND ( COMI EQ 'CLOSE' ) THEN
        ETEXT = '��� ����� ������ ���� �������'
        CALL STORE.END.ERROR
    END
    IF ( ID.NEW[2] NE '99' ) AND ( COMI EQ 'CLOSE' ) THEN
        BAL = R.NEW(TT.TID.TILL.CLOS.BAL)
        TEXT = 'CLOS.BAL=':BAL ; CALL REM
        DD  = DCOUNT(BAL,@VM)
        TEXT = 'DD=':DD ; CALL REM
        FOR I = 1 TO DD
****Updated by Nessreen Ahmed 28/2/2017******************
   ****        CALL F.READ(FN.TEL,ID.NEW,R.TEL,F.TEL,ERR)
   ****        BAL.NEW = R.TEL<TT.TID.TILL.CLOS.BAL><1,I>
****End of Update 28/2/2017******************************
            BAL.NEW = BAL<1,I>
            IF (BAL.NEW LT 0 AND BAL.NEW NE '' ) THEN
                ETEXT = '���� ������ ���� �����'
                CALL STORE.END.ERROR
                RETURN
            END ELSE
* IF (BAL.NEW EQ 0 OR BAL.NEW EQ '' ) THEN
                CUR = R.NEW(TT.TID.CURRENCY)<1,I>
                IF CUR NE '' THEN
                    R.NEW(TT.TID.TILL.BALANCE)<1,I> = 0.00
                END
            END
            IF (BAL.NEW EQ '' ) THEN
                CUR = R.NEW(TT.TID.CURRENCY)<1,I>
                IF CUR NE '' THEN
                    R.NEW(TT.TID.TILL.BALANCE)<1,I> = 0.00
                END
            END
        NEXT I
        CALL REBUILD.SCREEN
    END

    RETURN
