* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DFLT.ACC.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROJECT.IMP.STATUS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.LCY.REF

    IF MESSAGE NE 'VAL' THEN
        XX  = COMI
        CU.NO = ''
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,XX,CU.NO)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,XX,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CU.NO=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
*****************************************************************
*-- EDIT BY NESSMA 2016/10/25
        ACC.1.NO= "99400500":CU.NO:"500002"
*-- END  EDIT BY NESSMA 2016/10/25
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1> = ACC.1.NO
        R.NEW(INF.MLT.SIGN)<1,1>           = 'DEBIT'
        R.NEW(INF.MLT.TXN.CODE)<1,1>       = '78'
        R.NEW(INF.MLT.CURRENCY)<1,1>       = XX
*****************************************************************
*-- EDIT BY NESSMA 2016/10/25
        ACC.1.NO= "99400500":CU.NO:"500002"
*-- END  EDIT BY NESSMA 2016/10/03
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,2> = ACC.1.NO
        R.NEW(INF.MLT.SIGN)<1,2>           = 'DEBIT'
        R.NEW(INF.MLT.TXN.CODE)<1,2>       = '78'
        R.NEW(INF.MLT.CURRENCY)<1,2>       = XX
*-----------
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,3> = ACC.1.NO
        R.NEW(INF.MLT.SIGN)<1,3>           = 'CREDIT'
        R.NEW(INF.MLT.TXN.CODE)<1,3>       = '79'
        R.NEW(INF.MLT.CURRENCY)<1,3>       = XX
*-----------
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,4> = ACC.1.NO
        R.NEW(INF.MLT.SIGN)<1,4>           = 'CREDIT'
        R.NEW(INF.MLT.TXN.CODE)<1,4>       = '79'
        R.NEW(INF.MLT.CURRENCY)<1,4>       = XX
        CALL REBUILD.SCREEN
******************************************************************
        KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
        FN.ACC = 'F.ACCOUNT' ; F.ACC = ''
        CALL OPF(FN.ACC,F.ACC)

        T.SEL = "SELECT FBNK.ACCOUNT WITH CURRENCY EQ ":XX:" AND ONLINE.ACTUAL.BAL LT 0 AND ONLINE.ACTUAL.BAL NE '' AND ONLINE.ACTUAL.BAL NE 0 AND CATEGORY EQ 5091 BY @ID"
        TEXT = T.SEL ; CALL REM
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        TOT.AMT = 0
        TEXT = "SELECTED = ": SELECTED ; CALL REM
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E1)
                DD = I+4
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,DD> = KEY.LIST<I>
                R.NEW(INF.MLT.SIGN)<1,DD> = 'CREDIT'
                R.NEW(INF.MLT.TXN.CODE)<1,DD> = '79'
                R.NEW(INF.MLT.CURRENCY)<1,DD> = XX
                R.NEW(INF.MLT.AMOUNT.FCY)<1,DD> =  R.ACC<AC.ONLINE.ACTUAL.BAL>* -1
            NEXT I
            CALL REBUILD.SCREEN
            RETURN
        END

    END
