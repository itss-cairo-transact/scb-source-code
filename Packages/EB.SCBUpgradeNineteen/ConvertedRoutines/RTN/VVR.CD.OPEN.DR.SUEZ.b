* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CD.OPEN.DR.SUEZ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*IF INT LIQ ACCT IS EMPTY THEN DEFUALT IT WITH FEILD DRAWDOWN.ACCOUNT

    IF COMI EQ '' THEN
        COMI=R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    END ELSE
        CD.CURR= R.NEW(LD.CURRENCY)
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,ACC.CUR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.CUR=R.ITSS.ACCOUNT<AC.CURRENCY>

        IF ACC.CUR NE '' THEN
            IF CD.CURR NE ACC.CUR THEN
                ETEXT='MUST BE SAME CD CURRENCY';CALL STORE.END.ERROR
            END
            IF ACC.CUR EQ '1002' THEN
                ETEXT='1002 NOT ALLOWED';CALL STORE.END.ERROR
            END
        END
    END

    CALL REBUILD.SCREEN
    RETURN
END
