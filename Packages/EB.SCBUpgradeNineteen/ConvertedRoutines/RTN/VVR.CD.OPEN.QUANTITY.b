* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.QUANTITY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES

*IF CATEGORY CHANGED ERROR MESSAGE WILL BE DISPLAYED
    CD.TYPE=R.NEW( LD.LOCAL.REF)<1,LDLR.CD.TYPE>
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CD.TYPES':@FM:CD.TYPES.CATEGORY,CD.TYPE,MYCATEG)
F.ITSS.SCB.CD.TYPES = 'F.SCB.CD.TYPES'
FN.F.ITSS.SCB.CD.TYPES = ''
CALL OPF(F.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES)
CALL F.READ(F.ITSS.SCB.CD.TYPES,CD.TYPE,R.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES,ERROR.SCB.CD.TYPES)
MYCATEG=R.ITSS.SCB.CD.TYPES<CD.TYPES.CATEGORY>
**    IF MYCATEG NE COMI THEN ETEXT='��� ����� �������� �� �������'
IF MYCATEG NE COMI THEN ETEXT='No Change IS Allowed In Category'
    CALL REBUILD.SCREEN
    RETURN
END
