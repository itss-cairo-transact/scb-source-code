* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>443</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.COMM.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK

*TEXT = "1" ; CALL REM
    FN.CUR= 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
    T.SEL  = "";KEY.LIST = "";SELECTED = 0;ER.MSG = ''
    R.CUR.ACCT=''; R.CUR.USD=''

*********************to get data from screen and currency of acct****
    AMOUNT      = R.NEW(DOC.PRO.COMMISSION.AMOUNT)
    TYPE.CHANGE = R.NEW(DOC.PRO.COMMISSION.TYPE)
    PAY.AMT     = R.NEW(DOC.PRO.AMOUNT)
    CURR        = R.NEW(DOC.PRO.CURRENCY)
    ACCT        = R.NEW(DOC.PRO.COMM.ACCT)
    COM.AMT.OLD =  R.NEW(DOC.PRO.COMMISSION.AMOUNT)

    CURR.NO     = ACCT[9,2]
    CALL OPF( FN.CUR,F.CUR)
    T.SEL = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ " : CURR.NO
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        CURR.ACCT = KEY.LIST<1>
    END ELSE
        CURR.ACCT = 'EGP'
    END
*TEXT = "2" ; CALL REM
    CALL F.READ(FN.CUR,CURR.ACCT, R.CUR.ACCT, F.CUR ,E11)
    CUR.RATE.ACCT = R.CUR.ACCT<EB.CUR.MID.REVAL.RATE,1>
    IF CUR.RATE.ACCT EQ '' THEN
        CUR.RATE.ACCT = 1
    END
*****************************to get min rate*****************************
***UPDATED ON 19-5-2014
***PURPOSE:RATES MODIFICATION
*TEXT = "3" ; CALL REM
    COM.TYPE = COMI
    FN.COMM  = 'FBNK.FT.COMMISSION.TYPE';F.COMM='';R.COMM = '';E1=''
* FN.CUR   = 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''

    CALL OPF(FN.COMM,F.COMM)
*CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.COMM,COM.TYPE, R.COMM, F.COMM ,E1)
    RATE=R.COMM<FT4.MINIMUM.AMT><1,1>

*TEXT = "4" ; CALL REM
    CALL F.READ(FN.CUR,'USD', R.CUR.USD, F.CUR ,E11)

    RATE.USD = R.CUR.USD<EB.CUR.MID.REVAL.RATE,1>
*TEXT = "RATE.USD= ": RATE.USD ; CALL REM
*TEXT = "RATE=" : RATE ; CALL REM
    RATE.EGP = RATE * RATE.USD
    MIN.RATE = RATE.EGP / CUR.RATE.ACCT
*************************************************************************
    IF PAY.AMT NE '' AND ACCT NE '' AND COM.AMT.OLD EQ '' THEN
*TEXT = "5" ; CALL REM
** COM.TYPE = COMI
** FN.COMM  = 'FBNK.FT.COMMISSION.TYPE';F.COMM='';R.COMM = '';E1=''
** FN.CUR   = 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''

** CALL OPF(FN.COMM,F.COMM)
**CALL F.READ(FN.COMM,COM.TYPE, R.COMM, F.COMM ,E1)
        PERC  =  R.COMM<FT4.PERCENTAGE>
        AMT   = (PAY.AMT * PERC)/100
*TEXT = "6" ; CALL REM
*****************to get currency**********************************
        CALL OPF( FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR, R.CUR, F.CUR ,E11)
        CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
        IF CUR.RATE EQ '' THEN CUR.RATE = 1
        FIN.AMT = AMT * CUR.RATE
        FIN.AMT.2 = FIN.AMT / CUR.RATE.ACCT
******************************************************************
*TEXT = "6" ; CALL REM
        IF FIN.AMT.2 < MIN.RATE THEN
**            TEXT = "FIN.AMT.2  : " : FIN.AMT.2 ; CALL REM
**            TEXT = "MIN.RATE   : " : MIN.RATE  ; CALL REM
            R.NEW(DOC.PRO.COMMISSION.AMOUNT)  = DROUND(MIN.RATE,'2')
        END
        ELSE
*          TEXT = "7" ; CALL REM
**TEXT = "ELSE " ; CALL REM
**TEXT = "FIN.AMT.2  : " : FIN.AMT.2 ; CALL REM
            R.NEW(DOC.PRO.COMMISSION.AMOUNT)  = DROUND(FIN.AMT.2,'2')
        END
**        R.NEW(DOC.PRO.COMMISSION.AMOUNT)    =DROUND(FIN.AMT,'2')
        CALL REBUILD.SCREEN
    END
    RETURN
***********************************************
END
