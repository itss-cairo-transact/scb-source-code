* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>141</Rating>
*-----------------------------------------------------------------------------
** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.CU.GOVERNORATE

*IF THE RESIDENCE EQ 'EG' THE GOVERNORATE MUST BE ENTERED AND ACCORDING TO GOVERNORATE MUST SELECT THE REGION
*IF COMI IS DIFFERENT THAN THE  R.NEW OF THE SAME FIELD THEN CLEAR FIELD REGION
*HOW TO CLEAR THE DROP DOWN LIST AFTER CHOOSING ANOTHER GOVERNERATE
*BEFORE WRITE THE NAME OF THE ROUTINE IN THE VERSION YOU SHOULD FIRST CREATE PGM.FILE FOR THE ROUTINE

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    IF V$FUNCTION= 'I' THEN
        IF MESSAGE = 'VAL' THEN

*        IF COMI THEN
**************NESSREEN 23/9/2004****************
* IF R.NEW( EB.CUS.RESIDENCE) # 'EG' THEN ETEXT = 'FIELD.FOR.EG.RESIDENCE'

* END ELSE
************************************************
            IF NOT(COMI) THEN
                IF R.NEW( EB.CUS.RESIDENCE) = 'EG' THEN ETEXT = 'ENTER.GOVERNORATE'
            END
* END
* END ELSE
            IF COMI # R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.GOVERNORATE> THEN

                R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.REGION> = ''
                CALL REBUILD.SCREEN

            END
* IF PGM.VERSION = ',SCB.THIRD' THEN
*IF R.NEW(EB.CUS.RESIDENCE) # 'EG' THEN
*     R.NEW(EB.CUS.LOCAL.REF)<1,CULR.GOVERNORATE> = ''
*      R.NEW(EB.CUS.LOCAL.REF)<1,CULR.REGION> = ''
*       CALL REBUILD.SCREEN
*    END
* END
*  END
        END
    END
    RETURN
END
