* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>695</Rating>
*-----------------------------------------------------------------------------
**------------ WAEL ---------------*

    SUBROUTINE VVR.COL.DEP.CONV

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
****    IF V$FUNCTION = 'A' THEN

    IF R.NEW(COLL.APPLICATION.ID) # '' THEN

        IF R.NEW(COLL.APPLICATION.ID)[1,2] = "LD" THEN
            IF R.NEW(COLL.COLLATERAL.CODE) = "101" THEN

                LD.NUM = R.NEW(COLL.APPLICATION.ID)
                FN.DEP = 'F.LD.LOANS.AND.DEPOSITS' ; F.DEP = '' ; R.DEP = ''
                CALL OPF(FN.DEP,F.DEP)
                CALL F.READ(FN.DEP,LD.NUM, R.DEP, F.DEP, ETEXT)
                R.DEP<LD.LOCAL.REF,LDLR.COLLATERAL.ID> = ID.NEW
                IF R.DEP<LD.LOCAL.REF,LDLR.RENEW.IND> # 'YES' THEN
                    R.DEP<LD.LOCAL.REF,LDLR.RENEW.IND> = 'YES'
                    R.DEP<LD.LOCAL.REF,LDLR.RENEW.METHOD> = "1"
                END ELSE
                    IF R.DEP<LD.LOCAL.REF,LDLR.RENEW.METHOD> = '' THEN
                        R.DEP<LD.LOCAL.REF,LDLR.RENEW.METHOD> = "1"
                    END
                END

                LIN = INDEX(ID.NEW,".",2)
                COL.RIGHT = ID.NEW[1,LIN-1]
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR("COLLATERAL.RIGHT":@FM:COLL.RIGHT.LIMIT.REFERENCE,COL.RIGHT,XXX)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,COL.RIGHT,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
XXX=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.LIMIT.REFERENCE>
                IF XXX = '' THEN
                    R.DEP<LD.LOCAL.REF,LDLR.BLOCK.PURPOSE> = "BLOCKED"
                END ELSE
                    R.DEP<LD.LOCAL.REF,LDLR.BLOCK.PURPOSE> = "LIMIT"
                END
                CALL F.WRITE(FN.DEP,LD.NUM,R.DEP)
            END

            IF R.NEW(COLL.COLLATERAL.CODE) = "103" THEN

                LD.NUM = R.NEW(COLL.APPLICATION.ID)
                FN.DEP = 'F.LD.LOANS.AND.DEPOSITS' ; F.DEP = '' ; R.DEP = ''
                CALL OPF(FN.DEP,F.DEP)
                CALL F.READ(FN.DEP,LD.NUM, R.DEP, F.DEP, ETEXT)
                R.DEP<LD.LOCAL.REF,LDLR.COLLATERAL.ID> = ID.NEW
                LIN = INDEX(ID.NEW,".",2)
                COL.RIGHT = ID.NEW[1,LIN-1]
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR("COLLATERAL.RIGHT":@FM:COLL.RIGHT.LIMIT.REFERENCE,COL.RIGHT,XXX)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,COL.RIGHT,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
XXX=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.LIMIT.REFERENCE>
                IF XXX = '' THEN
                    R.DEP<LD.LOCAL.REF,LDLR.BLOCK.PURPOSE> = "BLOCKED"
                END ELSE
                    R.DEP<LD.LOCAL.REF,LDLR.BLOCK.PURPOSE> = "LIMIT"
                END
                CALL F.WRITE(FN.DEP,LD.NUM,R.DEP)
            END
        END
    END
****   END
    IF V$FUNCTION = 'A' AND  R.NEW(COLL.RECORD.STATUS)='RNAU' THEN

        LD.NUM = R.NEW(COLL.APPLICATION.ID)
        FN.DEP = 'F.LD.LOANS.AND.DEPOSITS' ; F.DEP = '' ; R.DEP = ''
        CALL OPF(FN.DEP,F.DEP)
        CALL F.READ(FN.DEP,LD.NUM, R.DEP, F.DEP, ETEXT)
        R.DEP<LD.LOCAL.REF,LDLR.COLLATERAL.ID> = ''
        R.DEP<LD.LOCAL.REF,LDLR.BLOCK.PURPOSE> = ''
        CALL F.WRITE(FN.DEP,LD.NUM,R.DEP)

    END
    RETURN
END
