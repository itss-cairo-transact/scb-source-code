* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*****NESSREEN AHMED 8/8/2012********************
*-----------------------------------------------------------------------------
* <Rating>639</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHK.END.CHQ.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT

    IF MESSAGE EQ '' THEN

        FN.CHQ.PRS ='FBNK.CHEQUE.REGISTER' ; R.CHQ.PRS = ''    ;  F.CHQ.PRS   =''
        CALL OPF(FN.CHQ.PRS,F.CHQ.PRS)

        IF COMI THEN
          *  POS.CHQ.NO = R.NEW(AC.PAY.FIRST.CHEQUE.NO)<1,AV>
            CHQ.NO.F = R.NEW(AC.PAY.FIRST.CHEQUE.NO)<1,AV>
            *TEXT = 'CHQ.ST=':CHQ.NO.F ; CALL REM
            CHQ.NO.T = COMI
            *TEXT = 'CHQ.ED=': CHQ.NO.T ; CALL REM

            IF (COMI # CHQ.NO.F) THEN
                IF (COMI > CHQ.NO.F) THEN
                    CUS.ID   =  R.NEW(AC.PAY.CUSTOMER.NO)
                    CHQ.NO   = COMI
                    FLAG     = 0
                    FOR  I =  CHQ.NO.F TO CHQ.NO.T
                        T.SEL ="SELECT FBNK.CHEQUE.REGISTER WITH CUSTOMER EQ ": CUS.ID
                        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                        IF SELECTED THEN
                            FOR TT = 1 TO SELECTED
                                CALL F.READ(FN.CHQ.PRS,KEY.LIST<TT>, R.CHQ.PRS, F.CHQ.PRS , ERR.CHQ)
                                CHQ.NOS = R.CHQ.PRS<CHEQUE.REG.CHEQUE.NOS>
*Line [ 59 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                DD = DCOUNT(CHQ.NOS,@VM)
                                FOR X = 1 TO DD
                                    CHN  = CHQ.NOS<1,X>
                                    SS = COUNT(CHN, "-")
                                    IF SS > 0 THEN
                                        ST.NO   = FIELD(CHN ,"-", 1)
                                        ED.NO   = FIELD(CHN ,"-", 2)
                                    END ELSE
                                        ST.NO   = CHN
                                        ED.NO   = CHN
                                    END
                                    IF (CHQ.NO GE ST.NO) AND (CHQ.NO LE ED.NO) THEN
                                        FLAG = "1"
                                    END
                                NEXT X
                            NEXT TT
                        END   ;***END OF IF SELECTED***
                    NEXT I
                    IF FLAG = "0" THEN
                        ETEXT = '��� ����� ������� ��� ����� ������ ' ; CALL STORE.END.ERROR
                    END
                END ELSE      ;***IF (COMI > CHQ.NO.F)***
                    ETEXT = '��� �� ���� ����� ������� ���� �� ������� ' ; CALL STORE.END.ERROR
                END
            END
            CALL REBUILD.SCREEN
        END
    END

    RETURN
END
