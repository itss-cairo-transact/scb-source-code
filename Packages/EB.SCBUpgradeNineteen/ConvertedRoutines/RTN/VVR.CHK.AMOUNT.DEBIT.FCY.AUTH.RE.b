* @ValidationCode : MjoyMDg1MTY1OTUwOkNwMTI1MjoxNjQxNzkxNjkxNTYyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 21:14:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1139</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

SUBROUTINE VVR.CHK.AMOUNT.DEBIT.FCY.AUTH.RE

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHQ.RETURN
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    $INCLUDE I_F.LIMIT
    IF MESSAGE # 'VAL' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> THEN ETEXT = '��� ����� ������' ; CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
***********************************************************
        IF COMI LT 20000 THEN ETEXT = '��� �� �� ��� ������ �� 20000'
        CALL STORE.END.ERROR
        ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEGG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*      IF CATEGG = '1202' THEN
        IF (CATEGG GE '1101' AND CATEGG LE 1450 ) THEN
            TEXT = '1202' ; CALL REM
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIMTREF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
            TEXT = 'LIMTREF=':LIMTREF ; CALL REM
            CUST = R.NEW(TT.TE.CUSTOMER.1)
            IDD = FMT(LIMTREF, "R%10")
            KEY.ID = CUST:'.':IDD
            TEXT = 'ID=':KEY.ID ; CALL REM
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,KEY.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVLM=R.ITSS.LIMIT<LI.AVAIL.AMT>
            TEXT = 'AVLM=':AVLM ; CALL REM
            TEXT = 'BAL=':BAL ; CALL REM
            EQT = AVLM + BAL - COMI
            TEXT = 'EQT=':EQT ; CALL REM
            IF EQT < 0 THEN
                ETEXT = "�� ��� �� ������ ������ �� �����"
                CALL STORE.END.ERROR
            END
        END
*       IF CATEGG # '1201' THEN
        IF NOT(CATEGG GE 1500 AND CATEGG LE 1590 ) THEN
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
**************NESSREEN 16/1/2007**********************
* FOR I=1 TO LOCK.NO
*   CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
*   LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
* NEXT I
* NET.BAL=BAL-LOCK.AMT1
            NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
***********NESSREEN  17/9/2006***********************************
**16/1/2007 IF LOCK.AMT1 > 0 THEN
            IF LOCK.AMT<1,LOCK.NO> > 0 THEN
**16/1/2007  TEXT = '������ ������� ����=':LOCK.AMT1 ; CALL REM
                TEXT = '������ ������� ����=':LOCK.AMT<1,LOCK.NO> ; CALL REM
                TEXT = '������ ������� =': NET.BAL ; CALL REM
            END
*****************************************************************
** CATEG = ACCT[11,4]
*  IF COMI GT NET.BAL AND CATEGG NE '1202' THEN
*     TEXT = 'COMI=':COMI ; CALL REM
*    TEXT = 'NET.BAL=':NET.BAL ; CALL REM
*   ETEXT = '������ �� ����'
*  CALL STORE.END.ERROR
*END
******************************************
        END
*******************************************************
    END
*****************************************
    IF MESSAGE EQ 'VAL' THEN
        FN.OFS.SOURCE ="F.OFS.SOURCE"
        F.OFS.SOURCE = ""

        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*        CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
        FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        F.OFS.IN = 0
        OFS.REC = ""
        OFS.OPERATION = "SCB.CHQ.RETURN"
        OFS.OPTIONS = "INP"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
        COMP = C$ID.COMPANY
        COM.CODE = COMP[8,2]
        OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

        OFS.TRANS.ID = ""
        OFS.MESSAGE.DATA = ""
        COMMA = ","
        AMT = COMI
*Line [ 129 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL TXTINP('THE SIGNATURE IS CORRECT', 8, 23, '1.1', @FM:'Y_N')
        IF COMI[1,1] = 'Y' THEN
***********************CHECK BALAMCE
            LOCK.AMT1 = ''
            ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
            TEXT = "ACCT.NO=":ACCT ; CALL REM
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT.NO, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
            TEXT = "BAL=":BAL ; CALL REM
*Line [ 174 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 139 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            FOR I=1 TO LOCK.NO
*Line [ 184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
                LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
            NEXT I
            NET.BAL=BAL-LOCK.AMT1
            CATEG = ACCT.NO[11,4]
            AMT.LOC = AMT
            IF AMT.LOC GT NET.BAL THEN
************************************
*Line [ 150 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                CALL TXTINP('THE AMOUNT IS NOT AVILABLE DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
*     CUST = ''
                IF COMI[1,1] = 'Y' THEN
                    ETEXT = 'AMOUNT NOT AVILABLE'
                    CALL STORE.END.ERROR


                    CURR = R.NEW(TT.TE.CURRENCY.1)
                    ACCT = R.NEW(TT.TE.ACCOUNT.1)
**    AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                    AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                    AMT.FCY = AMT
                    CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
                    NRTV = R.NEW(TT.TE.NARRATIVE.2)
                    STOP.TYPE = '20'
                    CHEQUE.TYPE = 'SCB'
                    STOP.DATE = TODAY
                END ELSE
                    ETEXT = 'AMOUNT NOT AVILABLE'
                    CALL STORE.END.ERROR

                END
            END
        END ELSE
*Line [ 175 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            CALL TXTINP('DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
            IF COMI[1,1] = 'Y' THEN
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR

***************************************
                CURR = R.NEW(TT.TE.CURRENCY.1)
                ACCT = R.NEW(TT.TE.ACCOUNT.1)
                AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                AMT.FCY = AMT
                CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
                NRTV = R.NEW(TT.TE.NARRATIVE.2)
                STOP.TYPE = '21'
                CHEQUE.TYPE = 'SCB'
                STOP.DATE = TODAY
            END ELSE
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR

            END
        END


        MUL.NO = 1

*Line [ 249 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'SCB.CHQ.RETURN':@FM:SCB.CHQR.PAYM.STOP.TYPE, ACCT,ST.TYPE)
F.ITSS.SCB.CHQ.RETURN = 'F.SCB.CHQ.RETURN'
FN.F.ITSS.SCB.CHQ.RETURN = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN,FN.F.ITSS.SCB.CHQ.RETURN)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN,ACCT,R.ITSS.SCB.CHQ.RETURN,FN.F.ITSS.SCB.CHQ.RETURN,ERROR.SCB.CHQ.RETURN)
ST.TYPE=R.ITSS.SCB.CHQ.RETURN<SCB.CHQR.PAYM.STOP.TYPE>
        TEXT = ST.TYPE ; CALL REM
        IF ST.TYPE THEN

*Line [ 205 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            MULL.NOO=DCOUNT(ST.TYPE,@VM)
            TEXT = MULL.NOO ; CALL REM
            MUL.NO = MUL.NO + MULL.NOO

        END

        IF CURR THEN
* OFS.MESSAGE.DATA =  "CUSTOMER=":CUST:COMMA
*OFS.MESSAGE.DATA :=  "CURRENCY=":CURR:COMMA
            OFS.MESSAGE.DATA :=  "PAYM.STOP.TYPE:":MUL.NO:"=":STOP.TYPE:COMMA
            OFS.MESSAGE.DATA :=  "FIRST.CHEQUE.NO:":MUL.NO:"=":CHQ.NO:COMMA
*        OFS.MESSAGE.DATA :=  "LAST.CHEQUE.NO=":CHQ.NO:COMMA
            OFS.MESSAGE.DATA :=  "CHEQUE.TYPE:":MUL.NO:"=":CHEQUE.TYPE:COMMA
            OFS.MESSAGE.DATA :=  "STOP.DATE:":MUL.NO:"=":STOP.DATE:COMMA
            IF AMT.LCY THEN
                OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.LCY:COMMA
            END ELSE
                OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.FCY:COMMA
            END
            OFS.MESSAGE.DATA :=  "BENEFICIARY:":MUL.NO:"=":NRTV


            ID.KEY.LIST= ACCT
            TEXT = "ID.KEY.LIST = ":ID.KEY.LIST ; CALL REM
            F.PATH = FN.OFS.IN
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ID.KEY.LIST:COMMA:OFS.MESSAGE.DATA
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.KEY.LIST:"-":TODAY ON ERROR
                TEXT = " ERROR "
                CALL REM
            END
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
******************************
        END
*****************************************
        COMI=AMT
        CALL REBUILD.SCREEN
    END

*****************************************

RETURN
END
