* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
**---------NESSREEN AHMED 17/11/2004---------**

    SUBROUTINE VVR.DC.ACCOUNT.CURR

*1-CHECK IF CURRENCY IS LOCAL THEN MAKE:
*  CURRENCY :NOINPUT
*  AMOUNT FCY :NOINPUT
*  EXCHANGE RATE :NOINPUT
*  AMOUNT LOCAL : EMPTY TO INPUT
*2-CHECK IF CURRENCY IS FOREIGN THEN MAKE:
*  AMOUNT LCY :NOINPUT
*  AMOUNT FCY & EXCHANGE RATE & CURRENCY EMPTY TO INPUT


*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATA.CAPTURE

   * IF V$FUNCTION = 'I' THEN
        ACC.NO = COMI
       * TEXT = ACC.NO ; CALL REM
        ACC.CURR = COMI[1,3]
       * TEXT = ACC.CURR ;CALL REM
        IF ACC.CURR = LCCY THEN
            T(DC.DC.CURRENCY)<3>='NOINPUT'
            T(DC.DC.AMOUNT.FCY)<3>='NOINPUT'
            T(DC.DC.EXCHANGE.RATE)<3>='NOINPUT'
           *T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
        END ELSE
            T(DC.DC.AMOUNT.LCY)<3> ='NOINPUT'
            T(DC.DC.AMOUNT.FCY)<3>= ''
            T(DC.DC.EXCHANGE.RATE)<3>= ''
            T(DC.DC.CURRENCY)<3>= ''
        END
        CALL REBUILD.SCREEN
   * END
    RETURN
END
