* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
** ----- 031.11.2010 NESSREEN AHMED -----
*-----------------------------------------------------------------------------
* <Rating>500</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.MAST.BIRTH

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.APP


    IF MESSAGE = 'VAL' THEN
        CUST.ID =  FIELD(ID.NEW,'.',1)
        ETEXT = ''
        IF COMI = 'MAIN' THEN
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.BIRTH.INCORP.DATE,CUST.ID,BIRTH.DATE)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
BIRTH.DATE=R.ITSS.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
            DATE.21 = ( BIRTH.DATE[ 1, 4] + 21) : BIRTH.DATE[ 5, 4]
            IF DATE.21 > TODAY THEN ETEXT = 'Main.Date.Should.Be.GE.21'
            IF R.NEW(SCB.MAST.PAYMENT.WAY)= '' THEN ETEXT = 'Should.Enter.Payment.way'
        END ELSE
            IF COMI = 'SUPPLEMENTARY FOR EXIST CARD' THEN
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CUSTOMER':@FM:EB.CUS.BIRTH.INCORP.DATE,CUST.ID,BIRTH.DATE)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
BIRTH.DATE=R.ITSS.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
                DATE.18 = ( BIRTH.DATE[ 1, 4] + 18) : BIRTH.DATE[ 5, 4]
                IF DATE.18 > TODAY THEN ETEXT = 'Main.Date.Should.Be.GE.18'
                IF R.NEW(SCB.MAST.PAYMENT.WAY) = '' AND R.NEW(SCB.MAST.PAY.ACCT.NO) = '' THEN ETEXT = 'NO.Payment.Details.For.Supp'
            END
        END
        TEXT = 'ID.NEW.BIR=':ID.NEW ; CALL REM
    END

    RETURN
END
