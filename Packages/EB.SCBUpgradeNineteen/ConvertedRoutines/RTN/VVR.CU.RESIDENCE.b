* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.CU.RESIDENCE

*IF RESIDENCE # EG THEN CLEAR THE GOVERNORATE AND REGION FIELDS

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    ETEXT = ""

*    IF COMI = 'EG' THEN
    IF COMI # R.NEW(EB.CUS.RESIDENCE) THEN
        R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.GOVERNORATE> = ''
        R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.REGION> = ''
        R.NEW( EB.CUS.TOWN.COUNTRY) = ''
        R.NEW( EB.CUS.STREET) = ''
    END
*   END ELSE
****NESSREEN  32/9/2004*********
    IF COMI # 'EG' THEN
****UPDATED BY NESSREEN AHMED 15/12/2009**********************
        IF (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.GOVERNORATE> = '' OR R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.REGION> = '' ) THEN
            R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.GOVERNORATE> = '999'
            R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.REGION> = '999'
        END
**************************************************************
    END
*******************************
    CALL REBUILD.SCREEN

*   END

    ETEXT = ""

    RETURN
END
