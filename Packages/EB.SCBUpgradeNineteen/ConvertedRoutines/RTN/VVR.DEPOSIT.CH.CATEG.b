* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>548</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.DEPOSIT.CH.CATEG

*1-CHECK IF CATEGORY IS NOT MATCHED WITH THE PERIOD OF THIS DEPOSIT
*2-CHECK IF FINAL MAT DATE IS LESS THAN ONE WEEK
*3-CHECK THE VALUE DATE AND CURRENCY MUST HAVE A VALUE
*4-CHECK THE MARKER OF EARLY.LIQ 'YES' OR 'NO' ACCORDING TO FINAL MAT DATE

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


DEFFUN GET.LD.CATEGORY( START.DATE, END.DATE, CURRENCY, LD.TYPE, LD.POSITION)

   IF V$FUNCTION = 'I' AND MESSAGE # 'VAL' AND COMI THEN

      IF R.NEW( LD.VALUE.DATE) AND R.NEW( LD.CURRENCY) THEN

         POSITION = R.NEW( LD.LOCAL.REF)< 1, LDLR.PERSONCORP>

            CATEGORY = GET.LD.CATEGORY( R.NEW( LD.VALUE.DATE), COMI, R.NEW( LD.CURRENCY), 'DEPOSIT', POSITION)
             *TEXT = CATEGORY ; CALL REM
             *TEXT = R.NEW(LD.CATEGORY) ; CALL REM
             IF PGM.VERSION = ",SCB.MOD" THEN
            IF CATEGORY # R.NEW( LD.CATEGORY) THEN
             ETEXT = "��� ������� ��� ������ �� ������"
            END
            END 
            IF PGM.VERSION = ",SCB.ELIQ" THEN
            IF COMI THEN
            MYDATE = TODAY
             CALL CDT('', MYDATE, '1W')

              IF COMI # MYDATE THEN ETEXT = "��� ������� ��� �����"
                 END
                 END

      END ELSE ETEXT = '���� �� ����� ����� ���� �������'

   END
*****************************************************************************
NEVAL=''
NEVAL = R.OLD(LD.FIN.MAT.DATE)

IF COMI THEN
     IF  COMI > NEVAL  THEN R.NEW(LD.LOCAL.REF)<1,LDLR.EARLY.LIQ.Y.N> = 'YES'

     ELSE R.NEW(LD.LOCAL.REF)<1,LDLR.EARLY.LIQ.Y.N>='NO'



     CALL REBUILD.SCREEN



END
*****************************************************************************
RETURN
END
