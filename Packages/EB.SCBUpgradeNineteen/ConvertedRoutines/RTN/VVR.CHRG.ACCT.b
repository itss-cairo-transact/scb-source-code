* @ValidationCode : MjotNTUzMjM1OTk6Q3AxMjUyOjE2NDUxMjgyMjk5ODQ6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 17 Feb 2022 12:03:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1235</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.CHRG.ACCT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LC.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT

    IF MESSAGE # 'VAL' THEN

*Line [ 36 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CHRG.NO = DCOUNT(R.NEW(TF.DR.CHARGE.CODE),@VM)

        IF COMI THEN

            FOR XX =  1 TO CHRG.NO
                R.NEW(TF.DR.CHARGE.ACCOUNT)<1,XX > = COMI

*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CURRENCY, R.NEW(TF.DR.CHARGE.ACCOUNT)<1,XX>,CHRG.CURR)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,CHRG.CURR,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CHRG.CURR=R.ITSS.ACCOUNT<@FM:AC.CURRENCY,R.NEW(TF.DR.CHARGE.ACCOUNT)<1,XX>>

                R.NEW(TF.DR.CHARGE.CURRENCY)<1,XX> = CHRG.CURR
                R.NEW(TF.DR.CHARGE.AMOUNT)<1,XX>   = ''
                R.NEW(TF.DR.AMORT.CHARGES)<1,XX>   = 'NO'
                R.NEW(TF.DR.PARTY.CHARGED)<1,XX>   = 'O'
                R.NEW(TF.DR.CHARGE.STATUS)<1,XX>   ='2'

            NEXT XX
        END
        CALL REBUILD.SCREEN
    END

RETURN
END
