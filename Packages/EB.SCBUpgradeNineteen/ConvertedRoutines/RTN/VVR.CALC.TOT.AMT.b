* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*WAGDY-----------------------------------------------------------------------------
* <Rating>189</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CALC.TOT.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH


    AMTT = 0 ;  TOTA = 0
    X= 0 ; Y= 0

*Line [ 44 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M   = DCOUNT(R.NEW(SCB.BT.OUR.REFERENCE),@VM)
    FOR K  = 1 TO NO.M
        IF R.NEW(SCB.BT.RETURN.REASON)<1,K> EQ '' THEN
            X =   R.NEW(SCB.BT.OUR.REFERENCE)<1,K>
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('BILL.REGISTER':@FM: EB.BILL.REG.AMOUNT,X,AMTT)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,X,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
AMTT=R.ITSS.BILL.REGISTER<EB.BILL.REG.AMOUNT>

            Y    += R.NEW(SCB.BT.CHRG.AMT)<1,K>
            TOTA +=AMTT
        END
    NEXT K

    CALL REBUILD.SCREEN
    R.NEW(SCB.BT.TOT.DEBIT.AMT)  = TOTA
    CALL REBUILD.SCREEN
    R.NEW(SCB.BT.TOT.CHRG.AMT)   = Y
    CALL REBUILD.SCREEN
    R.NEW(SCB.BT.TOT.CREDIT.AMT) = TOTA - Y
    CALL REBUILD.SCREEN

    IF COMI[1,3] EQ 'EGP' OR COMI[1,3] EQ 'EUR' OR COMI[1,3] EQ 'USD' THEN
*        E = "�� �� �� ���� ������ ����"  ; CALL ERR; MESSAGE =  'REPEAT'
    END
    RETURN
END
