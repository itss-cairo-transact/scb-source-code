* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.3

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES

    IF MESSAGE NE 'VAL' THEN

        DEFFUN SHIFT.DATE( )
        FN.CD.TYPE = 'F.SCB.CD.TYPES' ; F.CD.TYPE = '';R.CD.TYPE=''

        CALL OPF(FN.CD.TYPE,F.CD.TYPE)
        CALL F.READ(FN.CD.TYPE,COMI,R.CD.TYPE,F.CD.TYPE,E1)

        R.NEW(LD.CATEGORY)     = R.CD.TYPE<CD.TYPES.CATEGORY>
        R.NEW(LD.CURRENCY)     = FIELD(COMI,'-',1)
        R.NEW(LD.AMOUNT)       = FIELD(COMI,'-',2)
        R.NEW(LD.VALUE.DATE)   = TODAY
        NO.MON                 = FIELD(COMI,'-',3)
        VAL.DATE               = R.NEW(LD.VALUE.DATE)
        FIN.MAT                = SHIFT.DATE(VAL.DATE,NO.MON,UP)

        CALL CDT('EG',FIN.MAT,'-1C')

        R.NEW(LD.FIN.MAT.DATE) = FIN.MAT
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = FIN.MAT
        R.NEW(LD.INTEREST.RATE)= R.CD.TYPE<CD.TYPES.CD.RATE>
        CD.TYPE                = COMI

        YTEXT = "No Of Cd To Be ISsued For This Type "
        CALL TXTINP(YTEXT, 8, 32, "12", "A")
        R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY> = COMI
        COMI                   = CD.TYPE

        CALL REBUILD.SCREEN
    END
    RETURN
END
