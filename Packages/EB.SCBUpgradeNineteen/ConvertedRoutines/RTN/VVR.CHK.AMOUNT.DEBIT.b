* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>626</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.CHK.AMOUNT.DEBIT

*TO RE ENTERING THE AMOUNT ENTERED BY THE USER
*If the Category Not overdraft category and comi is greater than net balance an error message will be displayed

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*---------------------------------
    ETEXT = ''
    E = ''
    BAL = 0

    IF MESSAGE = '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN ETEXT = '��� ����� ������';CALL STORE.END.ERROR
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
        CALL REBUILD.SCREEN
        ETEXT = ''

        IF COMI GT 20000 THEN
            ETEXT = '��� �� �� ���� ������ ���� �� 20000'
            CALL STORE.END.ERROR
        END

        IF R.NEW(TT.TE.CURRENCY.1) = LCCY THEN
            ACCT =  R.NEW(TT.TE.ACCOUNT.1)<1,AV>
            AMT1 =  R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV>
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY, ACCT, CATEGG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEGG=R.ITSS.ACCOUNT<AC.CATEGORY>

            IF CATEGG NE 5010 THEN
                IF ( CATEGG GE 1101 AND CATEGG LE 1477 ) THEN
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR( 'ACCOUNT':@FM:AC.LIMIT.REF, ACCT, LIMTREF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIMTREF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
                    CUST = R.NEW(TT.TE.CUSTOMER.1)
                    IDD = FMT(LIMTREF, "R%10")
                    KEY.ID = CUST:'.':IDD
*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR( 'LIMIT':@FM:LI.AVAIL.AMT, KEY.ID, AVLM)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,KEY.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVLM=R.ITSS.LIMIT<LI.AVAIL.AMT>
                    EQT = AVLM + BAL - COMI
                    ETEXT = ''

                    IF EQT < 0 THEN
                        ETEXT = "�� ��� �� ������ ������ �� �����"
                        CALL STORE.END.ERROR
                    END
                END

                IF NOT( CATEGG GE 1500 AND CATEGG LE 1590) THEN
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 76 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    LOCK.NO = DCOUNT(LOCK.AMT,@VM)
                    NET.BAL = BAL-LOCK.AMT<1,LOCK.NO>

                    IF COMI GT NET.BAL THEN
                        IF NOT (CATEGG GE 1101 AND CATEGG LE 1477) AND NOT(CATEGG EQ 1481) THEN
                            ETEXT = '������ �� ����'
                            CALL STORE.END.ERROR
                        END
                    END
                END ;*END OF IF CATEG
            END     ;*END OF IF CATEG NE 5010
        END         ;**IF CURR = LCCY
    END   ;**IF MESSAGE = VAL

    IF MESSAGE = 'VAL' THEN
        IF COMI GT 20000 THEN
            ETEXT = '��� �� �� ���� ������ ���� �� 20000'
            CALL STORE.END.ERROR
        END
    END
*-----------------------------------------------------------
    RETURN
END
