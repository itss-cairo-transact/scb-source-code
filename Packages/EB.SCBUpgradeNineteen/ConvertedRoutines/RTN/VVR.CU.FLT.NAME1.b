* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
** ----- 18/7/2019 NESSREEN AHMED -----
    SUBROUTINE VVR.CU.FLT.NAME1

*TO DEFAULT NAME.1 BY SHORT.NAME

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    ETEXT = ""

    IF COMI THEN
        R.NEW(EB.CUS.NAME.1) = COMI
*  R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.REGION> = ''
    END

    CALL REBUILD.SCREEN
******
    IF MESSAGE = 'VAL' THEN
        WS.NATION           = R.NEW(EB.CUS.NATIONALITY)
        WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
        WS.CUSTOMER.STATUS  = R.NEW(EB.CUS.CUSTOMER.STATUS)
        WS.CONT.DATE        = R.NEW(EB.CUS.CONTACT.DATE)
        WS.PASSPORT.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER>
        WS.NATIONAL.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO>
        WS.PLACE.ISSUE      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE>
        WS.REG.NO           = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.COM.REG.NO>
        WS.ID.TYPE          = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE>
*  WS.NEW.SECTOR.NEW   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
        IF WS.NATION EQ 'EG' THEN
            IF WS.NATIONAL.ID EQ '' THEN
                ETEXT = '��� ����� ����� ������'
                CALL STORE.END.ERROR
            END
        END ELSE
            IF WS.PASSPORT.ID EQ '' THEN
                ETEXT = '��� ����� ��� �������'
                CALL STORE.END.ERROR
            END
        END
    END

*****

    RETURN
END
