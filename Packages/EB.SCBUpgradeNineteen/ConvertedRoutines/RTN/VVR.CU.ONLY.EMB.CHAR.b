* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------

** ----- 7/8/2005 NESSREEN AHMED -----
    SUBROUTINE VVR.CU.ONLY.EMB.CHAR

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*THE ROUTINE ACCEPT ONLY ENGLISH CHARACTERS AND DELETE EXTRA SPACES

    IF COMI THEN
        COMI.LEN = LEN( COMI)
        FOR I = 1 TO COMI.LEN
            CHARACTER =  COMI[ I, 1]
            IF NOT( CHARACTER >= CHAR( 65) AND CHARACTER <= CHAR( 97) OR CHARACTER = CHAR( 32) OR (CHARACTER >= CHAR( 97) AND CHARACTER <= CHAR( 122))) THEN ETEXT = 'ONLY.ENGLISH.LETTERS' ; EXIT
        NEXT I
        IF NOT( ETEXT) THEN COMI = TRIM( COMI)

    END

    RETURN
END
