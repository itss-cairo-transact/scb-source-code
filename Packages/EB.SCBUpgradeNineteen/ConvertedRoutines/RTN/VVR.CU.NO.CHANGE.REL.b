* @ValidationCode : MjotMTMxMjMwMzE5NDpDcDEyNTI6MTY0NTEyODgyMDM2MzpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 12:13:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
******** DINA_SCB *********

SUBROUTINE VVR.CU.NO.CHANGE.REL
*inforce the user to enter relation suitable with the chosen status
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*             TEXT ="EEEEEEE" ; CALL REM
    IF V$FUNCTION = 'I' THEN
*        CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.CU.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,STAT1)

*    TEXT ="111111111" ; CALL REM
*       EXIF NOT(ETEXT) THEN
*            TEXT ="ETEXT":ETEXT ; CALL REM
*            LOCATE R.NEW(EB.CUS.CUSTOMER.STATUS) IN STAT1<1,1> SETTING YY THEN

*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.REL,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,REL1)
        F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
        FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
        CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
        CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,REL1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
        REL1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.REL,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
*  TEXT = 'REL1=':REL1 ; CALL REM
        IF REL1 THEN
*Line [ 45 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CC = DCOUNT(REL1, @VM)
            FOR J = 1 TO CC
                ENQ<2,J> = "@ID"
                ENQ<3,J> = "EQ"
                ENQ<4,J> = REL1<1,J>
            NEXT J
        END
*TEXT = REL1 ; CALL REM
*TEXT = REL1<2> ; CALL REM
*                IF COMI THEN LOCATE COMI IN REL1<1,YY,1> SETTING M ELSE ETEXT = 'THIS.RELATION.NOT.ALLWED'
*               IF COMI THEN LOCATE COMI IN REL1<1,YY,1> SETTING M ELSE ETEXT = 'THIS.RELATION.NOT.ALLWED'
*          END
*     END
    END

RETURN
END
