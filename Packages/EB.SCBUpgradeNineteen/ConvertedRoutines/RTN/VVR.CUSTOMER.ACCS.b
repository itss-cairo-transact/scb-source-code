* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
**----------------MAI FEB 2019-------------------
    SUBROUTINE VVR.CUSTOMER.ACCS
** -------------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.MOBILE
*----------------------------------------
    GOSUB INITIALISE
*----------------------------------------
INITIALISE:
    CURR = 'EGP'
    CU.ACC = COMI
    CU.ID  = ID.NEW
    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = '' ;
    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN
            CALL OPF(FN.ACCOUNT,F.ACCOUNT)
            CALL F.READ(FN.ACCOUNT,CU.ACC, R.ACCOUNT, F.ACCOUNT ,E1)
            ACC.CUR = R.ACCOUNT<AC.CURRENCY>
            ACC.CUST = R.ACCOUNT<AC.CUSTOMER>
            CATEG = R.ACCOUNT<AC.CATEGORY>
**EDIT BY MAI SAAD 2021/06/21
*            IF CATEG EQ '1001' OR CATEG EQ '1002' OR CATEG EQ '1006' OR CATEG[1,2] EQ '65'  THEN
            IF CATEG EQ '1001' OR CATEG EQ '1002' OR CATEG EQ '1006' OR CATEG EQ '1003' OR CATEG[1,2] EQ '65'  THEN
**END OF UPDATE
                IF ACC.CUR NE CURR THEN
                    ETEXT='EGP CURRENCY ONLY';CALL STORE.END.ERROR
                END

                IF ACC.CUST NE CU.ID THEN
                    ETEXT='MUST BE CUSTOMER ACCOUNTS ONLY';CALL STORE.END.ERROR
                END
            END ELSE
                ETEXT='THIS CATEGORY IS NOT VALID';CALL STORE.END.ERROR
            END
        END
    END
    IS.WAL   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.IS.EWALLET>
    WALL.ACC = COMI

    IF IS.WAL EQ 'YES'AND WALL.ACC EQ '' THEN
        ETEXT = 'MUST ENTER ACCOUNT FOR EWALLET';
    END
    IF IS.WAL EQ 'NO' AND WALL.ACC NE '' THEN
        ETEXT = 'to remove WALLET, must remove associated account';
    END

    RETURN
***------------------------------***
    RETURN
END
