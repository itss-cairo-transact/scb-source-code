* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>828</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.COLLCITION.DRFT

* TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* FOR THAT CUSTOMER
* TO CHECK THAT THE CHEQUE IS NOT STOPPPED

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    T.SEL    = "SSELECT FBNK.FUNDS.TRANSFER WITH CREDIT.ACCT.NO EQ ": R.NEW(TT.TE.ACCOUNT.2) :" AND CHEQUE.NUMBER EQ ": COMI

    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED
    TEXT = KEY.LIST

    FN.FT ='FBNK.FUNDS.TRANSFER' ;R.FT='';F.FT=''
    CALL OPF(FN.FT,F.FT)
    CALL F.READ(FN.FT,KEY.LIST, R.FT, F.FT,E)
    R.NEW(TT.TE.AMOUNT.LOCAL.1) = ""
    R.NEW(TT.TE.LOCAL.REF)<TTLR.ISSUE.DATE> = ""
    R.NEW(TT.TE.CUSTOMER.1) = ""
    CALL REBUILD.SCREEN

    R.NEW(TT.TE.AMOUNT.LOCAL.1) = R.FT<FT.DEBIT.AMOUNT>
    R.NEW(TT.TE.LOCAL.REF)<TTLR.ISSUE.DATE> = R.FT<FT.CREDIT.VALUE.DATE>
    R.NEW(TT.TE.CUSTOMER.1) = R.FT<FT.BEN.CUSTOMER>
    CALL REBUILD.SCREEN

*IF V$FUNCTION = 'I' THEN

    IF COMI  THEN
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = 'DRFT.':R.NEW(TT.TE.ACCOUNT.2):'-':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED, CHQ.ID, REPRESENT)
    CHQ.ID = 'DRFT.':R.NEW(TT.TE.ACCOUNT.2):'.':COMI
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED, CHQ.ID, REPRESENT)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
REPRESENT=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.PRESENTED>
****END OF UPDATE 7/3/2016*****************************
*ETEXT = ''
        IF REPRESENT THEN ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
        ACCT.NO = R.NEW(TT.TE.ACCOUNT.2)
        CHQ='DRFT':'.':ACCT.NO:'...'
        CHQ.CUS = 'DRFT':'.':ACCT.NO
        T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED EQ '0' THEN
*  TEXT = 'NOT ISSUED' ; CALL REM
            ETEXT='�� ���� ����� ����� ���� ������ &': CHQ
            CALL STORE.END.ERROR
        END ELSE
*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CHEQUE.REGISTER':@FM:CHEQUE.REG.CHEQUE.NOS, CHQ.CUS, CHQ.NOS)
F.ITSS.CHEQUE.REGISTER = 'FBNK.CHEQUE.REGISTER'
FN.F.ITSS.CHEQUE.REGISTER = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER,FN.F.ITSS.CHEQUE.REGISTER)
CALL F.READ(F.ITSS.CHEQUE.REGISTER,CHQ.CUS,R.ITSS.CHEQUE.REGISTER,FN.F.ITSS.CHEQUE.REGISTER,ERROR.CHEQUE.REGISTER)
CHQ.NOS=R.ITSS.CHEQUE.REGISTER<CHEQUE.REG.CHEQUE.NOS>
            LF   = FIELD (CHQ.NOS, "-", 1)
            RH   = FIELD (CHQ.NOS, "-", 2)
            IF COMI GT RH OR COMI LT LF THEN
                ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
*TEXT = 'NOT.INVOLVED.IN.CHQ' ; CALL REM
            END
            FN.PAYMENT.STOP = 'F.PAYMENT.STOP' ; F.PAYMENT.STOP ='' ; R.PAYMENT.STOP = '' ; E= ''
            CALL OPF(FN.PAYMEN.STOP,F.PAYMENT.STOP)
            CALL F.READ(FN.PAYMENT.STOP,ACCT.NO, R.PAYMENT.STOP, F.PAYMENT.STOP , E)
            FIRST.NO = R.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
            LAST.NO = R.PAYMENT.STOP<AC.PAY.LAST.CHEQUE.NO>
            CHQ.TYPE = R.PAYMENT.STOP<AC.PAY.CHEQUE.TYPE>
            STOP.TYPE = R.PAYMENT.STOP<AC.PAY.PAYM.STOP.TYPE>

*Line [ 107 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT (STOP.TYPE,@VM)
            FOR X = 1 TO DD
                IF CHQ.TYPE<1,X> = 'DRFT' THEN
                    IF LAST.NO<1,X> = "" THEN
                        IF COMI = FIRST.NO<1,X> THEN ETEXT = '��� ����� �����' ; CALL STORE.END.ERROR
                    END ELSE
                        FOR I = FIRST.NO<1,X> TO LAST.NO<1,X>
                            IF COMI = I THEN ETEXT = '��� ����� �����'; CALL STORE.END.ERROR
                        NEXT I
                    END
                END
            NEXT X
            CALL F.RELEASE(FN.PAYMENT.STOP,ACCT.NO,F.PAYMENT.STOP)
        END
    END


    RETURN
END
