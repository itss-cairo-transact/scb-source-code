* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
    SUBROUTINE VVR.CRT.CHQ.MM

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
****************MAIN PROGRAM************************
MAIN.PROG:
*   IF MESSAGE NE 'VAL' THEN
    IF COMI = "0" THEN
        ETEXT = "��� ����� ��� ����" ;CALL STORE.END.ERROR
    END
**------------------------------------

    GOSUB INTIAL
    GOSUB CRTF.CHQ.ISSUE
    IF COMI THEN
        GOSUB CHQ.ISS.EMPTY
        GOSUB CHQ.NO.EXIST
        IF ER = 1 THEN
            ER = ''
        END ELSE
            GOSUB CHQ.STOP
            IF CURR = "1" THEN
                RETURN
            END ELSE
                GOSUB CRTF.CHQ.ISSUE
                IF SELECTED THEN
                    RETURN
                END
            END
        END
    END
    R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = COMI
    CALL REBUILD.SCREEN

    RETURN
*END
    R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = COMI
    CALL REBUILD.SCREEN
    RETURN
*************************************************************************************************
INTIAL:
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT =''

    RETURN

*********************************************************************************************************

CHQ.ISS.EMPTY:

    FN.CHQ.REG = 'FBNK.CHEQUE.REGISTER' ; F.CHQ.REG = ''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)

    CHQ.ID = 'SCB.':R.NEW(FT.DEBIT.ACCT.NO)
    TEXT = 'CHQ.ID=':CHQ.ID ; CALL REM
    T.SEL = "SELECT FBNK.CHEQUE.REGISTER WITH @ID EQ ":CHQ.ID
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
        CALL F.READ(FN.CHQ.REG,KEY.LIST,R.CHQ.REG,F.CHQ.REG,E1)
        CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>
        CHQ.STOP    = R.CHQ.REG<CHEQUE.REG.STOPPED.CHQS>
    END ELSE
***  ETEXT = '��� ����� �� ������ ��� �����' ;CALL STORE.END.ERROR
    END
    RETURN
***************************************************************************
CHQ.NO.EXIST:
*TEXT = CHQ.NOS ; CALL REM
    IF LEN(CHQ.NOS) EQ 1 THEN
        LF = CHQ.NOS
*TEXT = LF ; CALL REM
        IF COMI NE LF THEN
            ER = 1
***  ETEXT = '����� ��� (&) �� ������ ���': COMI ;CALL STORE.END.ERROR
        END
    END ELSE
        LF   = FIELD (CHQ.NOS, "-", 1)
        RH   = FIELD (CHQ.NOS, "-", 2)
        IF COMI GT RH AND COMI LT LF THEN
            ER = 1
***    ETEXT = '����� ��� (&) �� ������ ���': COMI ;CALL STORE.END.ERROR
        END
    END
*IF COMI GT RH OR COMI LT LF THEN
*    ER = 1
*    ETEXT = '����� ��� (&) �� ������ ���': COMI ;CALL STORE.END.ERROR
*END

    RETURN
***************************************************************************
CHQ.STOP:
*TEXT = 'CHQ.STOP' ;  CALL REM
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID.STOP = R.NEW(FT.DEBIT.ACCT.NO):"*": COMI
*TEXT = "CHQ.ID.STOP =":CHQ.ID.STOP ; CALL REM
****CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(FT.DEBIT.ACCT.NO),CHQ.TYPE)
F.ITSS.CHEQUE.TYPE.ACCOUNT = 'FBNK.CHEQUE.TYPE.ACCOUNT'
FN.F.ITSS.CHEQUE.TYPE.ACCOUNT = ''
CALL OPF(F.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT)
CALL F.READ(F.ITSS.CHEQUE.TYPE.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT,ERROR.CHEQUE.TYPE.ACCOUNT)
CHQ.TYPE=R.ITSS.CHEQUE.TYPE.ACCOUNT<CHQ.TYP.CHEQUE.TYPE>
    TY = CHQ.TYPE
*Line [ 139 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT (TY,@VM)
    FOR X = 1 TO DD
        CHQ.ID = CHQ.TYPE<1,X>:'.':R.NEW(FT.DEBIT.ACCT.NO):'.':COMI
*Line [ 149 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
CURR=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.STOPPED>
*TEXT = "CURR = ":CURR ; CALL REM
    IF CURR THEN
        ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
        GOSUB END.PROG
    END
   NEXT X
****END OF UPDATE 7/3/2016*****************************
**************************************************************************
* COUNTS1 =DCOUNT(CHQ.STOP,VM)
* TEXT = "COUNTS1": COUNTS1 ; CALL REM
    FOR I = 1 TO COUNTS1
        CHQ.LEN = LEN(CHQ.STOP<1,I>)
* TEXT=CHQ.LEN:'LEN';CALL REM

* IF CHQ.LEN THEN
*    GOSUB CHECK.DASH
*   TEXT='NESS1';CALL REM
*TEXT = "FIRST.NO": FIRST.NO ; CALL REM
*TEXT = "LAST.NO": LAST.NO ; CALL REM
*  END

* IF COMI LE LAST.NO AND COMI GE FIRST.NO THEN
*    ETEXT = ''
*   TEXT='NESS2';CALL REM
*  ETEXT = '����� ��� (&)�����' ; CALL STORE.END.ERROR
*    GOSUB END.PROG
*    ERS = 1
* I = COUNTS1
*                              END ELSE
*                              *TEXT='NESS3';CALL REM
*                              GOSUB CRTF.CHQ.ISSUE
*                              END
*      FIRST.NO = ''
*    LAST.NO = ''
    NEXT I

    RETURN
***************************************************************************
CRTF.CHQ.ISSUE:
*TEXT = 'CRT.CHQ.ISSUE' ;  CALL REM

    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.DEBIT.ACCT.NO):".": COMI
*TEXT = "SCB.CHQ.ID": SCB.CHQ.ID ; CALL REM
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ":SCB.CHQ.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*TEXT = "SLE": SELECTED ; CALL REM

    IF SELECTED THEN
*TEXT  = "SELECTED": SELECTED ; CALL REM
        ETEXT = '��� ������ ���� ����� �����' ;CALL STORE.END.ERROR

        RETURN

    END
    CALL REBUILD.SCREEN
    RETURN
***************************************************************************
*CHECK.DASH:
*TEXT = "CHECK DASH" ; CALL REM
    FOR J =1 TO CHQ.LEN
* TEXT = "CHQ.STOP":CHQ.STOP<1,I>
        A<J> = CHQ.STOP<1,I>[J,1]
* TEXT = "CHQ.STOP":CHQ.STOP<1,I>[J,1]
* TEXT = "A<J>":A<J>

        IF A<J> EQ '-' THEN
            FIRST.NO  = CHQ.STOP<1,I>[1,J-1]
            LAST.NO   = CHQ.STOP<1,I>[J+1,CHQ.LEN-J]
            X = 1
*       TEXT = "X1" : X ; CALL REM
            J = CHQ.LEN
        END
    NEXT J
*TEXT = "X2" : X ; CALL REM
    IF X = 0 THEN
        FIRST.NO  = CHQ.STOP<1,I>
        LAST.NO   = CHQ.STOP<1,I>
    END
    X = 0
    A = ''
    RETURN
***************************************************************************
END.PROG:
    ETEXT = ''
    R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = COMI
    CALL REBUILD.SCREEN
    RETURN

END
