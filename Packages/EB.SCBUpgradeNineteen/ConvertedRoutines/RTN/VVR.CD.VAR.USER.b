* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
****ABEER AS OF 2017-07-27**
    SUBROUTINE VVR.CD.VAR.USER

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI THEN
*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CUSTOMER.ID,ID.NEW,CUS.ID)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
CUS.ID=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('USER':@FM:EB.USE.SIGN.ON.NAME,COMI,USER.REC)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,COMI,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USER.REC=R.ITSS.USER<EB.USE.SIGN.ON.NAME>
        IF CUS.ID EQ '' THEN
            INVEST.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.INVESTED.AMOUNT>
            *TEXT=INVEST.AMT;CALL REM
            IF INVEST.AMT NE 'FRESH FUND' THEN
                E='FRESH FUND ONLY';CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
**************************************************************************
        IF CUS.ID NE '' THEN
            IF R.NEW(LD.VALUE.DATE) LT '20170727' THEN
                E ='Not allowed to modify field Before 27-7-2017 ' ; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                INVEST.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.INVESTED.AMOUNT>
                IF INVEST.AMT NE 'FRESH FUND' THEN
                    E ='FRESH FUND ONLY' ; CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END
    END ELSE
        INVEST.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.INVESTED.AMOUNT>
        IF INVEST.AMT EQ 'FRESH FUND' THEN
            E = 'MUST ENTER EMPLOYEE NO';CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
    RETURN
END
