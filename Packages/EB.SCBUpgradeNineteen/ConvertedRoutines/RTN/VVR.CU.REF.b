* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>96</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.REF
*if the relation reference is entered then the rel.code and customer must be entered to
*if customer status is minor or handicapped or under guardianship then the relation must
*be entered
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG

    IF V$FUNCTION = 'I' THEN
        STAT = R.NEW(EB.CUS.CUSTOMER.STATUS)
        IF COMI THEN
            IF NOT(R.NEW(EB.CUS.RELATION.CODE)) OR NOT(R.NEW(EB.CUS.REL.CUSTOMER)) THEN ETEXT='Missing.Rel.Code.or.Rel.Cus'
        END ELSE
*IF STAT = '15' OR STAT = '22' OR STAT = '23' AND NOT(COMI) THEN ETEXT = 'MISSING.REF'
*CALL DBR ('SCB.VER.IND.SEC.LEG':FM:VISL.RESTRICT.STATUS,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,R.STATUS)

*LOCATE STAT IN R.STATUS<1,1> SETTING B THEN ETEXT = 'MESSING.REFRENC'
        END
    END
************
    IF MESSAGE = 'VAL' THEN

        IF NOT(COMI) THEN
          *  IF R.NEW(EB.CUS.RELATION.CODE,1)= '42' THEN ETEXT='���� ����� �������'
        END
    END

*************
    RETURN
END
