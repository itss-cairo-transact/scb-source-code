* @ValidationCode : MjoxMjg3ODk3NDUyOkNwMTI1MjoxNjQ1MTU2MDEzNjI1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 17 Feb 2022 19:46:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
SUBROUTINE VVR.CHQ.CANCEL.CRT.2

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.REGISTER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*-------------------------------------------------
    GOSUB MAIN.PROG
RETURN
*-------------------------------------------------
MAIN.PROG:
*---------
    IF MESSAGE NE 'VAL' THEN
        GOSUB INTIAL
        IF NOT(COMI) THEN
            RETURN
        END
        GOSUB CHQ.STOP.SUB
        IF  CURR = "1" THEN
            RETURN
        END ELSE
            GOSUB CRF.CHQ.ISSUE
            IF NOT(SELECTED) THEN
                RETURN
            END
        END
    END
RETURN
*************************************************************************************************
INTIAL:
*-------
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT =''

RETURN
*********************************************************************************************************
CHQ.STOP.SUB:
*-------------
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
**** CHQ.ID.STOP = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:"*": COMI
**** CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>,CHQ.TYPE)
    F.ITSS.CHEQUE.TYPE.ACCOUNT = 'FBNK.CHEQUE.TYPE.ACCOUNT'
    FN.F.ITSS.CHEQUE.TYPE.ACCOUNT = ''
    CALL OPF(F.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT)
    CALL F.READ(F.ITSS.CHEQUE.TYPE.ACCOUNT,CHQ.TYPE,R.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT,ERROR.CHEQUE.TYPE.ACCOUNT)
    CHQ.TYPE=R.ITSS.CHEQUE.TYPE.ACCOUNT<@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>>
    TY = CHQ.TYPE
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT (TY,@VM)
    FOR X = 1 TO DD
        CHQ.ID = CHQ.TYPE<1,X>:'.':R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:'.':COMI
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
        F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
        FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
        CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
        CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
        CURR=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.STOPPED>
        IF CURR THEN
            ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
        END
    NEXT X
****END OF UPDATE 6/3/2016*****************************

RETURN
***********************************************************************
CRF.CHQ.ISSUE:
*--------------
    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)
    SCB.CHQ.ID = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:".": COMI
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

****UPDATED BY NESSREEN AHMED 14/12/2009***************************
    TEXT = 'ACC.NO=':R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO> ; CALL REM
    T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH DEBIT.ACCT EQ ":R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO> : " AND ( OLD.CHEQUE.NO EQ " :COMI :" OR CHEQ.NO EQ ":COMI:")"
***************************************************************************
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    TEXT = "SLE": SELECTED ; CALL REM

    IF NOT(SELECTED) THEN
        ETEXT = '�� ���� ��� ����� ����� ���� �����' ;CALL STORE.END.ERROR
        RETURN
    END ELSE
        GOSUB CRF.CHQ.PAID
    END
RETURN
***************************************************************************
CRF.CHQ.PAID:
*------------
    CALL F.READ(FN.CHQ.PRESENT,KEY.LIST,R.CHQ.PRESENT,F.CHQ.PRESENT,E1)

    CHQ.STAT     = R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS>
    CHQ.TRNS.PAY = R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT>
    CHQ.PAY.DATE = R.CHQ.PRESENT<DR.CHQ.PAY.DATE>
    CHQ.PAY.BRN  = R.CHQ.PRESENT<DR.CHQ.PAY.BRN>
    CHQ.AMT      = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
    CHQ.REC.DATE = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
    CHQ.BEN      = R.CHQ.PRESENT<DR.CHQ.BEN>

    IF CHQ.STAT EQ 4 THEN
        ETEXT = "����� �� ���� ������ ��� ������" ; CALL STORE.END.ERROR
    END

    IF CHQ.STAT = 2 AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
        ETEXT = "����� �� ���� ������ ��� ����" ; CALL STORE.END.ERROR
    END ELSE
        IF CHQ.STAT = 3 AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
            ETEXT = "����� ��� ������" ; CALL STORE.END.ERROR
        END ELSE
            R.NEW(FT.DEBIT.CURRENCY)  = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
            R.NEW(FT.DEBIT.ACCT.NO)   = R.CHQ.PRESENT<DR.CHQ.NOS.ACCT>
            R.NEW(FT.DEBIT.AMOUNT)    = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
            R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE>       = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
            R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>         = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>

            R.NEW(FT.CREDIT.CUSTOMER)    = R.NEW(FT.LOCAL.REF)<1,FTLR.DRAWER>
            R.NEW(FT.CREDIT.CURRENCY)    = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
            R.NEW(FT.CREDIT.ACCT.NO)     = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>
            R.NEW(FT.PROFIT.CENTRE.DEPT) = ''
            CALL REBUILD.SCREEN
        END
        RETURN
    END
