* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
****ABEER ****
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*TO DEFAULT FIELDS CATEGORY,CURRENCY,AMOUNT,VALUE DATE,FIN.MAT.DATE,INTEREST RATE FROM CD TYPE CHOSEN BY USER
*AND PROMPT THE USER TO ENTER THE NO OF CDS TO BE CREADTED FROM THE CHOSEN TYPE

    IF MESSAGE NE 'VAL' THEN
*******************************************
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.CD.TYPES':@FM:CD.TYPES.CATEGORY,COMI,CAT.TYP)
F.ITSS.SCB.CD.TYPES = 'F.SCB.CD.TYPES'
FN.F.ITSS.SCB.CD.TYPES = ''
CALL OPF(F.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES)
CALL F.READ(F.ITSS.SCB.CD.TYPES,COMI,R.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES,ERROR.SCB.CD.TYPES)
CAT.TYP=R.ITSS.SCB.CD.TYPES<CD.TYPES.CATEGORY>
        COR.TY = FIELD(COMI,'-',5)
        CUS.ID = R.NEW(LD.CUSTOMER.ID)
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.ID,CUS.SEC)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>

        IF CAT.TYP EQ '21025' THEN
            IF CUS.SEC EQ '1100' OR CUS.SEC EQ '1200' OR CUS.SEC EQ '1300' OR CUS.SEC EQ '1400' OR CUS.SEC EQ '2000' THEN
                IF COR.TY EQ 'COR' THEN
                    ETEXT = 'CORPORATE NOT ALLOWED ' ; CALL STORE.END.ERROR
                    RETURN
                END
            END
            IF CUS.SEC NE '1100' AND CUS.SEC NE '1200' AND CUS.SEC NE '1300' AND CUS.SEC NE '1400' AND CUS.SEC NE '2000' THEN
                IF COR.TY NE 'COR' THEN
                    ETEXT = 'MUST BE CORPORATE' ; CALL STORE.END.ERROR
                    RETURN
                END
            END
        END
        IF CAT.TYP NE '21025' THEN
            IF CUS.SEC NE '1100' AND CUS.SEC NE '1200' AND CUS.SEC NE '1300' AND CUS.SEC NE '1400' AND CUS.SEC NE '2000' THEN
                ETEXT = 'MUST BE PRIVATE' ; CALL STORE.END.ERROR
                RETURN
            END
        END

*********HYTHAM******20081228***************

        HH = FIELD(COMI,'-',4)

        IF HH EQ '60M' THEN

            ETEXT = '��� ����� ���������' ; CALL STORE.END.ERROR
        END
********HYTHAM******20081228***************


        DEFFUN SHIFT.DATE( )
        FN.CD.TYPE = 'F.SCB.CD.TYPES' ; F.CD.TYPE = '';R.CD.TYPE=''

        CALL OPF(FN.CD.TYPE,F.CD.TYPE)
        CALL F.READ(FN.CD.TYPE,COMI,R.CD.TYPE,F.CD.TYPE,E1)

        R.NEW(LD.CATEGORY)=R.CD.TYPE<CD.TYPES.CATEGORY>
        R.NEW(LD.CURRENCY)=FIELD(COMI,'-',1)
        R.NEW(LD.AMOUNT)=FIELD(COMI,'-',2)


***-----------------HYTHAM 20100725--------------------------****

        R.NEW(LD.DRAWDOWN.NET.AMT)  = FIELD(COMI,'-',2)
        R.NEW(LD.DRAWDOWN.ISSUE.PRC)= FIELD(COMI,'-',2)
        R.NEW(LD.ISSUE.PL.AMOUNT)   = '0.00'

***-----------------HYTHAM 20100725--------------------------****

        R.NEW(LD.VALUE.DATE)=TODAY

        NO.MON=FIELD(COMI,'-',4)
        VAL.DATE=R.NEW(LD.VALUE.DATE)
        FIN.MAT=SHIFT.DATE(VAL.DATE,NO.MON,UP)
**-----------------------------------------
*******MAI 20200608
        INT.MON =FIELD(COMI,'-',3)

        IF COR.TY EQ 'COR' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>='COR'
        END  ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> = INT.MON
        END
****** END OF MODOFOCATION

***-------------HYTHAM----- 20111120
**MODIFIED ON 9-7-2014 ON TEST3
        IF R.NEW(LD.CATEGORY) EQ '21025' OR R.NEW(LD.CATEGORY) EQ '21020' OR R.NEW(LD.CATEGORY) EQ '21021' OR R.NEW(LD.CATEGORY) EQ '21017' OR R.NEW(LD.CATEGORY) EQ '21018' THEN
            CALL CDT('EG',FIN.MAT,'-1C')
        END
***END OF MODIFICATION

***-------------HYTHAM----- 20111120

        R.NEW(LD.FIN.MAT.DATE)=FIN.MAT
        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=FIN.MAT
        R.NEW(LD.INTEREST.RATE)=R.CD.TYPE<CD.TYPES.CD.RATE>
        CD.TYPE=COMI
**************************
**        YTEXT = "��� �������� ������� ������� ���� �����  "
        YTEXT="No Of Cd To Be ISsued For This Type "
        CALL TXTINP(YTEXT, 8, 32, "12", "A")
        R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>=COMI
**************************
        COMI=CD.TYPE
        CALL REBUILD.SCREEN
    END
    RETURN
END
