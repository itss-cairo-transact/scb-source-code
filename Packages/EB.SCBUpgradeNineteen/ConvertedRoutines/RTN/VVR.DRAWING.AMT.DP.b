* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE VVR.DRAWING.AMT.DP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON

    IF MESSAGE EQ '' THEN
        IF COMI THEN
            **TEXT = "HHHHHHHHHH"   ; CALL REM
            FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
            CALL OPF(FN.LCC,F.LCC)
            KEY.LIST="" ; SELECTED="" ; ER.MSG=""
            ID.LC = ID.NEW[1,12]
***    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH @ID EQ  " : ID.LC
***    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            CALL F.READ(FN.LCC,ID.LC,R.LCC,F.LCC,READ.ERR)
TEXT = LC.REC(TF.LC.PROVIS.PERCENT) :'LC REC PERC GLOBUS';CALL REM
TEXT = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>:'LC REC PERC LOCAL';CALL REM

   IF  LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>  EQ ''  THEN
   LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT> = LC.REC(TF.LC.PROVIS.PERCENT)

  END

TEXT= LC.REC(TF.LC.PROVIS.AMOUNT):'LC REC PROV AMT GLOBUS';CALL REM
TEXT=LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV>:'LC REC LOCAL AMT';CALL REM

            IF  LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV>  EQ ''  THEN
  LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV> = LC.REC(TF.LC.PROVIS.AMOUNT)
            END
            IF COMI NE '' THEN
                R.NEW(TF.DR.PROV.AMT.REL)    = COMI
                R.NEW(TF.DR.PROV.REL.LC.CCY) = COMI
                R.NEW(TF.DR.PROV.REL.DOC.CCY)= COMI
TEXT=COMI:'COM';CALL REM
                LC.REC(TF.LC.PROVIS.PERCENT) = "100"
                LC.REC(TF.LC.PROVIS.AMOUNT)  = COMI
            END

            WRITE  R.LCC TO F.LCC , KEY.LIST ON ERROR
                PRINT "CAN NOT WRITE RECORD":KEY.LIST:"TO" :FN.LCC
            END

        END
    END
************************************************************
    RETURN
END
