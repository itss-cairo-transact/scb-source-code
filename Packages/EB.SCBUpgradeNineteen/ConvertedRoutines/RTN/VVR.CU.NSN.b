* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>705</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.NSN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

********** UPDATE BY MOHAMED SABRY 2012/03/13 ****************************

    WS.NSN.ID       = COMI
    WS.CU.BIRTH     = R.NEW(EB.CUS.BIRTH.INCORP.DATE)[3,6]
    WS.POSTING      = R.NEW(EB.CUS.POSTING.RESTRICT)
    WS.CU.STATUS    = R.NEW(EB.CUS.CUSTOMER.STATUS)
    WS.CONT.DATE    = R.NEW(EB.CUS.CONTACT.DATE)
    WS.NAT.BIRTH    = WS.NSN.ID[2,6]
    WS.NEW.SECTOR   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
    WS.ID.TYPE      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE>
    WS.CU.BIRTH.1ST = R.NEW(EB.CUS.BIRTH.INCORP.DATE)[1,2]
    WS.NAT.1ST      = WS.NSN.ID[1,1]
    WS.NAT.LEN      = LEN(WS.NSN.ID)
    WS.GENDER       = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.GENDER>[1,1]
    WS.NAT.GENDER   = WS.NSN.ID[13,1]

    IF WS.CU.BIRTH.1ST EQ 18 THEN
        WS.CU.BIRTH.1ST  = 1
    END
    IF WS.CU.BIRTH.1ST EQ 19 THEN
        WS.CU.BIRTH.1ST  = 2
    END
    IF WS.CU.BIRTH.1ST EQ 20 THEN
        WS.CU.BIRTH.1ST  = 3
    END

    WS.NAT.GENDER     = WS.NAT.GENDER / 2

    IF WS.NAT.GENDER  = INT(WS.NAT.GENDER) THEN
        WS.NAT.GENDER = 'F'
    END ELSE
        WS.NAT.GENDER = 'M'
    END

    IF (R.NEW(EB.CUS.NATIONALITY) EQ 'EG' ) AND (WS.NEW.SECTOR EQ '4650') AND ( WS.POSTING LT 90 ) AND ( WS.POSTING NE 70 ) AND WS.ID.TYPE NE 5 THEN
        IF WS.NSN.ID EQ '' AND WS.POSTING EQ 89 THEN
            GOTO NEXT.STEP
        END
        IF WS.NSN.ID EQ '' AND WS.CU.STATUS EQ 22 AND WS.CONT.DATE LT 20120426 THEN
            GOTO NEXT.STEP
        END

        NT.DAYS      = "C"
        CALL CDD("",R.NEW(EB.CUS.BIRTH.INCORP.DATE),TODAY,NT.DAYS)
        *TEXT = NT.DAYS ; CALL REM
        IF WS.NSN.ID EQ 0 AND NT.DAYS LE 365 AND WS.CU.STATUS EQ 22 AND WS.CONT.DATE GE 20140830 THEN
            COMI = '00000000000000'
            CALL REBUILD.SCREEN
            GOTO NEXT.STEP
        END
        IF (WS.NSN.ID EQ '') THEN
            ETEXT = '��� ����� ������ ���������'
            CALL STORE.END.ERROR
            RETURN
        END
        IF NOT (NUM(WS.NSN.ID)) THEN
            ETEXT = '���� ����� ����� ���'
            CALL STORE.END.ERROR
            RETURN
        END
        IF WS.NAT.LEN NE 14 THEN
            ETEXT = '��� ������� ��� ����'
            CALL STORE.END.ERROR
            RETURN
        END
        IF WS.CU.BIRTH NE WS.NAT.BIRTH THEN
            ETEXT = '����� ������ �� ����� ����� �������'
            CALL STORE.END.ERROR
            RETURN
        END
        IF WS.NAT.1ST NE WS.CU.BIRTH.1ST THEN
            ETEXT = '����� ����� �� ����� ����� �������'
            CALL STORE.END.ERROR
            RETURN
        END
        IF WS.GENDER NE WS.NAT.GENDER THEN
            ETEXT = '����� ������ �� ����� ��� �����'
            CALL STORE.END.ERROR
            RETURN
        END
    END
NEXT.STEP:
********** END OF UPDATE BY MOHAMED SABRY 2012/03/13 ****************************

*To inforce the user to enter NSN or ID.NUMBER and its related fields
    IF MESSAGE = 'VAL' THEN
        IF COMI  THEN

***Mahmoud 8/6/2009***
***Modified to validate ISSUE.DATE and EXPIRY.DATE with NSN ID ***

***            IF R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> # '' OR  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER> # '' OR  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.ISSUE.DATE> # ''  OR  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.EXPIRY.DATE> # ''  OR  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> # ''  THEN
            IF R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> # '' OR  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER> # '' OR  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> # ''  THEN
                ETEXT = 'Enter Either NSN or ID'
            END
        END
    END
************************************************************
    IF COMI THEN
        DATE.TODAY=TODAY
        DATE.BIRTH=R.NEW(EB.CUS.BIRTH.INCORP.DATE)[1,4]
        YEAR.16=DATE.BIRTH+16
        DATE.16=YEAR.16:R.NEW(EB.CUS.BIRTH.INCORP.DATE)[5,4]

********UPDATED BY NESSREEN AHMED 27/11/2011********************
********IF DATE.16 > DATE.TODAY THEN
********ETEXT='You Shouldnt Enter NSN NO'
********END ELSE
********END OF UPDATE 27/11/2011*********************************
        IF INDEX(COMI,'-',1) GT 0 THEN

            DAT=COMI[3,6]
            DAT1 = R.NEW(EB.CUS.BIRTH.INCORP.DATE)[3,6]
            IF DAT # DAT1 THEN ETEXT = 'WRONG DATE'
        END ELSE
            DAT = COMI[2,6]
            DAT1 = R.NEW(EB.CUS.BIRTH.INCORP.DATE)[3,6]
            IF DAT # DAT1 THEN ETEXT = 'WRONG DATE'
*                COMI = FMT(COMI,"##############")
        END
*********END
    END
    RETURN
END
