* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CU.LEGAL.FORM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.LEGAL.FORM


*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR("SCB.CUS.LEGAL.FORM":@FM:1,COMI,XX)
F.ITSS.SCB.CUS.LEGAL.FORM = 'F.SCB.CUS.LEGAL.FORM'
FN.F.ITSS.SCB.CUS.LEGAL.FORM = ''
CALL OPF(F.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM)
CALL F.READ(F.ITSS.SCB.CUS.LEGAL.FORM,COMI,R.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM,ERROR.SCB.CUS.LEGAL.FORM)
XX=R.ITSS.SCB.CUS.LEGAL.FORM<1>
    IF INDEX(XX,'**',1)  THEN
        ETEXT = 'YOU.CANT.USE.MAIN.LEGAL.FORM'
    END
    RETURN
END
