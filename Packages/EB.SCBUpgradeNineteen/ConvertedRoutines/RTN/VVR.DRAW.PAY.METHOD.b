* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.DRAW.PAY.METHOD

*The routine is to make sure that the value of the field PAYMENT.METHOD is 'N'
*otherwise it will bring an error message

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS


    TEXT = COMI ; CALL REM

    IF COMI = 'N' THEN
        TEXT = "GGGG" ; CALL REM
        CALL OPF('F.LETTER.OF.CREDIT',F.LETTER.OF.CREDIT)
*READ R.LETTER.OF.CREDIT FROM F.LETTER.OF.CREDIT,ID.NEW[1,12] THEN
        CALL F.READ ('F.LETTER.OF.CREDIT', ID.NEW[1,12], R.LETTER.OF.CREDIT, F.LETTER.OF.CREDIT, FILE.ERR)
        IF NOT(FILE.ERR) THEN
            REM.AC = R.LETTER.OF.CREDIT<TF.LC.THIRD.PARTY.CUSTNO>
        END
        CALL OPF( FN.ACCOUNT,F.ACCOUNT)
        T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":REM.AC:"AND CATEGORY EQ ":5000
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            TEXT = KEY.LIST ;CALL REM
            R.NEW(TF.DR.PAYMENT.ACCOUNT) = KEY.LIST<1,1>
        END

    END ELSE
        ETEXT = 'Wrong.Payment.Method'
    END
    RETURN
END
