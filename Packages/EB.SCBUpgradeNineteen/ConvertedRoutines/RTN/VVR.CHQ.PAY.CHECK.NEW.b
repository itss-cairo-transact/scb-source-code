* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>314</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CHQ.PAY.CHECK.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP   = C$ID.COMPANY
    XX     = COMP[8,2]
    YY.ACC = 994999:XX:10320501

    TEXT   = YY.ACC  ; CALL REM

    IF MESSAGE EQ "" THEN

        IF COMI THEN

            FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''

            CALL OPF( FN.BR,F.BR)
            CALL F.READ(FN.BR,COMI, R.BR, F.BR, ETEXT)

            IF ETEXT  THEN ETEXT = "������ ��� �����"

            IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ  8  THEN
                IF  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CHQ.PAY> EQ 'Y' THEN
                    ETEXT = "�� ����� ��� ����� �� ��� "
                END
            END

            IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT> NE YY.ACC  THEN
                ETEXT ="��� ����� �� ��� ���������"
            END


            IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> NE  8  THEN
                ETEXT = "��� ����� �� ����"
            END

            IF NOT(ETEXT) THEN
                R.NEW(INF.MLT.AMOUNT.LCY)<1,1>     = R.BR<EB.BILL.REG.AMOUNT>
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1> = YY.ACC
            END

            CALL REBUILD.SCREEN


        END
        RETURN
    END
