* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>197</Rating>
*-----------------------------------------------------------------------------
** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.CU.ID.NUMBER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*A CROSS VALIDATION ROUTINE (VALIDATION OCCURS WHILE COMMIT - IF MSG = VAL) TO VALIDATE
*IF ID NUMBER EXIST THEN ID TYPE MUST BE ENTERED
*IF ID TYPE EXIST OR ID PLACE EXIST THEN ID NUMBER MUST BE ENTERED

    IF MESSAGE = 'VAL' THEN

        IF R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ID.TYPE> AND R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.ID.TYPE> # 5 THEN

            IF NOT( COMI) THEN ETEXT = 'ENTER ID NUMBER'

        END ELSE

            IF COMI THEN ETEXT = 'NO ID TYPE'
        END

    END

    RETURN
END
