* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VVR.DRW.AC.LOAN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    FN.LCC = 'F.SCB.LOAN.AC' ;F.LCC = '' ; R.LCC = ''
    CALL OPF(FN.LCC,F.LCC)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

*    DAT.ID = "EG0010001"
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,DAY.MAT)

    DAY.MAT = TODAY

    T.SEL = "SELECT F.SCB.LOAN.AC WITH STATUS EQ 'O' AND MAT.DATE LT ": DAY.MAT
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)


    FOR I = 1 TO SELECTED
        CALL F.READ(FN.LCC,KEY.LIST<I>,R.LCC,F.LCC,READ.ERR)


        R.LCC<AC.LO.STATUS> = 'M'

*******************************UPDATED BY RIHAM R15**********************
*        WRITE  R.LCC TO F.LCC , KEY.LIST<I> ON ERROR
*            PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.LCC
*        END
        CALL F.WRITE(FN.LCC,KEY.LIST<I>,R.LCC)
********************************************

    NEXT I
************************************************************
    RETURN
END
