* @ValidationCode : MjoxMzQ5OTM5MzE5OkNwMTI1MjoxNjQ1MTI4NjQ1NzM5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 17 Feb 2022 12:10:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>149</Rating>
*-----------------------------------------------------------------------------
******** DINA_SCB ********

SUBROUTINE VVR.CU.NO.CHANGE.LEG


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

*To Inforce the user to enter only the Legal Form related to his Sector

    IF V$FUNCTION = 'I' THEN
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.INDUSTRY,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,IND1)
        F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
        FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
        CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
        CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,IND1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
        IND1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.INDUSTRY,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
        IF NOT(ETEXT) THEN
            LOCATE R.NEW(EB.CUS.INDUSTRY) IN IND1<1,1> SETTING YY THEN
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.VER.IND.SEC.LEG.1':@FM:VISL.LEGAL.FORM,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>,LEG1)
                F.ITSS.SCB.VER.IND.SEC.LEG.1 = 'F.SCB.VER.IND.SEC.LEG.1'
                FN.F.ITSS.SCB.VER.IND.SEC.LEG.1 = ''
                CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1)
                CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG.1,LEG1,R.ITSS.SCB.VER.IND.SEC.LEG.1,FN.F.ITSS.SCB.VER.IND.SEC.LEG.1,ERROR.SCB.VER.IND.SEC.LEG.1)
                LEG1=R.ITSS.SCB.VER.IND.SEC.LEG.1<@FM:VISL.LEGAL.FORM,R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>>
                LOCATE COMI IN LEG1<1,YY,1> SETTING MM ELSE ETEXT = 'THIS.LEGAL.FORM.NOT.ALLWED'
            END
        END
    END
RETURN
END
