* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*---------MAI 09 OCT 2018--------------------------------------------------------------------
* <Rating>205</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.CD.OPEN.NEW.ISL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE

***TO DEFAULT EXPIRY DATE AND VALUE DATE AND MAT AND CD AMOUNT
    IF MESSAGE NE 'VAL' THEN
        DEFFUN SHIFT.DATE( )
**BY MAI**
        CD.QUANT = COMI
        CD.TYP = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
        FN.CD.TYPE = 'F.SCB.CD.TYPES' ; F.CD.TYPE = '';R.CD.TYPE=''
        CALL OPF(FN.CD.TYPE,F.CD.TYPE)
        CALL F.READ(FN.CD.TYPE,CD.TYP,R.CD.TYPE,F.CD.TYPE,E1)
********************
        FN.BI.DATE = 'FBNK.BASIC.INTEREST.DATE' ; F.BI.DATE = '';R.BI.DATE=''
        CALL OPF(FN.BI.DATE, F.BI.DATE)
***Updated by Nessreen Ahmed 6/2/2019***************
***     BI.DATE.ID = '87EGP'
        BI.DATE.ID = '90EGP'
***End of Update 6/2/2019***************************
        *TEXT = 'BI.DATES.ID= ':  BI.DATE.ID;CALL REM;

        CALL F.READ(FN.BI.DATE,BI.DATE.ID,R.BI.DATE,F.BI.DATE,DATE.ERR)
        IF R.BI.DATE NE '' THEN
            BI.DATE = R.BI.DATE<EB.BID.EFFECTIVE.DATE,1>

*            TEXT = 'BI.DATE = ':  BI.DATE;CALL REM;
        END
***Updated by Nessreen Ahmed 6/2/2019***************
***BI.ID = '87EGP':BI.DATE
   BI.ID = '90EGP':BI.DATE
***End of Update 6/2/2019***************************
*        BI.ID = '88EGP':BI.DATE
     *   TEXT = 'BI.ID = ':  BI.ID;CALL REM;
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('BASIC.INTEREST':@FM:EB.BIN.INTEREST.RATE,BI.ID,INTEREST.VALUE)
F.ITSS.BASIC.INTEREST = 'F.BASIC.INTEREST'
FN.F.ITSS.BASIC.INTEREST = ''
CALL OPF(F.ITSS.BASIC.INTEREST,FN.F.ITSS.BASIC.INTEREST)
CALL F.READ(F.ITSS.BASIC.INTEREST,BI.ID,R.ITSS.BASIC.INTEREST,FN.F.ITSS.BASIC.INTEREST,ERROR.BASIC.INTEREST)
INTEREST.VALUE=R.ITSS.BASIC.INTEREST<EB.BIN.INTEREST.RATE>

** FN.CD.RATE = 'FBNK.BASIC.INTEREST' ; F.CD.RATE = '';R.CD.RATE=''
** CALL OPF(FN.CD.RATE,F.CD.RATE)
** CALL F.READ(FN.CD.RATE,BI.DATE,R.CD.RATE,F.CD.RATE,E11)
*************
        R.NEW(LD.INTEREST.RATE)= INTEREST.VALUE
        R.NEW(LD.CATEGORY)     = R.CD.TYPE<CD.TYPES.CATEGORY>
        R.NEW(LD.CURRENCY)     = FIELD(CD.TYP,'-',1)
****ADDED BY MAI
        IF CD.QUANT NE '' THEN
            R.NEW(LD.AMOUNT)      = FIELD(CD.TYP,'-',2) *  CD.QUANT
        END ELSE
            R.NEW(LD.AMOUNT)      = FIELD(CD.TYP,'-',2)
        END
***-----------------HYTHAM 20100725--------------------------****
*******COMMENTED BY MAI**********
*  R.NEW(LD.DRAWDOWN.NET.AMT)  = FIELD(COMI,'-',2)
*  R.NEW(LD.DRAWDOWN.ISSUE.PRC)= FIELD(COMI,'-',2)
*  R.NEW(LD.ISSUE.PL.AMOUNT)   = '0.00'
***-----------------HYTHAM 20100725--------------------------****
        R.NEW(LD.VALUE.DATE)   = TODAY
        NO.MON                 = FIELD(CD.TYP,'-',4)
        VAL.DATE               = R.NEW(LD.VALUE.DATE)
        FIN.MAT                = SHIFT.DATE(VAL.DATE,NO.MON,UP)
        R.NEW(LD.FIN.MAT.DATE) = FIN.MAT

        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = FIN.MAT

* R.NEW(LD.INTEREST.RATE)= R.CD.TYPE<CD.TYPES.CD.RATE>


*  (BY MAI) NO NEED FOR NEXT LINE BECAUSE PROMPT OF QAUNTITY WILL NOT BE DISPLAYED
*   CD.TYPE                = COMI
*   MN.NO                  = FIELD(COMI,'-',3)[1,1]
***ONLY 3 MONTHES SO NO NEED FOR FOLLOWING
*   IF MN.NO EQ '1' THEN
*       BAS.ID =  '84'
*   END
*   IF MN.NO EQ '3' THEN
*       BAS.ID =  '85'
*   END
*   IF MN.NO EQ '6' THEN
*       BAS.ID =  '86'
*   END

*  R.NEW(LD.INTEREST.KEY) = BAS.ID
*HH-------------------------------HHH----------------------------------HHH
** YTEXT="No Of Cd To Be ISsued For This Type "
** CALL TXTINP(YTEXT, 8, 32, "12", "A")
** R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY> = COMI
*HH-------------------------------HHH----------------------------------HHH
*    COMI = CD.TYPE
*****************************END OF COMMENTED LINES
        CALL REBUILD.SCREEN
    END
    RETURN
END
