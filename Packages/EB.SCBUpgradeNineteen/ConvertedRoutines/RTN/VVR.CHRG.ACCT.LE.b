* @ValidationCode : MjotMTEwOTk3MjU1MjpDcDEyNTI6MTY0NTEyODQxMTQ1NjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 12:06:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNineteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNineteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1235</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.CHRG.ACCT.LE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LC.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY


    IF MESSAGE # 'VAL' THEN

        IF COMI THEN

            FN.FT.COM  = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
            CALL OPF(FN.FT.COM,F.FT.COM)

            FN.FT.CHRG = 'FBNK.FT.CHARGE.TYPE' ; F.FT.CHRG = ''
            CALL OPF(FN.FT.CHRG,F.FT.CHRG)

            FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; E11 = ''
            CALL OPF(FN.CUR,F.CUR)
            DR.CURR    = R.NEW(TF.LC.LC.CURRENCY)
* CALL F.READ(FN.CUR,DR.CURR, R.CUR, F.CUR ,E11)

*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CHRG.NO = DCOUNT(R.NEW(TF.LC.CHARGE.CODE),@VM)
            DR.AMT     = R.NEW(TF.LC.LC.AMOUNT)

            FOR XX =  1 TO CHRG.NO

                R.NEW(TF.LC.CHARGE.ACCT)<1,XX>     = COMI
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CURRENCY, R.NEW(TF.LC.CHARGE.ACCT)<1,XX>,CHRG.CURR)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,CHRG.CURR,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CHRG.CURR=R.ITSS.ACCOUNT<@FM:AC.CURRENCY,R.NEW(TF.LC.CHARGE.ACCT)<1,XX>>
                R.NEW(TF.LC.CHARGE.CURRENCY)<1,XX> = CHRG.CURR
                R.NEW(TF.LC.AMORT.CHARGES)<1,XX>   = 'NO'
                R.NEW(TF.LC.PARTY.CHARGED)<1,XX>   = 'O'
                R.NEW(TF.LC.CHARGE.STATUS)<1,XX>   ='2'

**---------------------CALC COMM AND CAHRG ------------------**
**--------------            COMM             ------------------------**

                E.COM    = ''
                COM.CODE =  R.NEW(TF.LC.CHARGE.CODE)<1,XX>
                CALL F.READ(FN.FT.COM,COM.CODE,R.FT.COM,F.FT.COM,E.COM)
                IF NOT(E.COM) THEN

                    MESSAGE      = ''
                    E            = ''
                    ETEXT        = ''
                    ERR          = ''
                    COM.MIN2     = ''
                    COM.MAX2     = ''
                    COM.MIN1     = ''
                    COM.MAX1     = ''
                    NEW.AMT      = ''
                    NEW.AMT1     = ''
                    NEW.AMT2     = ''
                    COM.PER      = R.FT.COM<FT4.PERCENTAGE><1,1>
                    COM.MIN      = R.FT.COM<FT4.MINIMUM.AMT><1,1>
                    COM.MAX      = R.FT.COM<FT4.MAXIMUM.AMT><1,1>
                    FLAT.AMT     = R.FT.COM<FT4.FLAT.AMT>
                    COM.CURR     = R.FT.COM<FT4.CURRENCY>
                    FLG.MIN      = ''
                    FLG.MAX      = ''
                    FLG.MAX.1    = ''
                    FLG.MIN.1    = ''
                    MIN.AMT      = ''
                    MIN.AMT.1    = ''
                    MIN.AMT.2    = ''
                    MAX.AMT      = ''
                    MAX.AMT.1    = ''
                    MAX.AMT.2    = ''
                    DR.CURR      = R.NEW(TF.LC.LC.CURRENCY)

                    ALL.COM      = (( DR.AMT * COM.PER ) / 100 )

                    CALL F.READ(FN.CUR,COM.CURR, R.CUR, F.CUR ,E11)

                    DR.CURR =  COM.CURR

*                   IF DR.CURR NE CHRG.CURR THEN
                    IF COM.CURR NE CHRG.CURR THEN

*TEXT = "CUR" : COM.CURR ; CALL REM

                        CUR.RATE.DR  = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
*TEXT = "RATE=  " : CUR.RATE.DR ; CALL REM

                        IF DR.CURR EQ 'EGP'  THEN
                            CUR.RATE.DR = 1
                        END

                        FLAT.AMT1     = FLAT.AMT * CUR.RATE.DR
                        CALL EB.ROUND.AMOUNT ('USD',FLAT.AMT1,'',"2")

*TEXT ="FLAT1=  "  : FLAT.AMT1 ; CALL REM
                        NEW.AMT1     = ALL.COM * CUR.RATE.DR
                        CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")

                        COM.MIN1     = COM.MIN * CUR.RATE.DR
                        CALL EB.ROUND.AMOUNT ('USD',COM.MIN1,'',"2")

                        COM.MAX1     = COM.MAX * CUR.RATE.DR
                        CALL EB.ROUND.AMOUNT ('USD',COM.MAX1,'',"2")
*TEXT = "FLA=  " : FLAT.AMT ; CALL REM

*TEXT = "FLA1=  " : FLAT.AMT1 ; CALL REM
                        IF DR.CURR EQ "JPY" THEN

                            FLAT.AMT1     = FLAT.AMT / 100
                            CALL EB.ROUND.AMOUNT ('USD',FLAT.AMT1,'',"2")

                            NEW.AMT1  = ALL.COM / 100
                            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")

                            COM.MIN1     = COM.MIN / 100
                            CALL EB.ROUND.AMOUNT ('USD',COM.MIN1,'',"2")

                            COM.MAX1     = COM.MAX / 100
                            CALL EB.ROUND.AMOUNT ('USD',COM.MAX1,'',"2")

                        END

                        CALL F.READ(FN.CUR,CHRG.CURR, R.CUR.CHRG, F.CUR ,E11)
                        CUR.RATE.CHRG  = R.CUR.CHRG<EB.CUR.MID.REVAL.RATE><1,1>

                        IF CHRG.CURR EQ 'EGP' THEN
                            CUR.RATE.CHRG  = 1
                        END

                        FLAT.AMT2     = FLAT.AMT1 / CUR.RATE.CHRG
                        CALL EB.ROUND.AMOUNT ('USD',FLAT.AMT2,'',"2")

*TEXT ="FLAT2=  "  : FLAT.AMT2 ; CALL REM
*TEXT = "FLA2=  " : FLAT.AMT2 ; CALL REM

                        NEW.AMT2       = NEW.AMT1 / CUR.RATE.CHRG
                        CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")

                        COM.MIN2      = COM.MIN1 / CUR.RATE.CHRG
                        CALL EB.ROUND.AMOUNT ('USD',COM.MIN2,'',"2")

                        COM.MAX2      = COM.MAX1 / CUR.RATE.CHRG
                        CALL EB.ROUND.AMOUNT ('USD',COM.MAX2,'',"2")

                        IF CHRG.CURR EQ "JPY" THEN
                            FLAT.AMT2  = FLAT.AMT1 * 100
                            CALL EB.ROUND.AMOUNT ('USD',FLAT.AMT2,'',"2")
                            NEW.AMT2  = NEW.AMT1 * 100
                            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")
                            COM.MIN2      = COM.MIN1 * 100
                            CALL EB.ROUND.AMOUNT ('USD',COM.MIN2,'',"2")
                            COM.MAX2      = COM.MAX1 * 100
                            CALL EB.ROUND.AMOUNT ('USD',COM.MAX2,'',"2")
                        END

                        NEW.AMT = NEW.AMT2

                        BEGIN CASE

                            CASE NEW.AMT2 < COM.MIN2
                                NEW.AMT = COM.MIN2
                            CASE ( NEW.AMT2 > COM.MAX2 AND COM.MAX NE '' )
                                NEW.AMT = COM.MAX2
                            CASE FLAT.AMT NE ''
                                NEW.AMT = FLAT.AMT2
                            CASE OTHERWISE
                                NEW.AMT = NEW.AMT2
                        END CASE

*TEXT = "FLA3=  " : FLAT.AMT2 ; CALL REM
*TEXT = "NEW.AMT=  " : NEW.AMT ; CALL REM
                    END ELSE

***---------------------  20120508   -----------------------
                        NEW.AMT = ALL.COM

                        BEGIN CASE
                            CASE  NEW.AMT < COM.MIN
                                NEW.AMT = COM.MIN
                            CASE ( NEW.AMT > COM.MAX AND COM.MAX NE '' )
                                NEW.AMT = COM.MAX
                            CASE  FLAT.AMT NE ''
                                NEW.AMT = FLAT.AMT
                            CASE OTHERWISE
                                NEW.AMT = ALL.COM
                        END CASE

                    END

*TEXT = "FLAT=  " : FLAT.AMT ; CALL REM
*TEXT = "NEW.AMT=  " : NEW.AMT ; CALL REM

                    CALL EB.ROUND.AMOUNT ('USD',NEW.AMT,'',"2")

                    R.NEW(TF.LC.CHARGE.AMOUNT)<1,XX>   = NEW.AMT
                END
**-----------------------------------------------------------------**

**------------              CHRG          -------------------------**

                MESSAGE      = ''
                E            = ''
                ETEXT        = ''
                ERR          = ''
                DR.CURR      = ''
                CUR.RATE.DR  = ''
                MIN.AMT      = ''
                MIN.AMT.1    = ''
                MIN.AMT.2    = ''
                MAX.AMT      = ''
                MAX.AMT.1    = ''
                MAX.AMT.2    = ''
                COM.MIN2     = ''
                COM.MAX2     = ''
                COM.MIN1     = ''
                COM.MAX1     = ''
                NEW.AMT      = ''
                NEW.AMT1     = ''
                NEW.AMT2     = ''

                CHRG.CODE =  R.NEW(TF.LC.CHARGE.CODE)<1,XX>
                CALL F.READ(FN.FT.CHRG,CHRG.CODE,R.FT.CHRG,F.FT.CHRG,E.CHRG)

                IF NOT(E.CHRG) THEN

                    DR.CURR   = R.FT.CHRG<FT5.CURRENCY>
                    ALL.COM   = R.FT.CHRG<FT5.FLAT.AMT>

                    CALL F.READ(FN.CUR, DR.CURR, R.CUR, F.CUR ,E11)

                    IF DR.CURR NE CHRG.CURR THEN

                        CUR.RATE.CHRG  = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

                        IF DR.CURR EQ 'EGP'  THEN
                            CUR.RATE.CHRG = 1
                        END

                        NEW.AMT1     = ALL.COM * CUR.RATE.CHRG

                        CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")
                        IF DR.CURR EQ "JPY" THEN
                            NEW.AMT1  = ALL.COM / 100
                            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")
                        END

                        CALL F.READ(FN.CUR,CHRG.CURR, R.CUR.CHRG, F.CUR ,E11)
                        CUR.RATE.CHRG  = R.CUR.CHRG<EB.CUR.MID.REVAL.RATE><1,1>

                        IF CHRG.CURR EQ 'EGP'  THEN
                            CUR.RATE.CHRG  = '1'
                        END

                        NEW.AMT2       = NEW.AMT1 / CUR.RATE.CHRG
                        CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")


                        IF CHRG.CURR EQ "JPY" THEN
                            NEW.AMT2  = NEW.AMT1 * 100
                            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")
                        END

                        NEW.AMT = NEW.AMT2
                    END ELSE
                        NEW.AMT = ALL.COM
                    END
*TEXT = "NEW.AMT= " : NEW.AMT ; CALL REM
                    CALL EB.ROUND.AMOUNT ('USD',NEW.AMT,'',"2")
                    R.NEW(TF.LC.CHARGE.AMOUNT)<1,XX>   = NEW.AMT
                END

**-----------------------------------------------------------------**


**---------------------CALC COMM AND CAHRG ------------------**
            NEXT XX
        END
        CALL REBUILD.SCREEN
    END

RETURN
END
