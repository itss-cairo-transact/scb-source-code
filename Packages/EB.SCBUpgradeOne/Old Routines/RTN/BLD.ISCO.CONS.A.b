* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-45</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE BLD.ISCO.CONS.A(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.CS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.CF


    EOF  = ''
    R.CF = ''
    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
*============================ OPEN TABLES =========================

    FN.CF  = 'F.SCB.ISCO.CF' ; F.CF = '' ; R.CF = ''
    CALL OPF(FN.CF,F.CF)
    FN.CU  = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CS  = 'F.SCB.ISCO.CS' ; F.CS = '' ; R.CS = ''
    CALL OPF(FN.CS,F.CS)

    LOCATE "ACCT.CLOSING.DATE" IN ENQ<2,1> SETTING ISCO.POS THEN
        CLOSE.DATE = ENQ<4,ISCO.POS>
        FROM.DATE  = FIELD(CLOSE.DATE,' ',1)
        TO.DATE    = FIELD(CLOSE.DATE,' ',2)
    END

    T.SEL  = "SELECT F.SCB.ISCO.CF WITH ACCT.CLOSING.DATE GE ":FROM.DATE:" AND ACCT.CLOSING.DATE LE ":TO.DATE:" BY CF.ACC.NO"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*======================== BB.DATA ===========================

    CUST.NO = ''
    PRE.CUST.NO = ''
    TOT.APP.AMT = 0
    TOT.USE.AMT = 0
    K = 1
    Q = 1

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CF,KEY.LIST<I>, R.CF, F.CF, ETEXT)

            ISCO.ID  = KEY.LIST<I>
            ACCT.NO  = R.CF<ISCO.CF.CF.ACC.NO>

            IF ACCT.NO[1,1] EQ 0 THEN
                ACCT.NO = ACCT.NO[2,7]
            END
            ELSE
                ACCT.NO = ACCT.NO[1,8]
            END
            CALL F.READ( FN.CU,ACCT.NO, R.CU, F.CU, ETEXT1)
            CUS.STATUS = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
            CRT.STATUS = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.STAT>

            IF ( CUS.STATUS EQ '110' OR CUS.STATUS EQ '111' OR CRT.STATUS EQ 'MRG' ) THEN
                ENQ<2,Q> = "@ID"
                ENQ<3,Q> = "EQ"
                ENQ<4,Q> = ISCO.ID
                Q ++
            END
        NEXT I
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END

    IF Q EQ 1 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
    RETURN

END
