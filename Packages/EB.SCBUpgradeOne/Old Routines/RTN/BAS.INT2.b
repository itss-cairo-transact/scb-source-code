* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BAS.INT2(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY

***************************************
***********

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    T.SEL = "SELECT FBNK.BASIC.INTEREST WITH  @ID LIKE ...USD...  BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL ": SELECTED ; CALL REM
    Z = 1
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            IF LEN(KEY.LIST<I>) EQ 11 THEN
                ACC.NO = KEY.LIST<I>[1,4]
                ACC.NO2 = KEY.LIST<I+1>[1,4]
            END ELSE
                ACC.NO = KEY.LIST<I>[1,5]
                ACC.NO2 = KEY.LIST<I+1>[1,5]
            END
            IF (ACC.NO NE ACC.NO2) OR I = SELECTED  THEN
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = KEY.LIST<I>
                Z = Z+1

            END ELSE
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = "DUMMY"
                Z = Z+1
            END
        NEXT I
    END ELSE
        ENQ<2,Z> = "@ID"
        ENQ<3,Z> = "EQ"
        ENQ<4,Z> = "DUMMY"
        Z = Z+1


    END
    RETURN
END
