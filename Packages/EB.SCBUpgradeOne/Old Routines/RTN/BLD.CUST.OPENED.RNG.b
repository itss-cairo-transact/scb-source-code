* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUST.OPENED.RNG(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*-----------------------------------------*
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'    ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
*-----------------------------------------*
    DATA.FLAG = 0

    YTEXT = "Enter Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    START.DATE = COMI
    IF START.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END
*--------
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    END.DATE = COMI
    IF END.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END
*-------------------------------------------*
    T.SEL  = "SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":START.DATE
    T.SEL := " AND CONTACT.DATE LE ":END.DATE
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
    T.SEL := " AND COMPANY.BOOK EQ ":COMP.ID
    T.SEL := " AND SCCD.CUSTOMER NE 'YES'"
    T.SEL := " AND TEXT UNLIKE BR... BY CONTACT.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    HH = 1
    IF SELECTED THEN
        ENQ<2,HH> = "@ID"
        ENQ<3,HH> = "NE"
        ENQ<4,HH> = START.DATE :"*": END.DATE
        HH = HH + 1
        FOR II = 1 TO SELECTED
            ENQ<2,HH> = "@ID"
            ENQ<3,HH> = "EQ"
            ENQ<4,HH> = KEY.LIST<II>
            HH = HH + 1
        NEXT II
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
*----------------------------------------------------------------------*
    RETURN
END
