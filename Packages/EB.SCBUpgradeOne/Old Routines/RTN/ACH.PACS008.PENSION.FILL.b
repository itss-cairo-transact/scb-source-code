* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1317</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACH.PACS008.PENSION.FILL(ACH.DATE)
*    PROGRAM ACH.PACS008.PENSION.FILL

* CREATED 10-05-2015
* BY NOHA HAMED

*-----------------------------------
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
    FLAG = 1
    GOSUB OPEN.FILES
    EOF = ''
    I = 1
    LOOP WHILE NOT(EOF)
        GOSUB READ.TXT.FILE
    REPEAT

    CLOSESEQ VAR.FILE
    GOTO PROGRAM.END
**********************************************
OPEN.FILES:
*==========
*COMAND = "sh ACH/conv_ach.sh"
    COMAND = "sh ACH/LOAD.CRD/conv_ach.sh"
    EXECUTE COMAND

    *YTEXT = "Enter ACH Date : "
    *CALL TXTINP(YTEXT, 8, 22, "8", "D")
    T.DATE = ACH.DATE

**********OPEN TXT FILE**************
*DIR.NAME = "ACH"
    DIR.NAME = "ACH/LOAD.CRD"
    TEXT.FILE = "PACS008.txt"
    VAR.FILE = ''

    OPENSEQ DIR.NAME,TEXT.FILE TO VAR.FILE ELSE
        TEXT = "ERROR OPEN FILE ":TEXT.FILE ; CALL REM
        RETURN
    END
    IF IOCTL(VAR.FILE, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP
**********OPEN TABLES****************
    FN.PAC8 = "F.SCB.ACH.PACS008"; F.PAC8 = '' ;FVAR.PAC8 = ''

    OPEN FN.PAC8 TO FVAR.PAC8 ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    T.SEL = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ":T.DATE:"...."
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        SERIAL.NO1 = SELECTED + 1
    END
    ELSE
        SERIAL.NO1 = 1
    END

    R.PAC  = ''
    R.PAC8 = ''
**************************************
READ.TXT.FILE:
    R.LINE = '' ; R.PAC = ''; T.SEL='';KEY.LIST=''; SELECTED=0
    UPDATE.FLAG = ''; ERR=''; ER=''; L.LIST=''; SELECT.NO=0; Y.SEL=''
    ZEROS =''; NO.OF.RECORDS=0; NO.OF.REC=0; Z=''

    READSEQ R.LINE  FROM VAR.FILE THEN

        IF FLAG = 1 THEN
            BATCH.TYPE          = TRIM(R.LINE[3,3])
            BATCH.ID            = TRIM(R.LINE[6,35])
            CREATION.DATE       = TRIM(R.LINE[41,25])
            NO.OF.TRANS         = TRIM(R.LINE[66,15])
            SETTLEMENT.DATE     = TRIM(R.LINE[101,24])
            PACS.CHARGER.BEARER = TRIM(R.LINE[140,4])
            Y.SEL = "SELECT F.SCB.ACH.PACS008 WITH BATCH.ID EQ ": BATCH.ID
            CALL EB.READLIST(Y.SEL, KEY.LIST1, "", SELECTED1, ASD1)

            IF SELECTED1 NE '' AND SELECTED1 NE 0 THEN
                TEXT = 'THIS BATCH ALREADY EXIST'; CALL REM
                GOSUB PROGRAM.END
            END
        END
        IF FLAG NE 1 THEN
            IF I LE NO.OF.TRANS THEN
                PACS.INSTRUCT.IDENT           = TRIM(R.LINE[3,35])
                PACS.END.OF.END.IDENT         = TRIM(R.LINE[38,35])
                PACS.TRANSACTION.ID           = TRIM(R.LINE[73,35])
                PACS.AMOUNT                   = TRIM(R.LINE[108,17])
                PACS.CURRENCY                 = TRIM(R.LINE[125,3])
               * PACS.CHARGER.BEARER           = TRIM(R.LINE[128,4])
                PACS.DEBTOR.NAME              = TRIM(R.LINE[132,70])
                PACS.DEBTOR.ADDRESS1          = TRIM(R.LINE[202,70])
                PACS.DEBTOR.ADDRESS2          = TRIM(R.LINE[272,70])
                PACS.DEBTOR.ADDRESS3          = TRIM(R.LINE[342,70])
                PACS.DEBTOR.ADDRESS4          = TRIM(R.LINE[412,70])
                PACS.DEBTOR.ADDRESS5          = TRIM(R.LINE[482,70])
                PACS.DEBTOR.STREET.NAME       = TRIM(R.LINE[552,70])
                PACS.DEBTOR.BUILDING.NO       = TRIM(R.LINE[622,16])
                PACS.DEBTOR.POST.CODE         = TRIM(R.LINE[638,16])
                PACS.DEBTOR.TOWN.NAME         = TRIM(R.LINE[654,35])
                PACS.DEBTOR.COUNTRY.SUB.DIVISION    = TRIM(R.LINE[689,35])
                PACS.DEBTOR.COUNTRY           = TRIM(R.LINE[724,2])
                PACS.DEBTOR.IDENT.TYPE1       = TRIM(R.LINE[726,2])
                PACS.DEBTOR.IDENT.NO1         = TRIM(R.LINE[728,35])
                PACS.DEBTOR.IDENT.TYPE2       = TRIM(R.LINE[763,2])
                PACS.DEBTOR.IDENT.NO2         = TRIM(R.LINE[765,35])
                PACS.DEBTOR.IDENT.TYPE3       = TRIM(R.LINE[800,2])
                PACS.DEBTOR.IDENT.NO3         = TRIM(R.LINE[802,35])
                PACS.DEBTOR.IDENT.TYPE4       = TRIM(R.LINE[837,2])
                PACS.DEBTOR.IDENT.NO4         = TRIM(R.LINE[839,35])
                PACS.DEBTOR.ACCOUNT.NO        = TRIM(R.LINE[874,34])
                PACS.DEBTOR.ACCOUNT.TYPE      = TRIM(R.LINE[908,3])
                PACS.DEBTOR.PARTY.BIC         = TRIM(R.LINE[912,11])
                PACS.DEBTOR.PARTY.ID          = TRIM(R.LINE[923,35])
                PACS.DEBTOR.PARTY.BRANCH.ID   = TRIM(R.LINE[958,35])
                PACS.DEBTOR.PARTY.BRANCH.NAME = TRIM(R.LINE[993,35])
                PACS.CREDITOR.PARTY.BIC       = TRIM(R.LINE[1028,11])
                PACS.CREDITOR.PARTY.ID        = TRIM(R.LINE[1039,35])
                PACS.CREDITOR.PARTY.BRANCH.ID = TRIM(R.LINE[1074,35])
                PACS.CREDITOR.PARTY.BRANCH.NAME = TRIM(R.LINE[1109,35])
                PACS.CREDITOR.NAME              = TRIM(R.LINE[1144,70])
                PACS.CREDITOR.ADDRESS1          = TRIM(R.LINE[1214,70])
                PACS.CREDITOR.ADDRESS2          = TRIM(R.LINE[1284,70])
                PACS.CREDITOR.ADDRESS3          = TRIM(R.LINE[1354,70])
                PACS.CREDITOR.ADDRESS4          = TRIM(R.LINE[1424,70])
                PACS.CREDITOR.ADDRESS5          = TRIM(R.LINE[1494,70])
                PACS.CREDITOR.STREET.NAME       = TRIM(R.LINE[1564,70])
                PACS.CREDITOR.BUILDING.NO       = TRIM(R.LINE[1634,16])
                PACS.CREDITOR.POST.CODE         = TRIM(R.LINE[1650,16])
                PACS.CREDITOR.TOWN.NAME         = TRIM(R.LINE[1666,35])
                PACS.CREDITOR.COUNTRY.SUB.DIVISION     = TRIM(R.LINE[1701,35])
                PACS.CREDITOR.COUNTRY           = TRIM(R.LINE[1736,2])
                PACS.CREDITOR.IDENT.TYPE1       = TRIM(R.LINE[1738,2])
                PACS.CREDITOR.IDENT.NO1         = TRIM(R.LINE[1740,35])
                PACS.CREDITOR.IDENT.TYPE2       = TRIM(R.LINE[1775,2])
                PACS.CREDITOR.IDENT.NO2         = TRIM(R.LINE[1777,35])
                PACS.CREDITOR.IDENT.TYPE3       = TRIM(R.LINE[1812,2])
                PACS.CREDITOR.IDENT.NO3         = TRIM(R.LINE[1814,35])
                PACS.CREDITOR.IDENT.TYPE4       = TRIM(R.LINE[1849,2])
                PACS.CREDITOR.IDENT.NO4         = TRIM(R.LINE[1851,35])
                PACS.CREDITOR.ACCOUNT.NO        = TRIM(R.LINE[1886,34])
                PACS.CREDITOR.ACCOUNT.TYPE      = TRIM(R.LINE[1920,4])
                PACS.PURPOSE                    = TRIM(R.LINE[1924,35])
                PACS.REMITTANCE.INFO1           = TRIM(R.LINE[1959,140])
                PACS.REMITTANCE.INFO2           = TRIM(R.LINE[2099,140])
                PACS.REMITTANCE.INFO3           = TRIM(R.LINE[2239,140])
                PACS.REMITTANCE.INFO4           = TRIM(R.LINE[2379,140])
                PACS.REMITTANCE.INFO5           = TRIM(R.LINE[2519,140])
                I = I + 1
*******FILL IN TABLE
                R.PAC<PACS8.BATCH.ID>            =      BATCH.ID
                R.PAC<PACS8.CREATION.DATE.TIME>  =      CREATION.DATE
                R.PAC<PACS8.REQ.SETTLEMENT.DATE> =      SETTLEMENT.DATE
                R.PAC<PACS8.BATCH.TYPE>          =      'CRT'
                R.PAC<PACS8.TRN.DATE>            =      ''
                R.PAC<PACS8.INSTRUCTION.IDENT>   =      PACS.INSTRUCT.IDENT
                R.PAC<PACS8.END.OF.END.IDENT>    =      PACS.END.OF.END.IDENT
                R.PAC<PACS8.TRANSACTION.ID>      =      PACS.TRANSACTION.ID
                R.PAC<PACS8.AMOUNT>              =      PACS.AMOUNT
                R.PAC<PACS8.CURRENCY>            =      PACS.CURRENCY
                R.PAC<PACS8.CHARGER.BEARER>      =      PACS.CHARGER.BEARER
                R.PAC<PACS8.DEBTOR.NAME>         =      PACS.DEBTOR.NAME
                R.PAC<PACS8.DEBTOR.ADDRESS1>     =      PACS.DEBTOR.ADDRESS1
                R.PAC<PACS8.DEBTOR.ADDRESS3>     =      PACS.DEBTOR.ADDRESS3
                R.PAC<PACS8.DEBTOR.ADDRESS4>     =      PACS.DEBTOR.ADDRESS4
                R.PAC<PACS8.DEBTOR.ADDRESS5>     =      PACS.DEBTOR.ADDRESS5
                R.PAC<PACS8.DEBTOR.STREET.NAME>  =      PACS.DEBTOR.STREET.NAME
                R.PAC<PACS8.DEBTOR.BUILDING.NO>  =      PACS.DEBTOR.BUILDING.NO
                R.PAC<PACS8.DEBTOR.POST.CODE>    =      PACS.DEBTOR.POST.CODE
                R.PAC<PACS8.DEBTOR.TOWN.NAME>    =      PACS.DEBTOR.TOWN.NAME
                R.PAC<PACS8.DEBTOR.COUNTRY.SUB.DIVISION> =      PACS.DEBTOR.COUNTRY.SUB.DIVISION
                R.PAC<PACS8.DEBTOR.COUNTRY>      =      PACS.DEBTOR.COUNTRY
                R.PAC<PACS8.DEBTOR.IDENT.TYPE1>  =      PACS.DEBTOR.IDENT.TYPE1
                R.PAC<PACS8.DEBTOR.IDENT.NO1>    =      PACS.DEBTOR.IDENT.NO1
                R.PAC<PACS8.DEBTOR.IDENT.TYPE2>  =      PACS.DEBTOR.IDENT.TYPE2
                R.PAC<PACS8.DEBTOR.IDENT.NO2>    =      PACS.DEBTOR.IDENT.NO2
                R.PAC<PACS8.DEBTOR.IDENT.TYPE3>  =      PACS.DEBTOR.IDENT.TYPE3
                R.PAC<PACS8.DEBTOR.IDENT.NO3>    =      PACS.DEBTOR.IDENT.NO3
                R.PAC<PACS8.DEBTOR.IDENT.TYPE4>  =      PACS.DEBTOR.IDENT.TYPE4
                R.PAC<PACS8.DEBTOR.IDENT.NO4>    =      PACS.DEBTOR.IDENT.NO4
                R.PAC<PACS8.DEBTOR.ACCOUNT.NO>   =      PACS.DEBTOR.ACCOUNT.NO
                R.PAC<PACS8.DEBTOR.ACCOUNT.TYPE> =      PACS.DEBTOR.ACCOUNT.TYPE
                R.PAC<PACS8.DEBTOR.PARTY.BIC>    =      PACS.DEBTOR.PARTY.BIC
                R.PAC<PACS8.DEBTOR.PARTY.ID>     =      PACS.DEBTOR.PARTY.ID
                R.PAC<PACS8.DEBTOR.PARTY.BRANCH.ID>      =      PACS.DEBTOR.PARTY.BRANCH.ID
                R.PAC<PACS8.DEBTOR.PARTY.BRANCH.NAME>    =      PACS.DEBTOR.PARTY.BRANCH.NAME
                R.PAC<PACS8.CREDITOR.PARTY.BIC>  =      PACS.CREDITOR.PARTY.BIC
                R.PAC<PACS8.CREDITOR.PARTY.ID>   =      PACS.CREDITOR.PARTY.ID
                R.PAC<PACS8.CREDITOR.PARTY.BRANCH.ID>    =      PACS.CREDITOR.PARTY.BRANCH.ID
                R.PAC<PACS8.CREDITOR.PARTY.BRANCH.NAME>  =      PACS.CREDITOR.PARTY.BRANCH.NAME
                R.PAC<PACS8.CREDITOR.NAME>       =      PACS.CREDITOR.NAME
                R.PAC<PACS8.CREDITOR.ADDRESS1>   =      PACS.CREDITOR.ADDRESS1
                R.PAC<PACS8.CREDITOR.ADDRESS2>   =      PACS.CREDITOR.ADDRESS2
                R.PAC<PACS8.CREDITOR.ADDRESS3>   =      PACS.CREDITOR.ADDRESS3
                R.PAC<PACS8.CREDITOR.ADDRESS4>   =      PACS.CREDITOR.ADDRESS4
                R.PAC<PACS8.CREDITOR.ADDRESS5>   =      PACS.CREDITOR.ADDRESS5
                R.PAC<PACS8.CREDITOR.STREET.NAME>        =      PACS.CREDITOR.STREET.NAME
                R.PAC<PACS8.CREDITOR.BUILDING.NO>        =      PACS.CREDITOR.BUILDING.NO
                R.PAC<PACS8.CREDITOR.POST.CODE>  =      PACS.CREDITOR.POST.CODE
                R.PAC<PACS8.CREDITOR.TOWN.NAME>  =      PACS.CREDITOR.TOWN.NAME
                R.PAC<PACS8.CREDITOR.COUNTRY.SUB.DIVISION>       =      PACS.CREDITOR.COUNTRY.SUB.DIVISION
                R.PAC<PACS8.CREDITOR.COUNTRY>    =      PACS.CREDITOR.COUNTRY
                R.PAC<PACS8.CREDITOR.IDENT.TYPE1>=      PACS.CREDITOR.IDENT.TYPE1
                R.PAC<PACS8.CREDITOR.IDENT.NO1>  =      PACS.CREDITOR.IDENT.NO1
                R.PAC<PACS8.CREDITOR.IDENT.TYPE2>=      PACS.CREDITOR.IDENT.TYPE2
                R.PAC<PACS8.CREDITOR.IDENT.NO2>  =      PACS.CREDITOR.IDENT.NO2
                R.PAC<PACS8.CREDITOR.IDENT.TYPE3>=      PACS.CREDITOR.IDENT.TYPE3
                R.PAC<PACS8.CREDITOR.IDENT.NO3>  =      PACS.CREDITOR.IDENT.NO3
                R.PAC<PACS8.CREDITOR.IDENT.TYPE4>=      PACS.CREDITOR.IDENT.TYPE4
                R.PAC<PACS8.CREDITOR.IDENT.NO4>  =      PACS.CREDITOR.IDENT.NO4
                R.PAC<PACS8.CREDITOR.ACCOUNT.NO> =      PACS.CREDITOR.ACCOUNT.NO
                R.PAC<PACS8.CREDITOR.ACCOUNT.TYPE>=      PACS.CREDITOR.ACCOUNT.TYPE
                R.PAC<PACS8.PURPOSE>              =      PACS.PURPOSE
                R.PAC<PACS8.REMITTANCE.INFO1>    =      PACS.REMITTANCE.INFO1
                R.PAC<PACS8.REMITTANCE.INFO2>    =      PACS.REMITTANCE.INFO2
                R.PAC<PACS8.REMITTANCE.INFO3>    =      PACS.REMITTANCE.INFO3
                R.PAC<PACS8.REMITTANCE.INFO4>    =      PACS.REMITTANCE.INFO4
                R.PAC<PACS8.PACS.TYPE>           =      1
                SERIAL.NO.FINAL = FMT(SERIAL.NO1,"R%7")

                PAC.ID   = T.DATE:'.':SERIAL.NO.FINAL
                GOSUB WRITE.RECORD
                SERIAL.NO1 = SERIAL.NO1 + 1
            END

        END
        FLAG = FLAG + 1
    END ELSE
        EOF = 'END OF FILE'
    END
*    TEXT = "�� ����� ����� �����";CALL REM
    RETURN
**************************************************************
WRITE.RECORD:

    WRITE R.PAC TO FVAR.PAC8 , PAC.ID  ON ERROR
        STOP 'CAN NOT WRITE RECORD ':PAC.ID:' TO FILE ':F.PAC8
    END


    RETURN
**************************************************************
PROGRAM.END:
    TEXT = "�� ����� ":NO.OF.TRANS:"-":PACS.CHARGER.BEARER:" �����";CALL REM
END
