* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.ACCT.SUSP.120(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    COMP  = ID.COMPANY
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CA = 'FBNK.CUSTOMER.ACCOUNT' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    KEY.LIST=""  ; SELECTED=""   ;  ER.MSG=""
    K = 1
    T.SEL = "SELECT ":FN.CU:" WITH (CREDIT.CODE EQ 120) AND COMPANY.BOOK EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CA,KEY.LIST<I>,R.CA,F.CA,E2)

            LOOP
                REMOVE ACC.NO FROM R.CA SETTING POS.ACC
            WHILE ACC.NO:POS.ACC
                CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,ER.AC)
                AC.CATEG = R.AC<AC.CATEGORY>
                AC.AMT   = R.AC<AC.OPEN.ACTUAL.BAL>
                IF (AC.CATEG EQ 9090 OR (AC.CATEG GE 1001 AND AC.CATEG GE 1003) OR (AC.CATEG GE 1101 AND AC.CATEG LE 1599)) THEN
                    ENQ<2,K> = '@ID'
                    ENQ<3,K> = 'EQ'
                    ENQ<4,K> = ACC.NO
                    K++
                END ELSE
                    ENQ<2,K> = '@ID'
                    ENQ<3,K> = 'EQ'
                    ENQ<4,K> = 'DUMMY'
                END
            REPEAT
        NEXT I
    END ELSE
        ENQ<2,K> = '@ID'
        ENQ<3,K> = 'EQ'
        ENQ<4,K> = 'DUMMY'
    END

    RETURN
END
