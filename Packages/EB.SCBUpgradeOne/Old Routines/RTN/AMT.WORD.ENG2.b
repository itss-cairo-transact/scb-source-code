* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*--------------------------
* copy from AMT.WORD.ENG
* edit by nessma
*--------------------------
    SUBROUTINE AMT.WORD.ENG2(AMWD1, CUR.NAME)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*--------------------------------------------------
    I.AMT    = AMWD1
    II.AMT   = AMWD1
    I.AMT    = FIELD(I.AMT,'.',1)
    II.AMT   = FIELD(II.AMT,'.',2)
    LANG     = 'GB'
    N.O.LN   = 1
    LN.LNT   = 100
    FLG      = 0
    O.AMT    = ""
    ERR.MS   = ""
    AMWD1    = ""
    OO.AMT   = ""

    CALL DE.O.PRINT.WORDS(I.AMT,O.AMT,LANG,LN.LNT,N.O.LN,ERR.MS)
    O.AMT  = EREPLACE (O.AMT,"*"," ")
*========================
    OUT.VAR = O.AMT
    XX      = LEN(OUT.VAR)
    FOR II = XX TO 1 STEP -1
        IF OUT.VAR[II,1] EQ ' ' THEN
            OUT.VAR = OUT.VAR[1,II]
        END ELSE
            II = 1
        END
    NEXT II

    IF II.AMT EQ '00' THEN
        OUT.VAR  = "ONLY ": OUT.VAR : CUR.NAME
    END ELSE
        OUT.VAR  = "ONLY ": OUT.VAR : CUR.NAME : " AND " : II.AMT : "/100"
    END

    AMWD1 = OUT.VAR
END
