* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-------------------------------------NI7OOOOOOOOOO-----------------------
    SUBROUTINE BATCH.BR.1(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES


    FN.BATCH = 'FBNK.BILL.REGISTER' ; F.BATCH = '' ; R.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)
    KEY.LISTL=""; SELECTEDL="" ; ER.MSG=""
    IDDD="EG0010001"
** CALL DBR('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,IDDD,N.W.DAY)
**  HH = N.W.DAY
    HH = TODAY
    IF LEN(R.USER<EB.USE.DEPARTMENT.CODE>) EQ 1 THEN
        BNK.BRR = "17000" : R.USER<EB.USE.DEPARTMENT.CODE>
    END ELSE
        BNK.BRR = "1700" : R.USER<EB.USE.DEPARTMENT.CODE>
    END
***************************UPDATED BY REHAM 30/4/2009*************
*   LOCATE 'MATURITY.EXT' IN ENQ<2,1> SETTING RB.POS THEN
*        MATURITY.EXT = ENQ<4,RB.POS>
*  END
*******************************************************************
TODAY = "20091208"
  T.SELL  = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.STA EQ 6 AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND BIL.CHQ.TYPE EQ 10 AND BANK.BR NE ":BNK.BRR:" AND STATUS.DATE EQ ":TODAY
* T.SELL  = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.STA EQ 6 AND DEPT.CODE EQ 32 AND BIL.CHQ.TYPE EQ 10 AND BANK.BR NE ":BNK.BRR:" AND STATUS.DATE EQ ":TODAY
    CALL EB.READLIST(T.SELL,KEY.LISTL,"",SELECTEDL,ER.MSG)
    IF KEY.LISTL THEN
        FOR I = 1 TO SELECTEDL
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LISTL<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS"
    END

*  ENQ.LP = ''
*TEXT = SELECTED ; CALL REM
*    IF SELECTED THEN
*       FOR ENQ.LP = 1 TO SELECTED
**** TEXT = "HIIII"  ; CALL REM
*          ENQ<2,ENQ.LP> = '@ID'
*         ENQ<3,ENQ.LP> = 'EQ'
*        ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
*   NEXT ENQ.LP
* END ELSE
*    ENQ<2,1> = '@ID'
*   ENQ<3,1> = 'EQ'
*  ENQ<4,1> = 'DUMMY'
* END

    RETURN

END
