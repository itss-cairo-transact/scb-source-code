* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE ADVICE.EXT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
    IF ID.NEW NE '' THEN
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1231' THEN
            GOSUB INITIATE
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
            TEXT = "ADVICE.AMMARGIN.DECREASE" ; CALL REM
        END
    END ELSE
        IF ID.NEW EQ '' THEN
            GOSUB INITIATE
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
            TEXT = "ADVICE.AMMARGIN.DECREASE" ; CALL REM
        END
    END
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.EXT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
*    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
***AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NUM = R.LD<LD.CHRG.LIQ.ACCT>

    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NUM,CUSS)
    CALL DBR('ACCOUNT':@FM:AC.DEPT.CODE,AC.NUM,DEPTCODE)
    TEXT = CUSS ; CALL REM
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
*************************UPDATE NI7OOOOO ***********************
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NUM,CUSS)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
***************************************************************

***    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUSS,AC.OFICER)
***    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPTCODE,BRANCH)

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUSS.AC,CO.BOOK)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.BOOK,BRANCH)

    YYBRN = FIELD(BRANCH,'.',2)
***************************************************************
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE = R.LD<LD.FIN.MAT.DATE>
*****AC.NUM = R.LD<LD.CHRG.LIQ.ACCT>
    LG.DATE2= LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    FIN.DATE2= LG.DATE2[7,2]:"/":LG.DATE2[5,2]:"/":LG.DATE2[1,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
****************************************MODIFEID ON 1.7.2012

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,AC.NUM,DR.CAT)
    IF DR.CAT NE '1220' AND DR.CAT NE '9090' AND DR.CAT NE '9091'  THEN
        LG.COM1  = R.LD<LD.CHRG.AMOUNT><1,1>
        LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    END ELSE
        IF DR.CAT EQ '1220' OR DR.CAT EQ '9090' OR DR.CAT EQ '9091' THEN
            LG.COM1 =LOCAL.REF<1,LDLR.MRG.CHRG.AMT,1>
            LG.COM2 =LOCAL.REF<1,LDLR.MRG.CHRG.AMT,2>
        END
    END
*****************************************
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    LG.V.DATE = R.LD<LD.VALUE.DATE>
**************
    DATY= R.LD<LD.DATE.TIME>[1,6]
    DATY=     '20':R.LD<LD.DATE.TIME>[1,6]
    TEXT=DATY:'D';CALL REM
***************
    LG.VALUE.DATE = LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
    LG.VALUE.DATE2= DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    DIFFS =R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
    DIFF = ABS(DIFFS)
******************************************
    IF ID.NEW NE '' THEN
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
        AUTH  = OPERATOR
        REF         = ID.NEW
    END ELSE
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF         = COMI
    END
    LG.CO= R.LD<LD.CO.CODE>

************************
*------------------------------------------
    TOT.TAX = ''
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*    FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
*   CALL OPF(FN.LG,F.LG)
*   CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)

*Line [ 187 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    MYTAX = DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>, @SM)
    FOR I = 1 TO MYTAX
        TOT.TAX += R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES,I>
    NEXT I

*MYTAX1 = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.TAXES,1>
*MYTAX1 = R.LG<SCB.LG.CH.TAX.AMOUNT,1>
*MYTAX2  = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.TAXES,2>
*MYTAX2 = R.LG<SCB.LG.CH.TAX.AMOUNT,2>
*TOT.TAX = MYTAX1 + MYTAX2
*--------------------------------------------
    TOT = DIFF + LG.COM1
    RETURN
*=================================================
BODY:
    PR.HD ="'L'":SPACE(1):"����� :":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):DATY[7,2]:"/":SPACE(2):DATY[5,2]:"/":SPACE(2):DATY[1,4]
    PR.HD :="'L'":SPACE(50):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"����� ���"
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
*** PR.HD ="'L'":SPACE(1):"ADVICE.AMMARGIN.DECREASE.MADD"
    XX = SPACE(80)
* XX<1,1>[46,20] = "����� ��� SCBLG0001"
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:XX<1,1>
    END ELSE
        PR.HD :="'L'":SPACE(2):"/":""
    END

    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
    IF THIRD.ADDR2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    END ELSE
        PR.HD :="'L'":SPACE(2):"/":""
    END

    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO:"(":LG.NAME:")" :"����� �� �� ������� ��� ": FIN.DATE2  :"�����"  : LG.AMT : CRR
    PR.HD :="'L'":CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
*----------
    IF ID.NEW NE '' THEN
        OP.CODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    END ELSE
        IF ID.NEW EQ '' THEN
            OP.CODE =  MYCODE
        END
    END
*----------

    PR.HD :="'L'":SPACE(3):LG.COM1:SPACE(19):"������� ��(����� ������ � �-������ �������� ��": END.COM.DATE :")"
    PR.HD :="'L'":SPACE(3):LG.COM2:SPACE(19): "���� � ������� ������ "

    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    IF OP.CODE = '1242' THEN
        TOTY = DIFF+LG.COM1+LG.COM2 +TOT.TAX
        TOTL = FMT(TOTY, "R2#10")
        PR.HD :="'L'":SPACE(2):TOTL:SPACE(15):" ������������������������"
    END ELSE
        TOTY = LG.COM1+LG.COM2+TOT.TAX
        TOTL = FMT(TOTY,"R2#10")
        PR.HD :="'L'":SPACE(2):TOTL:SPACE(15):" ������������������������"
    END
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
    IF TOTY = '' THEN
        TOTY = 0
    END
    CALL WORDS.ARABIC(TOTY,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
    IF OUT.AMOUNT = '' THEN
        OUT.AMOUNT = 0
    END
*Line [ 271 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 273 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I
*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": LG.VALUE.DATE2

    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):REF
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.EXT"
    CALL LG.ADD(AUTH,LG.CO)
    RETURN
*===================================================
END
