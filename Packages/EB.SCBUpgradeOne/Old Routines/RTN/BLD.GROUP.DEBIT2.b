* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE  BLD.GROUP.DEBIT2(ENQ)
*    PROGRAM BLD.GROUP.DEBIT2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT

    COMP = ID.COMPANY
*----------------------------------------------------------------------**
    ACCT.ID  = ''
    MAX.DATE = ''
    SHIFT    = ''
    ROUND    = ''
    FN.ACC   = 'FBNK.GROUP.DEBIT.INT' ; F.ACC    = '' ; R.ACCT = ''
    CALL OPF(FN.ACC,F.ACC)
*-------------------------------------------*
    T.SEL = "SELECT FBNK.GROUP.DEBIT.INT WITH @ID LIKE ...EGP... AND @ID UNLIKE 99... BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
***--------------------------------------------------------------****
    Z = 1
    PERIOD.ARR = ''
    PERIOD.ARR<1,1> = 1
    PERIOD.ARR<1,2> = 4
    PERIOD.ARR<1,3> = 7
    PERIOD.ARR<1,4> = 10
    TOD.DATE     = TODAY
    TOD.MM       = TOD.DATE[5,2]
    TOD.MM = TOD.MM - 1 + 1

    FOR TT = 1 TO 4
        XX = PERIOD.ARR<1,TT>
        IF XX GT TOD.MM THEN
            PERIOD.INDEX = TT - 1
            TT = 4  ;*BREAK
        END
    NEXT TT

    IF SELECTED THEN
* DEBUG
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACCT, F.ACC ,E11)
            GROUP.1 = FIELD(KEY.LIST<I>,'EGP',1)
            ACC.NO  = GROUP.1:'EGP':FIELD(KEY.LIST<I>,'EGP',2)[1,4]

            GROUP.2 = FIELD(KEY.LIST<I+1>,'EGP',1)
            ACC.NO2 = GROUP.2:'EGP':FIELD(KEY.LIST<I+1>,'EGP',2)[1,4]

            IF ACC.NO NE ACC.NO2  THEN
                DATE.GCI   = FIELD(KEY.LIST<I>,'EGP',2)
                TOD.MM     = TOD.MM + 1 - 1
                DATE.GCI.M =  DATE.GCI[5,2]
                DATE.GCI.M = DATE.GCI.M + 1 - 1
                S.P = PERIOD.ARR<1,PERIOD.INDEX>

                IF DATE.GCI.M GE S.P AND DATE.GCI.M LE S.P+2 THEN
                    FOR LL = I TO 1 STEP -1
                        XX.GROUP.1 = FIELD(KEY.LIST<LL>,'EGP',1)
                        XX.ACC     = GROUP.1:'EGP':FIELD(KEY.LIST<LL>,'EGP',2)[1,4]

                        XX.GROUP.2 = FIELD(KEY.LIST<LL-1>,'EGP',1)
                        XX.ACC2    = GROUP.2:'EGP':FIELD(KEY.LIST<LL-1>,'EGP',2)[1,4]

                        IF XX.ACC NE XX.ACC2  THEN
                            DATE.XX1   = FIELD(KEY.LIST<LL>,'EGP',2)
                            DATE.XX1.M = DATE.XX1[5,2]
                            DATE.XX1.M = DATE.XX1.M + 1 - 1

                            IF DATE.XX1.M GE S.P AND DATE.XX1.M LE S.P+2 THEN
*PRINT KEY.LIST<LL>

                                ENQ<2,Z> = "@ID"
                                ENQ<3,Z> = "EQ"
                                ENQ<4,Z> = KEY.LIST<LL>
                                Z = Z + 1
                                LL = 1  ;*BREAK
                            END
                        END
                    NEXT LL
                END ELSE
                    IF GROUP.1 EQ GROUP.2 THEN
                        *TEXT = KEY.LIST<I>:"***":KEY.LIST<I+1> ; CALL REM

                        IF FIELD(KEY.LIST<I+1>,'EGP',2)[1,4] GT FIELD(KEY.LIST<I>,'EGP',2)[1,4] THEN
                            IF FIELD(KEY.LIST<I+1>,'EGP',1) NE FIELD(KEY.LIST<I+2>,'EGP',1) THEN
                                ENQ<2,Z> = "@ID"
                                ENQ<3,Z> = "EQ"
                                ENQ<4,Z> = KEY.LIST<I+1>
                                Z = Z + 1
                            END
                        END
                    END
                END
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END
    RETURN
*================================================================****
END
