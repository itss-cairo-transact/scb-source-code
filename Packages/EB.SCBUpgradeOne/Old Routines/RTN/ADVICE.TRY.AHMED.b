* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-327</Rating>
*-----------------------------------------------------------------------------
******* WAEL *******
*TO CREATE ADVICE
    SUBROUTINE ADVICE.TRY.AHMED
*    PROGRAM ADVICE.TRY.AHMED

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


    GOSUB CALLDB
    MYVER=''
    MYID = MYCODE:'.':MYTYPE
**CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)
    IF MYCODE = "1111" THEN
        GOSUB INITIATE
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
**TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
    END ELSE
        E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
    END
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.TRY.AHMED'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
*   LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
***************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*   SAM3  = LG.NO[10,4]
*   LG.NO='LG/': SAM:"/": SAM1:"/":SAM2
***************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
*************************************
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
** THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
    THIRD.NAME1=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
******************END OF UPDATED ********************************
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    MARG.AMT = FMT(MARG.AMT,"R2")
    LG.COM1 = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM1 = FMT(LG.COM1,"R2")
    LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    LG.COM2 = FMT(LG.COM2,"R2")
    LG.COM3 = R.LD<LD.CHRG.AMOUNT><1,3>
    LG.COM3 = FMT(LG.COM3,"R2")

    LG.STAMP  = '2.90'
    MARG.PER  = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE  = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    LG.V.DATE = R.LD<LD.VALUE.DATE>
    LG.VALUE.DATE =LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
    CURRRR = R.LD<LD.CURRENCY>

    IF ID.NEW NE '' THEN
        INPUTTER    = R.NEW(LD.INPUTTER)
        INP         = FIELD(INPUTTER,'_',2)
    END ELSE
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
    END

    IF ID.NEW NE '' THEN
        AUTH = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END

    IF ID.NEW EQ '' THEN
        REF         = COMI
    END ELSE
        REF = ID.NEW
    END
    LG.CO = R.LD<LD.CO.CODE>
*-----------------------------------------
**NO TAX MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*-------------------------------[B-------------
    TOT  = MARG.AMT + LG.COM1+LG.COM2+LG.COM3+LG.STAMP
    TOT1 = MARG.AMT + LG.COM1+LG.COM2+LG.COM3
    TOT=FMT(TOT,"R2")
    RETURN
    TEXT = "NEW " ; CALL REM
*=================================================
BODY:
    YYBRN = ''
    PR.HD ="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    XX = SPACE(80)
    XX<1,1>[46,20] = "����� ��� "
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2
    END ELSE
        PR.HD :="'L'":" "
    END
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1  : XX<1,1>
* IF THIRD.ADDR2 THEN
*     PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
* END
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� � �":LG.NO:"(":LG.NAME:")":" � ���� ������� ���� ��� ���������  "
    PR.HD :="'L'":" �����":"**":LG.AMT:"**":CRR:" ������ ��� " :FIN.DATE
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":":CRR:":SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":MARG.AMT:SPACE(11):MARG.PER:"%":" ������� ����� �� ������ ������ "
    IF MARG.PER LT 100 THEN
        PR.HD :="'L'":LG.COM1:SPACE(11): " ������� ��� " : END.COM.DATE
    END ELSE
        IF MARG.PER EQ 100 THEN
            PR.HD :="'L'":LG.COM1:SPACE(11): " ������� �� ������ �������� �� " : END.COM.DATE
        END
    END
    PR.HD :="'L'":LG.COM2:SPACE(12): "������� ����"
    PR.HD :="'L'":LG.COM3:SPACE(12): "������� ������"
    IF CURRRR EQ 'EGP' THEN
        PR.HD :="'L'":LG.STAMP:SPACE(9):"��� ���� "
    END
    IF CURRRR NE 'EGP' THEN
        PR.HD :="'L'":""
    END
    PR.HD :="'L'":"=================================================="
    IF CURRRR EQ 'EGP' THEN
        PR.HD :="'L'":TOT:SPACE(10):" ������������������������"
    END
    IF CURRRR NE 'EGP' THEN
        PR.HD :="'L'":TOT1:SPACE(10):"  ������������������������"
    END
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
*    CALL WORDS.ARABIC(TOT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
    IF CURRRR EQ 'EGP' THEN
        IN.AMOUNT=TOT
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    END
    IF CURRRR NE 'EGP' THEN
        IN.AMOUNT=TOT1
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    END
*Line [ 240 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 242 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I

*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": LG.VALUE.DATE
    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(10):"������":SPACE(10):"������"
    PR.HD :="'L'":AUTH:SPACE(5):REF:SPACE(5):INP
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.TRY.AHMED"
    CALL LG.ADD(AUTH,LG.CO)
    RETURN
*=================================================
END
