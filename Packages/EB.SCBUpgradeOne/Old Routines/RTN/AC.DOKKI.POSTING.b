* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------NI7OOOOOOOOOOOOOOOOOOOO------------------------------------------
    SUBROUTINE AC.DOKKI.POSTING(ENQ)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.CUS='FBNK.CUSTOMER'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC='FBNK.ACCOUNT'
    F.ACC=''
    CALL OPF(FN.ACC,F.ACC)

    T.SEL="SELECT FBNK.ACCOUNT WITH CO.CODE EQ EG0010011 AND CATEGORY EQ 6511 "
**T.SEL="SELECT FBNK.ACCOUNT WITH  CO.CODE EQ EG0010011 AND CATEGORY EQ 6511 AND (CUSTOMER GE 11100035 AND CUSTOMER LE 11100260) BY CUSTOMER"
**T.SEL="SELECT FBNK.ACCOUNT WITH  CO.CODE EQ EG0010011 AND CATEGORY EQ 6511 AND ( CUSTOMER EQ 11100203 OR CUSTOMER EQ 11100035) "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR J = 1 TO SELECTED

            CALL F.READ(FN.ACC,KEY.LIST<J>,R.ACC,F.ACC,E1)
            CUSNO   = R.ACC<AC.CUSTOMER>
            CALL F.READ(FN.CUS,CUSNO,R.CUS,F.CUS,E2)
            POSTING = R.CUS<EB.CUS.POSTING.RESTRICT>
*TEXT = "POS1 : " : POSTING ; CALL REM
* ENQ.LP   = '' ;
* OPER.VAL = ''
*********************
*   FOR I = 1 TO SELECTED
            IF POSTING LT 90 THEN
*TEXT = "POS2 : " : POSTING ; CALL REM
************* IF POSTING EQ '' THEN
                ENQ<2,J> = '@ID'
                ENQ<3,J> = 'EQ'
                ENQ<4,J> = KEY.LIST<J>
            END ELSE
                ENQ<2,J> = '@ID'
                ENQ<3,J> = 'EQ'
                ENQ<4,J> = 'DUUMY'
*END
            END
*  NEXT I
        NEXT J
    END ELSE

        ENQ<2,J> = '@ID'
        ENQ<3,J> = 'EQ'
        ENQ<4,J> = 'DUUMY'

    END
*TEXT = "END" ; CALL REM
**********************
    RETURN
END
