* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.CUS.BR.V.DATE(ENQ)
*    PROGRAM BLD.CUS.BR.V.DATE
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER


    EXECUTE 'COMO ON BLD.CUS.BR.V.DATE'
    FN.BR.DATE = 'F.SCB.CUS.BR.V.DATE' ; F.BR.DATE = ''
    CALL OPF(FN.BR.DATE,F.BR.DATE)
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    T.SEL.BR.DATE = 'SELECT ':FN.BR.DATE
    CALL EB.READLIST(T.SEL.BR.DATE,KEY.LIST.BR.DATE,'',SELECTED.BR.DATE, ERR.BR.DATE)
    K = 0
    FOR I=1 TO SELECTED.BR.DATE
        CALL F.READ(FN.CUS,KEY.LIST.BR.DATE<I>, R.CUS, F.CUS, ETEXT.CUS)

        POS.REST = R.CUS<EB.CUS.POSTING.RESTRICT>
        CRT KEY.LIST.BR.DATE<I>:',':POS.REST
        IF R.CUS AND KEY.LIST.BR.DATE<I>[1,2] NE '99' AND ( POS.REST LT 70 OR POS.REST EQ '' ) THEN
            K++
            ENQ<2,K> = '@ID'
            ENQ<3,K> = 'EQ'
            ENQ<4,K> = KEY.LIST.BR.DATE<I>
            CRT 'ACCEPTED'
        END
    NEXT I

    IF K EQ 0 THEN
        ENQ<2,K> = "@ID"
        ENQ<3,K> = "EQ"
        ENQ<4,K> = "DUMMY"
    END
    EXECUTE 'COMO OFF BLD.CUS.BR.V.DATE'

    RETURN
END
