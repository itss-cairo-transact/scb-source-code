* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BILL.ACC.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)

    IF ( CATEG GT 9000 AND CATEG LT 9999) THEN
        E = '��� ����� ��������� ��������' ; CALL ERR  ; MESSAGE = 'REPEAT'
    END

    IF COMI THEN
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,COMI,CUS.ID)

        IF CUS.ID NE '' THEN
            FF = R.NEW(SCB.BS.DRAWER)

            X = COMI[1,1]

            IF X EQ '0' THEN

                Y = COMI[2,7]

                IF Y NE FF THEN
                    ETEXT = "��� ������ ��� ����� ���� ������" ; CALL STORE.END.ERROR
                END
            END ELSE
                Y = COMI[1,8]
                IF Y NE FF THEN
                    ETEXT =  "��� ������ ��� ����� ���� ������ " ; CALL STORE.END.ERROR
                END
            END

        END

        ACC      = COMI
        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC,CUR)
        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR,CUR1)
        CURRR    = R.NEW(SCB.BS.CURRENCY)
        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CURRR,CUR2)

** IF CUR1 NE CUR2 THEN
**   E = '������ ������' ; CALL ERR  ; MESSAGE = 'REPEAT'
**  END


    END


    RETURN
END
