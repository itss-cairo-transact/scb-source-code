* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.GROUP.CR.2(ENQ)
*    PROGRAM BLD.GROUP.CR.2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT

    COMP = ID.COMPANY
*----------------------------------------------------------------------**
    ACCT.ID  = ''
    MAX.DATE = ''
    SHIFT    = ''
    ROUND    = ''
    FN.ACC   = 'FBNK.GROUP.CREDIT.INT' ; F.ACC    = '' ; R.ACCT = ''
    CALL OPF(FN.ACC,F.ACC)
*-----------------------------------------------------------------------*
    T.SEL = "SELECT FBNK.GROUP.CREDIT.INT WITH @ID LIKE 52... OR @ID LIKE 53... OR @ID LIKE 55... OR @ID LIKE 56...  BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
***--------------------------------------------------------------****
*    DEBUG
    Z = 1
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ACC.NEW = KEY.LIST<I>[1,5]
            KEY.NEW = KEY.LIST<I>
            IF I EQ 1 THEN
                ACC.OLD = ACC.NEW
                KEY.OLD = KEY.NEW
            END

            IF ACC.OLD NE ACC.NEW THEN
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = KEY.OLD
                Z = Z + 1
            END

            IF ( ACC.OLD EQ ACC.NEW ) AND ( I EQ SELECTED ) THEN
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = KEY.LIST<SELECTED>
                Z = Z + 1
            END

            IF ( ACC.OLD NE ACC.NEW ) AND ( I EQ SELECTED ) THEN
                ENQ<2,Z> = "@ID"
                ENQ<3,Z> = "EQ"
                ENQ<4,Z> = KEY.LIST<SELECTED>
                Z = Z + 1
            END

            ACC.OLD = ACC.NEW
            KEY.OLD = KEY.NEW
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END
    RETURN
*================================================================****
END
