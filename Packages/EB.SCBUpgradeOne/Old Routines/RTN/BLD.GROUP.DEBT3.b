* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE  BLD.GROUP.DEBT3(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT

    COMP = ID.COMPANY
*----------------------------------------------------------------------**
    ACCT.ID  = ''
    MAX.DATE = ''
    SHIFT    = ''
    ROUND    = ''
    FN.ACC   = 'FBNK.GROUP.DEBIT.INT' ; F.ACC    = '' ; R.ACCT = ''
    CALL OPF(FN.ACC,F.ACC)
*-------------------------------------------*
    T.SEL = "SELECT FBNK.GROUP.DEBIT.INT WITH @ID UNLIKE ...EGP... AND @ID UNLIKE 99... BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
***--------------------------------------------------------------****
    Z = 1
    PERIOD.ARR = ''
    PERIOD.ARR<1,1> = 1
    PERIOD.ARR<1,2> = 4
    PERIOD.ARR<1,3> = 7
    PERIOD.ARR<1,4> = 10
    TOD.DATE     = TODAY
    TOD.MM       = TOD.DATE[5,2]
    TOD.MM = TOD.MM - 1 + 1

    FOR TT = 1 TO 4
        XX = PERIOD.ARR<1,TT>
        IF XX GT TOD.MM THEN
            PERIOD.INDEX = TT - 1
            TT = 4  ;*BREAK
        END
    NEXT TT

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACCT, F.ACC ,E11)

            L.1 = LEN(KEY.LIST<I>)
            L.2 = LEN(KEY.LIST<I+1>)
            IF L.1 EQ 12 THEN
                ACC.NO  = KEY.LIST<I>[1,4]
            END ELSE
                ACC.NO  = KEY.LIST<I>[1,5]
            END

            IF L.2 EQ 12 THEN
                ACC.NO2  = KEY.LIST<I+1>[1,4]
            END ELSE
                ACC.NO2  = KEY.LIST<I+1>[1,5]
            END

            IF ACC.NO NE ACC.NO2  THEN
                IF LEN(KEY.LIST<I>) EQ 13 THEN
                    DATE.GCI   = KEY.LIST<I>[6,8]
                END ELSE
                    DATE.GCI   = KEY.LIST<I>[5,8]
                END

                TOD.MM     = TOD.MM + 1 - 1
                DATE.GCI.M =  DATE.GCI[5,2]
                DATE.GCI.M = DATE.GCI.M + 1 - 1
                S.P = PERIOD.ARR<1,PERIOD.INDEX>

                IF DATE.GCI.M GE S.P AND DATE.GCI.M LE S.P+2 THEN
                    FOR LL = I TO 1 STEP -1

                        XX.L.1 = LEN(KEY.LIST<LL>)
                        XX.L.2 = LEN(KEY.LIST<LL-1>)
                        IF XX.L.1 EQ 12 THEN
                            XX.ACC  = KEY.LIST<LL>[1,4]
                        END ELSE
                            XX.ACC  = KEY.LIST<LL>[1,5]
                        END

                        IF XX.L.2 EQ 12 THEN
                            XX.ACC2  = KEY.LIST<LL-1>[1,4]
                        END ELSE
                            XX.ACC2  = KEY.LIST<LL-1>[1,5]
                        END

                        IF XX.ACC EQ XX.ACC2  THEN

                            IF LEN(KEY.LIST<LL>) EQ 13 THEN
                                DATE.XX1   = KEY.LIST<LL>[6,8]
                            END ELSE
                                DATE.XX1   = KEY.LIST<LL>[5,8]
                            END

                            DATE.XX1.M = DATE.XX1[5,2]
                            DATE.XX1.M = DATE.XX1.M + 1 - 1

                            IF DATE.XX1.M GE S.P AND DATE.XX1.M LE S.P+2 THEN
                                ENQ<2,Z> = "@ID"
                                ENQ<3,Z> = "EQ"
                                ENQ<4,Z> = KEY.LIST<LL>
                                Z = Z + 1
                            END
                        END
                        IF XX.ACC NE XX.ACC2  THEN

                            IF LEN(KEY.LIST<LL>) EQ 13 THEN
                                DATE.XX1   = KEY.LIST<LL>[6,8]
                            END ELSE
                                DATE.XX1   = KEY.LIST<LL>[5,8]
                            END

                            DATE.XX1.M = DATE.XX1[5,2]
                            DATE.XX1.M = DATE.XX1.M + 1 - 1

                            IF DATE.XX1.M GE S.P AND DATE.XX1.M LE S.P+2 THEN

                                ENQ<2,Z> = "@ID"
                                ENQ<3,Z> = "EQ"
                                ENQ<4,Z> = KEY.LIST<LL>
                                Z = Z + 1
                                LL = 1  ;*BREAK
                            END
                        END
                    NEXT LL
                END ELSE
                    ENQ<2,Z> = "@ID"
                    ENQ<3,Z> = "EQ"
                    ENQ<4,Z> = KEY.LIST<I>
                    Z = Z + 1
                END
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END
    RETURN
*================================================================****
END
