* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.FT.CR.LIST(ENQ.DATA)

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT TEMENOS.BP I_FT.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*--------------------------------------------
    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT  = ""
    CALL OPF(FN.FT,F.FT)
    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = "" ; R.AC = ""
    CALL OPF(FN.AC,F.AC)
    INP = ""
*------------------------------------------------
    LOCATE "INPUTTER" IN ENQ.DATA<2,1> SETTING INP.USER THEN
        INP  = ENQ.DATA<4,INP.USER>
    END

*   SEL.CMD2 = ""
    SEL.CMD2  = "SELECT FBNK.FUNDS.TRANSFER WITH CO.CODE EQ ":ID.COMPANY
    SEL.CMD2 := " AND ( VERSION.NAME EQ ',SCB.AC5'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.AC444'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.CHQ.1'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.CHQ.CANCEL'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.CRT.1'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.CANCEL.CRT'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.AC.CURR'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.AC.FCY'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.CHQ.CUS.AMT'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.LOAN.AC.ETMDAT'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.3'"
    SEL.CMD2 := " OR VERSION.NAME EQ ',SCB.5' )"
    SEL.CMD2 := " AND INPUTTER LIKE ...":INP:"..."
    SELLIST2 = "" ; NOREC2 = "" ; RTNCD.2 = ""
    CALL EB.READLIST(SEL.CMD2,SELLIST2,"",NOREC2,RTNCD.2)
    HH = 1
    IF NOREC2 THEN
        FOR I = 1 TO NOREC2
            CALL F.READ(FN.FT,SELLIST2<I>,R.FT,F.FT,E.FT)
            CALL F.READ(FN.AC,R.FT<FT.CREDIT.ACCT.NO>,R.AC,F.AC,E.AC)
            IF R.AC<AC.CUSTOMER> NE '' THEN
                ENQ.DATA<2,HH> = "@ID"
                ENQ.DATA<3,HH> = "EQ"
                ENQ.DATA<4,HH> = SELLIST2<I>
                HH++
            END
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
*-------------------------------------------------
    RETURN
END
