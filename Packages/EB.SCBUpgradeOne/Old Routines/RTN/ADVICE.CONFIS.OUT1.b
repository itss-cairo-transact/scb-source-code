* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
******* WAEL *******
*-----------------------------------------------------------------------------
* <Rating>-250</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ADVICE.CONFIS.OUT1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


*** GOSUB CALLDB

    IF MYCODE # '1401' AND MYCODE # '1402' AND MYCODE # '1403' THEN
        E = "NOT.VALID.VERSION":" - " ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        GOSUB INITIATE
        GOSUB CALLDB
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
    END
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.CONFIS.OUT1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:
    FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

* IF ID.NEW = '' THEN
    YTEXT = "Enter the L/G. No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
* END ELSE
*    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
* END
    TEXT = COMI ; CALL REM
    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
*LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
********    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
    LG.NO = LOCAL.REF<1,LDLR.REF.LD>
**********  AC.NO = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NO = LOCAL.REF<1,LDLR.CR.NOSTRO.ACCT>
    TEXT = AC.NO ; CALL REM
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSID)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOC.REF)
    THIRD.NAME1=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
******************END OF UPDATED ********************************
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR.ID=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE = LG.DATE[3,2]:"/":LG.DATE[5,2]:"/":LG.DATE[0,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1 = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[3,2]:"/":END.DATE[5,2]:"/":END.DATE[0,4]
    LG.V.DATE = R.LD<LD.VALUE.DATE>
***    LG.VALUE.DATE = LG.V.DATE[3,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[0,4]
    LG.VALUE.DATE = LG.V.DATE[7,2]:'/':LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
** CONFIS.AMT = '10.00'  :CUR
** INT.AMT.1 =10.00
    IN.AMOUNT = CONFIS.AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    INTAMT.T= OUT.AMOUNT : ' ' : CUR : ' ' : ' �����'

    INPUTTER    = R.LD<LD.INPUTTER>
    INP         = FIELD(INPUTTER,'_',2)
    AUTHORISER  = R.LD<LD.AUTHORISER>
    AUTH        = FIELD(AUTHORISER,'_',2)
    REF         = COMI
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*--------------------------------------------
    TOT = MARG.AMT + LG.COM1+LG.COM2
    RETURN
*=================================================
BODY:
    PR.HD ="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :=AC.NO
    PR.HD :="'L'":SPACE(14):DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:SPACE(25):"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    PR.HD :="'L'":" "
*** PR.HD :="'L'":" ����� �������� ���� ���� �������� ��� ������ ������� �����  ."
    PR.HD :="'L'":" ����� �������� ���� ���� �������� ��� ������ ������� �����"
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO:"(":LG.NAME:")"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":: CONFIS.AMT:SPACE(21): " ���� ������ ���� ���� ��� :":LG.NO:"����� �������"
***  PR.HD :="'L'":: CONFIS.AMT:SPACE(21): " ���� ������ ���� ��� ���� ������ ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":CONFIS.AMT:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ���� : " : '2009/09/27'
    PR.HD := "'L'":INTAMT.T
    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):COMI
    HEADING PR.HD

    RETURN
*===================================================
END
