* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
 *   PROGRAM    ACCT.MA.VI
    SUBROUTINE ACCT.MA.VI

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    RETURN
*-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
INITIAL:
*-_-_-_-_-_-
    OPENSEQ "&SAVEDLISTS&" , "ACCT.MA.VI.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ACCT.MA.VI.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ACCT.MA.VI.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ACCT.MA.VI CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ACCT.MA.VI File IN &SAVEDLISTS&'
        END
    END
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC, F.AC)
    FN.CUS = "FBNK.CUSTOMER"  ; F.CUS = ""
    CALL OPF(FN.CUS, F.CUS)
    FN.CAR = "FBNK.CARD.ISSUE"  ; F.CAR = ""
    CALL OPF(FN.CAR,F.CAR)
    FN.CA = "FBNK.CUSTOMER.ACCOUNT" ; F.CA = ''
    CALL OPF(FN.CA,F.CA)
    T.SEL    = "" ; KEY.LIST = "" ; SELECTED = "" ; ERR.SEL = ""

    VISA   = "VI..."
    MASTER = "MA..."

    AC.AMT  = 0
    CRD.AMT = 0
    RETURN

*-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
PRINT.HEAD:
*-_-_-_-_-_-

    HEAD.DESC  = "CUSTOMER.ID":","
    HEAD.DESC := "NAME":","
    HEAD.DESC := "ADDRESS":","
    HEAD.DESC := "PHONE1":","
    HEAD.DESC := "PHONE2":","
    HEAD.DESC := "E-MAIL":","
    HEAD.DESC := "NATIONAL ID":","
    HEAD.DESC := "BRANCH":","
    HEAD.DESC := "CARD.ID":","
    HEAD.DESC := "EXPIRY DATE":","
    HEAD.DESC := "CARD ACC NO":","
    HEAD.DESC := "CARD ACCT BALANCE":","
    HEAD.DESC := "ACCOUNT ID":","
    HEAD.DESC := "BALANCE":","
    HEAD.DESC := "CURRENCY"
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
PROCESS:
*-_-_-_-_-_-

    T.SEL  = "SELECT FBNK.CARD.ISSUE WITH (@ID LIKE ":VISA: " OR @ID LIKE ":MASTER:")"
*   T.SEL := " WITH CANCELLATION.DATE EQ '' "
    T.SEL := " WITH CARD.CODE IN (101 104 201 204 501 504 601 604 ) BY ACCOUNT "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.SEL)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CARD.ID = KEY.LIST<I>
            CALL F.READ(FN.CAR,CARD.ID,R.CAR,F.CAR,ERR.CAR)
            ACC.NO = R.CAR<CARD.IS.ACCOUNT>
            EXP.DATE1 = R.CAR<CARD.IS.EXPIRY.DATE>
            EXP.DATE = EXP.DATE1[7,2]:'/':EXP.DATE1[5,2]:"/":EXP.DATE1[1,4]

            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
            CALL DBR ('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,ACC.NO,CRD.AMT)
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ERR.CUS)
            CUS.NAME  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,1>
            CUS.NAME := " ":R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,2>
            CUS.NAME := " ":R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,3>
            IF CUS.NAME EQ '' THEN
                CUS.NAME = R.CUS<EB.CUS.SHORT.NAME>
            END

            ADDRESS  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,1>
            ADDRESS := " "
            ADDRESS := R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,2>
            SMS = R.CUS<EB.CUS.LOCAL.REF><1,CULR.CU.SMS>
            TELEPHONE  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
            TELEPHONE := ","
            TELEPHONE := R.CUS<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>
            NAT.ID = R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            EMAIL  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
            BRANCH = R.CUS<EB.CUS.ACCOUNT.OFFICER>
            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH,BR.NAME)
            BR.NAME = FIELD(BR.NAME,".",2)
            CALL F.READ(FN.CA,CUS.ID,R.CA,F.CA,E2)

*Line [ 142 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            INDX = DCOUNT(R.CA, @FM)
            FOR HH = 1 TO INDX
                ACC.ID = R.CA<HH>
                CALL F.READ(FN.AC,ACC.ID,R.AC,F.AC,ER.AC)
                AC.CATEG = R.AC<AC.CATEGORY>
                IF AC.CATEG EQ 1535 OR AC.CATEG EQ 1001 OR AC.CATEG EQ 1003 OR (AC.CATEG GE 1005 AND AC.CATEG LE 1007) OR AC.CATEG EQ 1009 OR ( AC.CATEG GE 5601 AND AC.CATEG LE 6508 ) OR ( AC.CATEG GE 6511 AND AC.CATEG LE 6517 ) THEN
                    ACC.ID  = R.CA<HH>
                    AC.AMT  = R.AC<AC.ONLINE.ACTUAL.BAL>
                    AC.AMT  = DROUND(AC.AMT,'2')
                    CUR  = R.AC<AC.CURRENCY>
                    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,AC.CUR)

                    BB.DATA    = ""
                    BB.DATA    = CUS.ID:","
                    BB.DATA   := CUS.NAME:","
                    BB.DATA   := ADDRESS:","
                    BB.DATA   := TELEPHONE:","
                    BB.DATA   := EMAIL:","
                    BB.DATA   := NAT.ID:","
                    BB.DATA   := BR.NAME:","
                    BB.DATA   := "'":CARD.ID[6,16]:","
                    BB.DATA   := EXP.DATE:","
                    BB.DATA   := "'":ACC.NO:","
                    BB.DATA   := CRD.AMT:","
                    BB.DATA   := "'":ACC.ID:","
                    BB.DATA   := AC.AMT:","
                    BB.DATA   := AC.CUR

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                END

            NEXT HH

            CUS.ID        = ""
            CUS.NAME      = ""
            ADDRESS       = ""
            TELEPHONE     = ""
            EMAIL         = ""
            NAT.ID        = ""
            BR.NAME       = ""
            CARD.ID       = ""
            EXP.DATE      = ""
            ACC.NO        = ""
            ACC.ID        = ""
            CRD.AMT       = ""
            AC.AMT        = ""
        NEXT I
    END

    RETURN

END
