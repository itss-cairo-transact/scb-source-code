* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE ADVICE.CANCEL.DEBIT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB CALLDB
    MYID       = MYCODE:'.':MYTYPE
    IF MYCODE = "1301"  OR MYCODE EQ "1302" OR MYCODE EQ "1309" THEN
        GOSUB INITIATE
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "ADVICE CREATED SUCCESFULLY" ; CALL REM
    END
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.CANCEL.DEBIT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:
    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF        = R.LD<LD.LOCAL.REF>
    BENF1            = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2            = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    LG.NO            = LOCAL.REF<1,LDLR.OLD.NO>
    AC.NO            = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    THIRD.NO         = LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSS)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
    THIRD.NAME1      = LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2      = LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1      = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2      = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
******************END OF UPDATED ********************************
    YYBRN            = FIELD(BRANCH,'.',2)
    DATY             = TODAY
    LG.TYPE          = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME          = TYPE.NAME
    LG.AMT           = R.LD<LD.AMOUNT>
    CUR              = R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE          = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE         = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    MARG.AMT         = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1          = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2          = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER         = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE         = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE     = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    LG.V.DATE        = R.LD<LD.LOCAL.REF><1,LDLR.INSTAL.DUE.DATE>
    LG.VALUE.DATE    = LG.V.DATE
    INPUTTER         = R.LD<LD.INPUTTER>
    INP              = FIELD(INPUTTER,'_',2)
******************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF = COMI
    END
    LG.CO= R.LD<LD.CO.CODE>
******************
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
*--------------------------------------------
    TOT = MARG.AMT +LG.COM1+LG.COM2
    RETURN
*=================================================
BODY:
    PR.HD  ="'L'":SPACE(6):"����� ���"
    PR.HD :="'L'":"���� �       :":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������      :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������   :":AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� ����   :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������      :":CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������  :":THIRD.NAME1
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(15) :THIRD.NAME2
    END ELSE
        PR.HD :="'L'":" "
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������      :":THIRD.ADDR1
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO:"":LG.NAME:""
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(25):" �������������"
    PR.HD :="'L'":" "
    IF MYCODE EQ '1304' THEN
        PR.HD :="'L'":SPACE(6): LG.COM2:SPACE(19): "����� �����"
    END ELSE
        PR.HD :="'L'":''
    END
    LG.COM = LG.COM1
    PR.HD :="'L'":SPACE(6): LG.COM1:SPACE(19): "���� � ������� ������ "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":SPACE(6):LG.COM1:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    IN.AMOUNT = LG.COM1
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    IF IN.AMOUNT EQ 0 OR IN.AMOUNT EQ '' THEN
        OUT.AMOUNT = 0
    END
*Line [ 173 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 175 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I
    PR.HD :="'L'":SPACE(6) :"�� ��� ���� ������"
    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":SPACE(6):INP:SPACE(12):AUTH:SPACE(12):REF
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.CANCEL.DEBIT"
    CALL LG.ADD(AUTH,LG.CO)
    RETURN
*=================================================
END
