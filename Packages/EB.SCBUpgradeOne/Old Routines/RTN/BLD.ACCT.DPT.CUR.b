* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.ACCT.DPT.CUR(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*-------------------------------------------------------------------------
    COMP = ID.COMPANY
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.AC:" WITH (CATEGORY GE 1101 AND CATEGORY LE 1599)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CUS.ID = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            CR.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>

            INT.AC = R.AC<AC.INTEREST.COMP.ACCT>
            CALL DBR ('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,INT.AC,AMT1)
            CALL DBR ('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,KEY.LIST<I>,AMT2)
            AMT = AMT1 + AMT2
*=================================================================

*            IF CR.CODE EQ '110' OR CR.CODE EQ '120' AND AMT NE '' AND AMT NE '0' THEN
             IF CR.CODE EQ '110' THEN
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<I>
            END ELSE
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'
            END

        NEXT I
    END
    RETURN
END
