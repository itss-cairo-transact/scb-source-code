* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>28</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACH.PACS008.FT.OUT.FILL
*   PROGRAM ACH.PACS008.FT.OUT.FILL

* CREATED 14-12-2016
* BY NOHA HAMED

*-----------------------------------
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.REASONS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h


    GOSUB OPEN.FILES
    GOTO  PROGRAM.END

**********************************************

OPEN.FILES:
*==========
    T.DATE = TODAY
    TT     = TIMEDATE()
    KK     = TT[1,8]
    T.DATE.TIME = T.DATE:KK[1,2]:KK[4,2]:KK[7,2]

    FN.PAC8 = "F.SCB.ACH.PACS008"; F.PAC8 = '' ;FVAR.PAC8 = ''
    CALL OPF(FN.PAC8,F.PAC8)


    R.PAC     = ''
    R.PAC8    = ''
    R.REV.PAC = ''

*********GET HEADER DATA **************************************
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='INAU' THEN
        Y.SEL = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ...":T.DATE:"... BY-DSND REMITTANCE.INFO5"
        CALL EB.READLIST(Y.SEL,PAC.LIST, "", PAC.SEL, PAC.ER)

        IF PAC.LIST THEN
            SERIAL.NO = PAC.SEL
        END
        ELSE
            SERIAL.NO = 0
        END

*SETT.DATE       = R.NEW(FT.CREDIT.VALUE.DATE)
        SETT.DATE       = T.DATE
        SERIAL.NO.FINAL = SERIAL.NO + 1
        BATCH.SERIAL    = FMT(SERIAL.NO.FINAL,"R%7")
        TRANSACTION.ID  = 'TXSUCAEGCX-':T.DATE.TIME:'000-':BATCH.SERIAL
        CREATION.DATE   = T.DATE[1,4]:'-':T.DATE[5,2]:'-':T.DATE[7,2]:'T':TT[1,8]:'.00000'
        SETTLEMENT.DATE = SETT.DATE[1,4]:'-':SETT.DATE[5,2]:'-':SETT.DATE[7,2]

*****************GET CUS AND ACC *****************************

        DB.ACC.NO        = R.NEW(FT.DEBIT.ACCT.NO)
        IF DB.ACC.NO[1,1] EQ '0' THEN
            DB.CUS           = DB.ACC.NO[2,7]
        END
        ELSE
            DB.CUS = DB.ACC.NO[1,8]
        END
        CR.ACC.NO        = R.NEW(FT.BEN.ACCT.NO)
        IF CR.ACC.NO[1,1] EQ '0' THEN
            CR.CUS           = CR.ACC.NO[2,7]
        END
        ELSE

            CR.CUS           = CR.ACC.NO[1,8]
        END
*******GET CUS NAME*******************************************

        CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,DB.CUS,DB.CUS.NAME)
*CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,CR.CUS,CR.CUS.NAME)
        CR.CUS.NAME = R.NEW(FT.BEN.CUSTOMER)
*******GET BRN CODE*******************************************

*CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,DB.CUS,DB.BRN.CODE)
*DB.BRN.CODE      =  R.NEW(FT.LOCAL.REF)<1,FTLR.DEBIT.BRANCH>
        CR.BRN.CODE   =  R.NEW(FT.LOCAL.REF)<1,FTLR.BANK.BR.ACH>
        REASON.CODE   =  R.NEW(FT.LOCAL.REF)<1,FTLR.ACH.FT.REASON>
        CALL DBR('SCB.REASONS':@FM:RES.REASON.NAME,REASON.CODE,REASON)
*******GET RECEIVER BANK CODE*********************************

        BNK.CODE        =  R.NEW(FT.LOCAL.REF)<1,FTLR.BANK.ACH>
*IF LEN(BNKK.CODE) LT 11 THEN
*   BNK.CODE =  FMT(BNKK.CODE, "L2,&X#11")
*END
        CALL DBR('SCB.BANK':@FM:SCB.BAN.RESERVED.FIELD1,BNK.CODE,BIC.CODE)
*=====================FILL PACS008=========================

*R.PAC<PACS8.BATCH.ID>            =      BATCH.ID
        R.PAC<PACS8.CREATION.DATE.TIME>  =      CREATION.DATE
        R.PAC<PACS8.REQ.SETTLEMENT.DATE> =      SETTLEMENT.DATE
        R.PAC<PACS8.BATCH.TYPE>          =      'CRT'
        R.PAC<PACS8.TRN.DATE>            =      ''
        R.PAC<PACS8.INSTRUCTION.IDENT>   =      ID.NEW
        R.PAC<PACS8.END.OF.END.IDENT>    =      TRANSACTION.ID
        R.PAC<PACS8.TRANSACTION.ID>      =      TRANSACTION.ID
        R.PAC<PACS8.AMOUNT>              =      R.NEW(FT.DEBIT.AMOUNT)
        R.PAC<PACS8.CURRENCY>            =      'EGP'
        R.PAC<PACS8.CHARGER.BEARER>      =      'SLEV'
        R.PAC<PACS8.DEBTOR.NAME>         =      DB.CUS.NAME
        R.PAC<PACS8.DEBTOR.ACCOUNT.NO>   =      DB.ACC.NO
        R.PAC<PACS8.DEBTOR.ACCOUNT.TYPE> =      'CACC'
        R.PAC<PACS8.CREDITOR.NAME>       =      CR.CUS.NAME
        R.PAC<PACS8.CREDITOR.ACCOUNT.NO> =      CR.ACC.NO
        R.PAC<PACS8.CREDITOR.ACCOUNT.TYPE>      =  'CACC'
        R.PAC<PACS8.PURPOSE>                    =  REASON
        R.PAC<PACS8.REMITTANCE.INFO1>    =      CR.BRN.CODE
        R.PAC<PACS8.REMITTANCE.INFO2>    =      'FTO'
        R.PAC<PACS8.REMITTANCE.INFO5>    =      SERIAL.NO.FINAL
        R.PAC<PACS8.PACS.TYPE>           =      '2'
        R.PAC<PACS8.CREDITOR.PARTY.BIC>  =      BIC.CODE
        R.PAC<PACS8.DEBTOR.PARTY.BIC>    =      'SUCAEGCXXXX'
        PAC.ID   = T.DATE.TIME:'000-':BATCH.SERIAL

        CALL F.WRITE (FN.PAC8,PAC.ID,R.PAC)
    END
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='RNAU' THEN

        TT.SEL = "SELECT F.SCB.ACH.PACS008 WITH INSTRUCTION.IDENT EQ ":ID.NEW:" AND TRN.DATE EQ ''"
        CALL EB.READLIST(TT.SEL,PAC.REV.LIST, "", PAC.REV.SEL, PAC.REV.ER)

        IF PAC.REV.LIST THEN
            ACH.ID = PAC.REV.LIST<1>
            *TEXT = "ACH.ID= ":ACH.ID; CALL REM
            CALL F.READ(FN.PAC8,ACH.ID,R.REV.PAC,F.PAC8,E.REV)
            *TEXT = "ACH.ID= ":ACH.ID; CALL REM
            R.REV.PAC<PACS8.REMITTANCE.INFO2>    =      'FTOR'
            CALL F.WRITE (FN.PAC8,ACH.ID,R.REV.PAC)
        END
        ELSE
            TEXT = "You can't reverse ACH for this Transaction"; CALL REM
        END

    END
    RETURN
**************************************************************
PROGRAM.END:
    RETURN
END
