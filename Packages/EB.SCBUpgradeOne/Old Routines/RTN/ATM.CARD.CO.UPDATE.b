* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------MAI SAAD------------------------------------------------------------
* <Rating>1623</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  ATM.CARD.CO.UPDATE
*PROGRAM  ATM.CARD.CO.UPDATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

    GOSUB INITIALISE
    RETURN

INITIALISE:
*-----------------**--------------00----------------**-------------------*
    FN.CARD = 'FBNK.CARD.ISSUE'   ; F.CARD = ''
    CALL OPF(FN.CARD , F.CARD)

    DEFFUN SHIFT.DATE( )
    XX = 0
****************************************************
    DIR.NAME2 = '&SAVEDLISTS&'
    NEW.FILE2 = "CARDS.ERR.":TODAY

    OPENSEQ DIR.NAME2,NEW.FILE2 TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME2:' ':NEW.FILE2
        HUSH OFF
        PRINT 'FILE ':NEW.FILE2:' DELETE FROM ':DIR.NAME2
    END
    OPENSEQ DIR.NAME2, NEW.FILE2 TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE2:' CREATED IN ':DIR.NAME2
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE2:' to ':DIR.NAME2
        END
    END
**-------------------------------------------------------------------
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')

    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "CARD.ISSUE"
    OFS.OPTIONS      = "CARDS.CODE"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    COMMA = ","
    NEW.CODE = '901'
    T.SEL= "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ATMC.51186498... "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SELECTED = ":SELECTED;CALL REM;
*LOOP WHILE NOT(EOF)
    FOR I = 1 TO SELECTED
        CARD.ID  = KEY.LIST<I>
        *TEXT = "CARD ID  = ":CARD.ID ;CALL REM;
        CALL F.READ(FN.CARD, CARD.ID, R.CARD ,F.CARD, READ.ER.CARD)
** ----------------------- OFS ----------------
        OFS.MESSAGE.DATA :="LOCAL.REF:5=":NEW.CODE:COMMA 
        COMP =  R.CARD<CARD.IS.CO.CODE>
      *  TEXT = "COMP  = ":COMP ;CALL REM;
        BRN    = COMP[8,2]
        COMPO  = COMP
        OFS.USER.INFO     = "INPUTT":BRN:"//":COMPO
        OFS.IDD = CARD.ID
        F.PATH  = FN.OFS.IN
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.IDD:COMMA:OFS.MESSAGE.DATA
        OFS.ID  = "CARD.":CARD.ID:"-":TODAY
        IF BRN NE '' THEN
     *       TEXT = "IN CONDITION OF BRN"; CALL REM;
            XX = XX + 1
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-CARD.CODE' ON ERROR TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR TEXT = " ERROR ";CALL REM ; STOP
        END ELSE    ;**Else of not write**
            *TEXT = "IN ERROR CONDITION OF BRN"; CALL REM;
            CARD.DATA = CARD.ID
            CARD.DATA := "|CARD CODE="
            CARD.DATA :=  NEW.CODE
            CARD.DATA := "|Branch="
            CARD.DATA :=  BRN
            NM = NM + 1
            WRITESEQ CARD.DATA TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':CARD.DATA
            END
        END
        OFS.MESSAGE.DATA = ''
    NEXT I
*REPEAT

    TEXT = '�� �������� �� ��������' ; CALL REM
    TEXT = '������ �������� �������':XX ; CALL REM
    TEXT = '��� �������� ���� �� ���������':NM ; CALL REM
    RETURN
***------------------------------------------------------------------***
END
