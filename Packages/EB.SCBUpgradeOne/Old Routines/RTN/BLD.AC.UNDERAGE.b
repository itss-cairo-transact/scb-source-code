* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.AC.UNDERAGE (ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.UNDERAGE

    COMP = C$ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    B=""
    CUST=""

    T.SEL ="SELECT FBNK.ACCOUNT WITH CO.CODE EQ ":COMP:" AND CATEGORY IN (6501 6502 6503 6504 6511 6512) AND INT.NO.MONTHS NE '' AND INT.NO.MONTHS NE 0 AND INT.NO.MONTHS NE ' '"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
    KK = 1
    IF SELECTED  THEN
        FOR I = 1 TO SELECTED
            CALL DBR('SCB.ACCT.UNDERAGE':@FM:ACCT.UNDER.ACCT.FLAG,KEY.LIST<I>,AC.FLG)

            IF AC.FLG EQ '' THEN
                ENQ<2,KK> = "@ID"
                ENQ<3,KK> = "EQ"
                ENQ<4,KK> = KEY.LIST<I>
                KK ++
            END

        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END
*******************************************************************************************
    RETURN
END
