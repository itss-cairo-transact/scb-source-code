* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>544</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE APPL.TRN.INS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PROTOCOL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.APPLICATION.NAME
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.APPL.TRN.COUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    PRV_COMP_ID = ''
    GOSUB INITIATE

*  GOSUB PRINT.HEAD
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
**    TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='APPL.TRN.INS'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*=============================================================
CALLDB:
    FN.PROT.1 = 'F.PROTOCOL'             ; F.PROT.1 = '' ;R.PROT.1 = ''
    FN.PROT = 'F.PROTOCOL'             ; F.PROT = ''
    FN.APPL = 'F.SCB.APPLICATION.NAME' ; F.APPL = ''
    PRV_APPL =''
    NO.TRN = ''
    NO.TRN.1 = 0
    F.COUNT = "F.SCB.APPL.TRN.COUNT"; FVAR.COUNT=''; R.FILL = ''

**    OPEN F.COUNT TO FVAR.COUNT ELSE
**        TEXT = "ERROR OPEN FILE" ; CALL REM
**        RETURN
**    END

    CALL OPF(F.COUNT,FVAR.COUNT)

    CALL OPF(FN.PROT,F.PROT)
    CALL OPF(FN.APPL,F.APPL)

    T.SEL = "SELECT F.PROTOCOL WITH  LEVEL.FUNCTION LIKE ...A... AND ID NE '' BY COMPANY.ID BY APPLICATION BY TIME"
*    T.SEL = "SELECT F.PROTOCOL WITH  COMPANY.ID EQ EG0010013 AND LEVEL.FUNCTION LIKE ...A... AND ID NE '' BY APPLICATION"
*   T.SEL = "SELECT F.PROTOCOL WITH  PROCESS.DATE EQ 20091207 AND COMPANY.ID EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND LEVEL.FUNCTION LIKE ...A... AND ID NE '' BY APPLICATION"
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    APPL = ''    ; PRV_APPL = '';  J = 0
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
***    TEXT =SELECTED ;CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.PROT,KEY.LIST<I>,R.PROT,F.PROT,E1)
* APPL = R.PROT<EB.PTL.APPLICATION>
            APPL = FIELD(R.PROT<EB.PTL.APPLICATION>,",",1)
            IF APPL EQ 'LD.LOANS.AND.DEPOSITS' THEN
*===================================
                FINDSTR 'LG' IN R.PROT<EB.PTL.APPLICATION> SETTING FMS,VMS THEN
                    APPL = 'LG'
                END
                ELSE
                    APPL = R.PROT<EB.PTL.APPLICATION>
                END
*===================================
            END
            IF  APPL EQ 'LD.LOANS.AND.DEPOSITS,SCB.OPEN.UPDATE1' OR APPL EQ 'LD.LOANS.AND.DEPOSITS,SCB.ELIQ.REV.DEP' THEN
                APPL = 'LD.LOANS.AND.DEPOSITS,SCB.OPEN.TERM.11'
            END
            IF APPL EQ 'COLLATERAL.RIGHT' THEN
                APPL = 'COLLATERAL'
            END
            IF APPL EQ 'SCB.CHQ.RETURN' THEN
                APPL = 'SCB.CHQ.RETURN.NEW'
            END

            IF (PRV_APPL NE APPL AND NO.TRN GT 0 ) OR I EQ SELECTED  THEN
*=================================================
                PRINT
                T.SEL1 = "SELECT F.SCB.APPLICATION.NAME WITH APPL.NAME EQ '":PRV_APPL:"'"
                CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
                CALL F.READ(FN.APPL,KEY.LIST1<1>,R.APPL,F.APPL,E1)
                IF PRV_APPL EQ 'LD.LOANS.AND.DEPOSITS,SCB.CD.OPEN1' THEN
                    PARM = 2
                END
                ELSE
                    PARM = 1
                END

                IF SELECTED1 EQ 1 THEN
                    PRV_APPL = R.APPL<APPL.ARABIC.NAME>
                END
                NO.TRN = NO.TRN/PARM
*=======================================================
                R.FILL<COUNT.APPL.NAME>      = PRV_APPL
                R.FILL<COUNT.ARABIC.NAME>    = ''
                R.FILL<COUNT.ENGLISH.NAME>   = PRV_APPL
                R.FILL<COUNT.NO.OF.TRN>      = NO.TRN
                R.FILL<COUNT.BRANCH.NO>      = R.PROT<EB.PTL.COMPANY.ID>[8,2]
                R.FILL<COUNT.RECORD.STATUS>       = 'INAU'
                R.FILL<COUNT.CURR.NO>             = '1'
                R.FILL<COUNT.INPUTTER>            = 'APPL'
                R.FILL<COUNT.CO.CODE>             = R.PROT<EB.PTL.COMPANY.ID>
                R.FILL<COUNT.DATE.TIME>           = TODAY:R.PROT<EB.PTL.TIME>[1,4]

                ID.COUNT = TODAY:R.PROT<EB.PTL.COMPANY.ID>[8,2]:FMT(I,"R%4")

**                WRITE R.FILL TO FVAR.COUNT , ID.COUNT  ON ERROR
**                    STOP 'CAN NOT WRITE RECORD ':ID.COUNT:' TO FILE ':F.COUNT
**                END

                CALL F.WRITE(F.COUNT,ID.COUNT,R.FILL)
                CALL JOURNAL.UPDATE(ID.COUNT)

*=====================================================
                XX = SPACE(120)
                XX<1,1>[5,50]   = PRV_APPL
                XX<1,1>[60,5]   = NO.TRN
                NO.TRN = 0
                PRINT XX<1,1>
*==============================================
                J = I + 1
                IF J LE SELECTED AND J NE 1 THEN
                    CALL F.READ(FN.PROT,KEY.LIST<J>,R.PROT,F.PROT,E1)
                    IF PRV_COMP_ID NE R.PROT<EB.PTL.COMPANY.ID>  THEN
                        T.SEL.1 = "SELECT F.PROTOCOL WITH  LEVEL.FUNCTION LIKE ...A... AND ID NE '' AND COMPANY.ID EQ ":PRV_COMP_ID:" BY TIME"
                        CALL EB.READLIST(T.SEL.1,KEY.LIST1,"",SELECTED1,ER.MSG1)
                        CALL F.READ(FN.PROT.1,KEY.LIST1<SELECTED1>,R.PROT.1,F.PROT.1,E1.1)
                        PRINT STR('=',120)
* ===================================
                        BRN.NO = PRV_COMP_ID[2]
                        BRN.NO = TRIM(BRN.NO ,'0',"L")
                        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.TELEX.NO,BRN.NO,EMP.NO)
                        PRINT ; PRINT SPACE(10):"����� ������ ����� = ":NO.TRN.1:" �����":SPACE(15):"��� ��� �����  ":R.PROT.1<EB.PTL.TIME>[1,2]:":":R.PROT.1<EB.PTL.TIME>[3,2]:":":R.PROT.1<EB.PTL.TIME>[5,2]:SPACE(10):"�����������  ":EMP.NO
                        NO.TRN.1 = 0
                        PRV_COMP_ID = R.PROT<EB.PTL.COMPANY.ID>
                        GOSUB PRINT.HEAD
                    END
                END
            END
            PRV_APPL = APPL
            NO.TRN.1 = NO.TRN.1 + 1
            NO.TRN = NO.TRN + 1

        NEXT I
        PRINT STR('=',120)
        PRINT ; PRINT SPACE(10):"����� ������  ����� = ":NO.TRN.1:" �����"

        PRINT STR('=',120)
        PRINT ; PRINT SPACE(10):"������ ������ �����  = ":SELECTED:" �����"
        PRINT STR('=',120)
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,PRV_COMP_ID,BRANCH)
*  YYBRN = FIELD(BRANCH,'.',2)
    YYBRN = BRANCH
    DATY = TODAY

    T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):"���� ���� �������� ������� ������  ":T.DAY

*   PR.HD :="'L'":SPACE(50): " �� ������ " :ST.DATE :" ��� " : EN.DATE
    PR.HD :="'L'":SPACE(40):STR('_',55)


    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(20):" �����" :SPACE(35):" �����"
*    PR.HD := "'L'" : " ��� ������  " : SPACE (3) : " ��� ������  "  : SPACE (8) : " ����� ������   " : SPACE(3) : "  ����� ����� ������� " :  SPACE(3) : " ������  " : SPACE(2) :"�.������� - �.�������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
