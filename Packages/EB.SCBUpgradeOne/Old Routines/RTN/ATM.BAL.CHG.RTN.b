* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE ATM.BAL.CHG.RTN(OUT.MSG)
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_AT.ISO.COMMON
    $INSERT T24.BP I_F.OFS.SOURCE
    $INSERT T24.BP I_F.ATM.PARAMETER

    DE.48 = AT$INCOMING.ISO.REQ(48)

    TYPE.123 = '123'
    TYPE.Free = 'Free'
    Type.OnUs = 'OnUs'
    Type.Local = 'Local'
    Type.Local = 'Int'

    IF DE.48 NE '' THEN
        IF RIGHT(DE.48,4) EQ 'Free' THEN
            CHG.AMT = '3'
            DEBIT.ACCT = 'EGP1124300010099'
        END
        IF RIGHT(DE.48,4) EQ 'OnUs' THEN
            RETURN
        END
        IF RIGHT(DE.48,5) EQ 'Local' THEN
            CHG.AMT = 'EGP 3'
* 102 field will contain the Customer account number.
            CHG.AMT = '4.63'
            DE.102 = AT$INCOMING.ISO.REQ(102)
            DEBIT.ACCT = DE.102

        END

        IF RIGHT(DE.48,3) EQ '123' THEN
            DE.102 = AT$INCOMING.ISO.REQ(102)
            DEBIT.ACCT = DE.102
            CHG.AMT = '6'
        END
* Vendor account. Replace it with the actual vendor account for charges

        CR.ACCT = 'XXXXXXXXXXXX'
    END ELSE

        RETURN
    END
    CHG.VERSION = 'FUNDS.TRANSFER,ATM.CHG'
    USERNAME = AT$ATM.PARAMETER.REC<ATM.PARA.OFS.USER>
    PASSWORD = AT$ATM.PARAMETER.REC<ATM.PARA.OFS.PASSWORD>
    COMP.CODE = ID.COMPANY

    OFS.SOURCE.ID = AT$ATM.PARAMETER.REC<ATM.PARA.CHG.OFS.SOURC>

    FN.OFS.SOURCE = 'F.OFS.SOURCE'
    F.OFS.SOURCE = ''
    CALL OPF(FN.OFS.SOURCE, F.OFS.SOURCE)
    R.OFS.SRC.REC = ''

    CALL F.READ(FN.OFS.SOURCE,OFS.SOURCE.ID,R.OFS.SRC.REC,F.OFS.SOURCE,ERR.REC)
    OFS$SOURCE.REC = R.OFS.SRC.REC
    IF OFS$SOURCE.REC THEN
        OFS$SOURCE.ID = OFS.SOURCE.ID
    END
    CCY.CODE = 'EGP'
    CHG.DESC = 'BALANCE FEES'
    OFS.MSG = CHG.VERSION:'/I/PROCESS,':USERNAME:'/':PASSWORD:'/':COMP.CODE:',,DEBIT.ACCT.NO::=':DEBIT.ACCT:','
    OFS.MSG := 'CREDIT.CURRENCY::=':CCY.CODE:',CREDIT.AMOUNT::=':CHG.AMT:',CREDIT.ACCT.NO::=':CR.ACCT:',DEBIT.THEIR.REF:1:1=':CHG.DESC
    OFS.MSG := ',ORDERING.CUST:1=BANK,PROFIT.CENTRE.DEPT:1=1,'

    CALL OFS.PROCESS.MANAGER(OFS.MSG,OFS.RESP)

    RETURN
