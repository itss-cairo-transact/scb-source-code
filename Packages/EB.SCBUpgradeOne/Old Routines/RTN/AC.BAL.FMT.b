* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>349</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE AC.BAL.FMT
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.CONTRACT.BALANCES

    RTN.TIME=TIME()
    EXECUTE "COMO ON AC.BAL.FMT_":TODAY:'_':RTN.TIME

    GOSUB INIT
    GOSUB PROCESS

    EXECUTE "COMO OFF AC.BAL.FMT_":TODAY:'_':RTN.TIME
    CRT 'COMO saved as *** AC.BAL.FMT_':TODAY:'_':RTN.TIME:' ***'
    SLEEP 2

    RETURN

INIT:
    FN.ACCOUNT = "F.ACCOUNT"
    FV.ACCOUNT = ""

    SEL.CMD = ''
    AC.LIST = ''
    YR.ERR = ''
    NO.OF.AC = ''
    NO.OF.REC = ''
    YR.ACCOUNT = ''
    YERR = ''
    YID.ACCT = ''
    PROBLEM.ACCT = ''

    CALL OPF(FN.ACCOUNT,FV.ACCOUNT)

    F.ECB = 'F.EB.CONTRACT.BALANCES'; FN.ECB = ''
    CALL OPF(F.ECB, FN.ECB)

        FN.SAVEDLISTS = '&SAVEDLISTS&'
        F.SAVEDLISTS = ''
        CALL OPF(FN.SAVEDLISTS,F.SAVEDLISTS)

        READ AC.LIST FROM F.SAVEDLISTS,'ACC.LIST.CORR' ELSE
        AC.LIST=''
                CRT "Savedlists file ACC.LIST.CORR not found"
        END

    RETURN

PROCESS:

    LOOP
        REMOVE YID.ACCT FROM AC.LIST SETTING POS
    WHILE YID.ACCT:POS

        READU YR.ACCOUNT FROM FV.ACCOUNT, YID.ACCT THEN
            READU R.ECB FROM FN.ECB, YID.ACCT ELSE R.ECB = ''
                        OPEN.ACT = '' ; OPEN.CLR = '' ; ONL.ACT = '' ; ONL.CLR = '' ; WORK.BAL = '' ; PREV.OPEN.BAL = "" ; START.YEAR.BAL = "" ; OPEN.AVAIL.BAL = ""
            OPA.FMT.AMT = "" ; OPC.FMT.AMT = "" ; ONA.FMT.AMT = "" ; ONC.FMT.AMT = "" ; WBL.FMT.AMT = "" ; POB.FMT.AMT = "" ; SYB.FMT.AMT = "" ; OAB.FMT.AMT = ""
            OPEN.ACT = YR.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            OPEN.CLR = YR.ACCOUNT<AC.OPEN.CLEARED.BAL>
            ONL.ACT  = YR.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
            ONL.CLR  = YR.ACCOUNT<AC.ONLINE.CLEARED.BAL>
            WORK.BAL = YR.ACCOUNT<AC.WORKING.BALANCE>
            OPEN.AVAIL.BAL = YR.ACCOUNT<AC.OPEN.AVAILABLE.BAL>

            OPA.FMT.AMT = OPEN.ACT
            OPC.FMT.AMT = OPEN.CLR
            ONA.FMT.AMT = ONL.ACT
            ONC.FMT.AMT = ONL.CLR
            WBL.FMT.AMT = WORK.BAL
            OAB.FMT.AMT = OPEN.AVAIL.BAL

            CALL EB.ROUND.AMOUNT(YR.ACCOUNT<AC.CURRENCY>,OPEN.ACT,"","")
            CALL EB.ROUND.AMOUNT(YR.ACCOUNT<AC.CURRENCY>,OPEN.CLR,"","")
            CALL EB.ROUND.AMOUNT(YR.ACCOUNT<AC.CURRENCY>,ONL.ACT,"","")
            CALL EB.ROUND.AMOUNT(YR.ACCOUNT<AC.CURRENCY>,ONL.CLR,"","")
            CALL EB.ROUND.AMOUNT(YR.ACCOUNT<AC.CURRENCY>,WORK.BAL,"","")
                        CALL EB.ROUND.AMOUNT(YR.ACCOUNT<AC.CURRENCY>,OPEN.AVAIL.BAL,"","")


            IF (OPA.FMT.AMT NE OPEN.ACT) OR (OPC.FMT.AMT NE OPEN.CLR) OR (ONA.FMT.AMT NE ONL.ACT) OR (ONC.FMT.AMT NE ONL.CLR) OR (WBL.FMT.AMT NE WORK.BAL) OR (OAB.FMT.AMT NE OPEN.AVAIL.BAL)  THEN

                                IF R.ECB THEN
                                        R.ECB<ECB.OPEN.ACTUAL.BAL> = OPEN.ACT
                                        R.ECB<ECB.OPEN.CLEARED.BAL> = OPEN.CLR
                                        R.ECB<ECB.ONLINE.ACTUAL.BAL> = ONL.ACT
                                        R.ECB<ECB.ONLINE.CLEARED.BAL> = ONL.CLR
                                        R.ECB<ECB.WORKING.BALANCE> = WORK.BAL
                                        R.ECB<ECB.OPEN.AVAILABLE.BAL> = OPEN.AVAIL.BAL
                                        WRITE R.ECB TO FN.ECB, YID.ACCT
                                END

                YR.ACCOUNT<AC.OPEN.ACTUAL.BAL> = OPEN.ACT
                YR.ACCOUNT<AC.OPEN.CLEARED.BAL> = OPEN.CLR
                YR.ACCOUNT<AC.ONLINE.ACTUAL.BAL> = ONL.ACT
                YR.ACCOUNT<AC.ONLINE.CLEARED.BAL> = ONL.CLR
                YR.ACCOUNT<AC.WORKING.BALANCE> = WORK.BAL
                                YR.ACCOUNT<AC.OPEN.AVAILABLE.BAL> = OPEN.AVAIL.BAL

                CRT "Problematic account = ":YID.ACCT:"# BALANCES BEFORE FORMATTING = ":OPA.FMT.AMT:"#":OPC.FMT.AMT:"#":ONA.FMT.AMT:"#":ONC.FMT.AMT:"#":WBL.FMT.AMT:"#":OAB.FMT.AMT:"# FORMATTED = ":OPEN.ACT:"#":OPEN.CLR:"#":ONL.ACT:"#":ONL.CLR:"#":WORK.BAL:"#":OPEN.AVAIL.BAL
                WRITE YR.ACCOUNT TO FV.ACCOUNT,YID.ACCT

            END
        END

        RELEASE FN.ECB, YID.ACCT
        RELEASE FV.ACCOUNT, YID.ACCT
    YID.ACCT = ""

    REPEAT

    RETURN
END
