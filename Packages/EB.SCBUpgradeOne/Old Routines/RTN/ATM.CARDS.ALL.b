* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*****NESSREEN AHMED 2/6/2020***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ATM.CARDS.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CARD.TYPE


    TEXT = 'HELLO' ; CALL REM

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

**********OPEN FILE********************************
    OPENSEQ "&SAVEDLISTS&" , "ATM.CARDS.ALL.txt" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ATM.CARDS.ALL.txt"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ATM.CARDS.ALL.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ATM.CARDS.ALL.txt CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ATM.CARDS.ALL.txt File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
    BB.DATA = "Card Number":",":"Customer ID ":",":"Cust.Arabic Name":",":"Cust.Sector":",":"Acct No1":",":"Acct No2":",":"Acct No3":",":"Name on Card":",":"Card Code":",":"Code Description":",":"Issue Date":",":"Expiray Date":",":"Branch Code" :",":"Branch Name":",":"Cancellation Date"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    YTEXT = "Enter Start.Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.D = COMI
    YTEXT = "Enter End.Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ED.D = COMI

  **N.SEL = "SELECT FBNK.CARD.ISSUE WITH CANCELLATION.DATE EQ '' AND (CARD.CODE GE 801 AND CARD.CODE LT 1000) AND (ISSUE.DATE GE ": ST.D : " AND ISSUE.DATE LE ": ED.D : " ) BY ACCOUNT BY ISSUE.DATE"
    N.SEL = "SELECT FBNK.CARD.ISSUE WITH (CARD.CODE GE 801 AND CARD.CODE LT 1000) AND (ISSUE.DATE GE ": ST.D : " AND ISSUE.DATE LE ": ED.D : " ) BY ACCOUNT BY ISSUE.DATE"

    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)

    TEXT = '������ ��� ��������=':SELECTED.N ; CALL REM
    IF SELECTED.N THEN
        FOR I = 1 TO SELECTED.N
            CARD.ID = '' ; LOCAL.REF.CI = '' ;CI.ACCT = '' ;CI.NAME = '' ; CI.ISS.DATE = '' ; CI.EXP.DATE = '' ; CI.CO.CODE = ''  ; CI.CUST = '' ; BRANCH.NAME = '' ; CI.CARD.CODE = '' ; CARD.CODE.DES = '' ; BRANCH.NAME = ''
            CUST.SEC = '' ; SEC.NAME = '' ; ACCT1 = '' ; ACCT2 = '' ; ACCT3 = '' ; CI.CANC.DATE = ''

            CARD.ID = KEY.LIST.N<I>
            CALL F.READ(FN.CARD.ISSUE, CARD.ID, R.CARD.ISSUE, F.CARD.ISSUE, E1)
            LOCAL.REF.CI = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            CI.ACCT<I> = R.CARD.ISSUE<CARD.IS.ACCOUNT>
            ACCT1 = CI.ACCT<I>[1,16]
            ACCT2 = CI.ACCT<I>[17,17]
            ACCT3 = CI.ACCT<I>[34,17]
            CI.NAME<I> = R.CARD.ISSUE<CARD.IS.NAME>
            CI.ISS.DATE<I> = R.CARD.ISSUE<CARD.IS.ISSUE.DATE>
            CI.EXP.DATE<I> = R.CARD.ISSUE<CARD.IS.EXPIRY.DATE>
            CI.CO.CODE<I> = R.CARD.ISSUE<CARD.IS.CO.CODE>
            CI.CANC.DATE<I> = R.CARD.ISSUE<CARD.IS.CANCELLATION.DATE>

            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,CI.ACCT<I>,CI.CUST)

            CALL F.READ(FN.CUSTOMER,CI.CUST, R.CUSTOMER, F.CUSTOMER ,E3)
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
            AR.NAME = LOCAL.REF.C<1,CULR.ARABIC.NAME>
            CUST.SEC<I> = R.CUSTOMER<EB.CUS.SECTOR>
            CALL DBR('SECTOR':@FM:EB.SEC.DESCRIPTION,CUST.SEC<I>,SEC.NAME)

            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CI.CO.CODE<I>,BRANCH.NAME)

            CI.CARD.CODE<I> = LOCAL.REF.CI<1,LRCI.CARD.CODE>
            CALL DBR('SCB.VISA.CARD.TYPE':@FM:VICA.CODE.DESC,CI.CARD.CODE<I>,CARD.CODE.DES)

         *   BB.DATA = CARD.ID[6,16] :",":CI.CUST:",":AR.NAME:",":SEC.NAME:",": CI.ACCT<I>:",":CI.NAME<I>:",":CI.CARD.CODE<I>:",":CARD.CODE.DES:",":CI.ISS.DATE<I>:",":CI.EXP.DATE<I>:",":CI.CO.CODE<I>:",":BRANCH.NAME
             BB.DATA = CARD.ID[6,16] :",":CI.CUST:",":AR.NAME:",":SEC.NAME:",": ACCT1:",":ACCT2:",":ACCT3:",":CI.NAME<I>:",":CI.CARD.CODE<I>:",":CARD.CODE.DES:",":CI.ISS.DATE<I>:",":CI.EXP.DATE<I>:",":CI.CO.CODE<I>:",":BRANCH.NAME:",":CI.CANC.DATE

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END   ;***End of Card Issue records***
    TEXT = '�� �������� �� �������� ' ;CALL REM;
    RETURN
END
