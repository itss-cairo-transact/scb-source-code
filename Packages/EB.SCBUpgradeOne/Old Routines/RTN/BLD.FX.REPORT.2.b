* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.FX.REPORT.2(ENQ.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------
    FN.HOLD = "F.HOLD.CONTROL"   ; F.HOLD = ""
    CALL OPF(FN.HOLD,F.HOLD)

    COMP = ID.COMPANY
    DAT  = TODAY
    CALL CDT("",DAT,'-1W')

    T.SEL  = "SELECT F.HOLD.CONTROL WITH REPORT.NAME EQ 'REVALUATION.DETAIL'"
    T.SEL := " AND RUN.IN.BATCH EQ 'Y' AND COMPANY.ID EQ ":COMP
    T.SEL := " AND BANK.DATE EQ ":DAT
    T.SEL := " BY REPORT.NAME"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ.DATA<2,I> = '@ID'
            ENQ.DATA<3,I> = 'EQ'
            ENQ.DATA<4,I> = KEY.LIST<I> 
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = '@ID'
        ENQ.DATA<3,2> = 'EQ'
        ENQ.DATA<4,2> = 'DUUMY'
    END
*-----------------------------------------------------------
    RETURN
END
