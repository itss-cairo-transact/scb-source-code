* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-140</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE APPL.TRN.REV.DET.H
* PROGRAM APPL.TRN.REV.DET.H
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROTOCOL

    PRV_COMP_ID = ''
    GOSUB INITIATE

    GOSUB PRINT.HEAD
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='APPL.TRN.REV.DET.H'
    CALL PRINTER.ON(REPORT.ID,'')

    COMP.ID = ID.COMPANY

    YTEXT = "Enter Processing Date YYYYMMDD : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    PROCESS.DATE = COMI
    IF PROCESS.DATE EQ '' THEN
        TEXT = "You must enter Processing Date"; CALL REM
        RETURN
    END
    P.DATE = PROCESS.DATE[7,2]:'/':PROCESS.DATE[5,2]:'/':PROCESS.DATE[1,4]

    RETURN
*=============================================================
CALLDB:
    FN.PROT = 'F.SCB.PROTOCOL'        ; F.PROT = ''  ; R.PROT = ''
    FN.USR  = 'F.USER'                ; F.USR  = ''  ;R.USR = ''

    NO.TRN = ''
    NO.TRN.1 = 0
    FVAR.COUNT=''; R.FILL = ''

    *OPEN F.COUNT TO FVAR.COUNT ELSE
    *    TEXT = "ERROR OPEN FILE" ; CALL REM
    *    RETURN
    *END
    CALL OPF(FN.PROT,F.PROT)
    CALL OPF(FN.USR,F.USR)

*========= GET DATA FROM USER ================

*    YTEXT = "Enter Company Code : "
*    CALL TXTINP(YTEXT, 8, 22, "9", "A")
*    COMP.CODE = COMI
*   IF COMP.CODE EQ '' THEN
*        TEXT = "You must enter Company Code"; CALL REM
*        RETURN
*    END
*=========================================================

    T.SEL = "SELECT F.SCB.PROTOCOL WITH  LEVEL.FUNCTION LIKE ...R... AND ID NE '' AND USER UNLIKE AUTHOR... AND APPLICATION UNLIKE ...ENQUIRY... AND PROCESS.DATE EQ ":PROCESS.DATE:" AND REMARK UNLIKE '...AUTOMATIC LOGOUT...' AND COMPANY.ID EQ ":COMP.ID:" BY COMPANY.ID BY APPLICATION BY TIME"
  *  T.SEL = "SELECT F.SCB.PROTOCOL WITH  LEVEL.FUNCTION LIKE ...R... AND ID NE '' AND USER UNLIKE AUTHOR... AND APPLICATION UNLIKE ...ENQUIRY... AND PROCESS.DATE EQ 20140220 AND REMARK UNLIKE '...AUTOMATIC LOGOUT...' AND COMPANY.ID EQ EG0010001 BY COMPANY.ID BY APPLICATION BY TIME"

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""


 CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.PROT,KEY.LIST<I>,R.PROT,F.PROT,E1)
*=================================================

            PRINT
            EMP.NAM = R.PROT<PRTC.USER>
* CALL DBR('USER':@FM:EB.USE.USER.NAME,R.PROT<EB.PTL.USER>,EMP.NAM)
            CALL F.READ(FN.USR,R.PROT<PRTC.USER>,R.USR,F.USR,E2)
            BRAN.NO = R.USR<EB.USE.DEPARTMENT.CODE>
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USR<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN = FIELD(BRANCH,'.',2)
            XX = SPACE(120)
            XX<1,1>[2,40]   = R.PROT<PRTC.APPLICATION>
            XX<1,1>[45,25]   = R.PROT<PRTC.ID>
            XX<1,1>[75,15]   = R.PROT<PRTC.USER>
* XX<1,1>[95,20]   = EMP.NAM
            XX<1,1>[95,20]   = R.USR<EB.USE.USER.NAME>
            XX<1,1>[118,20]   = YYBRN
            NO.TRN = NO.TRN + 1
            NO.TRN.1 = NO.TRN.1 + 1

            PRINT XX<1,1>
*==============================================
*  IF NXT.COMP.ID NE R.PROT<EB.PTL.COMPANY.ID>  THEN
*      PRINT STR('=',120)
*      PRINT ; PRINT SPACE(10):"����� ������ ����� = ":NO.TRN:" �����"
*      NO.TRN = 0
*      GOSUB PRINT.HEAD
*  END

        NEXT I

        PRINT STR('=',120)
        PRINT ; PRINT SPACE(10):"����� ������ �����  = ":NO.TRN.1:" �����"
        PRINT STR('=',120)
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
** CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,NXT.COMP.ID,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH)
*  YYBRN = FIELD(BRANCH,'.',2)
    YYBRN = BRANCH
    DATY = TODAY

    T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"APPL.TRN.REV.DET.H"

    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):" ���� ���� �������� ������� ������  ":P.DATE

*   PR.HD :="'L'":SPACE(50): " �� ������ " :ST.DATE :" ��� " : EN.DATE
    PR.HD :="'L'":SPACE(40):STR('_',55)


    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(15):"�������" :SPACE(30):" ������":SPACE(30):"������":SPACE(23):"��� ������"
*    PR.HD := "'L'" : " ��� ������  " : SPACE (3) : " ��� ������  "  : SPACE (8) : " ����� ������   " : SPACE(3) : "  ����� ����� ������� " :  SPACE(3) : " ������  " : SPACE(2) :"�.������� - �.�������"
    PR.HD :="'L'":STR('_',120)
*    PRINT
    HEADING PR.HD
    RETURN
*==============================================================



END
