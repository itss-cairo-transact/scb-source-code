* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BILL.OVER.INPUT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*------------------------------------------------------
 
    IF COMI THEN    ;* CHG.AMT(BS)
        FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
        CALL OPF(FN.AC,F.AC)
        FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
        CALL OPF(FN.LIM,F.LIM)

        ACC = R.NEW(SCB.BS.CUST.ACCT)
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CUSID)
        CUS     = CUSID[1,3]
        AMT2    = COMI
*Line [ 52 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DC.AMT  = DCOUNT(AMT2,@VM)

        IF CUS NE 994 THEN
            FOR I = 1 TO DC.AMT

                CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,ACC,BAL2)
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CUS.ID)
                CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC,CUS.LIM)

                LMT.REF1 = FIELD(CUS.LIM,'.',1)
*****MAHMOUD MAGDY 2020/02/03
*                IF LEN(LMT.REF1) EQ 3 THEN
*                    XX = CUS.ID:'.':'0000':CUS.LIM
*                END
*                IF LEN(LMT.REF1) EQ 4 THEN
*                    XX = CUS.ID:'.':'000':CUS.LIM
*                END
                XX = CUS.ID:'.':FMT(LMT.REF1,'R%7'):'.':FIELD(CUS.LIM,'.',2)
****************************
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                LIMIT.AMT = R.LIM<LI.AVAIL.AMT>
                TOTAL.DB  = BAL2 - AMT2
                TOTAL.CR  = TOTAL.DB + LIMIT.AMT
                IF TOTAL.CR LT 0 THEN
                    ETEXT = "������ ����� ��� ������ ������ �� ����"
                END
                AMT = ''
            NEXT I
        END
    END
    RETURN
END
