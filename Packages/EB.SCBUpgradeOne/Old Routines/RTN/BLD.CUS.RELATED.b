* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
*( M.ELSAYED 2012-10-01 )
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUS.RELATED(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
*=============================================================*
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ;  R.CU = '' ; R.REL.CU = ''
    FN.REL.CU = 'FBNK.CUSTOMER' ; F.REL.CU = '' ;  R.CU = '' ; R.REL.CU = ''
    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.REL.CU,F.REL.CU)

    I = 0
    SECTOR.FLAG = 0
    POST.FLAG   = 0
*====================================================================*

    T.SEL = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT LT 90 AND SECTOR IN ( 1200 1400 ) AND COMPANY.BOOK EQ ":COMP
*   T.SEL = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT LT 90 AND SECTOR IN ( 1200 1400 ) "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    FOR ISEL = 1 TO SELECTED
        CALL F.READ(FN.CU,KEY.LIST<ISEL>,R.CU,F.CU,ES.CU)

        REL.CUS = R.CU<EB.CUS.REL.CUSTOMER>
*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        REL.COUNT = DCOUNT(REL.CUS,@VM)
        IF REL.COUNT GT 0 THEN
            FOR ICOUNT = 1 TO REL.COUNT
                CALL F.READ(FN.REL.CU,REL.CUS<1,ICOUNT>,R.REL.CU,F.REL.CU,ER.R.CU)
                WS.REL.SECTOR  = R.REL.CU<EB.CUS.SECTOR>
                WS.REL.POSTING = R.REL.CU<EB.CUS.POSTING.RESTRICT>

                WS.REL.CODE    = R.CU<EB.CUS.RELATION.CODE,ICOUNT>
                IF WS.REL.POSTING LT 90 THEN
                    IF (WS.REL.CODE EQ '1') OR (WS.REL.CODE EQ '6') OR (WS.REL.CODE EQ '10') THEN
                        IF WS.REL.SECTOR EQ 1100 OR WS.REL.SECTOR EQ 1300 THEN
                            SECTOR.FLAG = 1
                        END
                    END
                END
            NEXT ICOUNT

            IF SECTOR.FLAG NE 1 THEN
                I++
                ENQ<2,I>    = '@ID'
                ENQ<3,I>    = 'EQ'
                ENQ<4,I>    = KEY.LIST<ISEL>
                SECTOR.FLAG = 0
            END
        END ELSE
            I++
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<ISEL>
            SECTOR.FLAG = 0
        END

        SECTOR.FLAG = 0
        POST.FLAG   = 0
    NEXT ISEL
    TEXT = I :' ��� �������  ' ; CALL REM
    IF I = 0 THEN
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUMMY'
    END
    RETURN
*******************************************************************************************
END
