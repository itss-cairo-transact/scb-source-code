* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
***Copied from Hytham old program and updated by NESSREEN AHMED 23/6/2020***
*-----------------------------------------------------------------------------
* <Rating>557</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  ATM.ANNUAL.FEES.OFS.N

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RTN.FEES.SMS.OFS
*===============================================
    GOSUB INITIALISE
    GOSUB CHECK.RECORD
    RETURN
*----------------------------------------------------------------------*
INITIALISE:
    TEXT = 'YARAB' ; CALL REM
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "CARD.FEES.NS"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.TEMP = 'F.SCB.RTN.FEES.SMS.OFS'   ; F.TEMP = ''
    CALL OPF(FN.TEMP , F.TEMP)

    DB.ACCT = '' ; XX = 0 ; AC.COMP = '' ; NN = ''

    RETURN
*---------------------------------------------------------------------
CHECK.RECORD:
    NN.SEL = "SELECT CARD-CENTER WITH @ID LIKE FixAnnual..."
    CALL EB.READLIST(NN.SEL,N.LIST,"",SELECTED,ER.MSG.N)

    IF SELECTED GT 1 THEN
        TEXT = "ERROR ** MORE THAN ONE FILE" ; CALL REM
    END

    IF SELECTED EQ 1 THEN
        FILE.NAME = N.LIST<1>

        CALL F.READ(FN.TEMP,FILE.NAME,R.TEMP,F.TEMP,READ.ER)
        IF READ.ER THEN
            Path = "CARD-CENTER/":FILE.NAME
            GOSUB BUILD.RECORD
            GOSUB END.PROG
        END ELSE
            TEXT = "ERROR ** Already.Exist" ; CALL REM
        END

    END
    RETURN
*----------------------------------------------------------------------*

BUILD.RECORD:
    COMMA = ","

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 1

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN

            CUST.ID      = FIELD(Line,",",1)
            CARD.NO      = FIELD(Line,",",2)
            NARRAT       = FIELD(Line,",",3)
            CHECK.ACC    = FIELD(Line,",",4)
            SAV.ACC      = FIELD(Line,",",5)
            CRED.ACC     = FIELD(Line,",",6)
            DB.AMT       = FIELD(Line,",",7)

            XX = XX+1
***-----------TO SELECT DEBIT ACCOUNT--------------------***
            IF CHECK.ACC NE '' AND CHECK.ACC NE '0' THEN
                DB.ACCT = CHECK.ACC
            END ELSE
                IF SAV.ACC NE '' AND SAV.ACC NE '0' THEN
                    DB.ACCT = SAV.ACC
                END ELSE
                    IF CRED.ACC NE '' AND CRED.ACC NE '0' THEN
                        DB.ACCT = CRED.ACC
                    END
                END
            END
*TEXT = 'DB.ACCT=':DB.ACCT ; CALL REM
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,DB.ACCT,AC.COMP)
***------------------------------------------------------***
**---------------------------------------------------------------------**
*                          ***** OFS RECORD *****                       *
**---------------------------------------------------------------------**
            DAT      = TODAY
            TXN.TYPE = "AC52"
            CURR     = 'EGP'
            PL.ACC   = "PL57040"
            CRD      = "ATMC.":CARD.NO
            CALL DBR ('CARD.ISSUE': @FM:CARD.IS.ACCOUNT,CRD,ACCOUNT)
            ACC      = ACCOUNT<1,1>
            COMP          = AC.COMP
            COM.CODE      = COMP[8,2]
            OFS.USER.INFO = "AUTO.CHRGE//":COMP

            OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":TXN.TYPE:COMMA
            OFS.MESSAGE.DATA := "DEBIT.AMOUNT=":TRIM(DB.AMT):COMMA
            OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": CURR:COMMA
            OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": CURR:COMMA
            OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=99":COMMA
            OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
            OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":NARRAT:COMMA
            OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":PL.ACC:COMMA
            OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":CARD.NO:COMMA
            OFS.MESSAGE.DATA := "ORDERING.BANK=SCB":COMMA
            OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DAT:COMMA
            OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":DAT:COMMA
            OFS.MESSAGE.DATA := "PAYMENT.DETAILS=Annual Fix":COMMA
            OFS.MESSAGE.DATA := "COMMISSION.CODE=WAIVE"::COMMA
            OFS.MESSAGE.DATA := "CHARGE.CODE=WAIVE"::COMMA

            NN = NN+1
            F.PATH  = FN.OFS.IN
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
            OFS.ID  = "T99.":NN:".":CARD.NO:"-":TODAY

            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-ANN.CARD.FEES' ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-ANN.CARD.FEES' ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath

    RETURN
***------------------------------------------------------------------***
END.PROG:
*-- write in temp
    TEXT = 'TOT.REC=' :XX ; CALL REM
    ID.TEMP = FILE.NAME
    CALL F.READ(FN.TEMP,ID.TEMP,R.TEMP,F.TEMP,READ.ER)
    R.TEMP<FEES.SMS.RUN.DATE> = TODAY
    CALL F.WRITE(FN.TEMP,ID.TEMP,R.TEMP)
    CALL JOURNAL.UPDATE(ID.TEMP)
    TEXT = '�� �������� �� ��������' ; CALL REM

*-- delete file
    DIR.NAME = 'CARD-CENTER'
    TEXT = 'DIR=':DIR.NAME:'/':FILE.NAME
    HUSH ON
*    EXECUTE 'DELETE ':DIR.NAME:' ':FILE.NAME
    EXECUTE 'rm ':DIR.NAME:'/':FILE.NAME
    HUSH OFF
    RETURN
*----------------------------------------------------------------------*
END
