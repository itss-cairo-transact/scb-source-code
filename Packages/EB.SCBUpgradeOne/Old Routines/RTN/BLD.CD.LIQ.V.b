* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
***ABEER ***
    SUBROUTINE BLD.CD.LIQ.V

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.SCHEDULE.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.SCHEDULES.PAST
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.RATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*--------------------------------------------------------------------*
*                          INITIALISE                                *
*--------------------------------------------------------------------*

**********************

**********************
    F.LD  = '' ; FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    R.LD  = ''
    CALL OPF(FN.LD,F.LD)

    F.LMM.ACC  = '' ; FN.LMM.ACC = 'FBNK.LMM.ACCOUNT.BALANCES'
    R.LMM.ACC  = ''
    CALL OPF(FN.LMM.ACC,F.LMM.ACC)

    F.SCH.DATE = '' ; FN.SCH.DATE = 'FBNK.LMM.SCHEDULE.DATES'
    R.SCH.DATE = ''
    CALL OPF(FN.SCH.DATE,F.SCH.DATE)

    F.SCH.PAST = '' ; FN.SCH.PAST = 'FBNK.LMM.SCHEDULES.PAST'
    R.SCH.PAST = ''
    CALL OPF(FN.SCH.PAST,F.SCH.PAST)

    F.BAS.INT  = '' ; FN.BAS.INT  = 'FBNK.BASIC.INTEREST'
    R.BAS.INT  = ''
    CALL OPF(FN.BAS.INT,F.BAS.INT)

    F.SCB.RATE = '' ; FN.SCB.RATE = 'F.SCB.CD.RATES'
    R.SCB.RATE = ''
    CALL OPF(FN.SCB.RATE,F.SCB.RATE)

    SHIFT = '' ; ROUND = '' ; START.DATE = ""

    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)

    KEY.LIST   = "" ; SELECTED    = "" ; ER.MSG = ""
    HH         = 0
*--------------------------------------------------------------------*
    LD.ID = O.DATA
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ETEXT111)
    MYVAL = R.LD<LD.VALUE.DATE>
    TO.DAT = TODAY
****************
    ST.DATE = R.LD<LD.VALUE.DATE>
    NO.INT.MON ='6M'
    ST.DATE = SHIFT.DATE(ST.DATE,NO.INT.MON,UP)
***************
    IF ST.DATE GT TO.DAT THEN
        O.DATA = 'MUST BE GREATER THAN 6 MONTHS'
    END ELSE
       * R.NEW(LD.FIN.MAT.DATE) = TO.DAT
        IDD  = LD.ID:"00"
        CALL F.READ(FN.SCH.DATE, IDD, R.SCH.DATE, F.SCH.DATE, ETEXT)
        DATE.LIST= CONVERT(VM,"*", R.SCH.DATE)
        LOOP
            REMOVE ID.PAST FROM DATE.LIST  SETTING POS

        WHILE ID.PAST:POS

            SCHEDULE.DATES     = FIELDS(ID.PAST,"*",1,1)
            SCHEDULE.PROCESSED = FIELDS(ID.PAST,"*",2,1)

            FLAG.D = SCHEDULE.PROCESSED
            IF FLAG.D EQ 'D' THEN
                IDD.PAST = IDD[1,12]:SCHEDULE.DATES:'00'
                CALL F.READ(FN.SCH.PAST, IDD.PAST, R.SCH.PAST, F.SCH.PAST, ETEXT1)
                HH = HH + 1
*Line [ 108 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
               DECOUNT.INT  = DCOUNT(R.SCH.PAST<LD28.INT.RATE.USED>,@VM)
                FOR XX = 1 TO  DECOUNT.INT
                    INTT         = R.SCH.PAST<LD28.INT.RATE.USED><1,XX>
                    INT.AMT.MON =  R.SCH.PAST<LD28.CALC.INT.AMOUNT><1,XX>
                    INT.100=INTT/100
                    INT.AMT      = INT.AMT.MON/INT.100
***TO CALCULATE THE PENALTY ACCODRING NO OF DAYS
                    ACT.DAYS = "C"
                    DATE.VAL = R.LD<LD.VALUE.DATE>
                    CALL CDT('EG', DATE.VAL, '1W')
                    AMT      = R.LD<LD.AMOUNT>
                    FIN.DATE = TODAY
                    CALL CDD("",DATE.VAL,FIN.DATE,ACT.DAYS)

                    IF ( ACT.DAYS LT 365 AND ACT.DAYS GE 180 ) THEN
                        NEW.INT.RATE = INT.100 - 0.025
                    END
                    IF ( ACT.DAYS LT 730 AND ACT.DAYS GE 365 ) THEN
                        NEW.INT.RATE = INT.100 - 0.02
                    END
                    IF ( ACT.DAYS LT 1095 AND ACT.DAYS GE 730 ) THEN
                        NEW.INT.RATE = INT.100 - 0.015
                    END
*---ROUNDING SHOULD E USED
                    NEW.INTT.AMT  = INT.AMT * NEW.INT.RATE
                    CALL EB.ROUND.AMOUNT ('EGP',NEW.INTT.AMT,'',"")
                    NEW.AMT.CALC +=NEW.INTT.AMT
                    INT.AMT      = R.SCH.PAST<LD28.INT.RECEIVED><1,XX>
                    FIN.DAT      = R.SCH.PAST<LD28.DATE.TO><1,XX>
*---------------------------------------------------------------------*
*---                        AMOUNT PAYED                          ----*
*---------------------------------------------------------------------*
                    AMT.PAYED   += INT.AMT
                NEXT XX
            END
        REPEAT
************************************
        CALL F.READ(FN.LMM.ACC,IDD, R.LMM.ACC, F.LMM.ACC, ETEXT21)
        NEW.INT.D=R.LMM.ACC<LD27.DATE.INT.ACC.TO>

        NO.INT =DCOUNT(NEW.INT.D,@VM)
        LAST.MON = TODAY[1,6]:"01"
        FOR JJ = 1 TO NO.INT
            NEW.INT.D.EACH = R.LMM.ACC<LD27.DATE.INT.ACC.TO><1,JJ>
            IF NEW.INT.D.EACH LT LAST.MON THEN
                REST.INT.AMT.TODATE  = R.LMM.ACC<LD27.INT.AMT.TODATE><1,JJ>
                REST.INT.RATE = R.LMM.ACC<LD27.AC.INT.RATE><1,JJ>
                REST.INT.RATE.100 = REST.INT.RATE/100
                REST.INT.AMT      = REST.INT.AMT.TODATE/REST.INT.RATE.100
*****NEW PENALTY
                IF ( ACT.DAYS LT 365 AND ACT.DAYS GE 180 ) THEN
                    REST.NEW.INT.RATE =  REST.INT.RATE.100 - 0.025

                END
                IF ( ACT.DAYS LT 730 AND ACT.DAYS GE 365 ) THEN
                    REST.NEW.INT.RATE =  REST.INT.RATE.100 - 0.02
                END
                IF ( ACT.DAYS LT 1095 AND ACT.DAYS GE 730 ) THEN
                    REST.NEW.INT.RATE =  REST.INT.RATE.100 - 0.015
                END
                NEW.REST.INT.AMT  = REST.INT.AMT * REST.NEW.INT.RATE
                CALL EB.ROUND.AMOUNT ('EGP',NEW.REST.INT.AMT,'',"")
                REST.NEW.AMT.CALC +=NEW.REST.INT.AMT
**************
                REST.INT.PAY += REST.INT.AMT.TODATE
            END
        NEXT JJ
*        REST.ACT.AMT=  REST.INT.PAY - REST.NEW.AMT.CALC
****************************************
*TEXT=AMT.PAYED:'-INT PAYED';CALL REM
        REST.ACT.AMT =  NEW.AMT.CALC + REST.NEW.AMT.CALC
        ACT.AMT = AMT.PAYED - REST.ACT.AMT
        IF ACT.AMT GT 0 THEN
            CALL EB.ROUND.AMOUNT ('EGP',ACT.AMT,'',"")
           * R.NEW(LD.CHRG.AMOUNT)                  = ACT.AMT
           * R.NEW(LD.CHRG.CLAIM.DATE)              = TODAY
            AMT.LIQ = (AMT- ACT.AMT)
            O.DATA = AMT.LIQ

        END ELSE
            IF ACT.AMT LT 0 THEN
                AMTT = ACT.AMT * -1
                CALL EB.ROUND.AMOUNT ('EGP',AMTT,'',"")
                *R.NEW(LD.REIMBURSE.PRICE)          = ""
                *R.NEW(LD.REIMBURSE.AMOUNT)         = AMTT +  R.LD<LD.AMOUNT>
                *R.NEW(LD.CHRG.AMOUNT)              = "0.00"

                LIQ.AMT.ALL=  AMTT +  R.LD<LD.AMOUNT>

                O.DATA = LIQ.AMT.ALL

            END
        END
    END
    RETURN
END
