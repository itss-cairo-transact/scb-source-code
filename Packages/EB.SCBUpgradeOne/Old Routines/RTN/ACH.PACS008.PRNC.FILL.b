* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>28</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACH.PACS008.PRNC.FILL
*    PROGRAM ACH.PACS008.PRNC.FILL

* CREATED 26-08-2019
* BY RIHAM YOUSSIF

*-----------------------------------
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h


    GOSUB OPEN.FILES
    GOSUB READ.FILE
**    GOTO  PROGRAM.END

    RETURN

**********************************************
OPEN.FILES:
*==========
    YTEXT = "Enter ACH Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    T.DATE = COMI
**DEBUG
    SETT.DATE = TODAY
    TT     = TIMEDATE()
    KK     = TT[1,8]
    T.DATE.TIME = T.DATE:KK[1,2]:KK[4,2]:KK[7,2]

    FN.PAC8 = "F.SCB.ACH.PACS008"; F.PAC8 = '' ;FVAR.PAC8 = ''
    CALL OPF(FN.PAC8,F.PAC8)


    FN.CD   = "FBNK.LD.LOANS.AND.DEPOSITS";    F.CD = ''
    CALL OPF(FN.CD,F.CD)

    R.PAC  = ''
    R.PAC8 = ''
    R.CD   = ''
    F.SEL = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ...":T.DATE:"... AND CREDITOR.POST.CODE NE ''"
    CALL EB.READLIST(F.SEL,PAC.LIST, "", PAC.SEL, PAC.ER)
    IF PAC.LIST THEN

        TEXT ='THIS PROGRAM RUN BEFORE' ;CALL REM
        GOTO PROGRAM.END

    END




    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH FIN.MAT.DATE EQ ":T.DATE:" AND CATEGORY EQ '21103' AND INTEREST.TO.BNK NE '' AND INTEREST.TO.BNK NE '0017' AND ACCT.ANOTHER.BN NE '' BY INTEREST.TO.BNK BY CUSTOMER.ID"
    CALL EB.READLIST(T.SEL,CD.LIST, "", CD.SEL, CD.ER)

    IF CD.SEL THEN
        NO.OF.RECORDS = CD.SEL
    END
    ELSE
        TEXT = 'No Records Found'; CALL REM
        GOTO PROGRAM.END
    END

    Y.SEL = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ...":T.DATE:"... BY-DSND REMITTANCE.INFO5"
    CALL EB.READLIST(Y.SEL,PAC.LIST, "", PAC.SEL, PAC.ER)

    IF PAC.LIST THEN
        CALL F.READ(FN.PAC8,PAC.LIST<1>, R.PAC8, F.PAC8, PAC8.ER)
        SERIAL.NO = R.PAC8<PACS8.REMITTANCE.INFO5>
    END
    ELSE
        SERIAL.NO = 0
    END

    SERIAL.NO.FINAL = SERIAL.NO + 1
    BATCH.SERIAL    = FMT(SERIAL.NO.FINAL,"R%7")

    BATCH.ID        = 'SUCAEGCX-':T.DATE.TIME:'000-':BATCH.SERIAL
    CREATION.DATE   = T.DATE[1,4]:'-':T.DATE[5,2]:'-':T.DATE[7,2]:'T':TT[1,8]:'.00000'
    SETTLEMENT.DATE = SETT.DATE[1,4]:'-':SETT.DATE[5,2]:'-':SETT.DATE[7,2]

    RETURN
**********************************************
READ.FILE:

    FOR I = 1 TO CD.SEL

        CALL F.READ(FN.CD,CD.LIST<I>, R.CD, F.CD, CDD.ER)
        SCCD.ID = CD.LIST<I>
        IF I EQ 1 THEN
            TRANSACTION.ID  = 'TXSUCAEGCX-':T.DATE.TIME:'000-':BATCH.SERIAL
        END
        ELSE
            TRANSACTION.ID  = 'TXSUCAEGCX-':T.DATE.TIME:'000-':TRN.SERIAL
        END
************************CALC THE AMT AFTER TRIM COMMISSION AND CHARGE OF ACH**************

        AMT.CD             = R.CD<LD.AMOUNT>

        AMT = AMT.CD

        CUS.NO          = R.CD<LD.CUSTOMER.ID>
        CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,CUS.NO,CUS.NAME)
        LOCAL.REF       = R.CD<LD.LOCAL.REF>
        DEBIT.ACCT.NO   = LOCAL.REF<1,LDLR.ACCT.STMP>
        CREDT.ACCT.NO   = LOCAL.REF<1,LDLR.ACCT.ANOTHER.BN>

        BRN.CODE        =  R.CD<LD.CO.CODE>
        BNK.CODE        =  LOCAL.REF<1,LDLR.INTEREST.TO.BNK>

        BEGIN CASE
        CASE BNK.CODE = '0005'
            BNK.ABBR  = 'ALEXEGCXXXX'
        CASE BNK.CODE = '0029'
            BNK.ABBR  = 'EGGBEGCAXXX'
        CASE BNK.CODE = '0031'
            BNK.ABBR  = 'UBOEEGCXXXX'
        CASE BNK.CODE = '0036'
            BNK.ABBR  = 'AGRIEGCXXXX'
        CASE BNK.CODE = '0057'
            BNK.ABBR  = 'ARAIEGCXXXX'
        CASE BNK.CODE = '0009'
            BNK.ABBR  = 'BDACEGCAXXX'

        END CASE

*===========UPDATE SCCD FILE ==================

*      R.CD<SCCD.FLG>  = 'Y'
*******************************UPDATED BY RIHAM R15 24/04/2016**********************
*      WRITE R.CD TO F.CD , SCCD.ID  ON ERROR
*          STOP 'CAN NOT WRITE RECORD ':SCCD.ID:' TO FILE ':F.CD
*      END

*CALL F.WRITE (FN.CD,SCCD.ID,R.CD)
******************END OF UPDATED ********************************
*==============================================


        R.PAC<PACS8.BATCH.ID>            =      BATCH.ID
        R.PAC<PACS8.CREATION.DATE.TIME>  =      CREATION.DATE
        R.PAC<PACS8.REQ.SETTLEMENT.DATE> =      SETTLEMENT.DATE
        R.PAC<PACS8.BATCH.TYPE>          =      'CRT'
        R.PAC<PACS8.TRN.DATE>            =      ''
        R.PAC<PACS8.INSTRUCTION.IDENT>   =      CD.LIST<I>
        R.PAC<PACS8.END.OF.END.IDENT>    =      TRANSACTION.ID
        R.PAC<PACS8.TRANSACTION.ID>      =      TRANSACTION.ID
        R.PAC<PACS8.AMOUNT>              =      AMT
        R.PAC<PACS8.CURRENCY>            =      'EGP'
        R.PAC<PACS8.CHARGER.BEARER>      =      'SLEV'
        R.PAC<PACS8.DEBTOR.NAME>         =      CUS.NAME
        R.PAC<PACS8.DEBTOR.ACCOUNT.NO>   =      DEBIT.ACCT.NO
        R.PAC<PACS8.DEBTOR.ACCOUNT.TYPE> =      'CACC'
        R.PAC<PACS8.CREDITOR.NAME>       =      CUS.NAME
        R.PAC<PACS8.CREDITOR.ACCOUNT.NO> =      CREDT.ACCT.NO
        R.PAC<PACS8.CREDITOR.ACCOUNT.TYPE>      =  'CACC'
        R.PAC<PACS8.PURPOSE>                    =  'SCCD'
        R.PAC<PACS8.REMITTANCE.INFO1>    =      BRN.CODE
        R.PAC<PACS8.REMITTANCE.INFO5>    =      SERIAL.NO.FINAL
        R.PAC<PACS8.PACS.TYPE>           =      '2'
        R.PAC<PACS8.CREDITOR.PARTY.BIC>  =      BNK.ABBR
        R.PAC<PACS8.DEBTOR.PARTY.BIC>    =      'SUCAEGCXXXX'
        R.PAC<PACS8.CREDITOR.POST.CODE>    =      AMT.CD
        IF I EQ 1 THEN
            PAC.ID   = T.DATE.TIME:'000-':BATCH.SERIAL
        END
        ELSE
            PAC.ID   = T.DATE.TIME:'000-':TRN.SERIAL
        END

        GOSUB WRITE.RECORD
        SERIAL.NO.FINAL = SERIAL.NO.FINAL + 1
        TRN.SERIAL      = FMT(SERIAL.NO.FINAL,"R%6")

    NEXT I


    RETURN
**************************************************************
WRITE.RECORD:
*******************************UPDATED BY RIHAM R15 24/04/2016************
*    WRITE R.PAC TO FVAR.PAC8 , PAC.ID  ON ERROR
    WRITE R.PAC TO F.PAC8 , PAC.ID  ON ERROR
        STOP 'CAN NOT WRITE RECORD ':PAC.ID:' TO FILE ':F.PAC8
    END
*CALL F.WRITE (FN.PAC8,PAC.ID,R.PAC)
******************END OF UPDATED ********************************
    RETURN
**************************************************************

PROGRAM.END:
*   PRINT 'PROGRAM END'
END
