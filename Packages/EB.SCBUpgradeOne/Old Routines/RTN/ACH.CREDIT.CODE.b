* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE ACH.CREDIT.CODE(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    FN.ACH ='F.SCB.ACH.PACS008'
    F.ACH  =''
    CALL OPF(FN.ACH,F.ACH)

    T.SEL = "SELECT F.SCB.ACH.PACS008 WITH PACS.TYPE EQ 1"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL DBR('SCB.ACH.PACS008':@FM:PACS8.CREDITOR.ACCOUNT.NO,KEY.LIST<I>,ACCT.NO)
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUS.NO)
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,CUS.LOCAL)
            CREDIT.CODE = CUS.LOCAL<1,CULR.CREDIT.CODE>
            NEW.SEC     = CUS.LOCAL<1,CULR.NEW.SECTOR>
*SEC         = R.CUS<EB.CUS.SECTOR>
*TEXT = CREDIT.CODE; CALL REM
            IF CREDIT.CODE EQ '100' AND NEW.SEC NE '4650' THEN
                J = J + 1
                ENQ<2,J> = '@ID'
                ENQ<3,J> = 'EQ'
                ENQ<4,J> = KEY.LIST<I>
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
