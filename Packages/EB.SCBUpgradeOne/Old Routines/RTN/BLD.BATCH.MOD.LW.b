* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-22</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.BATCH.MOD.LW(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    I = 0
    KK = 0
*=============================================================*

    FN.BTCH = 'F.BATCH' ; F.BTCH = '' ;  R.BTCH = ''
    CALL OPF(FN.BTCH,F.BTCH)

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.LWD.DATE)

    WS.START.TIME = WS.LWD.DATE[3,6]:'1500'
    I = 0
*====================================================================*
    T.SEL = "SELECT ":FN.BTCH:" WITH DATE.TIME GE ":WS.START.TIME:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED,ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
*            CALL F.READ(FN.CAC,KEY.LIST<I>,R.CAC,F.CAC,E.CAC)
*            IF E.CAC THEN
            KK++
            ENQ<2,KK> = "@ID"
            ENQ<3,KK> = "EQ"
            ENQ<4,KK> = KEY.LIST<I>
*            END
        NEXT I
    END
    IF KK = 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
    RETURN
*******************************************************************************************
END
