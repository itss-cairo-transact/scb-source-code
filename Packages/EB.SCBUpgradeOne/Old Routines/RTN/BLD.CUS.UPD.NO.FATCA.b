* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUS.UPD.NO.FATCA(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FATT.FLAG = 0
    FLAG = ''
    H = 0
    LOCATE 'OPENING.DATE' IN ENQ.DATA<2,1> SETTING RB.POS THEN
        T.DATE = ENQ.DATA<4,RB.POS>
    END
    LOCATE 'COMPANY.BOOK' IN ENQ.DATA<2,1> SETTING RB.POS THEN
        CO.BOOK = ENQ.DATA<4,RB.POS>
        TEXT = CO.BOOK; CALL REM
    END

    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    FN.FA1  = 'FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO'
    R.FA1 = '' ; F.FA1 = ''
    FN.CUS = 'FBNK.CUSTOMER'; R.CUS = '' ; F.CUS = ''

    CALL OPF(FN.CUS,F.CUS)
    CALL OPF(FN.FA1,F.FA1)

    T.SEL  = "SELECT FBNK.CUSTOMER WITH OPENING.DATE GE " : T.DATE : " AND COMPANY.BOOK EQ ":CO.BOOK:" AND WITHOUT POSTING.RESTRICT IN (1 12 13 14 18 70 89 90 91 92 98 99)"
    T.SEL := " AND CREDIT.CODE NE 110 AND WITHOUT SECTOR IN (5000 5010 5020) AND SCCD.CUSTOMER NE 'YES' AND DRMNT.CODE NE 1 BY @ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*   TEXT = "SELECTED = ":SELECTED; CALL REM

    FOR I = 1 TO SELECTED

        CALL F.READ( FN.FA1,KEY.LIST<I>, R.FA1, F.FA1,E1)
*   TEXT = "KEY= ": E1 ; CALL REM

        IF E1 NE '' THEN
            FATT.FLAG    += 1
            H = H + 1
            ENQ.DATA<2,H> = "@ID"
            ENQ.DATA<3,H> = "EQ"
            ENQ.DATA<4,H> = KEY.LIST<I>
        END
    NEXT H
*   TEXT = "FTACA = ":FATT.FLAG;CALL REM

    IF FATT.FLAG EQ 0 THEN
        ENQ.DATA<2,1> = "@ID"
        ENQ.DATA<3,1> = "EQ"
        ENQ.DATA<4,1> = "DUMMY"

    END

    RETURN

*****************************************************************************
END
