* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>28</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACH.PACS008.FT.INTER.FILL
*   PROGRAM ACH.PACS008.FT.INTER.FILL

* CREATED 14-12-2016
* BY NOHA HAMED

*-----------------------------------
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.REASONS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h


    GOSUB OPEN.FILES
    GOTO  PROGRAM.END

**********************************************

OPEN.FILES:
*==========
    T.DATE = TODAY
    TT     = TIMEDATE()
    KK     = TT[1,8]
    T.DATE.TIME = T.DATE:KK[1,2]:KK[4,2]:KK[7,2]
    FN.UFT  ="FBNK.FUNDS.TRANSFER$NAU"  ; F.UFT  = '' ;R.UFT     = ''
    FN.PAC8 = "F.SCB.ACH.PACS008"       ; F.PAC8 = '' ;FVAR.PAC8 = ''
    FN.CU   = 'FBNK.CUSTOMER'           ; F.CU   = ''

    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.UFT,F.UFT)
    CALL OPF(FN.PAC8,F.PAC8)

    R.CU      = ''
    R.PAC     = ''
    R.PAC8    = ''
    R.REV.PAC = ''

*********GET HEADER DATA **************************************

    TEXT = V$FUNCTION; CALL REM
*  TEXT = R.NEW(FT.RECORD.STATUS); CALL REM
*  IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='INAU' THEN
*  IF V$FUNCTION = 'I' THEN
*  IF  R.NEW(FT.RECORD.STATUS) NE 'INAO' THEN

    CALL F.READ(FN.UFT,ID.NEW,R.UFT,F.UFT,ERR)
    TEXT = ERR ;  CALL REM
* IF NOT(ERR) AND V$FUNCTION NE 'D' THEN

* IF ERR THEN
    Y.SEL = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ...":T.DATE:"... BY-DSND REMITTANCE.INFO5"
    CALL EB.READLIST(Y.SEL,PAC.LIST, "", PAC.SEL, PAC.ER)
    TEXT = 'SELECTED = ' : PAC.SEL ;  CALL REM
    IF PAC.LIST THEN
        SERIAL.NO = PAC.SEL
    END
    ELSE
        SERIAL.NO = 0
    END

*SETT.DATE       = R.NEW(FT.CREDIT.VALUE.DATE)
    SETT.DATE       = T.DATE
    SERIAL.NO.FINAL = SERIAL.NO + 1
    BATCH.SERIAL    = FMT(SERIAL.NO.FINAL,"R%7")
    TRANSACTION.ID  = 'TXSUCAEGCX-':T.DATE.TIME:'000-':BATCH.SERIAL
    CREATION.DATE   = T.DATE[1,4]:'-':T.DATE[5,2]:'-':T.DATE[7,2]:'T':TT[1,8]:'.00000'
    SETTLEMENT.DATE = SETT.DATE[1,4]:'-':SETT.DATE[5,2]:'-':SETT.DATE[7,2]

*****************GET CUS AND ACC *****************************

    DB.ACC.NO        = R.NEW(FT.DEBIT.ACCT.NO)
    IF DB.ACC.NO[1,1] EQ '0' THEN
        DB.CUS           = DB.ACC.NO[2,7]
    END
    ELSE
        DB.CUS = DB.ACC.NO[1,8]
    END
    CR.ACC.NO        = R.NEW(FT.BEN.ACCT.NO)
    IF CR.ACC.NO[1,1] EQ '0' THEN
        CR.CUS           = CR.ACC.NO[2,7]
    END
    ELSE

        CR.CUS           = CR.ACC.NO[1,8]
    END
*******GET CUS NAME*******************************************
    DB.CUS.NAME = R.NEW(FT.ORDERING.CUST)
    CR.CUS.NAME = R.NEW(FT.BEN.CUSTOMER)
*******GET BRN CODE*******************************************
    DB.BRN.CODE   =  R.NEW(FT.LOCAL.REF)<1,FTLR.DEBIT.BRANCH>
    CR.BRN.CODE   =  R.NEW(FT.LOCAL.REF)<1,FTLR.BANK.BR.ACH>
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,CR.BRN.CODE,CR.BRN.NAME)
    REASON.CODE   =  R.NEW(FT.LOCAL.REF)<1,FTLR.ACH.FT.REASON>

    CALL DBR('SCB.REASONS':@FM:RES.REASON.NAME,REASON.CODE,REASON)
*******GET RECEIVER BANK CODE*********************************

    BNK.CODE        =  R.NEW(FT.LOCAL.REF)<1,FTLR.BANK.ACH>
    CALL DBR('SCB.BANK':@FM:SCB.BAN.RESERVED.FIELD1,BNK.CODE,BIC.CODE)
*=====================FT OR MG    =========================
    TRN.TYP = 'FT'
    IF R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME> EQ ',MG02' THEN
        TRN.TYP = 'MG'
    END
    IF R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME> EQ ',SCBINTER12' THEN
        TRN.TYP = 'IN'
        CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,DB.ACC.NO,CUS)
        CALL F.READ(FN.CU,CUS,R.CU,F.CU,E1)
        DB.CUS.NAME = R.CU<EB.CUS.NAME.1><1,1>
    END

*=====================FILL PACS008=========================

*R.PAC<PACS8.BATCH.ID>            =      BATCH.ID
    R.PAC<PACS8.CREATION.DATE.TIME>  =      CREATION.DATE
    R.PAC<PACS8.REQ.SETTLEMENT.DATE> =      SETTLEMENT.DATE
    R.PAC<PACS8.BATCH.TYPE>          =      'CRT'
    R.PAC<PACS8.TRN.DATE>            =      ''
    R.PAC<PACS8.INSTRUCTION.IDENT>   =      ID.NEW
* R.PAC<PACS8.END.OF.END.IDENT>    =      TRANSACTION.ID
    R.PAC<PACS8.END.OF.END.IDENT>    =     ID.NEW
* R.PAC<PACS8.TRANSACTION.ID>      =      TRANSACTION.ID
    R.PAC<PACS8.TRANSACTION.ID>      =  ID.NEW
*R.PAC<PACS8.AMOUNT>              =      R.NEW(FT.DEBIT.AMOUNT)
    R.PAC<PACS8.AMOUNT>              =      R.NEW(FT.LOC.AMT.CREDITED)
    R.PAC<PACS8.CURRENCY>            =      'EGP'
    R.PAC<PACS8.CHARGER.BEARER>      =      'SLEV'
    R.PAC<PACS8.DEBTOR.NAME>         =      DB.CUS.NAME
    R.PAC<PACS8.DEBTOR.ACCOUNT.NO>   =      DB.ACC.NO
    R.PAC<PACS8.DEBTOR.ACCOUNT.TYPE> =      'CACC'
    R.PAC<PACS8.DEBTOR.PARTY.BRANCH.ID>   = DB.BRN.CODE
    R.PAC<PACS8.CREDITOR.NAME>       =      CR.CUS.NAME
    R.PAC<PACS8.CREDITOR.ACCOUNT.NO> =      CR.ACC.NO
    R.PAC<PACS8.CREDITOR.ACCOUNT.TYPE>      =  'CACC'
    R.PAC<PACS8.CREDITOR.PARTY.BRANCH.ID>   = CR.BRN.CODE
    R.PAC<PACS8.CREDITOR.PARTY.BRANCH.NAME> = CR.BRN.NAME
    R.PAC<PACS8.PURPOSE>                    =  REASON
    R.PAC<PACS8.REMITTANCE.INFO1>    =      CR.BRN.CODE
*   R.PAC<PACS8.REMITTANCE.INFO2>    =      'FT'
    R.PAC<PACS8.REMITTANCE.INFO2>    =     TRN.TYP
    R.PAC<PACS8.REMITTANCE.INFO4>    =      R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT>
    R.PAC<PACS8.REMITTANCE.INFO5>    =      SERIAL.NO.FINAL
    R.PAC<PACS8.PACS.TYPE>           =      '2'
    R.PAC<PACS8.CREDITOR.PARTY.BIC>  =      BIC.CODE
    R.PAC<PACS8.DEBTOR.PARTY.BIC>    =      'SUCAEGCXXXX'
    PAC.ID   = T.DATE.TIME:'000-':BATCH.SERIAL
    TEXT = PAC.ID; CALL REM
    CALL F.WRITE (FN.PAC8,PAC.ID,R.PAC)
*END
**************************
* IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)='RNAU' THEN
    IF V$FUNCTION = 'R' THEN
        TT.SEL = "SELECT F.SCB.ACH.PACS008 WITH INSTRUCTION.IDENT EQ ":ID.NEW:" AND TRN.DATE EQ ''"
        CALL EB.READLIST(TT.SEL,PAC.REV.LIST, "", PAC.REV.SEL, PAC.REV.ER)

        IF PAC.REV.LIST THEN
            ACH.ID = PAC.REV.LIST<1>
*TEXT = "ACH.ID= ":ACH.ID; CALL REM
            CALL F.READ(FN.PAC8,ACH.ID,R.REV.PAC,F.PAC8,E.REV)
*TEXT = "ACH.ID= ":ACH.ID; CALL REM
            R.REV.PAC<PACS8.REMITTANCE.INFO2>    =      'FTR'
            CALL F.WRITE (FN.PAC8,ACH.ID,R.REV.PAC)
        END
        ELSE
            TEXT = "You can't reverse ACH for this Transaction"; CALL REM
        END

    END
    RETURN
**************************************************************
PROGRAM.END:
    RETURN
END
