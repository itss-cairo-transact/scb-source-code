* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ADD.MONTHS(REQ.DATE,MN.COUNT)
************************************************************************
***********by Mahmoud Elhawary ** 27/1/2010 *****************************
************************************************************************
    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE
************************************************************************
* convert the date required to add or subtract month(s)
************************************************************************
    TDR   = REQ.DATE
    TDMM  = REQ.DATE
    TDDAY = TDR[7,2]
    TDRMN = TDR[5,2]
    TDRYY = TDR[1,4]
    CALL LAST.DAY(TDMM)
    TDNMN = TDRMN
    YY.COUNT = FMT((MN.COUNT/12),"L2")
    YY.COUNT = FIELD(YY.COUNT,".",1)
    TDRYY    = TDRYY + YY.COUNT
    NUMBER.OF.MONTHS = MN.COUNT - (12 * YY.COUNT)
    NM.MNTH = TDRMN + NUMBER.OF.MONTHS
    TDNMN  = NM.MNTH
    IF NM.MNTH LT 1 THEN
        TDRYY    = TDRYY - 1
        TDNMN    = 12 + NM.MNTH
    END
    IF NM.MNTH GT 12 THEN
        TDRYY    = TDRYY + 1
        TDNMN    = NM.MNTH - 12
    END
    X = LEN(TDNMN)
    IF X = 1 THEN
        TDNMN = '0':TDNMN
    END
    TDREM = TDRYY:TDNMN:TDDAY
    TDTESTER = TDREM
    CALL LAST.DAY(TDTESTER)
    IF TDTESTER LT TDREM THEN
        TDREM = TDTESTER
    END
    IF TDR EQ TDMM THEN
        CALL LAST.DAY(TDREM)
    END
    TDD = TDREM
    REQ.DATE = TDD
    RETURN
