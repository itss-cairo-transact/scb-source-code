* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CU.N.AC(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    I = 0
    KK = 0
*=============================================================*

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ;  R.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CAC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CAC = '' ; R.CAC = ''
    CALL OPF(FN.CAC,F.CAC)
    I = 0
*====================================================================*
    T.SEL = "SELECT ":FN.CU:" WITH COMPANY.BOOK EQ ":COMP:" AND POSTING.RESTRICT LT 90 AND NEW.SECTOR NE '' BY @ID"
*    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND NEW.SECTOR NE '' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED,ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CAC,KEY.LIST<I>,R.CAC,F.CAC,E.CAC)
            IF E.CAC THEN
                KK++
                ENQ<2,KK> = "@ID"
                ENQ<3,KK> = "EQ"
                ENQ<4,KK> = KEY.LIST<I>
            END
        NEXT I
    END
    IF KK = 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'NO RECORDS'
    END
    RETURN
*******************************************************************************************
END
