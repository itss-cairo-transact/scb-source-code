* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>279</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ASP.AS.BAL.FMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT

    EXECUTE "COMO ON ASP.AS.BAL.FMT_":TODAY:"_":TIME()
    GOSUB INIT
    GOSUB PROCESS
    EXECUTE "COMO OFF ASP.AS.BAL.FMT_":TODAY:"_":TIME()
    RETURN

INIT:

    FN.ACC = "F.ACCOUNT"
    FV.ACC = ""
    FN.ASP = "F.ACCT.STMT.PRINT"
    FV.ASP = ""
    FN.AS = "F.ACCOUNT.STATEMENT"
    FV.AS = ""

    ACC.ID = "" ; SEL.CMD = "" ; SEL.LIST = "" ; NO.OF.ACC = "" ; ACC.ERR = ""

    CALL OPF(FN.ACC,FV.ACC)
    CALL OPF(FN.ASP,FV.ASP)
    CALL OPF(FN.AS,FV.AS)

    RETURN

PROCESS:

*   SEL.CMD = "GET.LIST ACC.LIST.CORR"
*   CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.ACC,ACC.ERR)
        OPEN '&SAVEDLISTS&' TO F.SL ELSE CRT 'Unable to open Savedlists directory'
        READ SEL.LIST FROM F.SL, "ACC.LIST.CORR" ELSE CRT 'Savedlists file ACC.LIST.CORR not found'
    LOOP
        ACC.ID = "" ; POS = ""
        REMOVE ACC.ID FROM SEL.LIST SETTING POS
    WHILE ACC.ID:POS
        R.ASP = ""
        READ R.ASP FROM FV.ASP,ACC.ID ELSE R.ASP = ""

        IF R.ASP THEN
        STMT.DATES = "" ; STMT.BALS = "" ; STMT.BAL = ""
        STMT.DATES = FIELDS(R.ASP,"/",1,1)
        STMT.BALS = FIELDS(R.ASP,"/",2,1)
        CNT = DCOUNT(STMT.BALS,@FM)
        STMT.BAL = STMT.BALS<CNT>
        IF STMT.BAL THEN
            CRT "BEFORE FORMAT: ":STMT.BAL
            CALL EB.ROUND.AMOUNT('CCY',STMT.BAL,"","")
            CRT "AFTER FORMAT: ":STMT.BAL
            STMT.BALS<CNT> = STMT.BAL
        END

        IF R.ASP THEN
            R.ASP = SPLICE(STMT.DATES, "/", STMT.BALS)
            WRITE R.ASP TO FV.ASP,ACC.ID
        END

        R.AS = ""
        READ R.AS FROM FV.AS,ACC.ID ELSE R.AS = ""

        IF R.AS THEN
        AS.BAL = ""
        AS.BAL = R.AS<AC.STA.FQU1.LAST.BALANCE>
        IF AS.BAL THEN
            CRT "BEFORE FORMAT: ":AS.BAL
            CALL EB.ROUND.AMOUNT('CCY',AS.BAL,"","")
            CRT "AFTER FORMAT: ":AS.BAL
            R.AS<AC.STA.FQU1.LAST.BALANCE> = AS.BAL
        END

        IF R.AS THEN WRITE R.AS TO FV.AS,ACC.ID
        END
        END

    REPEAT
    RETURN
END
