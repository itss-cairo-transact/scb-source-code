* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>779</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACCT.STMT.REBUILD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BATCH.FILES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    EXECUTE 'COMO ON ACCT.STMT.REBUILD'

    FN.ACC = "F.ACCOUNT"
    F.ACC = ""
    CALL OPF(FN.ACC,F.ACC)

    FN.SP = "F.STMT.PRINTED"
    F.SP = ""
    CALL OPF(FN.SP,F.SP)

    FN.ASP = "F.ACCT.STMT.PRINT"
    F.ASP = ""
    CALL OPF(FN.ASP,F.ASP)

    FN.SE = "F.STMT.ENTRY"
    F.SE = ""
    CALL OPF(FN.SE,F.SE)

    FN.AS = "F.ACCOUNT.STATEMENT"
    F.AS = ""
    CALL OPF(FN.AS,F.AS)

    MNE.ID = FN.SE[2,3]; ACC.ID= ""
    

    OPEN '&SAVEDLISTS&' TO SAVE.LIST ELSE SAVE.LIST = ""
   ACC.ID = ''


   READ ACC.ID FROM SAVE.LIST,'ASP.COR' ELSE
      ACC.ID=''
   END

    TOT.ACCS = DCOUNT(ACC.ID, @FM)
  

    FOR PR.AC =1 TO TOT.ACCS
        ASP.ID = ACC.ID<PR.AC>
        CRT "Processing account no : ":ASP.ID
        GOSUB CORRECT.PRINTED.ACCOUNTS
    NEXT PR.AC
    EXECUTE 'COMO OFF ACCT.STMT.REBUILD'

    RETURN

*========================
CORRECT.PRINTED.ACCOUNTS:
*========================

    ASP.REC = "" ; R.AS = ""
    READU R.ACCOUNT FROM F.ACC, ASP.ID ELSE R.ACCOUNT = ''

    READ ASP.REC FROM F.ASP, ASP.ID ELSE ASP.REC = ""
    READ R.AS FROM F.AS, ASP.ID ELSE R.AS = ""

    STMT.DATES = FIELDS(ASP.REC, "/", 1, 1)
    STMT.BALS = FIELDS(ASP.REC, "/", 2, 1)
*Line [ 93 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    NEXT.DATE.NO = DCOUNT(STMT.DATES, @FM)

*Line [ 96 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
   LOCATE '20170101' IN STMT.DATES<1> BY 'AR' SETTING ASP.POS ELSE NULL

    FOR I = ASP.POS  TO NEXT.DATE.NO
        IF STMT.DATES<I+1> THEN
            BALANCE.DATE = STMT.DATES<I>
            YBALANCE = ""
            CR.MVMT = ""
            DR.MVMT = ""
            ERR = ""
            CALL EB.GET.ACCT.BALANCE(ASP.ID, R.ACCOUNT, "BOOKING", BALANCE.DATE, "", YBALANCE, CR.MVMT, DR.MVMT, ERR)   
            STMT.BALS<I+1> = YBALANCE + 0
            IF R.AS THEN
                R.AS<AC.STA.FQU1.LAST.BALANCE> = YBALANCE + 0
                R.AS<AC.STA.FQU1.LAST.DATE> = BALANCE.DATE
            END
        END 
    NEXT I

    IF STMT.DATES THEN
        CRT 'ASP record before rebuild'
        CRT ASP.REC
        ASP.REC = SPLICE(STMT.DATES, '/', STMT.BALS)
        CRT 'ASP record after rebuild'
        CRT ASP.REC
        WRITE ASP.REC TO F.ASP, ASP.ID
    END

    IF R.AS THEN
        WRITE R.AS TO F.AS, ASP.ID
    END

    RETURN
END
