* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
**** CREATED BY MOHAMED SABRY 2011/06/23 ****
*********************************************
    SUBROUTINE BLD.INDIR.LI.NOT.CON(ENQ.DATA)
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY
    WS.COUNT = 0
    FN.LI      = "FBNK.LIMIT"                    ; F.LI      = ""  ; R.LI     = ""
    CALL OPF (FN.LI,F.LI)

    SEL.CMD ="SELECT " :FN.LI:" WITH @ID UNLIKE 994... AND INTERNAL.AMOUNT EQ '' AND ( COMMT.AMT.AVAIL NE '' OR TOTAL.OS NE '' ) AND LIMIT.PRODUCT IN ( 2505 2510 2520 5105 5110 5120 5620 ) AND COMPANY.CO EQ ":COMP:" BY @ID "
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,RTNCD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            WS.ESC.ID   = FIELD(KEY.LIST<I>,".",4)
            WS.CUSTOMER = FIELD(KEY.LIST<I>,".",1)

            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUSTOMER,LOCAL.REF)
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF  WS.ESC.ID NE '' THEN
                GO TO NEX.REC.I
            END
            IF ( WS.CREDIT.CODE EQ 110 OR WS.CREDIT.CODE EQ 120 ) THEN
                GO TO NEX.REC.I
            END
            WS.COUNT ++
            ENQ.DATA<2,WS.COUNT> = "@ID"
            ENQ.DATA<3,WS.COUNT> = "EQ"
            ENQ.DATA<4,WS.COUNT> = KEY.LIST<I>
NEX.REC.I:
        NEXT I

    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    IF WS.COUNT EQ 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
