* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.GET.STMT.LOCK.ACCT(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*===========================================
    FN.LOC = "FBNK.AC.LOCKED.EVENTS"  ; F.LOC = ""
    CALL OPF(FN.LOC,F.LOC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
*------------------
    END.DATE = TODAY
    FROM.DATE  = TODAY
    CALL CDT('',FROM.DATE,'-15C')
    TEXT = "FROM: ":FROM.DATE : "TO: ":END.DATE   ; CALL REM

    T.SEL  = "SELECT FBNK.AC.LOCKED.EVENTS WITH ACCOUNT.NUMBER NE ''"
    T.SEL := " BY ACCOUNT.NUMBER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    Z = 1
    IF SELECTED THEN
        FOR II = 1 TO SELECTED

            CALL F.READ(FN.LOC,KEY.LIST<II>,R.LOC,F.LOC,ER.LOC)
            ACCT.NUM = R.LOC<AC.LCK.ACCOUNT.NUMBER>
            NEW.REC  = ACCT.NUM

            IF II EQ 1 THEN
                OLD.REC = NEW.REC
            END

            IF NEW.REC NE OLD.REC THEN
                CALL EB.ACCT.ENTRY.LIST(NEW.REC<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)

                LOOP
                    REMOVE STE.ID FROM ID.LIST SETTING POS
                WHILE STE.ID:POS

                    IF STE.ID NE '' THEN
                        ENQ<2,Z> = '@ID'
                        ENQ<3,Z> = 'EQ'
                        ENQ<4,Z> = STE.ID
                        Z = Z + 1
                    END
                REPEAT
            END

            OLD.REC = NEW.REC
        NEXT II
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUMMY'
    END
*=============================================
    RETURN
END
