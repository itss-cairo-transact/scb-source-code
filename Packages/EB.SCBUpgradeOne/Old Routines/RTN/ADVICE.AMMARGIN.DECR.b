* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
**************** RIHAM YOUSSEF 05/11/2014 *******
*-----------------------------------------------------------------------------
* <Rating>-290</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ADVICE.AMMARGIN.DECR
***************PROGRAM ADVICE.AMMARGIN.DECR


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

        GOSUB CALLDB
        MYID = MYCODE:'.':MYTYPE
IF MYCODE EQ '1241' THEN
        GOSUB INITIATE
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM

    END

    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.AMMARGIN.DECR'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
    LOCAL.REF         = R.LD<LD.LOCAL.REF>
    BENF1             = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2             = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    LG.NO             = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO2            = LOCAL.REF<1,LDLR.OLD.NO>
    AC.NUM            = R.LD<LD.CHRG.LIQ.ACCT>
    AC.NO             = AC.NUM[1,8]:AC.NUM[9,2]:AC.NUM[11,4]:AC.NUM[15,2]
    THIRD.NO          = LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NUM,CUSS)
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
    THIRD.NAME1       = LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME.2      = LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1       = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2       = LOC.REF<1,CULR.ARABIC.ADDRESS,2>

*****************************************
**    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
**    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,CO.BOOK)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.BOOK,BRANCH)

    YYBRN             = FIELD(BRANCH,'.',2)
*****************************************
    VALUE.DATE        =  LOCAL.REF<1,LDLR.MATUR.DATE>
    IF VALUE.DATE NE '' THEN
        DATY          = LOCAL.REF<1,LDLR.MATUR.DATE>
    END ELSE
        DATY          = R.LD<LD.DATE.TIME>[1,6]
        DATY          = '20':R.LD<LD.DATE.TIME>[1,6]
    END
    LG.TYPE           = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME           = TYPE.NAME
    LG.AMT            = R.LD<LD.AMOUNT>
    CUR               = R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE           = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE          = LG.DATE[3,2]:"/":LG.DATE[5,2]:"/":LG.DATE[0,4]
    MARG.AMT          = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1           = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2           = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER          = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE          = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE      = END.DATE[7,2]:'/':END.DATE[5,2]:"/":END.DATE[1,4]
    DIFFS             = R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
    DIFF              = ABS(DIFFS)
    MYCODE            = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE            = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    TOT               = DIFF + LG.COM1
    MARG.OLD.PER = ''
*******************
    IF ID.NEW NE '' THEN
        INPUTTER    = R.NEW(LD.INPUTTER)
        INP         = FIELD(INPUTTER,'_',2)
    END ELSE
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
    END

    IF ID.NEW NE '' THEN
        AUTH        = OPERATOR
        MARG.OLD.PER= R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END

    IF ID.NEW EQ '' THEN
        REF         = COMI
    END ELSE
        REF = ID.NEW
    END
    LG.CO = R.LD<LD.CO.CODE>
***********************
    RETURN
*=================================================
BODY:
*    PR.HD  ="'L'":SPACE(6):"ADVICE.AMMARGIN.DECR"
    PR.HD ="'L'":SPACE(6):"����� ���"
    PR.HD :="'L'":"���� �       :":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������      :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������   :":AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� ����   :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������      :":CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������  :":THIRD.NAME1
    IF THIRD.NAME.2 THEN
        PR.HD :="'L'":SPACE(15) :THIRD.NAME.2
    END ELSE
        PR.HD :="'L'":" "
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������     :":THIRD.ADDR1
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO2:"(":LG.NAME:")"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(25):" �������������"
    PR.HD :="'L'":" "
*    IF MARG.OLD.PER EQ '100' THEN
*        PR.HD :="'L'":SPACE(6):LG.COM1:SPACE(19): "������� �� ������ �������� ��" : END.COM.DATE
 *   END ELSE
        PR.HD :="'L'":SPACE(6):LG.COM1:SPACE(19):"������� ��(����� ���� ������-������ �������� ��)": END.COM.DATE
   * END
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    TOTY    = LG.COM1+LG.COM2
    TOTY2   = LG.COM1+LG.COM2
    TOTL    = FMT(TOTY,"R2#10")
    PR.HD :="'L'":SPACE(6):TOTL:SPACE(19):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    CALL WORDS.ARABIC(TOTY,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 199 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 201 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I
    PR.HD :="'L'":SPACE(6) :"�� ��� ���� ������"
    PR.HD :="'L'":"======================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):"��� �������":SPACE(15):"������":SPACE(15):"������"
    PR.HD :="'L'":SPACE(6):INP:SPACE(20):AUTH:SPACE(20):REF
    HEADING PR.HD
    PRINT SPACE(6):"ADVICE.AMMARGIN.DECR"
    CALL LG.ADD(AUTH,LG.CO)
    RETURN
*===================================================
END
