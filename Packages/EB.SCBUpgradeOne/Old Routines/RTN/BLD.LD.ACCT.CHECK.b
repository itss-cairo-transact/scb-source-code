* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.LD.ACCT.CHECK(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*------------------------------------------------
    COMP = ID.COMPANY

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'  ;  F.LD = ''
    CALL OPF(FN.LD,F.LD)

    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH"
    T.SEL := " CO.CODE EQ " : COMP
    T.SEL := " AND CATEGORY GE 21001 AND CATEGORY LE 21015"
    T.SEL := " AND SECTOR NE ''"
    T.SEL := " AND AMOUNT GT 0"
    T.SEL := " AND RENEW.IND EQ 'YES'"
    T.SEL := " AND RENEW.METHOD EQ 2"
    T.SEL := " AND (( DRAWDOWN.ACCOUNT NE PRIN.LIQ.ACCT )"
    T.SEL := " OR ( DRAWDOWN.ACCOUNT NE INT.LIQ.ACCT )"
    T.SEL := " OR ( PRIN.LIQ.ACCT NE INT.LIQ.ACCT ))"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            ENQ<2,II> = '@ID'
            ENQ<3,II> = 'EQ'
            ENQ<4,II> = KEY.LIST<II>
        NEXT II
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUMMY'
*ENQ.ERROR = "NO RECORDS FOUND"
    END
**********************
    RETURN
END
