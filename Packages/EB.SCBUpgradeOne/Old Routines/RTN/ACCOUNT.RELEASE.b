* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACCOUNT.RELEASE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.GEN.CONDITION
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCOUNT.TYPES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.SERIAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    RETURN

*==============================================================
INITIATE:
    REPORT.ID='ACCOUNT.RELEASE'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*===============================================================
CALLDB:
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

*   T.SEL = "SELECT FBNK.ACCOUNT WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" BY WORKING.BALANCE"
    T.SEL = "SELECT FBNK.ACCOUNT BY @ID"
* 0110000610100102
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    X = 0
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACCT,KEY.LIST<I>,R.ACCT,F.ACCT,E1)
            ZZ = R.ACCT<AC.FROM.DATE>
            IF R.ACCT<AC.FROM.DATE> NE '' THEN
*Line [ 85 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                YYY = DCOUNT(R.ACCT<AC.LOCKED.AMOUNT>,@VM)
                TT = 0.00
                FOR  Z = 1 TO YYY
                    TT += R.ACCT<AC.LOCKED.AMOUNT,Z>
                NEXT Z
                IF TT = 0 THEN
                    IF NUM(KEY.LIST<I>) THEN
                        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.ACCT<AC.CUSTOMER>,LOCAL.REF)
                        LOC.REF   = R.ACCT<AC.LOCAL.REF>
                        CUS.NO    = R.ACCT<AC.CUSTOMER>
                        CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                        H.DATE    = R.ACCT<AC.LOCAL.REF><1,ACLR.HOLD.DATE,1>
                        IF H.DATE THEN
                            H.DATE    = H.DATE[7,2]:"/":H.DATE[5,2]:"/":H.DATE[0,4]
                        END
                        H.USER    = R.ACCT<AC.LOCAL.REF><1,ACLR.HOLD.USER,1>
                        CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                        ACCT.NO   = KEY.LIST<I>
                        XX = SPACE(120)
                        XX<1,1>[1,20]   = ACCT.NO
                        XX<1,1>[25,35]  = CUST.NAME
                        XX<1,1>[65,20]  = H.DATE
                        XX<1,1>[100,20] = H.USER
                        PRINT XX<1,1>
                    END
                    X ++
                END
            END
        NEXT I
        PRINT STR('=',120)
        PRINT ; PRINT "������� = ":X:" �����"
    END ELSE
        TEXT = "NO ACCOUNT EXIST"
    END
    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ��������������� ���� ��� ���� ������ "
    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������" :SPACE(15):" ��� ������" :SPACE(25):" ����� ��� ������":SPACE(25):"���� ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
