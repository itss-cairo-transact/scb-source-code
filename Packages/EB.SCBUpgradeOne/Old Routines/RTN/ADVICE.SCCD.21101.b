* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
********************************NI7OOOOOOOOOOOOOOOOOOOOO**********************
    SUBROUTINE ADVICE.SCCD.21101

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST


    FOR I = 1 TO 2
        GOSUB INITIATE
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    NEXT I
**********MODIFIED BY ABEER AS OF 2017-09-11
    RETURN
************END OF MODIFICATION

*********************
INITIATE:
    REPORT.ID='ADVICE.SCCD.21101'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*========================================================================
PROCESS:
*---------------------

    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    FN.CORD = 'FBNK.BASIC.INTEREST' ;F.CORD = '' ; R.CORD = ''
    CALL OPF(FN.CORD,F.CORD)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    DATE1 = ''
    DATE2 = ''
    XDATE = ''

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*------------------------------------------------------------------------
    AMT     = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT>
    CUR     = R.NEW(LD.CURRENCY)
    SPREAD  = R.NEW(LD.INTEREST.SPREAD)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR,CUR22)
    DATE1   = R.NEW(LD.VALUE.DATE)
    DATE2   = R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>
    IF DATE2 LT DATE1 THEN
        XDATE =  DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]
    END ELSE
        IF DATE2 GT DATE1 THEN
            XDATE =  DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
        END
    END
    IF DATE2  = '' THEN
        XDATE = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
    END
    FINDATE1  = R.NEW(LD.FIN.MAT.DATE)
    FINDATE   = FINDATE1[1,4]:'/':FINDATE1[5,2]:"/":FINDATE1[7,2]
    NAME11    = R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF>
    RAT       = R.NEW(LD.INTEREST.RATE)
    RAT2      = R.NEW(LD.INTEREST.KEY)
    RATE3     = RAT2:'...'
    CD.TYP    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
**********************
    BRN.CODE = R.NEW(LD.CO.CODE)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.CODE,YYBRN)
***************************
    CALL DBR('SCB.CD.TYPES':@FM:CD.TYPES.DESCRIPTION,CD.TYP,DESC)
    TYP    = FIELD(DESC,'-',2)

    IN.AMOUNT = AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR22 : ' ' : '�����'

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
    XX9  = SPACE(132)

*    XX<1,1>[3,15]      =  '��� �������   :'
    XX<1,1>[3,15]      =  '��� �������'
    XX<1,1>[45,15]     = ID.NEW

    XX2<1,1>[3,15]     = '�����       : '
    XX2<1,1>[45,35]    = NAME11

    XX3<1,1>[3,15]     = '���� ������� � ������ ������: '
    XX3<1,1>[45,35]    = AMT
    XX11<1,1>[45,35]   = OUT.AMT
*******************
    CD.TYP    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
    INT.MON = FIELD(CD.TYP,'-',3)

    XX4<1,1>[3,15]     = '���� ����� :'
    XX4<1,1>[45,3]     =R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    XX5<1,1>[3,15]     = '���� �������  : '
    XX5<1,1>[45,15]    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>


    NSN.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>
    XX6<1,1>[3,15]     = '����� ������'
    XX6<1,1>[45,15]    = NSN.ID


*********
    XX7<1,1>[3,15]= '����� ���� �������'

    XX8<1,1>[3,15]     = '��� �������'

    XX9<1,1>[3,15]     = '����� �������� ���������'


*********

**YYBRN  = FIELD(BRANCH.CO,'.',2)
    PR.HD ="'L'":"ADVICE.SCCD.21101"
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD = ''
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
*********************2018-01-22
***************************2018-01-22
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT STR('=',82)
    PRINT XX8<1,1>
    PRINT STR('=',82)
    PRINT XX9<1,1>
    PRINT STR('=',82)

    RETURN
END
