* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1199</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACCT.ACT.BAL.FMT
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    EXECUTE "COMO ON ACCT.ACT.BAL.FMT_":TODAY:"_":TIME()
    GOSUB INIT
    GOSUB PROCESS
    EXECUTE "COMO OFF ACCT.ACT.BAL.FMT_":TODAY:"_":TIME()

    RETURN

*****
INIT:
*****

    FN.ACCOUNT='F.ACCOUNT'
    F.ACCOUNT=''

    FN.ACCT.ACTIVITY= 'F.ACCT.ACTIVITY'
    FV.ACCT.ACTIVITY= ' '

    CALL OPF(FN.ACCOUNT,F.ACCOUNT)
    CALL OPF(FN.ACCT.ACTIVITY,FV.ACCT.ACTIVITY)

    RETURN

********
PROCESS:
********
*   SEL.CMD1 = "GET.LIST ACC.LIST.CORR"
*   CALL EB.READLIST(SEL.CMD1,SEL.LIST1,'',NO.OF.ACC,ACC.ERR)
        OPEN '&SAVEDLISTS&' TO F.SL ELSE CRT 'Unable to open Savedlists directory'
        READ SEL.LIST1 FROM F.SL, "ACC.LIST.CORR" ELSE CRT 'Savedlists file ACC.LIST.CORR not found'
    LOOP
        ACCOUNT.NO = "" ; POS = ""
        REMOVE ACCOUNT.NO FROM SEL.LIST1 SETTING POS
    WHILE ACCOUNT.NO:POS
        CRT "**************************************************"
        CRT "PROCESSING ACCOUNT:":ACCOUNT.NO
        SEL.CMD='SELECT ':FN.ACCT.ACTIVITY:' WITH @ID LIKE ':ACCOUNT.NO:'-...'
        CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

        IF SEL.LIST THEN
            TOT=""

            Y.ACC.ID = ' '
            ACT.BAL=''

            AA.POS = ' '
            AA.POS = NO.OF.REC
READACT.ID:
            IF AA.POS NE '0' THEN
                SEL.LIST=SORT(SEL.LIST)
                ACT.ID = SEL.LIST<AA.POS>

                READ ACT.REC FROM FV.ACCT.ACTIVITY,ACT.ID ELSE
                    CRT "No ACCT.ACTIVITY record for ":Y.ACC.ID
                    GOTO NEXT1
                END

                D1.NO=DCOUNT(ACT.REC<IC.ACT.DAY.NO>,@VM)
                ACT.BAL1= ACT.REC<IC.ACT.BALANCE,D1.NO>
                CRT "BEFORE FORMAT: ":ACT.BAL1
                CALL EB.ROUND.AMOUNT('CCY',ACT.BAL1,"","")
                CRT "AFTER FORMAT: ":ACT.BAL1
                ACT.REC<IC.ACT.BALANCE,D1.NO> = ACT.BAL1
                WRITE ACT.REC TO FV.ACCT.ACTIVITY,ACT.ID

                D.NO=DCOUNT(ACT.REC<IC.ACT.BK.DAY.NO>,@VM)
                ACT.BAL= ACT.REC<IC.ACT.BK.BALANCE,D.NO>
                IF ACT.BAL THEN
                    GOSUB COMPARE
                END

                IF ACT.BAL EQ '' AND D.NO GT 1 THEN
                    D.NO = D.NO-1
                    ACT.BAL= ACT.REC<IC.ACT.BK.BALANCE,D.NO>
                    IF ACT.BAL THEN
                        GOSUB COMPARE
                    END
                    IF ACT.BAL EQ '' THEN
                        AA.POS= AA.POS-1
                        GOTO READACT.ID
                    END
                END ELSE
                    IF ACT.BAL EQ '' THEN
                        AA.POS= AA.POS-1
                        GOTO READACT.ID
                    END
                END
            END
NEXT1:

        END
    REPEAT
    RETURN

********
COMPARE:
********
    CRT "BEFORE FORMAT: ":ACT.BAL
    CALL EB.ROUND.AMOUNT('CCY',ACT.BAL,"","")
    CRT "AFTER FORMAT: ":ACT.BAL
    ACT.REC<IC.ACT.BK.BALANCE,D.NO> = ACT.BAL
    WRITE ACT.REC TO FV.ACCT.ACTIVITY,ACT.ID

    RETURN
