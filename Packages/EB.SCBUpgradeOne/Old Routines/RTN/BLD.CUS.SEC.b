* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUS.SEC(ENQ.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*    $INCLUDE T24.BP I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.SECURITY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    COMP = ID.COMPANY
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
    KK = 0
    WS.CUS = 0

    FN.SCS = 'F.SCB.CUSTOMER.SECURITY' ; R.SCS = '' ; F.SCS = ''

    CALL OPF(FN.SCS,F.SCS)

    T.SEL = "SELECT F.SCB.CUSTOMER.SECURITY WITH @ID NE '' "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*    TEXT = SELECTED  ; CALL REM
    *IF SELECTED THEN
        FOR Y = 1 TO SELECTED
            CALL F.READ(FN.SCS,KEY.LIST<Y>,R.SCS,F.SCS,E.SCS)
* CU.ID = R.SCS<SCS.CUSTOMER.ID>

*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.CUS = DCOUNT(R.SCS<SCS.CUSTOMER.ID>,@VM)
            IF KEY.LIST<Y> EQ 'AE.1874932' THEN
                *TEXT = KEY.LIST<Y> :' COUNT ' :WS.CUS ; CALL REM
            END
         *   TEXT = KEY.LIST<Y> :' COUNT ' :WS.CUS
            IF WS.CUS GE 2 THEN
                KK++
                ENQ.DATA<2,KK> = '@ID'
                ENQ.DATA<3,KK> = 'EQ'
                ENQ.DATA<4,KK> = KEY.LIST<Y>
                WS.CUS = 0
            END
        NEXT Y
        IF KK EQ 0 THEN
            ENQ.DATA<2,2> = '@ID'
            ENQ.DATA<3,2> = 'EQ'
            ENQ.DATA<4,2> = 'DUMMY'
        END
    *END ELSE
*  IF KK EQ 0 THEN
    *    ENQ<2,2> = '@ID'
    *    ENQ<3,2> = 'EQ'
    *    ENQ<4,2> = 'DUMMY'
    *END
    *TEXT = KK  ; CALL REM
    RETURN
*****************************************************************************
END
