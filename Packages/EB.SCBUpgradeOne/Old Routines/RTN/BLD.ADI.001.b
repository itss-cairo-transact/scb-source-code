* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.ADI.001(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    FN.AC   = 'FBNK.ACCOUNT'           ; F.AC  = ''
    FN.ADI  = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = '' ; R.ADI = ''

    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.ADI,F.ADI)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
*************************************************************************
    T.SEL = "SELECT ":FN.ADI:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    Z = 1
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ADI,KEY.LIST<I>,R.ADI,F.ADI,E1)
            ACC.NO  = FIELD(KEY.LIST<I>,'-',1)
            ACC.NO2 = FIELD(KEY.LIST<I+1>,'-',1)

            CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,E2)
            CATEG = R.AC<AC.CATEGORY>

            IF CATEG NE 1220 AND CATEG NE 1221 AND CATEG NE 1222 AND CATEG NE 1223 AND CATEG NE 1224 AND CATEG NE 1225 AND CATEG NE 1226 AND CATEG NE 1227 AND CATEG NE 9090 THEN

                IF ACC.NO NE ACC.NO2  THEN

                    ENQ<2,Z> = "@ID"
                    ENQ<3,Z> = "EQ"
                    ENQ<4,Z> = KEY.LIST<I>
                    Z = Z + 1
                END
            END

        NEXT I
    END
    RETURN
*==============================================================
END
