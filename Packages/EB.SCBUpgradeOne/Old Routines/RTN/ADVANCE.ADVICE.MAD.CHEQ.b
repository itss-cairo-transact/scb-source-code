* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
******* WAEL *******

    SUBROUTINE ADVANCE.ADVICE.MAD.CHEQ

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB INITIATE
    GOSUB CALLDB
    IF LG.COM1 NE '' THEN
        IF LG.COM2 NE '' THEN
            GOSUB BODY
        END
    END
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVANCE.ADVICE.MAD.CHEQ'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
*  LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
********************************************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
*    LG.NO='LG/': SAM:"/": SAM1:"/":SAM2
    LG.NO=LG.NO
********************************************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
*****************************************
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
** THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
    THIRD.NAME1=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    OP.CODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
**************************************************
**    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
**    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)

    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,CO.BOOK)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.BOOK,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
**************************************************
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    LG.MATURITY = R.LD<LD.FIN.MAT.DATE>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE = R.LD<LD.FIN.MAT.DATE>
*    LG.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    MARG.AMT=FMT(MARG.AMT,"R2")
    LG.COM1 = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM1 = FMT(LG.COM1,"R2")
    LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    LG.COM2=FMT(LG.COM2,"R2")
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
*    LG.V.DATE = R.LD<LD.VALUE.DATE>
    LG.V.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    LG.VALUE.DATE =LG.MATURITY[7,2]:"/":LG.MATURITY[5,2]:"/":LG.MATURITY[1,4]
**************************
    IF ID.NEW NE '' THEN
        INPUTTER    = R.NEW(LD.INPUTTER)
        INP         = FIELD(INPUTTER,'_',2)
    END ELSE
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
    END

    IF ID.NEW NE '' THEN
        AUTH        = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END

    IF ID.NEW EQ '' THEN
        REF         = COMI
    END ELSE
        REF = ID.NEW
    END
    LG.CO = R.LD<LD.CO.CODE>
*------------------------------------------
***    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

*FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
*CALL OPF(FN.LG,F.LG)
*CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)

******NO TAX    MYTAX1 = R.LG<SCB.LG.CH.TAX.AMOUNT,1>
******NO TAX    MYTAX2 = R.LG<SCB.LG.CH.TAX.AMOUNT,2>
******NO TAX    TOT.TAX = MYTAX1 + MYTAX2
******NO TAX    TOT.TAX=FMT(TOT.TAX,"R2")
*--------------------------------------------
    IF OP.CODE EQ 1288 THEN
        TOT = LG.COM1 + LG.COM2
    END ELSE
        TOT = MARG.AMT + LG.COM1+LG.COM2
    END
    TOT=FMT(TOT,"R2")
    RETURN
*=================================================
BODY:
    PR.HD ="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    XX = SPACE(80)
    XX<1,1>[46,20] = " ����� ��� "
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:XX<1,1>
    END
    PR.HD :="'L'":" "
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2

    LNTHD1 = LEN(THIRD.ADDR1)+12 ; LNTHD2 = LEN(THIRD.ADDR2)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2
    PRINT
    PRINT "__________________________________________________"

    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":THIRD.ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":THIRD.ADDR2:SPACE(LNED2):"|"
    PRINT "|_________________________________________________|"

    PR.HD :="'L'":" "

* PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
* IF THIRD.ADDR2 THEN
*     PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
* END
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO:")":LG.NAME:"(":" � ���� ������� ���� ��� ���������  "
* PR.HD :="'L'":" �����":"**":LG.AMT:"**":CRR:"������ ��� " :FIN.DAT
    PR.HD :="'L'":" �����":"**":LG.AMT:"**":CRR
    PR.HD :="'L'":CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
*PR.HD :="'L'":MARG.AMT:SPACE(11):MARG.PER:"%":" ������� ����� �� ���� ���� "
    IF LG.COM1 THEN
* PR.HD :="'L'":LG.COM1:SPACE(19): "������� ���" : END.COM.DATE
        PR.HD :="'L'":LG.COM1:SPACE(19): "������� �� ����� ���� ������"
    END
    IF LG.COM2 THEN
        PR.HD :="'L'":LG.COM2:SPACE(21): "���� � ������� ������ "
    END
*****NO TAX    PR.HD :="'L'":TOT.TAX:SPACE(17):"��� ���� "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":TOT:SPACE(10):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
    IN.AMOUNT=TOT:'.00'
    CALL WORDS.ARABIC(TOT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 244 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 246 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I
*---------------------------
    PR.HD :="'L'":" "
*    PR.HD :="'L'":" ����� ����  ":" : ": DATY
    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):COMI
    HEADING PR.HD
    PRINT "ADVANCE.ADVICE.MAD.CHEQ"
    CALL LG.ADD(AUTH,LG.CO)
    RETURN
*=================================================
END
