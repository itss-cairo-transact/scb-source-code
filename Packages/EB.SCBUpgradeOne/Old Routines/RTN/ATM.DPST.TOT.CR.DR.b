* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>2001</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ATM.DPST.TOT.CR.DR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CASH.DPST
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.DPST.AC.DRCR
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID

** WRITTEN BY NESSREEN AHMED -SCB*******
* TO DEBIT OR CREDIT THE TOTAL PAYMENTS FROM CUSTOMERS VISA ACCOUNT AND PRINTOUT A REPORT CONTANING
* THE CUSTOMERS AND TOTAL AMOUNT TO BE DEBITED

    TEXT = '2ND' ; CALL REM

    F.VISA.TRANS.TOT = '' ; FN.VISA.TRANS.TOT = 'F.SCB.ATM.CASH.DPST' ; R.VISA.TRANS.TOT = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.TRANS.TOT,F.VISA.TRANS.TOT)

    F.VISA.USAGES.TOT = '' ; FN.VISA.USAGES.TOT = 'F.SCB.ATM.DPST.AC.DRCR' ; R.VISA.USAGES.TOT = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.VISA.USAGES.TOT,F.VISA.USAGES.TOT)

    F.ATM.TERM.ID = '' ; FN.ATM.TERM.ID = 'F.SCB.ATM.TERMINAL.ID' ; R.ATM.TERM.ID = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.ATM.TERM.ID,F.ATM.TERM.ID)

    YTEXT = "Enter the Required Date  : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    DATEE = '' ; YYYY = '' ; MM = '' ; MT = '' ; MU = '' ; YYDD = '' ; MON = '' ; YEARN = ''
** CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
    DATEE = COMI
    YYDD = DATEE
    BR = R.USER<EB.USE.DEPARTMENT.CODE>
    T.SEL = "SELECT F.SCB.ATM.CASH.DPST WITH POS.DATE GE ":YYDD :" BY @ID "
****************************
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    IF SELECTED THEN
        TEXT = '��� ������':SELECTED ; CALL REM
        XX=''  ; ZZ=''
        ACCT.NO = ''  ; POS.DATE=''
        TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''  ; TOT.CUS.ALL = ''
        TOT.AMT.CR = '' ; TOT.AMT.DB = '' ; BR.NO = ''
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.VISA.TRANS.TOT,KEY.LIST<I>,R.VISA.TRANS.TOT,F.VISA.TRANS.TOT,E2)
            IF NOT(E2) THEN
                TER.ID<I>       = R.VISA.TRANS.TOT<ATDP.TERMINAL.ID>
*Line [ 85 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(TER.ID<I>,@VM)
                FOR MM = 1 TO DD
                    TER.ID<MM>   = R.VISA.TRANS.TOT<ATDP.TERMINAL.ID,MM>
                    CALL F.READ(FN.ATM.TERM.ID,TER.ID<MM>,R.ATM.TERM.ID,F.ATM.TERM.ID,E2)
                    TERM.COMP<MM>    = R.ATM.TERM.ID<SCB.ATM.BRANCH>
                    POS.DATE<MM>     = R.VISA.TRANS.TOT<ATDP.POS.DATE,MM>
                    MSG.TYPE<MM>     = R.VISA.TRANS.TOT<ATDP.MSG.TYPE,MM>
                    PROCESS.CODE<MM> = R.VISA.TRANS.TOT<ATDP.PROCESS.CODE,MM>
                    BEGIN CASE
                    CASE (MSG.TYPE<MM>[1,4] = "CRDP") AND (PROCESS.CODE<MM>[1,6] = "190000")
                        CR.ACCT.NO<MM>   = R.VISA.TRANS.TOT<ATDP.CUST.ACCT>
                        DR.ACCT.NO<MM>   = R.ATM.TERM.ID<SCB.ATM.ACCOUNT.NUMBER.CRV>
                        AMT.CR<MM>       = R.VISA.TRANS.TOT<ATDP.TRANS.AMT,MM>
                    CASE (MSG.TYPE<MM>[1,4] = "DBDP") AND (PROCESS.CODE<MM>[1,6] = "200000")
                        CR.ACCT.NO<MM>   = R.ATM.TERM.ID<SCB.ATM.ACCOUNT.NUMBER.CRV>
                        DR.ACCT.NO<MM>   = R.VISA.TRANS.TOT<ATDP.CUST.ACCT>
                        AMT.DB<MM>       = R.VISA.TRANS.TOT<ATDP.TRANS.AMT,MM>
                    CASE OTHERWISE
                        TEXT = 'Error New codes'; CALL REM
                        RETURN
                    END CASE
                    VISA.NO<I>      = KEY.LIST<I>[1,16]
                    KEY.ID =  VISA.NO<I>:TODAY:'.':MM
                    CALL F.READ(FN.VISA.USAGES.TOT,KEY.ID,R.VISA.USAGES.TOT,F.VISA.USAGES.TOT,E2)
                    R.VISA.USAGES.TOT<ATDC.VISA.NO>= VISA.NO<I>
                    R.VISA.USAGES.TOT<ATDC.DB.ACCT.NO>= DR.ACCT.NO<MM>
                    R.VISA.USAGES.TOT<ATDC.CR.ACCT.NO>= CR.ACCT.NO<MM>
                    R.VISA.USAGES.TOT<ATDC.TOT.AMT.CR>= AMT.CR<MM>
                    R.VISA.USAGES.TOT<ATDC.TOT.AMT.DB>= AMT.DB<MM>
                    R.VISA.USAGES.TOT<ATDC.POS.DATE>= POS.DATE<MM>
                    R.VISA.USAGES.TOT<ATDC.BRANCH.NUMBER>= TERM.COMP<MM>
                    R.VISA.USAGES.TOT<ATDC.TERMINAL.ID>= TER.ID<MM>
*****************************************************
                    CALL F.WRITE(FN.VISA.USAGES.TOT,KEY.ID, R.VISA.USAGES.TOT)
                    CALL JOURNAL.UPDATE(KEY.ID)
**************************************************************************
                NEXT MM
            END
        NEXT I
    END
******************************************
    RETURN
*==============================================================

END
