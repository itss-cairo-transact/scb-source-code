* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-235</Rating>
*-----------------------------------------------------------------------------
******* WAEL *******

    SUBROUTINE ADVICE.BIDBONDAMEND.NEW.UPDATE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
**CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)
   * IF MYCODE EQ '1239' OR MYCODE EQ '1281'  OR MYCODE EQ '1282' THEN
        GOSUB INITIATE
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM

   * END

    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.BIDBONDAMEND.NEW.UPDATE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
***************************************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
    LG.NO='LG/': SAM:"/": SAM1:"/":SAM2
***************************************
**** AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NUM = R.LD<LD.CHRG.LIQ.ACCT>
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
    THIRD.NAME1=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    LGKIND  =LOCAL.REF<1,LDLR.LG.KIND>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE  = R.LD<LD.FIN.MAT.DATE>
    CATEG    = R.LD<LD.CATEGORY>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1  = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2  = R.LD<LD.CHRG.AMOUNT><1,2>
    LG.SWIFT = R.LD<LD.CHRG.AMOUNT><1,3>
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    LG.V.DATE = R.LD<LD.VALUE.DATE>
    LG.VALUE.DATE = LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    DIFFS =R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
    DIFF = ABS(DIFFS)

    INPUTTER    = R.NEW(LD.INPUTTER)
    INP         = FIELD(INPUTTER,'_',2)
    AUTHORISER  = R.NEW(LD.AUTHORISER)
    AUTH        = FIELD(AUTHORISER,'_',2)
    REF         = COMI
*------------------------------------------
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
** IF (CATEG GT 21096 AND CATEG LT 21099) THEN
    IF (LGKIND EQ 'CBB' OR LGKIND EQ 'CFN' OR LGKIND EQ 'CAD') THEN

        TOT = DIFF + LG.COM1 + LG.COM2 + LG.SWIFT
    END

    IF (LGKIND EQ 'BB' OR LGKIND EQ 'FN' OR LGKIND EQ 'AD') THEN
        TOT = DIFF + LG.COM1
    END

    RETURN
*=================================================
BODY:
    PR.HD  ="'L'":SPACE(1):"��� :":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]
    PR.HD :="'L'":SPACE(50):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    XX = SPACE(80)
    XX<1,1>[46,20] = " ����� ��� "
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:XX<1,1>
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
    IF THIRD.ADDR2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO :  LG.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
*----------
*   PR.HD :="'L'":SPACE(3):DIFF:SPACE(21):" ����� ���� ����� "
*----------
*    PR.HD :="'L'":SPACE(3):LG.COM1:SPACE(21):" ����� ����� ���� �������"
    PR.HD :="'L'":SPACE(3):LG.COM1 + LG.COM2 + LG.SWIFT:SPACE(22): "���� � ������� ������ ������� ����� "
*IF TOT.TAX # 0 THEN
*      PR.HD :="'L'":SPACE(3):TOT.TAX:SPACE(21):"��� ���� "
*END
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="

** IF (CATEG GT 21096 AND CATEG LT 21099) THEN
IF (LGKIND EQ 'CBB' OR LGKIND EQ 'CFN' OR LGKIND EQ 'CAD') THEN
        TOTY = LG.COM1 + LG.COM2 + LG.SWIFT
    END
IF (LGKIND EQ 'BB' OR LGKIND EQ 'FN' OR LGKIND EQ 'AD') THEN
        TOTY = LG.COM1
    END

    PR.HD :="'L'":SPACE(2):TOTY:SPACE(12):" ������������������������"

    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
    IN.AMOUNT=TOTY:".00"
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 201 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 203 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I

*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": LG.VALUE.DATE

    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):COMI


    HEADING PR.HD

    RETURN
*===================================================
END
