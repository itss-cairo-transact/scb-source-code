* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE ACC.EXPT(ENQ.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY


    FN.BILL.REG = "FBNK.ACCOUNT"
    F.BILL.REG = ''
    R.BILL.REG=''
    Y.BILL.REG.ID=''
    Y.BILL.REG.ERR=''
    K = 0
    XX = TODAY

    CALL OPF(FN.BILL.REG,F.BILL.REG)
    SEL.CMD ="SELECT FBNK.ACCOUNT WITH CATEGORY IN (6501 6502 6503 6504) AND CONDITION.GROUP IN (6 23 60 61 62 63 64 65) AND CO.CODE  EQ ":COMP
    CALL EB.READLIST(SEL.CMD,SELLIST,'',SELECTED,RTNCD)
    ENQ.LP = 0
    IF SELECTED THEN
***** ENQ.LP = 0
        FOR ENQ.LP = 1 TO SELECTED
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.DATA<2,2> = '@ID'
        ENQ.DATA<3,2> = 'EQ'
        ENQ.DATA<4,2> = 'DUUMY'
    END
    RETURN
END
