* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.ISCOR.LEG(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*----------------------------------------------------------------------**
    COMP = ID.COMPANY

    FN.ACC   = 'FBNK.CUSTOMER' ; F.ACC    = '' ; R.ACCT = ''
    CALL OPF(FN.ACC,F.ACC)
*-------------------------------------------*
    COMP   = ID.COMPANY
    T.SEL  = "SELECT FBNK.CUSTOMER WITHOUT LEGAL.FORM IN (53 54 33 32 61 2100"
    T.SEL := " 62 63 3200 1000 3000 3100 1100 3400 3300 2200 21 31 4100 4200 5000 6100 27 4400)"
    T.SEL := " AND WITH NEW.SECTOR NE 4650 AND NEW.SECTOR NE 5005 AND POSTING.RESTRICT LT 90"
    T.SEL := " AND SECTOR NE 5010 AND COMPANY.BOOK EQ ":COMP

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    TEXT =  "SEL=  " : SELECTED ; CALL REM
***--------------------------------------------------------------****
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,I> = "@ID"
        ENQ<3,I> = "EQ"
        ENQ<4,I> = "DUMM"
    END
    RETURN
*==============================================================********
END
