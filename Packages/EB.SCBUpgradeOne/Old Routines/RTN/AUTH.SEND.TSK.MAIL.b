* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>587</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE AUTH.SEND.TSK.MAIL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*    $INCLUDE           I_F.SEND.MAIL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROJECT.IMP.STATUS

*    MAIL.ID = ID.NEW

    OPENSEQ "MAIL" , "MAIL.TEST" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE MAIL MAIL.TEST'
        HUSH OFF
    END

    OPENSEQ "MAIL" , "MAIL.TEST" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE MAIL.TEST CREATED IN MAIL'
        END
        ELSE
            STOP 'Cannot create MAIL.TEST File IN MAIL'
        END
    END
************************
    TOO = "" ; TO.PERSON = "" ; RESP = ""
    TO.PERSON = R.NEW(WN.TASK.LEADER)<1,1>

    RESP      = R.NEW(WN.RESPONSIBILITY)<1,1>
    CALL DBR('USER': @FM:EB.USE.LOCAL.REF,TO.PERSON,XXXX)
    CALL DBR('USER': @FM:EB.USE.SIGN.ON.NAME,RESP,XX.NAME)
    TOO = XXXX<1,2>

    TSK.ID    = R.NEW(WN.TASK.ID)<1,1>
    T.ST.DATE = R.NEW(WN.START.DATE)<1,1>
    T.DU.DATE = R.NEW( WN.TARGET.DATE)<1,1>
    T.sum =     R.NEW(WN.REQUIRED.TASK)<1,1>

    SUBJ = "Task Authorised by ":RESP
    X1   = " == Task with ID  = ":TSK.ID:" was authorised by ":RESP
    X2   = " == Best Regards,"
    X3   = " == ": XX.NAME
    BB.DATA = ''

    BB.DATA  = TOO :'|'
    BB.DATA := SUBJ:'|'
    BB.DATA := X1
    BB.DATA := X2
    BB.DATA := X3
    TEXT = 'A mail will be sent to ':TOO; CALL REM
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**********************

    RETURN
END
