* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.BR.CUS.CUR(ENQ)
*    PROGRAM BLD.BR.CUS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    EXECUTE 'COMO ON BLD.BR.CUS'
    FN.BR.CUS = 'F.SCB.BR.CUS.CUR' ; F.BR.CUS = ''
    CALL OPF(FN.BR.CUS,F.BR.CUS)
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    T.SEL.BR.CUS = 'SELECT ':FN.BR.CUS
    CALL EB.READLIST(T.SEL.BR.CUS,KEY.LIST.BR.CUS,'',SELECTED.BR.CUS, ERR.BR.CUS)
    K = 0
    FOR I=1 TO SELECTED.BR.CUS
        CALL F.READ(FN.CUS,KEY.LIST.BR.CUS<I>, R.CUS, F.CUS, ETEXT.CUS)
*        IF KEY.LIST.BR.CUS<I> EQ '027300012' THEN DEBUG
        POS.REST = R.CUS<EB.CUS.POSTING.RESTRICT>
        CRT KEY.LIST.BR.CUS<I>:',':POS.REST
        IF R.CUS AND KEY.LIST.BR.CUS<I>[1,2] NE '99' AND ( POS.REST LT 70 OR POS.REST EQ '' ) THEN
            K++
            ENQ<2,K> = '@ID'
            ENQ<3,K> = 'EQ'
            ENQ<4,K> = KEY.LIST.BR.CUS<I>
            CRT 'ACCEPTED'
        END
    NEXT I

    IF K EQ 0 THEN
        ENQ<2,K> = "@ID"
        ENQ<3,K> = "EQ"
        ENQ<4,K> = "DUMMY"
    END

    EXECUTE 'COMO OFF BLD.BR.CUS'
    RETURN
END
