* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.CARD.ISSUE.CLOS(ENQ.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*----------------------------------------------------
    COMP = ID.COMPANY
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    FN.CARD = "FBNK.CARD.ISSUE"   ; F.CARD = ""
    CALL OPF(FN.CARD,F.CARD)

    T.SEL = "SELECT ":FN.CARD
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FLG = 0
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CARD,KEY.LIST<I>,R.CARD,F.CARD,E.CARD)
            ACCT.NO = R.CARD<CARD.IS.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACCT.NO,POST.ACCT)
*---------- HISTORY OR CLOSED
            IF (POST.ACCT NE "") OR (POST.ACCT GT 89) THEN
                FLG      = 1
                ENQ<2,1> = '@ID'
                ENQ<3,1> = 'EQ'
                ENQ<4,1> = KEY.LIST<I>
            END
        NEXT I
        IF I EQ SELECTED AND FLG EQ 0 THEN
            ENQ<2,1> = '@ID'
            ENQ<3,1> = 'EQ'
            ENQ<4,1> = "DUMMY"
        END
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = "DUMMY"
    END
*--------------------------------------------------------
    RETURN
END
