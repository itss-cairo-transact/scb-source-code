* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1217</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACCT.BAL.FROM.ACTIVITY

* This routine will rebuild account balance fields 23 - 27
* Relies on last ACCT.ACTIVITY & DATES.EXPOSURE
* Make sure there are no Unauthorised transactions for account before rebuild

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    RTN.TIME=TODAY:'_':OCONV(TIME(),"MTS")
    EXECUTE "COMO ON ACCT.BAL.FROM.ACTIVITY_":RTN.TIME

    SEL.LIST = '' ; NO.OF.REC = '' ; ERR = ''; CMD=''

* Save the problematic Account IDs in &SAVEDLISTS& directory under the name PROB.ACC
*    CMD= "GET.LIST PROB.ACC"
*    CALL EB.READLIST(CMD,SEL.LIST,'',CNT1,ER)
    OPEN "&SAVEDLISTS&" TO F.BP ELSE PRINT 'Unable to open SAVEDLIST'
    READ SEL.LIST FROM F.BP,'PROB.ACC' ELSE PRINT 'Savedlist not input'
    IF SEL.LIST THEN
        PRINT "Savedlist is Read"
        LOOP
            REMOVE ACC.ID FROM SEL.LIST SETTING POS
        WHILE ACC.ID:POS
            PRINT "Account ":ACC.ID:" is read from list"
            ACC.ID = TRIM(ACC.ID)
            GOSUB INITIALISE
            GOSUB PROCESS
        REPEAT
    END
PROGRAM.END:

    EXECUTE "COMO OFF ACCT.BAL.FROM.ACTIVITY_":RTN.TIME
    PRINT "COMO saved as *** ACCT.BAL.FROM.ACTIVITY_":RTN.TIME:" ***"
    SLEEP 2
    RETURN

************
INITIALISE:
************

    FN.ACCOUNT="F.ACCOUNT"
    FV.ACCOUNT=""
    CALL OPF(FN.ACCOUNT,FV.ACCOUNT)

    F.ECB="F.EB.CONTRACT.BALANCES"
    FN.ECB=""
    CALL OPF(F.ECB,FN.ECB)

    FN.STMT.ENTRY="F.STMT.ENTRY"
    FV.STMT.ENTRY=""
    CALL OPF(FN.STMT.ENTRY,FV.STMT.ENTRY)

    FN.DATE.EXPOSURE="F.DATE.EXPOSURE"
    FV.DATE.EXPOSURE=""
    CALL OPF(FN.DATE.EXPOSURE,FV.DATE.EXPOSURE)

    BALANCE.DATE1=""; BALANCE.DATE2=""; YBALANCE1=""; YBALANCE2=""
    CR.MVMT1=""; CR.MVMT2=""; DR.MVMT1=""; DR.MVMT2=""; ERR1=""; ERR2=""
    OPN.ACT.BAL=""; OPN.CLR.BAL=""; ONL.ACT.BAL=""; ONL.CLR.BAL=""; WORK.BAL=""
    EXP.CNT=""; J=""; R.ACCT=""; WB.DIFF=0

    RETURN

*********
PROCESS:
*********

    R.ACCT=''
    READ R.ACCT FROM FV.ACCOUNT, ACC.ID ELSE
        PRINT "Account ":ACC.ID:" does not exist, cannot rebuild balance"
        RETURN
    END

    READU R.ACCT FROM FV.ACCOUNT, ACC.ID THEN
        READU R.ECB FROM FN.ECB,ACC.ID ELSE R.ECB=""
        PRINT "Account ":ACC.ID:" is read"
***** Obtain open and online balance from ACCT.ACTIVITY using core routine *****

        BALANCE.DATE1 = R.DATES(EB.DAT.TODAY)
        BALANCE.DATE2 = R.DATES(EB.DAT.LAST.WORKING.DAY)

        CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT, "BOOKING", BALANCE.DATE1, "", YBALANCE1, CR.MVMT1, DR.MVMT1, ERR1)     ;* Get online actual balance
        CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT, "BOOKING", BALANCE.DATE2, "", YBALANCE2, CR.MVMT2, DR.MVMT2, ERR2)     ;* Get open actual balance

        OPN.ACT.BAL = YBALANCE2
        OPN.CLR.BAL = YBALANCE2
        ONL.ACT.BAL = YBALANCE1
        ONL.CLR.BAL = YBALANCE1
        WORK.BAL = YBALANCE1

***** Build cleared balance based on DATES.EXPOSURE *****

        IF R.ECB<ECB.NEXT.EXP.DATE> NE "" THEN
            EXP.CNT = DCOUNT(R.ECB<ECB.EXPOSURE.DATES>,@VM)
            FOR J = 1 TO EXP.CNT
                I=""; CNT=""; EXP.DATE=""; EXP.ID=""; R.EXPOSURE=""
                EXP.DATE = R.ECB<ECB.EXPOSURE.DATES,J>
                EXP.ID = ACC.ID:"-":EXP.DATE
                READ R.EXPOSURE FROM FV.DATE.EXPOSURE, EXP.ID ELSE R.EXPOSURE=""
                CNT = DCOUNT(R.EXPOSURE,@FM)
                FOR I = 1 TO CNT
                    R.ENTRY=""
                    READ R.ENTRY FROM FV.STMT.ENTRY, R.EXPOSURE<I> ELSE R.ENTRY=""
                    IF R.ENTRY<AC.STE.AMOUNT.FCY> THEN
                        IF R.ENTRY<AC.STE.BOOKING.DATE> NE TODAY THEN OPN.CLR.BAL -= R.ENTRY<AC.STE.AMOUNT.FCY>
                        ONL.CLR.BAL -= R.ENTRY<AC.STE.AMOUNT.FCY>
                        WORK.BAL -= R.ENTRY<AC.STE.AMOUNT.FCY>
                    END ELSE
                        IF R.ENTRY<AC.STE.BOOKING.DATE> NE TODAY THEN OPN.CLR.BAL -= R.ENTRY<AC.STE.AMOUNT.LCY>
                        ONL.CLR.BAL -= R.ENTRY<AC.STE.AMOUNT.LCY>
                        WORK.BAL -= R.ENTRY<AC.STE.AMOUNT.LCY>
                    END
                NEXT I
            NEXT J
        END

***** Maintain the difference in Working balance, due to ENTRY.HOLD *****

        WB.DIFF = R.ACCT<AC.ONLINE.CLEARED.BAL> - R.ACCT<AC.WORKING.BALANCE>
        IF WB.DIFF GT 0 THEN  ;* INAU debit (not CREDIT) is valid to update WORK.BAL
            WORK.BAL -= WB.DIFF
        END

        PRINT "Account##":ACC.ID:"##Before rebuild##":R.ACCT<AC.OPEN.ACTUAL.BAL>:"##":R.ACCT<AC.OPEN.CLEARED.BAL>:"##":R.ACCT<AC.ONLINE.ACTUAL.BAL>:"##":R.ACCT<AC.ONLINE.CLEARED.BAL>:"##":R.ACCT<AC.WORKING.BALANCE>

        R.ACCT<AC.OPEN.ACTUAL.BAL> = OPN.ACT.BAL
        R.ACCT<AC.OPEN.CLEARED.BAL> = OPN.CLR.BAL
        R.ACCT<AC.ONLINE.ACTUAL.BAL> = ONL.ACT.BAL
        R.ACCT<AC.ONLINE.CLEARED.BAL> = ONL.CLR.BAL
        R.ACCT<AC.WORKING.BALANCE> = WORK.BAL

        R.ECB<ECB.OPEN.ACTUAL.BAL>=R.ACCT<AC.OPEN.ACTUAL.BAL>
        R.ECB<ECB.OPEN.CLEARED.BAL>=R.ACCT<AC.OPEN.CLEARED.BAL>
        R.ECB<ECB.ONLINE.ACTUAL.BAL>=R.ACCT<AC.ONLINE.ACTUAL.BAL>
        R.ECB<ECB.ONLINE.CLEARED.BAL>=R.ACCT<AC.ONLINE.CLEARED.BAL>
        R.ECB<ECB.WORKING.BALANCE>=R.ACCT<AC.WORKING.BALANCE>

        PRINT "##After rebuild##":OPN.ACT.BAL:"##":OPN.CLR.BAL:"##":ONL.ACT.BAL:"##":ONL.CLR.BAL:"##":WORK.BAL
        WRITE R.ACCT TO FV.ACCOUNT, ACC.ID
        WRITE R.ECB TO FN.ECB,ACC.ID

    END
    RELEASE FV.ACCOUNT, ACC.ID
    RELEASE FN.ECB, ACC.ID

    RETURN
END
