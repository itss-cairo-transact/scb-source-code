* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-------CREATED BY RIHAM YOUSSEF----------*
*   PROGRAM ATM.FOREX.ADD
    SUBROUTINE  ATM.FOREX.ADD
*-----------------------------------------*

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.FOREX
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*--------------------------------------------*
    GOSUB CHECKLIST
    IF SW = 0 THEN
        SCB.COMM = "sh ATM.FOREX/g_forex.sh"
        EXECUTE SCB.COMM
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        GOSUB PROGRAM.END
    END
    RETURN

*----------
CHECKLIST:
*----------
    FN.ATFX    = 'F.SCB.ATM.FOREX'
    F.ATFX     = ''
    R.ATFX     = ''
    CALL OPF(FN.ATFX,F.ATFX)



    DAT.Y = TODAY
    KEY.LIST1 = "" ; SELECTED1 = "" ;  ER.FX1="" ; SW = 0
    T.SEL1 = "SELECT F.SCB.ATM.FOREX WITH BNK.DATE EQ ":DAT.Y
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.FX1)
    IF SELECTED1 THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
        SW = 1
    END ELSE
        SW = 0
    END
    RETURN
*----------
INITIALISE:
*----------
    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "SCB.ATM.FOREX"
    SCB.VERSION  = "1"

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION



    OPENSEQ "OFS.MNGR.IN" , "ATM.FOREX.IN" TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"ATM.FOREX.IN"
        HUSH OFF
    END

    OPENSEQ "OFS.MNGR.IN" , "ATM.FOREX.IN" TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create ATM.FOREX.IN File IN OFS.MNGR.IN'
        END
    END
    OPENSEQ "OFS.MNGR.OUT" , "ATM.FOREX.OUT" TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"ATM.FOREX.OUT"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "ATM.FOREX.OUT" TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create ATM.FOREX.OUT File IN OFS.MNGR.OUT'
        END
    END
    RETURN
*------------
BUILD.RECORD:
*------------
    FN.ATFX    = 'F.SCB.ATM.FOREX'
    F.ATFX     = ''
    R.ATFX     = ''
    CALL OPF(FN.ATFX,F.ATFX)

    FOREX  = "SCB.ATM.FOREX":"....csv"

    KEY.LIST2 = "" ; SELECTED2 = "" ;  ER.FX2=""
    T.SEL2 = "SELECT ATM.FOREX WITH @ID LIKE ":FOREX
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.FX2)
    IF SELECTED2 THEN
        FOR II = 1 TO SELECTED2
            Path.1 = "ATM.FOREX/":KEY.LIST2<II>

            OPENSEQ Path.1 TO MyPath.1 ELSE
                TEXT = "ERROR OPEN FILE" ; CALL REM
                RETURN
            END

            R.LINE = '' ; R.ATFX = ''; T.SEL='';KEY.LIST=''; SELECTED=0
            UPDATE.FLAG = ''; ERR=''; ER=''; L.LIST=''; SELECT.NO=0; Y.SEL=''
            ZEROS=''; NO.OF.RECORDS=0; NO.OF.REC=0; Z=''

            EOF = ''
            I   = 1

            TD = TODAY
            LOOP WHILE NOT(EOF)
                READSEQ R.LINE FROM MyPath.1 THEN
                    R.ATFX<ATFX.SERVER.DATE>     = TRIM(FIELD(R.LINE,",",1))[1,8]
                    R.ATFX<ATFX.SERVER.TIME>     = TRIM(FIELD(R.LINE,",",1))[10,2]:TRIM(FIELD(R.LINE,",",1))[13,2]
                    R.ATFX<ATFX.NEW.ATM.DATE>    = TRIM(FIELD(R.LINE,",",2))[1,8]
                    R.ATFX<ATFX.NEW.ATM.TIME>    = TRIM(FIELD(R.LINE,",",2))[10,2]:TRIM(FIELD(R.LINE,",",2))[13,2]
                    R.ATFX<ATFX.ATM.BANK.ID>     = TRIM(FIELD(R.LINE,",",3))
                    R.ATFX<ATFX.TRANS.SEQ>       = TRIM(FIELD(R.LINE,",",4))
                    R.ATFX<ATFX.CURRENCY>        = TRIM(FIELD(R.LINE,",",5))
                    R.ATFX<ATFX.SELL.AMT>        = TRIM(FIELD(R.LINE,",",6))
                    R.ATFX<ATFX.EQUI.VAL>        = TRIM(FIELD(R.LINE,",",7))
                    R.ATFX<ATFX.TOT.EQUI>        = TRIM(FIELD(R.LINE,",",8))
                    R.ATFX<ATFX.COM.EQUI>        = TRIM(FIELD(R.LINE,",",9))
                    R.ATFX<ATFX.TOT.NET.VAL>     = TRIM(FIELD(R.LINE,",",10))
                    R.ATFX<ATFX.RATE>            = TRIM(FIELD(R.LINE,",",11))
                    R.ATFX<ATFX.BNK.DATE>        = TD

                    OFS.MESSAGE.DATA  =  ",SERVER.DATE::=":R.ATFX<ATFX.SERVER.DATE>
                    OFS.MESSAGE.DATA :=  ",SERVER.TIME::=":R.ATFX<ATFX.SERVER.TIME>
                    OFS.MESSAGE.DATA :=  ",NEW.ATM.DATE::=":R.ATFX<ATFX.NEW.ATM.DATE>
                    OFS.MESSAGE.DATA :=  ",NEW.ATM.TIME::=":R.ATFX<ATFX.NEW.ATM.TIME>
                    OFS.MESSAGE.DATA :=  ",ATM.BANK.ID::=":R.ATFX<ATFX.ATM.BANK.ID>
                    OFS.MESSAGE.DATA :=  ",TRANS.SEQ::=":R.ATFX<ATFX.TRANS.SEQ>
                    OFS.MESSAGE.DATA :=  ",CURRENCY::=":R.ATFX<ATFX.CURRENCY>
                    OFS.MESSAGE.DATA :=  ",SELL.AMT::=":R.ATFX<ATFX.SELL.AMT>
                    OFS.MESSAGE.DATA :=  ",EQUI.VAL::=":R.ATFX<ATFX.EQUI.VAL>
                    OFS.MESSAGE.DATA :=  ",TOT.EQUI::=":R.ATFX<ATFX.TOT.EQUI>
                    OFS.MESSAGE.DATA :=  ",COM.EQUI::=":R.ATFX<ATFX.COM.EQUI>
                    OFS.MESSAGE.DATA :=  ",TOT.NET.VAL::=":R.ATFX<ATFX.TOT.NET.VAL>
                    OFS.MESSAGE.DATA :=  ",RATE::=":R.ATFX<ATFX.RATE>
                    OFS.MESSAGE.DATA :=  ",BNK.DATE::=":R.ATFX<ATFX.BNK.DATE>


                    SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,," : OFS.MESSAGE.DATA

                    BB.IN.DATA  = SCB.OFS.MESSAGE

                    WRITESEQ BB.IN.DATA TO BB.IN ELSE
                    END
*                    CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE,SCB.OFS.MESSAGE)
                    SCB.OFS.SOURCE = "SCBONLINE"
                    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)


                    BB.OUT.DATA  = SCB.OFS.MESSAGE

                    WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
                    END
                END ELSE
                    EOF = 1
                END

            REPEAT
            CLOSESEQ MyPath.1
            EXECUTE "COPY FROM ATM.FOREX ":KEY.LIST2<II> :" TO ATM.FOREX/ATM.FOREX.DONE"
            EXECUTE "DELETE ATM.FOREX ":KEY.LIST2<II>
        NEXT II
    END
    RETURN
*----------
PROGRAM.END:
*----------
    TEXT = "�� ����� ����� �����";CALL REM

    RETURN
*----------------------------------------------------------
END
