* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>561</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUST.CLOSED.XX(ENQ)
*    PROGRAM BLD.CUST.CLOSED.XX

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*-----------------------------------------*
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'          ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.H  = 'FBNK.CUSTOMER$HIS'    ;  F.CUS.H = ''
    CALL OPF(FN.CUS.H,F.CUS.H)
*-----------------------------------------*
    TOD    = TODAY
    T.YEAR = TOD[1,4]
    T.YEAR = T.YEAR[3,2]
    TODD   = TOD
*Line [ 44 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeOne.ADD.MONTHS(TODD,-1)
    TT.MONTH = TODD[5,2]
    MY.COND  = T.YEAR : TT.MONTH
    N.FLAG = 0
    Z = 1
*-------------------------------------------*
       T.SEL = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT GE 90 ) AND (SECTOR NE 5010 AND SECTOR NE 5020) AND COMPANY.BOOK EQ ":COMP.ID:" AND CURR.NO GT 1 AND TEXT UNLIKE BR... BY @ID"
   *** T.SEL = "SELECT FBNK.CUSTOMER WITH ((SECTOR NE 5010 AND SECTOR NE 5020) AND SECTOR EQ 2000) AND (POSTING.RESTRICT GE 90 ) AND COMPANY.BOOK EQ ":COMP.ID:" AND CURR.NO GT 1 BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<II>,R.ACC,F.ACC,CUS.ER)
            CUS.ID  = KEY.LIST<II>
            CUR.NO  = R.ACC<EB.CUS.CURR.NO>
            RR = 1
            MM = CUR.NO - 1
            FOR NN = CUR.NO TO 1 STEP -1
                IF RR EQ 1 THEN
                    CUST.ID      = CUS.ID:";"
                    CUST.ID.NXT  = CUS.ID:";":MM
                END ELSE
                    CUST.ID      = CUS.ID:";":MM
                    MM           = MM -1
                    CUST.ID.NXT  = CUS.ID:";":MM
                END
                IF RR EQ 1 THEN
                    RR = 2
                    CALL F.READ(FN.ACC,CUS.ID,R.ACC.N,F.ACC,CUS.ER44)
                    CUS.DAT.2 = R.ACC.N<EB.CUS.DATE.TIME><1,1>[1,4]
                    CUS.POST  = R.ACC.N<EB.CUS.POSTING.RESTRICT>
                END ELSE
                    CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                    CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,4]
                    CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                END

                CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                IF CUS.POST NE CUS.POST.NXT THEN
                    IF CUS.POST GE 90 THEN
                        IF CUS.DAT.2 EQ MY.COND THEN
                            N.FLAG  = 1
                            ENQ<2,Z> = "@ID"
                            ENQ<3,Z> = "EQ"
                            ENQ<4,Z> = KEY.LIST<II>
                            Z = Z + 1
                            NN   = 1
                        END
                    END
                END
            NEXT NN
        NEXT II

        IF Z EQ 1 THEN
            ENQ<2,Z> = "@ID"
            ENQ<3,Z> = "EQ"
            ENQ<4,Z> = "DUMMY"
        END
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
*----------------------------------------------------------------------*
    RETURN
END
