* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*--------------------------------------NI7OOOOOOOOOOOO---------------------------------------
    SUBROUTINE AC.NEGATIVE(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    TD = TODAY
    TD1 = TD[1,6]
    TD2 = TD1:'20'
    TD3 = TD1:'25'

    FN.AC = 'FBNK.ACCOUNT'
    F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    T.SEL= "SELECT FBNK.ACCOUNT WITH OPEN.ACTUAL.BAL LT 0 AND CATEGORY GE 1001 AND CATEGORY LE 1003 AND OPEN.ACTUAL.BAL NE ''"

******T.SEL= "SELECT FBNK.ACCOUNT WITH @ID EQ 0110123010100201"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.AC,KEY.LIST,R.AC,F.AC,AC.ERR1)
    CATEG    = R.AC<AC.CATEGORY>
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        IF (CATEG EQ '1002' AND (TD LE TD2 OR TD GE TD3)) THEN
            FOR ENQ.LP = 1 TO SELECTED
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
            NEXT ENQ.LP
        END ELSE
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = 'DUMMY'
        END

**********************
        RETURN
    END
