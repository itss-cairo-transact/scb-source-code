* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
***  CREATED BY MOHAMED SABRY 2012/07/05
******************************************************
    SUBROUTINE BLD.LD.BNK.LY(ENQ.DATA)
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    K = 0
    FN.CU      = "FBNK.CUSTOMER"                 ; F.CU      = ""  ; R.CU     = ""
    FN.LD      = "FBNK.LD.LOANS.AND.DEPOSITS"      ; F.LD      = ""  ; R.LD     = ""
    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.LD,F.LD)
    T.SEL  = "SELECT ":FN.LD:" WITH STATUS NE 'LIQ' "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E.LD)
            WS.CUS.ID      = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CU,WS.CUS.ID,R.CU,F.CU,E.CU)
            WS.SECTOR   = R.CU<EB.CUS.SECTOR>
            WS.NAT      = R.CU<EB.CUS.NATIONALITY>
            IF WS.NAT EQ 'LY' THEN
                IF WS.SECTOR GE 3000 AND WS.SECTOR LE 3223 THEN
                    K ++
                    ENQ.DATA<2,K> = "@ID"
                    ENQ.DATA<3,K> = "EQ"
                    ENQ.DATA<4,K> = KEY.LIST<I>
                END
            END
        NEXT I
    END
    IF K = 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
