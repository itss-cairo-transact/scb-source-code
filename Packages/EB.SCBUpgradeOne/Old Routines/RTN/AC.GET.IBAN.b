* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
************MAI MARCH2020*****
    SUBROUTINE AC.GET.IBAN(ARG)
***************************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY


    AC.ID = ARG
    IBAN.ID = 'T24.IBAN'

    CALL DBR('ACCOUNT':@FM:AC.ALT.ACCT.TYPE,ARG,ACC.TYP)
    CALL DBR('ACCOUNT':@FM:AC.ALT.ACCT.ID,AC.ID,ACC.IBAN.F)

    LOCATE IBAN.ID IN ACC.TYP<1,1> SETTING XX THEN
        IBAN.AC = ACC.IBAN.F<1,XX>
    END
**TEXT = "IBAN.AC=":IBAN.AC; CALL REM;
    ARG = IBAN.AC[1,4]:" ":IBAN.AC[5,4]:" ":IBAN.AC[9,4]:" ":IBAN.AC[13,4]:" "
    ARG:= IBAN.AC[17,4]:" ":IBAN.AC[21,4]:" ":IBAN.AC[25,4]:" ":IBAN.AC[29,1]

    RETURN
END
