* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
****NESSREEN AHMED 19/4/2016*************
*-----------------------------------------------------------------------------
* <Rating>1812</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ATM.CASH.PAYMENT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.ROUTINE.CHK
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CASH.DPST

*****WRITTEN BY NESSREEN AHMED SCB*****
***************************************
    Path = "NESRO/Visa_ATM_DPST/VD"
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    F.RT.CHK = '' ; FN.RT.CHK = 'F.SCB.ATM.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)
    YTEXT = "Enter the Posting Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    DATEE = '' ; YYYY = '' ; MM= ''  ; DDT = '' ; YY = ''
*  CALL DBR( 'DATES':@FM:EB.DAT.TODAY, 'EG0010001' , DATEE)
*   TEXT = 'DATE=':DATEE ; CALL REM

    DATEE = COMI
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    DDT = DATEE[7,2]
***************************************
    CHK.SEL = "SELECT F.SCB.ATM.ROUTINE.CHK WITH YEAR EQ ":YYYY:" AND MONTH EQ ":MM :" AND DAY EQ ":DDT :" AND ATM.CASH.PAYMENT EQ 'YES' "
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    KEYID = YYYY:MM:DDT
    **TEXT = 'KEYID=':KEYID ; CALL REM
    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
******************************************************
        TEXT = 'Start.Of.File' ; CALL REM
        EOF = ''
        LOOP WHILE NOT(EOF)

            CARD.NO = '' ;REASON.COD = ''; REASON.COD.TR = '' ; BANK.ACC = '' ; BANK.ACC.TR = '' ; ORG.MSGE = '' ; ORG.MSGE.TR = ''
            MSGE.TYPE = '' ; PROC.COD = '' ; BILL.CURR = '' ; BILL.AMT = '' ; BILL.AMT.TR = '' ; BILL.AMT.NN = '' ; BILL.AMT.1 = '' ; BILL.AMT.FMT = ''
            DB.CR = '' ; POST.DATE = '' ; PURCH.DATE = '' ; TRANS.CURR.GL = ''
            MSGE.DESC = '' ; MERCH.CITY = '' ; MERCH.COUNTRY = ''
            TRANS.AMT = '' ; TRANS.AMT.1 = '' ; TRANS.AMT.FMT = '' ; TERMINAL.ID = ''
            READSEQ Line FROM MyPath THEN
                CARD.NO           = Line[1,16]
                ORG.MSGE          = Line[70,5]
                IF ORG.MSGE[1,4] EQ "DPST" THEN
                    T.SEL =  "SELECT FBNK.CARD.ISSUE WITH @ID LIKE ...":CARD.NO
                    KEY.LIST=""
                    SELECTED=""
                    ER.MSG=""

                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                    IF SELECTED THEN
                        FOR I = 1 TO SELECTED
                            FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.CARD.ISSUE = '' ; RETRY1 = '' ; E1 = ''
                            KEY.TO.USE = KEY.LIST<I>
                            CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                            CALL F.READ(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                            LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                            CUST.DEPT = LOCAL.REF<1,LRCI.BRANCH.NO>
                            BANK.ACC =  R.CARD.ISSUE<CARD.IS.ACCOUNT>
                            CUST.NAME = R.CARD.ISSUE<CARD.IS.NAME>
                        NEXT I
                        TERMINAL.ID = Line[25,8]
                        TRM.ID.TR =  TRIM(TERMINAL.ID, "", "D")
                        REASON.COD        = Line[34,4]
                        REASON.COD.TR =  TRIM(REASON.COD, "", "D")
                        LENCOD = LEN(REASON.COD.TR)
                        IF LENCOD = 0 THEN
                            REASON.COD.TR = "NA"
                        END
                        ORG.MSGE          = Line[70,5]
                        ORG.MSGE.TR =  TRIM(ORG.MSGE, " ", "T")
                        MSGE.TYPE         = Line[76,4]
                        MSGE.TYPE.TR = TRIM(MSGE.TYPE, " ", "T")
                        PROC.COD          = Line[81,6]
                        PROC.COD.TR  = TRIM(PROC.COD, " ", "T")
                        BILL.CURR         = Line[88,3]
                        CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, BILL.CURR , BILL.CURR.GL)
                        BILL.AMT.NN       = Line[94,16]
                        BILL.AMT.1 = TRIM(BILL.AMT.NN, "0", "L")
                        BILL.AMT.FMT = BILL.AMT.1/100
                        DB.CR             = Line[111,2]
                        POST.DATE         = Line[114,8]
                        PURCH.DATE        = Line[123,8]
                        TRANS.CURR        = Line[132,3]
                        CALL DBR( 'SCB.VISA.CURR.CODES':@FM:SCB.CURR.CURRENCY, TRANS.CURR , TRANS.CURR.GL)
                        TRANS.AMT      = Line[138,16]
                        TRANS.AMT.1 = TRIM(TRANS.AMT, "0", "L")
                        TRANS.AMT.FMT = TRANS.AMT.1/100
                        MSGE.DESC         = Line[188,60]
                        CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MSGE.DESC
                        MERCH.CITY        = Line[249,60]
                        CONVERT CHAR( 95):CHAR( 151):CHAR( 150) TO CHAR( 32) IN MERCH.CITY
                        MERCH.COUNTRY     = Line[310,3]

                        F.ATM.DPST = '' ; FN.ATM.DPST = 'F.SCB.ATM.CASH.DPST' ; R.ATM.DPST = '' ; E2 = '' ; RETRY2 = ''
                        CALL OPF(FN.ATM.DPST,F.ATM.DPST)

                        ID.KEY = CARD.NO:YYYY:MM:DDT
**  TEXT = 'ID.KEY=':ID.KEY ; CALL REM
                        CALL F.READ(FN.ATM.DPST,ID.KEY, R.ATM.DPST, F.ATM.DPST ,E2)
                        IF NOT(E2) THEN
                            TRM.ID = R.ATM.DPST<ATDP.TERMINAL.ID>
*Line [ 150 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DD = DCOUNT(TRM.ID,@VM)
                            XX = DD+1
                            R.ATM.DPST<ATDP.TERMINAL.ID,XX>  = TRM.ID.TR
                            R.ATM.DPST<ATDP.ORG.MSG.TYPE,XX> = ORG.MSGE.TR
                            R.ATM.DPST<ATDP.MSG.TYPE,XX>     = MSGE.TYPE.TR
                            R.ATM.DPST<ATDP.PROCESS.CODE,XX> = PROC.COD.TR
                            R.ATM.DPST<ATDP.DB.CR.FLAG,XX>   = DB.CR
                            R.ATM.DPST<ATDP.REASON.CODE,XX>  = REASON.COD.TR
                            R.ATM.DPST<ATDP.BILL.CURR,XX>    = BILL.CURR.GL
                            R.ATM.DPST<ATDP.BILL.AMT,XX>     = BILL.AMT.FMT
                            R.ATM.DPST<ATDP.POS.DATE,XX>     = POST.DATE
                            R.ATM.DPST<ATDP.VALUE.DATE,XX>   = PURCH.DATE
                            R.ATM.DPST<ATDP.TRANS.CURR,XX>   = TRANS.CURR.GL
                            R.ATM.DPST<ATDP.TRANS.AMT,XX>    = TRANS.AMT.FMT
                            R.ATM.DPST<ATDP.MSG.DESC,XX>     = MSGE.DESC
                            R.ATM.DPST<ATDP.MERCH.CITY,XX>   = MERCH.CITY
                            R.ATM.DPST<ATDP.MERCH.COUNTRY,XX>= MERCH.COUNTRY

                            CALL F.WRITE(FN.ATM.DPST,ID.KEY, R.ATM.DPST)
                            CALL JOURNAL.UPDATE(ID.KEY)

                        END ELSE
                            TRM.ID = R.ATM.DPST<ATDP.TERMINAL.ID>
*Line [ 174 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DD = DCOUNT(TRM.ID,@VM)
                            XX = DD+1

                            R.ATM.DPST<ATDP.CARD.BR>         = CUST.DEPT
                            R.ATM.DPST<ATDP.CUST.NAME>       = CUST.NAME
                            R.ATM.DPST<ATDP.CUST.ACCT>       = BANK.ACC
                            R.ATM.DPST<ATDP.TERMINAL.ID,XX>  = TRM.ID.TR
                            R.ATM.DPST<ATDP.ORG.MSG.TYPE,XX> = ORG.MSGE.TR
                            R.ATM.DPST<ATDP.MSG.TYPE,XX>     = MSGE.TYPE.TR
                            R.ATM.DPST<ATDP.PROCESS.CODE,XX> = PROC.COD.TR
                            R.ATM.DPST<ATDP.DB.CR.FLAG,XX>   = DB.CR
                            R.ATM.DPST<ATDP.REASON.CODE,XX>  = REASON.COD.TR
                            R.ATM.DPST<ATDP.BILL.CURR,XX>    = BILL.CURR.GL
                            R.ATM.DPST<ATDP.BILL.AMT,XX>     = BILL.AMT.FMT
                            R.ATM.DPST<ATDP.POS.DATE,XX>     = POST.DATE
                            R.ATM.DPST<ATDP.VALUE.DATE,XX>   = PURCH.DATE
                            R.ATM.DPST<ATDP.TRANS.CURR,XX>   = TRANS.CURR.GL
                            R.ATM.DPST<ATDP.TRANS.AMT,XX>    = TRANS.AMT.FMT
                            R.ATM.DPST<ATDP.MSG.DESC,XX>     = MSGE.DESC
                            R.ATM.DPST<ATDP.MERCH.CITY,XX>   = MERCH.CITY
                            R.ATM.DPST<ATDP.MERCH.COUNTRY,XX>= MERCH.COUNTRY

                            CALL F.WRITE(FN.ATM.DPST,ID.KEY, R.ATM.DPST)
                            CALL JOURNAL.UPDATE(ID.KEY)
                        END

**********************************************************************
                    END ELSE  ;**END OF SELECTED***
                    END
                END
            END ELSE
                EOF = 1
            END

        REPEAT
        CLOSESEQ MyPath
        TEXT = 'END OF FILE' ; CALL REM
*********************************************************************
*Line [ 213 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeOne.ATM.DPST.TOT.CR.DR
*********************************************************************
        CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
        R.RT.CHK<ATCK.YEAR> = YYYY
        R.RT.CHK<ATCK.MONTH> = MM
        R.RT.CHK<ATCK.DAY> = DDT
        R.RT.CHK<ATCK.ATM.CASH.PAYMENT>   = 'YES'
        CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
        CALL JOURNAL.UPDATE(KEYID)
        TEXT = '�� ����� ����������� �����'; CALL REM

        RETURN
    END
