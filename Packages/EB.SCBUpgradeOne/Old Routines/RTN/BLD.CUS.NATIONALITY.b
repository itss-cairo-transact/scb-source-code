* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*----------------------------------------------------------------------------
    SUBROUTINE BLD.CUS.NATIONALITY(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
*==========================*
    XX  = TODAY
    XXX = TODAY
*Line [ 44 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeOne.ADD.MONTHS(XXX,1)
*=============================================================*
    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = '' ;  R.CUST = ''
    CALL OPF (FN.CUST,F.CUST)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF (FN.AC,F.AC)
    I = 0
*====================================================================*
    T.SEL  = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT LE 89 "
    T.SEL := " AND NATIONALITY NE 'EG' AND NEW.SECTOR EQ 4650 "
    T.SEL := " AND SECTOR NE 5010 AND SECTOR NE 5020 AND SECTOR NE 1100 AND SECTOR NE 1300 "
    T.SEL := " AND COMPANY.BOOK EQ ":COMP
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
   * TEXT = SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUMMY'
    END
    RETURN


*******************************************************************************************
END
