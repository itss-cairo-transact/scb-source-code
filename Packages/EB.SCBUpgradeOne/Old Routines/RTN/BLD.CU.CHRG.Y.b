* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>58</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/07/07
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CU.CHRG.Y(ENQ)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    KEY.LIST="" ;  SELECTED="" ; ER.MSG=""

    GOSUB INITIATE
    GOSUB OPEN.FILES
    GOSUB SEL.CU

    RETURN
****************************************
INITIATE:

    I = 0
    RETURN
****************************************
OPEN.FILES:
*Master files

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = "FBNK.ACCOUNT" ; F.AC = "" ; R.AC = "" ; ER.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.IND.AC = "FBNK.CUSTOMER.ACCOUNT" ; F.IND.AC = "" ; R.IND.AC = "" ; ER.IND.AC = ""
    CALL OPF(FN.IND.AC,F.IND.AC)

    RETURN
****************************************
SEL.CU:

*    T.SEL  = "SELECT ":FN.CU:" WITH @ID UNLIKE 994... AND POSTING.RESTRICT LT 89 AND COMPANY.BOOK EQ ":ID.COMPANY
    T.SEL  = "SELECT ":FN.CU:" WITH @ID UNLIKE 994... AND POSTING.RESTRICT LT 89 "
    T.SEL := " AND STMT.CHARGE.EXP EQ 'Y' WITHOUT SECTOR IN ( 5010 5020 ) BY COMPANY.BOOK BY @ID "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED
    IF SELECTED THEN
        FOR ICU = 1 TO SELECTED
            WS.GL.ID   = 0
            WS.GL.1005 = 0
            WS.GL.OTHR = 0
            CALL F.READ(FN.IND.AC,KEY.LIST<ICU>,R.IND.AC,F.IND.AC,ER.IND.AC)
            LOOP
                REMOVE AC.ID FROM R.IND.AC SETTING POS.AC
            WHILE AC.ID:POS.AC
                CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
                WS.GL.ID = R.AC<AC.CATEGORY>
                IF WS.GL.ID EQ 1005 THEN
                    WS.GL.1005 = 1
                END ELSE
                    WS.GL.OTHR = 1
                END
            REPEAT

            IF (WS.GL.1005 = 1 AND WS.GL.OTHR EQ 1)  THEN
                I++
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<ICU>
            END
        NEXT ICU

        IF I = 0 THEN
            ENQ<2,2> = '@ID'
            ENQ<3,2> = 'EQ'
            ENQ<4,2> = 'NO RECORDS'
        END
    END
    RETURN
****************************************
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*
*====================================================================*

END
