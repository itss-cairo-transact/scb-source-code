* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-249</Rating>
*-----------------------------------------------------------------------------
******* WAEL *******

    SUBROUTINE ADVICE.INW.AMEND

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)
    IF MYCODE NE "2231" THEN
        GOSUB INITIATE
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
        RETURN
    END
**ELSE
*  E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
** END
*===============================
INITIATE:
    REPORT.ID='ADVICE.INW.AMEND'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
***************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
    LG.NO= SAM:"/": SAM1:"/":SAM2
***************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
    LG.CUSTOMER =R.LD<LD.CUSTOMER.ID>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LG.CUSTOMER,LOC.REF)
    LG.CUST=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
******************END OF UPDATED ********************************
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    LG.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    ISS.DATE = LOCAL.REF<1,LDLR.ISSUE.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    LG.V.DATE = R.LD<LD.VALUE.DATE>
    ISSUE.DATE =ISS.DATE[7,2]:"/":ISS.DATE[5,2]:"/":ISS.DATE[1,4]
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    TOT = R.LD<LD.AMOUNT.INCREASE>
    RETURN
*=================================================
BODY:
    PR.HD :="'L'":" ���� �������� ��������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):ISS.DATE[7,2]:SPACE(2):ISS.DATE[5,2]:SPACE(2):ISS.DATE[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������� �� ������ ������ �������� "
**** PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
***    IF THIRD.NAME2 THEN
****        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:XX<1,1>
*** END
    PR.HD :="'L'":" "
*** PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
*** IF THIRD.ADDR2 THEN
****    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
***END
    PR.HD :="'L'":" "
    IF TOT GT 0 THEN
        PR.HD :="'L'":"  ����� �������� ���� ���� ������ ��� � � ���" : LG.NO : " ����� " : TOT :" ":CRR
    END ELSE
        IF TOT LT 0 THEN
            PR.HD :="'L'":" ����� �������� ���� ���� �������� ��� � � ���" : LG.NO : " ����� " : TOT :" ":CRR
        END
    END
    PR.HD :="'L'":" ���� ": LG.CUST :" ������� ����� ��" : FIN.DATE
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":TOT : SPACE(22): CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":TOT:SPACE(10):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
*    CALL WORDS.ARABIC(TOT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
    IN.AMOUNT=TOT:'.00'
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*Line [ 169 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 171 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I

*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": ISSUE.DATE
    HEADING PR.HD

    RETURN
*=================================================
END
