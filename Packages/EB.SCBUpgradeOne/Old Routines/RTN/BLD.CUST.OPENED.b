* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUST.OPENED(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*-----------------------------------------*
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'    ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
*-----------------------------------------*
    TOD    = TODAY
    T.YEAR = TOD[1,4]
    T.YEAR = T.YEAR[3,2]
    TODD   = TOD
*Line [ 40 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeOne.ADD.MONTHS(TODD,-1)
    MY.COND  = TODD[1,6]
*-------------------------------------------*
    T.SEL = "SELECT FBNK.CUSTOMER WITH CONTACT.DATE LIKE ":MY.COND:"... AND (SECTOR NE 5010 AND SECTOR NE 5020) AND COMPANY.BOOK EQ ":COMP.ID:" AND TEXT UNLIKE BR... BY CONTACT.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            ENQ<2,II> = "@ID"
            ENQ<3,II> = "EQ"
            ENQ<4,II> = KEY.LIST<II>
        NEXT II
    END ELSE
        ENQ<2,II> = "@ID"
        ENQ<3,II> = "EQ"
        ENQ<4,II> = "DUMMY"
    END
*----------------------------------------------------------------------*
    RETURN
END
