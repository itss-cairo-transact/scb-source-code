* @ValidationCode : MjoyMDQ1NTMyODc5OkNwMTI1MjoxNjQwNjgyMTUyMDE4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:02:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-110</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ARC.LOAN.DEPOSIT(CONTRACT.ID)

*-----------------------------------------------------------------------------
* DATE   : 11 FEB 2010
*
* PURPOSE : Standard archival process for LD that would purge the all LD.LOANS.AND.DEPOSITS$HIS
*           and LMM.ACCOUNT.BALANCES.HIST accordingly.
*
* NOTE    : All LD history records would be purged for the LMM.ACCOUNT.BALANCES.HIST record
*           PURGE.DATE check is supressed now. In case required, add at the corresponding para.
*
*-----------------------------------------------------------------------------

*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BATCH.FILES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ARCHIVE
*Line [ 40 ] Comment $INCLUDE I_ARC.LOAN.DEPOSIT.COMMON - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_ARC.LOAN.DEPOSIT.COMMON
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS

* MM contracts are supressed here

    

    IF CONTRACT.ID[1,2] EQ 'LD' OR CONTROL.LIST<1,1>["-",1,1] EQ "UPDATE.ARCHIVE" THEN

        GOSUB INITIALISE

        BEGIN CASE
            CASE CONTROL.LIST<1,1>["-",1,1] = "PURGE.LD$HIS"
                GOSUB PURGE.LD.HIS
            CASE CONTROL.LIST<1,1>["-",1,1] = "UPDATE.ARCHIVE"
                GOSUB UPDATE.CONCAT
* Updation of ARCHIVE file (LD.LOANS.AND.DEPOSITS)
        END CASE

    END

RETURN


*-----------------------------------------------------------------------------
INITIALISE:
*-----------------------------------------------------------------------------

    ARCHIVE.ID = CONTROL.LIST<1,1>["-",2,1]
    ARC.MARKER = CONTROL.LIST<1,1>["-",3,1]
    PURGE.DATE = CONTROL.LIST<1,1>["-",4,1]
    R.LD.PROC.REC = ''

    IF NOT(C$INITIALISED) THEN

        C$FN.LD.LOANS.AND.DEPOSITS$ARC = "F.LD.LOANS.AND.DEPOSITS$ARC"
        C$F.LD.LOANS.AND.DEPOSITS$ARC  = ""
        CALL OPF(C$FN.LD.LOANS.AND.DEPOSITS$ARC,C$F.LD.LOANS.AND.DEPOSITS$ARC)

        C$FN.LMM.ACCOUNT.BALANCES.HIST$ARC = "F.LMM.ACCOUNT.BALANCES.HIST$ARC"
        C$F.LMM.ACCOUNT.BALANCES.HIST$ARC  = ""
        CALL OPF(C$FN.LMM.ACCOUNT.BALANCES.HIST$ARC,C$F.LMM.ACCOUNT.BALANCES.HIST$ARC)

        C$FN.ARC.LD.PROCESSED = "F.ARC.LD.PROCESSED"
        C$F.ARC.LD.PROCESSED  = ""
        CALL OPF(C$FN.ARC.LD.PROCESSED,C$F.ARC.LD.PROCESSED)


        C$INITIALISED = 1

    END

RETURN


*-----------------------------------------------------------------------------
PURGE.LD.HIS:
*-----------------------------------------------------------------------------

    LD.ID = ''
    LD.ID = CONTRACT.ID[1,12] ;* Extracting the LD id from the LMM.ACCOUNT.BALANCES
    SEL.CMD.LD = ''
    LD.IDS = ''

    GOSUB CHECK.LD.EXIST
    IF NOT(LIVE.REC.EXIST) THEN
        GOSUB CHECK.PURGE.DATE
        IF NOT(WITHIN.RETENTION.PERIOD) THEN
            GOSUB PURGE.ALL.HIS.REC
        END
    END

RETURN

*-----------------------------------------------------------------------------
PURGE.ALL.HIS.REC:
*-----------------------------------------------------------------------------
*
* GOSUB GET.CURR.NO
    CURR.NO = ''
    CURR.NO = FIELD(LD.ID,';',2)
    LD.ID = CONTRACT.ID[1,12]

    IF CURR.NO LE 0 THEN      ;* Check if something to archive
        RETURN
    END

    FOR I = 1 TO CURR.NO

        LD.HIS.ID = LD.ID:";":I
        C$PROCESSED += 1

        IF ARC.MARKER THEN
            R.LD.HIS = ''
* Reading the information of the LD history record and writing the same to archive file

            CALL F.READ(C$FN.LD.LOANS.AND.DEPOSITS$HIS,LD.HIS.ID,R.LD.HIS,C$F.LD.LOANS.AND.DEPOSITS$HIS,ERR)
            CALL F.WRITE(C$FN.LD.LOANS.AND.DEPOSITS$ARC,LD.HIS.ID,R.LD.HIS)

        END
* Once written, the record is deleted for the LD history file

        CALL F.DELETE(C$FN.LD.LOANS.AND.DEPOSITS$HIS,LD.HIS.ID)

        C$DELETED += 1        ;* variable is updated after each deletion, so that the ARCHIVE file can be updated with the number of LD history record deleted.
    NEXT I
    R.LMM.HIS = ''
    CALL F.READ(C$FN.LMM.ACCOUNT.BALANCES.HIST,CONTRACT.ID,R.LMM.HIS,C$F.LMM.ACCOUNT.BALANCES.HIST,ERR)
    CALL F.WRITE(C$FN.LMM.ACCOUNT.BALANCES.HIST$ARC,CONTRACT.ID,R.LMM.HIS)
    CALL F.DELETE(C$FN.LMM.ACCOUNT.BALANCES.HIST,CONTRACT.ID)

* once all the LD history is deleted for the LD contract, then we are writing the ACCBAL record to archive and deleting from the LMM.ACCOUNT.BALANCES$HIST file


    GOSUB UPDATE.ARC.PROCESSED          ;* Update processed

RETURN
*-----------------------------------------------------------------------------
UPDATE.ARC.PROCESSED:
*-----------------------------------------------------------------------------

    ARC.LD.PROC.ID = "tSA":'-':ABS(TNO):'-':ID.COMPANY
    R.LD.PROC.REC<1> = C$PROCESSED
    R.LD.PROC.REC<2> = C$DELETED
    CALL F.WRITE(C$FN.ARC.LD.PROCESSED,ARC.LD.PROC.ID,R.LD.PROC.REC)

* The file F.ARC.LD.PROCESSED contain the number of LD history records processed and deleted during the execution of routine

RETURN

*-----------------------------------------------------------------------------
UPDATE.CONCAT:
*-----------------------------------------------------------------------------

    REC.PROCESSED = 0
    REC.DELETED = 0
    SEL.CMD = ''

    SEL.CMD = "SELECT ":C$FN.ARC.LD.PROCESSED
    CALL EB.READLIST(SEL.CMD,TSA.LIST,"","","")

    LOOP
        REMOVE Y.TSA FROM TSA.LIST SETTING POS
    WHILE Y.TSA:POS

        Y.ERR = ""
        CALL F.READ(C$FN.ARC.LD.PROCESSED,Y.TSA,R.LD.PROC.REC,C$F.ARC.LD.PROCESSED,Y.ERR)

        REC.PROCESSED += R.LD.PROC.REC<1>
        REC.DELETED += R.LD.PROC.REC<2>

    REPEAT
*
*-- Update the archive file with the date and time started and the user id
*
    GOSUB UPDATE.ARCHIVE

RETURN


*-----------------------------------------------------------------------------
UPDATE.ARCHIVE:
*-----------------------------------------------------------------------------

    R.ARCHIVE = ""
    ERR = ""
    CALL F.READ(C$FN.ARCHIVE,ARCHIVE.ID,R.ARCHIVE,C$F.ARCHIVE,ERR)
    R.ARCHIVE<ARC.RECS.PROCESSED,1> = REC.PROCESSED
    R.ARCHIVE<ARC.RECS.DELETED,1> = REC.DELETED
*
*-- Archival process end time
*
    YDATE = OCONV(DATE(),"DG")
    NOW = OCONV(TIME(),'MT')
    CONVERT ':' TO '' IN NOW
    R.ARCHIVE<ARC.TIME.ENDED,1> = YDATE[3,6]:NOW
*
*-- Curr number evaluation
*
    CURR.NO = R.ARCHIVE<ARC.CURR.NO>
    IF CURR.NO THEN
        CURR.NO +=1
    END ELSE
        CURR.NO = 1
    END
    R.ARCHIVE<ARC.CURR.NO> = CURR.NO
*
*-- Update stop requested flag
*
    STOP.REQ = R.ARCHIVE<ARC.STOP.INDICATOR>
    R.ARCHIVE<ARC.STOP.REQUESTED,1> = STOP.REQ
    CALL F.WRITE(C$FN.ARCHIVE,ARCHIVE.ID,R.ARCHIVE)

RETURN


*-----------------------------------------------------------------------------
CHECK.LD.EXIST:
*-----------------------------------------------------------------------------

* LIVE.REC.EXIST is set to 0, since for live LD file will not exist for the LMM.ACCOUNT.BALANCES.HIST

    LIVE.REC.EXIST = 0

RETURN

*-----------------------------------------------------------------------------


*-----------------------------------------------------------------------------
CHECK.PURGE.DATE:
*-----------------------------------------------------------------------------
*
* Add code here if PURGE.DATE check is required here, best is not to have this check,
* else the design of this routine may require change as PURGE.DATE check for
* LD.LOANS.AND.DEPOSITS$HIS and LMM.ACCOUNT.BALANCES.HIST records will be done seperatly (redundent work).
*
    HIS.ERR = ''; HIS.REC = ''; WITHIN.RETENTION.PERIOD = ''; F.LD.HIST = ''
    CALL OPF('F.LD.LOANS.AND.DEPOSITS$HIS', F.LD.HIST)
    CALL EB.READ.HISTORY.REC(F.LD.HIST, LD.ID, HIS.REC,HIS.ERR)
    IF HIS.REC AND HIS.REC<LD.FIN.MAT.DATE> LT PURGE.DATE  THEN
        WITHIN.RETENTION.PERIOD = 0     ;* Set to zero so that the records will be purged
    END ELSE
        WITHIN.RETENTION.PERIOD = 1
    END

RETURN
*-----------------------------------------------------------------------------


*-----------------------------------------------------------------------------
GET.CURR.NO:
*-----------------------------------------------------------------------------


    CURR.NO = 10
    ERR = 0
    LOOP
        LD.HIS.ID = LD.ID:";":CURR.NO
        READV R.FLD FROM C$F.LD.LOANS.AND.DEPOSITS$HIS,LD.HIS.ID,0 ELSE
            ERR = 1
        END
    UNTIL ERR
        CURR.NO += 10
    REPEAT
*
    LOOP
        CURR.NO -= 1
    UNTIL CURR.NO = 0
        LD.HIS.ID = LD.ID:";":CURR.NO
        READV R.FLD FROM C$F.LD.LOANS.AND.DEPOSITS$HIS,LD.HIS.ID,0 THEN         ;* Use READV as i am not interesed in REC content
            EXIT
        END
    REPEAT

RETURN
*-----------------------------------------------------------------------------

END
