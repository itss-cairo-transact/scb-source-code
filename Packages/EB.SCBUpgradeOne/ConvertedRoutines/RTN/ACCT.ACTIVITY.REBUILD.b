* @ValidationCode : MjotMTU2OTg3Njk3NTpDcDEyNTI6MTY0MDY4NjE3Njc4Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:09:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE ACCT.ACTIVITY.REBUILD
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ACTIVITY
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TABLE.CAPITALIS.CORR
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
    


    F.ACCT = 'F.ACCOUNT'
    FN.ACCT = ''
    CALL OPF(F.ACCT, FN.ACCT)

    F.ECB = 'F.EB.CONTRACT.BALANCES'
    FN.ECB = ''
    CALL OPF(F.ECB, FN.ECB)

    F.ACTIV = 'F.ACCT.ACTIVITY'
    FN.ACTIV = ''
    CALL OPF(F.ACTIV, FN.ACTIV)

    SEL.CMD = "" ; SEL.LIST = "" ; Y.NOR = "" ; Y.RET.CODE = ""
    Y.ACC.IDS = "" ; DIFF.AMTS = "" ; Y.CNT = ""

    SEL.CMD = "GET.LIST PROB.ACC.LIST"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",Y.NOR,Y.RET.CODE)

    IF SEL.LIST THEN
        LOOP
            REMOVE TEMP.ID FROM SEL.LIST SETTING POS
            ACC.ID = FIELD(TEMP.ID,"*",1)
        WHILE ACC.ID:POS
            GOSUB MAIN.PROCESS
        REPEAT
    END
RETURN

*************
MAIN.PROCESS:
*************


    R.ACCT = ''; R.ECB = ''; ACTIV.MNTHS = ''; ACCT.BAL = ''
    READU R.ACCT FROM FN.ACCT, ACC.ID ELSE
        R.ACCT = ''
        RELEASE FN.ACCT, ACC.ID
        CRT 'Account-':ACC.ID:'-missing'
        RETURN
    END

    READU R.ECB FROM FN.ECB, ACC.ID ELSE
        R.ECB = ''
        RELEASE FN.ACCT, ACC.ID
        RELEASE FN.ECB, ACC.ID
        CRT 'ECB-':ACC.ID:'-missing'
        RETURN
    END

    ACCT.BAL = R.ACCT<AC.ONLINE.ACTUAL.BAL>
    IF NOT(ACCT.BAL) THEN ACCT.BAL = 0

    CRT 'Processing account ':ACC.ID

    ACTIV.BAL = 0
    BALANCE.DATE = TODAY
    YBALANCE = ""
    CR.MVMT = ""
    DR.MVMT = ""
    ERR = ""
    CALL EB.GET.ACCT.BALANCE(ACC.ID, R.ACCT, "BOOKING", BALANCE.DATE, "", YBALANCE, CR.MVMT, DR.MVMT, ERR)
    ACTIV.BAL = YBALANCE
    IF NOT(ACTIV.BAL) THEN ACTIV.BAL = 0


    CRT 'Rebuilding ACCT.ACTIVITY for account ':ACC.ID:' Balance in account = ':ACCT.BAL:' balance in activity = ':ACTIV.BAL
    ACTIV.MNTHS = R.ECB<ECB.ACTIVITY.MONTHS>
    BK.BAL = ACCT.BAL
    VALUE.BAL = ACCT.BAL
    CONVERT @VM TO @FM IN ACTIV.MNTHS
    ACNT = DCOUNT(ACTIV.MNTHS, @FM)
    LAST.BK = ''; LAST.VAL = ''
    FOR J = ACNT TO 1 STEP -1
        ACTIV.ID = ACC.ID:'-':ACTIV.MNTHS<J>
        R.ACTIV = ''
        READ R.ACTIV FROM FN.ACTIV, ACTIV.ID ELSE R.ACTIV = ''

        BCNT = DCOUNT(R.ACTIV<IC.ACT.BK.DAY.NO>, @VM)
        FOR K = BCNT TO 1 STEP -1
            R.ACTIV<IC.ACT.BK.BALANCE, K> = BK.BAL
            BK.BAL = BK.BAL - R.ACTIV<IC.ACT.BK.CREDIT.MVMT, K> - R.ACTIV<IC.ACT.BK.DEBIT.MVMT, K>
            LAST.BK = ACTIV.MNTHS<J>
        NEXT K

        VCNT = DCOUNT(R.ACTIV<IC.ACT.DAY.NO>, @VM)
        FOR K = VCNT TO 1 STEP -1
            R.ACTIV<IC.ACT.BALANCE, K> = VALUE.BAL
            VALUE.BAL = VALUE.BAL - R.ACTIV<IC.ACT.TURNOVER.CREDIT, K> - R.ACTIV<IC.ACT.TURNOVER.DEBIT, K>
            LAST.VAL = ACTIV.MNTHS<J>
        NEXT K
        WRITE R.ACTIV TO FN.ACTIV, ACTIV.ID
    NEXT J

    IF LAST.BK THEN
        R.ACTIV = ''; ACTIV.ID = ACC.ID:'-':LAST.BK
        READ R.ACTIV FROM FN.ACTIV, ACTIV.ID ELSE R.ACTIV = ''
        BK.BAL = R.ACTIV<IC.ACT.BK.BALANCE, 1> - R.ACTIV<IC.ACT.BK.CREDIT.MVMT, 1> - R.ACTIV<IC.ACT.BK.DEBIT.MVMT, 1>
        IF BK.BAL GE 0 THEN
            R.ACTIV<IC.ACT.BK.CREDIT.MVMT, 1> += BK.BAL
        END ELSE
            R.ACTIV<IC.ACT.BK.DEBIT.MVMT, 1> += BK.BAL
        END
        WRITE R.ACTIV TO FN.ACTIV, ACTIV.ID
    END

    IF LAST.VAL THEN
        R.ACTIV = ''; ACTIV.ID = ACC.ID:'-':LAST.VAL
        READ R.ACTIV FROM FN.ACTIV, ACTIV.ID ELSE R.ACTIV = ''
        VALUE.BAL = R.ACTIV<IC.ACT.BALANCE, 1> - R.ACTIV<IC.ACT.TURNOVER.CREDIT, 1> - R.ACTIV<IC.ACT.TURNOVER.DEBIT, 1>
        IF BK.BAL GE 0 THEN
            R.ACTIV<IC.ACT.TURNOVER.CREDIT, 1> += VALUE.BAL
        END ELSE
            R.ACTIV<IC.ACT.TURNOVER.DEBIT, 1> += VALUE.BAL
        END
        WRITE R.ACTIV TO FN.ACTIV, ACTIV.ID
    END

*Line [ 149 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ACCT.STMT.REBUILD(ACC.ID)

    RELEASE FN.ACCT, ACC.ID
    RELEASE FN.ECB, ACC.ID

RETURN
END
