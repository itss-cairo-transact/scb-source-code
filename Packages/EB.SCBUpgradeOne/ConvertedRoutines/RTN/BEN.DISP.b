* @ValidationCode : MjoxMDMyMDU0OTg0OkNwMTI1MjoxNjQwNjgzNTE3Njg1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:25:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
******NESSREEN AHMED 26/05/2011*******
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BEN.DISP(BENF)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHQ.RETURN.NEW

    SS = "������/ "
    BNF = '' ; DEP = ''
    KEY.ID = BENF
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR( 'SCB.CHQ.RETURN.NEW':@FM:CHQ.RET.BENEFICIARY, KEY.ID , BNF)
F.ITSS.SCB.CHQ.RETURN.NEW = 'F.SCB.CHQ.RETURN.NEW'
FN.F.ITSS.SCB.CHQ.RETURN.NEW = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW,KEY.ID,R.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW,ERROR.SCB.CHQ.RETURN.NEW)
BNF=R.ITSS.SCB.CHQ.RETURN.NEW<CHQ.RET.BENEFICIARY>
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR( 'SCB.CHQ.RETURN.NEW':@FM:CHQ.RET.RETURN.DEP, KEY.ID , DEP)
F.ITSS.SCB.CHQ.RETURN.NEW = 'F.SCB.CHQ.RETURN.NEW'
FN.F.ITSS.SCB.CHQ.RETURN.NEW = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW,KEY.ID,R.ITSS.SCB.CHQ.RETURN.NEW,FN.F.ITSS.SCB.CHQ.RETURN.NEW,ERROR.SCB.CHQ.RETURN.NEW)
DEP=R.ITSS.SCB.CHQ.RETURN.NEW<CHQ.RET.RETURN.DEP>
    IF DEP = '5301' THEN
        BENF = SS : BNF
    END ELSE
        BENF = ""
    END

RETURN
END
