* @ValidationCode : MjotMjAxMzczNTc3NzpDcDEyNTI6MTY0MDY4NDU4NjIyMjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:43:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE  BLD.ACC.MAX.DATE(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.POSITION

    COMP = ID.COMPANY
*----------------------------------------------------------------------**
    ACCT.ID  = ''
    MAX.DATE = ''
    SHIFT    = ''
    ROUND    = ''
    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC    = '' ; R.ACCT = ''
    CALL OPF(FN.ACC,F.ACC)
    TODAY.DATE = TODAY
    T.YEAR     = TODAY.DATE[1,4]
    XX         = SPACE(130)
*-------------------------------------------*
    TOD.DATE = TODAY
* ADD.MONTHS(DATE.VARIABLE,NO.OF.MONTHS)
*Line [ 49 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ADD.MONTHS(TOD.DATE,-6)
*TEXT = "TOD= ":TOD.DATE ; CALL REM
*-------------------------------------------*
    T.SEL  = "SELECT FBNK.ACCOUNT WITH WORKING.BALANCE NE 0 AND WORKING.BALANCE NE ''"
    T.SEL := " AND CATEGORY NE 1001 AND CATEGORY NE 1002 AND CATEGORY NE 6501"
    T.SEL := " AND CATEGORY NE 6502 AND CATEGORY NE 6503 AND CATEGORY NE 6504"
    T.SEL := " AND CATEGORY NE 6505 AND CATEGORY NE 12001 AND CATEGORY NE 19090"
    T.SEL := " AND CUSTOMER EQ '' AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
***--------------------------------------------------------------****
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACCT, F.ACC ,E11)
        CUST.DR  = R.ACCT<AC.DATE.LAST.DR.CUST>
        CUST.CR  = R.ACCT<AC.DATE.LAST.CR.CUST>
        BANK.DR  = R.ACCT<AC.DATE.LAST.DR.BANK>
        BANK.CR  = R.ACCT<AC.DATE.LAST.CR.BANK>
        AUTO.DR  = R.ACCT<AC.DATE.LAST.DR.AUTO>
        AUTO.CR  = R.ACCT<AC.DATE.LAST.CR.AUTO>

        XX<1,1>  = CUST.DR
        XX<1,2>  = CUST.CR
        XX<1,3>  = BANK.DR
        XX<1,4>  = BANK.CR
        XX<1,5>  = AUTO.DR
        XX<1,6>  = AUTO.CR
        MAX.DATE = CUST.DR

        FOR NN = 2 TO 6
            IF XX<1,NN> GT MAX.DATE THEN
                MAX.DATE = XX<1,NN>
            END
        NEXT NN
        IF TOD.DATE GT MAX.DATE THEN
*            TEXT = "MAX = ": MAX.DATE  ; CALL REM
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        END
        ELSE
*            ENQ.ERROR = "NO RECORDS"
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = "DUMM"
        END
        XX   = ''
    NEXT I
RETURN
*==============================================================********
END
