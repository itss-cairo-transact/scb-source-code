* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>560</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/07/07
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CU.LE.200(ENQ)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    KEY.LIST="" ;  SELECTED="" ; ER.MSG=""

    GOSUB INITIATE
    GOSUB OPEN.FILES
    GOSUB SEL.CU

    RETURN
****************************************
INITIATE:

    WS.3Y.DATE        = TODAY
    WS.FT.REF         = ''
    WS.FT.1           = ''
    WS.CU.1000.6M     = 0
    WS.CU.500.6M      = 0
*Line [ 53 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ADD.MONTHS(WS.3Y.DATE,-36)

    WS.1Y.DATE        = TODAY
*Line [ 57 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ADD.MONTHS(WS.1Y.DATE,-12)

    WS.6M.DATE        = TODAY
*Line [ 61 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ADD.MONTHS(WS.6M.DATE,-6)

    I = 0
    RETURN
****************************************
OPEN.FILES:
*Master files

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    RETURN
****************************************
SEL.CU:

    T.SEL  = "SELECT ":FN.CU:" WITH @ID UNLIKE 994... AND @ID UNLIKE 998... AND POSTING.RESTRICT LT 89 AND COMPANY.BOOK EQ ":ID.COMPANY
    T.SEL := " AND POSTING.RESTRICT NE 70 AND CREDIT.CODE EQ '' AND CREDIT.STAT EQ '' WITHOUT SECTOR IN ( 5010 5020 ) OR @ID IN (444333 5556667 10001000 333333 999888 601010 222222 111111 306306 500500) BY COMPANY.BOOK BY @ID "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED
    IF SELECTED THEN
        FOR ISCDI = 1 TO SELECTED
            WS.LCY.AMT    = 0
            WS.LST.TRNS.D = ''
            WS.LD.FLG     = 0

            CALL F.READ(FN.CU,KEY.LIST<ISCDI>,R.CU,F.CU,ER.CU)

            IF R.CU<EB.CUS.POSTING.RESTRICT> GE 89 THEN
                GOTO NEXT.SCDI
            END

            IF R.CU<EB.CUS.POSTING.RESTRICT> EQ 1 THEN
                GOTO NEXT.SCDI
            END

            IF R.CU<EB.CUS.POSTING.RESTRICT> EQ 13 THEN
                GOTO NEXT.SCDI
            END

            WS.CU.ID      = KEY.LIST<ISCDI>
            WS.CU.BR.CODE = R.CU<EB.CUS.COMPANY.BOOK>[2]

            CALL FSCB.CU.LCY.LTRNSD(WS.CU.ID,WS.LCY.AMT,WS.LST.TRNS.D,WS.LD.FLG)

            IF WS.LD.FLG = 1 THEN
                GOTO NEXT.SCDI
            END

            IF WS.LCY.AMT GT 200 THEN
                GOTO NEXT.SCDI
            END

            IF WS.LST.TRNS.D GE WS.6M.DATE THEN
                GOTO NEXT.SCDI
            END

            I++
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = WS.CU.ID

NEXT.SCDI:
        NEXT ISCDI
        IF I = 0 THEN
            ENQ<2,2> = '@ID'
            ENQ<3,2> = 'EQ'
            ENQ<4,2> = 'NO RECORDS'
        END
    END
    RETURN
****************************************
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*
*====================================================================*

END
