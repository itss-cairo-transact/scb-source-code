* @ValidationCode : MjozNjA2MTg3NTE6Q3AxMjUyOjE2NDIwNzg1NTM5MDA6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 13 Jan 2022 14:55:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE BIL.COL.TODAY (ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Adding $INCLUDE I_F.SCB.BANK.BRANCH - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH

    FN.BATCH = 'FBNK.BILL.REGISTER' ; F.BATCH = '' ; R.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

    KEY.LIST=""; SELECTED="" ; ER.MSG=""
    SAM1 = TODAY

    COMP       = C$ID.COMPANY
    USR.DEP    = "1700":COMP[8,2]
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
FN.F.ITSS.SCB.BANK.BRANCH = ''
CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
CALL F.READ(F.ITSS.SCB.BANK.BRANCH,USR.DEP,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
LOCT=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.LOCATION>
    SAM1 = TODAY
    IF LOCT EQ '1'  THEN
        CALL CDT('',SAM1,"+1W")
    END
    IF LOCT EQ '2'  THEN
        CALL CDT('',SAM1,"+2W")
    END

    T.SEL  = "SELECT FBNK.BILL.REGISTER WITH ( BANK NE 0017 AND BANK NE 9902 ) AND  MATURITY.EXT LE ": SAM1 :" AND BILL.CHQ.STA EQ 5 AND CO.CODE EQ " :COMP:" AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BANK UNLIKE 99... AND CURRENCY EQ EGP "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.BATCH, KEY.LIST<I>, R.BATCH, F.BATCH, ETEXT)
    TEXT = "SELECTED" : SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END
*******************************************************************************************
RETURN
END
