* @ValidationCode : MjotMTE3NzE5ODQ3MzpDcDEyNTI6MTY0MDY4NDQyNDg5Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:40:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE BLD.AC.EXP.RATE(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    COMP = ID.COMPANY

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT ":FN.AC:" WITH CATEGORY EQ 1001"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*Line [ 46 ] Convert VM into @VM - ITSS - R21 Upgrade - 2021-12-23
            WS.AC.COUNT = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
            AD.DATE     = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
            ACI.ID      = KEY.LIST<I>:'-':AD.DATE

            CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 52 ] Convert VM into @VM - ITSS - R21 Upgrade - 2021-12-23
            WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
            WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>

*=================================================================
            IF WS.RATE NE '' THEN
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<I>
            END ELSE
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'
            END
        NEXT I
    END
RETURN
END
