* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
********************************MAHMOUD 12/3/2012*****************
* Routine to check ACCT.STMT.PRINT updated with wrong amount     *
* Run after monthly COB                                          *
******************************************************************
    PROGRAM ASP.TEST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*-----------------------------------------------------------------------------

    REPORT.NAME = "ASP.TEST"
    REPORT.ID   = "P.FUNCTION"
    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB INITIALISE
    GOSUB PRINT.HEAD
    GOSUB DATAFILE1
    GOSUB READ.ACCT.STMT.PRINT
    GOSUB PROCESS
    IF K = 0 THEN
        END.REP = " �� ���� ������ "
    END ELSE
        END.REP = " ����� ������� "
    END
    PRINT "********** ":END.REP:" **********"
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*-----------------------------------------------------------------------------
INITIALISE:
    FN.ACCT.STMT.PRINT = 'F.ACCT.STMT.PRINT'
    F.ACCT.STMT.PRINT = ''
    FN.ACCOUNT = 'F.ACCOUNT'
    F.ACCOUNT = ''
    FN.ACCOUNT.STATEMENT = 'F.ACCOUNT.STATEMENT'
    F.ACCOUNT.STATEMENT = ''
*    LAST.W1 = R.DATES(EB.DAT.LAST.WORKING.DAY)

    PATHNAME1 = "&SAVEDLISTS&"
    FILENAME1 = "ASP.FAIL"
    TD1 = TODAY
    K = 0

    LAST.W1 = TODAY
*Line [ 69 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ADD.MONTHS(LAST.W1,'-1')
    CALL LAST.WDAY(LAST.W1)

    CALL OPF(FN.ACCOUNT,F.ACCOUNT)
    CALL OPF(FN.ACCOUNT.STATEMENT,F.ACCOUNT.STATEMENT)
    CALL OPF(FN.ACCT.STMT.PRINT,F.ACCT.STMT.PRINT)
    RETURN
*-----------------------------------------------------------------------------
READ.ACCT.STMT.PRINT:
    SELECT.CMD = 'SELECT ':FN.ACCT.STMT.PRINT
    ACCT.STMT.PRINT.LIST = ''
    R.ACCOUNT.STATEMENT = ''
    R.ACCOUNT = ''
    LIST.NAME = ''
    SELECTED = ''
    SYSTEM.RETURN.CODE = ''
    CALL EB.READLIST(SELECT.CMD,ACCT.STMT.PRINT.LIST,LIST.NAME,SELECTED,SYSTEM.RETURN.CODE)
    RETURN
*-----------------------------------------------------------------------------
PROCESS:

    LOOP
        REMOVE ACCT.STMT.PRINT.ID FROM ACCT.STMT.PRINT.LIST SETTING ACCT.STMT.PRINT.MARK
    WHILE ACCT.STMT.PRINT.ID : ACCT.STMT.PRINT.MARK
        R.ACCT.STMT.PRINT = ''
        YERR = ''
        CALL F.READ(FN.ACCOUNT,ACCT.STMT.PRINT.ID,R.ACCOUNT,F.ACCOUNT,ER.AC.MSG)
        IF R.ACCOUNT<AC.CUSTOMER> NE '' THEN
            IF R.ACCOUNT<AC.OPEN.ACTUAL.BAL> NE '' THEN
                CALL F.READ(FN.ACCT.STMT.PRINT,ACCT.STMT.PRINT.ID,R.ACCT.STMT.PRINT,F.ACCT.STMT.PRINT,EG.ASP.MSG)
*Line [ 100 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-28
                COUNT.DATES= DCOUNT(R.ACCT.STMT.PRINT,@FM)
                EXTRACT.LAST.DATE= FIELD(R.ACCT.STMT.PRINT<COUNT.DATES>,'/',1)
                EXTRACT.P.L.DATE = FIELD(R.ACCT.STMT.PRINT<COUNT.DATES-1>,'/',1)          ;* Get previous statement date
                EXTRACT.LAST.AMOUNT= FIELD(R.ACCT.STMT.PRINT<COUNT.DATES>,'/',2)
                CALL F.READ(FN.ACCOUNT.STATEMENT,ACCT.STMT.PRINT.ID,R.ACCOUNT.STATEMENT,F.ACCOUNT.STATEMENT,ER.AC.ST.MSG)
                STMT.LAST.BAL = R.ACCOUNT.STATEMENT<AC.STA.FQU1.LAST.BALANCE>
                LAST.W        = R.ACCOUNT.STATEMENT<AC.STA.FQU1.LAST.DATE>
                IF LAST.W THEN
                    IF STMT.LAST.BAL THEN
                        IF EXTRACT.LAST.AMOUNT EQ '' THEN
                            CRT ACCT.STMT.PRINT.ID
                            GOSUB PRINTLN
                            LAST.DATES = 0
                            GOTO NEXT.REC
                        END
                        IF EXTRACT.P.L.DATE = LAST.W THEN   ;* Check for the updated record in last monthly closing
                            IF (EXTRACT.LAST.AMOUNT NE STMT.LAST.BAL) OR (EXTRACT.LAST.AMOUNT EQ '' ) THEN    ;* Check for wrong amount in ASP
                                CRT ACCT.STMT.PRINT.ID
                                GOSUB PRINTLN
                                LAST.DATES = 0
                            END
                        END
                    END
                END
            END
        END
NEXT.REC: REPEAT
    RETURN
*...................................
PRINTLN:
********
    K++
    XX  = ''
    XX<1,K>[1,20]    = ACCT.STMT.PRINT.ID
    XX<1,K>[20,20]   = FMT(STMT.LAST.BAL,"L2,")
    XX<1,K>[40,20]   = FMT(EXTRACT.LAST.AMOUNT,"L2,")
    PRINT XX<1,K>
    PRINT STR('.',100)

    XX.DATA  = ACCT.STMT.PRINT.ID
    WRITESEQ XX.DATA TO BB ELSE
        CRT " ERROR WRITE FILE "
    END
    RETURN

****************************************************************
PRINT.HEAD:
*---------
    DATY    = TODAY
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    T.DAY1 = TD1[7,2]:'/':TD1[5,2]:"/":TD1[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1)
    PR.HD :="'L'":SPACE(40):"����� �� ������� ���������� ���� ���� ������"
    PR.HD :="'L'":SPACE(43):"------------------------------------"
    PR.HD :="'L'":SPACE(1)
    PR.HD :="'L'":"��� ������":SPACE(10):"������ ������":SPACE(8):"������ �����"
    PR.HD :="'L'":STR('_',100)
    PR.HD :="'L'"
    PRINT
    HEADING PR.HD
    RETURN
*-----------------------------------------------------------------------
DATAFILE1:
*---------
    OPENSEQ PATHNAME1 , FILENAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATHNAME1:' ':FILENAME1
        HUSH OFF
    END
    OPENSEQ PATHNAME1 , FILENAME1 TO BB ELSE
        CREATE BB THEN
            CRT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END
    RETURN
********************************************************************
END
