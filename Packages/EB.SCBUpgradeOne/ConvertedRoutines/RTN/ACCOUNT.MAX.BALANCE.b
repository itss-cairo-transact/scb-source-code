* @ValidationCode : MjotMzY4MTI2MjY1OkNwMTI1MjoxNjQwNjE5MTMxODgyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 17:32:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-24</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ACCOUNT.MAX.BALANCE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.GEN.CONDITION
*Line [ 33 ] Comment $INCLUDE I_F.SCB.ACCOUNT.TYPES - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.ACCOUNT.TYPES
*Line [ 35 ] Comment $INCLUDE I_F.SCB.ACCT.SERIAL - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.ACCT.SERIAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SECTOR
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INDUSTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.STATUS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Comment $INCLUDE I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY

    S = ''

    GOSUB INITIATE
    IF COMI THEN
        GOSUB PRINT.HEAD
        GOSUB CALLDB

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    END
RETURN
*==============================================================
INITIATE:
    REPORT.ID='ACCOUNT.MAX.BALANCE'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = " Enter Number of Customers :  "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    NUM.CUS  = COMI
RETURN
*===============================================================
CALLDB:
    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    T.SEL = "SELECT FBNK.ACCOUNT WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" BY WORKING.BALANCE"
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",COMI,ER.MSG)

*MAX.CUS  = SELECTED - NUM.CUS
*MAX.CUS  = MAX.CUS  + 1

    X  =  NUM.CUS + 1

    IF KEY.LIST THEN

* FOR I = MAX.CUS TO SELECTED
* FOR I = SELECTED TO MAX.CUS STEP -1
        S = -1
        FOR I = X TO 1 STEP -1
            IF NUM(KEY.LIST<I>) THEN
                CALL F.READ(FN.ACCT,KEY.LIST<I>,R.ACCT,F.ACCT,E1)
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.ACCT<AC.CUSTOMER>,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,R.ACCT<AC.CUSTOMER>,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

                CUS.NO    = R.ACCT<AC.CUSTOMER>
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                ACCT.NO   = KEY.LIST<I>
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,R.ACCT<AC.CATEGORY>,ACCT.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,R.ACCT<AC.CATEGORY>,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
ACCT.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

                IF R.ACCT<AC.WORKING.BALANCE> < 0 THEN
                    DEPT.BAL  ="]": R.ACCT<AC.WORKING.BALANCE>:"["
                END ELSE
                    DEPT.BAL  = R.ACCT<AC.WORKING.BALANCE>
                END

                XX = SPACE(120)
                XX<1,1>[1,10]  = CUS.NO
                XX<1,1>[15,35] = CUST.NAME
                XX<1,1>[55,20] = ACCT.NO
                XX<1,1>[75,25] = ACCT.NAME
                XX<1,1>[107,15]= DEPT.BAL

                PRINT XX<1,1>
            END
            S ++
        NEXT I
        PRINT STR('=',120)
        PRINT ; PRINT "������� = ":S:" �����"
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

RETURN
*===============================================================
PRINT.HEAD:
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ���� ����� ����� ����� � " :  S  : " ����� "
    PR.HD :="'L'":SPACE(45):STR('_',35)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������" :SPACE(10):" ��� ������" :SPACE(20):" ��� ������":SPACE(10):" ��� ������ ":SPACE(17):"������ ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
