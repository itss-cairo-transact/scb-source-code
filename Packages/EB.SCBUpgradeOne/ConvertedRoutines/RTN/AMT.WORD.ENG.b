* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
******************************NI7OOOOOOOOOOOO*************
*** SUBROUTINE AMT.WORD.ENG(AMWD1)
    SUBROUTINE AMT.WORD.ENG(AMWD1)

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    I.AMT = AMWD1
    LANG = 'GB'
    N.O.LN = 2
    LN.LNT = 53
    FLG = 0
    O.AMT=""
    ERR.MS = ""
    AMWD1 = ""
    CALL DE.O.PRINT.WORDS(I.AMT,O.AMT,LANG,LN.LNT,N.O.LN,ERR.MS)
    VAR1 = O.AMT[1,54]
    CNT = 54
    LOOP
    UNTIL FLG = 1
        VAR2 = O.AMT[CNT,1]
        IF VAR2 = "*" THEN
            VAR1 = O.AMT[1,CNT]
            FLG = 1
        END
        ELSE
            CNT = CNT - 1
        END
    REPEAT
    AMWD1 = VAR1
    O.DATA= AMWD1
** TEXT = "AMWD1 : " : AMWD1 ; CALL REM
*** PRINT O.AMT
END
