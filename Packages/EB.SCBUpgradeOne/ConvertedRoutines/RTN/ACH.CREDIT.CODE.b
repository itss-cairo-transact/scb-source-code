* @ValidationCode : MjotMTc0OTI3MjgyNDpDcDEyNTI6MTY0MDY4MDE5NTk0OTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 10:29:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE ACH.CREDIT.CODE(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Comment $INCLUDE I_F.SCB.ACH.PACS008 - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ACH.PACS008
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    FN.ACH ='F.SCB.ACH.PACS008'
    F.ACH  =''
    CALL OPF(FN.ACH,F.ACH)

    T.SEL = "SELECT F.SCB.ACH.PACS008 WITH PACS.TYPE EQ 1"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.ACH.PACS008':@FM:PACS8.CREDITOR.ACCOUNT.NO,KEY.LIST<I>,ACCT.NO)
F.ITSS.SCB.ACH.PACS008 = 'F.SCB.ACH.PACS008'
FN.F.ITSS.SCB.ACH.PACS008 = ''
CALL OPF(F.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008)
CALL F.READ(F.ITSS.SCB.ACH.PACS008,KEY.LIST<I>,R.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008,ERROR.SCB.ACH.PACS008)
ACCT.NO=R.ITSS.SCB.ACH.PACS008<PACS8.CREDITOR.ACCOUNT.NO>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUS.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,CUS.LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CREDIT.CODE = CUS.LOCAL<1,CULR.CREDIT.CODE>
            NEW.SEC     = CUS.LOCAL<1,CULR.NEW.SECTOR>
*SEC         = R.CUS<EB.CUS.SECTOR>
*TEXT = CREDIT.CODE; CALL REM
            IF CREDIT.CODE EQ '100' AND NEW.SEC NE '4650' THEN
                J = J + 1
                ENQ<2,J> = '@ID'
                ENQ<3,J> = 'EQ'
                ENQ<4,J> = KEY.LIST<I>
            END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
RETURN
END
