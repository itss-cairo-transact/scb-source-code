* @ValidationCode : MjoxMDgyOTIyNzA6Q3AxMjUyOjE2NDA2ODUwOTcxMDQ6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:51:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLD.DRMNT.USA(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Comment $INCLUDE I_F.SCB.CU.DRMNT.INDEX - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CU.DRMNT.INDEX

    COMP     = ID.COMPANY
    COUNT.DR = 0
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    FN.DRMNT = "F.SCB.CU.DRMNT.INDEX"
    F.DRMNT = ''; R.DRMNT = ''
    CALL OPF(FN.DRMNT,F.DRMNT)

    FN.CUS = "FBNK.CUSTOMER"
    F.CUS = ''; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    T.SEL  = "SELECT F.SCB.CU.DRMNT.INDEX WITH POSTING LT 89"
    T.SEL := " AND DRMNT.CODE.DATE NE '' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*==============================================================
    IF SELECTED THEN
        Z = 1
        FOR KK = 1 TO SELECTED
            CALL F.READ(FN.DRMNT,KEY.LIST<KK>, R.DRMNT, F.DRMNT, DRMNT.ER)
            CUS.ID = KEY.LIST<KK>
            CALL F.READ(FN.CUS,CUS.ID, R.CUS, F.CUS, CU.ERR)
            NAT = R.CUS<EB.CUS.NATIONALITY>
            OTHER.NAT = R.CUS<EB.CUS.OTHER.NATIONALITY>

            IF (NAT EQ "US" OR OTHER.NAT EQ "US") THEN
                ENQ<2,Z> = '@ID'
                ENQ<3,Z> = 'EQ'
                ENQ<4,Z> = CUS.ID
                Z = Z +1
            END

        NEXT KK
    END ELSE
        ENQ<2,KK> = '@ID'
        ENQ<3,KK> = 'EQ'
        ENQ<4,KK> = "LARAAAA"
        ENQ.ERROR = 'NO RECORDS FOUND'
    END

RETURN
END
