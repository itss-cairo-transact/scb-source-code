* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-17</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.FT.CBE(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    XX=""

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF (FN.CUR,F.CUR)

    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,ERR)

    RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
***    TEXT = RATE ; CALL REM
    XX = RATE * 100000
***    TEXT = XX ; CALL REM


* FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' R.FT = ''
* CALL OPF (FN.FT,F.FT)

    T.SEL = " SELECT FBNK.FUNDS.TRANSFER WITH LOC.AMT.CREDITED GT ": XX: " AND CREDIT.ACCT.NO LIKE 994...500101 AND DEBIT.ACCT.NO UNLIKE 994... "
***    TEXT = SELECTED ; CALL REM
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
***    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
*        ENQ.ERROR = "NO RECORDS"
        ENQ<2,I> = "@ID"
        ENQ<3,I> = "EQ"
        ENQ<4,I> = 'DUM'

    END

*******************************************************************************************
    RETURN
END
