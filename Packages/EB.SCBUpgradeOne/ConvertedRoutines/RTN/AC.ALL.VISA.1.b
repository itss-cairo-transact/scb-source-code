* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------NI7OOOOOOOOOOOOOOOO------------------------------------------
    SUBROUTINE AC.ALL.VISA.1(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMPY = ID.COMPANY

    FN.CUS='FBNK.CUSTOMER'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
    FN.CARD='FBNK.CARD.ISSUE'
    F.CARD=''
    CALL OPF(FN.CARD,F.CARD)

* CUS.IND = R.CUS<EB.CUS.INDUSTRY>
* T.SEL="SELECT FBNK.CUSTOMER WITH INDUSTRY LT 999 AND INDUSTRY LIKE ...00":CUS.IND


 T.SEL="SELECT FBNK.ACCOUNT WITH CATEGORY IN (1205 1206 1207 1208) AND CO.CODE EQ ":COMPY
****  T.SEL="SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE 20090101 BY COMPANY.BOOK"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
* CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
            FOR ENQ.LP = 1 TO SELECTED
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
            NEXT ENQ.LP
    END

**********************
    RETURN
END
