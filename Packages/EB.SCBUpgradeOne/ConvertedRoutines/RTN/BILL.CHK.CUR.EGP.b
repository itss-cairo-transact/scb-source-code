* @ValidationCode : MjotMTEzMTUwOTc5NTpDcDEyNTI6MTY0MDY4Mzc4OTUzNDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:29:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE

SUBROUTINE BILL.CHK.CUR.EGP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 25 ] Comment $INCLUDE I_BR.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_BR.LOCAL.REFS
*--------------------------------------------
    IF COMI THEN
        CUR.ID = COMI
    END ELSE
        CUR.ID = R.NEW(EB.BILL.REG.CURRENCY)
    END

    IF CUR.ID NE 'EGP' THEN
*** ETEXT = "MUST BE EGP"
    END
RETURN
END
