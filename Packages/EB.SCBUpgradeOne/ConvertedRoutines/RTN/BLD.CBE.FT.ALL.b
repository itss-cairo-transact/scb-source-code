* @ValidationCode : MjotMTQyOTY3NDYzODpDcDEyNTI6MTY0MDcwMjY3MzU3NTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:44:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLD.CBE.FT.ALL(ENQ.DATA)
*Line [ 20 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 20 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 20 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*Line [ 20 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.FUNDS.TRANSFER
*--------------------------------------------
    FN.FT = "FBNK.FUNDS.TRANSFER$HIS"  ; F.FT  = ""
    CALL OPF(FN.FT,F.FT)
*------------------------------------------------
    TOD.TO   = TODAY
    TOD.FROM = TODAY
    CALL CDT ('' , TOD.FROM , "-5W")

    SEL.CMD  = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DEBIT.CUSTOMER NE ''"
    SEL.CMD := " AND CREDIT.CUSTOMER NE '' AND PROCESSING.DATE GE ":TOD.FROM
    SEL.CMD := " AND PROCESSING.DATE LE ": TOD.TO
    SEL.CMD := " BY DEBIT.CUSTOMER BY CURRENCY"
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    TEXT = NOREC  ; CALL REM

    NN = 1
    IF NOREC THEN
        FOR I = 1 TO NOREC
            CALL F.READ(FN.FT,SELLIST<I>,R.FT,F.FT,E.FT)
            DB.CUS = R.FT<FT.DEBIT.CUSTOMER>
            CR.CUS = R.FT<FT.CREDIT.CUSTOMER>
            IF DB.CUS NE CR.CUS THEN
                ENQ.DATA<2,NN> = "@ID"
                ENQ.DATA<3,NN> = "EQ"
                ENQ.DATA<4,NN> = SELLIST<I>
                NN ++
            END
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
*-------------------------------------------------
RETURN
END
