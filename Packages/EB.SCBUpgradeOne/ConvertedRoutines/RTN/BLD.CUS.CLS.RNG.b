* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>527</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUS.CLS.RNG(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*-----------------------------------------*
    GOSUB INITIAL
    GOSUB PROCESS
    GOSUB GET.ACCOUNTS
    RETURN
*-----------------------------------------*
INITIAL:
*-------
    N.FLAG  = "0"
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'          ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.H  = 'FBNK.CUSTOMER$HIS'    ;  F.CUS.H = ''
    CALL OPF(FN.CUS.H,F.CUS.H)

    DATA.FLAG = 0
    RETURN
*-----------------------------------------
PROCESS:
*-------
    YTEXT = "Enter Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    START.DATE = COMI

    IF  START.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter Start date"; CALL REM
        RETURN
    END
*---------
    YTEXT = "Enter End Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    END.DATE = COMI

    IF END.DATE EQ '' THEN
        DATA.FLAG = 1
        TEXT ="You must enter End Date"; CALL REM
        RETURN
    END
    Z = 2
*-------------------------------------------*
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT GE 90 )"
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
    T.SEL := " AND COMPANY.BOOK EQ ":COMP.ID:" AND CURR.NO GT 1"
*    T.SEL := " AND @ID EQ 13200888"
    T.SEL := " AND TEXT UNLIKE BR... BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = START.DATE :"*": END.DATE

        FOR II = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<II>,R.ACC,F.ACC,CUS.ER)
            CUS.ID  = KEY.LIST<II>
            CUR.NO  = R.ACC<EB.CUS.CURR.NO>
            RR = 1
            MM = CUR.NO - 1
            FOR NN = CUR.NO TO 1 STEP -1
                IF RR EQ 1 THEN
                    CUST.ID      = CUS.ID:";"
                    CUST.ID.NXT  = CUS.ID:";":MM
                END ELSE
                    CUST.ID      = CUS.ID:";":MM
                    MM           = MM -1
                    CUST.ID.NXT  = CUS.ID:";":MM
                END
                IF RR EQ 1 THEN
                    RR = 2
                    CALL F.READ(FN.ACC,CUS.ID,R.ACC.N,F.ACC,CUS.ER44)
                    CUS.DAT.2 = R.ACC.N<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.ACC.N<EB.CUS.POSTING.RESTRICT>
                END ELSE
                    CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                    CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                END

                CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                IF CUS.POST NE CUS.POST.NXT THEN
                    IF CUS.POST GE 90 THEN
                        IF CUS.DAT.2 GE START.DATE[3,6] AND CUS.DAT.2 LE END.DATE[3,6] THEN
                            N.FLAG   = 1
*ENQ<2,Z> = "@ID"
*ENQ<3,Z> = "EQ"
*ENQ<4,Z> = KEY.LIST<II>
* Z ++
                            GOSUB GET.ACCOUNTS

                            NN       = 1
                        END
                    END
                END
            NEXT NN
        NEXT II

        IF N.FLAG NE 1 THEN
            ENQ<2,1> = "@ID"
            ENQ<3,1> = "EQ"
            ENQ<4,1> = "DUMMY"
        END
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
    RETURN
*------------------------------------------------
GET.ACCOUNTS:
*------------
    CUS.ID = KEY.LIST<II>

    FN.ACC.H = "FBNK.ACCOUNT$HIS"   ; F.ACC.H = ""
    CALL OPF(FN.ACC.H,F.ACC.H)
    AC.SEL = "SELECT ":FN.ACC.H:" WITH CUSTOMER EQ " : CUS.ID
    AC.SEL := " AND RECORD.STATUS EQ 'CLOSED'"
    AC.SEL := " AND CLOSURE.DATE GE " : START.DATE
    AC.SEL := " AND CLOSURE.DATE LE " : END.DATE
    AC.SEL := " BY CLOSURE.DATE"
    CALL EB.READLIST(AC.SEL,AC.LIST,"",AC.SELECTED,AC.ER.MSG)

    IF AC.SELECTED THEN
        FOR NN = 1 TO AC.SELECTED
            ACC.ID.HIS = FIELD(AC.LIST<NN>,";",1)
            CALL F.READ.HISTORY(FN.ACC.H,ACC.ID.HIS,R.ACC.HIS,F.ACC.H,ERR.HIS)
            ENQ<2,Z> = "@ID"
            ENQ<3,Z> = "EQ"
            ENQ<4,Z> = ACC.ID.HIS
            Z ++
        NEXT NN
    END
    RETURN
*------------------------------------------------
END
