* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE

*-----------------------------------------------------------------------------
* <Rating>-4</Rating>  ( M.ELSAYED 2012-7-29 )
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUS.EXP.REG(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
*==========================*
    XX  = TODAY
    XXX = TODAY
*Line [ 45 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
    CALL EB.SCBUpgradeOne.ADD.MONTHS(XXX,1)
*=============================================================*
    FN.CUST = 'FBNK.CUSTOMER' ; F.CUST = '' ;  R.CUST = ''
    CALL OPF (FN.CUST,F.CUST)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF (FN.AC,F.AC)
    I = 0
*====================================================================*
    T.SEL = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT LE 89 AND AND POSTING.RESTRICT NE 70 AND POSTING.RESTRICT NE 18 AND POSTING.RESTRICT NE 12 AND SCCD.CUSTOMER NE 'YES' AND NEW.SECTOR NE 4650 AND DRMNT.CODE NE 1 AND DRMNT.CODE NE 110 AND CREDIT.CODE NE 110 AND LIC.EXP.DATE LE ":XXX:" AND LIC.EXP.DATE GE ":XX:" AND COMPANY.BOOK EQ ":COMP
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN

        TEXT = SELECTED ; CALL REM
        FOR I = 1 TO SELECTED

            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<I>

        NEXT I
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"

    END
    RETURN


*******************************************************************************************
END
