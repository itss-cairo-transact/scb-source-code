* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
***  CREATED BY MOHAMED EL SAYED 2012/02/14
******************************************************
    SUBROUTINE BLD.AC.CHK.CU(ENQ.DATA)
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    K = 0
    FN.CU      = "FBNK.CUSTOMER"                 ; F.CU      = ""  ; R.CU     = ""
    FN.AC      = "FBNK.ACCOUNT"                    ; F.AC      = ""  ; R.AC     = ""
    CALL OPF (FN.AC,F.AC)
    T.SEL  = "SELECT ":FN.AC:" WITH CATEGORY EQ 1507 AND WORKING.BALANCE LT 0 AND WORKING.BALANCE NE '' "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E.AC)
*           R.AC<AC.CUSTOMER> = FIELD (KEY.LIST<I>,'.',1)
            WS.CUS.ID         = R.AC<AC.CUSTOMER>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            WS.GROUP.NUM   = LOCAL.REF<1,CULR.GROUP.NUM>
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF WS.CREDIT.CODE LE 100 THEN
                K ++
                ENQ.DATA<2,K> = "@ID"
                ENQ.DATA<3,K> = "EQ"
                ENQ.DATA<4,K> = KEY.LIST<I>
            END
        NEXT I
    END
    IF K = 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
END
