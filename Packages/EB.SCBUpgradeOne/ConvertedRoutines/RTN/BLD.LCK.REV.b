* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
********************MAHMOUD & M.ELSAYED 18/9/2012**************************
*  PROGRAM BLD.LCK.REV

    SUBROUTINE BLD.LCK.REV(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS

    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
**********************************************************
CALLDB:
*-------
    ZZ = 0
    FN.LCK.H2 = 'FBNK.AC.LOCKED.EVENTS$HIS' ; F.LCK.H2 = '' ; R.LCK.H2 = ''
    CALL OPF(FN.LCK.H2,F.LCK.H2)
    FN.LCK.H1 = 'FBNK.AC.LOCKED.EVENTS$HIS' ; F.LCK.H1 = '' ; R.LCK.H1 = ''
    CALL OPF(FN.LCK.H1,F.LCK.H1)
    RETURN
**********************************************************
PROCESS:
*-------
    H.SEL  = "SELECT ":FN.LCK.H2:" WITH RECORD.STATUS EQ 'REVE'"
    H.SEL := " AND FROM.DATE GE '20120601'"
    H.SEL := " AND INPUTTER LIKE ...ATM..."
    H.SEL := " AND CO.CODE EQ EG0010099"
    H.SEL := " AND ADDITIONAL.DATA EQ 'Int'"
    H.SEL := " AND TERMINAL.LOC NE ''"
    CALL EB.READLIST(H.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED  THEN
*    TEXT = SELECTED ; CALL REM
        LOOP
            REMOVE LCK.ID.2 FROM K.LIST SETTING POS
        WHILE LCK.ID.2:POS
            CALL F.READ(FN.LCK.H2,LCK.ID.2,R.LCK.H2,F.LCK.H2,ER.LCK.H2)
            LCK.AMT.2     = R.LCK.H2<AC.LCK.LOCKED.AMOUNT>
            LCK.FROM.DATE = R.LCK.H2<AC.LCK.FROM.DATE>
            LCK.ACCT.NO   = R.LCK.H2<AC.LCK.ACCOUNT.NUMBER>
            LCK.ID.1 = FIELD(LCK.ID.2,';',1):";1"
            CALL F.READ(FN.LCK.H1,LCK.ID.1,R.LCK.H1,F.LCK.H1,ER.LCK.H1)
            LCK.AMT.1 = R.LCK.H1<AC.LCK.LOCKED.AMOUNT>
            LCK.DIFF  = LCK.AMT.2 - LCK.AMT.1
            IF LCK.DIFF NE 0 THEN
                ZZ++
                ENQ<2,ZZ> = '@ID'
                ENQ<3,ZZ> = 'EQ'
                ENQ<4,ZZ> = LCK.ID.2
            END ELSE
                ZZ++
                ENQ<2,ZZ> = '@ID'
                ENQ<3,ZZ> = 'EQ'
                ENQ<4,ZZ> = 'DUMMY'
            END
        REPEAT
        RETURN
*=========================================
    END
