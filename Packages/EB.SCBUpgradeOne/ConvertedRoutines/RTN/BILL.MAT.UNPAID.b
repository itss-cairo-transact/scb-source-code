* @ValidationCode : MjotMTc3MDk5OTAwNDpDcDEyNTI6MTY0MDY4Mzg5NDM0Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:31:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-15</Rating>M.ELSAYED<27/1/2009>
*-----------------------------------------------------------------------------
SUBROUTINE BILL.MAT.UNPAID(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Comment $INCLUDE I_BR.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY

*********************************************************
    BRANCH.NOO = R.USER<EB.USE.DEPARTMENT.CODE>

    IF LEN(BRANCH.NOO) EQ 1 THEN
        BRANCH.NO = 0 : BRANCH.NOO
    END ELSE
        BRANCH.NO = BRANCH.NOO
    END

    BRANCH.ID = "1700" : BRANCH.NO
* TETX = "BRANCH.ID":BRANCH.ID; CALL REM

*********************************************************

    SAM  = TODAY
*TEXT = "SAM" :SAM ; CALL REM

    CALL CDT('', SAM,'-1W')
    UNMATURED = SAM
*TEXT = "UNMATURED":UNMATURED; CALL REM
*T.SEL = "SELECT FBNK.BILL.REGISTER WITH MAT.DATE LE ":UNMATURED:" AND BANK.BR EQ ":BRANCH.ID :" AND BILL.CHQ.STA EQ 2"

    T.SEL = "SELECT FBNK.BILL.REGISTER WITH MAT.DATE LE ":UNMATURED:" AND BILL.CHQ.STA EQ 2 AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*TEXT = "SELECTED":SELECTED; CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "No FOUND"
    END

RETURN
END
