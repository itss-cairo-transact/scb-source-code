* @ValidationCode : Mjo4MjY3MjM3NjA6Q3AxMjUyOjE2NDA2MTg5Nzc0NzY6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 17:29:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ACCOUNT.BRANCH.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Comment $INCLUDE I_F.SCB.VISA.TRANS.TOT - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.VISA.TRANS.TOT
*Line [ 29 ] Comment $INCLUDE I_F.SCB.MAST.TRANS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.MAST.TRANS

    IF O.DATA EQ '' THEN
        O.DATA = 0.00
    END ELSE
        O.DATA = O.DATA
    END

RETURN
END
