* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>2613</Rating>
*-----------------------------------------------------------------------------
***NESSREEN AHMED 01/04/2009*********

    SUBROUTINE ATM.USR.APP.SUPP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP

* A ROUTINE TO CREATE A TEXT FILE CONTANING ALL THE MASTER CASH PAYMENT THAT HAPPENED THROUGH THE DAY

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "atm_iss_sup"
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
**********************FILLING DATA********************************
******************************************************************
***    T.SEL =  "SELECT F.SCB.VISA.APP WITH ATM.APP.DATE EQ ": TODAY
***    YTEXT = "Enter the Date : "
***    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    T.SEL =  "SELECT F.SCB.ATM.APP WITH SEND.STAT EQ R AND (CARD.TYPE EQ 810 OR CARD.TYPE EQ 811)"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = '��� �������=':SELECTED ; CALL REM
******HEADER***************************************************************************
    DIM HH(1)
    HH(1) = 'FH'
    HH(1) := 'ADD Supplementary Card Request                           '
    HH(1) := 'Suez Canal Bank               '
*****Update by Nessreen Ahmed 16/12/2018*****************
*****HH(1) := 'E'
    HH(1) := 'C'
*****End of Update 16/12/2018****************************
    WRITESEQ HH(1) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':HH(1)
    END

***************************************************************************************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            NAME1 = '' ; NAME2 = '' ;  NAME3 = '' ; TITLE = '' ; M.STAT = '' ; SEX = '' ; ID.NO = ''
            FN.VISA.APP = 'F.SCB.ATM.APP' ; F.VISA.APP = '' ; R.VISA.APP = '' ; RETRY1 = '' ; E1 = ''
            TITLE = '' ; M.STAT = '' ; GENDER = '' ; GEN = ''; ID = '' ; ID.NO = '' ; TYPE = '' ; ID.TYPE = '' ; BR.F = ''
            BIRTH.MO = '' ; MM = '' ; DD = '' ; YYYY = '' ; JOB = '' ; BR = '' ; CATEG = '' ; CATEG1 = '' ; CATEG2 = '' ; CATEG3 = ''
            ACC.CURR = '' ; ACC.SAV = '' ; ACC.SAL = ''   ; LOCAL.REF.C = ''
            FST.ACT = ''  ; FN.CARD.ISS = 'F.CARD.ISSUE' ; F.CARD.ISS = '' ; R.CARD.ISS = '' ; E2 = ''  ; ATM.NO = '' ; P.CUST = '' ; KEY.USE = '' ; OLD.CUST = ''  ; NAME.ON.CRD = '' ; MOB.NO.1 = '' ; CARD.PROD = ''
            CARD.TYPE = '' ; CARD.FEES = '' ; CARD.LIMIT = ''
            KEY.LIST.N=""  ; SELECTED.N="" ; ER.MSG.N=""  ; ATM.NO = ''

            KEY.USE = FIELD(KEY.LIST<I>, ".", 1)
            CALL OPF(FN.VISA.APP,F.VISA.APP)
            CALL F.READ(FN.VISA.APP, KEY.LIST<I>, R.VISA.APP, F.VISA.APP, E1)
            FST.ACT = R.VISA.APP<SCB.VISA.FAST.ACC>
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER,FST.ACT, P.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,FST.ACT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
P.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
******UPDATED BY NESSREEN AHMED 3/2/2020**********************************************
******N.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":FST.ACT : " AND CARD.CODE IN (801 802 804 805) BY CARD.CODE"
******KEY.LIST.N=""
******SELECTED.N=""
******ER.MSG.N=""
******CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
******CALL OPF(FN.CARD.ISS,F.CARD.ISS)
******CALL F.READ(FN.CARD.ISS, KEY.LIST.N<1>, R.CARD.ISS, F.CARD.ISS, E2)
******LOCAL.REF.C = R.CARD.ISS<CARD.IS.LOCAL.REF>
******ATM.NO<I> = LOCAL.REF.C<1,LRCI.VISA.NO>
******END OF UPDATE 3/2/2020***********************************************************

            FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
            CALL F.READ(FN.CUSTOMER, KEY.USE, R.CUSTOMER, F.CUSTOMER, E1)
            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>

            SER = I:STR(' ',10 - LEN(I))
            NAME1  = LOCAL.REF<1,CULR.FNAME>
            NAME2  = LOCAL.REF<1,CULR.MNAME>
            NAME3  = LOCAL.REF<1,CULR.LNAME>
            TITLE  = LOCAL.REF<1,CULR.ATM.TITLE>
            M.STAT = LOCAL.REF<1,CULR.ATM.MARITAL.ST>
***Updated By Nessreen Ahmed 29/11/2018************************************************
            NAME.ON.CRD = R.VISA.APP<SCB.VISA.NAME.ON.CARD>
            CARD.PROD   = R.VISA.APP<SCB.VISA.CARD.PRODUCT>
            BEGIN CASE
            CASE CARD.PROD = 'CLASSIC'
                CARD.TYPE  = '4'
                CARD.FEES  = '11'
                CARD.LIMIT = '5'
            CASE CARD.PROD = 'PLATINUM'
                CARD.TYPE  = '28'
                CARD.FEES  = '45'
                CARD.LIMIT = '35'
            CASE CARD.PROD = 'TITANIUM'
                CARD.TYPE  = '29'
                CARD.FEES  = '46'
                CARD.LIMIT = '36'
**Updated By Nessreen Ahmed 30/1/2020************************************************
            CASE CARD.PROD = 'MEEZA'
                CARD.TYPE  = '39'
                CARD.FEES  = '49'
                CARD.LIMIT = '39'
**End of update 30/1/2020************************************************************
            END CASE
***end of Update 29/11/2018***********************************
****UPDATED ON 1/12/2008*************************************************
            TOT.LEN = (LEN(NAME1)+LEN(NAME2)+LEN(NAME3)+2)
***         FULL.NAME1 = NAME1:STR(" ",1):NAME2:STR(" ",1):NAME3:STR(" ",30 - TOT.LEN)
***         FULL.NAME = FULL.NAME1[1,30]
            FULL.NAME = NAME.ON.CRD:STR(" ",30 - LEN(NAME.ON.CRD))

************************************************************************
***End of Update 29/11/2018************************************************
            GENDER = LOCAL.REF<1,CULR.GENDER>
            IF GENDER = 'M-���' THEN
                GEN = 'Male'
            END ELSE
                IF GENDER = 'F-����' THEN  GEN = 'Female'
            END
            NSN.B = LOCAL.REF<1,CULR.NSN.NO>
            NSN.1 = FIELD(NSN.B, "-", 1)
            NSN.2 = FIELD(NSN.B, "-", 2)
            NSN.3 = FIELD(NSN.B, "-", 3)
            NSN.4 = FIELD(NSN.B, "-", 4)
            NSN =  NSN.1: NSN.2: NSN.3:NSN.4
            ID  = LOCAL.REF<1,CULR.ID.NUMBER>
            TYPE = LOCAL.REF<1,CULR.ID.TYPE>
            IF NSN = '' THEN
                ID.NO = ID
                IF TYPE = '1' THEN ID.TYPE = 'Personal_No'
                IF TYPE = '2' THEN ID.TYPE = 'Family_No'
                IF TYPE = '3' THEN ID.TYPE = 'Passport'
                IF TYPE = '4' THEN ID.TYPE = 'Gun_ID_No'

            END ELSE
                ID.NO = NSN
                ID.TYPE = 'National_No'
            END
            TELPHONE1 = LOCAL.REF<1,CULR.TELEPHONE>
            TELPHONE = TELPHONE1<1,1,1>
***Updated By Nessreen Ahmed 19/11/2018************************************************
            MOB.NO.1 = R.CUSTOMER<EB.CUS.SMS.1>
            MOB.NO   = MOB.NO.1<1,1>
***End of Update 19/11/2018************************************************************
            BIRTH.DAT = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
            MM = BIRTH.DAT[5,2]
            DD = BIRTH.DAT[7,2]
            YYYY = BIRTH.DAT[1,4]
            BIRTH.MON = MM:DD:YYYY
            JOB.ADD = LOCAL.REF<1,CULR.JADDR>
            JOB = LOCAL.REF<1,CULR.PROFESSION>
            IF JOB = '8' THEN
                JOB.DESC = 'staff'
            END ELSE
                JOB.DESC = 'client'
            END

            FN.CUSTOMER.P = 'F.CUSTOMER' ; F.CUSTOMER.P = '' ; R.CUSTOMER.P = '' ; E2 = ''
            CALL OPF(FN.CUSTOMER.P,F.CUSTOMER.P)

            CALL F.READ(FN.CUSTOMER.P, P.CUST, R.CUSTOMER.P, F.CUSTOMER.P, E2)
            LOCAL.REF.P = R.CUSTOMER.P<EB.CUS.LOCAL.REF>
****         BR = R.CUSTOMER.P<EB.CUS.ACCOUNT.OFFICER>
            BR  = R.CUSTOMER<EB.CUS.COMPANY.BOOK>[2]
            OLD.CUST = LOCAL.REF.P<1,CULR.OLD.CUST.ID>
            IF LEN(BR) < 2 THEN
                BR.F = '0':BR
            END ELSE
                BR.F = BR
            END
**          BR.CUS = BR.F:KEY.LIST<I>
***Updated by Nessreen Ahmed 2/12/2018***********************************
******UDATED BY NESSREEN AHMED 26/6/2013*********************************
***         IF OLD.CUST = P.CUST THEN
***                BR.CUS = P.CUST
***            END ELSE
***                BR.CUS = BR.F:OLD.CUST
***            END
******* BR.CUS = P.CUST
*******END OF UPDATE 26/6/2013***************************************
******UPDATED BY NESSREEN AHMED 3/2/2020*******************************
******            IF LEN(P.CUST) < 8 THEN
******                BR.CUS = '0':P.CUST
******            END ELSE
******                BR.CUS = P.CUST
******            END
            IF CARD.PROD = 'MEEZA' THEN
                BR.CUS = P.CUST:'888'
                N.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":FST.ACT : " AND CARD.CODE IN (801 802 804 805) AND @ID LIKE ATMC.50780833... BY CARD.CODE"
                CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
                CALL OPF(FN.CARD.ISS,F.CARD.ISS)
                CALL F.READ(FN.CARD.ISS, KEY.LIST.N<1>, R.CARD.ISS, F.CARD.ISS, E2)
                LOCAL.REF.C = R.CARD.ISS<CARD.IS.LOCAL.REF>
                ATM.NO<I> = LOCAL.REF.C<1,LRCI.VISA.NO>
            END ELSE
                BR.CUS = P.CUST
                N.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":FST.ACT : " AND CARD.CODE IN (801 802 804 805) AND @ID UNLIKE ATMC.50780833... BY CARD.CODE"
                CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
                CALL OPF(FN.CARD.ISS,F.CARD.ISS)
                CALL F.READ(FN.CARD.ISS, KEY.LIST.N<1>, R.CARD.ISS, F.CARD.ISS, E2)
                LOCAL.REF.C = R.CARD.ISS<CARD.IS.LOCAL.REF>
                ATM.NO<I> = LOCAL.REF.C<1,LRCI.VISA.NO>
            END
******END OF UPDATE 3/2/2020********************************************
***End of Update 2/12/2018*********************************************
            TEXT = 'Primary=' :BR.CUS ; CALL REM
*********END OF UPDATE 5/11/2012***************************************
            ACC.1 = R.VISA.APP<SCB.VISA.ACC.N1>
            IF ACC.1 # '' THEN
*Line [ 264 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACC.1 , CATEG1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG1=R.ITSS.ACCOUNT<AC.CATEGORY>
*****UPDATED BY NESSREEN AHMED 22/10/2018************************
****UPDATED BY NESSREEN AHMED 18/9/2018********************
****        IF CATEG1 = 1001 THEN ACC.CURR = ACC.1
***         IF (CATEG1 = 1001) OR (CATEG1 = 1005) OR (CATEG1 = 1006) THEN ACC.CURR = ACC.1
                IF (CATEG1 = 1001) OR (CATEG1 = 1005) OR (CATEG1 = 1006) OR (CATEG1 = 1003) THEN ACC.CURR = ACC.1
****END OF UPDATE 17/9/2018*********************************
*****UPDATED BY NESSREEN AHMED 22/10/2018************************
****         IF CATEG1 = 1002 THEN ACC.SAL = ACC.1
                IF (CATEG1 = 1002) OR (CATEG1 = 1535) THEN ACC.SAL = ACC.1
*****UPDATED BY NESSREEN AHMED 26/1/2010*******************************************
****        IF (CATEG1 GE 6501) AND (CATEG1 LE 6504) THEN ACC.SAV = ACC.1
                IF (CATEG1 GE 6501) AND (CATEG1 LE 6512) THEN ACC.SAV = ACC.1
*****END OF UPDATE 26/1/2010*******************************************************
            END     ;**END OF ACC.1 #''
            ACC.2 = R.VISA.APP<SCB.VISA.ACC.N2>
            IF ACC.2 # '' THEN
*Line [ 287 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACC.2 , CATEG2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.2,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG2=R.ITSS.ACCOUNT<AC.CATEGORY>
****UPDATED BY NESSREEN AHMED 18/9/2018********************
*****UPDATED BY NESSREEN AHMED 22/10/2018************************
****        IF CATEG2 = 1001 THEN ACC.CURR = ACC.2
****        IF (CATEG1 = 1001) OR (CATEG1 = 1005) OR (CATEG1 = 1006) THEN  ACC.CURR = ACC.2
                IF (CATEG2 = 1001) OR (CATEG2 = 1005) OR (CATEG2 = 1006) OR (CATEG2 = 1003) THEN ACC.CURR = ACC.2
****END OF UPDATE 18/9/2018*********************************
*****UPDATED BY NESSREEN AHMED 22/10/2018************************
****         IF CATEG2 = 1002 THEN ACC.SAL = ACC.2
                IF (CATEG2 = 1002) OR (CATEG2 = 1535) THEN ACC.SAL = ACC.2
*****UPDATED BY NESSREEN AHMED 26/1/2010*******************************************
****        IF (CATEG2 GE 6501) AND (CATEG2 LE 6504) THEN ACC.SAV = ACC.2
                IF (CATEG2 GE 6501) AND (CATEG2 LE 6512) THEN ACC.SAV = ACC.2
*****END OF UPDATE 26/1/2010********************************************************
            END     ;**END OF ACC.2 #''
            ACC.3 = R.VISA.APP<SCB.VISA.AAC.N3>
            IF ACC.3 # '' THEN
*Line [ 310 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACC.3 , CATEG3)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.3,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG3=R.ITSS.ACCOUNT<AC.CATEGORY>
*****UPDATED BY NESSREEN AHMED 22/10/2018************************
****UPDATED BY NESSREEN AHMED 18/9/2018********************
****        IF CATEG3 = 1001 THEN ACC.CURR = ACC.3
****        IF (CATEG3 = 1001) OR (CATEG1 = 1005) OR (CATEG1 = 1006) THEN ACC.CURR = ACC.3
                IF (CATEG3 = 1001) OR (CATEG3 = 1005) OR (CATEG3 = 1006) OR (CATEG3 = 1003) THEN ACC.CURR = ACC.3
****END OF UPDATE 18/9/2018*********************************
*****UPDATED BY NESSREEN AHMED 22/10/2018************************
*****       IF CATEG3 = 1002 THEN ACC.SAL = ACC.3
                IF (CATEG3 = 1002) OR (CATEG3 = 1535) THEN ACC.SAL = ACC.3
******UPDATED BY NESSREEN AHMED 26/1/2010********************************************
****        IF (CATEG3 GE 6501) AND (CATEG3 LE 6504) THEN ACC.SAV = ACC.3
                IF (CATEG3 GE 6501) AND (CATEG3 LE 6512) THEN ACC.SAV = ACC.3
*****END OF UPDATE 26/1/2010*********************************************************
            END     ;**END OF ACC.3 # ''
            FAST.ACC = R.VISA.APP<SCB.VISA.FAST.ACC>
** TOT.LEN = (LEN(NAME1)+LEN(NAME2)+LEN(NAME3)+2)
            IF ACC.CURR # '' THEN
                ACC.CURR.F = ACC.CURR:STR(' ',28 - LEN(ACC.CURR)):'818'
            END ELSE
                ACC.CURR.F = STR(' ',28):STR(' ',3)
            END

            IF ACC.SAL # '' THEN
                ACC.SAL.F = ACC.SAL:STR(' ',28 - LEN(ACC.SAL)):'818'
            END ELSE
                ACC.SAL.F = STR(' ',28):STR(' ',3)
            END

            IF ACC.SAV # '' THEN
                ACC.SAV.F = ACC.SAV:STR(' ',28 - LEN(ACC.SAV)):'818'
            END ELSE
                ACC.SAV.F = STR(' ',28):STR(' ',3)
            END

***************FILLING BRIDGE******************************************************************************
            VISA.DATA = SER
            VISA.DATA := ATM.NO<I>:STR(' ',3)
            VISA.DATA := 'S'
            VISA.DATA := NAME1:STR(' ',40 - LEN(NAME1))
            VISA.DATA := NAME2:STR(' ',40 - LEN(NAME2))
            VISA.DATA := NAME3:STR(' ',40 - LEN(NAME3))
            VISA.DATA := TITLE:STR(' ',55 - LEN(TITLE))
            VISA.DATA := M.STAT:STR(' ',10 - LEN(M.STAT))
            VISA.DATA := GEN:STR(" ",10 - LEN(GEN))
            VISA.DATA := ID.NO:STR(" ",15 - LEN(ID.NO))
            VISA.DATA := ID.TYPE:STR(" ",115 - LEN(ID.TYPE))
***Updated By Nessreen Ahmed 29/11/2018************************************************
***         VISA.DATA := TELPHONE:STR(" ",68 - LEN(TELPHONE))
            VISA.DATA := STR(" ",14)
            VISA.DATA := MOB.NO:STR(" ",14 - LEN(MOB.NO))
            VISA.DATA := STR(" ",40)
***End of Update  29/11/2018***********************************************************
            VISA.DATA := BIRTH.MON:STR(" ",8 - LEN(BIRTH.MON))
            VISA.DATA := JOB.ADD:STR(" ",100 - LEN(JOB.ADD))
            VISA.DATA := JOB.DESC:STR(" ",484 - LEN(JOB.DESC))
***Updated By Nessreen Ahmed 8/4/2019***************************************************
***         VISA.DATA := STR(" ",150)
            STAT.ADD = JOB.ADD
            VISA.DATA := STAT.ADD:STR(" ",150 - LEN(STAT.ADD))
            VISA.DATA := BR.CUS:STR(" ",15 - LEN(BR.CUS))
***End of Update 8/4/2019***************************************************************
*********UPDATED ON 1/12/2008********************************************
**VISA.DATA := NAME1:STR(" ",1):NAME2:STR(" ",1):NAME3:STR(" ",30 - TOT.LEN)
            VISA.DATA := FULL.NAME
**************************************************************************
****Updated by Nessreen Ahmed 29/11/2018*********************************
***         VISA.DATA := '4':STR(" ",3)
***         VISA.DATA := '11':STR(" ",2)

            VISA.DATA := CARD.TYPE:STR(" ",4 - LEN(CARD.TYPE))
            VISA.DATA := CARD.FEES :STR(" ",4 - LEN(CARD.FEES))
****end of Update 29/11/2018*********************************************
            VISA.DATA := '818'
            VISA.DATA := '0'
            VISA.DATA := '0'
            VISA.DATA := '0'
            VISA.DATA := STR(" ",19)
            VISA.DATA := ACC.SAV.F
            VISA.DATA := ACC.CURR.F
            VISA.DATA := ACC.SAL.F
            VISA.DATA := FAST.ACC:STR(" ",28 - LEN(FAST.ACC))
****Updated by Nessreen Ahmed 29/11/2018*********************************
***         VISA.DATA := '5'
            VISA.DATA := CARD.LIMIT:STR(" ",4 - LEN(CARD.LIMIT))
***end of Update 29/11/2018*********************************************

*  VISA.DATA := STR(" ",20)
****Updated by Nessreen Ahmed 2/12/2018*********************************
***       VISA.DATA := STR(" ",135)
            VISA.DATA := STR(" ",132)
****End of Update 2/12/2018*********************************
            DIM ZZ(SELECTED)
            ZZ(SELECTED) = VISA.DATA

            WRITESEQ ZZ(SELECTED) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':ZZ(SELECTED)
            END
*****TO WRITE THE FLAG************************************
***********UPDATED BY NESSREN 31/03/2009**********************************
**          CALL F.READ(FN.VISA.APP,  KEY.TO.USE, R.VISA.APP, F.VISA.APP, E1)
            CALL F.READ(FN.VISA.APP,  KEY.LIST<I>, R.VISA.APP, F.VISA.APP, E1)
            R.VISA.APP<SCB.VISA.SEND.STAT> = TODAY

****            WRITE  R.VISA.APP TO F.VISA.APP , KEY.LIST<I> ON ERROR
****                PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.VISA.APP
****            END

            CALL F.WRITE(FN.VISA.APP,KEY.LIST<I>,R.VISA.APP)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)
***********************************************************
        NEXT I
    END
***********************************************************************************************
***********************************************************************************************
    TIMEE = TIMEDATE()
    HHH = FIELD(TIMEE, ":", 1)
    MIN = FIELD(TIMEE,":", 2)
    PART3 = FIELD(TIMEE,":", 3)
    SEC = PART3[1,2]
    TIMEFMT = HHH:MIN:SEC
    XX = SELECTED+1
    DIM NN(XX)
    NN(XX) = 'FT'
    NN(XX) := TODAY
    NN(XX) := TIMEFMT
    NN(XX) := SELECTED:STR(" ",10 - LEN(SELECTED))
    AHHHH = NN(XX)
    WRITESEQ NN(XX) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':NN(XX)
    END
*************************************************************
    TEXT = '�� ����� �����' ; CALL REM
*************************************************************
    RETURN

END
