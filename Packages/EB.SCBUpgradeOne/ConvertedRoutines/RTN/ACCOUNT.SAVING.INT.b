* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACCOUNT.SAVING.INT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT


    T.SEL = "SELECT FBNK.GROUP.CREDIT.INT WITH @ID LIKE ":O.DATA:"..."
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR("GROUP.CREDIT.INT":@FM:IC.GCI.CR.INT.RATE,KEY.LIST<SELECTED>,XXX)
F.ITSS.GROUP.CREDIT.INT = 'F.GROUP.CREDIT.INT'
FN.F.ITSS.GROUP.CREDIT.INT = ''
CALL OPF(F.ITSS.GROUP.CREDIT.INT,FN.F.ITSS.GROUP.CREDIT.INT)
CALL F.READ(F.ITSS.GROUP.CREDIT.INT,KEY.LIST<SELECTED>,R.ITSS.GROUP.CREDIT.INT,FN.F.ITSS.GROUP.CREDIT.INT,ERROR.GROUP.CREDIT.INT)
XXX=R.ITSS.GROUP.CREDIT.INT<IC.GCI.CR.INT.RATE>

    O.DATA = "%":XXX

    RETURN
END
