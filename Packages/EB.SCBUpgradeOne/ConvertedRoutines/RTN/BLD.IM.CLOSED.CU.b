* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>248</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.IM.CLOSED.CU(ENQ)
*    PROGRAM BLD.IM.CLOSED.CU
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
*=============================================================*
    FN.IM  = 'F.IM.REFERENCE' ; F.IM = '' ; R.IM = ''
    CALL OPF(FN.IM,F.IM)

    FN.IMD  = 'F.IM.DOCUMENT.IMAGE' ; F.IMD = '' ; R.IMD = ''
    CALL OPF(FN.IMD,F.IMD)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ;  R.CU = ''
    CALL OPF (FN.CU,F.CU)

    I = 0
*====================================================================*
*    T.SEL = "SELECT F.IM.REFERENCE WITH @ID EQ 03205191 "
    T.SEL = "SELECT F.IM.REFERENCE "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        FOR IM.CU = 1 TO SELECTED
            CALL F.READ(FN.IM,KEY.LIST<IM.CU>,R.IM,F.IM,E.IM)
            LOOP
                REMOVE IMD.ID  FROM R.IM SETTING POS
            WHILE IMD.ID:POS
                CALL F.READ (FN.IMD,IMD.ID,R.IMD,F.IMD,E.IMD)
                IF NOT(E.IMD) THEN
                    WS.CUS.ID  = R.IMD<IM.DOC.IMAGE.REFERENCE>
                    WS.CO.CODE = R.IMD<IM.DOC.CO.CODE>
                    CALL F.READ (FN.CU,WS.CUS.ID,R.CU,F.CU,E.CU)
                    IF E.CU THEN
                        IF WS.CO.CODE EQ ID.COMPANY THEN
                            I++
                            ENQ<2,I> = '@ID'
                            ENQ<3,I> = 'EQ'
                            ENQ<4,I> = IMD.ID
                        END
                    END
                    IF NOT(E.CU) THEN
                        WS.POS.REST  = R.CU<EB.CUS.POSTING.RESTRICT>
                        WS.CO.BOOK   = R.CU< EB.CUS.COMPANY.BOOK>
                        IF WS.POS.REST GT 89 AND WS.CO.CODE EQ ID.COMPANY  THEN
                            I++
                            ENQ<2,I> = '@ID'
                            ENQ<3,I> = 'EQ'
                            ENQ<4,I> = IMD.ID
                        END
                    END
                END
            REPEAT
        NEXT IM.CU
    END
    IF I = 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'NO RECORDS'
    END
    RETURN 
******************************************************************************************
END
