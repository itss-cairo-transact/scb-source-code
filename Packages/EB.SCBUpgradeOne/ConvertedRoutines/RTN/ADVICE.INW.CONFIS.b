* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-262</Rating>
*-----------------------------------------------------------------------------
******* WAEL *******

    SUBROUTINE ADVICE.INW.CONFIS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)
F.ITSS.SCB.LG.CHARGE = 'F.SCB.LG.CHARGE'
FN.F.ITSS.SCB.LG.CHARGE = ''
CALL OPF(F.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE)
CALL F.READ(F.ITSS.SCB.LG.CHARGE,MYID,R.ITSS.SCB.LG.CHARGE,FN.F.ITSS.SCB.LG.CHARGE,ERROR.SCB.LG.CHARGE)
MYVER=R.ITSS.SCB.LG.CHARGE<SCB.LG.CH.DESCRIPTION>
    GOSUB INITIATE
    GOSUB BODY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.INW.CONFIS'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    IF ID.NEW = '' THEN
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
***************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
    LG.NO= SAM:"/": SAM1:"/":SAM2
***************
    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
    LG.CUSTOMER =R.LD<LD.CUSTOMER.ID>
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LG.CUSTOMER,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,LG.CUSTOMER,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    LG.CUST=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
*Line [ 104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
******************END OF UPDATED ********************************
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.DATE = LOCAL.REF<1,LDLR.ACTUAL.EXP.DATE>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    ISS.DATE = LOCAL.REF<1,LDLR.ISSUE.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    LG.V.DATE = R.LD<LD.VALUE.DATE>
    ISSUE.DATE =ISS.DATE[7,2]:"/":ISS.DATE[5,2]:"/":ISS.DATE[1,4]
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    TOT = R.LD<LD.AMOUNT>
    RETURN
*=================================================
BODY:
    PR.HD :="'L'":" ���� �������� ��������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):ISS.DATE[7,2]:SPACE(2):ISS.DATE[5,2]:SPACE(2):ISS.DATE[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������� �� ������ ������ �������� "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� �������� ��� � � ���" : LG.NO : " ����� " : TOT :" ":CRR

    PR.HD :="'L'":" ���� ": LG.CUST :" ."
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":TOT:" ":CRR : SPACE(10):"���� �������� �� ���� ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":TOT:SPACE(10):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
*    CALL WORDS.ARABIC(TOT,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
    IN.AMOUNT=TOT:'.00'
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*Line [ 151 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 153 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I

*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": ISSUE.DATE
    HEADING PR.HD

    RETURN
*=================================================
END
