* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>561</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUST.CLOSED.RNG.7(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*-----------------------------------------*
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'          ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.H  = 'FBNK.CUSTOMER$HIS'    ;  F.CUS.H = ''
    CALL OPF(FN.CUS.H,F.CUS.H)
*-----------------------------------------*
    GOSUB INTIAL
    RETURN
*-----------------------------------------*
INTIAL:
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',LW.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LW.DAY=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    DD = TODAY
    DAY.NUM  = ICONV(DD,'DW')
    TOD.DAY  = OCONV(DAY.NUM,'DW')

    DD2 = TODAY
    CALL CDT('',DD2,'-1W')
    DAY.NUM2 = ICONV(DD2,'DW')
    DAY.NUM3 = OCONV(DAY.NUM2,'DW')

    DAY.MONTH = LW.DAY
    CALL LAST.WDAY(DAY.MONTH)

    TTT1 = TODAY
    TTT2 = TTT1[1,6]:"01"
    CALL CDT('',TTT2,'-1W')

    LW.DAYW = LW.DAY
    WS.D = '-':DAY.NUM3:'C'
    CALL CDT('',LW.DAYW,WS.D)

    S.DATE = LW.DAYW
    E.DATE = LW.DAY

    IF DAY.NUM3 GT TOD.DAY OR TOD.DAY EQ 7 THEN
        GOSUB PROCESS
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = S.DATE :"*": E.DATE
    END
    RETURN
*-------------------------------------------*
PROCESS:
*-------
    N.FLAG = 0
    Z      = 2

    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT GE 90 )"
    T.SEL := " AND COMPANY.BOOK EQ ":COMP.ID
    T.SEL := " AND CURR.NO GT 1 AND TEXT UNLIKE BR... BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "NE"
        ENQ<4,1> = S.DATE :"*": E.DATE

        FOR II = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<II>,R.ACC,F.ACC,CUS.ER)
            CUS.ID  = KEY.LIST<II>
            CUR.NO  = R.ACC<EB.CUS.CURR.NO>
            RR = 1
            MM = CUR.NO - 1
            FOR NN = CUR.NO TO 1 STEP -1
                IF RR EQ 1 THEN
                    CUST.ID      = CUS.ID:";"
                    CUST.ID.NXT  = CUS.ID:";":MM
                END ELSE
                    CUST.ID      = CUS.ID:";":MM
                    MM           = MM -1
                    CUST.ID.NXT  = CUS.ID:";":MM
                END
                IF RR EQ 1 THEN
                    RR = 2
                    CALL F.READ(FN.ACC,CUS.ID,R.ACC.N,F.ACC,CUS.ER44)
                    CUS.DAT.2 = R.ACC.N<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.ACC.N<EB.CUS.POSTING.RESTRICT>
                END ELSE
                    CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                    CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                END

                CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                IF CUS.POST NE CUS.POST.NXT THEN
                    IF CUS.POST GE 90 THEN
                        IF CUS.DAT.2 GE S.DATE[3,6] AND CUS.DAT.2 LE E.DATE[3,6] THEN
                            N.FLAG   = 1
                            ENQ<2,Z> = "@ID"
                            ENQ<3,Z> = "EQ"
                            ENQ<4,Z> = KEY.LIST<II>
                            Z        = Z + 1
                            NN       = 1
                        END
                    END
                END
            NEXT NN
        NEXT II

        IF Z EQ 2 THEN
            ENQ<2,Z> = "@ID"
            ENQ<3,Z> = "EQ"
            ENQ<4,Z> = S.DATE :"*": E.DATE
        END
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = S.DATE :"*": E.DATE
    END
*----------------------------------------------------------------------*
    RETURN
END
