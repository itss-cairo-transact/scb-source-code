* @ValidationCode : Mjo5Mzk0Mzg5MzY6Q3AxMjUyOjE2NDA2ODMxODA3ODc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:19:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>587</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE AUTH.SEND.TSK.SMS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.HOLD.CONTROL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Comment $INCLUDE I_F.SCB.PROJECT.IMP.STATUS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.PROJECT.IMP.STATUS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.USER.CONTACTS


    OPENSEQ "SMS" , "SMS.TEST" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE SMS SMS.TEST'
        HUSH OFF
    END

    OPENSEQ "SMS" , "SMS.TEST" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SMS.TEST CREATED IN SMS'
        END
        ELSE
            STOP 'Cannot create SMS.TEST File IN SMS'
        END
    END
************************
    TOO = "" ; TO.PERSON = "" ; RESP = ""
    TO.PERSON = R.NEW(WN.TASK.LEADER)<1,1>
    RESP      = R.NEW(WN.RESPONSIBILITY)<1,1>

*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.USER.CONTACTS': @FM:CONTACT.MOBILE,TO.PERSON,TOO)
F.ITSS.SCB.USER.CONTACTS = 'F.SCB.USER.CONTACTS'
FN.F.ITSS.SCB.USER.CONTACTS = ''
CALL OPF(F.ITSS.SCB.USER.CONTACTS,FN.F.ITSS.SCB.USER.CONTACTS)
CALL F.READ(F.ITSS.SCB.USER.CONTACTS,TO.PERSON,R.ITSS.SCB.USER.CONTACTS,FN.F.ITSS.SCB.USER.CONTACTS,ERROR.SCB.USER.CONTACTS)
TOO=R.ITSS.SCB.USER.CONTACTS<CONTACT.MOBILE>
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER': @FM:EB.USE.SIGN.ON.NAME,RESP,XX.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,RESP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
XX.NAME=R.ITSS.USER<EB.USE.SIGN.ON.NAME>

    TSK.ID    = R.NEW(WN.TASK.ID)<1,1>

    X1   = " The Task With ID  = ":TSK.ID :" was authorized by ": XX.NAME : " BR scb."
    BB.DATA = ''

    BB.DATA  = TOO :'|' : X1

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**********************

RETURN
END
