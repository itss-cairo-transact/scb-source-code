* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
***********RIHAM YOUSSEF 30/10/2014 ****************

    SUBROUTINE ADVICE.CANCEL.CREDIT
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB CALLDB
    MYID       = MYCODE:'.':MYTYPE
    IF MYCODE = "1301" OR MYCODE = "1302" OR MYCODE = "1303" THEN
        GOSUB INITIATE
        GOSUB BODY2
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "ADVICE CREATED SUCCESFULLY" ; CALL REM
    END
    RETURN
*===============================
INITIATE:
    REPORT.ID = 'ADVICE.CANCEL.CREDIT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:
    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF        = R.LD<LD.LOCAL.REF>
    BENF1            = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2            = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    LG.NO            = LOCAL.REF<1,LDLR.OLD.NO>
    AC.NO            = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    THIRD.NO         = LOCAL.REF<1,LDLR.THIRD.NUMBER>
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME1      = LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2      = LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1      = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2      = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************

*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
******************END OF UPDATED ********************************
    YYBRN            = FIELD(BRANCH,'.',2)
    DATY             = TODAY
    LG.TYPE          = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME          = TYPE.NAME
    LG.AMT           = R.LD<LD.AMOUNT>
    CUR              = R.LD<LD.CURRENCY>
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.DATE          = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE         = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    MARG.AMT         = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1          = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2          = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER         = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE         = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE     = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
    VAL.D       = LOCAL.REF<1,LDLR.INSTAL.DUE.DATE>

    INPUTTER         = R.LD<LD.INPUTTER>
    INP              = FIELD(INPUTTER,'_',2)
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF = COMI
    END
    LG.CO= R.LD<LD.CO.CODE>
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
*--------------------------------------------
    TOT = MARG.AMT +LG.COM1+LG.COM2
    RETURN
*=================================================
BODY2:
    PR.HD ="'L'":SPACE(6):"����� �����"
     PR.HD :="'L'":"���� �       :":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������      :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������   :":AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� ����   :":VAL.D[7,2]:"/":VAL.D[5,2]:"/":VAL.D[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������      :":CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������  :":THIRD.NAME1
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(15) :THIRD.NAME2
    END ELSE
        PR.HD :="'L'":" "
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������      :":THIRD.ADDR1
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ������ ���� ����� ����� �������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(2):SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(3): MARG.AMT:SPACE(21):" ����� ������� ������ �� �.� ��� � �":LG.NO
    PR.HD :="'L'":SPACE(27): " ������ ��� ������� � ����� "
    PR.HD :="'L'":SPACE(28):BENF1
    PR.HD :="'L'":SPACE(28):BENF2
    PR.HD :="'L'":SPACE(27):" �����  ": "/":LG.AMT:CRR
    PR.HD :="'L'":SPACE(27):"����� ������ � �� �������� �� ���������� "
    PR.HD :="'L'":SPACE(27):"������ ����� "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":MARG.AMT:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    IN.AMOUNT=MARG.AMT
    IF IN.AMOUNT EQ 0 THEN
        OUT.AMOUNT = 0
    END
    IF IN.AMOUNT NE 0 THEN
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*Line [ 176 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 178 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
            IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
                PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
            END ELSE
                PR.HD :="'L'":OUT.AMOUNT<1,I>
            END
        NEXT I
    END ELSE  
              PR.HD :="'L'":""
      END
    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":SPACE(6):INP:SPACE(12):AUTH:SPACE(12):REF
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.CANCEL.CREDIT"
    CALL LG.ADD(AUTH,LG.CO)
    RETURN
*=================================================
END
