* @ValidationCode : MjoxNDc4MzIxOTI5OkNwMTI1MjoxNjQwNjgzNjQ5NTQ3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:27:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BILL.ACC.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Comment $INCLUDE I_F.SCB.BR.SLIPS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.BR.SLIPS

*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>

    IF ( CATEG GT 9000 AND CATEG LT 9999) THEN
        E = '��� ����� ��������� ��������' ; CALL ERR  ; MESSAGE = 'REPEAT'
    END

    IF COMI THEN
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,COMI,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>

        IF CUS.ID NE '' THEN
            FF = R.NEW(SCB.BS.DRAWER)

            X = COMI[1,1]

            IF X EQ '0' THEN

                Y = COMI[2,7]

                IF Y NE FF THEN
                    ETEXT = "��� ������ ��� ����� ���� ������" ; CALL STORE.END.ERROR
                END
            END ELSE
                Y = COMI[1,8]
                IF Y NE FF THEN
                    ETEXT =  "��� ������ ��� ����� ���� ������ " ; CALL STORE.END.ERROR
                END
            END

        END

        ACC      = COMI
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC,CUR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR,CUR1)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR1=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
        CURRR    = R.NEW(SCB.BS.CURRENCY)
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CURRR,CUR2)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURRR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR2=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

** IF CUR1 NE CUR2 THEN
**   E = '������ ������' ; CALL ERR  ; MESSAGE = 'REPEAT'
**  END


    END


RETURN
END
