* @ValidationCode : MjotMTg3Nzc3NjQ5NzpDcDEyNTI6MTY0MDY4MjY3MzQ2MDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:11:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
****NESSREEN AHMED 02/05/2011***************
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ATM.CARDS.REQUEST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.READ.DB.ADVICE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CI.LOCAL.REFS

    DIR.NAME = '&SAVEDLISTS&'

    NEW.FILE = "ATM_200_LVE_CARDS"

*****1ST FILE OPEN*********************************************
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
************************************************************
    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    S.SEL = "SELECT FBNK.CARD.ISSUE WITH CARD.CODE GE 801 AND CANCELLATION.DATE EQ '' AND CO.CODE EQ EG0010013 AND FAST.ACCT.NO NE '' "
    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)
    TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM
    IF SELECTED.SS THEN
        SS = 200
        DIM VISA.DATA(SS)
        FOR I = 1 TO SS
            CR.CARD.NO = '' ; EXT.ACC.NEW = '' ; BR.CODE = '' ; INT.ACC = ''
            CALL F.READ(FN.CARD.ISSUE,KEY.LIST.SS<I>, R.CARD.ISSUE, F.CARD.ISSUE ,E3)
            LOCAL.REF      = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
            CR.CARD.NO<I>  = LOCAL.REF<1,LRCI.VISA.NO>
            FAST.ACCT<I>   = LOCAL.REF<1,LRCI.FAST.ACCT.NO>
            EXT.ACC.NEW<I> = R.CARD.ISSUE<CARD.IS.ACCOUNT>
            BR.CODE<I>     = R.CARD.ISSUE<CARD.IS.CO.CODE>

            VISA.DATA(I)  = CR.CARD.NO<I>
            VISA.DATA(I) := "|"
            VISA.DATA(I) := FAST.ACCT<I>

            WRITESEQ VISA.DATA(I) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':VISA.DATA(I)
            END
        NEXT I
    END
RETURN
END
