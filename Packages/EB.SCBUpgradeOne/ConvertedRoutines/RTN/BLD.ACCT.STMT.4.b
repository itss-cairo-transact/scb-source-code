* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.ACCT.STMT.4(ENQ)
****    PROGRAM    BLD.ACCT.STMT.4
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    COMP = ID.COMPANY

*****    COMP = 'EG0010010'

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    Y.CU.ID = '' ; R.CU = '' ; ERR.CU = ''
    CALL OPF(FN.CU,F.CU)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CREDIT.CODE = ""

    T.SEL.CU = "SELECT ":FN.CU:" WITH CREDIT.CODE EQ 120 AND COMPANY.BOOK EQ ":COMP
    CALL EB.READLIST(T.SEL.CU,KEY.LIST.CU,"",SELECTED.CU,ER.MSG.CU)

    LOOP
        REMOVE Y.CU.ID FROM KEY.LIST.CU SETTING POS.LD
    WHILE Y.CU.ID:POS.LD
        CALL F.READ(FN.CU,Y.CU.ID,R.CU,F.CU,ERR.CU)


        T.SEL = "SELECT ":FN.AC:" WITH CUSTOMER EQ ":Y.CU.ID:" AND (CATEGORY GE 1220 OR CATEGORY LE 1227) AND CO.CODE EQ ":COMP

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)


        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
                INT.AC = R.AC<AC.INTEREST.COMP.ACCT>
                CMP.AC = R.AC<AC.CO.CODE>
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,INT.AC,AMT1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,INT.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AMT1=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,KEY.LIST<I>,AMT2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,KEY.LIST<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AMT2=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
                AMT = AMT1 + AMT2
*=================================================================
                IF CMP.AC EQ COMP THEN
                    IF AMT NE '' AND AMT NE '0' THEN
                        ENQ<2,I> = '@ID'
                        ENQ<3,I> = 'EQ'
                        ENQ<4,I> = KEY.LIST<I>
                    END ELSE
                        ENQ<2,I> = '@ID'
                        ENQ<3,I> = 'EQ'
                        ENQ<4,I> = 'DUMMY'
                    END
                END
            NEXT I
        END  ELSE
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = 'DUMMY'
        END
    REPEAT
    RETURN
END
