* @ValidationCode : MjozNDI1NDY5NjI6Q3AxMjUyOjE2NDA2MTg4MTEwNTU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 17:26:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ACCOUNT.BRANCH.ALL.ATM


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Comment $INCLUDE I_F.SCB.VISA.TRANS.TOT - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.VISA.TRANS.TOT
*Line [ 34 ] Comment $INCLUDE I_F.SCB.MAST.TRANS - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_F.SCB.MAST.TRANS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
    AC = O.DATA

    WORK.BALANCE   = 0
    AVL.BALANCE    = 0
    AVAL.NO        = 0
    BAL.AVAL       = 0

    FN.AC = 'FBNK.ACCOUNT' ; R.AC = ''      ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    CALL F.READ(FN.AC,AC,R.AC,F.AC,E1)
    CUST.ID        = R.AC<AC.CUSTOMER>
*    CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,CUS.SECTOR)
    WORK.BALANCE   = R.AC<AC.WORKING.BALANCE>

*IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    LOAN = LOC.REF<1,CULR.LOAN.TYPE>
    POS = DCOUNT(LOAN,@SM)
    IF POS GT 0 THEN
        FOR X = 1 TO POS
            LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>
*            IF LOAN.TYPE EQ '103' OR LOAN.TYPE EQ '104' OR LOAN.TYPE EQ '105' OR LOAN.TYPE EQ '109' THEN
            IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                X = POS
                AVL.BALANCE    =  R.AC<AC.AVAILABLE.BAL>
                AVAL.NO        = DCOUNT(AVL.BALANCE,@VM)
                BAL.AVAL       = AVL.BALANCE<1,AVAL.NO>
                IF BAL.AVAL = '' OR BAL.AVAL = 0 THEN BAL.AVAL = WORK.BALANCE

                IF BAL.AVAL LT WORK.BALANCE THEN
                    O.DATA = BAL.AVAL
                END ELSE
                    O.DATA = WORK.BALANCE
                END
            END ELSE
                O.DATA =  WORK.BALANCE
            END

        NEXT X
    END ELSE
        O.DATA =  WORK.BALANCE
    END

    IF O.DATA EQ '' THEN
        O.DATA = 0.00
    END ELSE
        O.DATA = O.DATA
    END
RETURN
END
