* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.BILL.REG.LW(ENQ.DATA)
* UPDATED BY M.ELSAYED <27/1/2009>
* UPDATED BY HARES.M.M <11/09/2011>

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY


    FN.BILL.REG = "FBNK.BILL.REGISTER"
    F.BILL.REG = ''
    R.BILL.REG=''
    Y.BILL.REG.ID=''
    Y.BILL.REG.ERR=''
    K = 0

    TTD = TODAY

    CALL CDT('',TTD,'-1W')

    CALL OPF(FN.BILL.REG,F.BILL.REG)
    SEL.CMD ="SSELECT ":FN.BILL.REG:" WITH BIL.CHQ.TYPE EQ 10 AND BILL.CHQ.STA EQ 15 AND STATUS.DATE EQ ":TTD:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)

    IF NOREC >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
    RETURN
END
