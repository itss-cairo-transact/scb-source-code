* @ValidationCode : Mjo5MDQ2ODQ0Mzg6Q3AxMjUyOjE2NDA2ODMyMjQ3Mzc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:20:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BANK.WORK(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Comment $INCLUDE I_F.SCB.LG.OUR.BANK - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.LG.OUR.BANK

    FN.CUS='F.SCB.LG.OUR.BANK'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
* CUS.IND = R.CUS<EB.CUS.INDUSTRY>
* T.SEL="SELECT FBNK.CUSTOMER WITH INDUSTRY LT 999 AND INDUSTRY LIKE ...00":CUS.IND
    TD = TODAY
    CALL CDT('', TD, '+30C')
    TEXT = TD ; CALL REM
    T.SEL="SELECT F.SCB.LG.OUR.BANK WITH LG.EXPIRY.DATE LE " : TD

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
* CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
*TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
RETURN
END
