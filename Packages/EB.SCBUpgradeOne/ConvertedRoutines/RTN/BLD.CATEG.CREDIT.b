* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.CATEG.CREDIT(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

    COMP = ID.COMPANY
*----------------------------------------------------------------------**
    FN.ACC   = 'F.CATEGORY' ; F.ACC    = ''
    CALL OPF(FN.ACC,F.ACC)
*-------------------------------------------*
    T.SEL = "SELECT F.CATEGORY WITH DESCRIPTION UNLIKE RE... AND DESCRIPTION UNLIKE re... AND @ID NE 9090 AND ( @ID GE 1000 AND @ID LT 1100 ) OR ( @ID GT 1599 AND @ID LE 6799 ) BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

***--------------------------------------------------------------****
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            ENQ<2,II> = "@ID"
            ENQ<3,II> = "EQ"
            ENQ<4,II> = KEY.LIST<II>
        NEXT II
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END
    RETURN
*================================================================****
END
