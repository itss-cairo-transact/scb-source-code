* @ValidationCode : MjotMjA4MTczODc4NzpDcDEyNTI6MTY0MDY4MjM1NDM5MDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:05:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-39</Rating>
*-----------------------------------------------------------------------------
***NESSREEN AHMED 28/01/2015*********

SUBROUTINE ATM.CARD.ACTIVATION

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CI.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.APP

* A ROUTINE TO CREATE A TEXT FILE CONTANING ALL ACTIVATED CARDS THROUGH THE DAY

    DIR.NAME = '&SAVEDLISTS&'
    TIMEE = TIMEDATE()
    HHH = FIELD(TIMEE, ":", 1)
    MIN = FIELD(TIMEE,":", 2)
    TIMEFMT = HHH:MIN:SEC

    NEW.FILE = "Debit_Update_state" :TODAY :TIMEFMT
    TEXT = 'F=':NEW.FILE ; CALL REM

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END
    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
**********************FILLING DATA********************************
******************************************************************
    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; E1 = '' ; RETRY1= ''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    YTEXT = "���� ������� �������:"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI

    YTEXT = "���� ����� �������:"
    CALL TXTINP(YTEXT, 8, 22, "2", "A")
    ST.TIME = COMI

**  T.SEL =  "SELECT FBNK.CARD.ISSUE WITH CARD.STATUS EQ 80 AND VISA.RECV.DATE EQ ": TODAY
****UPDATED BY NESSREEN AHMED 11/6/2015 ****************************
****T.SEL =  "SELECT FBNK.CARD.ISSUE WITH CARD.STATUS EQ 80 AND VISA.RECV.DATE EQ ": ST.DATE
    SEL.DATE = ST.DATE[3,6]
    IF LEN(ST.TIME) < 2 THEN
        STIME = '0':ST.TIME
    END ELSE
        STIME = ST.TIME
    END
    FMT.TIME = ST.TIME:'00'
    DATETIME = SEL.DATE:FMT.TIME
    TEXT = '����� � ����� ��=' :DATETIME ; CALL REM
    T.SEL =  "SELECT FBNK.CARD.ISSUE WITH CARD.STATUS EQ 80 AND DATE.TIME GE ":DATETIME : " BY DATE.TIME "
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = '��� �������=':SELECTED ; CALL REM
******HEADER***************************************************************************
    DIM HH(1)
    HH(1) = 'FH'
    HH(1) := 'Update Card Data                                          '
    HH(1) := 'Suez Canal Bank               '

    WRITESEQ HH(1) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':HH(1)
    END

***************************************************************************************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            KEY.USE = KEY.LIST<I>
*            TEXT = 'KEY=':KEY.USE ; CALL REM
            CALL F.READ(FN.CARD.ISSUE,  KEY.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
            LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>

            CARD.NO<I> = LOCAL.REF<1,LRCI.VISA.NO>
***************FILLING BRIDGE******************************************************************************
            SER = I
            VISA.DATA = SER:STR(' ',10 - LEN(SER))
            VISA.DATA := CARD.NO<I>:STR(' ',3)
            VISA.DATA := '1'
            VISA.DATA := STR(' ',15)
            VISA.DATA := STR(' ',15)
            VISA.DATA := STR(' ',15)
            VISA.DATA := STR(' ',5)
            VISA.DATA := STR(' ',5)
            VISA.DATA := STR(' ',28):'818'
            VISA.DATA := STR(' ',28):'818'
            VISA.DATA := STR(' ',28):'818'
            VISA.DATA := STR(' ',28)
            VISA.DATA := '0'
            VISA.DATA := '0'
            VISA.DATA := '0'
            VISA.DATA := '  11'
            VISA.DATA := STR(' ',37)

            DIM ZZ(SELECTED)
            ZZ(SELECTED) = VISA.DATA

            WRITESEQ ZZ(SELECTED) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':ZZ(SELECTED)
            END
*****TO WRITE THE FLAG************************************
***********UPDATED BY NESSREN 31/03/2009**********************************
**          CALL F.READ(FN.VISA.APP,  KEY.TO.USE, R.VISA.APP, F.VISA.APP, E1)
**          CALL F.READ(FN.VISA.APP,  KEY.LIST<I>, R.VISA.APP, F.VISA.APP, E1)
**            R.VISA.APP<SCB.VISA.SEND.STAT> = TODAY
**            WRITE  R.VISA.APP TO F.VISA.APP , KEY.LIST<I> ON ERROR
**                PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.VISA.APP
**            END
***********************************************************
        NEXT I
    END
***********************************************************************************************
***********************************************************************************************
    TIMEE = TIMEDATE()
    HHH = FIELD(TIMEE, ":", 1)
    MIN = FIELD(TIMEE,":", 2)
    PART3 = FIELD(TIMEE,":", 3)
    SEC = PART3[1,2]
    TIMEFMT = HHH:MIN:SEC
    XX = SELECTED+1
    DIM NN(XX)
    NN(XX) = 'FT'
    NN(XX) := TODAY
    NN(XX) := TIMEFMT
    NN(XX) := SELECTED:STR(" ",10 - LEN(SELECTED))
    AHHHH = NN(XX)
    WRITESEQ NN(XX) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':NN(XX)
    END
*************************************************************
    TEXT = '�� ����� �����' ; CALL REM
*************************************************************
RETURN

END
