* @ValidationCode : MjotMTczNjg0NzAxMTpDcDEyNTI6MTY0MTczNzEwNDU2MTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 16:05:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE ADVICE.AMAMTDATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE
    $INCLUDE I_F.COMPANY

    GOSUB CALLDB
    IF MYCODE EQ '1233' OR MYCODE EQ '1235' THEN
        GOSUB INITIATE
        GOSUB BODY2
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END ELSE
        IF MYCODE EQ '1232' OR MYCODE EQ '1234' OR MYCODE EQ '1271' THEN
            GOSUB INITIATE
            GOSUB BODY
            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
        END
    END
    TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.AMAMTDATE'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
***    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NUM = R.LD<LD.CHRG.LIQ.ACCT>
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
** THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
    THIRD.NO=R.LD<LD.CUSTOMER.ID>
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME1=LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2=LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************************
**    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
**    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)

*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,CO.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CO.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.BOOK,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.BOOK,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YYBRN = FIELD(BRANCH,'.',2)
*******************************************
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 121 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    LG.AMT.INC =R.LD<LD.AMOUNT.INCREASE>
    LG.INC.TOT = LG.AMT + LG.AMT.INC
    CUR=R.LD<LD.CURRENCY>
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.DATE = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE = LG.DATE[3,2]:"/":LG.DATE[5,2]:"/":LG.DATE[0,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1 = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    APP.DATE = LOCAL.REF<1,LDLR.APPROVAL.DATE>
    OLDNO    = LOCAL.REF<1,LDLR.OLD.NO>
    TEXT = "APP.DATE : " : APP.DATE ; CALL REM
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
*APP.DATE     = APP.DATE[7,2]:"/":APP.DATE[5,2]:"/":APP.DATE[1,4]
    APP.DATE     = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    AMT.INC   = R.LD<LD.AMOUNT.INCREASE>
    AMT.TOT = (MARG.PER * AMT.INC)/100
    LG.V.DATE = R.LD<LD.VALUE.DATE>
    LG.VALUE.DATE = LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]

*LG.VALUE.DATE = LG.V.DATE[3,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[0,4]
    CONFIS.AMT = LOCAL.REF<1,LDLR.CONFISC.AMT>
    DIFFS = R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
***  DIFFS = (MARG.PER * AMT.INC) / 100
    DIFF  = ABS(DIFFS)
*********************************************************
    LG.STAMP = '2.90'

    INPUTTER    = R.LD<LD.INPUTTER>
    INP         = FIELD(INPUTTER,'_',2)
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
*    FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
*   CALL OPF(FN.LG,F.LG)
*   CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)

**MYTAX = DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>, SM)
**FOR I = 1 TO MYTAX
**    TOT.TAX += R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES,I>
** NEXT I

*MYTAX1 = R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>, SM)
*MYTAX1 = R.LG<SCB.LG.CH.TAX.AMOUNT,1>
*MYTAX2  = R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>, SM)
*MYTAX2 = R.LG<SCB.LG.CH.TAX.AMOUNT,2>
*TOT.TAX = MYTAX1 + MYTAX2
*--------------------------------------------
    TOT = DIFF + LG.COM1
*TEXT = DIFF ; CALL REM
**************************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF =COMI
    END
    LG.CO= R.LD<LD.CO.CODE>
RETURN
*=================================================
BODY2:

    PR.HD ="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    XX = SPACE(80)
    XX<1,1>[46,20] = "����� ����� "
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:XX<1,1>
    END ELSE
        PR.HD :="'L'":SPACE(2):""
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
    IF THIRD.ADDR2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    END ELSE
        PR.HD :="'L'":SPACE(2):""
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ������ ���� ����� ����� �������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(3): DIFF :SPACE(21):  "���� ����� ���� ������� ����� " :"%" :MARG.PER

****    PR.HD :="'L'":SPACE(3):LG.COM1:SPACE(21): " ����� ����� ���� �������"
****    PR.HD :="'L'":SPACE(3):LG.COM2:SPACE(21):  "���� � ������� ������ "

    IF CUR EQ 'EGP' THEN
        PR.HD :="'L'":SPACE(3):LG.STAMP:SPACE(21):"��� ���� "
    END ELSE
        IF CUR NE 'EGP' THEN
            PR.HD :="'L'":""
        END
    END

    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
*   DIFF2 = DIFF + LG.COM1 + LG.COM2 + TOT.TAX
    DIFF2 = DIFF + LG.COM1 + LG.COM2 + TOT.TAX + LG.STAMP
    PR.HD :="'L'":DIFF2:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":CRR
*---------------------------
    CALL WORDS.ARABIC(DIFF2,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 223 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)

*Line [ 226 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": APP.DATE

    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):REF
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.AMAMTDATE.ADD"
    CALL LG.ADD(AUTH,LG.CO)
RETURN
*===================================================
BODY:
    PR.HD ="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    XX = SPACE(80)
    XX<1,1>[46,20] = "����� ��� "
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:XX<1,1>
    END ELSE
        PR.HD :="'L'":SPACE(2):""
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
    IF THIRD.ADDR2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    END ELSE
        PR.HD :="'L'":SPACE(2):""
    END
    PR.HD :="'L'":CRR
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
*********************UP NI7OOOOOOOOOOOO(20101031)***************
    PR.HD :="'L'":" ��� �.� ��� ":OLDNO:"(":LG.NAME:")":"����� ���� ����� ����� " :AMT.INC
    PR.HD :="'L'":" ����� ���� ������� ����� " :  LG.INC.TOT
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'":" "
    IF MYCODE EQ '1271'  THEN
        PR.HD :="'L'":SPACE(1):LG.COM1:SPACE(21):"����� �����"
    END ELSE
        PR.HD :="'L'":SPACE(1):DIFF:SPACE(21): "���� ����� ���� ������� ����� " :"%":MARG.PER
* PR.HD :="'L'":SPACE(1):LG.COM1:SPACE(21):  " ����� ����� ���� �������"
******************UP NI7OOOO (20101031)**************************
        PR.HD :="'L'":SPACE(1):LG.COM1:SPACE(21):  "������� �� ������ �������� �� ":END.COM.DATE
        PR.HD :="'L'":SPACE(1):LG.COM2:SPACE(21): "���� � ������� ������ "

        IF CUR EQ 'EGP' THEN
            PR.HD :="'L'":SPACE(1):LG.STAMP:SPACE(21): "��� ������"
        END  ELSE
            IF CUR NE 'EGP' THEN
                PR.HD :="'L'":""
            END
        END

    END
** IF TOT.TAX # '' THEN
**   PR.HD :="'L'":SPACE(3):TOT.TAX:SPACE(21):"��� ���� "
**  END
    TOT.TAX=''
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    DIFF2 = DIFF + LG.COM1 + LG.COM2 + TOT.TAX + LG.STAMP
    PR.HD :="'L'":DIFF2:SPACE(15):" ������������������������"
****  PR.HD :="'L'":30:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
    CALL WORDS.ARABIC(DIFF2,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 312 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)

*Line [ 315 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
*---------------------------
    PR.HD :="'L'":" "
**************    PR.HD :="'L'":" ����� ����  ":" : ": LG.VALUE.DATE
    PR.HD :="'L'":" ����� ����  ":" : ": APP.DATE
    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):REF
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.AMAMTDATE.DEDUCT"
    CALL LG.ADD(AUTH,LG.CO)
RETURN
*===================================================
END
