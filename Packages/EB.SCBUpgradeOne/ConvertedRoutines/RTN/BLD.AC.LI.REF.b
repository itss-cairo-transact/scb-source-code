* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.AC.LI.REF(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    COMP = ID.COMPANY
    WS.COUNT = 0
    FN.AC      = "FBNK.ACCOUNT"                    ; F.AC      = ""  ; R.AC     = ""
    CALL OPF (FN.AC,F.AC)

    SEL.CMD ="SELECT " :FN.AC:" WITH LIMIT.REF EQ '' AND CATEGORY GE 1100 AND CATEGORY LE 1599 AND CO.CODE EQ ":COMP:" WITHOUT CATEGORY IN ( 1220 1221 1222 1223 1224 1225 1226 1227 ) BY CUSTOMER"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,RTNCD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,KEY.LIST<I>,WS.CUSTOMER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,KEY.LIST<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WS.CUSTOMER=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUSTOMER,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CUSTOMER,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF ( WS.CREDIT.CODE NE 110 AND WS.CREDIT.CODE NE 120 ) THEN
                WS.COUNT ++
                ENQ.DATA<2,WS.COUNT> = "@ID"
                ENQ.DATA<3,WS.COUNT> = "EQ"
                ENQ.DATA<4,WS.COUNT> = KEY.LIST<I>
            END
        NEXT I

    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    IF WS.COUNT EQ 0 THEN
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
    RETURN
