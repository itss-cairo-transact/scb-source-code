* @ValidationCode : Mjo5MjIxNzc0MTg6Q3AxMjUyOjE2NDA3MDIzMzI3MzY6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 16:38:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BLD.FT.LIST(ENQ.DATA)
*Line [ 20 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 22 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 24 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 26 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*Line [ 28 ] Removed T24.BP - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*--------------------------------------------
    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT  = ""
    CALL OPF(FN.FT,F.FT)
    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF(FN.AC,F.AC)
    INP = ""
*------------------------------------------------
    LOCATE "INPUTTER" IN ENQ.DATA<2,1> SETTING INP.USER THEN
        INP  = ENQ.DATA<4,INP.USER>
    END

    SEL.CMD  = "SELECT FBNK.FUNDS.TRANSFER WITH CO.CODE EQ ":ID.COMPANY
    SEL.CMD := " AND ( VERSION.NAME EQ ',SCB.AC5'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.AC444'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.AC.INT'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.INTNAL.AC.ETMDAT'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.DRFT.ABO'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.CRT.CCY'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.AC.CURR'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.AC.FCY'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.DRFT.ABO.FCY.HH'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.AC.NOS.CORRS.CHG'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.CRT.FCY'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.AC.CHQ'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.LOAN.AC.ETMDAT'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.TAXES'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.CUSTOMS'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.INS'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.RSF'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.1'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.KNGMAR'"
    SEL.CMD := " OR VERSION.NAME EQ ',SCB.7' )"
    SEL.CMD := " AND INPUTTER LIKE ...":INP:"..."
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    HH = 1
    IF NOREC THEN
        FOR I = 1 TO NOREC
            CALL F.READ(FN.FT,SELLIST<I>,R.FT,F.FT,E.FT)
            CALL F.READ(FN.AC,R.FT<FT.DEBIT.ACCT.NO>,R.AC,F.AC,E.AC)
            IF R.AC<AC.CUSTOMER> NE '' THEN
                ENQ.DATA<2,HH> = "@ID"
                ENQ.DATA<3,HH> = "EQ"
                ENQ.DATA<4,HH> = SELLIST<I>
                HH++
            END
        NEXT I
    END ELSE
        ENQ.DATA<2,2> = "@ID"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = "DUMMY"
    END
*-------------------------------------------------
RETURN
END
