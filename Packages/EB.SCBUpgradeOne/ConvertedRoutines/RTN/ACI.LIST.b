* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>36</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACI.LIST(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    COMP = ID.COMPANY

***************************************
***********
    FN.ACC= 'FBNK.ACCOUNT';F.ACC='';R.ACC= '';E1=''
    CALL OPF(FN.ACC,F.ACC)
*
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

*    T.SEL = "SELECT FBNK.ACCOUNT.DEBIT.INT WITH @ID LIKE " :'...122...' : " AND  INTEREST.DAY.BASIS NE 'NONE' BY @ID"
**    T.SEL = "SELECT FBNK.ACCOUNT.CREDIT.INT WITH INTEREST.DAY.BASIS NE 'NONE' AND ( @ID UNLIKE ...650101... AND  @ID UNLIKE ...65020... ) AND CR.INT.RATE NE 0  BY @ID "
*    T.SEL = "SELECT FBNK.ACCOUNT.CREDIT.INT WITH ( @ID UNLIKE ...650101... AND  @ID UNLIKE ...65020... ) AND CR.INT.RATE NE 0  BY @ID "
*MSABRY 27/1/2009
* T.SEL = "SELECT FBNK.ACCOUNT.CREDIT.INT WITH CO.CODE EQ ":COMP:" AND ( @ID UNLIKE ...650101... AND  @ID UNLIKE ...65020... ) AND CR.INT.RATE NE 0  BY @ID "
**********20131010
    T.SEL = "SELECT FBNK.ACCOUNT.CREDIT.INT WITH CO.CODE EQ ":COMP:" AND ( @ID UNLIKE ...650101... AND  @ID UNLIKE ...65020... AND  @ID UNLIKE ...65030... AND   @ID UNLIKE ...65040... ) AND CR.INT.RATE NE 0  BY @ID "

*******   T.SEL = "SELECT FBNK.ACCOUNT.CREDIT.INT WITH CO.CODE EQ ":COMP:"  BY @ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    Z = 1
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,FIELD(KEY.LIST<I>,'-',1),R.ACC,F.ACC,E1)
            IF NOT(E1) THEN
                ACC.NO  = FIELD(KEY.LIST<I>,'-',1)
                ACC.NO2 = FIELD(KEY.LIST<I+1>,'-',1)
                IF ACC.NO NE ACC.NO2 THEN
                    ENQ<2,Z> = "@ID"
                    ENQ<3,Z> = "EQ"
                    ENQ<4,Z> = KEY.LIST<I>
                    Z = Z+1
                END
            END
        NEXT I
    END ELSE
        ENQ<2,Z> = "@ID"
        ENQ<3,Z> = "EQ"
        ENQ<4,Z> = "NOLIST"
    END
    RETURN
END
