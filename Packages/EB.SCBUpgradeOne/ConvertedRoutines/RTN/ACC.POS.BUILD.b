* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACC.POS.BUILD(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT


    FN.CUS='FBNK.ACCOUNT'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
    FN.CUS.ACC ='FBNK.CUSTOMER.ACCOUNT'
    F.CUS.ACC  =''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    J = 0
    T.SEL="SELECT FBNK.ACCOUNT WITH CATEGORY EQ '1019' AND (ONLINE.ACTUAL.BAL NE 0 AND ONLINE.ACTUAL.BAL NE '') "
******* T.SEL="SELECT FBNK.ACCOUNT WITH @ID EQ 0120547710101901 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
****TEXT = SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR1)
            CUSID  = R.CUS<AC.CUSTOMER>
            BAL    = R.CUS<AC.ONLINE.ACTUAL.BAL>
****TEXT   = "CUS.ID l: " : CUSID ; CALL REM
***************         IF BAL NE 0 AND BAL NE '' THEN
            CALL F.READ(FN.CUS.ACC,CUSID,R.CUS.ACC,F.CUS.ACC,ERR1)
            LOOP
                REMOVE ACC.NO FROM R.CUS.ACC SETTING POS
            WHILE ACC.NO:POS
******************CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,ACC.NO,BAL)
                CALL F.READ(FN.CUS,ACC.NO,R.CUS,F.CUS,CUS.ERR1)
                BAL    = R.CUS<AC.ONLINE.ACTUAL.BAL>
                IF BAL NE 0 AND BAL NE '' THEN
****TEXT   = "ACC.NO : " : ACC.NO ; CALL REM
*********************
* FOR ENQ.LP = 1 TO COUNT.ACC
                    J++
*****TEXT = "J : " : J ; CALL REM
                    ENQ<2,J> = '@ID'
                    ENQ<3,J> = 'EQ'
                    ENQ<4,J> = ACC.NO
*******TEXT = "ACC.NO=  " : ACC.NO ; CALL REM
* NEXT ENQ.LP
                END
            REPEAT

        NEXT I
    END
**********************
    RETURN
END
