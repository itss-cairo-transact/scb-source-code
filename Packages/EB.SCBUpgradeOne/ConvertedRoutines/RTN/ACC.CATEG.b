* @ValidationCode : MjotMTI4NjkzMzQ1MjpDcDEyNTI6MTY0MDYxODY3MDE3Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Dec 2021 17:24:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE ACC.CATEG(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Comment $INCLUDE I_F.SCB.CUS.POS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CUS.POS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    TD = TODAY
    T.SEL = "SELECT F.SCB.CUS.POS WITH (CATEGORY IN (3200 1701 11382 11019 11218 11034 11214 11022 11216 11212 11035 11023 10000 2000 2001 11041 11052 11064 11054 11057 16110) OR (CATEGORY GE 50000 AND CATEGORY LE 50999) OR (CATEGORY GE 1220 AND CATEGORY LE 1227)) AND LCY.AMOUNT NE 0 AND SYS.DATE EQ ":TD:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*==============================================================
    IF SELECTED >= 1 THEN

        FOR KK = 1 TO SELECTED

            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,KK> = '@ID'
        ENQ<3,KK> = 'EQ'
        ENQ<4,KK> = 'DUMMY'
    END
RETURN
END
