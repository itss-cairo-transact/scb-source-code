* @ValidationCode : MjoxMTY2NzU1MTg3OkNwMTI1MjoxNjQwNjEwMTY3NTc4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 15:02:47
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE ADI.LIST.1417(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT

    COMP = ID.COMPANY

***************************************
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    FN.ACC   = 'FBNK.ACCOUNT';  F.ACC = '';  R.ACC = ''; E11 = ''
    CALL OPF(FN.ACC,F.ACC)

*    T.SEL = "SELECT FBNK.ACCOUNT WITH CO.CODE EQ ":COMP:" AND ACCT.DEBIT.INT NE '' BY @ID"
    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 1417 BY CO.CODE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    Z = 0
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            WS.AC.ID = KEY.LIST<I>

            CALL F.READ(FN.ACC,WS.AC.ID,R.AC, F.ACC ,E11)
*Line [ 59 ] Add @ into VM to be @VM - ITSS - R21 Upgrade - 2021-12-23
            WS.AC.DR.CNT   = DCOUNT(R.AC<AC.ACCT.DEBIT.INT>,@VM)
            WS.AD.DATE     = R.AC<AC.ACCT.DEBIT.INT><1,WS.AC.DR.CNT>
            WS.ADI.ID      = WS.AC.ID:'-':WS.AD.DATE

            Z        = Z+1
            ENQ<2,Z> = "@ID"
            ENQ<3,Z> = "EQ"
            ENQ<4,Z> = WS.ADI.ID

        NEXT I
    END
    IF Z = 0 THEN
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
RETURN
END
