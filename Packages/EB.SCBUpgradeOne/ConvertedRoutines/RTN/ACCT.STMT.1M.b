* @ValidationCode : MjotNjI5NDQ2ODUzOkNwMTI1MjoxNjQwNjE5NjY4MTg0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 17:41:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE ACCT.STMT.1M(ENQ)
*    PROGRAM ACCT.STMT.1M

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.STATEMENT
*Line [ 33 ] Comment $INCLUDE I_F.SCB.POST.LIST - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.POST.LIST
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    FN.AC ='FBNK.ACCOUNT.STATEMENT'
    F.AC  = ''
    R.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.ACC ='FBNK.ACCOUNT'
    F.ACC  = ''
    R.ACC  = ''
    CALL OPF(FN.ACC,F.ACC)



    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUST  = ""
    CALL OPF(FN.CUST,F.CUST)

*---------------------------------------------------------------------------

    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    ER.POST  = ""

    T.SEL = "SELECT FBNK.ACCOUNT.STATEMENT WITH STMT.FQU.1 LIKE ...M01... ";*BY STMT.FQU.1"
    T.SEL:= " AND CO.CODE EQ ":ID.COMPANY:" BY STMT.FQU.1"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*   TEXT = "SELECTED : " : SELECTED ; CALL REM
    TD = TODAY
    TY = TD[1,4]
    NN = 1
*DEBUG
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E.AC)
            FREQ       = R.AC<AC.STA.STMT.FQU.1>

            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E.ACC)
            AC.CODE = R.ACC<AC.CO.CODE>
            IF R.ACC<AC.CUSTOMER> THEN
                CATEG =   R.ACC<AC.CATEGORY>
                IF AC.CODE NE 'EG0010099' THEN
                    ID.CUST = KEY.LIST<I>[1,8] +1-1

                    CALL F.READ(FN.CUST,ID.CUST,R.CUST,F.CUST,ERR1.CUST)
                    CUS.POST = R.CUST<EB.CUS.POSTING.RESTRICT>
                    CUST.LCL = R.CUST<EB.CUS.LOCAL.REF>
                    ACT.CUST = CUST.LCL<1,CULR.ACTIVE.CUST>
                    CR.CODE  = CUST.LCL<1,CULR.CREDIT.CODE>
                    CUST.GOV = CUST.LCL<1,CULR.GOVERNORATE>
                    CUST.SEC = R.CUST<EB.CUS.SECTOR>
                    IF CATEG EQ '1001' OR CATEG EQ '1003' OR CATEG EQ '2000' OR CATEG EQ '2001' OR CATEG EQ '6501' OR CATEG EQ '6502' OR CATEG EQ '6503' OR CATEG EQ '6504' OR CATEG EQ '6511' OR CATEG EQ '6512' OR CATEG EQ '6513' OR CATEG EQ '6514' OR CATEG EQ '6515' OR CATEG EQ '6516' OR CATEG EQ '6517' THEN
                        IF CATEG GE '1101' OR CATEG LE '1599' THEN
                            IF CUS.POST LE '89' THEN
                                IF CR.CODE NE '110' AND CR.CODE NE '120' THEN
                                    IF CUST.SEC NE 1100 THEN
                                        IF CUST.GOV NE '98' THEN
                                            ENQ<2,NN> = "@ID"
                                            ENQ<3,NN> = "EQ"
                                            ENQ<4,NN> =  KEY.LIST<I>
                                            NN++
                                        END
                                    END
                                END
                            END
                        END
                    END
                END
            END
        NEXT I
    END ELSE
        ENQ<2,2> = "@ID"
        ENQ<3,2> = "EQ"
        ENQ<4,2> = "DUMMY"
    END
    IF NN EQ 1 THEN
        ENQ<2,2> = "@ID"
        ENQ<3,2> = "EQ"
        ENQ<4,2> = "DUMMY"
    END


**********************
RETURN
END
