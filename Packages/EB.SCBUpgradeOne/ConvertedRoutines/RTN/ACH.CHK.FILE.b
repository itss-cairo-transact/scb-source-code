* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>48</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACH.CHK.FILE
*    PROGRAM    ACH.CHK.FILE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h

    SEQ.FILE.NAME = 'ACH/CreditTransfer'
    REN.FILE =''
*---------------------------------------
    YTEXT =  " YYYYMMDD ���� ����� ����� "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    FILE.DATE = COMI
    T.SEL = "SELECT ACH/CreditTransfer  WITH @ID LIKE 62_PACS008_":FILE.DATE:"...txt.txt"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = ' NO. OF  FILES FOUND ':SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
        TEXT = 'FILE NO. ':I:'/':SELECTED ; CALL REM
        PRINT KEY.LIST<I>
        RECORD.NAME = KEY.LIST<I>
        OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
            PRINT 'Unable to Locate ':SEQ.FILE.POINTER
            STOP
            RETURN
        END
        IF IOCTL(SEQ.FILE.POINTER, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP
        EOF = ''
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            IF   Y.MSG[140,4] EQ 'PENG' OR Y.MSG[140,4] EQ 'PENS' OR Y.MSG[140,4] EQ 'SSBE' OR Y.MSG[140,4] EQ 'CASH' OR Y.MSG[140,4] EQ 'SALA' OR Y.MSG[140,4] EQ 'SUPP' OR Y.MSG[140,4] EQ 'CBET' OR Y.MSG[140,4] EQ 'PCRD' THEN

                TEXT = 'VAL.DATE ':Y.MSG[115,10]:' NO.OF.REC ':Y.MSG[77,5] ; CALL REM

*  YTEXT = "1.UPLOAD FILE  2.CANCEL FILE"
*  CALL TXTINP(YTEXT, 8, 22, "8", "A")
*  CP.ORD = COMI

*  IF CP.ORD EQ '1' THEN
                REN.FILE ="cp ":SEQ.FILE.NAME:"/":RECORD.NAME:" ":SEQ.FILE.NAME:"/../LOAD.CRD/":"ACH.CRD.TXT"
                EXECUTE REN.FILE
*Line [ 65 ] Adding EB.SCBUpgradeOne. in order to call another routine - ITSS - R21 Upgrade - 2021-12-28
                CALL EB.SCBUpgradeOne.ACH.PACS008.PENSION.FILL(FILE.DATE)
                REN.FILE ="mv ":SEQ.FILE.NAME:"/":RECORD.NAME:" ":SEQ.FILE.NAME:"/":RECORD.NAME:".bak"
                EXECUTE REN.FILE
*------TEMP -----------------
*                TEXT = Y.MSG[140,4]:" FILE UPLOADED SUCCESSFULLY"; CALL REM
*----------------------------
*     I = SELECTED
*  END

            END
        END

        CLOSESEQ SEQ.FILE.POINTER

    NEXT I
*IF CP.ORD EQ 1 THEN
*    TEXT = Y.MSG[140,4]:" FILE UPLOADED SUCCESSFULLY"; CALL REM
*END
*ELSE
*    TEXT = "FILE CANCELLED"; CALL REM
*END
END
