* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.ACCT.OPEN(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*===========================================
    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""
    CATEG    = ""

    LOCATE "CATEGORY" IN ENQ<2,1> SETTING CAT.POS THEN
        CATEG  = ENQ<4,CAT.POS>
    END

    IF (CATEG GE 5000 AND CATEG LE 5099) OR (CATEG GE 2000 AND CATEG LE 2001) THEN
        T.SEL  = "SELECT FBNK.ACCOUNT WITH"
        T.SEL := " CATEGORY EQ ":CATEG
*        T.SEL := " BY @ID BY CURRENCY"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR II = 1 TO SELECTED
                ENQ<2,II> = '@ID'
                ENQ<3,II> = 'EQ'
                ENQ<4,II> = KEY.LIST<II>
            NEXT II
        END ELSE
            ENQ<2,1> = '@ID'
            ENQ<3,1> = 'EQ'
            ENQ<4,1> = 'DUMMY'
        END
*  END ELSE
*      ENQ<2,1> = '@ID'
*      ENQ<3,1> = 'EQ'
*      ENQ<4,1> = 'DUMMY'
    END
*=============================================
    RETURN
END
