* @ValidationCode : MjozNTAxMDg0Mjg6Q3AxMjUyOjE2NDE3MzcyOTg3MDg6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 16:08:18
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-300</Rating>
*-----------------------------------------------------------------------------
************** RIHAM YOUSSEF 3/11/2014 *****************

SUBROUTINE ADVICE.AMMARGIN2

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CHARGE
    $INCLUDE I_F.COMPANY


    GOSUB CALLDB
    MYID = MYCODE:'.':MYTYPE
    GOSUB INITIATE
    GOSUB BODY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.AMMARGIN2'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*=================================================
CALLDB:
    IF ID.NEW = '' THEN
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
    LOCAL.REF       = R.LD<LD.LOCAL.REF>
    BENF1           = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2           = LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
    LG.NO           = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO2          = LOCAL.REF<1,LDLR.OLD.NO>
    AC.NUM          = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NO           = AC.NUM[1,8]:AC.NUM[9,2]:AC.NUM[11,4]:AC.NUM[15,2]
    THIRD.NO        = LOCAL.REF<1,LDLR.THIRD.NUMBER>
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME1     = LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME.2     = LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1     = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2     = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************************
**    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
**    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)

*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,CO.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CO.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.BOOK,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.BOOK,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YYBRN           = FIELD(BRANCH,'.',2)
*******************************************

    DATY            = TODAY
    LG.TYPE         = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 115 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME         = TYPE.NAME
    LG.AMT          = R.LD<LD.AMOUNT>
    CUR             = R.LD<LD.CURRENCY>
*Line [ 125 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.DATE         = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE        = LG.DATE[3,2]:"/":LG.DATE[5,2]:"/":LG.DATE[0,4]
    MARG.AMT        = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1         = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2         = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER        = LOCAL.REF<1,LDLR.MARGIN.PERC>
    SAM1            = MARG.PER - 100
    SAM             = ABS(SAM1)
    END.DATE        = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE    = END.DATE[3,2]:"/":END.DATE[5,2]:"/":END.DATE[0,4]
    LG.V.DATE       = LOCAL.REF<1,LDLR.MATUR.DATE>
    LG.VALUE.DATE   = LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
    CONFIS.AMT      = LOCAL.REF<1,LDLR.CONFISC.AMT>
    DIFFS           = R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
    DIFF            = ABS(DIFFS)
*************************
**************************
    IF ID.NEW NE '' THEN
        INPUTTER    = R.NEW(LD.INPUTTER)
        INP         = FIELD(INPUTTER,'_',2)
    END ELSE
        INPUTTER    = R.LD<LD.INPUTTER>
        INP         = FIELD(INPUTTER,'_',2)
    END

    IF ID.NEW NE '' THEN
        INP = R.NEW(LD.INPUTTER)
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>
***********
    IF ID.NEW       = ''  THEN
        REF         = COMI
    END ELSE
        REF         = ID.NEW
    END
*------------------------------------------
    MYTAX           = ''
    MYCODE          = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE          = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID            = MYCODE:'.':MYTYPE
    TOT             = DIFF + LG.COM1
RETURN
*=================================================
BODY:
    PR.HD  ="'L'":SPACE(6):"ADVICE.AMMARGIN2"
    PR.HD :="'L'":SPACE(6):"����� ���"
    PR.HD :="'L'":"���� �       :":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������      :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������   :":AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� ����   :":DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������      :":CRR
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������  :":THIRD.NAME1
    IF THIRD.NAME.2 THEN
        PR.HD :="'L'":SPACE(15) :THIRD.NAME.2
    END ELSE
        PR.HD :="'L'":" "
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������     :":THIRD.ADDR1
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� � �":LG.NO2:"(":LG.NAME:")"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(25):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):LG.COM1:SPACE(19):" ������� �� ����� ���� ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(6):LG.COM2:SPACE(19): "���� � ������� ������ "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    DIFF =  LG.COM1 + LG.COM2
    DIFF=FMT(DIFF,"R2")
    PR.HD :="'L'":SPACE(6):DIFF:SPACE(19):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    DIFF=FMT(DIFF,"R2")
    CALL WORDS.ARABIC(DIFF,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)
*Line [ 188 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 190 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
    PR.HD :="'L'":"=============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(15):"������":SPACE(15):"��� �������"
    PR.HD :="'L'":INP:SPACE(20):AUTH:SPACE(20):REF
    HEADING PR.HD
    CALL LG.ADD(AUTH,LG.CO)
RETURN
*===================================================
END
