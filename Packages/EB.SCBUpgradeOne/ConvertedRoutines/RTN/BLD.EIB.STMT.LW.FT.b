* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
***** CREATED BY MOHAMED SABRY 2015/05/10
    SUBROUTINE BLD.EIB.STMT.LW.FT(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*#    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    I = 0
*----------------------------------
    FN.STMT = 'FBNK.STMT.ENTRY'
    F.STMT  = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.ENT.LW = 'FBNK.ACCT.ENT.LWORK.DAY'
    F.ENT.LW  = ''
    CALL OPF(FN.ENT.LW,F.ENT.LW)

    DAT.ID    = 'EG0010001'
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.LPE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.LPE=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.LWD)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.LWD=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

************************ ACCT.ENT.LWORK.DAY ***************************
*    T.SEL = "SELECT ":FN.ENT.LW:" WITH CO.CODE EQ ":COMP:" BY @ID"

    T.SEL = "SELECT ":FN.ENT.LW:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",NO.REC.LW,ER.SEL)
*    TEXT = NO.REC.LW ; CALL REM
    IF NO.REC.LW THEN
        FOR Y = 1 TO NO.REC.LW
            CALL F.READ(FN.ENT.LW,KEY.LIST<Y>,R.ENT.LW,F.ENT.LW,EER.R.LW)
            LOOP
                REMOVE WS.STMT.ID FROM R.ENT.LW SETTING POS1
            WHILE WS.STMT.ID:POS1
                CALL F.READ(FN.STMT,WS.STMT.ID,R.STMT,F.STMT,ERR.STMT)
                WS.SYS.ID = R.STMT<AC.STE.SYSTEM.ID>
                WS.ATH.ID = R.STMT<AC.STE.AUTHORISER>

                IF WS.SYS.ID NE 'FT' THEN
                    GOTO NEXT.I.REC
                END

                IF WS.ATH.ID[10] NE 'SCB.EIB123' THEN
                    GOTO NEXT.I.REC
                END
                IF R.STMT<AC.STE.AMOUNT.LCY> LE 0 THEN
                    GOTO NEXT.I.REC
                END

*   WS.STMT.GL = R.STMT<AC.STE.CRF.PROD.CAT>
*   WS.STMT.TR = R.STMT<AC.STE.TRANSACTION.CODE>
*   WS.STMT.VD = R.STMT<AC.STE.VALUE.DATE>

                I++
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = WS.STMT.ID
NEXT.I.REC:
            REPEAT
        NEXT Y
    END
************************ IF NO REC IN TODAY AND LWD **********************
    IF I EQ 0 THEN
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'

    END
END
