* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE ACCT.CENTRAL.TXT
*    PROGRAM  ACCT.CENTRAL.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*****
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "ACCT.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/":' ':"ACCT.TXT"
        HUSH OFF
    END
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "ACCT.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ACCT.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create ACCT.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END

*****

    FN.AC  = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

*---------------------------
    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY GE 5000 AND CATEGORY LT 5099 AND CATEGORY NE 5082 AND CATEGORY NE 5083 AND CO.CODE EQ EG0010099 BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)
            AC.ID   = KEY.LIST<I>
            AC.NAME = R.AC<AC.ACCOUNT.TITLE.1>
            AC.BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            AC.CURR = R.AC<AC.CURRENCY>
            BB.DATA  = AC.ID:'|'
            BB.DATA := AC.NAME:'|'
            BB.DATA := AC.BAL:'|'
            BB.DATA := AC.CURR

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
