* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ACH.PACS002.TXT
*   PROGRAM ACH.PACS002.TXT

*NEW CREATED CREATED 23-06-2010
* NOHA HAMED

*-----------------------------------
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS002
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h

    HH.DATA    = ''
    BB.DATA    = ''
    TT.DATE    = ''

    FN.PAC2 = "F.SCB.ACH.PACS002"; F.PAC2=''; R.PAC2 = ''
    CALL OPF(FN.PAC2,F.PAC2)

    OPENSEQ "ACH" , "PACS002.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"ACH":' ':"PACS002.TXT"
        HUSH OFF
    END
    OPENSEQ "ACH" , "PACS002.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE PACS002.TXT CREATED IN ACH/'
        END
        ELSE
            STOP 'Cannot create PACS002.TXT File IN ACH/'
        END
    END

    YTEXT = "Enter ACH Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    T.DATE = COMI

*******************

    T.SEL = "SELECT F.SCB.ACH.PACS002 WITH @ID LIKE ":T.DATE:".... AND BATCH.TAKEN EQ ''"
  *  T.SEL = "SELECT F.SCB.ACH.PACS002 WITH @ID LIKE ":T.DATE:"...."
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    NO.RECORDS = SELECTED
    FLAG = 1

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.PAC2,KEY.LIST<I>, R.PAC2, F.PAC2, ETEXT)
            PAC2.ID               =  KEY.LIST<I>
            BATCH.ID              =  R.PAC2<PACS2.BATCH.ID                      >
            CREATION.DATE.TIME    =  R.PAC2<PACS2.CREATION.DATE.TIME>
            ORIGIN.BATCH.ID       =  R.PAC2<PACS2.ORIGIN.BATCH.ID>
            ORIGIN.BATCH.INDICATOR=  R.PAC2<PACS2.ORIGIN.BATCH.INDICATOR>
            GROUP.STATUS          =  R.PAC2<PACS2.GROUP.STATUS>
            STATUS.REASON         =  R.PAC2<PACS2.STATUS.REASON>
            STATUS.REASON.INF01   =  R.PAC2<PACS2.STATUS.REASON.INF01>
            STATUS.REASON.INFO2   =  R.PAC2<PACS2.STATUS.REASON.INFO2>
            STATUS.IDENT          =  R.PAC2<PACS2.STATUS.IDENT>
            ORIGIN.TRANS.IDENT    =  R.PAC2<PACS2.ORIGIN.TRANS.IDENT>
            STATUS.REASON.D       =  R.PAC2<PACS2.STATUS.REASON.D>
            ADD.STATUS.REASON.1   =  R.PAC2<PACS2.ADD.STATUS.REASON.1>
            ADD.STATUS.REASON.2   =  R.PAC2<PACS2.ADD.STATUS.REASON.2>

*******WRITE HEADER
            IF FLAG EQ 1 THEN
                HH.DATA  = 'FH':FMT(BATCH.ID,"R#35"):FMT(CREATION.DATE.TIME,"R#25")
                HH.DATA := FMT(ORIGIN.BATCH.ID,"R#35"):FMT(ORIGIN.BATCH.INDICATOR,"R#35")
                HH.DATA := FMT(GROUP.STATUS,"R#4"):FMT(STATUS.REASON,"R#35")
                HH.DATA := FMT(STATUS.REASON.INF01,"R#105"):FMT(STATUS.REASON.INFO2,"R#105")

                WRITESEQ HH.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END

*******WRITE BODY
            BB.DATA   = 'TR':FMT(STATUS.IDENT,"R#35"):FMT(ORIGIN.TRANS.IDENT,"R#35")
            BB.DATA  := FMT(STATUS.REASON.D,"R#35"):FMT(ADD.STATUS.REASON.1,"R#105")
            BB.DATA  := FMT(ADD.STATUS.REASON.2,"R#105")

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

*******UPDATE PACS002
            R.PAC2<PACS2.BATCH.TAKEN> = 'Y'
            CALL F.WRITE(FN.PAC2,KEY.LIST<I>,R.PAC2)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)

*******WRITE FOOTER
            IF FLAG EQ NO.RECORDS THEN
                TOT.REC   = NO.RECORDS + 1
                TT.DATA   = 'FT':FMT(TOT.REC,"R#18")
                WRITESEQ TT.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

            END
*******************
            FLAG = FLAG + 1
        NEXT I
    END
END
