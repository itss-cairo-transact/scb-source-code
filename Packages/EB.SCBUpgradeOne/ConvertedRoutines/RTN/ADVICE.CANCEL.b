* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-232</Rating>
*-----------------------------------------------------------------------------
******* WAEL *******

    SUBROUTINE ADVICE.CANCEL
*PROGRAM ADVICE.CANCEL

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB CALLDB
    MYID       = MYCODE:'.':MYTYPE


***IF MYCODE # '1301' AND MYCODE # '1302' AND MYCODE # '1303' THEN E = "NOT.VALID.VERSIONGGGGG":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'

    IF MYCODE = "1301"  THEN GOSUB INITIATE;GOSUB BODY;GOSUB BODY2;CALL PRINTER.OFF;CALL PRINTER.CLOSE(REPORT.ID,0,'');TEXT = "ADVICE CREATED SUCCESFULLY" ; CALL REM
    IF MYCODE = "1302"  THEN GOSUB INITIATE;GOSUB BODY;GOSUB BODY2;CALL PRINTER.OFF;CALL PRINTER.CLOSE(REPORT.ID,0,'');TEXT = "ADVICE CREATED SUCCESFULLY" ; CALL REM
    IF MYCODE = "1303"  THEN GOSUB INITIATE;GOSUB BODY2;CALL PRINTER.OFF;CALL PRINTER.CLOSE(REPORT.ID,0,'');TEXT = "ADVICE CREATED SUCCESFULLY" ; CALL REM
    IF MYCODE = "1304"  THEN GOSUB INITIATE;GOSUB BODY;CALL PRINTER.OFF;CALL PRINTER.CLOSE(REPORT.ID,0,'');TEXT = "ADVICE CREATED SUCCESFULLY" ; CALL REM

    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.CANCEL'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:

    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
*LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
    AC.NO = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
*************************UPDATE NI7OOOOO ***********************
    AC.NO = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 103 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSS,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
    THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
    THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
    THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*******************************UPDATED BY RIHAM R15**********************
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,THIRD.NO,AC.OFICER)
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,COMP.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
COMP.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BR    = COMP.BOOK[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*******************************END OF UPDATED ******************
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.DATE = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE = LG.DATE[7,2]:"/":LG.DATE[5,2]:"/":LG.DATE[1,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>

    LG.COM1 = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[7,2]:"/":END.DATE[5,2]:"/":END.DATE[1,4]
*   LG.V.DATE = R.LD<LD.VALUE.DATE>
    LG.V.DATE = R.LD<LD.LOCAL.REF><1,LDLR.INSTAL.DUE.DATE>
*   LG.V.DATE = R.LD<LD.FIN.MAT.DATE>
*   LG.V.DATE = R.LD<LD.LOCAL.REF><1,LDLR.APPROVAL.DATE>
*  LG.VALUE.DATE = LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
    LG.VALUE.DATE = LG.V.DATE
    INPUTTER    = R.NEW(LD.INPUTTER)
    INP         = FIELD(INPUTTER,'_',2)
    AUTHORISER  = R.NEW(LD.AUTHORISER)
    AUTH        = FIELD(AUTHORISER,'_',2)
    REF         = COMI
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE


*--------------------------------------------
    TOT = MARG.AMT + LG.COM1+LG.COM2
    RETURN
*=================================================
BODY:
    PR.HD  ="'L'":SPACE(6):"ADVICE.CANCEL"
    PR.HD :="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:SPACE(20):"����� ���  "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
*    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� �������� ���� ���� ������ ��� ������ ������� �����  ."
    PR.HD :="'L'":" ��� �.� ��� ":LG.NO:"":LG.NAME:""
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(22):" �������������"
    PR.HD :="'L'": CRR
    PR.HD :="'L'":" "
    IF MYCODE EQ '1304' THEN
        PR.HD :="'L'":SPACE(3): LG.COM2:SPACE(21): "����� �����"
    END
    LG.COM=LG.COM1
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(3): LG.COM1:SPACE(21): "���� � ������� ������ "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":LG.COM1:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    IN.AMOUNT = LG.COM1
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
********************UP NI7OOOOOO********************
    IF IN.AMOUNT EQ 0 OR IN.AMOUNT EQ '' THEN
        OUT.AMOUNT = 0
    END
****************************************************
*Line [ 190 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 192 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
    NEXT I

    PR.HD :="'L'":" ����� ����  ":" : ": DATY : SPACE(50) :"�� ��� ���� ������"

    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):COMI


    HEADING PR.HD

    RETURN

*===================================================
BODY2:
    LG.AMT = R.OLD(LD.AMOUNT)
    PR.HD  ="'L'":SPACE(6):"ADVICE.CANCEL.CREDIT"
    PR.HD :="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2:SPACE(20):"����� ����� "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
*  PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ������ ���� ����� ����� �������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(2):CRR:SPACE(22):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(3): MARG.AMT:SPACE(21):" ����� ������� ������ �� �.� ��� � �":LG.NO
    PR.HD :="'L'":SPACE(27): " ������ ��� ������� � ����� "
    PR.HD :="'L'":SPACE(28):BENF1
    PR.HD :="'L'":SPACE(28):BENF2
    PR.HD :="'L'":
*    PR.HD :="'L'":SPACE(27):" �����  ":"/":MARG.AMT:CRR:" ������� �� �������"
**  PR.HD :="'L'":SPACE(27):" �����  ": "/":LG.AMT:CRR:" ������� �� �������"
********************** UP NI7OOOOO (20101031)***************
    PR.HD :="'L'":SPACE(27):" �����  ": "/":LG.AMT:CRR
    PR.HD :="'L'":SPACE(27):" ����� ������ �� ������� ��� �������� �� ���������� ������ �����."
*************************************************************************
    PR.HD :="'L'":" "
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":MARG.AMT:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    IN.AMOUNT=MARG.AMT
    IF IN.AMOUNT EQ 0 THEN
        OUT.AMOUNT = 0
    END
    IF IN.AMOUNT NE 0 THEN
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*Line [ 260 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)
*Line [ 262 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
            IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
                PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
            END ELSE
                PR.HD :="'L'":OUT.AMOUNT<1,I>
            END
        NEXT I
    END
    PR.HD :="'L'":" ����� ����  ":" : ": LG.VALUE.DATE

    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(10):AUTH:SPACE(10):COMI


    HEADING PR.HD

*TEXT ="ADVICE CREATED SUCCESSFULLY" ; CALL REM
    RETURN
*=================================================
END
