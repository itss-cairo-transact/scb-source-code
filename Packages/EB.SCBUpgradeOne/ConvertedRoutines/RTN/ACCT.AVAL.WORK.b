* @ValidationCode : MjoxNzk0MzMxNjM3OkNwMTI1MjoxNjQyMDc4ODMyNjE1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 13 Jan 2022 15:00:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ACCT.AVAL.WORK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Comment $INCLUDE I_F.SCB.VISA.TRANS.TOT - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.VISA.TRANS.TOT
*Line [ 33 ] Comment $INCLUDE I_F.SCB.MAST.TRANS - ITSS - R21 Upgrade - 2021-12-23
* $INCLUDE I_F.SCB.MAST.TRANS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
    
    AC = O.DATA
    WORK.BALANCE   = 0
    AVL.BALANCE    = 0
    AVAL.NO        = 0
    BAL.AVAL       = 0

    FN.AC = 'FBNK.ACCOUNT' ; R.AC = ''      ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    CALL F.READ(FN.AC,AC,R.AC,F.AC,E1)
*Line [ 49 ] Update AC.AVL.BAL to AC.AVAILABLE.BAL - ITSS - R21 Upgrade - 2021-12-23
*AVL.BALANCE    =  R.AC<AC.AVL.BAL>
    AVL.BALANCE    =  R.AC<AC.AVAILABLE.BAL>
*TEXT = AVL.BALANCE ;CALL REM
    AVAL.NO        = DCOUNT(AVL.BALANCE,@VM)
    BAL.AVAL       = AVL.BALANCE<1,AVAL.NO>
    IF BAL.AVAL EQ '' THEN
        O.DATA         = 0
    END ELSE
        O.DATA = BAL.AVAL
    END
RETURN
END
