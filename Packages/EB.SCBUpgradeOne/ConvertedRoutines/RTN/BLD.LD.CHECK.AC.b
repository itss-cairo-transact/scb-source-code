* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.LD.CHECK.AC(ENQ)
*     PROGRAM BLD.LD.CHECK.AC
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*------------------------------------------------
    COMP = ID.COMPANY
    KK1 = 0
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'  ;  F.LD = ''
    CALL OPF(FN.LD,F.LD)

    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH STATUS EQ 'CUR' AND ( DRAWDOWN.ACCOUNT NE PRIN.LIQ.ACCT ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            KK1++
            CALL F.READ(FN.LD,KEY.LIST<II>,R.LD,F.LD,EER.R)
            DRAW.ACCT = R.LD<LD.DRAWDOWN.ACCOUNT>[1,8]
            PRIN.ACCT = R.LD<LD.PRIN.LIQ.ACCT>[1,8]

            IF DRAW.ACCT NE PRIN.ACCT THEN
                ENQ<2,II> = '@ID'
                ENQ<3,II> = 'EQ'
                ENQ<4,II> = KEY.LIST<II>
            END ELSE
                ENQ<2,II> = '@ID'
                ENQ<3,II> = 'EQ'
                ENQ<4,II> = 'DUMMY'
            END
        NEXT II
    END ELSE
        ENQ<2,1> = '@ID'
        ENQ<3,1> = 'EQ'
        ENQ<4,1> = 'DUMMY'
    END
**********************
    RETURN
END
