* @ValidationCode : Mjo0MTY3MzYzNjpDcDEyNTI6MTY0MDY4Mzk3NjI1Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:32:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
*-------------------------------NI7OOOOOOOOOO----------------------------------------------

SUBROUTINE BILL.OVER.INPUT.2

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.SLIPS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)


*** ACC     = R.NEW(DOC.PRO.COMM.ACCT)
    ACC     = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CUSID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSID=R.ITSS.ACCOUNT<AC.CUSTOMER>
    CUS = CUSID[1,3]

*** AMT2    = R.NEW(DOC.PRO.COMMISSION.AMOUNT)
    AMT     = COMI
    AMT2    = AMT[4,5]
**TEXT = "AMT2 : " : AMT2 ; CALL REM
**** DC.AMT  = DCOUNT(R.NEW(DOC.PRO.COMMISSION.AMOUNT),VM)
    DC.AMT  = DCOUNT(AMT2,@VM)
**************************************************************************
    IF CUS NE 994 THEN
        FOR I = 1 TO DC.AMT

*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,ACC,BAL2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL2=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CUS.ID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACC,CUS.LIM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.LIM=R.ITSS.ACCOUNT<AC.LIMIT.REF>

            LMT.REF1    = FIELD(CUS.LIM,'.',1)
            IF LEN(LMT.REF1) EQ 3 THEN
                XX = CUS.ID:'.':'0000':CUS.LIM
            END
            IF LEN(LMT.REF1) EQ 4 THEN
                XX = CUS.ID:'.':'000':CUS.LIM
            END


******        XX = CUS.ID:'.000':CUS.LIM

            T.SEL2 = "SELECT FBNK.LIMIT WITH @ID EQ ":XX
            CALL EB.READLIST(T.SEL2,KEY.LIST,"",SELECTED,ER.MSG)
            CALL F.READ(FN.LIM,KEY.LIST,R.LIM,F.LIM,E2)

            LIMIT.AMT = R.LIM<LI.AVAIL.AMT>

            TOTAL.DB  = BAL2 - AMT2
            TOTAL.CR  = TOTAL.DB + LIMIT.AMT
*** IF BAL LT AMT2 THEN
            IF TOTAL.CR LT 0 THEN
                ETEXT = "������ ����� ��� ������ ������ �� ����"
            END
            AMT = ''
        NEXT I
    END
***************************************************************************

RETURN
END
