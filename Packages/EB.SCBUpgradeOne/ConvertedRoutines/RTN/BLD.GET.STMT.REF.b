* @ValidationCode : Mjo4MzQ1MjY5MDI6Q3AxMjUyOjE2NDA2ODUyNDkxODY6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:54:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE BLD.GET.STMT.REF(ENQ)
**    PROGRAM BLD.GET.STMT.REF

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.AC.COM


**************
    EXECUTE "CLEAR-FILE F.SCB.AC.COM"
**************
*----------------------------------------------
    FN.AC  = "FBNK.ACCOUNT"    ; F.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.AC.COM  = "F.SCB.AC.COM"    ; F.AC.COM = ""
    CALL OPF(FN.AC.COM,F.AC.COM)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)

    FN.FT = 'FBNK.FUNDS.TRANSFER'    ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.LC = 'FBNK.LETTER.OF.CREDIT'    ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.IN   = 'F.INF.MULTI.TXN' ; F.IN= '' ; R.IN = ''
    CALL OPF(FN.IN,F.IN)

    FN.AC.COM='F.SCB.AC.COM' ; F.AC.COM = ''
    CALL OPF(FN.AC.COM,F.AC.COM)

    ZZ      = '0'
    NO.TRNS = '0'
*------------------
    YTEXT = 'START.DATE':'yyyymmdd'
    CALL TXTINP(YTEXT, 8, 22, "16", "A")

    FROM.DATE   = COMI
    YTEXT = 'END.DATE':'yyyymmdd'
    CALL TXTINP(YTEXT, 8, 22, "16", "A")

    END.DATE    = COMI

    YTEXT  = "Enter Account Number : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")

    ACC.ID = COMI

    ID.LIST     = ''
    BAL.DIFF    = ''
    OPENING.BAL = ''
    ETEXT       = ''
    E           = ''
    CALL EB.ACCT.ENTRY.LIST(ACC.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ERRR)

    LOOP
        REMOVE STD.ID FROM ID.LIST SETTING POS
    WHILE STD.ID:POS
        CALL F.READ(FN.STE,STD.ID,R.STE,F.STE,ER.STE)
        ID.REC = R.STE<AC.STE.OUR.REFERENCE>
        ID.APP = ID.REC[1,2]
****************************
        IF ID.APP EQ 'TF' THEN
            ID.LC = ID.REC[1,12]
            CALL F.READ(FN.LC,ID.LC,R.LC,F.LC,ETEXT1)
            ID.TRNS  = R.LC<TF.LC.OLD.LC.NUMBER>
        END
********************************
        IF ID.APP EQ 'FT' THEN

            CALL F.READ(FN.FT,ID.REC[1,12],R.FT,F.FT,E311)
            ID.TRNS = R.FT<FT.DEBIT.THEIR.REF>

            IF ID.TRNS EQ '' THEN
                FT.LOC  =  R.FT<FT.LOCAL.REF>
                ID.TRNS =  FT.LOC<1,FTLR.NOTE.CREDIT>
            END
            IF E311 THEN
                FT.ID.HIS = ID.REC[1,12]:';1'

                CALL F.READ(FN.FT.H,FT.ID.HIS,R.FT.H,F.FT.H,ETEXT211)
                ID.TRNS = R.FT.H<FT.DEBIT.THEIR.REF>

                IF ID.TRNS EQ '' THEN
                    FT.LOC.H  =  R.FT.H<FT.LOCAL.REF>
                    ID.TRNS =  FT.LOC.H<1,FTLR.NOTE.CREDIT>
                END
            END
        END
********************************
        IF ID.APP EQ 'IN' THEN
            ID.IN = FIELD(ID.REC,"-",1)
            ID.DET =FIELD(ID.REC,"-",2)
            CALL F.READ(FN.IN,ID.IN, R.IN, F.IN, ETEXT1)

            ID.TRNS =R.IN<INF.MLT.THEIR.REFERENCE><1,ID.DET>[1,12]
        END
*******************************

        CALL F.READ(FN.AC.COM,ID.TRNS,R.AC.COM,F.AC.COM,ER.STEE)

        LCY.AMT.TRNS = R.STE<AC.STE.AMOUNT.LCY>
        FCY.AMT.TRNS = R.STE<AC.STE.AMOUNT.FCY>
        BOOK.DAT     = R.STE<AC.STE.BOOKING.DATE>

***************
        IF ER.STEE THEN
            NO.TRNS = ''
        END ELSE
*Line [ 149 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
            NO.TRNS = DCOUNT(R.AC.COM<AC.COM.LCY.AMT.TRNS>,@VM)
            NO.TRNS = NO.TRNS + 1
        END
        R.AC.COM<AC.COM.ID.TRNS,NO.TRNS>       = ID.REC
        R.AC.COM<AC.COM.DATE.TRNS,NO.TRNS>     = BOOK.DAT
        R.AC.COM<AC.COM.LCY.AMT.TRNS,NO.TRNS>  = LCY.AMT.TRNS
        R.AC.COM<AC.COM.FCY.AMT.TRNS,NO.TRNS>  = FCY.AMT.TRNS
        WRITE R.AC.COM TO F.AC.COM , ID.TRNS ON ERROR
            PRINT 'CAN NOT WRITE RECORD ':FN.AC.COM
        END
        BAL.DIFF =  R.AC.COM<AC.COM.BAL.ACCOUNT> +  R.AC.COM<AC.COM.FCY.AMT.TRNS,NO.TRNS>
        R.AC.COM<AC.COM.BAL.ACCOUNT>  =  BAL.DIFF
        R.AC.COM<AC.COM.ACCOUNT>      =  ACC.ID
        CALL F.WRITE(FN.AC.COM ,ID.TRNS, R.AC.COM)
        CALL JOURNAL.UPDATE(ID.TRNS)
*****************
    REPEAT
*-------------------------------------
RETURN
END
