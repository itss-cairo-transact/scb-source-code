* @ValidationCode : MjotNDQ3NjAxNjkyOkNwMTI1MjoxNjQwNjgzNzQ0ODg4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:29:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------NI7OOOOOOOOOOO------------------------------------------
SUBROUTINE BILL.BATCH(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Comment $INCLUDE I_F.SCB.BT.BATCH - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.BT.BATCH
*Line [ 30 ] Comment $INCLUDE I_BR.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*    $INCLUDE I_BR.LOCAL.REFS

    FN.CUS='FBNK.BILL.REGISTER'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
    FN.BT='FBNK.SCB.BT.BATCH'
    F.BT=''
    CALL OPF(FN.BT,F.BT)

* CUS.IND = R.CUS<EB.CUS.INDUSTRY>
* T.SEL="SELECT FBNK.CUSTOMER WITH INDUSTRY LT 999 AND INDUSTRY LIKE ...00":CUS.IND

    T.SEL="SELECT F.SCB.BT.BATCH WITH DATE.TIME LIKE ...091011... AND VERSION.NAME EQ ',SCB.BT.BATCH.H' "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
* CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
    IF SELECTED THEN
*TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
RETURN
END
