* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
    SUBROUTINE BLD.ACCT.DEBIT(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    COMP  = ID.COMPANY
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    T.SEL  = "SELECT ":FN.AC:" WITH (( CATEGORY IN (1445 1415 1417 1418 1419 1420 1421 1481 1484 1485 1499 1408 1582 1483 1595 1493 1558 1525)"
    T.SEL := " OR CATEGORY IN (1001 1003 1005 1007 1059 1102 1201 1202 1205 1206 1207 1208 1211 1212 1216 1301 1302 1303 1377 1390)"
    T.SEL := " OR CATEGORY IN (1399 1401 1402 1404 1405 1406 1414 1455 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513)"
    T.SEL := " OR CATEGORY IN (1514 1516 1518 1519 1523 1534 1544 1559 1560 1566 1577 1588 1599 1407 1413 1480 2006 1217 6501 6502 6503 6504 6505 6506 6507 6508 6511 6513 6514 6515 6516))"
    T.SEL := " AND ONLINE.ACTUAL.BAL LT 0 AND ONLINE.ACTUAL.BAL NE '' AND ( CURRENCY EQ 'USD' OR CURRENCY EQ 'EUR')) BY CUSTOMER"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = '@ID'
            ENQ<3,I> = 'EQ'
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ<2,I> = '@ID'
        ENQ<3,I> = 'EQ'
        ENQ<4,I> = 'DUMMY'
    END

    RETURN
END
