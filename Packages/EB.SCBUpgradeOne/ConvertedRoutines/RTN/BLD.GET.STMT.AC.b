* @ValidationCode : MjoyMDYwMTI4NjYzOkNwMTI1MjoxNjQwNjg1MTgyMTgzOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:53:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>               **M.ELSAYED**  **CNV.GET.DATE**
*-----------------------------------------------------------------------------
SUBROUTINE BLD.GET.STMT.AC(ENQ)

* PROGRAM BLD.GET.STMT.AC
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Comment $INCLUDE I_FT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_FT.LOCAL.REFS
*===========================================
    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)
    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FT.L = 'FBNK.FUNDS.TRANSFER' ; F.FT.L = ''
    CALL OPF(FN.FT.L,F.FT.L)
    ZZ = 0
* DEBUG
*------------------
    YTEXT = "Enter Date. : "
    YTEXT = ' ������� ������� ':'yyyymmdd'

    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    FROM.DATE = COMI
    WS.CHK.DATE = COMI[5,4]
*   END.DATE = TODAY
*   CALL CDT('',END.DATE,'-1W')

*  END.TIME = END.DATE[6]:'1700'
* FROM.DATE  = TODAY
    CALL CDT('',FROM.DATE,'-1W')

* FROM.TIME = FROM.DATE[6]:'1700'
* TEXT = "FROM: ":FROM.DATE : "TO: ":END.DATE   ; CALL REM
    ACC.ID = 'EGP1103800010099'
    Z = 1
    END.DATE = COMI
    CALL CDT('',END.DATE,'+1W')

* TEXT = END.DATE :" ": FROM.DATE:" ": WS.CHK.DATE ; CALL REM
    CALL EB.ACCT.ENTRY.LIST(ACC.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    LOOP
        REMOVE STD.ID FROM ID.LIST SETTING POS
    WHILE STD.ID:POS

        CALL F.READ(FN.STE,STD.ID,R.STE,F.STE,ER.STE)
        DAT.TIME = R.STE<AC.STE.DATE.TIME,1>
        YEAR = DAT.TIME[1,2]

        FT.ID = R.STE<AC.STE.OUR.REFERENCE> :";1"
        FLG1 = 0
        CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
        IF NOT(ER.FT) THEN
            FLG1 = 1
        END ELSE
            FT.ID = R.STE<AC.STE.OUR.REFERENCE>
            CALL F.READ(FN.FT.L,FT.ID,R.FT,F.FT.L,ER.FT.L)
            IF NOT(ER.FT.L) THEN
                FLG1 = 1
            END
        END
        IF FLG1 = 1 THEN
* EBC.SET.DATE = R.FT<FT.LOCAL.REF,FTLR.EBC.SET.DATE>
            EBC.SET.DATE = R.FT<FT.LOCAL.REF,45>

            EBC.DATE = EBC.SET.DATE
*  TEXT = EBC.DATE:' ':FT.ID ; CALL REM
*  TEXT = EBC.DATE :" ":WS.CHK.DATE ; CALL REM
            IF EBC.DATE EQ WS.CHK.DATE THEN
                ZZ ++
                ENQ<2,ZZ> = '@ID'
                ENQ<3,ZZ> = 'EQ'
                ENQ<4,ZZ> = STD.ID
            END
        END
        IF ZZ = 0 THEN
            ENQ<2,1> = '@ID'
            ENQ<3,1> = 'EQ'
            ENQ<4,1> = 'DUMMY'
        END
*END
* END ELSE
*     ENQ<2,1> = '@ID'
*     ENQ<3,1> = 'EQ'
*     ENQ<4,1> = 'DUMMY'
* END
    REPEAT

*=============================================
RETURN
END
