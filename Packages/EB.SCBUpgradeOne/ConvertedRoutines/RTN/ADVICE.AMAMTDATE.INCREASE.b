* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
******* WAEL *******
*-----------------------------------------------------------------------------
* <Rating>-560</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE ADVICE.AMAMTDATE.INCREASE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    BRN.NAME = ""
    BRN.AREA = ""

    GOSUB CALLDB
    IF MYCODE EQ '1233' OR MYCODE EQ '1235' THEN
        GOSUB INITIATE
        GOSUB BODY
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
    TEXT = "ADVICE SUCCESSFULY CREATED" ; CALL REM
    RETURN
*===============================
INITIATE:
    REPORT.ID='ADVICE.AMAMTDATE.INCREASE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*=================================================
CALLDB:
    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
*    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
** AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    AC.NUM = R.LD<LD.CHRG.LIQ.ACCT>
    AC.NO = AC.NUM[1,8]:"/":AC.NUM[9,2]:"/":AC.NUM[11,4]:"-":AC.NUM[15,2]
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
* THIRD.NAME1 =LOC.REF<1,CULR.ARABIC.NAME>
* THIRD.NAME2 =LOC.REF<1,CULR.ARABIC.NAME.2>
* THIRD.ADDR1 =LOC.REF<1,CULR.ARABIC.ADDRESS,1>
* THIRD.ADDR2 =LOC.REF<1,CULR.ARABIC.ADDRESS,2>

    CUSLD    = R.LD<LD.CUSTOMER.ID>
    THIRD.NO = LOCAL.REF<1,LDLR.THIRD.NUMBER>
*Line [ 101 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSLD,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSLD,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF.THIRD)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF.THIRD=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    IF THIRD.NO EQ CUSLD THEN
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    END
    IF THIRD.NO NE CUSLD THEN
        TEXT = "NE" ; CALL REM
*Line [ 123 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSLD,LOC.REF.THIRD)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSLD,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF.THIRD=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        THIRD.NAME1 = LOC.REF.THIRD<1,CULR.ARABIC.NAME>
        TEXT = "NAME : " : THIRD.NAME1 ; CALL REM
        THIRD.NAME2 = LOC.REF.THIRD<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF.THIRD<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF.THIRD<1,CULR.ARABIC.ADDRESS,2>
    END

*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,THIRD.NO,CO.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CO.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
*Line [ 144 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.BOOK,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.BOOK,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YYBRN   = FIELD(BRANCH,'.',2)
*******************************************8
    CURRNO  = R.LD<LD.CURR.NO>
    DATY    = TODAY
    LG.TYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 157 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    LG.AMT  = R.LD<LD.AMOUNT>
    AMT.DECREASE=R.LD<LD.AMOUNT.INCREASE>
    AMT.AVAILABLE=R.LD<LD.COMMT.AVAIL.AMT>
    AMT.INC      =R.LD<LD.AMOUNT.INCREASE>
    MAR.PER      =LOCAL.REF<1,LDLR.MARGIN.PERC>
    AMT.TOT      = LG.AMT-AMT.DECREASE
    CUR=R.LD<LD.CURRENCY>
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.DATE = R.LD<LD.FIN.MAT.DATE>
    FIN.DATE = LG.DATE[3,2]:"/":LG.DATE[5,2]:"/":LG.DATE[0,4]
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    LG.COM1 = R.LD<LD.CHRG.AMOUNT><1,1>
    LG.COM2 = R.LD<LD.CHRG.AMOUNT><1,2>
    MARG.PER = LOCAL.REF<1,LDLR.MARGIN.PERC>
    END.DATE = LOCAL.REF<1,LDLR.END.COMM.DATE>
    END.COM.DATE = END.DATE[3,2]:"/":END.DATE[5,2]:"/":END.DATE[0,4]
*LG.V.DATE = R.LD<LD.VALUE.DATE>
    LG.V.DATE = R.LD<LD.AMT.V.DATE>
    NNN = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
********
    LG.VALUE.DATE = LG.V.DATE[7,2]:"/":LG.V.DATE[5,2]:"/":LG.V.DATE[1,4]
********************
DIFFN  = (AMT.INC * MAR.PER)/100
********************
    DIFF   = FMT(DIFFN,"L2")
DIFF  = ABS(DIFF)

    INPUTTER    = R.LD<LD.INPUTTER>
    INP         = FIELD(INPUTTER,'_',2)
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
        REF = ID.NEW
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
        REF = COMI
    END
    LG.CO= R.LD<LD.CO.CODE>
*------------------------------------------
    MYTAX = ''
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

    TOT = DIFF + LG.COM1
    RETURN
*=================================================
BODY:
    PR.HD ="'L'":SPACE(6):YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(14):DATY[7,2]:SPACE(2):DATY[5,2]:SPACE(2):DATY[1,4]:SPACE(20):AC.NO
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.NAME1
    XX = SPACE(80)
    XX<1,1>[46,20] = "����� ����� "
    IF THIRD.NAME2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.NAME2
    END ELSE
        PR.HD :="'L'":SPACE(2)
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR1
    IF THIRD.ADDR2 THEN
        PR.HD :="'L'":SPACE(2):"/":THIRD.ADDR2
    END ELSE
        PR.HD :="'L'":SPACE(2)
    END
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ������ ���� ����� ����� �������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(15):" �������������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(3): DIFF :SPACE(15): "����� ��� �� ���� ������� ������ ����� ������ ��� � �" : LG.NO
    PR.HD :="'L'"
    PR.HD :="'L'":SPACE(21): "�����" :LG.AMT

    PR.HD :="'L'":SPACE(21) : "����� �� ������ ����� " : AMT.DECREASE
    PR.HD :="'L'":SPACE(21) : "����� ���� ������� ����� " : AMT.AVAILABLE

    PR.HD :="'L'":CRR
    PR.HD :="'L'":"=================================================="
    DIFF2 = DIFF
    PR.HD :="'L'":DIFF2:SPACE(15):" ������������������������"
    PR.HD :="'L'":"=================================================="
    PR.HD :="'L'":" "
*---------------------------
    CALL WORDS.ARABIC(DIFF,OUT.AMOUNT,70,NO.OF.LINES,ER.MSG)

    IF DIFF = 0 THEN
        OUT.AMOUNT = 0
    END

*Line [ 218 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
    FOR I = 1 TO DCOUNT(OUT.AMOUNT,@VM)

*Line [ 221 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-28
        IF I = DCOUNT(OUT.AMOUNT,@VM) THEN
            PR.HD :="'L'":OUT.AMOUNT<1,I> :" ":CRR
        END ELSE
            PR.HD :="'L'":OUT.AMOUNT<1,I>
        END
        PRINT
    NEXT I
*---------------------------
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ����� ����  ":" : ": LG.VALUE.DATE

    PR.HD :="'L'":"==============================================================="
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(12):"������":SPACE(12):"������"
    PR.HD :="'L'":INP:SPACE(12):AUTH:SPACE(12):REF
    HEADING PR.HD
    PRINT SPACE(5):"ADVICE.AMAMTDATE.INCREASE.1233"
    CALL LG.ADD(AUTH,LG.CO)

    RETURN
*===================================================
END
