* @ValidationCode : Mjo1Mjk0NTIzMDA6Q3AxMjUyOjE2NDA2ODUzODkyMjI6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:56:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>-45</Rating>
*-----------------------------------------------------------------------------
*PROGRAM BLD.ISCO.SME
SUBROUTINE BLD.ISCO.SME.A(ENQ)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Comment $INCLUDE I_F.SCB.ISCO.SME.CS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.ISCO.SME.CS
*Line [ 34 ] Comment $INCLUDE I_F.SCB.ISCO.SME.CF - ITSS - R21 Upgrade - 2021-12-23
*  $INCLUDE I_F.SCB.ISCO.SME.CF


    EOF  = ''
    R.CF = ''
    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
*============================ OPEN TABLES =========================

    FN.CF  = 'F.SCB.ISCO.SME.CF' ; F.CF = '' ; R.CF = ''
    CALL OPF(FN.CF,F.CF)
    FN.CS  = 'F.SCB.ISCO.SME.CS' ; F.CS = '' ; R.CS = ''
    CALL OPF(FN.CS,F.CS)

    LOCATE "ACCT.CLOSING.DATE" IN ENQ<2,1> SETTING ISCO.POS THEN
        CLOSE.DATE = ENQ<4,ISCO.POS>
        FROM.DATE  = FIELD(CLOSE.DATE,' ',1)
        TO.DATE    = FIELD(CLOSE.DATE,' ',2)
    END
*TEXT = FROM.DATE:'-':TO.DATE;CALL REM

    T.SEL  = "SELECT F.SCB.ISCO.SME.CF WITH COMMENT EQ 'Y'"
    T.SEL := " AND ASSET.CLASS EQ '404' AND ACCT.CLOSING.DATE GE ":FROM.DATE:" AND ACCT.CLOSING.DATE LE ":TO.DATE:" BY CF.ACC.NO"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*======================== BB.DATA ===========================

    CUST.NO = ''
    PRE.CUST.NO = ''
    TOT.APP.AMT = 0
    TOT.USE.AMT = 0
    K = 1

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ENQ<2,I> = "@ID"
            ENQ<3,I> = "EQ"
            ENQ<4,I> = KEY.LIST<I>
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"

    END
RETURN

END
