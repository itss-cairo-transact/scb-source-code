* @ValidationCode : Mjo5MDgyMDgwMzY6Q3AxMjUyOjE2NDA2ODI1MDU4MDc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:08:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>1623</Rating>
*-----------------------------------------------------------------------------

SUBROUTINE  ATM.CARD.ISSUE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CI.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.APP

    GOSUB INITIALISE
RETURN

INITIALISE:

*----------------------------------------------------------------------*

    FN.CUS  = 'FBNK.CUSTOMER'   ; F.CUS = ''
    CALL OPF(FN.CUS , F.CUS)

    FN.ACC = 'FBNK.ACCOUNT'   ; F.ACC = ''
    CALL OPF(FN.ACC , F.ACC)

    FN.APP = 'F.SCB.ATM.APP'   ; F.APP = ''
    CALL OPF(FN.APP , F.APP)

    DEFFUN SHIFT.DATE( )
    XX = 0
****************************************************
    DIR.NAME2 = '&SAVEDLISTS&'

    NEW.FILE2 = "ATM.ERR.":TODAY
    OPENSEQ DIR.NAME2,NEW.FILE2 TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME2:' ':NEW.FILE2
        HUSH OFF
        PRINT 'FILE ':NEW.FILE2:' DELETE FROM ':DIR.NAME2
    END
    OPENSEQ DIR.NAME2, NEW.FILE2 TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE2:' CREATED IN ':DIR.NAME2
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE2:' to ':DIR.NAME2
        END
    END
**-------------------------------------------------------------------
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "CARD.ISSUE"
    OFS.OPTIONS      = "ATM.CARD"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    COMMA = ","
    FILE.NAME = 'ATMCRD.ISS'
    Path = "ATM.CARD/":FILE.NAME

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    EOF = ''
    I = 1
    CRD.COD = ''
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CHK.LIN      = FIELD(Line,",",1)[1,6]
*            CHK.LIN.BR   = FIELD(Line,",",1)[7,2]
****UPDATED BY NESSREEN AHMED 3/2/2020********************
*****Updated by Nessreen Ahmed 25/12/2018****************
***** IF CHK.LIN EQ  '511864'  THEN
****IF (CHK.LIN EQ '511864') OR (CHK.LIN EQ '537077') OR (CHK.LIN EQ '534453') THEN
            IF (CHK.LIN EQ '511864') OR (CHK.LIN EQ '537077') OR (CHK.LIN EQ '534453') OR (CHK.LIN EQ '507808') THEN
****END OF UPDATE 3/2/2020********************
*****End of Update 25/12/2018*****************************
                Pan              = FIELD(Line,",",1)
                Name.On.Card     = FIELD(Line,",",3)
                Checking.Account = FIELD(Line,",",6)
                Saving.Account   = FIELD(Line,",",8)
                Credit.Account   = FIELD(Line,",",9)
*****UPDATED BY NESSREEN AHMED 4/2/2020***********************************************
*****           Customer.Id      = FIELD(Line,",",10)
                Customer.Id1     = FIELD(Line,",",10)
*****END OF UPDATE 4/2/20202**********************************************************
                Date             = FIELD(Line,",",13)
                Card.Type        = FIELD(Line,",",18)
                NO.MON           = '120M'
                ISS.DATE         = Date[7,4]:Date[4,2]:Date[1,2]
                EXP.DATE         = SHIFT.DATE(ISS.DATE, NO.MON,UP)
****UPDATED BY NESSREEN AHMED 3/2/2020********************************************
                IF (CHK.LIN EQ '507808') THEN
                    BEGIN CASE
                        CASE LEN(Customer.Id1) = 11
                            Customer.Id = Customer.Id1[1,8]
                        CASE LEN(Customer.Id1) = 10
                            Customer.Id = Customer.Id1[1,7]
                    END CASE
                END ELSE
                    Customer.Id = Customer.Id1
                END
****END 3/2/2020******************************************************************
                CALL F.READ(FN.CUS,Customer.Id,R.CUS,F.CUS,READ.ER.CUS)
                CUS.SEC = R.CUS<EB.CUS.SECTOR>
******Updated by Nessreen Ahmed 16/12/2018*******************************
                CHK.LIN.BR   = FIELD(Line,",",1)[7,2]
                IF CHK.LIN.BR EQ '98'  THEN
                    CRD.COD = '901'
                END ELSE
******End of Update 16/12/2018*******************************************
                    IF CUS.SEC EQ '2000' AND  Card.Type EQ 'P' THEN
                        CRD.COD = '801'
                    END
                    IF CUS.SEC EQ '2000' AND  Card.Type EQ 'S' THEN
                        CRD.COD = '810'
                    END
                    IF ( CUS.SEC GE '1100' AND CUS.SEC LE '1400' ) AND Card.Type EQ 'P' THEN
                        CRD.COD = '802'
                    END
                    IF ( CUS.SEC GE '1100' AND CUS.SEC LE '1400' ) AND Card.Type EQ 'S' THEN
                        CRD.COD = '811'
                    END
                END
** ----------------------- OFS ----------------

                OFS.MESSAGE.DATA := "CARD.STATUS=90":COMMA

                IF ( Checking.Account NE '' AND Saving.Account NE '' AND Credit.Account NE '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Checking.Account:COMMA
                    OFS.MESSAGE.DATA :="ACCOUNT:2=":Saving.Account:COMMA
                    OFS.MESSAGE.DATA :="ACCOUNT:3=":Credit.Account:COMMA
                END

                IF ( Checking.Account NE '' AND Saving.Account NE '' AND Credit.Account EQ '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Checking.Account:COMMA
                    OFS.MESSAGE.DATA :="ACCOUNT:2=":Saving.Account:COMMA
                END

                IF ( Checking.Account NE '' AND Saving.Account EQ '' AND Credit.Account NE '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Checking.Account:COMMA
                    OFS.MESSAGE.DATA :="ACCOUNT:2=":Credit.Account:COMMA
                END

                IF ( Checking.Account EQ '' AND Saving.Account NE '' AND Credit.Account NE '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Saving.Account:COMMA
                    OFS.MESSAGE.DATA :="ACCOUNT:2=":Credit.Account:COMMA
                END

                IF ( Checking.Account EQ '' AND Saving.Account EQ '' AND Credit.Account NE '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Credit.Account:COMMA
                END
                IF ( Checking.Account EQ '' AND Saving.Account NE '' AND Credit.Account EQ '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Saving.Account:COMMA
                END

                IF ( Checking.Account NE '' AND Saving.Account EQ '' AND Credit.Account EQ '' ) THEN
                    OFS.MESSAGE.DATA :="ACCOUNT:1=":Checking.Account:COMMA
                END
                OFS.MESSAGE.DATA :="ISSUE.DATE=":ISS.DATE:COMMA
                OFS.MESSAGE.DATA :="EXPIRY.DATE=":EXP.DATE:COMMA
                OFS.MESSAGE.DATA :="NAME=":Name.On.Card:COMMA

                OFS.MESSAGE.DATA :="LOCAL.REF:3=":Customer.Id:COMMA
                OFS.MESSAGE.DATA :="LOCAL.REF:4=":Pan:COMMA
                OFS.MESSAGE.DATA :="LOCAL.REF:5=":CRD.COD:COMMA

*******UPDATED BY NESSREEN AHMED 15/9/2014************************
                APP.ID = ''
                T.SEL =  "SELECT F.SCB.ATM.APP WITH CUSTOMER EQ ": Customer.Id :" BY DATE.TIME "

                KEY.LIST=""
                SELECTED=""
                ER.MSG=""

                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
********    APP.ID  = Customer.Id:'.1'
                APP.ID = KEY.LIST<SELECTED>
********END OF UPDATE 15/9/2014****************************************
                CALL F.READ(FN.APP,APP.ID,R.APP,F.APP,READ.ER.APP)
                FAST.ACC =   R.APP<SCB.VISA.FAST.ACC>
                CALL F.READ(FN.ACC,FAST.ACC,R.ACC,F.ACC,READ.ER.ACC)

                BRN    = R.ACC<AC.CO.CODE>[8,2]
                OFS.MESSAGE.DATA :="LOCAL.REF:6=":BRN:COMMA
                OFS.MESSAGE.DATA :="LOCAL.REF:10=":FAST.ACC:COMMA

                COMPO  = "EG00100":BRN
                OFS.USER.INFO     = "INPUTT":BRN:"//":COMPO

                OFS.IDD = "ATMC.":Pan
                F.PATH  = FN.OFS.IN
                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.IDD:COMMA:OFS.MESSAGE.DATA
                OFS.ID  = "CARD.":Pan:"-":TODAY

***             IF FAST.ACC NE '' THEN
***                IF (FAST.ACC NE '') OR (BRN NE '') THEN
                IF BRN NE '' THEN
                    XX = XX + 1
                    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-CARD.AUTO.ISS' ON ERROR  TEXT = " ERROR ";CALL REM
                    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                END ELSE      ;**Else of not write**
                    VISA.DATA = Pan
                    VISA.DATA := "|Name.OnCard="
                    VISA.DATA := Name.On.Card
                    VISA.DATA := "|Checking.Acct="
                    VISA.DATA := Checking.Account
                    VISA.DATA := "|Sav.Acct="
                    VISA.DATA := Saving.Account
                    VISA.DATA := "|Credit.Acct="
                    VISA.DATA := Credit.Account
                    VISA.DATA := "|Cust.Id ="
                    VISA.DATA := Customer.Id
                    VISA.DATA := "|Fast.Acct="
                    VISA.DATA := FAST.ACC
                    VISA.DATA := "|Branch="
                    VISA.DATA := BRN
                    NM = NM + 1
                    WRITESEQ VISA.DATA TO V.FILE.IN ELSE
                        PRINT  'CAN NOT WRITE LINE ':VISA.DATA
                    END
                END
            END
            OFS.MESSAGE.DATA = ''
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath

    TEXT = '�� �������� �� ��������' ; CALL REM
    TEXT = '������ �������� �������':XX ; CALL REM
    TEXT = '��� �������� ���� �� ���������':NM ; CALL REM
RETURN
***------------------------------------------------------------------***
END
