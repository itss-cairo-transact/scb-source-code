* @ValidationCode : Mjo5NjkzMzI2ODA6Q3AxMjUyOjE2NDA2ODQzMTI0OTM6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:38:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE BILL.REG.CCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.SLIPS

**------------ 2011/11/15-----------HYTHAM --& NESMAAA --
    IF R.NEW(SCB.BS.CO.CODE) NE '' THEN
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.BR.SLIPS':@FM:SCB.BS.DRAWER,ID.NEW,BR.EXIT)
F.ITSS.SCB.BR.SLIPS = 'F.SCB.BR.SLIPS'
FN.F.ITSS.SCB.BR.SLIPS = ''
CALL OPF(F.ITSS.SCB.BR.SLIPS,FN.F.ITSS.SCB.BR.SLIPS)
CALL F.READ(F.ITSS.SCB.BR.SLIPS,ID.NEW,R.ITSS.SCB.BR.SLIPS,FN.F.ITSS.SCB.BR.SLIPS,ERROR.SCB.BR.SLIPS)
BR.EXIT=R.ITSS.SCB.BR.SLIPS<SCB.BS.DRAWER>
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.BR.SLIPS':@FM:SCB.BS.CURRENCY,ID.NEW,CUR.BR)
F.ITSS.SCB.BR.SLIPS = 'F.SCB.BR.SLIPS'
FN.F.ITSS.SCB.BR.SLIPS = ''
CALL OPF(F.ITSS.SCB.BR.SLIPS,FN.F.ITSS.SCB.BR.SLIPS)
CALL F.READ(F.ITSS.SCB.BR.SLIPS,ID.NEW,R.ITSS.SCB.BR.SLIPS,FN.F.ITSS.SCB.BR.SLIPS,ERROR.SCB.BR.SLIPS)
CUR.BR=R.ITSS.SCB.BR.SLIPS<SCB.BS.CURRENCY>
    END
    IF BR.EXIT THEN
        IF COMI NE CUR.BR THEN
            ETEXT = "�� ���� �������"
            CALL STORE.END.ERROR
        END
    END

**------------ 2011/11/15---------------
    IF COMI EQ 'EGP'  THEN
        E = "��� �� ���� ���� ������ "  ; CALL ERR  ; MESSAGE = 'REPEA'
    END

    ACC      = R.NEW(SCB.BS.CUST.ACCT)
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC,CUR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR,CUR1)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR1=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
    CURRENCY = COMI
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,COMI,CUR2)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,COMI,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR2=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

    IF CUR1 NE CUR2 THEN
        E = '������ ������' ; CALL ERR  ; MESSAGE = 'REPEAT'
    END


RETURN
END
