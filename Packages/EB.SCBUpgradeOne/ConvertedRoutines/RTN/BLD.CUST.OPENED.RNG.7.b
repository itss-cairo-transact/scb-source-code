* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE BLD.CUST.OPENED.RNG.7(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*-----------------------------------------*
    GOSUB INTIAL
    RETURN
*-------------------------------------------*
INTIAL:
    COMP.ID = ID.COMPANY
    FN.ACC  = 'FBNK.CUSTOMER'    ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    DATA.FLAG = 0
*----
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',LW.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LW.DAY=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

    DD   = TODAY

    DAY.NUM  = ICONV(DD,'DW')
    TOD.DAY  = OCONV(DAY.NUM,'DW')

    DD2 = TODAY
    CALL CDT('',DD2,'-1W')
    DAY.NUM2 = ICONV(DD2,'DW')
    DAY.NUM3 = OCONV(DAY.NUM2,'DW')

    DAY.MONTH = LW.DAY
    CALL LAST.WDAY(DAY.MONTH)

    TTT1 = TODAY
    TTT2 = TTT1[1,6]:"01"
    CALL CDT('',TTT2,'-1W')

    IF DAY.NUM3 GT TOD.DAY OR TOD.DAY EQ 7 THEN
        LW.DAYW = LW.DAY
        WS.D    = '-':DAY.NUM3:'C'
        CALL CDT('',LW.DAYW,WS.D)

        S.DATE   = LW.DAYW
        E.DATE   = LW.DAY
        GOSUB PROCESS
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
*----
    RETURN
*-------------------------------------------*
PROCESS:
*-------
    T.SEL  = "SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":S.DATE:" AND CONTACT.DATE LE ":E.DATE
    T.SEL := " AND COMPANY.BOOK EQ ":COMP.ID:" AND TEXT UNLIKE BR... BY CONTACT.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    HH = 2
    IF SELECTED THEN
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "NE"
        ENQ<4,1> = S.DATE :"*": E.DATE

        FOR II = 1 TO SELECTED
            ENQ<2,HH> = "@ID"
            ENQ<3,HH> = "EQ"
            ENQ<4,HH> = KEY.LIST<II>
            HH = HH + 1
        NEXT II
    END ELSE
        ENQ<2,1> = "@ID"
        ENQ<3,1> = "EQ"
        ENQ<4,1> = "DUMMY"
    END
    RETURN
*----------------------------------------------------
END
