* @ValidationCode : MjoxMTkxNDQxMzUwOkNwMTI1MjoxNjQwNjg2NzQ0OTQ1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:19:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE BLD.FT.B99.ALL(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*------------------------------------------
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
    COMP = ID.COMPANY
    DATD = TODAY

    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT = ""
    CALL OPF(FN.FT,F.FT)

    FN.AC = "FBNK.ACCOUNT"  ; F.AC = ""
    CALL OPF(FN.AC,F.AC)

    T.SEL  = "SELECT ":FN.FT:" WITH CREDIT.COMP.CODE EQ EG0010099"
    T.SEL := " OR DEBIT.COMP.CODE EQ EG0010099 BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*==============================================================
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
*Line [ 49 ] Convert KE.LIST<II> into KE.LIST - ITSS - R21 Upgrade - 2021-12-23
            CALL F.READ(FN.FT,KE.LIST,R.FT,F.FT,ETEXT)
            CR.COMP = R.FT<FT.CREDIT.COMP.CODE>
            DR.COMP = R.FT<FT.DEBIT.COMP.CODE>

            CR.ACC  = R.FT<FT.CREDIT.ACCT.NO>
            CALL F.READ(FN.AC,CR.ACC,R.AC,F.AC,E.AC)
            CR.CAT = R.AC<AC.CATEGORY>

            DR.ACC  = R.FT<FT.IN.DEBIT.ACCT.NO>
            CALL F.READ(FN.AC,DR.ACC,R.AC.1,F.AC,E.AC.1)
            DR.CAT = R.AC.1<AC.CATEGORY>

            IF CR.COMP EQ DR.COMP THEN
                IF CR.CAT NE '1002' OR DR.CAT NE '1002' THEN
                    ENQ<2,II> = '@ID'
                    ENQ<3,II> = 'EQ'
                    ENQ<4,II> = KEY.LIST<II>
                END
            END
        NEXT II
    END ELSE
        ENQ<2,2> = '@ID'
        ENQ<3,2> = 'EQ'
        ENQ<4,2> = 'DUMMY'
    END
RETURN
END
