* @ValidationCode : Mjo0MDkzMDc0ODE6Q3AxMjUyOjE2NDE3MzY3MjEyNTc6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:58:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
*-----------------------------------------------------------------------------
* <Rating>474</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE ACCT.DATA.CATEGORY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Comment $INCLUDE I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INDUSTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SECTOR
    $INCLUDE I_F.CATEGORY
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB CALLDB
    PRINT " ������� = " : SELECTED
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
RETURN
*==============================================================
INITIATE:
    REPORT.ID='ACCT.DATA.CATEGORY'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    T.SEL = "SELECT FBNK.ACCOUNT WITH DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE> :" BY CATEGORY"
*T.SEL = "SELECT FBNK.ACCOUNT WITH @ID EQ 0121264010120601 OR @ID EQ 0130209210120101 OR @ID EQ 0121116520120102 OR @ID EQ 0121270310120601 BY CATEGORY"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*   TEXT = SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
        IF NUM(KEY.LIST<I>) THEN
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,R.AC<AC.CATEGORY>,CATEGDESC)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,R.AC<AC.CATEGORY>,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEGDESC=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
            CATEG = R.AC<AC.CATEGORY>
            YY<1,1> = CATEGDESC
            PRINT YY<1,1>
            Y = 0
            FOR Z = I TO SELECTED
                CALL F.READ(FN.AC,KEY.LIST<Z>,R.AC,F.AC,E1)
                CATEG2 =  R.AC<AC.CATEGORY>
                IF CATEG # CATEG2 THEN
                    GOSUB ZZZZ
                    RETURN
                END
                ACCT.NO = KEY.LIST<Z>
                CUST.NO = R.AC<AC.CUSTOMER>
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.AC<AC.CUSTOMER>,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,R.AC<AC.CUSTOMER>,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
                WORK.BALANCE  = R.AC<AC.WORKING.BALANCE>
                Y = Y + 1
                YY = SPACE(120)
                YY<1,2>[1,10] = CUST.NO
                YY<1,2>[20,35] = CUST.NAME
                YY<1,2>[65,20] = ACCT.NO
                YY<1,2>[105,15] = WORK.BALANCE
                IF I # SELECTED THEN    ;*SELECTED THEN
*                YY<1,3>[1,70] = STR('-',120)
                END ELSE
                    YY<1,3>[1,70] = STR('=',120)
                END
                IF I # SELECTED  THEN
                    PRINT YY<1,2>
                END
                IF I # SELECTED  THEN
                    PRINT YY<1,3>
                END

            NEXT Z
ZZZZ:
            IF I # SELECTED  THEN
                PRINT " ������� = " :Y
                YY<1,4>[1,70] = STR('*',120)
                PRINT YY<1,4>
                IF Z # SELECTED THEN    ;*SELECTED THEN
                    I = Z - 1
                END ELSE
                    I = Z
                END
            END
        END
    NEXT I
*    TEXT = I ; CALL REM
*    IF I = SELECTED  THEN
*       PRINT " ������� = " : SELECTED
*  END
RETURN

*===============================================================
PRINT.HEAD:
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
*    PR.HD :="'L'":SPACE(50):" ���� ��������� ���� ���� ������ "
    PR.HD :="'L'":SPACE(50):" ������ ��������� ���� ���� ������ "

    PR.HD :="'L'":SPACE(45):STR('_',45)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(15):" ��� ������" :SPACE(25):" ��� ������":SPACE(25):" ������ ������"
    PR.HD :="'L'":STR('_',120)

    HEADING PR.HD
RETURN
*==============================================================
END
