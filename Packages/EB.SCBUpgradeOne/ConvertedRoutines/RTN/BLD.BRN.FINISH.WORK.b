* @ValidationCode : MjotMjk1MzU1NjEyOkNwMTI1MjoxNjQwNjg0NjUzNzI2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:44:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeOne  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeOne
*DONE
SUBROUTINE BLD.BRN.FINISH.WORK(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Comment $INCLUDE I_F.SCB.FINISH.WORK - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.FINISH.WORK
*-------------------------------------------------------------------------
    FN.WOR = 'F.SCB.FINISH.WORK' ; F.WOR = ''
    CALL OPF(FN.WOR,F.WOR)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    WS.DAT = TODAY

    T.SEL = "SELECT ":FN.COM:" WITH @ID NE 'EG0010077' AND @ID NE 'EG0010088' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.COM,KEY.LIST<I>,R.COM,F.COM,E1)
            T.SEL1 = "SELECT ":FN.WOR:" WITH BRANCH.CODE EQ ":KEY.LIST<I>:" AND FINISH.DATE EQ ":WS.DAT
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF NOT(SELECTED1) THEN
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = KEY.LIST<I>
            END ELSE
                ENQ<2,I> = '@ID'
                ENQ<3,I> = 'EQ'
                ENQ<4,I> = 'DUMMY'
            END

        NEXT I
    END
RETURN
END
