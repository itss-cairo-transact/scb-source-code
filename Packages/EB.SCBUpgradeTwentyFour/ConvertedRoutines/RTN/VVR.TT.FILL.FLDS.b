* @ValidationCode : MjotMTgzMzQyNDgzMjpDcDEyNTI6MTY0ODU2MzYzMzk2NjpNb3VuaXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Mar 2022 16:20:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyFour
*DONE
** ----- NESSREEN AHMED 22/3/2015-----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.TT.FILL.FLDS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS

    ETEXT = ''    ; TEXT = ''

    IF MESSAGE =  '' THEN
        IF COMI # R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BNK.CUSTOMER> THEN
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BK.CUS> = ''
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ARABIC.NAME> = ''
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.DPST.NSN.NO> = ''
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHK.FLG> = ''
            CALL REBUILD.SCREEN
        END
        IF COMI = 'NO' THEN
            T((TT.TE.LOCAL.REF)<1,TTLR.BK.CUS>)<3> = 'NOINPUT'
            CALL REBUILD.SCREEN
        END

    END
    IF MESSAGE = 'VAL' THEN
        IF (R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BK.CUS> = '') AND (R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BNK.CUSTOMER> = 'YES') THEN
            ETEXT = "��� ����� ��� ������"
            CALL STORE.END.ERROR
        END
        IF (R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BK.CUS> # '') AND (R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BNK.CUSTOMER> = 'NO') THEN
            ETEXT = "��� ����� ������ ��� ����"
            CALL STORE.END.ERROR
        END

    END
RETURN
END
