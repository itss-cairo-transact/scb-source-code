* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***ZEAD MOSTAFA***
*-----------------------------------------------------------------------------
* <Rating>639</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUS.MNEMONIC

*DEFAULT THE FIRST 2 CHR FROM BRANCH NAME AND SERIAL

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    ETEXT = ""

    IF R.NEW(EB.CUS.MNEMONIC) = '' THEN
        IF LEN(ID.NEW) = 7 THEN OLDCUST = ID.NEW[2,LEN(ID.NEW)]
        IF LEN(ID.NEW) > 7 THEN OLDCUST = ID.NEW[3,LEN(ID.NEW)]
*  TEXT = ID.NEW ; CALL REM


        IF R.USER<EB.USE.DEPARTMENT.CODE> = "1" THEN
            R.NEW(EB.CUS.MNEMONIC)='CA':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "2" THEN
            R.NEW(EB.CUS.MNEMONIC)='HE':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "3" THEN
            R.NEW(EB.CUS.MNEMONIC)='GI':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "4" THEN
            R.NEW(EB.CUS.MNEMONIC)='MO':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "5" THEN
            R.NEW(EB.CUS.MNEMONIC)='MA':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "6" THEN
            R.NEW(EB.CUS.MNEMONIC)='AB':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "7" THEN
            R.NEW(EB.CUS.MNEMONIC)='AR':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "9" THEN
            R.NEW(EB.CUS.MNEMONIC)='FO':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "10" THEN
            R.NEW(EB.CUS.MNEMONIC)='NA':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "11" THEN
            R.NEW(EB.CUS.MNEMONIC)='DO':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "12" THEN
            R.NEW(EB.CUS.MNEMONIC)='SP':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "13" THEN
            R.NEW(EB.CUS.MNEMONIC)='GR':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "14" THEN
            R.NEW(EB.CUS.MNEMONIC)='OC':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "15" THEN
            R.NEW(EB.CUS.MNEMONIC)='UC':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "20" THEN
            R.NEW(EB.CUS.MNEMONIC)='AL':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "21" THEN
            R.NEW(EB.CUS.MNEMONIC)='BU':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "22" THEN
*Line [ 94 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            R.NEW(EB.CUS.MNEMONIC)='@SM':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "30" THEN
            R.NEW(EB.CUS.MNEMONIC)='SI':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "35" THEN
            R.NEW(EB.CUS.MNEMONIC)='SH':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "40" THEN
            R.NEW(EB.CUS.MNEMONIC)='IS':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = "50" THEN
            R.NEW(EB.CUS.MNEMONIC)='SU':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = 60 THEN
            R.NEW(EB.CUS.MNEMONIC)='TA':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = 70 THEN
            R.NEW(EB.CUS.MNEMONIC)='RA':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = 90 THEN
            R.NEW(EB.CUS.MNEMONIC)='SA':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = 23 THEN
            R.NEW(EB.CUS.MNEMONIC)='AM':OLDCUST
        END
        IF R.USER<EB.USE.DEPARTMENT.CODE> = 31 THEN
            R.NEW(EB.CUS.MNEMONIC)='DM':OLDCUST
        END

        IF R.USER<EB.USE.DEPARTMENT.CODE> = 77 THEN
            R.NEW(EB.CUS.MNEMONIC)='TS':OLDCUST
        END

        IF R.USER<EB.USE.DEPARTMENT.CODE> = 55 THEN
            R.NEW(EB.CUS.MNEMONIC)='UT':OLDCUST
        END
    END
    RETURN
END
