* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.DOC.CO.VERSION


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*IF V$FUNCTION EQ 'A' THEN
    COM.CODE = ID.COMPANY
    CO.CODE  = R.NEW(DOC.PRO.CO.CODE)

    IF CO.CODE NE '' THEN
        IF CO.CODE NE COM.CODE THEN
            E='��� ������� �� ��� �����'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
*** SCB UPG 20160623 - E
            CALL STORE.END.ERROR
        END
    END
    IF R.NEW(DOC.PRO.RESERVED5) NE '' THEN
        IF PGM.VERSION # R.NEW(DOC.PRO.RESERVED5) THEN
            E = '��� ������� �� ��� ���� �������'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
*** SCB UPG 20160623 - E
            CALL STORE.END.ERROR
        END
    END
*END
    RETURN
END
