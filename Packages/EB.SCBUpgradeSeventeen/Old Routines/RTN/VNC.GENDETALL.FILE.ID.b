* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.GENDETALL.FILE.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENDETALL.FILE
*----------------------------------------------
    R.NEW(SDAF.LINE.ID) = ID.NEW

    WS.ID.TXT     = 'GENDETALL'
    WS.ID.TXT.CHK = FIELD(ID.NEW,".",1)
    WS.ID.NO      = FIELD(ID.NEW,".",2)
    CHK.ID        = LEN(WS.ID.NO)

    IF WS.ID.TXT.CHK NE 'GENDETALL' THEN
        E = "ERROR : THE ID MUST BE <GENDETALL><.><Four Digits>"
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
    END

    IF CHK.ID NE 4 THEN
        E = "ERROR : THE ID MUST BE <GENDETALL><.><Four Digits>"
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
    END

*----------------------------------------------
    RETURN
END
