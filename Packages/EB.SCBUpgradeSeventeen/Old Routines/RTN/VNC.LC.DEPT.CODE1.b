* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
************** INGY-SCB 06/04/2003**************
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LC.DEPT.CODE1
*A ROUTINE TO CHECK
*IF THE EB.USE.DEPARTMENT.CODE #'99' THEN
* IF THE  TF.LC.ACCOUNT.OFFICER # EB.USE.DEPARTMENT.CODE DISPLAY ERROR
* ELSE PUT IN LINK.DATA<TNO> PGM.VERSION
* IF TF.LC.LC.TYPE = 'LIP' THEN EMPTY TF.LC.LC.TYPE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

    IF V$FUNCTION='I' THEN
************NESSREEN-SCB 08/07/2006*********
*    R.NEW(TF.LC.ISSUE.DATE) = TODAY
********************************************
        LINK.DATA<TNO> = ''
        IF R.NEW(TF.LC.ACCOUNT.OFFICER) THEN
            IF R.NEW(TF.LC.ACCOUNT.OFFICER) # R.USER<EB.USE.DEPARTMENT.CODE> AND R.USER<EB.USE.DEPARTMENT.CODE> # '99' THEN
                E = 'THIS.OPER.FROM.OTHER.BRANCH' ;* CALL ERR ; MESSAGE = 'REPEAT'
            END
        END ELSE
            R.NEW(TF.LC.ACCOUNT.OFFICER) = R.USER<EB.USE.DEPARTMENT.CODE>
            LINK.DATA<TNO> = PGM.VERSION
            R.NEW(TF.LC.LOCAL.REF)<1,LCLR.VERSION.NAME> = PGM.VERSION
            IF R.NEW(TF.LC.LC.TYPE) = 'LIP' THEN
                R.NEW(TF.LC.LC.TYPE) = ''

            END
        END
    END
*TEXT = LINK.DATA<TNO> ; CALL REM

    RETURN
END
