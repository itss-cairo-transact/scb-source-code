* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
** ----- 15/01/2009 NESSREEN AHMED -----
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CU.CHECK.CR.STAT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN

        FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
        FN.ACCOUNT = 'F.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = '' ; RETRY2 = '' ; E2 = ''

        CALL OPF(FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ(FN.CUSTOMER, ID.NEW, R.CUSTOMER, F.CUSTOMER, E1)
        LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>

        CR.STAT = LOCAL.REF<1,CULR.CREDIT.STAT>
        IF CR.STAT NE '' THEN
            E = '�� ��� ������� �������� ������ '
            CALL ERR ; MESSAGE = 'REPEAT'
        END
***********************************
    END
    RETURN
END
