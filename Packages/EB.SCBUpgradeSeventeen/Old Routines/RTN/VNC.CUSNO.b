* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*******************************NI7OOOOOOOOOOOOOOOOOOO*********************
*-----------------------------------------------------------------------------
* <Rating>-42</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUSNO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LC    ='FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    FN.LC.HIS    ='FBNK.LETTER.OF.CREDIT$HIS' ; F.LC.HIS = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.LC.HIS,F.LC.HIS)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    FN.FT    ='FBNK.FUNDS.TRANSFER' ; F.FT = ''
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS' ; F.FT.HIS = ''
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    XX = O.DATA
    YY = O.DATA[1,2]
    IF YY EQ 'LD' THEN
        T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH @ID EQ ":XX
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CALL F.READ(FN.LD,XX,R.LD,F.LD,E2)
            CUS.NO = R.LD<LD.CUSTOMER.ID>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA = CUST.NAME
        END ELSE
            IF SELECTED EQ 0 THEN
                WW = XX:'...'
* T.SEL4 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":WW
* CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
                DD = XX:';1'
                CALL F.READ(FN.LD.HIS,DD,R.LD.HIS,F.LD.HIS,E2)
                CUS.NO = R.LD.HIS<LD.CUSTOMER.ID>
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL.REF)
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                O.DATA = CUST.NAME
            END
        END
    END
    IF YY EQ 'FT' THEN
        ZZ = XX:';1'
        CALL F.READ(FN.FT.HIS,ZZ,R.FT.HIS,F.FT.HIS,E2)
        AC.NO     = R.FT.HIS<FT.DEBIT.ACCT.NO>
        IF (AC.NO[1,2] NE 'PL' OR AC.NO[1,3] NE 'EGP') THEN
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSNO)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = CUST.NAME
        END
        IF AC.NO[1,2] EQ 'PL' THEN
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,AC.NO[3,5],CATEG.NAME)
            O.DATA    = CATEG.NAME
        END
    END
    IF YY EQ 'TF' THEN
        MM=XX[1,12]
        CALL F.READ(FN.LC,MM,R.LC,F.LC,E22)
        IF NOT(E22) THEN
            LC.CUS    = R.LC<TF.LC.APPLICANT.CUSTNO>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LC.CUS,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = CUST.NAME
        END
        IF E22 THEN
            NN = XX[1,12]:';1'
            CALL F.READ(FN.LC.HIS,NN,R.LC.HIS,F.LC.HIS,E33)
            LC.CUS.H    = R.LC.HIS<TF.LC.APPLICANT.CUSTNO>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LC.CUS.H,LOCAL.REF)
            CUST.NAME.HIS = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = CUST.NAME.HIS
        END
    END
    RETURN
END
