* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>929</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.CLAIM.DATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*******************************************************************************************************
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ;F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,READ.ERR)
    LG.CLAIMDATE   = R.LD<LD.CHRG.CLAIM.DATE>
    LG.CHRG.CODE   = R.LD<LD.CHRG.CODE>
    LG.CHRG.AMT = R.LD<LD.CHRG.AMOUNT>
    LG.CHRG.BOOK = R.LD<LD.CHRG.BOOKED.ON>

    IF LG.CHRG.CODE EQ '' THEN
        IF LG.CLAIMDATE NE '' OR LG.CHRG.BOOK NE '' THEN
            R.NEW(LD.CHRG.CLAIM.DATE) = ''
            R.NEW(LD.CHRG.BOOKED.ON)       = ''
        END
    END
*******************************************************************************************************
    RETURN
END
