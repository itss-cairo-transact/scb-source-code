* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
**** RANIA 27/02/2003 ****
*-----------------------------------------------------------------------------
* <Rating>90</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LG.CUS.LIMIT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS


MYLIMIT = ID.NEW :".":"0007010":"...."
  MYLIMIT1 = "....":"0010400":".":"...":ID.NEW


  T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
   T.SEL = "SSELECT FBNK.LIMIT WITH @ID LIKE ":MYLIMIT
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

     IF NOT(SELECTED) THEN

       T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        T.SEL = "SSELECT FBNK.LIMIT WITH @ID LIKE ":MYLIMIT1
         CALL EB.READLIST(T.SEL,KEY.LIST1,"",SELECTED1,ERR.MSG )
           R.NEW(SCB.LGCS.LIMIT.NO) = KEY.LIST1<1>

     END ELSE R.NEW(SCB.LGCS.LIMIT.NO) = KEY.LIST<1>

RETURN
END
