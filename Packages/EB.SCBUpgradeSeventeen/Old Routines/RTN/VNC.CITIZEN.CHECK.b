* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.CITIZEN.CHECK
**----------------------------------
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ENT.TODAY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    COMP = ID.COMPANY

    IF V$FUNCTION = 'I' THEN

        ACCT.ID   = ''
        FROM.DATE = ''
        END.DATE  = ''
        FROM.DATE = ''
        CC.SEL = '' ; K.LIST = '' ; SELECTED = '' ; ER.MSG = ''

        FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
        CALL OPF(FN.STE,F.STE)


        FN.AET = 'F.ACCT.ENT.TODAY' ; F.AET = '' ; R.AET = '' ; ER.AET = ''
        CALL OPF(FN.AET,F.AET)
                     *    DEBUG
        FROM.DATE = TODAY
        END.DATE  = TODAY
        ACCT.ID   = 'EGP1611400080099'
*??        CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
        CALL F.READ(FN.AET,ACCT.ID,ID.LIST,F.AET,ER.AET)
        LOOP
            REMOVE STE.ID FROM ID.LIST SETTING POS
        WHILE STE.ID:POS
            IF STE.ID THEN
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.CUR = R.STE<AC.STE.CURRENCY>
                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                STE.DAT = R.STE<AC.STE.BOOKING.DATE>
                STE.VAL = R.STE<AC.STE.VALUE.DATE>
                STE.TXN = R.STE<AC.STE.TRANSACTION.CODE>
                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
                STE.FCY = R.STE<AC.STE.AMOUNT.FCY>
                STE.OUR = R.STE<AC.STE.OUR.REFERENCE>
                STE.COM = R.STE<AC.STE.COMPANY.CODE>
                IF STE.REF[1,2] EQ 'IN' AND STE.COM EQ 'EG0080099' THEN
                    E = '�� ����� ������� �� ��� ' ;  CALL STORE.END.ERROR
                END

            END
        REPEAT
    END
