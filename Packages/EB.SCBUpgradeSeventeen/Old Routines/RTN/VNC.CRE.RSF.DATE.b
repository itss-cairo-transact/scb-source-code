* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE

*-----------------------------------------------------------------------------
* RIHAM YOUSSEF 27/7/2017
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CRE.RSF.DATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG

    IF V$FUNCTION = 'I' THEN
        TDR = TODAY
        CALL CDT('',TDR,"+1W")
        R.NEW(FT.CREDIT.VALUE.DATE)  = TDR

        USER = OPERATOR
        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,USER,DEP.NO)
        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEP.NO,DEP.NAME)
        DESC.DEP.NAME = FIELD(DEP.NAME,".",2)
        R.NEW(FT.ORDERING.CUST)     = DESC.DEP.NAME
        R.NEW(FT.DEBIT.VALUE.DATE)  = TODAY
        R.NEW(FT.DEBIT.CURRENCY)    = 'EGP'
        R.NEW(FT.CREDIT.CURRENCY)   = 'EGP'
        R.NEW(FT.CREDIT.ACCT.NO)    = '9949990010321401'
        R.NEW(FT.CREDIT.CUSTOMER)   = '99499900'

    END

    RETURN
END
