* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.VER.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'A' THEN
        VERSION.NAME = R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
        IF VERSION.NAME NE '' THEN
            HHH  = ",SCB.OPEN.OFS"
            HHHH = ",SCB.OPEN.TERM.11"
            IF PGM.VERSION # VERSION.NAME AND (VERSION.NAME # HHH AND VERSION.NAME # HHHH ) THEN
****UPDATED BY NESSREEN AHMED 28/7/2008*****************
                E = '��� ������� �� ��� ���� �������'
*** SCB UPG 20160621 - S
* CALL ERR  ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
************END OF UPDATED********************************
            END
        END
    END ELSE
*********************UPDATED BY RIHAM YOUSSIF***28/10/2010*******
        IF V$FUNCTION = 'R' THEN
*********************UPDATED BY MAHMOUD ***12/1/2011*******
            IF PGM.VERSION # ',SCB.ELIQ.REV.DEP' THEN
*********************END OF UPDATE*************************
                E = 'NO FUNCTION REVERSE' ;
*** SCB UPG 20160621 - S
* CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
            END
        END
    END
*************************************************************
    RETURN
END
