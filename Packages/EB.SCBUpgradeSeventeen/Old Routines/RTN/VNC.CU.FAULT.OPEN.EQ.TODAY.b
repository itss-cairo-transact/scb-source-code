* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------

** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VNC.CU.FAULT.OPEN.EQ.TODAY

* IF THE FUNC IS INPUT THEN THE CONTACT DATE DEFAULTED TO TODAY
* DEFAULT CUSTOMER.LIABILITY WITH ID

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    ETEXT = ""

    IF V$FUNCTION = 'I' THEN
        R.NEW( EB.CUS.CONTACT.DATE) = TODAY
        IF NOT (R.NEW(EB.CUS.CUSTOMER.LIABILITY)) THEN R.NEW(EB.CUS.CUSTOMER.LIABILITY) = ID.NEW
        DUMMYDAT=TODAY : 'M1201'
        R.NEW( EB.CUS.REVIEW.FREQUENCY) = DUMMYDAT
    END

    RETURN
END
