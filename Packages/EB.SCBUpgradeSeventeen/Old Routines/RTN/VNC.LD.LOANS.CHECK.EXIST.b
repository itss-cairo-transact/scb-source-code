* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>94</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.LOANS.CHECK.EXIST
* A Subroutine to make sure that the Loan ID is an existing & from Loans Contract
* CHECK IF CATEORY OF LOANS NOT EQ 21050 , 21051 , 21052 THEN ERROR MESSAGE
* CHECK IF LIQ.MODE # SEMI AUTOMATIC THEN ERROR MESSAGE
* Also to default VALUE.DATE by Today's date

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.PARMS1

    IF V$FUNCTION = 'I' THEN
        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CUSTOMER.ID,ID.NEW,MYDUMMY)
        IF ETEXT THEN E = 'MUST BE AN EXSISTING CONTRACT' ;CALL ERR ; MESSAGE = 'REPEAT'

*        CATEG = R.NEW(LD.CATEGORY)
*       CALL DBR('SCB.LOANS.PARMS':@FM:SCB.LO.PAR.DESCRIPTION,CATEG,MYID)
*      IF ETEXT THEN E = 'This is a Deposit Contract' ;CALL ERR ;MESSAGE = 'REPEAT'

*     IF R.NEW(LD.LIQUIDATION.MODE) # 'SEMI-AUTOMATIC' THEN
*        E = 'YOU CANNOT MAKE PARTIAL PAYMENT' ;CALL ERR ;MESSAGE = 'REPEAT'
*   END
    END
********************************************************************************

* R.NEW (LD.AMT.V.DATE) = TODAY

    RETURN
END
