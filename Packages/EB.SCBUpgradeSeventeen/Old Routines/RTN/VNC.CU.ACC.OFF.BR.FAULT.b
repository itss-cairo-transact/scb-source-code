* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------

** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VNC.CU.ACC.OFF.BR.FAULT

*VNC ROUTINE DEFAULT THE ACCOUNT OFFICER EQ DEPARTMANT CODE (BRANCH NO.)
*FROM USER OF THE BRANCH

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    ETEXT = ""
**************UPDATED BY RIHAM R15 *************
**** IF V$FUNCTION = 'I' THEN R.NEW( EB.CUS.ACCOUNT.OFFICER) = R.USER< EB.USE.DEPARTMENT.CODE>
    CUS.BR = R.NEW(EB.CUS.COMPANY.BOOK)[8,2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")
**NA***UPDATED BY NESSREEN AHMED 16/7/2019*******************
**NA***IF V$FUNCTION = 'I' THEN AC.OFICER = R.USER< EB.USE.DEPARTMENT.CODE>

*-- EDIT BY NESSMA 2016/08/22 --*
**NA*** IF V$FUNCTION = 'I' THEN
**NA*** R.NEW(EB.CUS.ACCOUNT.OFFICER) = R.USER< EB.USE.DEPARTMENT.CODE>
    ID.BRANCH.N = ID.COMPANY[2]
    ID.BRANCH   = TRIM(ID.BRANCH.N,"0","L")
        R.NEW(EB.CUS.ACCOUNT.OFFICER) = ID.BRANCH
**NA***END OF UPDATE 16/7/2019********************************
**NA*** END
*-- END EDIT --*
********************************************************


    RETURN
END
