* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
************ABEER AS OF 2019-01-03    
SUBROUTINE VNC.IMAGE.CHECK

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE

    IF V$FUNCTION ='I' OR V$FUNCTION ='A' OR V$FUNCTION ='R' THEN
***UPDATED BY NESSREEN AHMED 1/10/2019*************
*** CALL DBR('IM.DOCUMENT.IMAGE':@FM:IM.DOC.DEPT.CODE,COMI,IM.DPT.COD)
*** CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,OPERATOR,USR.DEP)
    CALL DBR('IM.DOCUMENT.IMAGE':@FM:IM.DOC.CO.CODE,COMI,IM.DPT.COD1)
    IM.CO.COD = IM.DPT.COD1[2]
    IM.DPT.COD = TRIM(IM.CO.COD,"0","L")
    ID.BRANCH.N = ID.COMPANY[2]
    USR.DEP   = TRIM(ID.BRANCH.N,"0","L")
***END OF UPDATE 1/10/2019***************************
******************************************************************************
       IF IM.DPT.COD NE USR.DEP THEN
            E = '��� ������ �� ��� ���'
            CALL STORE.END.ERROR
        END
    END
    RETURN
END
