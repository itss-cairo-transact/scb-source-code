* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>500</Rating>
*-----------------------------------------------------------------------------

*A Routine To Perform Version Defaults ,To Prevent Reverse
*To Check It Is Existing Record,
*To Check That One action ON LG is Allowed Daily,
*To Check That Authorization Happens From Same Version .

    SUBROUTINE VNC.LG.AMBNF.NEW

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*************************************************************************************************************
    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            LG.K      = MYLOCAL<1,LDLR.LG.KIND>
            END.COM.D = MYLOCAL<1,LDLR.END.COMM.DATE>
            IF LG.K EQ 'BB' THEN
                E ='BB - NOT.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            END

**   IF END.COM.D GT TODAY  THEN
**      E ='END.COM.D.must.be.LT.today' ; CALL ERR ; MESSAGE = 'REPEAT'
**   END

            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.FIN.MAT.DATE,ID.NEW,FIN.DATE)

            IF FIN.DATE LT TODAY  THEN
                E ='FIN.MAT.DATE.must.be.GT.today' ; CALL ERR ; MESSAGE = 'REPEAT'
            END

            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)

            IF ETEXT THEN
                E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                IF (MYCATEG LT '21096' OR MYCATEG GT '21099') THEN E ='Not.LG.Category'    ; CALL ERR ; MESSAGE = 'REPEAT'
                CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.STATUS,ID.NEW,MYSTATUS)
                IF MYSTATUS EQ 'LIQ' THEN
                    E ='This.Is.Liquidated.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
                END   ELSE
                    IF R.NEW(LD.DATE.TIME)=TODAY THEN E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
                    IF R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> NE '' THEN
                        R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
                    END
                END
            END
        END
    END
**************************************************************************************************************
    IF V$FUNCTION = 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        IF MYVERNAME NE PGM.VERSION THEN E='You.Must.Authorize.This.Record.From & ' : MYVERNAME; CALL ERR ; MESSAGE = 'REPEAT'
    END

    RETURN
END
