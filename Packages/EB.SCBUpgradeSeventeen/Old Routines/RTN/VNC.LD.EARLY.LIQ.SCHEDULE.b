* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-32</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.EARLY.LIQ.SCHEDULE

********PREPARED BY BAKRY*************************************************
*
*CALC INTERSET PAIED AND ACTUAL INTEREST WILL BE GIVEN
*
**************************************************************************
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.LD.ACC.BAL
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

***************************************************************************
    CALL DBR('LD.SCHEDULE.DEFINE':@FM:LD.SD.CURRENCY,ID.NEW,MYCHK)
    IF ETEXT THEN
*        ETEXT ='��� ������� ���� ������'  ; CALL STORE.END.ERROR
        TEXT ='��� ������� ���� ������ ���� ������� ���� ������� �������'  ; CALL REM ; RETURN
    END
*    IF R.NEW(LD.CURRENCY) NE 'EGP' THEN
*        TEXT ='��� ������� ���� ������� ������'  ; CALL REM ; RETURN
*    END
***************************************************************************
    CHK.FLG = 0
    R.NEW(LD.FIN.MAT.DATE) = TODAY
*   R.NEW(LD.INTEREST.BASIS)= "E"
*****************************************************************************
    TEMP.EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
    TEMP.ISSUE.DATE   = R.NEW(LD.VALUE.DATE)
    TEMP.DAY = 'C'
    CALL CDD("",TEMP.ISSUE.DATE,TEMP.EXP.DATE , TEMP.DAY)
    AMT = R.NEW(LD.AMOUNT)
    CURR = R.NEW(LD.CURRENCY)
    BEGIN CASE
****************************************************************************
    CASE TEMP.DAY LT 7
        CHK.FLG = 1
        R.NEW(LD.NEW.INT.RATE)= "0"
        R.NEW(LD.INT.RATE.V.DATE) = R.NEW(LD.VALUE.DATE)
        R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS> = TEMP.DAY
*****************************************************************************
    CASE TEMP.DAY LT 30 AND CURR NE  "EGP"
        CHK.FLG = 1
        R.NEW(LD.NEW.INT.RATE)= "0"
        R.NEW(LD.INT.RATE.V.DATE) = R.NEW(LD.VALUE.DATE)
        R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS> = TEMP.DAY
*****************************************************************************
    CASE TEMP.DAY GE 7 AND TEMP.DAY LT 30 AND CURR EQ "EGP"
        IF  CURR EQ "EGP" THEN
            IF R.NEW(LD.AMOUNT) GE '100000' THEN
                CATEG = "21001":TEMP.ISSUE.DATE
                GOSUB GET.RATE.DEPOSIT
            END
        END
*****************************************************************************
    CASE TEMP.DAY GE 30 AND TEMP.DAY LT 60
        CATEG = "21003":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
*****************************************************************************
    CASE TEMP.DAY GE 60 AND TEMP.DAY LT 90
        IF R.NEW(LD.CURRENCY) EQ "EGP" THEN
            CATEG = "21003":TEMP.ISSUE.DATE
            GOSUB GET.RATE.DEPOSIT
        END ELSE
            CATEG = "21004":TEMP.ISSUE.DATE
            GOSUB GET.RATE.DEPOSIT
        END
******************************************************************************
    CASE TEMP.DAY GE 90 AND TEMP.DAY LT 180
        CATEG = "21005":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
******************************************************************************
    CASE TEMP.DAY GE 180 AND TEMP.DAY LT 360
        CATEG = "21006":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
***************************************************
    CASE TEMP.DAY GE 360 AND TEMP.DAY LT 720
        CATEG = "21007":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
***************************************************
    CASE TEMP.DAY GE 720 AND TEMP.DAY LT 1080
        CATEG = "21008":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
***************************************************
    CASE TEMP.DAY GE 1080
        CATEG = "21009":TEMP.ISSUE.DATE
        GOSUB GET.RATE.DEPOSIT
***************************************************
    CASE OTHERWISE
        RETURN
    END CASE
***************************************************
    CUSS = R.NEW(LD.CUSTOMER.ID)
    CUS = LEN(R.NEW(LD.CUSTOMER.ID))
    IF CUS EQ 7 THEN
        CLASS = CUSS[2,1]
    END ELSE
        CLASS = CUSS[3,1]
    END
*************************************************************
    IF CLASS NE 1 AND R.NEW(LD.CUSTOMER.ID) NE "1304955" THEN
        R.NEW(LD.NEW.SPREAD) = 0
        R.NEW(LD.SPREAD.V.DATE) = R.NEW(LD.VALUE.DATE)
    END
    IF CHK.FLG EQ 0 THEN
        GOSUB GET.SCHEDUAL
    END
    RETURN
*************************************************************
GET.RATE.DEPOSIT:
*****************
    F.LD.LEVEL = '' ; FN.LD.LEVEL = 'F.SCB.LD.TYPE.LEVEL' ; R.LD.LEVEL = ''
    CALL OPF(FN.LD.LEVEL,F.LD.LEVEL)
    CALL F.READ(FN.LD.LEVEL, CATEG, R.LD.LEVEL, F.LD.LEVEL, ETEXT)
    LOCATE CURR IN R.LD.LEVEL<LDTL.CURRENCY,1> SETTING POS THEN
        VV = R.LD.LEVEL<LDTL.CURRENCY,1>
        EE = R.LD.LEVEL<LDTL.RATE,POS>
*Line [ 147 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        TEMP.COUNT =  DCOUNT (R.LD.LEVEL<LDTL.AMT.FROM,POS>,@SM)
        FOR I = 1 TO TEMP.COUNT
            AMT.FROM = R.LD.LEVEL<LDTL.AMT.FROM,POS,I>
            AMT.TO = R.LD.LEVEL<LDTL.AMT.TO,POS,I>
            IF AMT GE AMT.FROM AND AMT LE AMT.TO THEN
                INT.RATE = R.LD.LEVEL<LDTL.RATE,POS,I>
                A = R.NEW(LD.INTEREST.RATE)
                B = R.LD.LEVEL<LDTL.RATE,POS,I>
            END
            IF TEMP.COUNT EQ I AND AMT GE R.LD.LEVEL<LDTL.AMT.FROM,POS,I> THEN
                INT.RATE = R.LD.LEVEL<LDTL.RATE,POS,I>
            END
        NEXT I
    END
    RETURN
*****************************************************************************
GET.SCHEDUAL:
**************
    DATE.VAL = R.NEW(LD.VALUE.DATE)
    DATE.EXP = TODAY
*[1,6]:"01"
    CALL CDT("",DATE.EXP,'-1C')
    TEXT = DATE.EXP ; CALL REM
    SPR.RAT  = R.NEW(LD.INTEREST.SPREAD)
******************************************************************************
    CON.ID = ID.NEW:"00"
    CALL DBR('LMM.ACCOUNT.BALANCES':@FM:LD27.START.PERIOD.INT,CON.ID,MYSTART)
******************************************************************************
    IF MYSTART # TODAY THEN
        R.NEW(LD.NEW.INT.RATE) = 0
        R.NEW(LD.INT.RATE.V.DATE) =MYSTART
        R.NEW(LD.NEW.SPREAD) = 0
        R.NEW(LD.SPREAD.V.DATE) = MYSTART
    END
******************************************************************************
    LD.CAT = R.NEW(LD.CATEGORY)
******************************************************************************
*==========CALC INTREST AMOUNT PAYED ==========================================
    ACT.DAYS = "C"
    CALL CDD("",DATE.VAL,MYSTART,ACT.DAYS)
    ACT.INT.RATE = R.NEW(LD.INTEREST.RATE) + R.NEW(LD.INTEREST.SPREAD)
    ACT.INT = ((AMT*ACT.INT.RATE)*(ACT.DAYS/365)/100)
*==========CALC INTREST AMOUNT AFTER PENALTY RATE =========================
    NEW.ACT.INT.RATE = INT.RATE - 2
    DAYSSS = "C"
    CALL CDD("",DATE.VAL,DATE.EXP,DAYSSS)
    DAYSSS = DAYSSS + 1
    NEW.INT = ((AMT*NEW.ACT.INT.RATE)*(DAYSSS/365)/100)
    TEXT = "��� ������ ������ = " : NEW.ACT.INT.RATE ; CALL REM
    TEXT = "=��� ������ ��� ��� ����  " : ACT.DAYS ; CALL REM
    IF NEW.INT LT 0 AND NEW.ACT.INT.RATE LT 0 THEN NEW.INT = 0
    ACT.AMT = ACT.INT - NEW.INT
    TEXT = '������ �������= ' : DROUND(ACT.INT,2) :" ������ ������ =" : DROUND(NEW.INT,2) ; CALL REM
*============================================================================
    R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS> = DAYSSS : "  ":ACT.DAYS
*============================================================================
    IF ACT.AMT GT 0 THEN
        CALL EB.ROUND.AMOUNT ('EGP',ACT.AMT,'',"")
        R.NEW(LD.CHRG.AMOUNT) = ACT.AMT
        R.NEW(LD.CHRG.CLAIM.DATE) = TODAY
        R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT> = (AMT-ACT.AMT)
    END ELSE
        IF ACT.AMT LT 0 THEN
            AMTT = ACT.AMT * -1
            CALL EB.ROUND.AMOUNT ('EGP',AMTT,'',"")
            TEXT = "��� ������� ����� ���� �����":(AMTT) ; CALL REM
            R.NEW(LD.REIMBURSE.PRICE)= ""
            R.NEW(LD.REIMBURSE.AMOUNT) = AMTT +  R.NEW(LD.AMOUNT)
            R.NEW(LD.CHRG.AMOUNT)= "0.00"
        END ELSE
            IF R.NEW(LD.CHRG.AMOUNT) = '' THEN R.NEW(LD.CHRG.AMOUNT)= "0.00"
        END
        IF R.NEW(LD.FIN.MAT.DATE) # TODAY THEN ETEXT='��� �� ���� ����� �����'  ;CALL STORE.END.ERROR
        CALL REBUILD.SCREEN
        RETURN
************************************************************
    END
