* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1501</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.PERIOD.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP.AMT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP.P


*   TO DEFAULT FEILDS SCH.TYPE,SD.FREQUENCY,SD.DATE

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
            DEFFUN SHIFT.DATE( )
            LD.ID = '' ; LD.ID = ID.NEW
            GOSUB OPEN.FILES ; R.TERM = '' ; ER.MSG = ''
            GOSUB DELETE.FIELDS
            GOSUB PROCESS.LD.SCHEDULES

            IF ER.MSG THEN
                E = ER.MSG ; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
    END
    GOTO PROGRAM.END

*----------------------- OPEN FILES -------------------------------------------*
OPEN.FILES:

    VF.LD.LOANS.AND.DEPOSITS$NAU = ''
    CALL OPF('F.LD.LOANS.AND.DEPOSITS$NAU', VF.LD.LOANS.AND.DEPOSITS$NAU)

    RETURN
*------------------------------------------------------------------------------*
*------------------ PROCESS LD SCHEDULES --------------------------------------*
PROCESS.LD.SCHEDULES:

    CALL F.READ('FBNK.LD.LOANS.AND.DEPOSITS$NAU',
    LD.ID, R.TERM, VF.LD.LOANS.AND.DEPOSITS$NAU, ER.MSG)
    IF NOT(ER.MSG) THEN
*HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
        IF R.TERM<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE> EQ "BIDBOND" THEN
            CHRG.CODE = 4
        END
        IF R.TERM<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE> EQ "FINAL" THEN
            CHRG.CODE = 7
        END
        IF R.TERM<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE> EQ "ADVANCE" THEN
            CHRG.CODE = 9
        END

*        CHRG.CODE = R.TERM<LD.CHRG.CODE>
*HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
        LG.TYPE = R.TERM<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>

        IF LG.TYPE EQ 'BIDBOND' THEN
            PER = "0.003"
            MIN = "150"
        END ELSE
            PER = "0.006"
            MIN = "150"
        END

        FN.CUR= 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
        CALL OPF(FN.CUR,F.CUR)

        CALL F.READ(FN.CUR,R.TERM<LD.CURRENCY>, R.CUR, F.CUR ,E11)
        CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>

        IF R.TERM<LD.CURRENCY> EQ 'EGP' THEN
            COM.MIN = MIN
        END ELSE
            IF R.TERM<LD.CURRENCY> EQ 'JPY' THEN
                COM.MIN = ( MIN * 100) / CUR.RATE
            END ELSE
                COM.MIN =  MIN / CUR.RATE
            END
        END
        CALL EB.ROUND.AMOUNT ('USD',COM.MIN,'',"")


        COM.AMO = R.TERM<LD.AMOUNT> + R.TERM<LD.AMOUNT.INCREASE>
        COM.AMT = COM.AMO * PER
        CUSS = R.TERM<LD.CUSTOMER.ID>

        FN.CUS.EXP= 'F.SCB.CUS.EXP' ; F.CUS.EXP = ''
        CALL OPF(FN.CUS.EXP,F.CUS.EXP)
        CALL F.READ(FN.CUS.EXP,CUSS,R.CUS.EXP,F.FT.COM,E1111)
        LG.TYPE = R.TERM<LD.LOCAL.REF><1,LDLR.LG.KIND>
        TYPE.EXP = R.CUS.EXP<CUS.EXP.TYPE>

        LOCATE LG.TYPE IN TYPE.EXP<1,1> SETTING POS THEN
            PERC = R.CUS.EXP<CUS.EXP.PERCENT,POS>
        END

        CUSS           = R.TERM<LD.CUSTOMER.ID>
        LG.K           = R.TERM<LD.LOCAL.REF><1,LDLR.LG.KIND>
        DEAL.CURRENCY  = R.TERM<LD.CURRENCY>


        CUS.P   = CUSS:".":LG.K:".":DEAL.CURRENCY
        FN.CUS.EXP.P= 'F.SCB.CUS.EXP.P' ; F.CUS.EXP.P = ''
        CALL OPF(FN.CUS.EXP.P,F.CUS.EXP.P)
        CALL F.READ(FN.CUS.EXP.P,CUS.P,R.CUS.EXP.P,F.CUS.EXP.P,EE.P)

        CUS.AMT = CUSS:".":LG.K:".":DEAL.CURRENCY
        FN.CUS.EXP.T= 'F.SCB.CUS.EXP.AMT'   ; F.CUS.EXP.T = ''
        CALL OPF(FN.CUS.EXP.T,F.CUS.EXP.T)
        CALL F.READ(FN.CUS.EXP.T,CUS.AMT,R.CUS.EXP.T,F.CUS.EXP.T,EE.T)


        IF PERC THEN
            COM.AMT = COM.AMT * PERC
        END

*HHHH

        IF  R.CUS.EXP.T THEN
            PERC.T = R.CUS.EXP.T<EXP.AMT.PERCENT>
            AMTT   = R.CUS.EXP.T<EXP.AMT.AMOUNT>
        END

        IF AMTT THEN
            NEW.AMTTT   = R.TERM<LD.AMOUNT> + R.TERM<LD.AMOUNT.INCREASE>
            IF AMTT LT NEW.AMTTT THEN
                COM.AMT = (COM.AMO * PER )*  PERC.T
            END
        END
*HHHH

        IF NOT(EE.P) THEN

            P1 = R.CUS.EXP.P<EXP.P.PERC1>
            P2 = R.CUS.EXP.P<EXP.P.PERC2>
            P3 = R.CUS.EXP.P<EXP.P.PERC3>
            P4 = R.CUS.EXP.P<EXP.P.PERC4>

        END

*HHHH
        IF COM.AMT < MIN THEN
            COMM.AMTTT = MIN
        END ELSE
            COMM.AMTTT = COM.AMT
        END
        CALL EB.ROUND.AMOUNT ('EGP',COMM.AMTTT,'',"2")
        CHRG.AMT = COMM.AMTTT

        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MAYLOC)
        ST.DATE  = MAYLOC<1,LDLR.END.COMM.DATE>

        EN.DATE = R.TERM<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
        DIFF = "C"
        CALL CDD("",ST.DATE,EN.DATE,DIFF)
        SAM1 = DIFF / 90
        SAM2 = SAM1 + 0.9
        PERIOD.NO = FIELD(SAM2,".",1)
        PERIOD.NO = PERIOD.NO - 1
        CALL CDT('EG', ST.DATE, '1')

        IF NOT(EE.P) THEN

            P<1> = R.CUS.EXP.P<EXP.P.PERC1>
            P<2> = R.CUS.EXP.P<EXP.P.PERC2>
            P<3> = R.CUS.EXP.P<EXP.P.PERC3>
            P<4> = R.CUS.EXP.P<EXP.P.PERC4>


            IF  ST.DATE LT TODAY THEN
                FOR I = 1 TO PERIOD.NO  + 1
                    H = 1
                    FIRST.INT.MON = SHIFT.DATE(ST.DATE,'3M',UP)

                    GOSUB PROCESS.FRQ.MONTHS.EXP
                    H ++
                NEXT I
            END ELSE
                H = 1
                IF (CHRG.AMT * P<H> ) < MIN THEN
                    COMM.AMTTT = MIN
                END ELSE
                    COMM.AMTTT = CHRG.AMT * P<H>
                END

**------------------------HYTHAM----------20110510-----------------
*               FIRST.INT.MON = SHIFT.DATE(ST.DATE,'3M',UP)
                FIRST.INT.MON = ST.DATE
**------------------------HYTHAM----------20110510-----------------

                R.NEW(LD.SD.SCH.TYPE)<1,1>    = 'F'
                R.NEW(LD.SD.DATE)<1,1>        = FIRST.INT.MON
                R.NEW(LD.SD.AMOUNT)<1,1>      = COMM.AMTTT
                R.NEW(LD.SD.CHARGE.CODE)<1,1> = CHRG.CODE<1,1>
                ST.DATE                       = FIRST.INT.MON

                FOR I = 2 TO PERIOD.NO + 1

                    FIRST.INT.MON = SHIFT.DATE(ST.DATE,'3M',UP)

                    H ++
                    IF H > 4 THEN H = 1

                    GOSUB PROCESS.FRQ.MONTHS.EXP
                NEXT I
            END
*HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

        END ELSE
            IF  ST.DATE LT TODAY THEN
                FOR I = 1 TO PERIOD.NO + 1

                    FIRST.INT.MON = SHIFT.DATE(ST.DATE,'3M',UP)

                    GOSUB PROCESS.FRQ.MONTHS
                NEXT I
            END ELSE

                TEXT = "GE" ; CALL REM
**------------------------HYTHAM----------20110510-----------------
*               FIRST.INT.MON = SHIFT.DATE(ST.DATE,'3M',UP)
                FIRST.INT.MON = ST.DATE
**------------------------HYTHAM----------20110510-----------------

                R.NEW(LD.SD.SCH.TYPE)<1,1>    = 'F'
                R.NEW(LD.SD.DATE)<1,1>        = FIRST.INT.MON
                R.NEW(LD.SD.AMOUNT)<1,1>      = CHRG.AMT<1,1>
                R.NEW(LD.SD.CHARGE.CODE)<1,1> = CHRG.CODE<1,1>
                ST.DATE                       = FIRST.INT.MON
                FOR I = 2 TO PERIOD.NO + 1
*HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

                    FIRST.INT.MON = SHIFT.DATE(ST.DATE,'3M',UP)

*HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    GOSUB PROCESS.FRQ.MONTHS
                NEXT I
            END
        END
    END ELSE
        ER.MSG = 'Missing LD.LOANS.AND.DEPOSITS$NAU record ':LD.ID
    END

    RETURN
*------------------ PROCESS FREQUENCY IN MONTHS -------------------------------*
PROCESS.FRQ.MONTHS:

    R.NEW(LD.SD.SCH.TYPE)<1, I>   = 'F'
    R.NEW(LD.SD.DATE)<1,I>        = FIRST.INT.MON
    R.NEW(LD.SD.AMOUNT)<1,I>      = CHRG.AMT<1,1>
    R.NEW(LD.SD.CHARGE.CODE)<1,I> = CHRG.CODE<1,1>
    ST.DATE                       = FIRST.INT.MON

    RETURN
*------------------ PROCESS FREQUENCY IN MONTHS -------------------------------*
PROCESS.FRQ.MONTHS.EXP:

    IF (CHRG.AMT * P<H> ) < MIN THEN
        COMM.AMTTT = MIN
    END ELSE
        COMM.AMTTT = CHRG.AMT * P<H>
    END

    R.NEW(LD.SD.SCH.TYPE)<1,I>    = 'F'
    R.NEW(LD.SD.DATE)<1,I>        = FIRST.INT.MON
    R.NEW(LD.SD.AMOUNT)<1,I>      = COMM.AMTTT
    R.NEW(LD.SD.CHARGE.CODE)<1,I> = CHRG.CODE<1,1>
    ST.DATE                       = FIRST.INT.MON

    RETURN
*------------------ DELETE ALL FIELDS FROM THE GROUP --------------------------*
DELETE.FIELDS:

    R.NEW(LD.SD.SCH.TYPE)       = ''
    R.NEW(LD.SD.DATE)           = ''
    R.NEW(LD.SD.AMOUNT)         = ''
    R.NEW(LD.SD.RATE)           = ''
    R.NEW(LD.SD.CHARGE.CODE)    = ''
    R.NEW(LD.SD.CHG.BASE.AMT)   = ''
    R.NEW(LD.SD.NUMBER)         = ''
    R.NEW(LD.SD.FREQUENCY)      = ''
    R.NEW(LD.SD.DIARY.ACTION)   = ''
    R.NEW(LD.SD.NOTE.DENOM)     = ''
    R.NEW(LD.SD.NOTE.QUANT)     = ''
    R.NEW(LD.SD.CYCLED.DATES)   = ''
    R.NEW(LD.SD.FREQ.CODE)      = ''
    R.NEW(LD.SD.INCLUSIVE.CHG)  = ''

    RETURN
*------------------------------------------------------------------------------*
PROGRAM.END:

    RETURN

END
