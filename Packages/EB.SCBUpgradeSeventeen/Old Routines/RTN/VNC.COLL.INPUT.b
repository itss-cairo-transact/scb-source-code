* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------


    SUBROUTINE VNC.COLL.INPUT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*** TEXT = "BYEEEEEEEE" ; CALL REM
    IF MESSAGE # 'VAL'  THEN
        IF  R.NEW(COLL.COLLATERAL.CODE)= "101" OR "103" THEN
* T(COLL.LOCAL.REF)<1,COLR.AC.COLL> <3> = 'NOINPUT'
            LOCAL.REF.COLL = (COLL.LOCAL.REF)<1,COLR.AC.COLL>
            T(LOCAL.REF.COLL)<3> = 'NOINPUT'
            LOCAL.REF.BILL = (COLL.LOCAL.REF)<1,COLR.BIL.COLL>
            T(LOCAL.REF.BILL)<3> = 'NOINPUT'
* T(COLL.LOCAL.REF)<1,COLR.BIL.COLL><3> = 'NOINPUT'
        END ELSE
            IF  R.NEW(COLL.COLLATERAL.CODE)= "102" THEN
*    T(COLL.LOCAL.REF)<COLR.LD.COLL><3> = 'NOINPUT'
*    T(COLL.LOCAL.REF)<COLR.BIL.COLL><3> = 'NOINPUT'
            END
        END

    END
   * CALL REBUILD.SCREEN
    RETURN
END
