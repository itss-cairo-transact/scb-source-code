* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***** 18/9/2002 RANIA SCB *****
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LD.LOANS.PEP
* A Subroutine to make sure that the Loan ID is an existing & from Loans Contract
* Also to default VALUE.DATE by Today's date

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

IF V$FUNCTION = 'I' THEN

  MYID = '' ; CATEGORY = ''
  CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYID)
  IF ETEXT THEN E = 'Not An Existing Loan Contract' ;CALL ERR ;MESSAGE = 'REPEAT'
   ELSE IF CATEGORY # '21050' CATEGORY # '21051' OR CATEGORY # '21052' THEN E = 'This Is A Deposit Contract' ;CALL ERR ;MESSAGE = 'REPEAT'

END

********************************************************************************

R.NEW (LD.AMT.V.DATE) = TODAY


RETURN
END
