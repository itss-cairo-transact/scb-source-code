* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
********* RANIA 10/03/03 **************
*-----------------------------------------------------------------------------
* <Rating>705</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.ADVINCROPER


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS


    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'

    IF V$FUNCTION = 'I' THEN

        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)

        MYPRODUCT=MYLOCAL<1,LDLR.PRODUCT.TYPE>
        MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>

        MYDATE=R.NEW(LD.DATE.TIME)[1,6]
        MYDATE='20':MYDATE

   ***     IF MYDATE=TODAY THEN E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
************* Get Difference between 2 Amounts *************************
*Get ID Of Last Record In History
        IDHIS=ID.NEW:";":"..."
        T.SEL = "SSELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ": IDHIS
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            OLD.ID= KEY.LIST<SELECTED>
            ID=FIELD(OLD.ID,';',1)
*Get Old Amount
            CALL DBR ('LD.LOANS.AND.DEPOSITS$HIS':@FM:LD.AMOUNT,OLD.ID,OLD.AMT)
*Get New Amount
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,ID.NEW,NEW.AMT)
*Get Difference Between 2 Amounts
            DIFF = NEW.AMT - OLD.AMT
        END
*************************************************************************
        IF ETEXT THEN
            E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            IF MYCODE NE '1271' AND MYCODE NE '1281' AND MYCODE NE '1282'  THEN E ='Only.Function.See.Is.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            IF MYCATEG NE 21096  THEN E ='Not.LG.Category' ; CALL ERR ; MESSAGE = 'REPEAT'
            IF MYCODE = '1271' THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>='1252'
                R.NEW(LD.LOCAL.REF)<1,LDLR.CHEQUE.NO> = ''
                R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR> = DIFF
                R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.DATE>=TODAY
                R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
            END  ELSE
                IF MYCODE='1251' OR MYCODE='1252' THEN
                    E ='ALREADY OPERATED' ; CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END
*************************************************************************
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Edit.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
*************************************************************************
    END
*************************************************************************
    IF V$FUNCTION = 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END ELSE
        R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
        END
    END

***************************************************************************
    RETURN
END
