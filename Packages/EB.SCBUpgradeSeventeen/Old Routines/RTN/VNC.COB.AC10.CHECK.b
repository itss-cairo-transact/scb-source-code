* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.COB.AC10.CHECK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BATCH
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.REGEN.REQUEST
***--------------------------------------
    FN.BAT = 'F.BATCH' ; F.BAT = ''
    CALL OPF(FN.BAT,F.BAT)

    FN.DAT = 'F.DATES' ; F.DAT = ''
    CALL OPF(FN.DAT,F.DAT)


    FN.REG = 'F.RE.REGEN.REQUEST' ; F.REG = ''
    CALL OPF(FN.REG,F.REG)

    BAT.ID = 'BNK/EOD.LD.REN'
    DAT.ID = 'EG0010001'
    TD     = TODAY
    FLAG   = 0
    FLAG1  = 0

    CALL F.READ(FN.DAT,DAT.ID,R.DAT,F.DAT,E4)
    WS.COB.STATUS = R.DAT<EB.DAT.CO.BATCH.STATUS>

***--------------------------------------

    IF V$FUNCTION = 'I' THEN
        IF ID.NEW EQ 'COB' THEN
            T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC10' OR TRANSACTION.TYPE EQ 'AC45'"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF NOT(SELECTED) THEN
                E = '�� ��� ��� ����� �����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF WS.COB.STATUS EQ 'O' THEN
                CALL F.READ(FN.BAT,BAT.ID,R.BAT,F.BAT,E3)
*Line [ 62 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.STATUS.COUNT = DCOUNT(R.BAT<BAT.JOB.STATUS>,@VM)

                WS.STATUS  = R.BAT<BAT.JOB.STATUS><1,WS.STATUS.COUNT>
                WS.RUN.DAT = R.BAT<BAT.LAST.RUN.DATE><1,WS.STATUS.COUNT>
                IF WS.STATUS NE '0' OR WS.RUN.DAT NE TD THEN
                    E = 'Please Run The Service BNK/EOD.LD.REN Before COB'
                    CALL ERR ; MESSAGE = 'REPEAT'
                END
            END

***** STOPPED AT 2019/07/31  *****
***** CHECK RE.REGEN.REQUEST *****
**================================

**    WS.TODAY    = TODAY
**    WS.NEXT.DAY = TODAY

**    CALL CDT("",WS.NEXT.DAY,'+1W')

**    WS.CUR.MONTH  = WS.TODAY[5,2]
**    WS.NEXT.MONTH = WS.NEXT.DAY[5,2]

**    IF WS.CUR.MONTH NE WS.NEXT.MONTH THEN
**        CALL F.READ(FN.REG,WS.TODAY,R.REG,F.REG,E6)
**        IF E6 THEN
**            E = 'RE.REGEN.REQUEST NOT CREATED'
**            CALL ERR ; MESSAGE = 'REPEAT'
**        END

**    END

*********************************

        END


        IF ID.NEW EQ 'BNK/EOD.LD.REN' THEN

            IF WS.COB.STATUS EQ 'B' THEN
                E = 'The COB Already Running'
                CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE

                CALL F.READ(FN.BAT,BAT.ID,R.BAT,F.BAT,E3)
*Line [ 107 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.STATUS.COUNT = DCOUNT(R.BAT<BAT.JOB.STATUS>,@VM)
                FOR I = 1 TO WS.STATUS.COUNT
                    WS.STATUS  = R.BAT<BAT.JOB.STATUS><1,I>
                    WS.RUN.DAT = R.BAT<BAT.LAST.RUN.DATE><1,I>
                    IF WS.STATUS EQ '0' AND WS.RUN.DAT EQ TD THEN
                        FLAG  = 1
                    END

                    IF WS.STATUS EQ '1' THEN
                        FLAG1 = 1
                    END

                NEXT I


                IF FLAG EQ 1 THEN
                    E = 'This Service Run Before'
                    CALL ERR ; MESSAGE = 'REPEAT'
                END

                IF FLAG1 EQ 1 THEN
                    E = 'This Service Already Running'
                    CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END
    END

    RETURN
END
