* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.FATCA.CHRG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    FN.CUR = "FBNK.CURRENCY" ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.AC = "FBNK.ACCOUNT" ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,E3)
    RATE.USD = R.CUR<EB.CUR.MID.REVAL.RATE,1>
    AMT      = 5 * RATE.USD

    IF V$FUNCTION = "I" THEN

        DB.ACCT.NO = COMI
        CALL F.READ(FN.AC,DB.ACCT.NO,R.AC,F.AC,E3.AC)
        CURR = R.AC<AC.CURRENCY>

        IF CURR EQ 'USD' THEN
            AMT = 5
        END

        R.NEW(FT.DEBIT.AMOUNT)     = DROUND(AMT,1)
        R.NEW(FT.DEBIT.CURRENCY)   = CURR
        R.NEW(FT.CREDIT.CURRENCY)  = CURR
        R.NEW(FT.CREDIT.ACCT.NO)   = 'PL52216'
        CALL REBUILD.SCREEN
    END
    RETURN
END
