* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
********************************NI7OOOOOOOOOOOOOOOO******************
    SUBROUTINE VNC.FT.UP.DEBIT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    IF R.NEW(FT.DEBIT.CUSTOMER) EQ 7300737 THEN
      **  TEXT = "R.NEW : " : R.NEW(FT.DEBIT.CUSTOMER) ; CALL REM
        R.NEW(FT.SEND.PAYMENT.Y.N)   = 'Y'
        R.NEW(FT.DR.ADVICE.REQD.Y.N) = 'Y'
        R.NEW(FT.CR.ADVICE.REQD.Y.N) = 'Y'
       ** TEXT = R.NEW(FT.SEND.PAYMENT.Y.N)   ; CALL REM
       ** TEXT = R.NEW(FT.DR.ADVICE.REQD.Y.N) ; CALL REM
       ** TEXT = R.NEW(FT.CR.ADVICE.REQD.Y.N) ; CALL REM
        CALL REBUILD.SCREEN
    END
    RETURN
END
