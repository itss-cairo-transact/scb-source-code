* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
****NESSREEN AHMED 21/6/2009*****************************
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.IMAGE.BRN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE

*The User should only reverse or input the signature record on their branche only
    ETEXT = ""
    IM.DEP = ''
    OP.DEP = ''
****UPDATED BY NESSREEN AHMED 17/6/2010*******************
***    IF V$FUNCTION ='I' OR V$FUNCTION ='A' THEN
    IF V$FUNCTION ='I' OR V$FUNCTION ='A' OR V$FUNCTION ='R' THEN
        TEXT = 'F=':V$FUNCTION ; CALL REM
        IF R.NEW(IM.DOC.INPUTTER) THEN
            INP.IM = R.NEW(IM.DOC.INPUTTER)
            IM.IMPUTTER = FIELD(INP.IM, "_", 2)
*****UPDATED BY NESSREEN AHMED 29/06/2009********************************************
**CALL DBR( 'USER':@FM:EB.USE.DEPARTMENT.CODE,IM.IMPUTTER, IM.DEP)
            IM.DEP = R.NEW(IM.DOC.DEPT.CODE)
            CALL DBR( 'USER':@FM:EB.USE.DEPARTMENT.CODE,OPERATOR, OP.DEP)
******************************************************************************
            IF IM.DEP NE '' THEN
                IF IM.DEP # OP.DEP THEN
                    E = '��� ������ �� ��� ���'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                END
            END
        END
    END
    RETURN
END
