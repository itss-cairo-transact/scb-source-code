* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.EZN.CHECK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MALIA.EZN

    FN.EZN = 'F.SCB.MALIA.EZN' ; F.EZN = '' ; R.EZN = ''
    CALL OPF(FN.EZN,F.EZN)
    ID.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.OPERATION.CODE>
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='INA' THEN
        CALL F.READ(FN.EZN,ID.NO,R.EZN,F.EZN,ERR.EZN)
        IF R.EZN NE '' THEN
            E = '�� ����� ��� ����� �� ���'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END
    RETURN
END
