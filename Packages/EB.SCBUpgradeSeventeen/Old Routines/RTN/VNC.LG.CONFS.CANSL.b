* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>750</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.CONFS.CANSL

* SHOULD BE AN EXSISTING RECORD WITH LG CATEGORY
* FUNCTION REVERSE NOT ALLOWED
* ONLY ONE ACTION PER DAY ALLOWED
* AUTHORIZATION MUST BE FROM THE SAME VERSION

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*************************************************************************************************************
    IF V$FUNCTION = 'I' THEN

        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>
        MYREASON=MYLOCAL<1,LDLR.CANCEL.REASON>
        IF ETEXT THEN
            E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        END  ELSE
*     IF MYCODE EQ "1409" THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1407"
            R.NEW(LD.LOCAL.REF)<1,LDLR.FORW.DELIVER>   = 'FORWARDED'
            R.NEW(LD.CHRG.CODE)    = "14"
            R.NEW(LD.FIN.MAT.DATE) = TODAY
*     END
        END
    END

    RETURN
END
