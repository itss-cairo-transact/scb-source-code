* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.LG.AMEND.OUT


*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


    IF R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND> EQ 'FG' THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT> = ''
    END

**    IF V$FUNCTION = 'A' THEN
    CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
    MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
    REST      = 'Same.Version'
    IF NOT(ETEXT) THEN
        IF MYVERNAME NE PGM.VERSION THEN
            E = 'You.Must.Authorize.OR.Delete.OR.Amend.This.Record.From. & ': REST
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.PROJECT.NAME> = PGM.VERSION
        END

    END
**    R.NEW(LD.LOCAL.REF)<1,LDLR.PROJECT.NAME> = PGM.VERSION
**  END
    IF V$FUNCTION = 'R' THEN
        E = 'No REVERSE '
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E

    END

    RETURN
END
