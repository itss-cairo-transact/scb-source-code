* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
**---------DINA 18/09/2002---------**

SUBROUTINE VNC.LD.LOANS.PERSONCOMMIT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


IF V$FUNCTION # 'S' THEN
CALL DBR ("LD.LOANS.AND.DEPOSITS":@FM:LD.CUSTOMER.ID,ID.NEW,CUS)
IF NOT(ETEXT) THEN E = 'USE ONLY SEE FUNCTION'
ELSE
IF V$FUNCTION = 'I' THEN
 R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = 'PERSON'
 R.NEW(LD.MIS.ACCT.OFFICER) = R.USER<EB.USE.DEPARTMENT.CODE>
 R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
 R.NEW(LD.VALUE.DATE)=TODAY
END
END
END ELSE
 IF R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> # PGM.VERSION OR R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=""  THEN E = 'YOU CANNOT SEE THIS VERSION'
END
IF E THEN CALL ERR; MESSAGE = 'REPEAT'
RETURN
END
