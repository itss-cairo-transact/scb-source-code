* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.GET.DATA

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CARD.FEES
*---------------------------------------------
    R.NEW(CARD.FEES.VERSION.NAME) = PGM.VERSION
    TEMP.ID      = ID.NEW     ;*CLASSIC.801
    CARD.PRODUCT = FIELD(TEMP.ID,'.',1)
    CARD.TYPE    = FIELD(TEMP.ID,'.',2)
    R.NEW(CARD.FEES.CARD.PRODUCT) = CARD.PRODUCT

    BEGIN CASE
    CASE CARD.TYPE EQ 801
        R.NEW(CARD.FEES.CARD.TYPE)        = "PRIMARY"
        R.NEW(CARD.FEES.CLIENT.TYPE)      = "C"
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = 'NOINPUT'
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = ''
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = ''
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = ''
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    CASE CARD.TYPE EQ 804
        R.NEW(CARD.FEES.CARD.TYPE)        = "PRIMARY"
        R.NEW(CARD.FEES.CLIENT.TYPE)      = "C"
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = ''
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = 'NOINPUT'
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = 'NOINPUT'
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = 'NOINPUT'
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    CASE CARD.TYPE EQ 802
        R.NEW(CARD.FEES.CARD.TYPE)        = "PRIMARY"
        R.NEW(CARD.FEES.CLIENT.TYPE)      = "S"
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = 'NOINPUT'
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = ''
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = ''
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = ''
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    CASE CARD.TYPE EQ 805
        R.NEW(CARD.FEES.CARD.TYPE)        = "PRIMARY"
        R.NEW(CARD.FEES.CLIENT.TYPE)      = "S"
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = ''
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = 'NOINPUT'
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = 'NOINPUT'
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = 'NOINPUT'
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    CASE CARD.TYPE EQ 810
        R.NEW(CARD.FEES.CARD.TYPE)        = "SUPPLEMENTARY"
        R.NEW(CARD.FEES.CLIENT.TYPE)      = "C"
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = ''
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = ''
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = ''
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = ''
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    CASE CARD.TYPE EQ 811
        R.NEW(CARD.FEES.CARD.TYPE)        = "SUPPLEMENTARY"
        R.NEW(CARD.FEES.CLIENT.TYPE)      = "S"
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = ''
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = ''
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = ''
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = ''
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    CASE OTHERWISE
        R.NEW(CARD.FEES.CARD.TYPE)        = ""
        R.NEW(CARD.FEES.CLIENT.TYPE)      = ""
        R.NEW(CARD.FEES.CARD.REPLACEMENT) = ""
        T(CARD.FEES.CARD.REPLACEMENT)<3>  = ''
        R.NEW(CARD.FEES.NEW.ISSUE)        = ""
        T(CARD.FEES.NEW.ISSUE)<3>         = ''
        R.NEW(CARD.FEES.PIN.REISSUE)      = ""
        T(CARD.FEES.PIN.REISSUE)<3>       = ''
        R.NEW(CARD.FEES.REISSUE.CARD)     = ""
        T(CARD.FEES.REISSUE.CARD)<3>      = ''
        R.NEW(CARD.FEES.ANNUAL.FEES)     = ""
        T(CARD.FEES.ANNUAL.FEES)<3>      = ''
    END CASE

    CALL REBUILD.SCREEN
    RETURN
END
