* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
********* WAEL **************
*-----------------------------------------------------------------------------
* <Rating>963</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.AMMARGIN

* PREVENT USER FROM USING REVERSE FUNCTION
* ONLY ONE CHANGE PER DAY ALLOWED
* ONLY CHANGING THE EXSISTING RECORD ALLOWED WITH APPROPRIATE CATEGORY


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
************************************************************************
    IF V$FUNCTION = 'I' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        IF ETEXT THEN E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        ELSE
            MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
            MYTYPE =R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
            MYVER = R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
*------------------
            IF MYTYPE = "ADVANCE" THEN
                IF MYCODE = 1111 THEN
                    IF MYVER NE ',SCB.LG.ADV.OPER.EB' AND MYVER NE ',SCB.LG.AMBNF.DIFF.NEW' AND MYVER NE ',SCB.LG.ADV.OPER' THEN
                        E = 'LG ADVANCE RECORD NOT OPERATED' ; CALL ERR ; MESSAGE = 'REPEAT'
                    END
                END
                IF MYCODE = 1271 THEN E ='LG ADVANCE RECORD NOT OPERATED' ; CALL ERR ; MESSAGE = 'REPEAT'
            END
*------------------
*            IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> EQ '100' THEN E='LG.IS.FULLY.COVERED'; CALL ERR ; MESSAGE = 'REPEAT'
            IF (MYCODE LE 1309 AND  MYCODE GE 1301) THEN E='Canceled.LG.Record'; CALL ERR ; MESSAGE = 'REPEAT'
            IF (MYCODE LT 1409 AND  MYCODE GE 1401) THEN E='Confis.LG.Record'; CALL ERR ; MESSAGE = 'REPEAT'

            IF MYCATEG # 21096 THEN E ='Not.LG.Category' ; CALL ERR ; MESSAGE = 'REPEAT'
            ELSE
                DAT.TIME = R.NEW(LD.DATE.TIME)
                MYDATE = 20:DAT.TIME[1,6]
**    IF MYDATE = TODAY THEN E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES> THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>=''
            END
        END
        R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
    END
*********************************************************************
    IF V$FUNCTION = 'A' THEN
        TEXT='AUTH';CALL REM
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN

            IF MYVERNAME NE PGM.VERSION THEN
                TEXT='VER';CALL REM
                E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
        END
    END
*********************************************************************
    RETURN
END
