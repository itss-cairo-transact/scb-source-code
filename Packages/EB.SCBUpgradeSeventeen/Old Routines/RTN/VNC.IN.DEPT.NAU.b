* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*****************MAHMOUD 12/7/2012************************
    SUBROUTINE VNC.IN.DEPT.NAU

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    IF V$FUNCTION = 'A'  THEN
        COMP = ID.COMPANY
        USER.DEPT = R.USER<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
        IF COMP EQ 'EG0010099' THEN
            REC.INP = R.NEW(INF.MLT.INPUTTER)<1,1>
            INPUTT.1  = FIELD(REC.INP,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT.1,USR.LCL)
            IN.DEPT = USR.LCL<1,USER.SCB.DEPT.CODE>

*****UPDATED BY MAHMOUD 13/8/2012**********
            IF (IN.DEPT NE USER.DEPT) AND (USER.DEPT NE 5100) THEN
*            IF IN.DEPT NE USER.DEPT THEN
***END OF UPDATE***************************
                E = "Inputter not the same department"
            END
        END
    END
    RETURN
END
