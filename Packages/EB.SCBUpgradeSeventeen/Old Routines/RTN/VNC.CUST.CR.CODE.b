* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
**************** MAHMOUD 17/6/2014 ******************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUST.CR.CODE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.STAMP.EXP

    CUST.ID = ID.NEW
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,CUST.LCL)
    CUST.CRD.STA = CUST.LCL<1,CULR.CREDIT.STAT>
    CUST.CR.CODE = CUST.LCL<1,CULR.CREDIT.CODE>
    IF (CUST.CR.CODE GE 110) OR (CUST.CRD.STA NE '') THEN
        T(CHR.EXP.ACCOUNT.NUMBER)<3> = 'NOINPUT'
        T(CHR.EXP.CREDIT.ACCT)<3>    = ''
    END ELSE
        T(CHR.EXP.ACCOUNT.NUMBER)<3> = ''
        T(CHR.EXP.CREDIT.ACCT)<3>    = 'NOINPUT'
    END
    RETURN
END
