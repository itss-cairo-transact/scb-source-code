* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-32</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.LIQ.ERLY.INT

********PREPARED BY BAKRY*************************************************
*
*CALC INTERSET PAIED AND ACTUAL INTEREST WILL BE GIVEN
*
**************************************************************************
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.LD.ACC.BAL
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.RATE.PREPAID

***************************************************************************
    CHK.FLG = 0
    R.NEW(LD.FIN.MAT.DATE) = TODAY
*****************************************************************************
    TEMP.EXP.DATE     = R.NEW(LD.FIN.MAT.DATE)
    TEMP.ISSUE.DATE   = R.NEW(LD.VALUE.DATE)
    TEMP.DAY = 'C'
    CALL CDD("",TEMP.ISSUE.DATE,TEMP.EXP.DATE , TEMP.DAY)
    IF TEMP.DAY LT "30" THEN
        E = '��� �� ���� ���� �� ���'
        CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        AMT = R.NEW(LD.AMOUNT)
        CURR = R.NEW(LD.CURRENCY)
        BEGIN CASE
****************************************************************************
        CASE TEMP.DAY GE 30 AND TEMP.DAY LT 60
            CATEG = "1M"
            GOSUB GET.RATE.DEPOSIT
*****************************************************************************
        CASE TEMP.DAY GE 60 AND TEMP.DAY LT 90
            CATEG = "2M"
            GOSUB GET.RATE.DEPOSIT
*****************************************************************************
        CASE TEMP.DAY GE 90 AND TEMP.DAY LT 180
            CATEG = "3M"
            GOSUB GET.RATE.DEPOSIT
******************************************************************************
        CASE TEMP.DAY GE 180 AND TEMP.DAY LT 270
            CATEG = "6M"
            GOSUB GET.RATE.DEPOSIT
***************************************************
        CASE TEMP.DAY GE 270 AND TEMP.DAY LT 360
            CATEG = "9M"
            GOSUB GET.RATE.DEPOSIT
***************************************************
        CASE TEMP.DAY GE 360  ;*AND TEMP.DAY LT 720
            CATEG = "12M"
            GOSUB GET.RATE.DEPOSIT
***************************************************
*        CASE TEMP.DAY GE 720 AND TEMP.DAY LT 1080
*            CATEG = "21008"
*            GOSUB GET.RATE.DEPOSIT
***************************************************
*        CASE TEMP.DAY GE 1080
*            CATEG = "21009":TEMP.ISSUE.DATE
*            GOSUB GET.RATE.DEPOSIT
***************************************************
        CASE OTHERWISE
            RETURN
        END CASE
***************************************************
        CUSS = R.NEW(LD.CUSTOMER.ID)
        CUS = LEN(R.NEW(LD.CUSTOMER.ID))
        IF CUS EQ 7 THEN
            CLASS = CUSS[2,1]
        END ELSE
            CLASS = CUSS[3,1]
        END
*************************************************************
        IF CHK.FLG EQ 0 THEN
            GOSUB GET.SCHEDUAL
        END
    END
    RETURN
*************************************************************
GET.RATE.DEPOSIT:
*****************
    F.SCB.PREPAID = '' ; FN.SCB.PREPAID = 'F.SCB.LD.RATE.PREPAID' ; R.SCB.PREPAID = ''
    CALL OPF(FN.SCB.PREPAID,F.SCB.PREPAID)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.SCB.PREPAID:" WITH @ID LIKE 21011... BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        FOR I = 1 TO SELECTED
            IF SELECTED GT 1 THEN
                RAT.DAT = FIELD(KEY.LIST<I>,".",2)
                N.RAT.DAT = FIELD(KEY.LIST<I+1>,".",2)
                IF (TEMP.ISSUE.DATE GE RAT.DAT AND TEMP.ISSUE.DATE LT N.RAT.DAT) OR ( TEMP.ISSUE.DATE GE RAT.DAT AND I = SELECTED AND N.RAT.DAT = '' ) THEN
                    CALL F.READ(FN.SCB.PREPAID,KEY.LIST<I>, R.SCB.PREPAID, F.SCB.PSCB.PREPAID, ETEXT)

                    I = SELECTED
                    GOSUB GET.LIQ.RAT
                END
            END ELSE
                CALL F.READ(FN.SCB.PREPAID,KEY.LIST<I>, R.SCB.PREPAID, F.SCB.PREPAID, ETEXT)
                GOSUB GET.LIQ.RAT
            END
        NEXT I

    END
    RETURN
*****************************************************************************
GET.SCHEDUAL:
**************
*==========CALC INTREST AMOUNT AFTER PENALTY RATE =========================
    NEW.ACT.INT.RATE = INT.RATE / 2
    TEXT = "��� ������ ������ = " : NEW.ACT.INT.RATE ; CALL REM
    R.NEW(LD.NEW.INT.RATE) = NEW.ACT.INT.RATE
    R.NEW(LD.INT.RATE.V.DATE) = R.NEW(LD.VALUE.DATE)
*============================================================================
    IF R.NEW(LD.FIN.MAT.DATE) # TODAY THEN ETEXT='��� �� ���� ����� �����'  ;CALL STORE.END.ERROR
    CALL REBUILD.SCREEN
    RETURN
************************************************************
GET.LIQ.RAT:
*===========
    IF CATEG = "1M" THEN INT.RATE = R.SCB.PREPAID<LRP.RATE.1M>
    IF CATEG = "2M" THEN INT.RATE = R.SCB.PREPAID<LRP.RATE.2M>
    IF CATEG = "3M" THEN INT.RATE = R.SCB.PREPAID<LRP.RATE.3M>
    IF CATEG = "6M" THEN INT.RATE = R.SCB.PREPAID<LRP.RATE.6M>
    IF CATEG = "9M" THEN INT.RATE = R.SCB.PREPAID<LRP.RATE.9M>
    IF CATEG = "12M" THEN INT.RATE = R.SCB.PREPAID<LRP.RATE.12M>
    RETURN
END
