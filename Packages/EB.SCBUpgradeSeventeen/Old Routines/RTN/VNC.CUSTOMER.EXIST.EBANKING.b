* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
  ***CREATED BY MAI SAAD 26-9-2017
     SUBROUTINE  VNC.CUSTOMER.EXIST.EBANKING

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


     CALL DBR('CUSTOMER':@FM:EB.CUS.MNEMONIC,ID.NEW,CUSID)
     IF CUSID=""
     THEN
         E = 'MUST.BE.EXISTING.RECORD' ; CALL ERR ; MESSAGE = 'REPEAT'
     END
 ** CALL DBR('CUSTOMER':@FM:INTER.BNK.DATE,ID.NEW,EBANKIN.CUS)
 ** IF EBANKIN.CUS = ''  THEN
     ELSE
         IF NOT(R.NEW(EB.CUS.LOCAL.REF)<1,CULR.INTER.BNK.DATE>)
         THEN
             E = 'This customer is not e-banking user'
             CALL ERR ; MESSAGE = 'REPEAT'

         END
     END
     RETURN
 END
 
