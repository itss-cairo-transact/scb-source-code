* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUST.SRV
* Created by Noha Hamed

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.SERVICE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS


    FN.PAYMENT='F.SCB.CUST.TEL.SERVICE'
    F.PAYMENT=''
    CALL OPF(FN.PAYMENT,F.PAYMENT)
    IF V$FUNCTION = 'I'  THEN
        CUST.ID = ID.NEW[1,8]
        ZEROS=''
        CALL DBR( 'CUSTOMER':@FM:EB.CUS.MNEMONIC,CUST.ID,CUS.ID)
        IF ETEXT THEN
            E = '��� ���� ��� �����  ' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
        T.SEL="SELECT F.SCB.CUST.TEL.SERVICE WITH @ID LIKE ":CUST.ID:"..."
        CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)
        NO.OF.RECORDS=SELECTED+1
        LEN.NO=LEN(NO.OF.RECORDS)
        FOR I=LEN.NO TO 2
            ZEROS=ZEROS:'0'
        NEXT I
        SERIAL.NO=ZEROS:NO.OF.RECORDS
        ID.NEW=CUST.ID:'.':SERIAL.NO

*        ID.NEW = CUST.ID:'.':TODAY
        R.NEW(TEL.CUST.NO) = CUST.ID

    END
    RETURN
END
