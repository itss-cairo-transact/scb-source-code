* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>730</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.DEP.INT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR

    IF V$FUNCTION = 'I' THEN
        ACC.NO = FIELD(ID.NEW,"-",1)
        CALL DBR ('ACCOUNT':@FM:AC.LIMIT.REF,ACC.NO,LIMIT.NO)
        LIMIT.NOO = FIELD(LIMIT.NO,".",1)
        IF LIMIT.NOO EQ 102 THEN

            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CU.NO)
            LIMIT.CODE = CU.NO:".0000":LIMIT.NO
            TEXT =  LIMIT.CODE ; CALL REM

            CALL DBR ('LIMIT':@FM:LI.COLLAT.RIGHT,LIMIT.CODE,COL.NO)
            TEXT = COL.NO ; CALL REM
*Line [ 50 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT = DCOUNT(COL.NO,@SM)
            TEXT = TEMP.COUNT ; CALL REM
            MAX.INT.SAV = 0
            FOR I = 1 TO TEMP.COUNT

                FN.COLL = 'FBNK.COLLATERAL' ; F.COLL = '' ; R.COLL = ''
                CALL OPF( FN.COLL,F.COLL)
                T.SEL = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.NO<1,1,I>:"... AND COLLATERAL.CODE EQ ":LIMIT.NOO
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF SELECTED THEN
                    CALL F.READ(FN.COLL,KEY.LIST<1>,R.COLL,F.COLL,'')
                    APP.ID =  R.COLL<COLL.APPLICATION.ID>
TEXT = APP.ID ; CALL REM
                END

                CALL DBR ('ACCR.ACCT.CR':@FM:IC.ACRCR.CR.INT.RATE,APP.ID,INT.SAV)
TEXT = INT.SAV ; CALL REM
                IF INT.SAV GT MAX.INT.SAV THEN
                    MAX.INT.SAV = INT.SAV
                END

            NEXT I
            DEBIT.INT = MAX.INT.SAV
R.NEW(IC.ADI.DR.INT.RATE) = DEBIT.INT
R.NEW(IC.ADI.DR.MIN.RATE) = 2
TEXT = DEBIT.INT ; CALL REM
        END


        IF LIMIT.NOO EQ 101 OR LIMIT.NOO EQ 103 THEN
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CU.NO)
            LIMIT.CODE = CU.NO:".0000":LIMIT.NO
            TEXT =  LIMIT.CODE ; CALL REM

            CALL DBR ('LIMIT':@FM:LI.COLLAT.RIGHT,LIMIT.CODE,COL.NO)
            TEXT = COL.NO ; CALL REM
*Line [ 87 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT = DCOUNT(COL.NO,@SM)
            TEXT = TEMP.COUNT ; CALL REM

            FOR I = 1 TO TEMP.COUNT
                FN.COLL = 'FBNK.COLLATERAL' ; F.COLL = '' ; R.COLL = ''
                CALL OPF( FN.COLL,F.COLL)
                T.SEL = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.NO<1,1,I>:"... AND COLLATERAL.CODE EQ ":LIMIT.NOO
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF SELECTED THEN
                    CALL F.READ(FN.COLL,KEY.LIST<1>,R.COLL,F.COLL,'')
                    APP.ID =  R.COLL<COLL.APPLICATION.ID>
                    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,APP.ID,INT.APP.RATE)
                    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.SPREAD,APP.ID,SPR.APP.RATE)
                    FIRST.INT = INT.APP.RATE + SPR.APP.RATE
                    IF SELECTED GT 1 THEN
                        FOR I = 2 TO SELECTED
                            CALL F.READ(FN.COLL,KEY.LIST<I>,R.COLL,F.COLL,'')
                            APP.ID =  R.COLL<COLL.APPLICATION.ID>
                            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,APP.ID,INT.APP.RATE)
                            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.SPREAD,APP.ID,SPR.APP.RATE)
                            APP.INT = INT.APP.RATE + SPR.APP.RATE
                            IF APP.INT GT FIRST.INT THEN
                                FIRST.INT = APP.INT
                            END
                        NEXT I
                    END
                END
            NEXT I
            DEBIT.INT = FIRST.INT
 R.NEW(IC.ADI.DR.INT.RATE) = DEBIT.INT
 R.NEW(IC.ADI.DR.MIN.RATE) = 2
            TEXT = DEBIT.INT ; CALL REM
        END
    END
    RETURN
**********************************************************************************************************************
END
