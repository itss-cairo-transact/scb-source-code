* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN AHMED 5/3/2013**************

    SUBROUTINE VNC.CHQ.RETURN.PAY.CHECK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW

    ETEXT = ""

    IF V$FUNCTION = 'C'  THEN
        E = '��� �����'
*** SCB UPG 20160621 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
    END
    IF V$FUNCTION = 'A'  THEN
        CALL DBR('SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.INPUTTER, ID.NEW ,INPUTT)
        FMT.INP = FIELD(INPUTT, "_" ,2)
        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, FMT.INP , INP.CO)
        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, OPERATOR , AUTH.CO)
        IF INP.CO # AUTH.CO THEN
            E = '��� ����� ������ ��� ��� ���'
*** SCB UPG 20160621 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END ELSE
            CALL DBR('SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.RECORD.STAT, ID.NEW ,REC.STAT)
            TEXT = 'REC.STAT=':REC.STAT ; CALL REM
            IF REC.STAT # '����' THEN
                E = '��� ����� ��������'
*** SCB UPG 20160621 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
            END
        END
    END

    RETURN
END
