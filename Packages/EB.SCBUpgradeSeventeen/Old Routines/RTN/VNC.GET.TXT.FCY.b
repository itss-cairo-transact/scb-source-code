* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.GET.TXT.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN


    CALL !HUSHIT(0)
    COMAND = "sh /hq/opce/bclr/user/fcy.clearing/fcytxt.sh"
    EXECUTE COMAND

**------------------------------------------------------------------**
    Path = "/hq/opce/bclr/user/fcy.clearing/FCY.CLEAR"
**------------------------------------------------------------------**
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
**------------------------------------------------------------------**
    EOF   = ''
    I     = 1
    H     = 1
    E     = ""
    ETEXT = ''
    DEP   = ''

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,"|",24)
            AMT               = FIELD(Line,"|",10)
            DEPT.CODE         = FIELD(Line,"|",21)
            CURR              = FIELD(Line,"|",6)
*           BANK              = FIELD(Line,"|",6)

            IF (  DEPT.CODE NE OLD.DEPT AND I GT 1 )  THEN
                H = H +1
                I = 1
            END

            R.NEW(SCB.BR.OUR.REFERENCE)<1,H,I> = OUR.ID
            R.NEW(SCB.BR.AMOUNT)<1,H,I>        = AMT

            CALL DBR('NUMERIC.CURRENCY':@FM:1,CURR,CUR.NAME)
            R.NEW(SCB.BR.CURR)<1,H,I>          = CUR.NAME

            IF DEPT.CODE[3,1] EQ '0' THEN
                DEP = DEPT.CODE[4,1]
            END ELSE
                DEP = DEPT.CODE[3,2]
            END
            R.NEW(SCB.BR.BR.CODE)<1,H>       = DEP

            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,OUR.ID,MY.LOC)

            R.NEW(SCB.BR.BANK)<1,H,I>        = MY.LOC<1,BRLR.BANK>
            I = I + 1

            OLD.DEPT =  DEPT.CODE

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    RETURN
**------------------------------------------------------------------**

END
