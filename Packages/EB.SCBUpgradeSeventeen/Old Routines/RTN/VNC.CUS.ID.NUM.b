* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>197</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUS.ID.NUM

*IF THERE IS NO VALUE IN FIELD OLD.CUST.ID THEN DEFAULT THE VALUE
*IN THIS FIELD BY THE CUSTOMER ID THAT THE USER TYPED IN THE MASK

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    ETEXT = ""
***UPDATED BY NESSREEN AHMED***27/2/2014******
***    IF V$FUNCTION = 'I'  AND NOT(R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>)  THEN R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID> = ID.NEW
    IF V$FUNCTION = 'I' THEN
        IF NOT(R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>)  THEN R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID> = ID.NEW
        IF NOT(R.NEW(EB.CUS.MNEMONIC)) THEN R.NEW(EB.CUS.MNEMONIC) = ID.NEW
    END
***END OF UPDATE 27/2/2014**************************
    RETURN
END
