* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*********  RANIA 27/03/2003**************
*-----------------------------------------------------------------------------
* <Rating>753</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.ADVINCREASE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*******************************************************************************************************
    IF V$FUNCTION = 'I' THEN

        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)

        MYPRODUCT=MYLOCAL<1,LDLR.PRODUCT.TYPE>
        MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>
        ADV.OPER=MYLOCAL<1,LDLR.ADV.OPERATIVE>
        IF ETEXT THEN
            E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        END  ELSE
            IF MYPRODUCT NE 'ADVANCE' THEN E='Only Advance Is Allowed' ; CALL ERR ; MESSAGE='REPEAT'
            MYDATE=R.NEW(LD.DATE.TIME)[1,6]
            MYDATE='20':MYDATE
            IF MYCODE NE '1251' AND MYCODE NE '1252' AND MYCODE NE '1304' AND ADV.OPER NE 'YES' THEN E ='Only.Function.See.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*IF MYDATE=TODAY THEN E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> ='1271'
            R.NEW( LD.AMT.V.DATE ) = TODAY
* R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>=R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE>
            IF R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> NE ''THEN R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
            R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.SW.FUR.ID>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.SW.GTEE.DET>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.LG.REF.ID>=''

        END
*************************************************************************
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Edit.This.Record.From. & ': REST; CALL ERR  ; MESSAGE = 'REPEAT'
            END

*************************************************************************
        END ELSE
            IF R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> NE '1271' THEN
                E ='Only.Function.See.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
*******************************************************************************************************
        IF V$FUNCTION = 'A' THEN
            CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
            REST='Same.Version'
            IF NOT(ETEXT) THEN
                IF MYVERNAME NE PGM.VERSION THEN
                    E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR  ; MESSAGE = 'REPEAT'
                END
            END  ELSE
                R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
            END
        END


        RETURN
    END
