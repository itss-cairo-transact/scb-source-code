* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
************ RANIA 11/06/2003 *******************************
*-----------------------------------------------------------------------------
* <Rating>349</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.CHEQUEREQUEST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'

    IF V$FUNCTION = 'I' THEN

        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        IF NOT(ETEXT) THEN
            CODE = MYLOCAL<1,LDLR.OPERATION.CODE>
            PERC = MYLOCAL<1,LDLR.MARGIN.PERC>
**   IF R.NEW( LD.LOCAL.REF)< 1 ,LDLR.PRODUCT.TYPE> NE 'ADVANCE' OR ( CODE NE '1111' AND CODE NE '1281' AND CODE NE '1271') OR PERC NE '' THEN
            IF R.NEW( LD.LOCAL.REF)< 1 ,LDLR.PRODUCT.TYPE> NE 'ADVANCE' AND ( CODE NE '1111' AND CODE NE '1281' AND CODE NE '1271') OR PERC NE ''  THEN
                E ='Not.Allowed.To.Open.This.Record' ; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                IF  CODE  = '1111' THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1281'
                    R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
                END
                IF  CODE = '1271' THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1283'
                    R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION

                END
                IF  CODE = '1281' THEN
                    R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> = '1282'
                    R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
                END
                IF  CODE = '1283' THEN
                    R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> = '1284'
                    R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
                END
            END
            CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
            MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
            IF NOT(ETEXT) THEN
                IF MYVERNAME NE PGM.VERSION THEN
                    E='You.Must.Edit.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
                END
            END

        END

    END
*************************************************************************
    IF V$FUNCTION = 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END  ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
        END
    END

*************************************************************************
    RETURN
END
