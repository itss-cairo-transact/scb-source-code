* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.ISCO.CHECK.COMP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.UNKNOWN.CASES
*-------------------------------------------------
    IF V$FUNCTION EQ 'I' THEN

        COMP   = C$ID.COMPANY
*        IF COMP NE 'EG0010001' AND COMP NE 'EG0010011' AND COMP NE 'EG0010003' AND COMP NE 'EG0010002' AND COMP NE 'EG0010040' AND COMP NE 'EG0010012' AND COMP NE 'EG0010009' AND COMP NE 'EG0010005' AND COMP NE 'EG0010010' AND COMP NE 'EG0010015' AND COMP NE 'EG0010022' AND COMP NE 'EG0010020' THEN
*            E = "Can't access book from another company"; CALL ERR ; MESSAGE = 'REPEAT'
*        END

        ID.NO  = COMI
        ID.LEN = LEN(ID.NO)
        TEST   = FIELD(ID.NO,'21096',1)
        TEST2  = FIELD(ID.NO,'23101',1)

        IF (TEST NE '' AND TEST NE ID.NO) OR (TEST2 NE '' AND TEST2 NE ID.NO) THEN
            IF ID.LEN NE 17 THEN
                E = "You must enter acct with 17 character"; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END ELSE
            IF ID.LEN NE 16 THEN
                E = "You must enter acct with 16 character"; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
    END
    RETURN
