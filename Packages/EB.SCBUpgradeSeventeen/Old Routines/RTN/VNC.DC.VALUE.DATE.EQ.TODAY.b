* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
**---------NESSREEN---------**

    SUBROUTINE VNC.DC.VALUE.DATE.EQ.TODAY

* IF THE FUNC IS INPUT THEN THE VALUE.DATE DEFAULTED TO TODAY

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATA.CAPTURE

    IF V$FUNCTION = 'I' THEN
        CALL DBR('TELLER.USER':@FM:1,OPERATOR,USE.ID)
       * IF USE.ID[3,2] # '99' THEN
       *  IF USE.ID[3,2] # '98' THEN
         *   E = '����� ��� ���������������';CALL ERR ;MESSAGE = 'REPEAT'
        *END
        *DEP = R.USER<EB.USE.DEPARTMENT.CODE>
        *BR = FMT(DEP, "R%2")
        *R.NEW( DC.DC.ACCOUNT.NUMBER)= 'EGP10000':BR:'99'
        R.NEW( DC.DC.ACCOUNT.NUMBER)= 'EGP10000':'9999'
        R.NEW( DC.DC.VALUE.DATE) = TODAY
    END
    RETURN
END
