* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>234</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUS.DATE.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
    FN.CU   = 'FBNK.CUSTOMER'   ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    WS.SYS.DATE = TODAY

    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C'  THEN
        CUS.ID = FIELD(ID.NEW,"*",1)
        DAT.ID = FIELD(ID.NEW,"*",2)
        IF CUS.ID NE 999999 THEN
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            IF E1 THEN
                E = '��� ������ ��� ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            WS.NEW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
            WS.GROUP.NUM  = LOCAL.REF<1,CULR.GROUP.NUM>
            IF WS.NEW.SECTOR EQ 4650 THEN
                E = '������ ��� ��� ���� ���� �����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END

            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            IF WS.CREDIT.CODE LT 100 THEN
                E = '������ ��� �� ���� ���� ������'
                CALL ERR ; MESSAGE = 'REPEAT'
            END

            CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,CUS.ID,POS.RES)
            IF POS.RES GT 89 THEN
                E = '������ ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END

* IF DAT.ID LT WS.SYS.DATE THEN
*            IF DAT.ID # WS.SYS.DATE THEN
 *               E =  '��� �� ���� ������� ������ ����� ���'
  *              CALL ERR ; MESSAGE = 'REPEAT'
   *         END
*UPDATE BY MOHAMED SABRY 2011/07/03
*UPDATED BY NOHA HAMED 1/3/2017
*IF WS.GROUP.NUM GT 999999 THEN
*    E =  '��� ������ �� ������� �����-���� ����'
*   CALL ERR ; MESSAGE = 'REPEAT'
*END ELSE
            R.NEW(RM.GROUP.CODE) = WS.GROUP.NUM
            CALL DBR ('SCB.CUSTOMER.GROUP':@FM:CG.GROUP.NAME,WS.GROUP.NUM,WS.GROUP.NAME)
            R.NEW(RM.GROUP.NAME) = WS.GROUP.NAME<1,1>
            CALL REBUILD.SCREEN
*END

*   IF V$FUNCTION = 'R'  THEN
*       IF DAT.ID LT WS.SYS.DATE THEN
*           E =  '�� ���� ����� �� ��� ������ �������'
*           CALL ERR ; MESSAGE = 'REPEAT'
*       END
*   END

        END
    END

    RETURN
END
