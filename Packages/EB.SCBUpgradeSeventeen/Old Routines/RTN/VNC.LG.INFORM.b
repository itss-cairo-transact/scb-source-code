* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
********** WAEL **********
*-----------------------------------------------------------------------------
* <Rating>639</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.INFORM

* SHOULD BE AN EXSISTING RECORD WITH LG CATEGORY
* FUNCTION REVERSE NOT ALLOWED
* ONLY ONE ACTION PER DAY ALLOWED
* AUTHORIZATION MUST BE FROM THE SAME VERSION

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*************************************************************************************************************
    IF V$FUNCTION = 'I' THEN
   ****HASHED ON 14/6/2012 R.NEW(LD.CHRG.CODE) = '14'
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        IF ETEXT THEN
            E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1409" THEN
            E ='This.already.Informed' ; CALL ERR ; MESSAGE = 'REPEAT'
        END  ELSE
            MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
            ADV.OPER = R.NEW(LD.LOCAL.REF)<1,LDLR.ADV.OPERATIVE>
            MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
****************************************************
            IF MYCODE EQ '1111' OR MYCODE EQ '1281' OR MYCODE EQ '1282' THEN
                IF MYTYPE EQ 'ADVANCE' THEN
                    IF ADV.OPER NE 'YES' THEN
                        E ='CANT BE CONFISCATED' ; CALL ERR ; MESSAGE = 'REPEAT'
                    END
                END
            END
*----------------------------------------------------
            IF (MYCATEG LT '21096' OR MYCATEG GT '21099') THEN E ='Not.LG.Category'    ; CALL ERR ; MESSAGE = 'REPEAT'
            CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.STATUS,ID.NEW,MYSTATUS)
            IF MYSTATUS EQ 'LIQ' THEN
                E ='This.Is.Liquidated.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                DAT.TIME = R.NEW(LD.DATE.TIME)
                MYDATE = 20:DAT.TIME[1,6]
                *******TEXT = MYDATE ; CALL REM
                IF MYDATE = TODAY THEN
                   E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
               END
                IF R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> NE '' THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = "1409"
                END
            END
        END
    END
**************************************************************************************************************
    IF V$FUNCTION = 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From & ' : MYVERNAME; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
        END
    END
    RETURN
END
