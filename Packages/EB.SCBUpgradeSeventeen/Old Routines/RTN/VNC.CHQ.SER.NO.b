* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN AHMED 2011/4/5
    SUBROUTINE VNC.CHQ.SER.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW

    ETEXT = "" ; ID.NN = '' ; SER.NO = ''
    CHQ.NO = ''

    FN.CHQ.N = 'F.SCB.CHQ.RETURN.NEW$NAU'  ; F.CHQ.N = '' ; R.CHQ.N = ''
    CALL OPF(FN.CHQ.N,F.CHQ.N)

    FN.CHQ = 'F.SCB.CHQ.RETURN.NEW'  ; F.CHQ = '' ; R.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    CALL DBR( 'SCB.CHQ.RETURN.NEW':@FM:CHQ.RET.CHEQUE.NO, ID.NEW , CHQ.NO)
    TEXT = 'ID.NEW=':ID.NEW ; CALL REM
    IF CHQ.NO THEN
        E = '��� ����� ��������'
        CALL ERR
        MESSAGE = 'REPEAT'
    END

    CHQ.ID = FIELD(ID.NEW,'.',1)
    ID.NN = CHQ.ID:"..."
    T.SEL =  "SELECT F.SCB.CHQ.RETURN.NEW WITH @ID LIKE ": ID.NN

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    SER.NO = SELECTED+1
    KEY.TO.USE = CHQ.ID:'.':SER.NO

    CALL F.READ(FN.CHQ.N,KEY.TO.USE,R.CHQ.N,F.CHQ.N,READ.ERR.NAU)

    IF NOT(READ.ERR.NAU) THEN
        E = '��� ����� ����� �� ������'
        CALL ERR
        MESSAGE = 'REPEAT'
    END ELSE
        ID.NEW = KEY.TO.USE
    END

    RETURN
END
