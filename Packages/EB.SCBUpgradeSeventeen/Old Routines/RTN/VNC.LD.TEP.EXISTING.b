* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>198</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.TEP.EXISTING

* A ROUTINE TO DEFAULT THE FIELD LD.AMT.V.DATE WITH THE DATE OF TODAY
* A ROUTINE TO CHECK IF THE CONTRACT DOESN'T BELONG TO ALOAN CATEGORY THEN DISPLAY ERROR

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.PARMS1

***************************************************************
    R.NEW(LD.DRAWDOWN.ENT.DATE) = R.NEW(LD.VALUE.DATE)
***************************************************************

    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CUSTOMER.ID,ID.NEW,ID)
    IF ETEXT THEN E = 'MUST.BE.EXISTING';CALL ERR ; MESSAGE = 'REPEAT'

    CALL DBR('SCB.LOANS.PARMS':@FM:SCB.LO.PAR.DESCRIPTION,R.NEW(LD.CATEGORY),CATEG)
*   IF ETEXT THEN E = 'MUST.BELONG.TO.A.LOAN.CATEGORY';CALL ERR ; MESSAGE = 'REPEAT'

    IF R.NEW(LD.CATEGORY) LT '21050'OR R.NEW(LD.CATEGORY) GT '21055'THEN ETEXT = 'MUST.BE.A.LOAN.CATEGORY'

* R.NEW(LD.AMT.V.DATE) = TODAY

    RETURN
END
