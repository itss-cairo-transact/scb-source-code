* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>46</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.LOAN.GET.SCHED

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY


    IF V$FUNCTION = 'I' THEN

        DEFFUN SHIFT.DATE( )
        LD.ID = '' ; LD.ID = ID.NEW
        GOSUB OPEN.FILES ; R.TERM = '' ; ER.MSG = ''
        GOSUB DELETE.FIELDS
        GOSUB PROCESS.LD.SCHEDULES
        IF ER.MSG THEN
            E = ER.MSG ; CALL ERR ; MESSAGE = 'REPEAT'
        END

    END
    GOTO PROGRAM.END

*----------------------- OPEN FILES -------------------------------------------*
OPEN.FILES:

    VF.LD.LOANS.AND.DEPOSITS$NAU = ''
    CALL OPF('F.LD.LOANS.AND.DEPOSITS$NAU', VF.LD.LOANS.AND.DEPOSITS$NAU)

    RETURN
*------------------------------------------------------------------------------*
*------------------ PROCESS LD SCHEDULES --------------------------------------*
PROCESS.LD.SCHEDULES:
    CALL F.READ('F':R.COMPANY(EB.COM.MNEMONIC):'.LD.LOANS.AND.DEPOSITS$NAU',
    LD.ID, R.TERM, VF.LD.LOANS.AND.DEPOSITS$NAU, ER.MSG)
    IF NOT(ER.MSG) THEN
       R.NEW(LD.SD.SCH.TYPE)<1,1>='A'
        IF R.TERM<LD.ANNUITY.PAY.METHOD> EQ "BEGIN" THEN 
            ST.DATE = R.TERM<LD.VALUE.DATE>
******************************
            GOSUB PROCESS.FRQ.MONTHS

        END
    END ELSE
        ER.MSG = 'Missing LD.LOANS.AND.DEPOSITS$NAU record ':LD.ID
    END

    RETURN
*------------------ PROCESS FREQUENCY IN MONTHS -------------------------------*
PROCESS.FRQ.MONTHS:

*    R.NEW(LD.SD.FORWARD.BACKWARD)='5'
*    R.NEW(LD.SD.BASE.DATE.KEY)='1'
*    R.NEW(LD.SD.SCH.TYPE)<1,1>='A'
     R.NEW(LD.SD.DATE)<1,1>=ST.DATE




    RETURN
*------------------ DELETE ALL FIELDS FROM THE GROUP --------------------------*
DELETE.FIELDS:
    R.NEW(LD.SD.SCH.TYPE) = ''
    R.NEW(LD.SD.DATE) = ''
    R.NEW(LD.SD.AMOUNT) = ''
    R.NEW(LD.SD.RATE) = ''
    R.NEW(LD.SD.CHARGE.CODE) = ''
    R.NEW(LD.SD.CHG.BASE.AMT) = ''
    R.NEW(LD.SD.NUMBER) = ''
    R.NEW(LD.SD.FREQUENCY) = ''
    R.NEW(LD.SD.DIARY.ACTION) = ''
    R.NEW(LD.SD.NOTE.DENOM) = ''
    R.NEW(LD.SD.NOTE.QUANT) = ''
    R.NEW(LD.SD.CYCLED.DATES) = ''
    R.NEW(LD.SD.FREQ.CODE) = ''
    R.NEW(LD.SD.INCLUSIVE.CHG) = ''

    RETURN
*------------------------------------------------------------------------------* 
PROGRAM.END:

    RETURN

END
