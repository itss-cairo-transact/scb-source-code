* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*****CREATE BY NESSMA 2010/09/26
    SUBROUTINE VNC.DRMNT.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*---------------------------------------
    IF V$FUNCTION = 'I' THEN
        ETEXT = ""
        R.NEW(EB.CUS.POSTING.RESTRICT)               = '18'
        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.DATE>   = TODAY
        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>   = '1'
    END
*---------------------------------------
    IF V$FUNCTION = 'A' THEN
        WS.LOCAL     = R.USER<EB.USE.LOCAL.REF>
        WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
        IF WS.SCB.DEPT NE 5100 THEN
            ETEXT = "���� �� ���� ���� ����� �� �����"
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*---------------------------------------
    RETURN
END
