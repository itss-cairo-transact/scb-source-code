* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***Abeer***
    SUBROUTINE VNC.LG.DEL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*****Hashed on 08-03-2020
**   IF V$FUNCTION EQ 'I' THEN
**        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.RECORD.STATUS,COMI,REC.STAT)
**     IF REC.STAT EQ 'INAU' THEN
**       ETEXT = 'MUST DELETE RECORD';CALL STORE.END.ERROR
**      END ELSE
**       IF REC.STAT EQ '' THEN
**         ETEXT = ''
**       END
**      END
**  END
*******End of Modification 08-03-2020
    IF V$FUNCTION EQ 'D' OR V$FUNCTION EQ 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,COMI,MYLOCAL)
        MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                ETEXT='You.Must.Delete.This.Record.From.Same.Version';CALL STORE.END.ERROR
            END
        END ELSE
            ETEXT= ''
        END
    END
    RETURN
END
