* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
**-- CREATE BY  NESSREEN AHMED
    SUBROUTINE VNC.CHQ.RETURN.TODAY

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW
*------------------------------------------------
    COMP  = ID.COMPANY
    ETEXT = ""
    IF V$FUNCTION = 'I'  THEN
*-- EDIT BY NESSMA ON 20160104
        R.NEW(CHQ.RET.VERSION.NAME) = PGM.VERSION
*-- END EDIT
        IF R.NEW(CHQ.RET.STOP.DATE)= '' THEN
            R.NEW(CHQ.RET.STOP.DATE)   = TODAY
            R.NEW(CHQ.RET.CHEQUE.NO)   = FIELD(ID.NEW, "." ,1)
            R.NEW(CHQ.RET.RECORD.STAT) = '����'
        END
    END
    IF V$FUNCTION = 'C'  THEN
        E = '��� �����'
*** SCB UPG 20160619 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160619 - E
    END
********UPDATED BY NESSREEN AHMED 27/2/2013*********
    IF V$FUNCTION = 'A'  THEN
        E = '' ; ERR = ''
*-- EDIT BY NESSMA 20160104
        IF PGM.VERSION NE R.NEW(CHQ.RET.VERSION.NAME) THEN
            E = "��� ������� �� ��� ������"
            CALL STORE.END.ERROR
        END
*-- END EDIT
        CALL DBR('SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.INPUTTER, ID.NEW ,INPUTT)
        FMT.INP = FIELD(INPUTT, "_" ,2)
        INP.CO  =  R.NEW(CHQ.RET.CO.CODE)
        AUTH.CO = ID.COMPANY
        IF INP.CO # AUTH.CO THEN
            E = '��� ����� ������ ��� ��� ���'

*** SCB UPG 20160619 - S
*            CALL ERR
*            MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160619 - E

        END ELSE
            CALL DBR('SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.RECORD.STAT, ID.NEW ,REC.STAT)
            IF REC.STAT # '����' THEN
                E = '��� ����� ��������'
*** SCB UPG 20160619 - S
*                CALL ERR
*                MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160619 - E
            END
        END
    END
    RETURN
END
