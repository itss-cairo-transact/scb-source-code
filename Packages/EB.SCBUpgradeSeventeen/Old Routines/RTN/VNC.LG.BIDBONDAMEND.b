* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*********  RANIA 19/05/2003**************
*-----------------------------------------------------------------------------
* <Rating>500</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.BIDBONDAMEND

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*******************************************************************************************************
    IF V$FUNCTION = 'I' THEN
***    R.NEW(LD.CHRG.CODE) = ""
*** R.NEW(LD.CHRG.CODE) = 14
**        TEXT = R.NEW(LD.CHRG.CODE) ; CALL REM
**        TEXT = R.NEW(LD.CHRG.CODE)<1,1> ; CALL REM

***     CALL REBUILD.SCREEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)

        MYPRODUCT=MYLOCAL<1,LDLR.PRODUCT.TYPE>
        MYCODE=MYLOCAL<1,LDLR.OPERATION.CODE>

        IF ETEXT THEN E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        ELSE
            IF R.NEW(LD.DATE.TIME)=TODAY THEN E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            IF MYPRODUCT NE 'BIDBOND' THEN E = 'Only BidBond Is Allowed'; CALL ERR ; MESSAGE = 'REPEAT'
            R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> ='1239'
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES> THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>=''
            END
            IF R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> NE '' THEN R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
        END

    END
*******************************************************************************************************
    IF V$FUNCTION = 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR ;MESSAGE = 'REPEAT'
            END
        END
    END

    RETURN
END
