* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>785</Rating>
*-----------------------------------------------------------------------------
********* ABEER 9/3/2003**************
*A Routine To Perform Version Defaults ,To Prevent Reverse
*To Check It Is Existing Record,
*To Check That One action ON LG is Allowed Daily,
*To Check That Authorization Happens From Same Version .

    SUBROUTINE VNC.LG.AMEND.MOR

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
*******************************************************************************************************
    IF V$FUNCTION = 'I' THEN

        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
        VER.NAME=R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>
        MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
        MYTYPE=R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
        IF ETEXT THEN E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        ELSE
            IF (MYCODE LE 1309 AND  MYCODE GE 1301) THEN E='Canceled.LG.Record'; CALL ERR ; MESSAGE = 'REPEAT'
            IF (MYCODE LE 1409 AND  MYCODE GE 1401) THEN E='Confis.LG.Record'; CALL ERR ; MESSAGE = 'REPEAT'
           ************** IF MYCATEG NE 21096 THEN E ='Not.LG.Category'; CALL ERR ; MESSAGE = 'REPEAT'
            IF MYCATEG NE 21097 THEN E ='Not.LG.Category'; CALL ERR ; MESSAGE = 'REPEAT'
*  IF MYCODE NE '1251' AND MYCODE NE '1252' THEN E ='Only.Function.See.Allowed';CALL ERR ; MESSAGE = 'REPEAT'
            DATE.DAY= 20:R.NEW(LD.DATE.TIME)[1,6]
**************
            IF PGM.VERSION EQ ',SCB.LG.AMAMTINC' THEN
                IF MYTYPE EQ 'ADVANCE' THEN
***    E ='Advance Not Allowed'; CALL ERR ; MESSAGE = 'REPEAT'
                END
            END

**************
* IF MYTYPE EQ 'BIDBOND' THEN
*    IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> = '1111' THEN
*       E='Must.Request.Amending.For.Bidbond';CALL ERR ; MESSAGE = 'REPEAT'
*  END
*END

***************
*IF DATE.DAY=TODAY THEN
*  E ='Only.One.Action.Per.Day.Allowed'; CALL ERR ; MESSAGE = 'REPEAT'
*END  ELSE
            IF  VER.NAME THEN
                R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE> EQ '' THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>=TODAY
            END
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES> THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.TAXES>=''
            END

            R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.SW.FUR.ID>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.SW.GTEE.DET>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>=''
            R.NEW(LD.LOCAL.REF)<1,LDLR.LG.REF.ID>=''

*END
        END

    END
*************************************************************************************
    IF V$FUNCTION = 'A' THEN
        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From & ' : MYVERNAME;CALL ERR;MESSAGE='REPEAT'
            END
        END  ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
        END
    END
*******************************************************************************************************
    RETURN
END
