* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDIzNzk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***********CREATED BY MAHMOUD MAGDY 2018/07/09
    SUBROUTINE VNC.CHQ.VALIDATE 

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER


    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC , F.ACC)


    ID = ID.NEW 
    COMP = ID.COMPANY 
    ID.ACC = FIELD(ID,'.',1)
    CHQ.NO = FIELD(ID,'.',2)
    ACC.COMP = ID.ACC[15,2]
 
   CALL F.READ(FN.ACC,ID.ACC,R.ACC,F.ACC,ERR.ACC)
   IF NOT(R.ACC) THEN
       E = '��� ���� ���� ������'
       CALL ERR ;  MESSAGE ='REPEAT'
       RETURN
   END

    IF ACC.COMP NE COMP[8,2] THEN
        E="INVALID ID COMPANY"
        CALL ERR ;  MESSAGE ='REPEAT'
        RETURN
    END


    IF NOT(CHQ.NO MATCH '0N') OR CHQ.NO EQ '' THEN
        E = '����� ������� ��� �������'
        CALL ERR ;  MESSAGE ='REPEAT'
        RETURN
    END



END
