* @ValidationCode : MjotMTA4MTc3ODM5NzpDcDEyNTI6MTY0NTEwMzYxNDY1NTpLYXJlZW0gTW9ydGFkYTotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.INF.CHQ.VER
***********************MAHMOUD 26/8/2012**************************
* Check for the CHEQUE version when Authorise CHEQUE transaction *
******************************************************************

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION EQ 'A' THEN
        GOSUB INIT
*Line [ 36 ] Adding EB.SCBUpgradeSeventeen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 37 ] Adding EB.SCBUpgradeSeventeen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
        GOSUB CALLDB
        GOSUB PROCESS
    END
    RETURN
**************************************************************
INIT:
*-----
    APP.VER = APPLICATION:PGM.VERSION
    CHQ.FLG = ''
    RETURN
**************************************************************
CALLDB:
*-----
    FN.VER = "F.VERSION" ; F.VER = "" ; R.VER = "" ; ER.VER = ""
    CALL OPF(FN.VER,F.VER)
    RETURN
**************************************************************
PROCESS:
*-------
    ACCT.FLD  = R.NEW(INF.MLT.ACCOUNT.NUMBER)
*Line [ 57 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    INF.COUNT = DCOUNT(ACCT.FLD,@VM)
    FOR MM1 = 1 TO INF.COUNT
        ACCT.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,MM1>
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.NO,ACCT.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACCT.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF ACCT.CAT EQ '16151' THEN
            GOSUB CHECK.VER
        END
    NEXT MM1
    RETURN
**************************************************************
CHECK.VER:
*---------
    CALL F.READ(FN.VER,APP.VER,R.VER,F.VER,ER.VER)
    VER.FIELDS = R.VER<EB.VER.FIELD.NO>
*Line [ 72 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE 'CHEQUE.NUMBER-1' IN VER.FIELDS<1,1> SETTING POS.CHQ THEN NULL ELSE E = '��� ������� �� ���� ����� �������'
    
    RETURN
**************************************************************
END
