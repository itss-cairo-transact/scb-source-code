* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDkwMjM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.DOC.BRAN.NO

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
* Created by Noha Hamed

    IF V$FUNCTION EQ 'I' THEN
        BRN.NO = R.USER<EB.USE.DEPARTMENT.CODE>
***  CALL DBR ('CUSTOMER':@FM:EB.CUS.DEPT.CODE,CUS.NO,DEPTCODE)
        R.NEW(DOC.PRO.BRAN.NO)=BRN.NO
        R.NEW(DOC.PRO.ISSUE.DATE)=TODAY
        CALL REBUILD.SCREEN
    END
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POSS=R.ITSS.CUSTOMER<EB.CUS.TEXT>
     STO1=POSS<1,1>
     STO2=POSS<1,2>
     STO3=POSS<1,3>
     STO4=POSS<1,4>
     STO5=POSS<1,5>
     STO6=POSS<1,6>
     STO7=POSS<1,7>
     STO8=POSS<1,8>
     STO9=POSS<1,9>
     STO10=POSS<1,10>
     IF POSS NE '' THEN
         TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
     END


     ETEXT =''

    RETURN
END
