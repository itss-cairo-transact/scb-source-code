* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTUwODQ6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.ISCO.SME.CLOSED

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CF

    FN.ISC = 'F.SCB.ISCO.SME.CF'; F.ISC = ''
    CALL OPF(FN.ISC,F.ISC)
    FN.CU = 'FBNK.CUSTOMER'; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    FN.AC = 'FBNK.ACCOUNT'; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    T.TODAY    = TODAY
    MONTH      = T.TODAY[5,2]
    YEAR       = T.TODAY[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 11
    END
    ELSE
        NEW.MONTH = MONTH - 2
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
    CF.TYPE =  R.NEW(ISCO.CF.CF.TYPE)

    NN = ''
    BEGIN CASE
    CASE CF.TYPE = '21096'
        NN = 'LD'
    CASE CF.TYPE = '2310'
        NN = 'LC'
    END CASE

    OLD.ID = R.NEW(ISCO.CF.CF.ACC.NO):YEAR:NEW.MONTH:NN
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.ISCO.SME.CF':@FM:ISCO.CF.CF.ACCT.BALANCE,OLD.ID,LST.BAL)
F.ITSS.SCB.ISCO.SME.CF = 'F.SCB.ISCO.SME.CF'
FN.F.ITSS.SCB.ISCO.SME.CF = ''
CALL OPF(F.ITSS.SCB.ISCO.SME.CF,FN.F.ITSS.SCB.ISCO.SME.CF)
CALL F.READ(F.ITSS.SCB.ISCO.SME.CF,OLD.ID,R.ITSS.SCB.ISCO.SME.CF,FN.F.ITSS.SCB.ISCO.SME.CF,ERROR.SCB.ISCO.SME.CF)
LST.BAL=R.ITSS.SCB.ISCO.SME.CF<ISCO.CF.CF.ACCT.BALANCE>
*************
    T.DATE  = T.TODAY[1,6]:'01'
    CALL CDT('',T.DATE,"-1C")
    *TEXT = OLD.ID:'/':LST.BAL:'/':T.DATE; CALL REM
    IF V$FUNCTION = "I" THEN

        R.NEW(ISCO.CF.ACC.STATUS)           = '005'
        R.NEW(ISCO.CF.CF.SETTLEMENT.DATE)   = T.DATE
        R.NEW(ISCO.CF.AMT.OVERDUE)          = LST.BAL
        R.NEW(ISCO.CF.AMT.WRITTEN.OFF)      = LST.BAL
        R.NEW(ISCO.CF.CF.ACCT.BALANCE) = 0
        R.NEW(ISCO.CF.REASON.WRITTEN.OFF) = '004'
        R.NEW(ISCO.CF.NDPD) = 999

    END
    RETURN
END
