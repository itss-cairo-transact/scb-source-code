* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDYwMTk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE  VNC.CUR.RATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.USD.RATE.FCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

    ID.NEW = TODAY

    R.NEW(CUR.CURRENCY)<1,1> = 'SAR'
    R.NEW(CUR.CURRENCY)<1,2> = 'GBP'
    R.NEW(CUR.CURRENCY)<1,3> = 'EUR'
    R.NEW(CUR.CURRENCY)<1,4> = 'JPY'
    R.NEW(CUR.CURRENCY)<1,5> = 'CHF'
    R.NEW(CUR.CURRENCY)<1,6> = 'SEK'
    RETURN
END
