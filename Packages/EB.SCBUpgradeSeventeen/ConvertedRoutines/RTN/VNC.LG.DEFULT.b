* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MjI0NjM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LG.DEFULT



*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LETTER.OF.CREDIT':@FM:TF.LC.FULLY.UTILISED,ID.NEW,MYUTIL)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,ID.NEW,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
MYUTIL=R.ITSS.LETTER.OF.CREDIT<TF.LC.FULLY.UTILISED>
        IF MYUTIL EQ 'Y'  THEN
            E ='��� ������ �� ������' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LETTER.OF.CREDIT':@FM:TF.LC.LC.TYPE,ID.NEW,MYTYPE)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,ID.NEW,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
MYTYPE=R.ITSS.LETTER.OF.CREDIT<TF.LC.LC.TYPE>
        IF MYTYPE NE '' THEN
            E ='��� ������ ���� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            R.NEW(TF.LC.LOCAL.REF)<1,LCLR.VERSION.NAME>=PGM.VERSION
            R.NEW(TF.LC.ISSUE.DATE) = TODAY
        END
        RETURN
    END
