* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MjAzMTg6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VNC.LG.A

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


**********************
    CO.CODE=ID.COMPANY
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND> EQ 'CFN' AND CO.CODE NE 'EG0010099' THEN
      **  E = 'No ALLOWED '; CALL ERR ; MESSAGE = 'REPEAT'
ETEXT='NO ALLOWED';CALL STORE.END.ERROR
    END

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND> EQ 'CAD' AND CO.CODE NE 'EG0010099' THEN
**        E = 'No ALLOWED '; CALL ERR ; MESSAGE = 'REPEAT'
 ETEXT='NO ALLOWED';CALL STORE.END.ERROR
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND> EQ 'CBB' AND CO.CODE NE 'EG0010099' THEN
**        E = 'No ALLOWED '; CALL ERR ; MESSAGE = 'REPEAT'
 ETEXT='NO ALLOWED';CALL STORE.END.ERROR
    END
**********************
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND> EQ 'FG' THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE> = ''
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT> = ''
    END

**    IF V$FUNCTION = 'A' THEN
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
    MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
    REST      = 'Same.Version'
    IF NOT(ETEXT) THEN
        IF MYVERNAME NE PGM.VERSION THEN
** E = 'You.Must.Authorize.OR.Delete.OR.Amend.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
 E='You.Must.Authorize.OR.Delete.OR.Amend.This.Record.From. &': REST ;CALL STORE.END.ERROR
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.PROJECT.NAME> = PGM.VERSION
        END
    END
TEXT=MYVERNAME :'-VER NAME';CALL REM
**    R.NEW(LD.LOCAL.REF)<1,LDLR.PROJECT.NAME> = PGM.VERSION
**  END
    IF V$FUNCTION = 'R' THEN
**        E = 'No REVERSE '; CALL ERR ; MESSAGE = 'REPEAT'
ETEXT = 'NO REVERSE';CALL STORE.END.ERROR

    END

    RETURN
END
