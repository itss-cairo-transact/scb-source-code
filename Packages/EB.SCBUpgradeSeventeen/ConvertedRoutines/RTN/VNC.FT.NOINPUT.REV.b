* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTE0ODY6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>138</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.FT.NOINPUT.REV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS



    IF V$FUNCTION = "R" THEN
        FT.DR = R.NEW(FT.DEBIT.ACCT.NO)[1,2]
        FT.CR = R.NEW(FT.CREDIT.ACCT.NO)[1,2]

        CURR.DR=R.NEW(FT.DEBIT.CURRENCY)
        CURR.CR=R.NEW(FT.CREDIT.CURRENCY)

        FT.ID=ID.NEW:';1'
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.TRANSACTION.TYPE,FT.ID,FT.TYPE)
F.ITSS.FUNDS.TRANSFER$HIS = 'F.FUNDS.TRANSFER$HIS'
FN.F.ITSS.FUNDS.TRANSFER$HIS = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS)
CALL F.READ(F.ITSS.FUNDS.TRANSFER$HIS,FT.ID,R.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS,ERROR.FUNDS.TRANSFER$HIS)
FT.TYPE=R.ITSS.FUNDS.TRANSFER$HIS<FT.TRANSACTION.TYPE>

        IF FT.TYPE NE '' THEN
            IF CURR.DR NE 'EGP' THEN
                IF FT.CR EQ 'PL' OR FT.DR EQ 'PL' THEN
                    ETEXT = "REVERSE NOT ALLOWED" ; CALL STORE.END.ERROR
                    E = "REVERSE NOT ALLOWED" ; CALL STORE.END.ERROR

                END
            END
********************TO CHECK THE CHRG AMT CURRENCY
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CH.COUNT=DCOUNT(R.NEW(FT.CHARGE.AMT),@VM)
            **TEXT = 'CH.COUNT = ' :  CH.COUNT ; CALL REM
            FOR I = 1 TO CH.COUNT
                CHRG.CURR = R.NEW(FT.CHARGE.AMT)<1,I>
                CHRG.CUR= CHRG.CURR[1,3]

                IF CHRG.CUR NE 'EGP' THEN
                    ETEXT = "REVERSE NOT ALLOWED" ; CALL STORE.END.ERROR
                    E = "REVERSE NOT ALLOWED" ; CALL STORE.END.ERROR

                END
            NEXT I
********************TO CHECK THE COMM AMT CIRRENCY
*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COM.COUNT=DCOUNT(R.NEW(FT.COMMISSION.AMT),@VM)
           ** TEXT = "COM.COUNT" : CH.COUNT ; CALL REM
            FOR J = 1 TO COM.COUNT
                COM.CURR = R.NEW(FT.COMMISSION.AMT)<1,J>
                COM.CUR= COM.CURR[1,3]

                IF COM.CUR NE 'EGP' THEN
                    ETEXT = "REVERSE NOT ALLOWED" ; CALL STORE.END.ERROR
                    E = "REVERSE NOT ALLOWED" ; CALL STORE.END.ERROR

                END
            NEXT J
********************
        END
    END
    RETURN
END
