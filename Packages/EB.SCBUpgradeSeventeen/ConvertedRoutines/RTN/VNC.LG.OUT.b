* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MjQxNDE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>926</Rating>
*-----------------------------------------------------------------------------
******** RANIA 10/03/03 **************

*A Routine To Perform Defaults , To ensure That it Is Existing Contract,
* To Prevent Reverse Function, and To Ensure Authorising From Same Version

    SUBROUTINE VNC.LG.OUT


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS


    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
***************************************************************************************************
    IF V$FUNCTION = 'I' THEN

*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYCATEG)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYCATEG=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>

        MYPRODUCT = MYLOCAL<1,LDLR.PRODUCT.TYPE>
        MYCODE = MYLOCAL<1,LDLR.OPERATION.CODE>
        ADV.OPER = MYLOCAL<1,LDLR.ADV.OPERATIVE>

        IF ETEXT THEN
            E ='It.Should.Be.An.Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
*            IF MYCODE NE '1111' OR MYCODE NE '1281' OR MYCODE NE '1282' OR MYPRODUCT NE 'ADVANCE' THEN
            IF  MYPRODUCT NE 'ADVANCE' THEN
                E ='Only.Function.See.Is.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
            END
           ************ IF MYCATEG NE 21096  THEN
            IF MYCATEG NE 21097  THEN
                E ='Not.LG.Category' ; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                MYDATE=R.NEW(LD.DATE.TIME)[1,6]
                MYDATE='20':MYDATE
**IF MYDATE=TODAY THEN E ='Only.One.Action.Per.Day.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
                IF MYCODE='1111' OR MYCODE='1281' OR MYCODE='1282' THEN
                    IF ADV.OPER = 'YES' THEN
                        E ='ALREADY OPERATED' ; CALL ERR ; MESSAGE = 'REPEAT'
                    END ELSE
                        R.NEW( LD.LOCAL.REF)< 1,LDLR.OPERATION.CODE> ='1251'
                    END
                END ELSE
                    IF MYCODE='1251' OR MYCODE='1252' THEN
                        E ='ALREADY OPERATED' ; CALL ERR ; MESSAGE = 'REPEAT'
                    END ELSE
                        E='NOT ALLOWED';CALL ERR;MESSAGE='REPEAT'
                    END
                END
**              IF R.NEW(LD.CHRG.CODE)='1' THEN R.NEW(LD.CHRG.CODE)='4'
                IF R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME> NE '' THEN R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>='' THEN
                    R.NEW( LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>=R.NEW(LD.AMOUNT)
                END
                R.NEW( LD.LOCAL.REF)<1,LDLR.CHQ.DATE>=TODAY
**                R.NEW( LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>=R.NEW(LD.FIN.MAT.DATE)
                CUSTM = R.NEW(LD.CUSTOMER.ID)
            END
        END
        IF R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = 100 THEN
            R.NEW(LD.LIMIT.REFERENCE) = ' '
            R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> = R.NEW(LD.AMOUNT)
        END
**************************************************************************
*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Edit.This.Record.From. & ': REST; CALL ERR ; MESSAGE='REPEAT'
            END
        END


**************************************************************************
    END

**************************************************************************************************************
    IF V$FUNCTION = 'A' THEN
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR  ; MESSAGE = 'REPEAT'
            END
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> =MYVERNAME:"|": PGM.VERSION
        END
    END
    RETURN
END
