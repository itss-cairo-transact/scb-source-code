* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDMxOTU6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.CLCNTR.CHECK.ID

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*-------------------------------------------
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF (FN.USR, F.USR)
*-------------------------------------------
    CALL F.READ(FN.USR,ID.NEW,R.SCB.USR,F.USR,E3)

    DEPT.CO = R.SCB.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
    BR.CO = R.SCB.USR<EB.USE.DEPARTMENT.CODE>

    IF DEPT.CO NE '9936' AND BR.CO NE '36' THEN
        E = 'NOT ALLOW TO RESET THIS USER'
        CALL ERR ; MESSAGE = 'REPEAT'
    END
*-------------------------------------------
*    IF R.NEW(EB.PWR.USER.PW.ATTEMPT)[1,4] EQ "RAYA" OR R.NEW(EB.PWR.USER.ATTEMPT)[1,4] EQ "RAYA" THEN
*        E = 'NOT ALLOW TO RESET THIS USER'
*        CALL ERR ; MESSAGE = 'REPEAT'
*    END


    RETURN
END
