* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDc0NTE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>417</Rating>
*-----------------------------------------------------------------------------
** ----- 30.09.2002 MOHAMED&BAKRY&MONTASSER SCB -----

    SUBROUTINE VNC.CUSTOMER.DEAD

* IN THE INPUT FUNCTION MADE THE FIELD VERSION.NAME  EQUAL THE DEFAULT (PGM.VERSION)
* CHECK IF VERSION.NAME NE PGM.VERSION PRODUCE ERROR MSG
* E MSG INCLUDE & THAT REFER TO  THE RIGHT VERSION NAME
* IF THE FUNC IS INPUT THEN THE CONTACT.DATE DEFAULTED TO TODAY
* IF THE FUNC IS INPUT THEN THE OPENING.DATE DEFAULTED TO TODAY
* IF THE FUNC IS INPUT THEN THE TAX.EXEMP.DATE DEFAULTD TO TODAY
* DEFAULT THE ACCOUNT OFFICER EQ DEPARTMANT CODE (BRANCH NO.) FROM USER OF THE BRANCH
* DEFAULT REVIEW.FREQUENCY EQ M0601
* IF OLD.CUST.ID IS NULL THEN DEFAULT OLD.CUST.ID EQ ID.NEW

*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


    IF V$FUNCTION ='I' THEN
        IF NOT(R.NEW( EB.CUS.CONTACT.DATE)) THEN R.NEW( EB.CUS.CONTACT.DATE) = TODAY

        IF NOT (R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.OPENING.DATE>) THEN R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.OPENING.DATE>  = TODAY

        ZZ = FIELD(PGM.VERSION,".",1)
        ZZZ = FIELD(PGM.VERSION,".",2)

        ZZZZ = ZZ:".":ZZZ
*ZZZZ = PGM.VERSION




        IF  ZZZZ = ",SCB.CORPORATE" THEN
            DUMMYDAT = TODAY : 'M0601'
        END ELSE DUMMYDAT = TODAY : 'M1201'
*  CALL CFQ
*****************UPDATED BY RIHAM R15***************
        CUS.BR = R.NEW(EB.CUS.COMPANY.BOOK)[8,2]
        AC.OFICER = TRIM(CUS.BR, "0" , "L")
   *   NO CHANGE OF ACCOUNT.OFFICER BECAUSE IT READ FROM USER DEPARTMENT
        R.NEW( EB.CUS.ACCOUNT.OFFICER) = R.USER< EB.USE.DEPARTMENT.CODE>
        TEXT = "ACCT.OFICER=":R.NEW( EB.CUS.ACCOUNT.OFFICER) ; CALL REM
     *   TEXT = "ACCT.OFICER=":R.NEW(EB.CUS.COMPANY.BOOK) ; CALL REM
*****************************************************
        R.NEW( EB.CUS.REVIEW.FREQUENCY) = DUMMYDAT
        VERSION.NAME = R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.VERSION.NAME>
        IF NOT( VERSION.NAME) THEN R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.VERSION.NAME> = ZZZZ
        ELSE

            R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.VERSION.NAME> = PGM.VERSION
            IF R.OLD( EB.CUS.LOCAL.REF)< 1, CULR.VERSION.NAME> THEN
                VERSION.NAME = R.OLD( EB.CUS.LOCAL.REF)< 1, CULR.VERSION.NAME>
            END ELSE
                VERSION.NAME = R.NEW( EB.CUS.LOCAL.REF)< 1, CULR.VERSION.NAME>
            END

*TEXT = "VER " : VERSION.NAME ; CALL REM

*TEXT = "ZZ " : ZZZZ ; CALL REM
            IF VERSION.NAME # ZZZZ AND VERSION.NAME # ZZZZ:".UPDATE.DEAD" THEN
                VERSION.NAME1=VERSION.NAME[6,15]
                E= VERSION.NAME1:' ��� ������� ��� ����� �� ����'
                CALL ERR ; MESSAGE = 'REPEAT'


            END

        END

    END

    RETURN
END
