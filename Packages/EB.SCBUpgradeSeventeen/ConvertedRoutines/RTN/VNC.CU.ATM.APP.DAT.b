* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDQ3NTk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
** ----- 06.02.2008 NESSREEN AHMED -----
********UPDATED ON 31/03/2009*******************************************
*-----------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------
    SUBROUTINE VNC.CU.ATM.APP.DAT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP

*TO CHECK THAT THE ID.NEW IS AN EXISTING CUSTOMER AND TO DEFAULT APP.DATE BY TODAY'S DATE

    IF V$FUNCTION = 'I' THEN

        KEY.USE = FIELD(ID.NEW, ".", 1)

*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.MNEMONIC,KEY.USE,MYCUS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,KEY.USE,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
MYCUS=R.ITSS.CUSTOMER<EB.CUS.MNEMONIC>
        IF MYCUS EQ ''  THEN
       *     E = 'Shoud.Be.Existing.Customer'
       *     CALL ERR ; MESSAGE = 'REPEAT'
        END  ELSE
            IF R.NEW( SCB.VISA.ATM.APP.DATE) = '' THEN
                R.NEW( SCB.VISA.ATM.APP.DATE) = TODAY
                R.NEW( SCB.VISA.CUSTOMER)= KEY.USE
            END
********************************************************
        END
    END

    RETURN
END
