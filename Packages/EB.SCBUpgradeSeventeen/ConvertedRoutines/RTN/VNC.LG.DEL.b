* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MjI0OTM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***Abeer***
    SUBROUTINE VNC.LG.DEL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*****Hashed on 08-03-2020
**   IF V$FUNCTION EQ 'I' THEN
**        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.RECORD.STATUS,COMI,REC.STAT)
**     IF REC.STAT EQ 'INAU' THEN
**       ETEXT = 'MUST DELETE RECORD';CALL STORE.END.ERROR
**      END ELSE
**       IF REC.STAT EQ '' THEN
**         ETEXT = ''
**       END
**      END
**  END
*******End of Modification 08-03-2020
    IF V$FUNCTION EQ 'D' OR V$FUNCTION EQ 'A' THEN
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,COMI,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,COMI,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
        MYVERNAME = MYLOCAL<1,LDLR.VERSION.NAME>
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                ETEXT='You.Must.Delete.This.Record.From.Same.Version';CALL STORE.END.ERROR
            END
        END ELSE
            ETEXT= ''
        END
    END
    RETURN
END
