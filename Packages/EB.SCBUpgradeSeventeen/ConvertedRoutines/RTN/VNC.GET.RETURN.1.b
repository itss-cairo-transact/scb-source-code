* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTM2MzY6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>450</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.GET.RETURN.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN

*   TXT.ID = TODAY[1,4]:"-":TODAY[5,2]:"-":TODAY[7,2]:'-credit.txt'
    TXT.ID = TODAY[1,4]:"-":TODAY[5,2]:"-":TODAY[7,2]:'-credit.new.txt'
    Path = "/hq/opce/bclr/user/dltx/":TXT.ID
    TEXT = TXT.ID ; CALL REM

*Path = "/home/signat/OutgoingReplay.txt"

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 1
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,",",3)
            RETURN.REASON     = FIELD(Line,",",13)
            RESULT.CODE       = FIELD(Line,",",1)
            DEPT.CODE         = FIELD(Line,",",8)
            IF OUR.ID AND R.USER<EB.USE.DEPARTMENT.CODE> EQ DEPT.CODE THEN
***              DEPT.CODE = FIELD(Line,",",8)
***       IF R.USER<EB.USE.DEPARTMENT.CODE> = DEPT.CODE THEN
                IF RESULT.CODE EQ "820" THEN
                    R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = OUR.ID
                END
                IF RESULT.CODE EQ "520" THEN
                    R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = OUR.ID
                    R.NEW(SCB.BT.RETURN.REASON)<1,I> = RETURN.REASON
                END
***       END
                I = I + 1
            END

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    RETURN
END
