* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDM3MDI6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.COLL.AC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*    $INCLUDE           I_F.SCB.CU.CARD
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
***TEXT = "HIIIIIIIII" ; CALL REM



*T(TT.TE.CUSTOMER.1)<3> = ''
    IF MESSAGE # 'VAL'  THEN
        IF R.NEW(COLL.COLLATERAL.CODE) = "102" THEN
* TEXT = "COMI"  : COMI ; CALL REM
* T(SCB.CARD.DEPOSIT.COLATERAL.ID)<3> = ''
*           T(SCB.CARD.ACCOUNT.COLATERAL.ID)<3>='NOINPUT'
*        R.NEW(SCB.CARD.ACCOUNT.COLATERAL.ID)=''
*     END ELSE
*T(SCB.CARD.ACCOUNT.COLATERAL.ID)<3>=''
*         T(SCB.CARD.DEPOSIT.COLATERAL.ID)<3> = 'NOINPUT'
            CU = FIELD(ID.NEW,".",1)
            IF LEN (CU) = 7 THEN
                CUST = "0" : CU
            END
            IF LEN (CU) = 8 THEN
                CUST = CU
            END
**TEXT = "CUST" : CUST ; CALL REM
            SAV = "10650101"
            ACC = CUST : SAV
            R.NEW(COLL.LOCAL.REF)<1,COLR.AC.COLL>= ACC
            R.NEW(COLL.APPLICATION.ID)= ACC
**TEXT = ACC ; CALL REM
        END

* CALL REBUILD.SCREEN
        CUS.ID=FIELD(ID.NEW,'.',1)
        R.NEW(COLL.LOCAL.REF)<1,COLR.CUS.LIAB>= CUS.ID
    END
    RETURN
END
