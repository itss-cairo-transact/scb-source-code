* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTk0Mzk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
        *-----------BAKRY  9/11/2002---------------*
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LD.OPEN.TERM

*IN THE INPUT FUNCTION MADE THE FIELD VERSION.NAME  EQUAL THE DEFAULT (PGM.VERSION)
* CHECK IF VERSION.NAME NE PGM.VERSION PRODUCE ERROR MSG
*E MSG INCLUDE & THAT REFER TO  THE RIGHT VERSION NAME

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
   IF V$FUNCTION ='I' THEN

*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*              CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,CH)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
CH=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
              IF NOT(ETEXT) THEN

                E = '������ ������� ��� ����� �� & ' : @FM : PGM.VERSION
                CALL ERR ; MESSAGE = 'REPEAT'
              END ELSE

               FRST.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.FIRST.LD.ID>
               IF NOT(FRST.ID) THEN
                R.NEW(LD.LOCAL.REF)<1,LDLR.FIRST.LD.ID> = ID.NEW
               END
                END
                 R.NEW(LD.MIS.ACCT.OFFICER)=R.USER<EB.USE.DEPARTMENT.CODE>
                 VERSION.NAME = R.NEW(LD.LOCAL.REF)<1, LDLR.VERSION.NAME>
                  IF NOT(VERSION.NAME) THEN R.NEW(LD.LOCAL.REF)<1, LDLR.VERSION.NAME> = PGM.VERSION

                   END
RETURN
END
