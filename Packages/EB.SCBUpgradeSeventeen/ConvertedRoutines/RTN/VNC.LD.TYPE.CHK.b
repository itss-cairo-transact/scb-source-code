* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTk1ODQ6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.LD.TYPE.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*-------------------------------------------------

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LD.NAU = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD.NAU = ''
    CALL OPF(FN.LD.NAU,F.LD.NAU)

    CALL F.READ(FN.LD,COMI,R.LD,F.LD,ERR)
    LD.CATEG = R.LD<LD.CATEGORY>

    CALL F.READ(FN.LD.NAU,COMI,R.LD.NAU,F.LD.NAU,ERR)
    LD.CATEG.NAU = R.LD.NAU<LD.CATEGORY>

    IF (LD.CATEG GE 21001 AND LD.CATEG LE 21010) OR LD.CATEG EQ '' THEN
    END ELSE
        E = '�� ���� ������� �� ��� ������'
        CALL STORE.END.ERROR
    END

    IF (LD.CATEG.NAU GE 21001 AND LD.CATEG.NAU LE 21010) OR LD.CATEG.NAU EQ '' THEN
    END ELSE
        E = '�� ���� ������� �� ��� ������'
        CALL STORE.END.ERROR
    END

    RETURN
END
