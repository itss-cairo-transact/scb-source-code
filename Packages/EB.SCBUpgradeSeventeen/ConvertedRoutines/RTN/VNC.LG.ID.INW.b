* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MjMzNjg6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.LG.ID.INW

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE


    IF V$FUNCTION = 'R' THEN E ='Reverse.Function.Not.Allowed' ; CALL ERR ; MESSAGE = 'REPEAT'
    IF V$FUNCTION = 'I' THEN
        ID.NUM = ID.NEW[3,11]; LG='LG/'; LG.ID = LG:ID.NUM
        REST='Same.Version'
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.CUSTOMER.ID,ID.NEW,MYCUS)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYCUS=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>
*****************************************************************************************
        IF NOT(ETEXT) THEN
            E ='Existing.Contract' ; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
***************************************************************************
            R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.NUMBER,1> = LG.ID
            R.NEW( LD.LOCAL.REF)< 1,LDLR.LG.NUMBER,2> = ID.NEW
            IF R.NEW( LD.LOCAL.REF)< 1,LDLR.ISSUE.DATE>='' THEN  R.NEW( LD.LOCAL.REF)< 1,LDLR.ISSUE.DATE>= TODAY
            IF R.NEW( LD.VALUE.DATE) = '' THEN R.NEW( LD.VALUE.DATE)=TODAY
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>='' THEN R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>=LCCY
            R.NEW( LD.INTEREST.RATE)='0'
            R.NEW( LD.L.C.U.TYPE) = 'REVOLVING'
            R.NEW( LD.STA........Y.N)='NO'
            R.NEW( LD.LOCAL.REF)<1,LDLR.VERSION.NAME>=PGM.VERSION
            R.NEW( LD.MIS.ACCT.OFFICER)= R.USER<EB.USE.DEPARTMENT.CODE>
***********************
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
            MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
            IF NOT(ETEXT) THEN
                IF MYVERNAME NE PGM.VERSION THEN
                    E='You.Must.Edit.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END
    END
*****************************************************************************************
    IF V$FUNCTION = 'A' THEN
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.LOCAL.REF>
        MYVERNAME=MYLOCAL<1,LDLR.VERSION.NAME>
        REST='Same.Version'
        IF NOT(ETEXT) THEN
            IF MYVERNAME NE PGM.VERSION THEN
                E='You.Must.Authorize.This.Record.From. & ': REST; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
        R.NEW(LD.LOCAL.REF)<1,LDLR.PROJECT.NAME> = PGM.VERSION
    END
    RETURN
END
