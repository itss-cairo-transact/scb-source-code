* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDk1NDk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.DRAWING.PERCENT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON

    IF V$FUNCTION = 'D' OR V$FUNCTION EQ 'A' THEN

        TEXT = "DDELETE"

        FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
        CALL OPF(FN.LCC,F.LCC)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""
        ID.LC = ID.NEW[1,12]
        TEXT = "ID.NEW" : ID.LC ; CALL REM
        T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH @ID EQ  " : ID.LC
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CALL F.READ(FN.LCC,KEY.LIST,R.LCC,F.LCC,READ.ERR)

        IF  LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>  NE ''  THEN

            LC.REC(TF.LC.PROVIS.PERCENT) = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT>
            LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PERCENT> = ''
        END

        IF  LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV>  NE ''  THEN

            LC.REC(TF.LC.PROVIS.AMOUNT) = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV>
            LC.REC(TF.LC.LOCAL.REF)<1,LCLR.LOCAL.PROV> = ''
        END

*WRITE  R.LCC TO F.LCC , KEY.LIST ON ERROR
*   PRINT "CAN NOT WRITE RECORD":KEY.LIST:"TO" :FN.LCC
*END
        CALL F.WRITE (FN.LCC,KEY.LIST,R.LCC)

    END
************************************************************
    RETURN
END
