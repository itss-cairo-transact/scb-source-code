* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTQwMDI6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.GET.TXT.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN


    CALL !HUSHIT(0)
    COMAND = "sh /hq/opce/bclr/user/fcy.clearing/fcytxt.sh"
    EXECUTE COMAND

**------------------------------------------------------------------**
    Path = "/hq/opce/bclr/user/fcy.clearing/FCY.CLEAR"
**------------------------------------------------------------------**
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
**------------------------------------------------------------------**
    EOF   = ''
    I     = 1
    H     = 1
    E     = ""
    ETEXT = ''
    DEP   = ''

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,"|",24)
            AMT               = FIELD(Line,"|",10)
            DEPT.CODE         = FIELD(Line,"|",21)
            CURR              = FIELD(Line,"|",6)
*           BANK              = FIELD(Line,"|",6)

            IF (  DEPT.CODE NE OLD.DEPT AND I GT 1 )  THEN
                H = H +1
                I = 1
            END

            R.NEW(SCB.BR.OUR.REFERENCE)<1,H,I> = OUR.ID
            R.NEW(SCB.BR.AMOUNT)<1,H,I>        = AMT

*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('NUMERIC.CURRENCY':@FM:1,CURR,CUR.NAME)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CURR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR.NAME=R.ITSS.NUMERIC.CURRENCY<1>
            R.NEW(SCB.BR.CURR)<1,H,I>          = CUR.NAME

            IF DEPT.CODE[3,1] EQ '0' THEN
                DEP = DEPT.CODE[4,1]
            END ELSE
                DEP = DEPT.CODE[3,2]
            END
            R.NEW(SCB.BR.BR.CODE)<1,H>       = DEP

*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,OUR.ID,MY.LOC)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,OUR.ID,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
MY.LOC=R.ITSS.BILL.REGISTER<EB.BILL.REG.LOCAL.REF>

            R.NEW(SCB.BR.BANK)<1,H,I>        = MY.LOC<1,BRLR.BANK>
            I = I + 1

            OLD.DEPT =  DEPT.CODE

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    RETURN
**------------------------------------------------------------------**

END
