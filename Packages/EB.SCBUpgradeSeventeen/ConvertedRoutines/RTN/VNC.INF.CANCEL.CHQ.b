* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTQ2MDA6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.INF.CANCEL.CHQ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_INF.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*------------------------------------------------
    IID = 'ANY'
    TXT1 = "���� ��� ����� ������� "
    CALL TXTINP(TXT1, 8, 22, 14, IID)
    ISS.ID  = COMI
    CC.COM = 0
    FN.ISS.IN = 'F.INF.MULTI.TXN' ; R.ISS.IN = '' ; F.ISS.IN = '' ; ERR.IN = ''
    CALL OPF(FN.ISS.IN,F.ISS.IN)
    FN.ISS.FT = 'FBNK.FUNDS.TRANSFER' ; R.ISS.FT = '' ; F.ISS.FT = '' ; ERR.FT = ''
    CALL OPF(FN.ISS.FT,F.ISS.FT)
    FN.ISS.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; R.ISS.FT = '' ; F.ISS.FT.H = '' ; ERR.FT = ''
    CALL OPF(FN.ISS.FT.H,F.ISS.FT.H)
    FN.COMM = 'FBNK.FT.COMMISSION.TYPE' ; R.COMM = '' ; F.COMM = '' ; ERR.COMM = ''
    CALL OPF(FN.COMM,F.COMM)
    IF ISS.ID[1,2] EQ 'IN' THEN
        CALL F.READ(FN.ISS.IN,ISS.ID,R.ISS.IN,F.ISS.IN,ERR.IN)
        IF R.ISS.IN THEN
            CHQ.NO = R.ISS.IN<INF.MLT.CHEQUE.NUMBER>
            IF NOT(CHQ.NO) THEN
                E = " ��� ���� ����� ����� ��� " ; CALL ERR ; MESSAGE = 'REPEAT'
            END
*Line [ 55 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DDD = DCOUNT(R.ISS.IN<INF.MLT.ACCOUNT.NUMBER>,@VM)
            R.NEW(INF.MLT.LOCAL.REF)<1,INLR.SUPP.CODE> = R.ISS.IN<INF.MLT.LOCAL.REF><1,INLR.SUPP.CODE>
            FOR HH = 1 TO DDD
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,HH>  = R.ISS.IN<INF.MLT.ACCOUNT.NUMBER><1,HH>
                R.NEW(INF.MLT.PL.CATEGORY)<1,HH>     = R.ISS.IN<INF.MLT.PL.CATEGORY><1,HH>
                R.NEW(INF.MLT.GL.NUMBER)<1,HH>       = R.ISS.IN<INF.MLT.GL.NUMBER><1,HH>
                R.NEW(INF.MLT.DISC.PERCENT)<1,HH>    = R.ISS.IN<INF.MLT.DISC.PERCENT><1,HH>
                IF R.ISS.IN<INF.MLT.SIGN><1,HH> EQ 'CREDIT' THEN
                    R.NEW(INF.MLT.SIGN)<1,HH>        = 'DEBIT'
                    R.NEW(INF.MLT.TXN.CODE)<1,HH>    = 78
                END ELSE
                    R.NEW(INF.MLT.SIGN)<1,HH>        = 'CREDIT'
                    R.NEW(INF.MLT.TXN.CODE)<1,HH>    = 79
                END
                R.NEW(INF.MLT.CURRENCY.MARKET)<1,HH> = R.ISS.IN<INF.MLT.CURRENCY.MARKET><1,HH>
                R.NEW(INF.MLT.CURRENCY)<1,HH>        = R.ISS.IN<INF.MLT.CURRENCY><1,HH>
                R.NEW(INF.MLT.AMOUNT.LCY)<1,HH>      = R.ISS.IN<INF.MLT.AMOUNT.LCY><1,HH>
                R.NEW(INF.MLT.EXCHANGE.RATE)<1,HH>   = R.ISS.IN<INF.MLT.EXCHANGE.RATE><1,HH>
                R.NEW(INF.MLT.AMOUNT.FCY)<1,HH>      = R.ISS.IN<INF.MLT.AMOUNT.FCY><1,HH>
                R.NEW(INF.MLT.VALUE.DATE)<1,HH>      = R.ISS.IN<INF.MLT.VALUE.DATE><1,HH>
                R.NEW(INF.MLT.EXPOSURE.DATE)<1,HH>   = R.ISS.IN<INF.MLT.EXPOSURE.DATE><1,HH>
                R.NEW(INF.MLT.THEIR.REFERENCE)<1,HH> = R.ISS.IN<INF.MLT.THEIR.REFERENCE><1,HH>
                R.NEW(INF.MLT.CUSTOMER.ID)<1,HH>     = R.ISS.IN<INF.MLT.CUSTOMER.ID><1,HH>
                R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,HH> = R.ISS.IN<INF.MLT.ACCOUNT.OFFICER><1,HH>
                R.NEW(INF.MLT.PROD.CATEGORY)<1,HH>   = R.ISS.IN<INF.MLT.PROD.CATEGORY><1,HH>
                R.NEW(INF.MLT.OUR.REFERENCE)<1,HH>   = R.ISS.IN<INF.MLT.OUR.REFERENCE><1,HH>
                R.NEW(INF.MLT.POSITION.TYPE)<1,HH>   = R.ISS.IN<INF.MLT.POSITION.TYPE><1,HH>
                R.NEW(INF.MLT.DEPART.CODE)<1,HH>     = R.ISS.IN<INF.MLT.DEPART.CODE><1,HH>
                R.NEW(INF.MLT.DEALER.DESK)<1,HH>     = R.ISS.IN<INF.MLT.DEALER.DESK><1,HH>
                R.NEW(INF.MLT.BANK.SORT.CODE)<1,HH>  = R.ISS.IN<INF.MLT.BANK.SORT.CODE><1,HH>
                R.NEW(INF.MLT.CHEQUE.NUMBER)<1,HH>   = R.ISS.IN<INF.MLT.CHEQUE.NUMBER><1,HH>
            NEXT HH
        END
    END
    IF ISS.ID[1,2] EQ 'FT' THEN
        CALL F.READ(FN.ISS.FT,ISS.ID,R.ISS.FT,F.ISS.FT,ERR.FT)
        IF ERR.FT THEN
            ISS.ID.H = ISS.ID:';1'
            CALL F.READ(FN.ISS.FT.H,ISS.ID.H,R.ISS.FT,F.ISS.FT.H,ERR.FT.H)
        END
        IF R.ISS.FT THEN
            CHQ.NO     = R.ISS.FT<FT.CHEQUE.NUMBER>
            CHQ.NO.OLD = R.ISS.FT<FT.LOCAL.REF><1,FTLR.CHEQUE.NO>
            CR.ACCT    = R.ISS.FT<FT.CREDIT.ACCT.NO>
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CR.ACCT,CR.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
            DR.ACCT    = R.ISS.FT<FT.DEBIT.ACCT.NO>
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,DR.ACCT,DR.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
            IF CHQ.NO EQ '' AND CHQ.NO.OLD EQ '' THEN
                E = " ��� ���� ����� ����� ��� " ; CALL ERR ; MESSAGE = 'REPEAT'
            END
            R.NEW(INF.MLT.LOCAL.REF)<1,INLR.SUPP.CODE> = R.ISS.FT<FT.LOCAL.REF><1,FTLR.SUPP.CODE>
            R.NEW(INF.MLT.BILL.NO) = ''
**************************************************************************
            IF R.ISS.FT<FT.DEBIT.ACCT.NO>[1,2] EQ 'PL' THEN
                R.NEW(INF.MLT.PL.CATEGORY)<1,1>     = R.ISS.FT<FT.DEBIT.ACCT.NO>[3,6]
            END ELSE
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1>  = R.ISS.FT<FT.DEBIT.ACCT.NO>
            END
            R.NEW(INF.MLT.SIGN)<1,1>        = 'CREDIT'
            R.NEW(INF.MLT.TXN.CODE)<1,1>    = 79
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,1> = R.ISS.FT<FT.CURRENCY.MKT.DR>
            R.NEW(INF.MLT.CURRENCY)<1,1>        = R.ISS.FT<FT.DEBIT.CURRENCY>
            IF R.ISS.FT<FT.DEBIT.AMOUNT> NE '' THEN
                FT.AMT.LCY = R.ISS.FT<FT.DEBIT.AMOUNT>
            END ELSE
                FT.AMT.LCY = R.ISS.FT<FT.CREDIT.AMOUNT>
            END
            R.NEW(INF.MLT.AMOUNT.LCY)<1,1>      = FT.AMT.LCY
            R.NEW(INF.MLT.EXCHANGE.RATE)<1,1>   = R.ISS.FT<FT.IN.EXCH.RATE>
            R.NEW(INF.MLT.VALUE.DATE)<1,1>      = R.ISS.FT<FT.DEBIT.VALUE.DATE>
            R.NEW(INF.MLT.EXPOSURE.DATE)<1,1>   = R.ISS.FT<FT.EXPOSURE.DATE>
            R.NEW(INF.MLT.THEIR.REFERENCE)<1,1> = R.ISS.FT<FT.DEBIT.THEIR.REF>
            IF CR.ACCT[1,2] NE 'PL' THEN
                R.NEW(INF.MLT.CUSTOMER.ID)<1,1> = R.ISS.FT<FT.DEBIT.CUSTOMER>
            END
            R.NEW(INF.MLT.POSITION.TYPE)<1,1>   = R.ISS.FT<FT.POSITION.TYPE>
            IF DR.CAT EQ '16151' THEN
                R.NEW(INF.MLT.CHEQUE.NUMBER)<1,1>   = CHQ.NO
            END
*-------------------------------------------------------------------------

            IF R.ISS.FT<FT.CREDIT.ACCT.NO>[1,2] EQ 'PL' THEN
                R.NEW(INF.MLT.PL.CATEGORY)<1,2>     = R.ISS.FT<FT.CREDIT.ACCT.NO>[3,6]
            END ELSE
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,2>  = R.ISS.FT<FT.CREDIT.ACCT.NO>
            END
            R.NEW(INF.MLT.SIGN)<1,2>        = 'DEBIT'
            R.NEW(INF.MLT.TXN.CODE)<1,2>    = 78
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,2> = R.ISS.FT<FT.CURRENCY.MKT.CR>
            R.NEW(INF.MLT.CURRENCY)<1,2>        = R.ISS.FT<FT.CREDIT.CURRENCY>
            IF R.ISS.FT<FT.DEBIT.AMOUNT> NE '' THEN
                FT.AMT.LCY = R.ISS.FT<FT.DEBIT.AMOUNT>
            END ELSE
                FT.AMT.LCY = R.ISS.FT<FT.CREDIT.AMOUNT>
            END
*Line [ 151 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            XXX1 = DCOUNT(R.ISS.FT<FT.COMMISSION.TYPE>,@VM)
            FOR AAA = 1 TO XXX1
                 CC.COM += R.ISS.FT<FT.COMMISSION.AMT><1,AAA>[4,20]
            NEXT AAA
            FT.AMT.LCY = FT.AMT.LCY - CC.COM
            R.NEW(INF.MLT.AMOUNT.LCY)<1,2>      = FT.AMT.LCY
            R.NEW(INF.MLT.EXCHANGE.RATE)<1,2>   = ''
            R.NEW(INF.MLT.AMOUNT.FCY)<1,2>      = ''
            R.NEW(INF.MLT.VALUE.DATE)<1,2>      = R.ISS.FT<FT.CREDIT.VALUE.DATE>
            R.NEW(INF.MLT.EXPOSURE.DATE)<1,2>   = R.ISS.FT<FT.EXPOSURE.DATE>
            R.NEW(INF.MLT.THEIR.REFERENCE)<1,2> = R.ISS.FT<FT.CREDIT.THEIR.REF>
            IF CR.ACCT[1,2] NE 'PL' THEN
                R.NEW(INF.MLT.CUSTOMER.ID)<1,2> = R.ISS.FT<FT.CREDIT.CUSTOMER>
            END
            R.NEW(INF.MLT.ACCOUNT.OFFICER)<1,2> = ''
            R.NEW(INF.MLT.PROD.CATEGORY)<1,2>   = ''
            R.NEW(INF.MLT.OUR.REFERENCE)<1,2>   = ''
            R.NEW(INF.MLT.POSITION.TYPE)<1,2>   = R.ISS.FT<FT.POSITION.TYPE>
            R.NEW(INF.MLT.DEPART.CODE)<1,2>     = ''
            R.NEW(INF.MLT.DEALER.DESK)<1,2>     = ''
            R.NEW(INF.MLT.BANK.SORT.CODE)<1,2>  = ''
            IF CR.CAT EQ '16151' THEN
                R.NEW(INF.MLT.CHEQUE.NUMBER)<1,2>   = CHQ.NO
            END
**************************************************************************
*Line [ 177 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CCC = DCOUNT(R.ISS.FT<FT.COMMISSION.TYPE>,@VM)
            RRR  = CCC + 2
            FOR TTT = 1 TO CCC
                MMM = TTT + 2
                COMM.TYPE = R.ISS.FT<FT.COMMISSION.TYPE><1,TTT>
                CALL F.READ(FN.COMM,COMM.TYPE,R.COMM,F.COMM,ER.COMM)
                ACCT.NO = R.COMM<FT4.CATEGORY.ACCOUNT>
*                TEXT = ACCT.NO ; CALL REM
                XX11 = LEN(ACCT.NO)
                IF XX11 GT 10 THEN
                    R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,MMM>  = ACCT.NO
                END ELSE
                    R.NEW(INF.MLT.PL.CATEGORY)<1,MMM>     = ACCT.NO
                END
                R.NEW(INF.MLT.SIGN)<1,MMM>        = 'DEBIT'
                R.NEW(INF.MLT.TXN.CODE)<1,MMM>    = 78
                R.NEW(INF.MLT.CURRENCY.MARKET)<1,MMM> = R.ISS.FT<FT.CURRENCY.MKT.CR>
                R.NEW(INF.MLT.CURRENCY)<1,MMM>        = R.COMM<FT4.CURRENCY><1,1>
                R.NEW(INF.MLT.AMOUNT.LCY)<1,MMM>      = R.ISS.FT<FT.COMMISSION.AMT><1,TTT>[4,20]
                R.NEW(INF.MLT.VALUE.DATE)<1,MMM>      = R.ISS.FT<FT.CREDIT.VALUE.DATE>
            NEXT TTT
        END
    END
END
