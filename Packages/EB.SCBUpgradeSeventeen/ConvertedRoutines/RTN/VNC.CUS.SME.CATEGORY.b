* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDY2NDA6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
******** DINA_SCB ********

    SUBROUTINE VNC.CUS.SME.CATEGORY


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        FLAG = 'N'
        NO.OF.EMP    = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NO.OF.EMP>
        TURN.OVER    = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.TURNOVER>
        PAID.CAPITAL = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PAID.CAPITAL>
        SECTOR       = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
        IF SECTOR GE 2110 AND SECTOR LE 2260 THEN
            FLAG = 'Y'
        END

        IF TURN.OVER THEN

            BEGIN CASE
            CASE (TURN.OVER LT 1000000)
                SME.CAT  = '201'
            CASE (TURN.OVER GE 1000000 AND TURN.OVER LT 50000000)
                SME.CAT  = '205'
            CASE (TURN.OVER GE 50000000 AND TURN.OVER LE 200000000)
                SME.CAT  = '206'
            END CASE
        END

        IF (PAID.CAPITAL AND TURN.OVER EQ '') THEN

            BEGIN CASE
            CASE (PAID.CAPITAL LT 50000)
                SME.CAT  = '101'
            CASE (PAID.CAPITAL GE 50000 AND PAID.CAPITAL LT 3000000 AND FLAG EQ 'Y')
                SME.CAT  = '105'
            CASE (PAID.CAPITAL GE 50000 AND PAID.CAPITAL LT 5000000 AND FLAG EQ 'N')
                SME.CAT  = '105'
            CASE (PAID.CAPITAL GE 3000000 AND PAID.CAPITAL LE 5000000 AND FLAG EQ 'Y')
                SME.CAT  = '106'
            CASE (PAID.CAPITAL GE 5000000 AND PAID.CAPITAL LE 15000000 AND FLAG EQ 'N')
                SME.CAT  = '106'

            END CASE
        END

        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.SME.CATEGORY> = SME.CAT
*R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME> = ',SME.DATA'
        CALL REBUILD.SCREEN
        RETURN
    END
