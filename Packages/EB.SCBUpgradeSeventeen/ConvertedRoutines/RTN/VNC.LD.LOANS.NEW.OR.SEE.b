* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTkwNTQ6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
**---------DINA 18/09/2002---------**


SUBROUTINE VNC.LD.LOANS.NEW.OR.SEE


*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


IF V$FUNCTION # 'S' THEN
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*CALL DBR ("LD.LOANS.AND.DEPOSITS":@FM:LD.CUSTOMER.ID,ID.NEW,CUS)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
CUS=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>
IF NOT(ETEXT) THEN
  E = 'USE ONLY SEE FUNCTION';CALL ERR; MESSAGE = 'REPEAT'
END ELSE
IF V$FUNCTION = 'I' THEN
 R.NEW(LD.LOCAL.REF)<1,LDLR.PERSONCORP> = 'PERSON'
 R.NEW(LD.MIS.ACCT.OFFICER) = R.USER<EB.USE.DEPARTMENT.CODE>
 R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
 
END
END
END
 
RETURN
END
