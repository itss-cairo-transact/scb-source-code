* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTkzOTk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>95</Rating>
*-----------------------------------------------------------------------------
****    MOHAMED MAHMOUD IBRAHIM    ****

    SUBROUTINE VNC.LD.NEW.LOAN

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF V$FUNCTION = 'I' THEN
        R.NEW(LD.LOCAL.REF)<1,LDLR.VERSION.NAME> = PGM.VERSION
        R.NEW(LD.VALUE.DATE) = TODAY
*        R.NEW(LD.CHRG.CLAIM.DATE) = TODAY

        MYDUMMY = '' ; CATEG = '' ; MYLOAN = ''
*CATEG = R.NEW (LD.CATEGORY)
*CALL DBR('SCB.LOANS.PARMS':@FM:@ID,CATEG,MYDUMMY)
*
*IF ETEXT THEN
*    E = 'Must.Be.Within.Range'
*    CALL ERR ; MESSAGE = 'REPEAT'
*END

*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,MYLOAN)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOAN=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>
* IF NOT (ETEXT) THEN INPUT.BUFFER = C.U : ' LD,SCB.NEW.LOAN1  S ' : ID.NEW ;* V$FUNCTION= 'S' ; CALL REBUILD.SCREEN
        IF NOT (ETEXT) THEN  V$FUNCTION= 'S' ; CALL REBUILD.SCREEN

    END

    RETURN
END
