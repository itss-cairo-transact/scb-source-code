* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTUyOTY6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LC.ADD.COND


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT


DE = 'DEFAULT'
IF V$FUNCTION = 'I' THEN
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*   CALL DBR("SCB.LC.DEFAULT":@FM:SCB.LCV.NARR.PERIOD,DE,COND)
F.ITSS.SCB.LC.DEFAULT = 'F.SCB.LC.DEFAULT'
FN.F.ITSS.SCB.LC.DEFAULT = ''
CALL OPF(F.ITSS.SCB.LC.DEFAULT,FN.F.ITSS.SCB.LC.DEFAULT)
CALL F.READ(F.ITSS.SCB.LC.DEFAULT,DE,R.ITSS.SCB.LC.DEFAULT,FN.F.ITSS.SCB.LC.DEFAULT,ERROR.SCB.LC.DEFAULT)
COND=R.ITSS.SCB.LC.DEFAULT<SCB.LCV.NARR.PERIOD>
*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT (COND,@VM)
     TT = ''
     FOR J = 1 TO DD
     TT := ' ': COND<1,J>
     NEXT J
     R.NEW(TF.LC.NARRATIVE) = TRIM(TT)



END

RETURN
END
