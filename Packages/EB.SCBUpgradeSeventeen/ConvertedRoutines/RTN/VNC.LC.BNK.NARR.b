* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTUzNDc6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LC.BNK.NARR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT


    DE = 'DEFAULT'
    IF V$FUNCTION = 'I' THEN
        CALL OPF('F.SCB.LC.DEFAULT',F.SCB.LC.DEFAULT)
*READ R.SCB.LC.DEFAULT FROM F.SCB.LC.DEFAULT,"DEFAULT" THEN
        CALL F.READ ('F.SCB.LC.DEFAULT',"DEFAULT", R.SCB.LC.DEFAULT, F.SCB.LC.DEFAULT, FILE.ERR)
        IF NOT(FILE.ERR) THEN
            R.NEW(TF.LC.BANK.TO.BANK) =  R.SCB.LC.DEFAULT<SCB.LCV.BNK.T.BNK>
            R.NEW(TF.LC.PRESENT.PERIOD) = R.SCB.LC.DEFAULT<SCB.LCV.PERIOD.PRES>
        END
    END
*  R.NEW(TF.LC.BANK.TO.BANK)<1,1> = "PAYMENT TO BENEFICIARY UNDER RESERV"
*  R.NEW(TF.LC.BANK.TO.BANK)<1,2> = "E OR AGAINST INDEMNITY BY DEBITING"
*  R.NEW(TF.LC.BANK.TO.BANK)<1,3> = "OUR ACCOUNT OR BY DRAWING ON THE "
*  R.NEW(TF.LC.BANK.TO.BANK)<1,4> = "REIMBURSING BANK BEFORE OUR PRIOR"
*  R.NEW(TF.LC.BANK.TO.BANK)<1,5> = "APPROVAL IS NOT ACCEPTABLE"
*  R.NEW(TF.LC.BANK.TO.BANK)<1,6> = "ORIGINAL DOC.TO BE SENT BY D.H.L"
*    END
    RETURN
END
