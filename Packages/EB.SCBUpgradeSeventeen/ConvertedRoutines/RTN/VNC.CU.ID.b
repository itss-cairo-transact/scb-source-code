* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDU4MzM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
**---------BAKRY 15/01/2003 ---------**

SUBROUTINE VNC.CU.ID


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

NO2 = ''

IF V$FUNCTION = 'I' THEN
   IF R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER> THEN
   CH.NO = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER>
   KK = LEN(R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER>)
   FOR I = 1 TO KK
   NO1 = ''
   IF CH.NO[I,1] = "�" THEN R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE> = "2"
   IF ( CH.NO[I,1] >= CHAR(48) AND CH.NO[I,1] <= CHAR(57) ) THEN
   NO1 = CH.NO[I,1]
   NO2 = NO2 : NO1
   END
   NEXT I
   R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER> = NO2
   END

END

 RETURN
 END
