* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDM3NTg6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VNC.COLL.DEP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.R.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

**    IF ( V$FUNCTION = 'I' OR  V$FUNCTION = 'R' OR V$FUNCTION = 'A' ) THEN
    IF ( V$FUNCTION = 'I' OR  V$FUNCTION = 'R' ) THEN
        USER.DEP = R.USER <EB.USE.LOCAL.REF,USER.SCB.DEPT.CODE>
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COLLATERAL':@FM:COLL.LOCAL.REF,ID.NEW,LOCC)
F.ITSS.COLLATERAL = 'F.COLLATERAL'
FN.F.ITSS.COLLATERAL = ''
CALL OPF(F.ITSS.COLLATERAL,FN.F.ITSS.COLLATERAL)
CALL F.READ(F.ITSS.COLLATERAL,ID.NEW,R.ITSS.COLLATERAL,FN.F.ITSS.COLLATERAL,ERROR.COLLATERAL)
LOCC=R.ITSS.COLLATERAL<COLL.LOCAL.REF>
        RCRD.DEP = LOCC<1,COLR.DEPT.ISSUE>
        IF RCRD.DEP NE '' THEN
            IF USER.DEP NE RCRD.DEP THEN
         ***       E =  "��� ����� ����� ���� " ; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
**  LOCATE USER.DEP IN RCRD.DEP SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
**TEXT = "HHH"  : J ; CALL REM
* IF NOT(J) THEN
**    E =  "��� ����� ����� ���� " ; CALL ERR ; MESSAGE = 'REPEAT'
    END
    RETURN
END
