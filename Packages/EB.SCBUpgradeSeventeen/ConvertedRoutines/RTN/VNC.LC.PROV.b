* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTU4ODI6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*---- DALIA - SCB ----
*-----------------------------------------------------------------------------
* <Rating>400</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LC.PROV


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.ANNEX.INDEX
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
* CALL DBR("LETTER.OF.CREDIT":@FM:TF.LC.OPERATION,ID.NEW,OPER)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,ID.NEW,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
OPER=R.ITSS.LETTER.OF.CREDIT<TF.LC.OPERATION>
  IF ETEXT THEN E='MUST BE EXISTING'
  ELSE IF V$FUNCTION = 'I' AND R.NEW(TF.LC.RECORD.STATUS) = '' THEN
  IF R.NEW(TF.LC.EXTERNAL.REFERENCE) # "REL.PROVISION" THEN E = 'IT.IS.NOT.ALLOWED'
END
IF E THEN  CALL ERR ; MESSAGE = 'REPEAT'
RETURN
END
