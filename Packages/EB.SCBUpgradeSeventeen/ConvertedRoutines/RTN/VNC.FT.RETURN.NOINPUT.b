* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTE3NjI6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:31
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.FT.RETURN.NOINPUT
*To make fields Debit.Customer & Credit.customer in input mode
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF V$FUNCTION = "I" THEN
        T.DATE = TODAY
        R.NEW(FT.LOCAL.REF)<1, FTLR.VERSION.NAME> = PGM.VERSION

        T(FT.DEBIT.CUSTOMER)<3>      = ''
        R.NEW(FT.SEND.PAYMENT.Y.N)   = 'Y'
        R.NEW(FT.DR.ADVICE.REQD.Y.N) = 'N'
        R.NEW(FT.CR.ADVICE.REQD.Y.N) = 'N'
        R.NEW(FT.DEBIT.VALUE.DATE) = T.DATE
        R.NEW(FT.CREDIT.VALUE.DATE) = T.DATE

*        CALL REBUILD.SCREEN

    END
    RETURN
END
