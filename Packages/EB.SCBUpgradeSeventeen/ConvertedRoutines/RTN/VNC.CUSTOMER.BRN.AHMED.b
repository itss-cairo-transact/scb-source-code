* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDcyMDU6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUSTOMER.BRN.AHMED

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*The User should only update the customers in his branch and not in other branches
    ETEXT = ""
    IF V$FUNCTION ='I' THEN
**UPDATED BY NESSREEN AHMED IN 6/6/2008**
**  IF R.NEW( EB.CUS.DEPT.CODE) THEN
**      IF R.NEW( EB.CUS.DEPT.CODE) # R.USER< EB.USE.DEPARTMENT.CODE> THEN
*   IF R.NEW(EB.CUS.ACCOUNT.OFFICER) THEN
        IF ID.NEW THEN
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,ID.NEW,NEW)
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,ID.NEW,NEW)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ID.NEW,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
NEW=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
*  IF R.NEW(EB.CUS.ACCOUNT.OFFICER) # R.USER<EB.USE.DEPARTMENT.CODE> THEN

            IF NEW  # ID.COMPANY THEN
*E = 'THE CUSTOMER.IS FROM ANOTHER BRANCH'
                E = '��� ������ �� ��� ���'
                CALL ERR ; MESSAGE = 'REPEAT'

            END

        END
    END
    RETURN
END
