* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTkyNjU6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***ABEER AS OF 08/07/2020
    SUBROUTINE VNC.LD.MORT.CHK

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MORT.FIN



    IF V$FUNCTION EQ 'A' THEN
*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.MORT.FIN':@FM:SCB.MORTG.AUTHORISER,COMI,AUTH.REC)
F.ITSS.SCB.MORT.FIN = 'F.SCB.MORT.FIN'
FN.F.ITSS.SCB.MORT.FIN = ''
CALL OPF(F.ITSS.SCB.MORT.FIN,FN.F.ITSS.SCB.MORT.FIN)
CALL F.READ(F.ITSS.SCB.MORT.FIN,COMI,R.ITSS.SCB.MORT.FIN,FN.F.ITSS.SCB.MORT.FIN,ERROR.SCB.MORT.FIN)
AUTH.REC=R.ITSS.SCB.MORT.FIN<SCB.MORTG.AUTHORISER>
        IF AUTH.REC EQ '' THEN
            ETEXT='Details Not Entered Yet';CALL STORE.END.ERROR
        END

    END


    RETURN
END
