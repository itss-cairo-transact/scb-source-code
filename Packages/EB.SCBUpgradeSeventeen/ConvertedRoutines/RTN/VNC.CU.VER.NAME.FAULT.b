* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDU5ODE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>36</Rating>
*-----------------------------------------------------------------------------
** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VNC.CU.VER.NAME.FAULT

* IN THE INPUT FUNCTION MADE THE FIELD VERSION.NAME  EQUAL THE DEFAULT (PGM.VERSION)
* CHECK IF VERSION.NAME NE PGM.VERSION PRODUCE ERROR MSG
* E MSG INCLUDE & THAT REFER TO  THE RIGHT VERSION NAME

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------
    ETEXT = ""


*** UPDATE BY MOHAMED SABRY 2014/09/02

    VERSION.NAME  = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>

    IF V$FUNCTION = 'A' THEN
        IF (VERSION.NAME EQ ",SCB.PRIVATE.SUEZ" AND PGM.VERSION ",SCB.PRIVATE.SUEZ") THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME> = ",SCB.PRIVATE.UPDATE"
            CALL REBUILD.SCREEN
        END
    END
    IF V$FUNCTION = 'I' THEN
        IF (VERSION.NAME EQ ",SCB.PRIVATE.SUEZ" AND PGM.VERSION EQ ",SCB.PRIVATE.SUEZ") THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID> = ID.NEW
            RETURN
        END
    END
*** END UPDATE

    IF V$FUNCTION ='I' THEN
        ZZ   = FIELD(PGM.VERSION,".",1)
        ZZZ  = FIELD(PGM.VERSION,".",2)
        ZZZZ = ZZ : "." : ZZZ

        IF NOT(VERSION.NAME) THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME> = ZZZZ
        END ELSE
*----- EDIT BY NESSMA 3/3/2014
            VER.NM = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>
            IF VER.NM EQ ",SCB.PRIVATE" AND R.NEW(EB.CUS.SECTOR) EQ 1100 AND PGM.VERSION EQ ',SCB.STAFF.UPDATE.1' THEN
            END ELSE
*----- END 3/3/2014
****UPDATED BY NESSREEN AHMED 29/7/2019***************
****  IF VERSION.NAME # ZZZZ AND VERSION.NAME # ZZZZ:".UPDATE" AND VERSION.NAME # ZZZZ:".UPDATE.DEAD" THEN
                FINDSTR ZZZ IN VERSION.NAME SETTING POS THEN
**                   TEXT = ZZZ ; CALL REM
                END ELSE
****END OF UPDATE 29/7/2019***************************
                    E = 'YOU MUST RETRIVE THIS RECORD FROM VERSION &' : @FM : VERSION.NAME
                    VERSION.NAME1 = VERSION.NAME[6,15]
                    E = VERSION.NAME1:' ��� ������� ��� ����� �� ����'
                    CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END
    END
*-----------------------------------------------------------------
    RETURN
END
