* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDE1NDk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN AHMED 5/3/2013**************

    SUBROUTINE VNC.CHQ.RETURN.PAY.CHECK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW

    ETEXT = ""

    IF V$FUNCTION = 'C'  THEN
        E = '��� �����'
*** SCB UPG 20160621 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
    END
    IF V$FUNCTION = 'A'  THEN
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.INPUTTER, ID.NEW ,INPUTT)
F.ITSS.SCB.CHQ.RETURN.NEW$NAU = 'F.SCB.CHQ.RETURN.NEW$NAU'
FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW$NAU,FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW$NAU,ID.NEW,R.ITSS.SCB.CHQ.RETURN.NEW$NAU,FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU,ERROR.SCB.CHQ.RETURN.NEW$NAU)
INPUTT=R.ITSS.SCB.CHQ.RETURN.NEW$NAU<CHQ.RET.INPUTTER>
        FMT.INP = FIELD(INPUTT, "_" ,2)
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, FMT.INP , INP.CO)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,FMT.INP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.CO=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, OPERATOR , AUTH.CO)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,OPERATOR,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
AUTH.CO=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
        IF INP.CO # AUTH.CO THEN
            E = '��� ����� ������ ��� ��� ���'
*** SCB UPG 20160621 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END ELSE
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('SCB.CHQ.RETURN.NEW$NAU':@FM:CHQ.RET.RECORD.STAT, ID.NEW ,REC.STAT)
F.ITSS.SCB.CHQ.RETURN.NEW$NAU = 'F.SCB.CHQ.RETURN.NEW$NAU'
FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN.NEW$NAU,FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN.NEW$NAU,ID.NEW,R.ITSS.SCB.CHQ.RETURN.NEW$NAU,FN.F.ITSS.SCB.CHQ.RETURN.NEW$NAU,ERROR.SCB.CHQ.RETURN.NEW$NAU)
REC.STAT=R.ITSS.SCB.CHQ.RETURN.NEW$NAU<CHQ.RET.RECORD.STAT>
            TEXT = 'REC.STAT=':REC.STAT ; CALL REM
            IF REC.STAT # '����' THEN
                E = '��� ����� ��������'
*** SCB UPG 20160621 - S
*        CALL ERR
*        MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
            END
        END
    END

    RETURN
END
