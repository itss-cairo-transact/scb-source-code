* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTU5Mjc6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
************** INGY-SCB 104/2003**************



SUBROUTINE VNC.LC.VERSION.NAME
*A ROUTINE TO CHECK
                   *IF THE EB.USE.DEPARTMENT.CODE #'99' THEN
                   * IF THE  TF.LC.ACCOUNT.OFFICER # EB.USE.DEPARTMENT.CODE DISPLAY ERROR
                   * ELSE PUT IN LINK.DATA<TNO> PGM.VERSION

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

IF V$FUNCTION='I' THEN

  IF R.NEW(TF.LC.ACCOUNT.OFFICER) THEN
  IF R.NEW(TF.LC.ACCOUNT.OFFICER) # R.USER<EB.USE.DEPARTMENT.CODE> AND R.USER<EB.USE.DEPARTMENT.CODE> # '99' THEN
   E = 'THIS.OPER.FROM.OTHER.BRANCH';CALL ERR ;MESSAGE = 'REPEAT'
   END
   END ELSE
    R.NEW(TF.LC.ACCOUNT.OFFICER) = R.USER<EB.USE.DEPARTMENT.CODE>
    R.NEW(TF.LC.LOCAL.REF)<1,LCLR.VERSION.NAME> = PGM.VERSION
     LINK.DATA<TNO> = PGM.VERSION
   * R.NEW(TF.LC.AVAIL.WITH.CUSTNO) =
    R.NEW(TF.LC.AVAILABLE.WITH) = 'SCB'
     R.NEW(TF.LC.EXPIRY.PLACE) = 'AT OUR COUNTRIES'

    END
END


RETURN
END
