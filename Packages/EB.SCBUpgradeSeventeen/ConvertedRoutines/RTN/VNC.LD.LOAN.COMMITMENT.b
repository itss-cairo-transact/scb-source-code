* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTg4Njk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
**********WAEL********
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.LOAN.COMMITMENT
*A Routine To To check IF the record is existing and Category EQ 21095(Commitment)
*Allow function SEE only , also To Default LD.VALUE.DATE ,LD.INT.START.DATE,LD.DD.START.DATE,with Today.
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS


*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,ID.NEW,XXX)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
XXX=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CATEGORY>

    IF NOT(ETEXT) THEN
        IF XXX # 21095 THEN E = 'NOT.ALLOWED.TO.ENTER' ; CALL ERR ; MESSAGE = 'REPEAT'
        ELSE
            IF XXX = 21095 THEN E = 'ONLY.SEE.FUNCTION' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END ELSE

        R.NEW( LD.VALUE.DATE )= TODAY
        R.NEW( LD.INT.START.DATE )= TODAY
        R.NEW( LD.D.D.START.DATE )= TODAY

    END

    RETURN
END
