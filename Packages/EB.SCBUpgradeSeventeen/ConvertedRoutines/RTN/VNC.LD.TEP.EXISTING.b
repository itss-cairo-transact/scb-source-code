* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTk1NDg6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>198</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.TEP.EXISTING

* A ROUTINE TO DEFAULT THE FIELD LD.AMT.V.DATE WITH THE DATE OF TODAY
* A ROUTINE TO CHECK IF THE CONTRACT DOESN'T BELONG TO ALOAN CATEGORY THEN DISPLAY ERROR

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.PARMS1

***************************************************************
    R.NEW(LD.DRAWDOWN.ENT.DATE) = R.NEW(LD.VALUE.DATE)
***************************************************************

*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CUSTOMER.ID,ID.NEW,ID)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
ID=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.CUSTOMER.ID>
    IF ETEXT THEN E = 'MUST.BE.EXISTING';CALL ERR ; MESSAGE = 'REPEAT'

*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.LOANS.PARMS':@FM:SCB.LO.PAR.DESCRIPTION,R.NEW(LD.CATEGORY),CATEG)
F.ITSS.SCB.LOANS.PARMS = 'F.SCB.LOANS.PARMS'
FN.F.ITSS.SCB.LOANS.PARMS = ''
CALL OPF(F.ITSS.SCB.LOANS.PARMS,FN.F.ITSS.SCB.LOANS.PARMS)
CALL F.READ(F.ITSS.SCB.LOANS.PARMS,R.NEW(LD.CATEGORY),R.ITSS.SCB.LOANS.PARMS,FN.F.ITSS.SCB.LOANS.PARMS,ERROR.SCB.LOANS.PARMS)
CATEG=R.ITSS.SCB.LOANS.PARMS<SCB.LO.PAR.DESCRIPTION>
*   IF ETEXT THEN E = 'MUST.BELONG.TO.A.LOAN.CATEGORY';CALL ERR ; MESSAGE = 'REPEAT'

    IF R.NEW(LD.CATEGORY) LT '21050'OR R.NEW(LD.CATEGORY) GT '21055'THEN ETEXT = 'MUST.BE.A.LOAN.CATEGORY'

* R.NEW(LD.AMT.V.DATE) = TODAY

    RETURN
END
