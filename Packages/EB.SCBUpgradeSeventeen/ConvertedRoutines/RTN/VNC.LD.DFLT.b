* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTgwOTE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
***ABEER 11-8-2020
*TO DEFAULT PRINCIPAL AMOUNT FROM  CSV FILE CREATED BY THE USER
*FILE NAME IS "LOAN_OPERATOR(SECOND VALUE AFTER . ).csv" should created
*by user
*-----------------------------------------------------------------------------
* <Rating>45</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LD.DFLT

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

*TO DEFAULT FEILDS SCH.TYPE,SD.FREQUENCY,SD.DATE

    IF V$FUNCTION = 'I' THEN
**TEXT = 'INPUT MODE ';CALL REM
        DEFFUN SHIFT.DATE( )
        LD.ID = '' ; LD.ID = ID.NEW
        GOSUB OPEN.FILES ; R.TERM = '' ; ER.MSG = ''
        GOSUB DELETE.FIELDS
        GOSUB PROCESS.LD.SCHEDULES

        IF ER.MSG THEN
            E = ER.MSG ; CALL ERR ; MESSAGE = 'REPEAT'
        END

    END
    GOTO PROGRAM.END

*----------------------- OPEN FILES -------------------------------------------*
OPEN.FILES:

    VF.LD.LOANS.AND.DEPOSITS$NAU = ''
    CALL OPF('F.LD.LOANS.AND.DEPOSITS$NAU', VF.LD.LOANS.AND.DEPOSITS$NAU)

    RETURN
*------------------------------------------------------------------------------*
*------------------ PROCESS LD SCHEDULES --------------------------------------*
PROCESS.LD.SCHEDULES:

    CALL F.READ('FBNK.LD.LOANS.AND.DEPOSITS$NAU',
    LD.ID, R.TERM, VF.LD.LOANS.AND.DEPOSITS$NAU, ER.MSG)
    IF NOT(ER.MSG) THEN

******************************
        GOSUB PROCESS.FRQ.MONTHS

    END ELSE
        ER.MSG = 'Missing LD.LOANS.AND.DEPOSITS$NAU record ':LD.ID
    END

    RETURN
*------------------ PROCESS FREQUENCY IN MONTHS -------------------------------*
PROCESS.FRQ.MONTHS:

*    R.NEW(LD.SD.FORWARD.BACKWARD)='5'
*    R.NEW(LD.SD.BASE.DATE.KEY)='1'

**********************
    SEQ.FILE.NAME = "MECH"
*  SEQ.FILE.NAME ="c:\temp\TEST.csv"

    USR.NO = FIELD(OPERATOR,'.',2)
    FIL.NAME ="LOAN_":USR.NO:".csv"
* FIL.NAME ="TEST.csv"
    OPENSEQ SEQ.FILE.NAME , FIL.NAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE TEST.csv  CREATED IN MECH'
        END
        ELSE
            STOP 'Cannot create TEST.CSV'
        END
    END
    EOF = ''
    J = '0'
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM BB THEN

****************************

            J ++
            LD.PRIN  = FIELD(Y.MSG,',',1,1)
            NO.INT.MON='1M'

            IF J = 1 THEN
                ST.DATE = ''
                ST.DATE = R.TERM<LD.VALUE.DATE>
                ST.DATE=SHIFT.DATE(ST.DATE,NO.INT.MON,UP)
            END ELSE
                ST.DATE=SHIFT.DATE(ST.DATE,NO.INT.MON,UP)
            END
            R.NEW(LD.SD.DATE)<1,J>=ST.DATE


            EN.DATE = '' ; EN.DATE = R.TERM<LD.FIN.MAT.DATE>
            NO.INT.MON='1M'
            FIRST.INT.MON=SHIFT.DATE(ST.DATE,NO.INT.MON,UP)

            R.NEW(LD.SD.SCH.TYPE)<1,J> = 'PI'

*            R.NEW(LD.SD.FREQUENCY)<1,J>='1M'
            R.NEW(LD.SD.AMOUNT)<1,J>=LD.PRIN

*************************

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ BB
****************
    RETURN
*------------------ DELETE ALL FIELDS FROM THE GROUP --------------------------*
DELETE.FIELDS:
    R.NEW(LD.SD.SCH.TYPE) = ''
    R.NEW(LD.SD.DATE) = ''
    R.NEW(LD.SD.AMOUNT) = ''
    R.NEW(LD.SD.RATE) = ''
    R.NEW(LD.SD.CHARGE.CODE) = ''
    R.NEW(LD.SD.CHG.BASE.AMT) = ''
    R.NEW(LD.SD.NUMBER) = ''
    R.NEW(LD.SD.FREQUENCY) = ''
    R.NEW(LD.SD.DIARY.ACTION) = ''
    R.NEW(LD.SD.NOTE.DENOM) = ''
    R.NEW(LD.SD.NOTE.QUANT) = ''
    R.NEW(LD.SD.CYCLED.DATES) = ''
    R.NEW(LD.SD.FREQ.CODE) = ''
    R.NEW(LD.SD.INCLUSIVE.CHG) = ''

    RETURN
*------------------------------------------------------------------------------*
PROGRAM.END:

    RETURN

END
