* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDk2MTM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 14/05/2003***********

    SUBROUTINE VNC.DRAWINGS.DEFAULTS.DP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS

    IF V$FUNCTION='I' THEN
        CALL OPF('F.LETTER.OF.CREDIT',F.LETTER.OF.CREDIT)
*READ R.LETTER.OF.CREDIT FROM F.LETTER.OF.CREDIT,ID.NEW[1,12] THEN
        CALL F.READ ('F.LETTER.OF.CREDIT', ID.NEW[1,12], R.LETTER.OF.CREDIT,F.LETTER.OF.CREDIT,FILE.ERR)
        IF NOT(FILE.ERR) THEN

            R.NEW(TF.DR.PRESENTOR.CUST) = R.LETTER.OF.CREDIT<TF.LC.ADVISING.BK.CUSTNO>
            R.NEW(TF.DR.DOCUMENT.CODE) = R.LETTER.OF.CREDIT<TF.LC.DOCUMENT.CODE>
*R.NEW(TF.DR.ADDITIONAL.DOCS) = R.LETTER.OF.CREDIT<TF.LC.DOCUMENT.TXT>
            R.NEW(TF.DR.CLAUSES.TEXT) = R.LETTER.OF.CREDIT<TF.LC.CLAUSES.TEXT>
            VERSION.NAME =  R.NEW( TF.DR.LOCAL.REF)<1, DRLR.VERSION.NAME>
            IF NOT(VERSION.NAME) THEN
                R.NEW( TF.DR.LOCAL.REF)< 1, DRLR.VERSION.NAME> = PGM.VERSION
            END
        END
    END
    RETURN
END
