* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDc2ODY6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*------------MAI SAAD 2019-----------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VNC.DB.PARAMS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------------------
 *   TEXT = 'HI'; CALL REM
 *   TEXT = 'message = ': MESSAGE ; CALL REM
    IF MESSAGE NE 'VAL' THEN
        IF V$FUNCTION = 'I' THEN
*            TEXT = 'SETT';CALL REM;
            R.NEW(FT.TRANSACTION.TYPE)  = 'ACCM'
            R.NEW(FT.DEBIT.AMOUNT) = '15'
            R.NEW(FT.CREDIT.ACCT.NO) = 'PL57044'
            R.NEW(FT.CREDIT.CURRENCY) = 'EGP'

*        R.NEW(FT.TRANSACTION.TYPE)  = 'ACCM'
        END
    END
*-------------------------------------------------------
    RETURN
END
