* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDkzNzA6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.DR.OUR.REF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS

    IF V$FUNCTION = 'I'  THEN
        ID.LC = ID.NEW[1,12]
        TEXT = ID.LC ; CALL REM
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*         CALL DBR ('LETTER.OF.CREDIT':@FM:TF.LC.EXTERNAL.REFERENCE,ID.LC,DR.REF)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,ID.LC,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
DR.REF=R.ITSS.LETTER.OF.CREDIT<TF.LC.EXTERNAL.REFERENCE>
        TEXT = DR.REF ; CALL REM
        R.NEW(TF.DR.LOCAL.REF)<1,DRLR.OUR.REFERENCE> = DR.REF

    END

    RETURN
END
