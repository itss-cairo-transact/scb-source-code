* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTM0MjA6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>398</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.GET.CLR.FORN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN

    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    TOD = TODAY


*    Path = "/life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING/FCY.CLEARING.":TODAY

    Path = "FCY.CLEARING/FCY.CLEARING.":TODAY
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 1
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,"|",2)
            RETURN.REASON     = FIELD(Line,"|",7)
            DEPT.CODE         = FIELD(Line,"|",1)

            DEPT.USR  = TRIM(ID.COMPANY[8,2],'0',"L")

*     IF OUR.ID AND R.USER<EB.USE.DEPARTMENT.CODE> EQ DEPT.CODE THEN

            IF DEPT.USR EQ DEPT.CODE THEN

                IF RETURN.REASON EQ "" THEN
                    R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = OUR.ID
                END
                IF RETURN.REASON NE "" THEN
                    R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = OUR.ID
                    R.NEW(SCB.BT.RETURN.REASON)<1,I> = RETURN.REASON
                END
                I = I + 1
            END

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    RETURN
END
