* @ValidationCode : MjotNTUxMjY4MTA1OkNwMTI1MjoxNjQ1MTA2MTUyMzc1OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:55:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
***CREATED BY MAI SAAD 26-9-2017
SUBROUTINE  VNC.CUSTOMER.EXIST.EBANKING

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS


*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*     CALL DBR('CUSTOMER':@FM:EB.CUS.MNEMONIC,ID.NEW,CUSID)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,ID.NEW,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    CUSID=R.ITSS.CUSTOMER<EB.CUS.MNEMONIC>
    IF CUSID=""
    THEN
        E = 'MUST.BE.EXISTING.RECORD' ; CALL ERR ; MESSAGE = 'REPEAT'
    END
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
* ** CALL DBR('CUSTOMER':@FM:INTER.BNK.DATE,ID.NEW,EBANKIN.CUS)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,ID.NEW,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    EBANKIN.CUS=R.ITSS.CUSTOMER<CULR.INTER.BNK.DATE>
** IF EBANKIN.CUS = ''  THEN
    
    IF NOT(R.NEW(EB.CUS.LOCAL.REF)<1,CULR.INTER.BNK.DATE>)
    THEN
        E = 'This customer is not e-banking user'
        CALL ERR ; MESSAGE = 'REPEAT'

    END
   
RETURN
END
 
