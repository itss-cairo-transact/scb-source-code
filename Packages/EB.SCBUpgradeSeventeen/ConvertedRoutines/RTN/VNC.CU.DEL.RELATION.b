* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDUyNTM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1013</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CU.DEL.RELATION

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ;  R.CU = '' ; R.REL.CU = ''
    CALL OPF (FN.CU,F.CU)

    FN.REL.CU = 'FBNK.CUSTOMER' ; F.REL.CU = '' ;  R.CU = '' ; R.REL.CU = ''
    CALL OPF (FN.REL.CU,F.REL.CU)


    WS.SYS.DATE = TODAY
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C'  THEN
        OPENSEQ "OFS.MNGR.IN" , "IT.DEL.REL" TO BB.IN THEN
            CLOSESEQ BB.IN
*            HUSH ON
*            EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"IT.DEL.REL"
*            HUSH OFF
        END
        OPENSEQ "OFS.MNGR.IN" , "IT.DEL.REL" TO BB.IN ELSE
            CREATE BB.IN THEN
            END ELSE
                STOP 'Cannot create IT.DEL.REL File IN OFS.MNGR.IN'
            END
        END
        EOF = ''
        LOOP WHILE NOT(EOF)
            READSEQ Y.MSG FROM BB.IN THEN
            END ELSE
                EOF = 1
            END
        REPEAT

*DEBUG
        WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
        WS.PLACE.ID.ISSUE   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE>

        IF WS.PLACE.ID.ISSUE NE '' THEN
            IF NUM (WS.PLACE.ID.ISSUE) THEN
                IF WS.PLACE.ID.ISSUE LT 1000 THEN
                    WS.PLACE.ID.ISSUE   = WS.PLACE.ID.ISSUE + 1000
                    WS.PLACE.ID.ISSUE   = WS.PLACE.ID.ISSUE[2,3]
                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> = WS.PLACE.ID.ISSUE
                END ELSE
                    R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> = ''
                END
            END ELSE
                R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE> = ''

            END
        END
*=======================================================================================================================
*Line [ 78 ] Comment DEBUG - ITSS - R21 Upgrade - 2022-02-09
        **DEBUG
        IF WS.POST.REST GE 70 THEN
            REL.CUS = R.NEW(EB.CUS.REL.CUSTOMER)
*Line [ 81 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            REL.COUNT = DCOUNT(REL.CUS,@VM)
            IF REL.COUNT GT 0 THEN
                BB.IN.DATA  = ID.NEW
                FOR ICOUNT  = 1 TO REL.COUNT
                    IF REL.COUNT = 1 THEN
                        BB.IN.DATA :='|':R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT>
                        R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT>  = ''
                        R.NEW(EB.CUS.RELATION.CODE)<1,ICOUNT> = ''
                    END ELSE
                        BB.IN.DATA :='|':R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT>
                        DEL R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT>
                        DEL R.NEW(EB.CUS.RELATION.CODE)<1,ICOUNT>
                        ICOUNT --
                        REL.COUNT --
                        *REL.REL.CUS    = R.NEW(EB.CUS.REL.CUSTOMER)
                    END
                NEXT ICOUNT
                WRITESEQ BB.IN.DATA TO BB.IN ELSE
                END
            END
        END
*=======================================================================================================================
*DEBUG
        IF WS.POST.REST LE 90 THEN

            REL.REL.COUNT  = 0
            REL.REL.CUS    = R.NEW(EB.CUS.REL.CUSTOMER)
*Line [ 109 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            REL.REL.COUNT  = DCOUNT(REL.REL.CUS,@VM)

*    BB.IN.DATA = ''
*    BB.IN      = ''

            IF REL.REL.COUNT GT 0 THEN
                BB.IN.DATA     = ID.NEW
                FOR ICOUNT.REL = 1 TO REL.REL.COUNT

                    CALL F.READ(FN.REL.CU,REL.REL.CUS<1,ICOUNT.REL>,R.REL.CU,F.REL.CU,ER.R.CU)

                    WS.REL.POSTING = R.REL.CU<EB.CUS.POSTING.RESTRICT>

*WS.REL.CODE    = R.CU<EB.CUS.RELATION.CODE,ICOUNT.REL>
                    IF WS.REL.POSTING GE 90 THEN
                        IF REL.REL.COUNT = 1 THEN
                            BB.IN.DATA :='|':R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT.REL>
                            R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT.REL>  = ''
                            R.NEW(EB.CUS.RELATION.CODE)<1,ICOUNT.REL> = ''
                        END ELSE
                            BB.IN.DATA :='|':R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT.REL>
                            DEL R.NEW(EB.CUS.REL.CUSTOMER)<1,ICOUNT.REL>
                            DEL R.NEW(EB.CUS.RELATION.CODE)<1,ICOUNT.REL>
                            ICOUNT.REL --
                            REL.REL.COUNT --
                            REL.REL.CUS    = R.NEW(EB.CUS.REL.CUSTOMER)
                        END
                    END
                NEXT ICOUNT.REL
                IF ID.NEW NE BB.IN.DATA THEN
                    WRITESEQ BB.IN.DATA TO BB.IN ELSE
                    END
                END
            END
        END
*=======================================================================================================================
    END
    RETURN
END
