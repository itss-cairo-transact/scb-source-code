* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDk5OTY6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.EXP.P

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP.P

    IF V$FUNCTION = "I" THEN

        CUS  = FIELD(ID.NEW,".",1)
        TYPE = FIELD(ID.NEW,".",2)
        CURR = FIELD(ID.NEW,".",3)
        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        CALL F.READ(FN.CUS,CUS,R.CUS,F.CUS,E1)

        FN.CURR = 'FBNK.CURRENCY' ; F.CURR = ''
        CALL OPF(FN.CURR,F.CURR)
        CALL F.READ(FN.CURR,CURR,R.CURR,F.CURR,E2)

        IF NOT( TYPE EQ 'FN' OR TYPE EQ 'AD' OR TYPE EQ 'BB' ) THEN
            E3 = "HH"
        END

        IF ( E1 OR E2 OR E3 ) THEN

            E = "ID MUST BE CU.TYPE.CCY"
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END ELSE
            R.NEW(EXP.P.CUSTOMER) = CUS
            R.NEW(EXP.P.TYPE)     = TYPE
            R.NEW(EXP.P.CURRENCY) = CURR

        END

    END
    RETURN
END
