* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTU1MTE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LC.DEF.MT730

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS


    DE = 'DEF.MT730'
    IF V$FUNCTION = 'I' THEN
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.LC.DEFAULT':@FM:SCB.LCV.BNK.T.BNK,DE,BNK)
F.ITSS.SCB.LC.DEFAULT = 'F.SCB.LC.DEFAULT'
FN.F.ITSS.SCB.LC.DEFAULT = ''
CALL OPF(F.ITSS.SCB.LC.DEFAULT,FN.F.ITSS.SCB.LC.DEFAULT)
CALL F.READ(F.ITSS.SCB.LC.DEFAULT,DE,R.ITSS.SCB.LC.DEFAULT,FN.F.ITSS.SCB.LC.DEFAULT,ERROR.SCB.LC.DEFAULT)
BNK=R.ITSS.SCB.LC.DEFAULT<SCB.LCV.BNK.T.BNK>
        IF BNK THEN
*Line [ 37 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR I = 1 TO DCOUNT(BNK, @VM)
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.BANK.T.BANK.730,I> = BNK<1,I>
            NEXT I
        END

        RETURN
    END
