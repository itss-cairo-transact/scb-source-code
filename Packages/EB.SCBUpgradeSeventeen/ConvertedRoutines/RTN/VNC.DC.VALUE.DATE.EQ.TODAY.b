* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDc4NTA6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
**---------NESSREEN---------**

    SUBROUTINE VNC.DC.VALUE.DATE.EQ.TODAY

* IF THE FUNC IS INPUT THEN THE VALUE.DATE DEFAULTED TO TODAY

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATA.CAPTURE

    IF V$FUNCTION = 'I' THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('TELLER.USER':@FM:1,OPERATOR,USE.ID)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
USE.ID=R.ITSS.TELLER.USER<1>
       * IF USE.ID[3,2] # '99' THEN
       *  IF USE.ID[3,2] # '98' THEN
         *   E = '����� ��� ���������������';CALL ERR ;MESSAGE = 'REPEAT'
        *END
        *DEP = R.USER<EB.USE.DEPARTMENT.CODE>
        *BR = FMT(DEP, "R%2")
        *R.NEW( DC.DC.ACCOUNT.NUMBER)= 'EGP10000':BR:'99'
        R.NEW( DC.DC.ACCOUNT.NUMBER)= 'EGP10000':'9999'
        R.NEW( DC.DC.VALUE.DATE) = TODAY
    END
    RETURN
END
