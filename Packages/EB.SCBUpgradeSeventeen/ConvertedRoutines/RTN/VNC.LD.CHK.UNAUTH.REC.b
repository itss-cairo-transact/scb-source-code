* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTc1ODk6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
**------------NESSREEN AHMED 29/03/2009---------------*

    SUBROUTINE VNC.LD.CHK.UNAUTH.REC

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    IF V$FUNCTION ='I' THEN

**    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,ID.NEW,AMT)
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.AMOUNT,ID.NEW,AMT1)
F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = 'F.LD.LOANS.AND.DEPOSITS$NAU'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS$NAU,FN.F.ITSS.LD.LOANS.AND.DEPOSITS$NAU,ERROR.LD.LOANS.AND.DEPOSITS$NAU)
AMT1=R.ITSS.LD.LOANS.AND.DEPOSITS$NAU<LD.AMOUNT>
        TEXT = AMT1 ; CALL REM
        IF AMT1 THEN
            E='��� ����� ����� ����� ���� ����';CALL ERR ;MESSAGE = 'REPEAT'
        END
    END
    RETURN
END
