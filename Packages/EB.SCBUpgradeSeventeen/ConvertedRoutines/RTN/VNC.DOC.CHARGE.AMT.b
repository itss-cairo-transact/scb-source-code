* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDkwODM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
    SUBROUTINE VNC.DOC.CHARGE.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE

* Created by Noha Hamed
    KEY.LIST = '' ; SELECTED = 0 ; ASD = '' ; R.DOC = ""
    FN.DOC.PRO = "FBNK.FT.CHARGE.TYPE" ; F.DOC.PRO = ''
    FN.CUR = 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
    T.SEL  = "";KEY.LIST = "";SELECTED = 0;ER.MSG = ''
    R.CUR.ACCT=''; R.CUR.USD=''
**********************GET DATA FROM SCREEN AND CURRENCY OF ACTT*********

    AMOUNT       = R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,AV>
    CHR.TYPE     = R.NEW(DOC.PRO.CHARGE.TYPE)<1,AV>
    ACCT         = R.NEW( DOC.PRO.DEBIT.ACCT)
    TYPE.CHANGE  = R.NEW(DOC.PRO.CHARGE.TYPE)
    CHRG.AMT.OLD =  R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>
    CURR.NO    = ACCT[9,2]
    PAY.AMOUNT = R.NEW(DOC.PRO.AMOUNT)
    CALL OPF( FN.CUR,F.CUR)
    T.SEL = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ " : CURR.NO
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        CURR.ACCT = KEY.LIST<1>
    END ELSE
        CURR.ACCT = 'EGP'
    END
    CALL F.READ(FN.CUR,CURR.ACCT, R.CUR.ACCT, F.CUR ,E11)
    CUR.RATE.ACCT = R.CUR.ACCT<EB.CUR.MID.REVAL.RATE,1>
    IF CUR.RATE.ACCT EQ '' THEN
        CUR.RATE.ACCT = 1
    END
***********************************************************************

    IF ACCT NE '' AND  CHRG.AMT.OLD EQ '' THEN

        IF CHR.TYPE EQ "COPYCHRG"  THEN
            CALL OPF( FN.DOC.PRO,F.DOC.PRO)
            CALL F.READ( FN.DOC.PRO,CHR.TYPE, R.DOC, F.DOC.PRO, ETEXT)
            CURR.AMOUNT = R.DOC<FT5.FLAT.AMT>
            FIN.AMT = CURR.AMOUNT / CUR.RATE.ACCT
            R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>= FIN.AMT
            CALL REBUILD.SCREEN
        END
        ELSE
**************************TO GET CURRENCY*****************************
            CURR = R.NEW(DOC.PRO.CURRENCY)
            CALL F.READ(FN.CUR,CURR, R.CUR, F.CUR ,E11)
            CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
            IF CUR.RATE EQ '' THEN
                CUR.RATE = 1
            END

            CURR.AMOUNT = CUR.RATE * PAY.AMOUNT

            IF CURR.AMOUNT GE 1 AND CURR.AMOUNT LT 5000 THEN
                R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,AV>=5 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 5000 AND CURR.AMOUNT LT 20000 THEN
                R.NEW(DOC.PRO.CHARGE.AMOUNT)<1,AV>=100 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 20000 AND CURR.AMOUNT LT 50000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=150 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 50000 AND CURR.AMOUNT LT 100000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=200 / CUR.RATE.ACCT
            END
            ELSE IF  CURR.AMOUNT GE 100000 AND CURR.AMOUNT LT 150000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=300 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 150000 AND CURR.AMOUNT LT 200000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=350 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 200000 AND CURR.AMOUNT LT 250000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=400 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 250000 AND CURR.AMOUNT LT 300000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=500 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 300000 AND CURR.AMOUNT LT 400000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=600 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GE 400000 AND CURR.AMOUNT LE 500000 THEN
                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=750 / CUR.RATE.ACCT
            END
            ELSE IF CURR.AMOUNT GT 500000 THEN

                AMT = 750
                REST.AMT = CURR.AMOUNT - 500000
                LOOP WHILE REST.AMT GT 0 DO
                    IF REST.AMT GE 1 AND REST.AMT LT 5000 THEN
                        AMT = AMT + 5
                    END
                    ELSE IF REST.AMT GE 5000 AND REST.AMT LT 20000 THEN
                        AMT = AMT + 100
                    END
                    ELSE IF REST.AMT GE 20000 AND REST.AMT LT 50000 THEN
                        AMT = AMT + 150
                    END

                    ELSE IF REST.AMT GE 50000 AND REST.AMT LT 100000 THEN
                        AMT = AMT + 200
                    END
                    ELSE IF REST.AMT GE 100000 AND REST.AMT LT 150000 THEN
                        AMT = AMT + 300
                    END
                    ELSE IF REST.AMT GE 150000 AND REST.AMT LT 200000 THEN
                        AMT = AMT + 350
                    END
                    ELSE IF REST.AMT GE 200000 AND REST.AMT LT 250000 THEN
                        AMT = AMT + 400
                    END
                    ELSE IF REST.AMT GE 250000 AND REST.AMT LT 300000 THEN
                        AMT = AMT + 500
                    END
                    ELSE IF REST.AMT GE 300000 AND REST.AMT LT 400000 THEN
                        AMT = AMT + 600
                    END
                    ELSE IF REST.AMT GE 400000 THEN
                        AMT = AMT + 750
                    END

                    REST.AMT = REST.AMT - 500000
                REPEAT


                R.NEW( DOC.PRO.CHARGE.AMOUNT)<1,AV>=AMT / CUR.RATE.ACCT
            END
        END



        CALL REBUILD.SCREEN



        RETURN
    END
