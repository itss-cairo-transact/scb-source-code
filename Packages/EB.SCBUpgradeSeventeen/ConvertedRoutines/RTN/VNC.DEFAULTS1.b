* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDg2MTE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
****************************NI7OOOOOO***
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.DEFAULTS1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

    COMP = ID.COMPANY

*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*    TEXT = "BRANCH :": BRANCH ; CALL REM

    R.NEW(DEPT.SAMP.BRANCH.NO.HWALA)    = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.WH)       = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.LG11)     = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.LG22)     = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.TEL)      = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.TF1)      = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.TF2)      = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.TF3)      = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.BR)       = BRANCH
    R.NEW(DEPT.SAMP.BRANCH.NO.AC)       = BRANCH
    R.NEW(DEPT.SAMP.REQUEST.DATE.HWALA) = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.WH)    = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.LG11)  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.LG22)  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.TEL )  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.TF1 )  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.TF2 )  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.TF3 )  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.BR  )  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.AC  )  = TODAY
    R.NEW(DEPT.SAMP.REQUEST.DATE.FIN )  = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.HWALA)  = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.WH)     = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.LG11)   = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.LG22)   = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.TEL)    = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.TF1)    = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.TF2)    = TODAY
***    R.NEW(DEPT.SAMP.REPLAY.DATE.TF3)    = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.BR)     = TODAY
    R.NEW(DEPT.SAMP.REPLAY.DATE.AC)     = TODAY
    R.NEW(DEPT.SAMP.NAMES1.HWALA)       = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.WH)          = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.LG11)        = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.LG22)        = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.TEL)         = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.TF1)         = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.TF2)         = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.TF3)         = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.BR)          = "������ ������ ���������"
    R.NEW(DEPT.SAMP.NAMES1.AC)          = "������ ������ ���������"
    R.NEW(DEPT.SAMP.REQ.STA.HWALA)      = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.WH)         = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.LG11)       = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.LG22)       = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.TEL)        = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.TF1)        = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.TF2)        = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.TF3)        = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.BR)         = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.REQ.STA.AC)         = "WAIT OF APPROVED"
    R.NEW(DEPT.SAMP.TF2.TYPE)           = "1��� �������� �������"
    INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)
F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
FN.F.ITSS.USER.SIGN.ON.NAME = ''
CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,INPUTTER,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
INP=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>

*    R.NEW(DEPT.SAMP.INPUT.NAME.HWALA)<1,1> = INP

*Line [ 123 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT.CODE=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*    TEXT = "DEPT.CODE" : DEPT.CODE ; CALL REM

    IF DEPT.CODE NE 99 THEN
        T(DEPT.SAMP.REPLAY.DATE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.WH)<3>    = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.HWALA)<3>     = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.WH)<3>        = 'NOINPUT'
        T(DEPT.SAMP.NOTES.HWALA.MAR)<3>   = 'NOINPUT'
        T(DEPT.SAMP.NOTES.WH.MAR)<3>      = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.LG11)<3>  = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.LG11)<3>      = 'NOINPUT'
        T(DEPT.SAMP.NOTES.LG11.MAR)<3>    = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.LG22)<3>  = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.LG22)<3>      = 'NOINPUT'
        T(DEPT.SAMP.NOTES.LG22.MAR)<3>    = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.TEL)<3>   = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.TEL)<3>       = 'NOINPUT'
        T(DEPT.SAMP.NOTES.TEL.MAR)<3>     = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.BR)<3>    = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.BR)<3>        = 'NOINPUT'
        T(DEPT.SAMP.NOTES.BR.MAR)<3>      = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.AC)<3>    = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.AC)<3>        = 'NOINPUT'
        T(DEPT.SAMP.NOTES.AC.MAR)<3>      = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.TF1)<3>   = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.TF1)<3>       = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.TF2)<3>   = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.TF2)<3>       = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.TF3)<3>   = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.TF3)<3>       = 'NOINPUT'
        T(DEPT.SAMP.NOTES.TF2.MAR)<3>     = 'NOINPUT'
        T(DEPT.SAMP.NOTES.TF3.MAR)<3>     = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.HWALA)<3>  = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.WH)<3>     = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.TEL)<3>    = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.TF1)<3>    = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.TF2)<3>    = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.TF3)<3>    = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.BR)<3>     = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.AC)<3>     = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.LG11)<3>   = 'NOINPUT'
    END

    RETURN
END
