* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDcwNzM6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CUST.SRV
* Created by Noha Hamed

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.SERVICE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS


    FN.PAYMENT='F.SCB.CUST.TEL.SERVICE'
    F.PAYMENT=''
    CALL OPF(FN.PAYMENT,F.PAYMENT)
    IF V$FUNCTION = 'I'  THEN
        CUST.ID = ID.NEW[1,8]
        ZEROS=''
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR( 'CUSTOMER':@FM:EB.CUS.MNEMONIC,CUST.ID,CUS.ID)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.ID=R.ITSS.CUSTOMER<EB.CUS.MNEMONIC>
        IF ETEXT THEN
            E = '��� ���� ��� �����  ' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
        T.SEL="SELECT F.SCB.CUST.TEL.SERVICE WITH @ID LIKE ":CUST.ID:"..."
        CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)
        NO.OF.RECORDS=SELECTED+1
        LEN.NO=LEN(NO.OF.RECORDS)
        FOR I=LEN.NO TO 2
            ZEROS=ZEROS:'0'
        NEXT I
        SERIAL.NO=ZEROS:NO.OF.RECORDS
        ID.NEW=CUST.ID:'.':SERIAL.NO

*        ID.NEW = CUST.ID:'.':TODAY
        R.NEW(TEL.CUST.NO) = CUST.ID

    END
    RETURN
END
