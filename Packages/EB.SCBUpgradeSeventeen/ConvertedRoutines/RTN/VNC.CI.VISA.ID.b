* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDI3MjU6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
********NESSREEN AHMED 22/8/2007*******
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.CI.VISA.ID

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS


    IF V$FUNCTION = 'I'  THEN
        CARD.TY = ID.NEW[1,4]
        CARD.NO = ID.NEW[6,16]
        IF LEN(CARD.NO) # 16 THEN E = '��� �� ��� ��� ������' ; CALL ERR ; MESSAGE = 'REPEAT'
        IF CARD.TY = 'VICL' AND CARD.NO[1,6] # '404238' THEN
            E = '���� ��� �� ��� �� ��� ������'
            CALL ERR ;MESSAGE='REPEAT'
        END
        IF CARD.TY = 'VIGO' AND CARD.NO[1,6] # '404239' THEN
            E = '���� ��� �� ��� �� ��� ������'
            CALL ERR ;MESSAGE='REPEAT'
        END
        R.NEW(CARD.IS.LOCAL.REF)<1,LRCI.VISA.NO> = CARD.NO
    END
    RETURN
END
