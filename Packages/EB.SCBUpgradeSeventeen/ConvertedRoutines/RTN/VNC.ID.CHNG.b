* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MTQxOTQ6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.ID.CHNG
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT

*    FN.CUS='F.GROUP.CREDIT.INT'
*    F.CUS=''
*    CALL OPF(FN.CUS,F.CUS)

    IF V$FUNCTION = 'I' THEN
        ID.L = LEN(ID.NEW)
        IF ID.L EQ 12 THEN ID = ID.NEW[1,1]
        IF ID.L EQ 13 THEN ID = ID.NEW[1,2]
        ID= ID.NEW

        IF (ID[1,2] NE '54' AND ID[1,2] NE '57') THEN

            E = '��� ����� ������ ��� �����'

            CALL ERR ; MESSAGE = 'REPEAT'
        END
        RETURN
    END
*--------------------------------------------
    IF V$FUNCTION = 'A' THEN
        ID.L = LEN(ID.NEW)
        IF ID.L EQ 12 THEN ID = ID.NEW[1,1]
        IF ID.L EQ 13 THEN ID = ID.NEW[1,2]
        ID= ID.NEW
        IF (ID[1,2] NE '54' AND ID[1,2] NE '57') THEN
          E = '��� ����� ������� �� ��� ������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
        RETURN
    END
