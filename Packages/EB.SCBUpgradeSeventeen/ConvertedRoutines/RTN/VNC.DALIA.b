* @ValidationCode : MjoxNDE3NzQwNzU2OkNwMTI1MjoxNjQ1MTA2MTUyNDg3OkthcmVlbSBNb3J0YWRhOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:55:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
*-----------------------------------------------------------------------------
* <Rating>93</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.DALIA

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER

    Y=''
    BRANCH=''
    INPUTTER=''

    IF V$FUNCTION='I' THEN
*X = FIELD(R.NEW(TT.TE.INPUTTER<1, 1>),'_', 2)

*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*CALL DBR('TELLER$NAU':@FM:TT.TE.TELLER.ID.1, ID.NEW ,Y)
        F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
        FN.F.ITSS.TELLER$NAU = ''
        CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
        CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
        Y=R.ITSS.TELLER$NAU<TT.TE.TELLER.ID.1>
        IF NOT(ETEXT) THEN
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*   IF INPUTTER THEN CALL DBR( 'USER':@FM:EB.USE.DEPARTMENT.CODE, FIELD( INPUTTER< 1, 1>, '_', 2), BRANCH)
            F.ITSS.USER = 'F.USER'
            FN.F.ITSS.USER = ''
            CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
            CALL F.READ(F.ITSS.USER,FIELD( INPUTTER< 1, 1>, '_', 2),R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
            BRANCH=R.ITSS.USER<@FM:EB.USE.DEPARTMENT.CODE>
            R.NEW( TT.TE.NARRATIVE.1)= BRANCH
*  IF R.USER<EB.USE.DEPARTMENT.CODE> # BRANCH THEN
*       E = 'THIS.OPER.FROM.OTHER.BRANCH'
*        CALL ERR ; MESSAGE = 'REPEAT';
*  END ELSE
*
*         IF R.NEW(TT.TE.TRANSACTION.CODE) # 23 THEN
*           E='INVALID.TRANSACTION.CODE'
*           CALL ERR ; MESSAGE = 'REPEAT';
*         END
     
        END
    END
RETURN
END
