* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDUxMDM2MDM2MjE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 15:13:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Kareem Mortada
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeSeventeen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeSeventeen
*DONE
********MENNA  05/12/2017*******
    SUBROUTINE VNC.COL.CHEQ

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COL.CHEQ
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    CALL F.READ(FN.CU,CUST,R.CU,F.CU,E1)

    CUST  = FIELD(ID.NEW,".",1)

    R.NEW(COL.PRINT.DATE) = TODAY
    R.NEW(COL.CUST.ID)    = CUST

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = ''
    R.CUSTOMER  = ''           ; RETRY      = '' ; E1 = ''

    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E1)




    LOCAL.REF     = R.CUSTOMER<EB.CUS.LOCAL.REF>
    ARABIC.NAME   = LOCAL.REF<1,CULR.ARABIC.NAME>
    ARABIC.NAME.2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>


    BRANCH        = R.CUSTOMER<EB.CUS.COMPANY.BOOK>[2]

    R.NEW(COL.BRANCH.ID)     = BRANCH
    R.NEW(COL.CUST.NAME.1)   = ARABIC.NAME
    R.NEW(COL.CUST.NAME.2) = ARABIC.NAME.2


    RETURN




END
