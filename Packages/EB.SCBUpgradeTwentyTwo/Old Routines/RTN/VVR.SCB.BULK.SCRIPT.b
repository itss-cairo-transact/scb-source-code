* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.BULK.SCRIPT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC

    IF MESSAGE EQ "" THEN

        IF COMI THEN
            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
            T.SEL  = "SELECT F.SCB.SR.SCRIPT WITH @ID EQ ":COMI
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

            FN.SR ='F.SCB.SR.SCRIPT' ;R.SR ='';F.SR =''
            CALL OPF(FN.SR,F.SR)
            CALL F.READ(FN.SR,KEY.LIST, R.SR, F.SR,E)
            IF NOT(KEY.LIST) THEN
                ETEXT =  "������ ��� �����"
            END ELSE
                R.NEW(FT.BKCRAC.DR.ACCOUNT)        = R.SR<SR.SCRIPT.ACCOUNT.NO>
                R.NEW(FT.BKCRAC.CR.ACCOUNT)<1,1>   = "0130046610101001"
                R.NEW(FT.BKCRAC.CR.AMOUNT)<1,1>    = R.SR<SR.SCRIPT.TOTAL.SHARES>
                R.NEW(FT.BKCRAC.CR.ACCOUNT)<1,2>   = "0130046610101002"
                R.NEW(FT.BKCRAC.CR.AMOUNT)<1,2>    = R.SR<SR.SCRIPT.TOTAL.CHARGE>
                R.NEW(FT.BKCRAC.PROFIT.CENTRE.DEPT)= R.USER<EB.USE.DEPARTMENT.CODE>
                R.NEW(FT.BKCRAC.ORDERING.BK)<1,1>  = COMI
                R.NEW(FT.BKCRAC.ORDERING.BK)<1,2>  = R.SR<SR.SCRIPT.BUY.SHARES>

                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
