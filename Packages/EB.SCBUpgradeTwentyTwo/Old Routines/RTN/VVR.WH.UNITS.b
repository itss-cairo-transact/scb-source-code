* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.WH.UNITS
*----------------------------
*1- CALC VALUE.BALANCE = NO.OF.UNITS * UNIT.PRICE
*2- CALC TOTAL VALUE BALANCE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*---------------------------------------------------
    IF MESSAGE NE 'VAL' THEN
        R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,AV> = COMI
        IF COMI THEN

*-- EDIT BY NESSMA ON 06/07/2015
            VAL.BAL = R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,AV> * R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,AV>
            R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,AV> = VAL.BAL
            CALL REBUILD.SCREEN
*-------------------------------

            NOOO = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,AV>
            CALL DBR('SCB.WH.ITEMS':@FM:SCB.WH.IT.UNITS.BALANCE,NOOO,UNITS)

            IF R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,AV> GT UNITS THEN
                ETEXT = '������ �� ����'
                CALL STORE.END.ERROR
            END
        END
        IF NOT(R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,AV>) THEN
            COMI = 1
            R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,AV> = COMI
        END
    END

    IF MESSAGE EQ 'VAL' THEN
*-- EDIT BY NESSMA 21/2/2013
*Line [ 58 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NN      = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)
        SAM     = 0
        SAM.TOT = 0

        FOR INDX = 1 TO NN
            SAM      = R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,INDX> * R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,INDX>
            SAM.TOT += SAM
            SAM      = 0
        NEXT INDX

        R.NEW(SCB.WH.TRANS.TOTAL.BALANCE)  = SAM.TOT
    END
    CALL REBUILD.SCREEN
*-------------------------------------------------------
    RETURN
END
