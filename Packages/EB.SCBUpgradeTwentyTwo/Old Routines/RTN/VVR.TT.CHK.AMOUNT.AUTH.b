* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN  -----

SUBROUTINE VVR.TT.CHK.AMOUNT.AUTH

*TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER


*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER


IF MESSAGE # 'VAL' THEN 
   IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN ETEXT = '��� ����� ������'
   R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
   CALL REBUILD.SCREEN 
********UDATED IN 4/1/2007 BY NESSREEN AHMED*********
*   IF COMI GT 20000 THEN ETEXT = '��� �� �� ���� ������ ���� �� 20000' ; CALL STORE.END.ERROR
*****************************************************
END

RETURN
END
