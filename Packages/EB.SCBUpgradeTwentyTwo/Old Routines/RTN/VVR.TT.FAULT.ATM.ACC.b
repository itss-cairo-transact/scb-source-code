* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
******NESSREEN-SCB*21/7/2016***********
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.FAULT.ATM.ACC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS


    COMP = C$ID.COMPANY

    ETEXT = '' ; ACC.NO = ''  ; CURR = '' ; FX.ACC = ''

    IF COMI THEN
        F.ATM.AC = '' ; FN.ATM.AC = 'F.SCB.ATM.TERMINAL.ID' ; R.ATM.AC = '' ; E1 = '' ; RETRY1 = ''
        CALL OPF(FN.ATM.AC,F.ATM.AC)
        ATM.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ATM.NUMBER>
        TEXT = 'ATM.NO=':ATM.NO ; CALL REM
        CALL DBR( 'CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE, COMI,CURR)
        CALL F.READ(FN.ATM.AC, ATM.NO, R.ATM.AC, F.ATM.AC, E1)
        IF COMI EQ 'EGP' THEN
            ACC.NO = R.ATM.AC<SCB.ATM.ACCOUNT.NUMBER.CRV>
            R.NEW(TT.TE.ACCOUNT.1) = ACC.NO
            TEXT = 'ACC.NO=':ACC.NO ; CALL REM
        END ELSE
            CURR = COMI
            TEXT = 'CURR=' :CURR ; CALL REM
            LOCATE CURR IN R.ATM.AC<SCB.ATM.CURRENCY,1> SETTING NN THEN
                FX.ACC = R.ATM.AC<SCB.ATM.ACCOUNT.NUMBER.FOREX, NN>
                TEXT = 'FX.ACC=':FX.ACC ; CALL REM
            END
            R.NEW(TT.TE.ACCOUNT.1) = FX.ACC
        END
        USR.BR = ''
        USR = OPERATOR
        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,USR,USR.BR)
        BR = FMT(USR.BR, "R%2")
        ACCT.2 = COMI:"10000":BR:"9900":BR
        R.NEW(TT.TE.ACCOUNT.2)= ACCT.2

        IF COMI = LCCY THEN
            T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
            R.NEW(TT.TE.AMOUNT.FCY.1) = ''
            T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'
        END  ELSE
            T(TT.TE.AMOUNT.FCY.1)<3> = ''
            R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
            T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'
        END
        CALL REBUILD.SCREEN
    END

    RETURN
END
