* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.TT.DRA.CHQ

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************

    F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
    ID= COMI:'.':R.NEW(TT.TE.CHEQUE.NUMBER)
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
        CURR = COMI[9,2]
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****ID1 = 'DRFT.':COMI:"-":R.NEW(TT.TE.CHEQUE.NUMBER)
    ID1 = 'DRFT.':COMI:".":R.NEW(TT.TE.CHEQUE.NUMBER)
****CALL DBR("CHEQUES.PRESENTED":@FM:CHQ.PRE.REPRESENTED.COUNT,ID1,COUN)
    CALL DBR("CHEQUE.REGISTER.SUPPLEMENT":@FM:CC.CRS.DATE.PRESENTED,ID1,COUN)

        IF NOT(ETEXT) AND COUN = ""  AND R.COUNT<DR.CHQ.CHEQ.TYPE> = 1 THEN
            IF CURR =  '10' THEN
                R.NEW(TT.TE.AMOUNT.LOCAL.1) = R.COUNT<DR.CHQ.AMOUNT>
            END ELSE
                R.NEW(TT.TE.AMOUNT.FCY.1) = R.COUNT<DR.CHQ.AMOUNT>
            END
            CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CURR,CODE)
            R.NEW(TT.TE.CURRENCY.1) = CODE
* R.NEW(TT.TE.ACCOUNT.1) =  R.COUNT<DR.CHQ.NOS.ACCT>

* R.NEW(TT.TE.CHEQUE.NUMBER) =  R.COUNT<DR.CHQ.CHEQ.NO>
            CALL REBUILD.SCREEN
* IF V$FUNCTION = 'A' THEN
*  R.COUNT<DR.CHQ.CHEQ.STATUS> = 2
*  CALL F.WRITE(FN.COUNT,ID,R.COUNT)
*  CALL JOURNAL.UPDATE(ID)
* END
        END ELSE
            ETEXT = 'CHQ.STATUS.NOT.ALLOWED'
        END
    END ELSE
        ETEXT = 'RECORD.NOT.FOUND'
    END

    CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
    CLOSE FN.COUNT


    RETURN
END
