* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*------------------------------------------------------------------------
* <Rating>-8</Rating>
*-------------------------------------------------------------------------
    SUBROUTINE VVR.PL.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    PL.C = '0'
    IF R.NEW(INF.MLT.PL.CATEGORY)<1,AV> THEN
        PL.CAT  = R.NEW(INF.MLT.PL.CATEGORY)<1,AV>
        ***TEXT    = PL.CAT ; CALL REM
        IF (PL.CAT GE 50000 AND PL.CAT LE 70000) THEN
            PL.C = '1'
            ***TEXT = '1'  ; CALL REM
        END ELSE
            PL.C = '0'
            ***TEXT = '0' ; CALL REM
        END
        IF PL.C = '0' THEN
            ***TEXT = "YES " : <1,AV> ; CALL REM
            ETEXT = 'INVALID PL CATEGORY' ; CALL STORE.END.ERROR
            R.NEW(INF.MLT.PL.CATEGORY)<1,AV> = ''
        END

    END
    CALL REBUILD.SCREEN
    RETURN
END
*-------------------------------- End Of Record --------------------------------
