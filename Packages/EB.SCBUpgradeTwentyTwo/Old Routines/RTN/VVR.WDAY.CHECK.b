* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.WDAY.CHECK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
    XX = ''
    AA = ''
    IF COMI THEN
        FLG = 0
        CUR = R.NEW(LD.BUS.DAY.DEFN):"00"
        XX = COMI
        IF CUR EQ 'EG00' THEN
            CALL AWD(CUR,XX,AA)
            IF AA EQ 'H' THEN
                CALL CDT("",XX,'+1W')
                COMI = XX
            END

        END

        IF CUR NE 'EG00' THEN
            LOOP WHILE FLG = 0
                CALL AWD(CUR,XX,AA)
                CALL AWD("EG00",XX,BB)
                IF AA EQ 'W' AND BB EQ 'W' THEN
                    FLG = 1
                END ELSE
                    CALL CDT("EG00",XX,'+1W')
                END
            REPEAT
            COMI = XX
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
