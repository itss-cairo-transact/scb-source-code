* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.NEG.LIST.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CBE


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    WS.AC.AMT  = 0
    WS.LIQ.AMT = 0

    CALL F.READ(FN.AC,COMI,R.AC,F.AC,E1)
    WS.LIQ.ACCT = R.AC<AC.INTEREST.COMP.ACCT>
    WS.AC.AMT   = R.AC<AC.OPEN.ACTUAL.BAL>
    IF WS.LIQ.ACCT NE '' THEN
        CALL F.READ(FN.AC,WS.LIQ.ACCT,R.AC1,F.AC,E2)
        WS.LIQ.CATEG = R.AC1<AC.CATEGORY>
        IF WS.LIQ.CATEG EQ '9090' THEN
            WS.LIQ.AMT = R.AC1<AC.OPEN.ACTUAL.BAL>
        END
    END

    TOT.AMT = WS.AC.AMT + WS.LIQ.AMT

    R.NEW(CBE.USED.LIMIT) = TOT.AMT

    CALL REBUILD.SCREEN

    RETURN
END
