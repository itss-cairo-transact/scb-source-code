* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>698</Rating>
*-----------------------------------------------------------------------------
*--- 08/03/2009 NESSREEEN AHMED -------


    SUBROUTINE VVR.TT.CHK.NOS.CAT

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS



    ETEXT = ''
    E = ''

    CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
    IF V$FUNCTION = 'I' THEN

        IF INDEX(TT.NO,'98',1) > 1 THEN E = '��� ����� �������' ; CALL ERR ;MESSAGE = 'REPEAT'
        IF INDEX(TT.NO,'99',1) > 1 THEN E = '��� ����� �������' ; CALL ERR ;MESSAGE = 'REPEAT'

    END

    IF V$FUNCTION = 'R' THEN

        IF INDEX(TT.NO,'98',1) < 1 THEN E = '��� ����� �������'

    END
    IF E THEN CALL ERR ;MESSAGE = 'REPEAT'

    GOSUB INITIAL
    GOSUB ACCOUNT.CHECK       ;* IF ERROR = 'Y' THEN RETURN
    GOSUB CURRENCY.CHECK
    GOSUB DATE.CHECK          ;* IF ERROR = 'Y' THEN RETURN

    GOTO PROGRAM.END
***************************************************************************************************
INITIAL:

    CATEG = ''
    CUS.NAME = ''
    CURR.NAME = ''
    BALANCE = ''
    MYDATE = TODAY

    RETURN
***************************************************************************************************
ACCOUNT.CHECK:
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
    IF CATEG NE '5010' THEN
        ETEXT = '��� ������ ������� ������ ���' ; CALL STORE.END.ERROR ; R.NEW(TT.TE.ACCOUNT.2) = ''
    END ELSE
        CALL DBR('ACCOUNT':@FM:AC.SHORT.TITLE, COMI, CUS.NAME)
        R.NEW (TT.TE.NARRATIVE.1) = CUS.NAME
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY, COMI,CURR.NAME)
        R.NEW (TT.TE.CURRENCY.1) = CURR.NAME
    END
    RETURN
**************************************************************************************************
CURRENCY.CHECK:

    IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT' ;T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
    ELSE T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT' ;T(TT.TE.AMOUNT.FCY.1)<3>=''

    RETURN
* *************************************************************************************************
DATE.CHECK:
    CALL DBR( 'CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.NEW(TT.TE.CUSTOMER.1),LOCAL.REF)
    CUS.VAL = LOCAL.REF<1,CULR.DEPOSIT.VAL.TDY>
    CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR,R.NEW(TT.TE.CUSTOMER.1),SEC)
    IF SEC EQ 1100 OR SEC EQ 1200 OR SEC EQ 1300 OR SEC EQ 1400 THEN
        R.NEW (TT.TE.VALUE.DATE.1) = TODAY
    END ELSE
        IF (CUS.VAL EQ 'YES') AND (R.NEW (TT.TE.CURRENCY.1) = LCCY) THEN
            R.NEW (TT.TE.VALUE.DATE.1) = TODAY
        END ELSE
            IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN
                CALL CDT('', MYDATE, '1W')
                R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
            END ELSE
                IF MESSAGE = '' THEN
                    CALL CDT('', MYDATE, '2W')
                    R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
                END
            END

        END
    END
    RETURN
**************************************************************************************************
PROGRAM.END:
    RETURN
END
