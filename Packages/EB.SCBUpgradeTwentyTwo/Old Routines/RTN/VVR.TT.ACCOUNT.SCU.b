* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>339</Rating>
*-----------------------------------------------------------------------------
**---------INGY 17/07/2002---------**



    SUBROUTINE VVR.TT.ACCOUNT.SCU

*A ROUTINE TO :
* MAKE SURE IT IS NOT NOSTRO ACCOUNT
* MAKE SURE IT IS NOT INTERNAL ACCOUNT
*CHECK IF ACCOUNT.1 EQ ACCOUNT.2 THEN DISPLAY ERROR
*CHECK IF ACCOUNT.1 NE ACCOUNT.2 THEN
*CHECK IF CURRENCY OF ACCOUNT.2 NE THE CURRENCY OF ACCOUNT.1 THEN DISPLAY ERROR

*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT




* CATEGORY = ''
* CURR.ACC.2 = ''
*
* CUST.ID.2 = TRIM(COMI[5,7],"0","L")
* CUST.ID.1 = TRIM(R.NEW (TT.TE.ACCOUNT.1)[5,7],"0","L")
* CURR.ACC.2 = COMI [3,2]
* CATEGORY = COMI [12,4]

    CUST.ID.1= R.NEW (TT.TE.ACCOUNT.1)

    CALL DBR ('ACCOUNT':@FM:AC.SHORT.TITLE,CUST.ID.1,NAME1)
    CALL DBR ('ACCOUNT':@FM:AC.SHORT.TITLE,COMI,NAME2)

    IF COMI # R.NEW(TT.TE.ACCOUNT.2) THEN

****  R.NEW(TT.TE.CURRENCY.1) = ''
*   R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
*  R.NEW(TT.TE.AMOUNT.FCY.1) = ''
        R.NEW(TT.TE.ACCOUNT.2) = ''
        R.NEW(TT.TE.NARRATIVE.2) = ''
    END

    IF NOT(NUM(COMI)) THEN ETEXT='��� ����� ��������� �������� �����'
* ETEXT = 'Not.Allowed.For.Internal.Account'
    ELSE
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
        IF 5000 LE CATEG AND CATEG LE 5999 THEN
            ETEXT = 'THIS.IS.NOSTRO.ACCOUNT'
            ETEXT='��� ����� ������� ��������'
        END ELSE
            IF R.NEW (TT.TE.ACCOUNT.1) = COMI THEN ETEXT='������ ��� �� ���� �����'
*ETEXT = 'THE.ACCOUNTS.MUST.BE.DIFFERENT'
            ELSE
                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR.ACC.2)
                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,CUST.ID.1,CURR.ACC.1)

                IF  CURR.ACC.2 # CURR.ACC.1 THEN ETEXT='���� �������� ��� �� ���� �����'
* ETEXT = 'THE.CURRENCY.OF.THE.ACCOUNTS.MUST.BE.EQUAL'

            END



            IF NAME1 AND NAME2 THEN

                R.NEW(TT.TE.NARRATIVE.2)<1,1> = NAME1
                R.NEW(TT.TE.NARRATIVE.2)<1,2> = NAME2
                CALL REBUILD.SCREEN
            END
        END
    END


    RETURN
END
