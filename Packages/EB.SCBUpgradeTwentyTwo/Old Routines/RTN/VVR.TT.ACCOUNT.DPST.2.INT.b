* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
**---------INGY 22/07/2002---------**

*A ROUTINE TO CHECK IF ACCOUNT.1 NE A VALID INTRNAL ACCOUNT THEN DISPLAY ERROR MESSAGE
*AND TO CHECK IN CASE THAT ACCOUNT.1 EQ A VALID INTERNAL ACCOUNT IF CATEGORY ="10000"
*THEN DISPLAY ERROR MESSAGE
*A ROUTINE TO CHECK IF COMI(ACCOUNT.1) NE (ACCOUNT.1) THEN EMPTY FIELDS CURRENCY.1 &
*AMOUNT.LOCAL.1 & AMOUNT.FCY.1 & NARRATIVE


SUBROUTINE VVR.TT.ACCOUNT.DPST.2.INT


*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


  IF COMI # R.NEW(TT.TE.ACCOUNT.1) THEN

    R.NEW (TT.TE.ACCOUNT.1) = ''
    R.NEW (TT.TE.CURRENCY.1) = ''
    R.NEW (TT.TE.AMOUNT.LOCAL.1) = ''
    R.NEW (TT.TE.AMOUNT.FCY.1) = ''
    R.NEW (TT.TE.NARRATIVE.2)<1,1> = ''

  END

 CATEGORY = ''

 CATEGORY = COMI[4,5]
 AC.BRANCH= COMI[9,2]
 AC.BRANCH = TRIM( AC.BRANCH, '0', 'L') ;* remove leading zeros

 CALL DBR ('CATEGORY':@FM:1,CATEGORY,CATE)
 CALL DBR('ACCOUNT':@FM:3,COMI,TITLE)

 IF NUM(COMI[1,3]) THEN     ETEXT = 'MUST.BE.INERNAL.ACCOUNT'
 ELSE
  IF AC.BRANCH # R.USER<EB.USE.DEPARTMENT.CODE> THEN ETEXT = 'MUST.BE.SAME.DEPARTMENT'
  ELSE
    IF COMI[4,5] = '10000' THEN

      R.NEW (TT.TE.ACCOUNT.1) = ''
      R.NEW (TT.TE.CURRENCY.1) = ''
      R.NEW (TT.TE.AMOUNT.LOCAL.1) = ''
      R.NEW (TT.TE.AMOUNT.FCY.1) = ''
      R.NEW (TT.TE.NARRATIVE.2)<1,1> = ''
      CALL REBUILD.SCREEN
      ETEXT = 'THIS.CATEGORY.IS.NOT.ALLOWED'
   END ELSE
       R.NEW(TT.TE.CURRENCY.1) = COMI[1,3]
       R.NEW(TT.TE.NARRATIVE.2)<1,1> = COMI[1,3]:'-':CATE:'-':TITLE
      
   END

 IF R.NEW(TT.TE.CURRENCY.1) = LCCY THEN
    T(TT.TE.AMOUNT.LOCAL.1)<3> =''
    T(TT.TE.AMOUNT.FCY.1)<3> ='NOINPUT'
 END ELSE
   T(TT.TE.AMOUNT.FCY.1)<3> =''
   T(TT.TE.AMOUNT.LOCAL.1)<3> ='NOINPUT'

 END
END
END
CALL REBUILD.SCREEN

 RETURN
 END
