* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>379</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/12 ***
*******************************************

    SUBROUTINE VVR.MUA.CHK.TLR
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.OVERRIDE.INDEX
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

*IF MESSAGE # 'VAL' THEN
    GOSUB INITIATE
    GOSUB GET.CHK.DATA
* END
    IF MESSAGE EQ 'VAL' THEN
        IF R.NEW(MUA.DEP.ACCT.CODE) NE '5100' THEN
            WS.TLR.ID.NEW = R.NEW(MUA.TELLER.ID)
            IF WS.TLR.ID.NEW[2] EQ '99' THEN
*Line [ 48 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR '441' IN R.NEW(MUA.HMM.ID) SETTING FPOS, VPOS THEN '' ELSE
                    ETEXT = "�� ���� ������� ��������� �� ��� ������"
                    CALL STORE.END.ERROR
                END
            END
            IF WS.TLR.ID.NEW[2] EQ '98' THEN
*Line [ 55 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR '442' IN R.NEW(MUA.HMM.ID) SETTING FPOS, VPOS THEN '' ELSE
                    ETEXT = "�� ���� ������� ��������� �� ��� ������"
                    CALL STORE.END.ERROR
                END
            END
        END
    END
    CALL REBUILD.SCREEN
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.USR  = "F.USER"               ; F.USR   = ""
    CALL OPF (FN.USR,F.USR)


    WS.USR.CO = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.MOD.USR = WS.COMP[2]

    WS.ER.FLG = 0
    WS.RNK.DONE = 0
    RETURN
*-----------------------------------------------------
GET.CHK.DATA:
    WS.TLR.NEW          = R.NEW(MUA.TELLER.ID)
    WS.TLR.OLD          = R.OLD(MUA.TELLER.ID)

    IF WS.TLR.OLD[2] EQ '99' AND COMI[2] NE '99' THEN
        IF PGM.VERSION NE ',FWD' THEN
            ETEXT = "��� ����� �������� ���� ������� ����"
            CALL STORE.END.ERROR
        END
    END
    IF WS.TLR.OLD NE '' AND COMI NE '' THEN
        IF WS.TLR.OLD[1,2] NE COMI[1,2] THEN
            ETEXT = "��� ����� �������� ��� ���� ���"
            CALL STORE.END.ERROR
        END
    END
    IF COMI NE '' THEN
        IF ID.COMPANY[2] NE COMI[1,2] THEN
            ETEXT = "��� ����� �������� ��� ���� ���"
            CALL STORE.END.ERROR
        END
    END
    RETURN
*-----------------------------------------------------
