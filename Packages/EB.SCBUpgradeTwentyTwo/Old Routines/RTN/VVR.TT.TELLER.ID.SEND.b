* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>190</Rating>
*-----------------------------------------------------------------------------
*----- DINA_SCB ---- 14-7-2002

    SUBROUTINE VVR.TT.TELLER.ID.SEND

* a routine to make sure that teller.id.1 is head chashier of another branch
* to empty the field narrative.2 if the user changes the teller.id.1
* to default the field narrative.2 with the name of the branch we send to

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID


    IF  COMI # R.NEW(TT.TE.TELLER.ID.1) THEN R.NEW(TT.TE.NARRATIVE.2) = ''
    IF LEN(COMI) EQ 2 THEN COMI=COMI:'99'
    TELLER.COD = COMI
    CALL DBR ('TELLER.ID':@FM:TT.TID.USER,TELLER.COD,TT.USE)
    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,TT.USE,DEPT)
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
    DEPT.NAME=FIELD(DEPT.NAME,'.',2)
    R.NEW(TT.TE.NARRATIVE.2)<1,1> = DEPT.NAME

    IF COMI[3,2] # 99 THEN ETEXT = '����� ����� ������� ���'
    ELSE
        IF R.USER<EB.USE.DEPARTMENT.CODE> EQ TRIM(COMI[1,2] , '0', 'L') THEN
            ETEXT = '��� �� ���� ����� ���� ���'
        END
    END
    CALL REBUILD.SCREEN
   RETURN
END
