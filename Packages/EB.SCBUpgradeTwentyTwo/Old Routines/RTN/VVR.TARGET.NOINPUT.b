* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
*CREATED BY RIHAM YOUSSIF
*20200715
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TARGET.NOINPUT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.ACC  = 'F.ACCOUNT'; F.ACC = ''
    R.ACC    = ''
    CALL OPF(FN.ACC,F.ACC)



    IF MESSAGE NE 'VAL' THEN
        IF V$FUNCTION = 'I' THEN
            TAR = COMI
            IF TAR NE '1000' AND TAR NE '2000' AND TAR NE '6000' AND TAR NE '3000' AND TAR NE '7000' AND TAR NE '4000' AND TAR NE '8000' AND TAR NE '9000' THEN
                ETEXT = 'This Target Not Allowed'
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
