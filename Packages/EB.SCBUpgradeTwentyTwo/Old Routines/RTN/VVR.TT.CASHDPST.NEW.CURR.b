* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>2021</Rating>
*-----------------------------------------------------------------------------
*--- 14/07/2002 RANIA KAMAL -------

*A ROUTINE TO CHECK THAT THE LENGHT OF ACCOUNT EQ 17 & MAKE SURE THAT IT IS NOT NOSTRO/INTERNAL A/C
*ALSO TO DEFAULT NARRATIVE.2 & CURRENCY  ACCORDING TO ACCOUNT AND MAKE CURRENCY NOINPUT FIELD
*ALSO IF THE CURRENCY IS LOCAL AND ONLINE.ACTUAL.BALANCE <=0 THEN DEFAULT VALUE.DATE.1,VALUE.DATE.2 & EXPOSURE.DATE BY
*TODAY'S DATE PLUS 1W ELSE TODAY'S DATE.
*IF THE CURRENCY IS FOREIGN THEN DEFAULT VALUE.DATE.1,VALUE.DATE.2 & EXPOSURE.DATE BY
*TODAY'S DATE PLUS 2W ELSE TODAY'S DATE.


    SUBROUTINE VVR.TT.CASHDPST.NEW.CURR

*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS



    ETEXT = ''
    E = ''

    CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
**    TEXT = 'TT.NO=':TT.NO<1,1> ; CALL REM
**************************************************
**    AA = DCOUNT(TT.NO,SM)
**    TEXT = 'AA=':AA ; CALL REM
**************************************************
    IF V$FUNCTION = 'I' THEN

        IF INDEX(TT.NO,'98',1) > 1 THEN E = '��� ����� �������' ; CALL ERR ;MESSAGE = 'REPEAT'
        IF INDEX(TT.NO,'99',1) > 1 THEN E = '��� ����� �������' ; CALL ERR ;MESSAGE = 'REPEAT'

    END

    IF V$FUNCTION = 'R' THEN

        IF INDEX(TT.NO,'98',1) < 1 THEN E = '��� ����� �������'

    END
    IF E THEN CALL ERR ;MESSAGE = 'REPEAT'

***************************************************************************************************
*IF V$FUNCTION = 'I' THEN
*  IF MESSAGE # 'VAL' THEN
    GOSUB INITIAL
    GOSUB ACCOUNT.CHECK       ;* IF ERROR = 'Y' THEN RETURN
    GOSUB CURRENCY.CHECK
    GOSUB DATE.CHECK          ;* IF ERROR = 'Y' THEN RETURN

*END
    GOTO PROGRAM.END
**  END
***************************************************************************************************
INITIAL:

*ERROR = ''
    CATEG = ''
    CUS.NAME = ''
    CURR.NAME = ''
    BALANCE = ''
    MYDATE = TODAY

    RETURN
***************************************************************************************************
ACCOUNT.CHECK:
    IF PGM.VERSION EQ ",SCB.CASHDPST7.CLEAR" OR PGM.VERSION EQ ",SCB.CASHDPST7.BILLS" THEN RETURN
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
    IF 5000 LE CATEG AND CATEG LE 5999 THEN ETEXT = '��� ���� ������';R.NEW (TT.TE.ACCOUNT.2) = ''
    IF LEN(COMI) # 16                THEN ETEXT = '��� ���� ��� ����';R.NEW (TT.TE.ACCOUNT.2) = ''
    IF NOT(NUM(COMI))                THEN ETEXT = '��� ���� ��� ����';R.NEW (TT.TE.ACCOUNT.2) = ''
****************************************************************
*****NESSREEN AHMED*********************************************
*TO CHECK OVERDRAFT ACCOUNT
****************************************************************
    IF CATEG NE 1201 AND CATEG NE 1202 THEN
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""

        CURR = R.NEW(TT.TE.CURRENCY.1)
        CUST.ID = R.NEW(TT.TE.CUSTOMER.1)
        T.SEL = 'SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ': CUST.ID : ' AND CURRENCY EQ ': CURR : ' AND(CATEGORY EQ 1201 OR CATEGORY EQ 1202)'
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                WB = ''
                ETEXT = ''
                CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,KEY.LIST<I>,WB)
                IF WB # '' THEN
                    IF WB < 0 THEN
**  ETEXT = '��� �� ���� ������ ������� ����'
**  CALL STORE.END.ERROR
                    END
                END
            NEXT I
        END
        GOSUB ACCOUNT.DEFAULT
        CALL REBUILD.SCREEN
        RETURN
**************************************************************************************************
ACCOUNT.DEFAULT:
*****UPDATED BY NESSREEN AHMED 31/10/2011**************************
        IF COMI # R.NEW(TT.TE.ACCOUNT.1)<1,AV> THEN GOSUB CLEAN.FIELDS
*****END OF UPDATE 31/10/2011**************************************
        IF PGM.VERSION EQ ",SCB.CASHDPST7.CLEAR" OR PGM.VERSION EQ ",SCB.CASHDPST7.BILLS" THEN RETURN
        CALL DBR('ACCOUNT':@FM:AC.SHORT.TITLE, COMI, CUS.NAME)
        R.NEW (TT.TE.NARRATIVE.1) = CUS.NAME
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY, COMI,CURR.NAME)
        R.NEW (TT.TE.CURRENCY.1) = CURR.NAME

        RETURN
**************************************************************************************************
CURRENCY.CHECK:

        IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT' ;T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
        ELSE T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT' ;T(TT.TE.AMOUNT.FCY.1)<3>=''

        RETURN
* *************************************************************************************************
DATE.CHECK:
        IF PGM.VERSION EQ ",SCB.CASHDPST7.CLEAR" OR PGM.VERSION EQ ",SCB.CASHDPST7.BILLS" THEN RETURN
*    CALL DBR( 'ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL, COMI ,BALANCE)
*    R.NEW (TT.TE.VALUE.DATE.2) = TODAY
*    R.NEW (TT.TE.EXPOSURE.DATE.2) = TODAY
******************Nessreen Ahmed******************************************
********UPDATED IN 26/5/2008**********************************************
        CALL DBR( 'CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.NEW(TT.TE.CUSTOMER.1),LOCAL.REF)
        CUS.VAL = LOCAL.REF<1,CULR.DEPOSIT.VAL.TDY>
        CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR,R.NEW(TT.TE.CUSTOMER.1),SEC)
        IF SEC EQ 1100 OR SEC EQ 1200 OR SEC EQ 1300 OR SEC EQ 1400 THEN
            R.NEW (TT.TE.VALUE.DATE.1) = TODAY
*****UPDATED BY NESSREEN AHMED 5/3/2012*******************************
            R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY
*****END OF UPDATE 5/3/2012*******************************************
        END ELSE
****UPDATED BY NESSREEN AHMED 18/6/2013 *****************************
            ACCT = COMI
            CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACCT ,CATEG)
****UPDATED BY NESSREEN AHMED 2/7/2019************************************
****       IF (CATEG NE '6511' AND CATEG NE '6512') THEN
           IF (CATEG NE '6511' AND CATEG NE '6512' AND CATEG NE '1003') THEN
                IF (CUS.VAL EQ 'YES') AND (R.NEW (TT.TE.CURRENCY.1) = LCCY) THEN
                    R.NEW (TT.TE.VALUE.DATE.1) = TODAY
*****UPDATED BY NESSREEN AHMED 5/3/2012*******************************
                    R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY
*****END OF UPDATE 5/3/2012*******************************************
                END ELSE
                    IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN
                        CALL CDT('', MYDATE, '1W')
                        R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
                        R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
                    END ELSE
                        IF MESSAGE = '' THEN
                            CALL CDT('', MYDATE, '2W')
                            R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
                            R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
                        END
                    END
                END
            END ELSE          ;* EndElse of IF (CATEG NE 6511 OR CATEG NE 6512)
                R.NEW (TT.TE.VALUE.DATE.1) = TODAY
****UPDATED BY NESSREEN AHMED 26/8/2013*************
                R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY
****END OF UPDATE 26/8/2013*************************
            END     ;* End of IF (CATEG NE 6511 OR CATEG NE 6512)
        END
        RETURN
**************************************************************************************************
**************************************************************************************************
CLEAN.FIELDS:
******UPDATED BY NESSREEN AHMED 31/10/2011***********************
        R.NEW(TT.TE.ACCOUNT.1)<1,AV> = ''
        R.NEW(TT.TE.ACCOUNT.2) = ''
        R.NEW(TT.TE.CURRENCY.1)<1,AV>= ''
        R.NEW(TT.TE.CURRENCY.2)= ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
        R.NEW(TT.TE.NARRATIVE.2)<1,AV> = ''
*****UPDATED BY NESSREEN AHMED 26/8/2013**********
        R.NEW (TT.TE.EXPOSURE.DATE.1) = ''
*****END OF UPDATE 26/8/2013**********************

        RETURN
*****END OF UPDATE 31/10/2011*************************************
PROGRAM.END:
        RETURN
    END
