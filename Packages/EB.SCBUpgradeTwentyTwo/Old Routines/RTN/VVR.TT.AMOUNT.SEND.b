* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
*----- DINA_SCB ---- 14-7-2002

SUBROUTINE VVR.TT.AMOUNT.SEND

*a routine to default the field account.2 with currency.1 and
*the category of the transaction.code and teller.id.2

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

DEPT = R.USER<EB.USE.DEPARTMENT.CODE>
DEPT = FMT(DEPT , 'R%2')
T.CODE = R.NEW(TT.TE.TRANSACTION.CODE)
CALL DBR('TELLER.TRANSACTION':@FM:4,T.CODE,CATEG)
*CATEG = CATEG<1,2>
***********************************************************************************************
 
 IF R.NEW(TT.TE.CURRENCY.1) AND  COMI THEN

   R.NEW(TT.TE.ACCOUNT.2) = R.NEW(TT.TE.CURRENCY.1):CATEG:DEPT:99
   CALL REBUILD.SCREEN
 END

RETURN
END
