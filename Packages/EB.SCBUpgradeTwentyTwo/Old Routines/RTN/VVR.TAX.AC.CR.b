* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.TAX.AC.CR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_INF.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    IF COMI THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,AC.CATT)
        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,COMI,AC.COMP)
        IF AC.CATT EQ '16151' AND AC.COMP NE COMP THEN
            ETEXT = '��� ������ �� ��� �����'
        END
        IF AC.CATT EQ '16151' OR AC.CATT EQ '16522' OR AC.CATT EQ '16523' OR AC.CATT EQ '16528' OR AC.CATT EQ '16510' THEN
            R.NEW(INF.MLT.SIGN)<1,AV> = 'CREDIT'
            IF AC.CATT EQ '16151' THEN
                R.NEW(INF.MLT.TXN.CODE)<1,AV> = 135
            END ELSE
                R.NEW(INF.MLT.TXN.CODE)<1,AV> = 79
            END
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,AV> = 1
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
