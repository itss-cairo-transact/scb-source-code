* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN  -----
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.TT.CHEQUE.CUS.PRESENT

* TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* FOR THAT CUSTOMER
* TO CHECK THAT THE CHEQUE IS NOT STOPPPED

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************

*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

*IF V$FUNCTION = 'I' THEN
    IF COMI  THEN
**************
*        FT.DR=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.FT.DR.CHEQ>
*        FT.DR=FIELD(FT.DR,'.',2)

*      IF COMI NE  FT.DR  THEN
*          ETEXT='�� ����� ��� ��� �������';CALL STORE.END.ERROR
*      END

************
        CH.ACCT= R.NEW(TT.TE.LOCAL.REF)<1,TTLR.FT.DR.CHEQ>
        CH.ACCT=FIELD(CH.ACCT,'.',1)
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID = 'SCB.':CH.ACCT:'-':COMI
        CHQ.ID = 'SCB.':CH.ACCT:'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED, CHQ.ID, REPRESENT)
        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED, CHQ.ID, REPRESENT)
        TEXT = CHQ.ID ; CALL REM
        TEXT = REPRESENT ; CALL REM
*ETEXT = ''
****END OF UPDATE 8/3/2016*****************************
        IF REPRESENT THEN ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
        CHQ='SCB':'.':CH.ACCT:'...'
        CHQ.CUS = 'SCB':'.':CH.ACCT
        T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED EQ '0' THEN
*  TEXT = 'NOT ISSUED' ; CALL REM
            ETEXT='�� ���� ����� ����� ���� ������ &': CHQ
            CALL STORE.END.ERROR
        END ELSE
            CALL DBR ('CHEQUE.REGISTER':@FM:CHEQUE.REG.CHEQUE.NOS, CHQ.CUS, CHQ.NOS)
            LF   = FIELD (CHQ.NOS, "-", 1)
            RH   = FIELD (CHQ.NOS, "-", 2)
            IF COMI GT RH OR COMI LT LF THEN
                ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
*TEXT = 'NOT.INVOLVED.IN.CHQ' ; CALL REM
            END
**************************************************************************
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID.STOP = CH.ACCT:"*": COMI
****TEXT = "CHQ.ID.STOP =":CHQ.ID.STOP ; CALL REM
**** CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
            CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,CHQ.ACCT,CHQ.TYPE)
            TY = CHQ.TYPE
*Line [ 96 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT (TY,@VM)
            FOR X = 1 TO DD
                CHQ.ID = CHQ.TYPE<1,X>:'.':CH.ACCT:'.':COMI
                CALL DBR("CHEQUE.REGISTER.SUPPLEMENT":@FM:CC.CRS.DATE.STOPPED,CHQ.ID.STOP,CURR)
                TEXT = "CURR = ":CURR ; CALL REM
                IF CURR THEN
                    ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
                END
            NEXT X
****END OF UPDATE 8/3/2016*****************************
****************************************************************************
*  FN.PAYMENT.STOP = 'F.PAYMENT.STOP' ; F.PAYMENT.STOP ='' ; R.PAYMENT.STOP = '' ; E= ''
*  CALL OPF(FN.PAYMENT.STOP,F.PAYMENT.STOP)
*  CALL F.READ(FN.PAYMENT.STOP,CH.ACCT, R.PAYMENT.STOP, F.PAYMENT.STOP , E)
* FIRST.NO = R.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
* LAST.NO = R.PAYMENT.STOP<AC.PAY.LAST.CHEQUE.NO>
* CHQ.TYPE = R.PAYMENT.STOP<AC.PAY.CHEQUE.TYPE>
* STOP.TYPE = R.PAYMENT.STOP<AC.PAY.PAYM.STOP.TYPE>

* DD = DCOUNT (STOP.TYPE,VM)
* FOR X = 1 TO DD
* IF CHQ.TYPE<1,X> = 'SCB' THEN
* IF LAST.NO<1,X> = "" THEN
*    IF COMI = FIRST.NO<1,X> THEN ETEXT = '��� ����� �����' ; CALL STORE.END.ERROR
* END ELSE
*  FOR I = FIRST.NO<1,X> TO LAST.NO<1,X>
*     IF COMI = I THEN ETEXT = '��� ����� �����'; CALL STORE.END.ERROR
* NEXT I
* END
*  END
* NEXT X
*********************************************************************************
            CALL F.RELEASE(FN.PAYMENT.STOP,ACCT.NO,F.PAYMENT.STOP)
        END
    END
    RETURN
END
