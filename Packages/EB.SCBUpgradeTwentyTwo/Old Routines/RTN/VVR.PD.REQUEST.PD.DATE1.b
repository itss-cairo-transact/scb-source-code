* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.PD.REQUEST.PD.DATE1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PD.REQUEST1

    CHK.MONTH = COMI[5,2]
    BEGIN CASE
    CASE CHK.MONTH EQ "01"
        COMI = COMI[1,6]: 31
    CASE CHK.MONTH EQ "02"
        CHK.YEAR = COMI[1,4]
        SAM = CHK.YEAR / 4
        AFT.DOT = FIELD(SAM,".",2)
        IF AFT.DOT THEN COMI = COMI[1,6]: 28
        IF NOT(AFT.DOT) THEN COMI = COMI[1,6]: 29
    CASE CHK.MONTH EQ "03"
        COMI = COMI[1,6]: 31
    CASE CHK.MONTH EQ "04"
        COMI = COMI[1,6]: 30
    CASE CHK.MONTH EQ "05"
        COMI = COMI[1,6]: 31
    CASE CHK.MONTH EQ "06"
        COMI = COMI[1,6]: 30
    CASE CHK.MONTH EQ "07"
        COMI = COMI[1,6]: 31
    CASE CHK.MONTH EQ "08"
        COMI = COMI[1,6]: 31
    CASE CHK.MONTH EQ "09"
        COMI = COMI[1,6]: 30
    CASE CHK.MONTH EQ "10"
        COMI = COMI[1,6]: 31
    CASE CHK.MONTH EQ "11"
        COMI = COMI[1,6]: 30
    CASE CHK.MONTH EQ "12"
        COMI = COMI[1,6]: 31
    END CASE

    IF COMI THEN
        IF COMI GE  R.NEW(SCB.PD.SYSTEM.DATE) THEN
            R.NEW(SCB.PD.SYSTEM.DATE) = COMI
            CALL REBUILD.SCREEN
        END ELSE
            ETEXT = "��� ������� ��� ������"
        END
    END


    RETURN
END
