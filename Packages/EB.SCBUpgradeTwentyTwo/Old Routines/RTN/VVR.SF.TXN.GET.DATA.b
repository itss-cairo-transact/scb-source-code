* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SF.TXN.GET.DATA

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFE.TYPE
*---------------------------------------------
    TMP.CUS      = R.NEW(SCB.SF.TR.CUSTOMER.NO)
    TMP.SF.KE.NO = COMI
    TMP.ID       = TMP.CUS:".":TMP.SF.KE.NO
    CALL DBR ('SCB.SAFEKEEP.RIGHT':@FM:SCB.SAF.RIG.CUSTOMER.NO,TMP.ID,RG.CUS)
    IF NOT(RG.CUS) THEN
        ETEXT = 'no record in safe keep right '
    END ELSE
        CALL DBR ('SCB.SAFEKEEP':@FM:SCB.SAF.KEY.NO,TMP.SF.KE.NO,KEY.NO)
        R.NEW( SCB.SF.TR.KEY.NO) = KEY.NO

        CALL DBR('SCB.SAFEKEEP':@FM:SCB.SAF.TYPE, TMP.SF.KE.NO , TMP.TYPE)
        CALL DBR('SCB.SAFE.TYPE':@FM:SCB.SAF.TY.MARGIN.VALUE, TMP.TYPE , INSR)

        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)

        CALL F.READ(FN.CUS,R.NEW(SCB.SF.TR.CUSTOMER.NO),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>

        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
        END ELSE
            INSR = ''
        END

        R.NEW(SCB.SF.TR.MARGIN.VALUE) = INSR

*--- CALC COM.AMT
        CALL DBR('SCB.SAFE.TYPE':@FM:SCB.SAF.TY.KEY2.VALUE, TMP.TYPE , COM.AMT)

        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)

        CALL F.READ(FN.CUS,R.NEW(SCB.SF.TR.CUSTOMER.NO),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>
        TAR  = R.CUS<EB.CUS.TARGET>
        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
        END ELSE
            COM.AMT = COM.AMT / 2
        END

        IF TAR EQ '5870' THEN

            COM.AMT.DIS = COM.AMT * (25/100)
            COM.AMT     = COM.AMT - COM.AMT.DIS

        END

        IF TAR EQ '5860' THEN
            COM.AMT.DIS = COM.AMT * (15/100)
            COM.AMT     = COM.AMT - COM.AMT.DIS
        END

        IF TAR NE '5860' AND TAR NE '5870' THEN

            COM.AMT  = COM.AMT

        END

*        R.NEW(SCB.SF.TR.COMMISSION.AMT)  = COM.AMT * 0.14
        R.NEW(SCB.SF.TR.COMMISSION.AMT2)  = COM.AMT
*----------------
        CALL REBUILD.SCREEN
    END
    RETURN
END
