* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.TELLER.BILLS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS


    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    IF COMI[1,2] EQ "BR" THEN
        T.SEL = "SSELECT FBNK.BILL.REGISTER WITH @ID EQ ": COMI
        CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, E)
        CALL OPF( FN.BR,F.BR)

        CALL F.READ( FN.BR, KEY.LIST, R.BR, F.BR, ETEXT)
        IF NOT(KEY.LIST) THEN ETEXT = "������ ��� �����"
        CHQ.STAT = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>

        BEGIN CASE
        CASE CHQ.STAT EQ 7 OR CHQ.STAT EQ 14
            ETEXT = '�����' ; CALL STORE.END.ERROR
        CASE CHQ.STAT EQ 8 OR CHQ.STAT EQ 15
            ETEXT = '�����' ; CALL STORE.END.ERROR
        CASE CHQ.STAT EQ 9
            ETEXT = '��� ��� ����' ; CALL STORE.END.ERROR
        CASE CHQ.STAT EQ 10
            ETEXT = '��� ��� ������' ; CALL STORE.END.ERROR
        CASE CHQ.STAT EQ 13
            ETEXT = ' ������� ����� ��� ��� �����' ; CALL STORE.END.ERROR
        CASE OTHERWISE
            R.NEW(TT.TE.NARRATIVE.2)<1,1> = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.DR.NAME>
*--- UPDATED BY NESSMA ON 2014/02/25
            IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.DR.ADDRESS> THEN
                R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.DR.ADDRESS>
            END
*--- END EDITING ---
            R.NEW(TT.TE.AMOUNT.LOCAL.1)   = R.BR<EB.BILL.REG.AMOUNT>
            R.NEW(TT.TE.CUSTOMER.1)       = R.BR<EB.BILL.REG.DRAWER>
            R.NEW(TT.TE.ACCOUNT.1)        = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.LIQ.ACCT>
            CALL REBUILD.SCREEN
        END CASE
    END ELSE
        ETEXT = 'SHOULD START WITH BR' ; CALL STORE.END.ERROR
    END

    RETURN
END
