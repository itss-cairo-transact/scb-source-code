* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.SCB.BR.COLL.CHECK.BR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF( FN.BR,F.BR)

    IF COMI[1,2] EQ "BR" THEN
        CALL F.READ(FN.BR,COMI,R.BR,F.BR,E11)
    END ELSE
        T.SEL = "SSELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": COMI
        CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, E)
        TEXT = "SEL" : SELECTED ; CALL REM
        CALL F.READ( FN.BR, KEY.LIST, R.BR, F.BR, ETEXT)
        TEXT = "ID" : KEY.LIST  ; CALL REM
        COMI = KEY.LIST
    END
    RET.REASON = R.NEW(SCB.BT.RETURN.REASON)<1,AV>
    IF(ETEXT) THEN ETEXT = "������ ��� �����"

    BEGIN CASE
    CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> = 8
        ETEXT = "�����"

    CASE R.BR<EB.BILL.REG.CURRENCY> # "EGP"
        ETEXT = "��� �� ���� ������ ����"

    CASE  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> = 7
        ETEXT = "�����"

    CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 2 OR R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 6
        ETEXT = "��� ������� �� ����������"

    CASE ( R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT> EQ ''  AND RET.REASON EQ '' )
        ETEXT = "CHECK CUST.ACCT"
*--- EDIT BY HYTHAM ON 2011/04/21
    CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 5
        BNK  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BANK>

        IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.IN.OUT.BILL> EQ 'NO' THEN
        END ELSE
            ETEXT = "��� ����� ���� �������"
        END
    CASE OTHERWISE
        RETURN
    END CASE

    RETURN
END
