* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.TT.BN.CHQ.NEW


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ

    * TO CHECK IF CHEQUE CURRENCY IS LOCAL THEN DEFAULT AMOUNT LOCAL BY CHEQUE AMOUNT
    * AND IF IT IS FORIGN THEN DEFAULT AMOUNT FORIGN BY CHEQUE AMOUNT
    * DEFAULT FIELD CURRENCY.1 BY CHEQUE CURRENCY
    * DEFAULT FIELD ACCOUNT.2 BY CHEQ NOSTRO ACC.
    * DEFAULT FIELD CHEQUE NO BY THE DR.CHEQ.NO
    * DEFAULT ISSUE DATE BY THE DR.CHEQ.NO

    F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
    ID = COMI
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
*   IF R.COUNT<DR.CHQ.CHEQ.STATUS> = 1 AND R.COUNT<DR.CHQ.CHEQ.TYPE> = 2 THEN
        IF R.COUNT<DR.CHQ.CURRENCY> =  LCCY THEN
            R.NEW(TT.TE.AMOUNT.LOCAL.1) = R.COUNT<DR.CHQ.AMOUNT>
        END ELSE
            R.NEW(TT.TE.AMOUNT.FCY.1) = R.COUNT<DR.CHQ.AMOUNT>
        END
        R.NEW(TT.TE.CURRENCY.1) = R.COUNT<DR.CHQ.CURRENCY>
        R.NEW(TT.TE.ACCOUNT.1) =  R.COUNT<DR.CHQ.NOS.ACCT>
       * R.NEW(TT.TE.ACCOUNT.2) =  R.COUNT<DR.CHQ.NOS.ACCT>
        R.NEW(TT.TE.CHEQUE.NUMBER) =  R.COUNT<DR.CHQ.CHEQ.NO>
        R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>=R.COUNT<DR.CHQ.CHEQ.DATE>
        CALL REBUILD.SCREEN
* IF V$FUNCTION = 'A' THEN
*  R.COUNT<DR.CHQ.CHEQ.STATUS> = 2
*  CALL F.WRITE(FN.COUNT,ID,R.COUNT)
*  CALL JOURNAL.UPDATE(ID)
* END
* END ELSE
*    ETEXT = 'CHQ.STATUS.NOT.ALLOWED'
    END

    CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
    CLOSE FN.COUNT
    RETURN
END
