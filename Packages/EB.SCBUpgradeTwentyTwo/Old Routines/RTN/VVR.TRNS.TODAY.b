* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>13130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TRNS.TODAY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOL.ENT.TODAY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER

    F.COUNT = '' ; FN.COUNT = 'F.SCB.TRANS.TODAY'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.ACC = '' ; FN.ACC = 'FBNK.ACCOUNT'; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF( FN.STMT,F.STMT)

    F.ACCT.ENT = '' ; FN.ACCT.ENT = 'FBNK.ACCT.ENT.TODAY'; R.ACCT.ENT = ''
    CALL OPF(FN.ACCT.ENT,F.ACCT.ENT)
*****UPDATED ON 6/6/2008 BY NESSREEN AHMED********************************
    F.FT = '' ; FN.FT = 'FBNK.FUNDS.TRANSFER' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    F.BR = '' ; FN.BR = 'FBNK.BILL.REGISTER' ; R.BR = ''
    CALL OPF(FN.BR,F.BR)
    F.WH = '' ; FN.WH = 'F.SCB.WH.TRANS' ; R.WH = ''
    CALL OPF(FN.WH,F.WH)
    F.LD = '' ; FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    ORD.BNK = ''
**************************************************************************
*****UPDATED ON 24/08/2008 BY NESSREEN AHMED******************************
    F.USR = '' ; FN.USR = 'F.USER' ; R.USR = ''
    CALL OPF(FN.USR,F.USR)
**************************************************************************
******UPDATED ON 05/12/2008 BY NESSREEN AHMED*****************************
    F.TT = '' ; FN.TT = 'FBNK.TELLER' ; R.TT = ''
    CALL OPF(FN.TT,F.TT)

******UPDATED ON 10/12/2008 BY NESSREEN AHMED******************************
    F.DR = '' ; FN.DR = 'FBNK.DRAWINGS' ; R.DR = ''
    CALL OPF(FN.DR,F.DR)
***************************************************************************
******UPDATED ON 20/12/2009 BY NESSREEN AHMED******************************
    F.BOT = '' ; FN.BOT = 'F.SCB.BOAT.TOTAL' ; R.BOT = ''
    CALL OPF(FN.BOT,F.BOT)
    F.BO = '' ; FN.BO = 'F.SCB.BOAT.CUSTOMER' ; R.BO = ''
    CALL OPF(FN.BO,F.BO)

***************************************************************************
****UPDATED ON 04/01/2009 BY NESSREEN AHMED********************************
    SAM = '' ; XCC = '' ; HH = '' ; MM = '' ; TIMX= '' ; DD1 = '' ; MM = '' ; YY = '' ; DATX = '' ; DATX.TIMX = ''
    SAM    = TIMEDATE()
    XCC=OCONV(DATE(),"D-")
    HH = SAM[1,2]
    MM = SAM[4,2]
    TIMX = HH:MM
    MM = XCC[1,2]
    DD1 = XCC[4,2]
    YY = XCC[9,2]
    DATX = YY:MM:DD1
    DATX.TIMX = DATX:TIMX

***************************************************************************
*******UPDATED ON 29/6/2010 BY NESSREEN AHMED******************************
    FN.LCH = 'F.LETTER.OF.CREDIT$HIS' ; F.LCH = '' ; R.LCH = '' ; ELCH1 = ''
    FN.DRH = 'F.DRAWINGS$HIS' ; F.DRH = '' ; R.DRH = '' ; EDRH1 = ''
    CALL OPF(FN.LCH,F.LCH)
    CALL OPF(FN.DRH,F.DRH)
****************************************************************************
    T.SEL = "SELECT FBNK.ACCT.ENT.TODAY"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* CALL F.READ(FN.ACCT.ENT,'0110073410650101',R.ACCT.ENT,F.ACCT.ENT,READ.ERR)
    FOR I = 1 TO SELECTED
        TRNS.DATE = ''
        CALL F.READ(FN.ACCT.ENT,KEY.LIST<I>,R.ACCT.ENT,F.ACCT.ENT,READ.ERR)
****PRINT KEY.LIST<I>
****PRINT R.ACCT.ENT
        X = 1 ; Y = 22
        FOR Z = X TO LEN(R.ACCT.ENT)
            CALL F.READ(FN.STMT,R.ACCT.ENT[Z,Y], R.STMT,F.STMT, ETEXT)
*****UPDATED BY NESSREEN AHMED ON 9/12/2010*****************************************
**** ID.NO = "SE*":R.STMT<AC.STE.SYSTEM.ID>:"*":R.STMT<AC.STE.ACCOUNT.OFFICER>:"*":R.ACCT.ENT[Z,Y]:"*":TODAY
            TRNS.DATE = R.STMT<AC.STE.BOOKING.DATE>
            ID.NO = "SE*":R.STMT<AC.STE.SYSTEM.ID>:"*":R.STMT<AC.STE.ACCOUNT.OFFICER>:"*":R.ACCT.ENT[Z,Y]:"*":TRNS.DATE
***END OF UPDATE 9/12/2010*********************************************************
****PRINT ID.NO
****UPDATED BY NESSREEN AHMED ON 25/06/2008***
            CALL F.READ(FN.COUNT,ID.NO,R.COUNT,F.COUNT, ETEXT)
***28/08/2008    IF ETEXT THEN
            IF ETEXT THEN
***********************************************
                R.COUNT<TRANS.ACCOUNT.NUMBER> = R.STMT<AC.STE.ACCOUNT.NUMBER>
                CALL DBR('ACCOUNT':@FM:EB.ACCOUNT.TITLE.1,R.STMT<AC.STE.ACCOUNT.NUMBER>,TITLE)

                R.COUNT<TRANS.NAME> = TITLE
                R.COUNT<TRANS.AMOUNT.LCY> = R.STMT<AC.STE.AMOUNT.LCY>
                R.COUNT<TRANS.TRANSACTION.CODE> = R.STMT<AC.STE.TRANSACTION.CODE>
                R.COUNT<TRANS.CUSTOMER.ID> = R.STMT<AC.STE.CUSTOMER.ID>
                R.COUNT<TRANS.ACCOUNT.OFFICER> = R.STMT<AC.STE.ACCOUNT.OFFICER>
                R.COUNT<TRANS.PRODUCT.CATEGORY> = R.STMT<AC.STE.PRODUCT.CATEGORY>
                R.COUNT<TRANS.VALUE.DATE> = R.STMT<AC.STE.VALUE.DATE>
                R.COUNT<TRANS.CURRENCY> = R.STMT<AC.STE.CURRENCY>
                R.COUNT<TRANS.AMOUNT.FCY> = R.STMT<AC.STE.AMOUNT.FCY>
                R.COUNT<TRANS.BOOKING.DATE> = R.STMT<AC.STE.BOOKING.DATE>
                R.COUNT<TRANS.EXCHANGE.RATE> = R.STMT<AC.STE.EXCHANGE.RATE>
                R.COUNT<TRANS.OUR.REFERENCE> = R.STMT<AC.STE.TRANS.REFERENCE>
                R.COUNT<TRANS.SYSTEM.ID> = R.STMT<AC.STE.SYSTEM.ID>
************UPDATED BY NESSREEN AHMED ON 6/6/2008***********************
                SYSTEM.ID = R.STMT<AC.STE.SYSTEM.ID>
                INP.1 =  R.STMT<AC.STE.INPUTTER><1,1>
                INP = FIELD(INP.1, "_", 2)
                BEGIN CASE
                CASE SYSTEM.ID EQ 'FT'
                    ST.REF = R.STMT<AC.STE.TRANS.REFERENCE>
                    DBREF = ST.REF[1,12]
                    CALL F.READ(FN.FT,DBREF,R.FT,F.FT,FT.ERR)
                    TR.TYPE = R.FT<FT.TRANSACTION.TYPE>
                    DB.REF = R.FT<FT.DEBIT.THEIR.REF>
                    BEGIN CASE
                    CASE (TR.TYPE EQ 'AC22') OR (TR.TYPE EQ 'AC23') OR (TR.TYPE EQ 'AC25')
                        CALL F.READ(FN.BR,DB.REF,R.BR,F.BR,BR.ERR)
                        BR.INP.1 = R.BR<EB.BILL.REG.INPUTTER><1,1>
                        BR.INP = FIELD(BR.INP.1, "_", 2)
                        BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                        R.COUNT<TRANS.INPUTTER> = BR.INP
                        R.COUNT<TRANS.AUTHORISER> = BR.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        BR.INP.USR = FIELD(BR.INP.1, "_", 2)
                        CALL F.READ(FN.USR, BR.INP.USR, R.USR, F.USR, USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED ***************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                    CASE TR.TYPE EQ 'AC90'
                        ORD.BNK = R.FT<FT.ORDERING.BANK>
                        CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                        WH.INP.1 = R.WH<SCB.WH.TRANS.INPUTTER><1,1>
                        WH.INP = FIELD(WH.INP.1, "_", 2)
                        WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                        R.COUNT<TRANS.INPUTTER> = WH.INP
                        R.COUNT<TRANS.AUTHORISER> = WH.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        WH.INP.USR = FIELD(WH.INP.1, "_", 2)
                        CALL F.READ(FN.USR, WH.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                    CASE TR.TYPE EQ 'AC92'
                        ORD.BNK = R.FT<FT.ORDERING.BANK>
                        CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                        WH.INP.1 = R.WH<SCB.WH.TRANS.INPUTTER><1,1>
                        WH.INP = FIELD(WH.INP.1, "_", 2)
                        WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                        R.COUNT<TRANS.INPUTTER> = WH.INP
                        R.COUNT<TRANS.AUTHORISER> = WH.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        WH.INP.USR = FIELD(WH.INP.1, "_", 2)
                        CALL F.READ(FN.USR, WH.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
********************UPDATED BY NESSREEN AHMED 28/2/2012*******************************************
********************CASE TR.TYPE EQ 'ACLG'
                    CASE (TR.TYPE EQ 'ACLG') OR (TR.TYPE EQ 'AC77')
********************END OF UPDATE 28/2/2012*******************************************************
                        ORD.BNK = R.FT<FT.ORDERING.BANK>
                        CALL F.READ(FN.LD,ORD.BNK[1,12],R.LD,F.LD,LD.ERR)
                        LD.INP.1 = R.LD<LD.INPUTTER><1,1>
                        LD.INP = FIELD(LD.INP.1, "_", 2)
                        LD.AUTH = R.LD<LD.AUTHORISER>
                        R.COUNT<TRANS.INPUTTER> = LD.INP
                        R.COUNT<TRANS.AUTHORISER> = LD.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        LD.INP.USR = FIELD(LD.INP.1, "_", 2)
                        CALL F.READ(FN.USR, LD.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
*********UPDATED BY NESSREEN IN 16/6/2008*********************
                    CASE TR.TYPE EQ 'AC21'
                        ORD.BNK = R.FT<FT.ORDERING.BANK>
                        CALL F.READ(FN.BR,ORD.BNK,R.BR,F.BR,BR.ERR)
                        BR.INP.1 = R.BR<EB.BILL.REG.INPUTTER><1,1>
                        BR.INP = FIELD(BR.INP.1, "_", 2)
                        BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                        R.COUNT<TRANS.INPUTTER> = BR.INP
                        R.COUNT<TRANS.AUTHORISER> = BR.AUTH
***************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        BR.INP.USR = FIELD(BR.INP.1, "_", 2)
                        CALL F.READ(FN.USR, BR.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
*********UPDATED BY NESSREEN AHMED 7/5/2009********************
*********UPDATED BY NESSREEN IN 5/12/2008*********************
                    CASE TR.TYPE EQ 'AC10'
                        DB.REF = R.FT<FT.DEBIT.THEIR.REF>
                        ID.REF = DB.REF[1,2]
                        BEGIN CASE
                        CASE ID.REF = 'TT'
                            CALL F.READ(FN.TT,DB.REF,R.TT,F.TT,TT.ERR)
                            TT.INP.1 = R.TT<TT.TE.INPUTTER><1,1>
                            TT.INP = FIELD(TT.INP.1, "_", 2)
                            TT.AUTH= R.TT<TT.TE.AUTHORISER>
***7/5/2009*** R.COUNT<TRANS.INPUTTER> = TT.INP
***7/5/2009*** R.COUNT<TRANS.AUTHORISER> = TT.AUTH
                            CALL F.READ(FN.USR, TT.INP, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
***7/5/2009***  R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************UPDATED BY NESSREEN 12/04/2009************************************************
***7/5/2009***      R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                        CASE ID.REF = 'FT'
                            CALL F.READ(FN.FT,DB.REF,R.FT,F.FT,FT.ERR)
                            FT.INP.1 = R.FT<FT.INPUTTER><1,1>
                            FT.INP = FIELD(FT.INP.1, "_", 2)
                            FT.AUTH =R.FT<FT.AUTHORISER>
***7/5/2009***      R.COUNT<TRANS.INPUTTER> = FT.INP
***7/5/2009***      R.COUNT<TRANS.AUTHORISER> = FT.AUTH
                            CALL F.READ(FN.USR, FT.INP, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
***7/5/2009***     R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************UPDATED BY NESSREEN 12/04/2009************************************************
***7/5/2009***        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                        CASE ID.REF = 'TF'
                            CALL F.READ(FN.DR,DB.REF,R.DR,F.DR,DR.ERR)
                            DR.INP.1 = R.DR<TF.DR.INPUTTER><1,1>
                            DR.INP = FIELD(DR.INP.1, "_", 2)
                            DR.AUTH =R.DR<TF.DR.AUTHORISER>
***7/5/2009*** R.COUNT<TRANS.INPUTTER> = DR.INP
***7/5/2009*** R.COUNT<TRANS.AUTHORISER> = DR.AUTH
                            CALL F.READ(FN.USR, DR.INP, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
***7/5/2009*** R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************UPDATED BY NESSREEN 12/04/2009************************************************
***7/5/2009*** R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                        END CASE
*********************END OF UPDATED*************************************************************
***************************************************
*********************UPDATED BY NESSREEN AHMED ON 20/10/2009**************************************
                    CASE (TR.TYPE EQ 'AC15') OR (TR.TYPE EQ 'AC16')
**TEXT = 'TR.TYPE=':TR.TYPE ; CALL REM
                        DB.REF = R.FT<FT.DEBIT.THEIR.REF>
**TEXT = 'DTR=':DB.REF ; CALL REM
                        ID.REF3 = DB.REF[1,3]
                        ID.REF2 = DB.REF[1,2]
                        IF ID.REF3 EQ 'BOT' THEN
**TEXT = 'BOT' ; CALL REM
                            CALL F.READ(FN.BOT,DB.REF,R.BOT,F.BOT,BOT.ERR)
                            BOT.INP.1 = R.BOT<BOT.INPUTTER><1,1>
**TEXT = 'INP=':BOT.INP.1 ; CALL REM
                            BOT.INP = FIELD(BOT.INP.1, "_", 2)
                            BOT.AUTH = R.BOT<BOT.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = BOT.INP
                            R.COUNT<TRANS.AUTHORISER> = BOT.AUTH
                            BOT.INP.USR = FIELD(BOT.INP.1, "_", 2)
                            CALL F.READ(FN.USR, BOT.INP.USR, R.USR, F.USR, USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                        END ELSE
                            IF ID.REF2 EQ 'BO' THEN
**TEXT = 'BO' ; CALL REM
                                CALL F.READ(FN.BO,DB.REF,R.BO,F.BO,BO.ERR)
                                BO.INP.1 = R.BO<BO.INPUTTER><1,1>
**TEXT = 'BO.INP=':BO.INP.1 ; CALL REM
                                BO.INP = FIELD(BO.INP.1, "_", 2)
                                BO.AUTH = R.BO<BO.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = BO.INP
                                R.COUNT<TRANS.AUTHORISER> = BO.AUTH
                                BO.INP.USR = FIELD(BO.INP.1, "_", 2)
                                CALL F.READ(FN.USR, BO.INP.USR, R.USR, F.USR, USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                            END
                        END

************END OF UPDATED 20/10/2009**************************************************************
***************************************************************************************************
                    CASE OTHERWISE
*****UPDATED BY NESSREEN AHMED ON 11/01/2009*********************
**     FT.INP =  R.FT<FT.INPUTTER><1,1>
                        FT.INP =  R.STMT<AC.STE.INPUTTER>
                        FT.INP.USR = FIELD(FT.INP, "_", 2)
                        R.COUNT<TRANS.INPUTTER> = FT.INP.USR
**     R.COUNT<TRANS.AUTHORISER> = R.FT<FT.AUTHORISER>
                        R.COUNT<TRANS.AUTHORISER> = R.STMT<AC.STE.AUTHORISER>
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        CALL F.READ(FN.USR, FT.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                    END CASE
*********UPDATED BY NESSREEN IN 16/6/2008******************************************
                CASE SYSTEM.ID EQ 'CQ'
                    STT.REF = R.STMT<AC.STE.TRANS.REFERENCE>
                    DBREFS = STT.REF[1,12]
                    CALL F.READ(FN.BR,DBREFS,R.BR,F.BR,BR.ERR)
*******UPDATED BY NESSREEN AHMED 09/07/2008*******************
                    IF BR.ERR THEN
                        STE.INP =  R.STMT<AC.STE.INPUTTER><1,1>
                        STE.INP.USR = FIELD(STE.INP, "_", 2)
                        R.COUNT<TRANS.INPUTTER> = STE.INP.USR
                        R.COUNT<TRANS.AUTHORISER> = R.STMT<AC.STE.AUTHORISER>
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        CALL F.READ(FN.USR, STE.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                    END ELSE
*******END OF UPDATED******************************************
                        BR.INP.1 = R.BR<EB.BILL.REG.INPUTTER><1,1>
                        BR.INP = FIELD(BR.INP.1, "_", 2)
                        BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                        R.COUNT<TRANS.INPUTTER> = BR.INP
                        R.COUNT<TRANS.AUTHORISER> = BR.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        BR.INP.USR = FIELD(BR.INP.1, "_", 2)
                        CALL F.READ(FN.USR, BR.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                    END
******UPDATED BY NESSREEN AHHHMED ON 18/12/2008**************************************************
********UPDATED BY NESSREEN AHMED ON 10/12/2008*************************************************
****               CASE SYSTEM.ID EQ 'LD'
****                   STLD.REF = R.STMT<AC.STE.TRANS.REFERENCE>
****                   CALL F.READ(FN.LD,STLD.REF,R.LD,F.LD,E1)
****                   LD.INP.1 = R.LD<LD.INPUTTER><1,1>
****                   LD.INP = FIELD(LD.INP.1, "_", 2)
****                   LD.AUTH = R.LD<LD.AUTHORISER>
****                   R.COUNT<TRANS.INPUTTER> = LD.INP
****                   R.COUNT<TRANS.AUTHORISER> = LD.AUTH

****                   CALL F.READ(FN.USR, LD.INP, R.USR, F.USR,USR.ERR)
****                   LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
****                   USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
****                   R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********END OF UPDATED************************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                    R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
****************UPDATED BY NESSREEN 15/08/2009*****************************************************
                CASE SYSTEM.ID EQ 'SYS'
                    BS.ID = ''  ;  SYS.REF = ''   ; STE.INP.BS = '' ;  STE.INP.BS.1 = '' ; STE.AUTH.BS.1 = ''
                    SYS.REF = R.STMT<AC.STE.TRANS.REFERENCE>
                    BS.ID = SYS.REF[1,2]
**IF (BS.ID EQ 'BS') OR (BS.ID EQ 'BR') THEN
                    STE.INP.BS.1 =  R.STMT<AC.STE.INPUTTER><1,1>
                    STE.AUTH.BS.1 = R.STMT<AC.STE.AUTHORISER>
                    IF STE.INP.BS.1 = '' AND STE.AUTH.BS.1 # '' THEN
                        STE.INP.BS.1 = STE.AUTH.BS.1
                        STE.INP.BS = FIELD(STE.INP.BS.1, "_", 2)
                        R.COUNT<TRANS.INPUTTER> = STE.INP.BS
                        R.COUNT<TRANS.AUTHORISER> = STE.AUTH.BS.1
                        CALL F.READ(FN.USR, STE.INP.BS, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                    END ELSE
                        IF STE.INP.BS.1 # ''  THEN
                            STE.INP.BS.1 =  R.STMT<AC.STE.INPUTTER><1,1>
                            STE.INP.BS = FIELD(STE.INP.BS.1, "_", 2)
                            R.COUNT<TRANS.INPUTTER> = STE.INP.BS
                            R.COUNT<TRANS.AUTHORISER> = STE.AUTH.BS.1
                            CALL F.READ(FN.USR, STE.INP.BS, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>

                        END   ;*END OF STE.INP.BS.1 # ''
                    END       ;*END OF IF STE.INP.BS.1 = '' AND STE.AUTH.BS.1 # ''

**                   END       ;*END OF BS.ID EQ 'BS' OR BS.ID EQ 'BR'
****************END OF UPDATE 15/08/2009************************************************************
                CASE OTHERWISE
                    STE.INP.1 =  R.STMT<AC.STE.INPUTTER><1,1>
                    STE.INP = FIELD(STE.INP.1, "_", 2)
                    R.COUNT<TRANS.INPUTTER> = STE.INP
                    R.COUNT<TRANS.AUTHORISER> = R.STMT<AC.STE.AUTHORISER>
********************UPDATED BY NESSREEN 24/08/2008**********************************************

                    STE.INP.USR = FIELD(STE.INP.1, "_", 2)
                    CALL F.READ(FN.USR, STE.INP.USR, R.USR, F.USR,USR.ERR)
                    LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                    USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                    R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 12/04/2009************************************************
                    R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****12/04/2009************************************************
                END CASE
************************************************************************
                R.COUNT<TRANS.STMT.NO> = R.STMT<AC.STE.STMT.NO>
                R.COUNT<TRANS.DATE.TIME> = R.STMT<AC.STE.DATE.TIME>
                R.COUNT<TRANS.CRF.PROD.CAT> = R.STMT<AC.STE.CRF.PROD.CAT>
                R.COUNT<TRANS.REF> = R.ACCT.ENT[Z,Y]
*******UPDATED IN 1/6/2008 BY NESSREEN AHMED**********************
****UPDATED BY NESSREEN 12/04/2009****** R.COUNT<TRANS.COMPANY.CO>  = R.STMT<AC.STE.COMPANY.CODE>
******************************************************************
*******UPDATED IN 25/11/2008 BY NESSREEN AHMED*******************
                R.COUNT<TRANS.NARRATIVE> = R.STMT<AC.STE.NARRATIVE>
*******************************************************************
                WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
                    PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
                END
*                CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
            END     ;**END OF ETEXT ***
            Z = Z + 22
        NEXT Z
    NEXT I
***********************************************************************
*                                                                     *
*****************************CATEG ENTRY ******************************
*                                                                     *
***********************************************************************

    R.COUNT = ''
    FN.CATEG = 'FBNK.CATEG.ENTRY' ; F.CATEG = '' ; R.CATEG = ''

    CALL OPF( FN.CATEG,F.CATEG)
    F.CATEG.TODAY = '' ; FN.CATEG.TODAY = 'F.CATEG.ENT.TODAY'; R.CATEG.TODAY = ''
    CALL OPF(FN.CATEG.TODAY,F.CATEG.TODAY)

    T.SEL = "SELECT FBNK.CATEG.ENT.TODAY"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*   CALL F.READ(FN.ACCT.ENT,'0110073410650101',R.ACCT.ENT,F.ACCT.ENT,READ.ERR)
    FOR I = 1 TO SELECTED
        TRN.DATE = ''
        CALL F.READ(FN.CATEG.TODAY,KEY.LIST<I>,R.CATEG.TODAY,F.CATEG.TODAY,READ.ERR)
**PRINT KEY.LIST<I>
**PRINT R.CATEG.TODAY
        X = 1 ; Y = 22
        FOR Z = X TO LEN(R.CATEG.TODAY)

            CALL F.READ(FN.CATEG,R.CATEG.TODAY[Z,Y], R.CATEG,F.CATEG, ETEXT)
*PRINT R.ACCT.ENT[Z,Y]
***UPDATED BY NESSREEN AHMED ON 9/12/2010*****************************************
***    ID.NO = "CE*":R.CATEG<AC.CAT.SYSTEM.ID>:"*":R.CATEG<AC.CAT.ACCOUNT.OFFICER>:"*":R.CATEG.TODAY[Z,Y]:"*":TODAY
            TRN.DATE = R.CATEG<AC.STE.BOOKING.DATE>
            ID.NO = "CE*":R.CATEG<AC.CAT.SYSTEM.ID>:"*":R.CATEG<AC.CAT.ACCOUNT.OFFICER>:"*":R.CATEG.TODAY[Z,Y]:"*":TRN.DATE
***END OF UPDATE 9/12/2010********************************************************
**         PRINT ID.NO
***UPDATED BY NESSREN AHMED ON 25/6/2008************
            CALL F.READ(FN.COUNT,ID.NO, R.COUNT,F.COUNT, ETEXT)
            IF ETEXT THEN
*********
                IF R.CATEG<AC.CAT.TRANSACTION.CODE> NE 450 THEN
                    R.COUNT<TRANS.ACCOUNT.NUMBER> = R.CATEG<AC.CAT.ACCOUNT.NUMBER>
                    R.COUNT<TRANS.AMOUNT.LCY> = R.CATEG<AC.CAT.AMOUNT.LCY>
                    R.COUNT<TRANS.TRANSACTION.CODE> = R.CATEG<AC.CAT.TRANSACTION.CODE>
                    R.COUNT<TRANS.CUSTOMER.ID> = R.CATEG<AC.CAT.CUSTOMER.ID>
                    R.COUNT<TRANS.PL.CATEGORY> = R.CATEG<AC.CAT.PL.CATEGORY>
                    R.COUNT<TRANS.ACCOUNT.OFFICER> = R.CATEG<AC.CAT.ACCOUNT.OFFICER>
                    R.COUNT<TRANS.PRODUCT.CATEGORY> = R.CATEG<AC.CAT.PRODUCT.CATEGORY>
                    R.COUNT<TRANS.VALUE.DATE> = R.CATEG<AC.CAT.VALUE.DATE>
                    R.COUNT<TRANS.CURRENCY> = R.CATEG<AC.CAT.CURRENCY>
                    R.COUNT<TRANS.AMOUNT.FCY> = R.CATEG<AC.CAT.AMOUNT.FCY>
                    R.COUNT<TRANS.BOOKING.DATE> = R.CATEG<AC.STE.BOOKING.DATE>
                    R.COUNT<TRANS.EXCHANGE.RATE> = R.CATEG<AC.CAT.EXCHANGE.RATE>
                    R.COUNT<TRANS.OUR.REFERENCE> = R.CATEG<AC.CAT.TRANS.REFERENCE>
                    R.COUNT<TRANS.SYSTEM.ID> = R.CATEG<AC.CAT.SYSTEM.ID>
*********UPDATED BY NESSREEN AHMED ON 6/6/2008***********************
                    SYSTEM.ID.C = R.CATEG<AC.CAT.SYSTEM.ID>
                    BEGIN CASE
                    CASE SYSTEM.ID.C EQ 'FT'
                        CE.REF = R.CATEG<AC.CAT.TRANS.REFERENCE>
                        C.DB.REF = CE.REF[1,12]
                        CALL F.READ(FN.FT,C.DB.REF,R.FT,F.FT,FT.ERR)
                        TR.TYPE = R.FT<FT.TRANSACTION.TYPE>
                        DB.REF = R.FT<FT.DEBIT.THEIR.REF>
                        BEGIN CASE
                        CASE (TR.TYPE EQ 'AC22') OR (TR.TYPE EQ 'AC23') OR (TR.TYPE EQ 'AC25')
                            CALL F.READ(FN.BR,DB.REF,R.BR,F.BR,BR.ERR)
                            BR.INP.1 = R.BR<EB.BILL.REG.INPUTTER><1,1>
                            BR.INP = FIELD(BR.INP.1, "_", 2)
                            BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = BR.INP
                            R.COUNT<TRANS.AUTHORISER> = BR.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                            BR.INP.USR = FIELD(BR.INP.1, "_", 2)
                            CALL F.READ(FN.USR, BR.INP.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                        CASE TR.TYPE EQ 'AC90'
                            ORD.BNK = R.FT<FT.ORDERING.BANK>
                            CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                            WH.INP = R.WH<SCB.WH.TRANS.INPUTTER><1,1>
                            WH.INP.USR = FIELD(WH.INP, "_", 2)
                            WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = WH.INP.USR
                            R.COUNT<TRANS.AUTHORISER> = WH.AUTH
*******************UPDATED BY NESSREEN 24/08/2008**********************************************
**WH.INP.USR = FIELD(WH.INP, "_", 2)
                            CALL F.READ(FN.USR, WH.INP.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                        CASE TR.TYPE EQ 'AC92'
                            ORD.BNK = R.FT<FT.ORDERING.BANK>
                            CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                            WH.INP = R.WH<SCB.WH.TRANS.INPUTTER><1,1>
                            WH.INP.USR = FIELD(WH.INP, "_", 2)
                            WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = WH.INP.USR
                            R.COUNT<TRANS.AUTHORISER> = WH.AUTH
*******************UPDATED BY NESSREEN 24/08/2008**********************************************
** WH.INP.USR = FIELD(WH.INP, "_", 2)
                            CALL F.READ(FN.USR, WH.INP.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
********************UPDATED BY NESSREEN AHMED 28/2/2012*******************************************
********************CASE TR.TYPE EQ 'ACLG'
                        CASE (TR.TYPE EQ 'ACLG') OR (TR.TYPE EQ 'AC77')
********************END OF UPDATE 28/2/2012*******************************************************
                            ORD.BNK = R.FT<FT.ORDERING.BANK>
                            CALL F.READ(FN.LD,ORD.BNK[1,12],R.LD,F.LD,LD.ERR)
                            LD.INP = R.LD<LD.INPUTTER><1,1>
                            LD.INP.USR = FIELD(LD.INP, "_", 2)
                            LD.AUTH = R.LD<LD.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = LD.INP.USR
                            R.COUNT<TRANS.AUTHORISER> = LD.AUTH
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** LD.INP.USR = FIELD(LD.INP, "_", 2)
                            CALL F.READ(FN.USR, LD.INP.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
*********UPDATED BY NESSREEN IN 16/6/2008*********************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                        CASE TR.TYPE EQ 'AC21'
                            ORD.BNK = R.FT<FT.ORDERING.BANK>
                            CALL F.READ(FN.BR,ORD.BNK,R.BR,F.BR,BR.ERR)
                            BR.INP = R.BR<EB.BILL.REG.INPUTTER><1,1>
                            BR.INP.USR = FIELD(BR.INP, "_", 2)
                            BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = BR.INP.USR
                            R.COUNT<TRANS.AUTHORISER> = BR.AUTH
***************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** BR.INP.USR = FIELD(BR.INP, "_", 2)
                            CALL F.READ(FN.USR, BR.INP.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
*********UPDATED BY NESSREEN IN 5/12/2008*********************
*********UPDATED BY NESSREEN IN 7/5/2009********************************
                        CASE TR.TYPE EQ 'AC10'
                            DB.REF = R.FT<FT.DEBIT.THEIR.REF>
                            ID.REF = DB.REF[1,2]
                            BEGIN CASE
                            CASE ID.REF = 'TT'
                                CALL F.READ(FN.TT,DB.REF,R.TT,F.TT,TT.ERR)
                                TT.INP.1 = R.TT<TT.TE.INPUTTER><1,1>
                                TT.INP = FIELD(TT.INP.1, "_", 2)
                                TT.AUTH= R.TT<TT.TE.AUTHORISER>
***7/5/2009***                  R.COUNT<TRANS.INPUTTER> = TT.INP
                                R.COUNT<TRANS.AUTHORISER> = TT.AUTH
                                CALL F.READ(FN.USR, TT.INP, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
***7/5/2009***                  R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************UPDATED BY NESSREEN 22/04/2009************************************************
***7/5/2009***                         R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                            CASE ID.REF = 'FT'
                                CALL F.READ(FN.FT,DB.REF,R.FT,F.FT,FT.ERR)
                                FT.INP.1 = R.FT<FT.INPUTTER><1,1>
                                FT.INP = FIELD(FT.INP.1, "_", 2)
                                FT.AUTH =R.FT<FT.AUTHORISER>
***7/5/2009***                  R.COUNT<TRANS.INPUTTER> = FT.INP
***7/5/2009***                  R.COUNT<TRANS.AUTHORISER> = FT.AUTH
                                CALL F.READ(FN.USR, FT.INP, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
***7/5/2009***                  R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************UPDATED BY NESSREEN 22/04/2009************************************************
***7/5/2009***                  R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                            CASE ID.REF = 'TF'
                                CALL F.READ(FN.DR,DB.REF,R.DR,F.DR,DR.ERR)
                                DR.INP.1 = R.DR<TF.DR.INPUTTER><1,1>
                                DR.INP = FIELD(DR.INP.1, "_", 2)
                                DR.AUTH =R.DR<TF.DR.AUTHORISER>
***7/5/2009***                  R.COUNT<TRANS.INPUTTER> = DR.INP
***7/5/2009***                  R.COUNT<TRANS.AUTHORISER> = DR.AUTH
                                CALL F.READ(FN.USR, DR.INP, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
***7/5/2009***                  R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************UPDATED BY NESSREEN 22/04/2009************************************************
***7/5/2009***                  R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                            END CASE
*********************END OF UPDATED*************************************************************
********UPDATED BY NESSREEN AHMED 20/10/2009***************************************************
                        CASE (TR.TYPE EQ 'AC15') OR (TR.TYPE EQ 'AC16')
**TEXT = 'TR.TYPE.CA=':TR.TYPE ; CALL REM
                            DB.REF = R.FT<FT.DEBIT.THEIR.REF>
**TEXT = 'DTR.CA=':DB.REF ; CALL REM
                            ID.REF3 = DB.REF[1,3]
                            ID.REF2 = DB.REF[1,2]
                            IF ID.REF3 EQ 'BOT' THEN
**TEXT = 'BOT.CA' ; CALL REM
                                CALL F.READ(FN.BOT,DB.REF,R.BOT,F.BOT,BOT.ERR)
                                BOT.INP.1 = R.BOT<BOT.INPUTTER><1,1>
**TEXT = 'INP=':BOT.INP.1 ; CALL REM
                                BOT.INP = FIELD(BOT.INP.1, "_", 2)
                                BOT.AUTH = R.BOT<BOT.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = BOT.INP
                                R.COUNT<TRANS.AUTHORISER> = BOT.AUTH
                                BOT.INP.USR = FIELD(BOT.INP.1, "_", 2)
                                CALL F.READ(FN.USR, BOT.INP.USR, R.USR, F.USR, USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                            END ELSE
                                IF ID.REF2 EQ 'BO' THEN
**TEXT = 'BO.CA'  ; CALL REM
                                    CALL F.READ(FN.BO,DB.REF,R.BO,F.BO,BO.ERR)
                                    BO.INP.1 = R.BO<BO.INPUTTER><1,1>
**TEXT = 'BO.INP.CA=':BO.INP.1 ; CALL REM
                                    BO.INP = FIELD(BO.INP.1, "_", 2)
                                    BO.AUTH = R.BO<BO.AUTHORISER>
                                    R.COUNT<TRANS.INPUTTER> = BO.INP
                                    R.COUNT<TRANS.AUTHORISER> = BO.AUTH
                                    BO.INP.USR = FIELD(BO.INP.1, "_", 2)
                                    CALL F.READ(FN.USR, BO.INP.USR, R.USR, F.USR, USR.ERR)
                                    LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                    USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                    R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                    R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                                END
                            END


*************END OF UPDATED 20/10/2009**************************************************************
                        CASE OTHERWISE
****UPDATED BY NESSREN AHMED 11/01/2009*****************************
**    FT.INP = R.FT<FT.INPUTTER><1,1>
                            FT.INP = R.CATEG<AC.CAT.INPUTTER><1,1>
                            FT.INP.USR = FIELD(FT.INP, "_", 2)
                            R.COUNT<TRANS.INPUTTER> = FT.INP.USR
**    R.COUNT<TRANS.AUTHORISER> = R.FT<FT.AUTHORISER>
                            R.COUNT<TRANS.AUTHORISER> = R.CATEG<AC.CAT.AUTHORISER>
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** FT.INP.USR = FIELD(FT.INP, "_", 2)
                            CALL F.READ(FN.USR, FT.INP.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                        END CASE
****************UPDATED BY NESSREEN 15/08/2009*****************************************************
                    CASE SYSTEM.ID.C EQ 'SYS'
                        BS.ID.CE = ''  ;  SYS.REF.CE = ''   ; CE.INP.BS = '' ;  CE.INP.BS.1 = '' ; CE.AUTH.BS.1 = ''
                        SYS.REF.CE = R.CATEG<AC.CAT.TRANS.REFERENCE>
                        BS.ID.CE = SYS.REF.CE[1,2]
** IF (BS.ID.CE EQ 'BS') OR (BS.ID.CE EQ 'BR') THEN
                        CE.INP.BS.1 =  R.CATEG<AC.CAT.INPUTTER><1,1>
                        CE.AUTH.BS.1 = R.CATEG<AC.CAT.AUTHORISER>
                        IF CE.INP.BS.1 = '' AND CE.AUTH.BS.1 # '' THEN
                            CE.INP.BS.1 = CE.AUTH.BS.1
                            CE.INP.BS = FIELD(CE.INP.BS.1, "_", 2)
                            R.COUNT<TRANS.INPUTTER> = CE.INP.BS
                            R.COUNT<TRANS.AUTHORISER> = CE.AUTH.BS.1
                            CALL F.READ(FN.USR, CE.INP.BS, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                        END ELSE
                            IF CE.INP.BS.1 # '' THEN
                                CE.INP.BS.1 =  R.CATEG<AC.CAT.INPUTTER><1,1>
                                CE.INP.BS = FIELD(CE.INP.BS.1, "_", 2)
                                R.COUNT<TRANS.INPUTTER> = CE.INP.BS
                                R.COUNT<TRANS.AUTHORISER> = CE.AUTH.BS.1
                                CALL F.READ(FN.USR, CE.INP.BS, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                            END         ;*END OF IF CE.INP.BS.1 # ''
                        END   ;*END OF ELSE

****************END OF UPDATE 15/08/2009************************************************************
                    CASE OTHERWISE
                        CATEG.INP = R.CATEG<AC.CAT.INPUTTER><1,1>
                        CATEG.INP.USR = FIELD(CATEG.INP, "_", 2)
                        R.COUNT<TRANS.INPUTTER> = CATEG.INP.USR
                        R.COUNT<TRANS.AUTHORISER> = R.CATEG<AC.CAT.AUTHORISER>
********************UPDATED BY NESSREEN 24/08/2008**********************************************
**CATEG.INP.USR = FIELD(CATEG.INP, "_", 2)
                        CALL F.READ(FN.USR, CATEG.INP.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                    END CASE
************************************************************************
                    R.COUNT<TRANS.STMT.NO> = R.CATEG<AC.CAT.STMT.NO>
                    R.COUNT<TRANS.DATE.TIME> = R.CATEG<AC.CAT.DATE.TIME>
                    R.COUNT<TRANS.CRF.PROD.CAT> = R.CATEG<AC.CAT.CRF.PROD.CAT>
                    R.COUNT<TRANS.REF> = R.CATEG.TODAY[Z,Y]
********************UPDATED BY NESSREEN 22/04/2009************************************************
*******UPDATED IN 1/6/2008 BY NESSREEN AHMED**********************
**************R.COUNT<TRANS.COMPANY.CO>  = R.CATEG<AC.CAT.COMPANY.CODE>
******************************************************************
********************END OF UPDATED *****22/04/2009************************************************
                    WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
                        PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
                    END
*                    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)

                END ;***END OF IF TRANS.CODE # 450
            END     ;***END OF ETEXT****
            Z = Z + 22
        NEXT Z

    NEXT I
****************************************************************************************************************
    FN.CON = 'FBNK.CONSOL.ENT.TODAY' ; F.CON = '' ; R.CON = ''

    CALL OPF( FN.CON,F.CON)
    T.SEL = "SELECT FBNK.CONSOL.ENT.TODAY WITH TXN.CODE NE 'ACC'"
**********************************
    R.COUNT = ''
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**PRINT SELECTED
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CON,KEY.LIST<I>, R.CON,F.CON, ETEXT)
**        CALL F.READ(FN.CON,'LD0805600003',R.CON,F.CON, ETEXT)
* ID.NO = KEY.LIST<I>:"*":TODAY
***UPDATED BY NESSREEN AHMED ON 9/12/2010*****************************************
            TRN.DATE = ''
***   ID.NO = "CO*":R.CON<RE.CET.PRODUCT>:"*":R.CON<RE.CET.ACCOUNT.OFFICER>:"*":KEY.LIST<I>:"*":TODAY
            TRN.DATE = R.CON<RE.CET.BOOKING.DATE>
            ID.NO = "CO*":R.CON<RE.CET.PRODUCT>:"*":R.CON<RE.CET.ACCOUNT.OFFICER>:"*":KEY.LIST<I>:"*":TRN.DATE
***END OF UPDATE 9/12/2010*****************************************
**PRINT ID.NO
***UPDATED BY NESSREN AHMED ON 25/6/2008************
            CALL F.READ(FN.COUNT,ID.NO, R.COUNT,F.COUNT, ETEXT)
            IF ETEXT THEN
*********
                R.COUNT<TRANS.TRANSACTION.CODE> = R.CON<RE.CET.TXN.CODE> : ".":R.CON<RE.CET.TYPE>
                IF R.CON<RE.CET.LOCAL.DR> LT 0 THEN
                    R.COUNT<TRANS.AMOUNT.LCY> = R.CON<RE.CET.LOCAL.DR>
                END
                IF R.CON<RE.CET.LOCAL.CR> GT 0 THEN
                    R.COUNT<TRANS.AMOUNT.LCY> = R.CON<RE.CET.LOCAL.CR>
                END
                IF R.CON<RE.CET.FOREIGN.DR> LT 0 THEN
                    R.COUNT<TRANS.AMOUNT.FCY> = R.CON<RE.CET.FOREIGN.DR>
                END
                IF R.CON<RE.CET.FOREIGN.CR> GT 0 THEN
*****UPDATED BY NESSREEN AHMED ON 24/06/2008**********************
**  R.COUNT<TRANS.AMOUNT.FCY> = R.CON<RE.CET.LOCAL.CR>
                    R.COUNT<TRANS.AMOUNT.FCY> = R.CON<RE.CET.FOREIGN.CR>
*****END OF UPDATED************************************************
                END
                R.COUNT<TRANS.CUSTOMER.ID> = R.CON<RE.CET.CUSTOMER>
                R.COUNT<TRANS.ACCOUNT.OFFICER> = R.CON<RE.CET.ACCOUNT.OFFICER>
                R.COUNT<TRANS.PRODUCT.CATEGORY> = R.CON<RE.CET.PRODUCT.CATEGORY>
                R.COUNT<TRANS.VALUE.DATE> = R.CON<RE.CET.VALUE.DATE>
                R.COUNT<TRANS.CURRENCY> = R.CON<RE.CET.CURRENCY>
                R.COUNT<TRANS.BOOKING.DATE> = R.CON<RE.CET.BOOKING.DATE>
                R.COUNT<TRANS.EXCHANGE.RATE> = R.CON<RE.CET.EXCHANGE.RATE>
                R.COUNT<TRANS.OUR.REFERENCE> = R.CON<RE.CET.TXN.REF>
                R.COUNT<TRANS.SYSTEM.ID> = R.CON<RE.CET.PRODUCT>
                R.COUNT<TRANS.REF> = KEY.LIST<I>
******UPDATED ON 1/6/2008 BY NESSREEN****************************
                FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ;  E1 = ''
                FN.LDH = 'F.LD.LOANS.AND.DEPOSITS$HIS' ; F.LDH = '' ; R.LDH = '' ;  EH1 = ''
                FN.LC = 'F.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = '' ; E2 = ''
                FN.DR = 'F.DRAWINGS' ; F.DR = '' ; R.DR = '' ; E3 = ''
                PROD = R.CON<RE.CET.PRODUCT>
                IF PROD EQ 'LD' THEN
                    CALL OPF(FN.LD,F.LD)
                    CALL F.READ(FN.LD,R.CON<RE.CET.TXN.REF>,R.LD,F.LD,E1)
                    IF NOT(E1) THEN

                        INP = R.LD<LD.INPUTTER>
*****UPDATED BY NESSREEN AHMED ON 03/01/2009******************************
*** DD = DCOUNT(INP,VM)
*** INP.LD = INP<1,DD>
                        INP.LD = INP<1,1>
******************************************************************
                        INP.LD.USR = FIELD(INP.LD, "_", 2)
                        AUTH.LD = R.LD<LD.AUTHORISER>
                        COMP.LD = R.LD<LD.CO.CODE>
                        R.COUNT<TRANS.INPUTTER> = INP.LD.USR
                        R.COUNT<TRANS.AUTHORISER> = AUTH.LD
                        R.COUNT<TRANS.COMPANY.CO> = COMP.LD
********************UPDATED BY NESSREEN 24/08/2008**********************************************
                        INP.LD.USR = FIELD(INP.LD, "_", 2)
                        CALL F.READ(FN.USR, INP.LD.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                    END ELSE
                        CALL OPF(FN.LDH,F.LDH)
                        ID.LDH1 = R.CON<RE.CET.TXN.REF>:';1'
                        ID.LDH2 = R.CON<RE.CET.TXN.REF>:';2'
                        ID.LDH3 = R.CON<RE.CET.TXN.REF>:';3'
                        ID.LDH4 = R.CON<RE.CET.TXN.REF>:';4'
                        ID.LDH5 = R.CON<RE.CET.TXN.REF>:';5'
                        CALL F.READ(FN.LD,ID.LDH5,R.LD,F.LD,EH5)
                        IF EH5 THEN
                            CALL F.READ(FN.LDH,ID.LDH4,R.LDH,F.LDH,EH4)
                            IF EH4 THEN
                                CALL F.READ(FN.LDH,ID.LDH3,R.LDH,F.LDH,EH3)
                                IF EH3 THEN
                                    CALL F.READ(FN.LDH,ID.LDH2,R.LDH,F.LDH,EH2)
                                    IF EH2 THEN
                                        CALL F.READ(FN.LDH,ID.LDH1,R.LDH,F.LDH,EH1)
                                    END ELSE
                                        INP = R.LDH<LD.INPUTTER>
*Line [ 966 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                        DD = DCOUNT(INP,@VM)
                                        INP.LD = INP<1,DD>
                                        INP.LD.USR = FIELD(INP.LD, "_", 2)
                                        AUTH.LD = R.LDH<LD.AUTHORISER>
                                        COMP.LD = R.LDH<LD.CO.CODE>
********************UPDATED BY NESSREEN 17/02/2009**********************************************
**   R.COUNT<TRANS.INPUTTER> = INP.LD
                                        R.COUNT<TRANS.INPUTTER> = INP.LD.USR
*******************END OF UPDATED***************************************************************
                                        R.COUNT<TRANS.AUTHORISER> = AUTH.LD
********************UPDATED BY NESSREEN 22/04/2009************************************************
*************************************** R.COUNT<TRANS.COMPANY.CO> = COMP.LD
********************END OF UPDATED *****22/04/2009************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** INP.LD.USR = FIELD(INP.LD, "_", 2)
                                        CALL F.READ(FN.USR, INP.LD.USR, R.USR, F.USR,USR.ERR)
                                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                                    END ;*END OF EH2
                                END ELSE
                                    INP = R.LDH<LD.INPUTTER>
*Line [ 993 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                    DD = DCOUNT(INP,@VM)
                                    INP.LD = INP<1,DD>
                                    INP.LD.USR = FIELD(INP.LD, "_", 2)
                                    AUTH.LD = R.LDH<LD.AUTHORISER>
                                    COMP.LD = R.LDH<LD.CO.CODE>
                                    R.COUNT<TRANS.INPUTTER> = INP.LD.USR
                                    R.COUNT<TRANS.AUTHORISER> = AUTH.LD
********************UPDATED BY NESSREEN 22/04/2009************************************************
**********************************  R.COUNT<TRANS.COMPANY.CO> = COMP.LD
********************END OF UPDATED *****22/04/2009************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** INP.LD.USR = FIELD(INP.LD, "_", 2)
                                    CALL F.READ(FN.USR, INP.LD.USR, R.USR, F.USR,USR.ERR)
                                    LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                    USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                    R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                                    R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                                END     ;*END OF EH3
                            END ELSE
                                INP = R.LDH<LD.INPUTTER>
*Line [ 1017 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                DD = DCOUNT(INP,@VM)
                                INP.LD = INP<1,DD>
                                INP.LD.USR = FIELD(INP.LD, "_", 2)
                                AUTH.LD = R.LDH<LD.AUTHORISER>
                                COMP.LD = R.LDH<LD.CO.CODE>
                                R.COUNT<TRANS.INPUTTER> = INP.LD.USR
                                R.COUNT<TRANS.AUTHORISER> = AUTH.LD
********************UPDATED BY NESSREEN 24/08/2008**********************************************
********************************* R.COUNT<TRANS.COMPANY.CO> = COMP.LD
********************END OF UPDATED *****22/04/2009************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
**  INP.LD.USR = FIELD(INP.LD, "_", 2)
                                CALL F.READ(FN.USR, INP.LD.USR, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                                R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                            END         ;* END OF EH4
                        END ELSE
                            INP = R.LDH<LD.INPUTTER>
*Line [ 1041 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DD = DCOUNT(INP,@VM)
                            INP.LD = INP<1,DD>
                            INP.LD.USR = FIELD(INP.LD, "_", 2)
                            AUTH.LD = R.LDH<LD.AUTHORISER>
                            COMP.LD = R.LDH<LD.CO.CODE>
                            R.COUNT<TRANS.INPUTTER> = INP.LD.USR
                            R.COUNT<TRANS.AUTHORISER> = AUTH.LD
********************UPDATED BY NESSREEN 22/04/2009************************************************
**************************** R.COUNT<TRANS.COMPANY.CO> = COMP.LD
********************END OF UPDATED *****22/04/2009************************************************
**INP.LD.USR = FIELD(INP.LD, "_", 2)
                            CALL F.READ(FN.USR, INP.LD.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                        END   ;*END OF EH5
                    END
********UPDATED BY NESSREEN AHMED ON 04/01/2009*************
                    R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
************************************************************
                END
****************************END OF UPDATED IN 20/07/2008*****************
                IF PROD EQ 'LCD' THEN
                    CALL OPF(FN.DR,F.DR)
                    CALL F.READ(FN.DR,R.CON<RE.CET.TXN.REF>,R.DR,F.DR,E3)
****UPDATED BY NESSREEN AHMED 29/6/2010*******************************************************
                    IF NOT(E3) THEN
****END 29/6/2010********************************************************
                        INP.DR = R.DR<TF.DR.INPUTTER><1,1>
                        INP.DR.USR = FIELD(INP.DR, "_", 2)
                        AUTH.DR = R.DR<TF.DR.AUTHORISER>
                        COMP.DR = R.DR<TF.DR.CO.CODE>
                        R.COUNT<TRANS.INPUTTER> =  INP.DR.USR
                        R.COUNT<TRANS.AUTHORISER> = AUTH.DR
********************UPDATED BY NESSREEN 22/04/2009************************************************
********************R.COUNT<TRANS.COMPANY.CO> = COMP.DR
*******************END OF UPDATED *****22/04/2009************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** INP.DR.USR = FIELD(INP.DR, "_", 2)
                        CALL F.READ(FN.USR, INP.DR.USR, R.USR, F.USR,USR.ERR)
                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
********************UPDATED BY NESSREEN 15/01/2009************************
                        R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
******************************************************************************
********************UPDATED BY NESSREEN 22/04/2009************************************************
                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                    END ELSE
*******************UPDATED BY NESSREEN 29/6/2010************************
                        CALL OPF(FN.DRH,F.DRH)
                        ID.DRH1 = R.CON<RE.CET.TXN.REF>:';1'
                        ID.DRH2 = R.CON<RE.CET.TXN.REF>:';2'
                        ID.DRH3 = R.CON<RE.CET.TXN.REF>:';3'
                        ID.DRH4 = R.CON<RE.CET.TXN.REF>:';4'
                        ID.DRH5 = R.CON<RE.CET.TXN.REF>:';5'
                        CALL F.READ(FN.DRH,ID.DRH5,R.DRH,F.DRH,EDRH5)
                        IF EDRH5 THEN
                            CALL F.READ(FN.DRH,ID.DRH4,R.DRH,F.DRH,EDRH4)
                            IF EDRH4 THEN
                                CALL F.READ(FN.DRH,ID.DRH3,R.DRH,F.DRH,EDRH3)
                                IF EDRH3 THEN
                                    CALL F.READ(FN.DRH,ID.DRH2,R.DRH,F.DRH,EDRH2)
                                    IF EDRH2 THEN
                                        CALL F.READ(FN.DRH,ID.DRH1,R.DRH,F.DRH,EDRH1)
                                    END ELSE      ;* ELSE OF EDRH2
                                        INP.DR = R.DRH<TF.DR.INPUTTER><1,1>
                                        INP.DR.USR = FIELD(INP.DR, "_", 2)
                                        AUTH.DR = R.DR<TF.DR.AUTHORISER>
                                        COMP.DR = R.DR<TF.DR.CO.CODE>
                                        R.COUNT<TRANS.INPUTTER> =  INP.DR.USR
                                        R.COUNT<TRANS.AUTHORISER> = AUTH.DR
                                        CALL F.READ(FN.USR, INP.DR.USR, R.USR, F.USR,USR.ERR)
                                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                        R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                                    END ;*END OF EDRH2
                                END ELSE          ;* ELSE OF EDRH3
                                    INP.DR = R.DRH<TF.DR.INPUTTER><1,1>
                                    INP.DR.USR = FIELD(INP.DR, "_", 2)
                                    AUTH.DR = R.DR<TF.DR.AUTHORISER>
                                    COMP.DR = R.DR<TF.DR.CO.CODE>
                                    R.COUNT<TRANS.INPUTTER> =  INP.DR.USR
                                    R.COUNT<TRANS.AUTHORISER> = AUTH.DR
                                    CALL F.READ(FN.USR, INP.DR.USR, R.USR, F.USR,USR.ERR)
                                    LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                    USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                    R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                    R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                    R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                                END     ;* END OF EDRH3
                            END ELSE    ;* ELSE OF EDRH4
                                INP.DR = R.DRH<TF.DR.INPUTTER><1,1>
                                INP.DR.USR = FIELD(INP.DR, "_", 2)
                                AUTH.DR = R.DR<TF.DR.AUTHORISER>
                                COMP.DR = R.DR<TF.DR.CO.CODE>
                                R.COUNT<TRANS.INPUTTER> =  INP.DR.USR
                                R.COUNT<TRANS.AUTHORISER> = AUTH.DR
                                CALL F.READ(FN.USR, INP.DR.USR, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                            END         ;* END OF EDRH4
                        END ELSE        ;* ELSE OF EDRH5
                            INP.DR = R.DRH<TF.DR.INPUTTER><1,1>
                            INP.DR.USR = FIELD(INP.DR, "_", 2)
                            AUTH.DR = R.DR<TF.DR.AUTHORISER>
                            COMP.DR = R.DR<TF.DR.CO.CODE>
                            R.COUNT<TRANS.INPUTTER> =  INP.DR.USR
                            R.COUNT<TRANS.AUTHORISER> = AUTH.DR
                            CALL F.READ(FN.USR, INP.DR.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                            R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                        END   ;* END OF EDRH5
                    END       ;*END OF IF NOT(E3)
                END ;*END OF IF PROD EQ 'LCD'
                IF PROD EQ 'LCM' THEN
                    CALL OPF(FN.LC,F.LC)
                    CALL F.READ(FN.LC,R.CON<RE.CET.TXN.REF>,R.LC,F.LC,E2)
**************UPDATED BY NESSREEN 29/6/2010******************************
                    IF NOT(E2) THEN
                        INP.LC = R.LC<TF.LC.INPUTTER><1,1>
                        INP.LC.USR = FIELD(INP.LC, "_", 2)
                        AUTH.LC = R.LC<TF.LC.AUTHORISER>
                        COMP.LC = R.LC<TF.LC.CO.CODE>
********************UPDATED BY NESSREEN 15/08/2009***********************************************
                        D.T.LC = '' ; D.LC = ''
                        D.T.LC = R.LC<TF.LC.DATE.TIME>
                        D.LC = D.T.LC[1,6]
                        IF DATX = D.LC THEN
**************************************************************************************************
                            R.COUNT<TRANS.INPUTTER> = INP.LC.USR
                            R.COUNT<TRANS.AUTHORISER> = AUTH.LC
********************UPDATED BY NESSREEN 22/04/2009************************************************
*****************      R.COUNT<TRANS.COMPANY.CO> = COMP.LC
********************END OF UPDATED *****22/04/2009************************************************
********************UPDATED BY NESSREEN 24/08/2008**********************************************
** INP.LC.USR = FIELD(INP.LC, "_", 2)
                            CALL F.READ(FN.USR, INP.LC.USR, R.USR, F.USR,USR.ERR)
                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
********************END OF UPDATED *************************************************************
**************UPDATED BY NESSREEN 15/01/2009****************************************************
                            R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
********************************************************************************************
*********************UPDATED BY NESSREEN 22/04/2009************************************************
                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
********************END OF UPDATED *****22/04/2009************************************************
                        END   ;**END OF IF DATX = D.LC *******
**************UPDATED BY NESSREEN 29/6/2010************************************************
                    END ELSE
                        CALL OPF(FN.LCH,F.LCH)
                        ID.LCH1 = R.CON<RE.CET.TXN.REF>:';1'
                        ID.LCH2 = R.CON<RE.CET.TXN.REF>:';2'
                        ID.LCH3 = R.CON<RE.CET.TXN.REF>:';3'
                        ID.LCH4 = R.CON<RE.CET.TXN.REF>:';4'
                        ID.LCH5 = R.CON<RE.CET.TXN.REF>:';5'
                        CALL F.READ(FN.LCH,ID.LCH5,R.LCH,F.LCH,ELCH5)
                        IF ELCH5 THEN
                            CALL F.READ(FN.LCH,ID.LCH4,R.LCH,F.LCH,ELCH4)
                            IF ELCH4 THEN
                                CALL F.READ(FN.LCH,ID.LCH3,R.LCH,F.LCH,ELCH3)
                                IF ELCH3 THEN
                                    CALL F.READ(FN.LCH,ID.LCH2,R.LCH,F.LCH,ELCH2)
                                    IF ELCH2 THEN
                                        CALL F.READ(FN.LCH,ID.LCH1,R.LCH,F.LCH,ELCH1)
                                    END ELSE      ;* ELSE OF ELCH2
                                        INP.LC = R.LCH<TF.LC.INPUTTER><1,1>
                                        INP.LC.USR = FIELD(INP.LC, "_", 2)
                                        AUTH.LC = R.LC<TF.LC.AUTHORISER>
                                        COMP.LC = R.LC<TF.LC.CO.CODE>
                                        D.T.LC = '' ; D.LC = ''
                                        D.T.LC = R.LC<TF.LC.DATE.TIME>
                                        D.LC = D.T.LC[1,6]
                                        IF DATX = D.LC THEN
                                            R.COUNT<TRANS.INPUTTER> = INP.LC.USR
                                            R.COUNT<TRANS.AUTHORISER> = AUTH.LC
                                            CALL F.READ(FN.USR, INP.LC.USR, R.USR, F.USR,USR.ERR)
                                            LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                            USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                            R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                            R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                            R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                                        END       ;**END OF IF DATX = D.LC *******
                                    END ;* END OF ELCH2
                                END ELSE          ;* ELSE OF ELCH3
                                    INP.LC = R.LCH<TF.LC.INPUTTER><1,1>
                                    INP.LC.USR = FIELD(INP.LC, "_", 2)
                                    AUTH.LC = R.LC<TF.LC.AUTHORISER>
                                    COMP.LC = R.LC<TF.LC.CO.CODE>
                                    D.T.LC = '' ; D.LC = ''
                                    D.T.LC = R.LC<TF.LC.DATE.TIME>
                                    D.LC = D.T.LC[1,6]
                                    IF DATX = D.LC THEN
                                        R.COUNT<TRANS.INPUTTER> = INP.LC.USR
                                        R.COUNT<TRANS.AUTHORISER> = AUTH.LC
                                        CALL F.READ(FN.USR, INP.LC.USR, R.USR, F.USR,USR.ERR)
                                        LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                        USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                        R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                        R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                        R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                                    END ;**END OF IF DATX = D.LC *******
                                END     ;* END OF ELCH3
                            END ELSE    ;* ELSE OF ELCH4
                                INP.LC = R.LCH<TF.LC.INPUTTER><1,1>
                                INP.LC.USR = FIELD(INP.LC, "_", 2)
                                AUTH.LC = R.LC<TF.LC.AUTHORISER>
                                COMP.LC = R.LC<TF.LC.CO.CODE>
                                D.T.LC = '' ; D.LC = ''
                                D.T.LC = R.LC<TF.LC.DATE.TIME>
                                D.LC = D.T.LC[1,6]
                                IF DATX = D.LC THEN
                                    R.COUNT<TRANS.INPUTTER> = INP.LC.USR
                                    R.COUNT<TRANS.AUTHORISER> = AUTH.LC
                                    CALL F.READ(FN.USR, INP.LC.USR, R.USR, F.USR,USR.ERR)
                                    LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                    USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                    R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                    R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                    R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                                END     ;**END OF IF DATX = D.LC *******
                            END         ;* END OF ELCH4
                        END ELSE        ;* ELSE OF ELCH5
                            INP.LC = R.LCH<TF.LC.INPUTTER><1,1>
                            INP.LC.USR = FIELD(INP.LC, "_", 2)
                            AUTH.LC = R.LC<TF.LC.AUTHORISER>
                            COMP.LC = R.LC<TF.LC.CO.CODE>
                            D.T.LC = '' ; D.LC = ''
                            D.T.LC = R.LC<TF.LC.DATE.TIME>
                            D.LC = D.T.LC[1,6]
                            IF DATX = D.LC THEN
                                R.COUNT<TRANS.INPUTTER> = INP.LC.USR
                                R.COUNT<TRANS.AUTHORISER> = AUTH.LC
                                CALL F.READ(FN.USR, INP.LC.USR, R.USR, F.USR,USR.ERR)
                                LOCAL.REF = R.USR<EB.USE.LOCAL.REF>
                                USR.DEPT = LOCAL.REF<1,USER.SCB.DEPT.CODE>
                                R.COUNT<TRANS.USER.DEPT.CODE> = USR.DEPT
                                R.COUNT<TRANS.DATE.TIME> = DATX.TIMX
                                R.COUNT<TRANS.COMPANY.CO> = R.USR<EB.USE.COMPANY.CODE><1,1>
                            END         ;**END OF IF DATX = D.LC *******
                        END   ;* END OF ELCH5
                    END       ;* END OF IF NOT(E2)
                END ;* END OF PROD EQ 'LCM'
*****************************************************
                WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
                    PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
                END
*                CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
            END     ;***OF NOT(ETEXT)
        NEXT I
    END

*************************************************************
    RETURN
END
