* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*******************************NI7OOOOOO*************
*-----------------------------------------------------------------------------
* <Rating>880</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.MOADEL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    IF COMI THEN
        FN.CUR  = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''
        CALL OPF(FN.CUR,F.CUR)

        IF R.NEW(DEPT.SAMP.AMT.TF2) NE '' THEN
            IF COMI NE 'EGP' THEN
                CUR.COD = COMI
            END

            CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
            CASH.C = '10'
*Line [ 46 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
            CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
            IF CUR.COD EQ 'JPY' THEN
                CUR.RATE = CUR.RATE / 100
            END
            AMT.XX = R.NEW(DEPT.SAMP.AMT.TF2) * CUR.RATE
            R.NEW(DEPT.SAMP.AMT.LOC.TF2) = AMT.XX
        END

        IF R.NEW(DEPT.SAMP.AMT.HWALA) NE '' OR R.NEW(DEPT.SAMP.AMT.WH) NE '' THEN
            IF COMI NE 'EGP' THEN
                CUR.COD = COMI
            END

            CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
            CASH.C = '10'
*Line [ 63 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
            CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
            IF CUR.COD EQ 'JPY' THEN
                CUR.RATE = CUR.RATE / 100
            END
            AMT.XX1 = R.NEW(DEPT.SAMP.AMT.HWALA) * CUR.RATE
            AMT.XX1 = R.NEW(DEPT.SAMP.AMT.WH)    * CUR.RATE
            R.NEW(DEPT.SAMP.AMT.LOC.TF2) = AMT.XX1
        END


        IF R.NEW(DEPT.SAMP.AMT.LG11) NE '' THEN
            IF COMI NE 'EGP' THEN
                CUR.COD = COMI
            END

            CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
            CASH.C = '10'
*Line [ 82 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
            CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
            IF CUR.COD EQ 'JPY' THEN
                CUR.RATE = CUR.RATE / 100
            END
            AMT.XX2 = R.NEW(DEPT.SAMP.AMT.LG11) * CUR.RATE
            R.NEW(DEPT.SAMP.AMT.LOC.TF2) = AMT.XX2
        END

        IF R.NEW(DEPT.SAMP.AMT.BR) NE '' THEN
            IF COMI NE 'EGP' THEN
                CUR.COD = COMI
            END

            CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
            CASH.C = '10'
*Line [ 99 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
            CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
            IF CUR.COD EQ 'JPY' THEN
                CUR.RATE = CUR.RATE / 100
            END
            AMT.XX3 = R.NEW(DEPT.SAMP.AMT.BR) * CUR.RATE
            R.NEW(DEPT.SAMP.AMT.LOC.TF2) = AMT.XX3
        END

        IF R.NEW(DEPT.SAMP.AMT.TEL) NE '' THEN
            IF COMI NE 'EGP' THEN
                CUR.COD = COMI
            END

            CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
            CASH.C = '10'
*Line [ 116 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS ELSE NULL
            CUR.RATE    = R.CUR<EB.CUR.SELL.RATE,POS>
            IF CUR.COD EQ 'JPY' THEN
                CUR.RATE = CUR.RATE / 100
            END
            AMT.XX3 = R.NEW(DEPT.SAMP.AMT.TEL) * CUR.RATE
            R.NEW(DEPT.SAMP.AMT.LOC.TF2) = AMT.XX3
        END
    END
    RETURN
END
