* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*****NESSREEN AHMED ON 27/01/2009******************
*---------------------------------- -------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.TT.FCY.VAL.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*TO CHECK IF THE CURRENCY IS FORIGN AND THE FORIGN AMOUNT IS GT 100000 THEN
*IT WILL DEFAULT THE VALUE DATE BY 3W FROM TODAY'S DATE AND ASK THE USER
*IF THE CUSTOMER WILL MAKE A DEPOSIT THEN IT WILL DEFAULT VALUE DATE BY
*2W FROM TODAY'S DATE
*********************
    ETEXT  = ''   ; NN = '' ; YY = '' ; MIDRT.E = '' ;  MIDRT = ''
    E      = ''   ; EQV.CURR = '' ; EQU.EGP = '' ; MID.RATE = '' ; CLASS = '0'
****UPDATED BY NESSREEN AHMED 05/02/2009 ********************************
    IF MESSAGE = '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1) THEN
            ETEXT = '��� ����� ������' ; CALL STORE.END.ERROR
            R.NEW(TT.TE.AMOUNT.FCY.1) = COMI
            R.NEW (TT.TE.VALUE.DATE.1)= ''
*****UPDATED BY NESSREEN AHMED 21/2/2012*******************
            R.NEW (TT.TE.EXPOSURE.DATE.1) = ''
*****END OF UPDATE 21/2/2012*******************************
        END
*************************************************************************
        MYDATE = TODAY
        FN.CURR = 'F.CURRENCY' ; F.CURR = '' ; R.CURR = '' ; E1 = ''
        CALL OPF(FN.CURR,F.CURR)

        CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR,R.NEW(TT.TE.CUSTOMER.1),SEC)
******UPDATED BY NESSREEN AHMED 17/6/2013***************************************
        ACCT = R.NEW(TT.TE.ACCOUNT.1)
        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACCT ,CATEG)
        IF (CATEG NE '6511' AND CATEG NE '6512') THEN
******END OF UPDATE 17/6/2013*******************************
            IF SEC EQ 1100 OR SEC EQ 1200 OR SEC EQ 1300 OR SEC EQ 1400 THEN CLASS = '1'
            IF R.NEW(TT.TE.CURRENCY.1) NE LCCY AND CLASS = '0'  THEN
*UPDATED BY NESSREEN AHMED 12/3/2014**********************************************************
*IF R.NEW(TT.TE.CURRENCY.1) EQ 'USD' THEN
                IF R.NEW(TT.TE.TRANSACTION.CODE) # '52' THEN
                    *TEXT = 'NOT52' ; CALL REM
                    CALL CDT('EG00', MYDATE, '3W')
                    R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
*****UPDATED BY NESSREEN AHMED 21/2/2012******************************
                    R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
*****END OF UPDATE 21/2/2012******************************************
                END ELSE
                    *TEXT = 'EQ52' ; CALL REM
                    CALL CDT('EG00', MYDATE, '2W')
                    R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
                    R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
                END
*               END ELSE
*                   CURR.FCY = R.NEW(TT.TE.CURRENCY.1)
*                   CALL F.READ(FN.CURR, CURR.FCY , R.CURR, F.CURR, E1)
*                   MID.R.T.E = R.CURR<EB.CUR.MID.REVAL.RATE>
*                   CURR.MK.E = R.CURR<EB.CUR.CURRENCY.MARKET>
*                   LOCATE '10' IN R.CURR<EB.CUR.CURRENCY.MARKET,1> SETTING NN THEN
*                       MIDRT.E = R.CURR<EB.CUR.MID.REVAL.RATE,NN>
*                   END
*                   CALL F.READ(FN.CURR,  'USD', R.CURR, F.CURR, E1)
*                   MID.R.T = R.CURR<EB.CUR.MID.REVAL.RATE>
*                   CURR.MK = R.CURR<EB.CUR.CURRENCY.MARKET>
*                   LOCATE '10' IN R.CURR<EB.CUR.CURRENCY.MARKET,1> SETTING YY THEN
*                       MIDRT = R.CURR<EB.CUR.MID.REVAL.RATE,YY>
*                   END
*                   IF CURR.FCY  NE 'JPY' THEN
*                       MID.RATE =  MIDRT.E
*                       EQU.EGP = SMUL(COMI, MID.RATE)
*                       CALL EB.ROUND.AMOUNT ('CURR.FCY',EQU.EGP,'',"2")
*                   END ELSE
*                       MID.RATE = MIDRT.E / 100
*                       EQU.EGP = SMUL(COMI, MID.RATE)
*                       CALL EB.ROUND.AMOUNT ('CURR.FCY',EQU.EGP,'',"2")
*                   END       ;* CURR.FCY  NE 'JPY'
*                   EQV.CURR = SDIV(EQU.EGP, MIDRT)
*                   CALL EB.ROUND.AMOUNT ('CURR.FCY',EQV.CURR,'',"2")
*                   IF EQV.CURR GT 100000 THEN
*                       MYDATE = TODAY
*                       CALL CDT('EG00', MYDATE, '3W')
*                       R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
*                       R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
*                   END ELSE
*                       MYDATE = TODAY
*                       CALL CDT('EG00', MYDATE, '2W')
*                       R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
*                       R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
*                   END
*               END ;* IF R.NEW(TT.TE.CURRENCY.1) EQ 'USD'
*end of update 12/3/2014******************************************************************************
            END     ;* IF CURRENCY NE LCCY
        END         ;* End of if (CATEG NE 6511 OR CATEG NE 6512)
        CALL REBUILD.SCREEN
    END   ;* IF MESSAGE = ''

    RETURN
END
