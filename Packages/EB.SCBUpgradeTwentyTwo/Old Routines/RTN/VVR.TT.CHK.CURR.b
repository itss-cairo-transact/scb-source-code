* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.CURR

*1- TO CHECK IF CURRENCY IS LOCAL MAKE AMOUNT FCY FILED NOINPUT
*2- TO CHECK IF CURRENCY IS FORIEGN MAKE AMOUNT LCY FILED NOINPUT

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

**********************************************************************************************
    IF COMI NE "EGP" AND COMI NE "SAR" AND COMI NE "USD" AND COMI NE "KWD" AND COMI NE "GBP" AND COMI NE "EUR" AND COMI NE "JPY" AND COMI NE "CHF" AND COMI NE "CAD" AND COMI NE "AUD" AND COMI NE "NOK" AND COMI NE "SEK" AND COMI NE "DKK" THEN
        ETEXT = '����� ������� ��� ��� ������'
    END ELSE

        IF COMI = 'EGP' THEN
**      R.NEW(TT.TE.AMOUNT.FCY.1) = ''
**    T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
            T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'
        END
    END
    RETURN

END
