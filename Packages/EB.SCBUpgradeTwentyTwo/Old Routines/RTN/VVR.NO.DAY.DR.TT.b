* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.NO.DAY.DR.TT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*----------------------------------------------------
    IF R.NEW(TT.TE.ACCOUNT.1)[1,2] NE 'PL' THEN
        ACCT=  R.NEW(TT.TE.ACCOUNT.1)
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT,CATT)

        IF COMI = '' THEN
            IF CATT EQ "1230" OR CATT EQ "1595" OR CATT EQ "1596" OR CATT EQ "1598" OR CATT EQ "1585" OR CATT EQ "1587" OR CATT EQ "1512" OR CATT EQ "1514" OR CATT EQ "1524" OR CATT EQ "1404" OR CATT EQ "1438" OR CATT EQ "1589" THEN
                ETEXT = " ��� ������� " ;CALL STORE.END.ERROR
            END
        END ELSE
            IF CATT NE '1230' AND CATT NE '1595' AND CATT NE '1596' AND CATT NE '1598' AND CATT NE '1585' AND CATT NE '1587' AND CATT NE '1512' AND CATT NE '1514' AND CATT NE '1524' AND CATT NE '1508' AND CATT NE '1502' AND CATT NE '1567' AND CATT NE '1527' AND CATT NE '1528' AND CATT NE '1404' AND CATT NE "1438" AND CATT NE '1589' THEN
                ETEXT = 'WRONG CATEGORY';CALL STORE.END.ERROR
            END
        END
    END
************************************************************
    RETURN
END
