* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
**---------INGY 28/12/2003---------**

*A ROUTINE TO CHECK IF ACCOUNT .2 NE A VALID INTRNAL ACCOUNT THEN DISPLAY ERROR MESSAGE
*A ROUTINE TO  EMPTY FIELDS CURRENCY.1 AND AMOUNT.LOCAL.LOCAL.1 AND AMOUNT.FCY.1 IF ACCOUNT.2 IS CHANGED
* MAKE AMOUNT.LOCAL.1 INPUT IF IT IS LOCAL CURRENCY & MAKE IT NOINPUT IF IT IS FOREIGN CURRENCY
* MAKE AMOUNT.FCY.1 INPUT IF IT IS FOREIGN CURRENCY & MAKE IT NOINPUT IF IT IS LOCAL CURRENCY

    SUBROUTINE VVR.TT.ACCT.2.WITHDRAW

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


    IF COMI # R.NEW(TT.TE.ACCOUNT.1)<1,AV> THEN

        R.NEW (TT.TE.ACCOUNT.1)<1,AV> = ''
        R.NEW (TT.TE.CURRENCY.2) = ''
        R.NEW (TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
* R.NEW (TT.TE.AMOUNT.LOCAL.2) = ''
        R.NEW (TT.TE.AMOUNT.FCY.1)<1,AV> = ''
* R.NEW (TT.TE.AMOUNT.FCY.2) = ''
        R.NEW (TT.TE.NARRATIVE.2)<1,1> = ''

    END

    CATEGORY = ''
    IF NOT(NUM(COMI)) THEN

        CATEGORY = COMI[4,5]
        AC.BRANCH= COMI[9,2]
        AC.BRANCH = TRIM( AC.BRANCH, '0', 'L')    ;* remove leading zeros
    END ELSE
        CATEGORY = COMI[11,4]
        CURR = COMI[9,2]
        AC.BRANCH = COMI[1,2]
        AC.BRANCH = TRIM( AC.BRANCH, '0', 'L')
    END
    CALL DBR ('NUMERIC.CURRENCY':@FM:1,CURR,CUR)
    CALL DBR ('CATEGORY':@FM:1,CATEGORY,CATE)
    CALL DBR('ACCOUNT':@FM:3,COMI,TITLE)
*****UPDATED BY NESSREEN AHMED 18/06/2009*********************************************
**IF PGM.VERSION = ",SCB.WITHDRAW.FR.INT" THEN
    IF PGM.VERSION = ",SCB.WITHDRAW.FR.INT" OR PGM.VERSION = ",SCB.WITHDRAW.FR.INA" THEN
******************************************************************************************
        IF NUM(COMI[1,3]) THEN
            ETEXT='������ ������ ���'
        END   ELSE
            R.NEW(TT.TE.NARRATIVE.2)<1,1> = COMI[1,3]:'-':CATE:'-':TITLE
            R.NEW(TT.TE.CURRENCY.2) = COMI[1,3]
        END

    END
*****UPDATED BY NESSREEN AHMED 18/06/2009*********************************************
** IF PGM.VERSION = ",SCB.WITHDRAW.FR.BNK" THEN
    IF PGM.VERSION = ",SCB.WITHDRAW.FR.BNK" OR PGM.VERSION = ",SCB.WITHDRAW.FR.BN" THEN
****************************************************************************************
        IF CATEGORY LT 5000 OR CATEGORY GT 5999 THEN
*            ETEXT = 'MUST.BE.NOSTRO.ACCOUNT'
            ETEXT='������ ������ ���'
        END   ELSE
            R.NEW(TT.TE.CURRENCY.2)= CUR
            R.NEW(TT.TE.NARRATIVE.2)<1,1> = CUR:'-':CATE:'-':TITLE
        END
    END
*IF AC.BRANCH # R.USER<EB.USE.DEPARTMENT.CODE> THEN
*   ETEXT = 'MUST.BE.SAME.DEPARTMENT'
* END    ELSE
    IF CATEGORY = '10000' THEN
        R.NEW (TT.TE.ACCOUNT.1)<1,AV> = ''
        R.NEW (TT.TE.CURRENCY.2) = ''
        R.NEW (TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
* R.NEW (TT.TE.AMOUNT.LOCAL.2) = ''
        R.NEW (TT.TE.AMOUNT.FCY.1)<1,AV> = ''
* R.NEW (TT.TE.AMOUNT.FCY.2) = ''
        R.NEW (TT.TE.NARRATIVE.2)<1,1> = ''
        CALL REBUILD.SCREEN
*        ETEXT = 'THIS.CATEGORY.IS.NOT.ALLOWED'
        ETEXT='��� ����� ���� �������'
    END
* ELSE
* END

    IF R.NEW(TT.TE.CURRENCY.2) = LCCY THEN
        T(TT.TE.AMOUNT.LOCAL.1)<3> =''
* T(TT.TE.AMOUNT.LOCAL.2)<3> =''
        T(TT.TE.AMOUNT.FCY.1)<3> ='NOINPUT'
* T(TT.TE.AMOUNT.FCY.2)<3> ='NOINPUT'
    END ELSE
        T(TT.TE.AMOUNT.FCY.1)<3> =''
* T(TT.TE.AMOUNT.FCY.2)<3> =''
        T(TT.TE.AMOUNT.LOCAL.1)<3> ='NOINPUT'
* T(TT.TE.AMOUNT.LOCAL.2)<3> ='NOINPUT'

    END
*END

    CALL REBUILD.SCREEN

    RETURN
END
