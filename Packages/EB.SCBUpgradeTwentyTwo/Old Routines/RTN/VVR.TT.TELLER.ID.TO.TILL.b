* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>249</Rating>
*-----------------------------------------------------------------------------
*----- DINA_SCB ---- 17-7-2002


SUBROUTINE VVR.TT.TELLER.ID.TO.TILL

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID

IF AF =TT.TE.TELLER.ID.1 THEN
*IF  COMI THEN
 IF COMI[3,2] # 99 THEN ETEXT = 'MUST BE A HEAD CHASHIER'
 ELSE
   IF R.USER<EB.USE.DEPARTMENT.CODE> # TRIM(COMI[1,2] , '0', 'L') THEN
    ETEXT = 'MUST BE THE SAME BRANCH'
   END
  END
END ELSE
IF AF = TT.TE.TELLER.ID.2 THEN
  IF COMI[3,2] = 99 THEN ETEXT = 'CANNOT BE A HEAD CHASHIER'
  ELSE
   IF R.USER<EB.USE.DEPARTMENT.CODE> # TRIM(COMI[1,2] , '0', 'L') THEN
    ETEXT = 'MUST BE THE SAME BRANCH'
   END
  END
END

RETURN
END
