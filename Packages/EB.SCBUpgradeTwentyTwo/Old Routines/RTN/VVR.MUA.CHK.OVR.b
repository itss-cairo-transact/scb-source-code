* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>123</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/08 ***
*******************************************

    SUBROUTINE VVR.MUA.CHK.OVR
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.OVERRIDE.INDEX
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

*IF MESSAGE # 'VAL' THEN
    GOSUB INITIATE
    GOSUB GET.CHK.DATA
* END

    CALL REBUILD.SCREEN
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.USR  = "F.USER"               ; F.USR   = ""
    CALL OPF (FN.USR,F.USR)

    FN.SOI = 'F.SCB.OVERRIDE.INDEX' ; F.SOI = ''
    CALL OPF(FN.SOI,F.SOI)

    WS.USR.CO = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.MOD.USR = WS.COMP[2]

    WS.ER.FLG = 0
    WS.RNK.DONE = 0
    WS.FLG = 1
    RETURN
*-----------------------------------------------------
GET.CHK.DATA:
    WS.USR.CO           = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.USR.ID           = R.NEW(MUA.USER.ID)
    WS.USR.POS          = R.NEW(MUA.USER.BNK.POSITION)
    WS.USR.COMP         = 'EG00100':WS.USR.CO[2]
    WS.USR.SCB.DEPT     = R.NEW(MUA.DEP.ACCT.CODE)
    WS.USR.TO.DATE      = R.NEW(MUA.END.DATE.PROFILE)

    CALL F.READ(FN.SOI,COMI,R.SOI,F.SOI,ER.SOI)

    WS.EXP.USR          = R.SOI<SOI.EXCEPTION.USER.ID>
*Line [ 77 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.EXP.USR.CNT      = DCOUNT(WS.EXP.USR,@VM)
    WS.EXP.TO.DATE      = R.SOI<SOI.TO.DATE>
    WS.OVR.RNK          = R.SOI<SOI.USR.STAFF.RANK>
*Line [ 81 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.RNK.CNT          = DCOUNT(WS.OVR.RNK,@VM)

    WS.OVR.CO           = R.SOI<SOI.COMPANY.CODE>
*Line [ 85 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CO.CNT       = DCOUNT(WS.OVR.CO.CNT,@VM)
    WS.OVR.DEPT         = R.SOI<SOI.DEP.ACCT.CODE>
    WS.FUN.REQ          = R.SOI<SOI.FUNCTION>

*** UPDATED BY MSABRY 2014/11/16
    IF AV EQ 1 THEN
        IF COMI EQ '' THEN
            RETURN
        END
    END
    IF AV NE 1 THEN
        IF COMI EQ '' THEN
            ETEXT = "�� ���� ��� ����� ����"
            CALL STORE.END.ERROR
        END
        IF R.NEW(MUA.OVER.CLASS)<1,1> EQ '' THEN
            ETEXT = "�� ���� ��� ����� ����� ����"
            CALL STORE.END.ERROR
        END
    END

    WS.OVR.ID  = R.NEW(MUA.OVER.CLASS)
*Line [ 108 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CNT = DCOUNT (WS.OVR.ID,@VM)
    FOR IOVR = 1 TO WS.OVR.CNT
*        IF WS.OVR.ID<1,IOVR> EQ R.NEW(MUA.OVER.CLASS)<1,AV> THEN
        IF AV NE IOVR THEN
            IF WS.OVR.ID<1,IOVR> EQ COMI THEN
                WS.FLG ++
                IF WS.FLG GT 1 THEN
                    ETEXT = "�� ���� ����� ���� ���������"
                    CALL STORE.END.ERROR
                    RETURN
                END
            END
        END
    NEXT IOVR

*** END OF UPDATED BY MSABRY 2014/11/16
*--------------------------- CHK EXP USER -----------------------------------------------
    IF WS.EXP.USR NE '' THEN
        FOR IEXP.USR.CNT = 1 TO WS.EXP.USR.CNT
            IF WS.USR.ID EQ WS.EXP.USR<1,IEXP.USR.CNT> THEN
                IF WS.EXP.TO.DATE<1,IEXP.USR.CNT> NE '' THEN
                    IF WS.EXP.TO.DATE<1,IEXP.USR.CNT> LT WS.USR.TO.DATE THEN
                        ETEXT = "��� ����� ��������� �������� ��������"
                        CALL STORE.END.ERROR
                    END
                    IF WS.USR.TO.DATE GT WS.EXP.TO.DATE<1,IEXP.USR.CNT> THEN
                        ETEXT = "��� �� ���� ����� ������ �������� ��� �� ":WS.EXP.TO.DATE<1,IEXP.USR.CNT>
                        CALL STORE.END.ERROR
                    END
                END
                RETURN
            END
        NEXT IEXP.USR.CNT
    END
*--------------------------- CHK IF GIVE TO USER -----------------------------------------------
    IF  R.SOI<SOI.GIVE.TO.USER> NE 'Y' THEN
        ETEXT = "��� ����� �������� ��� ��������"
        CALL STORE.END.ERROR
        RETURN
    END
*--------------------------- CHK COMPANY.CODE -----------------------------------------------
    IF WS.OVR.CO NE '' THEN
        FINDSTR WS.USR.COMP IN WS.OVR.CO SETTING FPOS, VPOS THEN
*Line [ 152 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
            ETEXT = "��� ����� �������� ��� �������� ���� �����"
            CALL STORE.END.ERROR
            RETURN
        END
    END
*--------------------------- CHK SCB.DEPT.CODE -----------------------------------------------
    IF WS.OVR.DEPT NE '' THEN
        FINDSTR WS.USR.SCB.DEPT IN WS.OVR.DEPT SETTING FPOS, VPOS THEN
*Line [ 163 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
            ETEXT = "��� ����� ������� �������� ���� �������"
            CALL STORE.END.ERROR
            RETURN
        END
    END
*--------------------------- CHK RANK -----------------------------------------------
    IF WS.OVR.RNK NE '' THEN
        IF WS.USR.SCB.DEPT NE 5100  AND WS.USR.SCB.DEPT NE 5200 THEN
            FOR IRNK.CNT = 1 TO WS.RNK.CNT
                IF WS.USR.POS EQ WS.OVR.RNK<1,IRNK.CNT> THEN
                    WS.RNK.DONE = 1
                END
            NEXT IRNK.CNT
            IF WS.RNK.DONE NE 1 THEN
                ETEXT = "��� ����� ���� ������� ������� ���� ��������"
                CALL STORE.END.ERROR
                RETURN
            END
        END
    END
*--------------------------- CHK FUNCTION -----------------------------------------------

    IF WS.FUN.REQ NE '' THEN
        FINDSTR WS.FUN.REQ IN R.NEW(MUA.FUNCTION) SETTING FPOS, VPOS THEN
*Line [ 190 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
            IF WS.FUN.REQ EQ 'A.AUTHORISER' THEN
                ETEXT = "��� �������� ������ ������ ���"
                CALL STORE.END.ERROR
                RETURN
            END
            IF WS.FUN.REQ EQ 'I.INPUTER' THEN
                ETEXT = "��� �������� ������ ������ ���"
                CALL STORE.END.ERROR
                RETURN
            END
        END

    END
*------------------------------------------------------------------------------------
    RETURN
*-----------------------------------------------------
