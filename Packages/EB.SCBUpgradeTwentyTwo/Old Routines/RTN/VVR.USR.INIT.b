* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 201/11/27 ***
*******************************************

    SUBROUTINE VVR.USR.INIT

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    WS.COMP = ID.COMPANY
    IF APPLICATION EQ 'USER' THEN
        IF COMI # R.OLD(EB.USE.DEPARTMENT.CODE) THEN
            GOSUB CLEAR.USR
            GOSUB EDIT.USR
            CALL REBUILD.SCREEN
        END
    END

    RETURN
*------------------------------------------------------------------------
CLEAR.USR:

*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.NEW(EB.USE.COMPANY.RESTR),@VM)
    FOR I = 1 TO WS.COUNT
        DEL R.NEW(EB.USE.COMPANY.RESTR)<1,I>
        DEL R.NEW(EB.USE.APPLICATION)<1,I>
        DEL R.NEW(EB.USE.VERSION)<1,I>
        DEL R.NEW(EB.USE.FUNCTION)<1,I>
        DEL R.NEW(EB.USE.FIELD.NO)<1,I>
        DEL R.NEW(EB.USE.DATA.COMPARISON)<1,I>
        DEL R.NEW(EB.USE.DATA.FROM)<1,I>
        DEL R.NEW(EB.USE.DATA.TO)<1,I>
        I --
        WS.COUNT --
    NEXT I

*Line [ 63 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CNT = DCOUNT(R.NEW(EB.USE.OVERRIDE.CLASS),@VM)
    FOR I.OVR.CNT = 1 TO WS.OVR.CNT
        DEL R.NEW(EB.USE.OVERRIDE.CLASS)<1,I.OVR.CNT>
        I.OVR.CNT --
        WS.OVR.CNT --
    NEXT I.OVR.CNT

*Line [ 71 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CO.CNT = DCOUNT(R.NEW(EB.USE.COMPANY.CODE),@VM)
    FOR ICO.CNT = 1 TO WS.CO.CNT
        DEL R.NEW(EB.USE.COMPANY.CODE)<1,ICO.CNT>
        ICO.CNT --
        WS.CO.CNT --
    NEXT ICO.CNT

    RETURN
*------------------------------------------------------------------------
EDIT.USR:
    IF MESSAGE NE 'VAL' THEN
        R.NEW(EB.USE.DEPARTMENT.CODE)    =  COMI
        R.NEW(EB.USE.LOCAL.REF)<1,USER.SCB.DEPT.CODE> = COMI
    END

    WS.BR                            = (COMI + 100)[2]
    R.NEW(EB.USE.COMPANY.CODE)<1,1>  = "EG00100":WS.BR
    R.NEW(EB.USE.INIT.APPLICATION)   = '?':0

    R.NEW(EB.USE.COMPANY.RESTR)<1,1> = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,1>   = 'ALL.PG.H'
    R.NEW(EB.USE.FUNCTION)<1,1>      = 'E L S P'


    WS.COUNT = 2
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = '@LOCK.DEV.AREA'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = 'FUNDS.TRANSFER'
    R.NEW(EB.USE.FUNCTION)<1,WS.COUNT>        = 'E L S P'
    R.NEW(EB.USE.FIELD.NO)<1,WS.COUNT>        = '1'
    R.NEW(EB.USE.DATA.COMPARISON)<1,WS.COUNT> = 'NE'
    R.NEW(EB.USE.DATA.FROM)<1,WS.COUNT>       = 'AC12'
    R.NEW(EB.USE.DATA.TO)<1,WS.COUNT>         = 'AC13'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = 'ALL.PG.M'


    IF MESSAGE EQ 'VAL' THEN
        IF R.NEW(EB.USE.LOCAL.REF)<1,USER.SCB.DEPT.CODE> EQ '5100' THEN
            GOSUB BR.MNGR
        END
    END
    RETURN
*-------------------------------------------------------------------
BR.MNGR:

    IF R.NEW(EB.USE.COMPANY.CODE)<1,1> EQ 'EG0010099' THEN
        RETURN
    END
    IF R.NEW(EB.USE.COMPANY.CODE)<1,1> EQ 'EG0010088' THEN
        RETURN
    END

    R.NEW(EB.USE.INIT.APPLICATION)   = '?':400

    R.NEW(EB.USE.COMPANY.RESTR)<1,1> = R.NEW(EB.USE.COMPANY.CODE)<1,1>
    R.NEW(EB.USE.APPLICATION)<1,1>   = '@I400'
    R.NEW(EB.USE.FUNCTION)<1,1>      = ''

    WS.COUNT = 2
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = '@LOCK.DEV.AREA'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = 'FUNDS.TRANSFER'
    R.NEW(EB.USE.FUNCTION)<1,WS.COUNT>        = 'E L S P'
    R.NEW(EB.USE.FIELD.NO)<1,WS.COUNT>        = '1'
    R.NEW(EB.USE.DATA.COMPARISON)<1,WS.COUNT> = 'NE'
    R.NEW(EB.USE.DATA.FROM)<1,WS.COUNT>       = 'AC12'
    R.NEW(EB.USE.DATA.TO)<1,WS.COUNT>         = 'AC13'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = 'ALL.PG.M'
    RETURN
*-------------------------------------------------------------------
