* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.PL.ACCT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
* TO CHECK IF THE ACCOUNT IS PL ACCOUNT
    IF MESSAGE NE 'VAL' THEN
        IF LEN(COMI) GT 5 THEN ETEXT='��� ����� ��� ������� ����� � �����'
*IF COMI LT 60000 THEN
        IF COMI LT 50000 THEN
* ETEXT='��� ����� ���������� ����� � ����� ������ ��60000�� �� ����'
* ETEXT='��� ����� ���������� �.� ������ �� 50000 ���� ����'
        END ELSE
            IF NUM(COMI) THEN
                COMI='PL':COMI
            END
        END
    END
    IF MESSAGE EQ 'VAL' THEN
        PL.CAT = COMI[3,5]
       ** TEXT='COMI BEFORE=':COMI;CALL REM
        IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
            COMI = COMI:'7070'
           ** TEXT='COMI AFTER CONTAC=':COMI;CALL REM
        END
    END
    **TEXT='COMI=':COMI;CALL REM
    RETURN
END
