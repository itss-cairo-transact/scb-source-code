* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
**************DINA-SCB***************

SUBROUTINE VVR.TT.TELER2.TOTILL.AM
*1-to allow user just to enter teller id and dept will be defaulted
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

IF MESSAGE # 'VAL' THEN
DEP=R.USER<EB.USE.DEPARTMENT.CODE>
DEP= STR('0', 2-LEN(DEP) ):DEP
IF LEN(COMI) # 2 AND NOT(R.NEW(TT.TE.TELLER.ID.2)) THEN
 ETEXT = '���� ��� ����� ����� ������'
 END ELSE
 COMI =DEP*100 + COMI
 COMI= STR('0', 4-LEN(COMI) ):COMI
 CALL REBUILD.SCREEN
 END
 IF COMI[3,2] = 98 THEN ETEXT = '��� �� �� ���� ���� ����'
END

RETURN
END
