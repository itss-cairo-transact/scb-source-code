* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
******MAI SAAD 6/2/2019********************
    SUBROUTINE VVR.WALLET.MOBILE.CUST

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.MOBILE
*----------------------------------------
    GOSUB INITIALISE
*----------------------------------------
INITIALISE:
    KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
    FN.CUS   = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    CUS.MOBILE = COMI
    IF LEN(CUS.MOBILE) # 11 THEN
        ETEXT = 'ENTER VALID MOBILE NUMBER' ;   ;CALL STORE.END.ERROR
        CALL REBUILD.SCREEN
    END ELSE
****UPDATED BY NESSREEN AHMED 18/2/2019********
***UPDATED BY MAI  06JULY2020
        IS.WALL = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.IS.EWALLET>
        IF IS.WALL EQ 'YES' THEN 
            N.SEL = "SELECT FBNK.CUSTOMER WITH IS.EWALLET EQ 'YES' AND SMS.1 EQ ": CUS.MOBILE
            CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
            IF SELECTED.N THEN
                ETEXT = '��� ������� ������ �� ����� ����'
                CALL STORE.END.ERROR
            END
        END
****END OF UPDATE 18/2/2019*******************
*----------------------------------------------------
        RETURN

    END
