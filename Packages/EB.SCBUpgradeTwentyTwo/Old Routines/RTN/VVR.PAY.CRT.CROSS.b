* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.PAY.CRT.CROSS

*TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUES.STOPPED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

****************MAIN PROGRAM************************
MAIN.PROG:
    IF MESSAGE NE 'VAL' THEN
        GOSUB INTIAL
        IF NOT(COMI) THEN
            RETURN
        END
        GOSUB CHQ.STOP
* TEXT = "EXIT.LP":EXIT.LP
        IF CURR = "1" THEN
*  EXIT.LP = ''
            RETURN
        END ELSE

            GOSUB CRF.CHQ.ISSUE
            IF NOT(SELECTED) THEN
                RETURN
            END

        END
    END

*************************************************************************************************
INTIAL:
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT =''

    RETURN
********************************************************************************************************
*CHQ.STOP.SUB:
*  EXIT.LP = ''
* COUNTS1 =DCOUNT(CHQ.STOP,VM)
* FOR I = 1 TO COUNTS1 WHILE EXIT.LP = ''
* IF CHQ.STOP<1,I> THEN
*DASH.POS = INDEX(CHQ.STOP<1,I>,"-",1)
*FIRST.NO  = FIELD(CHQ.STOP<1,I>,"-",1)
*LAST.NO   = FIELD(CHQ.STOP<1,I>,"-",2)
*IF NOT(DASH.POS) THEN
*LAST.NO = FIRST.NO
*END
*TEXT = "FIRST.NO": FIRST.NO ; CALL REM
*TEXT = "LAST.NO": LAST.NO ; CALL REM
*END
*IF COMI GE FIRST.NO AND COMI LE LAST.NO THEN
*EXIT.LP = 'YES'
*END
*NEXT I

*IF EXIT.LP = 'YES' THEN
*ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
*END

    RETURN


*********************************************************************************************************
CHQ.STOP:

* TEXT = 'CHQ.STOP' ;  CALL REM
    CHQ.ID.STOP = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:"*": COMI
*  TEXT = "CHQ.ID.STOP =":CHQ.ID.STOP ; CALL REM
    CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
*  TEXT = "CURR = ":CURR ; CALL REM
    IF CURR THEN
        ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
    END



***************************************************************************
* COUNTS1 =DCOUNT(CHQ.STOP,VM)
* TEXT = "COUNTS1": COUNTS1 ; CALL REM
* FOR I = 1 TO COUNTS1
*IF CHQ.STOP<1,I>[3,1] EQ "-" OR CHQ.STOP<1,I>[2,1] EQ "-" THEN

* FIRST.NO = CHQ.STOP<1,I>[1,2]
* TEXT = "FIRST.NO": FIRST.NO ; CALL REM

* LAST.NO = CHQ.STOP<1,I>[4,2]
* TEXT = "LAST.NO": LAST.NO ; CALL REM

*IF LAST.NO = '' THEN
*    LAST.NO = FIRST.NO
*   TEXT = "LAST": LAST.NO ; CALL REM
* END
* IF COMI LE LAST.NO AND COMI GE FIRST.NO THEN
*    ERS = 1
*   I = COUNTS1
*  ETEXT = '����� ��� (&)�����'
* END
* FIRST.NO = ''
* LAST.NO = ''
* NEXT I

    RETURN
***************************************************************************
CRF.CHQ.ISSUE:
*TEXT = 'CRF.CHQ.ISSUE' ;  CALL REM

    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:".": COMI
*TEXT = "SCB.CHQ.ID": SCB.CHQ.ID ; CALL REM
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ":SCB.CHQ.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*TEXT = "SLE": SELECTED ; CALL REM

    IF NOT(SELECTED) THEN
*   TEXT  = "NOT SELECTED": SELECTED ; CALL REM
        ETEXT = '�� ���� ��� ����� ����� ���� �����'
        RETURN
    END ELSE
        GOSUB CRF.CHQ.PAID
    END
    RETURN
***************************************************************************
CRF.CHQ.PAID:
* TEXT = "CRF.CHQ.PAID" ; CALL REM
    CALL F.READ(FN.CHQ.PRESENT,KEY.LIST,R.CHQ.PRESENT,F.CHQ.PRESENT,E1)

    CHQ.STAT = R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS>
* TEXT = "CHQ.STAT": CHQ.STAT ; CALL REM
    CHQ.TRNS.PAY = R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT>
*TEXT = "CHQ.TRNS": CHQ.TRNS.PAY ; CALL REM
    CHQ.PAY.DATE = R.CHQ.PRESENT<DR.CHQ.PAY.DATE>
*TEXT = "CHQ.PAY.DATE": CHQ.PAY.DATE ; CALL REM
    CHQ.PAY.BRN = R.CHQ.PRESENT<DR.CHQ.PAY.BRN>
* TEXT = "CHQ.PAY.BRN": CHQ.PAY.BRN ; CALL REM
    CHQ.AMT = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
    CHQ.REC.DATE = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
    CHQ.BEN = R.CHQ.PRESENT<DR.CHQ.BEN>

    IF CHQ.STAT = 2 THEN
*   AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN

*    TEXT  = "SAMEH" ; CALL REM
        ETEXT = "����� ��� ����" ; CALL STORE.END.ERROR
    END ELSE
        IF CHQ.STAT = 3 THEN
*AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
*       TEXT  = "SAMEH" ; CALL REM
            ETEXT = "����� �� ������" ; CALL STORE.END.ERROR
        END ELSE
       CALL REBUILD.SCREEN
*      TEXT = "UPDATE.SCB.FT.DR.CHQ"
            R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> = R.CHQ.PRESENT<DR.CHQ.ISSUE.BRN>
            R.NEW(FT.DEBIT.CURRENCY) = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
            R.NEW(FT.DEBIT.ACCT.NO) = R.CHQ.PRESENT<DR.CHQ.NOS.ACCT>
            R.NEW(FT.DEBIT.AMOUNT) = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
            R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE> = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
            R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>
            R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER< EB.USE.DEPARTMENT.CODE>
*CALL REBUILD.SCREEN

        END

        RETURN

***************************************************************************

    END
