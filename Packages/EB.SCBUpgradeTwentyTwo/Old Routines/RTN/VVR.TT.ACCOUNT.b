* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN AHMED 14/07/2002,  -----
*-----------------------------------------------------------------------------
* <Rating>437</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.TT.ACCOUNT

*TO CHECK IF FIELD LENGTH # 17 AND IT IS  A NOSTRO OR INTERNAL ACCOUNT
*THEN ERROR ELSE DEFAULT NARRATIVE WITH CUSTOMER.SHORT.NAME AND CURRENCY.1 WITH THE CURRENCY IN ACCOUNT.1
*IF CURRENCY IS LOCAL CURR THEN LET FIELD AMOUNT.FCY IS NOINPUT
*ELSE LET AMOUNT.LOCAL IS NOINPUT
*DEFAULT EXPOSURE.DATE.2 AND VALUE.DATE.2 EQ TODAY



*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

IF COMI THEN
  IF  LEN(COMI) = 17  THEN
      GOSUB INTIAL
      GOSUB CHECK.IF.NOS.OR.INT
      
  END ELSE
   ETEXT = "INVALID MASK"

  END

END


GOTO PROGRAM.END

***********************************************************************************************************************

INTIAL:

*-------------TO INTIALIZE ALL VARIABLES -----------------------------------------*

 ETEXT = '' ; E = '' ; CURR.NAME1 = '' ; SHOR.NAME1 = ''


RETURN
***********************************************************************************************************************


CHECK.IF.NOS.OR.INT:


*-------------TO CHECK IF IT IS NOSTRO OR INTERNAL ACCOUNT---------------------------*

       IF NOT(NUM(COMI)) THEN ETEXT = "SHOULD.NOT.INT.ACCT"
          IF COMI[12,4] >= '5000' AND COMI[12,4] <='5999' THEN ETEXT = "SHOULD.NOT.NOSTRO.ACC"


       GOSUB GET.CUSNAME.CURR

  RETURN
* **********************************************************************************************************************

GET.CUSNAME.CURR:

   ETEXT = ''

   CALL DBR( 'NUMERIC.CURRENCY':@FM:1,COMI[3, 2],CURR.NAME1)
         IF ETEXT THEN E = ETEXT

   R.NEW(TT.TE.CURRENCY.1) = CURR.NAME1


   CALL DBR( 'CUSTOMER':@FM:EB.CUS.SHORT.NAME,COMI[5, 7],SHOR.NAME1)
          IF ETEXT THEN E = ETEXT
   R.NEW(TT.TE.NARRATIVE.2) = SHOR.NAME1


    GOSUB CHECK.IF.CURR.LOCAL


 RETURN

******************************************************************************************************************
CHECK.IF.CURR.LOCAL:

   IF CURR.NAME1 = LCCY THEN

     T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
     R.NEW(TT.TE.AMOUNT.FCY.1) = ''
     T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'

   END ELSE

    T(TT.TE.AMOUNT.FCY.1)<3> = ''
    R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
    T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'

  END

  GOSUB DEF.DAT

RETURN

**********************************************************************************************************************
DEF.DAT:


* R.NEW(TT.TE.EXPOSURE.DATE.2) = TODAY
R.NEW(TT.TE.VALUE.DATE.2) = TODAY



RETURN

***********************************************************************************************************************
PROGRAM.END:

CALL REBUILD.SCREEN

RETURN
END
