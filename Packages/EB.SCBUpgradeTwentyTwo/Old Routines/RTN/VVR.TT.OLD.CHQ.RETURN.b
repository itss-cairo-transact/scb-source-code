* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****23/10/2003 ABEER *************
*-----------------------------------------------------------------------------
* <Rating>618</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.OLD.CHQ.RETURN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
****UPDATED BY NESSREEN AHMED 19/8/2017*************
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP.TYPE
****END OF UPDATE 19/8/2017*************************

    IF R.NEW(TT.TE.CUSTOMER.1)  EQ '' THEN
        CUS.ID  = R.NEW(TT.TE.CUSTOMER.2)
    END ELSE
        CUS.ID  = R.NEW(TT.TE.CUSTOMER.1)
    END

*   CHQ.NO  = R.NEW(TT.TE.CHEQUE.NUMBER)
    CHQ.NO  = COMI
    FLAG    = 0

    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;     R.CUS.AC = '' ;  F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    FN.CHQ.AC ='FBNK.CHEQUE.ISSUE.ACCOUNT' ; R.CHQ.AC = '' ;  F.CHQ.AC=''
    CALL OPF(FN.CHQ.AC,F.CHQ.AC)

    FN.CHQ.REG ='FBNK.CHEQUE.REGISTER' ; R.CHQ.REG = ''    ;  F.CHQ.REG   =''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)


    CALL F.READ( FN.CUS.AC,CUS.ID, R.CUS.AC, F.CUS.AC,ETEXT1)

*****UPDATED BY NESSREEN AHMED 19/8/2017********************
     FN.CHQ.REG.SUP ='FBNK.CHEQUE.REGISTER.SUPPLEMENT' ; R.CHQ.REG.SUP = ''    ;  F.CHQ.REG.SUP   =''
    CALL OPF(FN.CHQ.REG.SUP,F.CHQ.REG.SUP)
*****END OF UPDATE 19/8/2017********************************
    IF COMI THEN
****************************
        IF COMI # R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO> THEN
            ETEXT = '��� ����� ��� �����';CALL STORE.END.ERROR
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO> = COMI
            CALL REBUILD.SCREEN
        END
*****************************************
***************UPDATED BY NESSREEN AHMED 20/6/2011*************************************
*==========a)To Check IF Cheq Is Issued ===============================================
        T.SEL ="SELECT FBNK.CHEQUE.REGISTER WITH CUSTOMER EQ ": CUS.ID
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**TEXT = 'SELECTED=':SELECTED ; CALL REM
        IF SELECTED THEN
**  TEXT = 'SEL=':SELECTED ; CALL REM
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.CHQ.REG,KEY.LIST<I>, R.CHQ.REG, F.CHQ.REG , ERR.CHQ)
                CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>

*Line [ 105 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(CHQ.NOS,@VM)
**TEXT = 'DD=':DD ; CALL REM
                FOR X = 1 TO DD
                    CHN  = CHQ.NOS<1,X>
                    SS = COUNT(CHN, "-")
                    IF SS > 0 THEN
                        ST.NO   = FIELD(CHN ,"-", 1)
                        ED.NO   = FIELD(CHN ,"-", 2)
                    END ELSE
                        ST.NO   = CHN
                        ED.NO   = CHN
                    END
                    IF (CHQ.NO GE ST.NO) AND (CHQ.NO LE ED.NO) THEN
                        FLAG = "1"
**TEXT = 'RANGE' ; CALL REM
                    END ELSE
                    END
                NEXT X
            NEXT I
        END
        ETEXT = ''
        IF FLAG = "0" THEN
            ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
        END
        ETEXT = ''

******NESSREEN AHMED 10/4/2007***********
**        ACCT.NO =R.NEW(TT.TE.ACCOUNT.2)
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
**********************************
*==========b)To Check IF Cheq Is Stopped ============================================
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.AC)
        FN.CUAC = 'F.CUSTOMER.ACCOUNT' ; F.CUAC = '' ; R.CUAC = ''  ; E1.CUAC = ''
        CALL OPF(FN.CUAC,F.CUAC)
        CALL F.READ(FN.CUAC, CUST.AC , R.CUAC, F.CUAC, E1.CUAC)
****updated by Nessreen Ahmed 21/5/2012******************************
****        CHQ.ID = ACC.ID:'*':COMI
        CHQ.NO = TRIM(COMI, "0" , "L")
**Updated by Nessreen Ahmed 4/9/2016 For R15*****
**CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID,CHQ.STOP)
        KEY.ID = "...":CUST.AC:"....":CHQ.NO
        N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND STATUS EQ STOPPED "
        KEY.LIST.N=""
        SELECTED.N=""
        ER.MSG.N=""
        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
* TEXT = 'SELECTED.N=':SELECTED.N ; CALL REM
* TEXT = 'KEY.LIST.N<1>=':KEY.LIST.N<1> ; CALL REM
        IF SELECTED.N THEN
****UPDATED BY NESSREEN AHMED 19/8/2017*******************
****ETEXT='��� ����� �����'
            CALL F.READ( FN.CHQ.REG.SUP,KEY.LIST.N, R.CHQ.REG.SUP, F.CHQ.REG.SUP,ERR.SUP)
            STOP.TYPE = R.CHQ.REG.SUP<CC.CRS.PAYM.STOP.TYPE>
            CALL DBR ('PAYMENT.STOP.TYPE':@FM:AC.PAT.DESCRIPTION,STOP.TYPE,STOP.DESC)
            ETEXT='��� ����� �����':' * ':STOP.DESC
****END OF UPDATE  19/8/2017*******************
            CALL STORE.END.ERROR
        END
**********************************************************************************
***       CALL DBR ('PAYMENT.STOP':@FM:AC.PAY.FIRST.CHEQUE.NO,ACCT.NO,MYCHECK)
***       LOCATE COMI IN  MYCHECK<1,1>  SETTING I THEN
*            ETEXT='This.Cheque.Already.Stopped'
***           ETEXT='��� ����� �����'
***      END ELSE
*==========c)To Check IF Cheq Is Paid ******************************
        ETEXT = ''
        FOR NN = 1 TO 100
**   TEXT = 'COMI=' :COMI ; CALL REM
            PAY.CHQ.ID= COMI:'.': NN
            CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCNO)
            IF NOT(ETEXT) THEN
*                ETEXT='This.Cheque.Aleardy.Paid'
                ETEXT='��� ����� �� ����'
                CALL STORE.END.ERROR
                NN = 100
            END
            ETEXT = ''
        NEXT  NN
** IF ETEXT THEN
**************UPDATED BY HYTHAM 15/09/2009********************
        KEY.LIST.DR = ""  ;  ER.MSG = ""  ; SELECTED.DR = ""

        T.SEL.DR = "SELECT F.SCB.FT.DR.CHQ WITH CHEQ.NO EQ ":COMI:" OR OLD.CHEQUE.NO EQ ": COMI
        CALL EB.READLIST(T.SEL.DR,KEY.LIST.DR,"",SELECTED.DR,ER.MSG)
        IF SELECTED.DR THEN
            ETEXT= "���� ��� ����� ����� ���� �����"  ; CALL STORE.END.ERROR
        END
*    ------------------------------------------------------------    *
**************END OF UPDATED 15/09/2009***************************

**   END
    END   ;*** END OF IF COMI

*********
    OLD.CHQ.NO = COMI
    IF MESSAGE EQ 'VAL' THEN
****UPDATED BY NESSREEN AHMED 16/03/2009*********
**    CALL VVR.TT.CHEQ.ISSUE
*************************************************
****UPDATED BY NESSREEN AHMED 09/04/2009*********
***       CALL VVR.TT.CHQ.RETURN
**************************************************
        COMI=OLD.CHQ.NO
        R.NEW(TT.TE.CHEQUE.NUMBER) = COMI
        CALL REBUILD.SCREEN

    END
*********
    RETURN
END
