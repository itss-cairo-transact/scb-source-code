* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.NO.CHQ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*----------------------------------------
    IF MESSAGE EQ 'VAL' THEN
        WS.CHQ.NO  = R.NEW(CHQA.NO.OF.BOOKS)
*Line [ 29 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNT.CHQ = DCOUNT(R.NEW(CHQA.CHQ.NO.START),@VM)
        IF WS.CHQ.NO NE DCOUNT.CHQ THEN
            ETEXT = '��� ���� ����� ������ �� ����� ��� �������'
            CALL STORE.END.ERROR
        END
        FOR I = 1 TO DCOUNT.CHQ
            WS.CHQ.START = R.NEW(CHQA.CHQ.NO.START)<1,I>
            WS.LEN.CHQ = LEN(WS.CHQ.START)
            IF WS.LEN.CHQ LT 11 THEN
                ETEXT = '����� ����� ������ ��� �� 11 ���'
                CALL STORE.END.ERROR
            END
            T.SEL = "SELECT F.SCB.CHEQ.APPL WITH CHQ.NO.START EQ ":WS.CHQ.START
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                ETEXT = '��� ������� ����� �� ���'
                CALL STORE.END.ERROR
            END
        NEXT I
    END
    RETURN
************************************************************
END
