* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*----------------15-7-2002 ZOZO SCB-----------------------*

SUBROUTINE VVR.TT.DEFAULT.ACCOUNT.2

*DEFAULT FIELD ACCOUNT.2 WITH
*CURRENCY.1:CATEGORY(FROM TELLER.TRANSACTION):TELLER.ID.1 BRANCH:99

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION

  TELL.ID.2=''
  ETEXT=''
  BRANCH1=''
  TRAN.CODE=''


IF V$FUNCTION = 'I' THEN
          TRAN.CODE= R.NEW(TT.TE.TRANSACTION.CODE)
          CURR= R.NEW(TT.TE.CURRENCY.1)
          TELL.ID.2 = R.NEW(TT.TE.TELLER.ID.2)

           CALL DBR('TELLER.TRANSACTION':@FM:TT.TR.CAT.DEPT.CODE.1,TRAN.CODE,CATEG)

             IF NOT(ETEXT) THEN
             
               R.NEW(TT.TE.ACCOUNT.2)= CURR: CATEG : TELL.ID.2

             END
             CALL REBUILD.SCREEN


END

RETURN
END
