* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SR.NO.OF.SHARES

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT

*IF MESSAGE = 'VAL' THEN
    SAM     = COMI / 2
    IF COMI LE 99 THEN
        SAM.NO.DOTTTT = FIELD(SAM,".",2)
        IF SAM.NO.DOTTTT THEN
            SAM.NO.DOT = FIELD(SAM,".",1)
            R.NEW(SR.SCRIPT.BUY.SHARES) = SAM.NO.DOT+1

        END ELSE
            SAM.NO.DOT = FIELD(SAM,".",1)
            R.NEW(SR.SCRIPT.BUY.SHARES) = SAM.NO.DOT

        END
    END ELSE
        SAM.NO.DOT = FIELD(SAM,".",1)
        R.NEW(SR.SCRIPT.BUY.SHARES) = SAM.NO.DOT
    END

    CALL REBUILD.SCREEN

    RETURN
END
