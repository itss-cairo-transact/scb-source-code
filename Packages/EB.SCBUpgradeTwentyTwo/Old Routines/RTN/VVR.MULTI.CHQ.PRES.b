* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>44</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.MULTI.CHQ.PRES

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    ETEXT = ''
    IF COMI  THEN
        ACCT.NO =R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,AV>
**        TEXT = "ACCCC=  " : ACCT.NO ; CALL REM
**        TEXT = "AVVVV= " : AV ; CALL REM
********************UPDATED BY RIHAM R13 **************
*    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN1)
        CUS.BR = MYBRN1[8,2]
        MYBRN = TRIM(CUS.BR, "0" , "L")
**************************END OF UPDATE ****************
**        PAY.CHQ.ID = COMI:'.':MYBRN
**        TEXT = "P.ID":PAY.CHQ.ID ; CALL REM
        CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCN)
**        TEXT = "MYACCN":MYACCN ; CALL REM
        IF MYACCN THEN
            ETEXT='��� ����� �� ����'
        END ELSE
**************************************************************
*    TEXT = "CHQ.STOP" ; CALL REM
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID.STOP = ACCT.NO:"*": COMI
*    TEXT = "ID.STOP":CHQ.ID.STOP ; CALL REM
            CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,ACCT.NO,CHQ.TYPE)
            TY = CHQ.TYPE
*Line [ 81 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT (TY,@VM)
            FOR X = 1 TO DD
                CHQ.ID = CHQ.TYPE<1,X>:'.':ACCT.NO:'.':COMI
                CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
****  CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY, CHQ.ID.STOP, CURR)
****END OF UPDATE 8/3/2016*****************************
                ETEXT = ''
                IF CURR THEN
                    ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
                END ELSE

****************************************************************
                    F.COUNT = '' ; FN.COUNT = 'F.AC.LOCKED.EVENTS'
                    CALL OPF(FN.COUNT,F.COUNT)
                    LOCAL.REF = FN.COUNT<AC.LCK.LOCAL.REF>
                    CHQ.NO = LOCAL.REF<1,AC.LCK.LOC.CHQ.NO>

                    T.SEL = "SELECT FBNK.AC.LOCKED.EVENTS WITH CHQ.NO EQ ":COMI:" AND ACCOUNT.NUMBER EQ ":ACCT.NO
                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*  TEXT = "SELECTED":SELECTED ; CALL REM
                    IF SELECTED THEN
                        ETEXT = "��� ����� ���� ����" ;CALL STORE.END.ERROR
                    END

                END
            NEXT X
        END

    END

***************************************************************
    OLD.CHQ.NO = COMI
    IF MESSAGE EQ 'VAL' THEN
*****UPDATED BY NESSREEN AHMED ON 16/03/2009*************
**     CALL VAR.FT.OFS
*********************************************************
* CALL VVR.FT.CHQ.RETURN
        COMI=OLD.CHQ.NO
        R.NEW(FT.CHEQUE.NUMBER) = COMI
        CALL REBUILD.SCREEN
    END

    RETURN
END
