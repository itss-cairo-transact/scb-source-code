* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.START.DATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.KEST.LOANS
*-------------------------NESSMA-----------------
    SATRT.DATE       = COMI
    NO.OF.INSTALL    = R.NEW(KEST.NO.OF.INSTALLMENT)
    PERIODIC.INSTALL = R.NEW(KEST.PERIODIC.INSTALLMENT)
    IF PERIODIC.INSTALL EQ 4 THEN
        PERIODIC.INSTALL = 6
    END
    IF PERIODIC.INSTALL EQ 5 THEN
        PERIODIC.INSTALL = 12
    END
    SAT.YY           = SATRT.DATE[1,4]
    SAT.MM           = SATRT.DATE[5,2]
    SAT.DD           = SATRT.DATE[7,2]
*------------------------------------------------
    FOR NN = 2 TO NO.OF.INSTALL
        CALL ADD.MONTHS(SATRT.DATE,PERIODIC.INSTALL)
    NEXT NN
    MAT.DATE = SATRT.DATE
    R.NEW(KEST.MATURITY.DATE) = MAT.DATE
*------------------------------------------------
    CALL REBUILD.SCREEN
    RETURN
END
