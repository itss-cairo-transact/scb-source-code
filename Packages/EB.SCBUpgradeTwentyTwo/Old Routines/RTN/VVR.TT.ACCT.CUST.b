* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1030</Rating>
*-----------------------------------------------------------------------------

** ----- 11 07 2002 KHALED HOSNY -----
SUBROUTINE VVR.TT.ACCT.CUST

* CHECK ON ACCOUNT.1 (CUSTOMER ACCOUNT) IN TELLER  CASH DEPOSIT
* ACCOUNT.1 MUST BE = 17 , MUST NOT INTERNAL AND NOT NOSTRO ACCOUNT


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

   IF LEVEL.STATUS THEN RETURN
   IF MESSAGE = 'REPEAT' THEN RETURN

   IF V$FUNCTION = 'I' THEN
       GOSUB INIT
       GOSUB CHECK.ACCT
       IF ERROR = 'Y' THEN RETURN
          GOSUB NARRATIV
       IF ERROR = 'Y' THEN RETURN
          GOSUB CURRENCY.COD
       IF ERROR = 'Y' THEN RETURN
          GOSUB CORR.CURRENCY

  END

RETURN

********************************************************************************************
INIT:
ETEXT = '' ; ERROR = ''; MMIIDD= '' ; E1 = ''; DDDD = TODAY

RETURN
*********************************************************************************************
CHECK.ACCT:
      IF  LEN( COMI) # 17  THEN ERROR = 'Y'; ETEXT = 'THE LENGTH OF ACCOUNT IS NOT CORRECT (&)' :@FM : COMI
        IF NOT(NUM(COMI)) THEN ERROR = 'Y';ETEXT = 'NOT.ALLOWED.FOR.CUSTOMER ACCT'
            IF COMI[12,4] >= '5000' AND COMI[12,4] <='5999' THEN ERROR = 'Y'; ETEXT = 'CATEGORY NOT RELATED TO THE TELLER FUNCTION (&)' :@FM :COMI[12,4]
RETURN
**********************************************************************************************
NARRATIV:
      CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,COMI[5,7],MMIIDD)
      IF ETEXT THEN ERROR = 'Y' ;E1 = 'CUSTOMER NUMBER NOT FOUND(&)':@FM:COMI[5,7]
            ELSE
      R.NEW(TT.TE.NARRATIVE.2,1) = MMIIDD
      END
RETURN
**********************************************************************************************
CURRENCY.COD:
        MMIIDD = ''
        CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,COMI[3,2],MMIIDD)
        IF ETEXT THEN ERROR = 'Y' ;E1 = 'CURRENCY NUMBER NOT FOUND(&)':@FM:COMI[3,2]
        ELSE
           R.NEW(TT.TE.CURRENCY.1) = MMIIDD;T(TT.TE.CURRENCY.1)<3>='NOINPUT'
        END
RETURN
*********************************************************************************************
CORR.CURRENCY:
   IF MMIIDD = LCCY  THEN
     R.NEW(TT.TE.AMOUNT.FCY.1)= ''
     T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
     T(TT.TE.AMOUNT.LOCAL.1)<3>=''
     GOSUB VAL.DATE.LOCAL
   END
   ELSE
       R.NEW(TT.TE.AMOUNT.LOCAL.1)= ''
       T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
       T(TT.TE.AMOUNT.FCY.1)<3>=''
       GOSUB VAL.DATE.FOREIGN
       END
RETURN
*********************************************************************************************
VAL.DATE.LOCAL:
   MMIIDD = ''
     CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,COMI,MMIIDD)
     IF NOT(MMIIDD) THEN MMIIDD = 0
IF  MMIIDD <= 0 THEN
      CALL CDT('', DDDD, '1W')
      R.NEW(TT.TE.VALUE.DATE.1) = DDDD
      R.NEW(TT.TE.EXPOSURE.DATE.1) = DDDD
END
ELSE
      R.NEW(TT.TE.VALUE.DATE.1) = TODAY
      R.NEW(TT.TE.EXPOSURE.DATE.1) = TODAY
END
RETURN
***********************************************************************************************
VAL.DATE.FOREIGN:
   CALL CDT('', DDDD, '2W')
   R.NEW(TT.TE.VALUE.DATE.1) = DDDD
   R.NEW(TT.TE.EXPOSURE.DATE.1) = DDDD


RETURN
***********************************************************************************************

       CALL REBUILD.SCREEN


RETURN

END
