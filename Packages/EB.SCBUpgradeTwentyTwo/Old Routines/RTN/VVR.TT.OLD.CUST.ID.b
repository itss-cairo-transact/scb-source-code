* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.OLD.CUST.ID


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS



    IF COMI THEN
        USER1=R.USER<EB.USE.DEPARTMENT.CODE>
*        TEXT=USER1:'USER';CALL REM
        IF LEN(COMI) LT 7 THEN
            CUST = STR('0', 7- LEN(COMI) ):COMI
            T.SEL ="SSELECT FBNK.CUSTOMER WITH OLD.CUST.ID EQ ": CUST :" AND ACCOUNT.OFFICER EQ " : USER1
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)


            R.NEW(TT.TE.CUSTOMER.2)=KEY.LIST<1>
            T(TT.TE.CUSTOMER.2)<3> = 'NOINPUT'
            T(TT.TE.CHEQUE.NUMBER)<3> = 'NOINPUT'

        END ELSE
            IF LEN(COMI)  GE 7 THEN
                T.SEL ="SSELECT FBNK.CUSTOMER WITH OLD.CUST.ID EQ ": COMI :" AND ACCOUNT.OFFICER EQ " : USER1
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

                R.NEW(TT.TE.CUSTOMER.2)=KEY.LIST<1>
                T(TT.TE.CUSTOMER.2)<3> = 'NOINPUT'
                T(TT.TE.CHEQUE.NUMBER)<3> =''
            END
        END
    END
    CALL REBUILD.SCREEN

    RETURN
END
