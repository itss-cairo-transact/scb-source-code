* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.CURR1.TOBR

* EMPTY ACCOUNT.1 AND ACCOUNT.2 AND CURRENCY.2 WHEN CURRENCY.1 CHANGED
* MAKE LOCAL AMOUNT NOINPUT IF CURRENCY NOT EQUAL EGP
* MAKE FORIGN AMOUNT NOINPUT IF CURRENCY EQUAL EGP

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

***   IF COMI # R.NEW(TT.TE.CURRENCY.1) THEN
***       R.NEW(TT.TE.ACCOUNT.1) = ""
***       R.NEW(TT.TE.ACCOUNT.2) = ""
***       R.NEW(TT.TE.CURRENCY.2) = ""
***       CALL REBUILD.SCREEN
***   END
    IF COMI = LCCY THEN
        T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.FCY.1) = ''
        T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'
    END  ELSE
        T(TT.TE.AMOUNT.FCY.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
        T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'
    END

    USR.BR = ''
    USR = OPERATOR
    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,USR,USR.BR)
    BR = FMT(USR.BR, "R%2")
    BR.ACCT = COMI:"11140000100":BR
    ACCT.2 = COMI:"10000":BR:"9900":BR
    R.NEW(TT.TE.ACCOUNT.1)= BR.ACCT
    R.NEW(TT.TE.ACCOUNT.2)= ACCT.2

    CALL REBUILD.SCREEN

    RETURN

END
