* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****23/10/2003 ABEER *************
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.OLD.CHQ

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
****UPDATED BY NESSREEN AHMED 19/8/2017*************
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP.TYPE
****END OF UPDATE 19/8/2017*************************
*TO CHECK IF OLD CHEQ NO ENTERED IS ALREADY PAID OR STOPPED AN ERROR MESSAGE WILL BE DISPLAYED
*ELSE A CHEQ WILL BE ISSUED
*ALSO ERROR MESSAGE WILL BE DISPLAYED IF BOTH FIELDS OLD AND NEW CHEQ NO IS ENTERED

*****UPDATED BY NESSREEN AHMED 19/8/2017********************
    FN.CHQ.REG.SUP ='FBNK.CHEQUE.REGISTER.SUPPLEMENT' ; R.CHQ.REG.SUP = ''    ;  F.CHQ.REG.SUP   =''
    CALL OPF(FN.CHQ.REG.SUP,F.CHQ.REG.SUP)
*****END OF UPDATE 19/8/2017********************************
    IF COMI THEN
****************************
        IF COMI # R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO> THEN
            ETEXT = '��� ����� ��� �����';CALL STORE.END.ERROR
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO> = COMI
            CALL REBUILD.SCREEN
        END
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
***************UPDATED BY RIHAM R13 ******************
*     CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN)
****UPDATED BY NESSREEN AHMED 8/3/2016 R15***********
        BR.CODE = TRIM(MYBRN, "0" , "L")
****    PAY.CHQ.ID= COMI:'.':MYBRN
        PAY.CHQ.ID= COMI:'.':BR.CODE
****END OF UPDATE 8/3/2016***********************
*********** To Check IF Cheq Is Paid ******************************
        CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCNO)
        IF MYACCNO THEN
            ETEXT='��� ����� �� ����'
        END ELSE
********************SOTP CHQ************************
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15*********
****CHQ.ID.STOP = ACCT.NO:"*": COMI
****CALL DBR('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
**UPDATED BY NESSREEN AHMED 5/9/2016 For R15***
**CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,ACCT.NO,CHQ.TYPE)
**TY = CHQ.TYPE
**DD = DCOUNT (TY,VM)
**FOR X = 1 TO DD
**CHQ.ID = CHQ.TYPE<1,X>:'.':ACCT.NO:'.':COMI
**CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST)
            CHQ.NO = TRIM(COMI, "0" , "L")
            KEY.ID = "...":CUST:"....":CHQ.NO

            N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND STATUS EQ STOPPED "
            KEY.LIST.N=""
            SELECTED.N=""
            ER.MSG.N=""
            CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
*TEXT = 'SELECTED=':SELECTED.N ; CALL REM
            IF SELECTED.N THEN
****UPDATED BY NESSREEN AHMED 19/8/2017*******************
****    ETEXT = ''
****    ETEXT = '����� ��� (&) ����� ':@FM:COMI ;CALL STORE.END.ERROR
                CALL F.READ( FN.CHQ.REG.SUP,KEY.LIST.N, R.CHQ.REG.SUP, F.CHQ.REG.SUP,ERR.SUP)
                STOP.TYPE = R.CHQ.REG.SUP<CC.CRS.PAYM.STOP.TYPE>
                CALL DBR ('PAYMENT.STOP.TYPE':@FM:AC.PAT.DESCRIPTION,STOP.TYPE,STOP.DESC)
                ETEXT = ''
                ETEXT = '����� ��� (&) ����� ':@FM:COMI :' * ':STOP.DESC
                CALL STORE.END.ERROR
****END OF UPDATE  19/8/2017*******************
            END
**NEXT X
        END
    END
********************************************************************
    OLD.CHQ.NO = COMI
    IF MESSAGE EQ 'VAL' THEN
*****UPDATED BY NESSREEN AHMED ON 16/03/2009************
**        CALL VVR.TT.CHEQ.ISSUE
********************************************************
*CALL VVR.TT.CHQ.RETURN
        COMI=OLD.CHQ.NO
        R.NEW(TT.TE.CHEQUE.NUMBER) = COMI
        CALL REBUILD.SCREEN
    END
    RETURN
END
