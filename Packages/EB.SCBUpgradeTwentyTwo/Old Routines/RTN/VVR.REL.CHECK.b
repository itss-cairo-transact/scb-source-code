* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>130</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.REL.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

    IF MESSAGE = 'VAL' THEN

        CALL DBR ('LETTER.OF.CREDIT':@FM:TF.LC.LOCAL.REF,ID.NEW,MYLOCAL)
        CALL DBR ('LETTER.OF.CREDIT':@FM:TF.LC.PRO.OUT.AMOUNT,ID.NEW,AMTT)
* TEXT= AMTT ; CALL REM
        MAT.EXP= R.NEW(TF.LC.ADVICE.EXPIRY.DATE)
        MYCODE=MYLOCAL<1,LCLR.OPERATION.CODE>

        IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CANCEL.REASON> = "EXPIRED" THEN
            ETEXT = ''
            IF MYCODE EQ "1309" THEN
                IF R.NEW(TF.LC.REL.PROVIS) NE 'Y' THEN
                    IF AMTT GT '0' THEN
                        ETEXT='MUST BE EQ Y ';CALL STORE.END.ERROR
                    END
                END
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = '1301'
                TEXT = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> ; CALL REM
            END ELSE
                R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1309"
                R.NEW(TF.LC.REL.PROVIS) = ''
                R.NEW(TF.LC.FULLY.UTILISED) = ''

            END
        END
*****************************************************************

        IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CANCEL.REASON> ="BENF." THEN

            IF COMI NE 'Y' THEN
                ETEXT='MUST BE EQ Y ';CALL STORE.END.ERROR
            END
            R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1303"
        END
******************************************************************
        IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CANCEL.REASON> ="CUSTOMER" THEN
            IF COMI NE 'Y' THEN
                IF AMTT GT '0' THEN
                    ETEXT='MUST BE EQ Y ';CALL STORE.END.ERROR
                END
            END

            R.NEW(TF.LC.LOCAL.REF)<1,LCLR.OPERATION.CODE> = "1302"

        END
*****************************************************************
        IF R.NEW(TF.LC.LOCAL.REF)<1,LCLR.CANCEL.REASON> EQ 'NO.CHEQ' THEN
            IF COMI NE 'Y' THEN
                IF AMTT GT '0' THEN
                    ETEXT='MUST BE EQ Y ';CALL STORE.END.ERROR
                END
            END


            IF R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE> EQ 'ADVANCE' THEN
                IF MYCODE NE '1111' AND MYCODE NE '1271'  AND MYCODE NE '1281'  AND MYCODE NE '1282' THEN
                    ETEXT = 'Not Allowed';CALL STORE.END.ERROR
                    ETEXT = ''
                END
                IF MYCODE EQ '1111' OR MYCODE EQ '1281' OR MYCODE EQ '1282' THEN
                    IF ADV.OPER EQ '' THEN
                        R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1304'
                        R.NEW(LD.FIN.MAT.DATE) = TODAY
                        R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = TODAY
                    END ELSE
                        IF ADV.OPER EQ 'YES' THEN
                            ETEXT='ALREADY OPERATED';CALL STORE.END.ERROR
                            ETEXT = ''
                        END
                    END
                END
                IF MYCODE EQ '1271' OR MYCODE EQ '1283' OR MYCODE EQ '1284' THEN
                    CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT.INCREASE,ID.NEW,MYINC)
                    R.NEW(LD.AMOUNT.INCREASE) = MYINC*-1
                    R.NEW(LD.AMT.V.DATE) = TODAY
                    R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE> = '1305'
                    R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> =MAT.EXP
                    R.NEW(LD.FIN.MAT.DATE) = '20991231'
                END
            END ELSE
              *  ETEXT='Only Advance IS Allowed';CALL STORE.END.ERROR
                ETEXT = ''
            END
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
