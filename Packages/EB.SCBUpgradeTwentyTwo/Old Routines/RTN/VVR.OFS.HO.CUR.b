* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>3659</Rating>
*-----------------------------------------------------------------------------
*    SUBROUTINE VVR.OFS.HO.CUR
    PROGRAM VVR.OFS.HO.CUR

*TO CREATE ONTHER FT FOR CURRENCY MARKET

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POS.MVMT.TODAY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS

    GOSUB CHECKEXIST
    IF SW2 = 0 THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
    END
    RETURN
*-----------------------------------------------
CHECKEXIST:
    KEY.LIST="" ; SELECTED="" ;  ER.FT="" ; SW2 = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC10'"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE PLEASE CALL IT DEP." ; CALL REM
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    RETURN
*-----------------------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:
*-----------
    V.DAT = TODAY
    COMMA = ","
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1="" ; CUR.BASE = ''
    FN.POS = 'FBNK.POS.MVMT.TODAY' ; F.POS = '' ; R.POS = ''
    FN.FT  = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.FTH = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FTH = '' ; R.FTH = ''
    FN.LD  = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    FN.LCM = 'FBNK.LETTER.OF.CREDIT' ; F.LCM = '' ; R.LCM = ''
    FN.TT  = 'FBNK.TELLER' ; F.TT = '' ; R.TT = ''
    FN.LCD = 'FBNK.DRAWINGS' ; F.LCD = '' ; R.LCD = ''
    FN.LCDH = 'FBNK.DRAWINGS$HIS' ; F.LCDH = '' ; R.LCDH = ''
    FN.AC  = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''

    CALL OPF(FN.POS,F.POS)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.FTH,F.FTH)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.LCD,F.LCD)
    CALL OPF(FN.LCDH,F.LCDH)
    CALL OPF(FN.LCM,F.LCM)
    CALL OPF(FN.AC,F.AC)

    T.SEL1 = "SELECT ":FN.POS:" WITH COMPANY.CODE NE 'EG0010099' AND AMOUNT.FCY NE '' AND @ID LIKE ...":V.DAT:"... BY DATE.TIME"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
*    TEXT = "SELECTED1 = ":SELECTED1  ; CALL REM
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.POS,KEY.LIST1<I>,R.POS,F.POS,E1)
            CUR.BASE = ''
            REC.BRAN = R.POS<PSE.COMPANY.CODE>[2]
            REC.BRAN = R.POS<PSE.COMPANY.CODE>[2]
**********************20090118*****************
            COMP = R.POS<PSE.COMPANY.CODE>        ;*C$ID.COMPANY
            COM.CODE = COMP[8,2]
*            IF COMP EQ 'EG0010001' THEN
*                OFS.USER.INFO = "/"
*            END ELSE
            OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*            END
**********************20090118*****************

            REC.ID = R.POS<PSE.TRANS.REFERENCE>

            IF R.POS<PSE.SYSTEM.ID> = 'FT' THEN
                GOSUB GETFT
            END

            IF R.POS<PSE.SYSTEM.ID> = 'TT' THEN
                GOSUB GETTT
            END

            IF R.POS<PSE.SYSTEM.ID> = 'LCD' THEN
                GOSUB GETLCD
            END

            IF R.POS<PSE.SYSTEM.ID> = 'LCM' THEN
                GOSUB GETLCM
            END

*            IF R.POS<PSE.SYSTEM.ID> = 'MLT' THEN
*                GOSUB GETMLT
*            END

        NEXT I
    END
    RETURN

**************************************************************
GETFT:
*----
    SW1 = '' ; REC.ID1 = ''
    CALL F.READ(FN.FT,REC.ID,R.FT,F.FT,E2)
*============================================
    IF E2 THEN
*============================================
        REC.ID1 = REC.ID:";2"
        CALL F.READ(FN.FTH,REC.ID1,R.FT,F.FTH,E7)
        IF R.FT<FT.PROCESSING.DATE> LT TODAY THEN
            IF R.FT<FT.RECORD.STATUS> = 'REVE' THEN
*============================================
                T.SEL2 = '' ; KEY.LIST2 = '' ; SELECTED2 = ''
                ER.MSG2 = ''
*============================================
                T.SEL2 = "SELECT ":FN.FTH:" WITH DEBIT.THEIR.REF EQ ":REC.ID
                T.SEL2 := " AND PROCESSING.DATE EQ ":R.FT<FT.PROCESSING.DATE>
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
                IF SELECTED2 THEN
                    GOSUB CR.REV.OFS
                END
            END
        END
    END ELSE
*============================================
        TR.TYPE = R.FT<FT.TRANSACTION.TYPE>
        IF TR.TYPE NE 'AC44' THEN
            IF R.FT<FT.DEBIT.CURRENCY> # R.FT<FT.CREDIT.CURRENCY> THEN
                IF R.FT<FT.DEBIT.ACCT.NO>[1,2] # 'PL' AND R.FT<FT.CREDIT.ACCT.NO>[1,2] # 'PL' THEN
                    REC.COMM.AMT = R.FT<FT.COMMISSION.CODE>
                    REC.COMM.CUR = REC.COMM.AMT[1,3]
                    REC.COMM.AMT = REC.COMM.AMT[4,LEN(REC.COMM.AMT)]

                    REC.CHRG.AMT = R.FT<FT.CHARGE.AMT>
                    REC.CHRG.CUR = REC.CHRG.AMT[1,3]
                    REC.CHRG.AMT = REC.CHRG.AMT[4,LEN(REC.CHRG.AMT)]

                    IF R.FT<FT.DEBIT.AMOUNT> THEN
                        REC.AMT = R.FT<FT.DEBIT.AMOUNT>
                        SW1 = '1'
                    END ELSE
                        REC.AMT = R.FT<FT.CREDIT.AMOUNT>
                        SW1 = '2'
                    END

                    REC.DR.CUR = R.FT<FT.CREDIT.CURRENCY>
                    REC.CR.CUR = R.FT<FT.DEBIT.CURRENCY>

                    IF R.FT<FT.DEBIT.AMOUNT> THEN
                        IF R.FT<FT.COMMISSION.CODE> = "DEBIT PLUS CHARGES" THEN
                            IF R.FT<FT.COMMISSION.FOR> = "RECEIVER" THEN
                                IF REC.COMM.CUR = R.FT<FT.DEBIT.CURRENCY> THEN
                                    REC.AMT -= REC.COMM.AMT
                                END
                            END
                        END

                        IF R.FT<FT.CHARGE.CODE> = "DEBIT PLUS CHARGES" THEN
                            IF R.FT<FT.CHARGE.FOR> = "RECEIVER" THEN
                                IF REC.CHRG.CUR = R.FT<FT.DEBIT.CURRENCY> THEN
                                    REC.AMT -= REC.CHRG.AMT
                                END
                            END
                        END


                    END ELSE

                        IF R.FT<FT.COMMISSION.CODE> = "CREDIT LESS CHARGES" THEN
                            IF R.FT<FT.COMMISSION.FOR> = "RECEIVER" THEN
                                IF REC.COMM.CUR = R.FT<FT.CREDIT.CURRENCY> THEN
                                    REC.AMT += REC.COMM.AMT
                                END
                            END
                        END

                        IF R.FT<FT.CHARGE.CODE> = "CREDIT LESS CHARGES" THEN
                            IF R.FT<FT.CHARGE.FOR> = "RECEIVER" THEN
                                IF REC.CHRG.CUR = R.FT<FT.CREDIT.CURRENCY> THEN
                                    REC.AMT += REC.CHRG.AMT
                                END
                            END
                        END
                    END

                    IF R.FT<FT.CHARGE.CODE> = "DEBIT PLUS CHARGES" AND R.FT<FT.CREDIT.AMOUNT> THEN
                        IF R.FT<FT.CREDIT.CURRENCY> # LCCY THEN
                            REC.AMT += R.FT<FT.TOT.REC.CHG.CRCCY>
                        END
                    END

                    IF REC.DR.CUR = LCCY OR REC.CR.CUR = LCCY THEN
                        IF REC.CR.CUR # LCCY THEN
                            IF TR.TYPE NE 'AC43' THEN
                                REC.DR.ACC = REC.DR.CUR:"1110200010099"
                                REC.CR.ACC = REC.CR.CUR:"1110100010099"
                            END
                            IF TR.TYPE EQ 'AC43' THEN
                                REC.DR.ACC = REC.DR.CUR:"1110500010099"
                                REC.CR.ACC = REC.CR.CUR:"1110400010099"
                            END

                        END ELSE
                            IF TR.TYPE NE 'AC43' THEN
                                REC.CR.ACC = REC.CR.CUR:"1110200010099"
                                REC.DR.ACC = REC.DR.CUR:"1110100010099"
                            END
                            IF TR.TYPE EQ 'AC43' THEN
                                REC.CR.ACC = REC.CR.CUR:"1110500010099"
                                REC.DR.ACC = REC.DR.CUR:"1110400010099"

                            END
                        END

***************************************************
                    END ELSE
                        REC.DR.ACC = REC.DR.CUR:"1110300010099"
                        REC.CR.ACC = REC.CR.CUR:"1110300010099"
                    END
*            REC.RATE = R.FT<FT.TREASURY.RATE>
                    REC.RATE = R.FT<FT.CUSTOMER.RATE>
                    CUR.BASE = R.FT<FT.BASE.CURRENCY>
                    GOSUB CR.TT.OFS
                END
            END
        END
    END
    RETURN
*-------------------------------------------------------------------
GETTT:
*----
    SW1 = ''
*        REC.ID = REC.ID:";1"
    CALL F.READ(FN.TT,REC.ID,R.TT,F.TT,E2)

    IF R.TT<TT.TE.CURRENCY.1> # R.TT<TT.TE.CURRENCY.2> THEN
        IF R.TT<TT.TE.ACCOUNT.1>[1,2] # 'PL' AND R.TT<TT.TE.ACCOUNT.2>[1,2] # 'PL' THEN

            IF  R.TT<TT.TE.CURRENCY.1> = LCCY OR R.TT<TT.TE.CURRENCY.2> = LCCY THEN

                IF R.TT<TT.TE.AMOUNT.FCY.1> THEN
                    REC.AMT = R.TT<TT.TE.AMOUNT.FCY.1>
                END ELSE
                    REC.AMT = R.TT<TT.TE.AMOUNT.FCY.2>
                END
                IF R.TT<TT.TE.TRANSACTION.CODE> = '23' OR R.TT<TT.TE.TRANSACTION.CODE> = '67' OR R.TT<TT.TE.TRANSACTION.CODE> = 58 THEN
                    REC.DR.CUR = R.TT<TT.TE.CURRENCY.2>
                    REC.CR.CUR = R.TT<TT.TE.CURRENCY.1>

                    REC.DR.ACC = REC.DR.CUR:"1110200010099"
                    REC.CR.ACC = REC.CR.CUR:"1110100010099"

                    SW1 = '1'
                END
                IF R.TT<TT.TE.TRANSACTION.CODE> = '24' OR R.TT<TT.TE.TRANSACTION.CODE> = '68' THEN
                    REC.DR.CUR = R.TT<TT.TE.CURRENCY.1>
                    REC.CR.CUR = R.TT<TT.TE.CURRENCY.2>

                    REC.CR.ACC = REC.CR.CUR:"1110200010099"
                    REC.DR.ACC = REC.DR.CUR:"1110100010099"

                    SW1 = '2'
                END




                REC.RATE = R.TT<TT.TE.DEAL.RATE>
                GOSUB CR.TT.OFS
            END
        END
    END
    RETURN

*-------------------------------------------------------------------
GETLCD:
*------
    SW1 = '' ; SW3 = ''
    CALL F.READ(FN.LCD,REC.ID,R.LCD,F.LCD,E2)

*--------------------------------------
    IF E2 THEN
        T.SEL3  = "SELECT ":FN.LCDH:" WITH @ID LIKE ":REC.ID:"... AND RECORD.STATUS EQ 'REVE'"
        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
***REC.IDH = REC.ID : ";" : SELECTED3
***CALL F.READ(FN.LCDH,REC.IDH,R.LCDH,F.LCDH,E6)
        IF SELECTED3 THEN
            T.SEL4 = "SELECT ":FN.FTH:" WITH DEBIT.THEIR.REF EQ ":REC.ID
            CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
            IF SELECTED4 THEN
                KEY.LIST2 = ''
                KEY.LIST2<1> = KEY.LIST4<1>
                GOSUB CR.REV.OFS
            END
        END

    END ELSE

        CALL F.READ(FN.AC,R.LCD<TF.DR.DRAWDOWN.ACCOUNT>,R.AC,F.AC,E3)

        IF R.LCD<TF.DR.DRAW.CURRENCY> # R.AC<AC.CURRENCY> THEN
            IF R.LCD<TF.DR.DRAW.CURRENCY> = R.POS<PSE.CURRENCY> THEN

                IF  R.LCD<TF.DR.DRAW.CURRENCY> = LCCY OR R.AC<AC.CURRENCY> = LCCY THEN
                    IF R.AC<AC.CURRENCY> # LCCY THEN
                        REC.DR.CUR = R.LCD<TF.DR.DRAW.CURRENCY>
                        REC.CR.CUR = R.AC<AC.CURRENCY>
                        REC.DR.ACC = REC.DR.CUR:"1110200010099"
                        REC.CR.ACC = REC.CR.CUR:"1110100010099"
                        REC.AMT    = R.POS<PSE.AMOUNT.LCY>
                        SW1 = '2'
                    END ELSE
                        REC.DR.CUR = R.LCD<TF.DR.DRAW.CURRENCY>
                        REC.CR.CUR = R.AC<AC.CURRENCY>
                        REC.CR.ACC = REC.CR.CUR:"1110200010099"
                        REC.DR.ACC = REC.DR.CUR:"1110100010099"
                        REC.AMT    = R.POS<PSE.AMOUNT.FCY>
                        SW1 = '2'
                    END
                    REC.RATE = R.POS<PSE.CROSS.RATE>
                END ELSE
                    IF R.LCD<TF.DR.DRAW.CURRENCY> = R.POS<PSE.CURRENCY> THEN
                        REC.DR.CUR = R.LCD<TF.DR.DRAW.CURRENCY>
                        REC.CR.CUR = R.AC<AC.CURRENCY>
                        REC.DR.ACC = REC.DR.CUR:"1110300010099"
                        REC.CR.ACC = REC.CR.CUR:"1110300010099"
                        REC.AMT    = R.POS<PSE.AMOUNT.FCY>
                        SW1 = '2'
                        REC.RATE = R.LCD<TF.DR.RATE.BOOKED>
                    END
                END
                IF REC.AMT LT 0 THEN REC.AMT = REC.AMT * -1
*======================= ADD CHARGES AND COMMISSION TO POSITION ADD AT 20081217
                AMT.CHRG = '' ; AMT.CHRG1 = ''
                CHRG.CURR = R.LCD<TF.DR.OTHER.CHARGES>
*Line [ 400 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                XX= DCOUNT(CHRG.CURR,@VM)
                FOR KK  = 1 TO XX

                    IF INDEX(CHRG.CURR<1,KK>,REC.DR.CUR,1) THEN
                        AMT.CHRG1 = FIELD(CHRG.CURR<1,KK>,REC.DR.CUR,2)
                        AMT.CHRG1 = TRIM(AMT.CHRG1,' ','L')
                        IF REC.DR.CUR EQ 'JPY' THEN
                            AMT.CHRG = ''
                            AAA = LEN(AMT.CHRG1)
                            FOR NNN = 1 TO AAA
                                YYY = AMT.CHRG1[NNN,1]
                                IF NOT(NUM(YYY)) THEN
                                    NNN = AAA
                                END ELSE
                                    AMT.CHRG = AMT.CHRG:YYY
                                END
                            NEXT NNN
                        END ELSE
                            AMT.CHRG = FIELD(AMT.CHRG1,".",1):".":FIELD(AMT.CHRG1,".",2)[1,2]
                        END
                        REC.AMT += AMT.CHRG
                    END
                    SW3 = 1
                NEXT KK
                IF SW3 = 1 THEN REC.RATE = R.LCD<TF.DR.RATE.BOOKED>
*==============================================================
*            IF REC.AMT GT 0 THEN
                GOSUB CR.TT.OFS
*            END
            END
        END
    END
    RETURN

*-------------------------------------------------------------------
GETLCM:
*------
    SW1 = ''

    FN.AC1 = 'FBNK.ACCOUNT' ; F.AC1 = '' ; R.AC1 = ''
    CALL OPF(FN.AC1,F.AC1)

    CALL F.READ(FN.LCM,REC.ID,R.LCM,F.LCM,E4)

*    R.LCM<TF.LC.PROVIS.ACC>
*    R.LCM<TF.LC.PROVIS.AMOUNT>
*    R.LCM<TF.LC.CREDIT.PROVIS.ACC

    CALL F.READ(FN.AC,R.LCM<TF.LC.PROVIS.ACC>,R.AC,F.AC,E5)
    CALL F.READ(FN.AC1,R.LCM<TF.LC.CREDIT.PROVIS.ACC>,R.AC1,F.AC1,E6)

    IF NOT(E5) AND NOT(E6) THEN
        CHK.AMOUNT = R.POS<PSE.AMOUNT.FCY>
        IF CHK.AMOUNT LT 0 THEN CHK.AMOUNT = CHK.AMOUNT * -1
        IF CHK.AMOUNT = R.LCM<TF.LC.PROVIS.AMOUNT> THEN
*TEXT = R.LCM<TF.LC.PROVIS.AMOUNT> ; CALL REM
*TEXT = R.POS<PSE.AMOUNT.FCY> ; CALL REM
            IF R.AC1<AC.CURRENCY> # R.AC<AC.CURRENCY> THEN


*          REC.COMM.AMT = R.FT<FT.COMMISSION.CODE>
*          REC.COMM.CUR = REC.COMM.AMT[1,3]
*          REC.COMM.AMT = REC.COMM.AMT[4,LEN(REC.COMM.AMT)]

*          REC.CHRG.AMT = R.FT<FT.CHARGE.AMT>
*          REC.CHRG.CUR = REC.CHRG.AMT[1,3]
*          REC.CHRG.AMT = REC.CHRG.AMT[4,LEN(REC.CHRG.AMT)]

*          REC.AMT = R.LCD<TF.DR.PAYMENT.AMOUNT>

                IF  R.AC1<AC.CURRENCY> = LCCY OR R.AC<AC.CURRENCY> = LCCY THEN
                    IF R.POS<PSE.AMOUNT.FCY> LT 0 THEN
                        REC.DR.CUR = R.AC<AC.CURRENCY>
                        REC.CR.CUR = R.AC1<AC.CURRENCY>
                        REC.DR.ACC = REC.DR.CUR:"1110200010099"
                        REC.CR.ACC = REC.CR.CUR:"1110100010099"
                        REC.AMT    = CHK.AMOUNT
                        SW1 = '2'
                    END ELSE
                        REC.DR.CUR = R.AC1<AC.CURRENCY>
                        REC.CR.CUR = R.AC<AC.CURRENCY>
                        REC.CR.ACC = REC.CR.CUR:"1110200010099"
                        REC.DR.ACC = REC.DR.CUR:"1110100010099"
                        REC.AMT    = R.POS<PSE.AMOUNT.FCY>
                        SW1 = '2'
                    END
                    REC.RATE = R.LCM<TF.LC.PROV.EXCH.RATE>
                END ELSE
                    IF R.AC1<AC.CURRENCY> = R.POS<PSE.CURRENCY> THEN
                        REC.DR.CUR = R.AC1<AC.CURRENCY>
                        REC.CR.CUR = R.AC<AC.CURRENCY>
                        REC.DR.ACC = REC.DR.CUR:"1110300010099"
                        REC.CR.ACC = REC.CR.CUR:"1110300010099"
                        REC.AMT    = R.POS<PSE.AMOUNT.FCY>
                        SW1 = '2'
                        REC.RATE = R.LCM<TF.LC.PROV.EXCH.RATE>
                    END
                END
                IF REC.AMT LT 0 THEN REC.AMT = REC.AMT * -1
                GOSUB CR.TT.OFS
            END
        END
    END
    RETURN

*-------------------------------------------------------------------
CR.TT.OFS:
*********
    DATEE = TODAY
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC10":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":REC.DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":REC.CR.CUR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":REC.DR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":REC.CR.ACC:COMMA

    IF SW1 = '1' THEN
        OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT=":REC.AMT:COMMA
    END
    IF SW1 = '2' THEN
        OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":REC.AMT:COMMA
    END

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "TREASURY.RATE=":REC.RATE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    IF CUR.BASE THEN
        OFS.MESSAGE.DATA :=  "BASE.CURRENCY=":CUR.BASE:COMMA
    END
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":REC.ID


    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
* UPDATED BY MOHAMED SABRY 2014/04/14
*    OFS.ID = "T":TNO:".P.":REC.ID
    OFS.ID = "T":TNO:".P.":"FREE.MARKET-":TODAY:"-":REC.ID

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    RETURN
************************************************************
CR.REV.OFS:
*********
**************************************************CRAETE FT BY OFS**********************************************
    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:"SCB1/R":COMMA:OFS.USER.INFO:COMMA:KEY.LIST2<1>
*    OFS.ID = "T":TNO:".P.REV.":REC.ID
* UPDATED BY MOHAMED SABRY 2014/04/14
    OFS.ID = "T":TNO:".P.REV.":"FREE.MARKET-":TODAY:"-":REC.ID

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    RETURN
************************************************************
************************************************************
