* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>279</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.MULTI.RET

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

    IF MESSAGE EQ "" THEN

        IF COMI THEN
            NOM    = COMI
            ACC    = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1>

            FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
            CALL OPF( FN.BR,F.BR)

            BR.ID = R.NEW(INF.MLT.BILL.NO)

            CALL F.READ(FN.BR,BR.ID, R.BR, F.BR, ETEXT)

*COL.DAT =  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.REAL.COLL.DATE>

            COL.DAT = TODAY
***--------------------------

            COMP<1,1> = "EG0010040"
            COMP<1,2> = "EG0010050"
            ID.COMP   = ID.COMPANY
            FINDSTR ID.COMP IN COMP SETTING IDD THEN
                NEW.VAL.DATE = TODAY
            END  ELSE
                CALL CDT("",COL.DAT,'+1W')
                NEW.VAL.DATE = COL.DAT
            END


***-------------------------

            R.NEW(INF.MLT.VALUE.DATE)<1,1>     = NEW.VAL.DATE
            T.SEL  = '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''

            T.SEL  = "SELECT F.SCB.RETIREMENTS WITH @ID EQ ": ACC :'.':NOM
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
            FN.RET ='F.SCB.RETIREMENTS' ;R.RET ='';F.RET =''
            CALL OPF(FN.RET,F.RET)

            CALL F.READ(FN.RET,KEY.LIST, R.RET, F.RET,E)
*Line [ 82 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DCOUNT.RET = DCOUNT (R.RET<SCB.RET.ACCOUNT>,@VM)

            FOR I  = 1 TO DCOUNT.RET + 1
                H  = I + 1
                HH = DCOUNT.RET + 2

                R.NEW(INF.MLT.SIGN)<1,H>            = 'CREDIT'
                R.NEW(INF.MLT.TXN.CODE)<1,H>        = 79
                R.NEW(INF.MLT.CURRENCY.MARKET)<1,H> = 1
                R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,H>  = R.RET<SCB.RET.ACCOUNT><1,I>
                R.NEW(INF.MLT.AMOUNT.LCY)<1,H>      = R.RET<SCB.RET.AMOUNT><1,I>
                R.NEW(INF.MLT.CURRENCY)<1,H>        = 'EGP'
                R.NEW(INF.MLT.VALUE.DATE)<1,H>      = NEW.VAL.DATE
                R.NEW(INF.MLT.EXPOSURE.DATE)<1,H>   = NEW.VAL.DATE

                AMT  + = R.NEW(INF.MLT.AMOUNT.LCY)<1,H>
            NEXT I
            PL.AMT =  R.NEW(INF.MLT.AMOUNT.LCY)<1,1> - AMT
            R.NEW(INF.MLT.SIGN)<1,HH>               = 'CREDIT'
            R.NEW(INF.MLT.TXN.CODE)<1,HH>           = 79
            R.NEW(INF.MLT.CURRENCY.MARKET)<1,HH>    = 1
            R.NEW(INF.MLT.PL.CATEGORY)<1,HH>        = "52014"
            R.NEW(INF.MLT.AMOUNT.LCY)<1,HH>         = PL.AMT
            R.NEW(INF.MLT.VALUE.DATE)<1,HH>         = NEW.VAL.DATE
            R.NEW(INF.MLT.EXPOSURE.DATE)<1,HH>       = NEW.VAL.DATE
            CALL REBUILD.SCREEN

        END
    END
    RETURN
END
