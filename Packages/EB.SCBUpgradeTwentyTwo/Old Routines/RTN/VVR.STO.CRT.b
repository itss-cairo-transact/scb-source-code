* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.STO.CRT

*TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    GOSUB INTIAL
    GOSUB MAIN.PROG
    RETURN
****************MAIN PROGRAM************************
MAIN.PROG:
*---------
    CHQ.NUMBER = COMI
    IF NOT(CHQ.NUMBER) THEN
        RETURN
    END
    GOSUB CHQ.STOP
    IF CURR = "1" THEN
        RETURN
    END ELSE
        GOSUB CRF.CHQ.ISSUE
        IF NOT(SELECTED) THEN
            RETURN
        END
    END
    RETURN
*************************************************************************************************
INTIAL:
*-----
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT ='' ; CHQ.NUMBER = ''
    RETURN
********************************************************************************************************
CHQ.STOP:
*--------
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID.STOP = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:"*":CHQ.NUMBER
****CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
    CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>,CHQ.TYPE)
    TY = CHQ.TYPE
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT (TY,@VM)
    FOR X = 1 TO DD
        CHQ.ID = CHQ.TYPE<1,X>:'.':R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:'.':COMI
        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
        IF CURR THEN
            ETEXT = "����� ��� (&) ����� ":@FM:CHQ.NUMBER ;CALL STORE.END.ERROR
        END
    NEXT X
****END OF UPDATE 8/3/2016*****************************
    RETURN
***************************************************************************
CRF.CHQ.ISSUE:
*-------------
    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:".":CHQ.NUMBER
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    OLDCHQ = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>

    CALL F.READ(FN.CHQ.PRESENT,SCB.CHQ.ID,R.CHQ.PRESENT,F.CHQ.PRESENT,E1)

    IF E1 THEN
        ETEXT = '�� ���� ��� ����� ����� ���� �����'
        RETURN
    END ELSE
        GOSUB CRF.CHQ.PAID
    END
    RETURN
***************************************************************************
CRF.CHQ.PAID:
*------------

    CHQ.STAT     = R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS>
    CHQ.TRNS.PAY = R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT>
    CHQ.PAY.DATE = R.CHQ.PRESENT<DR.CHQ.PAY.DATE>
    CHQ.PAY.BRN  = R.CHQ.PRESENT<DR.CHQ.PAY.BRN>
    CHQ.AMT      = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
    CHQ.REC.DATE = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
    CHQ.BEN      = R.CHQ.PRESENT<DR.CHQ.BEN>

    IF CHQ.STAT EQ '4' THEN
        ETEXT = "����� �����"
        CALL STORE.END.ERROR
    END

    IF CHQ.STAT = 2 AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
        ETEXT = "����� ��� ����" ; CALL STORE.END.ERROR
    END ELSE
        IF CHQ.STAT = 3 AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
            ETEXT = "����� �� ������" ; CALL STORE.END.ERROR
        END ELSE
            R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF>  = R.CHQ.PRESENT<DR.CHQ.ISSUE.BRN>
            R.NEW(FT.DEBIT.CURRENCY)                = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
            R.NEW(FT.DEBIT.ACCT.NO)                 = R.CHQ.PRESENT<DR.CHQ.NOS.ACCT>
            R.NEW(FT.DEBIT.AMOUNT)                  = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
            R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE> = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
            R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>         = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>
            R.NEW(FT.PROFIT.CENTRE.DEPT)                  = R.USER< EB.USE.DEPARTMENT.CODE>
            CALL REBUILD.SCREEN
        END
    END
    RETURN
***************************************************************************
END
