* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN AHMED 14/07/2002,  -----
*-----------------------------------------------------------------------------
* <Rating>664</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.ACCOUNT.FIELD

*TO CHECK IF FIELD LENGTH # 17 AND IT IS  A NOSTRO OR INTERNAL ACCOUNT
*THEN ERROR ELSE DEFAULT NARRATIVE WITH CUSTOMER.SHORT.NAME AND CURRENCY.1 WITH THE CURRENCY IN ACCOUNT.2
*IF CURRENCY IS LOCAL CURR THEN LET FIELD AMOUNT.FCY IS NOINPUT
*ELSE LET AMOUNT.LOCAL IS NOINPUT
*DEFAULT EXPOSURE.DATE.2 AND VALUE.DATE.2 EQ TODAY



*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*   IF MESSAGE = 'VAL' THEN
*       RETURN
*   END
    IF COMI THEN
        ETEXT = ''
        IF  LEN(COMI) = 16  THEN
            GOSUB INTIAL
******Updated by Nessreen Ahmed 18/4/2016 for R15***************************
******CALL DBR( 'AC.COLLATERAL':@FM:1,COMI,ACCFND)
****End of Update 18/4/2016 for R15 ****************************
* IF ACCFND THEN ETEXT = '��� ������ �� �����' ; CALL STORE.END.ERROR
            CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
*************NESSREEN AHMED 30/8/2006
            IF CATEG = '1001' THEN
                CALL DBR( 'ACCOUNT':@FM:AC.POSTING.RESTRICT,COMI,POST)
* TEXT = 'POST=':POST ; CALL REM
                IF POST # '' THEN ETEXT = 'THIS.ACCOUNT.IS.BLOCKED' ;CALL STORE.END.ERROR
            END
            GOSUB CHECK.IF.NOS.OR.INT

***         CALL DBR( 'ACCOUNT':@FM:AC.CONDITION.GROUP,COMI,GROUP1)
* IF GROUP1 = '22' THEN ETEXT = 'NOT.ALLOWED.GROUP'
        END ELSE
* ETEXT = 'Invalid.Mask'
            ETEXT='��� �� ����� ������'
        END

    END


    GOTO PROGRAM.END

***********************************************************************************************************************

INTIAL:

*-------------TO INTIALIZE ALL VARIABLES -----------------------------------------*

    ETEXT = '' ; E = '' ; CURR.NAME1 = '' ; SHOR.NAME1 = ''


    RETURN
***********************************************************************************************************************

CHECK.IF.NOS.OR.INT:


*-------------TO CHECK IF IT IS NOSTRO OR INTERNAL ACCOUNT------------------------*

    IF COMI # R.NEW(TT.TE.ACCOUNT.1)<1,AV> THEN GOSUB CLEAN.FIELDS

    IF NOT(NUM(COMI)) THEN ETEXT='��� ����� ������ ������ ������'
*ETEXT = 'Not.Allowed.For.Internal.Account'
*CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)

*    IF CATEG >= '5000' AND CATEG <='5999' THEN ETEXT='��� ����� ������ ������ ������'
*ETEXT = 'Not.Allowed.For.NOSTRO.Account'
    ELSE GOSUB GET.CUSNAME.CURR



    RETURN
* **********************************************************************************************************************

GET.CUSNAME.CURR:

    ETEXT = ''
    CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY,COMI,CURR.NAME1)
* CALL DBR( 'NUMERIC.CURRENCY':@FM:1,COMI[9,2],CURR.NAME1)
    IF ETEXT THEN E = ETEXT

    R.NEW(TT.TE.CURRENCY.1) = CURR.NAME1
    R.NEW(TT.TE.CURRENCY.2) = CURR.NAME1

    ETEXT = ''

*CUST.NO = TRIM( COMI[ 5, 7], '0', 'L')
******UPDATED BY NESSREEN AHMED 4/3/2013******************
    IF MESSAGE = '' THEN
        CALL DBR( 'ACCOUNT':@FM:AC.SHORT.TITLE,COMI,SHOR.NAME1)
        IF ETEXT THEN E = ETEXT
        R.NEW(TT.TE.NARRATIVE.2)<1,AV> = SHOR.NAME1
    END
******END OF UPDATE 4/3/2013*******************************

    GOSUB CHECK.IF.CURR.LOCAL


    RETURN

******************************************************************************************************************
CHECK.IF.CURR.LOCAL:

    IF CURR.NAME1 = LCCY THEN

        T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
        T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'

    END ELSE

        T(TT.TE.AMOUNT.FCY.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
        T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'

    END

    GOSUB DEF.DAT

    RETURN

**********************************************************************************************************************
DEF.DAT:

    R.NEW(TT.TE.VALUE.DATE.1) = TODAY
* R.NEW(TT.TE.EXPOSURE.DATE.2) = TODAY


    RETURN
***********************************************************************************************************************
CLEAN.FIELDS:

    R.NEW(TT.TE.ACCOUNT.1)<1,AV> = ''
    R.NEW(TT.TE.ACCOUNT.2) = ''
    R.NEW(TT.TE.CURRENCY.1)<1,AV>= ''
    R.NEW(TT.TE.CURRENCY.2)= ''
    R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
    R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
    R.NEW(TT.TE.NARRATIVE.2)<1,AV> = ''

    RETURN

***********************************************************************************************************************
PROGRAM.END:

    CALL REBUILD.SCREEN

    RETURN
END
