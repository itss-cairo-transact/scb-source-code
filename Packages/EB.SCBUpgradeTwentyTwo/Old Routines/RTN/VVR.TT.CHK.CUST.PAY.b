* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN  -----
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.CUST.PAY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

*TO Check if the Customer is Already Paid the Charges for New Account or not

    CALL DBR( 'CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,COMI ,POST)
    IF POST = '' THEN ETEXT = '��� ������ ��� ������ ��� ������ �� ���'
********AHMED********************************
    CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,COMI,POST.REST)
    IF POST.REST = 12 OR POST.REST = 14 OR POST.REST = 16 OR POST.REST = 23 OR POST.REST = 25 OR POST.REST = 76 OR POST.REST = 71 OR POST.REST = 72 OR POST.REST = 78 OR POST.REST = 80 OR POST.REST = 90 OR POST.REST = 99 OR POST.REST= 70 OR POST.REST= 13 THEN
        CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST.REST,DESC)
        ETEXT = '���� ������ ':DESC ;CALL STORE.END.ERROR
    END
***********************************************
    RETURN
END
