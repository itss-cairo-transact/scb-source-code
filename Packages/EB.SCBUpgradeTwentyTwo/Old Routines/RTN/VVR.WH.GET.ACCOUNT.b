* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.WH.GET.ACCOUNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS

    IF MESSAGE NE "VAL" THEN
        IF COMI THEN
            IF R.NEW(SCB.WH.TRANS.COMMISSION.TYPE) OR R.NEW(SCB.WH.TRANS.CHARGE.TYPE) THEN
                CUST = R.NEW(SCB.WH.TRANS.CUSTOMER)
                IF LEN(CUST) EQ 7 THEN CUST = "0":CUST
                CURR = COMI
                CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,COMI,CURR)
                CATE = "1001"
                SER  = "01"

                R.NEW(SCB.WH.TRANS.ACCT.CHRG.COMM) = CUST:CURR:CATE:SER

                CALL DBR('ACCOUNT':@FM:1,R.NEW(SCB.WH.TRANS.ACCT.CHRG.COMM),ACCT.EXIST)
                IF NOT(ACCT.EXIST) THEN TEXT = "THERE IS NO CURRENT ACCOUNT" ; CALL REM

                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
