* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN  -----
*-----------------------------------------------------------------------------
* <Rating>443</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHQUES.PRESENTED

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUES.STOPPED
****END OF UPDATE 8/3/2016**************************
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP

*IF V$FUNCTION = 'I' THEN
    ETEXT = ''
    IF COMI  THEN
****T24 14/6/2006**** CHQ.ID = 'SCB.':R.NEW(TT.TE.ACCOUNT.2):'-':COMI
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID = 'SCB.':R.NEW(TT.TE.ACCOUNT.1):'-':COMI
        CHQ.ID = 'SCB.':R.NEW(TT.TE.ACCOUNT.1):'.':COMI
************************************************************************
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED, CHQ.ID, REPRESENT)
        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED, CHQ.ID, REPRESENT)
****END OF UPDATE 8/3/2016*****************************
        ETEXT = ''
        IF REPRESENT THEN ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
****T24 14/6/2006**** ACCT.NO =R.NEW(TT.TE.ACCOUNT.2)
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
*************************************************************************
*****UPDATED BY NESSREEN 22/04/2009*********************
**  CHQ='SCB':'.':ACCT.NO:'...'
**  CHQ.CUS = 'SCB':'.':ACCT.NO\
**  T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CUST = ACCT.NO[1,8]
        CHQ = 'SCB.':CUST
        T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED EQ '0' THEN
***   ETEXT='�� ���� ����� ����� ���� ������ &': CHQ
***   CALL STORE.END.ERROR
        END ELSE
            CALL DBR ('CHEQUE.REGISTER':@FM:CHEQUE.REG.CHEQUE.NOS, CHQ.CUS, CHQ.NOS)
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT (CHQ.NOS,@VM)
            FOR X = 1 TO DD
                CHN  = CHQ.NOS<1,X>
                LF   = FIELD(CHN ,"-", 1)
                RH   = FIELD(CHN ,"-", 2)
                IF COMI GE LF AND COMI LE RH THEN
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID.STOP = R.NEW(TT.TE.ACCOUNT.1):"*": COMI
*TEXT = "CHQ.ID.STOP =":CHQ.ID.STOP ; CALL REM
**** CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY, CHQ.ID.STOP, CURR)
                    CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(TT.TE.ACCOUNT.1),CHQ.TYPE)
                    TY = CHQ.TYPE
*Line [ 86 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    MB = DCOUNT (TY,@VM)
                    FOR Y = 1 TO MB
                        CHQ.ID = CHQ.TYPE<1,Y>:'.':R.NEW(TT.TE.ACCOUNT.1):'.':COMI
                        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
*TEXT = "CURR = ":CURR ; CALL REM
                        ETEXT = ''
                        IF CURR THEN
                            ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
                        END
                    NEXT Y
                    RETURN
                END ELSE
****    ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
                END
            NEXT DD
****T24 19/6/2006
* FN.PAYMENT.STOP = 'F.PAYMENT.STOP' ; F.PAYMENT.STOP ='' ; R.PAYMENT.STOP = '' ; E= ''
* CALL OPF(FN.PAYMEN.STOP,F.PAYMENT.STOP)
* CALL F.READ(FN.PAYMENT.STOP,ACCT.NO, R.PAYMENT.STOP, F.PAYMENT.STOP , E)
* FIRST.NO = R.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
* TEXT = 'FIRST.NO=':FIRST.NO ; CALL REM
* LAST.NO = R.PAYMENT.STOP<AC.PAY.LAST.CHEQUE.NO>
* CHQ.TYPE = R.PAYMENT.STOP<AC.PAY.CHEQUE.TYPE>
* STOP.TYPE = R.PAYMENT.STOP<AC.PAY.PAYM.STOP.TYPE>

* DD = DCOUNT (STOP.TYPE,VM)
* FOR X = 1 TO DD
*     IF CHQ.TYPE<1,X> = 'SCB' THEN
*         IF LAST.NO<1,X> = "" THEN
*             IF COMI = FIRST.NO<1,X> THEN ETEXT = '��� ����� �����' ; CALL STORE.END.ERROR
*         END ELSE
*             FOR I = FIRST.NO<1,X> TO LAST.NO<1,X>
*                 IF COMI = I THEN ETEXT = '��� ����� �����'; CALL STORE.END.ERROR
*             NEXT I
*         END
*     END
* NEXT X
* CALL F.RELEASE(FN.PAYMENT.STOP,ACCT.NO,F.PAYMENT.STOP)
*     CHQ.ID.STOP = R.NEW(TT.TE.ACCOUNT.1):"*": COMI
*    TEXT = "CHQ.ID.STOP =":CHQ.ID.STOP ; CALL REM
*    CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
*    TEXT = "CURR = ":CURR ; CALL REM
*    IF CURR THEN
*    ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
*    END
        END
    END
    RETURN
END
