* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
*********INGY**************

    SUBROUTINE VVR.TT.CHQ.CERT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

 * TO CHECK THAT THE CHEQUE TYPE EQUAL 5
 * IF VALUE IN THIS FIELD IS CHANGED THEN EMPTY FIELD DR.CHEQ AND CHEQUE.NO


    IF V$FUNCTION = "I" THEN

        IF COMI # '5 CHEQUE-��� � �����' THEN
            ETEXT = '���� �� ���� ��� ����� �����'
            CALL STORE.END.ERROR
        END

        IF COMI NE R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.TYPE> THEN
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.FT.DR.CHEQ>=''
            R.NEW(TT.TE.CHEQUE.NUMBER)=''
            CALL REBUILD.SCREEN
        END
        LINK.DATA<TNO> = COMI

    END
    RETURN
END
