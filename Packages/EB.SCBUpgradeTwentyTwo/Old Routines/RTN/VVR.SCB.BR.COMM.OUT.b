* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.SCB.BR.COMM.OUT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*------------------------------------------------------
    IF  R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP' THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = ''

        CURR.CO = R.NEW(EB.BILL.REG.CURRENCY)

        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE '0017' THEN
            IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'NO' ) THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '9943330010508001'
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '99433300105085':ID.COMPANY[8,2]
            END
        END

        IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN
            IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'NO' ) THEN
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE '0017' THEN
                    COM.AMT   = R.NEW(EB.BILL.REG.AMOUNT)
                    COM.ID    = "CHQCOLLOUT"
                    FN.FT.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
                    CALL OPF(FN.FT.COM,F.FT.COM)
                    CALL F.READ(FN.FT.COM,COM.ID,R.FT.COM,F.FT.COM,E1)

                    LOCATE CURR.CO IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                    PER  = R.FT.COM<FT4.PERCENTAGE,J>
                    MIN  = R.FT.COM<FT4.MINIMUM.AMT,J>
                    MAX  = R.FT.COM<FT4.MAXIMUM.AMT,J>

                    IF R.NEW(EB.BILL.REG.DRAWER) EQ '2300227' AND TODAY LE '20171221' THEN
                        MAX = "100" * COM.PR
                    END
                    COMM.AMT = (COM.AMT*PER/100)

                    IF COMM.AMT LT MIN THEN
                        COMM.AMT = MIN
                    END ELSE
                        IF COMM.AMT GT MAX THEN
                            COMM.AMT = MAX
                        END
                    END
                    CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
                    IF COMM.AMT LT MIN THEN
                        COMM.AMT = MIN
                    END ELSE
                        IF COMM.AMT GT MAX THEN
                            COMM.AMT = MAX
                        END
                    END
                    CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")

                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = "CHQCOLLOUT"
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR.CO:COMM.AMT


                    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
                    CALL OPF(FN.CUS,F.CUS)
                    CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
                    SEC  = R.CUS<EB.CUS.SECTOR>
                    IF ( SEC NE 1100 AND SEC NE 1200 ) THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = "CHQCOLLOUT"
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR.CO:COMM.AMT
                    END ELSE
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                    END

                    FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' ; E11=''
                    CALL OPF(FN.BR.COM,F.BR.COM)
                    BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)

                    CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)
                    IF NOT(E11) THEN
                        COM.AMT   = R.BR.COM<BR.CUS.COMM.AMT>
                        COM.PR    = R.BR.COM<BR.CUS.COMM.PREC>
                        CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>

                        IF CHRG.AMT EQ 'YES' OR COM.AMT EQ 0 THEN
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                        END

                        IF CHRG.AMT EQ 'YES' THEN
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
                            CALL REBUILD.SCREEN
                        END
                    END
                    CALL REBUILD.SCREEN
                END
            END
        END

        IF COMI EQ '0017' THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>    = ''
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
        END
        CALL REBUILD.SCREEN
    END
*----------------------------------------------------------------------
    RETURN
END
