* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>337</Rating>
*-----------------------------------------------------------------------------
**------Ox---BAKRY 21/07/2002---------**

    SUBROUTINE VVR.TT.ACC.CHEQ

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE

*To Check If account Changed Then fields account,currency,amount will be empty
*if it is customer account default fields currency,value date with today's date
*if PGM.VERSION eq SCB.DEPCHQ1 then internal account is not allowed

    ETEXT = ''
    IF MESSAGE # 'VAL' AND V$FUNCTION = 'I' THEN
        CURR.NAME = ''
        CATE = ''
        CURR = ''
***************************************************************************************
        IF COMI # R.NEW(TT.TE.ACCOUNT.1) THEN

            R.NEW(TT.TE.CURRENCY.1) = ''
            R.NEW(TT.TE.CURRENCY.2) = ''
            R.NEW(TT.TE.ACCOUNT.1) = ''
            R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
            R.NEW(TT.TE.AMOUNT.FCY.1) = ''
            R.NEW(TT.TE.DENOMINATION) = ''
            R.NEW(TT.TE.UNIT) = ''
        END
***************************************************************************************
* CURRENCY = COMI [3,2]
*  CU1.NAME = TRIM( COMI[ 5, 7], '0', 'L')
        IF NUM(COMI[1,3]) THEN
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR.NAME)
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,COMI,AC.NAME)

* IF   CATEG >= 5000 AND CATEG <= 5999  THEN
*   ETEXT = 'CATEGORY NOT ALLOWOED'
*     ETEXT='��� ����� ���� ������� ������'
* END

            IF CURR.NAME AND AC.NAME THEN
                R.NEW(TT.TE.CURRENCY.1) = CURR.NAME
                R.NEW(TT.TE.CURRENCY.2) = CURR.NAME
                R.NEW(TT.TE.VALUE.DATE.1) = TODAY
                R.NEW(TT.TE.NARRATIVE.2)<1,2> = ''
                R.NEW(TT.TE.NARRATIVE.2)<1,1> = AC.NAME
            END
        END
***************************************************************************************
        IF CURR.NAME = LCCY THEN
            T(TT.TE.AMOUNT.LOCAL.1)<3> =''
            T(TT.TE.AMOUNT.FCY.1)<3> ='NOINPUT'
            R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
        END ELSE
            T(TT.TE.AMOUNT.FCY.1)<3> =''
            T(TT.TE.AMOUNT.LOCAL.1)<3> ='NOINPUT'
            R.NEW(TT.TE.AMOUNT.FCY.1) = ''
        END
***************************************************************************************
        IF PGM.VERSION = ",SCB.DEPCHQ1" THEN
            R.NEW(TT.TE.CURRENCY.2) = CURR.NAME
            IF NOT(NUM(COMI[1,3])) THEN ETEXT='��� ����� ��������� ��������'
*ETEXT = "INTERNAL ACCOUNT NOT ALLAWOED"
        END
***************************************************************************************
        IF R.USER<EB.USE.LANGUAGE> = "2" THEN
            CALL DBR("ACCOUNT":@FM:4,COMI,AC.NAME)
            CALL REBUILD.SCREEN
        END
*************************************************************************
*IF COMI[3,2] # R.NEW(TT.TE.ACCOUNT.2)[3,2] THEN ETEXT = 'YOU CANT USE DIFFERENT CURRENCY'
        CALL REBUILD.SCREEN
        TYPE='SCB'
        CALL DBR("CHEQUE.TYPE":@FM:CHEQUE.TYPE.CATEGORY,TYPE,MYCATEG)
        LOCATE CATEG IN  MYCATEG<1,1>  SETTING I THEN
        END ELSE
*    ETEXT='��� ������� ��� �������'
*  ETEXT='Category.Not.Found.In.Cheque.Type'
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
