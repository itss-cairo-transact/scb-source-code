* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>580</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/10 ***
*******************************************

    SUBROUTINE VVR.MUA.CHK.HMM
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MAINMENU
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SMS.GROUP

*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

*IF MESSAGE # 'VAL' THEN
    GOSUB INITIATE
    GOSUB GET.CHK.DATA
* END
    WS.USR.CO = R.NEW(MUA.DEPARTMENT.CODE)
    WS.MOD.USR = WS.COMP[2]

    CALL REBUILD.SCREEN
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.USR  = "F.USER"               ; F.USR   = ""
    CALL OPF (FN.USR,F.USR)

    FN.UA   = "F.USER.ABBREVIATION"  ; F.UA  = "" ; R.UA = ""
    CALL OPF (FN.UA,F.UA)


    FN.SMS = 'F.USER.SMS.GROUP' ; F.SMS = ''
    CALL OPF(FN.SMS,F.SMS)

    WS.USR.CO           = R.NEW(MUA.DEPARTMENT.CODE)
    WS.MOD.USR          = WS.COMP[2]
    WS.USR.SCB.DEPT     = R.NEW(MUA.DEP.ACCT.CODE)
    WS.FUN              = FIELD(COMI,".",1)
    WS.FLG              = 0
    RETURN
*-----------------------------------------------------
GET.CHK.DATA:
    IF (( R.NEW(MUA.HMM.ID)<1,AV> LT 400 ) OR ( R.NEW(MUA.HMM.ID)<1,AV> GT 499 )) THEN
        ETEXT = '�� ���� ������� ��� ������� ������'
        CALL STORE.END.ERROR
        RETURN
    END ELSE
*WS.FUN = FIELD(COMI,".",1)
        IF WS.FUN # 'S' THEN
            WS.SMS.ID = WS.FUN:R.NEW(MUA.HMM.ID)<1,AV>
            CALL F.READ(FN.SMS,WS.SMS.ID,R.SMS,F.SMS,ER.SMS)
            IF NOT(ER.SMS) THEN
                IF R.NEW(MUA.HMM.ID)<1,AV> EQ 460 THEN
                    ETEXT = '��� ������� ��������� ���'
                    CALL STORE.END.ERROR
                    RETURN
                END
                IF WS.USR.SCB.DEPT NE '5100' AND WS.USR.SCB.DEPT NE '5200' THEN
                    IF WS.SMS.ID EQ 'A443' THEN
                        ETEXT = '�� ���� ������� ��� �������� �� ������� �������'
                        CALL STORE.END.ERROR
                        RETURN
                    END
                    IF R.NEW(MUA.HMM.ID)<1,AV> GE 441 AND R.NEW(MUA.HMM.ID)<1,AV> LE 449 THEN
                        IF R.NEW(MUA.TELLER.ID) EQ '' THEN
                            ETEXT = '��� ����� ��� ���� ������'
                            CALL STORE.END.ERROR
                            RETURN
                        END
                    END
                    IF R.NEW(MUA.HMM.ID)<1,AV> NE '444' THEN
                        IF R.NEW(MUA.HMM.ID)<1,AV> EQ '442' AND R.NEW(MUA.TELLER.ID)[2] NE '98' THEN
                            ETEXT = '�� ���� ������� ��� ���� ������ �� ������� ��������'
                            CALL STORE.END.ERROR
                            RETURN
                        END
                        IF R.NEW(MUA.HMM.ID)<1,AV> EQ '441' AND R.NEW(MUA.TELLER.ID)[2] NE '99' THEN
                            ETEXT = '�� ���� ������� ��� ���� ������ �� ������� ��������'
                            CALL STORE.END.ERROR
                            RETURN
                        END
                    END
                END
            END
            IF ER.SMS THEN
                ETEXT = '�� ���� ������� ��� �������� �� ������� �������'
                CALL STORE.END.ERROR
                RETURN
            END
        END
    END
    WS.HMM.ID  = R.NEW(MUA.HMM.ID)
*Line [ 131 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.HMM.CNT = DCOUNT (WS.HMM.ID,@VM)
    FOR IHMM = 1 TO WS.HMM.CNT
        IF WS.HMM.ID<1,IHMM> EQ R.NEW(MUA.HMM.ID)<1,AV> THEN
            WS.FLG ++
            IF WS.FLG GT 1 THEN
                ETEXT = '�� ���� ����� �������'
                CALL STORE.END.ERROR
                RETURN
            END
        END
    NEXT IHMM
*    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 491) THEN
*        IF (WS.USR.CO NE 1) AND (WS.USR.CO NE 4) THEN
*            ETEXT = '�� ���� ������� ��� �������'
*            CALL STORE.END.ERROR
*            RETURN
*        END
*    END
*    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 490) THEN
*        IF (WS.USR.CO EQ 1) OR (WS.USR.CO EQ 4) THEN
*            ETEXT = '�� ���� ������� ��� �������'
*            CALL STORE.END.ERROR
*            RETURN
*        END
*    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 427) THEN
        IF (WS.USR.CO NE 2) AND (WS.USR.CO NE 20) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
** IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 428) THEN
**     IF (WS.USR.CO NE 50) AND (WS.USR.CO NE 51) THEN
**         ETEXT = '�� ���� ������� ��� �������'
**         CALL STORE.END.ERROR
**         RETURN
**     END
** END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 429) THEN
        IF (WS.USR.CO NE 1) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 432) THEN
        IF (WS.USR.CO NE 11) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 435) THEN
        ETEXT = '�� ���� ������� ��� �������'
        CALL STORE.END.ERROR
        RETURN
    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 439) THEN
        IF (WS.USR.CO NE 11) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 447) THEN
        IF (WS.USR.CO NE 35) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 461) THEN
        IF (WS.USR.CO NE 3) AND (WS.USR.CO NE 10) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END
    IF (R.NEW(MUA.HMM.ID)<1,AV> EQ 462) THEN
        IF (WS.USR.CO NE 11) THEN
            ETEXT = '�� ���� ������� ��� �������'
            CALL STORE.END.ERROR
            RETURN
        END
    END

    IF R.NEW(MUA.HMM.ID)<1,AV> EQ 400 AND WS.USR.SCB.DEPT NE 5100 THEN
        ETEXT = '�� ���� ������� ��� �������'
        CALL STORE.END.ERROR
        RETURN
    END
*** UPDATED BY AMIRA 20160303
    IF R.NEW(MUA.HMM.ID)<1,AV> EQ 401 AND WS.USR.SCB.DEPT NE 5200 THEN
        ETEXT = '�� ���� ������� ��� �������'
        CALL STORE.END.ERROR
        RETURN
    END
    RETURN
*-----------------------------------------------------
