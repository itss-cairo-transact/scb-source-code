* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***** DINA_SCB *** 17/7/2002

SUBROUTINE VVR.TT.CURR.TO.TILL

*1- a routin to empty the fields amount.fcy.1 , amount.local.1, account.1, account.2 and default the field
*2- currency.2 with comi if the user changes the currency
*3-to check if the currency is local then make amount.local.1 an input field and make the amount.fcy.1 a
* noinput field
*4-to check if the currency is forign then make amount.fcy.1 an input field and make the amount.local.1 a
* noinput field

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER


IF COMI # R.NEW(TT.TE.CURRENCY.1) THEN
 IF COMI = LCCY THEN
  T(TT.TE.AMOUNT.LOCAL.1)<3>=''
  T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
 END ELSE
   T(TT.TE.AMOUNT.FCY.1)<3>=''
   T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
  END
 R.NEW(TT.TE.AMOUNT.FCY.1)= ''
 R.NEW(TT.TE.ACCOUNT.1) = ''
 R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
 R.NEW(TT.TE.CURRENCY.2) = COMI
 R.NEW(TT.TE.ACCOUNT.2) = ''
 CALL REBUILD.SCREEN
END

RETURN
END
