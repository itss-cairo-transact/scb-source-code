* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>198</Rating>
*-----------------------------------------------------------------------------
 SUBROUTINE VVR.TT.FT.DRACHEQ



*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

KEY.LIST = ""
T.SEL = 'SELECT FBNK.FUNDS.TRANSFER WITH CREDIT.ACCT.NO EQ ': COMI  :' AND CHEQUE.NUMBER EQ ': R.NEW(TT.TE.CHEQUE.NUMBER)


      CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
      IF KEY.LIST THEN
       ID = KEY.LIST<SELECTED>
       F.COUNT = '' ; FN.COUNT = 'FBNK.FUNDS.TRANSFER' ; R.COUNT = ''
     END
  IF NOT(KEY.LIST) THEN
  T.SEL = 'SELECT FBNK.FUNDS.TRANSFER$HIS WITH CREDIT.ACCT.NO EQ ':COMI :' AND CHEQUE.NUMBER EQ ': R.NEW(TT.TE.CHEQUE.NUMBER)
  CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
       IF KEY.LIST THEN
       ID = KEY.LIST<SELECTED>
  F.COUNT = '' ; FN.COUNT = 'FBNK.FUNDS.TRANSFER$HIS' ; R.COUNT = ''
  END
  END
 CALL OPF(FN.COUNT,F.COUNT)
 CALL F.READ( FN.COUNT, ID, R.COUNT, F.COUNT, TT.ERR)
 IF TT.ERR THEN ETEXT = 'FT.Reference.Does.Not.Exist'
 ELSE
 CUR  = R.COUNT<FT.DEBIT.CURRENCY>
 *ACCT = R.COUNT<TT.TE.ACCOUNT.1>

 AMT = R.COUNT<FT.DEBIT.AMOUNT>
  R.NEW(TT.TE.AMOUNT.LOCAL.1) = AMT; CALL REBUILD.SCREEN
 *AMT.LCY = R.COUNT<TT.TE.AMOUNT.LOCAL.1>
 IF CUR = LCCY THEN
  R.NEW(TT.TE.AMOUNT.LOCAL.1) = AMT; CALL REBUILD.SCREEN

END ELSE R.NEW(TT.TE.AMOUNT.FCY.1) = AMT
  
CALL REBUILD.SCREEN

      END

 RETURN
 END 
