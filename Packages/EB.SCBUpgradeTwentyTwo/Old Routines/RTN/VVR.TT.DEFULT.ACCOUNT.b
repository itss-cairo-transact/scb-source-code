* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*******ZEAD MOSTAFA***********
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.DEFULT.ACCOUNT

*1-TO DEFULT ACCOUNT THAT RELATED TO THE CURRENCY AND CATEGORY

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
    IF COMI THEN
        T.SEL = 'SELECT FBNK.ACCOUNT WITH CURRENCY EQ ': COMI :' AND CATEGORY EQ 5080'
        KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
        IF KEY.LIST THEN
*****UPDATED BY NESSREEN AHMED 26/03/2009********************
     **   R.NEW(TT.TE.ACCOUNT.2)= KEY.LIST<1>
           R.NEW(TT.TE.ACCOUNT.1)= KEY.LIST<1>
        END ELSE
     **      R.NEW(TT.TE.ACCOUNT.2) = ''
             R.NEW(TT.TE.ACCOUNT.1) = ''
*************************************************************
        END
    END
    RETURN
END
