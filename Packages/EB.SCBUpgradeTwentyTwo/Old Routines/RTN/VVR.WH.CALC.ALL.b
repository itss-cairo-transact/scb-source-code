* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.WH.CALC.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS

    SAM1 = 0


    R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,AV> = COMI
*Line [ 34 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NN = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)

    FOR I = 1 TO NN
        ITEM.NO = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>
        CALL DBR('SCB.WH.ITEMS':@FM:SCB.WH.IT.UNIT.PRICE,ITEM.NO,ITEM.PRICE)

        IF NOT(R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I>) THEN
            COMI = 1
            R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I> = COMI
        END

        ZZ =  R.NEW(SCB.WH.TRANS.NO.OF.UNITS)<1,I>

        SAM = ITEM.PRICE * ZZ
        SAM = FMT(SAM,"R2")
        R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,I> = SAM

        SAM1 +=  R.NEW(SCB.WH.TRANS.VALUE.BALANCE)<1,I>
        SAM1  = FMT(SAM1,"R2")
    NEXT I

    R.NEW(SCB.WH.TRANS.TOTAL.BALANCE) = SAM1
    CALL REBUILD.SCREEN
    RETURN
END
