* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CU.TT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

* TO EMPTY THE FOLLOWING FIELDS IF THE THE VALUE IN THIS FIELD:THE CUSTOMER,
* THE ACCOUNT, AMOUNT.LCY, AMOUNT.FCY, CHEQ.NO,DEBIT.THEIR.REF.
* TO CHECK ALSO THAT THE CUSTIMER IS NOT MINOR , OR HANDICAP,OR DEAD
* OR DIDNT PAY THE OPENING ACCOUNT FEES

    IF R.NEW(TT.TE.CUSTOMER.2) THEN
        IF COMI # R.NEW(TT.TE.CUSTOMER.2) THEN
            R.NEW(TT.TE.CUSTOMER.2) = ''
            R.NEW(TT.TE.ACCOUNT.2) = ''
            R.NEW(TT.TE.AMOUNT.LOCAL.1) =''
            R.NEW(TT.TE.AMOUNT.FCY.1) = ''
            R.NEW(TT.TE.CHEQUE.NUMBER) =''
             R.NEW(FT.DEBIT.THEIR.REF)=''
            CALL REBUILD.SCREEN
        END
    END

    FN.CUSTOMER = 'F.CUSTOMER';F.CUSTOMER='';R.CUSTOMER = '';E=''


*IF COMI THEN

    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL F.READ(FN.CUSTOMER,COMI, R.CUSTOMER, F.CUSTOMER ,E)

    BIRTH.D  = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
    REL.CUS  = R.CUSTOMER<EB.CUS.REL.CUSTOMER>
    STAT.CUS = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
    POS.REST = R.CUSTOMER<EB.CUS.POSTING.RESTRICT>

    IF POS.REST = '23' THEN
        ETEXT = '��� ������ ���� � ����� ����'
    END

    IF POS.REST = '73' THEN
        ETEXT = '��� ������ �� �������� ����'
    END

    IF POS.REST = '18' THEN
        ETEXT = '������ ����� ��� ���� ����� '
    END

    IF POS.REST = '11' THEN
        ETEXT = '��� ������ �� ���� ������� ��� ������'
    END
    CALL REBUILD.SCREEN
*END

    RETURN
END
