* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>190</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN  -----

    SUBROUTINE VVR.TT.CHQ.RETURN

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*********
*TEXT='RETURN';CALL REM
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "SCB.CHQ.RETURN"
    OFS.OPTIONS = "INP"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""
    COMMA = ","
    AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
*Line [ 74 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('THE SIGNATURE IS CORRECT', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
***********************CHECK BALAMCE
        LOCK.AMT1 = ''
        ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
        TEXT = "ACCT.NO=":ACCT ; CALL REM
        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT.NO, BAL)
        TEXT = "BAL=":BAL ; CALL REM
        CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LOCK.NO=DCOUNT(LOCK.AMT,@VM)
*        FOR I=1 TO LOCK.NO
*            CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
*          LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
*     NEXT I
        NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
        CATEG = ACCT.NO[11,4]
**  IF R.NEW(TT.TE.AMOUNT.LOCAL.1) THEN
        IF R.NEW(TT.TE.CURRENCY.1) EQ 'EGP' THEN
**  AMT.LOC = R.NEW(TT.TE.AMOUNT.LOCAL.1)
            AMT.LOC = AMT
        END ELSE
            AMT.LOC = R.NEW(TT.TE.AMOUNT.FCY.1)
        END
**********
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.NO,AC.CAT)
*        IF AC.CAT # '1201' AND AC.CAT # '1202' THEN
        IF NOT(AC.CAT GE 1101 AND AC.CAT LE 1590)THEN
**********
        IF AMT.LOC GT NET.BAL THEN
************************************
*Line [ 106 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            CALL TXTINP('THE AMOUNT IS NOT AVILABLE DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
*     CUST = ''
            IF COMI[1,1] = 'Y' THEN
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR


                CURR = R.NEW(TT.TE.CURRENCY.1)
                ACCT = R.NEW(TT.TE.ACCOUNT.1)
**    AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                AMT.LCY = AMT
                AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
                CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
                NRTV = R.NEW(TT.TE.NARRATIVE.2)
                STOP.TYPE = '20'
                CHEQUE.TYPE = 'OSCB'
                STOP.DATE = TODAY
            END ELSE
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR

            END
        END
*************
    END
**************
END ELSE
*Line [ 134 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        ETEXT = 'AMOUNT NOT AVILABLE'
        CALL STORE.END.ERROR

***************************************
        CURR = R.NEW(TT.TE.CURRENCY.1)
        ACCT = R.NEW(TT.TE.ACCOUNT.1)
        AMT.LCY = AMT
        AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
        CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
        NRTV = R.NEW(TT.TE.NARRATIVE.2)
        STOP.TYPE = '21'
        CHEQUE.TYPE = 'OSCB'
        STOP.DATE = TODAY
    END ELSE
        ETEXT = 'AMOUNT NOT AVILABLE'
        CALL STORE.END.ERROR

    END
END


MUL.NO = 1

CALL DBR( 'SCB.CHQ.RETURN':@FM:SCB.CHQR.PAYM.STOP.TYPE, ACCT,ST.TYPE)
TEXT = ST.TYPE ; CALL REM
IF ST.TYPE THEN

*Line [ 164 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    MULL.NOO=DCOUNT(ST.TYPE,@VM)
    TEXT = MULL.NOO ; CALL REM
    MUL.NO = MUL.NO + MULL.NOO

END

IF CURR THEN
* OFS.MESSAGE.DATA =  "CUSTOMER=":CUST:COMMA
*OFS.MESSAGE.DATA :=  "CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "PAYM.STOP.TYPE:":MUL.NO:"=":STOP.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "FIRST.CHEQUE.NO:":MUL.NO:"=":CHQ.NO:COMMA
*        OFS.MESSAGE.DATA :=  "LAST.CHEQUE.NO=":CHQ.NO:COMMA
    OFS.MESSAGE.DATA :=  "CHEQUE.TYPE:":MUL.NO:"=":CHEQUE.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "STOP.DATE:":MUL.NO:"=":STOP.DATE:COMMA
    IF AMT.LCY THEN
        OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.LCY:COMMA
    END ELSE
        OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.FCY:COMMA
    END
    OFS.MESSAGE.DATA :=  "BENEFICIARY:":MUL.NO:"=":NRTV


    ID.KEY.LIST= ACCT
    TEXT = "ID.KEY.LIST = ":ID.KEY.LIST ; CALL REM
    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ID.KEY.LIST:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.KEY.LIST:"-":TODAY ON ERROR
        TEXT = " ERROR "
        CALL REM
    END
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
******************************
END
*****************************************
*********
RETURN
END
