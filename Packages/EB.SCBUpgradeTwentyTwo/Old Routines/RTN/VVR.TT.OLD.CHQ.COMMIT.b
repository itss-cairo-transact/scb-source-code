* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****23/10/2003 ABEER *************
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.OLD.CHQ.COMMIT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

**N** OLD.CHQ.NO=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.NO>
    OLD.CHQ.NO=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
    ACCT.NO =R.NEW(TT.TE.ACCOUNT.2)

**************UPDATED BY RIHAM R15*************

*    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN)
**************************************************************
*-- EDIT BY NESSMA 2016/08/02
    MYBRN = MYBRN[8,2]
    MYBRN = TRIM(MYBRN , "0" , "L")
*-- END EDIT
    PAY.CHQ.ID= OLD.CHQ.NO:'.':MYBRN

    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO,MYCURR)
    CHQ='SCB':'.':ACCT.NO:'...'

    CALL DBR ('PAYMENT.STOP':@FM:AC.PAY.FIRST.CHEQUE.NO,ACCT.NO,MYCHECK)
    CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCNO)
**********To Issue A Cheq Record In Cheque.Issue*******************
**N** IF R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.NO>  THEN
    IF R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>  THEN
        T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED EQ '0' THEN
            NO.ISSUED='1'
            CHQ.START='1'
        END ELSE
            CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST<SELECTED>,CHECK.ST.NO)
            CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.NUMBER.ISSUED,KEY.LIST<SELECTED>,NO.ISSUED)

            NEW.CHQ.STRT.NO= CHECK.ST.NO+NO.ISSUED
            NO.ISSUED='1'
            CHQ.START= NEW.CHQ.STRT.NO
        END
**********************************************************************
*        FN.P.CHQ='F.SCB.P.CHEQ';ID=PAY.CHQ.ID;R.P.CHQ='';F.P.CHQ=''
*        CALL F.READ(FN.P.CHQ,ID,R.P.CHQ,F.P.CHQ,ETEXT)
*        R.P.CHQ<P.CHEQ.CHEQ.VAL>=R.NEW(TT.TE.AMOUNT.LOCAL.1)

*        R.P.CHQ<P.CHEQ.CHEQ.DAT>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>
*        R.P.CHQ<P.CHEQ.TRM.DAT>=TODAY
*        R.P.CHQ<P.CHEQ.ACCOUNT.NO>=R.NEW(TT.TE.ACCOUNT.2)
*        R.P.CHQ<P.CHEQ.OLD.KEY>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.ACCT>
*        CALL F.WRITE(FN.P.CHQ,ID,R.P.CHQ)
*******************************************************************
*****UPDATED BY NESSREEN AHMED 16/03/2009********************
**    CALL VAR.TT.OFS(ACCT.NO,NO.ISSUED,CHQ.START)
********************************************************************
        T.SEL1 ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG)
        CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST1<SELECTED1>,CHQ.LAST)
        IF CHQ.LAST LT CHQ.START THEN
            FN.P.CHQ='F.SCB.P.CHEQ';ID=PAY.CHQ.ID;R.P.CHQ='';F.P.CHQ=''
            CALL F.READ(FN.P.CHQ,ID,R.P.CHQ,F.P.CHQ,ETEXT)
            R.P.CHQ<P.CHEQ.CHEQ.VAL>=R.NEW(TT.TE.AMOUNT.LOCAL.1)
            R.P.CHQ<P.CHEQ.CHEQ.DAT>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>
****UPDATED BY NESSREEN AHMED 16/03/2009********************
**     R.P.CHQ<P.CHEQ.TRM.DAT>=TODAY
            R.P.CHQ<P.CHEQ.TRN.DAT>=TODAY
**************************************************************
            R.P.CHQ<P.CHEQ.ACCOUNT.NO>=R.NEW(TT.TE.ACCOUNT.2)
* R.P.CHQ<P.CHEQ.OLD.KEY>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.ACCT>
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
            COMP = C$ID.COMPANY
            R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP
*********************************************************************
            CALL F.WRITE(FN.P.CHQ,ID,R.P.CHQ)

        END ELSE
            E='Must.Issue.Cheque.First';CALL ERR ;MESSAGE = 'REPEAT'
        END

********************************************************************
    END

    RETURN
END
