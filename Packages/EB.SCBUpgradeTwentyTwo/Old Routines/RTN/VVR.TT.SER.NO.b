* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****06/06/2010 NESSREEN AHMED ************
*-----------------------------------------------------------------------------
* <Rating>65</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.SER.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.SER.NO
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    ETEXT = ''   ; SRN = '' ; SER.STAT = ''
    IF COMI THEN
        IF COMI # R.NEW(TT.TE.LOCAL.REF)<1,TTLR.SER.NO> THEN
            ETEXT = '��� ����� ��� �������';CALL STORE.END.ERROR
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.SER.NO> = COMI
            CALL REBUILD.SCREEN
        END
**        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
**        PAY.SER.NO = COMI
        CALL DBR ('SCB.P.SER.NO':@FM:PSN.SER.STATUS,COMI,SER.STAT)
        IF SER.STAT THEN
            IF SER.STAT EQ 'STOPPED'  THEN
                ETEXT='��� ������� �� ����'
                CALL STORE.END.ERROR
**    ETEXT = ''
            END
            IF SER.STAT EQ 'PAID'  THEN
                ETEXT='��� ������� �� ����'
                CALL STORE.END.ERROR
**    ETEXT = ''
            END
        END ELSE
            ETEXT = ''
        END
    END
    RETURN
END
