* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>86</Rating>
*-----------------------------------------------------------------------------
*************INGY-SCB************
*To Check If Customer Changed Then fields account,currency will be empty
*To Check ALso If Customer has any Posting Restriction against the account will display error message
    SUBROUTINE VVR.TT.CU.MINOR

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

    IF COMI NE R.NEW(TT.TE.CUSTOMER.1) THEN
        CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
        STO1=POSS<1,1>
        STO2=POSS<1,2>
        STO3=POSS<1,3>
        STO4=POSS<1,4>
        STO5=POSS<1,5>
        STO6=POSS<1,6>
        STO7=POSS<1,7>
        STO8=POSS<1,8>
        STO9=POSS<1,9>
        STO10=POSS<1,10>
        IF POSS NE '' THEN
            TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4  :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
        END
    END
    ETEXT =''
    IF R.NEW(TT.TE.CUSTOMER.2) THEN
        IF COMI # R.NEW(TT.TE.CUSTOMER.2) THEN
            R.NEW(TT.TE.ACCOUNT.2) = ''
            R.NEW(TT.TE.CURRENCY.1) = ''
            R.NEW(TT.TE.CURRENCY.2) = ''
            CALL REBUILD.SCREEN
        END
    END
    FN.CUSTOMER = 'F.CUSTOMER';F.CUSTOMER='';R.CUSTOMER = '';E=''


    IF COMI THEN
        CALL OPF(FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ(FN.CUSTOMER,COMI, R.CUSTOMER, F.CUSTOMER ,E)

*   CALL DBR('CUSTOMER':@FM:EB.CUS.REL.CUSTOMER,COMI,REL.CUS)
*  CALL DBR('CUSTOMER':@FM:EB.CUS.BIRTH.INCORP.DATE,COMI,BIRTH.D)
*    TEXT = REL.CUS ; CALL REM
*   TEXT = BIRTH.D ; CALL REM
        BIRTH.D = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
        REL.CUS = R.CUSTOMER<EB.CUS.REL.CUSTOMER>
        STAT.CUS = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
        POS.REST = R.CUSTOMER<EB.CUS.POSTING.RESTRICT>

*       IF BIRTH.D THEN
        YEARS.21 = ( BIRTH.D[1, 4] + 21 ) : BIRTH.D[ 5, 4]
*        IF (REL.CUS) AND (YEARS.21 > TODAY) THEN
        IF (STAT.CUS = '22') AND (REL.CUS # '') THEN
            TEXT = '��� ������ ���� � ����� ���� &':REL.CUS ; CALL STORE.OVERRIDE(R.NEW(AC.CURR.NO)+1)
        END ELSE
            IF (STAT.CUS = '15') AND (REL.CUS # '') THEN
                TEXT = '��� ������ ��� �������� � ������ &':REL.CUS ; CALL STORE.OVERRIDE(R.NEW(AC.CURR.NO)+1)
            END ELSE
                IF STAT.CUS = '18' THEN
                    TEXT = '������ ����� ��� ���� ����� &':REL.CUS ; CALL STORE.OVERRIDE(R.NEW(AC.CURR.NO)+1)
                END ELSE
****NESSREEN AHMED 8/3/2007*********************
**  IF POS.REST = '11' THEN
**      TEXT = '��� ������ �� ���� ������� ��� ������';  CALL STORE.OVERRIDE(R.NEW(AC.CURR.NO)+1)
**  END
                END
            END

        END
*        END

    END
*******************************************************
******NESSREEN AHMED 8/3/2007*********
**    CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,COMI,POST.REST)
**    IF POST.REST = 11 OR POST.REST = 12 OR POST.REST = 14 OR POST.REST = 16 OR POST.REST = 23 OR POST.REST = 25 OR POST.REST = 76 OR POST.REST = 71 OR POST.REST = 72 OR POST.REST = 78 OR POST.REST = 80 OR POST.REST = 90 OR POST.REST = 99 OR POST.REST= 70 OR POST.REST= 13 OR POST.REST= 1 THEN
**        CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST.REST,DESC)
**        ETEXT = '���� ������ ':DESC ;CALL STORE.END.ERROR
**    END

    RETURN
END
