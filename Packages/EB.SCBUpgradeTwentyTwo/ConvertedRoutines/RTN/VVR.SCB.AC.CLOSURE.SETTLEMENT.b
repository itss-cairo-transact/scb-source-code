* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
**** 13/6/2002 DINA & INGY SCB *****

*A ROUTINE 1- DOES NOT ALLOW AN INTERNAL ACCOUNT TO BE WRITTEN IN THE FIELD SETTLEMENT ACCOUNT
*          2- IF CATEGORY OF ID.NEW NOSTRO MUST BE CATEGORY OF SETTLEMENT ACCOUNT NOSTRO AND VISE VERSA

    SUBROUTINE VVR.SCB.AC.CLOSURE.SETTLEMENT

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF COMI AND NOT(NUM(COMI)) THEN
*   ETEXT ='Not.Allowed.For.Internal.Account'
        ETEXT = '��� ����� �������� ���������'
    END ELSE
*------------------------
***ADDED ON 5.1.2007
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG.COMI )
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG.COMI=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ID.NEW,CATEG.ID )
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ID.NEW,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
***************
*****HASEHD ON 5.1.2007        CATEG.ID = ID.NEW[12,4]  ;  CATEG.COMI = COMI[12,4]
        TEXT=CATEG.COMI;CALL REM
        TEXT=CATEG.ID;CALL REM
        IF 5000 LE CATEG.ID AND CATEG.ID LE 5999 THEN
            IF CATEG.COMI GE 5000 AND CATEG.COMI LE 5999 THEN
* ETEXT ='Not.Allowed.For.NOSTRO.Account'
                ETEXT = '��� ����� ������� ��������'
            END
        END ELSE
            IF 5000 GE CATEG.COMI AND CATEG.COMI GE 5999 THEN
*  ETEXT ='Not.Allowed.For.Customer.Account'
                ETEXT = '��� ����� ������� �������'
            END
        END
*-------------------------


    END

    RETURN

END
