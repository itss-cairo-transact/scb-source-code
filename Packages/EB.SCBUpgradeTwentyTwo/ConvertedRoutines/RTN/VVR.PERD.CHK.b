* @ValidationCode : Mjo4MjgzMzMyNDM6Q3AxMjUyOjE2NDIyNTQ2Nzk1NjQ6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:51:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****CREATED BY NESSMA
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.PERD.CHK

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LOANS.INST
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.PERD.INSTALL
*-------------------------------------------
    IF MESSAGE # 'VAL' THEN

        PERD.ID = COMI
        IF PERD.ID EQ 0 THEN
*Line [ 38 ] Update INST.NO.OF.INSTALLMENT to LINST.NO.OF.INSTALLMENT - ITSS - R21 Upgrade - 2021-12-23
*R.NEW(INST.NO.OF.INSTALLMENT) = 1
            R.NEW(LINST.NO.OF.INSTALLMENT) = 1
        END
*-------------------------------------------
        CALL REBUILD.SCREEN
*-------------------------------------------
    END
RETURN
END
