* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-------MAI 22 AUG 2019----------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.NCUS.CD.NO
**  PROGRAM VVR.NCUS.CD.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION ='I' THEN
        COMP          = C$ID.COMPANY
        CD.NO = COMI
        FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
**********CHECK IF IT WAS PAID BEFORE
        FN.CD.NSN = 'F.SCB.CD.NATIONAL.ID' ; F.CD.NSN = '' ; R.CD.NSN = ''
        CALL OPF(FN.CD.NSN,F.CD.NSN)
        CALL F.READ( FN.CD.NSN,CD.NO, R.CD.NSN, F.CD.NSN,ERR)
        TT.REC  = R.CD.NSN<SCB.CD.LD.NO,1>
        TT.STAT = R.CD.NSN<SCB.CD.LD.STATUS,1>
** IF TT.REC EQ '' THEN
        IF TT.STAT NE "PAID" THEN
************END OF UPDATE
            CALL F.READ(FN.LD,CD.NO,R.LD,F.LD,E2)
            LD.LOC.REF           = R.LD<LD.LOCAL.REF>
            LD.COMP              = R.LD<LD.CO.CODE>
            LD.STAT              = R.LD<LD.STATUS>
            LD.CAT               = R.LD<LD.CATEGORY>
            LD.AMT               = R.LD<LD.AMOUNT>
            LD.LIQ.AMT           = LD.LOC.REF<1,LDLR.CD.LIQ.AMT>
            LD.RESPECT.TO        = LD.LOC.REF<1,LDLR.IN.RESPECT.OF>
            NATIONAL.ID          = LD.LOC.REF<1,LDLR.EBAN>
*************
            IF COMP NE LD.COMP THEN
                ETEXT = 'This CD FROM ANOTHER BRANCH'
                CALL STORE.END.ERROR
            END ELSE
                IF LD.CAT EQ '21101' THEN
                    IF LD.STAT EQ 'LIQ' THEN
                        IF LD.LIQ.AMT NE '' THEN
                            R.NEW(TT.TE.AMOUNT.LOCAL.1) =  LD.LIQ.AMT
                        END ELSE
                            R.NEW(TT.TE.AMOUNT.LOCAL.1) =  LD.AMT * 1.8
                        END
                        R.NEW(TT.TE.NARRATIVE.2)<1,2> = LD.RESPECT.TO
                        R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NSN.NO>= NATIONAL.ID
                    END ELSE
*                   E='NOT LIUATED CD'; CALL ERR ; MESSAGE = 'REPEAT'
                        ETEXT = 'NOT LIQUATED LD'
                        CALL STORE.END.ERROR
                    END
                END ELSE
                    ETEXT = 'This screen is for non customers only'
                    CALL STORE.END.ERROR
* CALL REBUILD.SCREEN
                END
            END
        END ELSE
            IF TT.STAT EQ "PAID" THEN
                ETEXT = 'This CD PAID BEFORE UNDER REF NO':TT.REC
                CALL STORE.END.ERROR
            END
        END
    END
    RETURN
END
