* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SR.CUSTOMER

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*IF MESSAGE = 'VAL' THEN

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    T.SEL = "SELECT FBNK.CUSTOMER WITH @ID EQ ":COMI
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL OPF( FN.CU,F.CU)
    CALL F.READ( FN.CU,KEY.LIST, R.CU, F.CU, ETEXT)

    R.NEW(SR.SCRIPT.ADDRESS)       = ""
    R.NEW(SR.SCRIPT.CUSTOMER.CODE) = ""
    R.NEW(SR.SCRIPT.HOLD.DEPT)     = ""
    R.NEW(SR.SCRIPT.NO.OF.SHARES)  = ""
    R.NEW(SR.SCRIPT.BUY.SHARES)    = ""
    R.NEW(SR.SCRIPT.TOTAL.SHARES)  = ""
    R.NEW(SR.SCRIPT.TOTAL.CHARGE)  = ""
    R.NEW(SR.SCRIPT.GROSS.TOTAL)   = ""
    R.NEW(SR.SCRIPT.DIFF.SHARES.NO)= ""

    R.NEW(SR.SCRIPT.ADDRESS)       = R.CU<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS>
    R.NEW(SR.SCRIPT.CUSTOMER.CODE) = R.CU<EB.CUS.MNEMONIC>[4,7]
    R.NEW(SR.SCRIPT.HOLD.DEPT)     = R.CU<EB.CUS.LOCAL.REF,CULR.CBE.NO>
    R.NEW(SR.SCRIPT.NO.OF.SHARES)  = R.CU<EB.CUS.LOCAL.REF,CULR.BROKER.TYPE>

    CALL REBUILD.SCREEN
*END

    RETURN
END
