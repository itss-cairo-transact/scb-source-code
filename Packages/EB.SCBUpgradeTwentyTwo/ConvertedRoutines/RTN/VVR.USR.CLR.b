* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-40</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/07 ***
*******************************************

    SUBROUTINE VVR.USR.CLR

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SMS.GROUP
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    WS.COMP = ID.COMPANY
    IF APPLICATION EQ 'USER' THEN
        GOSUB CLEAR.USR
        CALL REBUILD.SCREEN
    END
    IF APPLICATION EQ 'USER.ABBREVIATION' THEN
        GOSUB CLEAR.UA
        CALL REBUILD.SCREEN
    END
    RETURN
**-------------------------------------------------------------------
CLEAR.USR:

*Line [ 52 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.NEW(EB.USE.COMPANY.RESTR),@VM)
    FOR I = 1 TO WS.COUNT
    *    IF R.NEW(EB.USE.COMPANY.RESTR)<1,I> = '' THEN
            DEL R.NEW(EB.USE.COMPANY.RESTR)<1,I>
            DEL R.NEW(EB.USE.APPLICATION)<1,I>
            DEL R.NEW(EB.USE.VERSION)<1,I>
            DEL R.NEW(EB.USE.FUNCTION)<1,I>
            DEL R.NEW(EB.USE.FIELD.NO)<1,I>
            DEL R.NEW(EB.USE.DATA.COMPARISON)<1,I>
            DEL R.NEW(EB.USE.DATA.FROM)<1,I>
            DEL R.NEW(EB.USE.DATA.TO)<1,I>
            I --
            WS.COUNT --
    *    END
    NEXT I
*Line [ 68 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CNT = DCOUNT(R.NEW(EB.USE.OVERRIDE.CLASS),@VM)
    FOR I.OVR.CNT = 1 TO WS.OVR.CNT
    *    IF R.NEW(EB.USE.OVERRIDE.CLASS)<1,I.OVR.CNT> EQ '' THEN
            DEL R.NEW(EB.USE.OVERRIDE.CLASS)<1,I.OVR.CNT>
            I.OVR.CNT --
            WS.OVR.CNT --
    *    END
    NEXT I.OVR.CNT
*Line [ 77 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CO.CNT = DCOUNT(R.NEW(EB.USE.COMPANY.CODE),@VM)
    FOR ICO.CNT = 1 TO WS.CO.CNT
    *    IF R.NEW(EB.USE.COMPANY.CODE)<1,ICO.CNT> = '' THEN
            DEL R.NEW(EB.USE.COMPANY.CODE)<1,ICO.CNT>
            ICO.CNT --
            WS.CO.CNT --
    *    END
    NEXT ICO.CNT

    RETURN
**-------------------------------------------------------------------
CLEAR.UA:
    PRINT "CLEAR-UA..."
    PRINT R.NEW(EB.UAB.ABBREVIATION)
*Line [ 92 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.UA.CNT = DCOUNT(R.NEW(EB.UAB.ABBREVIATION),@VM)
    PRINT "UA COUNT ....":WS.UA.CNT
    FOR I.UA.CNT = 1 TO WS.UA.CNT
        PRINT "TEST DEL ....":R.NEW(EB.UAB.ABBREVIATION)<1,I.UA.CNT>:"CONT NO ....":I.UA.CNT
    *    IF R.NEW(EB.UAB.ABBREVIATION)<1,I.UA.CNT> EQ '' THEN
            DEL R.NEW(EB.UAB.ABBREVIATION)<1,I.UA.CNT>
            DEL R.NEW(EB.UAB.ORIGINAL.TEXT)<1,I.UA.CNT>
            I.UA.CNT --
            WS.UA.CNT --
    *    END
    NEXT I.UA.CNT
    PRINT "AFTR DEL ....":R.NEW(EB.UAB.ABBREVIATION)

    RETURN
**-------------------------------------------------------------------
EDIT.USR:
    WS.USR.CO = R.NEW(EB.USE.DEPARTMENT.CODE) + 100
    WS.USR.CO = 'EG00100':WS.USR.CO[2,2]
    R.NEW(EB.USE.COMPANY.RESTR)<1,1> = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,1>   = 'ALL.PG'
    R.NEW(EB.USE.FUNCTION)<1,1>      = 'E L S P'

    R.NEW(EB.USE.COMPANY.RESTR)<1,2> = WS.USR.CO
    R.NEW(EB.USE.APPLICATION)<1,2>   = '@A400'

    R.NEW(EB.USE.COMPANY.RESTR)<1,3> = WS.USR.CO
    R.NEW(EB.USE.APPLICATION)<1,3>   = '@I400'
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.NEW(EB.USE.COMPANY.RESTR),@VM)
    RETURN
**-------------------------------------------------------------------
GET.SMS:
    T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A428' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A493' 'A497' 'A499' 'A48' ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR ISMS = 1 TO SELECTED
            WS.COUNT++
            R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT> = WS.USR.CO
            R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>   = '@':KEY.LIST<ISMS>
        NEXT ISMS
    END
    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT> = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>   = '@LOCK.DEV.AREA'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = 'FUNDS.TRANSFER'
    R.NEW(EB.USE.FUNCTION)<1,WS.COUNT>        = 'E L S P'
    R.NEW(EB.USE.FIELD.NO)<1,WS.COUNT>        = '1'
    R.NEW(EB.USE.DATA.COMPARISON)<1,WS.COUNT> = 'NE'
    R.NEW(EB.USE.DATA.FROM)<1,WS.COUNT>       = 'AC12'
    R.NEW(EB.USE.DATA.TO)<1,WS.COUNT>         = 'AC13'
    RETURN
**-------------------------------------------------------------------
