* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****CREATED BY NESSMA
*-----------------------------------------------------------------------------
* <Rating>-9</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.S.DATE.UPD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.INSTALL2
*-------------------------------------------
    START.DATE = COMI
    TOD.DATE   = TODAY
*    IF START.DATE LT TOD.DATE THEN
*        ETEXT = "���� �� ���� ���� �� ����� ����� ����� "
*        CALL STORE.END.ERROR
*    END ELSE
*-------------------------------------------
*   PERD.INSTALL = R.NEW(INS.PERIODIC.INSTALLMENT)
    NO.INSTALL   = R.NEW(INS.NO.OF.INSTALLMENT)
    R.NEW(INS.INSTALLMENT.DATE)<1,1> = COMI

    IF MESSAGE # 'VAL' THEN
        FOR NN = 2 TO NO.INSTALL
*        CALL ADD.MONTHS(START.DATE,PERD.INSTALL)
*            R.NEW(INS.INSTALLMENT.DATE)<1,NN> = START.DATE
            R.NEW(INS.INSTALLMENT.DATE)<1,NN> = ""
            R.NEW(INS.STATUS)<1,NN>           = 'N'
        NEXT NN

*    R.NEW(INS.MATURITY.DATE) =  START.DATE
*    R.NEW(INS.STATUS)        = 'Y'
    END
    CALL REBUILD.SCREEN
*-------------------------------------------
    RETURN
END
