* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.REG.NO

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PAY.CBE.CODES

* Created by Noha Hamed
* INF.MULTI.TXN
* 12 LC
* 14 DRAWINGS

    FN.CBE  = 'F.SCB.MONTHLY.PAY.CBE' ; F.CBE = '' ; R.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.CO  = 'F.SCB.PAY.CBE.CODES'  ; F.CO = ''   ; R.CO = ''
    CALL OPF(FN.CO,F.CO)

    FUN.CODE = R.NEW(CBE.PAY.FUNTP.CODE)
    DOC.TYPE = R.NEW(CBE.PAY.DOCTP.CODE)
    PAY.CODE = R.NEW(CBE.PAY.PAYMENT.CODE)

    IF V$FUNCTION EQ 'I' THEN

        T.SEL = "SELECT F.SCB.PAY.CBE.CODES WITH TABLE.FLAG EQ 06 AND CODE EQ ":COMI
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        IF SELECTED EQ 0 THEN
            ETEXT = 'MISSING RECORD'
            CALL STORE.END.ERROR
        END
        IF DOC.TYPE EQ 2 AND FUN.CODE EQ 4 THEN
            IF  PAY.CODE EQ 3 OR PAY.CODE EQ 20 OR PAY.CODE EQ 21 OR PAY.CODE EQ 22 OR PAY.CODE EQ 23 OR PAY.CODE EQ 24 OR PAY.CODE EQ 25 THEN

            END
            ELSE
                ETEXT = 'MISSING RECORD'
                CALL STORE.END.ERROR
            END
        END
    END


CALL REBUILD.SCREEN

RETURN
END
