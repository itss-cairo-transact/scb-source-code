* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*****NESSREEN AHMED 18/10/2018********
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.SUEZ.USD.CHRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*-----------------------------------------
    FN.CURR ='FBNK.CURRENCY' ; F.CURR = ''
    CALL OPF(FN.CURR,F.CURR)

    IF COMI THEN
        AMT.LCY             = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        AMT.FCY             = COMI
      *  AMT.DEB             = R.NEW(FT.DEBIT.AMOUNT)
        CALL F.READ(FN.CURR,'USD',R.CURR,F.CURR,ERR1)
        RATE.USD            = R.CURR<EB.CUR.MID.REVAL.RATE><1,1>
        EQV.AMT             = AMT.FCY * RATE.USD
        TOT.AMT             =  EQV.AMT + AMT.LCY
        TOT.AMT.PER         = DROUND((TOT.AMT * 0.00114),2)

        IF TOT.AMT.PER LE '11.4' THEN
            AMT.FIN = 11.4
        END

        IF TOT.AMT.PER GE  '114' THEN
            AMT.FIN = 114
        END
        IF TOT.AMT.PER GT '11.4' AND TOT.AMT.PER LT '114' THEN
            AMT.FIN = TOT.AMT.PER
        END

        R.NEW(TT.TE.CHRG.AMT.LOCAL)<1,2>  = AMT.FIN

        CALL REBUILD.SCREEN

    END
    RETURN
END
