* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*****NESSREEN AHMED 11/12/2019************
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.FLT.DPST.DATA
*A Routine to default Depositer Name and NSN.No if he value is YES

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    ETEXT =''  ; CUS.NSN.NO = ''
    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    IF COMI EQ 'YES' THEN
        CUST.NO = R.NEW(TT.TE.CUSTOMER.1)
        CALL F.READ(FN.CUSTOMER, CUST.NO, R.CUSTOMER, F.CUSTOMER, E1)
        LOCAL.REF   = R.CUSTOMER<EB.CUS.LOCAL.REF>
        CUS.NSN.NO  = LOCAL.REF<1,CULR.NSN.NO>
        CUS.AR.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUS.ID.NO   = LOCAL.REF<1,CULR.ID.NUMBER>
        IF CUS.NSN.NO NE '' THEN
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.DPST.NSN.NO> = CUS.NSN.NO
        END ELSE
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.DPST.NSN.NO> = CUS.ID.NO
        END
        R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ARABIC.NAME> = CUS.AR.NAME

        CALL REBUILD.SCREEN
    END
    IF MESSAGE EQ 'VAL' THEN
       R.NEW(TT.TE.NARRATIVE.1) = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ARABIC.NAME>
    END
    RETURN
END
