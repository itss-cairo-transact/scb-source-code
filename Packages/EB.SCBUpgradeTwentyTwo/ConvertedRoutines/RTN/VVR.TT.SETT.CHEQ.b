* @ValidationCode : Mjo3MTQ3OTUxMzQ6Q3AxMjUyOjE2NDIyNTYyMTY1OTc6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 16:16:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN  -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.TT.SETT.CHEQ

* TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* FOR THAT CUSTOMER

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.ISSUE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS
*Line [ 41 ] Adding I_F.ACCOUNT, as it used on line 53 - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT

    IF COMI  THEN
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID = 'SETT.':R.NEW(TT.TE.ACCOUNT.2):'-':COMI
**Updated by Nessreen Ahmed 4/9/2016 For R15*****
** CHQ.ID = 'SETT.':R.NEW(TT.TE.ACCOUNT.2):'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.REPRESENTED.COUNT, CHQ.ID, REPRESENT.COUNT)
** CALL DBR("CHEQUE.REGISTER.SUPPLEMENT":@FM:CC.CRS.DATE.PRESENTED,CHQ.ID,REPRESENT.COUNT)
****END OF UPDATE 8/3/2016*****************************
**     IF REPRESENT.COUNT NE '' THEN
        ACCT.NO = R.NEW(TT.TE.ACCOUNT.2)
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.AC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.AC=R.ITSS.ACCOUNT<AC.CUSTOMER>
        KEY.ID = "...":CUST.AC:"....":COMI
        N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND (STATUS EQ PRESENTED OR STATUS EQ CLEARED) "
        KEY.LIST.N=""
        SELECTED.N=""
        ER.MSG.N=""
        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        IF SELECTED.N THEN
            ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
        END ELSE
            ETEXT=''
            FT.DR=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.FT.DR.CHEQ>
            FT.DR=FIELD(FT.DR,'.',2)
            IF COMI NE  FT.DR  THEN
                ETEXT='�� ����� ��� ��� �������';CALL STORE.END.ERROR
            END
        END
    END
RETURN
END
