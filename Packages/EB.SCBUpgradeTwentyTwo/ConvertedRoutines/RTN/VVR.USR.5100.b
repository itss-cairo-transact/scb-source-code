* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/01/26 ***
*******************************************

    SUBROUTINE VVR.USR.5100

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SMS.GROUP
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    WS.COMP = ID.COMPANY
    IF MESSAGE # 'VAL' THEN
        IF COMI EQ '5100' THEN
            IF (R.NEW(EB.USE.DEPARTMENT.CODE) NE 99) AND (R.NEW(EB.USE.DEPARTMENT.CODE) NE 88) THEN
*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.COUNT = DCOUNT(R.NEW(EB.USE.COMPANY.RESTR),@VM)
                GOSUB CLEAR.USR
                GOSUB EDIT.USR
                GOSUB GET.SMS
                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
**-------------------------------------------------------------------
CLEAR.USR:
    FOR I = 1 TO WS.COUNT
        DEL R.NEW(EB.USE.COMPANY.RESTR)<1,I>
        DEL R.NEW(EB.USE.APPLICATION)<1,I>
        DEL R.NEW(EB.USE.VERSION)<1,I>
        DEL R.NEW(EB.USE.FUNCTION)<1,I>
        DEL R.NEW(EB.USE.FIELD.NO)<1,I>
        DEL R.NEW(EB.USE.DATA.COMPARISON)<1,I>
        DEL R.NEW(EB.USE.DATA.FROM)<1,I>
        DEL R.NEW(EB.USE.DATA.TO)<1,I>
        I --
        WS.COUNT --
    NEXT I
    RETURN
**-------------------------------------------------------------------
EDIT.USR:
    WS.USR.CO = R.NEW(EB.USE.DEPARTMENT.CODE) + 100
    WS.USR.CO = 'EG00100':WS.USR.CO[2,2]
    R.NEW(EB.USE.COMPANY.RESTR)<1,1> = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,1>   = 'ALL.PG.H'
    R.NEW(EB.USE.FUNCTION)<1,1>      = 'E L S P'

    R.NEW(EB.USE.COMPANY.RESTR)<1,2> = WS.USR.CO
    R.NEW(EB.USE.APPLICATION)<1,2>   = '@A400'

    R.NEW(EB.USE.COMPANY.RESTR)<1,3> = WS.USR.CO
    R.NEW(EB.USE.APPLICATION)<1,3>   = '@I400'
*Line [ 79 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.NEW(EB.USE.COMPANY.RESTR),@VM)
    RETURN
**-------------------------------------------------------------------
GET.SMS:
    T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A428' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A493' 'A497' 'A499' 'A48' ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR ISMS = 1 TO SELECTED
            WS.COUNT++
            R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT> = WS.USR.CO
            R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>   = '@':KEY.LIST<ISMS>
        NEXT ISMS
    END
    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT> = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>   = '@LOCK.DEV.AREA'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT>   = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>     = 'FUNDS.TRANSFER'
    R.NEW(EB.USE.FUNCTION)<1,WS.COUNT>        = 'E L S P'
    R.NEW(EB.USE.FIELD.NO)<1,WS.COUNT>        = '1'
    R.NEW(EB.USE.DATA.COMPARISON)<1,WS.COUNT> = 'NE'
    R.NEW(EB.USE.DATA.FROM)<1,WS.COUNT>       = 'AC12'
    R.NEW(EB.USE.DATA.TO)<1,WS.COUNT>         = 'AC13'

    WS.COUNT++
    R.NEW(EB.USE.COMPANY.RESTR)<1,WS.COUNT> = 'ALL'
    R.NEW(EB.USE.APPLICATION)<1,WS.COUNT>   = 'ALL.PG.M'

    RETURN
**-------------------------------------------------------------------
