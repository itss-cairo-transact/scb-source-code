* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*----------------20/08/2009 NESSREEN AHMED-----------------------*
*-----------------------------------------------------------------------------
* <Rating>744</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.ACCPT.FR.BRN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
*******************************************************************************************
    IF COMI THEN
        ETEXT='' ; BRANCH2 =''
        FN.TELLER = 'FBNK.TELLER' ; F.TELLER ='' ; R.TELLER = ''
        FN.TELLER.H = 'FBNK.TELLER$HIS' ; F.TELLER.H ='' ; R.TELLER.H = ''
        CALL OPF(FN.TELLER,F.TELLER)
        CALL OPF(FN.TELLER.H,F.TELLER.H)
        CALL F.READ(FN.TELLER,COMI, R.TELLER, F.TELLER ,E1)
        IF NOT(E1) THEN
            BR = ''
            CURR = R.TELLER<TT.TE.CURRENCY.1>
      **      TEXT = 'HI' ; CALL REM
      **      TEXT = 'CURR=':CURR ; CALL REM
            R.NEW(TT.TE.CURRENCY.1)= R.TELLER<TT.TE.CURRENCY.1>
            R.NEW(TT.TE.ACCOUNT.1)= R.TELLER<TT.TE.ACCOUNT.1>
            TELL.1= R.TELLER<TT.TE.TELLER.ID.2>
            BR = COMP[8,2]
            ACCT.2 = CURR:"10000":BR:"9900":BR
            R.NEW(TT.TE.ACCOUNT.2)= ACCT.2
            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            RECV.BR = LOCAL.REF<1,TTLR.BRANCH.NO>
     **       TEXT = 'RBR=':RECV.BR ; CALL REM
     **       TEXT = 'BR=':BR ; CALL REM
            IF BR = RECV.BR THEN
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('TELLER.ID':@FM:TT.TID.USER,TELL.1,T.USER)
F.ITSS.TELLER.ID = 'FBNK.TELLER.ID'
FN.F.ITSS.TELLER.ID = ''
CALL OPF(F.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID)
CALL F.READ(F.ITSS.TELLER.ID,TELL.1,R.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID,ERROR.TELLER.ID)
T.USER=R.ITSS.TELLER.ID<TT.TID.USER>
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,T.USER,DEPT)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,T.USER,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                BR.NAME = FIELD(DEPT.NAME,'.',2)
                R.NEW(TT.TE.NARRATIVE.2)<1,1> = BR.NAME
                IF R.TELLER<TT.TE.CURRENCY.1> = LCCY THEN
                    R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
                    T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
                    R.NEW(TT.TE.AMOUNT.FCY.1) = ''
                    T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'
                END ELSE
                    R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.TELLER<TT.TE.AMOUNT.FCY.1>
                    T(TT.TE.AMOUNT.FCY.1)<3> = ''
                    R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
                    T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'
                END
**     CALL REBUILD.SCREEN
            END ELSE
                ETEXT = '��� ����� ��� ���' ; CALL STORE.END.ERROR
            END
        END ELSE
            TT.H = ''  ;  TELL.2 = '' ; T.USER = '' ; DEPT = ''  ; DEPT.NAME = ''
            TT.H = COMI:';1'
            CALL F.READ(FN.TELLER.H,TT.H, R.TELLER.H, F.TELLER.H ,E2)
            IF NOT(E2) THEN
                BR = ''
                CURR = R.TELLER.H<TT.TE.CURRENCY.1>
                R.NEW(TT.TE.CURRENCY.1)= R.TELLER.H<TT.TE.CURRENCY.1>
                R.NEW(TT.TE.ACCOUNT.1)= R.TELLER.H<TT.TE.ACCOUNT.1>
                TELL.2= R.TELLER.H<TT.TE.TELLER.ID.2>
                BR = COMP[8,2]
                ACCT.2 = CURR:"10000":BR:"9900":BR
                R.NEW(TT.TE.ACCOUNT.2)= ACCT.2
                LOCAL.REF = R.TELLER.H<TT.TE.LOCAL.REF>
                RECV.BR = LOCAL.REF<1,TTLR.BRANCH.NO>
                IF BR = RECV.BR THEN
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                    CALL DBR('TELLER.ID':@FM:TT.TID.USER,TELL.2,T.USER)
F.ITSS.TELLER.ID = 'FBNK.TELLER.ID'
FN.F.ITSS.TELLER.ID = ''
CALL OPF(F.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID)
CALL F.READ(F.ITSS.TELLER.ID,TELL.2,R.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID,ERROR.TELLER.ID)
T.USER=R.ITSS.TELLER.ID<TT.TID.USER>
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,T.USER,DEPT)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,T.USER,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                    BR.NAME = FIELD(DEPT.NAME,'.',2)
                    R.NEW(TT.TE.NARRATIVE.2)<1,1> = BR.NAME
                    IF R.TELLER.H<TT.TE.CURRENCY.1> = LCCY THEN
                        R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.TELLER.H<TT.TE.AMOUNT.LOCAL.1>
                        T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
                        R.NEW(TT.TE.AMOUNT.FCY.1) = ''
                        T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'
                    END ELSE
                        R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.TELLER.H<TT.TE.AMOUNT.FCY.1>
                        T(TT.TE.AMOUNT.FCY.1)<3> = ''
                        R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
                        T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'
                    END
***   CALL REBUILD.SCREEN
                END ELSE
                    ETEXT = '��� ����� ��� ���' ; CALL STORE.END.ERROR
                END
            END ELSE
                ETEXT = '��� �� ��� �������' ; CALL STORE.END.ERROR
            END
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
