* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.RENEW.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN

        IF COMI NE "1" THEN
            ETEXT = '��� ������ ��� 1 ���'
            CALL REBUILD.SCREEN
        END


*-------------------
        IF (R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.IND>='YES') AND NOT(COMI) THEN
            ETEXT='��� ����� ���� '
        END ELSE
            IF (R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.IND>='NO') AND COMI THEN
                ETEXT ='��� ����� ��������'
            END
        END

        IF COMI = 2 OR COMI = 3 OR COMI= 4 OR COMI =  8  THEN
            IF NOT(R.NEW(LD.INT.LIQ.ACCT)) THEN R.NEW(LD.INT.LIQ.ACCT) = R.NEW(LD.DRAWDOWN.ACCOUNT)

            IF R.NEW(LD.DRAWDOWN.ACCOUNT) NE R.NEW(LD.INT.LIQ.ACCT) THEN
                ETEXT = '����� �������� �� ����� �� ��� �������'
            END
*--- edit by nessma 2011/03/13 ---------------------------------------
            ACCT.12 = R.NEW(LD.DRAWDOWN.ACCOUNT)
            ACCT.67 = R.NEW(LD.PRIN.LIQ.ACCT)
            ACCT.72 = R.NEW(LD.INT.LIQ.ACCT)
            RENEW   = R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.IND>
****-**** COMI=R.NEW(LD.LOCAL.REF)<1,LDLR.RENEW.METHOD>=2 ****-****

            IF RENEW EQ 'YES' THEN
                IF (ACCT.12 NE ACCT.67) OR (ACCT.12 NE ACCT.72) THEN
*                ETEXT = "���� ����� ������� ��� ����� ����� ������ ����� �������"
*                CALL STORE.END.ERROR
                END
            END
*---------------------------------------------------------------------
        END
    END

*-------------------

    RETURN
END
