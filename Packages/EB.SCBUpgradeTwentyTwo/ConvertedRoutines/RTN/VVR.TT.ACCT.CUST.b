* @ValidationCode : MjozMDMxOTUwODI6Q3AxMjUyOjE2NDUwMTM2MzcwMDA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Feb 2022 14:13:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1030</Rating>
*-----------------------------------------------------------------------------

** ----- 11 07 2002 KHALED HOSNY -----
SUBROUTINE VVR.TT.ACCT.CUST

* CHECK ON ACCOUNT.1 (CUSTOMER ACCOUNT) IN TELLER  CASH DEPOSIT
* ACCOUNT.1 MUST BE = 17 , MUST NOT INTERNAL AND NOT NOSTRO ACCOUNT


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY

    IF LEVEL.STATUS THEN RETURN
    IF MESSAGE = 'REPEAT' THEN RETURN

    IF V$FUNCTION = 'I' THEN
        GOSUB INIT
        GOSUB CHECK.ACCT
        IF Y.ERROR = 'Y' THEN RETURN
        GOSUB NARRATIV
        IF Y.ERROR = 'Y' THEN RETURN
        GOSUB CURRENCY.COD
        IF Y.ERROR = 'Y' THEN RETURN
        GOSUB CORR.CURRENCY

    END

RETURN

********************************************************************************************
INIT:
    ETEXT = '' ; MMIIDD= '' ; E1 = ''; DDDD = TODAY
*Line [ 61 ] Update ERROR to Y.ERROR as ERROR is a reserved keyword - ITSS - R21 Upgrade - 2021-12-23
*ERROR = ''
    Y.ERROR = ''

RETURN
*********************************************************************************************
CHECK.ACCT:
*Line [ 68 ] Update ERROR to Y.ERROR as ERROR is a reserved keyword - ITSS - R21 Upgrade - 2021-12-23
*IF  LEN( COMI) # 17  THEN ERROR = 'Y'; ETEXT = 'THE LENGTH OF ACCOUNT IS NOT CORRECT (&)' :@FM : COMI
    IF  LEN( COMI) # 17  THEN Y.ERROR = 'Y'; ETEXT = 'THE LENGTH OF ACCOUNT IS NOT CORRECT (&)' :@FM : COMI
*Line [ 71 ] Update ERROR to Y.ERROR as ERROR is a reserved keyword - ITSS - R21 Upgrade - 2021-12-23
*IF NOT(NUM(COMI)) THEN ERROR = 'Y';ETEXT = 'NOT.ALLOWED.FOR.CUSTOMER ACCT'
    IF NOT(NUM(COMI)) THEN Y.ERROR = 'Y';ETEXT = 'NOT.ALLOWED.FOR.CUSTOMER ACCT'
*Line [ 74 ] Update ERROR to Y.ERROR as ERROR is a reserved keyword - ITSS - R21 Upgrade - 2021-12-23
*IF COMI[12,4] >= '5000' AND COMI[12,4] <='5999' THEN ERROR = 'Y'; ETEXT = 'CATEGORY NOT RELATED TO THE TELLER FUNCTION (&)' :@FM :COMI[12,4]
    IF COMI[12,4] >= '5000' AND COMI[12,4] <='5999' THEN Y.ERROR = 'Y'; ETEXT = 'CATEGORY NOT RELATED TO THE TELLER FUNCTION (&)' :@FM :COMI[12,4]
RETURN
**********************************************************************************************
NARRATIV:
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,COMI[5,7],MMIIDD)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,COMI[5,7],R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    MMIIDD=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>
*Line [ 81 ] Update ERROR to Y.ERROR as ERROR is a reserved keyword - ITSS - R21 Upgrade - 2021-12-23
*IF ETEXT THEN ERROR = 'Y' ;E1 = 'CUSTOMER NUMBER NOT FOUND(&)':@FM:COMI[5,7]
    IF ETEXT THEN Y.ERROR = 'Y' ;E1 = 'CUSTOMER NUMBER NOT FOUND(&)':@FM:COMI[5,7]
    ELSE
        R.NEW(TT.TE.NARRATIVE.2,1) = MMIIDD
    END
RETURN
**********************************************************************************************
CURRENCY.COD:
    MMIIDD = ''
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,COMI[3,2],MMIIDD)
    F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
    FN.F.ITSS.NUMERIC.CURRENCY = ''
    CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
    CALL F.READ(F.ITSS.NUMERIC.CURRENCY,COMI[3,2],R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
    MMIIDD=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
*Line [ 92 ] Update ERROR to Y.ERROR as ERROR is a reserved keyword - ITSS - R21 Upgrade - 2021-12-23
*IF ETEXT THEN ERROR = 'Y' ;E1 = 'CURRENCY NUMBER NOT FOUND(&)':@FM:COMI[3,2]
    IF ETEXT THEN Y.ERROR = 'Y' ;E1 = 'CURRENCY NUMBER NOT FOUND(&)':@FM:COMI[3,2]
    ELSE
        R.NEW(TT.TE.CURRENCY.1) = MMIIDD;T(TT.TE.CURRENCY.1)<3>='NOINPUT'
    END
RETURN
*********************************************************************************************
CORR.CURRENCY:
    IF MMIIDD = LCCY  THEN
        R.NEW(TT.TE.AMOUNT.FCY.1)= ''
        T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
        T(TT.TE.AMOUNT.LOCAL.1)<3>=''
        GOSUB VAL.DATE.LOCAL
    END
    ELSE
        R.NEW(TT.TE.AMOUNT.LOCAL.1)= ''
        T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
        T(TT.TE.AMOUNT.FCY.1)<3>=''
        GOSUB VAL.DATE.FOREIGN
    END
RETURN
*********************************************************************************************
VAL.DATE.LOCAL:
    MMIIDD = ''
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,COMI,MMIIDD)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    MMIIDD=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
    IF NOT(MMIIDD) THEN MMIIDD = 0
    IF  MMIIDD <= 0 THEN
        CALL CDT('', DDDD, '1W')
        R.NEW(TT.TE.VALUE.DATE.1) = DDDD
        R.NEW(TT.TE.EXPOSURE.DATE.1) = DDDD
    END
    ELSE
        R.NEW(TT.TE.VALUE.DATE.1) = TODAY
        R.NEW(TT.TE.EXPOSURE.DATE.1) = TODAY
    END
RETURN
***********************************************************************************************
VAL.DATE.FOREIGN:
    CALL CDT('', DDDD, '2W')
    R.NEW(TT.TE.VALUE.DATE.1) = DDDD
    R.NEW(TT.TE.EXPOSURE.DATE.1) = DDDD


RETURN
***********************************************************************************************

CALL REBUILD.SCREEN


RETURN

END
