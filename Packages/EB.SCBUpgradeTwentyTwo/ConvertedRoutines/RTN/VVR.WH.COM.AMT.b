* @ValidationCode : MjoyOTA5Nzg4Njg6Q3AxMjUyOjE2NDIyNTYxNjA2NzE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 16:16:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.WH.COM.AMT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.FT.COMMISSION.TYPE
    $INSERT           I_F.SCB.WH.TRANS
*----------------------------------------------
* Created by Noha Hamed
    COM.AMT = ""
    IF COMI THEN
        FN.COM = "FBNK.FT.COMMISSION.TYPE" ; F.COM = ''
        CALL OPF(FN.COM,F.COM)

*Line [ 36 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NN = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)

        TOT.AMT  = R.NEW(SCB.WH.TRANS.TOTAL.BALANCE)
        COM.TYPE = R.NEW(SCB.WH.TRANS.COMMISSION.TYPE)

        CALL F.READ( FN.COM,COM.TYPE, R.COM, F.COM, E1)
        COM.PER = R.COM<FT4.PERCENTAGE>
        COM.AMT = (COM.PER * TOT.AMT ) / 100
        R.NEW(SCB.WH.TRANS.COMMISSION.AMT) = COM.AMT
    END
*-- EDIT BY NESSMA 2011/01/23
    R.NEW(SCB.WH.TRANS.TOTAL.COMM.CHRG)= COM.AMT
*----------------------------
    CALL REBUILD.SCREEN

RETURN
END
