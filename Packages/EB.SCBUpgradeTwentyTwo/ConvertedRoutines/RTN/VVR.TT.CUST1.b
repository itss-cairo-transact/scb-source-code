* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*************DINA-SCB************
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CUST1
*A ROUTINE TO EMPTY:ACCOUNT.1,ACCOUNT.2,CUSTOMER.2,CURRENCY.1,CURRENCY.2 IF CUSTOMR IS CHANGED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*------ WAEL -------
*CALL DBR('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,COMI,POST.RES)
*IF POST.RES = 11 THEN T(TT.TE.TRANSACTION.CODE)<3>='NOINPUT' ; R.NEW(TT.TE.TRANSACTION.CODE) = 10
*--------------------

    ETEXT =''

*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,SECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
    IF SECTOR EQ '5000' OR (SECTOR GE '3000' AND SECTOR LE '4000') THEN ETEXT='������ ���'
*ETEXT= 'Wrong.Sector'
    IF R.NEW(TT.TE.CUSTOMER.1) THEN
        IF COMI # R.NEW(TT.TE.CUSTOMER.1) THEN
            R.NEW(TT.TE.ACCOUNT.1) = ''
            R.NEW(TT.TE.ACCOUNT.2) = ''
            R.NEW(TT.TE.CUSTOMER.1)=''
            R.NEW(TT.TE.CURRENCY.1) = ''
            R.NEW(TT.TE.CURRENCY.2) = ''
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
