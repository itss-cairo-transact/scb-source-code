* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
*----- DINA_SCB ---- 18-7-2002

SUBROUTINE VVR.TT.AMOUNT.TO.TILL2
*routine defaults the account.2
*inforce user to rewrite the amount
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER



T.CODE = R.NEW(TT.TE.TRANSACTION.CODE)
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*CALL DBR('TELLER.TRANSACTION':@FM:12,T.CODE,CATEG)
F.ITSS.TELLER.TRANSACTION = 'FBNK.TELLER.TRANSACTION'
FN.F.ITSS.TELLER.TRANSACTION = ''
CALL OPF(F.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION)
CALL F.READ(F.ITSS.TELLER.TRANSACTION,T.CODE,R.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION,ERROR.TELLER.TRANSACTION)
CATEG=R.ITSS.TELLER.TRANSACTION<12>

***********************************************************************************************

 IF R.NEW(TT.TE.CURRENCY.1) AND  COMI THEN

   R.NEW(TT.TE.ACCOUNT.2) = R.NEW(TT.TE.CURRENCY.1):CATEG:R.NEW(TT.TE.TELLER.ID.2)
   END
 IF MESSAGE # 'VAL' THEN

 IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1) THEN ETEXT = '��� ����� ������'
  R.NEW(TT.TE.AMOUNT.FCY.1) = COMI
  CALL REBUILD.SCREEN
END

RETURN
END
