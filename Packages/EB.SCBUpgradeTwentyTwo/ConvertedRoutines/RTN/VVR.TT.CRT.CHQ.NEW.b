* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>355</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CRT.CHQ.NEW

* TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* FOR THAT CUSTOMER
* TO CHECK THAT THE CHEQUE IS NOT STOPPPED

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
****UPDATED BY NESSREEN AHMED 19/8/2017*************
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP.TYPE
****END OF UPDATE 19/8/2017*************************
*------------------------------------------------
    IF COMI  THEN
        ERR.MSG = ''
        ETEXT   = ''

        FN.CHQ.PRES = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRES = ''
        R.CHQ.PRES  = ''
        CALL OPF(FN.CHQ.PRES,F.CHQ.PRES)
*****UPDATED BY NESSREEN AHMED 19/8/2017********************
        FN.CHQ.REG.SUP ='FBNK.CHEQUE.REGISTER.SUPPLEMENT' ; R.CHQ.REG.SUP = ''    ;  F.CHQ.REG.SUP   =''
        CALL OPF(FN.CHQ.REG.SUP,F.CHQ.REG.SUP)
*****END OF UPDATE 19/8/2017********************************

        CHQ.ID.SCB = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NEW.ACCT.OPENED>:'.':COMI
        TEXT = "CHQ.ID =":CHQ.ID.SCB ; CALL REM
        CALL F.READ(FN.CHQ.PRES,CHQ.ID.SCB,R.CHQ.PRES,F.CHQ.PRES,ERR.MSG)
        IF ERR.MSG THEN
            ETEXT = ''
            ETEXT = '�� ���� ��� ����� ����� ���� �����' ; CALL STORE.END.ERROR
        END
        R.NEW(TT.TE.CURRENCY.1) = R.CHQ.PRES<DR.CHQ.CURRENCY>
        CRR = R.NEW(TT.TE.CURRENCY.1)
        IF CRR EQ 'EGP' THEN
            R.NEW(TT.TE.AMOUNT.LOCAL.1) = R.CHQ.PRES<DR.CHQ.AMOUNT>
        END
        IF CRR NE 'EGP' THEN
            R.NEW(TT.TE.AMOUNT.FCY.1) = R.CHQ.PRES<DR.CHQ.AMOUNT>
        END

        R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE> = R.CHQ.PRES<DR.CHQ.CHEQ.DATE>
        R.NEW(TT.TE.ACCOUNT.1) = R.CHQ.PRES<DR.CHQ.NOS.ACCT>
        CH.BN = R.CHQ.PRES<DR.CHQ.BEN>
*Line [ 86 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        BNF = DCOUNT(CH.BN,@VM)
        XX = 0
        IF BNF THEN
            FOR I = 1 TO BNF
                IF R.CHQ.PRES<DR.CHQ.BEN><1,I> THEN
                    XX++
                    R.NEW(TT.TE.NARRATIVE.1)<1,1,XX> = R.CHQ.PRES<DR.CHQ.BEN><1,I>
                END
            NEXT I
            R.NEW(TT.TE.CHEQUE.NUMBER) = COMI
            CALL REBUILD.SCREEN
        END
        CHQ.STAT = R.CHQ.PRES<DR.CHQ.CHEQ.STATUS>
        IF CHQ.STAT = 2 THEN
            ETEXT = ''
            ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
        END
*-------------------------------------------------
**UPDATED BY NESSREEN AHMED 5/9/2016 For R15***
****CHQ.ID.STOP = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NEW.ACCT.OPENED>:"*":COMI
****CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
        ACCT.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NEW.ACCT.OPENED>
****    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST)
        CHQ.NO = TRIM(COMI, "0" , "L")
        KEY.ID = "...":ACCT.NO:".":CHQ.NO
        N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND STATUS EQ STOPPED "
        KEY.LIST.N=""
        SELECTED.N=""
        ER.MSG.N=""
        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        IF SELECTED.N THEN
****UPDATED BY NESSREEN AHMED 19/8/2017*******************
**** ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
            CALL F.READ( FN.CHQ.REG.SUP,KEY.LIST.N, R.CHQ.REG.SUP, F.CHQ.REG.SUP,ERR.SUP)
            STOP.TYPE = R.CHQ.REG.SUP<CC.CRS.PAYM.STOP.TYPE>
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('PAYMENT.STOP.TYPE':@FM:AC.PAT.DESCRIPTION,STOP.TYPE,STOP.DESC)
F.ITSS.PAYMENT.STOP.TYPE = 'F.PAYMENT.STOP.TYPE'
FN.F.ITSS.PAYMENT.STOP.TYPE = ''
CALL OPF(F.ITSS.PAYMENT.STOP.TYPE,FN.F.ITSS.PAYMENT.STOP.TYPE)
CALL F.READ(F.ITSS.PAYMENT.STOP.TYPE,STOP.TYPE,R.ITSS.PAYMENT.STOP.TYPE,FN.F.ITSS.PAYMENT.STOP.TYPE,ERROR.PAYMENT.STOP.TYPE)
STOP.DESC=R.ITSS.PAYMENT.STOP.TYPE<AC.PAT.DESCRIPTION>
            ETEXT = "����� ��� (&) ����� ":@FM:COMI:' * ':STOP.DESC
            CALL STORE.END.ERROR
****END OF UPDATE  19/8/2017*******************
        END
*--------------------------------------------------
        CHQ.STAT = R.CHQ.PRES<DR.CHQ.CHEQ.STATUS>
        IF CHQ.STAT = 3  THEN
            ETEXT = ''
            ETEXT = "����� ����"   ; CALL STORE.END.ERROR
        END
    END
    RETURN
END
