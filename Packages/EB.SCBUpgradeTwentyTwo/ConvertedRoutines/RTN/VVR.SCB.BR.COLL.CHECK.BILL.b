* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>97</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.BR.COLL.CHECK.BILL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH

*----- EDIT BY NESSMA ON 2014/12/31
*Line [ 37 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M      = DCOUNT(R.NEW(SCB.BT.OUR.REFERENCE),@VM)
    HH        = 0
    IF NO.M GT 1 THEN
        FOR K = 1 TO NO.M
* IF R.NEW(SCB.BT.OUR.REFERENCE)<1,AV> EQ R.NEW(SCB.BT.OUR.REFERENCE)<1,K> THEN
            IF COMI EQ R.NEW(SCB.BT.OUR.REFERENCE)<1,K> THEN
                HH ++
            END
        NEXT K
    END
    IF HH GT 1 THEN
        E = "CHECK REFERENCE "
        CALL ERR ; MESSAGE = 'REPEAT'
        CALL STORE.END.ERROR
    END
*------------------------------------------------------------
    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    IF COMI[1,2] EQ "BR" THEN
        CALL F.READ( FN.BR, COMI, R.BR, F.BR, ETEXT)
    END ELSE
        T.SEL = "SSELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": COMI
        CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, E)
        CALL OPF( FN.BR,F.BR)
        CALL F.READ( FN.BR, KEY.LIST, R.BR, F.BR, ETEXT)
    END

    RET.REASON = R.NEW(SCB.BT.RETURN.REASON)<1,AV>

    IF (ETEXT) THEN ETEXT = "������ ��� �����"
    BEGIN CASE
    CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 15
        ETEXT = "�����"

    CASE  R.BR<EB.BILL.REG.CURRENCY> NE "EGP"
**        ETEXT = "��� �� ���� ������ ����"

    CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 14
        ETEXT = "�����"

    CASE (R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT> EQ '' AND RET.REASON EQ '' )
        ETEXT = "CHECK CUST.ACCT"

    CASE NOT(R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 2 OR R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 6)
        ETEXT = "��� ������� �� �������"

    CASE OTHERWISE
        RETURN
    END CASE

    RETURN
END
