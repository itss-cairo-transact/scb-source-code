* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.REST.PAY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE

* Created by Noha Hamed
    TEXT = "1 " ; CALL REM
    KEY.LIST = '' ; SELECTED = 0 ; ASD = '' ; R.DOC = ""
    FN.DOC.PRO="F.SCB.DOCUMENT.PROCURE" ; F.DOC.PRO = ''
    CALL OPF(FN.DOC.PRO,F.DOC.PRO)
    IDD          = ID.NEW
    PAY.AMOUNT  = COMI
    CALL F.READ( FN.DOC.PRO,IDD, R.DOC, F.DOC.PRO, E1)
    TYPE = R.NEW(DOC.PRO.PAYMENT.TYPE)
    NEW.AMT = R.NEW(DOC.PRO.AMOUNT)
    TEXT = "2 " ; CALL REM

    IF TYPE NE 1 THEN

        REST.AMOUNT = R.DOC<DOC.PRO.PAY.REST>
        AMOUNT      = R.DOC<DOC.PRO.AMOUNT>
        IF REST.AMOUNT EQ '' AND AMOUNT EQ '' THEN
            R.NEW(DOC.PRO.PAY.REST)= NEW.AMT-PAY.AMOUNT
            R.NEW(DOC.PRO.PAY.AMOUNT)=PAY.AMOUNT
        END
        IF REST.AMOUNT NE '' AND AMOUNT NE '' THEN
            R.NEW(DOC.PRO.PAY.REST)= REST.AMOUNT-PAY.AMOUNT
            R.NEW(DOC.PRO.PAY.AMOUNT)=PAY.AMOUNT

        END

        CALL REBUILD.SCREEN
    END
    CALL REBUILD.SCREEN
    RETURN
END
