* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>47</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.BR.SLIPS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS

    T.SEL    = '' ; SELECTED = '' ;  KEY.LIST = ''
    FN.SLIPS = 'FBNK.SCB.BR.SLIPS' ; F.SLIPS = '' ; R.SLIPS = ''
    SLIP.REF = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.SLIP.REFER>
    CALL OPF( FN.SLIPS,F.SLIPS)
    CALL F.READ( FN.SLIPS,SLIP.REF, R.SLIPS, F.SLIPS, ETEXT)

*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.ID = DCOUNT (R.SLIPS<SCB.BS.OPERATION.TYPE>,@VM)
    FOR NUMBER.ID = 1 TO DCOUNT.ID
        IF COMI THEN
            CALL F.READ( FN.SLIPS,SLIP.REF, R.SLIPS, F.SLIPS, ETEXT)
*   IF COMI GT R.SLIPS<SCB.BS.REG.AMT><1,NUMBER.ID> THEN
            IF COMI GT R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.TOT.AMT> THEN
                ETEXT = "������ ���� �� ��������"
                RETURN
            END
*            IF R.SLIPS<SCB.BS.REG.NO><1,NUMBER.ID> EQ 1 THEN
*                IF COMI NE R.SLIPS<SCB.BS.REG.AMT><1,NUMBER.ID>  THEN
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.NO.CHQ> EQ 1 THEN
                IF COMI NE R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.TOT.AMT> THEN
                    ETEXT = "������ ��� �� ����� �������"
                    RETURN
                END
            END
        END
    NEXT NUMBER.ID
    CALL REBUILD.SCREEN
*--------------
    RETURN
END
