* @ValidationCode : Mjo0NTgzMzUyMTY6Q3AxMjUyOjE2NDIyNTY1NTY4MTQ6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 16:22:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- INGY-----**
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.TT.DRFT.CHEQ

* TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* FOR THAT CUSTOMER
* TO CHECK THAT THE CHEQUE IS NOT STOPPPED

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.ISSUE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PAYMENT.STOP
****UPDATED BY NESSREEN AHMED 19/8/2017*************
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PAYMENT.STOP.TYPE
*Line [ 50 ] Adding I_F.ACCOUNT - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
****END OF UPDATE 19/8/2017*************************
    IF COMI  THEN
*****UPDATED BY NESSREEN AHMED 19/8/2017********************
        FN.CHQ.REG.SUP ='FBNK.CHEQUE.REGISTER.SUPPLEMENT' ; R.CHQ.REG.SUP = ''    ;  F.CHQ.REG.SUP   =''
        CALL OPF(FN.CHQ.REG.SUP,F.CHQ.REG.SUP)
*****END OF UPDATE 19/8/2017********************************

****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****CHQ.ID = 'DRFT.':R.NEW(TT.TE.ACCOUNT.1):'-':COMI
***  CHQ.ID = 'DRFT.':R.NEW(TT.TE.ACCOUNT.1):'.':COMI
**UPDATED BY NESSREEN AHMED 5/9/2016 For R15***
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.REPRESENTED.COUNT, CHQ.ID, REPRESENT.COUNT)
**      CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED, CHQ.ID, REPRESENT.COUNT)
**      IF REPRESENT.COUNT NE '' THEN
        N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND (STATUS EQ PRESENTED OR STATUS EQ CLEARED) "
        KEY.LIST.N=""
        SELECTED.N=""
        ER.MSG.N=""
        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        IF SELECTED.N THEN
**END OF UPDATED 5/9/2016***************
            ETEXT = '��� ����� �� ���� �� ���' ; CALL STORE.END.ERROR
        END ELSE
            ETEXT=''
            FT.DR=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.FT.DR.CHEQ>
            FT.DR=FIELD(FT.DR,'.',2)
            IF COMI NE  FT.DR  THEN
                ETEXT='�� ����� ��� ��� �������';CALL STORE.END.ERROR
            END
        END
**************************************************************************
**UPDATED BY NESSREEN AHMED 5/9/2016 For R15***
****CHQ.ID.STOP = R.NEW(TT.TE.ACCOUNT.1):"*": COMI
****CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
        ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
        CHQ.NO = TRIM(COMI, "0" , "L")
        KEY.ID = "...":ACCT.NO:".":CHQ.NO
        N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND STATUS EQ STOPPED "
        KEY.LIST.N=""
        SELECTED.N=""
        ER.MSG.N=""
        CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
        IF SELECTED.N THEN
****UPDATED BY NESSREEN AHMED 19/8/2017*******************
****   ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
            CALL F.READ( FN.CHQ.REG.SUP,KEY.LIST.N, R.CHQ.REG.SUP, F.CHQ.REG.SUP,ERR.SUP)
            STOP.TYPE = R.CHQ.REG.SUP<CC.CRS.PAYM.STOP.TYPE>
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('PAYMENT.STOP.TYPE':@FM:AC.PAT.DESCRIPTION,STOP.TYPE,STOP.DESC)
F.ITSS.PAYMENT.STOP.TYPE = 'F.PAYMENT.STOP.TYPE'
FN.F.ITSS.PAYMENT.STOP.TYPE = ''
CALL OPF(F.ITSS.PAYMENT.STOP.TYPE,FN.F.ITSS.PAYMENT.STOP.TYPE)
CALL F.READ(F.ITSS.PAYMENT.STOP.TYPE,STOP.TYPE,R.ITSS.PAYMENT.STOP.TYPE,FN.F.ITSS.PAYMENT.STOP.TYPE,ERROR.PAYMENT.STOP.TYPE)
STOP.DESC=R.ITSS.PAYMENT.STOP.TYPE<AC.PAT.DESCRIPTION>
            ETEXT = "����� ��� (&) ����� ":@FM:COMI :' * ':STOP.DESC
            CALL STORE.END.ERROR
****END OF UPDATE  19/8/2017*******************
        END
**END OF UPDATE 5/9/2016*****************************
**************************************************************************
*  ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
* FN.PAYMENT.STOP = 'F.PAYMENT.STOP' ; F.PAYMENT.STOP ='' ; R.PAYMENT.STOP = '' ; E= ''
* CALL OPF(FN.PAYMENT.STOP,F.PAYMENT.STOP)
* CALL F.READ(FN.PAYMENT.STOP,ACCT.NO, R.PAYMENT.STOP, F.PAYMENT.STOP , E)
* FIRST.NO = R.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
* LAST.NO = R.PAYMENT.STOP<AC.PAY.LAST.CHEQUE.NO>
* CHQ.TYPE = R.PAYMENT.STOP<AC.PAY.CHEQUE.TYPE>
* STOP.TYPE = R.PAYMENT.STOP<AC.PAY.PAYM.STOP.TYPE>

* DD = DCOUNT (STOP.TYPE,VM)
* FOR X = 1 TO DD
* IF CHQ.TYPE<1,X> = 'DRFT' THEN
*    IF LAST.NO<1,X> = "" THEN
*    IF COMI = FIRST.NO<1,X> THEN ETEXT = '��� ����� �����' ; CALL STORE.END.ERROR
* END ELSE
*  FOR I = FIRST.NO<1,X> TO LAST.NO<1,X>
*     IF COMI = I THEN ETEXT = '��� ����� �����' ; CALL STORE.END.ERROR
* NEXT I
*  END
*  END
* NEXT X
***************************************************************************
        CALL F.RELEASE(FN.PAYMENT.STOP,ACCT.NO,F.PAYMENT.STOP)
    END


RETURN
END
