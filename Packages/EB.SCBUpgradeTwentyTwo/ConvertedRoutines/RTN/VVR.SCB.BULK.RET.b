* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>314</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.BULK.RET

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC

    IF MESSAGE EQ "" THEN

        IF COMI THEN
            R.NEW(FT.BKCRAC.DR.CURRENCY)= COMI[9,2]
            R.NEW(FT.BKCRAC.CR.CURRENCY)= COMI[9,2]
            IF COMI[1,3] EQ "EGP" THEN
                CURR = COMI[1,3]
                R.NEW(FT.BKCRAC.DR.CURRENCY)        = CURR
                R.NEW(FT.BKCRAC.CR.CURRENCY)        = CURR
                IF COMI[11,1] EQ "0" THEN
                    DEPT=COMI[12,1]
                END ELSE
                    DEPT=COMI[11,2]
                END
                R.NEW(FT.BKCRAC.PROFIT.CENTRE.DEPT) = DEPT
            END
            IF COMI[7,1] EQ "0" THEN
                DEPT=COMI[8,1]
            END ELSE
                DEPT=COMI[7,2]
            END
            R.NEW(FT.BKCRAC.PROFIT.CENTRE.DEPT) = DEPT
**************
            SAM  = COMI
            CALL TXTINP('ENTER GROUP NUMBER', 8, 23, '20', 'ANY')
            SAM1 = COMI
***************
            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
            T.SEL  = "SELECT F.SCB.RETIREMENTS WITH @ID EQ ":SAM:".":SAM1
            COMI = SAM
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
            FN.RET ='F.SCB.RETIREMENTS' ;R.RET ='';F.RET =''
            CALL OPF(FN.RET,F.RET)
            CALL F.READ(FN.RET,KEY.LIST, R.RET, F.RET,E)
*Line [ 70 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DCOUNT.RET = DCOUNT (R.RET<SCB.RET.ACCOUNT>,@VM)
            FOR I = 1 TO DCOUNT.RET
                R.NEW(FT.BKCRAC.CR.ACCOUNT)<1,I> = R.RET<SCB.RET.ACCOUNT><1,I>
                R.NEW(FT.BKCRAC.CR.AMOUNT)<1,I>  = R.RET<SCB.RET.AMOUNT><1,I>
            NEXT I
            CALL REBUILD.SCREEN
        END
*R.NEW(FT.BKCRAC.DR.ACCOUNT) = SAM
    END
    RETURN
END
