* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SF.TXN.GET.DATA.EXPD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFE.TYPE
*---------------------------------------------
    TMP.CUS = R.NEW(SCB.SF.TR.CUSTOMER.NO)
    TMP.SF.KE.NO = COMI
    TMP.ID = TMP.CUS:".":TMP.SF.KE.NO
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('SCB.SAFEKEEP.RIGHT':@FM:SCB.SAF.RIG.CUSTOMER.NO,TMP.ID,RG.CUS)
F.ITSS.SCB.SAFEKEEP.RIGHT = 'F.SCB.SAFEKEEP.RIGHT'
FN.F.ITSS.SCB.SAFEKEEP.RIGHT = ''
CALL OPF(F.ITSS.SCB.SAFEKEEP.RIGHT,FN.F.ITSS.SCB.SAFEKEEP.RIGHT)
CALL F.READ(F.ITSS.SCB.SAFEKEEP.RIGHT,TMP.ID,R.ITSS.SCB.SAFEKEEP.RIGHT,FN.F.ITSS.SCB.SAFEKEEP.RIGHT,ERROR.SCB.SAFEKEEP.RIGHT)
RG.CUS=R.ITSS.SCB.SAFEKEEP.RIGHT<SCB.SAF.RIG.CUSTOMER.NO>
    IF NOT(RG.CUS) THEN
        ETEXT = 'no record in safe keep right '
    END ELSE

*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('SCB.SAFEKEEP':@FM:SCB.SAF.KEY.NO,TMP.SF.KE.NO,KEY.NO)
F.ITSS.SCB.SAFEKEEP = 'F.SCB.SAFEKEEP'
FN.F.ITSS.SCB.SAFEKEEP = ''
CALL OPF(F.ITSS.SCB.SAFEKEEP,FN.F.ITSS.SCB.SAFEKEEP)
CALL F.READ(F.ITSS.SCB.SAFEKEEP,TMP.SF.KE.NO,R.ITSS.SCB.SAFEKEEP,FN.F.ITSS.SCB.SAFEKEEP,ERROR.SCB.SAFEKEEP)
KEY.NO=R.ITSS.SCB.SAFEKEEP<SCB.SAF.KEY.NO>
        R.NEW( SCB.SF.TR.KEY.NO) = KEY.NO
********************************************************************
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('SCB.SAFEKEEP':@FM:SCB.SAF.TYPE, TMP.SF.KE.NO , TMP.TYPE)
F.ITSS.SCB.SAFEKEEP = 'F.SCB.SAFEKEEP'
FN.F.ITSS.SCB.SAFEKEEP = ''
CALL OPF(F.ITSS.SCB.SAFEKEEP,FN.F.ITSS.SCB.SAFEKEEP)
CALL F.READ(F.ITSS.SCB.SAFEKEEP,TMP.SF.KE.NO,R.ITSS.SCB.SAFEKEEP,FN.F.ITSS.SCB.SAFEKEEP,ERROR.SCB.SAFEKEEP)
TMP.TYPE=R.ITSS.SCB.SAFEKEEP<SCB.SAF.TYPE>
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('SCB.SAFE.TYPE':@FM:SCB.SAF.TY.MARGIN.VALUE, TMP.TYPE , INSR)
F.ITSS.SCB.SAFE.TYPE = 'F.SCB.SAFE.TYPE'
FN.F.ITSS.SCB.SAFE.TYPE = ''
CALL OPF(F.ITSS.SCB.SAFE.TYPE,FN.F.ITSS.SCB.SAFE.TYPE)
CALL F.READ(F.ITSS.SCB.SAFE.TYPE,TMP.TYPE,R.ITSS.SCB.SAFE.TYPE,FN.F.ITSS.SCB.SAFE.TYPE,ERROR.SCB.SAFE.TYPE)
INSR=R.ITSS.SCB.SAFE.TYPE<SCB.SAF.TY.MARGIN.VALUE>

        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)

        CALL F.READ(FN.CUS,R.NEW(SCB.SF.TR.CUSTOMER.NO),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>

        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
        END ELSE
            INSR = ''
        END

        R.NEW(SCB.SF.TR.MARGIN.VALUE) = INSR 
*----------------
        CALL REBUILD.SCREEN
    END
    RETURN
END
