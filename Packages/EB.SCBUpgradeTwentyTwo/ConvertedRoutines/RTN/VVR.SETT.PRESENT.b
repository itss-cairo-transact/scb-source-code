* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>155</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SETT.PRESENT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

* TO CHECK THAT THE ACCOUNT NO OR CHEQUE TYPE IS EXIST
* TO CHECK THAT THE CHEQUE IS ISSUED BEFORE THAT
* TO CHECK THAT THE CHEQUE IS NOT STOPPED
* AND IF CHEQUE IS NOT PRESENTED BEFORE THEN CHECK THAT IT IS ISSUED
* TO CHECK THAT THE CHEQUE IS NOT CANCELLED

****************MAIN PROGRAM************************
    IF MESSAGE = '' THEN
        GOSUB INTIAL
        GOSUB CHQ.ISS.EMPTY
        IF CHQ.NOS THEN
            GOSUB CHQ.NO.EXIST
            IF EXIT.FLG = 'YES' THEN
                GOSUB CHQ.STOP.SUB
                IF CURR = '' THEN
                    GOSUB DRAFT.CHQ.ISSUE
                END
            END
        END
        CALL REBUILD.SCREEN
    END

    GOTO END.PROG
*************************************************************************************************
INTIAL:
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT =''

    RETURN
*********************************************************************************************************

CHQ.ISS.EMPTY:
    FN.CHQ.REG = 'FBNK.CHEQUE.REGISTER' ; F.CHQ.REG = ''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)
    CHQ.ID = 'STTL.':R.NEW(FT.CREDIT.ACCT.NO)
    CALL F.READ(FN.CHQ.REG,CHQ.ID,R.CHQ.REG,F.CHQ.REG,E1)
    IF NOT(E1) THEN
        CHQ.NOS  = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>
        CHQ.STOP = R.CHQ.REG<CHEQUE.REG.STOPPED.CHQS>
    END ELSE
        ETEXT = '��� ����� �� ������ ��� �����'
    END
    RETURN
***************************************************************************
CHQ.NO.EXIST:
    EXIT.FLG = ''
*        TEXT = 'CHQ.NOS' ;  CALL REM
*Line [ 92 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    COUNTS1 =DCOUNT(CHQ.NOS,@VM)
*        TEXT = "COUNTS1": COUNTS1 ; CALL REM
    FOR I = 1 TO COUNTS1 WHILE EXIT.FLG = ''
        IF CHQ.NOS<1,I> THEN
            DASH.POS = INDEX(CHQ.NOS<1,I>,"-",1)
            FIRST.NO  = FIELD(CHQ.NOS<1,I>,"-",1)
            LAST.NO   = FIELD(CHQ.NOS<1,I>,"-",2)
            IF NOT(DASH.POS) THEN
                LAST.NO = FIRST.NO
            END
            IF COMI GE FIRST.NO AND COMI LE LAST.NO THEN
                EXIT.FLG = 'YES'
            END

        END
    NEXT

    IF EXIT.FLG = '' THEN
        ETEXT = '����� ��� (&) �� ������ ���':@FM:COMI ;CALL STORE.END.ERROR
    END

    RETURN
***************************************************************************
CHQ.STOP.SUB:
* TEXT = "CHQ.STOP.SUB" ; CALL REM
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
**** CHQ.ID.STOP = R.NEW(FT.CREDIT.ACCT.NO):"*": COMI
*    TEXT = "CHQ.ID.STOP =":CHQ.ID.STOP ; CALL REM
**** CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID.STOP,CURR)
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,R.NEW(FT.CREDIT.ACCT.NO),CHQ.TYPE)
F.ITSS.CHEQUE.TYPE.ACCOUNT = 'FBNK.CHEQUE.TYPE.ACCOUNT'
FN.F.ITSS.CHEQUE.TYPE.ACCOUNT = ''
CALL OPF(F.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT)
CALL F.READ(F.ITSS.CHEQUE.TYPE.ACCOUNT,R.NEW(FT.CREDIT.ACCT.NO),R.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT,ERROR.CHEQUE.TYPE.ACCOUNT)
CHQ.TYPE=R.ITSS.CHEQUE.TYPE.ACCOUNT<CHQ.TYP.CHEQUE.TYPE>
    TY = CHQ.TYPE
*Line [ 124 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT (TY,@VM)
    FOR X = 1 TO DD
        CHQ.ID = CHQ.TYPE<1,X>:'.':R.NEW(FT.CREDIT.ACCT.NO):'.':COMI
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CURR)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
CURR=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.STOPPED>
*   TEXT = "CURR = ":CURR ; CALL REM
        IF CURR THEN
            ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
        END
    NEXT X
****END OF UPDATE 8/3/2016*****************************
**************************************************************************
* EXIT.LP = ''
*COUNTS1 =DCOUNT(CHQ.STOP,VM)
* FOR I = 1 TO COUNTS1 WHILE EXIT.LP = ''
* IF CHQ.STOP<1,I> THEN
* DASH.POS = INDEX(CHQ.STOP<1,I>,"-",1)
*FIRST.NO  = FIELD(CHQ.STOP<1,I>,"-",1)
* LAST.NO   = FIELD(CHQ.STOP<1,I>,"-",2)
*  IF NOT(DASH.POS) THEN
*       LAST.NO = FIRST.NO
*    END
*                TEXT = "FIRST.NO": FIRST.NO ; CALL REM
*                TEXT = "LAST.NO": LAST.NO ; CALL REM
* END
*  IF COMI GE FIRST.NO AND COMI LE LAST.NO THEN
*       EXIT.LP = 'YES'
*    END
* NEXT I

*  IF EXIT.LP = 'YES' THEN
*       ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
*    END

    RETURN
***************************************************************************
DRAFT.CHQ.ISSUE:
*        TEXT = 'DRAFT.CHQ.ISSUE' ;  CALL REM
    ERR.MSG = ''
    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.CREDIT.ACCT.NO):".": COMI
*        TEXT = "SCB.CHQ.ID": SCB.CHQ.ID ; CALL REM
    CALL F.READ(FN.CHQ.PRESENT,SCB.CHQ.ID,R.CHQ.PRESENT,F.CHQ.PRESENT,ERR.MSG)
    IF ERR.MSG THEN
* ETEXT = '�� ���� ��� ����� ���� ����� ���'
    END
    ELSE
        CHQ.STAT = R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS>
        CHQ.TRNS.PAY = R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT>
        CHQ.PAY.DATE = R.CHQ.PRESENT<DR.CHQ.PAY.DATE>
        CHQ.PAY.BRN = R.CHQ.PRESENT<DR.CHQ.PAY.BRN>
        CHQ.AMT = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
        CHQ.REC.DATE = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
        CHQ.BEN = R.CHQ.PRESENT<DR.CHQ.BEN>
        IF CHQ.STAT = 1 THEN
            ETEXT = '����� ��� ������'  ; CALL STORE.END.ERROR
        END
        ELSE
            IF CHQ.STAT = 2 AND CHQ.TRNS.PAY NE '' AND CHQ.PAY.DATE NE '' AND CHQ.PAY.BRN NE '' THEN
                ETEXT = "����� ��� ����" ; CALL STORE.END.ERROR
            END
            ELSE
                IF CHQ.STAT = 3  THEN
                    ETEXT = "����� ����"   ; CALL STORE.END.ERROR
                END ELSE
* TEXT = "UPDATE.SCB.FT.DR.CHQ" ; CALL REM
* R.NEW(FT.DEBIT.AMOUNT) = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
* R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE> = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
* R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
* R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
* R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER<EB.USE.DEPARTMENT.CODE>
* R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>
                END

            END
        END
        RETURN
***************************************************************************
END.PROG:
        RETURN
    END
