* @ValidationCode : MjotMjkwMzU1NTk3OkNwMTI1MjoxNjQyMjU0NDcxOTk0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:47:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-51</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/10 ***
*******************************************

SUBROUTINE VVR.MUA.5100

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SMS.GROUP
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USR.LOCAL.REF
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.OVERRIDE.INDEX

    WS.COMP = ID.COMPANY
    IF MESSAGE # 'VAL' THEN
        IF COMI NE '5100' AND COMI NE '5200' THEN
            GOSUB CLEAR.USR
            CALL REBUILD.SCREEN
        END
        IF COMI EQ '5100' THEN
            GOSUB GET.SMS
            GOSUB GET.OVR.INDEX
            CALL REBUILD.SCREEN
        END
        IF COMI EQ '5200' THEN
            GOSUB GET.SMS.5200
            GOSUB GET.OVR.INDEX
            CALL REBUILD.SCREEN
        END
    END
RETURN
**-------------------------------------------------------------------
CLEAR.USR:
*    IF R.OLD(MUA.DEP.ACCT.CODE) EQ '5100' THEN
*Line [ 62 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.HMM.CNT = DCOUNT(R.NEW(MUA.HMM.ID),@VM)
    FOR I = 1 TO WS.HMM.CNT
        DEL R.NEW(MUA.HMM.ID)<1,I>
        DEL R.NEW(MUA.FUNCTION)<1,I>
        I --
        WS.HMM.CNT --
    NEXT I
*   END
RETURN
**-------------------------------------------------------------------
EDIT.USR:
RETURN
**-------------------------------------------------------------------
GET.SMS:
    WS.USR.DPT           = R.NEW(MUA.DEPARTMENT.CODE)
**STANDARD
    T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496') BY @ID"

    IF WS.USR.DPT = 1 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 2 OR WS.USR.DPT = 20 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 4 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 50 OR WS.USR.DPT = 51 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 11 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A435' 'A447' 'A461' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 35 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 3 OR WS.USR.DPT = 10 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END

    IF WS.USR.DPT = 36 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A401' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' ) BY @ID"
    END

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR ISMS = 1 TO SELECTED
            R.NEW(MUA.HMM.ID)<1,ISMS>     = KEY.LIST<ISMS>[2,3]
            R.NEW(MUA.FUNCTION)<1,ISMS>   = 'A.AUTHORISER'
            IF R.NEW(MUA.HMM.ID)<1,ISMS> EQ 460 THEN
                R.NEW(MUA.FUNCTION)<1,ISMS>   = 'S.SEE'
            END
        NEXT ISMS
    END
RETURN
**-------------------------------------------------------------------
GET.OVR.INDEX:
    IF COMI = 5200 THEN
        T.SEL.OVR = "SELECT F.SCB.OVERRIDE.INDEX WITH @ID NE DRMT AND GIVE.TO.USER EQ 'Y' AND ( COMPANY.CODE EQ '' OR COMPANY.CODE EQ ":ID.COMPANY:" ) BY @ID"
    END ELSE
        T.SEL.OVR = "SELECT F.SCB.OVERRIDE.INDEX WITH GIVE.TO.USER EQ 'Y' AND ( COMPANY.CODE EQ '' OR COMPANY.CODE EQ ":ID.COMPANY:" ) BY @ID"
    END
    CALL EB.READLIST(T.SEL.OVR,OVR.KEY,"",SELECTED.OVR,ER.OVR)
    IF SELECTED.OVR THEN
        FOR IOVR = 1 TO SELECTED.OVR
            R.NEW(MUA.OVER.CLASS)<1,IOVR>   = OVR.KEY<IOVR>
*Line [ 130 ] Update SELECTED.OVR to IOVR - ITSS - R21 Upgrade - 2021-12-26
*NEXT SELECTED.OVR
        NEXT IOVR
    END
RETURN
**-------------------------------------------------------------------
GET.SMS.5200:
    WS.USR.DPT           = R.NEW(MUA.DEPARTMENT.CODE)
**STANDARD
    T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"

    IF WS.USR.DPT = 1 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 2 OR WS.USR.DPT = 20 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 4 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 50 OR WS.USR.DPT = 51 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400'  'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 11 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A429' 'A435' 'A447' 'A461' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 35 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END
    IF WS.USR.DPT = 3 OR WS.USR.DPT = 10 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' 'A496' ) BY @ID"
    END

    IF WS.USR.DPT = 36 THEN
        T.SEL = "SELECT F.USER.SMS.GROUP WITH @ID LIKE A4... WITHOUT @ID IN ( 'A400' 'A404' 'A427' 'A429' 'A432' 'A435' 'A439' 'A447' 'A461' 'A462' 'A480' 'A491' 'A497' 'A499' 'A48' ) BY @ID"
    END

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR ISMS = 1 TO SELECTED
            R.NEW(MUA.HMM.ID)<1,ISMS>     = KEY.LIST<ISMS>[2,3]
            R.NEW(MUA.FUNCTION)<1,ISMS>   = 'A.AUTHORISER'
            IF R.NEW(MUA.HMM.ID)<1,ISMS> EQ 460 THEN
                R.NEW(MUA.FUNCTION)<1,ISMS>   = 'S.SEE'
            END
        NEXT ISMS
    END
RETURN
