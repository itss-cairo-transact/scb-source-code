* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN  -----

    SUBROUTINE VVR.TT.EMP.CURR.ACC

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
    IF COMI NE R.NEW(TT.TE.CUSTOMER.1) THEN
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POSS=R.ITSS.CUSTOMER<EB.CUS.TEXT>
        STO1=POSS<1,1>
        STO2=POSS<1,2>
        STO3=POSS<1,3>
        STO4=POSS<1,4>
        STO5=POSS<1,5>
        STO6=POSS<1,6>
        STO7=POSS<1,7>
        STO8=POSS<1,8>
        STO9=POSS<1,9>
        STO10=POSS<1,10>

        IF POSS NE '' THEN
            TEXT = " -������� �������- " : STO1 : STO2 : STO3 :STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
        END
    END

*To Check If Customer Changed Then fields account,currency will be empty


    IF COMI # R.NEW(TT.TE.CUSTOMER.1 ) THEN

        R.NEW( TT.TE.CURRENCY.1 )= ''
        R.NEW( TT.TE.ACCOUNT.1 )= ''

    END

    CALL REBUILD.SCREEN
*************************

*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,COMI,POST.REST)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POST.REST=R.ITSS.CUSTOMER<EB.CUS.POSTING.RESTRICT>
*******NESSREEN AHMED 8/3/2007**********
****UPDATED BY NESSREEN AHMED 21/12/2008  ***REMOVING HASHING*********
  IF POST.REST = 12 OR POST.REST = 14 OR POST.REST = 16 OR POST.REST = 23 OR POST.REST = 25 OR POST.REST = 76 OR POST.REST = 71 OR POST.REST = 72 OR POST.REST = 78 OR POST.REST = 80 OR POST.REST = 90 OR POST.REST = 99 OR POST.REST= 70 OR POST.REST = 13 OR POST.REST = 1 THEN
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*      CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST.REST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST.REST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
***UPDATED BY NESSREEN AHMED 24/11/2014***
***      ETEXT = '���� ������ ':DESC ; CALL STORE.END.ERROR
***END OF UPDATE 24/11/2014***
  END


    RETURN

END
