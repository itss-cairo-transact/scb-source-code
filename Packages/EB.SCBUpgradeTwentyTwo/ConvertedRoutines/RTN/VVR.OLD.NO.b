* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>226</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.OLD.NO

*TO CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUES.STOPPED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS



    IF MESSAGE # 'VAL' THEN
        IF COMI THEN
            FN.SCB.FT.DR.CHQ = 'F.SCB.FT.DR.CHQ' ; F.SCB.FT.DR.CHQ = ''
            CALL OPF(FN.SCB.FT.DR.CHQ,F.SCB.FT.DR.CHQ)

            T.SEL ="SELECT F.SCB.FT.DR.CHQ WITH OLD.CHEQUE.NO EQ ": COMI
*** T.SEL ="SELECT F.SCB.FT.DR.CHQ WITH CHEQ.NO EQ ": COMI
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            CALL F.READ(FN.SCB.FT.DR.CHQ,KEY.LIST,R.CHQ.PRESENT,F.SCB.FT.DR.CHQ,E1)


            R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>   = R.CHQ.PRESENT<DR.CHQ.CURRENCY>
            TEXT=R.CHQ.PRESENT<DR.CHQ.CURRENCY>:'CUR';CALL REM

******HYTHAM------- 20120117****************************
            IF R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,2] NE 'PL' THEN
                R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO> = R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>
            END
******HYTHAM------- 20120117****************************
TEXT= R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,2]:'DR ACCT';CALL REM
            IF R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,2] NE 'PL' THEN
*******IF R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,2] EQ 'PL' THEN
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>,CUSSS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSSS=R.ITSS.ACCOUNT<AC.CUSTOMER>

*******CALL DBR('CATEGORY':@FM:,R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>,CUSSS)

***     END
***     R.NEW(FT.LOCAL.REF)<1,FTLR.DRAWER> = R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[2,7]
                R.NEW(FT.LOCAL.REF)<1,FTLR.DRAWER> = CUSSS
*TEXT = "CUS : " : CUSSS ; CALL REM
                R.NEW(FT.CHEQUE.NUMBER) = R.CHQ.PRESENT<DR.CHQ.CHEQ.NO>
                TEXT= R.CHQ.PRESENT<DR.CHQ.CHEQ.NO>:'CHQ NO';CALL REM
                 TEXT=  R.NEW(FT.CHEQUE.NUMBER):'R.NEW';CALL REM
                R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> = R.CHQ.PRESENT<DR.CHQ.ISSUE.BRN>
                R.NEW(FT.DEBIT.CURRENCY) = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
                R.NEW(FT.DEBIT.ACCT.NO) = R.CHQ.PRESENT<DR.CHQ.NOS.ACCT>
                R.NEW(FT.DEBIT.AMOUNT) = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
                R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE> = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
                R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
                R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
                R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>
                R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER< EB.USE.DEPARTMENT.CODE>
            END
******  CALL REBUILD.SCREEN
            IF R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>[1,2] EQ 'PL' THEN
*TEXT = "ACCT  :" R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT> ; CALL REM

*                CALL DBR('CATEGORY':@FM:EB.CAT.MNEMONIC,R.CHQ.PRESENT<DR.CHQ.DEBIT.ACCT>,CUSSS)
*               R.NEW(FT.LOCAL.REF)<1,FTLR.DRAWER> = CUSSS
*TEXT = "CUS : " : CUSSS ; CALL REM
                R.NEW(FT.CHEQUE.NUMBER) = R.CHQ.PRESENT<DR.CHQ.CHEQ.NO>
                R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> = R.CHQ.PRESENT<DR.CHQ.ISSUE.BRN>
                R.NEW(FT.DEBIT.CURRENCY) = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
                R.NEW(FT.DEBIT.ACCT.NO) = R.CHQ.PRESENT<DR.CHQ.NOS.ACCT>
                R.NEW(FT.DEBIT.AMOUNT) = R.CHQ.PRESENT<DR.CHQ.AMOUNT>
                R.NEW(FT.LOCAL.REF)<1,FTLR.RECEIV.DATE> = R.CHQ.PRESENT<DR.CHQ.CHEQ.DATE>
                R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = R.CHQ.PRESENT<DR.CHQ.BEN,1>
                R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2> = R.CHQ.PRESENT<DR.CHQ.BEN,2>
                R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = R.CHQ.PRESENT<DR.CHQ.OLD.CHEQUE.NO>
                R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER< EB.USE.DEPARTMENT.CODE>
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN

***************************************************************************

END
