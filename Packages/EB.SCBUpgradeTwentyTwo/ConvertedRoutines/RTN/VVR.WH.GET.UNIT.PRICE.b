* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.WH.GET.UNIT.PRICE

*1- TO GET THE UNIT.PRICE FROM SCB.WH.ITEMS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*-----------------------------------------
***FOR I = 1 TO DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),VM)
***    IF R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I> THEN ETEXT = "�� �������� �� ���"
***NEXT I
*IF NOT(R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,AV>) THEN
    R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,AV> = COMI
    XX = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,AV>
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('SCB.WH.ITEMS':@FM:SCB.WH.IT.UNIT.PRICE,XX,UNT.PR)
F.ITSS.SCB.WH.ITEMS = 'F.SCB.WH.ITEMS'
FN.F.ITSS.SCB.WH.ITEMS = ''
CALL OPF(F.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS)
CALL F.READ(F.ITSS.SCB.WH.ITEMS,XX,R.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS,ERROR.SCB.WH.ITEMS)
UNT.PR=R.ITSS.SCB.WH.ITEMS<SCB.WH.IT.UNIT.PRICE>
    R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,AV>= UNT.PR
*-- EDIT BY NESSMA
    CUR.CODE = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)
    CUR.CODE = CUR.CODE[11,2]
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.CODE,CUR.NAME)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CUR.CODE,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR.NAME=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
    R.NEW(SCB.WH.TRANS.CURRENCY) = CUR.NAME
    ITEM.ID = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)

*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('SCB.WH.ITEMS':@FM:SCB.WH.IT.UNITS.BALANCE,ITEM.ID,UNT.ITEM)
F.ITSS.SCB.WH.ITEMS = 'F.SCB.WH.ITEMS'
FN.F.ITSS.SCB.WH.ITEMS = ''
CALL OPF(F.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS)
CALL F.READ(F.ITSS.SCB.WH.ITEMS,ITEM.ID,R.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS,ERROR.SCB.WH.ITEMS)
UNT.ITEM=R.ITSS.SCB.WH.ITEMS<SCB.WH.IT.UNITS.BALANCE>
*    R.NEW(SCB.WH.TRANS.NO.OF.UNITS) = UNT.ITEM
*-----------------
    CALL REBUILD.SCREEN
*END
    RETURN
END
