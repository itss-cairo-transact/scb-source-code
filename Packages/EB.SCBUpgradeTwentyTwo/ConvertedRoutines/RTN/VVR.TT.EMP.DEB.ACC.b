* @ValidationCode : MjotMTAwODk0MTEwOkNwMTI1MjoxNjQyMjU2NTA4NzYyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 16:21:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN  -----

SUBROUTINE VVR.TT.EMP.DEB.ACC

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER

*A ROUTINE TO EMPTY ACCOUNT.2 IF CURRENCY.2

*IF COMI # R.NEW(TT.TE.CURRENCY.2 ) THEN R.NEW(TT.TE.ACCOUNT.2 )= ''
    IF COMI # R.NEW(TT.TE.CURRENCY.1 ) THEN R.NEW(TT.TE.ACCOUNT.1 )= ''

    CALL REBUILD.SCREEN


RETURN

END
