* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TELLER.SECURITY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*IF MESSAGE NE "VAL" THEN
*IF COMI<1,1> THEN COMI<1,2> = COMI<1,1>

    FN.SR = 'F.SCB.SR.SCRIPT' ; F.SR = '' ; R.SR = ''
    T.SEL = "SELECT F.SCB.SR.SCRIPT WITH @ID EQ ": COMI

    CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, E)

    CALL OPF( FN.SR,F.SR)
    CALL F.READ( FN.SR, KEY.LIST, R.SR, F.SR, ETEXT)
    IF NOT(KEY.LIST) THEN
        ETEXT = "������ ��� �����"
    END ELSE
        R.NEW(TT.TE.NARRATIVE.2)<1,1> = R.SR<SR.SCRIPT.CUSTOMER.CODE>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.SR<SR.SCRIPT.CUSTOMER>,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,R.SR<SR.SCRIPT.CUSTOMER>,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME = LOC.REF<1,CULR.ARABIC.NAME>
        R.NEW(TT.TE.NARRATIVE.2)<1,2>    = CUST.NAME
        R.NEW(TT.TE.NARRATIVE.2)<1,3>    = R.SR<SR.SCRIPT.ADDRESS>
        R.NEW(TT.TE.NARRATIVE.2)<1,4>    = R.SR<SR.SCRIPT.BUY.SHARES>
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,1> = R.SR<SR.SCRIPT.TOTAL.SHARES>
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,2> = R.SR<SR.SCRIPT.TOTAL.CHARGE>
        R.NEW(TT.TE.CUSTOMER.1)          = "1300466"
        R.NEW(TT.TE.ACCOUNT.1)<1,1>      = "0130046610101001"
        R.NEW(TT.TE.ACCOUNT.1)<1,2>      = "0130046610101002"
        R.NEW(TT.TE.NARRATIVE.1)<1,2>    = COMI
        CALL REBUILD.SCREEN
    END
*END

    RETURN
END
