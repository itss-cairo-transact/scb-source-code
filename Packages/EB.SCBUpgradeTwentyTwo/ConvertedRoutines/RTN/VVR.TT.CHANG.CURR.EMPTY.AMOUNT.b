* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*----------------15-7-2002 ZOZO SCB-----------------------*
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.TT.CHANG.CURR.EMPTY.AMOUNT
*CHECK IF CURRENCY.1 =LCCY ,ACTIVATE AMOUNT.LOCAL & DEACTIVATE AMOUNT.FOREIGN
*CHECK IF CURRENCY.1 =FCCY ,ACTIVATE AMOUNT.FOREIGN & DEACTIVATE AMOUNT.LOCAL
*COMI = CURRENCY.1
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
IF COMI = LCCY THEN
     T(TT.TE.AMOUNT.LOCAL.1)<3>=''
     T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
END ELSE
     T(TT.TE.AMOUNT.FCY.1)<3>=''
     T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
END
RETURN
END
