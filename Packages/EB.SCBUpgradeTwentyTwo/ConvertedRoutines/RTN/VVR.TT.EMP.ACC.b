* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*************NESSREEN-SCB************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.EMP.ACC
*A ROUTINE TO EMPTY:ACCOUNT.1,ACCOUNT.2,CURRENCY.2 IF CURRENCY IS CHANGED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    ETEXT = ''

*    IF COMI NE "EGP" AND COMI NE "SAR" AND COMI NE "USD" AND COMI NE "KWD" AND COMI NE "GBP" AND COMI NE "EUR" AND COMI NE "JPY" AND COMI NE "CHF" AND COMI NE "CAD" AND COMI NE "AUD" AND COMI NE "NOK" AND COMI NE "SEK" AND COMI NE "DKK" THEN
*        ETEXT = '����� ������� ��� ��� ������'
*    END ELSE

    IF R.NEW(TT.TE.CURRENCY.1) THEN
        IF COMI # R.NEW(TT.TE.CURRENCY.1) THEN
            R.NEW(TT.TE.ACCOUNT.1) = ''
            R.NEW(TT.TE.ACCOUNT.2) = ''
*R.NEW(TT.TE.CUSTOMER.1)=''
            R.NEW(TT.TE.CURRENCY.2) = ''
            CALL REBUILD.SCREEN
        END
    END
*   END
    RETURN
END
