* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.BR.COLL.RET.CHECK.F

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH


    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    IF COMI[1,2] EQ "BR" THEN
        T.SEL = "SSELECT FBNK.BILL.REGISTER WITH @ID EQ ": COMI
    END ELSE
        T.SEL = "SSELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": COMI
    END
    CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, E)

    CALL OPF( FN.BR,F.BR)
    CALL F.READ( FN.BR, KEY.LIST, R.BR, F.BR, ETEXT)

    IF NOT(KEY.LIST) THEN ETEXT = "������ ��� �����"

    IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 15 OR R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 8 THEN
        ETEXT = "�����"
    END
*    IF R.BR<EB.BILL.REG.CURRENCY> EQ  "EGP" THEN
*        ETEXT = "��� ��� ���� ������ ����"
*    END



    IF R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 14 OR R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 7 THEN
        ETEXT = "�����"
    END

    RETURN
END
