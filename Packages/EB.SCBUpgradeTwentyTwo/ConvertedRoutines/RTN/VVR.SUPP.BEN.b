* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SUPP.BEN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUPPLIER

    SS.CODE = COMI
    IF MESSAGE # 'VAL' THEN
        IF SS.CODE THEN
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('SCB.SUPPLIER':@FM:SUPP.SUPP.NAME,SS.CODE,SS.NAME)
F.ITSS.SCB.SUPPLIER = 'F.SCB.SUPPLIER'
FN.F.ITSS.SCB.SUPPLIER = ''
CALL OPF(F.ITSS.SCB.SUPPLIER,FN.F.ITSS.SCB.SUPPLIER)
CALL F.READ(F.ITSS.SCB.SUPPLIER,SS.CODE,R.ITSS.SCB.SUPPLIER,FN.F.ITSS.SCB.SUPPLIER,ERROR.SCB.SUPPLIER)
SS.NAME=R.ITSS.SCB.SUPPLIER<SUPP.SUPP.NAME>
            R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1> = SS.NAME
**        R.NEW(FT.BEN.CUSTOMER) = SS.NAME
        END ELSE
            ETEXT = '��� ����� ��� ������'
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
