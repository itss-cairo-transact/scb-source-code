* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>964</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.ACCOUNT.FIELD.POST

*TO CHECK IF FIELD LENGTH # 17 AND IT IS  A NOSTRO OR INTERNAL ACCOUNT
*THEN ERROR ELSE DEFAULT NARRATIVE WITH CUSTOMER.SHORT.NAME AND CURRENCY.1 WITH THE CURRENCY IN ACCOUNT.2
*IF CURRENCY IS LOCAL CURR THEN LET FIELD AMOUNT.FCY IS NOINPUT
*ELSE LET AMOUNT.LOCAL IS NOINPUT
*DEFAULT EXPOSURE.DATE.2 AND VALUE.DATE.2 EQ TODAY

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL

    IF COMI THEN
***********************************************************
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,COMI,POS.REST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
POS.REST=R.ITSS.ACCOUNT<AC.POSTING.RESTRICT>
*TEXT = COMI ;  CALL REM
        IF POS.REST = 71 THEN
*TEXT = POS.REST ; CALL REM
            ETEXT = ' ����� ������� �� ������ ����� ����� '
            GOTO PROGRAM.END
        END
***********************************************************
        IF  LEN(COMI) = 16  THEN
            GOSUB INTIAL
******Updated by Nessreen Ahmed 18/4/2016 for R15*************************
******            CALL DBR( 'AC.COLLATERAL':@FM:1,COMI,ACCFND)
*TEXT = 'ACCFND':ACCFND ; CALL REM
** IF ACCFND THEN ETEXT = '��� ������ �� �����' ; CALL STORE.END.ERROR
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*************NESSREEN AHMED 4/7/2005*************************
******            IF ACCFND THEN
*  TEXT = 'HI' ; CALL REM
******                FN.COLLATERAL = 'FBNK.COLLATERAL' ; F.COLLATERAL = '' ; R.COLLATERAL = '' ; RETRY = '' ; E1 = ''
******                KEY.TO.USE = ACCFND
* TEXT = 'KEY':KEY.TO.USE ; CALL REM
******                CALL OPF(FN.COLLATERAL,F.COLLATERAL)
******                CALL F.READ(FN.COLLATERAL,KEY.TO.USE,R.COLLATERAL,F.COLLATERAL,E1)
******                APPID = R.COLLATERAL<COLL.APPLICATION.ID>
*  TEXT = 'APPID':APPID ; CALL REM
******                NOMV = R.COLLATERAL<COLL.NOMINAL.VALUE>
*  TEXT = 'NOMV':NOMV ; CALL REM
******                EXCV = R.COLLATERAL<COLL.EXECUTION.VALUE>
*  TEXT = 'EXV':EXCV ; CALL REM
******                IF APPID = COMI THEN
******                    AVAIL = SSUB(NOMV,EXCV)
*     TEXT = 'AVAIL=':AVAIL ; CALL REM
******                END
******            END
****End of Update 18/4/2016 for R15 ****************************
*********************************************************************
            GOSUB CHECK.IF.NOS.OR.INT
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR( 'ACCOUNT':@FM:AC.CONDITION.GROUP,COMI,GROUP1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
GROUP1=R.ITSS.ACCOUNT<AC.CONDITION.GROUP>
* IF GROUP1 = '22' THEN ETEXT = 'NOT.ALLOWED.GROUP'
        END ELSE
* ETEXT = 'Invalid.Mask'
            ETEXT='��� �� ����� ������'
        END

    END


    GOTO PROGRAM.END

***********************************************************************************************************************

INTIAL:

*-------------TO INTIALIZE ALL VARIABLES -----------------------------------------*

    ETEXT = '' ; E = '' ; CURR.NAME1 = '' ; SHOR.NAME1 = ''


    RETURN
***********************************************************************************************************************

CHECK.IF.NOS.OR.INT:


*-------------TO CHECK IF IT IS NOSTRO OR INTERNAL ACCOUNT------------------------*

    IF COMI # R.NEW(TT.TE.ACCOUNT.1)<1,AV> THEN GOSUB CLEAN.FIELDS

    IF NOT(NUM(COMI)) THEN ETEXT='��� ����� ������ ������ ������'
*ETEXT = 'Not.Allowed.For.Internal.Account'
*CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)

    IF CATEG >= '5000' AND CATEG <='5999' THEN ETEXT='��� ����� ������ ������ ������'
*ETEXT = 'Not.Allowed.For.NOSTRO.Account'
    ELSE GOSUB GET.CUSNAME.CURR



    RETURN
* **********************************************************************************************************************

GET.CUSNAME.CURR:

    ETEXT = ''
*Line [ 144 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY,COMI,CURR.NAME1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.NAME1=R.ITSS.ACCOUNT<AC.CURRENCY>
* CALL DBR( 'NUMERIC.CURRENCY':@FM:1,COMI[9,2],CURR.NAME1)
    IF ETEXT THEN E = ETEXT

    R.NEW(TT.TE.CURRENCY.1) = CURR.NAME1
    R.NEW(TT.TE.CURRENCY.2) = CURR.NAME1

    ETEXT = ''

*CUST.NO = TRIM( COMI[ 5, 7], '0', 'L')
******UPDATED BY NESSREEN AHMED 4/3/2013******************
    IF MESSAGE = '' THEN
*Line [ 162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.SHORT.TITLE,COMI,SHOR.NAME1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
SHOR.NAME1=R.ITSS.ACCOUNT<AC.SHORT.TITLE>
        IF ETEXT THEN E = ETEXT
        R.NEW(TT.TE.NARRATIVE.2) = SHOR.NAME1
    END
*****END OF UPDATE 4/3/2013*************************
    GOSUB CHECK.IF.CURR.LOCAL


    RETURN

******************************************************************************************************************
CHECK.IF.CURR.LOCAL:

    IF CURR.NAME1 = LCCY THEN

        T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
        T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'

    END ELSE

        T(TT.TE.AMOUNT.FCY.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
        T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'

    END

    GOSUB DEF.DAT

    RETURN

**********************************************************************************************************************
DEF.DAT:

    R.NEW(TT.TE.VALUE.DATE.1) = TODAY
* R.NEW(TT.TE.EXPOSURE.DATE.2) = TODAY


    RETURN
***********************************************************************************************************************
CLEAN.FIELDS:

    R.NEW(TT.TE.ACCOUNT.1)<1,AV> = ''
    R.NEW(TT.TE.ACCOUNT.2) = ''
    R.NEW(TT.TE.CURRENCY.1)= ''
    R.NEW(TT.TE.CURRENCY.2)= ''
    R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
    R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
    R.NEW(TT.TE.NARRATIVE.2) = ''

    RETURN

***********************************************************************************************************************
PROGRAM.END:

    CALL REBUILD.SCREEN

    RETURN
END
