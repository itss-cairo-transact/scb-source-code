* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>295</Rating>
*-----------------------------------------------------------------------------
** ----- ?.??.2002 Unknown SCB -----
** ----- 6.06.2002 Pawel TEMENOS -----
    SUBROUTINE VVR.XX.ONLY.ARABIC.ACT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*THE ROUTINE ACCEPT ONLY ARABIC CHARACTERS
*AND REPLACES AND DELETE THE UNDESIRABLE ARABIC CHARACTERS

    IF COMI THEN

        LEN.COMI = LEN( COMI)
        FOR I = 1 TO LEN.COMI

            CHARACTER = COMI[ I, 1]
            IF NOT( CHARACTER >= CHAR( 193) AND CHARACTER <= CHAR( 237) OR  CHARACTER = CHAR( 32) OR  ( CHARACTER >= CHAR( 40) AND CHARACTER <= CHAR( 57))) THEN ETEXT = '���� ����� ������� ���' ; EXIT
*ETEXT = 'ONLY.ARABIC.LETTERS' ; EXIT

        NEXT I

        IF NOT( ETEXT) THEN CONVERT CHAR( 194):CHAR( 195):CHAR( 197):CHAR( 201):CHAR( 236):CHAR( 220) TO CHAR( 199):CHAR( 199):CHAR( 199):CHAR( 229):CHAR( 237) IN COMI

        IF NOT( ETEXT) THEN COMI = TRIM( COMI)
    END
* IF APPLICATION = 'CUSTOMER'  AND V$FUNCTION = 'I' AND MESSAGE = 'VAL' THEN
* R.NEW(EB.CUS.SHORT.NAME)<1,2> = R.NEW(EB.CUS.LOCAL.REF)<1,1>
* CALL REBUILD.SCREEN
*END
    RETURN
END
