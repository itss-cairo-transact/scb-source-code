* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE   VVR.TT.DR.CH.TYPE


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    * TO CHECK IF THE CHECK TYPE IS NOT DRAFT THEN PRODUCE ERROR MESSAGE
    * IF THE USER CHANGE THE VALUE IN THIS FIELD THEN
    * EMPTY FIELD DR.CHEQ AND CHEQUE.NUMBER

    IF COMI # '1 DRAFT- ��� ����� ����� �����' AND COMI # '2 DRAFT-��� ����� ����� ���������' THEN
        ETEXT = '���� �� ���� ��� �����' ;CALL STORE.END.ERROR
    END
    IF COMI NE R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.TYPE> THEN
        R.NEW(TT.TE.LOCAL.REF)<1,TTLR.FT.DR.CHEQ>=''
        R.NEW(TT.TE.CHEQUE.NUMBER)=''
        CALL REBUILD.SCREEN
    END
    LINK.DATA<TNO> = COMI
    RETURN
END
