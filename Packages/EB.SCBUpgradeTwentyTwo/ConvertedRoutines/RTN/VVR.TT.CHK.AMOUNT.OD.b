* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN  -----
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.AMOUNT.OD

*TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER
*IF THE ACCOUNT'S CATEGORY IS 1201 THEN CHECK IF THE WORKING BALANCE IS LT ZERO
*THEN SUBTRACT THE AMOUNT IN THE WORKING BALANCE FROM THE ENTERED AMOUNT AND IF IT
*GT ZERO THEN ETEXT SAYS THAT SHOULD ONLY ENTER THE REMAINING AMOUNT TO COVER THE DEBIT ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    ETEXT = ''    ; TEXT = ''
    IF MESSAGE =  '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN
            ETEXT = '��� ����� ������'
            R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
        END
    END
    ACCT = R.NEW(TT.TE.ACCOUNT.1)
    CATEG = ACCT[11,4]
    IF (CATEG GE 1500 AND CATEG LE 1590 ) THEN
        WB = 0
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACCT,WB)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WB=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
        IF WB < 0 THEN
            X = SADD(COMI,WB)
            IF X > 0 THEN
                TEXT = '��� ��� ����':WB:'� ��� ��� ���� ���� ������ ������' ; CALL REM
            END
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
