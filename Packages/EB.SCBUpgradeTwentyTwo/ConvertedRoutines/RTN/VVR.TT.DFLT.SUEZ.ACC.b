* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.TT.DFLT.SUEZ.ACC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*** UPDATED BY MOHAMED SABRY 2014/08/22

    WS.AMT = COMI

    WS.AMT.CHK = WS.AMT / 10
    WS.F.CHK = FIELD (WS.AMT.CHK,".",2)

    IF WS.F.CHK GT 0 THEN
        ETEXT = '���� ������� ��� �� ���� ������ ��� 10'
        CALL STORE.END.ERROR
    END

    IF PGM.VERSION EQ ",SCB.DEPST.TO.SUEZ.C" THEN
        IF WS.AMT GE 1000 THEN
            ETEXT = '��� ��� ���� ���� ������� �� 990'
            CALL STORE.END.ERROR
        END

        USR.BR = ''
        USR = OPERATOR
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,USR,USR.BR)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,USR,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USR.BR=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
        BR = FMT(USR.BR, "R%2")
        BR.ACCT = "EGP16103000100":BR
        R.NEW(TT.TE.ACCOUNT.1)= BR.ACCT
* R.NEW(TT.TE.ACCOUNT.2)= ACCT.2

    END


    CALL REBUILD.SCREEN

    RETURN

END
