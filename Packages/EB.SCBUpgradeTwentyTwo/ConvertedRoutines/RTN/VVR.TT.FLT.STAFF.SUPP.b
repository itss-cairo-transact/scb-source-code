* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
* --MAI SAAD 13/09/2020 -----
*-------------------------------------------------------------------------
* <Rating>-4</Rating>
*-------------------------------------------------------------------------
    SUBROUTINE VVR.TT.FLT.STAFF.SUPP
*TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER AND DEFAULT ACC
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    ETEXT = ''    ; TEXT = ''
    IF MESSAGE =  '' THEN
        IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN
            ETEXT = '��� ����� ������'
            R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
        END
    END
    R.NEW(TT.TE.ACCOUNT.1) = '9949990010322301'
* MYDATE = TODAY
* CALL CDT('', MYDATE, '2W')
* R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
* R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE

    CALL REBUILD.SCREEN
    RETURN
END
