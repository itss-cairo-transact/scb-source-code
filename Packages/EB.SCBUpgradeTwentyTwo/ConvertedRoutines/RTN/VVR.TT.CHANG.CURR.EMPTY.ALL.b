* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
*----------------15-7-2002 ZOZO SCB-----------------------*

SUBROUTINE VVR.TT.CHANG.CURR.EMPTY.ALL
* IF THE USER CHANGES CURRENCY THEN EMPTY FIELDS AMOUNT.LOCAL.1 , AMOUNT.FCY.1 , NARRATIVE.2
* AND ACCOUNT.2
*CHECK IF CURRENCY.1 =LCCY , ACTIVATE AMOUNT.LOCAL & DEACTIVATE AMOUNT.FOREIGN
*CHECK IF CURRENCY.1 =FCCY , ACTIVATE AMOUNT.FOREIGN & DEACTIVATE AMOUNT.LOCAL
*COMI = CURRENCY.1
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

CURR=''
IF MESSAGE='VAL' THEN
    IF V$FUNCTION ='I' THEN  R.NEW(TT.TE.CURRENCY.1)= CURR

       IF R.NEW(TT.TE.CURRENCY.1) # CURR THEN

           R.NEW(TT.TE.AMOUNT.FCY.1)=''
           R.NEW(TT.TE.NARRATIVE.2)=''
           R.NEW(TT.TE.ACCOUNT.2)=''
           R.NEW(TT.TE.AMOUNT.LOCAL.1)=''
      END

END
ELSE
     IF R.NEW(TT.TE.CURRENCY.1) # CURR THEN

           R.NEW(TT.TE.AMOUNT.FCY.1)=''
           R.NEW(TT.TE.NARRATIVE.2)=''
           R.NEW(TT.TE.ACCOUNT.2)=''
           R.NEW(TT.TE.AMOUNT.LOCAL.1)=''
    END
END
*****************CHECK IF LCCY OR FCY CURRENCY*********************************************

 IF COMI = LCCY THEN
    T(TT.TE.AMOUNT.LOCAL.1)<3>=''
    T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
 END ELSE
     T(TT.TE.AMOUNT.FCY.1)<3>=''
     T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
  END
********************************************************************************************


RETURN
END
