* @ValidationCode : Mjo1OTEyMzkzMjA6Q3AxMjUyOjE2NDIyNTU4ODc3MTY6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 16:11:27
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN  -----

SUBROUTINE VVR.TT.CHK.AMOUNT.FCY.AUTH

*TO RETRY ENTERING THE AMOUNT ENTERED BY THE USER


*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 32 ] Adding I_F.CURRENCY, as it used on line 38- ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

    IF MESSAGE # 'VAL' THEN
        CURR = R.NEW(TT.TE.CURRENCY.1)
*Line [ 38 ] Update CCY.NAME to EB.CUR.CCY.NAME - ITSS - R21 Upgrade - 2021-12-23
*CALL DBR ('CURRENCY':@FM:CCY.NAME , CURR , DESC)
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME , CURR , DESC)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
DESC=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        IF COMI # R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> THEN
            ETEXT = '��� ����� ������'
            R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = COMI
        END
        CALL REBUILD.SCREEN
    END ELSE
*****UDATED IN 4/1/2007 BY NESSREEN AHMED******
*    IF R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> GT 20000 THEN ETEXT = '��� �� �� ���� ������ ���� �� 20000 � �'

    END

RETURN
END
