* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/08/22 ***
*******************************************

    SUBROUTINE VVR.TT.CHK.1005

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    WS.AC.ID = COMI

    IF PGM.VERSION EQ ",KS.CUSTOMER" THEN
        CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,ER.AC)
        WS.GL.ID = R.AC<AC.CATEGORY>
        WS.CY.ID = R.AC<AC.CURRENCY>
        WS.CU.ID = R.AC<AC.CUSTOMER>

*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR("CUSTOMER":@FM:EB.CUS.NATIONALITY,WS.CU.ID,CUS.NAT)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,WS.CU.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.NAT=R.ITSS.CUSTOMER<EB.CUS.NATIONALITY>
        IF CUS.NAT NE 'EG' THEN
            ETEXT='Egyptian Only';CALL STORE.END.ERROR
        END

        IF WS.GL.ID NE 1005 THEN
            ETEXT = "��� �� ���� ����� ��� 1005"
            CALL STORE.END.ERROR

        END  ELSE
            CALL F.READ(FN.CU,WS.CU.ID,R.CU,F.CU,ER.CU)

            SEC.ID  = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            ID.NO   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            COMM.NO = R.CU<EB.CUS.LOCAL.REF><1,CULR.COM.REG.NO>
**            LIC.ID.NO   = R.CU<EB.CUS.LOCAL.REF><1,CULR.TAX.EXEMPTION>

            IF SEC.ID EQ '4650'  THEN
                R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NSN.NO> = ID.NO
                R.NEW(TT.TE.NARRATIVE.2)<1,1>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            END  ELSE
                IF COMM.NO THEN

                    R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NSN.NO> = COMM.NO
                    R.NEW(TT.TE.NARRATIVE.2)<1,1>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                END ELSE

**                    R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NSN.NO> = LIC.ID.NO
                    R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NSN.NO>   = WS.CU.ID
                    R.NEW(TT.TE.NARRATIVE.2)<1,1>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                END
            END
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
