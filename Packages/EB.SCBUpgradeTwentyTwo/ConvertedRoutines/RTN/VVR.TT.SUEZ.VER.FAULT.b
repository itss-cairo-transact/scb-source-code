* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*****NESSREEN AHMED 30/10/2018
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.SUEZ.VER.FAULT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    IF COMI THEN
    *    FN.TELLER = 'FBNK.TELLER' ; F.TELLER ='' ; R.TELLER = ''
         FN.TELLER = 'FBNK.TELLER$NAU' ; F.TELLER ='' ; R.TELLER = ''
        CALL OPF(FN.TELLER,F.TELLER)
        CALL F.READ(FN.TELLER,COMI, R.TELLER, F.TELLER ,E1)
        IF NOT(E1) THEN
            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            FCY.AMT = LOCAL.REF<1,TTLR.PAID.AMT>
            R.NEW(TT.TE.CURRENCY.1)= R.TELLER<TT.TE.CURRENCY.1>
            CR.ACCT  = R.TELLER<TT.TE.ACCOUNT.1>
            CR.ACCT.1 = CR.ACCT[1,8]
            CR.ACCT.2 = CR.ACCT[11,6]
            CR.US.ACCT = CR.ACCT.1:"20":CR.ACCT.2
            R.NEW(TT.TE.AMOUNT.FCY.1) = FCY.AMT
            R.NEW(TT.TE.ACCOUNT.1)       = CR.US.ACCT
            R.NEW(TT.TE.CURRENCY.1)      = 'USD'
*            R.NEW(TT.TE.CUSTOMER.1)      = '99499900'
*            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.PAID.AMT> = LINK.DATA<3>
*            R.NEW(TT.TE.THEIR.REFERENCE) = LINK.DATA<4>

        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
