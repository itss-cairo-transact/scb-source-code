* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1025</Rating>
*-----------------------------------------------------------------------------
*--- 14/07/2002 RANIA KAMAL -------

*A ROUTINE TO CHECK THAT THE LENGHT OF ACCOUNT EQ 17 & MAKE SURE THAT IT IS NOT NOSTRO/INTERNAL A/C
*ALSO TO DEFAULT NARRATIVE.2 & CURRENCY  ACCORDING TO ACCOUNT AND MAKE CURRENCY NOINPUT FIELD
*ALSO IF THE CURRENCY IS LOCAL AND ONLINE.ACTUAL.BALANCE <=0 THEN DEFAULT VALUE.DATE.1,VALUE.DATE.2 & EXPOSURE.DATE BY
*TODAY'S DATE PLUS 1W ELSE TODAY'S DATE.
*IF THE CURRENCY IS FOREIGN THEN DEFAULT VALUE.DATE.1,VALUE.DATE.2 & EXPOSURE.DATE BY
*TODAY'S DATE PLUS 2W ELSE TODAY'S DATE.


    SUBROUTINE VVR.TT.CASHDPST

*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.NO=R.ITSS.TELLER.USER<1>

    IF V$FUNCTION = 'I' THEN

        IF INDEX(TT.NO,'98',1) > 1 THEN E = '��� ����� ��������'
        IF INDEX(TT.NO,'99',1) > 1 THEN E = '��� ����� ��������' ; CALL ERR ;MESSAGE = 'REPEAT'

    END

    IF V$FUNCTION = 'R' THEN

        IF INDEX(TT.NO,'98',1) < 1 THEN E = '��� ����� ��������'

    END
    IF E THEN CALL ERR ;MESSAGE = 'REPEAT'

***************************************************************************************************
*IF V$FUNCTION = 'I' THEN

    GOSUB INITIAL
    GOSUB ACCOUNT.CHECK       ;* IF ERROR = 'Y' THEN RETURN
    GOSUB CURRENCY.CHECK
    GOSUB DATE.CHECK          ;* IF ERROR = 'Y' THEN RETURN

*END
    GOTO PROGRAM.END
***************************************************************************************************
INITIAL:

*ERROR = ''
    CATEG = ''
    CUS.NAME = ''
    CURR.NAME = ''
    BALANCE = ''
    MYDATE = TODAY

    RETURN
***************************************************************************************************
ACCOUNT.CHECK:
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF 5000 LE CATEG AND CATEG LE 5999 THEN ETEXT = 'THIS.IS.NOSTRO.ACCOUNT';R.NEW (TT.TE.ACCOUNT.1)<1,AV> = ''
    IF LEN(COMI) # 16                THEN ETEXT='��� ����� ������ ��� ����';R.NEW (TT.TE.ACCOUNT.1) = ''
*ETEXT = 'NOT.SUITABLE.ACCOUNT.LENGHT'
    IF NOT(NUM(COMI))                THEN ETEXT='��� ���� ����� �����' ;R.NEW (TT.TE.ACCOUNT.1)<1,AV> = ''
*ETEXT = 'THIS.IS.INTERNAL.ACCOUNT'
    ELSE GOSUB ACCOUNT.DEFAULT
    CALL REBUILD.SCREEN

    RETURN
**************************************************************************************************
ACCOUNT.DEFAULT:

*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.SHORT.TITLE, COMI, CUS.NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.NAME=R.ITSS.ACCOUNT<AC.SHORT.TITLE>
    R.NEW (TT.TE.NARRATIVE.2)<1,AV> = CUS.NAME
*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.CURRENCY, COMI,CURR.NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.NAME=R.ITSS.ACCOUNT<AC.CURRENCY>
    R.NEW (TT.TE.CURRENCY.1) = CURR.NAME

    RETURN
**************************************************************************************************
CURRENCY.CHECK:

    IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT' ;T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
    ELSE T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT' ;T(TT.TE.AMOUNT.FCY.1)<3>=''

    RETURN
* *************************************************************************************************
DATE.CHECK:

*    CALL DBR( 'ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL, COMI ,BALANCE)
*   IF R.NEW (TT.TE.CURRENCY.1) = LCCY AND BALANCE <= 0 THEN CALL CDT('', MYDATE, '1W') ; R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE ;R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
*   IF R.NEW (TT.TE.CURRENCY.1) = LCCY AND BALANCE > 0 THEN  R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY ;R.NEW (TT.TE.VALUE.DATE.1) = TODAY
*    IF R.NEW (TT.TE.CURRENCY.1) # LCCY THEN CALL CDT('', MYDATE, '2W') ; R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE ;R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
******************Nessreen Ahmed*********************************************
    IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN
* ETEXT = 'LCCY' ; CALL REM
        CALL CDT('', MYDATE, '1W')
* TEXT = MYDATE ; CALL REM
**14/6/2004**
        R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
        R.NEW (TT.TE.VALUE.DATE.1) = MYDATE
*        R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY
*        R.NEW (TT.TE.VALUE.DATE.1) = TODAY
*****************************************************
    END ELSE
* TEXT = 'FCCY' ; CALL REM
        CALL CDT('', MYDATE, '2W')
* TEXT = MYDATE ; CALL REM
**14/6/2004**
        R.NEW (TT.TE.EXPOSURE.DATE.1) = MYDATE
        R.NEW (TT.TE.VALUE.DATE.1) = MYDATE

*        R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY
*        R.NEW (TT.TE.VALUE.DATE.1) = TODAY
********************************************************
    END
    CALL REBUILD.SCREEN
    RETURN
**************************************************************************************************

PROGRAM.END:

    RETURN
END
