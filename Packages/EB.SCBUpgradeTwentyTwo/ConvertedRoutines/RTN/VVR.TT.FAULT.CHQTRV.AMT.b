* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1462</Rating>
*-----------------------------------------------------------------------------
*--- 06-12-2009 NESSREEN AHMED -------

    SUBROUTINE VVR.TT.FAULT.CHQTRV.AMT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    ETEXT = ''; E = ''   ; CURR.NO = '' ; NEW.AMT = '' ; FCY.AMT = ''
**IF MESSAGE = '' THEN
**   IF COMI # R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> THEN
**       ETEXT = '��� ����� ������'
**       R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = COMI
**   END
**END
    IF COMI THEN
        NEW.AMT = COMI - 5
        R.NEW(TT.TE.AMOUNT.FCY.1) = NEW.AMT
        CALL REBUILD.SCREEN
    END
    FCY.AMT = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
    IF COMI # FCY.AMT THEN
        R.NEW(TT.TE.AMOUNT.FCY.1) = ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
    END

    CALL REBUILD.SCREEN

    RETURN
END
