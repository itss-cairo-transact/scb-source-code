* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>2850</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.UPDATE.ARABIC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    IF V$FUNCTION = 'I' THEN
        TT =  R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ARABIC.NAME>
        TT1 = TRIM(TT)
        LEN.R.NEW = LEN( TT1)
        IF FIELD(TT1," ",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1,".",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1,"/",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1,"/",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1,"/",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1,"/",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
        IF FIELD(TT1,"/",1) = "�" THEN TT1 = TT1[2,LEN(TT1)]
*--------------------------------------------------------

        IF FIELD(TT1," ",1) = "������"  THEN
            IF FIELD(TT1," ",2) ="/" THEN
                TT1 = CHANGE(TT1, "������" , SPACE(1))
            END
        END
        IF FIELD(TT1," ",1)= "������/" THEN
            TT1 = CHANGE(TT1, "������" , SPACE(1))
        END

        IF FIELD(TT1," ",1) = "������"  THEN
            IF FIELD(TT1," ",2) ="/" THEN
                TT1 = CHANGE(TT1, "������" , SPACE(1))
            END
        END

        IF FIELD(TT1," ",1)= "������/" THEN
            TT1 = CHANGE(TT1, "������" , SPACE(1))
        END

*----------------------------------------------------

        IF FIELD(TT1," ",1) = "�����"  THEN
            IF FIELD(TT1," ",2) ="/" THEN
                TT1 = CHANGE(TT1, "�����" , SPACE(1))
            END
        END

        IF FIELD(TT1," ",1)="�����/" THEN
            TT1 = CHANGE(TT1, "�����" , SPACE(1))
        END
*----------------------------------------------------
        FOR I = 1 TO LEN.R.NEW
            CHARACTER = TT1[ I, 1]
            CONVERT CHAR(49):CHAR(48):CHAR(33):CHAR(34):CHAR(35):CHAR(36):CHAR(37):CHAR(39):CHAR(40):CHAR(41):CHAR(42):CHAR(43):CHAR(44):CHAR(45):CHAR(46):CHAR(47):CHAR(58):CHAR(59):CHAR(60):CHAR(61):CHAR(62):CHAR(63):CHAR(64):CHAR(91):CHAR(92):CHAR(93):CHAR(95):CHAR(96):CHAR(123):CHAR(124):CHAR(125):CHAR(126) TO CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32):CHAR(32)  IN TT1
            CONVERT CHAR( 194):CHAR( 195):CHAR( 197):CHAR( 201):CHAR( 236):CHAR( 220) TO CHAR( 199):CHAR( 199):CHAR( 199):CHAR( 229):CHAR( 237) IN TT1
        NEXT I

        IF FIELD(TT1," ",1) = "�������" THEN TT1 = TT1[8,LEN(TT1)]
        IF FIELD(TT1," ",1) = "���" THEN TT1 = TT1[4,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�����" THEN TT1 = TT1[6,LEN(TT1)]
        IF FIELD(TT1," ",1) = "������" THEN TT1 = TT1[7,LEN(TT1)]
        IF FIELD(TT1," ",1) = "�����" THEN TT1 = TT1[6,LEN(TT1)]
        IF FIELD(TT1," ",1) = "������" THEN TT1 = TT1[7,LEN(TT1)]
        TT = CHANGE(TT1, "�������" , SPACE(1))
        TT1 = CHANGE(TT, "������" , SPACE(1))
        TT = CHANGE(TT1, "��������" , SPACE(1))
        TT1 = CHANGE(TT, "�������" , SPACE(1))
        TT = CHANGE(TT1, "��������" , SPACE(1))
        TT1 = CHANGE(TT, "�������" , SPACE(1))
        TT = CHANGE(TT1, "���� ��������" , SPACE(1))
        TT1 = CHANGE(TT, "���� �������" , SPACE(1))
        TT = CHANGE(TT1, "��������" , SPACE(1))
        TT1 = CHANGE(TT, "�������" , SPACE(1))
        TT = CHANGE(TT1, "�����" , SPACE(1))
        TT1 = CHANGE(TT, "����" , SPACE(1))
        TT = CHANGE(TT1, "������" , SPACE(1))
        TT1 = CHANGE(TT, "�����" , SPACE(1))
        TT = CHANGE(TT1, "��������" , SPACE(1))
* IF FIELD(TT," ",1) = "�������" THEN TT1 = TT1[8,LEN(TT1)]
* IF FIELD(TT1," ",1) = "���" THEN TT1 = TT1[4,LEN(TT1)]
* IF FIELD(TT1," ",1) = "�����" THEN TT1 = TT1[6,LEN(TT1)]
        TT1 = CHANGE(TT, "���� �������" , SPACE(1))
        TT = CHANGE(TT1, "���� ��������" , SPACE(1))
        TT1 = CHANGE(TT,"������",SPACE(1))
        TT = CHANGE(TT1,"������",SPACE(1))
        TT1 = CHANGE(TT,"����",SPACE(1))
        TT = CHANGE(TT1,"����",SPACE(1))
        TT1 = CHANGE(TT,"����",SPACE(1))
* IF FIELD(TT1," ",1) = "������" THEN TT1 = TT1[7,LEN(TT1)]
* IF FIELD(TT1," ",1) = "�����" THEN TT1 = TT1[6,LEN(TT1)]
        TT = CHANGE(TT1,"��������",SPACE(1))
        TT1 = CHANGE(TT,"������",SPACE(1))
        TT = CHANGE(TT1,"����",SPACE(1))
        TT1 = CHANGE(TT,"������",SPACE(1))
        TT = CHANGE(TT1,"����",SPACE(1))
        TT1 = CHANGE(TT,"������",SPACE(1))
        TT = CHANGE(TT1,"������",SPACE(1))
* IF FIELD(TT1," ",1) = "������" THEN TT1 = TT1[7,LEN(TT1)]
        IF TT1 = "" THEN TT1 = "�������"
        R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ARABIC.NAME> = TRIM(TT1)

*      FN.CUSTOMER = 'FBNK.CUSTOMER$NAU' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
*     CALL OPF( FN.CUSTOMER,F.CUSTOMER)
*    CALL F.READ(FN.CUSTOMER,ID.NEW, R.CUSTOMER,F.CUSTOMER, ETEXT)
*   OLDCUST = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
*****************
*  R.NEW(EB.CUS.MNEMONIC)='CI':OLDCUST
****************UPDATED BY RIHAM R15************************
        CUS.BR = R.NEW(EB.CUS.COMPANY.BOOK)[8,2]
        AC.OFICER = TRIM(CUS.BR, "0" , "L")


        IF AC.OFICER = "1" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='CA':OLDCUST
        END
        IF AC.OFICER = "2" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='HE':OLDCUST
        END
        IF AC.OFICER = "3" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='GI':OLDCUST
        END
        IF AC.OFICER = "4" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='MO':OLDCUST
        END
        IF AC.OFICER = "5" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='MA':OLDCUST
        END
        IF AC.OFICER = "6" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='AB':OLDCUST
        END
        IF AC.OFICER = "7" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='AR':OLDCUST
        END
        IF AC.OFICER = "9" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='FO':OLDCUST
        END
        IF AC.OFICER = "10" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='NA':OLDCUST
        END
        IF AC.OFICER = "11" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='DO':OLDCUST
        END
        IF AC.OFICER = "12" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='SP':OLDCUST
        END
        IF AC.OFICER = "13" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='GR':OLDCUST
        END
        IF AC.OFICER = "14" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='OC':OLDCUST
        END
        IF AC.OFICER = "15" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='UC':OLDCUST
        END
        IF AC.OFICER = "20" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='AL':OLDCUST
        END
        IF AC.OFICER = "21" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='BU':OLDCUST
        END
        IF AC.OFICER = "22" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
*Line [ 205 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            R.NEW(EB.CUS.MNEMONIC)='@SM':OLDCUST
        END
        IF AC.OFICER = "30" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='SI':OLDCUST
        END
        IF AC.OFICER = "35" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='SH':OLDCUST
        END
        IF AC.OFICER = "40" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='IS':OLDCUST
        END
        IF AC.OFICER = "50" THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='SU':OLDCUST
        END
        IF AC.OFICER = 60 THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='TA':OLDCUST
        END
        IF AC.OFICER = 70 THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='RA':OLDCUST
        END
        IF AC.OFICER = 90 THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='SA':OLDCUST
        END
        IF AC.OFICER = 23 THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='AM':OLDCUST
        END

        IF AC.OFICER = 77 THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='TS':OLDCUST
        END
        IF AC.OFICER = 55 THEN
            OLDCUST = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.OLD.CUST.ID>
            R.NEW(EB.CUS.MNEMONIC)='UT':OLDCUST
        END
    END
    RETURN
END
